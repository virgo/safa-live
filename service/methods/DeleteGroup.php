<?php

//$class = end(explode('/', str_replace('.php', '', __FILE__)));

class DeleteGroup extends Structure {
    function register() {
        parent::$server->register(__CLASS__,
            array('session' => 'xsd:string', 'group_id' => 'xsd:int'),
            array('status'=>'xsd:int', 'result' => 'xsd:string'),
            $this->namespace,                         // namespace
            $this->namespace . '#'.__CLASS__,       // soapaction
            'rpc',                // style
            'encoded',            // use
            'GetCities'        // documentation
          );
    }
    function run($hash, $group_id) {
        $access = $this->model->access($hash);
        if($access['status'] == 0)
            return $access;
        return $this->operations($access, $group_id);
    }
    
    function operations($license, $group_id){
        $rows = R::getAll('SELECT * FROM safa_groups WHERE safa_group_id = "'.$group_id.'" AND safa_ea_id = "'.$license['safa_ea_id'].'"');
        if( ! $rows) return array('status' => (int) 4, 'result' => "No groups available");
        R::exec('DELETE FROM safa_group_passports WHERE safa_group_id = "'.$group_id.'"');
        R::exec('DELETE FROM safa_groups WHERE safa_group_id = "'.$group_id.'"');
        return array('status' => (int) 1, 'result' => "Done");
    }
}

$DeleteGroup = new DeleteGroup();
$DeleteGroup->register();

function DeleteGroup($hash, $group_id)
{
    return $GLOBALS['DeleteGroup']->run($hash, $group_id);
}
