<?php

class InsertTrip extends Structure {
    function request() {
        //=====================================================================
        // trip_tourismplaces
        //=====================================================================
        parent::$server->wsdl->addComplexType('trip_tourismplace', 'complexType', 'struct', 'all', '', array(
            'datetime' => array('name' => 'datetime', 'type' => 'xsd:string'),
            'safa_tourismplace_id' => array('name' => 'safa_tourismplace_id', 'type' => 'xsd:int'),
        ));
        parent::$server->wsdl->addComplexType(
                'trip_tourismplaces', 'complexType', 'array', '', 'SOAP-ENC:Array', array(), array(
            array('ref' => 'SOAP-ENC:arrayType', 'wsdl:arrayType' => 'tns:trip_tourismplace[]')), 'tns:trip_tourismplace'
        );
        
        //=====================================================================
        // internalsegments
        //=====================================================================
        parent::$server->wsdl->addComplexType('internalsegment', 'complexType', 'struct', 'all', '', array(
            'safa_internalsegmenttype_id' => array('name' => 'safa_internalsegmenttype_id', 'type' => 'xsd:int'),
            'safa_trip_internaltrip_id' => array('name' => 'safa_trip_internaltrip_id', 'type' => 'xsd:int'),
            'safa_internalsegmentestatus_id' => array('name' => 'safa_internalsegmentestatus_id', 'type' => 'xsd:int'),
            'start_datetime' => array('name' => 'start_datetime', 'type' => 'xsd:string'),
            'end_datetime' => array('name' => 'end_datetime', 'type' => 'xsd:string'),
            'erp_port_id' => array('name' => 'erp_port_id', 'type' => 'xsd:int'),
            'erp_start_hotel_id' => array('name' => 'erp_start_hotel_id', 'type' => 'xsd:int'),
            'erp_end_hotel_id' => array('name' => 'erp_end_hotel_id', 'type' => 'xsd:int'),
            'safa_tourism_place_id' => array('name' => 'safa_tourism_place_id', 'type' => 'xsd:int'),
            'seats_count' => array('name' => 'seats_count', 'type' => 'xsd:int'),
            'notes' => array('name' => 'notes', 'type' => 'xsd:string'),
        ));
        parent::$server->wsdl->addComplexType(
                'internalsegments', 'complexType', 'array', '', 'SOAP-ENC:Array', array(), array(
            array('ref' => 'SOAP-ENC:arrayType', 'wsdl:arrayType' => 'tns:internalsegment[]')), 'tns:internalsegment'
        );
        //=====================================================================
        // trip_supervisors
        //=====================================================================
        parent::$server->wsdl->addComplexType('trip_supervisor', 'complexType', 'struct', 'all', '', array(
            'name_ar' => array('name' => 'name_ar', 'type' => 'xsd:string'),
            'name_la' => array('name' => 'name_la', 'type' => 'xsd:string'),
            'phone' => array('name' => 'phone', 'type' => 'xsd:string'),
            'mobile' => array('name' => 'mobile', 'type' => 'xsd:string'),
            'mobile_sa' => array('name' => 'mobile_sa', 'type' => 'xsd:string'),
            'notes' => array('name' => 'notes', 'type' => 'xsd:string'),
        ));
        parent::$server->wsdl->addComplexType(
                'trip_supervisors', 'complexType', 'array', '', 'SOAP-ENC:Array', array(), array(
            array('ref' => 'SOAP-ENC:arrayType', 'wsdl:arrayType' => 'tns:trip_supervisor[]')), 'tns:trip_supervisor'
        );
        //=====================================================================
        // trip_internaltrip
        //=====================================================================
        parent::$server->wsdl->addComplexType('trip_internaltrip', 'complexType', 'struct', 'all', '', array(
            'safa_ito_id' => array('name' => 'safa_ito_id', 'type' => 'xsd:int'),
            'safa_tripstatus_id' => array('name' => 'safa_tripstatus_id', 'type' => 'xsd:int'),
            'operator_reference' => array('name' => 'operator_reference', 'type' => 'xsd:string'),
            'attachement' => array('name' => 'attachement', 'type' => 'xsd:string'),
            'datetime' => array('name' => 'datetime', 'type' => 'xsd:string'),
            'internalsegments' => array('name' => 'internalsegments', 'type' => 'tns:internalsegments'),
        ));
        parent::$server->wsdl->addComplexType(
                'trip_internaltrips', 'complexType', 'array', '', 'SOAP-ENC:Array', array(), array(
            array('ref' => 'SOAP-ENC:arrayType', 'wsdl:arrayType' => 'tns:trip_internaltrip[]')
                ), 'tns:trip_internaltrip'
        );
        
        //=====================================================================
        // trip_hotels
        //=====================================================================
        parent::$server->wsdl->addComplexType('trip_hotel', 'complexType', 'struct', 'all', '', array(
            'nights_count' => array('name' => 'nights_count', 'type' => 'xsd:int'),
            'erp_hotel_id' => array('name' => 'erp_hotel_id', 'type' => 'xsd:int'),
            'erp_meal_id' => array('name' => 'erp_meal_id', 'type' => 'xsd:int'),
            'checkin_datetime' => array('name' => 'checkin_datetime', 'type' => 'xsd:string'),
            'checkout_datetime' => array('name' => 'checkout_datetime', 'type' => 'xsd:string'),
            'trip_hotel_rooms' => array('name' => 'trip_hotel_rooms', 'type' => 'tns:trip_hotel_rooms'),
        ));
        parent::$server->wsdl->addComplexType(
                'trip_hotels', 'complexType', 'array', '', 'SOAP-ENC:Array', array(), array(
            array('ref' => 'SOAP-ENC:arrayType', 'wsdl:arrayType' => 'tns:trip_hotel[]')
                ), 'tns:trip_hotel'
        );
        //=====================================================================
        // trip_hotel_rooms
        //=====================================================================
        parent::$server->wsdl->addComplexType('trip_hotel_room', 'complexType', 'struct', 'all', '', array(
            'erp_hotelroomsize_id' => array('name' => 'erp_hotelroomsize_id', 'type' => 'xsd:int'),
            'rooms_count' => array('name' => 'rooms_count', 'type' => 'xsd:int')
        ));
        parent::$server->wsdl->addComplexType(
                'trip_hotel_rooms', 'complexType', 'array', '', 'SOAP-ENC:Array', array(), array(
            array('ref' => 'SOAP-ENC:arrayType', 'wsdl:arrayType' => 'tns:trip_hotel_room[]')
                ), 'tns:trip_hotel_room'
        );
        //=====================================================================
        // trip_externalsegments
        //=====================================================================
        parent::$server->wsdl->addComplexType('trip_externalsegment', 'complexType', 'struct', 'all', '', array(
            'start_port_hall_id' => array('name' => 'start_port_hall_id', 'type' => 'xsd:int'),
            'end_port_hall_id' => array('name' => 'end_port_hall_id', 'type' => 'xsd:int'),
            'departure_datetime' => array('name' => 'departure_datetime', 'type' => 'xsd:string'),
            'arrival_datetime' => array('name' => 'arrival_datetime', 'type' => 'xsd:string'),
            'safa_transporter_id' => array('name' => 'safa_transporter_id', 'type' => 'xsd:int'),
            'safa_externalsegmenttype_id' => array('name' => 'safa_externalsegmenttype_id', 'type' => 'xsd:int'),
            'trip_code' => array('name' => 'trip_code', 'type' => 'xsd:string'),
        ));
        parent::$server->wsdl->addComplexType(
                'trip_externalsegments', 'complexType', 'array', '', 'SOAP-ENC:Array', array(), array(
            array('ref' => 'SOAP-ENC:arrayType', 'wsdl:arrayType' => 'tns:trip_externalsegment[]')
                ), 'tns:trip_externalsegment'
        );
        //=====================================================================
        // TRIP
        //=====================================================================
        parent::$server->wsdl->addComplexType('trip', 'complexType', 'struct', 'all', '', array(
            'safa_trip_id' => array('name' => 'safa_trip_id', 'type' => 'xsd:int'),
            'remove_travellers' => array('name' => 'remove_travellers', 'type' => 'xsd:int'),
            'name' => array('name' => 'name', 'type' => 'xsd:string'),
            'date' => array('name' => 'date', 'type' => 'xsd:string'),
            'safa_tripstatus_id' => array('name' => 'safa_tripstatus_id', 'type' => 'xsd:int'),
            'safa_trip_confirm_id' => array('name' => 'safa_trip_confirm_id', 'type' => 'xsd:int'),
            'erp_transportertype_id' => array('name' => 'erp_transportertype_id', 'type' => 'xsd:int'),
            // ARRAYS
            'trip_externalsegments' => array('name' => 'trip_externalsegments', 'type' => 'tns:trip_externalsegments'),
            'trip_hotels' => array('name' => 'trip_hotels', 'type' => 'tns:trip_hotels'),
            'trip_internaltrip' => array('name' => 'trip_internaltrip', 'type' => 'tns:trip_internaltrip'),
            'trip_supervisors' => array('name' => 'trip_supervisors', 'type' => 'tns:trip_supervisors'),
            'trip_tourismplaces' => array('name' => 'trip_tourismplaces', 'type' => 'tns:trip_tourismplaces'),
        ));
    }
    function register() {
        parent::$server->register("InsertTrip", 
                array('username' => 'xsd:string', 'password' => 'xsd:string', 'trip' => 'tns:trip'), 
                array('status' => 'xsd:int', 'result' => 'xsd:string'), $this->namespace, // namespace
                $this->namespace . '#InsertTrip', // soapaction
                'rpc', // style
                'encoded', // use
                'InsertTrip'        // documentation
        );
    }
    function run($username, $password, $trip) {
        $contract = $this->model->login($username, $password);
        if ( ! $contract) { return array('status' => (int) 2, 'result' => "Invalid access information");}
        $result['status'] = (int) 1;

        // INSERT TRIP
        if($trip['safa_trip_id']) {
            $this->delete_trip_related_files($trip['safa_trip_id'], $trip['remove_travellers']);
            $result['result'] = $trip['safa_trip_id'];
        }
        else
        {
            $result = $this->Trip($trip, $contract);
        }
        
        if(isset($trip['trip_externalsegments']))
        $this->externalsegments($trip['trip_externalsegments'], $id);

        if(isset($trip['trip_hotels']))
        $this->hotels($trip['trip_hotels'], $id);
        
        if(isset($trip['trip_internaltrip']))
        $this->internaltrip($trip['trip_internaltrip'], $id);
        
        if(isset($trip['trip_supervisors']))
        $this->supervisors($trip['trip_supervisors'], $id);
        
        if(isset($trip['trip_tourismplaces']))
        $this->tourismplaces($trip['trip_tourismplaces'], $id);
        
        return $result;
    }
    function Trip($trip_details, $contract) {
        if(isset($trip_details['safa_trip_id']) && $trip_details['safa_trip_id'] > 0)
        {
            $this->delete_trip_related_files($trip_details['safa_trip_id']);
            R::exec("UPDATE safa_trips SET 
                `name` = '".$trip_details['name']."',
                `date` = '".$trip_details['date']."',
                `safa_tripstatus_id` = '".$trip_details['safa_tripstatus_id']."',
                `safa_trip_confirm_id` = '".$trip_details['safa_trip_confirm_id']."',
                `erp_transportertype_id` = '".$trip_details['erp_transportertype_id']."'
            WHERE safa_trip_id='".$trip_details['safa_trip_id']."'");
            $trip_id = $trip_details['safa_trip_id'];
        }
        else
        {
            
            $safa_uo_contracts_eas = R::getRow("SELECT * FROM safa_uo_contracts_eas WHERE safa_uo_contract_id = '".$contract['safa_uo_contract_id']."' LIMIT 1");
            $package = R::getRow("SELECT * FROM `safa_ea_packages` WHERE safa_uo_contract_ea_id = '".$safa_uo_contracts_eas['safa_uo_contract_ea_id']."' LIMIT 1");
            $_trip['safa_ea_package_id'] =  $package['safa_ea_package_id'];
            $_trip['name'] =  $trip_details['name'];
            $_trip['date'] =  $trip_details['date'];
            $_trip['safa_tripstatus_id'] =  $trip_details['safa_tripstatus_id'];
            $_trip['safa_trip_confirm_id'] =  $trip_details['safa_trip_confirm_id'];
            $_trip['erp_transportertype_id'] =  $trip_details['erp_transportertype_id'];
            $trip_id = $this->insert('safa_trips', $_trip);
        }
        return $trip_id;
    }
    function externalsegments($trip_externalsegments, $id) {
        if (isset($trip_externalsegments) && is_array($trip_externalsegments) && count($trip_externalsegments)) {
            foreach ($trip_externalsegments as $externalsegments) {
                //insert
                $externalsegments['safa_trip_id'] = $id;
                $externalsegments_id = $this->insert('safa_trip_externalsegments', $externalsegments);
            }
        }
    }
    function hotels($trip_hotels, $id) {
        if (isset($trip_hotels) && is_array($trip_hotels) && count($trip_hotels)) {
            foreach ($trip_hotels as $hotel) {
                $_hotel = new stdClass();
                $_hotel->erp_hotel_id = $hotel['erp_hotel_id'];
                $_hotel->erp_meal_id = $hotel['erp_meal_id'];
                $_hotel->safa_trip_id = $id;
                $_hotel->nights_count = $hotel['nights_count'];
                $_hotel->checkin_datetime = $hotel['checkin_datetime'];
                $_hotel->checkout_datetime = $hotel['checkout_datetime'];
                $_hotel_id = $this->insert('safa_trip_hotels', $_hotel);
                if(isset($hotel['trip_hotel_rooms']) && count($hotel['trip_hotel_rooms']) && is_array($hotel['trip_hotel_rooms']))
                $this->hotel_rooms($hotel['trip_hotel_rooms'], $_hotel_id);
            }
        }
    }
    function hotel_rooms($trip_hotel_rooms, $id) {
        if (isset($trip_hotel_rooms) && is_array($trip_hotel_rooms) && count($trip_hotel_rooms)) {
            foreach ($trip_hotel_rooms as $hotel_rooms) {
                $_hotel_rooms = new stdClass();
                $_hotel_rooms->safa_trip_hotel_id = $id;
                $_hotel_rooms->erp_hotelroomsize_id = $hotel_rooms['erp_hotelroomsize_id'];
                $_hotel_rooms->rooms_count = $hotel_rooms['rooms_count'];
                $id = $this->insert('safa_trip_hotel_rooms', $_hotel_rooms);
            }
        }
    }
    function internaltrip($trip_internaltrip, $id) {
        if (isset($trip_internaltrip) && is_array($trip_internaltrip) && count($trip_internaltrip)) {
            foreach ($trip_internaltrip as $internaltrip) {
                $_internaltrip = new stdClass();
                $_internaltrip->safa_trip_id = $id;
                $_internaltrip->safa_ito_id = $internaltrip['safa_ito_id'];
                $_internaltrip->safa_tripstatus_id = $internaltrip['safa_tripstatus_id'];
                $_internaltrip->operator_reference = $internaltrip['operator_reference'];
                $_internaltrip->datetime = $internaltrip['datetime'];
                $_internaltrip_id = $this->insert('safa_trip_internaltrips', $_internaltrip);
                $this->internalsegments($internaltrip['internalsegments'], $_internaltrip_id);
            }
        }
    }
    function supervisors($trip_supervisors, $id) {
        if (isset($trip_supervisors) && is_array($trip_supervisors) && count($trip_supervisors)) {
            foreach ($trip_supervisors as $supervisors) {
                $_supervisors = new stdClass();
                $_supervisors->name_ar = $supervisors['name_ar'];
                $_supervisors->name_la = $supervisors['name_la'];
                $_supervisors->phone = $supervisors['phone'];
                $_supervisors->mobile = $supervisors['mobile'];
                $_supervisors->mobile_sa = $supervisors['mobile_sa'];
                $_supervisors->notes = $supervisors['notes'];
                $_supervisors->safa_trip_id = $id;
                $_supervisors_id = $this->insert('safa_trip_supervisors', $_supervisors);
            }
        }
    }
    function tourismplaces($trip_tourismplaces, $id) {
        if (isset($trip_tourismplaces) && is_array($trip_tourismplaces) && count($trip_tourismplaces)) 
        {
            foreach ($trip_tourismplaces as $tourismplaces)
            {
                $_tourismplaces = new stdClass();
                $_tourismplaces->safa_trip_id = $id;
                $_tourismplaces->safa_tourismplace_id = $tourismplaces['safa_tourismplace_id'];
                $_tourismplaces->datetime = $tourismplaces['datetime'];
                $_tourismplaces_id = $this->insert('safa_trip_tourismplaces', $_tourismplaces);
            }
        }
    }
    function internalsegments($internalsegments, $id) {
        if (isset($internalsegments) && is_array($internalsegments) && count($internalsegments)) {
            foreach ($internalsegments as $internalsegment) {
                $_internalsegment = new stdClass();
                $_internalsegment->safa_internalsegmenttype_id = $internalsegment['safa_internalsegmenttype_id'];
                $_internalsegment->safa_trip_internaltrip_id = $id;
                $_internalsegment->safa_internalsegmentestatus_id = $internalsegment['safa_internalsegmentestatus_id'];
                $_internalsegment->start_datetime = $internalsegment['start_datetime'];
                $_internalsegment->end_datetime = $internalsegment['end_datetime'];
                $_internalsegment->erp_port_id = $internalsegment['erp_port_id'];
                $_internalsegment->erp_start_hotel_id = $internalsegment['erp_start_hotel_id'];
                $_internalsegment->erp_end_hotel_id = $internalsegment['erp_end_hotel_id'];
                $_internalsegment->safa_tourism_place_id = $internalsegment['safa_tourism_place_id'];
                $_internalsegment->seats_count = $internalsegment['seats_count'];
                $_internalsegment->notes = $internalsegment['notes'];
                $_internalsegment_id = $this->insert('safa_internalsegments', $_internalsegment);
            }
        }
    }
    function delete_trip_related_files($id, $remove_travellers) {
        if($remove_travellers)
        R::exec("DELETE FROM safa_trip_travellers WHERE safa_trip_id = '".$id."'");
        foreach(R::getAll("SELECT * FROM safa_trip_internaltrips WHERE  safa_trip_id = '".$id."'") as $segment)
        R::exec("DELETE FROM safa_internalsegments WHERE safa_trip_internaltrip_id = '".$id."'");
        R::exec("DELETE FROM safa_trip_internaltrips WHERE safa_trip_id = '".$id."'");
        R::exec("DELETE FROM safa_trip_tourismplaces WHERE safa_trip_id = '".$id."'");
        R::exec("DELETE FROM safa_trip_supervisors WHERE safa_trip_id = '".$id."'");
        foreach(R::getAll("SELECT * FROM safa_trip_hotels WHERE safa_trip_id = '".$id."'") as $row)
        R::exec("DELETE FROM safa_trip_hotel_rooms WHERE safa_trip_hotel_id = '".$row['safa_trip_hotel_id']."'");
        R::exec("DELETE FROM safa_trip_hotels WHERE safa_trip_id = '".$id."'");
        R::exec("DELETE FROM safa_trip_externalsegments WHERE safa_trip_id = '".$id."'");
        R::exec("DELETE FROM safa_trips WHERE safa_trip_id = '".$id."'");
    }
 }

$insertTrip = new InsertTrip();
$insertTrip->request();
$insertTrip->register();
function InsertTrip($username, $password, $trip) {
    global $insertTrip;
    return $insertTrip->run($username, $password, $trip);
}