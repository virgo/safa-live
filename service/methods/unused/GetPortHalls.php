<?php

class GetPortHalls extends Structure{
    function response() {
        parent::$server->wsdl->addComplexType('erp_port_hall','complexType','struct','all','',
            array( 'erp_port_hall_id' => array('name' => 'erp_port_hall_id','type' => 'xsd:int'),
            'erp_port_id' => array('name' => 'erp_port_id','type' => 'xsd:int'),
            'code' => array('name' => 'code','type' => 'xsd:string'),
            'name_ar' => array('name' => 'name_ar','type' => 'xsd:string'),
            'name_la' => array('name' => 'name_la','type' => 'xsd:string')
        ));
        parent::$server->wsdl->addComplexType(
            'erp_port_halls',
            'complexType',
            'array',
            '',
            'SOAP-ENC:Array',
            array(),
            array(
                array('ref'=>'SOAP-ENC:arrayType','wsdl:arrayType'=>'tns:erp_port_hall[]')
            ),
            'tns:erp_port_hall'
        );
    }
    function register() {
        parent::$server->register("GetPortHalls",
            array('username' => 'xsd:string', 'password' => 'xsd:string'),
            array('status'=>'xsd:int', 'erp_port_halls'=>'tns:erp_port_halls', 'result' => 'xsd:string'),
            $this->namespace,                         // namespace
            $this->namespace . '#GetPortHalls',       // soapaction
            'rpc',                // style
            'encoded',            // use
            'GetPortHalls'        // documentation
          );
    }
    function run($username, $password) {
        $contract = $this->model->login($username, $password);
        if ( ! $contract) { return array('status' => (int) 2, 'result' => "Invalid access information");}
        $result['status'] = (int) 1;
        $result['erp_port_halls'] = R::getAll("SELECT * FROM `erp_port_halls`");
        return $result;
    }
}
$GetAirportHalls = new GetPortHalls();
$GetAirportHalls->response();
$GetAirportHalls->register();
function GetPortHalls($username, $password) { global $GetAirportHalls;
    return $GetAirportHalls->run($username, $password);
}