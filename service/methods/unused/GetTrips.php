<?php

class GetTrips extends Structure{
    function response() {
        parent::$server->wsdl->addComplexType('safa_trip','complexType','struct','all','',
            array( 'safa_trip_id' => array('name' => 'safa_trip_id','type' => 'xsd:int'),
            'name' => array('name' => 'name_ar','type' => 'xsd:string'),
            ));
        parent::$server->wsdl->addComplexType(
            'safa_trips',
            'complexType',
            'array',
            '',
            'SOAP-ENC:Array',
            array(),
            array(
                array('ref'=>'SOAP-ENC:arrayType','wsdl:arrayType'=>'tns:safa_trip[]')
            ),
            'tns:safa_trip'
        );
    }
    function register() {
        parent::$server->register("GetTrips",
            array('username' => 'xsd:string', 'password' => 'xsd:string'),
            array('status'=>'xsd:int', 'safa_trips'=>'tns:safa_trips'),
            $this->namespace,                         // namespace
            $this->namespace . '#GetTrips',       // soapaction
            'rpc',                // style
            'encoded',            // use
            'GetTrips'        // documentation
          );
    }
    function run($username, $password) {
        $contract = $this->model->login($username, $password);
        if ( ! $contract) { return array('status' => (int) 2, 'result' => "Invalid access information");}
        $result['status'] = (int) 1;
        $result['safa_trips'] = R::getAll("SELECT safa_trips.safa_trip_id, safa_trips.name FROM safa_trips 
            JOIN safa_ea_packages ON safa_ea_packages.safa_ea_package_id = safa_trips.safa_ea_package_id
            JOIN safa_uo_contracts_eas ON safa_uo_contracts_eas.safa_uo_contract_ea_id = safa_ea_packages.safa_uo_contract_ea_id
            WHERE safa_uo_contracts_eas.safa_uo_contract_id = '".$contract['safa_uo_contract_id']."'");
        return $result;
    }
}
$GetTrips = new GetTrips();
$GetTrips->response();
$GetTrips->register();
function GetTrips($username, $password) { global $GetTrips;
    return $GetTrips->run($username, $password);
}
