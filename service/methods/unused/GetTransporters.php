<?php

class GetTransporters extends Structure{
    function response() {
        parent::$server->wsdl->addComplexType('transporter','complexType','struct','all','',
        array( 
            'safa_transporter_id' => array('name' => 'safa_transporter_id','type' => 'xsd:int'),
            'erp_country_id' => array('name' => 'erp_country_id','type' => 'xsd:int'),
            'erp_transportertype_id' => array('name' => 'erp_transportertype_id','type' => 'xsd:int'),
            'name_ar' => array('name' => 'name_ar','type' => 'xsd:string'),
            'name_la' => array('name' => 'name_la','type' => 'xsd:string'),
            'code' => array('name' => 'code','type' => 'xsd:string'),
        ));
        parent::$server->wsdl->addComplexType(
            'transporters',
            'complexType',
            'array',
            '',
            'SOAP-ENC:Array',
            array(),
            array(
                array('ref'=>'SOAP-ENC:arrayType','wsdl:arrayType'=>'tns:transporter[]')
            ),
            'tns:transporter'
        );
    }
    function register() {
        parent::$server->register("GetTransporters",
            array('username' => 'xsd:string', 'password' => 'xsd:string'),
            array('status'=>'xsd:int', 'transporters'=>'tns:transporters', 'result' => 'xsd:string'),
            $this->namespace,                         // namespace
            $this->namespace . '#GetTransporters',       // soapaction
            'rpc',                // style
            'encoded',            // use
            'GetTransporters'        // documentation
          );
    }
    function run($username, $password) {
        $contract = $this->model->login($username, $password);
        if ( ! $contract) { return array('status' => (int) 2, 'result' => "Invalid access information");}
        $result['status'] = (int) 1;
        $result['transporters'] = R::getAll("SELECT * FROM `safa_transporters`");
        return $result;
    }
}
$GetTransporters = new GetTransporters();
$GetTransporters->response();
$GetTransporters->register();
function GetTransporters($username, $password) { global $GetTransporters;
    return $GetTransporters->run($username, $password);
}