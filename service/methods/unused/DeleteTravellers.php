<?php

class DeleteTravellers extends Structure {
    function register() {
        parent::$server->register("DeleteTravellers", 
                array('username' => 'xsd:string', 'password' => 'xsd:string', 'safa_trip_id' => 'xsd:int'), 
                array('status' => 'xsd:int', 'result' => 'xsd:string'), $this->namespace, // namespace
                $this->namespace . '#DeleteTravellers', // soapaction
                'rpc', // style
                'encoded', // use
                'DeleteTravellers'        // documentation
        );
    }
    function run($username, $password, $trip_id) {
        $contract = $this->model->login($username, $password);
        if ( ! $contract) { return array('status' => (int) 2, 'result' => "Invalid access information");}
        $result['status'] = (int) 1;
        
        $trip = R::getAll("SELECT safa_trips.safa_trip_id, safa_trips.name FROM safa_trips 
            JOIN safa_ea_packages ON safa_ea_packages.safa_ea_package_id = safa_trips.safa_ea_package_id
            JOIN safa_uo_contracts_eas ON safa_uo_contracts_eas.safa_uo_contract_ea_id = safa_ea_packages.safa_uo_contract_ea_id
            WHERE safa_uo_contracts_eas.safa_uo_contract_id = '".$contract['safa_uo_contract_id']."' AND safa_trips.safa_trip_id = '".$trip_id."'");
        
        if( ! $trip) {
            $result['status'] = 2;
            $result['result'] = "You are not permitted to preform this action.";
            return $result;
        }
        try {
            Structure::$db->exec('DELETE FROM safa_trip_travellers WHERE safa_trip_id = "'.$trip_id.'"');
        }
        catch (Exception $e)
        {
            $result['result'] = $e->getMessage();
            $result['status'] = (int) 2;
        }
        return $result;
    }
}

$DeleteTraveller = new DeleteTravellers();
$DeleteTraveller->register();
function DeleteTravellers($username, $password, $trip_id) {
    global $DeleteTraveller;
    return $DeleteTraveller->run($username, $password, $trip_id);
}