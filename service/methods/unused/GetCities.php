<?php

class GetCities extends Structure {
    function response() {
        parent::$server->wsdl->addComplexType('city','complexType','struct','all','',
            array( 'erp_city_id' => array('name' => 'erp_city_id','type' => 'xsd:int'),
            'name_ar' => array('name' => 'name_ar','type' => 'xsd:string'),
            'name_la' => array('name' => 'name_la','type' => 'xsd:string'),
            'erp_country_id' => array('name' => 'erp_country_id','type' => 'xsd:int')
        ));
        parent::$server->wsdl->addComplexType(
            'cities',
            'complexType',
            'array',
            '',
            'SOAP-ENC:Array',
            array(),
            array(
                array('ref'=>'SOAP-ENC:arrayType','wsdl:arrayType'=>'tns:city[]')
            ),
            'tns:city'
        );
    }
    function register() {
        parent::$server->register("GetCities",
            array('username' => 'xsd:string', 'password' => 'xsd:string'),
            array('status'=>'xsd:int', 'cities'=>'tns:cities', 'result' => 'xsd:string'),
            $this->namespace,                         // namespace
            $this->namespace . '#GetCities',       // soapaction
            'rpc',                // style
            'encoded',            // use
            'GetCities'        // documentation
          );
    }
    function run($username, $password) {
        $contract = $this->model->login($username, $password);
        if ( ! $contract) { return array('status' => (int) 2, 'result' => "Invalid access information");}
        $result['status'] = (int) 1;
        $result['cities'] = R::getAll("SELECT * FROM erp_cities");
        return $result;
    }
}
$getCities = new GetCities();
$getCities->response();
$getCities->register();
function GetCities($username, $password) { global $getCities;
    return $getCities->run($username, $password);
}