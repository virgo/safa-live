<?php

class InsertTraveller extends Structure {
    function request() {
        parent::$server->wsdl->addComplexType('trip_traveller', 'complexType', 'struct', 'all', '', array(
            'safa_trip_id' => array('name' => 'safa_trip_id', 'type' => 'xsd:int'),
            'offline_id' => array('name' => 'offline_id', 'type' => 'xsd:int'),
            'no' => array('name' => 'no', 'type' => 'xsd:int'),
            'display_order' => array('name' => 'display_order', 'type' => 'xsd:int'),
            'title_id' => array('name' => 'title_id', 'type' => 'xsd:int'),
            'first_name_la' => array('name' => 'first_name_la', 'type' => 'xsd:string'),
            'second_name_la' => array('name' => 'second_name_la', 'type' => 'xsd:string'),
            'third_name_la' => array('name' => 'third_name_la', 'type' => 'xsd:string'),
            'fourth_name_la' => array('name' => 'fourth_name_la', 'type' => 'xsd:string'),
            'first_name_ar' => array('name' => 'first_name_ar', 'type' => 'xsd:string'),
            'second_name_ar' => array('name' => 'second_name_ar', 'type' => 'xsd:string'),
            'third_name_ar' => array('name' => 'third_name_ar', 'type' => 'xsd:string'),
            'fourth_name_ar' => array('name' => 'fourth_name_ar', 'type' => 'xsd:string'),
            'picture' => array('name' => 'picture', 'type' => 'xsd:string'),
            'nationality_id' => array('name' => 'nationality_id', 'type' => 'xsd:int'),
            'previous_nationality_id' => array('name' => 'previous_nationality_id', 'type' => 'xsd:int'),
            'passport_no' => array('name' => 'passport_no', 'type' => 'xsd:string'),
            'passport_type_id' => array('name' => 'passport_type_id', 'type' => 'xsd:int'),
            'passport_issue_date' => array('name' => 'passport_issue_date', 'type' => 'xsd:string'),
            'passport_expiry_date' => array('name' => 'passport_expiry_date', 'type' => 'xsd:string'),
            'passport_issuing_city' => array('name' => 'passport_issuing_city', 'type' => 'xsd:string'),
            'passport_issuing_country_id' => array('name' => 'passport_issuing_country_id', 'type' => 'xsd:int'),
            'passport_dpn_count' => array('name' => 'passport_dpn_count', 'type' => 'xsd:int'),
            'dpn_serial_no' => array('name' => 'dpn_serial_no', 'type' => 'xsd:int'),
            'relative_relation_id' => array('name' => 'relative_relation_id', 'type' => 'xsd:int'),
            'educational_level_id' => array('name' => 'educational_level_id', 'type' => 'xsd:int'),
            'occupation' => array('name' => 'occupation', 'type' => 'xsd:string'),
            'email_address' => array('name' => 'email_address', 'type' => 'xsd:string'),
            'street_name' => array('name' => 'street_name', 'type' => 'xsd:string'),
            'date_of_birth' => array('name' => 'date_of_birth', 'type' => 'xsd:string'),
            'birth_city' => array('name' => 'birth_city', 'type' => 'xsd:string'),
            'birth_country_id' => array('name' => 'birth_country_id', 'type' => 'xsd:int'),
            'relative_no' => array('name' => 'relative_no', 'type' => 'xsd:int'),
            'relative_gender_id' => array('name' => 'relative_gender_id', 'type' => 'xsd:int'),
            'gender_id' => array('name' => 'gender_id', 'type' => 'xsd:int'),
            'marital_status_id' => array('name' => 'marital_status_id', 'type' => 'xsd:int'),
            'zip_code' => array('name' => 'zip_code', 'type' => 'xsd:string'),
            'city' => array('name' => 'city', 'type' => 'xsd:string'),
            'region_name' => array('name' => 'region_name', 'type' => 'xsd:string'),
            'erp_country_id' => array('name' => 'erp_country_id', 'type' => 'xsd:int'),
            'mofa' => array('name' => 'mofa', 'type' => 'xsd:string'),
            'enumber' => array('name' => 'enumber', 'type' => 'xsd:string'),
            'id_number' => array('name' => 'id_number', 'type' => 'xsd:string'),
            'job2' => array('name' => 'job2', 'type' => 'xsd:string'),
            'address' => array('name' => 'address', 'type' => 'xsd:string'),
            'remarks' => array('name' => 'remarks', 'type' => 'xsd:string'),
            'canceled' => array('name' => 'canceled', 'type' => 'xsd:string'),
            'mobile' => array('name' => 'mobile', 'type' => 'xsd:string'),
            'sim_serial' => array('name' => 'sim_serial', 'type' => 'xsd:string'),
            'visa_number' => array('name' => 'visa_number', 'type' => 'xsd:string'),
            'notes' => array('name' => 'notes', 'type' => 'xsd:string'),
            'entry_datetime' => array('name' => 'entry_datetime', 'type' => 'xsd:string'),
            'erp_meal_id' => array('name' => 'erp_meal_id', 'type' => 'xsd:int')
        ));
    }
    function register() {
        parent::$server->register("InsertTraveller", 
                array('username' => 'xsd:string', 'password' => 'xsd:string', 'trip_traveller' => 'tns:trip_traveller'), 
                array('status' => 'xsd:int', 'result' => 'xsd:string'), $this->namespace, // namespace
                $this->namespace . '#InsertTraveller', // soapaction
                'rpc', // style
                'encoded', // use
                'InsertTraveller'        // documentation
        );
    }
    function run($username, $password, $trip_traveller) {
        $contract = $this->model->login($username, $password);
        if ( ! $contract) { return array('status' => (int) 2, 'result' => "Invalid access information");}
        $result['status'] = (int) 1;
        $trip_traveller['religion'] = 1;
        
        $trip = R::getAll("SELECT safa_trips.safa_trip_id, safa_trips.name FROM safa_trips 
            JOIN safa_ea_packages ON safa_ea_packages.safa_ea_package_id = safa_trips.safa_ea_package_id
            JOIN safa_uo_contracts_eas ON safa_uo_contracts_eas.safa_uo_contract_ea_id = safa_ea_packages.safa_uo_contract_ea_id
            WHERE safa_uo_contracts_eas.safa_uo_contract_id = '".$contract['safa_uo_contract_id']."' AND safa_trips.safa_trip_id = '".$trip_traveller['safa_trip_id']."'");
        
        if( ! $trip) {
            $result['status'] = 2;
            $result['result'] = "You are not permitted to preform this action.";
            return $result;
        }
        if(isset($trip_traveller['relative_no'])) {
            $row = R::getRow ("SELECT * FROM `safa_trip_travellers` WHERE `offline_id` = '".$trip_traveller['relative_no']."' AND `safa_trip_id` = '".$trip_traveller['safa_trip_id']."'");
            $trip_traveller['relative_no'] = $row['safa_trip_traveller_id'];
        }
        $result = $this->insert('safa_trip_travellers', $trip_traveller);
        return $result;
    }
}

$InsertTraveller = new InsertTraveller();
$InsertTraveller->request();
$InsertTraveller->register();
function InsertTraveller($username, $password, $trip_traveller) {
    global $InsertTraveller;
    return $InsertTraveller->run($username, $password, $trip_traveller);
}