<?php

class GetEAPackages extends Structure{
    function response() {
        parent::$server->wsdl->addComplexType('meal','complexType','struct','all','',
            array( 'erp_meal_id' => array('name' => 'erp_meal_id','type' => 'xsd:int'),
            'code' => array('name' => 'code','type' => 'xsd:string'),
            'name_ar' => array('name' => 'name_ar','type' => 'xsd:string'),
            'name_la' => array('name' => 'name_la','type' => 'xsd:string'),
            'description_ar' => array('name' => 'description_ar','type' => 'xsd:string'),
            'description_la' => array('name' => 'description_la','type' => 'xsd:string'),
        ));
        parent::$server->wsdl->addComplexType(
            'meals',
            'complexType',
            'array',
            '',
            'SOAP-ENC:Array',
            array(),
            array(
                array('ref'=>'SOAP-ENC:arrayType','wsdl:arrayType'=>'tns:meal[]')
            ),
            'tns:meal'
        );
    }
    function register() {
        parent::$server->register("GetEAPackages",
            array('username' => 'xsd:string', 'password' => 'xsd:string'),
            array('status'=>'xsd:int', 'meals'=>'tns:meals', 'result' => 'xsd:string'),
            $this->namespace,                         // namespace
            $this->namespace . '#GetEAPackages',       // soapaction
            'rpc',                // style
            'encoded',            // use
            'GetEAPackages'        // documentation
          );
    }
    function run($username, $password) {
        $contract = $this->model->login($username, $password);
        if ( ! $contract) { return array('status' => (int) 2, 'result' => "Invalid access information");}
        $result['status'] = (int) 1;
        $result['meals'] = R::getAll("SELECT * FROM `safa_ea_packages`");
        return $result;
    }
}
$GetEAPackages = new GetEAPackages();
$GetEAPackages->response();
$GetEAPackages->register();
function GetEAPackages($username, $password) { global $GetEAPackages;
    return $GetEAPackages->run($username, $password);
}