<?php

class GetPorts extends Structure{
    function response() {
        parent::$server->wsdl->addComplexType('port','complexType','struct','all','',
            array( 'erp_port_id' => array('name' => 'erp_port_id','type' => 'xsd:int'),
            'erp_city_id' => array('name' => 'erp_city_id','type' => 'xsd:int'),
            'code' => array('name' => 'code','type' => 'xsd:string'),
            'name_ar' => array('name' => 'name_ar','type' => 'xsd:string'),
            'name_la' => array('name' => 'name_la','type' => 'xsd:string'),
            'country_name' => array('name' => 'country_name','type' => 'xsd:string'),
            'country_code' => array('name' => 'country_code','type' => 'xsd:string'),
            'world_area_code' => array('name' => 'world_area_code','type' => 'xsd:int'),
            'safa_transportertype_id' => array('name' => 'safa_transportertype_id','type' => 'xsd:int'),
        ));
        parent::$server->wsdl->addComplexType(
            'ports',
            'complexType',
            'array',
            '',
            'SOAP-ENC:Array',
            array(),
            array(
                array('ref'=>'SOAP-ENC:arrayType','wsdl:arrayType'=>'tns:port[]')
            ),
            'tns:port'
        );
    }
    function register() {
        parent::$server->register("GetPorts",
            array('username' => 'xsd:string', 'password' => 'xsd:string', 'country_code' => 'xsd:string'),
            array('status'=>'xsd:int', 'ports'=>'tns:ports', 'result' => 'xsd:string'),
            $this->namespace,                         // namespace
            $this->namespace . '#GetPorts',       // soapaction
            'rpc',                // style
            'encoded',            // use
            'GetPorts'        // documentation
          );
    }
    function run($username, $password, $country_code) {
        $contract = $this->model->login($username, $password, $country_code);
        if ( ! $contract) { return array('status' => (int) 2, 'result' => "Invalid access information");}
        if( ! $country_code) { return array('status' => (int) 2); }
        $result['status'] = (int) 1;
        $result['ports'] = R::getAll("SELECT * FROM `erp_ports` WHERE country_code = '$country_code'");

        return $result;
    }
}
$GetPorts = new GetPorts();
$GetPorts->response();
$GetPorts->register();
function GetPorts($username, $password, $country_code) { global $GetPorts;
    return $GetPorts->run($username, $password, $country_code);
}