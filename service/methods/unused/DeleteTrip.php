<?php

class DeleteTrip extends Structure{
    function register() {
        self::$server->register("DeleteTrip",
            array('username' => 'xsd:string', 'password' => 'xsd:string', 'trip_id'=>'xsd:int'),
            array('status'=>'xsd:int', 'result' => 'xsd:string'),
            $this->namespace,      // namespace
            $this->namespace . '#DeleteTrip',       // soapaction
            'rpc',                // style
            'encoded',            // use
            'DeleteTrip'        // documentation
          );
    }
    function run($username, $password, $id) {
        $contract = $this->model->login($username, $password);
        if ( ! $contract) { return array('status' => (int) 2, 'result' => "Invalid access information");}
        $result['status'] = (int) 1;
        $trip = R::getAll("SELECT safa_trips.safa_trip_id, safa_trips.name FROM safa_trips 
            JOIN safa_ea_packages ON safa_ea_packages.safa_ea_package_id = safa_trips.safa_ea_package_id
            JOIN safa_uo_contracts_eas ON safa_uo_contracts_eas.safa_uo_contract_ea_id = safa_ea_packages.safa_uo_contract_ea_id
            WHERE safa_uo_contracts_eas.safa_uo_contract_id = '".$contract['safa_uo_contract_id']."' AND safa_trips.safa_trip_id = '".$id."'");
        
        if( ! $trip) {
            $result['status'] = 2;
            $result['result'] = "You are not permitted to preform this action.";
            return $result;
        }
        try{
            // IF TRIP EXISTS, DELETE ALL RELATIVE PASSPORTS
            R::exec("DELETE FROM safa_trip_travellers WHERE safa_trip_id = '".$id."'");
            foreach(R::getAll("SELECT * FROM safa_trip_internaltrips WHERE  safa_trip_id = '".$id."'") as $segment)
            R::exec("DELETE FROM safa_internalsegments WHERE safa_trip_internaltrip_id = '".$id."'");
            R::exec("DELETE FROM safa_trip_internaltrips WHERE safa_trip_id = '".$id."'");
            R::exec("DELETE FROM safa_trip_tourismplaces WHERE safa_trip_id = '".$id."'");
            R::exec("DELETE FROM safa_trip_supervisors WHERE safa_trip_id = '".$id."'");
            foreach(R::getAll("SELECT * FROM safa_trip_hotels WHERE safa_trip_id = '".$id."'") as $row)
            R::exec("DELETE FROM safa_trip_hotel_rooms WHERE safa_trip_hotel_id = '".$row['safa_trip_hotel_id']."'");
            R::exec("DELETE FROM safa_trip_hotels WHERE safa_trip_id = '".$id."'");
            R::exec("DELETE FROM safa_trip_externalsegments WHERE safa_trip_id = '".$id."'");
            R::exec("DELETE FROM safa_trips WHERE safa_trip_id = '".$id."'");
        }
        catch (Exception $e){
            $result['result'] = $e->getMessage();
            $result['status'] = 2;
        }
        // DELETE TRIP
        return $result;
    }
}
$DeleteTrip = new DeleteTrip();
$DeleteTrip->register();
function DeleteTrip($username, $password, $trip_id) { global $DeleteTrip;
   return $DeleteTrip->run($username, $password, $trip_id);
}
