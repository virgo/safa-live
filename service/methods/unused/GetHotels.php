<?php

class GetHotels extends Structure{
    function response() {
        parent::$server->wsdl->addComplexType('hotel','complexType','struct','all','',
            array( 'erp_hotel_id' => array('name' => 'erp_hotel_id','type' => 'xsd:int'),
            'name_ar' => array('name' => 'name_ar','type' => 'xsd:string'),
            'name_la' => array('name' => 'name_la','type' => 'xsd:string'),
            'erp_city_id' => array('name' => 'erp_city_id','type' => 'xsd:int'),
            'erp_hotellevel_id' => array('name' => 'erp_hotellevel_id','type' => 'xsd:int'),
            'address_ar' => array('name' => 'address_ar','type' => 'xsd:string'),
            'address_la' => array('name' => 'address_la','type' => 'xsd:string'),
            ));
        parent::$server->wsdl->addComplexType(
            'hotels',
            'complexType',
            'array',
            '',
            'SOAP-ENC:Array',
            array(),
            array(
                array('ref'=>'SOAP-ENC:arrayType','wsdl:arrayType'=>'tns:hotel[]')
            ),
            'tns:hotel'
        );
    }
    function register() {
        parent::$server->register("GetHotels",
            array('username' => 'xsd:string', 'password' => 'xsd:string'),
            array('status'=>'xsd:int', 'hotels'=>'tns:hotels', 'result' => 'xsd:string'),
            $this->namespace,                         // namespace
            $this->namespace . '#GetHotels',       // soapaction
            'rpc',                // style
            'encoded',            // use
            'GetHotels'        // documentation
          );
    }
    function run($username, $password) {
        $contract = $this->model->login($username, $password);
        if ( ! $contract) { return array('status' => (int) 2, 'result' => "Invalid access information");}
        $result['status'] = (int) 1;
        $result['hotels'] = R::getAll("SELECT erp_hotel_id, name_ar, name_la, erp_city_id, erp_hotellevel_id, address_ar, address_la FROM erp_hotels");
        return $result;
    }
}
$GetHotels = new GetHotels();
$GetHotels->response();
$GetHotels->register();
function GetHotels($username, $password) { global $GetHotels;
    return $GetHotels->run($username, $password);
}
