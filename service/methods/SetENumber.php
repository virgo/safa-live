<?php

class SetENumber extends Structure {
    function register() {
        parent::$server->register(__CLASS__, 
            array('session' => 'xsd:string', 'mofa' => 'xsd:int', 'enumber' => 'xsd:string'), 
            array('status' => 'xsd:int', 'result' => 'xsd:string'),
            $this->namespace, // namespace
            $this->namespace . '#' . __CLASS__, // soapaction
            'rpc', // style
            'encoded', // use
            __CLASS__        // documentation
        );
    }

    function run($hash, $mofa, $enumber) {
        $access = $this->model->access($hash);
        if($access['status'] == 0)
            return $access;
        return $this->operations($access, $mofa, $enumber);
    }

    function operations($license, $mofa, $enumber) {
        // CHECK THE MOFA IF EXISTS
        $rows = R::getAll('SELECT mofa, enumber, mofa_date, upload_date '
                . 'FROM safa_umrahgroups_passports '
                . 'JOIN safa_umrahgroups USING (safa_umrahgroup_id) '
                . 'WHERE safa_umrahgroups.safa_ea_id = "' . $license['safa_ea_id'].'" '
                . 'AND safa_umrahgroups_passports.mofa = "'.$mofa.'"');
        if ( ! $rows )
            return array('status' => (int) 11, 'result' => "INVALID MOFA / NOT ACCESSIBLE");
        $this->update('safa_umrahgroups_passports', array('enumber' => $enumber), 'WHERE mofa="'.$mofa.'"');
        if ( $rows )
            return array('status' => (int) 1, 'result' => "Done");
    }
}

$SetENumber = new SetENumber();
$SetENumber->register();

function SetENumber($hash, $mofa, $enumber) {
    return $GLOBALS['SetENumber']->run($hash, $mofa, $enumber);
}
