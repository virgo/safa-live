<?php

//$class = end(explode('/', str_replace('.php', '', __FILE__)));

class DeletePassport extends Structure {
    function register() {
        parent::$server->register(__CLASS__,
            array('session' => 'xsd:string', 'passport_id' => 'xsd:int'),
            array('status'=>'xsd:int', 'result' => 'xsd:string'),
            $this->namespace,                         // namespace
            $this->namespace . '#'.__CLASS__,       // soapaction
            'rpc',                // style
            'encoded',            // use
            'GetCities'        // documentation
          );
    }
    function run($hash, $group_id) {
        $access = $this->model->access($hash);
        if($access['status'] == 0)
            return $access;
        return $this->operations($access, $group_id);
    }
    
    function operations($license, $passport_id){
        $rows = R::getAll('SELECT * FROM safa_group_passports '
                . 'INNER JOIN safa_groups ON safa_groups.safa_group_id = safa_group_passports.safa_group_id '
                . 'WHERE safa_group_passports.safa_group_passport_id = "'.$passport_id.'" AND safa_groups.safa_ea_id = "'.$license['safa_ea_id'].'"');
        
        if( ! $rows) return error( (int) 4,"No passport available");
        
        
        // CHECK PASSPORT IF RELATED TO UMRAHGROUP. TRIP
        $rows = R::getAll('SELECT * FROM safa_group_passports '
                . 'INNER JOIN `safa_umrahgroup_passports` ON `safa_umrahgroup_passports`.`safa_group_passport_id` = `safa_group_passports`.`safa_group_passport_id` '
                . 'WHERE safa_group_passports.safa_group_passport_id = "'.$passport_id.'"');
        if($rows) return error( (int) 7,"This passport related to Umrah Group");
        
        $rows = R::getAll('SELECT * FROM safa_group_passports '
                . 'INNER JOIN `safa_trip_travellers` ON `safa_trip_travellers`.`safa_group_passport_id` = `safa_group_passports`.`safa_group_passport_id` '
                . 'WHERE safa_group_passports.safa_group_passport_id = "'.$passport_id.'"');
        if($rows) return error( (int) 7,"This passport related to trip");
        
        R::exec('DELETE FROM safa_group_passports WHERE safa_group_passport_id = "'.$passport_id.'"');
        return array('status' => (int) 1, 'result' => "Done");
    }
}

$DeletePassport = new DeletePassport();
$DeletePassport->register();

function DeletePassport($hash, $group_id)
{
    return $GLOBALS['DeletePassport']->run($hash, $group_id);
}
