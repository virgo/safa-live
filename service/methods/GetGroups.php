<?php

//$class = end(explode('/', str_replace('.php', '', __FILE__)));

class GetGroups extends Structure {
    function response() {
        parent::$server->wsdl->addComplexType('group','complexType','struct','all','',
            array( 
                'group_id' => array('name' => 'group_id','type' => 'xsd:int'),
                'name' => array('name' => 'name','type' => 'xsd:string'),
                'last_update' => array('name' => 'last_update','type' => 'xsd:string'),
        ));
        parent::$server->wsdl->addComplexType('groups','complexType','array','','SOAP-ENC:Array',
            array(),
            array(array('ref'=>'SOAP-ENC:arrayType',
                      'wsdl:arrayType'=>'tns:group[]'
                )),
            'tns:group'
        );
    }
    function request() {
        parent::$server->register(__CLASS__,
            array('session' => 'xsd:string', 'datetime' => 'xsd:string'),
            array('status'=>'xsd:int', 'groups'=>'tns:groups', 'result' => 'xsd:string'),
            $this->namespace,                         // namespace
            $this->namespace . '#'.__CLASS__,       // soapaction
            'rpc',                // style
            'encoded',            // use
            __CLASS__        // documentation
          );
    }
    function run($hash, $datetime) {
        $access = $this->model->access($hash);
        if($access['status'] == 0)
            return $access;
        return $this->operations($access, $datetime);
    }
    function operations($access, $datetime = 0){
        $condition = null;
        if($datetime)
        $condition = ' AND `last_update` > "' . $datetime . '"';
        $rows = R::getAll('SELECT safa_group_id as group_id, name, last_update FROM safa_groups WHERE safa_ea_id = "'.$access['safa_ea_id'] .'"'. $condition);
        if($rows) return array('status' => (int) 1, 'result' => "Done", 'groups' => $rows);
        else return array('status' => (int) 4, 'result' => "No groups available");
    }
}

$GetGroups = new GetGroups();
$GetGroups->response();
$GetGroups->request();

function GetGroups($hash, $datetime)
{
    return $GLOBALS['GetGroups']->run($hash, $datetime);
}
