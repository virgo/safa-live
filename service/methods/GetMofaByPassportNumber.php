<?php

class GetMofaByPassportNumber extends Structure {
     function response() {
        parent::$server->wsdl->addComplexType('mofa','complexType','struct','all','',
            array( 
                'mofa' => array('name' => 'mofa','type' => 'xsd:int'),
                'enumber' => array('name' => 'enumber','type' => 'xsd:string'),
                'mofa_date' => array('name' => 'mofa_date','type' => 'xsd:string'),
                'upload_date' => array('name' => 'upload_date','type' => 'xsd:string'),
        ));
        parent::$server->wsdl->addComplexType('mofas','complexType','array','','SOAP-ENC:Array',
            array(),
            array(array('ref'=>'SOAP-ENC:arrayType',
                      'wsdl:arrayType'=>'tns:mofa[]'
                )),
            'tns:mofa'
        );
    }
    function register() {
        parent::$server->register(__CLASS__, 
            array('session' => 'xsd:string', 'passport_number' => 'xsd:string'), 
            array('status' => 'xsd:int', 'result' => 'xsd:string', 'mofas' => 'tns:mofas'),
            $this->namespace, // namespace
            $this->namespace . '#' . __CLASS__, // soapaction
            'rpc', // style
            'encoded', // use
            __CLASS__        // documentation
        );
    }

    function run($hash, $passport_number) {
        $access = $this->model->access($hash);
        if($access['status'] == 0)
            return $access;
        return $this->operations($access, $passport_number);
    }

    function operations($license, $passport_number) {
        // CHECK THE GROUP IF EXISTS
        $rows = R::getAll('SELECT mofa, enumber, mofa_date, upload_date '
                . 'FROM safa_umrahgroups_passports '
                . 'JOIN safa_umrahgroups USING (safa_umrahgroup_id) '
                . 'WHERE safa_umrahgroups.safa_ea_id = "' . $license['safa_ea_id'].'" '
                . 'AND safa_umrahgroups_passports.passport_no = "'.$passport_number.'"');
        if ( $rows )
            return array('status' => (int) 1, 'result' => "Done", 'mofas' => $rows);
    }
}

$GetMofaByPassportNumber = new GetMofaByPassportNumber();
$GetMofaByPassportNumber->response();
$GetMofaByPassportNumber->register();

function GetMofaByPassportNumber($hash, $passport_number) {
    return $GLOBALS['GetMofaByPassportNumber']->run($hash, $passport_number);
}
