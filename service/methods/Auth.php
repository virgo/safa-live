<?php

class Auth extends Structure {
    function register() {
        Structure::$server->register("Auth",
            array('hash' => 'xsd:string'),
            array('status'=>'xsd:int', 
            	  'result' => 'xsd:string', 
            	  'code' => 'xsd:int', 
            	  'datetime' => 'xsd:string', 
            	  'session' => 'xsd:string',
            	  'country' => 'xsd:int',
                  'credit' => 'xsd:int',
            	  'usage' => 'xsd:int',
            	  'remain' => 'xsd:int',
            	  'over_credit' => 'xsd:int'
            ),
            $this->namespace,                         // namespace
            $this->namespace . '#Auth',       // soapaction
            'rpc',                // style
            'encoded',            // use
            'Auth'        // documentation
          );
    }
    function run($hash) {
        return $this->model->auth($hash);
    }
}

$Auth = new Auth();
$Auth->register();

function Auth($hash) { global $Auth;
   return $Auth->run($hash);
}
