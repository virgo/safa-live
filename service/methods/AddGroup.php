<?php

class AddGroup extends Structure {

    function register() {
        parent::$server->register(__CLASS__, 
            array('session' => 'xsd:string', 'name' => 'xsd:string', 'date' => 'xsd:string'), 
            array('status' => 'xsd:int', 'result' => 'xsd:string', 'group_id' => 'xsd:int'),
            $this->namespace, // namespace
            $this->namespace . '#' . __CLASS__, // soapaction
            'rpc', // style
            'encoded', // use
            __CLASS__        // documentation
        );
    }

    function run($hash, $name, $date) {
        $access = $this->model->access($hash);
        if($access['status'] == 0)
            return $access;
        return $this->operations($access, $name, $date);
    }

    function operations($license, $name, $date) {
        // CHECK THE GROUP IF EXISTS
        $rows = R::getAll('SELECT safa_group_id as group_id, name FROM safa_groups WHERE name = "'.$name.'" AND safa_ea_id = "' . $license['safa_ea_id'].'"');
        if ( $rows )
            return array('status' => (int) 11, 'result' => "THIS GROUP IS EXISTS");
        
        // INSERT/UPDATE OPERATION
//            $rows2 = R::exec('INSERT INTO `safa_groups` '
//                    . '(`name`, `creation_date`, `safa_ea_id`)'
//                    . 'VALUES'
//                    . '("'.$name.'","'.$date.'","'.$license['safa_ea_id'].'")');
            $safa_groups = new stdClass();
            $safa_groups->name = $name;
            $safa_groups->creation_date = $date;
            $safa_groups->safa_ea_id = $license['safa_ea_id'];
            $id = $this->insert("safa_groups", $safa_groups);
            
            return array('status' => (int) 1, 'result' => "DONE", 'group_id' => $id['result']);
    }
}

$AddGroup = new AddGroup();
$AddGroup->register();

function AddGroup($hash, $name, $date) {
    return $GLOBALS['AddGroup']->run($hash, $name, $date);
}
