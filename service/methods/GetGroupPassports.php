<?php

//$class = end(explode('/', str_replace('.php', '', __FILE__)));

class GetGroupPassports extends Structure {

    function request() {
        parent::$server->wsdl->addComplexType('passport_of_group', 'complexType', 'struct', 'all', '', array(
            'passport_id' => array('name' => 'passport_id', 'type' => 'xsd:int'),
            'offline_id' => array('name' => 'offline_id', 'type' => 'xsd:int'),
            'no' => array('name' => 'no', 'type' => 'xsd:int'),
            'title_id' => array('name' => 'title_id', 'type' => 'xsd:int'),
            'group_id' => array('name' => 'group_id', 'type' => 'xsd:int'),
            'first_name_la' => array('name' => 'first_name_la', 'type' => 'xsd:string'),
            'second_name_la' => array('name' => 'second_name_la', 'type' => 'xsd:string'),
            'third_name_la' => array('name' => 'third_name_la', 'type' => 'xsd:string'),
            'fourth_name_la' => array('name' => 'fourth_name_la', 'type' => 'xsd:string'),
            'first_name_ar' => array('name' => 'first_name_ar', 'type' => 'xsd:string'),
            'second_name_ar' => array('name' => 'second_name_ar', 'type' => 'xsd:string'),
            'third_name_ar' => array('name' => 'third_name_ar', 'type' => 'xsd:string'),
            'fourth_name_ar' => array('name' => 'fourth_name_ar', 'type' => 'xsd:string'),
            'picture' => array('name' => 'picture', 'type' => 'xsd:string'),
            'nationality_id' => array('name' => 'nationality_id', 'type' => 'xsd:int'),
            'previous_nationality_id' => array('name' => 'previous_nationality_id', 'type' => 'xsd:int'),
            'passport_no' => array('name' => 'passport_no', 'type' => 'xsd:string'),
            'passport_type_id' => array('name' => 'passport_type_id', 'type' => 'xsd:int'),
            'passport_issue_date' => array('name' => 'passport_issue_date', 'type' => 'xsd:string'),
            'passport_expiry_date' => array('name' => 'passport_expiry_date', 'type' => 'xsd:string'),
            'passport_issuing_city' => array('name' => 'passport_issuing_city', 'type' => 'xsd:string'),
            'passport_issuing_country_id' => array('name' => 'passport_issuing_country_id', 'type' => 'xsd:int'),
            'passport_dpn_count' => array('name' => 'passport_dpn_count', 'type' => 'xsd:int'),
            'dpn_serial_no' => array('name' => 'dpn_serial_no', 'type' => 'xsd:int'),
            'relative_relation_id' => array('name' => 'relative_relation_id', 'type' => 'xsd:int'),
            'educational_level_id' => array('name' => 'educational_level_id', 'type' => 'xsd:int'),
            'occupation' => array('name' => 'occupation', 'type' => 'xsd:string'),
            'email_address' => array('name' => 'email_address', 'type' => 'xsd:string'),
            'street_name' => array('name' => 'street_name', 'type' => 'xsd:string'),
            'date_of_birth' => array('name' => 'date_of_birth', 'type' => 'xsd:string'),
            'age' => array('name' => 'age', 'type' => 'xsd:string'),
            'birth_city' => array('name' => 'birth_city', 'type' => 'xsd:string'),
            'birth_country_id' => array('name' => 'birth_country_id', 'type' => 'xsd:int'),
            'relative_no' => array('name' => 'relative_no', 'type' => 'xsd:int'),
            'relative_gender_id' => array('name' => 'relative_gender_id', 'type' => 'xsd:int'),
            'gender_id' => array('name' => 'gender_id', 'type' => 'xsd:int'),
            'marital_status_id' => array('name' => 'marital_status_id', 'type' => 'xsd:int'),
            'religion' => array('name' => 'religion', 'type' => 'xsd:string'),
            'zip_code' => array('name' => 'zip_code', 'type' => 'xsd:string'),
            'city' => array('name' => 'city', 'type' => 'xsd:string'),
            'region_name' => array('name' => 'region_name', 'type' => 'xsd:string'),
            'erp_country_id' => array('name' => 'erp_country_id', 'type' => 'xsd:int'),
            'id_number' => array('name' => 'id_number', 'type' => 'xsd:string'),
            'job2' => array('name' => 'job2', 'type' => 'xsd:string'),
            'address' => array('name' => 'address', 'type' => 'xsd:string'),
            'remarks' => array('name' => 'remarks', 'type' => 'xsd:string'),
            'canceled' => array('name' => 'canceled', 'type' => 'xsd:string'),
            'mobile' => array('name' => 'mobile', 'type' => 'xsd:string'),
            'sim_serial' => array('name' => 'sim_serial', 'type' => 'xsd:string'),
            'notes' => array('name' => 'notes', 'type' => 'xsd:string'),
            'entry_datetime' => array('name' => 'entry_datetime', 'type' => 'xsd:string'),
            'mofa' => array('name' => 'mofa', 'type' => 'xsd:string'),
            'enumber' => array('name' => 'enumber', 'type' => 'xsd:string'),
            'lastest_modification' => array('name' => 'lastest_modification', 'type' => 'xsd:string'),
        ));
        parent::$server->wsdl->addComplexType('passports', 'complexType', 'array', '', 'SOAP-ENC:Array', 
                array(), 
                array( 
                    array('ref' => 'SOAP-ENC:arrayType',
                          'wsdl:arrayType' => 'tns:passport_of_group[]'
                      )), 'tns:passport_of_group'
                );
    }

    function register() {
        parent::$server->register(__CLASS__, 
                        array('session' => 'xsd:string', 'group_id' => 'xsd:int', 'datetime' => 'xsd:string'), 
                        array('status' => 'xsd:int', 'result' => 'xsd:string', 'passports' => 'tns:passports'), $this->namespace, // namespace
                $this->namespace . '#' . __CLASS__, // soapaction
                'rpc', // style
                'encoded', // use
                __CLASS__        // documentation
            );
    }

    function run($hash, $group_id, $datetime) {
        $access = $this->model->access($hash);
        if($access['status'] == 0)
            return $access;
        return $this->operations($access, $group_id, $datetime);
    }

    function operations($license, $group_id, $datetime = 0) {
        // CHECK THE GROUP
        $rows = R::getAll('SELECT safa_groups.safa_group_id as group_id, name '
                . 'FROM safa_groups '
                . 'WHERE safa_group_id = "'.$group_id.'" AND safa_ea_id = "' . $license['safa_ea_id'].'" '
                );
        if ( ! $rows )
            return error(4, "INVALID GROUP_ID");
        
        $datetime_con = $datetime_join = null;
        if($datetime && $datetime != '2013-01-01 00:00:00') {
            $datetime_join = 'LEFT JOIN `safa_group_passport_modifications` ON '
                    . '`safa_group_passport_modifications`.`safa_group_passport_id` = `safa_group_passports`.`safa_group_passport_id`';
            $datetime_con = 'AND `safa_group_passport_modifications`.`datetime` > "' . $datetime. '"';
        }
        $rows2 = R::getAll('SELECT  
                    (SELECT datetime FROM safa_group_passport_modifications WHERE safa_group_passport_modifications.safa_group_passport_id = safa_group_passports.safa_group_passport_id ORDER BY datetime DESC LIMIT 1 ) AS lastest_modification,
                    safa_group_passports.safa_group_passport_id as passport_id,
                    safa_group_id as group_id,
                    offline_id,
                    no,
                    title_id,
                    first_name_la,
                    second_name_la,
                    third_name_la,
                    fourth_name_la,
                    first_name_ar,
                    second_name_ar,
                    third_name_ar,
                    fourth_name_ar,
                    picture,
                    nationality_id,
                    previous_nationality_id,
                    passport_no,
                    passport_type_id,
                    passport_issue_date,
                    passport_expiry_date,
                    passport_issuing_city,
                    passport_issuing_country_id,
                    passport_dpn_count,
                    dpn_serial_no,
                    relative_relation_id,
                    educational_level_id,
                    occupation,
                    email_address,
                    street_name,
                    date_of_birth,
                    age,
                    birth_city,
                    birth_country_id,
                    relative_no,
                    relative_gender_id,
                    gender_id,
                    marital_status_id,
                    religion,
                    zip_code,
                    city,
                    region_name,
                    erp_country_id,
                    id_number,
                    job2,
                    address,
                    remarks,
                    canceled,
                    mobile,
                    sim_serial,
                    notes,
                    entry_datetime,
                    mofa,
                    enumber'
                . ' FROM safa_group_passports '
                . $datetime_join
                . 'WHERE safa_group_id = "' . $group_id . '"'
                . $datetime_con);

        if ( ! $rows2 )
            return error(8, "NO PASSPORTS");
        
        else {
            return array('status' => 1, 'result' => 'DONE', 'passports' => $rows2);
        }

    }

    function Passport($group_id, $pass, $op = 'add') {
        $passport_fields = array(
            'offline_id',
            'no',
            'title_id',
            'first_name_la',
            'second_name_la',
            'third_name_la',
            'fourth_name_la',
            'first_name_ar',
            'second_name_ar',
            'third_name_ar',
            'fourth_name_ar',
            'picture',
            'nationality_id',
            'previous_nationality_id',
            'passport_no',
            'passport_type_id',
            'passport_issue_date',
            'passport_expiry_date',
            'passport_issuing_city',
            'passport_issuing_country_id',
            'passport_dpn_count',
            'dpn_serial_no',
            'relative_relation_id',
            'educational_level_id',
            'occupation',
            'email_address',
            'street_name',
            'date_of_birth',
            'age',
            'birth_city',
            'birth_country_id',
            'relative_no',
            'relative_gender_id',
            'gender_id',
            'marital_status_id',
            'religion',
            'zip_code',
            'city',
            'region_name',
            'erp_country_id',
            'id_number',
            'job2',
            'address',
            'remarks',
            'canceled',
            'mobile',
            'sim_serial',
            'notes',
            'entry_datetime',
            'mofa',
            'enumber',
            'lastest_modification'
        );

        $db_insert = array();
        
        foreach ( $passport_fields as $field )
            if ( isset($pass[$field]) )
                $db_insert[$field] = $pass[$field];

        if ( $op != 'update' ) {
            $db_insert['safa_group_id'] = $group_id;
            $a = $this->insert('safa_group_passports', $db_insert);
        } else {
            $a = $this->update('safa_group_passports', $db_insert, "WHERE `safa_group_passport_id` = '" . $pass['passport_id'] . "'");
            $a['result'] = $pass['passport_id'];
        }
        if ( $a['status'] != 1 )
            return $a;
        else {
            return array(
                'status' => 1,
                'result' => 'Done',
                'passport_id' => $a['result']
            );
        }
    }
}

$GetGroupPassports = new GetGroupPassports();
$GetGroupPassports->request();
$GetGroupPassports->register();

function GetGroupPassports($hash, $group_id, $datetime = false) {
    return $GLOBALS['GetGroupPassports']->run($hash, $group_id, $datetime);
}
