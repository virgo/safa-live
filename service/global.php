<?php  
header('Content-Type: text/html; charset=utf-8');
ini_set('memory_limit', '-1');
set_time_limit(0);
ini_set('display_errors', 1);
error_reporting(E_ALL);
//ini_set('log_errors', 1); 
//ini_set('error_log', dirname(__FILE__) . '/error_log.txt'); 
date_default_timezone_set('GMT');


require_once('../application/config/database.php');
$host = $db['default']['hostname'];
$database = $db['default']['database'];
$username = $db['default']['username'];
$password = $db['default']['password'];

R::setup("mysql:host=$host;dbname=$database",$username,$password);
R::freeze( true );
R::setStrictTyping(false);
Structure::$db = new PDO("mysql:host=$host;dbname=$database",$username,$password, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'"));
Structure::$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); // Set Errorhandling to Exception
Structure::$db->setAttribute(PDO::MYSQL_ATTR_INIT_COMMAND, "SET NAMES 'utf8'");

function error($status, $message) {
    return array('status' => (int) $status, 'result' => $message);
}