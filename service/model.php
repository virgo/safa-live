<?php

class model{
    
    function auth($hash)
    {
        if(R::getRow("SELECT * FROM `crm_ea_machines` WHERE `hash` = '$hash' AND `safa_ea_id` IS NOT NULL"))
        {
            return $this->login($hash);
        }
        else
        {
            return $this->register($hash);
            
        }
    }
    
    function register($hash) {
        $license = R::getRow("SELECT * FROM `crm_ea_machines` WHERE `hash` = '$hash'");
        if( ! $license)
        {
            $num = substr(time(), 1, 9);
            $license = R::exec("INSERT INTO `crm_ea_machines` "
                    . "(`hash`, `code`)"
                    . " VALUES "
                    . "('".$hash."', '".$num."')");
        }
        else
            $num = $license['code'];
        
        return array(
                    'status' => (int) 2,
                    'result' => "Confirmation code has been sent",
                    'code' => $num,
                    'datetime' => date('Y-m-d H:i:s')
                );
    }
    function login($hash) {
        $license = R::getRow("SELECT * FROM `crm_ea_machines`"
                . " INNER JOIN `safa_eas` ON `safa_eas`.`safa_ea_id` = `crm_ea_machines`.`safa_ea_id`"
                . " WHERE `hash` = '$hash'");
        
        $update_activity = R::exec("UPDATE `crm_ea_machines` SET "
                    . "`last_access` = '".date('Y-m-d H:i:s')."' "
                    . " ,`ip` = '".$_SERVER['REMOTE_ADDR']."'"
                    . " WHERE "
                    . "`crm_ea_machine_id` = '".$license['crm_ea_machine_id']."'");
        
        if($license)
        {
            R::exec("DELETE FROM `crm_sessions` "
                  . "WHERE `crm_ea_machine_id` = '".$license['crm_ea_machine_id']."'");
            $license['session'] = session_id();
            $session = R::exec("INSERT INTO `crm_sessions` "
                        . "(`crm_ea_machine_id`, `session_id`, `datetime`)"
                        . " VALUES "
                        . "('".$license['crm_ea_machine_id']."', '".$license['session']."', '".date('Y-m-d H:i:s')."')");
            $credit = $this->get_credit($license['safa_ea_id']);
            $license['status'] = (int) 1;
            $license['result'] = "You're Active";
            $license['country'] = $license['erp_country_id'];
            $license['over_credit'] = $credit['over_credit'];
            $license['credit'] = $credit['pcredit'] - $credit['mcredit'];
            $license['remain'] = $license['credit'] - $credit['usage'];
            $license['usage'] = $credit['usage'];
        }
        else
        {
            $license['status'] = (int) 0;
            $license['code'] = NULL;
            $license['result'] = "Invalid access information";
        }
        $license['datetime'] = date('Y-m-d H:i:s');
        
        return $license;
    }
    
    function access($session)
    {
        $time = time() - (8*60*60);
        $check_session = R::getRow("SELECT * FROM `crm_sessions` "
                . "INNER JOIN `crm_ea_machines` ON `crm_ea_machines`.`crm_ea_machine_id` = `crm_sessions`.`crm_ea_machine_id`"
                . " WHERE `session_id` = '$session' AND `datetime` >= '".date('Y-m-d H:i:s', $time)."'");
        
        if( ! $check_session)
        {
             R::exec("DELETE FROM `crm_sessions` "
                    . " WHERE "
                    . "`datetime` <= '".date('Y-m-d H:i:s', $time)."'");
             
            $return['status'] = (int) 0;
            $return['code'] = NULL;
            $return['result'] = "Invalid access information";
        }
        else
        {
            $return = $check_session;
            R::exec("UPDATE `crm_sessions` SET "
                    . "`datetime` = '".date('Y-m-d H:i:s')."'"
                    . " WHERE "
                    . "`crm_session_id` = '".$check_session['crm_session_id']."'");
            $return['status'] = (int) 1;
        }
        return $return;
    }
    public function get_credit($id) {
        return $credit = R::getRow(
                    "SELECT SUM(crm_ea_licenses.over_credit) AS over_credit"
                    .", (SELECT SUM(crm_ea_payments.credit) FROM `crm_ea_payments` WHERE `crm_ea_payments`.`crm_ea_license_id` = `crm_ea_licenses`.`crm_ea_license_id` AND `crm_ea_payments`.`payment_type` = 1) AS pcredit"
                    .", (SELECT SUM(crm_ea_payments.credit) FROM `crm_ea_payments` WHERE `crm_ea_payments`.`crm_ea_license_id` = `crm_ea_licenses`.`crm_ea_license_id` AND `crm_ea_payments`.`payment_type` = 2) AS mcredit"
                    .", (SELECT COUNT(*) FROM `safa_groups` JOIN `safa_group_passports` ON `safa_groups`.`safa_group_id` = `safa_group_passports`.`safa_group_id` WHERE `safa_groups`.`safa_ea_id` = '$id' ) AS 'usage'"
                    ." FROM crm_ea_licenses"
                    ." WHERE crm_ea_licenses.safa_ea_id = '$id'");
    }
}