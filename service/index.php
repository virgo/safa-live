<?php define('BASEPATH', $_SERVER['PHP_SELF']);
session_start();

/**
 * SYSTEM FILES
 */
require_once('lib/structure.php');
require_once('lib/nusoap.php');
require_once('lib/rb.php');
require_once('lib/session.php');
require_once('global.php');
require_once('model.php');


/**
 * Methods
 */
$methods = array(
            'Auth',
            'GetGroups',
            'AddPassport',
            'GetGroupPassports',
            'DeleteGroup',
            'AddGroup',
            'DeletePassport',
            'GetMofaByPassportNumber',
            'SetENumber'
        );

foreach($methods as $method)
    require_once('methods/'.$method.'.php');

//############################################################################
$HTTP_RAW_POST_DATA = isset($HTTP_RAW_POST_DATA) ? $HTTP_RAW_POST_DATA : '';
Structure::$server->service($HTTP_RAW_POST_DATA);
//############################################################################