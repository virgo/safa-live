<?php

class Structure {
    public static $server;
    public $model;
    public $namespace;
    public static $db = FALSE;
    
    public function __construct() {
        $this->model = new model();
        if( ! self::$server){
        self::$server = new soap_server();
        self::$server->soap_defencoding = 'UTF-8';
        self::$server->decode_utf8 = false;
//        self::$server->wsdl = stdClass();
        if(isset(self::$server->wsdl))
        self::$server->wsdl->schemaTargetNamespace = $this->namespace;
        self::$server->configureWSDL('Safa');
        $this->namespace = "http://ksaagent.umrah-booking.com";
        }

    }
    public function request(){}
    public function response(){}
    public function register(){}
    public function insert($table, $fields){
        if(is_object($fields))
            $fields = get_object_vars ($fields);
        $keys = NULL;
        $values = NULL;
        $i = 0;
        foreach($fields as $key => $value) { $i ++;
            $keys .= "`".$key."`";
            if($i < count($fields))
            $keys .=',';
            $keys .=' ';
            
            $values .= "'".$value."'";
            if($i < count($fields))
            $values .= ',';
            $values .= ' ';
        }
        $query = "INSERT INTO `$table` ".'('.$keys.') VALUES ('.$values.')';
        try 
        {
            self::$db->exec($query);
            $result['result'] = self::$db->lastInsertId();
            $result['status'] = 1;
        }
        catch(Exception $e) 
        {
            $result['result'] = $e->getMessage();
            $result['status'] = 2;
        }
        return $result;
    }
     public function update($table, $fields, $where){
        if(is_object($fields))
            $fields = get_object_vars ($fields);
        $f = NULL;
        $i = 0;
        foreach($fields as $key => $value) { $i++;
            $f .= "`".$key."` = "."'".$value."'";
            if($i < count($fields))
            $f .=',';
            $f .=' ';
        }
        $query = "UPDATE `$table` SET ".$f.' '.$where;
//        echo $query;
        try
        {
            self::$db->exec($query);
            $result['result'] = self::$db->lastInsertId();
            $result['status'] = 1;
        }
        catch(Exception $e) 
        {
            $result['result'] = $e->getMessage();
            $result['status'] = 5;
        }
        return $result;
    }
}