var t = 0;
var amadeusCount = 0;
// Render Flight with details for Brows Reservation 
function trim(str) {
    return str.replace(/^\s+|\s+$/g, "");
}
//function reset() 
//{
//    $('.embed').html(' ');
//}
function Render(pnr, name, dpst, nms, tkt, airline, number, class1, dates, airports, status, seats, deptur, arrival, depositprice)
{
    document.getElementById("pnr").value = pnr;
    document.getElementById("pay").value = convert_date(dpst);
    document.getElementById("getname").value = convert_date(nms);
    document.getElementById("ticket").value = convert_date(tkt);
    document.getElementById("deposVal").value = depositprice;
    document.getElementById("chname").value = convert_date(name);
    $('.embed').html(' ');
    $.each(airline, function(index, value) {
        new_row(index);
        document.getElementById("airline[" + index + "]").value = airline[index];
        document.getElementById("flightno[" + index + "]").value = number[index];
        document.getElementById("class[" + index + "]").value = global_classes[class1[index]];
        document.getElementById("datefields[" + index + "]").value = convert_date(dates[index]);
        document.getElementById("airports[" + index + "]").value = airports[index];
        document.getElementById("status[" + index + "]").value = global_status[status[index]];
        document.getElementById("seatcont[" + index + "]").value = seats[index];
        document.getElementById("deptime[" + index + "]").value = deptur[index];
        document.getElementById("arrtime[" + index + "]").value = arrival[index];
    });
}
// searching for word in sentance 
function check_sentence(line, word)
{
    var matchPos1 = line.indexOf(word);
    if (matchPos1 != -1)
    {
        return true;
    } else {
        return false;
    }
}
// main function to parse text ...
function parsing(checkNames, deposet, getNames, tktIssue, currencySymbols)
{
    checkNames = checkNames.toUpperCase();
    deposet = deposet.toUpperCase();
    getNames = getNames.toUpperCase();
    tktIssue = tktIssue.toUpperCase();
    currencySymbols = currencySymbols.toUpperCase();
    var name, dpst, nms, tkt, pnr, splitline = "";
    // step 1 : Convert text into uppar case
    var alltext = document.getElementById("am").value.toUpperCase();

    // This command to stop user click parsing again
    if (alltext.replace(/^\s+|\s+$/, '') == "")
    {
        alert("Please, Enter Amadeus Code First");
        return false;
    }
    //$("#am"+btnname).val().toUpperCase();
    // step 2 : split by \n new line
    pnr = "";
    name = "";
    dpst = "";
    nms = "";
    tkt = "";
    airline = "";
    number = "";
    class1 = "";
    dates = "";
    airports = "";
    status = "";
    seats = "";
    deptur = "";
    arrival = "";
    depositprice = "";

    var splited = alltext.split('\n');
    var pnrreplace = splited[0].replace(/\s\s+/g, ' ');
    var pnrsplit = pnrreplace.split(" ");
    pnr = pnrsplit[pnrsplit.length - 2];
    var splitsplit = "";
    // step 3 :loop for array which element has line
    var airline = new Array();
    var number = new Array();
    var class1 = new Array();
    var dates = new Array();
    var airports = new Array();
    var status = new Array();
    var seats = new Array();
    var deptur = new Array();
    var arrival = new Array();
    var j = 0;

    /*for(i=0;i<splited.length;i++)
     {
     if(check_sentence(splited[i]," OP ")==true)
     {
     
     }else if(check_sentence(splited[i]," OP ")==true)
     {
     
     }
     }*/

    for (i = 0; i < splited.length; i++)
    {// Begin_For
        //
        //
        // step 4 : checking OP if existed or not

        if (check_sentence(splited[i], " OP ") == true)
        { // if-1


            if (check_sentence(splited[i], checkNames))
            {
                splitsplit = splited[i].split("/");
                name = splitsplit[1];
            }
            else
            {
                if (check_sentence(splited[i], deposet))
                {

                    splitsplit = splited[i].split("/");
                    dpst = splitsplit[1];


                    depositSearch = deposet.toUpperCase();
                    price = trim(splited[i].substring(splited[i].indexOf(depositSearch) + depositSearch.length + 1, splited[i].length));
                    /*price=price.replace(/\s\s+/g,'');
                     price=price.replace(/^\s/g,'');*/
                    valuePrs = price.split(' ');
                    if (valuePrs.length > 1)
                    {
                        if (valuePrs[0] == currencySymbols)
                        {

                            depositprice = valuePrs[1];
                        } else {
                            depositprice = valuePrs[0];
                        }
                    } else if (valuePrs.length == 1)
                    {
                        sym_length = currencySymbols.length;
                        depositlen = valuePrs[0].length;
                        strpos = depositlen - sym_length;
                        if (valuePrs[0].substring(0, sym_length) == currencySymbols)
                        {
                            depositprice = valuePrs[0].substring(sym_length);
                        } else if (valuePrs[0].substring(strpos) == currencySymbols)
                        {
                            depositprice = valuePrs[0].substring(0, strpos);
                        } else {
                            depositprice = valuePrs[0];
                        }

                    }

                } else {
                    if (check_sentence(splited[i], getNames))
                    {

                        splitsplit = splited[i].split("/");
                        nms = splitsplit[1];
                    }
                    else
                    {
                        if (check_sentence(splited[i], tktIssue))
                        {
                            splitsplit = splited[i].split("/");
                            tkt = splitsplit[1];
                        }
                    }
                }

            }
        }// if-1
        // step 5 : checking the first lines to retrieve Airline and airport ....
        else
        { //EE

            var temp = splited[i];
            // step 6 : searching for airline rows by matching with patern "*1A/E*"

            if (check_sentence(temp, "A/E*"))
            {

                splitline = temp.replace(/\s\s+/g, ' ');
                if (splitline.indexOf(' ') == 0)
                {
                    splitline = splitline.replace(' ', '');
                }
                splitline = splitline.split(" ");
                //alert(splitline);
                if (splitline[1].length > 2)
                {

                    airline[j] = splitline[1].substr(0, 2);
                    number[j] = splitline[1].substr(2, 6);
                    class1[j] = splitline[2];
                    dates[j] = splitline[3];
                    airports[j] = splitline[5];
                    status[j] = splitline[6].substr(0, 2);
                    seats[j] = splitline[6].substr(2, 4);
                    if (splitline.length == 11)
                    {
                        deptur[j] = splitline[7];
                        // Example without number in the medile 
                        //2  SV5780 Y 13NOV 7 MEDCAI HK45         0525 0620   *1A/E*                    	// Example 8* in world document 
                        arrival[j] = splitline[8];
                    }
                    else
                    {
                        deptur[j] = splitline[8];
                        arrival[j] = splitline[9];
                    }
                    j++;
                }
                else
                {
                    airline[j] = splitline[1];
                    number[j] = splitline[2];
                    class1[j] = splitline[3];
                    dates[j] = splitline[4];
                    airports[j] = splitline[6];
                    status[j] = splitline[7].substr(0, 2);
                    seats[j] = splitline[7].substr(2, 4);
                    if (splitline.length == 10)
                    {
                        deptur[j] = splitline[8];
                        // Example without number in the medile 
                        //2  SV5780 Y 13NOV 7 MEDCAI HK45         0525 0620   *1A/E*                    	// Example 8* in world document 
                        arrival[j] = splitline[9];
                    }
                    else
                    {
                        deptur[j] = splitline[9];
                        arrival[j] = splitline[10];
                    }
                    j++;
                }
            }
        }//EE

    }
    Render(pnr, name, dpst, nms, tkt, airline, number, class1, dates, airports, status, seats, deptur, arrival, depositprice);
//	var elem = document.getElementById('btn');
//        elem.style.display = 'none';
}
// main function to parse text ...
function parsingEdit(btnname, checkNames, deposet, getNames, tktIssue, currencySymbols)
{
    checkNames = checkNames.toUpperCase();
    deposet = deposet.toUpperCase();
    getNames = getNames.toUpperCase();
    tktIssue = tktIssue.toUpperCase();
    currencySymbols = currencySymbols.toUpperCase();
    pnr = "";
    name = "";
    dpst = "";
    nms = "";
    tkt = "";
    airline = "";
    number = "";
    class1 = "";
    dates = "";
    airports = "";
    status = "";
    seats = "";
    deptur = "";
    arrival = "";
    depositprice = "";
    /// Get name button which contains div number 
    var name, dpst, nms, tkt, pnr, splitline = "";
    // step 1 : Convert text into uppar case
    var alltext = document.getElementById("am" + btnname).value.toUpperCase(); //$("#am"+btnname).val().toUpperCase();
    if (alltext.replace(/^\s+|\s+$/, '') == "")
    {
        alert("Please, Enter Amadeus Code First");
        return false;
    }
    // step 2 : split by \n new line
    var splited = alltext.split('\n');
    var pnrreplace = splited[0].replace(/\s\s+/g, ' ');
    var pnrsplit = pnrreplace.split(" ");
    pnr = pnrsplit[pnrsplit.length - 2];
    var splitsplit = "";
    // step 3 :loop for array which element has line
    var airline = new Array();
    var number = new Array();
    var class1 = new Array();
    var dates = new Array();
    var airports = new Array();
    var status = new Array();
    var seats = new Array();
    var deptur = new Array();
    var arrival = new Array();
    var j = 0;
    for (i = 0; i < splited.length; i++)
    {// Begin_For
        // step 4 : checking OP if existed or not
        if (check_sentence(splited[i], " OP ") == true)
        { // if-1
            if (check_sentence(splited[i], checkNames))
            {
                splitsplit = splited[i].split("/");
                name = splitsplit[1];
            }
            else
            {
                if (check_sentence(splited[i], deposet))
                {
                    /*splitsplit=splited[i].split("/");
                     dpst=splitsplit[1];
                     depositSearch="to "+deposet;
                     
                     depositSearch=depositSearch.toUpperCase();
                     price=splitsplit[4].replace(depositSearch,'');
                     price=price.replace(/\s\s+/g,'');
                     price=price.replace(/^\s/g,'');
                     valuePrs=price.split(' ');
                     if(valuePrs[0] == currencySymbols)
                     {
                     // alert(valuePrs[1]);
                     depositprice=valuePrs[1]; 
                     }else{
                     depositprice=valuePrs[0]; 
                     }*/
                    splitsplit = splited[i].split("/");
                    dpst = splitsplit[1];


                    depositSearch = deposet.toUpperCase();
                    price = trim(splited[i].substring(splited[i].indexOf(depositSearch) + depositSearch.length + 1, splited[i].length));
                    /*price=price.replace(/\s\s+/g,'');
                     price=price.replace(/^\s/g,'');*/
                    valuePrs = price.split(' ');
                    if (valuePrs.length > 1)
                    {
                        if (valuePrs[0] == currencySymbols)
                        {

                            depositprice = valuePrs[1];
                        } else {
                            depositprice = valuePrs[0];
                        }
                    } else if (valuePrs.length == 1)
                    {
                        sym_length = currencySymbols.length;
                        depositlen = valuePrs[0].length;
                        strpos = depositlen - sym_length;
                        if (valuePrs[0].substring(0, sym_length) == currencySymbols)
                        {
                            depositprice = valuePrs[0].substring(sym_length);
                        } else if (valuePrs[0].substring(strpos) == currencySymbols)
                        {
                            depositprice = valuePrs[0].substring(0, strpos);
                        } else {
                            depositprice = valuePrs[0];
                        }

                    }
                }
                else
                {
                    if (check_sentence(splited[i], getNames))
                    {
                        splitsplit = splited[i].split("/");
                        nms = splitsplit[1];
                    }
                    else
                    {
                        if (check_sentence(splited[i], tktIssue))
                        {
                            splitsplit = splited[i].split("/");
                            tkt = splitsplit[1];
                        }
                    }
                }

            }
        }// if-1
        // step 5 : checking the first lines to retrieve Airline and airport ....
        else
        { //EE
            var temp = splited[i];
            // step 6 : searching for airline rows by matching with patern "*1A/E*"
            if (check_sentence(temp, "A/E*"))
            {
                splitline = temp.replace(/\s\s+/g, ' ');
                if (splitline.indexOf(' ') == 0)
                {
                    splitline = splitline.replace(' ', '');
                }
                splitline = splitline.split(" ");
                // for example SV1200
                if (splitline[1].length > 2)
                {

                    airline[j] = splitline[1].substr(0, 2);
                    number[j] = splitline[1].substr(2, 6);
                    class1[j] = splitline[2];
                    dates[j] = splitline[3];
                    airports[j] = splitline[5];
                    status[j] = splitline[6].substr(0, 2);
                    seats[j] = splitline[6].substr(2, 4);
                    if (splitline.length == 10)
                    {
                        deptur[j] = splitline[7];
                        // Example without number in the medile 
                        //2  SV5780 Y 13NOV 7 MEDCAI HK45         0525 0620   *1A/E*                    	
                        // Example 8* in world document 
                        arrival[j] = splitline[8];
                    }
                    else
                    {
                        deptur[j] = splitline[8];
                        arrival[j] = splitline[9];
                    }
                    j++;
                }
                // for Example SV 209
                else
                {
                    airline[j] = splitline[1];
                    number[j] = splitline[2];
                    class1[j] = splitline[3];
                    dates[j] = splitline[4];
                    airports[j] = splitline[6];
                    status[j] = splitline[7].substr(0, 2);
                    seats[j] = splitline[7].substr(2, 4);
                    if (splitline.length == 10)
                    {
                        deptur[j] = splitline[8];
                        // Example without number in the medile 
                        //2  SV5780 Y 13NOV 7 MEDCAI HK45         0525 0620   *1A/E*                    	// Example 8* in world document 
                        arrival[j] = splitline[9];
                    }
                    else
                    {
                        deptur[j] = splitline[9];
                        arrival[j] = splitline[10];
                    }
                    j++;
                }
            }
        }//EE

    }// Begin_For
    RenderEdit(btnname, pnr, name, dpst, nms, tkt, airline, number, class1, dates, airports, status, seats, deptur, arrival, depositprice);
    var elem = document.getElementById('btn' + btnname);
    elem.style.display = 'none';
}

function convert_date(date){
    if( ! date)
        return '';
    day = date.substring(0,2);
    month_short = date.substring(2,5);
    var months =new Array(); 
    months['JAN'] = 1;
    months['FEB'] = 2;
    months['MAR'] = 3;
    months['APR'] = 4;
    months['MAY'] = 5;
    months['JUN'] = 6;
    months['JUL'] = 7;
    months['AUG'] = 8;
    months['SEP'] = 9;
    months['OCT'] = 10;
    months['NOV'] = 11;
    months['DEC'] = 12;
    return "2013-" + months[month_short] + "-" + day;
}