$(document).ready(function() {
    // Pnotify notifier
    $.pnotify.defaults.history = false;
    $.pnotify.defaults.delay = 3000;
    // Fancybox
    if ($("a.fb").length > 0) {
        $("a.fb").fancybox({padding: 10,
            'transitionIn': 'elastic',
            'transitionOut': 'elastic',
            'speedIn': 600,
            'speedOut': 200
        });
    }
    // Uniform
    //$("input:checkbox, input:radio").not('input.ibtn').uniform();

    // Select2
    if ($(".select").length > 0) {
        $(".select").select2();
    }
    // Tagsinput
    if ($(".tags").length > 0)
        $(".tags").tagsInput({'width': '100%',
            'height': 'auto',
            'onAddTag': function(text) {
                notify('Tags input', 'Added tag: ' + text);
            },
            'onRemoveTag': function(text) {
                notify('Tags input', 'Removed tag: ' + text);
            }});

    // Masked input        
    if ($("input[class^=mask_]").length > 0) {
        $("input.mask_tin").mask('99-9999999', {completed: function() {
                notify('Masked input', 'mask_tin completed');
            }});
        $("input.mask_ssn").mask('999-99-9999', {completed: function() {
                notify('Masked input', 'mask_ssn completed');
            }});
        $("input.mask_date").mask('9999-99-99', {completed: function() {
                notify('Masked input', 'mask_date completed');
            }});
        $("input.mask_product").mask('a*-999-a999', {completed: function() {
                notify('Masked input', 'mask_product completed');
            }});
        $("input.mask_phone").mask('99 (999) 999-99-99', {completed: function() {
                notify('Masked input', 'mask_phone completed');
            }});
        $("input.mask_phone_ext").mask('99 (999) 999-9999? x99999', {completed: function() {
                notify('Masked input', 'mask_phone_ext completed');
            }});
        $("input.mask_credit").mask('9999-9999-9999-9999', {completed: function() {
                notify('Masked input', 'mask_credit completed');
            }});
        $("input.mask_percent").mask('99%', {completed: function() {
                notify('Masked input', 'mask_percent completed');
            }});
    }
    if ($("#ms").length > 0)
        $("#ms").multiSelect({
            afterSelect: function(value, text) {
                notify('Multiselect', 'Selected: ' + text + '[' + value + ']');
            },
            afterDeselect: function(value, text) {
                notify('Multiselect', 'Deselected: ' + text + '[' + value + ']');
            }});


    if ($("#msc").length > 0) {
        $("#msc").multiSelect({
            selectableHeader: "<div class='multipleselect-header'>الخيارات المتاحة</div>",
            selectedHeader: "<div class='multipleselect-header'>الخيارات المختارة</div>",
        });

        $("#ms_select").click(function() {
            $('#msc').multiSelect('select_all');
        });
        $("#ms_deselect").click(function() {
            $('#msc').multiSelect('deselect_all');
        });
    }

    // Breadcrumb
    if ($(".breadCrumb").length > 0)
        $(".breadCrumb").jBreadCrumb({easing: 'swing'});

    // Validation
    if ($("#validate").length > 0)
        $("#validate, #validate_custom").validationEngine('attach', {promptPosition: "topLeft"});

    // Datepicker
    $(".datepicker").datepicker({dateFormat: 'yy-mm-dd'});

    if ($("#datepicker").length > 0) {

        $("#datepicker").datepicker({dateFormat: 'yy-mm-dd',
            onSelect: function(date) {
                notify('Datepicker', 'Date: ' + date)
            }});
    }


    if ($(".table").length > 0) {
        $(".table").dataTable({bFilter: false, bInfo: false, bPaginate: false, bLengthChange: false,
            bSort: true,
            bAutoWidth: true,
            "aoColumnDefs": [{"bSortable": false,
                    "aTargets": [-1, 0]}]});
    }
    if ($(".fTable").length > 0) {
        $(".fTable").dataTable({bSort: true,
            "iDisplayLength": 5, "aLengthMenu": [5, 10, 25, 50, 100], // can be removed for basic 10 items per page
            "aoColumnDefs": [{"bSortable": false,
                    "aTargets": [-1, 0]}]});
    }
    if ($(".fsTable").length > 0) {
        $(".fsTable").dataTable({bSort: true,
            bAutoWidth: true,
            
            //By Gouda, to disable warning message.
            "bRetrieve":true,
            "bDestroy": true,
            
            "iDisplayLength": 25, "aLengthMenu": [5, 10, 25, 50, 100, 200, 500], // can be removed for basic 10 items per page
            "bFilter": false,
            "sPaginationType": "full_numbers",
            "aoColumnDefs": [{"bSortable": false,
                    "aTargets": [-1, 0]}]});

    }
    if ($(".ffTable").length > 0) {
        if ($(".ffTable").length > 0)
            $(".ffTable").dataTable({bSort: true,
                bAutoWidth: true,
                "iDisplayLength": 5, "aLengthMenu": [5, 10, 25, 50, 100], // can be removed for basic 10 items per page
                "bFilter": false,
                "bPaginate": false,
                "bInfo": false,
                "sPaginationType": "full_numbers",
                "aoColumnDefs": [{"bSortable": false,
                        "aTargets": [-1, 0]}]});

    }
    if ($(".aTable").length > 0) {
        $(".aTable").dataTable({bSort: true,
            "sPaginationType": "full_numbers",
            "bProcessing": true,
            "sAjaxSource": 'php/ajax_datatable.php'});
    }

});


function notify(title, text) {

    $.pnotify({
        title: title,
        text: text,
        addclass: 'custom',
        hide: true,
        opacity: .8,
        nonblock: true,
        nonblock_opacity: .5
    });
}