/* Created by jankoatwarpspeed.com */
(function($){
    $.fn.formToWizard = function(options) {
        options = $.extend({  
            submitButton: "" 
        }, options); 
        
        if($("#wizard_validate").length > 0){
            $("#wizard_validate").validationEngine('attach',{promptPosition : "topLeft"});
            alert('');
            $('#wizard_validate').stepy({
                back: function(index) {                                                                
                    //if(!$("#wizard_validate").validationEngine('validate')) return false; //uncomment if u need to validate on back click                
                }, 
                next: function(index) {                
                    alert('hello');
                    if(!$("#wizard_validate").validationEngine('validate')) return false;                
                }, 
                finish: function(index) {                
                    if(!$("#wizard_validate").validationEngine('validate')) return false;
                }            
            });
        }
        
        var element = this;

        var steps = $(element).find("fieldset");
        var count = steps.size();
        var submmitButtonName = "#" + options.submitButton;
        $(submmitButtonName).hide();
        
        // 2
        $(element).before("<ul id='steps'></ul>");

        steps.each(function(i) {
            $(this).wrap("<div class='content-holder' id='step" + i + "'></div>");
            $(this).before("<p class='buttons-holder' id='step" + i + "commands'></p>");

// append

            // 2
            var name = $(this).find("legend").html();
            $("#steps").append("<li class='step-size' id='stepDesc" + i + "'>الخطوة " + (i + 1) + "<span>" + name + "</span></li>");

            if (i == 0) {
                createNextButton(i);
                selectStep(i);
            }
            else if (i == count - 1) {
                $("#step" + i).hide();
                createPrevButton(i);
            }
            else {
                $("#step" + i).hide();
                createPrevButton(i);
                createNextButton(i);
            }
        });

        function createPrevButton(i) {
            var stepName = "step" + i;
            $("#" + stepName + "commands").append("<a href='#' id='" + stepName + "Prev' class='prev btn'>▶ Back</a>");

            $("#" + stepName + "Prev").bind("click", function(e) {
                $("#" + stepName).hide();
                $("#step" + (i - 1)).show();
                $(submmitButtonName).hide();
                selectStep(i - 1);
            });
        }

        function createNextButton(i) {
                  
            var stepName = "step" + i;
            $("#" + stepName + "commands").append("<a href='#' id='" + stepName + "Next' class='next btn'>Next ◀</a>");

            $("#" + stepName + "Next").bind("click", function(e) {
                $("#" + stepName).hide();
                $("#step" + (i + 1)).show();
                if (i + 2 == count)
                    $(submmitButtonName).show();
                selectStep(i + 1);
            });
        }

        function selectStep(i) {
            $("#steps li").removeClass("current");
            $("#stepDesc" + i).addClass("current");
        }

    }
})(jQuery); 