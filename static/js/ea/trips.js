
function validate_form() {
	 var error=0;
	 var error_text='';
	 if (document.getElementById('name').value.replace(/^\s+|\s+$/g, '')=='')
	 {
		 document.getElementById('name').focus();
		 error=error+1;
		 error_text=error_text+'يجب ادخال اسم الرحلة\n';
	 }

	 if (document.getElementById('the_date').value.replace(/^\s+|\s+$/g, '')=='')
	 {
		 document.getElementById('the_date').focus();
		 error=error+1;
		 error_text=error_text+'يجب ادخال تاريخ الرحلة\n';
	 }

	 if (document.getElementById('safa_ea_season_id').value=='')
	 {
		 document.getElementById('safa_ea_season_id').focus();
		 error=error+1;
		 error_text=error_text+'يجب ادخال الموسم\n';
	 }

	 if (document.getElementById('safa_ea_id').value=='')
	 {
		 document.getElementById('safa_ea_id').focus();
		 error=error+1;
		 error_text=error_text+'يجب ادخال العقد\n';
	 }
	 
	 if (document.getElementById('safa_ea_package_id').value=='')
	 {
		 document.getElementById('safa_ea_package_id').focus();
		 error=error+1;
		 error_text=error_text+'يجب ادخال برنامج الوكيل\n';
	 }
	 
	 if (document.getElementById('safa_tripstatus_id').value=='')
	 {
		 document.getElementById('safa_tripstatus_id').focus();
		 error=error+1;
		 error_text=error_text+'يجب ادخال الحالة\n';
	 }
	 if (document.getElementById('safa_trip_confirm_id').value=='')
	 {
		 document.getElementById('safa_trip_confirm_id').focus();
		 error=error+1;
		 error_text=error_text+'يجب ادخال التأكيد\n';
	 }
	 if (document.getElementById('erp_transportertype_id').value=='')
	 {
		 document.getElementById('erp_transportertype_id').focus();
		 error=error+1;
		 error_text=error_text+'يجب ادخال نوع النقل\n';
	 }
	 	 
	 if(error>0)
	 {
		 alert(error_text);
		 return false;
	 }
	 return true;
	
}
