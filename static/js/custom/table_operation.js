
function check_list_delete_submit(btn_sub_id, table_class, form_id, delete_msg,container_id) {
    $(document).ready(function(){ 
        $("#"+btn_sub_id).click(function() {
           var flag = 0; 
        $("." + table_class).find("[type='checkbox']").each(function(index, object) {
            if ($(object).attr("class") != "checkall" && $(object).parent().attr("class") == "checked") {
                flag = 1;
                return false;
            }
        });
        if (flag == 1)
        {
           if($("#"+container_id).show())
                $("#"+container_id).hide()
            
            var delete_msg_="";
          
            if(delete_msg!=undefined && delete_msg!= false)
            {

                  delete_msg_=delete_msg ;   
            }
            else
            {
                delete_msg_="Are you sure you want to delete";
            }         
            
            var conf= confirm(delete_msg_);
            if(conf==true)
            {

                document.getElementById(form_id).submit();
            }
            else{
                return false;
            }
        }
        else 
        {
             $("#"+container_id).dialog({autoOpen: false, modal: true});        
             $("#"+container_id).dialog('open');
        }
      return false ;  
    }); 
    });
}

$(document).ready(function(){
    $("#show_hide_search").click(function() {
        $("#search_panal").toggle(function() {
           
        });
    });
});

