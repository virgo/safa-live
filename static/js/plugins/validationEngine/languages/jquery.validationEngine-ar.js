(function($){
    $.fn.validationEngineLanguage = function(){
    };
    $.validationEngineLanguage = {
        newLang: function(){
            $.validationEngineLanguage.allRules = {
                "required": { // Add your regex rules here, you can take telephone as an example
                    "regex": "none",
                    "alertText": "هذا الحقل مطلوب *",
                    "alertTextCheckboxMultiple": " من فضللك اختر من احدى الاختيارات*",
                    "alertTextCheckboxe": "* هذا الحقل مطلوب",
                    "alertTextDateRange": "* حقول التواريخ مطلوبه "
                },
                "requiredInFunction": { 
                    "func": function(field, rules, i, options){
                        return (field.val() == "test") ? true : false;
                    },
                    "alertText": "* هذا الحقل يجب ان يساوى test"
                },
                "dateRange": {
                    "regex": "none",
                    "alertText": "* غير صحيح",
                    "alertText2": "التواريخ"
                },
                "dateTimeRange": {
                    "regex": "none",
                    "alertText": "* غير صحيح",
                    "alertText2": "الوقت"
                },
                "minSize": {
                    "regex": "none",
                    "alertText": "* الحد الادنى ",
                    "alertText2": " حروف"
                },
                "maxSize": {
                    "regex": "none",
                    "alertText": "* الحد الاقصى  ",
                    "alertText2": "حروف"
                },
                "groupRequired": {
                    "regex": "none",
                    "alertText": " يجب ادخال بي حقل واحد على الاقل من الحقول الاتية*"
                },
                "min": {
                    "regex": "none",
                    "alertText": "* قيمة الحد الادنى  "
                },
                "max": {
                    "regex": "none",
                    "alertText": "* قيمة الحد االاقصى "
                },
                "past": {
                    "regex": "none",
                    "alertText": "* Date prior to "
                },
                "future": {
                    "regex": "none",
                    "alertText": "* Date past "
                },	
                "maxCheckbox": {
                    "regex": "none",
                    "alertText": "* Maximum ",
                    "alertText2": " options allowed"
                },
                "minCheckbox": {
                    "regex": "none",
                    "alertText": "* Please select ",
                    "alertText2": " options"
                },
                "equals": {
                    "regex": "none",
                    "alertText": "غير مساوى لحقل كلمة المرور"
                },
                "creditCard": {
                    "regex": "none",
                    "alertText": "* Invalid credit card number"
                },
                "phone": {
                    // credit: jquery.h5validate.js / orefalo
                    "regex": /^([\+][0-9]{1,3}[\ \.\-])?([\(]{1}[0-9]{2,6}[\)])?([0-9\ \.\-\/]{3,20})((x|ext|extension)[\ ]?[0-9]{1,4})?$/,
                    "alertText": "رقم الهاتف غير صحيح"
                },
                "email": {
                    // HTML5 compatible email regex ( http://www.whatwg.org/specs/web-apps/current-work/multipage/states-of-the-type-attribute.html#    e-mail-state-%28type=email%29 )
                    "regex": /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
                    "alertText": "البريد الالكترونى غير صحيح"
                },
                "integer": {
                    "regex": /^[\-\+]?\d+$/,
                    "alertText": "* الحقل يجب ان يحتوى على رقم"
                },
                "number": {
                    // Number, including positive, negative, and floating decimal. credit: orefalo
                    "regex": /^[\-\+]?((([0-9]{1,3})([,][0-9]{3})*)|([0-9]+))?([\.]([0-9]+))?$/,
                    "alertText": "* الجقل يجب ان يحتوى على رقم زو علمة عشرية"
                },
                "date": {                    
                    //	Check if date is valid by leap year
			"func": function (field) {
					var pattern = new RegExp(/^(\d{4})[\/\-\.](0?[1-9]|1[012])[\/\-\.](0?[1-9]|[12][0-9]|3[01])$/);
					var match = pattern.exec(field.val());
					if (match == null)
					   return false;
	
					var year = match[1];
					var month = match[2]*1;
					var day = match[3]*1;					
					var date = new Date(year, month - 1, day); // because months starts from 0.
	
					return (date.getFullYear() == year && date.getMonth() == (month - 1) && date.getDate() == day);
				},                		
			 "alertText": "* Invalid date, must be in YYYY-MM-DD format"
                },
                "datetime": {                    
                    //	Check if date is valid by leap year
			"func": function (field) {
					var pattern = new RegExp(/^(\d{4})[\/\-\.](0?[1-9]|1[012])[\/\-\.](0?[1-9]|[12][0-9]|3[01])$/); // ^(([0]?[1-9]|1[0-2])/([0-2]?[0-9]|3[0-1])/[1-2]\d{3}) (20|21|22|23|[0-1]?\d{1}):([0-5]?\d{1})$
					var match = pattern.exec(field.val());
					if (match == null)
					   return false;
	
					var year = match[1];
					var month = match[2]*1;
					var day = match[3]*1;					
					var date = new Date(year, month - 1, day); // because months starts from 0.
	
					return (date.getFullYear() == year && date.getMonth() == (month - 1) && date.getDate() == day);
				},                		
			 "alertText": "* Invalid date, must be in YYYY-MM-DD hh:ii:ss format"
                },
                "ipv4": {
                    "regex": /^((([01]?[0-9]{1,2})|(2[0-4][0-9])|(25[0-5]))[.]){3}(([0-1]?[0-9]{1,2})|(2[0-4][0-9])|(25[0-5]))$/,
                    "alertText": "* Invalid IP address"
                },
                "url": {
                    "regex": /^(https?|ftp):\/\/(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)?(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(\#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/i,
                    "alertText": "* Invalid URL"
                },
                "onlyNumberSp": {
                    "regex": /^[0-9\ ]+$/,
                    "alertText": "* Numbers only"
                },
                 "UserName": {
                    "regex":/^[a-z0-9_-]+$/i,
                    "alertText": " اسم المستخدم  غير صحيح"
                },
                        
                        
                "onlyLetterSp": {
                    "regex": /^[a-zA-Z\ \']+$/,
                    "alertText": "* Letters only"
                },
                "onlyLetterNumber":{
                    "regex": /^[0-9a-zA-Z]+$/,
                    "alertText": "الحقل يجب ان يحتوى على حروف وارقام فقط"
                },
                "onlyLetterNumber_":{
                    "regex": /^([A-Z]{2})+[0-9]+$/,
                    "alertText": "الحقل يجب ان يحتوى على كود رحلة صحيح"
                },
                // --- CUSTOM RULES -- Those are specific to the demos, they can be removed or changed to your likings
                "ajaxUserCall": {
                     
                },
                "ajaxUserCallPhp": {
                    "url":'',// validate_username_url,
                    // you may want to pass extra data on the ajax call
                    "extraData": "edit=false",
                    "alertTextOk": "* This username is available",
                    "alertText": "* This user is already taken",
                    "alertTextLoad": "* Validating, please wait"
                    
                },
                
                "ajaxNameCall": {
                    // remote json service location
                    "url": "ajaxValidateFieldName",
                    // error
                    "alertText": "* This name is already taken",
                    // if you provide an "alertTextOk", it will show as a green prompt when the field validates
                    "alertTextOk": "* This name is available",
                    // speaks by itself
                    "alertTextLoad": "* Validating, please wait"
                },
				 "ajaxNameCallPhp": {
	                    // remote json service location
	                    "url": "phpajax/ajaxValidateFieldName.php",
	                    // error
	                    "alertText": "* This name is already taken",
	                    // speaks by itself
	                    "alertTextLoad": "* Validating, please wait"
	                },
                "validate2fields": {
                    "alertText": "* Please input HELLO"
                },
	            //tls warning:homegrown not fielded 
                "dateFormat":{
                    "regex": /^\d{4}[\/\-](0?[1-9]|1[012])[\/\-](0?[1-9]|[12][0-9]|3[01])$|^(?:(?:(?:0?[13578]|1[02])(\/|-)31)|(?:(?:0?[1,3-9]|1[0-2])(\/|-)(?:29|30)))(\/|-)(?:[1-9]\d\d\d|\d[1-9]\d\d|\d\d[1-9]\d|\d\d\d[1-9])$|^(?:(?:0?[1-9]|1[0-2])(\/|-)(?:0?[1-9]|1\d|2[0-8]))(\/|-)(?:[1-9]\d\d\d|\d[1-9]\d\d|\d\d[1-9]\d|\d\d\d[1-9])$|^(0?2(\/|-)29)(\/|-)(?:(?:0[48]00|[13579][26]00|[2468][048]00)|(?:\d\d)?(?:0[48]|[2468][048]|[13579][26]))$/,
                    "alertText": "* Invalid Date"
                },
                //tls warning:homegrown not fielded 
                "dateTimeFormat": {
	                "regex": /^\d{4}[\/\-](0?[1-9]|1[012])[\/\-](0?[1-9]|[12][0-9]|3[01])\s+(1[012]|0?[1-9]){1}:(0?[1-5]|[0-6][0-9]){1}:(0?[0-6]|[0-6][0-9]){1}\s+(am|pm|AM|PM){1}$|^(?:(?:(?:0?[13578]|1[02])(\/|-)31)|(?:(?:0?[1,3-9]|1[0-2])(\/|-)(?:29|30)))(\/|-)(?:[1-9]\d\d\d|\d[1-9]\d\d|\d\d[1-9]\d|\d\d\d[1-9])$|^((1[012]|0?[1-9]){1}\/(0?[1-9]|[12][0-9]|3[01]){1}\/\d{2,4}\s+(1[012]|0?[1-9]){1}:(0?[1-5]|[0-6][0-9]){1}:(0?[0-6]|[0-6][0-9]){1}\s+(am|pm|AM|PM){1})$/,
                    "alertText": "* Invalid Date or Date Format",
                    "alertText2": "Expected Format: ",
                    "alertText3": "mm/dd/yyyy hh:mm:ss AM|PM or ", 
                    "alertText4": "yyyy-mm-dd hh:mm:ss AM|PM"
                },
                "rarrival_datetime": {
                    "func": function (field) {
                        startdate = field.val();
                        enddate  = $('#trip_return_departure_datetime' + field.attr('rel')).val();
                        var d1 = new Date(startdate);
                        var d2 = new Date(enddate);
                        var d3 = new Date(d2.getTime() + (24 * 60 * 60 * 1000));
                        if (d2 > d1 || d3 < d1){
                           return false;
                        }else{
                            return true;
                        }
                    },                		
                        "alertText": "* يجب ان يكون تاريخ الوصول بعد تاريخ الاقلاع، ولا اكثر من تاريخ الاقلاع الا بيوم واحد فقط"
                },
                "garrival_datetime": {
                    "func": function (field) {
                        startdate = field.val();
                        enddate  = $('#trip_going_departure_datetime' + field.attr('rel')).val();
                        var d1 = new Date(startdate);
                        var d2 = new Date(enddate);
                        var d3 = new Date(d2.getTime() + (24 * 60 * 60 * 1000));
                        if (d2 > d1 || d3 < d1){
                           return false;
                        }else{
                            return true;
                        }
                    },                		
                        "alertText": "* يجب ان يكون تاريخ الوصول بعد تاريخ الاقلاع، ولا اكثر من تاريخ الاقلاع الا بيوم واحد فقط"
                },
                "hotel_checkin_datetime_1": {
                    "func": function (field) {
                        var arrival_date, arrival_rel, average_arrival_date;
                        // ENSURE IT'S THE FIRST HOTEL
                        if(field.attr('rel') != 1)
                            return true;
                        // SEEK FOR THE ARRIVAL SEGMENT AND CHECK IT'S ARRIVAL DATE
                        $('.trip_going_safa_externalsegmenttype_id').each(function() {
                            if($(this).val() == 1)
                                arrival_rel = $(this).attr('rel');
                        });
                        if(arrival_rel){
                            arrival_date_ = $('#trip_going_arrival_datetime' + arrival_rel).val().split(' ');
                            arrival_date = arrival_date_[0];
                        }
                        else
                            arrival_date = $('#date').val();
    
                        var d1 = new Date(arrival_date);
                        var d2 = new Date(field.val());
                        var d3 = new Date(d1.getTime() + (24 * 60 * 60 * 1000));

                        if (d2 < d1 || d3 < d2){
                            return false;
                        }else{
                            return true;
                        }
                    },
                        "alertText": "تاريخ غير صحيح، يرجي التحقق منه."
                },
                "hotel_checkin_datetime": {
                    "func": function (field) {
                        // ENSURE IT'S THE FIRST HOTEL
                        if(field.attr('rel') == 1)
                            return true;

                         // LATEST CHECKOUT DATE
                        var d1 = new Date($('#trip_hotel_checkout_datetime' + (field.attr('rel') - 1)).val());
                        var d2 = new Date(field.val());

                        if (d1 > d2){ //|| d1 < d2
                            return false;
                        }else{
                            return true;
                        }
                    },
                        "alertText": "تاريخ غير صحيح، يرجي التحقق منه."
                },
                "trip_hotel_checkout_datetime": {
                    "func": function (field) {
                        // CHECKIN DATE
                        var checkin_date = $('#trip_hotel_checkin_datetime' + field.attr('rel')).val();
                        // NIGHTS
                        var nights = $('#trip_hotel_nights_count' + field.attr('rel')).val();
                        
                        var d1 = new Date(checkin_date); // CHECKIN
                        var d2 = new Date(field.val()); // CHECKOUT
                        var d3 = new Date(d1.getTime() + (nights * 24 * 60 * 60 * 1000)); // EXPECTED
                        var d4 = new Date(d3.getTime() + (24 * 60 * 60 * 1000)); // EXPECTED + 1 day
                       
                        if (d2 > d4 || d2 < d3){
                            return false;
                        }else{
                            return true;
                        }
                    },
                        "alertText": "تاريخ غير صحيح، يرجي التحقق منه."
                },
                "trip_going_departure_datetime": {
                    "func": function (field) {
                        // CHECKIN DATE
                        var trip_date = $('#date').val() + ' 00:00';
                        // NIGHTS
                        var segment_date = field.val();
                        
                        var d1 = new Date(trip_date); // CHECKIN
                        var d2 = new Date(segment_date); // CHECKOUT
                        //var d3 = new Date();
                        
                        if (d1 > d2){
                            return false;
                        }else{
                          if(field.attr('rel') == '1')
                            return true;
    
                          var d3 = new Date($('#trip_going_arrival_datetime' + (field.attr('rel')-1)).val());
                          if(d3 > d2)
                            return false;
                            
                            return true;
                        }
                    },
                        "alertText": "تاريخ غير صحيح، يرجي التحقق منه."
                },
                "trip_return_departure_datetime": {
                    "func": function (field) {
//                        // CHECK LATEST ARRIVAL DATE ?
                        var max_var = 0;
                        $( ".trip_going_departure_datetime" ).each(function( index ) {
                           max_var = index + 1;
                        });
                        // CHECKIN DATE
                        var latest_arrival = $('#trip_going_arrival_datetime' + max_var).val();
                        // NIGHTS
                        var segment_date = field.val();
                        
                        var d1 = new Date(latest_arrival); // CHECKIN
                        var d2 = new Date(segment_date); // CHECKOUT
                        //var d3 = new Date();
                        if (d1 > d2){
                            return false;
                        }else{
                          if(field.attr('rel') == '1')
                            return true;
    
                          var d3 = new Date($('#trip_return_arrival_datetime' + (field.attr('rel')-1)).val());
                          if(d3 < d2)
                            return false;
                            
                            return true;
                        }
                    },
                        "alertText": "تاريخ غير صحيح، يرجي التحقق منه."
                },
                "trip_going_safa_externalsegmenttype_id": {
                    "func": function (field) {
                        var gseek = true;
                        $( ".trip_going_safa_externalsegmenttype_id" ).each(function( index ) {
                            if(field.val() == '4' && $(this).val() == '1' && field.attr('rel') > $(this).attr('rel'))
                                gseek = false;
                        });
                            return gseek;
                    },
                        "alertText": "لا يمكن انشاء مقطع ترانزيت بعد مقطع وصول"
                },
                "trip_return_safa_externalsegmenttype_id": {
                    "func": function (field) {
                        var gseek = true;
                        $( ".trip_return_safa_externalsegmenttype_id" ).each(function( index ) {
                            if(field.val() == '3' && $(this).val() == '2' && field.attr('rel') > $(this).attr('rel'))
                                gseek = false;
                        });
                            return gseek;
                    },
                        "alertText": "لا يمكن انشاء مقطع داخلي بعد مقطع مغادرة"
                },
                "one_arrival_segment": {
                    "func": function (field) {
                        var gseek = 0;
                        $( ".trip_going_safa_externalsegmenttype_id" ).each(function( index ) {
                            if($(this).val() == 1)
                                gseek++;
                        });
                            if(gseek == 1)
                                return true;
                            else
                                return false;
                            
                    },
                        "alertText": "لابد من وجود مقطع وصول واحد"
                },
                "trip_hotel_checkout_datetime_": {
                    "func": function (field) {
                        // SELECT Latest HOTEL
                        
                        var trip_hotel_checkout_datetime = $('.trip_hotel_checkout_datetime').last().attr('rel');
//                        
                        if(field.attr('rel') != trip_hotel_checkout_datetime)
                            return true;
                        // trip_return_safa_externalsegmenttype_id = 2
//                        var trip_return_safa_externalsegmenttype_id = false;
//                        $( ".trip_return_safa_externalsegmenttype_id" ).each(function( index ) {
//                            if($(this).val() == '2')
//                                trip_return_safa_externalsegmenttype_id = $(this).attr('rel');
//                        });
                        var trip_return_safa_externalsegmenttype_id = $('.trip_return_safa_externalsegmenttype_id option[value="2"]').parent().attr('rel'); // REL OF EXTERNAL SEGMENT
                        var trip_return_departure_datetime = $('#trip_return_departure_datetime' + trip_return_safa_externalsegmenttype_id).val().split(' '); // DEPARTURE DATE OF EXTERNAL SEGMENT
                        var checkout_date = field.val(); // CHECKOUT DATE
                        
                        var checkout = new Date(checkout_date); // CHECKOUT
                        var departure = new Date(trip_return_departure_datetime[0]); // Departure Date
                        var expected = new Date(departure.getTime() - (24 * 60 * 60 * 1000)); // EXPECTED Departure - 1 day
                        if (departure >= checkout && checkout >= expected){
                            return true;
                        }else{
                            return false;
                        }
                    },
                        "alertText": "لا بد ان يكون تاريخ الخروج من آخر فندق متناسب مع تاريخ رحلة العودة."
                },
                "trip_tourismplace_datetime": {
                    "func": function (field) {
                        var result = false;
                        // FIELD CITY
                        var city = $('#trip_tourismplace_city_id' + field.attr('rel')).val();
                        
                        // FIELD DATE -> VALUE
                        var date = field.val();
                        
                        // SEEK FOR SAME CITY WITH COMPATABLE DATE
                        $('.trip_hotel_erp_city_id').each(function( index ){
                            if($(this).val() == city)
                            {
                                var dateofsightseen_ = field.val().split(' ');
                                var dateofsightseen = new Date(dateofsightseen_[0]);
                                
                                // checkin date 
                                var checkin = new Date($("#trip_hotel_checkin_datetime" + $(this).attr('rel')).val());
                                // checkout date
                                var checkout = new Date($("#trip_hotel_checkout_datetime" + $(this).attr('rel')).val());
                                if(checkin <= dateofsightseen && dateofsightseen <= checkout)
                                    result = true;
                            }
                        });
                         return result;                       
                    },
                        "alertText": "لايمكن اضافة مزار لمدينة في حال لم يكن لديه اقامة فيها"
                },
            };
            
        }
    };

    $.validationEngineLanguage.newLang();
    
})(jQuery);
