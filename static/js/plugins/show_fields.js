$(document).ready(function() {
    $("#sortable").sortable();
    check_box_operations();
    $("#update_sortable").click(function(){
         check_for_checked($(this))
    })
});
function check_box_operations() {
    $("#sortable").find("input").each(function(index, object) {
        if ($(object).is(":checked")) {
            $(object).attr("checked", "checked");
        }
        else {
            $(object).removeAttr("checked");
        }
        $(object).click(function() {
            if ($(object).is(":checked")) {
                $(object).attr("checked", "checked");
            }
            else {
                $(object).removeAttr("checked");
            }
        })

    })
}
function check_for_checked(button){
    var i=0;
    var fields=new Array();
    $("#sortable").find("input").each(function(index,object){
        if($(object).attr("checked")){
            fields[i]=$(object).val();
            i++;
        } 
    });
    if(fields.length<1){
        fields=null;
    }
   if(fields!=null){
       var request_url=  $(button).attr("pulg-url");
       var grid_module=$(button).attr("mod-name");
      $.post(request_url,{fields:fields.toString(),module_name:grid_module},function(data){
         if(data=="1"){
              location.reload();
         }
      })
   }
   else{
       $("#show_hide_fields_error_alert").parent().parent().css({"display":"block"});
   }
}
  $("#remove_widget").click(function(){
     var widget=document.getElementById("show_fields_plugin");
       widget.parentNode.removeChild(widget);
       var script=document.getElementsByClassName("show_fields_plugin")[0];
       script.parentNode.removeChild(script);
  });
