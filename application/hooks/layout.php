<?php

class Layout {

    public $layout = 'new';
    public $plugins = array();
    public $head_embed = null;
    private $destination = 'ea';
    private $closed = TRUE;

    public function __construct() {
        $this->CI = & get_instance();;
    }
    public function render() {
        $this->close_message();
        
        $output = $this->CI->output->get_output();
            
        if( ! empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') 
            $this->layout = 'ajax';

        if (isset($this->CI->layout))
            $this->layout = $this->CI->layout;

        if (isset($this->CI->destination))
            $this->destination = $this->CI->destination;

        if ($this->layout == 'none')
            return;

        if ($this->CI->output->enable_profiler === TRUE) {
            $this->CI->load->library('profiler');
            if (!empty($this->CI->output->_profiler_sections)) {
                $this->CI->profiler->set_sections($this->CI->output->_profiler_sections);
            }
            $output = $output . $this->CI->profiler->run();
        }
        elseif($this->CI->output->enable_profiler)
        {
            $this->CI->load->library('profiler');
            if (!empty($this->CI->output->_profiler_sections)) {
                $this->CI->profiler->set_sections($this->CI->output->_profiler_sections);
            }
            $headers  = 'MIME-Version: 1.0' . "\r\n";
            $headers .= 'Content-type: text/html; charset=utf8' . "\r\n";
            mail($this->CI->output->enable_profiler, 'Profiler', $this->CI->profiler->run(), $headers);
        }
        
        echo str_replace('{layout}', $output, $this->CI->load->view('layout/' . $this->layout, array('destination' => $this->destination, 'plugins' => $this->plugins, 'head_embed' => $this->head_embed), TRUE));
    }
    
    private function close_message() {
        if ( config('close_status') && strtotime(config('close_from')) <= time() && strtotime(config('close_to')) >= time() ) {
            echo $this->CI->load->view('error', array(
                'title' => lang('global_system_message'),
                'message' => str_replace(array('{from_time}' . '{to_time}'), array(
                    $this->CI->config->item('close_from'), 
                    $this->CI->config->item('close_message')
                ), $this->CI->config->item('close_to'))
            ), TRUE); exit();
        }
       
    }

}
