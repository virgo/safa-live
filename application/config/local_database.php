<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$active_group = 'default';
$query_builder = TRUE;

if ($_SERVER['SERVER_NAME'] == 'localhost')
{
    $db['default'] = array(
            'dsn'	=> '',
            'hostname' =>'localhost',
            'username' => 'root',
            'password' => 'root',
            'database' => 'safa_live',
            'dbdriver' => 'mysql',
            'dbprefix' => '',
            'pconnect' => FALSE,
            'db_debug' => TRUE,
            'cache_on' => FALSE,
            'cachedir' => '',
            'char_set' => 'utf8',
            'dbcollat' => 'utf8_general_ci',
            'swap_pre' => '',
            'autoinit' => FALSE,
            'encrypt' => FALSE,
            'compress' => FALSE,
            'stricton' => FALSE,
            'failover' => array()
    );
    $db['flight_status'] = array(
            'dsn'	=> '',
            'hostname' => 'localhost',
            'username' => 'root',
            'password' => 'root',
            'database' => 'flight_status',
            'dbdriver' => 'mysql',
            'dbprefix' => '',
            'pconnect' => FALSE,
            'db_debug' => TRUE,
            'cache_on' => FALSE,
            'cachedir' => '',
            'char_set' => 'utf8',
            'dbcollat' => 'utf8_general_ci',
            'swap_pre' => '',
            'autoinit' => FALSE,
            'encrypt' => FALSE,
            'compress' => FALSE,
            'stricton' => FALSE,
            'failover' => array()
    );
}    
else
{
    $db['default'] = array(
            'dsn'	=> '',
            'hostname' => '192.168.1.112',
            'username' => 'root',
            'password' => 'root',
            'database' => 'safa_live',
            'dbdriver' => 'mysql',
            'dbprefix' => '',
            'pconnect' => FALSE,
            'db_debug' => TRUE,
            'cache_on' => FALSE,
            'cachedir' => '',
            'char_set' => 'utf8',
            'dbcollat' => 'utf8_general_ci',
            'swap_pre' => '',
            'autoinit' => FALSE,
            'encrypt' => FALSE,
            'compress' => FALSE,
            'stricton' => FALSE,
            'failover' => array()
    );

    $db['flight_status'] = array(
            'dsn'	=> '',
            'hostname' => '192.168.1.112',
            'username' => 'root',
            'password' => 'root',
            'database' => 'flight_status',
            'dbdriver' => 'mysql',
            'dbprefix' => '',
            'pconnect' => FALSE,
            'db_debug' => TRUE,
            'cache_on' => FALSE,
            'cachedir' => '',
            'char_set' => 'utf8',
            'dbcollat' => 'utf8_general_ci',
            'swap_pre' => '',
            'autoinit' => FALSE,
            'encrypt' => FALSE,
            'compress' => FALSE,
            'stricton' => FALSE,
            'failover' => array()
    );    
}

if(! defined('SLDB'))
define('SLDB', $db['default']['database']);
if(! defined('FSDB'))
define('FSDB', $db['flight_status']['database']);
   
/* End of file database.php */
/* Location: ./application/config/database.php */
