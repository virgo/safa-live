<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$route['default_controller'] = 'welcome';
$route['404_override'] = 'errors/fourohfour';
$route['admin'] = 'login/index/admin';
$route['uo'] = 'login/index/uo';
$route['ea'] = 'login/index/ea';
$route['ito'] = 'login/index/ito';
$route['gov'] = 'login/index/gov';
$route['hm'] = 'login/index/hm';
$route['logout'] = 'login/logout';

$route['admin/logout'] = 'admin/login/logout';
$route['uo/logout'] ='uo/login/logout';
$route['ea/logout'] = 'ea/login/logout';
$route['ito/logout'] = 'ito/login/logout';
$route['gov/logout'] = 'gov/login/logout';
$route['hm/logout'] = 'hm/login/logout';
$route['error'] = 'welcome/error';
$route['css'] = 'minify/css';
$route['js'] = 'minify/js';
$route['cronjob'] = 'flight/cronjob';


/* End of file routes.php */
/* Location: ./application/config/routes.php */