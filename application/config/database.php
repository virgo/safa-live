<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


if(ENVIRONMENT == 'development')
    include 'local_database.php';
else
    include 'online_database.php';