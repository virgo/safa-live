<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

if ($_SERVER['SERVER_NAME'] == 'beta.uo.umrah-booking.com')
    $database = 'new';
elseif ($_SERVER['SERVER_NAME'] == 'www.safalive.com')
    $database = '1435';
else
    $database = '1435';

$active_group = 'default';
$query_builder = TRUE;

$db['default'] = array(
    'dsn' => '',
    'hostname' => 'uo.umrah-booking.com',
    'username' => 'uoumrah_global',
    'password' => '1L#gR{$VlhbR',
    'database' => 'uoumrah_' . $database,
    'dbdriver' => 'mysql',
    'dbprefix' => '',
    'pconnect' => FALSE,
    'db_debug' => TRUE,
    'cache_on' => FALSE,
    'cachedir' => '',
    'char_set' => 'utf8',
    'dbcollat' => 'utf8_general_ci',
    'swap_pre' => '',
    'autoinit' => TRUE,
    'encrypt' => FALSE,
    'compress' => FALSE,
    'stricton' => FALSE,
    'failover' => array()
);
$db['flight_status'] = array(
    'dsn' => '',
    'hostname' => 'uo.umrah-booking.com',
    'username' => 'uoumrah_global',
    'password' => '1L#gR{$VlhbR',
    'database' => 'uoumrah_flight_status',
    'dbdriver' => 'mysql',
    'dbprefix' => '',
    'pconnect' => FALSE,
    'db_debug' => TRUE,
    'cache_on' => FALSE,
    'cachedir' => '',
    'char_set' => 'utf8',
    'dbcollat' => 'utf8_general_ci',
    'swap_pre' => '',
    'autoinit' => TRUE,
    'encrypt' => FALSE,
    'compress' => FALSE,
    'stricton' => FALSE,
    'failover' => array()
);

define('SLDB', $db['default']['database']);
define('FSDB', $db['flight_status']['database']);

/* End of file database.php */
/* Location: ./application/config/database.php */