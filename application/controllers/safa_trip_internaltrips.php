<?php

class Safa_trip_internaltrips extends Safa_Controller {

	function __construct() {
		parent::__construct();

		//Side menu session, By Gouda.
		if (session('uo_id')) {
			session('side_menu_id', 5);
		} else if (session('ea_id')) {
			session('side_menu_id', 3);
		} else if (session('ito_id')) {
			session('side_menu_id', 3);
		}

		$this->layout = 'new';

		$this->load->model('trip_internaltrip_model');
		$this->load->model('flight_availabilities_model');
		$this->load->model('erp_cruise_availabilities_model');

		$this->load->model('notification_model');
		$this->load->model('ports_model');
		$this->load->model('port_halls_model');
		$this->load->model('safa_uos_model');
		$this->load->model('internaltrip_status_model');
		$this->load->model('safa_trips_model');
		$this->load->model('contracts_model');
		$this->load->model('internalpassages_model');
		$this->load->model('hotels_model');
		$this->load->model('trip_details_model');

		$this->load->model('hotelroomsizes_model');
		$this->load->model('safa_group_passports_model');

		$this->load->model('internalsegments_drivers_and_buses_model');
		$this->load->model('transporters_drivers_model');
		$this->load->model('transporters_buses_model');
		$this->load->model('tourismplaces_model');
		$this->load->model('transporters_model');

		$this->load->model('erp_cruise_availabilities_model');
		$this->load->model('erp_cruise_availabilities_detail_model');

		$this->load->model('safa_uos_departments_model');
		$this->load->model('eas_model');
		$this->load->model('itos_model');

		$this->load->model('safa_buses_brands_model');
		$this->load->model('safa_buses_models_model');

		$this->load->model('safa_reservation_forms_model');
		$this->load->model('safa_passport_accommodation_rooms_model');
		$this->load->model('safa_group_passport_accommodation_model');


		$this->lang->load('trip_internaltrip');
		$this->lang->load('internalpassages');
		$this->lang->load('flight_availabilities');
		$this->lang->load('cruise_availabilities');

		$this->lang->load('admins/transporters');
		$this->lang->load('trip_details');
		$this->lang->load('trips_reports');

		$this->load->helper('form_helper');
		$this->load->helper('db_helper');
		$this->load->helper('itconf_helper');
		$this->load->helper('trip');

		$this->load->library('hijrigregorianconvert');

		permission();

		if (session('uo_id')) {
			$this->trip_internaltrip_model->uo_id = session('uo_id');
			$this->safa_trips_model->safa_uo_id = session('uo_id');
		} else if (session('ea_id')) {
			$this->trip_internaltrip_model->ea_id = session('ea_id');
			$this->safa_trips_model->safa_ea_id = session('ea_id');
		} else if (session('ito_id')) {
			$this->trip_internaltrip_model->ito_id = session('ito_id');
			$this->safa_trips_model->safa_ito_id = session('ito_id');
		}
	}

	function index() {
		if (session('uo_id')) {
			$this->safa_uos_model->safa_uo_id = session('uo_id');
			$safa_uo_row = $this->safa_uos_model->get();
			$data['safa_uo_code'] = $safa_uo_row->code;
		} else if (session('ea_id')) {

			//			$this->contracts_model->safa_uo_contract_id = $this->input->post('uo_contract_id');
			//         	$contracts_row=$this->contracts_model->get();
			//         	if(count($contracts_row)>0) {
			//        		$this->notification_model->detail_receiver_id = $contracts_row->safa_uo_id ;
			//        		$this->notification_model->saveDetails();
			//         	}
			//			$data['safa_ea_code'] = $safa_ea_row->code;
		}


		if (session('uo_id')) {
			$data["safa_uo_contracts"] = ddgen('safa_uo_contracts', array('safa_uo_contract_id', name()), array('safa_uo_id' => session('uo_id')), FALSE, FALSE);
		} else if (session('ea_id')) {
			$safa_uo_contracts_rows = $this->trip_internaltrip_model->get_ea_contracts();
			$safa_uo_contracts_arr = array();
			$safa_uo_contracts_arr[""] = lang('global_select_from_menu');
			foreach ($safa_uo_contracts_rows as $safa_uo_contracts_row) {
				$safa_uo_contracts_arr[$safa_uo_contracts_row->contract_id] = $safa_uo_contracts_row->safa_uo_contracts_eas_name;
			}
			$data["safa_uo_contracts"] = $safa_uo_contracts_arr;
		} else if (session('ito_id')) {

			$safa_ito_contracts_rows = $this->trip_internaltrip_model->get_ito_contracts();
			$safa_uo_contracts_arr = array();
			$safa_uo_contracts_arr[""] = lang('global_select_from_menu');
			foreach ($safa_ito_contracts_rows as $safa_uo_contracts_row) {
				$safa_uo_contracts_arr[$safa_uo_contracts_row->contract_id] = $safa_uo_contracts_row->contract_name;
			}
			$data["safa_uo_contracts"] = $safa_uo_contracts_arr;
		}

		$data["bus_transporters"] = $this->create_transportes_array();
		$data["safa_intrernaltripstatus"] = ddgen("safa_internaltripstatus", array("safa_internaltripstatus_id", name()));
		$data["safa_intrernaltripstatus_color"] = $this->db->select(name() . ',color,code')->from('safa_internaltripstatus')->get()->result();

		if (session('uo_id')) {
			$data["safa_ito"] = $this->get_uo_contracts_itos();
		} else if (session('ea_id')) {
			$data["safa_ito"] = $this->get_ea_contracts_itos();
		}

		if (isset($_GET['search'])) {
			$this->search();
		}

		$data["total_rows"] = $this->trip_internaltrip_model->get(true);

		$this->trip_internaltrip_model->offset = $this->uri->segment("3");
		//$this->trip_internaltrip_model->limit = $this->config->item('per_page');
		$this->trip_internaltrip_model->order_by = array('serial', 'desc');
		$data["items"] = $this->trip_internaltrip_model->get();
		$config['uri_segment'] = 3;
		$config['base_url'] = site_url('safa_trip_internaltrips/index');
		$config['total_rows'] = $data["total_rows"];

		$config['per_page'] = $this->config->item('per_page');
		$config['suffix'] = $this->input->get() ? '?' . http_build_query($_GET) : NULL;
		$this->load->library('pagination');
		$this->pagination->initialize($config);
		$data['pagination'] = $this->pagination->create_links();
		$this->load->view('safa_trip_internaltrips/index', $data);
	}

	function get_uo_contracts_itos() {
		$result = $this->trip_internaltrip_model->get_uo_contracts_itos();
		$itos = array();
		$itos[""] = lang('global_select_from_menu');
		$name = name();
		foreach ($result as $itos_id => $ito) {
			$itos[$ito->safa_ito_id] = $ito->$name;
		}
		return $itos;
	}

	function get_ea_contracts_itos() {
		$result = $this->trip_internaltrip_model->get_ea_contracts_itos();
		$itos = array();
		$itos[""] = lang('global_select_from_menu');
		$name = name();
		foreach ($result as $itos_id => $ito) {
			$itos[$ito->safa_ito_id] = $ito->$name;
		}
		return $itos;
	}

	function create_transportes_array() {
		if (session('uo_id')) {
			$result = $this->trip_internaltrip_model->get_uo_transporters_withContracts();
		} else { //if(session('ea_id'))
			$result = $this->trip_internaltrip_model->get_transporters_withContracts();
		}

		$transporters = array();
		$transporters[""] = lang('global_select_from_menu');
		$name = name();
		foreach ($result as $transporter_id => $transporter) {
			$transporters[$transporter->transporter_id] = $transporter->$name;
		}
		return $transporters;
	}

	function get_uo_contract_trips($safa_uo_contract_id = FALSE) {
		$this->layout = 'ajax'; //  to prevent the hook in loading the header in the response//
		$trips_ea = $this->trip_internaltrip_model->get_uo_contract_trips($safa_uo_contract_id);
		$trips = array();
		echo "<option value=''>" . lang('global_select_from_menu') . "</option>";
		if (isset($trips_ea) && count($trips_ea) > 0) {

			foreach ($trips_ea as $trip) {
				echo "<option value='" . $trip->safa_trip_id . "'>" . $trip->name . "</option>";
			}
		}
	}

	function search() {
		if ($this->input->get("safa_trip_internaltrip_id"))
		$this->trip_internaltrip_model->safa_trip_internaltrip_id = $this->input->get("safa_trip_internaltrip_id");
		if ($this->input->get("safa_trip_id"))
		$this->trip_internaltrip_model->safa_trip_id = $this->input->get("safa_trip_id");
		if ($this->input->get("safa_ito_id"))
		$this->trip_internaltrip_model->safa_ito_id = $this->input->get("safa_ito_id");
		if ($this->input->get("safa_transporter_id"))
		$this->trip_internaltrip_model->safa_transporter_id = $this->input->get("safa_transporter_id");
		if ($this->input->get("safa_tripstatus_id"))
		$this->trip_internaltrip_model->safa_tripstatus_id = $this->input->get("safa_tripstatus_id");
		if ($this->input->get("safa_internaltripstatus_id"))
		$this->trip_internaltrip_model->safa_internaltripstatus_id = $this->input->get("safa_internaltripstatus_id");
		if ($this->input->get("operator_reference"))
		$this->trip_internaltrip_model->operator_reference = $this->input->get("operator_reference");
		if ($this->input->get("attachement"))
		$this->trip_internaltrip_model->attachement = $this->input->get("attachement");
		if ($this->input->get("datetime"))
		$this->trip_internaltrip_model->datetime = $this->input->get("datetime");

		if ($this->input->get("safa_uo_contract_id")) {
			$this->trip_internaltrip_model->safa_uo_contract_id = $this->input->get("safa_uo_contract_id");
		}


		if ($this->input->get("serial"))
		$this->trip_internaltrip_model->serial = $this->input->get("serial");
		if ($this->input->get("confirmation_number"))
		$this->trip_internaltrip_model->confirmation_number = $this->input->get("confirmation_number");

		if ($this->input->get("arrival_date_from"))
		$this->trip_internaltrip_model->arrival_date_from = $this->input->get("arrival_date_from");
		if ($this->input->get("arrival_date_to"))
		$this->trip_internaltrip_model->arrival_date_to = $this->input->get("arrival_date_to");
		if ($this->input->get("leaving_date_from"))
		$this->trip_internaltrip_model->leaving_date_from = $this->input->get("leaving_date_from");
		if ($this->input->get("leaving_date_to"))
		$this->trip_internaltrip_model->leaving_date_to = $this->input->get("leaving_date_to");
	}

	function manage($id = FALSE) {

		$this->db->trans_start();

		if (session('uo_id')) {
			$this->safa_uos_model->safa_uo_id = session('uo_id');
			$safa_uo_row = $this->safa_uos_model->get();
			$data['safa_uo_code'] = $safa_uo_row->code;

			$safa_uo_row = $this->trip_internaltrip_model->get_max_serial_by_uo(session('uo_id'));
			$data['safa_uo_serial'] = $safa_uo_row->max_serial + 1;
		}

		$data['safa_uos'] = ddgen('safa_uos', array('safa_uo_id', name()), FALSE, FALSE, FALSE);
		
		$data['cities'] = ddgen('erp_cities', array('erp_city_id', name()), array('erp_country_id' => 966), FALSE, TRUE);
		$data['hotels'] = ddgen('erp_hotels', array('erp_hotel_id', name()), array('erp_city_id' => 1), array(name(), 'ASC'), TRUE);

		//$all_flights_rows = $this->flight_availabilities_model->get_for_trip();
		//$data['all_flights_rows'] = $all_flights_rows;

		$data['erp_flight_classes'] = ddgen('erp_flight_classes', array('erp_flight_class_id', 'name'), false, false, true);
		$data['erp_flight_status'] = ddgen('erp_flight_status', array('erp_flight_status_id', 'name'), false, false, true);
		$data['erp_path_types'] = ddgen('erp_path_types', array('erp_path_type_id', name()), false, false, true);

		//$data['safa_tourismplaces'] = ddgen('safa_tourismplaces', array('safa_tourismplace_id', name()), FALSE, FALSE, TRUE);
		$safa_tourismplaces_rows = $this->tourismplaces_model->get();
		$safa_tourismplaces_arr = array();
		//$safa_tourismplaces_arr[""] = lang('global_select_from_menu');
		$safa_tourismplaces_counter = 0;
		foreach ($safa_tourismplaces_rows as $safa_tourismplaces_row) {
			$safa_tourismplaces_arr[$safa_tourismplaces_row->safa_tourismplace_id . '---' . $safa_tourismplaces_row->erp_city_id] = $safa_tourismplaces_row->{name()};
		}
		$data['safa_tourismplaces'] = $safa_tourismplaces_arr;


		$data['erp_hotelroomsizes'] = $this->hotelroomsizes_model->get();

		$data['erp_transportertypes'] = ddgen('erp_transportertypes', array('erp_transportertype_id', name()), FALSE, FALSE, TRUE);


		$first_erp_port_id = 0;
		$erp_ports_rows = $this->ports_model->get_all_have_halls();
		//echo $this->db->last_query()."\n<br />\n";
		$erp_ports_arr = array();
		$erp_ports_arr[""] = lang('global_select_from_menu');
		$erp_ports_counter = 0;
		foreach ($erp_ports_rows as $erp_ports_row) {
			$erp_ports_arr[$erp_ports_row->erp_port_id] = $erp_ports_row->code;

			if ($erp_ports_counter == 0) {
				$first_erp_port_id = $erp_ports_row->erp_port_id;
			}
			$erp_ports_counter++;
		}
		$data["erp_ports"] = $erp_ports_arr;

		if (!$id) {
			$this->port_halls_model->erp_port_id = $first_erp_port_id;
		}
		$erp_ports_halls_rows = $this->port_halls_model->get();
		//echo $this->db->last_query()."\n<br />\n";
		$erp_ports_halls_arr = array();
		$erp_ports_halls_arr[""] = lang('global_select_from_menu');
		foreach ($erp_ports_halls_rows as $erp_ports_halls_row) {
			$erp_ports_halls_arr[$erp_ports_halls_row->erp_port_hall_id] = $erp_ports_halls_row->{name()};
		}
		$data["erp_ports_halls"] = $erp_ports_halls_arr;


		$first_sa_erp_port_id = 0;
		$erp_sa_ports_rows = $this->ports_model->get_all_have_halls('SA');
		//echo $this->db->last_query()."\n<br />\n";
		$erp_sa_ports_arr = array();
		$erp_sa_ports_arr[""] = lang('global_select_from_menu');
		$erp_sa_ports_counter = 0;
		foreach ($erp_sa_ports_rows as $erp_sa_ports_row) {
			$erp_sa_ports_arr[$erp_sa_ports_row->erp_port_id] = $erp_sa_ports_row->code;
			if ($erp_sa_ports_counter == 0) {
				$first_sa_erp_port_id = $erp_sa_ports_row->erp_port_id;
			}
			$erp_sa_ports_counter++;
		}
		$data["erp_sa_ports"] = $erp_sa_ports_arr;


		$this->port_halls_model->country_code = 'SA';
		if (!$id) {
			$this->port_halls_model->erp_port_id = $first_sa_erp_port_id;
		}
		$erp_sa_ports_halls_rows = $this->port_halls_model->get();
		$erp_sa_ports_halls_arr = array();
		//$erp_sa_ports_halls_arr[""] = lang('global_select_from_menu');
		foreach ($erp_sa_ports_halls_rows as $erp_sa_ports_halls_row) {
			$erp_sa_ports_halls_arr[$erp_sa_ports_halls_row->erp_port_hall_id] = $erp_sa_ports_halls_row->{name()};
		}
		$data["erp_sa_ports_halls"] = $erp_sa_ports_halls_arr;


		//------------- Land ---------------------------------------------------
		$this->ports_model->safa_transportertype_id = 1;
		$this->ports_model->country_code = 'SA';
		$land_erp_ports_sa_rows = $this->ports_model->get();
		$land_erp_ports_sa_arr = array();
		foreach ($land_erp_ports_sa_rows as $land_erp_ports_sa_row) {
			$land_erp_ports_sa_arr[$land_erp_ports_sa_row->erp_port_id] = $land_erp_ports_sa_row->{name()};
		}
		$data["land_erp_ports_sa"] = $land_erp_ports_sa_arr;
		//------------- Sea  ---------------------------------------------------
		$this->ports_model->safa_transportertype_id = 3;
		$sea_erp_ports_rows = $this->ports_model->get();
		$sea_erp_ports_arr = array();
		foreach ($sea_erp_ports_rows as $sea_erp_ports_row) {
			$sea_erp_ports_arr[$sea_erp_ports_row->erp_port_id] = $sea_erp_ports_row->{name()};
		}
		$data["sea_erp_ports"] = $sea_erp_ports_arr;

		$this->ports_model->country_code = 'SA';
		$sea_erp_ports_sa_rows = $this->ports_model->get();
		$sea_erp_ports_sa_arr = array();
		foreach ($sea_erp_ports_sa_rows as $sea_erp_ports_sa_row) {
			$sea_erp_ports_sa_arr[$sea_erp_ports_sa_row->erp_port_id] = $sea_erp_ports_sa_row->{name()};
		}
		$data["sea_erp_ports_sa"] = $sea_erp_ports_sa_arr;
		//--------------------------------------------------------------------------------
		//		$erp_airlines_query = $this->db->query('select ' . FSDB . '.fs_airlines.fs_airline_id, CONCAT(safa_transporters.'.name().',\' \', ' . FSDB . '.`fs_airlines`.`iata`) as transporter_code_name from  ' . FSDB . '.`fs_airlines`, safa_transporters where  safa_transporters.code=' . FSDB . '.fs_airlines.iata order by  transporter_code_name');
		//		$erp_airlines_rows = $erp_airlines_query->result();
		//		//$erp_airlines_rows = $this->db->order_by('iata', 'asc')->where("iata <> ''")->get(FSDB . '.fs_airlines')->result();
		//		$erp_airlines_arr = array();
		//		foreach ($erp_airlines_rows as $erp_airlines_row) {
		//			$erp_airlines_arr[$erp_airlines_row->fs_airline_id] = $erp_airlines_row->transporter_code_name;
		//		}
		//		$data["erp_airlines"] = $erp_airlines_arr;
		//$data["safa_transporters"] = ddgen('safa_transporters', array('code', name()),array('erp_transportertype_id'=>2), FALSE, TRUE);

		$safa_transporters_query = $this->db->query('select safa_transporters.code, CONCAT(safa_transporters.' . name() . ',\' \', `safa_transporters`.`code`) as transporter_code_name from  safa_transporters where  safa_transporters.erp_transportertype_id=2 order by  transporter_code_name');
		$safa_transporters_rows = $safa_transporters_query->result();
		//$safa_transporters_rows = $this->db->order_by('iata', 'asc')->where("iata <> ''")->get(FSDB . '.fs_airlines')->result();
		$safa_transporters_arr = array();
		foreach ($safa_transporters_rows as $safa_transporters_row) {
			$safa_transporters_arr[$safa_transporters_row->code] = $safa_transporters_row->transporter_code_name;
		}
		$data["flight_transporters"] = $safa_transporters_arr;

		$safa_transporters_query = $this->db->query('select safa_transporters.code, CONCAT(safa_transporters.' . name() . ',\' \', `safa_transporters`.`code`) as transporter_code_name from  safa_transporters where  safa_transporters.erp_transportertype_id=3 order by  transporter_code_name');
		$safa_transporters_rows = $safa_transporters_query->result();
		//$safa_transporters_rows = $this->db->order_by('iata', 'asc')->where("iata <> ''")->get(FSDB . '.fs_airlines')->result();
		$safa_transporters_arr = array();
		foreach ($safa_transporters_rows as $safa_transporters_row) {
			$safa_transporters_arr[$safa_transporters_row->code] = $safa_transporters_row->transporter_code_name;
		}
		$data["cruise_transporters"] = $safa_transporters_arr;


		if (session('uo_id')) {
			$data["safa_uo_contracts"] = ddgen('safa_uo_contracts', array('safa_uo_contract_id', name()), array('safa_uo_id' => session('uo_id')), FALSE, TRUE);
		} else if (session('ea_id')) {
			$safa_uo_contracts_rows = $this->trip_internaltrip_model->get_ea_contracts();
			$safa_uo_contracts_arr = array();
			$safa_uo_contracts_arr[""] = lang('global_select_from_menu');
			foreach ($safa_uo_contracts_rows as $safa_uo_contracts_row) {
				$safa_uo_contracts_arr[$safa_uo_contracts_row->contract_id] = $safa_uo_contracts_row->safa_uo_contracts_eas_name;
			}
			$data["safa_uo_contracts"] = $safa_uo_contracts_arr;
		} else if (session('ito_id')) {

			$safa_ito_contracts_rows = $this->trip_internaltrip_model->get_ito_contracts();
			$safa_uo_contracts_arr = array();
			$safa_uo_contracts_arr[""] = lang('global_select_from_menu');
			foreach ($safa_ito_contracts_rows as $safa_uo_contracts_row) {
				$safa_uo_contracts_arr[$safa_uo_contracts_row->contract_id] = $safa_uo_contracts_row->contract_name;
			}
			$data["safa_uo_contracts"] = $safa_uo_contracts_arr;
		}


		$data["bus_transporters"] = $this->create_transportes_array();


		//$data["safa_intrernaltripstatus"] = ddgen("safa_internaltripstatus", array("safa_internaltripstatus_id", name()));
		$safa_intrernaltripstatus_rows = $this->internaltrip_status_model->get();
		$safa_intrernaltripstatus_arr = array();
		foreach ($safa_intrernaltripstatus_rows as $safa_intrernaltripstatus_row) {
			if ($id) {
				$safa_intrernaltripstatus_arr[$safa_intrernaltripstatus_row->safa_internaltripstatus_id] = $safa_intrernaltripstatus_row->{name()};
			} else {
				if (session('ea_id')) {
					if ($safa_intrernaltripstatus_row->safa_internaltripstatus_id == 1 || $safa_intrernaltripstatus_row->safa_internaltripstatus_id == 2) {
						$safa_intrernaltripstatus_arr[$safa_intrernaltripstatus_row->safa_internaltripstatus_id] = $safa_intrernaltripstatus_row->{name()};
					}
				} else if (session('uo_id')) {
					if ($safa_intrernaltripstatus_row->safa_internaltripstatus_id == 4) {
						$safa_intrernaltripstatus_arr[$safa_intrernaltripstatus_row->safa_internaltripstatus_id] = $safa_intrernaltripstatus_row->{name()};
					}
				} else {
					$safa_intrernaltripstatus_arr[$safa_intrernaltripstatus_row->safa_internaltripstatus_id] = $safa_intrernaltripstatus_row->{name()};
				}
			}
		}
		$data["safa_intrernaltripstatus"] = $safa_intrernaltripstatus_arr;


		$data["safa_internalpassagetypes"] = ddgen("safa_internalsegmenttypes", array("safa_internalsegmenttype_id", name()));
		$data["erp_hotels"] = ddgen("erp_hotels", array("erp_hotel_id", name()), '', '', true);

		$data["safa_internaltrip_types"] = ddgen("safa_internaltrip_types", array("safa_internaltrip_type_id", name()), FALSE, FALSE, TRUE);



		//--------------- Drivers And Buses -------------------------------------------------------
		$structure = array('safa_transporters_drivers_id', name());
		$key = $structure['0'];
		if (isset($structure['1'])) {
			$value = $structure['1'];
		}
		$drivers_arr = array();
		//$this->transporters_drivers_model->safa_transporters_id = $safa_transporters_id;
		$drivers = $this->transporters_drivers_model->get();
		$drivers_arr[""] = lang('global_select_from_menu');
		foreach ($drivers as $driver) {
			$drivers_arr[$driver->$key] = $driver->$value;
		}
		$data['safa_transporters_drivers'] = $drivers_arr;


		$structure = array('safa_transporters_buses_id', 'bus_no');
		$key = $structure['0'];
		if (isset($structure['1'])) {
			$value = $structure['1'];
		}
		$buses_arr = array();
		//$this->transporters_buses_model->safa_transporters_id = $safa_transporters_id;
		$buses = $this->transporters_buses_model->get();
		$buses_arr[""] = lang('global_select_from_menu');
		foreach ($buses as $bus) {
			$buses_arr[$bus->$key] = $bus->$value;
		}
		$data['safa_transporters_buses'] = $buses_arr;

		$data["safa_buses_brands"] = ddgen("safa_buses_brands", array("safa_buses_brands_id", name()), FALSE, FALSE, FALSE);

		//$data["safa_buses_models"] = ddgen("safa_buses_models", array("safa_buses_models_id", name()), FALSE, FALSE, TRUE);
		$data["safa_buses_models"] = array();
		//-----------------------------------------------------------------------------------------



		if (session('uo_id')) {
			$data["safa_ito"] = $this->get_uo_contracts_itos();
		} else if (session('ea_id')) {
			$data["safa_ito"] = $this->get_ea_contracts_itos();
		}

		$data['item_safa_reservation_forms_rows']=array();
		$data['going_trip_flights_rows_count'] = 0;
		$data['cruise_availabilities_details_rows_count'] = 0;
			
		if ($id) {
			if (session('uo_id')) {
				$this->trip_internaltrip_model->safa_uo_id = session('uo_id');
			} else if (session('ea_id')) {
				$this->trip_internaltrip_model->safa_ea_id = session('ea_id');
			} else if (session('ito_id')) {
				$this->trip_internaltrip_model->owner_erp_company_type_id = 5;
				$this->trip_internaltrip_model->safa_ito_id = session('ito_id');
			}
			$this->trip_internaltrip_model->safa_trip_internaltrip_id = $id;
			$item = $this->trip_internaltrip_model->get();
			if(!$item) show_404();

			$data['item'] = $item;

			//------------------------ Going Flights ----------------------------------------
			//$this->flight_availabilities_model->erp_path_type_id=1;
			$this->flight_availabilities_model->erp_flight_availability_id = $item->erp_flight_availability_id;
			$going_trip_flights_rows = $this->flight_availabilities_model->get_for_trip();
			$data['going_trip_flights_rows'] = $going_trip_flights_rows;
			$data['going_trip_flights_rows_count'] = count($going_trip_flights_rows);

			$this->flight_availabilities_model->erp_flight_availability_id = $item->erp_flight_availability_id;
			$flight_availabilities_details_rows = $this->flight_availabilities_model->get_details_for_trip();
			$data['flight_availabilities_details_rows_count'] = count($flight_availabilities_details_rows);
			
			$arr_going_trip_flights = array();
			foreach ($going_trip_flights_rows as $going_trip_flights_row) {
				$arr_going_trip_flights[$going_trip_flights_row->erp_flight_availability_id] = $going_trip_flights_row->erp_flight_availability_id;
			}
			//session('going_trip_flights_session', $arr_going_trip_flights);
			$_SESSION['going_trip_flights_session'] = $arr_going_trip_flights;

			//-------------------------------------------------------------------------------
			//------------------------ Cruises ----------------------------------------
			$this->erp_cruise_availabilities_detail_model->erp_cruise_availability_id = $item->erp_cruise_availability_id;
			$going_trip_cruises_rows = $this->erp_cruise_availabilities_detail_model->get();
			$data['cruise_availabilities_details_rows'] = $going_trip_cruises_rows;
			$data['cruise_availabilities_details_rows_count'] = count($going_trip_cruises_rows);

			$arr_going_trip_cruises = array();
			foreach ($going_trip_cruises_rows as $going_trip_cruises_row) {
				$arr_going_trip_cruises[$going_trip_cruises_row->erp_cruise_availability_id] = $going_trip_cruises_row->erp_cruise_availability_id;
			}
			$_SESSION['cruise_availabilities_details_session'] = $arr_going_trip_cruises;
			//-------------------------------------------------------------------------------


			/* --------- get the internal passages ----------------------------------------------------- */
			$this->internalpassages_model->safa_trip_internaltrip_id = $id;
			$this->internalpassages_model->order_by = array("safa_internalsegments.start_datetime", 'ASC');
			$data["internalpassages"] = $internalpassages_rows = $this->internalpassages_model->get();

			$this->internalpassages_model->safa_trip_internaltrip_id = $id;
			$this->internalpassages_model->order_by = array("safa_internalsegment_id", 'ASC');
			$data["internalpassages_hotels"] = $this->internalpassages_model->get_hotels();

			$this->internalpassages_model->safa_trip_internaltrip_id = $id;
			$this->internalpassages_model->order_by = array("safa_internalsegment_id", 'ASC');
			$data["internalpassages_tourismplaces"] = $this->internalpassages_model->get_tourismplaces();

			/* ----------------------------------------------------------------------------------------- */



			//---------- Details --------------------------
			//			$this->safa_package_execlusive_meals_model->safa_package_id = $id;
			//			$item_execlusive_meals_prices = $this->safa_package_execlusive_meals_model->get();
			//			$data['item_execlusive_meals_prices'] = $item_execlusive_meals_prices;
			//-----------------------------------------------


			$this->db->from('safa_trip_internaltrips_hotels');
			$this->db->where('safa_trip_internaltrip_id', $id);
			$data['safa_trip_internaltrips_hotels_rows'] = $this->db->get()->result();


			//-------------- Safa Group Passports -------------------
			$this->safa_group_passports_model->safa_trip_internaltrip_id = $id;
			$data['safa_group_passports'] = $this->safa_group_passports_model->get_for_table();
			//-------------------------------------------------------

			//--------------- Drivers And Buses -------------------------------------------------------
			$data["item_drivers_and_buses"] = array();
			foreach ($internalpassages_rows as $internalpassages_row) {
				if ($internalpassages_row->safa_internalsegmenttype_id == 1) {
					$data["item_drivers_and_buses"] = $this->internalsegments_drivers_and_buses_model->get_distinct_bus_brands_by_safa_internalsegment($internalpassages_row->safa_internalsegment_id);
				}
			}
			//-----------------------------------------------------------------------------------------

			//--------------- Accomodation Forms -------------------------------------------------------
			$this->safa_reservation_forms_model->safa_trip_id = $item->safa_trip_id;
			$item_safa_reservation_forms_rows = $this->safa_reservation_forms_model->get();
			$data['item_safa_reservation_forms_rows'] = $item_safa_reservation_forms_rows;
			//-----------------------------------------------------------------------------------------
		}


		$this->load->library("form_validation");
		$this->form_validation->set_rules('datetime', 'lang:transport_request_date', 'trim|required');
		//$this->form_validation->set_rules('safa_uo_contract_id', 'lang:transport_request_contract', 'trim|required');
		//$this->form_validation->set_rules('safa_trip_id', 'lang:transport_request_trip_id', 'trim|required');
		$this->form_validation->set_rules('safa_internaltripstatus_id', 'lang:transport_request_status', 'trim|required');

		if (session('uo_id') || session('ea_id')) {
			if ($this->input->post('safa_internaltrip_type_id') == '1') {
				$this->form_validation->set_rules('safa_ito_id', 'lang:transport_request_opertator', 'trim|required');
			}
		}

		$this->form_validation->set_rules('safa_transporter_id', 'lang:transport_request_transportername', 'trim');

		$this->form_validation->set_rules('operator_reference', 'lang:transport_request_res_code', 'trim');
		$this->form_validation->set_rules('text_input', '', 'trim');

		$this->form_validation->set_rules('erp_transportertype_id', 'lang:erp_transportertype_id', 'trim|required');

		if (session('uo_id') || session('ea_id')) {
			$this->form_validation->set_rules('safa_internaltrip_type_id', 'lang:safa_internaltrip_type_id', 'trim|required');
			$this->form_validation->set_rules('safa_uo_contract_id', 'lang:transport_request_contract', 'trim|required');
		} else if (session('ito_id')) {
			$this->form_validation->set_rules('safa_uo_id', 'lang:safa_uo_id', 'trim|required');
		}

		if (!$id) {
			if (session('uo_id')) {
				//$this->form_validation->set_rules('serial', 'lang:serial', 'trim|required|unique_col_by_uo[safa_trip_internaltrips.serial]');
				if ($this->input->post('confirmation_number') != '0') {
					//$this->form_validation->set_rules('confirmation_number', 'lang:confirmation_number', 'trim|required|unique_col_by_uo[safa_trip_internaltrips.confirmation_number]');
				}
			} else if (session('ea_id')) {
				if ($this->input->post('confirmation_number') != '0') {
					//$this->form_validation->set_rules('confirmation_number', 'lang:confirmation_number', 'trim|required|unique_col_by_ea[safa_trip_internaltrips.confirmation_number]');
				}
			}
		}

		if ($this->input->post('text_input') && $_FILES['userfile']['name']) {
			$this->form_validation->set_rules('userfile', 'lang:userfile', 'trim|callback_fileupload');
		}


		if ($this->form_validation->run() == false) {
			$this->load->view("safa_trip_internaltrips/manage", $data);
		} else {

			if (isset($_FILES["transport_request_res_file"])) {
				if (count($_FILES["transport_request_res_file"]['tmp_name']) > 0) {
					//Get the temp file path
					$tmpFilePath = $_FILES["transport_request_res_file"]['tmp_name'];

					//Make sure we have a filepath
					if ($tmpFilePath != "") {
						//Setup our new file path
						$newFilePath = './static/temp/uo_files/' . $_FILES["transport_request_res_file"]['name'];

						//Upload the file into the temp dir
						//To solve arabic files names problem.
						$is_file_uploaded = move_uploaded_file($tmpFilePath, iconv('utf-8', 'windows-1256', $newFilePath));
						//$is_file_uploaded=move_uploaded_file($tmpFilePath, $newFilePath);



						$file_name = $_FILES["transport_request_res_file"]['name'];
						$this->trip_internaltrip_model->attachement = $file_name;
					}
				}
			}

			if ($this->input->post('hdn_safa_trip_id')) {
				//$this->trip_internaltrip_model->safa_trip_id = $this->input->post('safa_trip_id');
			} else {
				// $this->trip_internaltrip_model->safa_trip_id = null;
			}

			if (session('uo_id')) {
				$this->trip_internaltrip_model->owner_erp_company_type_id = 2;
				$this->trip_internaltrip_model->safa_uo_id = session('uo_id');
			} else if (session('ea_id')) {
				$this->trip_internaltrip_model->owner_erp_company_type_id = 3;
				$this->trip_internaltrip_model->safa_ea_id = session('ea_id');
			}

			if (session('ito_id')) {
				$this->trip_internaltrip_model->owner_erp_company_type_id = 5;
				
				$this->trip_internaltrip_model->safa_ito_id = session('ito_id');
				
				$this->trip_internaltrip_model->safa_uo_id = $this->input->post('safa_uo_id');
				
			} else {
				if ($this->input->post('safa_ito_id'))
				$this->trip_internaltrip_model->safa_ito_id = $this->input->post('safa_ito_id');
			}

			if ($this->input->post('safa_transporter_id'))
			$this->trip_internaltrip_model->safa_transporter_id = $this->input->post('safa_transporter_id');

			$this->trip_internaltrip_model->safa_internaltripstatus_id = $this->input->post('safa_internaltripstatus_id');
			$this->trip_internaltrip_model->operator_reference = $this->input->post('operator_reference');
			//$this->trip_internaltrip_model->attachement = $file_name;
			$this->trip_internaltrip_model->datetime = $this->input->post('datetime');

			//$this->trip_internaltrip_model->erp_company_type_id = 3;
			//$this->trip_internaltrip_model->erp_company_id = $this->input->post('uo_ea_id');
			//$this->trip_internaltrip_model->trip_title = $this->input->post('trip_title');

			$safa_uo_code = '';
			$safa_uo_serial = '';
			if ($id==0) {
				if (session('uo_id')) {
					//$this->trip_internaltrip_model->trip_title = $this->input->post('safa_uo_code') . '-' . $this->input->post('serial');
					//$this->trip_internaltrip_model->serial = $this->input->post('serial');

					
					$this->safa_uos_model->safa_uo_id = session('uo_id');
					$safa_uo_row = $this->safa_uos_model->get();
					if (count($safa_uo_row) > 0) {
						$safa_uo_code = $safa_uo_row->code;
					}

					$safa_uo_row = $this->trip_internaltrip_model->get_max_serial_by_uo(session('uo_id'));
					if (count($safa_uo_row) > 0) {
						$safa_uo_serial = $safa_uo_row->max_serial + 1;
					}

					$this->trip_internaltrip_model->trip_title = $safa_uo_code . '-' . $safa_uo_serial;
					$this->trip_internaltrip_model->serial = $safa_uo_serial;

				} else if (session('ea_id') ) {

					if (!$id && $this->input->post('hdn_safa_trip_internaltrip_id') == 0) {
						$this->contracts_model->safa_uo_contract_id = $this->input->post('safa_uo_contract_id');
						$contracts_row = $this->contracts_model->get();
						if (count($contracts_row) > 0) {
							
							$this->safa_uos_model->safa_uo_id = $contracts_row->safa_uo_id;
							$safa_uo_row = $this->safa_uos_model->get();
							if (count($safa_uo_row) > 0) {
								$safa_uo_code = $safa_uo_row->code;
							}

							$safa_uo_row = $this->trip_internaltrip_model->get_max_serial_by_uo($contracts_row->safa_uo_id);
							if (count($safa_uo_row) > 0) {
								$safa_uo_serial = $safa_uo_row->max_serial + 1;
							}

							$this->trip_internaltrip_model->trip_title = $safa_uo_code . '-' . $safa_uo_serial;
							$this->trip_internaltrip_model->serial = $safa_uo_serial;
						}
					}
				} else if ( session('ito_id')) {
					
					if (!$id && $this->input->post('hdn_safa_trip_internaltrip_id') == 0) {
						
							$this->safa_uos_model->safa_uo_id =$this->input->post('safa_uo_id');
							$safa_uo_row = $this->safa_uos_model->get();
							if (count($safa_uo_row) > 0) {
								$safa_uo_code = $safa_uo_row->code;
							}

							$safa_uo_row = $this->trip_internaltrip_model->get_max_serial_by_uo($this->input->post('safa_uo_id'));
							if (count($safa_uo_row) > 0) {
								$safa_uo_serial = $safa_uo_row->max_serial + 1;
							}

							$this->trip_internaltrip_model->trip_title = $safa_uo_code . '-' . $safa_uo_serial;
							$this->trip_internaltrip_model->serial = $safa_uo_serial;
						
					}
				}
			} else {			
				$this->trip_internaltrip_model->trip_title = $this->input->post('safa_uo_code') . '-' . $this->input->post('serial');
				$this->trip_internaltrip_model->serial = $this->input->post('serial');
			}
				

			$this->trip_internaltrip_model->trip_supervisors = $this->input->post('trip_supervisors');
			$this->trip_internaltrip_model->trip_supervisors_phone = $this->input->post('trip_supervisors_phone');
			$this->trip_internaltrip_model->safa_uo_contract_id = $this->input->post('safa_uo_contract_id');

			$this->trip_internaltrip_model->adult_seats = $this->input->post('adult_seats');
			$this->trip_internaltrip_model->child_seats = $this->input->post('child_seats');
			$this->trip_internaltrip_model->baby_seats = $this->input->post('baby_seats');

			$this->trip_internaltrip_model->confirmation_number = $this->input->post('confirmation_number');

			$this->trip_internaltrip_model->erp_transportertype_id = $this->input->post('erp_transportertype_id');

			if (session('uo_id') || session('ea_id')) {
				$this->trip_internaltrip_model->safa_internaltrip_type_id = $this->input->post('safa_internaltrip_type_id');
			} else if (session('ito_id')) {
				$this->trip_internaltrip_model->safa_internaltrip_type_id = 1;
			}

			$this->trip_internaltrip_model->notes = $this->input->post('notes');


			$mode = 'add';
			$flight_availability_id = 0;
			$cruise_availability_id = 0;

			if ($id) {
				$this->trip_internaltrip_model->safa_trip_internaltrip_id = $id;
				$safa_trip_internaltrip_id = $id;


				if ($this->input->post('erp_transportertype_id') == 2) {
					$this->save_flight_availability($item->erp_flight_availability_id);
					$flight_availability_id = $item->erp_flight_availability_id;
					$this->trip_internaltrip_model->erp_flight_availability_id = $flight_availability_id;
				} else if ($this->input->post('erp_transportertype_id') == 3) {
					$this->save_cruise_availability($item->erp_cruise_availability_id);
					$cruise_availability_id = $item->erp_cruise_availability_id;
					$this->trip_internaltrip_model->erp_cruise_availability_id = $cruise_availability_id;
				}



				$this->trip_internaltrip_model->save();

				$mode = 'edit';
			} else if ($this->input->post('hdn_safa_trip_internaltrip_id') != 0) {
				$id = $this->input->post('hdn_safa_trip_internaltrip_id');

				$this->trip_internaltrip_model->safa_trip_internaltrip_id = $id;
				$safa_trip_internaltrip_id = $id;

				if ($this->input->post('erp_transportertype_id') == 2) {
					$flight_availability_id = $this->save_flight_availability();
					$this->trip_internaltrip_model->erp_flight_availability_id = $flight_availability_id;
				} else if ($this->input->post('erp_transportertype_id') == 3) {
					$cruise_availability_id = $this->save_cruise_availability();
					$this->trip_internaltrip_model->erp_cruise_availability_id = $cruise_availability_id;
				}

				$this->trip_internaltrip_model->save();

				$mode = 'edit';
			} else {

				if ($this->input->post('erp_transportertype_id') == 2) {
					$flight_availability_id = $this->save_flight_availability();
					$this->trip_internaltrip_model->erp_flight_availability_id = $flight_availability_id;
				} else if ($this->input->post('erp_transportertype_id') == 3) {
					$cruise_availability_id = $this->save_cruise_availability();
					$this->trip_internaltrip_model->erp_cruise_availability_id = $cruise_availability_id;
				}

				$safa_trip_internaltrip_id = $this->trip_internaltrip_model->save();
			}

			if (isset($safa_trip_internaltrip_id)) {


				$flight_seats = (int) $this->input->post('adult_seats') + (int) $this->input->post('child_seats') + (int) $this->input->post('baby_seats');
				$internal_seats = (int) $this->input->post('adult_seats') + (int) $this->input->post('child_seats');

				if ($id) {
					if ($this->input->post('hdn_are_new_segments_generated') == 1) {
						$this->add_internalsegments($safa_trip_internaltrip_id, $mode);
					} else {
						$this->add_internalsegments_for_edit_mode($safa_trip_internaltrip_id);
					}
				} else {
					$this->add_internalsegments($safa_trip_internaltrip_id, $mode);
				}

				$this->save_rooms_count($safa_trip_internaltrip_id);

				$this->update_safa_group_passports($safa_trip_internaltrip_id);


				if ($this->input->post('safa_uo_contract_id')) {
					$this->send_notification($safa_trip_internaltrip_id, $mode);
				}
			}



			$this->db->trans_complete();

			if ($id) {
				//				$this->load->view('redirect_new_message', array('msg' => lang('global_updated_message'),
				//                    'url' => site_url('safa_trip_internaltrips'),
				//                    'id' => $this->uri->segment("4"),
				//                    'model_title' => lang('safa_trip_internaltrips'), 'action' => lang('edit_safa_trip_internaltrips')));

				$this->load->view('safa_trip_internaltrips/confirmation_message', array('msg' => lang('edit_safa_trip_internaltrips'),
                    'back_url' => site_url('safa_trip_internaltrips'),
                    'id' => $safa_trip_internaltrip_id,
                    'confirmation_url' => site_url('safa_trip_internaltrips/pdf_export/' . $safa_trip_internaltrip_id . '/D'),
                    'model_title' => lang('safa_trip_internaltrips'), 'action' => lang('edit_safa_trip_internaltrips'),
				));
			} else {
				//					$this->load->view('redirect_new_message', array('msg' => lang('global_added_message'),
				//                    'url' => site_url('safa_trip_internaltrips'),
				//                    'id' => $this->uri->segment("4"),
				//                    'model_title' => lang('safa_trip_internaltrips'), 'action' => lang('add_safa_trip_internaltrips')));
				//$this->load->view('safa_trip_internaltrips/confirmation_message', array('msg' => lang('are_you_want_to_print_safa_trip_internaltrips'),
				$this->load->view('safa_trip_internaltrips/confirmation_message', array('msg' => lang('add_safa_trip_internaltrips').'<br/>'.lang('trip_code').': '.$safa_uo_code . '-' . $safa_uo_serial,
                    'back_url' => site_url('safa_trip_internaltrips'),
                    'id' => $safa_trip_internaltrip_id,
                    'confirmation_url' => site_url('safa_trip_internaltrips/pdf_export/' . $safa_trip_internaltrip_id . '/D'),
                    'model_title' => lang('safa_trip_internaltrips'), 'action' => lang('add_safa_trip_internaltrips'),
				));
			}
		}
	}

	function save_for_accommodation_by_ajax() {
		if ($this->input->post('hdn_safa_trip_internaltrip_id') == 0) {
			if (session('uo_id')) {
				$this->trip_internaltrip_model->trip_title = $this->input->post('safa_uo_code') . '-' . $this->input->post('serial');
				$this->trip_internaltrip_model->serial = $this->input->post('serial');

				$this->safa_trips_model->name = $this->input->post('safa_uo_code') . '-' . $this->input->post('serial');
			} else if (session('ea_id')) {

				$this->contracts_model->safa_uo_contract_id = $this->input->post('safa_uo_contract_id');
				$contracts_row = $this->contracts_model->get();
				if (count($contracts_row) > 0) {
					$safa_uo_code = '';
					$safa_uo_serial = '';

					$this->safa_uos_model->safa_uo_id = $contracts_row->safa_uo_id;
					$safa_uo_row = $this->safa_uos_model->get();
					if (count($safa_uo_row) > 0) {
						$safa_uo_code = $safa_uo_row->code;
					}

					$safa_uo_row = $this->trip_internaltrip_model->get_max_serial_by_uo($contracts_row->safa_uo_id);
					if (count($safa_uo_row) > 0) {
						$safa_uo_serial = $safa_uo_row->max_serial + 1;
					}

					$this->trip_internaltrip_model->trip_title = $safa_uo_code . '-' . $safa_uo_serial;
					$this->trip_internaltrip_model->serial = $safa_uo_serial;

					$this->safa_trips_model->name = $safa_uo_code . '-' . $safa_uo_serial;
				}
			}

			//------------------------------------------ Save Trip -----------------------------------------------
			$this->safa_trips_model->date = $this->input->post('datetime');

			if ($this->input->post('erp_transportertype_id') == 1) {
				$this->safa_trips_model->arrival_date = $this->input->post('land_going_datetime');
			} else if ($this->input->post('erp_transportertype_id') == 2) {
				$arrival_date_arr = $this->input->post('flight_arrival_date');
				if (isset($arrival_date_arr[0])) {
					$this->safa_trips_model->arrival_date = $arrival_date_arr[0];
				}
			} else if ($this->input->post('erp_transportertype_id') == 3) {
				$arrival_date_arr = $this->input->post('cruise_arrival_date');
				if (isset($arrival_date_arr[0])) {
					$this->safa_trips_model->arrival_date = $arrival_date_arr[0];
				}
			}

			$this->safa_trips_model->safa_tripstatus_id = 1;
			$this->safa_trips_model->erp_transportertype_id = $this->input->post('erp_transportertype_id');
			//$this->safa_trips_model->erp_country_id = $this->input->post('erp_country_id');
			$this->safa_trips_model->notes = $this->input->post('notes');

			$this->safa_trips_model->safa_trip_confirm_id = 2;
			$safa_trip_id = $this->safa_trips_model->save();
			//-----------------------------------------------------------------------------------------------------
			//		if (isset($_FILES["transport_request_res_file"])) {
			//			if (count($_FILES["transport_request_res_file"]['tmp_name']) > 0) {
			//				//Get the temp file path
			//				$tmpFilePath = $_FILES["transport_request_res_file"]['tmp_name'];
			//
			//				//Make sure we have a filepath
			//				if ($tmpFilePath != "") {
			//					//Setup our new file path
			//					$newFilePath = './static/temp/uo_files/' . $_FILES["transport_request_res_file"]['name'];
			//
			//					//Upload the file into the temp dir
			//					//To solve arabic files names problem.
			//					$is_file_uploaded = move_uploaded_file($tmpFilePath, iconv('utf-8', 'windows-1256', $newFilePath));
			//					//$is_file_uploaded=move_uploaded_file($tmpFilePath, $newFilePath);
			//
			//
			//
			//					$file_name = $_FILES["transport_request_res_file"]['name'];
			//					$this->trip_internaltrip_model->attachement = $file_name;
			//				}
			//			}
			//		}


			$this->trip_internaltrip_model->safa_trip_id = $safa_trip_id;

			if (session('uo_id')) {
				$this->trip_internaltrip_model->safa_uo_id = session('uo_id');
			} else if (session('ea_id')) {
				$this->trip_internaltrip_model->safa_ea_id = session('ea_id');
			}


			if ($this->input->post('safa_ito_id'))
			$this->trip_internaltrip_model->safa_ito_id = $this->input->post('safa_ito_id');
			if ($this->input->post('safa_transporter_id'))
			$this->trip_internaltrip_model->safa_transporter_id = $this->input->post('safa_transporter_id');

			$this->trip_internaltrip_model->safa_internaltripstatus_id = $this->input->post('safa_internaltripstatus_id');
			$this->trip_internaltrip_model->operator_reference = $this->input->post('operator_reference');
			//$this->trip_internaltrip_model->attachement = $file_name;
			$this->trip_internaltrip_model->datetime = $this->input->post('datetime');

			//$this->trip_internaltrip_model->erp_company_type_id = 3;
			//$this->trip_internaltrip_model->erp_company_id = $this->input->post('uo_ea_id');
			//$this->trip_internaltrip_model->trip_title = $this->input->post('trip_title');



			$this->trip_internaltrip_model->trip_supervisors = $this->input->post('trip_supervisors');
			$this->trip_internaltrip_model->trip_supervisors_phone = $this->input->post('trip_supervisors_phone');
			$this->trip_internaltrip_model->safa_uo_contract_id = $this->input->post('safa_uo_contract_id');

			$this->trip_internaltrip_model->adult_seats = $this->input->post('adult_seats');
			$this->trip_internaltrip_model->child_seats = $this->input->post('child_seats');
			$this->trip_internaltrip_model->baby_seats = $this->input->post('baby_seats');

			$this->trip_internaltrip_model->confirmation_number = $this->input->post('confirmation_number');

			$this->trip_internaltrip_model->erp_transportertype_id = $this->input->post('erp_transportertype_id');

			$this->trip_internaltrip_model->safa_internaltrip_type_id = $this->input->post('safa_internaltrip_type_id');

			$this->trip_internaltrip_model->notes = $this->input->post('notes');


			if ($this->input->post('erp_transportertype_id') == 2) {
				$flight_availability_id = $this->save_flight_availability();
				$this->trip_internaltrip_model->erp_flight_availability_id = $flight_availability_id;
			} else if ($this->input->post('erp_transportertype_id') == 3) {
				$cruise_availability_id = $this->save_cruise_availability();
				$this->trip_internaltrip_model->erp_cruise_availability_id = $cruise_availability_id;
			}

			$safa_trip_internaltrip_id = $this->trip_internaltrip_model->save();


			$data_arr[] = array('safa_trip_internaltrip_id' => $safa_trip_internaltrip_id,
                'safa_trip_id' => $safa_trip_id,
			);
			echo json_encode($data_arr);
			exit;
		} else {
			$data_arr[] = array('safa_trip_internaltrip_id' => $this->input->post('hdn_safa_trip_internaltrip_id'),
                'safa_trip_id' => $this->input->post('hdn_safa_trip_id'),
			);
			echo json_encode($data_arr);
			exit;
		}
	}

	function get_accommodation_hotels_passports_by_ajax() {
		$safa_ea_id = 0;
		if (session('ea_id')) {
			$safa_ea_id = session('ea_id');
		}

		$this->safa_reservation_forms_model->safa_ea_id = $safa_ea_id;
		$this->safa_reservation_forms_model->order_by = array('safa_reservation_form_id', 'desc');
		$this->safa_reservation_forms_model->limit = 1;
		$safa_reservation_forms_row = $this->safa_reservation_forms_model->get();

		//----------------------- Hotels -------------------------------
		$this->safa_passport_accommodation_rooms_model->safa_reservation_form_id = $safa_reservation_forms_row[0]->safa_reservation_form_id;
		$accommodation_hotels_rows = $this->safa_passport_accommodation_rooms_model->get_hotels();

		$accommodation_hotels_arr = array();
		$accommodation_hotels_counter = 0;
		foreach ($accommodation_hotels_rows as $accommodation_hotels_row) {
			$accommodation_hotels_arr[$accommodation_hotels_counter] = $accommodation_hotels_row;
			$accommodation_hotels_counter++;
		}

		//----------------------- Group Passports -------------------------------
		$safa_group_passport_id_rows = $this->safa_group_passport_accommodation_model->get_distinct_passports_by_trip_uo_form(false, false, $safa_reservation_forms_row[0]->safa_reservation_form_id);
		$safa_group_passport_id_arr = array();
		foreach ($safa_group_passport_id_rows as $safa_group_passport_id_row) {
			$safa_group_passport_id_arr[] = $safa_group_passport_id_row->safa_group_passport_id;
		}

		$this->safa_group_passports_model->safa_group_passport_id_arr = $safa_group_passport_id_arr;
		$safa_group_passport_accommodation_rows = $this->safa_group_passports_model->get_for_table();
		$safa_group_passport_accommodation_arr = array();
		$safa_group_passport_accommodation_counter = 0;
		foreach ($safa_group_passport_accommodation_rows as $safa_group_passport_accommodation_row) {
			$safa_group_passport_accommodation_arr[$safa_group_passport_accommodation_counter] = $safa_group_passport_accommodation_row;
			$safa_group_passport_accommodation_counter++;
		}

		$data_arr[] = array('accommodation_hotels_arr' => $accommodation_hotels_arr,
            'safa_group_passport_accommodation_arr' => $safa_group_passport_accommodation_arr,
            'safa_reservation_form_id' => $safa_reservation_forms_row[0]->safa_reservation_form_id,
		);
		echo json_encode($data_arr);
		exit;
	}

	function manage_design($id = FALSE) {

		$this->db->trans_start();

		if (session('uo_id')) {
			$this->safa_uos_model->safa_uo_id = session('uo_id');
			$safa_uo_row = $this->safa_uos_model->get();
			$data['safa_uo_code'] = $safa_uo_row->code;

			$safa_uo_row = $this->trip_internaltrip_model->get_max_serial_by_uo(session('uo_id'));
			$data['safa_uo_serial'] = $safa_uo_row->max_serial + 1;
		}


		$data['cities'] = ddgen('erp_cities', array('erp_city_id', name()), array('erp_country_id' => 966), FALSE, TRUE);
		$data['hotels'] = ddgen('erp_hotels', array('erp_hotel_id', name()), array('erp_city_id' => 1), array(name(), 'ASC'), TRUE);

		//$all_flights_rows = $this->flight_availabilities_model->get_for_trip();
		//$data['all_flights_rows'] = $all_flights_rows;

		$data['erp_flight_classes'] = ddgen('erp_flight_classes', array('erp_flight_class_id', 'name'), false, false, true);
		$data['erp_flight_status'] = ddgen('erp_flight_status', array('erp_flight_status_id', 'name'), false, false, true);
		$data['erp_path_types'] = ddgen('erp_path_types', array('erp_path_type_id', name()), false, false, true);
		$data['safa_tourismplaces'] = ddgen('safa_tourismplaces', array('safa_tourismplace_id', name()), FALSE, FALSE, TRUE);

		$data['erp_hotelroomsizes'] = $this->hotelroomsizes_model->get();

		$data['erp_transportertypes'] = ddgen('erp_transportertypes', array('erp_transportertype_id', name()), FALSE, FALSE, TRUE);


		$first_erp_port_id = 0;
		$erp_ports_rows = $this->ports_model->get_all_have_halls();
		$erp_ports_arr = array();
		$erp_ports_counter = 0;
		foreach ($erp_ports_rows as $erp_ports_row) {
			$erp_ports_arr[$erp_ports_row->erp_port_id] = $erp_ports_row->code;

			if ($erp_ports_counter == 0) {
				$first_erp_port_id = $erp_ports_row->erp_port_id;
			}
			$erp_ports_counter++;
		}
		$data["erp_ports"] = $erp_ports_arr;

		if (!$id) {
			$this->port_halls_model->erp_port_id = $first_erp_port_id;
		}
		$erp_ports_halls_rows = $this->port_halls_model->get();
		$erp_ports_halls_arr = array();
		foreach ($erp_ports_halls_rows as $erp_ports_halls_row) {
			$erp_ports_halls_arr[$erp_ports_halls_row->erp_port_hall_id] = $erp_ports_halls_row->{name()};
		}
		$data["erp_ports_halls"] = $erp_ports_halls_arr;


		$first_sa_erp_port_id = 0;
		$erp_sa_ports_rows = $this->ports_model->get_all_have_halls('SA');
		$erp_sa_ports_arr = array();
		$erp_sa_ports_counter = 0;
		foreach ($erp_sa_ports_rows as $erp_sa_ports_row) {
			$erp_sa_ports_arr[$erp_sa_ports_row->erp_port_id] = $erp_sa_ports_row->code;
			if ($erp_sa_ports_counter == 0) {
				$first_sa_erp_port_id = $erp_sa_ports_row->erp_port_id;
			}
			$erp_sa_ports_counter++;
		}
		$data["erp_sa_ports"] = $erp_sa_ports_arr;


		$this->port_halls_model->country_code = 'SA';
		if (!$id) {
			$this->port_halls_model->erp_port_id = $first_sa_erp_port_id;
		}
		$erp_sa_ports_halls_rows = $this->port_halls_model->get();
		$erp_sa_ports_halls_arr = array();
		foreach ($erp_sa_ports_halls_rows as $erp_sa_ports_halls_row) {
			$erp_sa_ports_halls_arr[$erp_sa_ports_halls_row->erp_port_hall_id] = $erp_sa_ports_halls_row->{name()};
		}
		$data["erp_sa_ports_halls"] = $erp_sa_ports_halls_arr;


		//------------- Sea And Land ---------------------------------------------------
		$this->ports_model->safa_transportertype_id = 1;
		$this->ports_model->country_code = 'SA';
		$land_erp_ports_sa_rows = $this->ports_model->get();
		$land_erp_ports_sa_arr = array();
		foreach ($land_erp_ports_sa_rows as $land_erp_ports_sa_row) {
			$land_erp_ports_sa_arr[$land_erp_ports_sa_row->erp_port_id] = $land_erp_ports_sa_row->{name()};
		}
		$data["land_erp_ports_sa"] = $land_erp_ports_sa_arr;

		$this->ports_model->safa_transportertype_id = 3;
		$this->ports_model->country_code = 'SA';
		$sea_erp_ports_sa_rows = $this->ports_model->get();
		$sea_erp_ports_sa_arr = array();
		foreach ($sea_erp_ports_sa_rows as $sea_erp_ports_sa_row) {
			$sea_erp_ports_sa_arr[$sea_erp_ports_sa_row->erp_port_id] = $sea_erp_ports_sa_row->{name()};
		}
		$data["sea_erp_ports_sa"] = $sea_erp_ports_sa_arr;
		//--------------------------------------------------------------------------------
		//		$erp_airlines_query = $this->db->query('select ' . FSDB . '.fs_airlines.fs_airline_id, CONCAT(safa_transporters.'.name().',\' \', ' . FSDB . '.`fs_airlines`.`iata`) as transporter_code_name from  ' . FSDB . '.`fs_airlines`, safa_transporters where  safa_transporters.code=' . FSDB . '.fs_airlines.iata order by  transporter_code_name');
		//		$erp_airlines_rows = $erp_airlines_query->result();
		//		//$erp_airlines_rows = $this->db->order_by('iata', 'asc')->where("iata <> ''")->get(FSDB . '.fs_airlines')->result();
		//		$erp_airlines_arr = array();
		//		foreach ($erp_airlines_rows as $erp_airlines_row) {
		//			$erp_airlines_arr[$erp_airlines_row->fs_airline_id] = $erp_airlines_row->transporter_code_name;
		//		}
		//		$data["erp_airlines"] = $erp_airlines_arr;
		//$data["safa_transporters"] = ddgen('safa_transporters', array('code', name()),array('erp_transportertype_id'=>2), FALSE, TRUE);

		$safa_transporters_query = $this->db->query('select safa_transporters.code, CONCAT(safa_transporters.' . name() . ',\' \', `safa_transporters`.`code`) as transporter_code_name from  safa_transporters where  safa_transporters.erp_transportertype_id=2 order by  transporter_code_name');
		$safa_transporters_rows = $safa_transporters_query->result();
		//$safa_transporters_rows = $this->db->order_by('iata', 'asc')->where("iata <> ''")->get(FSDB . '.fs_airlines')->result();
		$safa_transporters_arr = array();
		foreach ($safa_transporters_rows as $safa_transporters_row) {
			$safa_transporters_arr[$safa_transporters_row->code] = $safa_transporters_row->transporter_code_name;
		}
		$data["flight_transporters"] = $safa_transporters_arr;


		if (session('uo_id')) {
			$data["safa_uo_contracts"] = ddgen('safa_uo_contracts', array('safa_uo_contract_id', name()), array('safa_uo_id' => session('uo_id')), FALSE, TRUE);
		} else if (session('ea_id')) {
			$safa_uo_contracts_rows = $this->trip_internaltrip_model->get_ea_contracts();
			$safa_uo_contracts_arr = array();
			$safa_uo_contracts_arr[""] = lang('global_select_from_menu');
			foreach ($safa_uo_contracts_rows as $safa_uo_contracts_row) {
				$safa_uo_contracts_arr[$safa_uo_contracts_row->contract_id] = $safa_uo_contracts_row->safa_uo_contracts_eas_name;
			}
			$data["safa_uo_contracts"] = $safa_uo_contracts_arr;
		}


		$data["bus_transporters"] = $this->create_transportes_array();


		//$data["safa_intrernaltripstatus"] = ddgen("safa_internaltripstatus", array("safa_internaltripstatus_id", name()));
		$safa_intrernaltripstatus_rows = $this->internaltrip_status_model->get();
		$safa_intrernaltripstatus_arr = array();
		foreach ($safa_intrernaltripstatus_rows as $safa_intrernaltripstatus_row) {
			if ($id) {
				$safa_intrernaltripstatus_arr[$safa_intrernaltripstatus_row->safa_internaltripstatus_id] = $safa_intrernaltripstatus_row->{name()};
			} else {
				if (session('ea_id')) {
					if ($safa_intrernaltripstatus_row->safa_internaltripstatus_id == 1 || $safa_intrernaltripstatus_row->safa_internaltripstatus_id == 2) {
						$safa_intrernaltripstatus_arr[$safa_intrernaltripstatus_row->safa_internaltripstatus_id] = $safa_intrernaltripstatus_row->{name()};
					}
				} else if (session('uo_id')) {
					if ($safa_intrernaltripstatus_row->safa_internaltripstatus_id == 4) {
						$safa_intrernaltripstatus_arr[$safa_intrernaltripstatus_row->safa_internaltripstatus_id] = $safa_intrernaltripstatus_row->{name()};
					}
				}
			}
		}
		$data["safa_intrernaltripstatus"] = $safa_intrernaltripstatus_arr;


		$data["safa_internalpassagetypes"] = ddgen("safa_internalsegmenttypes", array("safa_internalsegmenttype_id", name()));
		$data["erp_hotels"] = ddgen("erp_hotels", array("erp_hotel_id", name()), '', '', true);

		$data["safa_internaltrip_types"] = ddgen("safa_internaltrip_types", array("safa_internaltrip_type_id", name()), FALSE, FALSE, TRUE);



		//--------------- Drivers And Buses -------------------------------------------------------
		$structure = array('safa_transporters_drivers_id', name());
		$key = $structure['0'];
		if (isset($structure['1'])) {
			$value = $structure['1'];
		}
		$drivers_arr = array();
		//$this->transporters_drivers_model->safa_transporters_id = $safa_transporters_id;
		$drivers = $this->transporters_drivers_model->get();
		$drivers_arr[""] = lang('global_select_from_menu');
		foreach ($drivers as $driver) {
			$drivers_arr[$driver->$key] = $driver->$value;
		}
		$data['safa_transporters_drivers'] = $drivers_arr;


		$structure = array('safa_transporters_buses_id', 'bus_no');
		$key = $structure['0'];
		if (isset($structure['1'])) {
			$value = $structure['1'];
		}
		$buses_arr = array();
		//$this->transporters_buses_model->safa_transporters_id = $safa_transporters_id;
		$buses = $this->transporters_buses_model->get();
		$buses_arr[""] = lang('global_select_from_menu');
		foreach ($buses as $bus) {
			$buses_arr[$bus->$key] = $bus->$value;
		}
		$data['safa_transporters_buses'] = $buses_arr;

		$data["safa_buses_brands"] = ddgen("safa_buses_brands", array("safa_buses_brands_id", name()), FALSE, FALSE, TRUE);

		//-----------------------------------------------------------------------------------------



		if (session('uo_id')) {
			$data["safa_ito"] = $this->get_uo_contracts_itos();
		} else if (session('ea_id')) {
			$data["safa_ito"] = $this->get_ea_contracts_itos();
		}

		if ($id) {

			$this->trip_internaltrip_model->safa_trip_internaltrip_id = $id;
			$item = $this->trip_internaltrip_model->get();
			$data['item'] = $item;

			//------------------------ Going Flights ----------------------------------------
			//$this->flight_availabilities_model->erp_path_type_id=1;
			$this->flight_availabilities_model->erp_flight_availability_id = $item->erp_flight_availability_id;
			$going_trip_flights_rows = $this->flight_availabilities_model->get_for_trip();
			$data['going_trip_flights_rows'] = $going_trip_flights_rows;
			$data['going_trip_flights_rows_count'] = count($going_trip_flights_rows);

			$arr_going_trip_flights = array();
			foreach ($going_trip_flights_rows as $going_trip_flights_row) {
				$arr_going_trip_flights[$going_trip_flights_row->erp_flight_availability_id] = $going_trip_flights_row->erp_flight_availability_id;
			}
			//session('going_trip_flights_session', $arr_going_trip_flights);
			$_SESSION['going_trip_flights_session'] = $arr_going_trip_flights;

			//-------------------------------------------------------------------------------

			/* --------- get the internal passages ----------------------------------------------------- */
			$this->internalpassages_model->safa_trip_internaltrip_id = $id;
			$this->internalpassages_model->order_by = array("safa_internalsegments.start_datetime", 'ASC');
			$data["internalpassages"] = $this->internalpassages_model->get();

			$this->internalpassages_model->safa_trip_internaltrip_id = $id;
			$this->internalpassages_model->order_by = array("safa_internalsegment_id", 'ASC');
			$data["internalpassages_hotels"] = $this->internalpassages_model->get_hotels();

			$this->internalpassages_model->safa_trip_internaltrip_id = $id;
			$this->internalpassages_model->order_by = array("safa_internalsegment_id", 'ASC');
			$data["internalpassages_tourismplaces"] = $this->internalpassages_model->get_tourismplaces();

			/* ----------------------------------------------------------------------------------------- */



			//---------- Details --------------------------
			//			$this->safa_package_execlusive_meals_model->safa_package_id = $id;
			//			$item_execlusive_meals_prices = $this->safa_package_execlusive_meals_model->get();
			//			$data['item_execlusive_meals_prices'] = $item_execlusive_meals_prices;
			//-----------------------------------------------


			$this->db->from('safa_trip_internaltrips_hotels');
			$this->db->where('safa_trip_internaltrip_id', $id);
			$data['safa_trip_internaltrips_hotels_rows'] = $this->db->get()->result();


			//-------------- Safa Group Passports -------------------
			$this->safa_group_passports_model->safa_trip_internaltrip_id = $id;
			$data['safa_group_passports'] = $this->safa_group_passports_model->get_for_table();
			//-------------------------------------------------------
			//--------------- Drivers And Buses -------------------------------------------------------
			//$this->internalsegments_drivers_and_buses_model->safa_internalsegments_id = $data["internalpassages"][0]->safa_internalsegment_id;
			//$item_drivers_and_buses = $this->internalsegments_drivers_and_buses_model->get();
			$item_drivers_and_buses = $this->internalsegments_drivers_and_buses_model->get_distinct_by_safa_trip_internaltrip($id);
			$data['item_drivers_and_buses'] = $item_drivers_and_buses;
			//-----------------------------------------------------------------------------------------
		}


		$this->load->library("form_validation");
		$this->form_validation->set_rules('datetime', 'lang:transport_request_date', 'trim|required');
		$this->form_validation->set_rules('safa_uo_contract_id', 'lang:transport_request_contract', 'trim|required');
		//$this->form_validation->set_rules('safa_trip_id', 'lang:transport_request_trip_id', 'trim|required');
		$this->form_validation->set_rules('safa_internaltripstatus_id', 'lang:transport_request_status', 'trim|required');

		if ($this->input->post('safa_internaltrip_type_id') == '1') {
			$this->form_validation->set_rules('safa_ito_id', 'lang:transport_request_opertator', 'trim|required');
		}

		$this->form_validation->set_rules('safa_transporter_id', 'lang:transport_request_transportername', 'trim');

		$this->form_validation->set_rules('operator_reference', 'lang:transport_request_res_code', 'trim');
		$this->form_validation->set_rules('text_input', '', 'trim');

		$this->form_validation->set_rules('erp_transportertype_id', 'lang:erp_transportertype_id', 'trim|required');

		$this->form_validation->set_rules('safa_internaltrip_type_id', 'lang:safa_internaltrip_type_id', 'trim|required');

		if (!$id) {
			if (session('uo_id')) {
				$this->form_validation->set_rules('serial', 'lang:serial', 'trim|required|unique_col_by_uo[safa_trip_internaltrips.serial]');
				if ($this->input->post('confirmation_number') != '0') {
					$this->form_validation->set_rules('confirmation_number', 'lang:confirmation_number', 'trim|required|unique_col_by_uo[safa_trip_internaltrips.confirmation_number]');
				}
			} else if (session('ea_id')) {
				if ($this->input->post('confirmation_number') != '0') {
					$this->form_validation->set_rules('confirmation_number', 'lang:confirmation_number', 'trim|required|unique_col_by_ea[safa_trip_internaltrips.confirmation_number]');
				}
			}
		}

		if ($this->input->post('text_input') && $_FILES['userfile']['name']) {
			$this->form_validation->set_rules('userfile', 'lang:userfile', 'trim|callback_fileupload');
		}


		if ($this->form_validation->run() == false) {
			$this->load->view("safa_trip_internaltrips/manage_design", $data);
		} else {

			if (isset($_FILES["transport_request_res_file"])) {
				if (count($_FILES["transport_request_res_file"]['tmp_name']) > 0) {
					//Get the temp file path
					$tmpFilePath = $_FILES["transport_request_res_file"]['tmp_name'];

					//Make sure we have a filepath
					if ($tmpFilePath != "") {
						//Setup our new file path
						$newFilePath = './static/temp/uo_files/' . $_FILES["transport_request_res_file"]['name'];

						//Upload the file into the temp dir
						//To solve arabic files names problem.
						$is_file_uploaded = move_uploaded_file($tmpFilePath, iconv('utf-8', 'windows-1256', $newFilePath));
						//$is_file_uploaded=move_uploaded_file($tmpFilePath, $newFilePath);



						$file_name = $_FILES["transport_request_res_file"]['name'];
						$this->trip_internaltrip_model->attachement = $file_name;
					}
				}
			}

			if ($this->input->post('safa_trip_id')) {
				$this->trip_internaltrip_model->safa_trip_id = $this->input->post('safa_trip_id');
			} else {
				// $this->trip_internaltrip_model->safa_trip_id = null;
			}

			if (session('uo_id')) {
				$this->trip_internaltrip_model->safa_uo_id = session('uo_id');
			} else if (session('ea_id')) {
				$this->trip_internaltrip_model->safa_ea_id = session('ea_id');
			}


			if ($this->input->post('safa_ito_id'))
			$this->trip_internaltrip_model->safa_ito_id = $this->input->post('safa_ito_id');
			if ($this->input->post('safa_transporter_id'))
			$this->trip_internaltrip_model->safa_transporter_id = $this->input->post('safa_transporter_id');

			$this->trip_internaltrip_model->safa_internaltripstatus_id = $this->input->post('safa_internaltripstatus_id');
			$this->trip_internaltrip_model->operator_reference = $this->input->post('operator_reference');
			//$this->trip_internaltrip_model->attachement = $file_name;
			$this->trip_internaltrip_model->datetime = $this->input->post('datetime');

			//$this->trip_internaltrip_model->erp_company_type_id = 3;
			//$this->trip_internaltrip_model->erp_company_id = $this->input->post('uo_ea_id');
			//$this->trip_internaltrip_model->trip_title = $this->input->post('trip_title');

			if (session('uo_id')) {
				$this->trip_internaltrip_model->trip_title = $this->input->post('safa_uo_code') . '-' . $this->input->post('serial');
				$this->trip_internaltrip_model->serial = $this->input->post('serial');
			} else if (session('ea_id')) {

				$this->contracts_model->safa_uo_contract_id = $this->input->post('safa_uo_contract_id');
				$contracts_row = $this->contracts_model->get();
				if (count($contracts_row) > 0) {
					$safa_uo_code = '';
					$safa_uo_serial = '';

					$this->safa_uos_model->safa_uo_id = $contracts_row->safa_uo_id;
					$safa_uo_row = $this->safa_uos_model->get();
					if (count($safa_uo_row) > 0) {
						$safa_uo_code = $safa_uo_row->code;
					}

					$safa_uo_row = $this->trip_internaltrip_model->get_max_serial_by_uo($contracts_row->safa_uo_id);
					if (count($safa_uo_row) > 0) {
						$safa_uo_serial = $safa_uo_row->max_serial + 1;
					}

					$this->trip_internaltrip_model->trip_title = $safa_uo_code . '-' . $safa_uo_serial;
					$this->trip_internaltrip_model->serial = $safa_uo_serial;
				}
			}

			$this->trip_internaltrip_model->trip_supervisors = $this->input->post('trip_supervisors');
			$this->trip_internaltrip_model->trip_supervisors_phone = $this->input->post('trip_supervisors_phone');
			$this->trip_internaltrip_model->safa_uo_contract_id = $this->input->post('safa_uo_contract_id');

			$this->trip_internaltrip_model->adult_seats = $this->input->post('adult_seats');
			$this->trip_internaltrip_model->child_seats = $this->input->post('child_seats');
			$this->trip_internaltrip_model->baby_seats = $this->input->post('baby_seats');

			$this->trip_internaltrip_model->confirmation_number = $this->input->post('confirmation_number');

			$this->trip_internaltrip_model->erp_transportertype_id = $this->input->post('erp_transportertype_id');

			$this->trip_internaltrip_model->safa_internaltrip_type_id = $this->input->post('safa_internaltrip_type_id');

			$this->trip_internaltrip_model->notes = $this->input->post('notes');


			$mode = 'add';
			$flight_availability_id = 0;
			if ($id) {
				$this->trip_internaltrip_model->safa_trip_internaltrip_id = $id;
				$safa_trip_internaltrip_id = $id;
				$this->trip_internaltrip_model->save();

				$this->save_flight_availability($item->erp_flight_availability_id);
				$flight_availability_id = $item->erp_flight_availability_id;


				$mode = 'edit';
			} else {
				$safa_trip_internaltrip_id = $this->trip_internaltrip_model->save();
				$flight_availability_id = $this->save_flight_availability();
			}

			if (isset($safa_trip_internaltrip_id)) {


				$flight_seats = (int) $this->input->post('adult_seats') + (int) $this->input->post('child_seats') + (int) $this->input->post('baby_seats');
				$internal_seats = (int) $this->input->post('adult_seats') + (int) $this->input->post('child_seats');

				$this->db->where('safa_trip_internaltrip_id', $safa_trip_internaltrip_id)
				->set('erp_flight_availability_id', $flight_availability_id)
				->update('safa_trip_internaltrips');

				$this->add_internalsegments($safa_trip_internaltrip_id, $mode);

				$this->save_rooms_count($safa_trip_internaltrip_id);

				$this->update_safa_group_passports($safa_trip_internaltrip_id);


				if ($this->input->post('safa_uo_contract_id')) {
					$this->send_notification($safa_trip_internaltrip_id, $mode);
				}
			}



			$this->db->trans_complete();

			if ($id) {
				$this->load->view('redirect_new_message', array('msg' => lang('global_updated_message'),
                    'url' => site_url('safa_trip_internaltrips'),
                    'id' => $this->uri->segment("4"),
                    'model_title' => lang('safa_trip_internaltrips'), 'action' => lang('edit_safa_trip_internaltrips')));
			} else {
				$this->load->view('redirect_new_message', array('msg' => lang('global_added_message'),
                    'url' => site_url('safa_trip_internaltrips'),
                    'id' => $this->uri->segment("4"),
                    'model_title' => lang('safa_trip_internaltrips'), 'action' => lang('add_safa_trip_internaltrips')));
			}
		}
	}

	function save_flight_availability($flight_availability_id = 0) {
		if ($flight_availability_id == 0) {
			$this->db->set('pnr', post('name'));
			// $this->db->set('safa_ea_id', session('ea_id'));
			$this->db->set('by_trip_internaltrip', 1);
			$this->db->insert('erp_flight_availabilities');
			$flight_availability_id = $this->db->insert_id();
		}

		$arr = array(
            'erp_flight_availabilities_detail_id',
            'safa_transporter_code',
            'flight_number',
            'erp_flight_class_id',
            'flight_date',
            'flight_arrival_date',
            'erp_port_id_from',
            'erp_port_id_to',
            'erp_port_hall_id_from',
            'erp_port_hall_id_to',
            'airports',
            'erp_flight_status_id',
            'seats_count',
            'safa_externaltriptype_id',
            'erp_path_type_id'
            );

            foreach ($arr as $item)
            $$item = post($item);

            if ($safa_transporter_code) {
            	foreach ($safa_transporter_code as $key => $value) {


            		$this->db->set('erp_flight_availability_id', $flight_availability_id);
            		$this->db->set('airline_code', $safa_transporter_code[$key]);


            		$current_flight_datetime = $flight_date[$key];
            		$current_flight_datetime_arr = explode(' ', $current_flight_datetime);
            		$current_flight_date = $current_flight_datetime_arr[0];
            		$current_flight_time = '';
            		if (isset($current_flight_datetime_arr[1])) {
            			$current_flight_time = $current_flight_datetime_arr[1];
            		}

            		$current_arrival_datetime = $flight_arrival_date[$key];
            		$current_arrival_datetime_arr = explode(' ', $current_arrival_datetime);
            		$current_arrival_date = $current_arrival_datetime_arr[0];
            		$current_arrival_time = '';
            		if (isset($current_arrival_datetime_arr[1])) {
            			$current_arrival_time = $current_arrival_datetime_arr[1];
            		}

            		$this->db->set('flight_date', $current_flight_date);
            		$this->db->set('arrival_date', $current_arrival_date);
            		$this->db->set('departure_time', $current_flight_time);
            		$this->db->set('arrival_time', $current_arrival_time);

            		$this->db->set('flight_number', $flight_number[$key]);
            		$this->db->set('erp_flight_class_id', $erp_flight_class_id[$key]);

            		if (isset($erp_port_id_from[$key]))
            		$this->db->set('erp_port_id_from', $erp_port_id_from[$key]);
            		if (isset($erp_port_id_to[$key]))
            		$this->db->set('erp_port_id_to', $erp_port_id_to[$key]);

            		if (isset($erp_port_hall_id_from[$key]))
            		$this->db->set('erp_port_hall_id_from', $erp_port_hall_id_from[$key]);
            		if (isset($erp_port_hall_id_to[$key]))
            		$this->db->set('erp_port_hall_id_to', $erp_port_hall_id_to[$key]);

            		$this->db->set('erp_path_type_id', $erp_path_type_id[$key]);
            		$this->db->set('safa_externaltriptype_id', $safa_externaltriptype_id[$key]);

            		$this->db->set('erp_flight_status_id', $erp_flight_status_id[$key]);
            		$this->db->set('seats_count', $seats_count[$key]);


            		if ($erp_flight_availabilities_detail_id[$key] == 0) {
            			$this->db->insert('erp_flight_availabilities_detail');
            		} else {
            			$this->db->where('erp_flight_availabilities_detail.erp_flight_availabilities_detail_id', $erp_flight_availabilities_detail_id[$key])->update('erp_flight_availabilities_detail');
            		}
            	}
            }

            $flights_remove = $this->input->post('flights_remove');
            if (ensure($flights_remove)) {
            	foreach ($flights_remove as $erp_flight_availabilities_detail_id) {
            		$this->db->where('erp_flight_availabilities_detail.erp_flight_availabilities_detail_id', $erp_flight_availabilities_detail_id)->delete('erp_flight_availabilities_detail');
            	}
            }

            return $flight_availability_id;
	}

	function save_cruise_availability($cruise_availability_id = 0) {
		if ($cruise_availability_id == 0) {
			$this->erp_cruise_availabilities_model->by_trip_internaltrip = 1;
			$cruise_availability_id = $this->erp_cruise_availabilities_model->save();
		}

		$arr = array(
            'erp_cruise_availabilities_detail_id',
            'safa_transporter_code',
            'cruise_number',
            'erp_cruise_class_id',
            'cruise_date',
            'cruise_arrival_date',
            'erp_port_id_from',
            'erp_port_id_to',
            'erp_cruise_status_id',
            'seats_count',
            'safa_externaltriptype_id',
            'erp_path_type_id'
            );

            foreach ($arr as $item)
            $$item = post($item);

            if ($safa_transporter_code) {
            	foreach ($safa_transporter_code as $key => $value) {

            		if ($erp_cruise_availabilities_detail_id[$key] != 0) {
            			$this->erp_cruise_availabilities_detail_model->erp_cruise_availabilities_detail_id = $erp_cruise_availabilities_detail_id[$key];
            		} else {
            			$this->erp_cruise_availabilities_detail_model->erp_cruise_availabilities_detail_id = false;
            		}

            		$this->erp_cruise_availabilities_detail_model->erp_cruise_availability_id = $cruise_availability_id;
            		$this->erp_cruise_availabilities_detail_model->safa_transporter_code = $safa_transporter_code[$key];


            		$current_cruise_datetime = $cruise_date[$key];
            		$current_cruise_datetime_arr = explode(' ', $current_cruise_datetime);
            		$current_cruise_date = $current_cruise_datetime_arr[0];
            		$current_cruise_time = '';
            		if (isset($current_cruise_datetime_arr[1])) {
            			$current_cruise_time = $current_cruise_datetime_arr[1];
            		}

            		$current_arrival_datetime = $cruise_arrival_date[$key];
            		$current_arrival_datetime_arr = explode(' ', $current_arrival_datetime);
            		$current_arrival_date = $current_arrival_datetime_arr[0];
            		$current_arrival_time = '';
            		if (isset($current_arrival_datetime_arr[1])) {
            			$current_arrival_time = $current_arrival_datetime_arr[1];
            		}


            		$this->erp_cruise_availabilities_detail_model->cruise_date = $current_cruise_date;
            		$this->erp_cruise_availabilities_detail_model->arrival_date = $current_arrival_date;
            		$this->erp_cruise_availabilities_detail_model->departure_time = $current_cruise_time;
            		$this->erp_cruise_availabilities_detail_model->arrival_time = $current_arrival_time;

            		$this->erp_cruise_availabilities_detail_model->cruise_number = $cruise_number[$key];
            		$this->erp_cruise_availabilities_detail_model->erp_cruise_class_id = $erp_cruise_class_id[$key];

            		if (isset($erp_port_id_from[$key])) {
            			$this->erp_cruise_availabilities_detail_model->erp_port_id_from = $erp_port_id_from[$key];
            		}
            		if (isset($erp_port_id_to[$key])) {
            			$this->erp_cruise_availabilities_detail_model->erp_port_id_to = $erp_port_id_to[$key];
            		}
            		$this->erp_cruise_availabilities_detail_model->erp_path_type_id = $erp_path_type_id[$key];
            		$this->erp_cruise_availabilities_detail_model->safa_externaltriptype_id = $safa_externaltriptype_id[$key];
            		$this->erp_cruise_availabilities_detail_model->erp_cruise_status_id = $erp_cruise_status_id[$key];
            		$this->erp_cruise_availabilities_detail_model->seats_count = $seats_count[$key];


            		$this->erp_cruise_availabilities_detail_model->save();
            	}
            }


            //------------------ Delete removed rows ---------------------------------------------------------------
            $this->erp_cruise_availabilities_detail_model->safa_transporter_code = false;
            $this->erp_cruise_availabilities_detail_model->cruise_date = false;
            $this->erp_cruise_availabilities_detail_model->arrival_date = false;
            $this->erp_cruise_availabilities_detail_model->departure_time = false;
            $this->erp_cruise_availabilities_detail_model->arrival_time = false;
            $this->erp_cruise_availabilities_detail_model->cruise_number = false;
            $this->erp_cruise_availabilities_detail_model->erp_cruise_class_id = false;
            $this->erp_cruise_availabilities_detail_model->erp_port_id_from = false;
            $this->erp_cruise_availabilities_detail_model->erp_port_id_to = false;
            $this->erp_cruise_availabilities_detail_model->erp_path_type_id = false;
            $this->erp_cruise_availabilities_detail_model->safa_externaltriptype_id = false;
            $this->erp_cruise_availabilities_detail_model->erp_cruise_status_id = false;
            $this->erp_cruise_availabilities_detail_model->seats_count = false;

            $cruises_remove = $this->input->post('cruises_remove');
            if (ensure($cruises_remove)) {
            	foreach ($cruises_remove as $erp_cruise_availabilities_detail_id) {
            		$this->erp_cruise_availabilities_detail_model->erp_cruise_availabilities_detail_id = $erp_cruise_availabilities_detail_id;
            		$this->erp_cruise_availabilities_detail_model->delete();
            	}
            }
            //-------------------------------------------------------------------------------------------------------


            return $cruise_availability_id;
	}

	function add_internalsegments_($safa_trip_internaltrip_id, $internal_seats)
	{
		$safa_externaltriptype_id = post('safa_externaltriptype_id');
		$erp_port_id_to = post('erp_port_id_to');
		$erp_port_id_from = post('erp_port_id_from');
		$erp_path_type_id = post('erp_path_type_id');
		$flight_arrival_date = post('flight_arrival_date');
		$flight_date = post('flight_date');
		$seatcont = post('seats_count');

		$passengers = false;
		$portto = null;
		$portfrom = null;

		$backdate = '';

		$flight_arrival = '';

		if ($erp_port_id_to) {
			foreach ($erp_port_id_to as $key => $port_id) {
				if ($safa_externaltriptype_id[$key] == 1 && $erp_path_type_id[$key] == 2) {
					$portto = $port_id;
					$passengers = $seatcont[$key];
					$flight_arrival = $flight_arrival_date[$key];
				}
				if ($safa_externaltriptype_id[$key] == 2 && $erp_path_type_id[$key] == 1) {
					$portfrom = $erp_port_id_from[$key];
					$backdate = $flight_date[$key];
				}
			}
		}


		$erp_city_id = post('erp_city_id');
		$erp_hotel_id = post('erp_hotel_id');
		$arrival_date = post('arrival_date');
		$nights = post('nights');
		$exit_date = post('exit_date');
		$oldhotel = false;

		$movetime = 0;

		$hotel_safa_internalsegment_ids = post('hotel_safa_internalsegment_id');

		if ($erp_hotel_id == '') {
			$erp_hotel_id = array();
		}

		if (count($erp_hotel_id) > 0) {
			foreach ($erp_hotel_id as $hkey => $hotelid) {
				if (!$oldhotel) {
					$movetime = 0;
					switch ($portto) {
						case 6053:
							if (isset($erp_city_id[$key])) {
								if ($erp_city_id[$key] == 1)
								$movetime = 3;
								elseif ($erp_city_id[$key] == 2)
								$movetime = 7;
							}
							break;
						case 6056:
							if (isset($erp_city_id[$key])) {
								if ($erp_city_id[$key] == 1)
								$movetime = 7;
								elseif ($erp_city_id[$key] == 2)
								$movetime = 2;
							}
							break;
						case 6070:
							if (isset($erp_city_id[$key])) {
								if ($erp_city_id[$key] == 1)
								$movetime = 6;
								elseif ($erp_city_id[$key] == 2)
								$movetime = 6;
							}
							break;
					}
					$arrivestamp = strtotime($flight_arrival) + (3600 * $movetime);
					$arrivaldate = date('Y-m-d h:i', $arrivestamp);

					//By Gouda
					//------------------------------------------------------------------------------------
					if ($this->input->post('erp_transportertype_id') == 1) {
						$flight_arrival = $this->input->post('land_going_datetime');
						$movetime = 6;
						$arrivestamp = strtotime($flight_arrival) + (3600 * $movetime);
						$arrivaldate = date('Y-m-d H:i', $arrivestamp);

						$portto = $this->input->post('land_going_erp_port_id');
					} else if ($this->input->post('erp_transportertype_id') == 3) {
						$movetime = 6;

						$flight_arrival = $this->input->post('sea_going_datetime');

						$arrivestamp = strtotime($flight_arrival) + (3600 * $movetime);
						$arrivaldate = date('Y-m-d H:i', $arrivestamp);

						$portto = $this->input->post('sea_going_erp_port_id');
					}
					//------------------------------------------------------------------------------------
					//By Gouda.
					//------------------------------------------------------------------------------------
					$current_safa_internalsegment_id = $hotel_safa_internalsegment_ids[$hkey];

					if ($current_safa_internalsegment_id == 0) {

						$this->internalpassages_model->safa_trip_internaltrip_id = $safa_trip_internaltrip_id;
						$this->internalpassages_model->safa_internalsegmenttype_id = 1;
						$this->internalpassages_model->delete();

						$this->db->insert('safa_internalsegments', array(
                            'safa_internalsegmenttype_id' => 1,
                            'safa_trip_internaltrip_id' => $safa_trip_internaltrip_id,
                            'safa_internalsegmentestatus_id' => 1,
                            'start_datetime' => $flight_arrival,
                            'end_datetime' => $arrivaldate,
                            'erp_port_id' => $portto,
                            'erp_start_hotel_id' => NULL,
                            'erp_end_hotel_id' => $hotelid,
                            'safa_tourism_place_id' => NULL,
                            'safa_uo_user_id' => NULL,
                            'seats_count' => $internal_seats
						));

						$current_safa_internalsegment_id = $this->db->insert_id();
					} else {

						$this->db->where('safa_internalsegment_id', $current_safa_internalsegment_id);
						$this->db->update('safa_internalsegments', array(
                            'safa_internalsegmenttype_id' => 1,
                            'safa_trip_internaltrip_id' => $safa_trip_internaltrip_id,
                            'safa_internalsegmentestatus_id' => 1,
                            'start_datetime' => $flight_arrival,
                            'end_datetime' => $arrivaldate,
                            'erp_port_id' => $portto,
                            'erp_start_hotel_id' => NULL,
                            'erp_end_hotel_id' => $hotelid,
                            'safa_tourism_place_id' => NULL,
                            'safa_uo_user_id' => NULL,
                            'seats_count' => $internal_seats
						));
					}
					//------------------------------------------------------------------------------------
				} else {

					//By Gouda.
					//------------------------------------------------------------------------------------
					$current_safa_internalsegment_id = $hotel_safa_internalsegment_ids[$hkey];

					if ($current_safa_internalsegment_id == 0) {

						$startdate = $arrival_date[$hkey] . " " . $this->config->item('start_moving');
						$arrivestamp = strtotime($startdate) + (3600 * 7);
						$arrivaldate = date('Y-m-d h:i', $arrivestamp);
						$this->db->insert('safa_internalsegments', array(
                            'safa_internalsegmenttype_id' => 4,
                            'safa_trip_internaltrip_id' => $safa_trip_internaltrip_id,
                            'safa_internalsegmentestatus_id' => 1,
                            'start_datetime' => $startdate,
                            'end_datetime' => $arrivaldate,
                            'erp_port_id' => NULL,
                            'erp_start_hotel_id' => $oldhotel,
                            'erp_end_hotel_id' => $hotelid,
                            'safa_tourism_place_id' => NULL,
                            'safa_uo_user_id' => NULL,
                            'seats_count' => $internal_seats
						));

						$current_safa_internalsegment_id = $this->db->insert_id();
					} else {

						$startdate = $arrival_date[$hkey] . " " . $this->config->item('start_moving');
						$arrivestamp = strtotime($startdate) + (3600 * 7);
						$arrivaldate = date('Y-m-d h:i', $arrivestamp);

						$this->db->where('safa_internalsegment_id', $current_safa_internalsegment_id);
						$this->db->update('safa_internalsegments', array(
                            'safa_internalsegmenttype_id' => 4,
                            'safa_trip_internaltrip_id' => $safa_trip_internaltrip_id,
                            'safa_internalsegmentestatus_id' => 1,
                            'start_datetime' => $startdate,
                            'end_datetime' => $arrivaldate,
                            'erp_port_id' => NULL,
                            'erp_start_hotel_id' => $oldhotel,
                            'erp_end_hotel_id' => $hotelid,
                            'safa_tourism_place_id' => NULL,
                            'safa_uo_user_id' => NULL,
                            'seats_count' => $internal_seats
						));
					}
					//------------------------------------------------------------------------------------
				}

				$oldhotel = $hotelid;



				// By Gouda.
				//------------------------------------------------------------------------------------
				$hotels_rooms_count = $this->input->post('hotels_rooms_count');
				if (ensure($hotels_rooms_count[$hkey])) {
					foreach ($hotels_rooms_count[$hkey] as $roomsize => $rooms_count) {
						$this->db->set('safa_internalsegment_id', $current_safa_internalsegment_id);
						$this->db->set('erp_hotel_id', end($erp_hotel_id));
						$this->db->set('erp_hotelroomsize_id', $roomsize);
						$this->db->set('rooms_count', $rooms_count);

						if (strpos($hkey, 'N') === false) {
							$this->db->where('safa_internalsegment_id', $current_safa_internalsegment_id);
							$this->db->where('erp_hotelroomsize_id', $roomsize);
							if ($this->db->from('safa_trip_internaltrips_hotels')->count_all_results()) {
								$this->db->where('safa_internalsegment_id', $current_safa_internalsegment_id);
								$this->db->where('erp_hotelroomsize_id', $roomsize);
								$this->db->set('rooms_count', $rooms_count);
								$this->db->update('safa_trip_internaltrips_hotels');
							} else {
								$this->db->set('safa_internalsegment_id', $current_safa_internalsegment_id);
								$this->db->set('erp_hotel_id', end($erp_hotel_id));
								$this->db->set('erp_hotelroomsize_id', $roomsize);
								$this->db->set('rooms_count', $rooms_count);
								$this->db->insert('safa_trip_internaltrips_hotels');
							}
						} else {
							$this->db->set('safa_internalsegment_id', $current_safa_internalsegment_id);
							$this->db->set('erp_hotel_id', end($erp_hotel_id));
							$this->db->set('erp_hotelroomsize_id', $roomsize);
							$this->db->set('rooms_count', $rooms_count);
							$this->db->insert('safa_trip_internaltrips_hotels');
						}
					}
				}
				//-----------------------------------------------------------------------------------
			}



			$hotels_remove = $this->input->post('hotels_remove');
			if (ensure($hotels_remove)) {
				foreach ($hotels_remove as $safa_internalsegment_id) {
					$this->db->where('safa_internalsegments.safa_internalsegment_id', $safa_internalsegment_id)->delete('safa_internalsegments');
				}
			}



			switch ($portfrom) {
				case 6053:
					if (end($erp_city_id) == 1)
					$movetime = 6;
					elseif (end($erp_city_id) == 2)
					$movetime = 12;

					break;
				case 6056:
					if (end($erp_city_id) == 1)
					$movetime = 7;
					elseif (end($erp_city_id) == 2)
					$movetime = 4;
					break;
				case 6070:
					if (end($erp_city_id) == 1)
					$movetime = 6;
					elseif (end($erp_city_id) == 2)
					$movetime = 6;
					break;
			}

			$arrivestamp = strtotime($backdate) - (3600 * $movetime);
			$arrivaldate = date('Y-m-d H:i', $arrivestamp);

			$end_datetime = strtotime($backdate) - (3600 * ($movetime - 3));

			if (count($erp_hotel_id) > 0) {


				//By Gouda
				//------------------------------------------------------------------------------------
				if ($this->input->post('erp_transportertype_id') == 1) {
					$arrivaldate = $this->input->post('land_return_datetime');
					$end_datetime = strtotime($arrivaldate) + (3600 * (3));

					$portfrom = $this->input->post('land_return_erp_port_id');
				} else if ($this->input->post('erp_transportertype_id') == 3) {
					$arrivaldate = $this->input->post('sea_return_datetime');
					$end_datetime = strtotime($arrivaldate) + (3600 * (3));

					$portfrom = $this->input->post('sea_return_erp_port_id');
				}
				//------------------------------------------------------------------------------------
				//By Gouda.
				//------------------------------------------------------------------------------------
				$this->internalpassages_model->safa_trip_internaltrip_id = $safa_trip_internaltrip_id;
				$this->internalpassages_model->safa_internalsegmenttype_id = 2;
				$check_internalpassages_row = $this->internalpassages_model->get();

				$current_safa_internalsegment_id = null;

				if (count($check_internalpassages_row) == 0) {

					$this->db->insert('safa_internalsegments', array(
                        'safa_internalsegmenttype_id' => 2,
                        'safa_trip_internaltrip_id' => $safa_trip_internaltrip_id,
                        'safa_internalsegmentestatus_id' => 1,
                        'start_datetime' => $arrivaldate,
                        'end_datetime' => $end_datetime,
                        'erp_port_id' => $portfrom,
                        'erp_start_hotel_id' => end($erp_hotel_id),
                        'erp_end_hotel_id' => NULL,
                        'safa_tourism_place_id' => NULL,
                        'safa_uo_user_id' => NULL,
                        'seats_count' => $internal_seats
					));
				} else {
					$this->db->where('safa_internalsegment_id', $check_internalpassages_row[0]->safa_internalsegment_id);
					$this->db->update('safa_internalsegments', array(
                        'safa_internalsegmenttype_id' => 2,
                        'safa_trip_internaltrip_id' => $safa_trip_internaltrip_id,
                        'safa_internalsegmentestatus_id' => 1,
                        'start_datetime' => $arrivaldate,
                        'end_datetime' => $end_datetime,
                        'erp_port_id' => $portfrom,
                        'erp_start_hotel_id' => end($erp_hotel_id),
                        'erp_end_hotel_id' => NULL,
                        'safa_tourism_place_id' => NULL,
                        'safa_uo_user_id' => NULL,
                        'seats_count' => $internal_seats
					));

					$current_safa_internalsegment_id = $check_internalpassages_row[0]->safa_internalsegment_id;
				}
				//------------------------------------------------------------------------------------
			}

			$safa_tourismplace_id = post('safa_tourismplace_id');
			$tourism_date = post('tourism_date');

			$tourism_safa_internalsegment_ids = post('tourism_safa_internalsegment_id');

			if ($safa_tourismplace_id) {
				if (count($safa_tourismplace_id) > 0) {
					foreach ($safa_tourismplace_id as $tkey => $tourism) {
						$tourismdata = $this->db->where('safa_tourismplace_id', $tourism)->get('safa_tourismplaces')->row();
						$hotelkey = array_search($tourismdata->erp_city_id, $erp_city_id);
						$movestamp = strtotime($tourism_date[$tkey]) + (3600 * 2);
						$movedate = date('Y-m-d H:i', $movestamp);

						$current_safa_internalsegment_id = $tourism_safa_internalsegment_ids[$tkey];

						$current_erp_start_hotel_id = null;
						if (isset($erp_hotel_id[$hotelkey])) {
							$current_erp_start_hotel_id = $erp_hotel_id[$hotelkey];
						}

						if ($current_safa_internalsegment_id == 0) {
							$this->db->insert('safa_internalsegments', array(
                                'safa_internalsegmenttype_id' => 3,
                                'safa_trip_internaltrip_id' => $safa_trip_internaltrip_id,
                                'safa_internalsegmentestatus_id' => 1,
                                'start_datetime' => $tourism_date[$tkey],
                                'end_datetime' => $movedate,
                                'erp_port_id' => NULL,
                                'erp_start_hotel_id' => $current_erp_start_hotel_id,
                                'erp_end_hotel_id' => NULL,
                                'safa_tourism_place_id' => $tourism,
                                'safa_uo_user_id' => NULL,
                                'seats_count' => $internal_seats
							));
						} else {

							$this->db->where('safa_internalsegment_id', $current_safa_internalsegment_id);
							$this->db->update('safa_internalsegments', array(
                                'safa_internalsegmenttype_id' => 3,
                                'safa_trip_internaltrip_id' => $safa_trip_internaltrip_id,
                                'safa_internalsegmentestatus_id' => 1,
                                'start_datetime' => $tourism_date[$tkey],
                                'end_datetime' => $movedate,
                                'erp_port_id' => NULL,
                                'erp_start_hotel_id' => $current_erp_start_hotel_id,
                                'erp_end_hotel_id' => NULL,
                                'safa_tourism_place_id' => $tourism,
                                'safa_uo_user_id' => NULL,
                                'seats_count' => $internal_seats
							));
						}
					}


					$tourismplaces_remove = $this->input->post('tourismplaces_remove');
					if (ensure($tourismplaces_remove)) {
						foreach ($tourismplaces_remove as $safa_internalsegment_id) {
							$this->db->where('safa_internalsegments.safa_internalsegment_id', $safa_internalsegment_id)->delete('safa_internalsegments');
						}
					}
				}
			}
		}
	}

	function add_internalsegments_for_edit_mode($id)
	{
		// Save internalpassages
		$safa_internalsegmenttype_id = $this->input->post('internalpassage_type');
		$start_datetime = $this->input->post('internalpassage_startdatatime');
		$end_datetime = $this->input->post('internalpassage_enddatatime');
		//$seats_count = $this->input->post('seatscount');
		$notes = $this->input->post('internalpassage_notes');
		$safa_internalsegmentestatus_id = 1;


		$erp_end_hotel_id = $this->input->post('internalpassage_hotel_end');
		$erp_start_hotel_id = $this->input->post('internalpassage_hotel_start');

		$erp_port_start_id = $this->input->post('internalpassage_port_start');
		$erp_port_end_id = $this->input->post('internalpassage_port_end');

		//$safa_tourism_place_start_id = $this->input->post('internalpassages_torismplace_start');
		$safa_tourism_place_end_id = $this->input->post('internalpassage_torismplace_end');


		if (ensure($safa_internalsegmenttype_id))
		foreach ($safa_internalsegmenttype_id as $key => $value) {

			$this->internalpassages_model->safa_trip_internaltrip_id = $id;
			$safa_internalsegment_id = $this->internalpassages_model->safa_internalsegment_id = $key;
			$current_internalpassage_row_count = $this->internalpassages_model->get(true);


			// Save internalpassages
			$this->internalpassages_model->safa_internalsegmenttype_id = $safa_internalsegmenttype_id[$key];
			$this->internalpassages_model->start_datetime = $start_datetime[$key];
			$this->internalpassages_model->end_datetime = $end_datetime[$key];
			//$this->internalpassages_model->seats_count = $seats_count[$key];
			$this->internalpassages_model->notes = $notes[$key];

			//$this->internalpassages_model->safa_internalsegmentestatus_id = 1;



			if ($safa_internalsegmenttype_id[$key] == 1) {
				$this->internalpassages_model->erp_port_id = $erp_port_start_id[$key];
				$this->internalpassages_model->erp_end_hotel_id = $erp_end_hotel_id[$key];
			} else if ($safa_internalsegmenttype_id[$key] == 2) {
				$this->internalpassages_model->erp_start_hotel_id = $erp_start_hotel_id[$key];
				$this->internalpassages_model->erp_port_id = $erp_port_end_id[$key];
			} else if ($safa_internalsegmenttype_id[$key] == 3) {
				$this->internalpassages_model->erp_start_hotel_id = $erp_start_hotel_id[$key];
				$this->internalpassages_model->safa_tourism_place_id = $safa_tourism_place_end_id[$key];
			} else if ($safa_internalsegmenttype_id[$key] == 4) {
				$this->internalpassages_model->erp_start_hotel_id = $erp_start_hotel_id[$key];
				$this->internalpassages_model->erp_end_hotel_id = $erp_end_hotel_id[$key];
			}


			if ($current_internalpassage_row_count == 0) {
				$this->internalpassages_model->safa_internalsegment_id = false;
				$safa_internalsegment_id = $this->internalpassages_model->save();
			} else {
				$this->internalpassages_model->save();
			}


			if($safa_internalsegmenttype_id[$key]==1 || $current_internalpassage_row_count == 0) {
				//----------------- Save Segment Drivers and Buses------------------------------------------
				$this->internalsegments_drivers_and_buses_model->safa_internalsegments_id = $safa_internalsegment_id;
				$arriving_internalsegments_drivers_and_buses_rows = $this->internalsegments_drivers_and_buses_model->get();
				$this->internalsegments_drivers_and_buses_model->delete();
					

				$safa_buses_brands = $this->input->post('safa_buses_brands');
				$safa_buses_models = $this->input->post('safa_buses_models');
				$bus_count = $this->input->post('bus_count');
				$seats_count = $this->input->post('seats_count');

				if (ensure($safa_buses_brands))
				foreach ($safa_buses_brands as $key => $value) {


					$current_bus_count = $bus_count[$key];

					$current_seats_count = floor($seats_count[$key] / $current_bus_count);
					$reset_current_seats_count = $seats_count[$key] % $current_bus_count;

					//$buses_arr = array();
					$this->transporters_buses_model->auto_generate = 1;
					$this->transporters_buses_model->safa_buses_brands_id = $safa_buses_brands[$key];
					$this->transporters_buses_model->safa_buses_models_id = $safa_buses_models[$key];
					$buses_rows = $this->transporters_buses_model->get();
					//					foreach ($buses_rows as $buses_row) {
					//						$buses_arr[] = $buses_row->safa_transporters_buses_id;
					//					}

					for ($i = 0; $i < $current_bus_count; $i++) {

						$this->internalsegments_drivers_and_buses_model->safa_transporters_drivers_id = null;
						//						if(isset($buses_arr[$i])) {
						//							$this->internalsegments_drivers_and_buses_model->safa_transporters_buses_id = $buses_arr[$i];
						//						} else if(isset($buses_arr[0])) {
						//							$this->internalsegments_drivers_and_buses_model->safa_transporters_buses_id = $buses_arr[0];
						//						}

						if (isset($buses_rows[0])) {
							$this->internalsegments_drivers_and_buses_model->safa_transporters_buses_id = $buses_rows[0]->safa_transporters_buses_id;
						}
						if ($i == 0) {
							$this->internalsegments_drivers_and_buses_model->seats_count = $current_seats_count + $reset_current_seats_count;
						} else {
							$this->internalsegments_drivers_and_buses_model->seats_count = $current_seats_count;
						}

						$this->internalsegments_drivers_and_buses_model->save();
							
						$this->internalsegments_drivers_and_buses_model->safa_transporters_drivers_id = false;
						$this->internalsegments_drivers_and_buses_model->safa_transporters_buses_id = false;
						$this->internalsegments_drivers_and_buses_model->seats_count = false;
					}
				}
			} else if(isset($arriving_internalsegments_drivers_and_buses_rows)) {

				$are_current_internalsegments_drivers_and_buses_like_arriving_internalsegments=false;

				$this->internalsegments_drivers_and_buses_model->safa_internalsegments_id = $safa_internalsegment_id;
				$current_internalsegments_drivers_and_buses_rows = $this->internalsegments_drivers_and_buses_model->get();
				if(count($current_internalsegments_drivers_and_buses_rows)==count($arriving_internalsegments_drivers_and_buses_rows)) {

					$current_internalsegments_drivers_and_buses_rows_counter=0;
					foreach($current_internalsegments_drivers_and_buses_rows as $current_internalsegments_drivers_and_buses_row) {

						$current_internalsegments_drivers_and_buses_row_array = array(
						'safa_transporters_buses_id'=>$current_internalsegments_drivers_and_buses_row->safa_buses_models_id, 
						'seats_count'=>$current_internalsegments_drivers_and_buses_row->seats_count
						);

						$arriving_internalsegments_drivers_and_buses_row_array = array(
						'safa_transporters_buses_id'=>$arriving_internalsegments_drivers_and_buses_rows[$current_internalsegments_drivers_and_buses_rows_counter]->safa_buses_models_id, 
						'seats_count'=>$arriving_internalsegments_drivers_and_buses_rows[$current_internalsegments_drivers_and_buses_rows_counter]->seats_count
						);

						if($current_internalsegments_drivers_and_buses_row_array==$arriving_internalsegments_drivers_and_buses_row_array) {
							$are_current_internalsegments_drivers_and_buses_like_arriving_internalsegments=true;
						} else {
							$are_current_internalsegments_drivers_and_buses_like_arriving_internalsegments=false;
							break;
						}

						$current_internalsegments_drivers_and_buses_rows_counter++;
					}
				}


				//------------------ If Segments Buses Are Equaled -----------------------------------------
				if($are_current_internalsegments_drivers_and_buses_like_arriving_internalsegments || count($arriving_internalsegments_drivers_and_buses_rows)==0) {

					//----------------- Save Segment Drivers and Buses------------------------------------------
					$this->internalsegments_drivers_and_buses_model->safa_internalsegments_id = $safa_internalsegment_id;
					$arriving_internalsegments_drivers_and_buses_rows = $this->internalsegments_drivers_and_buses_model->get();
					$this->internalsegments_drivers_and_buses_model->delete();


					$safa_buses_brands = $this->input->post('safa_buses_brands');
					$safa_buses_models = $this->input->post('safa_buses_models');
					$bus_count = $this->input->post('bus_count');
					$seats_count = $this->input->post('seats_count');

					if (ensure($safa_buses_brands))
					foreach ($safa_buses_brands as $key => $value) {


						$current_bus_count = $bus_count[$key];

						$current_seats_count = floor($seats_count[$key] / $current_bus_count);
						$reset_current_seats_count = $seats_count[$key] % $current_bus_count;

						//$buses_arr = array();
						$this->transporters_buses_model->auto_generate = 1;
						$this->transporters_buses_model->safa_buses_brands_id = $safa_buses_brands[$key];
						$this->transporters_buses_model->safa_buses_models_id = $safa_buses_models[$key];
						$buses_rows = $this->transporters_buses_model->get();
						//					foreach ($buses_rows as $buses_row) {
						//						$buses_arr[] = $buses_row->safa_transporters_buses_id;
						//					}

						for ($i = 0; $i < $current_bus_count; $i++) {

							$this->internalsegments_drivers_and_buses_model->safa_transporters_drivers_id = null;
							//						if(isset($buses_arr[$i])) {
							//							$this->internalsegments_drivers_and_buses_model->safa_transporters_buses_id = $buses_arr[$i];
							//						} else if(isset($buses_arr[0])) {
							//							$this->internalsegments_drivers_and_buses_model->safa_transporters_buses_id = $buses_arr[0];
							//						}

							if (isset($buses_rows[0])) {
								$this->internalsegments_drivers_and_buses_model->safa_transporters_buses_id = $buses_rows[0]->safa_transporters_buses_id;
							}
							if ($i == 0) {
								$this->internalsegments_drivers_and_buses_model->seats_count = $current_seats_count + $reset_current_seats_count;
							} else {
								$this->internalsegments_drivers_and_buses_model->seats_count = $current_seats_count;
							}

							$this->internalsegments_drivers_and_buses_model->save();

							$this->internalsegments_drivers_and_buses_model->safa_transporters_drivers_id = false;
							$this->internalsegments_drivers_and_buses_model->safa_transporters_buses_id = false;
							$this->internalsegments_drivers_and_buses_model->seats_count = false;
						}
					}

				}
				//------------------ //////// End If Segments Buses Are Equaled ////////-----------------------------------------


			}
			//-----------------------------------------------------------------------------------------------



			// By Gouda
			$this->internalpassages_model->safa_trip_internaltrip_id = false;
			$this->internalpassages_model->safa_internalsegmenttype_id = false;
			$this->internalpassages_model->start_datetime = false;
			$this->internalpassages_model->end_datetime = false;
			$this->internalpassages_model->seats_count = false;
			$this->internalpassages_model->notes = false;

			$this->internalpassages_model->erp_port_id = false;
			$this->internalpassages_model->erp_start_hotel_id = false;
			$this->internalpassages_model->erp_end_hotel_id = false;
			$this->internalpassages_model->safa_tourism_place_id = false;
		}
		// --- Delete Deleted rooms rows from Database --------------

		$internalpassages_remove = $this->input->post('passages_remove');
		if (ensure($internalpassages_remove)) {
			foreach ($internalpassages_remove as $internalpassage_remove) {
				$this->internalpassages_model->safa_internalsegment_id = $internalpassage_remove;
				$this->internalpassages_model->delete();
			}
		}
	}

	function add_internalsegments($id, $mode = '') {
		//------------------------------- Delete All segments to set ithem again --------------------
		$this->internalpassages_model->safa_trip_internaltrip_id = $id;
		$this->internalpassages_model->delete();
		//-------------------------------------------------------------------------------------------
		// Save internalpassages
		$safa_internalsegmenttype_id = $this->input->post('internalpassage_type');
		$start_datetime = $this->input->post('internalpassage_startdatatime');
		$end_datetime = $this->input->post('internalpassage_enddatatime');
		//$seats_count = $this->input->post('seatscount');
		$notes = $this->input->post('internalpassage_notes');
		$safa_internalsegmentestatus_id = 1;


		$erp_end_hotel_id = $this->input->post('internalpassage_hotel_end');
		$erp_start_hotel_id = $this->input->post('internalpassage_hotel_start');

		$erp_port_start_id = $this->input->post('internalpassage_port_start');
		$erp_port_end_id = $this->input->post('internalpassage_port_end');

		//$safa_tourism_place_start_id = $this->input->post('internalpassages_torismplace_start');
		$safa_tourism_place_end_id = $this->input->post('internalpassage_torismplace_end');


		if (ensure($safa_internalsegmenttype_id))
		foreach ($safa_internalsegmenttype_id as $key => $value) {

			$this->internalpassages_model->safa_trip_internaltrip_id = $id;
			$safa_internalsegment_id = $this->internalpassages_model->safa_internalsegment_id = $key;
			$current_internalpassage_row_count = $this->internalpassages_model->get(true);


			// Save internalpassages
			$this->internalpassages_model->safa_internalsegmenttype_id = $safa_internalsegmenttype_id[$key];
			$this->internalpassages_model->start_datetime = $start_datetime[$key];
			$this->internalpassages_model->end_datetime = $end_datetime[$key];
			//$this->internalpassages_model->seats_count = $seats_count[$key];
			$this->internalpassages_model->notes = $notes[$key];

			//$this->internalpassages_model->safa_internalsegmentestatus_id = 1;



			if ($safa_internalsegmenttype_id[$key] == 1) {
				$this->internalpassages_model->erp_port_id = $erp_port_start_id[$key];
				$this->internalpassages_model->erp_end_hotel_id = $erp_end_hotel_id[$key];
			} else if ($safa_internalsegmenttype_id[$key] == 2) {
				$this->internalpassages_model->erp_start_hotel_id = $erp_start_hotel_id[$key];
				$this->internalpassages_model->erp_port_id = $erp_port_end_id[$key];
			} else if ($safa_internalsegmenttype_id[$key] == 3) {
				$this->internalpassages_model->erp_start_hotel_id = $erp_start_hotel_id[$key];
				$this->internalpassages_model->safa_tourism_place_id = $safa_tourism_place_end_id[$key];
			} else if ($safa_internalsegmenttype_id[$key] == 4) {
				$this->internalpassages_model->erp_start_hotel_id = $erp_start_hotel_id[$key];
				$this->internalpassages_model->erp_end_hotel_id = $erp_end_hotel_id[$key];
			}


			if ($current_internalpassage_row_count == 0) {
				$this->internalpassages_model->safa_internalsegment_id = false;
				$safa_internalsegment_id = $this->internalpassages_model->save();
			} else {
				$this->internalpassages_model->save();
			}

			//----------------- Save Segment Drivers and Buses------------------------------------------
			//if ($mode == 'add') {
			//$drivers_and_buses_safa_transporters_drivers = $this->input->post('drivers_and_buses_safa_transporters_drivers');
			//$drivers_and_buses_safa_transporters_buses = $this->input->post('drivers_and_buses_safa_transporters_buses');

			$safa_buses_brands = $this->input->post('safa_buses_brands');
			$safa_buses_models = $this->input->post('safa_buses_models');
			$bus_count = $this->input->post('bus_count');
			$seats_count = $this->input->post('seats_count');


			if (ensure($safa_buses_brands))
			foreach ($safa_buses_brands as $key => $value) {
				$this->internalsegments_drivers_and_buses_model->safa_internalsegments_id = $safa_internalsegment_id;

				$current_bus_count = $bus_count[$key];

				$current_seats_count = floor($seats_count[$key] / $current_bus_count);
				$reset_current_seats_count = $seats_count[$key] % $current_bus_count;

				//$buses_arr = array();
				$this->transporters_buses_model->auto_generate = 1;
				$this->transporters_buses_model->safa_buses_brands_id = $safa_buses_brands[$key];
				$this->transporters_buses_model->safa_buses_models_id = $safa_buses_models[$key];
				$buses_rows = $this->transporters_buses_model->get();
				//					foreach ($buses_rows as $buses_row) {
				//						$buses_arr[] = $buses_row->safa_transporters_buses_id;
				//					}

				for ($i = 0; $i < $current_bus_count; $i++) {

					$this->internalsegments_drivers_and_buses_model->safa_transporters_drivers_id = null;
					//						if(isset($buses_arr[$i])) {
					//							$this->internalsegments_drivers_and_buses_model->safa_transporters_buses_id = $buses_arr[$i];
					//						} else if(isset($buses_arr[0])) {
					//							$this->internalsegments_drivers_and_buses_model->safa_transporters_buses_id = $buses_arr[0];
					//						}

					if (isset($buses_rows[0])) {
						$this->internalsegments_drivers_and_buses_model->safa_transporters_buses_id = $buses_rows[0]->safa_transporters_buses_id;
					}
					if ($i == 0) {
						$this->internalsegments_drivers_and_buses_model->seats_count = $current_seats_count + $reset_current_seats_count;
					} else {
						$this->internalsegments_drivers_and_buses_model->seats_count = $current_seats_count;
					}

					$this->internalsegments_drivers_and_buses_model->save();
				}
			}
			//}
			//-----------------------------------------------------------------------------------------------
			// By Gouda
			$this->internalpassages_model->safa_trip_internaltrip_id = false;
			$this->internalpassages_model->safa_internalsegmenttype_id = false;
			$this->internalpassages_model->start_datetime = false;
			$this->internalpassages_model->end_datetime = false;
			$this->internalpassages_model->seats_count = false;
			$this->internalpassages_model->notes = false;

			$this->internalpassages_model->erp_port_id = false;
			$this->internalpassages_model->erp_start_hotel_id = false;
			$this->internalpassages_model->erp_end_hotel_id = false;
			$this->internalpassages_model->safa_tourism_place_id = false;
		}


		// --- Delete Deleted rooms rows from Database --------------
		//		$internalpassages_remove = $this->input->post('passages_remove');
		//		if (ensure($internalpassages_remove)) {
		//			foreach ($internalpassages_remove as $internalpassage_remove) {
		//				$this->internalpassages_model->safa_internalsegment_id = $internalpassage_remove;
		//				$this->internalpassages_model->delete();
		//			}
		//		}
	}

	function save_rooms_count($id) {
		$erp_hotel_id = post('erp_hotel_id');
		if ($erp_hotel_id == '') {
			$erp_hotel_id = array();
		}

		if (count($erp_hotel_id) > 0) {
			foreach ($erp_hotel_id as $hkey => $hotelid) {

				$hotels_rooms_count = $this->input->post('hotels_rooms_count');
				if (isset($hotels_rooms_count[$hkey])) {
					foreach ($hotels_rooms_count[$hkey] as $roomsize => $rooms_count) {
						$this->db->set('safa_trip_internaltrip_id', $id);
						$this->db->set('erp_hotel_id', $hotelid);
						$this->db->set('erp_hotelroomsize_id', $roomsize);
						$this->db->set('rooms_count', $rooms_count);

						if (strpos($hkey, 'N') === false) {
							$this->db->where('safa_trip_internaltrip_id', $id);
							$this->db->where('erp_hotel_id', $hotelid);
							$this->db->where('erp_hotelroomsize_id', $roomsize);
							if ($this->db->from('safa_trip_internaltrips_hotels')->count_all_results()) {
								$this->db->where('safa_trip_internaltrip_id', $id);
								$this->db->where('erp_hotel_id', $hotelid);
								$this->db->where('erp_hotelroomsize_id', $roomsize);
								$this->db->set('rooms_count', $rooms_count);
								$this->db->update('safa_trip_internaltrips_hotels');
							} else {
								$this->db->set('safa_trip_internaltrip_id', $id);
								$this->db->set('erp_hotel_id', $hotelid);
								$this->db->set('erp_hotelroomsize_id', $roomsize);
								$this->db->set('rooms_count', $rooms_count);
								$this->db->insert('safa_trip_internaltrips_hotels');
							}
						} else {
							$this->db->set('safa_trip_internaltrip_id', $id);
							$this->db->set('erp_hotel_id', $hotelid);
							$this->db->set('erp_hotelroomsize_id', $roomsize);
							$this->db->set('rooms_count', $rooms_count);
							$this->db->insert('safa_trip_internaltrips_hotels');
						}
					}
				}
			}
		}
	}

	function update_safa_group_passports($id) {
		if ($this->input->post('safa_trip_traveller_ids')) {
			$safa_group_passports_remove = $this->input->post('safa_group_passports_remove');
			if (ensure($safa_group_passports_remove)) {
				foreach ($safa_group_passports_remove as $safa_group_passport_remove) {
					$this->safa_group_passports_model->safa_group_passport_id = $safa_group_passport_remove;
					$this->safa_group_passports_model->safa_trip_internaltrip_id = null;
					$this->safa_group_passports_model->save();
				}
			}
			foreach ($this->input->post('safa_trip_traveller_ids') as $safa_trip_traveller_id) {
				$this->safa_group_passports_model->safa_group_passport_id = $safa_trip_traveller_id;
				$this->safa_group_passports_model->safa_trip_internaltrip_id = $id;
				$this->safa_group_passports_model->save();
			}
		}
	}

	function send_notification($safa_trip_internaltrip_id, $mode) {
		if ($mode == 'add') {
			$erp_system_events_id = 19;
		} else {
			$erp_system_events_id = 20;
		}
		//----------- Send Notification For EA, UO Company ------------------------------
		$msg_datetime = date('Y-m-d H:i', time());

		$this->notification_model->notification_type = 'automatic';
		$this->notification_model->erp_importance_id = 1;
		$this->notification_model->sender_type_id = 1;
		$this->notification_model->sender_id = 0;

		$this->notification_model->erp_system_events_id = $erp_system_events_id;

		$this->notification_model->language_id = 2;
		$this->notification_model->msg_datetime = $msg_datetime;


		$internaltripstatus_name = '';
		$safa_internaltripstatus_id = $this->input->post('safa_internaltripstatus_id');
		$this->internaltrip_status_model->safa_internaltripstatus_id = $safa_internaltripstatus_id;
		$internaltrip_status_row = $this->internaltrip_status_model->get();
		if (count($internaltrip_status_row) > 0) {
			$internaltripstatus_name = $internaltrip_status_row->{name()};
		}
		$trip_name = '';
		$safa_trip_id = $this->input->post('safa_trip_id');
		$this->safa_trips_model->safa_trip_id = $safa_trip_id;
		$trip_row = $this->safa_trips_model->get();
		if (count($trip_row) > 0) {
			$trip_name = $trip_row->name;
		}

		//Link to replace.
		$link_internaltrip = "<a href='" . site_url('safa_trip_internaltrips/manage/' . $safa_trip_internaltrip_id) . "'  target='_blank'> $safa_trip_internaltrip_id </a>";

		$this->notification_model->tags = "#*safa_trip_internaltrip_id#*=$link_internaltrip*****#*internaltripstatus#*=$internaltripstatus_name*****#*the_date#*=$msg_datetime*****#*trip#*=$trip_name";
		$this->notification_model->parent_id = '';

		$erp_notification_id = $this->notification_model->save();

		//-------- Notification Details ---------------
		$this->notification_model->detail_erp_notification_id = $erp_notification_id;
		if (session('uo_id')) {
			$this->notification_model->detail_receiver_type = 3;
		} else if (session('ea_id')) {
			$this->notification_model->detail_receiver_type = 2;
		}


		$this->contracts_model->safa_uo_contract_id = $this->input->post('safa_uo_contract_id');
		$contracts_row = $this->contracts_model->search();
		if (count($contracts_row) > 0) {
			if (session('uo_id')) {
				$this->notification_model->detail_receiver_id = $contracts_row[0]->safa_ea_id;
				if ($contracts_row[0]->safa_ea_id != '') {
					$this->notification_model->saveDetails();
				}
			} else if (session('ea_id')) {
				$this->notification_model->detail_receiver_id = $contracts_row[0]->safa_uo_id;
				$this->notification_model->saveDetails();
			}
		}
		//--------------------------------
		//-------------------------------------------------------------------
		//----------- Send Notification For ITO Company ------------------------------
		if ($this->input->post('safa_ito_id') != '' && $this->input->post('safa_ito_id') != null) {
			$msg_datetime = date('Y-m-d H:i', time());

			$this->notification_model->notification_type = 'automatic';
			$this->notification_model->erp_importance_id = 1;
			$this->notification_model->sender_type_id = 1;
			$this->notification_model->sender_id = 0;

			$this->notification_model->erp_system_events_id = $erp_system_events_id;

			$this->notification_model->language_id = 2;
			$this->notification_model->msg_datetime = $msg_datetime;


			$internaltripstatus_name = '';
			$safa_internaltripstatus_id = $this->input->post('safa_internaltripstatus_id');
			$this->internaltrip_status_model->safa_internaltripstatus_id = $safa_internaltripstatus_id;
			$internaltrip_status_row = $this->internaltrip_status_model->get();
			if (count($internaltrip_status_row) > 0) {
				$internaltripstatus_name = $internaltrip_status_row->{name()};
			}
			$trip_name = '';
			$safa_trip_id = $this->input->post('safa_trip_id');
			$this->safa_trips_model->safa_trip_id = $safa_trip_id;
			$trip_row = $this->safa_trips_model->get();
			if (count($trip_row) > 0) {
				$trip_name = $trip_row->name;
			}

			//Link to replace.
			$link_internaltrip = "<a href='" . site_url('ito/trip_internaltrip/edit_incoming/' . $safa_trip_internaltrip_id) . "'  target='_blank' > $safa_trip_internaltrip_id </a>";

			$this->notification_model->tags = "#*safa_trip_internaltrip_id#*=$link_internaltrip*****#*internaltripstatus#*=$internaltripstatus_name*****#*the_date#*=$msg_datetime*****#*trip#*=$trip_name";
			$this->notification_model->parent_id = '';

			$erp_notification_id = $this->notification_model->save();

			//-------- Notification Details ---------------
			$this->notification_model->detail_erp_notification_id = $erp_notification_id;
			$this->notification_model->detail_receiver_type = 5;
			$this->notification_model->detail_receiver_id = $this->input->post('safa_ito_id');
			$this->notification_model->saveDetails();

			//--------------------------------
		}
		//-------------------------------------------------------------------
	}

	function delete_($id = false) {
		if (!$id)
		show_404();

		$this->trip_internaltrip_model->safa_trip_internaltrip_id = $id;
		$trip_internaltrip_row = $this->trip_internaltrip_model->get();

		if (count($trip_internaltrip_row) > 0) {
			$this->flight_availabilities_model->erp_flight_availability_id = $trip_internaltrip_row->erp_flight_availability_id;

			$flight_availabilities_row = $this->flight_availabilities_model->get();
			if (count($flight_availabilities_row) > 0) {
				if ($flight_availabilities_row->by_trip_internaltrip == 1) {
					$this->flight_availabilities_model->delete();
				}
			}

			if (!$this->trip_internaltrip_model->delete())
			show_404();
			$this->load->view('redirect_message', array('msg' => lang('global_deleted_message'),
                'url' => site_url('safa_trip_internaltrips'),
                'model_title' => lang('safa_trip_internaltrips'), 'action' => lang('delete_safa_trip_internaltrips')));
		}
	}

	function delete($id = false) {
		if (!$id)
		show_404();
	 	
		$this->trip_internaltrip_model->safa_trip_internaltrip_id = $id;
		$this->trip_internaltrip_model->deleted = 1;
		$this->trip_internaltrip_model->delete_reason = $this->input->post('delete_reason');

		$this->trip_internaltrip_model->save();

		$this->load->view('redirect_message', array('msg' => lang('global_deleted_message'),
            'url' => site_url('safa_trip_internaltrips'),
            'model_title' => lang('safa_trip_internaltrips'), 'action' => lang('delete_safa_trip_internaltrips')));
	}

	function delete_confirmation($id) 
	{
		if (session('ito_id')) {
			$this->trip_internaltrip_model->owner_erp_company_type_id = 5;
		}
		$this->trip_internaltrip_model->safa_trip_internaltrip_id = $id;
		$item = $this->trip_internaltrip_model->get();
		if(!$item) show_404();
			
		$this->load->view('safa_trip_internaltrips/delete_confirmation', array('msg' => lang('global_are_you_sure_you_want_to_delete'),
            'back_url' => site_url('safa_trip_internaltrips'),
            'confirmation_url' => site_url('safa_trip_internaltrips/delete/' . $id),
            'model_title' => lang('safa_trip_internaltrips'), 'action' => lang('delete_safa_trip_internaltrips'),
		));
	}

	function confirm($id = false)
	{
		if (!$id)
		show_404();

		$this->trip_internaltrip_model->safa_trip_internaltrip_id = $id;
		if(session('uo_id')) {
			$this->trip_internaltrip_model->safa_internaltripstatus_id = 4;
		} else if(session('ito_id')) {
			$this->trip_internaltrip_model->safa_internaltripstatus_id = 5;
		} if(session('ea_id')) {
			$this->trip_internaltrip_model->safa_internaltripstatus_id = 2;
		}

		$this->trip_internaltrip_model->save();

		$this->load->view('redirect_message', array('msg' => lang('confirmation_message'),
            'url' => site_url('safa_trip_internaltrips'),
            'model_title' => lang('safa_trip_internaltrips'), 'action' => lang('confirmation')));
	}

	function get_hotel_by_city() {
		$cityid = $this->input->post('cityid');
		if ($cityid == 0) {
			$hotels = ddgen('erp_hotels', array('erp_hotel_id', name()), '', array(name(), 'ASC'), TRUE);
		} else {
			$hotels = ddgen('erp_hotels', array('erp_hotel_id', name()), array('erp_city_id' => $cityid), array(name(), 'ASC'), TRUE);
		}

		$hotels_options = '';
		foreach ($hotels as $key => $value)
		$hotels_options .= "<option value='$key'>$value</option>\n";

		$this->hotels_model->order_by = array('erp_hotel_id', 'desc');
		$this->hotels_model->limit = 1;
		$hotels_rows = $this->hotels_model->get();


		$data_arr[] = array('hotels_options' => $hotels_options,
            'hotel_selected' => $hotels_rows[0]->erp_hotel_id,
		);
		echo json_encode($data_arr);
		exit;
	}

	function check_serial_by_safa_uo_contract_ajax($safa_trip_internaltrip_id = 0) {
		$serial = $this->input->post('serial');

		$this->trip_internaltrip_model->uo_id = session('uo_id');
		$this->trip_internaltrip_model->serial = $serial;
		if ($safa_trip_internaltrip_id != 0) {
			$this->trip_internaltrip_model->where_condition = "(safa_trip_internaltrips.safa_trip_internaltrip_id!=$safa_trip_internaltrip_id )";
		}

		$trip_internaltrip_rows = $this->trip_internaltrip_model->get();

		$are_repeated = 0;
		if ($serial != '' && $serial != 0) {
			if (count($trip_internaltrip_rows) > 0) {
				$are_repeated = 1;
			}
		}

		$data_arr[] = array('are_repeated' => $are_repeated);
		echo json_encode($data_arr);
		exit;
	}

	function get_confirmation_number_by_safa_uo_contract_and_safa_ito_ajax() {

		$safa_uo_contract_id = $this->input->post('safa_uo_contract_id');
		$this->contracts_model->safa_uo_contract_id = $safa_uo_contract_id;
		$contracts_row = $this->contracts_model->get();
		$safa_uo_id = $contracts_row->safa_uo_id;

		$safa_ito_id = $this->input->post('safa_ito_id');

		$this->trip_internaltrip_model->uo_id = $safa_uo_id;
		$this->trip_internaltrip_model->safa_ito_id = $safa_ito_id;
		$this->trip_internaltrip_model->order_by[0] = 'safa_trip_internaltrips.confirmation_number';
		$this->trip_internaltrip_model->order_by[1] = 'DESC';
		$this->trip_internaltrip_model->get_single_row = TRUE;
		$this->trip_internaltrip_model->limit = 1;

		$trip_internaltrip_rows = $this->trip_internaltrip_model->get();

		$confirmation_number = 0;
		if (count($trip_internaltrip_rows) > 0) {
			if (isset($trip_internaltrip_rows[0])) {
				$confirmation_number = (int) $trip_internaltrip_rows[0]->confirmation_number;
			}
		}

		$data_arr[] = array('confirmation_number' => $confirmation_number + 1);
		echo json_encode($data_arr);
		exit;
	}

	function check_confirmation_number_by_safa_uo_contract_and_safa_ito_ajax($safa_trip_internaltrip_id = 0) {

		$safa_uo_contract_id = $this->input->post('safa_uo_contract_id');
		$this->contracts_model->safa_uo_contract_id = $safa_uo_contract_id;
		$contracts_row = $this->contracts_model->get();
		$safa_uo_id = $contracts_row->safa_uo_id;

		$safa_ito_id = $this->input->post('safa_ito_id');

		$confirmation_number = $this->input->post('confirmation_number');

		$this->trip_internaltrip_model->uo_id = $safa_uo_id;
		$this->trip_internaltrip_model->safa_ito_id = $safa_ito_id;
		$this->trip_internaltrip_model->confirmation_number = $confirmation_number;
		if ($safa_trip_internaltrip_id != 0) {
			$this->trip_internaltrip_model->where_condition = "(safa_trip_internaltrips.safa_trip_internaltrip_id!=$safa_trip_internaltrip_id )";
		}

		$trip_internaltrip_rows = $this->trip_internaltrip_model->get();

		$are_repeated = 0;
		if ($confirmation_number != '' && $confirmation_number != 0) {
			if (count($trip_internaltrip_rows) > 0) {
				$are_repeated = 1;
			}
		}

		$data_arr[] = array('are_repeated' => $are_repeated);
		echo json_encode($data_arr);
		exit;
	}

	function get_ports_halls_by_port($erp_port_id = false) {
		$this->port_halls_model->erp_port_id = $erp_port_id;
		$erp_ports_halls_rows = $this->port_halls_model->get();
		$erp_ports_halls_arr = array();
		foreach ($erp_ports_halls_rows as $erp_ports_halls_row) {
			$erp_ports_halls_arr[$erp_ports_halls_row->erp_port_hall_id] = $erp_ports_halls_row->{name()};
		}

		$return = '';
		foreach ($erp_ports_halls_arr as $key => $value)
		$return .= "<option value='$key'>$value</option>\n";

		$this->layout = 'ajax';
		echo $return;
		die();
	}

	public function get_ports_halls_by_states() {
		$this->layout = 'ajax';
		$this->load->library('flight_states');

		$flight_carrier = $this->input->post('safa_transporter_code');
		$flight_number = $this->input->post('flight_number');
		//$flight_date = $this->input->post('flight_date');
		$flight_date = date('Y-m-d', strtotime($this->input->post('flight_date')));

		$this->flight_states->flight_carrier = $flight_carrier;
		$this->flight_states->flight_number = $flight_number;
		$this->flight_states->flight_date = $flight_date;


		$erp_port_id_from = '';
		$erp_port_id_to = '';
		$erp_port_hall_id_from = '';
		$erp_port_hall_id_to = '';

		$airports_rows = $this->flight_states->getAirports();
		foreach ($airports_rows as $airports_row) {
			if ($airports_row->countryCode == 'SA') {
				$erp_port_id_from_iata = $flight_states_row->iata;

				$this->ports_model->code = $erp_port_id_from_iata;
				$erp_ports_rows = $this->ports_model->get();
				if (count($erp_ports_rows) > 0) {
					$erp_port_id_from = $erp_sa_ports_rows[0]->erp_port_id;
				}
			} else {
				$erp_port_id_to_iata = $flight_states_row->iata;

				$this->ports_model->code = $erp_port_id_to_iata;
				$erp_ports_rows = $this->ports_model->get();
				if (count($erp_ports_rows) > 0) {
					$erp_port_id_to = $erp_sa_ports_rows[0]->erp_port_id;
				}
			}
		}

		$resources_rows = $this->flight_states->getResources();

		if (count($resources_rows) > 0) {
			$erp_port_hall_id_from = $resources_rows[0]->departureTerminal;
			$erp_port_hall_id_to = $resources_rows[0]->arrivalTerminal;
		}

		$data_arr[] = array(
            'erp_port_id_from' => $erp_port_id_from,
            'erp_port_id_to' => $erp_port_id_to,
            'erp_port_hall_id_from' => $erp_port_hall_id_from,
            'erp_port_hall_id_to' => $erp_port_hall_id_to,
		);

		echo json_encode($data_arr);
		exit;
	}

	function add_hotel_popup($hotel_counter) {
		$this->layout = 'js_new';

		$data = array();
		$data['hotel_counter'] = $hotel_counter;
		if (!isset($_POST['smt_save'])) {
			$this->load->view('safa_trip_internaltrips/add_hotel_popup', $data);
		} else {

			$this->load->library("form_validation");
			$this->form_validation->set_rules('name_ar', 'lang:hotels_name_ar', 'trim');
			$this->form_validation->set_rules('name_la', 'lang:hotels_name_la', 'trim');
			//$this->form_validation->set_rules('erp_city_id', 'lang:hotels_erp_city_id', 'trim');

			if ($this->form_validation->run() == false) {

				$this->load->view("safa_trip_internaltrips/add_hotel_popup", $data);
			} else {
				$this->hotels_model->name_ar = $this->input->post('name_ar');
				$this->hotels_model->name_la = $this->input->post('name_la');
				$this->hotels_model->erp_city_id = $this->input->post('erp_city_id');

				$hotel_id = $this->hotels_model->save();


				//----------- Send Notification For Company ------------------------------
				$msg_datetime = date('Y-m-d H:i', time());

				$this->notification_model->notification_type = 'automatic';
				$this->notification_model->erp_importance_id = 1;
				$this->notification_model->sender_type_id = 1;
				$this->notification_model->sender_id = 0;

				$this->notification_model->erp_system_events_id = 25;

				$this->notification_model->language_id = 2;
				$this->notification_model->msg_datetime = $msg_datetime;

				/*
				 $current_phase_name=$this->input->post('current_phase');
				 $next_phase_id=$this->input->post('next_phase');
				 $this->safa_contract_phases_model->safa_contract_phases_id=$next_phase_id;
				 $next_phase_row=$this->safa_contract_phases_model->get();
				 $next_phase_name=$next_phase_row->{name()};

				 $tags="#*current_contract_phase#*=$current_phase_name*****#*next_contract_phase#*=$next_phase_name";
				 */



				//Link to replace.
				$link_hotels = "<a href='" . site_url('hotels/hotel_details/' . $hotel_id) . "'  target='_blank'> " . $this->input->post('name_ar') . " </a>";

				$tags = "#*the_date#*=" . date('Y-m-d H:i', time()) . "*****#*hotel_id#*=$link_hotels*****#*safa_uo#*=" . session('uo_name');

				$this->notification_model->tags = $tags;
				$this->notification_model->parent_id = '';

				$erp_notification_id = $this->notification_model->save();

				//-------- Notification Details ---------------
				$this->notification_model->detail_erp_notification_id = $erp_notification_id;

				$this->notification_model->detail_receiver_type = 1;

				$this->notification_model->detail_receiver_id = 1;
				$this->notification_model->saveDetails();
				//--------------------------------
				//-------------------------------------------------------------------



				$this->load->view('message_popup', array('msg' => lang('hotel_added_successfully'),
                    'url' => site_url("safa_trip_internaltrips/add_hotel_popup"),
                    'id' => $this->uri->segment("4"),
                    'model_title' => lang('add_hotel'), 'action' => lang('add_hotel')));
			}
		}
	}

	function add_bus_brand_popup($bus_brand_counter) {
		$this->layout = 'js_new';

		$data = array();
		$data['bus_brand_counter'] = $bus_brand_counter;
		if (!isset($_POST['smt_save'])) {
			$this->load->view('safa_trip_internaltrips/add_bus_brand_popup', $data);
		} else {

			$this->load->library("form_validation");
			$this->form_validation->set_rules('name_ar', 'lang:name_ar', 'trim');
			$this->form_validation->set_rules('name_la', 'lang:name_la', 'trim');

			if ($this->form_validation->run() == false) {

				$this->load->view("safa_trip_internaltrips/add_bus_brand_popup", $data);
			} else {
				$this->safa_buses_brands_model->name_ar = $this->input->post('name_ar');
				$this->safa_buses_brands_model->name_la = $this->input->post('name_la');

				$safa_buses_brands_id = $this->safa_buses_brands_model->save();

				$this->load->view('message_popup', array('msg' => lang('bus_brand_added_successfully'),
                    'url' => site_url("safa_trip_internaltrips/add_bus_brand_popup"),
                    'id' => $this->uri->segment("4"),
                    'model_title' => lang('add_bus_brand'), 'action' => lang('add_bus_brand')));
			}
		}
	}

	function add_bus_model_popup($bus_model_counter, $safa_buses_brands_id)
	{
		$this->layout = 'js_new';

		$data = array();
		$data['bus_model_counter'] = $bus_model_counter;
		$data['safa_buses_brands_id'] = $safa_buses_brands_id;

		if (!isset($_POST['smt_save'])) {
			$this->load->view('safa_trip_internaltrips/add_bus_model_popup', $data);
		} else {

			$this->load->library("form_validation");
			$this->form_validation->set_rules('name_ar', 'lang:name_ar', 'trim');
			$this->form_validation->set_rules('name_la', 'lang:name_la', 'trim');

			if ($this->form_validation->run() == false) {

				$this->load->view("safa_trip_internaltrips/add_bus_brand_popup", $data);
			} else {

				$this->safa_buses_models_model->safa_buses_brands_id = $safa_buses_brands_id;
				$this->safa_buses_models_model->name_ar = $this->input->post('name_ar');
				$this->safa_buses_models_model->name_la = $this->input->post('name_la');

				$safa_buses_models_id = $this->safa_buses_models_model->save();

				$this->transporters_buses_model->safa_transporters_id = null;

				//$this->transporters_buses_model->bus_no = 'غير محدد';
				$this->transporters_buses_model->bus_no =$this->input->post('name_ar');
				$this->safa_buses_brands_model->safa_buses_brands_id = $safa_buses_brands_id;
				$safa_buses_brands_row = $this->safa_buses_brands_model->get();
				if(count($safa_buses_brands_row)>0) {
					$this->transporters_buses_model->bus_no =$safa_buses_brands_row->{name()}.' - '.$this->input->post('name_ar');
				}

				$this->transporters_buses_model->safa_buses_brands_id = $safa_buses_brands_id;
				$this->transporters_buses_model->safa_buses_models_id = $safa_buses_models_id;
				$this->transporters_buses_model->industry_year = '';
				$this->transporters_buses_model->passengers_count = '';
				$this->transporters_buses_model->notes = '';
				$this->transporters_buses_model->auto_generate = 1;

				$this->transporters_buses_model->save();

				$this->load->view('message_popup', array('msg' => lang('bus_model_added_successfully'),
                    'url' => site_url("safa_trip_internaltrips/add_bus_brand_popup"),
                    'id' => $this->uri->segment("4"),
                    'model_title' => lang('add_bus_model'), 'action' => lang('add_bus_model')));
			}
		}
	}

	function get_bus_brands() {
		$bus_brands = ddgen('safa_buses_brands', array('safa_buses_brands_id', name()), '', array(name(), 'ASC'), TRUE);


		$bus_brands_options = '';
		foreach ($bus_brands as $key => $value)
		$bus_brands_options .= "<option value='$key'>$value</option>\n";

		$this->safa_buses_brands_model->order_by = array('safa_buses_brands_id', 'desc');
		$this->safa_buses_brands_model->limit = 1;
		$bus_brands_rows = $this->safa_buses_brands_model->get();


		$data_arr[] = array('bus_brands_options' => $bus_brands_options,
            'bus_brand_selected' => $bus_brands_rows[0]->safa_buses_brands_id,
		);
		echo json_encode($data_arr);
		exit;
	}

	function add_transporter_popup($erp_transportertype_id = 0) {
		$this->layout = 'js_new';

		$data = array();

		$data['erp_countries'] = ddgen('erp_countries', array('erp_country_id', name()));

		$data['erp_transportertype_id'] = $erp_transportertype_id;

		if (!isset($_POST['smt_save'])) {
			$this->load->view('safa_trip_internaltrips/add_transporter_popup', $data);
		} else {

			$this->load->library("form_validation");
			$this->form_validation->set_rules('erp_country_id', 'lang:safa_transporter_erp_country_id', 'trim|required');
			$this->form_validation->set_rules('name_ar', 'lang:safa_transporter_name_ar', 'trim|required|is_unique[safa_transporters.name_ar]');
			$this->form_validation->set_rules('name_la', 'lang:safa_transporter_name_la', 'trim|required|is_unique[safa_transporters.name_la]');
			$this->form_validation->set_rules('code', 'lang:safa_transporter_code', 'trim|required|max_length[15]');

			if ($this->form_validation->run() == false) {
				$this->load->view("safa_trip_internaltrips/add_transporter_popup", $data);
			} else {

				$this->transporters_model->erp_country_id = $this->input->post('erp_country_id');
				$this->transporters_model->name_ar = $this->input->post('name_ar');
				$this->transporters_model->name_la = $this->input->post('name_la');
				$this->transporters_model->erp_transportertype_id = $erp_transportertype_id;
				$this->transporters_model->code = $this->input->post('code');
				$transporter_id = $this->transporters_model->save();


				//----------- Send Notification For Company ------------------------------
				//            $msg_datetime = date('Y-m-d H:i', time());
				//
				//            $this->notification_model->notification_type = 'automatic';
				//            $this->notification_model->erp_importance_id = 1;
				//            $this->notification_model->sender_type_id = 1;
				//            $this->notification_model->sender_id = 0;
				//
				//            $this->notification_model->erp_system_events_id = 25;
				//
				//            $this->notification_model->language_id = 2;
				//            $this->notification_model->msg_datetime = $msg_datetime;
				//
				//            /*
				//              $current_phase_name=$this->input->post('current_phase');
				//              $next_phase_id=$this->input->post('next_phase');
				//              $this->safa_contract_phases_model->safa_contract_phases_id=$next_phase_id;
				//              $next_phase_row=$this->safa_contract_phases_model->get();
				//              $next_phase_name=$next_phase_row->{name()};
				//
				//              $tags="#*current_contract_phase#*=$current_phase_name*****#*next_contract_phase#*=$next_phase_name";
				//             */
				//
				//
				//
				//            //Link to replace.
				//            $link_hotels = "<a href='" . site_url('hotels/hotel_details/' . $hotel_id) . "'  target='_blank'> " . $this->input->post('name_ar') . " </a>";
				//
				//            $tags = "#*the_date#*=" . date('Y-m-d H:i', time()) . "*****#*hotel_id#*=$link_hotels*****#*safa_uo#*=".session('uo_name');
				//
				//            $this->notification_model->tags = $tags;
				//            $this->notification_model->parent_id = '';
				//
				//            $erp_notification_id = $this->notification_model->save();
				//
				//            //-------- Notification Details ---------------
				//            $this->notification_model->detail_erp_notification_id = $erp_notification_id;
				//
				//            $this->notification_model->detail_receiver_type = 1;
				//
				//            $this->notification_model->detail_receiver_id = 1;
				//            $this->notification_model->saveDetails();
				//--------------------------------
				//-------------------------------------------------------------------



				$this->load->view('message_popup', array('msg' => lang('transporter_added_successfully'),
                    'url' => site_url("safa_trip_internaltrips/add_transporter_popup"),
                    'id' => $this->uri->segment("4"),
                    'model_title' => lang('add_transporter'), 'action' => lang('add_transporter')));
			}
		}
	}

	function get_transporters($erp_transportertype_id) {
		$safa_transporters_query = $this->db->query('select safa_transporters.code, CONCAT(safa_transporters.' . name() . ',\' \', `safa_transporters`.`code`) as transporter_code_name from  safa_transporters where  safa_transporters.erp_transportertype_id=' . $erp_transportertype_id . ' order by  transporter_code_name');
		$safa_transporters_rows = $safa_transporters_query->result();
		//$safa_transporters_rows = $this->db->order_by('iata', 'asc')->where("iata <> ''")->get(FSDB . '.fs_airlines')->result();
		$safa_transporters_arr = array();
		foreach ($safa_transporters_rows as $safa_transporters_row) {
			$safa_transporters_arr[$safa_transporters_row->code] = $safa_transporters_row->transporter_code_name;
		}


		$return = '';
		foreach ($safa_transporters_arr as $key => $value)
		$return .= "<option value='$key'>$value</option>\n";

		$this->layout = 'ajax';
		echo $return;
		exit();
	}

	function internalsegments_report($id, $pdfview = false, $format = 'F') {
		$this->layout = 'ajax';

		$this->trip_details_model->safa_trip_internaltrip_id = $id;
		$data['internalsegments'] = $internalsegments_rows =$this->trip_details_model->get_internal_segments();
		//echo $this->db->last_query();exit;
		$data['safa_uo_code'] = '';
		if (count($data['internalsegments']) > 0) {

			$data['buses'] = $this->internalsegments_drivers_and_buses_model->get_distinct_bus_brands_by_safa_internalsegment($data['internalsegments'][0]->safa_internalsegment_id);

			$data["arriving_segment_bus_brands"] = array();
			foreach ($internalsegments_rows as $internalsegments_row) {
				if ($internalsegments_row->safa_internalsegmenttype_id == 1) {
					$data["arriving_segment_bus_brands"] = $this->internalsegments_drivers_and_buses_model->get_distinct_bus_brands_by_safa_internalsegment($internalsegments_row->safa_internalsegment_id);
				}
			}

			if (session('uo_id')) {
				$data['uo_dir'] = session('uo_id');
				$this->safa_uos_model->safa_uo_id = session('uo_id');
				$safa_uo_row = $this->safa_uos_model->get();
				$data['safa_uo_code'] = $safa_uo_row->code;
			} else if (session('ea_id')) {
				$this->contracts_model->safa_uo_contract_id = $data['internalsegments'][0]->safa_uo_contract_id;
				$contracts_row = $this->contracts_model->get();
				if (count($contracts_row) > 0) {
					$safa_uo_code = '';
					$safa_uo_serial = '';

					$this->safa_uos_model->safa_uo_id = $contracts_row->safa_uo_id;
					$data['uo_dir'] = $contracts_row->safa_uo_id;
					$safa_uo_row = $this->safa_uos_model->get();
					if (count($safa_uo_row) > 0) {
						$data['safa_uo_code'] = $safa_uo_row->code;
					}
				}
			}
		}



		if (strtolower($pdfview) == 'pdf') {
			$page = $this->load->view('uo/trips/internalsegments', $data, TRUE);
			$this->load->helper('pdf_helper');
			$pdf_orientation = 'P';
			$pdf_unit = 'px';
			$pdf_pageformat = 'A4';
			$pdf_unicode = true;
			$pdf_encoding = 'UTF-8';

			$pdf = new virgo_pdf($pdf_orientation, $pdf_unit, $pdf_pageformat, $pdf_unicode, $pdf_encoding, false);
			$tahoma = $pdf->addTTFfont('static/fonts/arial.ttf', 'TrueTypeUnicode', '', 96);
			$pdf->SetFont($tahoma, '', 10, '', false);
			$pdf->SetCreator('Safa Live');
			$pdf->SetAuthor('Safa Live');
			$pdf->SetTitle('Safa Live Reports');
			$pdf->SetSubject('Safa Live');

			$pdf->setPrintHeader(false);

			$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

			// set margins
			$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
			$pdf->SetHeaderMargin(20);
			$pdf->SetFooterMargin(0);

			// remove default footer
			$pdf->setPrintFooter(false);

			// set auto page breaks
			$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

			// set image scale factor
			$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

			$pdf->setRTL(true);
			$pdf->AddPage();

			$pdf->writeHTML($page, true, false, true, false, '');


			$pdf->lastPage();
			ob_end_clean();

			$this->load->helper('download');
			if ($format == 'D') {
				$pdf->Output("internalsegments_$id.pdf", 'D');
			} else {
				$pdf->Output("./static/uploads/safa_trip_internaltrips_pdf/internalsegments_$id.pdf", 'F');
			}
		} else {
			$this->load->view('uo/trips/internalsegments', $data);
		}
	}

	function send_email_to_uo_popup($id) {
		$this->layout = 'js_new';

		$this->trip_internaltrip_model->safa_trip_internaltrip_id = $id;
		$item = $this->trip_internaltrip_model->get();

		$this->safa_uos_model->safa_uo_id = $item->safa_uo_id;
		$safa_uo_row = $this->safa_uos_model->get();
		$safa_uo_code = $safa_uo_row->code;
		$safa_uo_name = $safa_uo_row->{name()};
		$serial = $item->serial;
		$safa_uo_contract_name = $item->safa_uo_contract_name;


		$safa_ea_name = '';
		$safa_ea_email = '';
		if (session('ea_id')) {
			$this->eas_model->safa_ea_id = session('ea_id');
			$safa_eas_row = $this->eas_model->get();

			$safa_ea_name = session('ea_name');
			$safa_ea_email = $safa_eas_row->email;
		}

		$going_datetime = '';
		$return_datetime = '';
		$this->internalpassages_model->safa_trip_internaltrip_id = $id;
		$this->internalpassages_model->order_by = array("safa_internalsegments.start_datetime", 'ASC');
		$internalpassages = $this->internalpassages_model->get();
		if (isset($internalpassages)) {
			foreach ($internalpassages as $inernalpassage) {
				if ($inernalpassage->safa_internalsegmenttype_id == 1) {
					$going_datetime = $inernalpassage->start_datetime;
				} else if ($inernalpassage->safa_internalsegmenttype_id == 2) {
					$return_datetime = $inernalpassage->start_datetime;
				}
			}
		}

		$this->safa_uos_departments_model->safa_uo_id = $item->safa_uo_id;
		$safa_uos_departments_rows = $this->safa_uos_departments_model->get();



		$data["safa_uos_departments_rows"] = $safa_uos_departments_rows;

		if (!isset($_POST['smt_send'])) {
			$this->load->view('safa_trip_internaltrips/send_email_to_uo_popup', $data);
		} else {

			$emails_str = '';
			foreach ($this->input->post('emails') as $email) {
				$emails_str = $emails_str . $email . ',';
			}

			$this->load->library('email');

			//$email_subject = $safa_ea_name.' | '.lang('safa_trip_internaltrip_email_subject'). ' | '.$going_datetime;
			$going_datetime = date('Y/m/d H:i', strtotime($going_datetime));
			$email_subject = "$safa_ea_name - بيان وصول رحلة رقم $safa_uo_code - $serial   بتاريخ $going_datetime";

			//$email_message = lang('safa_trip_internaltrip_email_message');
			$email_message = 'السادة : شركة ' . $safa_uo_name . ' المحترمين<br/>
			يرجى الاطلاع على بيان الوصول رقم ' . $safa_uo_code . '-' . $serial . '  المرفق<br/>
			 <br/>
			' . $safa_uo_contract_name . '<br/>
			(' . session('ea_user_name') . ')<br/>
			للاطلاع على بيان الوصول من خلال نظام صفا اضغط <a href="' . site_url('safa_trip_internaltrips/manage/' . $id) . '" >هنا</a><br/>
			';


			//$this->email->from(config('webmaster_email'), config('title'));
			$this->email->from('notifications@safalive.com', $safa_ea_name);

			$this->email->to($emails_str);
			$this->email->reply_to($safa_ea_email);
			$this->email->cc($safa_ea_email);

			$this->email->bcc('notifications@safalive.com');

			$this->pdf_export($id);
			$this->email->attach("./static/uploads/safa_trip_internaltrips_pdf/safa_trip_internaltrips_$id.pdf");

			$this->email->subject($email_subject);
			$this->email->message($email_message);
			$sending_email_result = $this->email->send();

			if ($sending_email_result) {

				unlink("./static/uploads/safa_trip_internaltrips_pdf/safa_trip_internaltrips_$id.pdf");

				//----------- Send Notification For Company ------------------------------
				$msg_datetime = date('Y-m-d H:i', time());

				$this->notification_model->notification_type = 'automatic';
				$this->notification_model->erp_importance_id = 1;
				$this->notification_model->sender_type_id = 1;
				$this->notification_model->sender_id = 0;

				$this->notification_model->erp_system_events_id = 26;

				$this->notification_model->language_id = 2;
				$this->notification_model->msg_datetime = $msg_datetime;

				$tags = '';
				if (count($item) > 0) {

					//Link to replace.

					$this->safa_uos_model->safa_uo_id = $item->safa_uo_id;
					$safa_uo_row = $this->safa_uos_model->get();
					$safa_uo_code = $safa_uo_row->code;
					$safa_uo_row = $this->trip_internaltrip_model->get_max_serial_by_uo($item->safa_uo_id);
					$safa_uo_serial = $safa_uo_row->max_serial;

					$link_safa_trip_internaltrips = "<a href='" . site_url('safa_trip_internaltrips/manage/' . $id) . "'  target='_blank'> " . $safa_uo_code . '-' . $safa_uo_serial . " </a>";

					$tags = "#*safa_trip_internaltrip_id#*=" . $link_safa_trip_internaltrips;
				}

				$this->notification_model->tags = $tags;
				$this->notification_model->parent_id = '';

				$erp_notification_id = $this->notification_model->save();

				//-------- Notification Details ---------------
				$this->notification_model->detail_erp_notification_id = $erp_notification_id;

				$this->notification_model->detail_receiver_type = 2;

				if (count($item) > 0) {

					$this->notification_model->detail_receiver_id = $item->safa_uo_id;
					$this->notification_model->saveDetails();
				}
				//----------------------------------------------
				//-------------------------------------------------------------------


				$this->trip_internaltrip_model->safa_trip_internaltrip_id = $id;
				$this->trip_internaltrip_model->uo_email_sending_datetime = $msg_datetime;
				$this->trip_internaltrip_model->save();


				$this->load->view('message_popup', array('msg' => lang('send_email_to_uo_message'),
                    'url' => site_url("safa_trip_internaltrip_id/send_email_to_uo_popup"),
                    'id' => $this->uri->segment("4"),
                    'model_title' => lang('safa_trip_internaltrips'), 'action' => lang('send_to_uo')));
			}
		}
	}

	function send_email_to_ito_popup($id) {
		$this->layout = 'js_new';

		$this->trip_internaltrip_model->safa_trip_internaltrip_id = $id;
		$item = $this->trip_internaltrip_model->get();
		$serial = $item->serial;

		if ($item->safa_ito_id != 0 && $item->safa_ito_id != '') {

			$safa_ea_name = '';
			$safa_ea_email = '';
			if (session('ea_id')) {
				$this->eas_model->safa_ea_id = session('ea_id');
				$safa_eas_row = $this->eas_model->get();

				$safa_ea_name = session('ea_name');
				$safa_ea_email = $safa_eas_row->email;
			}

			$going_datetime = '';
			$return_datetime = '';
			$this->internalpassages_model->safa_trip_internaltrip_id = $id;
			$this->internalpassages_model->order_by = array("safa_internalsegments.start_datetime", 'ASC');
			$internalpassages = $this->internalpassages_model->get();
			if (isset($internalpassages)) {
				foreach ($internalpassages as $inernalpassage) {
					if ($inernalpassage->safa_internalsegmenttype_id == 1) {
						$going_datetime = $inernalpassage->start_datetime;
					} else if ($inernalpassage->safa_internalsegmenttype_id == 2) {
						$return_datetime = $inernalpassage->start_datetime;
					}
				}
			}

			$safa_uo_id = 0;
			$safa_uo_contract_name = '';
			if ($item->safa_uo_id == '') {
				$this->contracts_model->safa_uo_contract_id = $item->safa_uo_contract_id;
				$contracts_row = $this->contracts_model->get();
				if (count($contracts_row) > 0) {
					$safa_uo_id = $contracts_row->safa_uo_id;
					$safa_uo_contract_name = $contracts_row->{name()};
				}
			} else {
				$safa_uo_id = $item->safa_uo_id;
			}


			$this->safa_uos_model->safa_uo_id = $safa_uo_id;
			$safa_uos_row = $this->safa_uos_model->get();
			$safa_uo_email = $safa_uos_row->email;
			$safa_uo_code = $safa_uos_row->code;


			$this->itos_model->safa_ito_id = $item->safa_ito_id;
			$safa_itos_row = $this->itos_model->get();
			$safa_ito_email = $safa_itos_row->email;
			$safa_ito_name = $safa_itos_row->{name()};


			$this->load->library('email');

			//$email_subject = $safa_ea_name.' | '.lang('safa_trip_internaltrip_email_subject'). ' | '.$going_datetime;
			//$email_message = lang('safa_trip_internaltrip_email_message');


			$going_datetime = date('Y/m/d H:i', strtotime($going_datetime));
			$email_subject = "$safa_ea_name - بيان وصول رحلة رقم $safa_uo_code - $serial   بتاريخ $going_datetime";

			$email_message = 'السادة : شركة ' . $safa_ito_name . ' المحترمين<br/>
			يرجى الاطلاع على بيان الوصول رقم ' . $safa_uo_code . '-' . $serial . '  المرفق<br/>
			 <br/>
			' . $safa_uo_contract_name . '<br/>
			(' . session('ea_user_name') . ')<br/>
			للاطلاع على بيان الوصول من خلال نظام صفا اضغط <a href="' . site_url('safa_trip_internaltrips/manage/' . $id) . '" >هنا</a><br/>
			';


			//$this->email->from(config('webmaster_email'), config('title'));
			$this->email->from('notifications@safalive.com', $safa_ea_name);

			$this->email->to($safa_ito_email);
			if (session('ea_id')) {
				$this->email->reply_to($safa_ea_email);

				if ($item->safa_uo_active == 1) {
					$this->email->cc($safa_uo_email);
				}
			} else if (session('uo_id')) {
				$this->email->reply_to($safa_uo_email);
			}

			$this->email->bcc('notifications@safalive.com');

			$this->internalsegments_report($id, 'pdf');
			$this->email->attach("./static/uploads/safa_trip_internaltrips_pdf/internalsegments_$id.pdf");

			$this->email->subject($email_subject);
			$this->email->message($email_message);
			$sending_email_result = $this->email->send();

			if ($sending_email_result) {

				unlink("./static/uploads/safa_trip_internaltrips_pdf/internalsegments_$id.pdf");

				//----------- Send Notification For Company ------------------------------
				$msg_datetime = date('Y-m-d H:i', time());

				$this->notification_model->notification_type = 'automatic';
				$this->notification_model->erp_importance_id = 1;
				$this->notification_model->sender_type_id = 1;
				$this->notification_model->sender_id = 0;

				$this->notification_model->erp_system_events_id = 26;

				$this->notification_model->language_id = 2;
				$this->notification_model->msg_datetime = $msg_datetime;

				$tags = '';
				$safa_uo_code = '';
				$safa_uo_serial = '';
				if (count($item) > 0) {

					//Link to replace.

					$this->safa_uos_model->safa_uo_id = $safa_uo_id;
					$safa_uo_row = $this->safa_uos_model->get();
					$safa_uo_code = $safa_uo_row->code;
					$safa_uo_row = $this->trip_internaltrip_model->get_max_serial_by_uo($safa_uo_id);
					$safa_uo_serial = $safa_uo_row->max_serial;

					$link_safa_trip_internaltrips = "<a href='" . site_url('safa_trip_internaltrips/manage/' . $id) . "'  target='_blank'> " . $safa_uo_code . '-' . $safa_uo_serial . " </a>";

					$tags = "#*safa_trip_internaltrip_id#*=" . $link_safa_trip_internaltrips;
				}

				$this->notification_model->tags = $tags;
				$this->notification_model->parent_id = '';

				$erp_notification_id = $this->notification_model->save();

				//-------- Notification Details ---------------
				$this->notification_model->detail_erp_notification_id = $erp_notification_id;
				$this->notification_model->detail_receiver_type = 2;

				if (count($item) > 0) {

					$this->notification_model->detail_receiver_id = $safa_uo_id;
					$this->notification_model->saveDetails();
				}
				//----------------------------------------------
				//-------- ITO Notification Details ---------------
				$link_safa_trip_internaltrips = "<a href='" . site_url('ito/trip_internaltrip/edit_incoming/' . $id) . "'  target='_blank'> " . $safa_uo_code . '-' . $safa_uo_serial . " </a>";
				$tags = "#*safa_trip_internaltrip_id#*=" . $link_safa_trip_internaltrips;
				$this->notification_model->tags = $tags;
				$this->notification_model->parent_id = '';
				$erp_notification_id = $this->notification_model->save();
				$this->notification_model->detail_erp_notification_id = $erp_notification_id;
				$this->notification_model->detail_receiver_type = 5;
				if (count($item) > 0) {
					$this->notification_model->detail_receiver_id = $item->safa_ito_id;
					$this->notification_model->saveDetails();
				}
				//-------------------------------------------------------------------



				$this->load->view('redirect_message_popup', array('msg' => lang('send_email_to_uo_message'),
                    'url' => site_url("safa_trip_internaltrip_id/send_email_to_uo_popup"),
                    'id' => $this->uri->segment("4"),
                    'model_title' => lang('safa_trip_internaltrips'), 'action' => lang('send_to_uo')));
			}
		}
	}

	function send_email_by_ito_to_ea_uo_popup($id)
	{
		$this->layout = 'js_new';

		$data['safa_trip_internaltrip_id'] = $id;

		$this->trip_internaltrip_model->safa_trip_internaltrip_id = $id;
		$item = $this->trip_internaltrip_model->get();
		$serial = $item->serial;




		$going_datetime = '';
		$return_datetime = '';
		$this->internalpassages_model->safa_trip_internaltrip_id = $id;
		$this->internalpassages_model->order_by = array("safa_internalsegments.start_datetime", 'ASC');
		$internalpassages = $this->internalpassages_model->get();
		if (isset($internalpassages)) {
			foreach ($internalpassages as $inernalpassage) {
				if ($inernalpassage->safa_internalsegmenttype_id == 1) {
					$going_datetime = $inernalpassage->start_datetime;
				} else if ($inernalpassage->safa_internalsegmenttype_id == 2) {
					$return_datetime = $inernalpassage->start_datetime;
				}
			}
		}

		$safa_uo_id = 0;
		$safa_uo_code='';
		$safa_uo_name='';
		$safa_uo_email='';
		$safa_uo_contract_name = '';

		$safa_ea_id = 0;
		$safa_ea_name = '';
		$safa_ea_email = '';

		$this->contracts_model->safa_uo_contract_id = $item->safa_uo_contract_id;
		$contracts_rows = $this->contracts_model->search();
		if (count($contracts_rows) > 0) {
			$safa_uo_id = $contracts_rows[0]->safa_uo_id;
			$safa_uo_contract_name = $contracts_rows[0]->{name()};
			$safa_ea_id = $contracts_rows[0]->safa_ea_id;
		}



		$this->eas_model->safa_ea_id = $safa_ea_id;
		$this->eas_model->join = true;
		$safa_eas_row = $this->eas_model->get();

		if(count($safa_eas_row)>0) {
			if(name()=='name_ar') {
				$safa_ea_name = $safa_eas_row->arabic_name;
			} else {
				$safa_ea_name = $safa_eas_row->english_name;
			}
			$safa_ea_email = $safa_eas_row->email;
		}

		$this->safa_uos_model->safa_uo_id = $safa_uo_id;
		$safa_uos_row = $this->safa_uos_model->get();
		if(count($safa_uos_row)>0) {
			$safa_uo_email = $safa_uos_row->email;
			$safa_uo_code = $safa_uos_row->code;
			$safa_uo_name = $safa_uos_row->{name()};
		}

		$this->itos_model->safa_ito_id = $item->safa_ito_id;
		$safa_itos_row = $this->itos_model->get();
		$safa_ito_email = $safa_itos_row->email;
		$safa_ito_name = $safa_itos_row->{name()};


		$this->load->library('email');

		//$email_subject = $safa_ea_name.' | '.lang('safa_trip_internaltrip_email_subject'). ' | '.$going_datetime;
		//$email_message = lang('safa_trip_internaltrip_email_message');


		$going_datetime = date('Y/m/d H:i', strtotime($going_datetime));
		$email_subject = "$safa_ea_name - تأكيد الحجز رقم $safa_uo_code - $serial   بتاريخ $going_datetime";


		if ($item->safa_uo_active == 1 || isset($_POST['smt_send'])) {

			if(isset($_POST['smt_send'])) {
				$safa_uo_email = $_POST['uo_email'];
			}
				
			if($item->safa_internaltripstatus_id==4) {
				$email_message = 'السادة : شركة ' . $safa_uo_name . ' المحترمين<br/>
			يرجى الاطلاع على بيان الوصول رقم ' . $safa_uo_code . '-' . $serial . '  المرفق<br/>
			 <br/>
			' . $safa_uo_contract_name . '<br/>
			(' . session('ito_user_name') . ')<br/>
			للاطلاع على بيان الوصول من خلال نظام صفا اضغط <a href="' . site_url('safa_trip_internaltrips/manage/' . $id) . '" >هنا</a><br/>
			';


				//$this->email->from(config('webmaster_email'), config('title'));
				$this->email->from('notifications@safalive.com', $safa_ito_name);

				$this->email->to($safa_uo_email);
				$this->email->reply_to($safa_ito_email);


				$this->email->bcc('notifications@safalive.com');
			} else {

				$email_message = 'السادة : شركة ' . $safa_ea_name . ' المحترمين<br/>
			يرجى الاطلاع على بيان الوصول رقم ' . $safa_uo_code . '-' . $serial . '  المرفق<br/>
			 <br/>
			' . $safa_uo_contract_name . '<br/>
			(' . session('ito_user_name') . ')<br/>
			للاطلاع على بيان الوصول من خلال نظام صفا اضغط <a href="' . site_url('safa_trip_internaltrips/manage/' . $id) . '" >هنا</a><br/>
			';


				//$this->email->from(config('webmaster_email'), config('title'));
				$this->email->from('notifications@safalive.com', $safa_ito_name);

				$this->email->to($safa_uo_email);
				$this->email->reply_to($safa_ito_email);

				if ($item->safa_uo_active == 1) {
					$this->email->cc($safa_uo_email);
				}

				$this->email->bcc('notifications@safalive.com');
			}


			$this->confirmation_pdf($id);
			$this->email->attach("./static/uploads/safa_trip_internaltrips_pdf/confirmation_pdf_$id.pdf");

			$this->email->subject($email_subject);
			$this->email->message($email_message);
			$sending_email_result = $this->email->send();

			if ($sending_email_result) {

				unlink("./static/uploads/safa_trip_internaltrips_pdf/confirmation_pdf_$id.pdf");

				//----------- Send Notification For Company ------------------------------
				$msg_datetime = date('Y-m-d H:i', time());

				$this->notification_model->notification_type = 'automatic';
				$this->notification_model->erp_importance_id = 1;
				$this->notification_model->sender_type_id = 1;
				$this->notification_model->sender_id = 0;

				$this->notification_model->erp_system_events_id = 27;

				$this->notification_model->language_id = 2;
				$this->notification_model->msg_datetime = $msg_datetime;

				$tags = '';
				$safa_uo_code = '';
				$safa_uo_serial = '';
				if (count($item) > 0) {

					//Link to replace.

					$this->safa_uos_model->safa_uo_id = $safa_uo_id;
					$safa_uo_row = $this->safa_uos_model->get();
					$safa_uo_code = $safa_uo_row->code;
					$safa_uo_row = $this->trip_internaltrip_model->get_max_serial_by_uo($safa_uo_id);
					$safa_uo_serial = $safa_uo_row->max_serial;

					$link_safa_trip_internaltrips = "<a href='" . site_url('safa_trip_internaltrips/manage/' . $id) . "'  target='_blank'> " . $safa_uo_code . '-' . $safa_uo_serial . " </a>";

					$tags = "#*safa_trip_internaltrip_id#*=" . $link_safa_trip_internaltrips;
				}

				$this->notification_model->tags = $tags;
				$this->notification_model->parent_id = '';

				$erp_notification_id = $this->notification_model->save();

				//-------- Notification Details ---------------
				$this->notification_model->detail_erp_notification_id = $erp_notification_id;

				if (count($item) > 0) {

					if($item->safa_internaltripstatus_id==4) {
						$this->notification_model->detail_receiver_type = 2;
						$this->notification_model->detail_receiver_id = $safa_uo_id;
						$this->notification_model->saveDetails();
					} else {
						$this->notification_model->detail_receiver_type = 2;
						$this->notification_model->detail_receiver_id = $safa_uo_id;
						$this->notification_model->saveDetails();

						$this->notification_model->detail_receiver_type = 3;
						$this->notification_model->detail_receiver_id = $safa_ea_id;
						$this->notification_model->saveDetails();

					}
				}
				//----------------------------------------------

				$this->load->view('redirect_message_popup', array('msg' => lang('send_trip_internaltrip_email_by_ito_message'),
                    'url' => site_url("safa_trip_internaltrip_id/send_email_by_ito_to_ea_uo_popup"),
                    'id' => $this->uri->segment("4"),
                    'model_title' => lang('safa_trip_internaltrips'), 'action' => lang('send_trip_internaltrip_email_by_ito')));
			}
		} else {
			$this->load->view("safa_trip_internaltrips/send_email_by_ito_to_ea_uo_popup", $data);
		}
	}

	function pdf_export($id, $format = 'F') {
		$data = array();
		if (!$id) {
			show_404();
		} else {

			$this->trip_internaltrip_model->safa_trip_internaltrip_id = $id;
			$item = $this->trip_internaltrip_model->get();
			$data['item'] = $item;

			if ($item->erp_transportertype_id == 2) {
				//------------------------ Going Flights ----------------------------------------
				$this->flight_availabilities_model->erp_path_type_id = 2;
				$this->flight_availabilities_model->erp_flight_availability_id = $item->erp_flight_availability_id;
				$going_trip_flights_rows = $this->flight_availabilities_model->get_details_for_trip();
				$data['going_trip_flights_rows'] = $going_trip_flights_rows;

				$this->flight_availabilities_model->erp_path_type_id = 1;
				$this->flight_availabilities_model->erp_flight_availability_id = $item->erp_flight_availability_id;
				$going_trip_flights_rows = $this->flight_availabilities_model->get_details_for_trip();
				$data['return_trip_flights_rows'] = $going_trip_flights_rows;


				//-------------------------------------------------------------------------------
			} else if ($item->erp_transportertype_id == 3) {

				//------------------------ Cruises ----------------------------------------
				$this->erp_cruise_availabilities_detail_model->erp_path_type_id = 2;
				$this->erp_cruise_availabilities_detail_model->erp_cruise_availability_id = $item->erp_cruise_availability_id;
				$going_trip_cruises_rows = $this->erp_cruise_availabilities_detail_model->get();
				$data['going_cruise_availabilities_details_rows'] = $going_trip_cruises_rows;

				$this->erp_cruise_availabilities_detail_model->erp_path_type_id = 1;
				$this->erp_cruise_availabilities_detail_model->erp_cruise_availability_id = $item->erp_cruise_availability_id;
				$going_trip_cruises_rows = $this->erp_cruise_availabilities_detail_model->get();
				$data['return_cruise_availabilities_details_rows'] = $going_trip_cruises_rows;

				//-------------------------------------------------------------------------------
			}

			/* --------- get the internal passages ----------------------------------------------------- */
			$this->internalpassages_model->safa_trip_internaltrip_id = $id;
			$this->internalpassages_model->order_by = array("safa_internalsegments.start_datetime", 'ASC');
			$data["internalpassages"] = $internalpassages_rows = $this->internalpassages_model->get();

			$data["arriving_segment_bus_brands"] = array();
			foreach ($internalpassages_rows as $internalpassages_row) {
				if ($internalpassages_row->safa_internalsegmenttype_id == 1) {
					$data["arriving_segment_bus_brands"] = $this->internalsegments_drivers_and_buses_model->get_distinct_bus_brands_by_safa_internalsegment($internalpassages_row->safa_internalsegment_id);
				}
			}

			$data['erp_hotelroomsizes'] = $this->hotelroomsizes_model->get();

			$this->internalpassages_model->safa_trip_internaltrip_id = $id;
			$this->internalpassages_model->order_by = array("safa_internalsegment_id", 'ASC');
			$data["internalpassages_hotels"] = $this->internalpassages_model->get_hotels();

			$this->internalpassages_model->safa_trip_internaltrip_id = $id;
			$this->internalpassages_model->order_by = array("safa_internalsegment_id", 'ASC');
			$data["internalpassages_tourismplaces"] = $this->internalpassages_model->get_tourismplaces();

			/* ----------------------------------------------------------------------------------------- */



			//---------- Details --------------------------
			//			$this->safa_package_execlusive_meals_model->safa_package_id = $id;
			//			$item_execlusive_meals_prices = $this->safa_package_execlusive_meals_model->get();
			//			$data['item_execlusive_meals_prices'] = $item_execlusive_meals_prices;
			//-----------------------------------------------


			$this->db->from('safa_trip_internaltrips_hotels');
			$this->db->where('safa_trip_internaltrip_id', $id);
			$data['safa_trip_internaltrips_hotels_rows'] = $this->db->get()->result();


			//-------------- Safa Group Passports -------------------
			$this->safa_group_passports_model->safa_trip_internaltrip_id = $id;
			$data['safa_group_passports'] = $this->safa_group_passports_model->get_for_table();
			//-------------------------------------------------------
			//--------------- Drivers And Buses -------------------------------------------------------
			//$this->internalsegments_drivers_and_buses_model->safa_internalsegments_id = $data["internalpassages"][0]->safa_internalsegment_id;
			//$item_drivers_and_buses = $this->internalsegments_drivers_and_buses_model->get();
			$item_drivers_and_buses = $this->internalsegments_drivers_and_buses_model->get_distinct_by_safa_trip_internaltrip($id);
			$data['item_drivers_and_buses'] = $item_drivers_and_buses;
			//-----------------------------------------------------------------------------------------
		}


		$data['uo_dir'] = '';
		if (session('uo_id')) {
			$data['uo_dir'] = session('uo_id');
		} else if (session('ea_id')) {
			$this->contracts_model->safa_uo_contract_id = $item->safa_uo_contract_id;
			$contracts_row = $this->contracts_model->get();
			if (count($contracts_row) > 0) {
				$this->safa_uos_model->safa_uo_id = $contracts_row->safa_uo_id;
				$data['uo_dir'] = $contracts_row->safa_uo_id;
			}
		}

		$this->load->helper('pdf_helper');
		$pdf_orientation = 'P';
		$pdf_unit = 'px';
		$pdf_pageformat = 'A4';
		$pdf_unicode = true;
		$pdf_encoding = 'UTF-8';

		$pdf = new virgo_pdf($pdf_orientation, $pdf_unit, $pdf_pageformat, $pdf_unicode, $pdf_encoding, false);
		$tahoma = $pdf->addTTFfont('static/fonts/tradbdo.ttf', 'TrueTypeUnicode', '', 96);
		$pdf->SetFont($tahoma, '', 10, '', false);
		$pdf->SetCreator('Safa Live');
		$pdf->SetAuthor('Safa Live');
		$pdf->SetTitle('Safa Live Reports');
		$pdf->SetSubject('Safa Live');

		$pdf->setPrintHeader(false);

		// set default monospaced font
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

		// set margins
		$pdf->SetMargins(20, 20, 40);
		$pdf->SetHeaderMargin(20);
		$pdf->SetFooterMargin(0);

		// remove default footer
		$pdf->setPrintFooter(false);

		// set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

		// set image scale factor
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

		$pdf->setRTL(true);
		$pdf->AddPage();


		$html = $this->load->view("safa_trip_internaltrips/pdf", $data, true);

		$pdf->writeHTML($html, true, false, true, false, '');

		$pdf->lastPage();
		ob_end_clean();

		$this->load->helper('download');

		if ($format == 'D') {
			$pdf->Output("safa_trip_internaltrips_$id.pdf", $format);
		}
		$pdf->Output("./static/uploads/safa_trip_internaltrips_pdf/safa_trip_internaltrips_$id.pdf", $format);

		//$pdf->Output("./static/uploads/safa_trip_internaltrips_pdf/safa_trip_internaltrips_$id.pdf", 'I');
	}

	function confirmation_pdf($id, $format = 'F') {
		$data = array();
		if (!$id) {
			show_404();
		} else {

			$this->trip_internaltrip_model->safa_trip_internaltrip_id = $id;
			$item = $this->trip_internaltrip_model->get();
			$data['item'] = $item;

			if ($item->erp_transportertype_id == 2) {
				//------------------------ Going Flights ----------------------------------------
				$this->flight_availabilities_model->erp_path_type_id = 2;
				$this->flight_availabilities_model->erp_flight_availability_id = $item->erp_flight_availability_id;
				$going_trip_flights_rows = $this->flight_availabilities_model->get_details_for_trip();
				$data['going_trip_flights_rows'] = $going_trip_flights_rows;

				$this->flight_availabilities_model->erp_path_type_id = 1;
				$this->flight_availabilities_model->erp_flight_availability_id = $item->erp_flight_availability_id;
				$going_trip_flights_rows = $this->flight_availabilities_model->get_details_for_trip();
				$data['return_trip_flights_rows'] = $going_trip_flights_rows;


				//-------------------------------------------------------------------------------
			} else if ($item->erp_transportertype_id == 3) {

				//------------------------ Cruises ----------------------------------------
				$this->erp_cruise_availabilities_detail_model->erp_path_type_id = 2;
				$this->erp_cruise_availabilities_detail_model->erp_cruise_availability_id = $item->erp_cruise_availability_id;
				$going_trip_cruises_rows = $this->erp_cruise_availabilities_detail_model->get();
				$data['going_cruise_availabilities_details_rows'] = $going_trip_cruises_rows;

				$this->erp_cruise_availabilities_detail_model->erp_path_type_id = 1;
				$this->erp_cruise_availabilities_detail_model->erp_cruise_availability_id = $item->erp_cruise_availability_id;
				$going_trip_cruises_rows = $this->erp_cruise_availabilities_detail_model->get();
				$data['return_cruise_availabilities_details_rows'] = $going_trip_cruises_rows;

				//-------------------------------------------------------------------------------
			}

			/* --------- get the internal passages ----------------------------------------------------- */
			$this->internalpassages_model->safa_trip_internaltrip_id = $id;
			$this->internalpassages_model->order_by = array("safa_internalsegments.start_datetime", 'ASC');
			$data["internalpassages"] = $internalpassages_rows = $this->internalpassages_model->get();

			$data["arriving_segment_bus_brands"] = array();
			foreach ($internalpassages_rows as $internalpassages_row) {
				if ($internalpassages_row->safa_internalsegmenttype_id == 1) {
					$data["arriving_segment_bus_brands"] = $this->internalsegments_drivers_and_buses_model->get_distinct_bus_brands_by_safa_internalsegment($internalpassages_row->safa_internalsegment_id);
				}
			}

			$data['erp_hotelroomsizes'] = $this->hotelroomsizes_model->get();

			$this->internalpassages_model->safa_trip_internaltrip_id = $id;
			$this->internalpassages_model->order_by = array("safa_internalsegment_id", 'ASC');
			$data["internalpassages_hotels"] = $this->internalpassages_model->get_hotels();

			$this->internalpassages_model->safa_trip_internaltrip_id = $id;
			$this->internalpassages_model->order_by = array("safa_internalsegment_id", 'ASC');
			$data["internalpassages_tourismplaces"] = $this->internalpassages_model->get_tourismplaces();

			/* ----------------------------------------------------------------------------------------- */



			//---------- Details --------------------------
			//			$this->safa_package_execlusive_meals_model->safa_package_id = $id;
			//			$item_execlusive_meals_prices = $this->safa_package_execlusive_meals_model->get();
			//			$data['item_execlusive_meals_prices'] = $item_execlusive_meals_prices;
			//-----------------------------------------------


			$this->db->from('safa_trip_internaltrips_hotels');
			$this->db->where('safa_trip_internaltrip_id', $id);
			$data['safa_trip_internaltrips_hotels_rows'] = $this->db->get()->result();


			//-------------- Safa Group Passports -------------------
			$this->safa_group_passports_model->safa_trip_internaltrip_id = $id;
			$data['safa_group_passports'] = $this->safa_group_passports_model->get_for_table();
			//-------------------------------------------------------
			//--------------- Drivers And Buses -------------------------------------------------------
			//$this->internalsegments_drivers_and_buses_model->safa_internalsegments_id = $data["internalpassages"][0]->safa_internalsegment_id;
			//$item_drivers_and_buses = $this->internalsegments_drivers_and_buses_model->get();
			$item_drivers_and_buses = $this->internalsegments_drivers_and_buses_model->get_distinct_by_safa_trip_internaltrip($id);
			$data['item_drivers_and_buses'] = $item_drivers_and_buses;
			//-----------------------------------------------------------------------------------------
		}


		$data['uo_dir'] = '';
		if (session('uo_id')) {
			$data['uo_dir'] = session('uo_id');
		} else if (session('ea_id') || session('ito_id')) {
			$this->contracts_model->safa_uo_contract_id = $item->safa_uo_contract_id;
			$contracts_row = $this->contracts_model->get();
			if (count($contracts_row) > 0) {
				$this->safa_uos_model->safa_uo_id = $contracts_row->safa_uo_id;
				$data['uo_dir'] = $contracts_row->safa_uo_id;
			}
		}

		$data['safa_uo_code'] ='';
		$data['safa_uo_name'] = '';
		if (session('uo_id')) {
			$this->safa_uos_model->safa_uo_id = session('uo_id');
			$safa_uo_row = $this->safa_uos_model->get();
			$data['safa_uo_code'] = $safa_uo_row->code;
			$data['safa_uo_name'] = $safa_uo_row->{name()};
		} else if (session('ea_id') || session('ito_id')) {

			$this->contracts_model->safa_uo_contract_id =$item->safa_uo_contract_id;
			$contracts_row = $this->contracts_model->get();
			if (count($contracts_row) > 0) {

				$this->safa_uos_model->safa_uo_id = $contracts_row->safa_uo_id;
				$safa_uo_row = $this->safa_uos_model->get();
				if (count($safa_uo_row) > 0) {
					$data['safa_uo_code']  = $safa_uo_row->code;
					$data['safa_uo_name'] = $safa_uo_row->{name()};
				}

			}
		}

		$data['ito_dir'] =$item->safa_ito_id;
		$data['transporter_dir'] =  $item->safa_transporter_id;

		if($item->safa_ito_id=='') {
			$data['ito_dir'] = '0';
		}
		if($item->safa_transporter_id=='') {
			$data['transporter_dir'] ='0';
		}

		$this->load->helper('pdf_helper');
		$pdf_orientation = 'P';
		$pdf_unit = 'px';
		$pdf_pageformat = 'A4';
		$pdf_unicode = true;
		$pdf_encoding = 'UTF-8';

		$pdf = new virgo_pdf($pdf_orientation, $pdf_unit, $pdf_pageformat, $pdf_unicode, $pdf_encoding, false);
		$tahoma = $pdf->addTTFfont('static/fonts/tradbdo.ttf', 'TrueTypeUnicode', '', 96);
		$pdf->SetFont($tahoma, '', 10, '', false);
		$pdf->SetCreator('Safa Live');
		$pdf->SetAuthor('Safa Live');
		$pdf->SetTitle('Safa Live Reports');
		$pdf->SetSubject('Safa Live');

		$pdf->setPrintHeader(false);

		// set default monospaced font
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

		// set margins
		$pdf->SetMargins(20, 20, 40);
		$pdf->SetHeaderMargin(20);
		$pdf->SetFooterMargin(0);

		// remove default footer
		$pdf->setPrintFooter(false);

		// set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

		// set image scale factor
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

		$pdf->setRTL(true);
		$pdf->AddPage();

		if($format == 'html')
		{
			$this->load->view("safa_trip_internaltrips/confirmation_pdf", $data);
		} else {
			$html = $this->load->view("safa_trip_internaltrips/confirmation_pdf", $data, true);

			$pdf->writeHTML($html, true, false, true, false, '');

			$pdf->lastPage();
			ob_end_clean();

			$this->load->helper('download');

			if ($format == 'D') {
				$pdf->Output("confirmation_pdf_$id.pdf", $format);
			}
			$pdf->Output("./static/uploads/safa_trip_internaltrips_pdf/confirmation_pdf_$id.pdf", $format);

			//$pdf->Output("./static/uploads/safa_trip_internaltrips_pdf/safa_trip_internaltrips_$id.pdf", 'I');
		}
	}

	function view($id)
	{
		$data = array();
		if (!$id) {
			show_404();
		} else {


			$this->trip_internaltrip_model->safa_trip_internaltrip_id = $id;
			$item = $this->trip_internaltrip_model->get();
			$data['item'] = $item;


			if (session('uo_id')) {
				$this->safa_uos_model->safa_uo_id = session('uo_id');
				$safa_uo_row = $this->safa_uos_model->get();
				$data['safa_uo_code'] = $safa_uo_row->code;
			} else if (session('ea_id') || session('ito_id')) {

				$this->contracts_model->safa_uo_contract_id =$item->safa_uo_contract_id;
				$contracts_row = $this->contracts_model->get();
				if (count($contracts_row) > 0) {

					$this->safa_uos_model->safa_uo_id = $contracts_row->safa_uo_id;
					$safa_uo_row = $this->safa_uos_model->get();
					if (count($safa_uo_row) > 0) {
						$data['safa_uo_code']  = $safa_uo_row->code;
					}

				}
			} else {
                            $this->contracts_model->safa_uo_contract_id =$item->safa_uo_contract_id;
				$contracts_row = $this->contracts_model->get();
				if (count($contracts_row) > 0) {

					$this->safa_uos_model->safa_uo_id = $contracts_row->safa_uo_id;
					$safa_uo_row = $this->safa_uos_model->get();
					if (count($safa_uo_row) > 0) {
						$data['safa_uo_code']  = $safa_uo_row->code;
					}

				}
                        }

			if ($item->erp_transportertype_id == 2) {

				//------------------------ Flights ----------------------------------------
				//$this->flight_availabilities_model->erp_path_type_id=1;
				$this->flight_availabilities_model->erp_flight_availability_id = $item->erp_flight_availability_id;
				$going_trip_flights_rows = $this->flight_availabilities_model->get_for_trip();
				$data['going_trip_flights_rows'] = $going_trip_flights_rows;
				$data['going_trip_flights_rows_count'] = count($going_trip_flights_rows);

				$arr_going_trip_flights = array();
				foreach ($going_trip_flights_rows as $going_trip_flights_row) {
					$arr_going_trip_flights[$going_trip_flights_row->erp_flight_availability_id] = $going_trip_flights_row->erp_flight_availability_id;
				}
				//session('going_trip_flights_session', $arr_going_trip_flights);
				$_SESSION['going_trip_flights_session'] = $arr_going_trip_flights;

				//-------------------------------------------------------------------------------

			} else if ($item->erp_transportertype_id == 3) {

				//------------------------ Cruises ----------------------------------------
				$this->erp_cruise_availabilities_detail_model->erp_cruise_availability_id = $item->erp_cruise_availability_id;
				$going_trip_cruises_rows = $this->erp_cruise_availabilities_detail_model->get();
				$data['cruise_availabilities_details_rows'] = $going_trip_cruises_rows;
				$data['cruise_availabilities_details_rows_count'] = count($going_trip_cruises_rows);

				$arr_going_trip_cruises = array();
				foreach ($going_trip_cruises_rows as $going_trip_cruises_row) {
					$arr_going_trip_cruises[$going_trip_cruises_row->erp_cruise_availability_id] = $going_trip_cruises_row->erp_cruise_availability_id;
				}
				$_SESSION['cruise_availabilities_details_session'] = $arr_going_trip_cruises;
				//-------------------------------------------------------------------------------

			}

			/* --------- get the internal passages ----------------------------------------------------- */
			$this->internalpassages_model->safa_trip_internaltrip_id = $id;
			$this->internalpassages_model->order_by = array("safa_internalsegments.start_datetime", 'ASC');
			$data["internalpassages"] = $internalpassages_rows = $this->internalpassages_model->get();

			$data["arriving_segment_bus_brands"] = array();
			foreach ($internalpassages_rows as $internalpassages_row) {
				if ($internalpassages_row->safa_internalsegmenttype_id == 1) {
					$data["arriving_segment_bus_brands"] = $this->internalsegments_drivers_and_buses_model->get_distinct_bus_brands_by_safa_internalsegment($internalpassages_row->safa_internalsegment_id);
				}
			}

			$data['erp_hotelroomsizes'] = $this->hotelroomsizes_model->get();

			$this->internalpassages_model->safa_trip_internaltrip_id = $id;
			$this->internalpassages_model->order_by = array("safa_internalsegment_id", 'ASC');
			$data["internalpassages_hotels"] = $this->internalpassages_model->get_hotels();

			$this->internalpassages_model->safa_trip_internaltrip_id = $id;
			$this->internalpassages_model->order_by = array("safa_internalsegment_id", 'ASC');
			$data["internalpassages_tourismplaces"] = $this->internalpassages_model->get_tourismplaces();

			/* ----------------------------------------------------------------------------------------- */



			//---------- Details --------------------------
			//			$this->safa_package_execlusive_meals_model->safa_package_id = $id;
			//			$item_execlusive_meals_prices = $this->safa_package_execlusive_meals_model->get();
			//			$data['item_execlusive_meals_prices'] = $item_execlusive_meals_prices;
			//-----------------------------------------------


			$this->db->from('safa_trip_internaltrips_hotels');
			$this->db->where('safa_trip_internaltrip_id', $id);
			$data['safa_trip_internaltrips_hotels_rows'] = $this->db->get()->result();


			//-------------- Safa Group Passports -------------------
			$this->safa_group_passports_model->safa_trip_internaltrip_id = $id;
			$data['safa_group_passports'] = $this->safa_group_passports_model->get_for_table();
			//-------------------------------------------------------
			//--------------- Drivers And Buses -------------------------------------------------------
			//$this->internalsegments_drivers_and_buses_model->safa_internalsegments_id = $data["internalpassages"][0]->safa_internalsegment_id;
			//$item_drivers_and_buses = $this->internalsegments_drivers_and_buses_model->get();
			$item_drivers_and_buses = $this->internalsegments_drivers_and_buses_model->get_distinct_by_safa_trip_internaltrip($id);
			$data['item_drivers_and_buses'] = $item_drivers_and_buses;
			//-----------------------------------------------------------------------------------------

			//--------------- Accomodation Forms -------------------------------------------------------
			$this->safa_reservation_forms_model->safa_trip_id = $item->safa_trip_id;
			$item_safa_reservation_forms_rows = $this->safa_reservation_forms_model->get();
			$data['item_safa_reservation_forms_rows'] = $item_safa_reservation_forms_rows;
			//-----------------------------------------------------------------------------------------
		}


		$data['uo_dir'] = '';
		if (session('uo_id')) {
			$data['uo_dir'] = session('uo_id');
		} else if (session('ea_id')) {
			$this->contracts_model->safa_uo_contract_id = $item->safa_uo_contract_id;
			$contracts_row = $this->contracts_model->get();
			if (count($contracts_row) > 0) {
				$this->safa_uos_model->safa_uo_id = $contracts_row->safa_uo_id;
				$data['uo_dir'] = $contracts_row->safa_uo_id;
			}
		} else {
                    $this->contracts_model->safa_uo_contract_id = $item->safa_uo_contract_id;
			$contracts_row = $this->contracts_model->get();
			if (count($contracts_row) > 0) {
				$this->safa_uos_model->safa_uo_id = $contracts_row->safa_uo_id;
				$data['uo_dir'] = $contracts_row->safa_uo_id;
			}
                }

		$this->load->view("safa_trip_internaltrips/view", $data);
	}

	function get_safa_uo_contracts_by_uo_ajax($safa_uo_id=0) 
	{
		$this->contracts_model->safa_uo_id = $safa_uo_id;
		$contracts_rows = $this->contracts_model->get();
		$contracts_arr = array();
		$contracts_arr[""] = lang('global_select_from_menu');
		foreach ($contracts_rows as $contracts_row) {
			$contracts_arr[$contracts_row->safa_uo_contract_id] = $contracts_row->{name()};
		}

		$contracts_options = '';
		foreach ($contracts_arr as $key => $value)
		$contracts_options .= "<option value='$key'>$value</option>\n";
		
		$data_arr[] = array('contracts_options' => $contracts_options,
		);
		
		echo json_encode($data_arr);
		exit;
	}
	
	function get_counts_ajax() {
		$this->layout = 'ajax';

		$all = $this->trip_internaltrip_model->get(true);

		$this->trip_internaltrip_model->where_condition = "(safa_trip_internaltrips.safa_internaltripstatus_id=3 OR safa_trip_internaltrips.safa_internaltripstatus_id=5)";
		$reserved_from_ea = $this->trip_internaltrip_model->get(true);

		$this->trip_internaltrip_model->where_condition = "(safa_trip_internaltrips.safa_internaltripstatus_id=2 OR safa_trip_internaltrips.safa_internaltripstatus_id=4)";
		$requested_from_ea = $this->trip_internaltrip_model->get(true);

		$this->trip_internaltrip_model->where_condition = "(safa_trip_internaltrips.safa_internaltripstatus_id=3 AND safa_trip_internaltrips.confirmation_number!=0)";
		$with_confirmation_number = $this->trip_internaltrip_model->get(true);

		$counts_arr[] = array(
            'all' => $all,
            'reserved_from_ea' => $reserved_from_ea,
            'requested_from_ea' => $requested_from_ea,
            'with_confirmation_number' => $with_confirmation_number);

		echo json_encode($counts_arr);
	}

}
