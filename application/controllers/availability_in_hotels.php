<?php

class availability_in_hotels extends Safa_Controller {

    public function index() {
        $this->layout = 'new';
        //  $this->lang->load('availability-in-hotels');
        $data['nationalities'] = ddgen('erp_nationalities', array('erp_nationality_id', name()));
        $this->load->view('des_test/availability_in_hotels', $data);
    }

}
