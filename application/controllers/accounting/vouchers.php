<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Vouchers extends Safa_Controller {

    public function __construct() {
        parent::__construct();
        
        $this->layout = 'new';
        
        if (session('uo_id')) {
            //Side menu session, By Gouda.
            session('side_menu_id', 6);
        }
        
        $this->load->library('accounting');
        $this->load->model('entries_master_model');
        $this->load->model('entries_details_model');
        $this->load->model('vouchers_model');
        $this->load->model('vouchers_defs_model');
        $this->lang->load('vouchers');

        permission();
    }

    public function index() {
        $this->vouchers_model->limit = $this->config->item('per_page');
        $this->vouchers_model->offset = $this->uri->segment('4');
        $config['uri_segment'] = 4;
        $config['base_url'] = site_url('accounting/vouchers/index');
        $config['total_rows'] = $this->vouchers_model->get(TRUE);
        $config['per_page'] = $this->config->item('per_page');
        $this->load->library('pagination');
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();
        if (!permission('manage_accounting'))
            $this->vouchers_model->branch_id = session('branch_id');
        $data['items'] = $this->vouchers_model->get();

        $this->load->view('vouchers/index', $data);
    }

    public function add($id = FALSE) {
        if ($id && is_numeric($id)) {
            $this->vouchers_model->id = $id;
            $data['item'] = $this->vouchers_model->get();
            if (!$data['item'])
                show_404();
            $payment_request = $this->vouchers_model->getPaymentRequest();
            if ($payment_request) {
                $data['payment_request'] = $payment_request->no;
            }
            else
                $data['payment_request'] = '';
        }
        elseif ($this->input->get('pay_no')) {
            $this->vouchers_model->payment_request_no = $this->input->get('pay_no');
            $payment_request = $this->vouchers_model->getPaymentRequest();
            if ($payment_request->voucher_id != '0') {
                $data['payment_request'] = '';
            } else {
                $data['payment_request'] = $payment_request->no;
            }
        } else {
            $data['payment_request'] = '';
        }

        $data['cash_account'] = $this->accounting->getSetting('branches', 'id', session('branch_id'), 'cash_account_id');
        if (!$data['cash_account'])
            $data['cash_account'] = $this->accounting->getSetting('firms', 'id', session('firm_id'), 'cash_account');
//        $data['cash_account'] = $this->config->item('cash_account');
        $data['accounts'] = ddgen('accounts', array('id', name()));
        $data['currencies'] = ddgen('erp_currencies', array('erp_currency_id', name()), FALSE, FALSE, TRUE);

        $this->load->library('form_validation');
        $this->form_validation->set_rules('voucher_number', 'lang:voucher_number', 'trim|required|integer');
        $this->form_validation->set_rules('payment_request_number', 'lang:payment_request_number', 'trim|integer|callback_check_payment_request');
        $this->form_validation->set_rules('voucher_defs_id', 'lang:voucher_defs_id', 'trim|required');
        $this->form_validation->set_rules('voucher_date', 'lang:voucher_date', 'trim|required');
        $this->form_validation->set_rules('currency', 'lang:currency', 'trim|required');
        $this->form_validation->set_rules('rate', 'lang:rate', 'trim|required');
        $this->form_validation->set_rules('cash_account', 'lang:cash_account', 'trim|required');
        $this->form_validation->set_rules('account_name', 'lang:account_name', 'trim');
        $this->form_validation->set_rules('account', 'lang:account', 'trim|required|integer|callback_test_account');
        $this->form_validation->set_rules('amount', 'lang:amount', 'trim|required');
        $this->form_validation->set_rules('description', 'lang:description', 'trim');
        $this->form_validation->set_rules('remarks', 'lang:remarks', 'trim');
//        $this->form_validation->set_rules('entry_number', 'lang:entry_number', 'trim|required|integer');

        if ($this->form_validation->run() == FALSE) {
            if ($id)
                $this->load->view('vouchers/edit', $data);
            else
                $this->load->view('vouchers/add', $data);
        } else {
            if ($id)
                $this->delete($id, FALSE);

            $entry_date = $_POST['voucher_date'];
            $entry_currency = $_POST['currency'];
            $entry_rate = $_POST['rate'];

            $this->vouchers_model->voucher_number = $this->input->post('voucher_number');
            $this->vouchers_model->voucher_defs_id = $this->input->post('voucher_defs_id');
            $data_voucher = $this->vouchers_model->get(true);
            if (($data_voucher) == 0) {
                $this->vouchers_model->firm_id = session('firm_id');
                $this->vouchers_model->voucher_date = $this->input->post('voucher_date');
                $this->vouchers_model->currency = $this->input->post('currency');
                $this->vouchers_model->rate = $this->input->post('rate');
                $this->vouchers_model->cash_account = $this->input->post('cash_account');
                $this->vouchers_model->account = $this->input->post('account');
                $this->vouchers_model->amount = $this->input->post('amount');
                $this->vouchers_model->description = $this->input->post('description');
                $this->vouchers_model->remarks = $this->input->post('remarks');
                $this->vouchers_model->entry_number = $this->input->post('entry_number');
                if (!$id) {
                    $this->vouchers_model->branch_id = session('branch_id');
                    $this->vouchers_model->user_id = session('user_id');
                } else {
                    if ($data['item']->branch_id)
                        $this->vouchers_model->branch_id = $data['item']->branch_id;
                    else
                        $this->vouchers_model->branch_id = session('branch_id');

                    if ($data['item']->user_id)
                        $this->vouchers_model->user_id = $data['item']->user_id;
                    else
                        $this->vouchers_model->user_id = session('user_id');

                    if ($data['item']->user_id != session('user_id'))
                        $this->vouchers_model->edit_user_id = session('user_id');
                }
                $voucher_id = $this->vouchers_model->save();

                $this->vouchers_defs_model->id = $this->input->post('voucher_defs_id');
                $data_def = $this->vouchers_defs_model->get();

                /* adding entries */
                if ($data_def->main_type == '1') {
                    $this->accounting->addSimpleEntry($this->vouchers_model->cash_account
                            , $this->vouchers_model->account
                            , $this->vouchers_model->amount
                            , $data_def->id
                            , $voucher_id
                            , $this->vouchers_model->currency
                            , $this->vouchers_model->rate
                            , $this->vouchers_model->description
                            , $this->vouchers_model->remarks
                            , $this->vouchers_model->voucher_date);
                } elseif ($data_def->main_type == '2') {
                    $this->accounting->addSimpleEntry($this->vouchers_model->account
                            , $this->vouchers_model->cash_account
                            , $this->vouchers_model->amount
                            , $data_def->id
                            , $voucher_id
                            , $this->vouchers_model->currency
                            , $this->vouchers_model->rate
                            , $this->vouchers_model->description
                            , $this->vouchers_model->remarks
                            , $this->vouchers_model->voucher_date);
                }
                /* end adding entries */
                // update payment request if set
                if ($data['payment_request'] || $this->input->post('payment_request_number')) {

                    if ($data['payment_request'] && ($data['payment_request'] != $this->input->post('payment_request_number'))) {
                        $this->vouchers_model->payment_request_no = $data['payment_request'];
                        $this->vouchers_model->id = '0';
                        $this->vouchers_model->payment_date = '';
                        $this->vouchers_model->updatePaymentRequest();
                    }
                    $this->vouchers_model->payment_request_no = $this->input->post('payment_request_number');
                    $this->vouchers_model->id = $voucher_id;
                    $this->vouchers_model->payment_date = date('Y-m-d');
                    $this->vouchers_model->updatePaymentRequest();
                }
                // end update payment request if set

                if ($id) {
                    $this->common->report('{report_vouchers_updated} #' . $this->vouchers_model->id);
                    $this->load->view('redirect', array('msg' => lang('global_updated_successfully'), 'url' => site_url('admin/vouchers')));
                } else {
                    $this->common->report('{report_new_vouchers_added} #' . $this->vouchers_model->id);
                    $this->load->view('redirect', array('msg' => lang('global_added_successfully'), 'url' => site_url('admin/vouchers')));
                }
            } else {
                if ($id) {
                    $data['err'] = '<p style="color:red;font-weight:bold">This number used before please change it</p> ';
                    $this->load->view('vouchers/edit', $data);
                } else {
                    $data['err'] = '<p style="color:red;font-weight:bold">This number used before please change it</p> ';
                    $this->load->view('vouchers/add', $data);
                }
            }
        }
    }

    public function print_voucher($id = FALSE) {
        if (!$id || !is_numeric($id))
            show_404();

        $this->vouchers_model->id = $id;
        $data['item'] = $this->vouchers_model->get();
        if (!$data['item'])
            redirect(site_url('accounting/vouchers'));

        $this->vouchers_defs_model->id = $data['item']->voucher_defs_id;
        $data['vouchers_def'] = $this->vouchers_defs_model->get();

        $payment_request = $this->vouchers_model->getPaymentRequest();
        if ($payment_request) {
            $data['payment_request'] = $payment_request;
            $this->load->model('res_model');
            $this->res_model->res_id = $payment_request->res_id;
            $data['res'] = $this->res_model->get();
        }
        else
            $data['payment_request'] = '';

        $this->load->model('currencies_model');
        $this->currencies_model->id = $data['item']->currency;
        $currency = $this->currencies_model->get();

        if ($this->config->item('language') == 'arabic')
            $name = 'short_name_ar';
        else
            $name = 'short_name';

        $data['currency_short'] = $currency->{$name};

        $this->load->view('vouchers/print_voucher', $data);
    }

    public function edit($id = FALSE) {
        $this->add($id);
//        
//        if (!$id)
//            show_404();
//        $this->vouchers_model->id = $id;
//        $data['item'] = $this->vouchers_model->get();
//        if (!$data['item'])
//            show_404();
//        
//        $data['cash_account'] = $this->config->item('cash_account');
//        $data['accounts'] = ddmenu('accounts',FALSE,FALSE,array('kind' => '2'));
//        
//        $this->load->library('form_validation');
//        $this->form_validation->set_rules('voucher_number', 'lang:voucher_number', 'trim|required|integer');
//        $this->form_validation->set_rules('voucher_defs_id', 'lang:voucher_defs_id', 'trim|required');
//        $this->form_validation->set_rules('voucher_date', 'lang:voucher_date', 'trim|required');
//        $this->form_validation->set_rules('currency', 'lang:currency', 'trim|required');
//        $this->form_validation->set_rules('rate', 'lang:rate', 'trim|required');
//        $this->form_validation->set_rules('cash_account', 'lang:cash_account', 'trim|required');
//        $this->form_validation->set_rules('account', 'lang:account', 'trim|required|integer');
//        $this->form_validation->set_rules('amount', 'lang:amount', 'trim|required');
//        $this->form_validation->set_rules('description', 'lang:description', 'trim');
//        $this->form_validation->set_rules('remarks', 'lang:remarks', 'trim');
////        $this->form_validation->set_rules('entry_number', 'lang:entry_number', 'trim|requirinteger');
//        
//        if ($this->form_validation->run() == FALSE) {
//            $this->load->view('admin/vouchers/edit', $data);
//        } else {
//            
//            $this->vouchers_model->voucher_number = $this->input->post('voucher_number');
//            $this->vouchers_model->voucher_defs_id = $this->input->post('voucher_defs_id');
//            $this->vouchers_model->voucher_id = $id;
//            
//            $data1 = $this->vouchers_model->get(true);
//            if (($data1) == 1) {
//                if (!session('firm_id'))
//                    $this->vouchers_model->firm_id = 0;
//                else
//                    $this->vouchers_model->firm_id = session('firm_id');
//                $this->vouchers_model->voucher_date = $this->input->post('voucher_date');
//                $this->vouchers_model->currency = $this->input->post('currency');
//                $this->vouchers_model->rate = $this->input->post('rate');
//                $this->vouchers_model->cash_account = $this->input->post('cash_account');
//                $this->vouchers_model->account = $this->input->post('account');
//                $this->vouchers_model->amount = $this->input->post('amount');
//                $this->vouchers_model->description = $this->input->post('description');
//                $this->vouchers_model->remarks = $this->input->post('remarks');
//                $this->vouchers_model->entry_number = $this->input->post('entry_number');
////                $this->vouchers_model->save();
//                $this->common->report('{report_vouchers_updated} #' . $this->vouchers_model->id);
//                
//                $this->load->view('admin/redirect', array('msg' => lang('global_updated_successfully'), 'url' => site_url('admin/vouchers')));
//            }
//            else {
//                $data['err'] = '<p style="color:red;font-weight:bold">This number used before please change it</p> ';
//                $this->load->view('admin/vouchers/edit', $data);
//            }
//        }
    }

    private function delete($id = FALSE, $redirect = TRUE) {

        if (!$id)
            show_404();

        $this->vouchers_model->id = $id;
        $this->vouchers_model->firm_id = session('firm_id');
        $voutcher = $this->vouchers_model->get();
        if (permission('manage_accounting')  ||
                ($voutcher && $voutcher->branch_id == session('branch_id'))) {
            $payment_request = $this->vouchers_model->getPaymentRequest();
            if ($voutcher) {
                $this->entries_master_model->transaction_entry = '1';
                $this->entries_master_model->transaction_kind = $voutcher->voucher_defs_id;
                $this->entries_master_model->transaction_id = $id;
                $master = $this->entries_master_model->get();

                if ($this->accounting->deleteEntry($master[0]->id, TRUE))
                    $this->vouchers_model->delete();

                if ($payment_request) {
                    $this->vouchers_model->payment_request_no = $payment_request->no;
                    $this->vouchers_model->id = '0';
                    $this->vouchers_model->payment_date = '';
                    $this->vouchers_model->updatePaymentRequest();
                }

                $this->vouchers_model->id = FALSE;
                $this->entries_master_model->id = FALSE;
                $this->entries_details_model->id = FALSE;
                $this->entries_details_model->entry_id = FALSE;
                $this->entries_master_model->transaction_entry = FALSE;
                $this->entries_master_model->transaction_kind = FALSE;
                $this->entries_master_model->transaction_id = FALSE;
            }
        }
        if ($redirect) {
            $this->common->report('{report_vouchers_deleted} #' . $id);
            $this->load->view('admin/redirect', array('msg' => lang('global_deleted_successfully'), 'url' => site_url('admin/vouchers')));
        }
    }

    function ajax($id = FALSE) {
        if ($id) {
            $voucher_number = $this->db->select('max(voucher_number) as max_no')->where('voucher_defs_id', $id)->get('vouchers')->row();
            if ($voucher_number)
                $max_no = $voucher_number->max_no + 1;
            else
                $max_no = 1;
            echo $max_no;
        }
    }

    function get_rate($id) {
        $this->db->where("firm_id", session("firm_id"));
        $this->db->where("currency_id", $id);
        $query = $this->db->get("firm_currencies");
        $currency = $query->row();

        if ($currency)
            echo $currency->rate;
    }

    function test_account($var) {
        $res = $this->db->where('firm_id', session('firm_id'))
                ->where('id', $var)
                ->get('accounts')
                ->num_rows();
        if ($res) {
            return TRUE;
        } else {
            $this->form_validation->set_message('test_account', lang('account_not_set'));
            return FALSE;
        }
    }

    function get_payment_request($no) {
        $q = (int) $no;
// remove slashes if they were magically added
        if ($q > 0) {
            $this->vouchers_model->payment_request_no = $q;
            $row = $this->vouchers_model->getPaymentRequest();
            if ($row) {
                $this->load->model('res_model');
                $this->res_model->res_id = $row->res_id;
                $res = $this->res_model->get();

                $account_id = $this->accounting->get_account_id('clients', $row->client_id);
                $account_name = $this->accounting->get_account_name($account_id);
                $result = array("id" => $row->res_payments_id
                    , "amount" => $row->amount
                    , "currency_id" => $row->currency_id
                    , "account_id" => $account_id
                    , "account_name" => $account_name
                    , "code" => $res->code
                    , "payment_date" => $row->payment_date
                    , "voucher_id" => $row->voucher_id);

                // json_encode is available in PHP 5.2 and above, or you can install a PECL module in earlier versions
                echo json_encode($result);
            }
//            else echo 'no row';
        }
//        else echo '< 0';
    }

    function get_currency_name($id) {
        $q = (int) $id;
        if ($q > 0) {
            echo ddmenu('currencies', $id);
        }
    }

    function check_payment_request($no) {
        $q = (int) $no;
// remove slashes if they were magically added
        if ($q > 0) {
            $this->vouchers_model->payment_request_no = $q;
            $row = $this->vouchers_model->getPaymentRequest();
            if ($row) {
                if ($row->voucher_id != '0') {
                    $this->form_validation->set_message('check_payment_request', lang('paid') . $row->payment_date);
                    return FALSE;
                } else {
                    return TRUE;
                }
            }
//            else echo '';
        }
//        else echo '';
    }

}

/* End of file vouchers.php */
/* Location: ./application/controllers/admin/vouchers.php */