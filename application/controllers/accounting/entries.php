<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Entries extends Safa_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('accounting');
//        $this->load->helper('ddmenu');
//        $this->load->model('currencies_model');
        $this->load->model('entries_master_model');
        $this->load->model('entries_details_model');
        $this->lang->load('entries');

        $this->layout = 'new';
        permission();
    }

    public function index() {
        $this->entries_master_model->limit = $this->config->item('per_page');
        $this->entries_master_model->offset = $this->uri->segment('4');
        $config['uri_segment'] = 4;
        $config['base_url'] = site_url('accounting/entries/index');
        $config['total_rows'] = $this->entries_master_model->get(true);
        $config['per_page'] = $this->config->item('per_page');
        $this->load->library('pagination');
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();

        $data['items'] = $this->entries_master_model->get();
        $this->load->view('entries/index', $data);
    }

    public function add($id = FALSE) {
        $max_id = $this->db->select('max(entry_number) as max_id')->where('firm_id', session('firm_id'))->get('entries_master')->row();
        $max = (int) $max_id->max_id;
        $min_id = $this->db->select('min(entry_number) as min_id')->where('firm_id', session('firm_id'))->get('entries_master')->row();
        $min = (int) $min_id->min_id;
        $data['currencies'] = ddgen('erp_currencies', array('erp_currency_id', name()));

        if (!$id) {
            $number = $this->db->select('max(entry_number) as max_no')->where('firm_id', session('firm_id'))->get('entries_master')->row();
            if ($number)
                $data['number'] = (int) $number->max_no + 1;
            else
                $data['number'] = 1;
        }
        else {

            $data['items'] = FALSE;
            $num = $this->input->post('entry_number');
            if ($this->input->post('pre')) {
                while (!$data['items']) {
                    $num--;
                    if ($num <= $min) {
                        $num = $min;
                        $data['min'] = TRUE;
                    }
                    $this->entries_master_model->entry_number = $num;
                    $data['master'] = $this->entries_master_model->get();
                    $this->entries_details_model->entry_number = $num;
                    $data['items'] = $this->entries_details_model->get();
                }
            } else if ($this->input->post('next')) {
                while (!$data['items']) {
                    $num++;
                    if ($num >= $max) {
                        $num = $max;
                        $data['max'] = TRUE;
                    }
                    $this->entries_master_model->entry_number = $num;
                    $data['master'] = $this->entries_master_model->get();
                    $this->entries_details_model->entry_number = $num;
                    $data['items'] = $this->entries_details_model->get();
                }
            } else {
                $this->entries_master_model->id = $id;
                $data['master'] = $this->entries_master_model->get();
                $this->entries_details_model->entry_id = $id;
                $data['items'] = $this->entries_details_model->get();
            }
            $data['number'] = $id;
            if (!$data['master'])
                redirect(site_url('accounting/entries'));
        }

        $this->load->library('form_validation');
        if (!$id)
            $this->form_validation->set_rules('entry_number', 'lang:entry_number', 'trim|required|integer|callback_unqnum');
        $this->form_validation->set_rules('entry_date', 'lang:entry_date', 'trim|required');
        $this->form_validation->set_rules('entry_currency', 'lang:entry_currency', 'trim|required|integer');
        $this->form_validation->set_rules('entry_rate', 'lang:entry_rate', 'trim|required');

        if ($this->form_validation->run() == FALSE || !$this->input->post('submit')) {
            if ($id)
                $this->load->view('entries/edit', $data);
            else
                $this->load->view('entries/add', $data);
        } else {
            if ($id)
                $this->delete($id, FALSE);
            $debit_sum = (float) $_POST['debit_sum'];
            $credit_sum = (float) $_POST['credit_sum'];
            if ($debit_sum == $credit_sum) {
                $this->accounting->entry_number = $_POST['entry_number'];
                $this->accounting->master_date = $_POST['entry_date'];
                $this->accounting->currency = $_POST['entry_currency'];
                $this->accounting->rate = $_POST['entry_rate'];

                $records = count($_POST['account']);

                for ($i = 0; $i < $records; $i++) {
                    $this->accounting->setRecord((int) $_POST['account_hidden'][$i]
                            , (float) $_POST['debit'][$i], (float) $_POST['credit'][$i]
                            , $_POST['currency'][$i], (float) $_POST['rate'][$i]
                            , $_POST['description'][$i], $_POST['remarks'][$i]
                            , (int) $_POST['second_account_hidden'][$i]);
                }

                if ($this->accounting->saveEntry()) {
                    if ($id)
                        $this->load->view('admin/redirect', array('msg' => lang('global_updated_successfully'), 'url' => site_url('entries/index')));
                    else
                        $this->load->view('admin/redirect', array('msg' => lang('global_added_successfully'), 'url' => site_url('entries/add')));
                }
                else {
                    $data['err'] = $this->accounting->getError();
                    if ($id)
                        $this->load->view('entries/edit', $data);
                    else
                        $this->load->view('entries/add', $data);
                }
            }
        }
    }

    public function edit($id = FALSE) {
        $this->add($id);
//        if (!$id)
//            show_404();
//        $data['currencies'] = currencies();
//        $this->entries_master_model->id = $id;
//        $data['master'] = $this->entries_master_model->get();
//        $this->entries_details_model->entry_id = $id;
//        $data['items'] = $this->entries_details_model->get();
//        
//        $this->load->library('form_validation');
//        $this->form_validation->set_rules('entry_number', 'lang:entry_number', 'trim|required|integer');
//        $this->form_validation->set_rules('entry_date', 'lang:entry_date', 'trim|required');
//        $this->form_validation->set_rules('entry_currency', 'lang:entry_currency', 'trim|required');
//        $this->form_validation->set_rules('entry_rate', 'lang:entry_rate', 'trim|required');
//        
//        if ($this->form_validation->run() == FALSE) {
//            $this->load->view('entries/edit', $data);
//        }
//        else
//        {
//            $this->delete($id, FALSE);
//            $this->add($id);
//        }
    }

    public function delete($id = FALSE, $redirect = TRUE) {

        if (!$id)
            show_404();

        $this->accounting->deleteEntry($id);

        if ($redirect)
            $this->load->view('admin/redirect', array('msg' => lang('global_deleted_successfully'), 'url' => site_url('entries')));
    }

    function unqnum($var) {
        $res = $this->db->where('firm_id', session('firm_id'))
                ->where('entry_number', $var)
                ->get('entries_master')
                ->num_rows();
        if ($res) {
            $this->form_validation->set_message('unqnum', lang('number_exist'));
            return FALSE;
        } else {
            return TRUE;
        }
    }

}

/* End of file entries.php */
/* Location: ./application/controllers/entries.php */