<?php

if (!defined('BASEPATH'))
    exit('No direct script usersess allowed');

class Vouchers_defs extends Safa_Controller {

    public function __construct() {

        parent::__construct();
        $this->load->model('vouchers_defs_model');
        $this->lang->load('admin/vouchers_defs');
        $this->load->helper('ddmenu');
        permission();
    }

    public function index() {

        $this->vouchers_defs_model->limit = $this->config->item('per_page');
        $this->vouchers_defs_model->offset = $this->uri->segment('4');
        $config['uri_segment'] = 4;
        $config['base_url'] = site_url('vouchers_defs/index');
        $config['total_rows'] = $this->vouchers_defs_model->get(TRUE);
        $config['per_page'] = $this->config->item('per_page');
        $this->load->library('pagination');
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();
        $data['items'] = $this->vouchers_defs_model->get();

        $this->load->view('admin/vouchers_defs/index', $data);
    }

    public function add() {

        $this->load->library('form_validation');
        $data['cash_account'] = $this->config->item('cash_account');
        $data['currencies'] = currencies();


        $this->form_validation->set_rules('main_type', 'lang:vouchers_defs_main_type', 'required|trim');
        if (config('language') == 'arabic') {
            $this->form_validation->set_rules('name_ar', 'lang:vouchers_defs_name_ar', 'required|trim');
        } else {
            $this->form_validation->set_rules('name_la', 'lang:vouchers_defs_name_la', 'required|trim');
        }
        $this->form_validation->set_rules('cash_account', 'lang:vouchers_defs_cash_account', 'required|trim');
        $this->form_validation->set_rules('currency', 'lang:vouchers_defs_currency', 'required|trim');
        $this->form_validation->set_rules('notes', 'lang:vouchers_defs_notes', 'trim');
        if ($this->form_validation->run() == FALSE) {
            $this->load->view('admin/vouchers_defs/add', $data);
        } else {

            if ($this->permissions->check('manage_all_firms') && $this->input->post('all_firms') == '1')
                $this->vouchers_defs_model->firm_id = '0';
            else
                $this->vouchers_defs_model->firm_id = session('firm_id');

            $this->vouchers_defs_model->main_type = $this->input->post('main_type');
            if (config('language') == 'arabic') {
                $this->vouchers_defs_model->name_ar = $this->input->post('name_ar');
                $this->vouchers_defs_model->name_la = $this->input->post('name_ar');
            } else {
                $this->vouchers_defs_model->name_ar = $this->input->post('name_la');
                $this->vouchers_defs_model->name_la = $this->input->post('name_la');
            }
            $this->vouchers_defs_model->cash_account = $this->input->post('cash_account');
            $this->vouchers_defs_model->currency = $this->input->post('currency');
            $this->vouchers_defs_model->notes = $this->input->post('notes');


            $this->vouchers_defs_model->save();
            $this->common->report('{report_new_vouchers_defs_added} #' . $this->vouchers_defs_model->id);
            $this->load->view('admin/redirect', array('msg' => lang('global_added_successfully'), 'url' => site_url('admin/vouchers_defs/index')));
        }
    }

    public function edit($id = FALSE) {

        $this->load->model('vouchers_defs_model');

        if (!$id)
            show_404();
        $this->vouchers_defs_model->id = $id;
        $data['item'] = $this->vouchers_defs_model->get();
        if (!$data['item'] || ($data['item']->firm_id == '0' && !permission('manage_all_firms')))
            show_404();

        $data['currencies'] = currencies();

        $this->load->library('form_validation');

        $this->form_validation->set_rules('main_type', 'lang:vouchers_defs_main_type', 'required|trim');
        if (config('language') == 'arabic') {
            $this->form_validation->set_rules('name_ar', 'lang:vouchers_defs_name_ar', 'required|trim');
        } else {
            $this->form_validation->set_rules('name_la', 'lang:vouchers_defs_name_la', 'required|trim');
        }
        $this->form_validation->set_rules('cash_account', 'lang:vouchers_defs_cash_account', 'required|trim');
        $this->form_validation->set_rules('currency', 'lang:vouchers_defs_currency', 'required|trim');
        $this->form_validation->set_rules('notes', 'lang:vouchers_defs_notes', 'trim');

        if ($this->form_validation->run() == FALSE) {

            $this->load->view('admin/vouchers_defs/edit', $data);
        } else {

            if ($this->permissions->check('manage_all_firms') && $this->input->post('all_firms') == '1')
                $this->vouchers_defs_model->firm_id = '0';
            else
                $this->vouchers_defs_model->firm_id = session('firm_id');

            $this->vouchers_defs_model->main_type = $this->input->post('main_type');
            if (config('language') == 'arabic') {
                $this->vouchers_defs_model->name_ar = $this->input->post('name_ar');
            } else {
                $this->vouchers_defs_model->name_la = $this->input->post('name_la');
            }
            $this->vouchers_defs_model->cash_account = $this->input->post('cash_account');
            $this->vouchers_defs_model->currency = $this->input->post('currency');
            $this->vouchers_defs_model->notes = $this->input->post('notes');


            $this->vouchers_defs_model->id = $id;
            $this->common->report('{report_vouchers_defs_update_accountd} #' . $this->vouchers_defs_model->id);
            $this->vouchers_defs_model->save();
            $this->load->view('admin/redirect', array('msg' => lang('global_upcash_accountd_successfully'), 'url' => site_url('admin/vouchers_defs/index')));
        }
    }

    function delete($id = FALSE) {

        if (!$id)
            show_404();
        if (!permission('manage_all_firms'))
            $this->vouchers_defs_model->firm_id = session('firm_id');

        $this->common->report('{report_vouchers_defs_deleted}');
        $this->vouchers_defs_model->id = $id;
        if (!$this->vouchers_defs_model->delete())
            show_404();
        $this->load->view('admin/redirect', array('msg' => lang('global_deleted_successfully'), 'url' => site_url('admin/vouchers_defs/index')));

        // $this->load->view('admin/redirect', array('msg' => lang('global_added_successfully'), 'url' => site_url('admin/vouchers_defs/index')));
    }

}

/* End of file vouchers_defs.php */
/* Location: ./application/controllers/admin/vouchers_defs.php */