<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Vouchers_types extends Safa_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->model('vouchers_types_model');
        $this->lang->load('admin/vouchers_types');
        $this->load->helper('ddmenu');
        permission();
    }

    public function index() {


        $this->vouchers_types_model->limit = $this->config->item('per_page');
        $this->vouchers_types_model->offset = $this->uri->segment('4');
        $config['uri_segment'] = 4;
        $config['base_url'] = site_url('admin/vouchers_types');
        $config['total_rows'] = $this->db->get('vouchers_types')->num_rows();
        $config['per_page'] = $this->config->item('per_page');
        $this->load->library('pagination');
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();
        $data['items'] = $this->vouchers_types_model->get();

        $this->load->view('admin/vouchers_types/index', $data);
    }

    public function add() {


        $this->load->library('form_validation');

        $this->form_validation->set_rules('name_ar', 'lang:name_ar', 'trim|required');
        $this->form_validation->set_rules('name_la', 'lang:name_la', 'trim|required');


        if ($this->form_validation->run() == FALSE) {


            $this->load->view('admin/vouchers_types/add');
        } else {

            $this->vouchers_types_model->name_ar = $this->input->post('name_ar');
            $this->vouchers_types_model->name_la = $this->input->post('name_la');
            $this->vouchers_types_model->save();

            $this->common->report('{report_new_vouchers_types_added} #' . $this->vouchers_types_model->id);
            $this->load->view('admin/redirect', array('msg' => lang('global_added_successfully'), 'url' => site_url('admin/vouchers_types')));
        }
    }

    public function edit($id = FALSE) {

        if (!$id)
            show_404();


        $this->vouchers_types_model->id = $id;
        $data['item'] = $this->vouchers_types_model->get();

        if (!$data['item'])
            show_404();

        $this->load->library('form_validation');
        $this->form_validation->set_rules('name_ar', 'lang:name_ar', 'trim|required');
        $this->form_validation->set_rules('name_la', 'lang:name_la', 'trim|required');

        if ($this->form_validation->run() == FALSE) {

            $this->load->view('admin/vouchers_types/edit', $data);
        } else {

            $this->vouchers_types_model->name_ar = $this->input->post('name_ar');
            $this->vouchers_types_model->name_la = $this->input->post('name_la');


            $this->vouchers_types_model->id = $id;
            $this->vouchers_types_model->save();

            $this->common->report('{report_vouchers_types_updated} #' . $this->vouchers_types_model->id);
            $this->load->view('admin/redirect', array('msg' => lang('global_updated_successfully'), 'url' => site_url('admin/vouchers_types')));
        }
    }

    function delete($id = FALSE) {

        if (!$id)
            show_404();
        if ($id > 2) {
            $this->vouchers_types_model->id = $id;
            $this->vouchers_types_model->delete();
            $this->common->report('{report_vouchers_types_deleted} #' . $id);
        }
        $this->load->view('admin/redirect', array('msg' => lang('global_deleted_successfully'), 'url' => site_url('admin/vouchers_types')));
    }

}

/* End of file vouchers_types.php */
/* Location: ./application/controllers/admin/vouchers_types.php */