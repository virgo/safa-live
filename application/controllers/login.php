<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Login extends Safa_Controller {

    public $membership;

    public function __construct() {
        parent::__construct();
        $this->load->model('safa_uo_users_model');
        $this->lang->load('login');
        $this->layout = 'js';
    }

    public function index($membership = FALSE) {
        // IF THERE'S NO MEMBERSHIP DETERMINED
        if (!$membership)
            show_404();
    
        if ($membership == 'ea' or $membership == 'uo' or $membership == 'hm' or $membership == 'gov')
            $this->layout = 'ajax';
        $this->membership = $membership;
        // IF ALREADY LOGGED IN REDIRECT TO DASHBOARD
        $this->alreadyLoggedIn($membership);
        // DESTROY SESSION IF USER ATTEMPT TO USE ANOTHER MEMBERSHIP
        if (session('type'))
            if (session('type') != $membership && !$_POST) {
                profiling(10);
                reset_cookies();
            }
                
        // LANGUAGES
        $data['languages'] = ddgen('erp_languages', FALSE, FALSE, FALSE, TRUE);
        $this->load->library('form_validation');
        $this->form_validation->set_rules('email', 'lang:login_username', 'required');
        $this->form_validation->set_rules('password', 'lang:login_password', 'required|callback_check_login');
        $this->form_validation->set_rules('remember', '', 'trim');
        $this->form_validation->set_rules('language', 'lang:login_language', 'trim');
        if ($this->form_validation->run() == FALSE) {
            $this->load->view('login/' . $membership, $data);
        } else {
            $user = $this->getUserInformation();
            if (!$this->input->post('language') && !session('language'))
                $language = 2;
            else {
                if ($this->input->post('language'))
                    $language = $this->input->post('language');
                else
                    $language = session('language');
            }

            if ($membership == 'admin') {
                $user_id = $user->{'erp_admin_id'};
                $name = $user->username;
                $id = $user->{'erp_' . $membership . '_id'};
                $user_name =  $user->username;
            } else {
                $user_id = $user->{'safa_' . $membership . '_user_id'};
                $name = $user->{name()};
                $id = $user->{'safa_' . $membership . '_id'};
                $user_name =  $user->user_name;
            }
            session($membership . '_email', $this->input->post('email'));
            session($membership . '_username', $user->username);
            session($membership . '_user_name', $user_name);
            session($membership . '_user_id', $user_id);
            session($membership . '_id', $id);
            session($membership . '_name', $name);
            session('password', sha1(PASSWORD_SALT.md5($this->input->post('password'))));
            session('language', $language);
            session('type', $membership);
            redirect($membership . '/dashboard');
        }
    }

    public function forget_password($membership = FALSE) {
        // IF THERE'S NO MEMBERSHIP DETERMINED
        if (!$membership)
            show_404();
        $this->layout = 'js_new';
        $this->load->library('form_validation');
        $this->form_validation->set_rules('email', 'lang:global_forget_password_enter_email', 'required|trim|valid_email|callback_checkforgottenpassword['.$membership.']');
        if ($this->form_validation->run() == FALSE) {
            $data['languages'] = ddgen('erp_languages', FALSE, FALSE, FALSE, TRUE);
            $this->load->view('login/forget_password', $data);
        } else {
            $user = $this->db->where('email', $this->input->post('email'))->get('safa_'.$membership.'_users')->row();
            $verification_code = rand(000000, 999999);
            $this->db->where('safa_' . $membership . '_user_id', $user->{'safa_' . $membership . '_user_id'})->update('safa_' . $membership . '_users', array('reset_password' => time(), 'ver_code' => $verification_code));
            $link = site_url('login/reset/' . $membership . '/'. $user->{'safa_' . $membership . '_user_id'} . '/' . $verification_code);
            $username = $user->username;
            $message1 = str_replace('{link}', $link, lang('global_forgot_pass_msg'));
            $message = str_replace('{username}', $username, $message1);
            $this->load->library('email');
            $this->email->from(config('webmaster_email'), config('title'));
            $this->email->to($user->email);
            $this->email->subject(lang('global_forget_subject_msg'));
            $this->email->message($message);
            $this->email->set_mailtype("html");
            $this->email->send();
            // echo $this->email->print_debugger();
//            $this->load->view('uo/redirect', array('msg' => lang('global_check_your_email')));
            $this->load->view('uo/redirect', array('msg' => lang('global_check_your_email'), 'url' => site_url($membership)));
        }
    }

    public function reset($membership, $id, $ver_code) {
        $user = $this->db->where('safa_'.$membership.'_user_id', $id)->where('ver_code', $ver_code)->get('safa_'.$membership.'_users')->row();

        if (!$user)
            show_404();
        if (!$user->reset_password >= time() - 7200)
            show_error(lang('reset_password_time_out'));
        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
        $this->form_validation->set_rules('new_pass', 'lang:login_newpass', 'trim|required');
        $this->form_validation->set_rules('conf_password', 'lang:login_confpass', 'trim|required|matches[new_pass]');
        $this->form_validation->set_rules('remember', '', 'trim');
        if ($this->form_validation->run() == FALSE) {
            $this->load->view('login/reset_password');
        } else {
            $this->safa_uo_users_model->safa_uo_user_id = $id;
            $this->safa_uo_users_model->password = md5($this->input->post('new_pass'));
            $this->safa_uo_users_model->ver_code = $this->gencode();
            $this->safa_uo_users_model->save();
            $this->load->library('email');
            $this->email->from(config('webmaster_email'), config('title'));
            $this->email->to($user->email);
            $this->email->subject(lang('reset_password_congrates_title'));
            $this->email->message(lang('reset_password_congrates'));
            $this->email->set_mailtype("html");
            $this->email->send();
            redirect('');
        }
    }

    public function set_language($language_id) {
        session('language', $language_id);
        $ref = $this->input->server('HTTP_REFERER', TRUE);
        redirect($ref);
    }

    public function logout() {
        $type = session('type');
        reset_cookies();
        redirect($type);
    }

    /**
     * CALLBACKS
     * @author Muhammad El-Saeed <m.elsaeed@virgotel.com>
     */
    public function check_login() {
        $password = $this->input->post('password');
        if ($this->membership != 'admin') {
            $seek = $this->db->where('safa_' . $this->membership . '_users.email', $this->input->post('email'))
                         ->where('safa_' . $this->membership . '_users.password', md5($password))
                         ->get('safa_' . $this->membership . '_users')->row();
        } elseif ($this->membership == 'admin') {
            $seek = $this->db->where('email', $this->input->post('email'))
                         ->where('password', md5($password))
                         ->get('erp_' . $this->membership . 's')->row();
        }
//        print_r($seek);
        if ( ! $seek) {
            $this->form_validation->set_message('check_login', lang('callback_incorrect_data'));
            return FALSE;
        }
        return TRUE;
    }

    public function checkforgottenpassword($email, $membership) {
        $user = $this->db->where('email', $email)->get('safa_'.$membership.'_users')->row();
        if (!$user) {
            $this->form_validation->set_message('checkforgottenpassword', lang('global_err_invalid_email'));
            return FALSE;
        } else {
            return TRUE;
        }
    }

    /**
     * METHODS
     * @author Muhammad El-Saeed <m.elsaeed@virgotel.com>
     */
    protected function alreadyLoggedIn($membership) {
        if (session($membership . '_email'))
            redirect($membership . '/dashboard');
    }

    protected function getUserInformation($password = FALSE) {
        if ($password){
            if ($this->membership != 'admin')
                $pass_field = 'safa_' . $this->membership . '_users.password';
            else
                $pass_field = 'password';
            
            $this->db->where($pass_field, md5($password));
        }
        if ($this->membership != 'admin') {
            $this->db->select('safa_' . $this->membership . '_users.*, safa_' . $this->membership . 's.* , safa_' . $this->membership . '_users.' . name() . ' as user_name');
            return $this->db->where('safa_' . $this->membership . '_users.email', $this->input->post('email'))
                            ->join('safa_' . $this->membership . 's', 'safa_' . $this->membership . '_id')
                            ->get('safa_' . $this->membership . '_users')->row();
        } elseif ($this->membership == 'admin') {
            return $this->db->where('email', $this->input->post('email'))
                            ->get('erp_' . $this->membership . 's')->row();
        }
    }

}

/* End of file login.php */
/* Location: ./application/controllers/login.php */
