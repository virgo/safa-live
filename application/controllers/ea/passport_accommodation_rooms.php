<?php

class Passport_accommodation_rooms extends Safa_Controller {

	function __construct() {
		parent::__construct();

		$this->layout = 'new';
		session('side_menu_id', 1);
		$this->load->helper('form_helper');
		$this->load->helper('db_helper');
		$this->load->helper('itconf_helper');

		$this->load->model('safa_trips_model');
		$this->load->model('contracts_model');
		$this->load->model('safa_packages_model');
		$this->load->model('hotelroomsizes_model');
		$this->load->model('safa_package_periods_model');
		$this->load->model('package_hotels_model');
		$this->load->model('safa_group_passports_model');
		$this->load->model('package_periods_cities_model');
		$this->load->model('hotels_model');

		$this->load->model('safa_reservation_forms_model');
		$this->load->model('safa_passport_accommodation_rooms_model');
		$this->load->model('safa_group_passport_accommodation_model');

		$this->load->model('hotel_availability_search_model');
		$this->load->model('hotels_availability_room_details_model');

		$this->load->model('safa_umrahgroups_passports_model');
		$this->load->model('contracts_settings_model');
		$this->load->model('trip_internaltrip_model');
		
		$this->lang->load('passport_accommodation_rooms');

		permission();
		if (session('ea_id')) {
			$this->safa_reservation_forms_model->safa_ea_id = session('ea_id');
		}
	}

	function index() {
		if (isset($_GET['search'])) {
			$this->search();
		}
		$data["total_rows"] = $this->safa_reservation_forms_model->get(true);

		$this->safa_reservation_forms_model->offset = $this->uri->segment("4");
		//$this->safa_reservation_forms_model->limit = $this->config->item('per_page');
		$this->safa_reservation_forms_model->order_by = array('safa_reservation_form_id', 'desc');
		$data["items"] = $this->safa_reservation_forms_model->get();
		$config['uri_segment'] = 4;
		$config['base_url'] = site_url('ea/passport_accommodation_rooms/index');
		$config['total_rows'] = $data["total_rows"];

		$config['per_page'] = $this->config->item('per_page');
		$config['suffix'] = $this->input->get() ? '?' . http_build_query($_GET) : NULL;
		$this->load->library('pagination');
		$this->pagination->initialize($config);
		$data['pagination'] = $this->pagination->create_links();
		$this->load->view('ea/passport_accommodation_rooms/index', $data);
	}

	function manage($id = FALSE) {
		//------------ Packages For this EA ----------------------------
		$this->contracts_model->safa_ea_id = session('ea_id');
		$uo_contracts_rows = $this->contracts_model->search();
		$safa_packages_arr = array();
		$safa_packages_arr[""] = lang('global_select_from_menu');

		foreach ($uo_contracts_rows as $uo_contracts_row) {

			$this->safa_packages_model->get_by_uo_contract($uo_contracts_row->safa_uo_contract_id);
			$safa_packages_rows = $this->safa_packages_model->get();
			foreach ($safa_packages_rows as $safa_packages_row) {
				$safa_packages_arr[$safa_packages_row->safa_package_id] = $safa_packages_row->{name()};
			}
		}

		$data['safa_packages'] = $safa_packages_arr;

		$data['erp_hotels'] = ddgen('erp_hotels', array('erp_hotel_id', name()), FALSE, FALSE, TRUE);
		$data['erp_hotelroomsizes'] = ddgen('erp_hotelroomsizes', array('erp_hotelroomsize_id', name()), FALSE, FALSE, TRUE);
		$data['safa_groups'] = ddgen('safa_groups', array('safa_group_id', 'name'), array('safa_groups.safa_ea_id' => session('ea_id')), FALSE, TRUE);
		$data['safa_trips'] = ddgen('safa_trips', array('safa_trip_id', 'name'), array('safa_trips.safa_ea_id' => session('ea_id')), FALSE, FALSE);

		//---------------------------------------------------------------


		$data['package_periods_arr'] = array();
		$data['package_hotels_arr'] = array();

		if ($id) {

			$this->safa_reservation_forms_model->safa_reservation_form_id = $id;
			$data['item'] = $this->safa_reservation_forms_model->get();

			//---------- Details --------------------------
			$this->safa_passport_accommodation_rooms_model->safa_reservation_form_id = $id;
			$item_hotels = $this->safa_passport_accommodation_rooms_model->get_hotels();
			$data['item_hotels'] = $item_hotels;

			$data['item_hotels_select_arr'] = array();
			foreach ($item_hotels as $item_hotel) {
				$data['item_hotels_select_arr'][] = $item_hotel->erp_hotel_id;
			}

			//By Gouda.
			//$data['erp_hotelroomsizes_rows'] = $this->hotelroomsizes_model->get();
			$data['erp_hotelroomsizes_rows'] = $this->safa_passport_accommodation_rooms_model->get_hotelroomsizes();


			/*
			 $this->safa_passport_accommodation_rooms_model->safa_reservation_form_id = $id;
			 $item_passport_accommodation_rooms = $this->safa_passport_accommodation_rooms_model->get();
			 $data['item_passport_accommodation_rooms'] = $item_passport_accommodation_rooms;

			 $this->safa_group_passport_accommodation_model->safa_reservation_form_id = $id;
			 $item_group_passport_accommodations = $this->safa_group_passport_accommodation_model->get();
			 $data['item_group_passport_accommodations'] = $item_group_passport_accommodations;
			 */

			//-----------------------------------------------
		}


		$this->load->library("form_validation");
		$this->form_validation->set_rules('safa_trip_id', 'lang:safa_trip_id', 'trim|required');

		if (!$id) {
			$this->form_validation->set_rules('safa_package_id', 'safa_package_id:name_la', 'trim|required');
			$this->form_validation->set_rules('safa_package_periods_id', 'lang:safa_package_periods_id', 'trim|required');
		}

		$this->form_validation->set_rules('erp_hotel_id[]', 'lang:erp_hotel_id', 'trim|required');

		if ($this->form_validation->run() == false) {
			$this->load->view('ea/passport_accommodation_rooms/manage', $data);
		} else {

			$this->safa_reservation_forms_model->safa_ea_id = session('ea_id');
			$this->safa_reservation_forms_model->safa_trip_id = $this->input->post('safa_trip_id');
			if (!$id) {
				$this->safa_reservation_forms_model->safa_package_period_id = $this->input->post('safa_package_periods_id');
			}


			if ($id) {
				$this->safa_reservation_forms_model->safa_reservation_form_id = $id;
				$safa_reservation_form_id = $id;
				$this->safa_reservation_forms_model->save();
				$this->edit_passports_for_edit_mode($safa_reservation_form_id);
				$this->add_passports_for_edit_mode($safa_reservation_form_id);
				$this->delete_safa_passport_accommodation_rooms_for_edit_mode();

				if(session('ea_id')) {
					$redirect_url = site_url('ea/passport_accommodation_rooms');
				} else if(session('uo_id')) {
					$redirect_url = site_url('uo/trips');
				}

				$this->load->view('redirect_new_message', array('msg' => lang('global_updated_message'),
                    'url' => $redirect_url,
                    'id' => $this->uri->segment("4"),
                    'model_title' => lang('title'), 'action' => lang('edit_title')));
			} else {
				$safa_reservation_form_id = $this->safa_reservation_forms_model->save();
				//$this->save_rooms_and_passports($safa_reservation_form_id);
				$this->add_passports_for_edit_mode($safa_reservation_form_id);

				$this->load->view('redirect_new_message', array('msg' => lang('global_added_message'),
                    'url' => site_url('ea/passport_accommodation_rooms'),
                    'id' => $this->uri->segment("4"),
                    'model_title' => lang('title'), 'action' => lang('add_title')));
			}
		}
	}

	function manage_popup($safa_trip_id = FALSE, $id = FALSE)
	{
		//$this->layout = 'js_new';
		
		$safa_trip_internaltrip_id=0;
		$this->trip_internaltrip_model->safa_trip_id = $safa_trip_id;
		$safa_trip_internaltrip_rows = $this->trip_internaltrip_model->get();
		if(count($safa_trip_internaltrip_rows)>0) {
				$safa_trip_internaltrip_id=$safa_trip_internaltrip_rows[0]->safa_trip_internaltrip_id;
		}
		
		$data['safa_trip_internaltrips_safa_trip_id'] = $safa_trip_id;
		$this->safa_trips_model->safa_trip_id = $safa_trip_id;
		$safa_trip_row = $this->safa_trips_model->get();
		$safa_trip_name = $safa_trip_row->name;
		$data['safa_trip_name'] = $safa_trip_name;
		 
		 
		//------------ Packages For this EA ----------------------------
		$this->contracts_model->safa_ea_id = session('ea_id');
		$uo_contracts_rows = $this->contracts_model->search();
		$safa_packages_arr = array();
		$safa_packages_arr[""] = lang('global_select_from_menu');

		foreach ($uo_contracts_rows as $uo_contracts_row) {

			$this->safa_packages_model->get_by_uo_contract($uo_contracts_row->safa_uo_contract_id);
			$safa_packages_rows = $this->safa_packages_model->get();
			foreach ($safa_packages_rows as $safa_packages_row) {
				$safa_packages_arr[$safa_packages_row->safa_package_id] = $safa_packages_row->{name()};
			}
		}

		$data['safa_packages'] = $safa_packages_arr;

		$data['erp_hotels'] = ddgen('erp_hotels', array('erp_hotel_id', name()), FALSE, FALSE, TRUE);
		$data['erp_hotelroomsizes'] = ddgen('erp_hotelroomsizes', array('erp_hotelroomsize_id', name()), FALSE, FALSE, TRUE);
		$data['safa_groups'] = ddgen('safa_groups', array('safa_group_id', 'name'), array('safa_groups.safa_ea_id' => session('ea_id')), FALSE, TRUE);
		$data['safa_trips'] = ddgen('safa_trips', array('safa_trip_id', 'name'), array('safa_trips.safa_ea_id' => session('ea_id')), FALSE, FALSE);

		//---------------------------------------------------------------


		$data['package_periods_arr'] = array();
		$data['package_hotels_arr'] = array();

		if ($id) {

			$this->safa_reservation_forms_model->safa_reservation_form_id = $id;
			$data['item'] = $this->safa_reservation_forms_model->get();

			//---------- Details --------------------------
			$this->safa_passport_accommodation_rooms_model->safa_reservation_form_id = $id;
			$item_hotels = $this->safa_passport_accommodation_rooms_model->get_hotels();
			$data['item_hotels'] = $item_hotels;

			$data['item_hotels_select_arr'] = array();
			foreach ($item_hotels as $item_hotel) {
				$data['item_hotels_select_arr'][] = $item_hotel->erp_hotel_id;
			}

			//By Gouda.
			//$data['erp_hotelroomsizes_rows'] = $this->hotelroomsizes_model->get();
			$data['erp_hotelroomsizes_rows'] = $this->safa_passport_accommodation_rooms_model->get_hotelroomsizes();


			/*
			 $this->safa_passport_accommodation_rooms_model->safa_reservation_form_id = $id;
			 $item_passport_accommodation_rooms = $this->safa_passport_accommodation_rooms_model->get();
			 $data['item_passport_accommodation_rooms'] = $item_passport_accommodation_rooms;

			 $this->safa_group_passport_accommodation_model->safa_reservation_form_id = $id;
			 $item_group_passport_accommodations = $this->safa_group_passport_accommodation_model->get();
			 $data['item_group_passport_accommodations'] = $item_group_passport_accommodations;
			 */

			//-----------------------------------------------
		}


		$this->load->library("form_validation");
		$this->form_validation->set_rules('safa_trip_id', 'lang:safa_trip_id', 'trim|required');

		if (!$id) {
			$this->form_validation->set_rules('safa_package_id', 'safa_package_id:name_la', 'trim|required');
			$this->form_validation->set_rules('safa_package_periods_id', 'lang:safa_package_periods_id', 'trim|required');
		}

		$this->form_validation->set_rules('erp_hotel_id[]', 'lang:erp_hotel_id', 'trim|required');

		if ($this->form_validation->run() == false) {
			$this->load->view('ea/passport_accommodation_rooms/manage_popup', $data);
		} else {

			$this->safa_reservation_forms_model->safa_ea_id = session('ea_id');
			$this->safa_reservation_forms_model->safa_trip_id = $this->input->post('safa_trip_id');
			if (!$id) {
				$this->safa_reservation_forms_model->safa_package_period_id = $this->input->post('safa_package_periods_id');
			}


			if ($id) {
				$this->safa_reservation_forms_model->safa_reservation_form_id = $id;
				$safa_reservation_form_id = $id;
				$this->safa_reservation_forms_model->save();
				$this->edit_passports_for_edit_mode($safa_reservation_form_id, $safa_trip_internaltrip_id);
				$this->add_passports_for_edit_mode($safa_reservation_form_id, $safa_trip_internaltrip_id);
				$this->delete_safa_passport_accommodation_rooms_for_edit_mode();

				if(session('ea_id')) {
					$redirect_url = site_url('ea/passport_accommodation_rooms');
				} else if(session('uo_id')) {
					$redirect_url = site_url('uo/trips');
				}

				//                $this->load->view('message_popup', array('msg' => lang('global_updated_message'),
				//                    'url' => $redirect_url,
				//                    'id' => $this->uri->segment("4"),
				//                    'model_title' => lang('title'), 'action' => lang('edit_title')));
			} else {
				$safa_reservation_form_id = $this->safa_reservation_forms_model->save();
				//$this->save_rooms_and_passports($safa_reservation_form_id);
				$this->add_passports_for_edit_mode($safa_reservation_form_id, $safa_trip_internaltrip_id);

				//                $this->load->view('message_popup', array('msg' => lang('global_added_message'),
				//                    'url' => site_url('ea/passport_accommodation_rooms'),
				//                    'id' => $this->uri->segment("4"),
				//                    'model_title' => lang('title'), 'action' => lang('add_title')));
			}


			
			redirect(site_url('safa_trip_internaltrips/manage/'.$safa_trip_internaltrip_id));
			
		}
	}

	public function get_all_by_package_ajax() {
		$hotels_table_by_package_ajax_arr = $this->get_hotels_table_by_package_ajax();

		$data_arr[] = array('periods' => $this->get_periods_by_package(),
            'hotels_table' => $hotels_table_by_package_ajax_arr['hotels_table'],
            'hotels_rooms_for_storage' => $hotels_table_by_package_ajax_arr['hotels_rooms_for_storage'],
            'hotels' => $this->get_hotels_by_package(),
		);
		echo json_encode($data_arr);
		exit;
	}

	public function get_periods_by_package() {

		$safa_package_id = $this->input->post('safa_package_id');

		$structure = array('safa_package_periods_id', 'erp_package_period_name');
		$key = $structure['0'];
		if (isset($structure['1'])) {
			$value = $structure['1'];
		}

		$package_periods_arr = array();
		$this->safa_package_periods_model->safa_package_id = $safa_package_id;
		$package_periods_rows = $this->safa_package_periods_model->get();
		foreach ($package_periods_rows as $package_periods_row) {
			$package_periods_arr[$package_periods_row->$key] = $package_periods_row->$value;
		}


		$form_dropdown = form_dropdown('safa_package_periods_id', $package_periods_arr, set_value('safa_package_periods_id', ''), 'class="chosen-select chosen-rtl input-full"  tabindex="4" id="safa_package_periods_id" ');
		$form_dropdown = str_replace('<select id="safa_package_periods_id" class="chosen-select input-full" name="safa_package_periods_id">', '', $form_dropdown);
		$form_dropdown = str_replace('</select>', '', $form_dropdown);

		return $form_dropdown;

		//echo $form_dropdown;
		//exit;
	}

	public function get_hotels_by_package() {

		$safa_package_id = $this->input->post('safa_package_id');

		$structure = array('erp_hotel_id', 'erp_hotel_name', 'erp_city_name');
		$key = $structure['0'];
		if (isset($structure['1'])) {
			$value = $structure['1'];
		}
		if (isset($structure['2'])) {
			$value_city = $structure['2'];
		}

		$package_hotels_arr = array();


		$this->package_hotels_model->safa_package_id = $safa_package_id;
		$package_hotels_rows = $this->package_hotels_model->get();

		if (count($package_hotels_rows) > 0) {
			$package_hotels_arr[''] = lang('global_all');
		}

		foreach ($package_hotels_rows as $package_hotels_row) {
			$package_hotels_arr[$package_hotels_row->$key] = $package_hotels_row->$value .' - '. $package_hotels_row->$value_city;
		}


		$form_dropdown_options = '';
		foreach ($package_hotels_arr as $key => $value) {
			$form_dropdown_options = $form_dropdown_options . "<option value='$key'>$value</option>";
		}

		return $form_dropdown_options;
		//echo $form_dropdown_options;
		//exit;
	}

	public function get_hotels_by_package_for_table_ajax($safa_package_id = 0, $safa_trip_id = 0, $safa_package_periods_id = 0) {

		$structure = array('erp_hotel_id', 'erp_hotel_name', 'erp_city_name');
		$key = $structure['0'];
		if (isset($structure['1'])) {
			$value = $structure['1'];
		}
		if (isset($structure['2'])) {
			$value_city = $structure['2'];
		}

		$package_hotels_arr = array();
		$this->package_hotels_model->safa_package_id = $safa_package_id;
		$package_hotels_rows = $this->package_hotels_model->get();

		$hotels_counter = 0;
		$erp_hotel_id = 0;

		if (count($package_hotels_rows) > 0) {
			$package_hotels_arr[''] = lang('global_all');
		}
		foreach ($package_hotels_rows as $package_hotels_row) {
			if ($hotels_counter == 0) {
				$erp_hotel_id = $package_hotels_row->$key;
			}
			$hotels_counter++;

			$package_hotels_arr[$package_hotels_row->$key] = $package_hotels_row->$value .' - '. $package_hotels_row->$value_city;
		}


		$form_dropdown_options = '';
		foreach ($package_hotels_arr as $key => $value) {
			$form_dropdown_options = $form_dropdown_options . "<option value='$key'>$value</option>";
		}

		//------------------------------------- Dates ------------------------------------
		$from_date = '';
		$to_date = '';
		$dates_arr = array();
		$this->safa_trips_model->safa_trip_id = $safa_trip_id;
		$safa_trip_row = $this->safa_trips_model->get();
		if (count($safa_trip_row) != 0) {
			$from_date = $safa_trip_row->arrival_date;
		}

		$this->hotels_model->erp_hotel_id = $erp_hotel_id;
		$hotel_row = $this->hotels_model->get();
		if (count($hotel_row) > 0) {

			$this->package_periods_cities_model->safa_package_period_id = $safa_package_periods_id;
			$this->package_periods_cities_model->erp_city_id = $hotel_row->erp_city_id;
			$package_periods_cities_rows = $this->package_periods_cities_model->get();

			if (count($package_periods_cities_rows) != 0 && $from_date != '') {
				$to_date = date('Y-m-d', strtotime($from_date . ' + ' . $package_periods_cities_rows[0]->city_days . ' days'));
			}
		}
		//----------------------------------------------------------------------------------


		$data_arr[] = array('form_dropdown_options' => $form_dropdown_options,
            'from_date' => $from_date,
            'to_date' => $to_date,
		);
		echo json_encode($data_arr);

		exit;
	}

	public function get_hotels_table_by_package_ajax()
	{

		$safa_package_id = $this->input->post('safa_package_id');
		$safa_trip_id = $this->input->post('safa_trip_id');
		//$safa_package_periods_id = $this->input->post('safa_package_periods_id');

		$first_entry_date_for_trip = $this->safa_passport_accommodation_rooms_model->get_first_entry_date_for_trip($safa_trip_id);



		$safa_package_periods_id = 0;
		$this->safa_package_periods_model->safa_package_id = $safa_package_id;
		$package_periods_rows = $this->safa_package_periods_model->get();
		if (count($package_periods_rows) > 0) {
			$safa_package_periods_id = $package_periods_rows[0]->safa_package_periods_id;
		}

		$structure = array('erp_hotel_id', 'erp_hotel_name', 'erp_city_name');
		$key = $structure['0'];
		if (isset($structure['1'])) {
			$value = $structure['1'];
		}
		if (isset($structure['2'])) {
			$value_city = $structure['2'];
		}

		$package_hotels_arr = array();
		$this->package_hotels_model->safa_package_id = $safa_package_id;
		$package_hotels_rows = $this->package_hotels_model->get();

		$hotels_counter = 0;
		$erp_hotel_id = 0;

		$hotels_table = "
		<table width='100%'>
			<thead>
		
				
		
				<tr>
					<td class='TAC'>" . lang('erp_hotel_id') . "</td>
		
					<td class='TAC span2'>" . lang('from_date') . "</td>
		
					<td class='TAC span2'>" . lang('to_date') . "</td>
				</tr>
			</thead>
		
			<tbody class='tbl_hotels' id='tbl_hotels' style='line-height:16px;'>";




		$package_hotels_rows_count=count($package_hotels_rows);
		foreach ($package_hotels_rows as $package_hotels_row) {

			$erp_hotel_id = $package_hotels_row->$key;
			$erp_hotel_name = $package_hotels_row->$value.' ('.  $package_hotels_row->$value_city.')';


			//------------------------------------- Dates ------------------------------------
			$from_date = '';
			$to_date = '';
			$dates_arr = array();

			if($hotels_counter==0) {
				$this->safa_trips_model->safa_trip_id = $safa_trip_id;
				$safa_trip_row = $this->safa_trips_model->get();
				if (count($safa_trip_row) != 0) {
					$from_date = $safa_trip_row->arrival_date;
				}
			} else {
				$from_date = $to_date_for_from;
			}

			$to_date_for_from='';

			$this->hotels_model->erp_hotel_id = $erp_hotel_id;
			$hotel_row = $this->hotels_model->get();
			if (count($hotel_row) > 0) {

				$this->package_periods_cities_model->safa_package_periods = $safa_package_periods_id;
				$this->package_periods_cities_model->erp_city_id = $hotel_row->erp_city_id;
				$package_periods_cities_rows = $this->package_periods_cities_model->get();

				if (count($package_periods_cities_rows) > 0 && $from_date != '') {
					$to_date = date('Y-m-d', strtotime($from_date . ' + ' . $package_periods_cities_rows[0]->city_days . ' days'));
					$to_date_for_from = $to_date;
				}
			}
			//----------------------------------------------------------------------------------

			if ($from_date == '0000-00-00' || $from_date == '' || $from_date == null) {
				$from_date = '';
				$to_date = '';
			}

			$up_down_icons='';
			if($hotels_counter==0) {
				$up_down_icons="<i id='hotel_li_$hotels_counter' class=' icon-sort-down icon-2x pull-left'></i>";
			} else if($hotels_counter+1 == $package_hotels_rows_count) {
				$up_down_icons="<i id='hotel_li_$hotels_counter'  class=' icon-sort-up icon-2x pull-left'></i>";
			} else {
				$up_down_icons="<i id='hotel_li_$hotels_counter'  class=' icon-sort-up icon-2x pull-left'></i>
            	<i id='hotel_li_$hotels_counter' class=' icon-sort-down icon-2x pull-left'></i>";
			}

			if(count($first_entry_date_for_trip)>0) {
				$up_down_icons='';
			}

			if($hotels_counter==0 && count($first_entry_date_for_trip)>0) {
				$up_down_icons='';

				$from_date = $first_entry_date_for_trip->from_date ;
				$to_date = $first_entry_date_for_trip->to_date ;

				$erp_hotel_id = $first_entry_date_for_trip->erp_hotel_id ;
				$erp_hotel_name = $first_entry_date_for_trip->erp_hotel_name ;

				$hotels_table = $hotels_table . "
	        	<tr>
				<td> 
					<div> 
		            <input type='hidden' name='erp_hotel_id[$hotels_counter]' id='erp_hotel_id$hotels_counter' value='$erp_hotel_id' />
		            $erp_hotel_name
					</div>
				</td>
				
				<td>
		            <input type='hidden' name='from_date[$hotels_counter]' id='from_date$hotels_counter' value='$from_date' />
		            $from_date
				</td>
				
				<td>
	            	
				<!--
				<input type='hidden' name='to_date[$hotels_counter]' id='to_date$hotels_counter' value='$to_date' />
				$to_date
	            -->
	            	
	            	
	            <input type='text' name='to_date[$hotels_counter]' id='to_date$hotels_counter' value='$to_date' class='validate[required] to_date$hotels_counter'/>
            	
			    <script>
				$.datepicker.setDefaults({
			      //showOn: 'button', 
			      //buttonImage: 'images/calendar.gif', 
			      //buttonImageOnly: true,
			      controlType: 'select',
			      timeFormat: 'HH:mm',
			      dateFormat: 'yy-mm-dd',
			      //minDate: '2013-09-10',
			      //maxDate: '2013-10-10'
				});
				$('#from_date$hotels_counter').datepicker({
				      onSelect: function(selectedDate) {
				           $('#to_date$hotels_counter').datepicker('option', 'minDate', selectedDate);
				           get_to_date_for_hotel(this.value,$hotels_counter);
				      }
				});
				
				$('#to_date$hotels_counter').datepicker({
				      onSelect: function(selectedDate) {
				            $('#from_date$hotels_counter').datepicker('option', 'maxDate', selectedDate);
				            $('#from_date" . ($hotels_counter + 1) . "').datepicker('option', 'minDate', selectedDate);
				      }
				});
				
				
	    
				</script>
			
			
			    </td>
				</tr>";


			} else {


				$hotels_table = $hotels_table . "<tr>
			<td> 
			
			<div> 
			$up_down_icons
            <input type='hidden' name='erp_hotel_id[$hotels_counter]' id='erp_hotel_id$hotels_counter' value='$erp_hotel_id' />
            $erp_hotel_name
			</div>
			</td>
			
			<td>
			<input type='text'  name='from_date[$hotels_counter]' id='from_date$hotels_counter' value='$from_date' class='validate[required] from_date$hotels_counter'  />
            
			</td>
			
			<td>
			    <input type='text' name='to_date[$hotels_counter]' id='to_date$hotels_counter' value='$to_date' class='validate[required] to_date$hotels_counter'/>
            	
		    <script>
			$.datepicker.setDefaults({
		      //showOn: 'button', 
		      //buttonImage: 'images/calendar.gif', 
		      //buttonImageOnly: true,
		      controlType: 'select',
		      timeFormat: 'HH:mm',
		      dateFormat: 'yy-mm-dd',
		      //minDate: '2013-09-10',
		      //maxDate: '2013-10-10'
			});
			$('#from_date$hotels_counter').datepicker({
			      onSelect: function(selectedDate) {
			           $('#to_date$hotels_counter').datepicker('option', 'minDate', selectedDate);
			           get_to_date_for_hotel(this.value,$hotels_counter);
			      }
			});
			
			$('#to_date$hotels_counter').datepicker({
			      onSelect: function(selectedDate) {
			            $('#from_date$hotels_counter').datepicker('option', 'maxDate', selectedDate);
			            $('#from_date" . ($hotels_counter + 1) . "').datepicker('option', 'minDate', selectedDate);
			      }
			});
			
			
    
			</script>
			
		    </td>
			</tr>";
			}
				
				
			$hotels_counter++;
		}

		$hotels_table = $hotels_table . "</tbody>
		</table>
		<script>
		$('.icon-sort-up,.icon-sort-down').click(function(){
		        var row = $(this).parents('tr:first');
		        if ($(this).is('.icon-sort-up')) {
		        	
		        	var li_id = $(this).attr('id');
		        	var li_counter = li_id.replace('hotel_li_', '');	
		        	li_counter--;
		        	
		        	$(this).removeClass('icon-sort-up');
		            $(this).addClass('icon-sort-down');
		            
		            $('#hotel_li_'+li_counter).removeClass('icon-sort-down');
		            //$('#hotel_li_'+li_counter).addClass('icon-sort-up');
		            
		            row.insertBefore(row.prev());
		            
		            var current_from_date = $('#from_date'+li_counter).val();
		            var current_to_date = $('#to_date'+li_counter).val();
		            
		            
		            
		            if($('#from_date'+(li_counter+1)).val()!='') {
		            	$('#from_date'+li_counter).val($('#from_date'+(li_counter+1)).val());
		            }
		            if($('#to_date'+(li_counter+1)).val()!='') {
		            	$('#to_date'+li_counter).val($('#to_date'+(li_counter+1)).val());
		            }
		            
		            if(current_from_date!=undefined) {
		            	$('#from_date'+(li_counter+1)).val(current_from_date);
		            } 
		            if(current_to_date!=undefined) {
		            	$('#to_date'+(li_counter+1)).val(current_to_date);
		            }
		            
		        } else {
		        	
		            
		            $(this).removeClass('icon-sort-down');
		            $(this).addClass('icon-sort-up');
		            
		            var li_id = $(this).attr('id');
		        	var li_counter = li_id.replace('hotel_li_', '');	
		        	li_counter++;
		        	
		            $('#hotel_li_'+li_counter).removeClass('icon-sort-up');
		            //$('#hotel_li_'+li_counter).addClass('icon-sort-down');
		            
		            row.insertAfter(row.next());
		            
		            var current_from_date = $('#from_date'+li_counter).val();
		            var current_to_date = $('#to_date'+li_counter).val();
		            
		            if($('#from_date'+(li_counter-1)).val()!='') {
		            	$('#from_date'+li_counter).val($('#from_date'+(li_counter-1)).val());
		            }
		            if($('#to_date'+(li_counter-1)).val()!='') {
		            	$('#to_date'+li_counter).val($('#to_date'+(li_counter-1)).val());
		            }
		            
		            if(current_from_date!='undefined') {
		            	$('#from_date'+(li_counter-1)).val(current_from_date);
		            }
		            if(current_to_date!='undefined') {
		            	$('#to_date'+(li_counter-1)).val(current_to_date);
		            }
		            
		        }
		    });
		 </script>   
		";

		$hotels_rooms_for_storage = '';
		$package_hotels_counter=0;
		foreach ($package_hotels_rows as $package_hotels_row) {

			if($package_hotels_counter==0 && count($first_entry_date_for_trip)>0) {
				$up_down_icons='';

				$erp_hotel_id = $first_entry_date_for_trip->erp_hotel_id;
				$erp_hotel_name = $first_entry_date_for_trip->erp_hotel_name .' ('.  $first_entry_date_for_trip->erp_city_name.')'  ;

				$hotels_rooms_for_storage = $hotels_rooms_for_storage . "<div class='dv_distribution_rooms_groups_passports' id='dv_distribution_rooms_groups_passports$erp_hotel_id'>";
				$hotels_rooms_for_storage = $hotels_rooms_for_storage . " <div class='widget TAC' id='dv_hotel_title_$erp_hotel_id' style='background-color: rgb(233, 226, 212);margin-top: 5px;padding: 2px;margin-right: 0px;'>$erp_hotel_name</div>";
				$hotels_rooms_for_storage = $hotels_rooms_for_storage . " <input type='hidden' name='hdn_erp_hotel_id[]' id='hdn_erp_hotel_id' value='$erp_hotel_id' />";
				$hotels_rooms_for_storage = $hotels_rooms_for_storage . "</div>";
			} else {
				$erp_hotel_id = $package_hotels_row->$key;
				$erp_hotel_name = $package_hotels_row->$value .' ('.  $package_hotels_row->$value_city.')'  ;

				$hotels_rooms_for_storage = $hotels_rooms_for_storage . "<div class='dv_distribution_rooms_groups_passports' id='dv_distribution_rooms_groups_passports$erp_hotel_id'>";
				$hotels_rooms_for_storage = $hotels_rooms_for_storage . " <div class='widget TAC' id='dv_hotel_title_$erp_hotel_id' style='background-color: rgb(233, 226, 212);margin-top: 5px;padding: 2px;margin-right: 0px;'>$erp_hotel_name</div>";
				$hotels_rooms_for_storage = $hotels_rooms_for_storage . " <input type='hidden' name='hdn_erp_hotel_id[]' id='hdn_erp_hotel_id' value='$erp_hotel_id' />";
				$hotels_rooms_for_storage = $hotels_rooms_for_storage . "</div>";
				 
			}
			 
			$package_hotels_counter++;
		}

		$data_arr['hotels_table'] = $hotels_table;
		$data_arr['hotels_rooms_for_storage'] = $hotels_rooms_for_storage;


		return $data_arr;
		//echo json_encode($data_arr);
		//exit;
	}

	public function get_passports_by_groups()
	{
		$safa_group_ids = $this->input->post('safa_group_ids');
		$safa_group_ids_arr = explode(',', $safa_group_ids);

		foreach ($safa_group_ids_arr as $s_group_id) {

			$group_passports_rows = $this->safa_group_passports_model->get_by_groups_arr(array($s_group_id));
			$group = $this->db->where('safa_group_id', $s_group_id)->get('safa_groups')->row();

			if (count($group) > 0) {
				echo " <h2><a href=\"#\">" . $group->name . "</a></h2><div style='overflow-y:hidden;'>";
				echo "<ul class='draggable' alt='$s_group_id' style='overflow:hidden;' >";
				foreach ($group_passports_rows as $group_passports_row) {

					//--------------- safa_group_passport_id ----------------------------
					$safa_group_passport_id = $group_passports_row->safa_group_passport_id;
					//----------------------------------------------------
					 
					$this->safa_umrahgroups_passports_model->ea_id = session('ea_id');
					$this->safa_umrahgroups_passports_model->safa_group_passport_id = $safa_group_passport_id;
					$safa_umrahgroups_passports_row = $this->safa_umrahgroups_passports_model->get_for_table();
					if(count($safa_umrahgroups_passports_row)>0) {
						 
						$visa_number = $safa_umrahgroups_passports_row[0]->visa_number;
						if($visa_number=='') {

							$safa_uo_contract_id = $safa_umrahgroups_passports_row[0]->safa_uo_contract_id;
							$this->contracts_settings_model->safa_uo_contract_id = $safa_uo_contract_id;
							$contracts_settings_row = $this->contracts_settings_model->get();
							if(count($contracts_settings_row)>0) {
								$add_passports_accomodation_without_visa = $contracts_settings_row->add_passports_accomodation_without_visa;
								if($add_passports_accomodation_without_visa!=1) {
									continue;
								}
							}
						}
					}




					$li_style = "style=''";

					//--------------- Full Name -----------------------------
					if (name() == 'name_ar') {
						$group_passports_row->first_fourth_name_ar = trim($group_passports_row->first_fourth_name_ar);
						if (!empty($group_passports_row->first_fourth_name_ar)) {
							$name_by_lang = $group_passports_row->first_fourth_name_ar;
						} else {
							$name_by_lang = $group_passports_row->first_fourth_name_la;
						}
					} else {
						$group_passports_row->first_fourth_name_la = trim($group_passports_row->first_fourth_name_la);
						if (!empty($group_passports_row->first_fourth_name_la)) {
							$name_by_lang = $group_passports_row->first_fourth_name_la;
						} else {
							$name_by_lang = $group_passports_row->first_fourth_name_ar;
						}
					}
					//-----------------------------------------------------
					//--------------- Relative ----------------------------
					$relative_no_value = '';
					if ($group_passports_row->relative_no != 0) {
						$li_style = "style='background: #bbb;'";
						$relative_no_value = '- ' . $group_passports_row->relative_no;
					}
					//----------------------------------------------------


					echo "<li $li_style > $safa_group_passport_id - $name_by_lang $relative_no_value
			<input type='hidden' name='hdn_safa_group_passport_ids[]' id='hdn_safa_group_passport_ids' value='$safa_group_passport_id' />
			<input type='hidden' name='hdn_safa_group_passport_names[]' id='hdn_safa_group_passport_names' value='$name_by_lang' />
			
			<!-- <a href='javascript:void(0);' onclick='' id='lnk_li_delete_passport'><span class='icon-trash' id='lnk_li_delete_passport'></span></a> -->
                        							
			</li>";
				}
				echo "</ul></div>";
			}
		}
		exit;
	}

	public function get_available_rooms_ajax() {
		$available_rooms_count = 0;

		if (!$this->input->post('erp_hotelroomsize_id_posted')) {

			$safa_package_id = $this->input->post('safa_package_id');
			$erp_hotel_id = $this->input->post('erp_hotel_id');
			$erp_hotelroomsize_id = $this->input->post('erp_hotelroomsize_id');
			$rooms_count = $this->input->post('rooms_count');
			$erp_hotel_from_date = $this->input->post('erp_hotel_from_date');
			$erp_hotel_to_date = $this->input->post('erp_hotel_to_date');


			$this->hotels_model->join = true;
			$this->hotels_model->erp_hotel_id = $erp_hotel_id;
			$hotels_row = $this->hotels_model->get();

			$this->hotel_availability_search_model->hotel_id = $erp_hotel_id;
			$this->hotel_availability_search_model->room_type_id = $erp_hotelroomsize_id;
			$this->hotel_availability_search_model->date_from = $erp_hotel_from_date;
			$this->hotel_availability_search_model->date_to = $erp_hotel_to_date;

			$hotel_availability_rows = $this->hotel_availability_search_model->get();
			$available_rooms_count = 0;
			if (count($hotel_availability_rows) > 0) {
				foreach ($hotel_availability_rows as $hotel_availability_row) {
					$available_rooms_count = $available_rooms_count + $hotel_availability_row->available_count;
				}
			}

			$hotel_availability_arr[$hotels_row->hotel_name] = $available_rooms_count;

			$erp_hotels_availability_room_details_arr[$hotels_row->hotel_name] = $this->get_rooms_availabilities_arr($erp_hotel_id, $erp_hotelroomsize_id, $erp_hotel_from_date, $erp_hotel_to_date);
		} else {

			$safa_package_id = $this->input->post('safa_package_id');

			$erp_hotelroomsize_id = $this->input->post('erp_hotelroomsize_id_posted');


			$hotel_availability_arr = array();
			$erp_hotels_availability_room_details_arr = array();

			$erp_hotel_id_arr = $this->input->post('erp_hotel_id');
			$from_date_arr = $this->input->post('from_date');
			$to_date_arr = $this->input->post('to_date');

			if (ensure($from_date_arr)) {
				foreach ($from_date_arr as $from_date_key => $from_date_value) {

					$erp_hotel_id = $erp_hotel_id_arr[$from_date_key];
					$erp_hotel_from_date = $from_date_arr[$from_date_key];
					$erp_hotel_to_date = $to_date_arr[$from_date_key];

					$this->hotel_availability_search_model->hotel_id = $erp_hotel_id;
					$this->hotel_availability_search_model->room_type_id = $erp_hotelroomsize_id;
					$this->hotel_availability_search_model->date_from = $erp_hotel_from_date;
					$this->hotel_availability_search_model->date_to = $erp_hotel_to_date;

					$hotel_availability_rows = $this->hotel_availability_search_model->get();
					$available_rooms_count = 0;
					if (count($hotel_availability_rows) > 0) {
						foreach ($hotel_availability_rows as $hotel_availability_row) {
							$available_rooms_count = $available_rooms_count + $hotel_availability_row->available_count;
						}
					}

					$hotel_availability_arr[$erp_hotel_id] = $available_rooms_count;

					$erp_hotels_availability_room_details_arr[$erp_hotel_id] = $this->get_rooms_availabilities_arr($erp_hotel_id, $erp_hotelroomsize_id, $erp_hotel_from_date, $erp_hotel_to_date);
				}
			}
		}

		$data_arr[] = array(
            'hotel_availability_arr' => $hotel_availability_arr,
            'erp_hotels_availability_room_details_arr' => $erp_hotels_availability_room_details_arr,
		);

		echo json_encode($data_arr);
		exit;
	}

	public function apply_rooms_and_passports_ajax() {
		$arr_safa_group_passport_ids = $this->input->post('hdn_safa_group_passport_ids');

		$erp_hotelroomsizes_rows = $this->hotelroomsizes_model->get();

		if (check_array($erp_hotelroomsizes_rows)) {

			$erp_hotelroomsize_dv_width = (100 / count($erp_hotelroomsizes_rows) ) - 2;

			foreach ($erp_hotelroomsizes_rows as $erp_hotelroomsizes_row) {

				$erp_hotelroomsize_id = $erp_hotelroomsizes_row->erp_hotelroomsize_id;
				$erp_hotelroomsize_name = $erp_hotelroomsizes_row->{name()};

				$dv_style = " style='width: $erp_hotelroomsize_dv_width%; float: left; border: solid 1px; border-color:#595B5B; -moz-border-radius: 5px; border-radius: 5px; background:#F1F1F1; text-align:center ' ";

				echo "<div $dv_style>" . $erp_hotelroomsize_name . "<br/>";


				$erp_hotelroomsize_ids_inputs_arr = $this->input->post('erp_hotelroomsize_id');
				$rooms_count_inputs_arr = $this->input->post('rooms_count');
				$hdn_safa_group_passport_ids_group_arr = $this->input->post('hdn_safa_group_passport_ids_group');

				if (ensure($erp_hotelroomsize_ids_inputs_arr)) {
					foreach ($erp_hotelroomsize_ids_inputs_arr as $key => $value) {
						if ($erp_hotelroomsize_id == $erp_hotelroomsize_ids_inputs_arr[$key]) {
							$current_rooms_count = $rooms_count_inputs_arr[$key];

							$all_room_safa_group_passport_ids_arr = explode(',', $hdn_safa_group_passport_ids_group_arr[$key]);

							for ($i = 0; $i < $current_rooms_count; $i++) {
								echo " <ul class='droppable' style='width: 98%; margin:2px; z-index:0px; '>";
								echo " <input type='hidden' name='hdn_safa_group_passport_ids_group[]' value='0' />";

								for ($j = 1; $j <= $erp_hotelroomsize_id; $j++) {

									if (isset($all_room_safa_group_passport_ids_arr[$i + $j])) {
										$current_room_safa_group_passport_id = $all_room_safa_group_passport_ids_arr[$i + $j];

										echo "<li> $current_room_safa_group_passport_id
										<input type='hidden' name='hdn_safa_group_passport_ids[]' id='hdn_safa_group_passport_ids' value='$current_room_safa_group_passport_id' />
										<input type='hidden' name='hdn_safa_group_passport_names[]' id='hdn_safa_group_passport_names'  value='$current_room_safa_group_passport_id' />
										
										</li>";
									}
								}

								echo "</ul>";
							}
						}
					}
				}

				echo '</div>';
			}
		}
	}

	public function save_rooms_and_passports($id = 0) 
	{
		$arr_safa_group_passport_ids = $this->input->post('hdn_safa_group_passport_ids');
		$erp_hotelroomsizes_rows = $this->hotelroomsizes_model->get();

		//		$erp_hotel_ids = $this->input->post('erp_hotel_id');
		//		if (ensure($erp_hotel_ids)) {
		//			foreach ($erp_hotel_ids as $erp_hotel_id) {

		$erp_hotel_id_arr = $this->input->post('erp_hotel_id');
		$from_date_arr = $this->input->post('from_date');
		$to_date_arr = $this->input->post('to_date');

		if (ensure($erp_hotel_id_arr)) {
			foreach ($erp_hotel_id_arr as $key => $value) {

				$saved_group_passports_ids_arr = array();

				$erp_hotel_id = $erp_hotel_id_arr[$key];
				$from_date = $from_date_arr[$key];
				$to_date = $to_date_arr[$key];

				if (check_array($erp_hotelroomsizes_rows)) {

					$erp_hotelroomsize_dv_width = (100 / count($erp_hotelroomsizes_rows) ) - 2;

					foreach ($erp_hotelroomsizes_rows as $erp_hotelroomsizes_row) {

						$erp_hotelroomsize_id = $erp_hotelroomsizes_row->erp_hotelroomsize_id;
						$hotelroomsize_beds_count = $erp_hotelroomsizes_row->beds_count;
						$erp_hotelroomsize_name = $erp_hotelroomsizes_row->{name()};

						$dv_style = " style='width: $erp_hotelroomsize_dv_width%; float: left; border: solid 1px; border-color:#595B5B; -moz-border-radius: 5px; border-radius: 5px; background:#F1F1F1; text-align:center ' ";

						//echo "<div $dv_style>".$erp_hotelroomsize_name."<br/>";


						$erp_hotelroomsize_ids_inputs_arr = $this->input->post('erp_hotelroomsize_id');
						$rooms_count_inputs_arr = $this->input->post('rooms_count');
						$hdn_safa_group_passport_ids_group_arr = $this->input->post('hdn_safa_group_passport_ids_group');

						if (ensure($erp_hotelroomsize_ids_inputs_arr)) {
							foreach ($erp_hotelroomsize_ids_inputs_arr as $key => $value) {
								if ($erp_hotelroomsize_id == $erp_hotelroomsize_ids_inputs_arr[$key]) {
									$current_rooms_count = $rooms_count_inputs_arr[$key];

									$all_room_safa_group_passport_ids_arr = explode(',', $hdn_safa_group_passport_ids_group_arr[$key]);

									for ($i = 0; $i < $current_rooms_count; $i++) {

										//------------------- Save safa_passport_accommodation_rooms ----------------
										$this->safa_passport_accommodation_rooms_model->erp_hotelroomsize_id = $erp_hotelroomsize_id;
										$this->safa_passport_accommodation_rooms_model->erp_hotel_id = $erp_hotel_id;
										$this->safa_passport_accommodation_rooms_model->from_date = $from_date;
										$this->safa_passport_accommodation_rooms_model->to_date = $to_date;

										$this->safa_passport_accommodation_rooms_model->safa_reservation_form_id = $id;
										$safa_passport_accommodation_room_id = $this->safa_passport_accommodation_rooms_model->save();
										//---------------------------------------------------------------------------
										//echo " <ul class='droppable' style='width: 98%; margin:2px; z-index:0px; '>";
										//echo " <input type='hidden' name='hdn_safa_group_passport_ids_group[]' value='0' />";

										for ($j = 1; $j <= $hotelroomsize_beds_count; $j++) {


											$are_saved = false;

											if (isset($all_room_safa_group_passport_ids_arr[$i + $j])) {
												$current_room_safa_group_passport_id = $all_room_safa_group_passport_ids_arr[$i + $j];

												foreach ($saved_group_passports_ids_arr as $saved_group_passports_id) {
													if ($current_room_safa_group_passport_id == $saved_group_passports_id) {
														$are_saved = true;
													}
												}

												if (!$are_saved) {
													//------------------- Save safa_group_passport_accommodation ----------------
													$this->safa_group_passport_accommodation_model->safa_passport_accommodation_room_id = $safa_passport_accommodation_room_id;
													$this->safa_group_passport_accommodation_model->safa_group_passport_id = $current_room_safa_group_passport_id;
													$this->safa_group_passport_accommodation_model->safa_package_periods_id = $this->input->post('safa_package_periods_id');
													$safa_group_passport_accommodation_id = $this->safa_group_passport_accommodation_model->save();
													//---------------------------------------------------------------------------
													//------------------- Save Trip For safa_group_passport ----------------
													$this->safa_group_passports_model->safa_group_passport_id = $current_room_safa_group_passport_id;
													$this->safa_group_passports_model->safa_trip_id = $this->input->post('safa_trip_id');
													$this->safa_group_passports_model->save();
													//---------------------------------------------------------------------------


													$saved_group_passports_ids_arr[] = $current_room_safa_group_passport_id;
												}

												/*
												 echo "<li> $current_room_safa_group_passport_id
												 <input type='hidden' name='hdn_safa_group_passport_ids[]' value='$current_room_safa_group_passport_id' />
												 </li>";
												 */
											}
										}

										//echo "</ul>";
									}
								}
							}
						}

						//echo '</div>';
					}
				}
			}
		}
	}

	public function add_passports_for_edit_mode($id = 0, $safa_trip_internaltrip_id = 0) {
		$erp_hotel_id_arr = $this->input->post('erp_hotel_id');
		$from_date_arr = $this->input->post('from_date');
		$to_date_arr = $this->input->post('to_date');

		//$rooms_counter_for_all_hotels=1;

		$erp_hotelroomsize_ids_inputs_arr = $this->input->post('new_hdn_erp_hotelroomsize_ids');
		$hdn_safa_group_passport_ids_group_arr = $this->input->post('hdn_safa_group_passport_ids_group');


		if (ensure($erp_hotel_id_arr)) {
			foreach ($erp_hotel_id_arr as $hotel_key => $hotel_value) {

				for ($rooms_counter_for_all_hotels = 1; $rooms_counter_for_all_hotels <= count($erp_hotelroomsize_ids_inputs_arr); $rooms_counter_for_all_hotels++) {

					$erp_hotel_id = $erp_hotel_id_arr[$hotel_key];
					$from_date = $from_date_arr[$hotel_key];
					$to_date = $to_date_arr[$hotel_key];


					if (ensure($erp_hotelroomsize_ids_inputs_arr)) {
						foreach ($erp_hotelroomsize_ids_inputs_arr as $erp_hotelroomsize_key => $erp_hotelroomsize_value) {

							if ($erp_hotelroomsize_key == '_new_room' . $erp_hotel_id . '_' . $rooms_counter_for_all_hotels) {
								$erp_hotelroomsize_id = $erp_hotelroomsize_ids_inputs_arr[$erp_hotelroomsize_key];

								//------------------- Save safa_passport_accommodation_rooms ----------------
								$this->safa_passport_accommodation_rooms_model->safa_passport_accommodation_room_id = false;
								$this->safa_passport_accommodation_rooms_model->erp_hotelroomsize_id = $erp_hotelroomsize_id;
								$this->safa_passport_accommodation_rooms_model->erp_hotel_id = $erp_hotel_id;
								$this->safa_passport_accommodation_rooms_model->from_date = $from_date;
								$this->safa_passport_accommodation_rooms_model->to_date = $to_date;

								$this->safa_passport_accommodation_rooms_model->safa_reservation_form_id = $id;


								//---------------------- Availability Part ------------------------------------------
								if ($this->input->post('hdn_room_availability' . $erp_hotelroomsize_key)) {

									$this->hotels_availability_room_details_model->erp_hotels_availability_sub_room_id = false;
									$this->hotels_availability_room_details_model->number = false;
									$this->hotels_availability_room_details_model->from_date = false;
									$this->hotels_availability_room_details_model->to_date = false;
									$this->hotels_availability_room_details_model->status = false;

									$this->hotels_availability_room_details_model->erp_hotels_availability_room_detail_id = $this->input->post('hdn_room_availability' . $erp_hotelroomsize_key);
									$hotels_availability_room_details_row = $this->hotels_availability_room_details_model->get();

									if (count($hotels_availability_room_details_row) > 0) {

										$this->hotels_availability_room_details_model->erp_hotels_availability_room_detail_id = false;
										$this->hotels_availability_room_details_model->erp_hotels_availability_sub_room_id = $hotels_availability_room_details_row->erp_hotels_availability_sub_room_id;
										$this->hotels_availability_room_details_model->number = $hotels_availability_room_details_row->number;
										$this->hotels_availability_room_details_model->status = 2;

										$hotels_availability_room_details_row_to_check_insert = $this->hotels_availability_room_details_model->get();


										$this->hotels_availability_room_details_model->from_date = $hotels_availability_room_details_row->from_date;
										$this->hotels_availability_room_details_model->to_date = $hotels_availability_room_details_row->to_date;

										if (count($hotels_availability_room_details_row_to_check_insert) == 0) {
											$this->hotels_availability_room_details_model->save();
										}

										$this->safa_passport_accommodation_rooms_model->erp_hotels_availability_room_detail_id = $this->input->post('hdn_room_availability' . $erp_hotelroomsize_key);
									}
								} else {
									$this->safa_passport_accommodation_rooms_model->erp_hotels_availability_room_detail_id = false;
								}
								//------------------------------------------------------------------------------------



								$safa_passport_accommodation_room_id = $this->safa_passport_accommodation_rooms_model->save();
								//---------------------------------------------------------------------------


								foreach ($hdn_safa_group_passport_ids_group_arr as $safa_group_passport_id_key => $safa_group_passport_id_value) {
									//if($this->start_with($key, '_new_room'.$erp_hotel_id)) {
									if ($safa_group_passport_id_key == '_new_room' . $erp_hotel_id . '_' . $rooms_counter_for_all_hotels) {

										$all_room_safa_group_passport_ids_arr = explode(',', $safa_group_passport_id_value);
										foreach ($all_room_safa_group_passport_ids_arr as $room_safa_group_passport_id) {

											if ($room_safa_group_passport_id != '') {
												//------------------- Save safa_group_passport_accommodation ----------------
												$this->safa_group_passport_accommodation_model->safa_passport_accommodation_room_id = $safa_passport_accommodation_room_id;
												$this->safa_group_passport_accommodation_model->safa_group_passport_id = $room_safa_group_passport_id;


												if ($this->input->post('safa_package_periods_id')) {
													$this->safa_group_passport_accommodation_model->safa_package_periods_id = $this->input->post('safa_package_periods_id');
												} else {
													$this->safa_group_passport_accommodation_model->safa_package_periods_id = $this->input->post('hdn_safa_package_periods_id');
												}

												$safa_group_passport_accommodation_id = $this->safa_group_passport_accommodation_model->save();

												//---------------------------------------------------------------------------
												//------------------- Save Trip For safa_group_passport ----------------
												$this->safa_group_passports_model->safa_group_passport_id = $room_safa_group_passport_id;
												$this->safa_group_passports_model->safa_trip_id = $this->input->post('safa_trip_id');
												
												if($safa_trip_internaltrip_id!=0) {
													$this->safa_group_passports_model->safa_trip_internaltrip_id = $safa_trip_internaltrip_id;
												}
												
												$this->safa_group_passports_model->save();
												//---------------------------------------------------------------------------
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}

	public function edit_passports_for_edit_mode($id = 0, $safa_trip_internaltrip_id = 0) 
	{
		$erp_hotel_id_arr = $this->input->post('erp_hotel_id');
		$from_date_arr = $this->input->post('from_date');
		$to_date_arr = $this->input->post('to_date');
		

		if (ensure($erp_hotel_id_arr)) {
			foreach ($erp_hotel_id_arr as $hotel_key => $hotel_value) {

				$erp_hotel_id = $erp_hotel_id_arr[$hotel_key];
				$from_date = $from_date_arr[$hotel_key];
				$to_date = $to_date_arr[$hotel_key];




				$erp_hotelroomsize_ids_inputs_arr = $this->input->post('hdn_erp_hotelroomsize_ids');
				$hdn_safa_group_passport_ids_group_arr = $this->input->post('hdn_safa_group_passport_ids_group');

				if (ensure($erp_hotelroomsize_ids_inputs_arr)) {
					foreach ($erp_hotelroomsize_ids_inputs_arr as $key => $value) {


						//------------------- Save safa_passport_accommodation_rooms ----------------
						$this->safa_passport_accommodation_rooms_model->safa_passport_accommodation_room_id = $value;

						$this->safa_passport_accommodation_rooms_model->erp_hotels_availability_room_detail_id = false;
						$this->safa_passport_accommodation_rooms_model->from_date = false;
						$this->safa_passport_accommodation_rooms_model->to_date = false;

						$safa_passport_accommodation_rooms_row = $this->safa_passport_accommodation_rooms_model->get();
						if($erp_hotel_id==$safa_passport_accommodation_rooms_row->erp_hotel_id) {

							if ($this->input->post('hdn_room_availability' . $value)) {

								//---------------------- Availability Part ----------------------------

								$this->hotels_availability_room_details_model->erp_hotels_availability_sub_room_id = false;
								$this->hotels_availability_room_details_model->number = false;
								$this->hotels_availability_room_details_model->from_date = false;
								$this->hotels_availability_room_details_model->to_date = false;
								$this->hotels_availability_room_details_model->status = false;

								$this->hotels_availability_room_details_model->erp_hotels_availability_room_detail_id = $this->input->post('hdn_room_availability' . $value);
								$hotels_availability_room_details_row = $this->hotels_availability_room_details_model->get();

								if (count($hotels_availability_room_details_row) > 0) {

									$this->hotels_availability_room_details_model->erp_hotels_availability_room_detail_id = false;
									$this->hotels_availability_room_details_model->erp_hotels_availability_sub_room_id = $hotels_availability_room_details_row->erp_hotels_availability_sub_room_id;
									$this->hotels_availability_room_details_model->number = $hotels_availability_room_details_row->number;
									$this->hotels_availability_room_details_model->status = 2;

									$hotels_availability_room_details_row_to_check_insert = $this->hotels_availability_room_details_model->get();


									$this->hotels_availability_room_details_model->from_date = $hotels_availability_room_details_row->from_date;
									$this->hotels_availability_room_details_model->to_date = $hotels_availability_room_details_row->to_date;

									if (count($hotels_availability_room_details_row_to_check_insert) == 0) {
										$this->hotels_availability_room_details_model->save();
									}
									$this->safa_passport_accommodation_rooms_model->erp_hotels_availability_room_detail_id = $this->input->post('hdn_room_availability' . $value);
								}
								//---------------------------------------------------------------------
							} else {
								$this->safa_passport_accommodation_rooms_model->erp_hotels_availability_room_detail_id = false;
							}
							$this->safa_passport_accommodation_rooms_model->from_date = $from_date;
							$this->safa_passport_accommodation_rooms_model->to_date = $to_date;
							$safa_passport_accommodation_room_id = $this->safa_passport_accommodation_rooms_model->save();
							//---------------------------------------------------------------------------


							if (isset($hdn_safa_group_passport_ids_group_arr[$key . '-' . $erp_hotel_id])) {
								$all_room_safa_group_passport_ids_arr = explode(',', $hdn_safa_group_passport_ids_group_arr[$key . '-' . $erp_hotel_id]);

								foreach ($all_room_safa_group_passport_ids_arr as $room_safa_group_passport_id) {

									if ($room_safa_group_passport_id != '') {
										//------------------- Save safa_group_passport_accommodation ----------------
										$this->safa_group_passport_accommodation_model->safa_passport_accommodation_room_id = $value;
										$this->safa_group_passport_accommodation_model->safa_group_passport_id = $room_safa_group_passport_id;
										$current_safa_group_passport_id_rows = $this->safa_group_passport_accommodation_model->get();
										if (count($current_safa_group_passport_id_rows) == 0) {
											$this->safa_group_passport_accommodation_model->safa_package_periods_id = $this->input->post('hdn_safa_package_periods_id');
											$safa_group_passport_accommodation_id = $this->safa_group_passport_accommodation_model->save();
										}
										//---------------------------------------------------------------------------
										//------------------- Save Trip For safa_group_passport ----------------
										$this->safa_group_passports_model->safa_group_passport_id = $room_safa_group_passport_id;
										$this->safa_group_passports_model->safa_trip_id = $this->input->post('safa_trip_id');
										if($safa_trip_internaltrip_id!=0) {
											$this->safa_group_passports_model->safa_trip_internaltrip_id = $safa_trip_internaltrip_id;
										}
										$this->safa_group_passports_model->save();
										//---------------------------------------------------------------------------
									}
								}


								//------------- Delete from room ----------------------------
								$deleted_rows = $this->safa_group_passport_accommodation_model->delete_by_room_id_and_ids_arr_not_in($value, $all_room_safa_group_passport_ids_arr);

								//------------------------------ Delete From internaltrips ------------------------------
								foreach($deleted_rows as $deleted_row) {
									$this->safa_group_passports_model->safa_group_passport_id = $deleted_row->safa_group_passport_id;
									$this->safa_group_passports_model->safa_trip_internaltrip_id = null;
									$this->safa_group_passports_model->save();
									$this->safa_group_passports_model->safa_trip_internaltrip_id = false;
								}
								//---------------------------------------------------------------------------------------
							}
						}

					}
				}
			}
		}
	}

	public function delete_safa_passport_accommodation_rooms_for_edit_mode() {
		// --- Delete Deleted rows (rooms) from Database --------------

		$safa_passport_accommodation_rooms_remove = $this->input->post('safa_passport_accommodation_rooms_remove');
		if (ensure($safa_passport_accommodation_rooms_remove)) {
			foreach ($safa_passport_accommodation_rooms_remove as $safa_passport_accommodation_room_remove) {
				$this->safa_passport_accommodation_rooms_model->safa_passport_accommodation_room_id = $safa_passport_accommodation_room_remove;
				$this->safa_passport_accommodation_rooms_model->delete();
			}
		}
		//---------------------------------------------------------------
	}

	public function get_from_date_first_hotel_by_trip_ajax($safa_trip_id = 0) {
		$this->safa_trips_model->safa_trip_id = $safa_trip_id;
		$safa_trip_row = $this->safa_trips_model->get();
		if (count($safa_trip_row) == 0) {
			echo '';
		} else {
			echo $safa_trip_row->arrival_date;
		}
		exit;
	}

	public function get_from_to_date_first_hotel_by_trip_and_package_period_ajax($safa_trip_id = 0, $safa_package_periods_id = 0, $erp_hotel_id = 0) {
		$from_date = '';
		$to_date = '';
		$dates_arr = array();

		$this->safa_trips_model->safa_trip_id = $safa_trip_id;
		$safa_trip_row = $this->safa_trips_model->get();
		if (count($safa_trip_row) != 0) {
			$from_date = $safa_trip_row->arrival_date;
		}

		$this->package_periods_cities_model->erp_package_period_city_id = $safa_package_periods_id;
		$this->package_periods_cities_model->erp_city_id = $erp_hotel_id;
		$package_periods_cities_rows = $this->package_periods_cities_model->get();
		if (count($package_periods_cities_rows) != 0 && $from_date != '') {

			$to_date = $from_date + $package_periods_cities_rows[0]->city_days;
		}

		$dates_arr[] = array('from_date' => $from_date,
            'to_date' => $to_date,
		);
		echo json_encode($dates_arr);
		exit;
	}

	public function get_to_date_by_from_date_and_package_period_ajax() {
		$safa_package_periods_id = $this->input->post('safa_package_periods_id');
		$from_date = $this->input->post('from_date');
		$erp_hotel_id = $this->input->post('erp_hotel_id');

		$to_date = '';

		$this->hotels_model->erp_hotel_id = $erp_hotel_id;
		$hotel_row = $this->hotels_model->get();
		if (count($hotel_row) > 0) {

			$this->package_periods_cities_model->safa_package_period_id = $safa_package_periods_id;
			$this->package_periods_cities_model->erp_city_id = $hotel_row->erp_city_id;
			$package_periods_cities_rows = $this->package_periods_cities_model->get();

			if (count($package_periods_cities_rows) != 0 && $from_date != '') {
				$to_date = date('Y-m-d', strtotime($from_date . ' + ' . $package_periods_cities_rows[0]->city_days . ' days'));
			}
		}

		echo $to_date;
		exit;
	}

	public function start_with($s, $prefix) {
		return strpos($s, $prefix) === 0;
	}

	function delete($id = false) {
		if (!$id)
		show_404();

		$this->safa_reservation_forms_model->safa_reservation_form_id = $id;

		$safa_reservation_forms_row = $this->safa_reservation_forms_model->get();
		if (count($safa_reservation_forms_row) > 0) {
			$this->safa_trips_model->safa_trip_id = $safa_reservation_forms_row->safa_trip_id;
			$safa_trips_row = $this->safa_trips_model->get();
			if ($safa_trips_row->safa_tripstatus_id == 3 || $safa_trips_row->safa_tripstatus_id == 4) {
				show_404();
			}
		}

		if (!$this->safa_reservation_forms_model->delete())
		show_404();
		$this->load->view('redirect_message', array('msg' => lang('global_deleted_message'),
            'url' => site_url('ea/passport_accommodation_rooms'),
            'model_title' => lang('passport_accommodation_rooms'), 'action' => lang('delete_title')));
	}

	function room_availability_popup($hdn_room_availability_id = 0, $erp_hotel_id = 0, $erp_hotelroomsize_id = 0) {
		$this->layout = 'js_new';

		$data['hdn_room_availability_id'] = $hdn_room_availability_id;
		$data['erp_hotel_id'] = $erp_hotel_id;
		$data['erp_hotelroomsize_id'] = $erp_hotelroomsize_id;

		$this->load->view('ea/passport_accommodation_rooms/room_availability_popup', $data);
	}

	function get_rooms_availabilities_ajax() {
		$this->layout = 'js_new';

		$erp_hotel_id = $this->input->post('erp_hotel_id');
		$erp_hotelroomsize_id = $this->input->post('erp_hotelroomsize_id');
		$erp_hotel_from_date = $this->input->post('erp_hotel_from_date');
		$erp_hotel_to_date = $this->input->post('erp_hotel_to_date');


		$this->hotels_model->join = true;
		$this->hotels_model->erp_hotel_id = $erp_hotel_id;
		$hotels_row = $this->hotels_model->get();

		$this->hotel_availability_search_model->hotel_id = $erp_hotel_id;
		$this->hotel_availability_search_model->room_type_id = $erp_hotelroomsize_id;
		$this->hotel_availability_search_model->date_to = $erp_hotel_to_date;
		$this->hotel_availability_search_model->date_from = $erp_hotel_from_date;
		$this->hotel_availability_search_model->date_to = $erp_hotel_to_date;

		$hotel_availability_rows_reserved = $this->hotel_availability_search_model->get_availability_room_details(2);
		$hotel_availability_rows = $this->hotel_availability_search_model->get_availability_room_details(1);

		echo "
		<table cellpadding='0' class='' cellspacing='0' width='100%' >
        <thead>
        <tr>
        <th>" . lang('room_number') . "</th>
		</tr>
        </thead>
        <tbody>";

		foreach ($hotel_availability_rows as $hotel_availability_row) {

			//-------------------------------------------------------------------------------
			$are_reserved = false;
			foreach ($hotel_availability_rows_reserved as $hotel_availability_row_reserved) {
				if ($hotel_availability_row->erp_hotels_availability_sub_room_id == $hotel_availability_row_reserved->erp_hotels_availability_sub_room_id && $hotel_availability_row->number == $hotel_availability_row_reserved->number) {
					$are_reserved = true;
					break;
				}
			}
			if ($are_reserved) {
				continue;
			}
			//-------------------------------------------------------------------------------


			$erp_hotels_availability_room_detail_id = $hotel_availability_row->erp_hotels_availability_room_detail_id;
			$number = $hotel_availability_row->number;
			echo "
			<tr>
	        <td>
	        <a href='javascript:void(0);' onclick='return select_room_availability($erp_hotels_availability_room_detail_id, $number, $erp_hotel_id);' >$number</a>
			</td>
			</tr>";
		}

		echo "
		</tbody>
        </table>
		";

		exit;
	}

	function get_rooms_availabilities_arr($erp_hotel_id, $erp_hotelroomsize_id, $erp_hotel_from_date, $erp_hotel_to_date) {

		$this->hotels_model->join = true;
		$this->hotels_model->erp_hotel_id = $erp_hotel_id;
		$hotels_row = $this->hotels_model->get();

		$this->hotel_availability_search_model->hotel_id = $erp_hotel_id;
		$this->hotel_availability_search_model->room_type_id = $erp_hotelroomsize_id;
		$this->hotel_availability_search_model->date_to = $erp_hotel_to_date;
		$this->hotel_availability_search_model->date_from = $erp_hotel_from_date;
		$this->hotel_availability_search_model->date_to = $erp_hotel_to_date;

		$hotel_availability_rows_reserved = $this->hotel_availability_search_model->get_availability_room_details(2);
		$hotel_availability_rows = $this->hotel_availability_search_model->get_availability_room_details(1);

		$hotel_availability_arr = array();
		foreach ($hotel_availability_rows as $hotel_availability_row) {

			//-------------------------------------------------------------------------------
			$are_reserved = false;
			foreach ($hotel_availability_rows_reserved as $hotel_availability_row_reserved) {
				if ($hotel_availability_row->erp_hotels_availability_sub_room_id == $hotel_availability_row_reserved->erp_hotels_availability_sub_room_id && $hotel_availability_row->number == $hotel_availability_row_reserved->number) {
					$are_reserved = true;
					break;
				}
			}
			if ($are_reserved) {
				continue;
			}
			//-------------------------------------------------------------------------------


			$erp_hotels_availability_room_detail_id = $hotel_availability_row->erp_hotels_availability_room_detail_id;
			$number = $hotel_availability_row->number;

			$hotel_availability_arr[$erp_hotels_availability_room_detail_id] = $number;
		}
		return $hotel_availability_arr;
	}

}
