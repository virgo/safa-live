<?php

if (!defined('BASEPATH'))
exit('No direct script access allowed');

class All_movements extends Safa_Controller {

	public $module = "trips_reports";

	public function __construct() {
		parent::__construct();

		//Side menu session, By Gouda.
		session('side_menu_id', 6);

		$this->layout = 'new';

		$this->load->model('trips_reports_model');
		$this->load->model('safa_uo_users_model');

		//By Gouda
		$this->load->model('notification_model');
		$this->load->model('internalsegments_drivers_and_buses_model');
		$this->load->model('internalpassages_model');
		$this->load->model('transporters_drivers_model');
		$this->load->model('transporters_buses_model');
		$this->load->model('trip_internaltrip_model');
		$this->load->model('internalsegments_drivers_and_buses_model');

		$this->load->model('hotels_model');
		$this->load->model('ports_model');
		$this->load->model('tourismplaces_model');


		$this->lang->load('trips_reports');
		$this->load->helper('trip');
		$this->load->helper('array');

		$this->load->library('flight_states');

		permission();

		if (session('ea_id')) {
			$this->ea_id = session('ea_id');
			$this->trip_internaltrip_model->ea_id =session('ea_id');
		}
	}

	public function index()
	{

		$safa_uo_contracts_rows = $this->trip_internaltrip_model->get_ea_contracts();
		$safa_uo_contracts_arr = array();
		$safa_uo_contracts_arr[""] = lang('global_select_from_menu');
		foreach ($safa_uo_contracts_rows as $safa_uo_contracts_row) {
			$safa_uo_contracts_arr[$safa_uo_contracts_row->contract_id] = $safa_uo_contracts_row->safa_uo_contracts_eas_name;
		}
		$data["safa_uo_contracts"] = $safa_uo_contracts_arr;
			
		if (!$_POST) {
			$this->trips_reports_model->from_date = date("Y-m-d");
			$this->trips_reports_model->to_date = date("Y-m-d");
		}

		//from_date
		if ($this->input->post('from_city')) {
			$this->trips_reports_model->erp_city_id = $this->input->post('from_city');
		}

		//to_date
		if ($this->input->post('to_city')) {
			$this->trips_reports_model->end_erp_city_id = $this->input->post('to_city');
		}


		//from_date
		if ($this->input->post('from_date')) {
			$this->trips_reports_model->from_date = $this->input->post('from_date');
		}

		//to_date
		if ($this->input->post('to_date')) {
			$this->trips_reports_model->to_date = $this->input->post('to_date');
		}

		//from_time
		if ($this->input->post('from_time')) {
			$this->trips_reports_model->from_time = $this->input->post('from_time');
		}

		//to_time
		if ($this->input->post('to_time')) {
			$this->trips_reports_model->to_time = $this->input->post('to_time');
		}

		//from_count
		if ($this->input->post('from_count')) {
			$this->trips_reports_model->from_count = $this->input->post('from_count');
		}

		//safa_ito_id
		if ($this->input->post('safa_ito_id')) {
			$this->trips_reports_model->safa_ito_id = $this->input->post('safa_ito_id');
		}

		//safa_internalsegmenttype_id
		if ($this->input->post('safa_internalsegmenttype_id')) {
			$this->trips_reports_model->safa_internalsegmenttype_id = $this->input->post('safa_internalsegmenttype_id');
		}

		//operator_reference
		if ($this->input->post('operator_reference')) {
			$this->trips_reports_model->operator_reference = $this->input->post('operator_reference');
		}

		if ($this->input->post('safa_uo_contract_id')) {
			$this->trips_reports_model->safa_uo_contract_id = $this->input->post('safa_uo_contract_id');
		}


		//from_erp_hotel_id
		if ($this->input->post('from_erp_hotel_id')) {
			$this->trips_reports_model->from_erp_hotel_id = $this->input->post('from_erp_hotel_id');
		}

		//to_erp_hotel_id
		if ($this->input->post('to_erp_hotel_id')) {
			$this->trips_reports_model->to_erp_hotel_id = $this->input->post('to_erp_hotel_id');
		}

		//erp_port_id
		if ($this->input->post('erp_port_id')) {
			$this->trips_reports_model->erp_port_id = $this->input->post('erp_port_id');
		}

		if ($this->input->post('safa_internalsegmentestatus_id')) {
			$this->trips_reports_model->safa_internalsegmentestatus_id = $this->input->post('safa_internalsegmentestatus_id');
		}
		/* implementiming the segments for diff agents */
		if (session('ea_id'))
		$this->trips_reports_model->ea_id = session('ea_id');

		if ($this->input->post('safa_uo_user_id'))
		$this->trips_reports_model->uo_user_id = $this->input->post('safa_uo_user_id');

		/**         * ***************************************** */
		$data['trips_ds'] = $all_movements_rows = $this->trips_reports_model->all_movements();


		//-------------- Get All dirvers and buses for these segments -------------------------------------------
		//        $safa_internalsegments_arr=array();
		//        foreach($all_movements_rows as $all_movements_row){
		//        	$safa_internalsegments_arr[] = $all_movements_row->safa_internalsegment_id;
		//        }
		//        $this->internalsegments_drivers_and_buses_model->safa_internalsegments_arr = $safa_internalsegments_arr;
		//        $data['trips_drivers'] = $this->internalsegments_drivers_and_buses_model->get();
		//-------------------------------------------------------------------------------------------------------


		//echo $this->db->last_query();die();

		//$uo_id = session('uo_id');
		$cols = array();
		$uo_user_id = session('uo_user_id');
		if ($this->input->post('cols')) {
			 
			$cols_uo_unset = array('safa_trip_internaltrip_serial', 'contract', 'code', 'supervisor', 'operator', 'confirmation_number', 'operation_order_id', 'buses_count', 'segment_type', 'starting', 'ending', 'from', 'to', 'seats', 'status', 'agent', 'individuals_count', 'passports_count' , 'drivers');
			foreach($cols_uo_unset as $col_uo_unset) {
				setcookie(''. $uo_user_id . '_' . $col_uo_unset . '', '' . $col_uo_unset . '', time()-3600);
			}
			 
			$cols = $this->input->post('cols');
			foreach($cols as $col) {
				setcookie('' . $uo_user_id . '_' . $col . '', '' . $col . '', time() + 60 * 60 * 24 * 365);
			}

		} else {
			$cols = array('safa_trip_internaltrip_serial', 'contract', 'code', 'supervisor', 'operator', 'confirmation_number', 'operation_order_id','buses_count', 'segment_type', 'starting', 'ending', 'from', 'to', 'seats', 'status', 'agent', 'individuals_count', 'passports_count' , 'drivers');
		}



		$colums = array();
		foreach ($cols as $col) {
			if ($this->input->cookie('' . $uo_user_id . '_' . $col . ''))
			$colums[] = $this->input->cookie('' . $uo_user_id . '_' . $col . '');
		}

		if (!empty($colums) && count($colums) >= 1 && is_array($colums)) {
			$data['cols'] = $colums;
		} else {
			$data['cols'] = $cols;
		}



		$data['hotels'] = $this->trips_reports_model->get_hotels();
		$data['places'] = $this->trips_reports_model->get_places();
		$data['ports'] = $this->trips_reports_model->get_ports();

		if ($this->input->post('excel_export')) {
			$this->layout = 'js';
			$data['print_statement'] = "";
			$this->load->helper('download');
			$data = $this->load->view("ea/all_movements/excel", $data, TRUE);
			$name = 'all-' . date('Y-m-d-His') . '.xls';
			force_download($name, $data);
		} elseif ($this->input->post('print_report')) {
			$this->layout = 'js';
			$data['print_statement'] = "<script>print()</script>";
			$this->load->view("ea/all_movements/excel", $data);
		} else {
			$this->load->view("ea/all_movements/index", $data);
		}
	}

	function get_uo_usergroup($user_id) {

		//        if (!permission('all_movements_get_uo_usergroup'))
		//                show_message(lang('global_you_dont_have_permission'));

		$this->load->model('safa_uo_users_model');
		$this->safa_uo_users_model->safa_uo_user_id = $user_id;
		$this->safa_uo_users_model->custom_select = array('safa_uo_users.safa_uo_usergroup_id');
		$uo_user_usergroup_id = $this->safa_uo_users_model->get()->safa_uo_usergroup_id;
		return $uo_user_usergroup_id;
	}

	function select_agent_ajax() {
		//            if (!permission('all_movements_select_agent_ajax'))
		//                show_message(lang('global_you_dont_have_permission'));
		$this->layout = 'ajax';
		$uo_user_id = $this->input->post('uo_user_id');
		$this->load->model('safa_uo_users_model');
		$this->safa_uo_users_model->safa_uo_id = session('uo_id');


		// Update By Gouda.
		//$drop_list = form_dropdown('safa_uo_user_change', ddgen('safa_uo_users', array('safa_uo_user_id', name()), array('safa_uo_usergroup_id' => AGENT_USERGROUP, "safa_uo_id" => session("uo_id"))) , $uo_user_id);

		$safa_internalsegmenttype_id = $this->input->post('safa_internalsegmenttype_id');
		$from_place = $this->input->post('from_place');
		//$to_place = $this->input->post('to_place');

		$this->safa_uo_users_model->safa_uo_id = session('uo_id');
		if($safa_internalsegmenttype_id==1) {
			$this->ports_model->erp_port_id = $from_place;
			$port_row = $this->ports_model->get();

			$this->safa_uo_users_model->safa_uo_user_type_id_in = array(2,3);
			$this->safa_uo_users_model->erp_city_id =$port_row->erp_city_id;

			if(count($port_row)>0) {
				if($port_row->safa_transportertype_id==2) {
					$this->safa_uo_users_model->port_type_in=array('all','air');
				} else if($port_row->safa_transportertype_id==3) {
					$this->safa_uo_users_model->port_type_in==array('all','sea');
				}
			}

		} else {
			$this->safa_uo_users_model->safa_uo_user_type_id_in = array(1,3);
			 
			$this->hotels_model->erp_hotel_id = $from_place;
			$hotel_row = $this->hotels_model->get();
			if(count($hotel_row)>0) {
				$this->safa_uo_users_model->erp_city_id =$hotel_row->erp_city_id;
			}
		}
		$safa_uo_users_rows = $this->safa_uo_users_model->get();

		$safa_uo_users_arr = array();
		foreach($safa_uo_users_rows as $safa_uo_users_row) {
			$safa_uo_users_arr[$safa_uo_users_row->safa_uo_user_id]= $safa_uo_users_row->{name()};
		}

		$drop_list = form_dropdown('safa_uo_user_change', $safa_uo_users_arr , $uo_user_id);
		 


		echo $drop_list;
		if ($uo_user_id == 0)
		$set_agent_label = lang('assign_uo_user');
		else
		$set_agent_label = lang('change_uo_user');
		echo "<br>";
		echo form_button('set_agent', $set_agent_label, 'class="btn btn-mini"');
	}

	function set_agent_segment_ajax() {
		//            if (!permission('all_movements_set_agent_segment_ajax'))
		//                    show_message(lang('global_you_dont_have_permission'));
		$this->layout = 'ajax';
		$segment_id = $this->input->post('segment_id');
		$agent_id = $this->input->post('agent_id');
		$this->load->model('internalpassages_model');
		$this->internalpassages_model->safa_internalsegment_id = $segment_id;
		if ($agent_id == 0)
		$this->internalpassages_model->safa_uo_user_id = NULL;
		ELSE
		$this->internalpassages_model->safa_uo_user_id = $agent_id;
		$updated = $this->internalpassages_model->save();
		if ($updated > 0) {
			if ($agent_id != 0) {
				echo item('safa_uo_users', name(), array('safa_uo_users.safa_uo_user_id' => $agent_id));
				echo "<br>";
				echo"<a  href='javascript:void(0)' class='icon-pencil' title=" . lang('change_uo_user') . " onclick='get_agent_view(" . $agent_id . ",this)'></a>";
			} else {
				echo lang('uo_user_agent_notexist');
				echo "<br>";
				echo"<a  href='javascript:void(0)' class='icon-pencil' title=" . lang('assign_uo_user') . " onclick='get_agent_view(" . $agent_id . ",this)'></a>";
			}
		} else {
			echo "0";
		}
	}

	function rollback_agent_segment_ajax() {
		//        if (!permission('all_movements_rollback_agent_segment_ajax'))
		//                    show_message(lang('global_you_dont_have_permission'));
		$this->layout = 'ajax';
		$agent_id = $this->input->post('uo_id');
		if ($agent_id != 0) {
			echo item('safa_uo_users', name(), array('safa_uo_users.safa_uo_user_id' => $agent_id));
			echo "<br>";
			echo"<a  href='javascript:void(0)' class='icon-pencil' title=" . lang('change_uo_user') . " onclick='get_agent_view(" . $agent_id . ",this)'></a>";
		} else {
			echo lang('uo_user_agent_notexist');
			echo "<br>";
			echo"<a  href='javascript:void(0)' class='icon-pencil' title=" . lang('assign_uo_user') . " onclick='get_agent_view(" . $agent_id . ",this)'></a>";
		}
	}

	function update_segment_status() {
		//        if (!permission('all_movements_update_segment_status'))
		//                    show_message(lang('global_you_dont_have_permission'));
		$this->layout = 'ajax';
		$agent_id = $this->input->post('safa_uo_users');
		$internal_segment_id = $this->input->post('internal_segment_id');
		$segment_notes = $this->input->post('update_segment_status_notes');
		$uo_user_id = session('uo_user_id');
		$status_id = $this->input->post('internal_segment_status');
		/* set the log record into table safa_internalsegemnt_log */
		$this->db->set('safa_internalsegment_id', $internal_segment_id);
		$this->db->set('safa_uo_user_id', $uo_user_id);
		$this->db->set('safa_uo_agent_id', $agent_id);
		$this->db->set('safa_internalsegmentstatus_id', $status_id);
		$this->db->set('notes', $segment_notes);
		$effected_row = $this->db->insert('safa_internalsegment_log');

		// updating  the internal  passages//
		if ($effected_row > 0) {
			$this->load->model('internalpassages_model');
			$this->internalpassages_model->safa_internalsegment_id = $internal_segment_id;
			$this->internalpassages_model->safa_internalsegmentestatus_id = $status_id;
			$this->internalpassages_model->safa_uo_user_id = $agent_id;
			if ($segment_notes) {
				$this->internalpassages_model->status_notes = $segment_notes;
			}

			//By Gouda
			//$updated = $this->internalpassages_model->save();


			//By Gouda, To send notification
			//-----------------------------------------------------------------------------------------------------
			$erp_system_events_id = 11;

			$internalsegment_row = $this->db->select('safa_internalsegments.*, safa_trips.safa_ea_id
					 , safa_packages.erp_company_id
					 ,safa_trip_internaltrips.safa_ito_id
					 , safa_trips.safa_trip_id 
					 , safa_trips.name as trip_name
					 , safa_trips.date')
			->where('safa_internalsegments.safa_internalsegment_id', $internal_segment_id)
			->join('safa_trip_internaltrips', 'safa_trip_internaltrips.safa_trip_internaltrip_id = safa_internalsegments.safa_trip_internaltrip_id', 'left')
			->join('safa_trips', 'safa_trips.safa_trip_id = safa_trip_internaltrips.safa_trip_id', 'left')
			->join('safa_group_passports', 'safa_group_passports.safa_trip_id = safa_trips.safa_trip_id', 'left')
			->join('safa_group_passport_accommodation', 'safa_group_passport_accommodation.safa_group_passport_id = safa_group_passports.safa_group_passport_id', 'left')
			->join('safa_package_periods', 'safa_package_periods.safa_package_periods_id = safa_group_passport_accommodation.safa_package_periods_id', 'left')
			->join('safa_packages', 'safa_packages.safa_package_id = safa_package_periods.safa_package_id', 'left')
			->get('safa_internalsegments')
			->row();



			$msg_datetime = date('Y-m-d H:i:s', time());

			$trip_id = $internalsegment_row->safa_trip_id;
			$trip_name = $internalsegment_row->trip_name;
			$trip_date = $internalsegment_row->date;
			$the_date = $msg_datetime;
			$hotel_arrival_datetime = $internalsegment_row->end_datetime;
			$hotel_leaving_datetime = $internalsegment_row->start_datetime;

			$port_id = $internalsegment_row->erp_port_id;
			$start_hotel = $internalsegment_row->erp_start_hotel_id;
			$end_hotel = $internalsegment_row->erp_end_hotel_id;
			$tourism_place = $internalsegment_row->safa_tourism_place_id;

			$from_place = '';
			$to_place = '';

			if ($status_id == 3) {
				if ($internalsegment_row->safa_internalsegmenttype_id == 1) {
					$from_place_row = $this->db->select('erp_ports.name_la, erp_ports.name_ar , erp_ports.erp_city_id')
					->where('erp_port_id', $port_id)
					->join('erp_cities', 'erp_cities.erp_city_id = erp_ports.erp_city_id', 'left')
					->get('erp_ports')
					->row();

					if (name() == 'name_la') {
						$from_place = $from_place_row->name_la;
					} else {
						$from_place = $from_place_row->name_ar;
					}


					$to_place_row = $this->db->select('erp_hotels.name_la, erp_hotels.name_ar, erp_cities.name_ar as city_name_ar, erp_cities.erp_city_id')
					->where('erp_hotel_id', $end_hotel)
					->join('erp_cities', 'erp_cities.erp_city_id = erp_hotels.erp_city_id', 'left')
					->get('erp_hotels')
					->row();

					if (name() == 'name_la') {
						$to_place = $to_place_row->name_la;
					} else {
						$to_place = $to_place_row->name_ar;
					}
				} else if ($internalsegment_row->safa_internalsegmenttype_id == 2) {

					$from_place_row = $this->db->select('erp_hotels.name_la, erp_hotels.name_ar, erp_cities.name_ar as city_name_ar, erp_cities.erp_city_id')
					->where('erp_hotel_id', $start_hotel)
					->join('erp_cities', 'erp_cities.erp_city_id = erp_hotels.erp_city_id', 'left')
					->get('erp_hotels')
					->row();

					if (name() == 'name_la') {
						$from_place = $from_place_row->name_la;
					} else {
						$from_place = $from_place_row->name_ar;
					}


					$to_place_row = $this->db->select(' erp_ports.name_la, erp_ports.name_ar , erp_ports.erp_city_id')
					->where('erp_port_id', $port_id)
					->join('erp_cities', 'erp_cities.erp_city_id = erp_ports.erp_city_id', 'left')
					->get('erp_ports')
					->row();

					if (name() == 'name_la') {
						$to_place = $to_place_row->name_la;
					} else {
						$to_place = $to_place_row->name_ar;
					}
				} else if ($internalsegment_row->safa_internalsegmenttype_id == 3) {

					$from_place_row = $this->db->select('erp_hotels.name_la, erp_hotels.name_ar, erp_cities.name_ar as city_name_ar, erp_cities.erp_city_id')
					->where('erp_hotel_id', $start_hotel)
					->join('erp_cities', 'erp_cities.erp_city_id = erp_hotels.erp_city_id', 'left')
					->get('erp_hotels')
					->row();

					if (name() == 'name_la') {
						$from_place = $from_place_row->name_la;
					} else {
						$from_place = $from_place_row->name_ar;
					}


					$to_place_row = $this->db->select('safa_tourismplaces.name_la, safa_tourismplaces.name_ar , safa_tourismplaces.erp_city_id')
					->where('safa_tourismplace_id', $tourism_place)
					->join('erp_cities', 'erp_cities.erp_city_id = safa_tourismplaces.erp_city_id', 'left')
					->get('safa_tourismplaces')
					->row();

					if (name() == 'name_la') {
						$to_place = $to_place_row->name_la;
					} else {
						$to_place = $to_place_row->name_ar;
					}
				} else if ($internalsegment_row->safa_internalsegmenttype_id == 4) {

					$from_place_row = $this->db->select('erp_hotels.name_la, erp_hotels.name_ar, erp_cities.name_ar as city_name_ar, erp_cities.erp_city_id')
					->where('erp_hotel_id', $start_hotel)
					->join('erp_cities', 'erp_cities.erp_city_id = erp_hotels.erp_city_id', 'left')
					->get('erp_hotels')
					->row();

					if (name() == 'name_la') {
						$from_place = $from_place_row->name_la;
					} else {
						$from_place = $from_place_row->name_ar;
					}

					$to_place_row = $this->db->select('erp_hotels.name_la, erp_hotels.name_ar, erp_cities.name_ar as city_name_ar, erp_cities.erp_city_id')
					->where('erp_hotel_id', $end_hotel)
					->join('erp_cities', 'erp_cities.erp_city_id = erp_hotels.erp_city_id', 'left')
					->get('erp_hotels')
					->row();

					if (name() == 'name_la') {
						$to_place = $to_place_row->name_la;
					} else {
						$to_place = $to_place_row->name_ar;
					}
				}
			}

			//----------- Send Notification For Company ------------------------------

			if ($erp_system_events_id != 0) {
				$msg_datetime = date('Y-m-d H:i:s', time());

				$this->notification_model->notification_type = 'automatic';
				$this->notification_model->erp_importance_id = 1;
				$this->notification_model->sender_type_id = 1;
				$this->notification_model->sender_id = 0;

				$this->notification_model->erp_system_events_id = $erp_system_events_id;

				$this->notification_model->language_id = 2;
				$this->notification_model->msg_datetime = $msg_datetime;

				//Link to replace.
				$link_trip_name = "<a href='" . site_url('ea/trips/form/' . $trip_id) . "'  target='_blank'> " . $trip_name . " </a>";

				$this->notification_model->tags = "#*trip#*=$link_trip_name*****#*trip_date#*=$trip_date*****#*the_date#*=$the_date*****#*hotel_arrival_datetime#*=$hotel_arrival_datetime*****#*hotel_leaving_datetime#*=$hotel_leaving_datetime*****#*from_place#*=$from_place*****#*to_place#*=$to_place";
				$this->notification_model->parent_id = '';

				$erp_notification_id = $this->notification_model->save();

				//-------- Notification Details ---------------
				if ($internalsegment_row->safa_ea_id) {
					$this->notification_model->detail_erp_notification_id = $erp_notification_id;
					$this->notification_model->detail_receiver_type = 2;
					$this->notification_model->detail_receiver_id = $internalsegment_row->safa_ea_id;
					$this->notification_model->saveDetails();
				}
				//----------------------------------------------
				//-------- Notification Details ---------------
				$this->notification_model->detail_erp_notification_id = $erp_notification_id;
				$this->notification_model->detail_receiver_type = 3;
				if ($internalsegment_row->erp_company_id != '') {
					$this->notification_model->detail_receiver_id = $internalsegment_row->erp_company_id;
					$this->notification_model->saveDetails();
				}
				//----------------------------------------------
				//-------- Notification Details ---------------
				$this->notification_model->detail_erp_notification_id = $erp_notification_id;
				$this->notification_model->detail_receiver_type = 4;
				if ($internalsegment_row->safa_ito_id != '') {
					$this->notification_model->detail_receiver_id = $internalsegment_row->safa_ito_id;
					$this->notification_model->saveDetails();
				}
				//----------------------------------------------
			}
			//-------------------------------------------------------------------
			//-------------------------------------------------------------------------------------------------




			$response = array();
			$response['updated'] = 1;
			$response['content'] = item('safa_internalsegmentstatus', name(), array('safa_internalsegmentstatus.safa_internalsegmentstatus_id' => $status_id));
			$response['content'].="<br>";
			$response['content'].="<a class='icon-pencil' title='" . lang('update_segment_status') . "'  href='#edit-segment-status'
                        onclick='openStatusChangepanal(" . $internal_segment_id . "," . $status_id . ")'
                        role='button' data-toggle='modal'></a>";
			if ($segment_notes)
			$response['notes'] = $segment_notes;
		}
		else {
			$response['updated'] = 0;
		}
		echo json_encode($response);
	}

	function get_segment_agent($segment_id) {
		//        if (!permission('all_movements_get_segment_status'))
		//                    show_message(lang('global_you_dont_have_permission'));
		$this->layout = 'ajax';
		$returndata = array();
		$this->load->model('internalpassages_model');
		$this->internalpassages_model->safa_internalsegment_id = $segment_id;
		//$this->internalpassages_model->custom_select = "safa_internalsegments.safa_uo_user_id";



		//------------------------------------------------- By Gouda -------------------------------------
		$internalpassages_row = $this->internalpassages_model->get();

		$safa_internalsegmenttype_id = $internalpassages_row->safa_internalsegmenttype_id;

		$this->safa_uo_users_model->safa_uo_id = session('uo_id');
		if($safa_internalsegmenttype_id==2) {
			$this->ports_model->erp_port_id =  $internalpassages_row->erp_port_id;
			$port_row = $this->ports_model->get();

			$this->safa_uo_users_model->safa_uo_user_type_id_in = array(2,3);
			$this->safa_uo_users_model->erp_city_id =$port_row->erp_city_id;

			if(count($port_row)>0) {
				if($port_row->safa_transportertype_id==2) {
					$this->safa_uo_users_model->port_type_in=array('all','air');
				} else if($port_row->safa_transportertype_id==3) {
					$this->safa_uo_users_model->port_type_in==array('all','sea');
				}
			}

		} else if($safa_internalsegmenttype_id==3) {
			$this->tourismplaces_model->safa_tourismplace_id =  $internalpassages_row->safa_tourism_place_id;
			$tourismplace_row = $this->tourismplaces_model->get();

			$this->safa_uo_users_model->safa_uo_user_type_id_in = array(3);
			$this->safa_uo_users_model->erp_city_id =$tourismplace_row->erp_city_id;

		} else {
			$this->safa_uo_users_model->safa_uo_user_type_id_in = array(1,3);
			 
			$this->hotels_model->erp_hotel_id = $internalpassages_row->erp_end_hotel_id;
			$hotel_row = $this->hotels_model->get();
			if(count($hotel_row)>0) {
				$this->safa_uo_users_model->erp_city_id =$hotel_row->erp_city_id;
			}
		}

		$safa_uo_users_rows = $this->safa_uo_users_model->get();
		$safa_uo_users_options='';
		foreach($safa_uo_users_rows as $safa_uo_users_row) {
			$safa_uo_users_options=$safa_uo_users_options.'<option value="'.$safa_uo_users_row->safa_uo_user_id.'">'.$safa_uo_users_row->{name()}.'</option>';
		}
		$returndata['safa_uo_users_options'] = $safa_uo_users_options;
		//-----------------------------------------------------------------------------------------



		if ($internalpassages_row->safa_uo_user_id){

			//By Gouda.
			//$returndata['safa_uo_user_id'] = $internalpassages_row->safa_uo_user_id;
			$returndata['safa_uo_internalpassages_row'] = $internalpassages_row->safa_uo_agent_id;
			 
			$returndata['safa_internalsegmentestatus_id'] = $internalpassages_row->safa_internalsegmentestatus_id;
			$returndata['status_notes'] = $internalpassages_row->status_notes;
		} else {
			$returndata['safa_uo_user_id'] = 0;
			$returndata['safa_internalsegmentestatus_id'] = 0;
			$returndata['status_notes'] = '';
		}

		echo json_encode($returndata);exit;
	}

	//By Gouda.
	function drivers_and_buses($id = FALSE)
	{
		$this->internalpassages_model->safa_internalsegment_id = $id;
		$safa_internalsegment_row = $this->internalpassages_model->get();
		$safa_trip_internaltrip_id = $safa_internalsegment_row->safa_trip_internaltrip_id;

		$this->trip_internaltrip_model->safa_trip_internaltrip_id = $safa_trip_internaltrip_id;
		$safa_internalsegment_row = $this->trip_internaltrip_model->get();
		$safa_transporters_id = $safa_internalsegment_row->safa_transporter_id;

		$data['safa_transporters_id'] = $safa_transporters_id;

		$structure = array('safa_transporters_drivers_id', name());
		$key = $structure['0'];
		if (isset($structure['1'])) {
			$value = $structure['1'];
		}
		$drivers_arr = array();
		$this->transporters_drivers_model->safa_transporters_id = $safa_transporters_id;
		$drivers = $this->transporters_drivers_model->get();
		$drivers_arr[""] = lang('global_select_from_menu');
		foreach ($drivers as $driver) {
			$drivers_arr[$driver->$key] = $driver->$value;
		}
		$data['safa_transporters_drivers'] = $drivers_arr;


		$structure = array('safa_transporters_buses_id', 'bus_no');
		$key = $structure['0'];
		if (isset($structure['1'])) {
			$value = $structure['1'];
		}
		$buses_arr = array();
		$this->transporters_buses_model->safa_transporters_id = $safa_transporters_id;
		$buses = $this->transporters_buses_model->get();
		$buses_arr[""] = lang('global_select_from_menu');
		foreach ($buses as $bus) {
			$buses_arr[$bus->$key] = $bus->$value;
		}
		$this->transporters_buses_model->safa_transporters_id = false;
		$this->transporters_buses_model->auto_generate = 1;
		$buses = $this->transporters_buses_model->get();
		foreach ($buses as $bus) {
			$buses_arr[$bus->$key] = $bus->$value;
		}

		$data['safa_transporters_buses'] = $buses_arr;


		$this->internalsegments_drivers_and_buses_model->safa_internalsegments_id = $id;
		$item_drivers_and_buses = $this->internalsegments_drivers_and_buses_model->get();
		$data['item_drivers_and_buses'] = $item_drivers_and_buses;


		$this->load->library("form_validation");
		//$this->form_validation->set_rules('safa_transporters_drivers_id', 'lang:safa_transporters_drivers_id', 'trim|required');
		//$this->form_validation->set_rules('safa_transporters_buses_id', 'lang:safa_transporters_buses_id', 'trim|required');
		//if ($this->form_validation->run() == false) {
		if (!$this->input->post('smt_save')) {
			$this->load->view("uo/trips_reports/drivers_and_buses", $data);
		} else {
			//$this->internalsegments_drivers_and_buses_model->safa_internalsegments_id = $id;
			// Save driver_and_buss
			$drivers_and_buses_safa_transporters_drivers = $this->input->post('drivers_and_buses_safa_transporters_drivers');
			$drivers_and_buses_safa_transporters_buses = $this->input->post('drivers_and_buses_safa_transporters_buses');

			if (ensure($drivers_and_buses_safa_transporters_drivers))
			foreach ($drivers_and_buses_safa_transporters_drivers as $key => $value) {
				$this->internalsegments_drivers_and_buses_model->safa_transporters_drivers_id = $drivers_and_buses_safa_transporters_drivers[$key];
				$this->internalsegments_drivers_and_buses_model->safa_transporters_buses_id = $drivers_and_buses_safa_transporters_buses[$key];


				$this->internalsegments_drivers_and_buses_model->safa_internalsegments_drivers_and_buses_id = $key;
				$current_driver_and_bus_row_count = $this->internalsegments_drivers_and_buses_model->get(true);
				if ($current_driver_and_bus_row_count == 0) {
					$this->internalsegments_drivers_and_buses_model->safa_internalsegments_drivers_and_buses_id = false;
					$this->internalsegments_drivers_and_buses_model->save();
				} else {
					$this->internalsegments_drivers_and_buses_model->save();
				}
			}
			// --- Delete Deleted driver_and_buss rows from Database --------------
			//By Gouda
			$this->internalsegments_drivers_and_buses_model->safa_transporters_drivers_id = false;
			$this->internalsegments_drivers_and_buses_model->safa_transporters_buses_id = false;

			$drivers_and_buses_remove = $this->input->post('drivers_and_buses_remove');
			if (ensure($drivers_and_buses_remove)) {
				foreach ($drivers_and_buses_remove as $driver_and_bus_remove) {
					$this->internalsegments_drivers_and_buses_model->safa_internalsegments_drivers_and_buses_id = $driver_and_bus_remove;
					$this->internalsegments_drivers_and_buses_model->delete();
				}
			}
			//---------------------------------------------------------------


			$ref = $this->input->post('ref');
			$previous_screen = $this->input->post('previous_screen');
				
			$this->load->view('redirect_message', array('msg' => lang('global_added_message'), 'url' => $ref, 'model_name' => 'all_movements', 'model_title' => $previous_screen, 'action' => $previous_screen));
		}
	}

	function getDriverData()
	{
		$safa_transporters_drivers_id = $this->input->post('safa_transporters_drivers_id');


		$this->transporters_drivers_model->safa_transporters_drivers_id = $safa_transporters_drivers_id;
		$driver_row = $this->transporters_drivers_model->get();
		if(count($driver_row)>0) {
			echo lang('phone') . ':' . $driver_row->phone;
		}
		exit;
	}

	function getBusData()
	{
		$safa_transporters_buses_id = $this->input->post('safa_transporters_buses_id');

		$this->transporters_buses_model->safa_transporters_buses_id = $safa_transporters_buses_id;
		$this->transporters_buses_model->join = true;
		$bus_row = $this->transporters_buses_model->get();

		if(count($bus_row)>0) {
			echo lang('safa_buses_brands_id') . ':' . $bus_row->buses_brands_name . '<br/>';
			echo lang('safa_buses_models_id') . ':' . $bus_row->buses_models_name . '<br/>';
			echo lang('industry_year') . ':' . $bus_row->industry_year . '<br/>';
			echo lang('passengers_count') . ':' . $bus_row->passengers_count . '<br/>';
		}

		exit;
	}

	public function getBusesDriversByTransporterIdAjax()
	{
		$safa_transporters_id = $this->input->post('safa_transporter_id');
		 
		$structure = array('safa_transporters_drivers_id', name());
		$key = $structure['0'];
		if (isset($structure['1'])) {
			$value = $structure['1'];
		}
		//$drivers_arr = array();
		$drivers_dd_options='';
		$this->transporters_drivers_model->safa_transporters_id = $safa_transporters_id;
		$drivers = $this->transporters_drivers_model->get();
		//$drivers_arr[""] = lang('global_select_from_menu');
		foreach ($drivers as $driver) {
			//$drivers_arr[$driver->$key] = $driver->$value;
			$drivers_dd_options=$drivers_dd_options."<option value='".$driver->$key."'>".$driver->$value."</option>";
		}


		$structure = array('safa_transporters_buses_id', 'bus_no');
		$key = $structure['0'];
		if (isset($structure['1'])) {
			$value = $structure['1'];
		}
		//$buses_arr = array();
		$buses_dd_options='';
		$this->transporters_buses_model->safa_transporters_id = $safa_transporters_id;
		$buses = $this->transporters_buses_model->get();
		//$buses_arr[""] = lang('global_select_from_menu');
		foreach ($buses as $bus) {
			//$buses_arr[$bus->$key] = $bus->$value;
			$buses_dd_options=$buses_dd_options."<option value='".$bus->$key."'>".$bus->$value."</option>";
		}

		$data_arr[] = array('drivers' => $drivers_dd_options,
            'buses' => $buses_dd_options,
		);
		echo json_encode($data_arr);
		exit;
	}

}
