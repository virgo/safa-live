<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Contract_approving_phases extends Safa_Controller {

    public $module = "contract_approving_phases";

    public function __construct() {
        parent::__construct();
        $this->layout = 'new';
        //Side menu session, By Gouda.
        session('side_menu_id', 4);
        $this->load->model('contracts_model');
        $this->load->model('safa_contract_phases_model');
        $this->load->model('safa_contract_approving_phases_model');
        $this->load->model('notification_model');
        $this->load->model('ea_packages_model');
        $this->load->model('safa_packages_model');
        $this->lang->load('contract_approving_phases');
        $this->load->helper('form_helper');
        $this->load->helper('db_helper');
        $this->load->helper('itconf_helper');
        $this->load->helper('packages');
        $this->load->library("form_validation");
        permission();
    }

    public function index($safa_uo_contracts_id = false) {

        if (!$safa_uo_contracts_id) {
            show_404();
        }
        $this->layout = 'js';


        $data['contract_phases'] = ddgen('safa_contract_phases', array('safa_contract_phases_id', name()), FALSE, FALSE, TRUE);


        $this->contracts_model->safa_uo_contract_id = $safa_uo_contracts_id;
        $items = $this->contracts_model->search();
        $data['item'] = $items[0];

        //--------------------------------- Next contract phases documents
        $next_phase_id = $items[0]->safa_contract_phases_id_next;

        $this->safa_contract_approving_phases_model->safa_uo_contracts_id = $safa_uo_contracts_id;
        $this->safa_contract_approving_phases_model->safa_contract_phases_id = $next_phase_id;
        $required_documents_rows_for_contract = $this->safa_contract_approving_phases_model->get();

        $data['required_documents'] = $required_documents_rows_for_contract;

        //--------------------------------

        if (!isset($_POST['smt_save'])) {
            $this->load->view('ea/contract_approving_phases/index', $data);
        } else {

            $safa_uo_contracts_id_folder = './static/uploads/contracts_documents/' . $safa_uo_contracts_id;
            if (!file_exists($safa_uo_contracts_id_folder)) {
                mkdir($safa_uo_contracts_id_folder, 0777, true);
            }

            foreach ($required_documents_rows_for_contract as $required_documents_row_for_contract) {

                //---------------------------------------- --------------------------------------------
                //iconv_set_encoding("output_encoding", "UTF-8");
                $safa_contract_phases_documents_id = $required_documents_row_for_contract->safa_contract_phases_documents_id;
                if (isset($_FILES["document_file_$safa_contract_phases_documents_id"])) {
                    if (count($_FILES["document_file_$safa_contract_phases_documents_id"]['tmp_name']) > 0) {
                        //Get the temp file path
                        $tmpFilePath = $_FILES["document_file_$safa_contract_phases_documents_id"]['tmp_name'];

                        //Make sure we have a filepath
                        if ($tmpFilePath != "") {
                            //Setup our new file path
                            $newFilePath = './static/uploads/contracts_documents/' . $safa_uo_contracts_id . '/' . $_FILES["document_file_$safa_contract_phases_documents_id"]['name'];

                            //Upload the file into the temp dir
                            //To solve arabic files names problem.
                            $is_file_uploaded = move_uploaded_file($tmpFilePath, iconv('utf-8', 'windows-1256', $newFilePath));
                            //$is_file_uploaded=move_uploaded_file($tmpFilePath, $newFilePath);

                            if ($is_file_uploaded) {

                                $this->safa_contract_approving_phases_model->safa_contract_approving_phases_id = $required_documents_row_for_contract->safa_contract_approving_phases_id;
                                $this->safa_contract_approving_phases_model->document_path = $newFilePath;
                                $this->safa_contract_approving_phases_model->safa_contract_approving_phases_status_id = 1;
                                $this->safa_contract_approving_phases_model->save();
                            }
                        }
                    }
                }
            }
            //-------------------------------------------------------------------------------------------------
            //----------- Send Notification For EA ------------------------------
            $msg_datetime = date('Y-m-d H:i', time());

            $this->notification_model->notification_type = 'automatic';
            $this->notification_model->erp_importance_id = 1;
            $this->notification_model->sender_type_id = 1;
            $this->notification_model->sender_id = 0;
            $this->notification_model->erp_system_events_id = 13;
            $this->notification_model->language_id = 2;
            $this->notification_model->msg_datetime = $msg_datetime;

            /*
              $current_phase_name=$this->input->post('current_phase');
              $next_phase_id=$this->input->post('next_phase');
              $this->safa_contract_phases_model->safa_contract_phases_id=$next_phase_id;
              $next_phase_row=$this->safa_contract_phases_model->get();
              $next_phase_name=$next_phase_row->{name()};

              $tags="#*current_contract_phase#*=$current_phase_name*****#*next_contract_phase#*=$next_phase_name";
             */
            $this->notification_model->tags = '';
            $this->notification_model->parent_id = '';


            $erp_notification_id = $this->notification_model->save();

            //-------- Notification Details ---------------
            $this->notification_model->detail_erp_notification_id = $erp_notification_id;
            $this->notification_model->detail_receiver_type = 2;
            $this->notification_model->detail_receiver_id = $items[0]->safa_uo_id;
            $this->notification_model->saveDetails();
            //--------------------------------
            //-------------------------------------------------------------------


            $this->load->view('redirect_message_popup', array('msg' => lang('uploaded_successfully_message'),
                'url' => site_url("ea/contract_approving_phases/index/$safa_uo_contracts_id"),
                'id' => $this->uri->segment("4"),
                'model_title' => lang('title'), 'action' => lang('upload_files')));
        }
    }

    public function removeFile($safa_contract_approving_phases_id) {
        $this->safa_contract_approving_phases_model->safa_contract_approving_phases_id = $safa_contract_approving_phases_id;
        $required_documents_row_for_contract = $this->safa_contract_approving_phases_model->get();
        if (count($required_documents_row_for_contract) > 0) {
            $file_name = iconv('utf-8', 'windows-1256', $required_documents_row_for_contract->document_path);
            if (file_exists($file_name)) {
                unlink($file_name);
                $this->safa_contract_approving_phases_model->document_path = '';
                $this->safa_contract_approving_phases_model->save();
            }
        }
        $safa_uo_contracts_id = $required_documents_row_for_contract->safa_uo_contracts_id;

        redirect(site_url("ea/contract_approving_phases/index/$safa_uo_contracts_id"));
    }

    public function view_all() {
        $this->layout = 'new';

        $this->contracts_model->by_eas_id = session('ea_id');
        $this->contracts_model->uo_contracts_eas_disabled = 0;
        $data["total_rows"] = $this->contracts_model->search(true);
        $data["items"] = $this->contracts_model->search();

        $this->contracts_model->offset = $this->uri->segment("4");
        $this->contracts_model->limit = $this->config->item('per_page');
        $this->contracts_model->order_by = array('safa_uo_contract_id', 'desc');

        $config['uri_segment'] = 4;
        $config['base_url'] = site_url('ea/contract_approving_phases/view_all');
        $config['total_rows'] = $data["total_rows"];

        $config['per_page'] = $this->config->item('per_page');
        $config['suffix'] = $this->input->get() ? '?' . http_build_query($_GET) : NULL;
        $this->load->library('pagination');
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();
        $this->load->view('ea/contract_approving_phases/view_all', $data);
    }

    public function add() {
        $this->layout = 'js';
        $data = array();

        if (!isset($_POST['smt_save'])) {
            $this->load->view('ea/contract_approving_phases/add', $data);
        } else {

            $username = $this->input->post('username');

            $password = $this->input->post('password');
            $password = md5($password);

            $this->contracts_model->contract_username = $username;
            $this->contracts_model->contract_password = $password;

            $contracts_rows = $this->contracts_model->search();

            if (count($contracts_rows) > 0) {
                $this->contracts_model->safa_uo_contract_id = $contracts_rows[0]->safa_uo_contract_id;

                $total_contracts_rows = $this->contracts_model->get_contract_ea(true);

                $this->contracts_model->by_eas_id = session('ea_id');


                $total_ea_contracts_rows = $this->contracts_model->get_contract_ea(true);
                $ea_contracts_rows = $this->contracts_model->search();
                if ($total_ea_contracts_rows > 0) {


                    if ($ea_contracts_rows[0]->safa_uo_contract_ea_id != '') {
                        $this->contracts_model->uo_contracts_eas_safa_uo_contract_ea_id = $ea_contracts_rows[0]->safa_uo_contract_ea_id;
                        $this->contracts_model->uo_contracts_eas_disabled = 0;

                        if ($total_contracts_rows == 0) {
                            $this->contracts_model->uo_contracts_eas_is_original_owner = 1;
                        }

                        $this->contracts_model->uo_contracts_eas_name_ar = $this->input->post('name_ar');
                        $this->contracts_model->uo_contracts_eas_name_la = $this->input->post('name_la');

                        $this->contracts_model->save_contract_ea();

                        $this->load->view('redirect_message', array('msg' => lang('contract_opened_successfully_message'),
                            'url' => site_url("ea/contract_approving_phases/add"),
                            'id' => $this->uri->segment("4"),
                            'model_title' => lang('title'), 'action' => lang('contract_open')));
                    } else {
                        $this->load->view('redirect_message_popup', array('msg' => lang('contract_already_opened_message'),
                            'url' => site_url("ea/contract_approving_phases/add"),
                            'id' => $this->uri->segment("4"),
                            'model_title' => lang('title'), 'action' => lang('contract_open')));
                    }
                } else {
                    $this->contracts_model->uo_contracts_eas_disabled = 0;

                    if ($total_contracts_rows == 0) {
                        $this->contracts_model->uo_contracts_eas_is_original_owner = 1;
                    }

                    $this->contracts_model->uo_contracts_eas_name_ar = $this->input->post('name_ar');
                    $this->contracts_model->uo_contracts_eas_name_la = $this->input->post('name_la');

                    $safa_uo_contract_ea_id = $this->contracts_model->save_contract_ea();


                    //Add UO Package For This safa_uo_contracts_eas
                    //------------------------------------------------------------------------------------------------
                    $safa_uo_contract_id = $contracts_rows[0]->safa_uo_contract_id;

                    //------------- Special Package ------------------------------
//                    $this->ea_packages_model->safa_uo_contract_ea_id = $safa_uo_contract_ea_id;
//                    $this->ea_packages_model->safa_ea_season_id = 1;
//                    $this->safa_packages_model->private = 1;
//                    $private_packages_rows = $this->safa_packages_model->get();
//                    if (count($private_packages_rows) > 0) {
//                        $this->ea_packages_model->safa_package_id = $private_packages_rows[0]->safa_package_id;
//                        $this->ea_packages_model->save();
//                    }
//                    //------------------------------------------------------------    
//
//                    $packages_arr = get_package_by_contract($safa_uo_contract_id);
//                    foreach ($packages_arr as $package_id) {
//                        $this->ea_packages_model->safa_uo_contract_ea_id = $safa_uo_contract_ea_id;
//                        $this->ea_packages_model->safa_ea_season_id = 1;
//                        $this->ea_packages_model->safa_package_id = $package_id;
//                        $this->ea_packages_model->save();
//                    }
                    //------------------------------------------------------------------------------------------------


                    $this->load->view('redirect_message_popup', array('msg' => lang('contract_opened_successfully_message'),
                        'url' => site_url("ea/contract_approving_phases/add"),
                        'id' => $this->uri->segment("4"),
                        'model_title' => lang('title'), 'action' => lang('contract_open')));
                }
            } else {
                $this->load->view('error_message', array('msg' => lang('invalid_username_or_password'),
                    'url' => site_url("ea/contract_approving_phases/add"),
                    'id' => $this->uri->segment("4"),
                    'model_title' => lang('title'), 'action' => lang('contract_open')));
            }
        }
    }

    function delete($safa_uo_contract_id = FALSE) {
        $this->contracts_model->by_eas_id = session('ea_id');
        $this->contracts_model->safa_uo_contract_id = $safa_uo_contract_id;
        if ($this->contracts_model->delete_contract_ea()) {
            $this->load->view('redirect_message', array('msg' => lang('global_deleted_message'),
                'url' => site_url('ea/contract_approving_phases/view_all'),
                'model_title' => lang('title'), 'action' => lang('')));
        }
    }

}

/* End of file contract_approving_phases.php */
/* Location: ./application/controllers/contract_approving_phases.php */