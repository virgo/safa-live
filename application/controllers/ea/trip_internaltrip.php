<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Trip_internaltrip extends Safa_Controller {

    public $module = "trip_internaltrip";
    public $temp_upload_path;
    public $uploaded_file_name;

    public function __construct() {
        parent::__construct();
        $this->layout = 'new';
        //Side menu session, By Gouda.
        session('side_menu_id', 2);
        //By Gouda, For Notifications
        $this->load->model('contracts_model');
        $this->load->model('notification_model');
        $this->load->model('internaltrip_status_model');

        $this->load->model('safa_trips_model');
        $this->load->model('trip_internaltrip_model');
        $this->load->model('internalpassages_model');
        $this->load->model('safa_uos_model');
        
        $this->lang->load('trip_internaltrip');
        $this->lang->load('internalpassages');
        $this->load->helper('form_helper');
        $this->load->helper('db_helper');
        $this->load->helper('itconf_helper');
        permission();
        if (session('ea_id')) {
			$this->trip_internaltrip_model->ea_id = session('ea_id');
			$this->safa_trips_model->safa_ea_id = session('ea_id');
		} 
    }

    public function index($trip_id = false) {

        //$data["uo_contracts"] = $this->create_contracts_array();
        $data["uos"] = $this->create_uos_array();
        
        $data["transporters"] = $this->create_transportes_array();
        $data["safa_intrernaltripstatus"] = ddgen("safa_internaltripstatus", array("safa_internaltripstatus_id", name()));
        $data["safa_intrernaltripstatus_color"] = $this->db->select(name() . ',color,code')->from('safa_internaltripstatus')->get()->result();
        $data["safa_ito"] = $this->get_ea_contracts_itos();
        
        if (isset($_GET['search']))
            $this->search();
        
        $data["total_rows"] = $this->trip_internaltrip_model->get(true);

        $this->trip_internaltrip_model->offset = $this->uri->segment("4");
        //$this->trip_internaltrip_model->limit = $this->config->item('per_page');
        $this->trip_internaltrip_model->order_by = array('safa_trip_internaltrip_id', 'desc');
        $data["items"] = $this->trip_internaltrip_model->get();
        $config['uri_segment'] = 4;
        $config['base_url'] = site_url('ea/trip_internaltrip/index');
        $config['total_rows'] = $data["total_rows"];

        $config['per_page'] = $this->trip_internaltrip_model->limit;
        $config['suffix'] = $this->input->get() ? '?' . http_build_query($_GET) : NULL;
        $this->load->library('pagination');
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();
        $this->load->view('ea/trip_internaltrip/index', $data);
    }

    public function add() {
        /* get the refrance data */
        //$data["uo_contracts"] = $this->create_contracts_array();
        $data["uos"] = $this->create_uos_array();
        
        $data["safa_trips"] = ddgen('safa_trips', array('safa_trip_id', 'name'), array('safa_ea_id' => session('ea_id')), FALSE, FALSE);
        
        
        $data["transporters"] = $this->create_transportes_array();
        
        //$data["safa_intrernaltripstatus"] = ddgen("safa_internaltripstatus", array("safa_internaltripstatus_id", name()));
       	$safa_intrernaltripstatus_rows = $this->internaltrip_status_model->get();
		$safa_intrernaltripstatus_arr = array();
		foreach ($safa_intrernaltripstatus_rows as $safa_intrernaltripstatus_row) {
			if(session('ea_id')) {
				if($safa_intrernaltripstatus_row->safa_internaltripstatus_id==1 || $safa_intrernaltripstatus_row->safa_internaltripstatus_id==2) {
					$safa_intrernaltripstatus_arr[$safa_intrernaltripstatus_row->safa_internaltripstatus_id] = $safa_intrernaltripstatus_row->{name()};
				}
			} else if(session('uo_id')) {
				if($safa_intrernaltripstatus_row->safa_internaltripstatus_id==4) {
					$safa_intrernaltripstatus_arr[$safa_intrernaltripstatus_row->safa_internaltripstatus_id] = $safa_intrernaltripstatus_row->{name()};
				}
			}
		}
		$data["safa_intrernaltripstatus"] =$safa_intrernaltripstatus_arr;
		
        
        $data["safa_ito"] = $this->get_ea_contracts_itos();
        $this->load->library("form_validation");
        /*         * ************* */
        $this->form_validation->set_rules('datetime', 'lang:transport_request_date', 'trim|required');
        $this->form_validation->set_rules('safa_uo_contract_id', 'lang:transport_request_contract', 'trim|required');
        //$this->form_validation->set_rules('safa_trip_id', 'lang:transport_request_trip_id', 'trim|required');
        $this->form_validation->set_rules('safa_internaltripstatus_id', 'lang:transport_request_status', 'trim|required');
        if (PHASE > 1):
            $this->form_validation->set_rules('safa_ito_id', 'lang:transport_request_opertator', 'trim');
        else:
            $this->form_validation->set_rules('safa_ito_id', 'lang:transport_request_opertator', 'trim|required');
        endif;
        $this->form_validation->set_rules('safa_transporter_id', 'lang:transport_request_transportername', 'trim');
        $this->form_validation->set_rules('operator_reference', 'lang:transport_request_res_code', 'trim');


        $this->form_validation->set_rules('text_input', '', 'trim');
        if ($this->input->post('text_input') && $_FILES['userfile']['name']) {
            $this->form_validation->set_rules('userfile', 'lang:userfile', 'trim|callback_fileupload');
        }
        if ($this->form_validation->run() == false) {
            $this->load->view("ea/trip_internaltrip/add", $data);
        } else {

            //$file_name = $this->input->post('text_input');

            if (isset($_FILES["transport_request_res_file"])) {
                if (count($_FILES["transport_request_res_file"]['tmp_name']) > 0) {
                    //Get the temp file path
                    $tmpFilePath = $_FILES["transport_request_res_file"]['tmp_name'];

                    //Make sure we have a filepath
                    if ($tmpFilePath != "") {
                        //Setup our new file path
                        $newFilePath = './static/temp/ea_files/' . $_FILES["transport_request_res_file"]['name'];

                        //Upload the file into the temp dir
                        //To solve arabic files names problem.
                        $is_file_uploaded = move_uploaded_file($tmpFilePath, iconv('utf-8', 'windows-1256', $newFilePath));
                        //$is_file_uploaded=move_uploaded_file($tmpFilePath, $newFilePath);



                        $file_name = $_FILES["transport_request_res_file"]['name'];
                        $this->trip_internaltrip_model->attachement = $file_name;
                    }
                }
            }

            
            if($this->input->post('safa_trip_id')) {
            	 $this->trip_internaltrip_model->safa_trip_id = $this->input->post('safa_trip_id');
            } else {
            	$this->trip_internaltrip_model->safa_trip_id = null;
            }
            $this->trip_internaltrip_model->safa_ea_id = session('ea_id');
            
            
            
            if ($this->input->post('safa_ito_id'))
                $this->trip_internaltrip_model->safa_ito_id = $this->input->post('safa_ito_id');
            if ($this->input->post('safa_transporter_id'))
                $this->trip_internaltrip_model->safa_transporter_id = $this->input->post('safa_transporter_id');
            $this->trip_internaltrip_model->safa_internaltripstatus_id = $this->input->post('safa_internaltripstatus_id');
            $this->trip_internaltrip_model->operator_reference = $this->input->post('operator_reference');
            //$this->trip_internaltrip_model->attachement = $file_name;
            $this->trip_internaltrip_model->confirmation_number = gen_itconf();

            $this->trip_internaltrip_model->datetime = $this->input->post('datetime');
            
            //$this->trip_internaltrip_model->erp_company_type_id = 2;
            //$this->trip_internaltrip_model->erp_company_id = $this->input->post('uo_id');

            $this->trip_internaltrip_model->safa_uo_contract_id = $this->input->post('safa_uo_contract_id');
            
            $this->contracts_model->safa_uo_contract_id = $this->input->post('safa_uo_contract_id');
         	$contracts_row=$this->contracts_model->get();
            
            $this->safa_uos_model->safa_uo_id = $contracts_row->safa_uo_id;
			$safa_uo_row = $this->safa_uos_model->get();
			$safa_uo_code = $safa_uo_row->code;
			
			$safa_uo_row = $this->trip_internaltrip_model->get_max_serial_by_uo($contracts_row->safa_uo_id);
			$safa_uo_serial = $safa_uo_row->max_serial+1;
			$this->trip_internaltrip_model->serial = $safa_uo_serial;
		
			$this->trip_internaltrip_model->trip_title = $safa_uo_code.'-'.$safa_uo_serial;
			
            
            $internaltrip_id = $this->trip_internaltrip_model->save();

            /*
              if ($file_name)// uploading when the file exist//
              $this->move_upload();
             */

            if (isset($internaltrip_id)) {

            //By Gouda.
            //----------- Send Notification For UO Company ------------------------------
       		$msg_datetime = date('Y-m-d H:i', time());
    
       		$this->notification_model->notification_type = 'automatic';
        	$this->notification_model->erp_importance_id = 1;
        	$this->notification_model->sender_type_id = 1;
	        $this->notification_model->sender_id = 0;
	        
	        $this->notification_model->erp_system_events_id = 19;
	        
	        $this->notification_model->language_id = 2;
	        $this->notification_model->msg_datetime = $msg_datetime;
        
	        
	        $internaltripstatus_name='';
	        $safa_internaltripstatus_id=$this->input->post('safa_internaltripstatus_id');
	        $this->internaltrip_status_model->safa_internaltripstatus_id=$safa_internaltripstatus_id;
	        $internaltrip_status_row=$this->internaltrip_status_model->get();
	        if(count($internaltrip_status_row)>0) {
	        	$internaltripstatus_name=$internaltrip_status_row->{name()};
	        }
	        $trip_name='';
	        $safa_trip_id=$this->input->post('safa_trip_id');
	        $this->safa_trips_model->safa_trip_id=$safa_trip_id;
	        $trip_row=$this->safa_trips_model->get();
	        if(count($trip_row)>0) {
	        	$trip_name=$trip_row->name;
	        }
	        
	        //Link to replace.
	        $link_internaltrip ="<a href='".site_url('uo/trip_internaltrip/edit/'.$internaltrip_id)."' target='_blank'> $internaltrip_id </a>";
	        
	        $this->notification_model->tags = "#*safa_trip_internaltrip_id#*=$link_internaltrip*****#*internaltripstatus#*=$internaltripstatus_name*****#*the_date#*=$msg_datetime*****#*trip#*=$trip_name";
	        $this->notification_model->parent_id = '';
	        	        
	        $erp_notification_id=$this->notification_model->save();
	        
	        //-------- Notification Details ---------------
	        $this->notification_model->detail_erp_notification_id = $erp_notification_id;
         	$this->notification_model->detail_receiver_type = 2;
         	
         	//$this->contracts_model->safa_uo_contract_id = $this->input->post('uo_contract_id');
         	//$contracts_row=$this->contracts_model->get();
         	if(count($contracts_row)>0) {
        		$this->notification_model->detail_receiver_id = $contracts_row->safa_uo_id ;
        		$this->notification_model->saveDetails();
         	} 
	        //--------------------------------
       		//-------------------------------------------------------------------
       		
         	
         	//----------- Send Notification For ITO Company ------------------------------
         	if($this->input->post('safa_ito_id')!='') {
       		$msg_datetime = date('Y-m-d H:i', time());
    
       		$this->notification_model->notification_type = 'automatic';
        	$this->notification_model->erp_importance_id = 1;
        	$this->notification_model->sender_type_id = 1;
	        $this->notification_model->sender_id = 0;
	        
	        $this->notification_model->erp_system_events_id = 19;
	        
	        $this->notification_model->language_id = 2;
	        $this->notification_model->msg_datetime = $msg_datetime;
        
	        
	        $internaltripstatus_name='';
	        $safa_internaltripstatus_id=$this->input->post('safa_internaltripstatus_id');
	        $this->internaltrip_status_model->safa_internaltripstatus_id=$safa_internaltripstatus_id;
	        $internaltrip_status_row=$this->internaltrip_status_model->get();
	        if(count($internaltrip_status_row)>0) {
	        	$internaltripstatus_name=$internaltrip_status_row->{name()};
	        }
	        $trip_name='';
	        $safa_trip_id=$this->input->post('safa_trip_id');
	        $this->safa_trips_model->safa_trip_id=$safa_trip_id;
	        $trip_row=$this->safa_trips_model->get();
	        if(count($trip_row)>0) {
	        	$trip_name=$trip_row->name;
	        }
	        
	        //Link to replace.
	        $link_internaltrip ="<a href='".site_url('ito/trip_internaltrip/edit_incoming/'.$internaltrip_id)."'  target='_blank'> $internaltrip_id </a>";
	        
	        $this->notification_model->tags = "#*safa_trip_internaltrip_id#*=$link_internaltrip*****#*internaltripstatus#*=$internaltripstatus_name*****#*the_date#*=$msg_datetime*****#*trip#*=$trip_name";
	        $this->notification_model->parent_id = '';
	        	        
	        $erp_notification_id=$this->notification_model->save();
	        
	        //-------- Notification Details ---------------
	        $this->notification_model->detail_erp_notification_id = $erp_notification_id;
         	$this->notification_model->detail_receiver_type = 5;
         	$this->notification_model->detail_receiver_id = $this->input->post('safa_ito_id');
        	$this->notification_model->saveDetails();
         	
	        //--------------------------------
         	}
       		//-------------------------------------------------------------------
         	
         	
            	
            	$this->load->view('redirect_message', array('msg' => lang('global_added_message'),
                    'url' => site_url('ea/trip_internaltrip/edit/' . $internaltrip_id),
                    'model_title' => lang('node_title'), 'action' => lang('add_transport_request')));
            }
        }
    }

    public function edit($id = FALSE) {
        if (!$id)
            show_404();
        $this->trip_internaltrip_model->safa_trip_internaltrip_id = $id;
        $data['item']= $item = $this->trip_internaltrip_model->get();

        if (!$data['item'])
            show_404();
        $trip_id = $data['item']->safa_trip_id;
        //$data["contract_id"] = $this->trip_internaltrip_model->get_trip_contracts($trip_id);
        //$data["contract_id"] = $this->get_trip_contract_id($trip_id);
        
        
    	if($item->safa_ea_id!='') {
	        $this->contracts_model->safa_uo_id = $item->erp_company_id;
	        $this->contracts_model->by_eas_id = session('ea_id');
	        $contracts_rows = $this->contracts_model->search();
	    } else {
	        $this->contracts_model->safa_uo_id = $item->safa_uo_id;
			$this->contracts_model->by_eas_id = session('ea_id');
			$contracts_rows = $this->contracts_model->search();
	   }
	   
	   $data["safa_uo_id"] = 0;
	   if(count($contracts_rows)>0) {
       	$data["safa_uo_id"] = $contracts_rows[0]->safa_uo_id;
	   }
        
        //$data["uo_contracts"] = $this->create_contracts_array();
        $data["uos"] = $this->create_uos_array();
        
        $data["safa_trips"] = ddgen('safa_trips', array('safa_trip_id', 'name'), array('safa_ea_id' => session('ea_id')), FALSE, FALSE);
        
        $data["transporters"] = $this->create_transportes_array();
        
        //$data["safa_intrernaltripstatus"] = ddgen("safa_internaltripstatus", array("safa_internaltripstatus_id", name()));
		$safa_intrernaltripstatus_rows = $this->internaltrip_status_model->get();
		$safa_intrernaltripstatus_arr = array();
		foreach ($safa_intrernaltripstatus_rows as $safa_intrernaltripstatus_row) {
			if(session('ea_id')) {
				if($safa_intrernaltripstatus_row->safa_internaltripstatus_id==1 || $safa_intrernaltripstatus_row->safa_internaltripstatus_id==2) {
					$safa_intrernaltripstatus_arr[$safa_intrernaltripstatus_row->safa_internaltripstatus_id] = $safa_intrernaltripstatus_row->{name()};
				}
			} else if(session('uo_id')) {
				if($safa_intrernaltripstatus_row->safa_internaltripstatus_id==4) {
					$safa_intrernaltripstatus_arr[$safa_intrernaltripstatus_row->safa_internaltripstatus_id] = $safa_intrernaltripstatus_row->{name()};
				}
			}
		}
		$data["safa_intrernaltripstatus"] =$safa_intrernaltripstatus_arr;
		
        
        $data["safa_ito"] = $this->get_ea_contracts_itos();

        $this->internalpassages_model->safa_trip_internaltrip_id = $id;
        $this->internalpassages_model->order_by = array("safa_internalsegments.start_datetime", 'ASC');
        $data["internalpassages"] = $this->internalpassages_model->get();
        $data["trip_internal_id"] = $id;

        $this->load->library("form_validation");
        
        $this->form_validation->set_rules('datetime', 'lang:transport_request_date', 'trim|required');
        $this->form_validation->set_rules('safa_uo_contract_id', 'lang:transport_request_contract', 'trim|required');
        //$this->form_validation->set_rules('safa_trip_id', 'lang:transport_request_trip_id', 'trim|required');
        $this->form_validation->set_rules('safa_internaltripstatus_id', 'lang:transport_request_status', 'trim|required');
        if (PHASE > 1):
            $this->form_validation->set_rules('safa_ito_id', 'lang:transport_request_opertator', 'trim');
        else:
            $this->form_validation->set_rules('safa_ito_id', 'lang:transport_request_opertator', 'trim|required');
        endif;
        $this->form_validation->set_rules('safa_transporter_id', 'lang:transport_request_transportername', 'trim');
        $this->form_validation->set_rules('operator_reference', 'lang:transport_request_res_code', 'trim|required');
        if ($this->input->post('text_input') && $_FILES['userfile']['name']) {
            $this->form_validation->set_rules('userfile', 'lang:userfile', 'trim|callback_fileupload');
        }
        if ($this->form_validation->run() == false) {
            $this->load->view("ea/trip_internaltrip/edit", $data);
        } else {
        	
        	$file_name='';
            if (isset($_FILES["transport_request_res_file"])) {
                if (count($_FILES["transport_request_res_file"]['tmp_name']) > 0) {
                    $tmpFilePath = $_FILES["transport_request_res_file"]['tmp_name'];

                    if ($tmpFilePath != "") {
                        $newFilePath = './static/temp/ea_files/' . $_FILES["transport_request_res_file"]['name'];

                        $is_file_uploaded = move_uploaded_file($tmpFilePath, iconv('utf-8', 'windows-1256', $newFilePath));

                        $file_name = $_FILES["transport_request_res_file"]['name'];
                        $this->trip_internaltrip_model->attachement = $file_name;
                    }
                }
            }

            $this->trip_internaltrip_model->safa_trip_internaltrip_id = $id;
            
            if($this->input->post('safa_trip_id')) {
            	 $this->trip_internaltrip_model->safa_trip_id = $this->input->post('safa_trip_id');
            } else {
            	$this->trip_internaltrip_model->safa_trip_id = null;
            }
            $this->trip_internaltrip_model->safa_ea_id = session('ea_id');
            
            
            if ($this->input->post('safa_ito_id'))
                $this->trip_internaltrip_model->safa_ito_id = $this->input->post('safa_ito_id');
            else
                $this->trip_internaltrip_model->safa_ito_id = NULL;
            if ($this->input->post('safa_transporter_id'))
                $this->trip_internaltrip_model->safa_transporter_id = $this->input->post('safa_transporter_id');
            else
                $this->trip_internaltrip_model->safa_transporter_id = NULL;
            $this->trip_internaltrip_model->safa_internaltripstatus_id = $this->input->post('safa_internaltripstatus_id');
            $this->trip_internaltrip_model->operator_reference = $this->input->post('operator_reference');
            //$this->trip_internaltrip_model->attachement = $file_name;
            $this->trip_internaltrip_model->datetime = $this->input->post('datetime');

            $this->trip_internaltrip_model->ea_notes = $this->input->post('ea_notes');

            //$this->trip_internaltrip_model->erp_company_type_id = 2;
            //$this->trip_internaltrip_model->erp_company_id = $this->input->post('uo_id');
                        
            $this->trip_internaltrip_model->safa_uo_contract_id = $this->input->post('safa_uo_contract_id');
            
            $this->contracts_model->safa_uo_contract_id = $this->input->post('safa_uo_contract_id');
         	$contracts_row=$this->contracts_model->get();
            
            $this->safa_uos_model->safa_uo_id = $contracts_row->safa_uo_id;
			$safa_uo_row = $this->safa_uos_model->get();
			$safa_uo_code = $safa_uo_row->code;
			
			$safa_uo_row = $this->trip_internaltrip_model->get_max_serial_by_uo($contracts_row->safa_uo_id);
			$safa_uo_serial = $safa_uo_row->max_serial+1;
			$this->trip_internaltrip_model->serial = $safa_uo_serial;
		
			$this->trip_internaltrip_model->trip_title = $safa_uo_code.'-'.$safa_uo_serial;
			
            
            
            /*
              if ($file_name)// uploading when the file exist//
              $this->move_upload();
             */


            $internaltrip_id = $this->trip_internaltrip_model->save();



            //By Gouda.
            //----------- Send Notification For UO Company ------------------------------
       		$msg_datetime = date('Y-m-d H:i', time());
    
       		$this->notification_model->notification_type = 'automatic';
        	$this->notification_model->erp_importance_id = 1;
        	$this->notification_model->sender_type_id = 1;
	        $this->notification_model->sender_id = 0;
	        
	        $this->notification_model->erp_system_events_id = 20;
	        
	        $this->notification_model->language_id = 2;
	        $this->notification_model->msg_datetime = $msg_datetime;
        
	        
	        $internaltripstatus_name='';
	        $safa_internaltripstatus_id=$this->input->post('safa_internaltripstatus_id');
	        $this->internaltrip_status_model->safa_internaltripstatus_id=$safa_internaltripstatus_id;
	        $internaltrip_status_row=$this->internaltrip_status_model->get();
	        if(count($internaltrip_status_row)>0) {
	        	$internaltripstatus_name=$internaltrip_status_row->{name()};
	        }
	        $trip_name='';
	        $safa_trip_id=$this->input->post('safa_trip_id');
	        $this->safa_trips_model->safa_trip_id=$safa_trip_id;
	        $trip_row=$this->safa_trips_model->get();
	        if(count($trip_row)>0) {
	        	$trip_name=$trip_row->name;
	        }
	        
	        //Link to replace.
	        $link_internaltrip ="<a href='".site_url('uo/trip_internaltrip/edit/'.$id)."'  target='_blank'> $id </a>";
	        
	        $this->notification_model->tags = "#*safa_trip_internaltrip_id#*=$link_internaltrip*****#*internaltripstatus#*=$internaltripstatus_name*****#*the_date#*=$msg_datetime*****#*trip#*=$trip_name";
	        $this->notification_model->parent_id = '';
	        	        
	        $erp_notification_id=$this->notification_model->save();
	        
	        //-------- Notification Details ---------------
	        $this->notification_model->detail_erp_notification_id = $erp_notification_id;
         	$this->notification_model->detail_receiver_type = 2;
         	
         	//$this->contracts_model->safa_uo_contract_id = $this->input->post('uo_contract_id');
         	//$contracts_row=$this->contracts_model->get();
         	if(count($contracts_row)>0) {
        		$this->notification_model->detail_receiver_id = $contracts_row->safa_uo_id ;
        		//$this->notification_model->detail_receiver_id = $this->input->post('uo_id');
        		$this->notification_model->saveDetails();
         	} 
	        //--------------------------------
       		//-------------------------------------------------------------------
       		
         	
         	//----------- Send Notification For ITO Company ------------------------------
         	if($this->input->post('safa_ito_id')!='') {
       		$msg_datetime = date('Y-m-d H:i', time());
    
       		$this->notification_model->notification_type = 'automatic';
        	$this->notification_model->erp_importance_id = 1;
        	$this->notification_model->sender_type_id = 1;
	        $this->notification_model->sender_id = 0;
	        
	        $this->notification_model->erp_system_events_id = 20;
	        
	        $this->notification_model->language_id = 2;
	        $this->notification_model->msg_datetime = $msg_datetime;
        
	        
	        $internaltripstatus_name='';
	        $safa_internaltripstatus_id=$this->input->post('safa_internaltripstatus_id');
	        $this->internaltrip_status_model->safa_internaltripstatus_id=$safa_internaltripstatus_id;
	        $internaltrip_status_row=$this->internaltrip_status_model->get();
	        if(count($internaltrip_status_row)>0) {
	        	$internaltripstatus_name=$internaltrip_status_row->{name()};
	        }
	        $trip_name='';
	        $safa_trip_id=$this->input->post('safa_trip_id');
	        $this->safa_trips_model->safa_trip_id=$safa_trip_id;
	        $trip_row=$this->safa_trips_model->get();
	        if(count($trip_row)>0) {
	        	$trip_name=$trip_row->name;
	        }
	        
	        //Link to replace.
	        $link_internaltrip ="<a href='".site_url('ito/trip_internaltrip/edit_incoming/'.$id)."'  target='_blank'> $id </a>";
	        
	        $this->notification_model->tags = "#*safa_trip_internaltrip_id#*=$link_internaltrip*****#*internaltripstatus#*=$internaltripstatus_name*****#*the_date#*=$msg_datetime*****#*trip#*=$trip_name";
	        $this->notification_model->parent_id = '';
	        	        
	        $erp_notification_id=$this->notification_model->save();
	        
	        //-------- Notification Details ---------------
	        $this->notification_model->detail_erp_notification_id = $erp_notification_id;
         	$this->notification_model->detail_receiver_type = 5;
         	$this->notification_model->detail_receiver_id = $this->input->post('safa_ito_id');
        	$this->notification_model->saveDetails();
         	
	        //--------------------------------
         	}
       		//-------------------------------------------------------------------
         	
            
            
            
            $this->load->view('redirect_message', array('msg' => lang('global_updated_message'),
                'url' => site_url('ea/trip_internaltrip'),
                'id' => $this->uri->segment("4"),
                'model_title' => lang('node_title'), 'action' => lang('edit_trip_internaltrip')));
        }
    }

    function delete_($id = false) {
        if (!$id)
            show_404();

        $this->trip_internaltrip_model->safa_trip_internaltrip_id = $id;
        if (!$this->trip_internaltrip_model->delete())
            show_404();
        $this->load->view('redirect_message', array('msg' => lang('global_deleted_message'),
            'url' => site_url('ea/trip_internaltrip'),
            'model_title' => lang('node_title'), 'action' => ''));
    }

	function delete($id = false)
	{
		if (!$id)
		show_404();

		$this->trip_internaltrip_model->safa_trip_internaltrip_id = $id;
		$this->trip_internaltrip_model->deleted = 1;
		$this->trip_internaltrip_model->save();
		
		$this->load->view('redirect_message', array('msg' => lang('global_deleted_message'),
            'url' => site_url('ea/trip_internaltrip'),
            'model_title' => lang('node_title'), 'action' => ''));
	}
	
    function delete_all() {
        if ($this->input->post("delete_items")) {
            foreach ($this->input->post("delete_items") as $item) {
                $this->trip_internaltrip_model->safa_trip_internaltrip_id = $item;

                $this->trip_internaltrip_model->delete();
            }
        } else
            show_404();
        redirect("ea/trip_internaltrip/index");
    }

    function search() {
        if ($this->input->get("safa_trip_internaltrip_id"))
            $this->trip_internaltrip_model->safa_trip_internaltrip_id = $this->input->get("safa_trip_internaltrip_id");
        if ($this->input->get("safa_trip_id"))
            $this->trip_internaltrip_model->safa_trip_id = $this->input->get("safa_trip_id");
        if ($this->input->get("safa_ito_id"))
            $this->trip_internaltrip_model->safa_ito_id = $this->input->get("safa_ito_id");
        if ($this->input->get("safa_transporter_id"))
            $this->trip_internaltrip_model->safa_transporter_id = $this->input->get("safa_transporter_id");
        if ($this->input->get("safa_tripstatus_id"))
            $this->trip_internaltrip_model->safa_tripstatus_id = $this->input->get("safa_tripstatus_id");
        if ($this->input->get("safa_internaltripstatus_id"))
            $this->trip_internaltrip_model->safa_internaltripstatus_id = $this->input->get("safa_internaltripstatus_id");
        if ($this->input->get("operator_reference"))
            $this->trip_internaltrip_model->operator_reference = $this->input->get("operator_reference");
        if ($this->input->get("attachement"))
            $this->trip_internaltrip_model->attachement = $this->input->get("attachement");
        if ($this->input->get("datetime"))
            $this->trip_internaltrip_model->datetime = $this->input->get("datetime");
        /* search utility */
        if ($this->input->get("uo_contract_id"))
            $this->trip_internaltrip_model->by_contract = $this->input->get("uo_contract_id");
        /* search utility */
    }

    function create_contracts_array() {
        $result = $this->trip_internaltrip_model->get_ea_contracts();
        $contracts = array();
        $contracts[""] = lang('global_select_from_menu');
        //$name = name();
        foreach ($result as $con_id => $contract) {
            $contracts[$contract->contract_id] = $contract->safa_uo_contracts_eas_name;
        }
        return $contracts;
    }

    function create_uos_array() {
        $result = $this->trip_internaltrip_model->get_ea_uos();
        $eas = array();
        $eas[""] = lang('global_select_from_menu');
        foreach ($result as $row) {
            $eas[$row->safa_uo_contract_id] = $row->safa_uo_contracts_eas_name;
        }
        return $eas;
    }
   
    function create_transportes_array() {
        $result = $this->trip_internaltrip_model->get_transporters_withContracts();
        $transporters = array();
        $transporters[""] = lang('global_select_from_menu');
        $name = name();
        foreach ($result as $transporter_id => $transporter) {
            $transporters[$transporter->transporter_id] = $transporter->$name;
        }
        return $transporters;
    }

    function get_ea_trips($contract_id = FALSE) {
        $this->layout = 'ajax'; //  to prevent the hook in loading the header in the response//
        $trips_ea = $this->trip_internaltrip_model->get_ea_trips(session('ea_id'));
        $trips = array();
        echo "<option value=''>" . lang('global_select_from_menu') . "</option>";
        if (isset($trips_ea) && count($trips_ea) > 0) {

            foreach ($trips_ea as $trip) {
                echo "<option value='" . $trip->safa_trip_id . "'>" . $trip->name . "</option>";
            }
        }
    }

    function safa_tripstatus($safa_ito_id = FALSE) {
        $safa_intrernaltripstatus = ddgen("safa_internaltripstatus", array("safa_internaltripstatus_id", name()));
//        $trips = array();
//        echo "<option value=''>" . lang('global_select_from_menu') . "</option>";
        if (isset($safa_intrernaltripstatus) && count($safa_intrernaltripstatus) > 0) {

            foreach ($safa_intrernaltripstatus as $intrernaltripstatus => $value) {
                echo "<option value='" . $intrernaltripstatus . "'>" . $value . "</option>";
            }
        }
    }

    function fileupload() {

        $this->change_upload_name();
        $config['upload_path'] = './static/temp/ea_files';
        $config['allowed_types'] = 'pdf|jpg|jpeg';
        $config['max_size'] = '1024';
        $config['max_width'] = '1024';
        $config['max_height'] = '768';
        $config['overwrite'] = 'true';
        $config['encrypt_name'] = 'true';
        $this->load->library('upload', $config);

        if (!$this->upload->do_upload()) {
            $error = $this->upload->display_errors();
            $this->form_validation->set_message('fileupload', $error);
            return false;
        } else {
            /* itinitailize object with uploaded data */
            /*
              $data = $this->upload->data();
              $this->uploaded_file_name = $data['file_name'];
              $this->temp_upload_path = $data['file_path'];
             */
        }
    }

    function move_upload() {
        if (file_exists('./static/temp/ea_files/' . session('internaltrip_file_upload_time') . $this->input->post('text_input'))) {
            if (!copy('./static/temp/ea_files/' . session('internaltrip_file_upload_time') . $this->input->post('text_input'), './static/ea_files/' . session('internaltrip_file_upload_time') . $this->input->post('text_input')))
                if (!move_uploaded_file('./static/temp/ea_files/' . session('internaltrip_file_upload_time') . $this->input->post('text_input'), './static/uo_files/' . session('internaltrip_file_upload_time') . $this->input->post('text_input')))
                    echo "<script>alert('error uploading a file')</script>";
        } else
            echo "<script>alert('error uploading a file')</script>";
    }

    function get_ito_refcode($ito_id = false) {
        $this->layout = 'ajax';
        $this->load->model('itos_model');
        $this->itos_model->custom_select = 'reference_code';
        $this->itos_model->safa_ito_id = $ito_id;
        $result = $this->itos_model->get();
        if (isset($result))
            echo $result->reference_code;
    }

    function get_ea_contracts_itos() { // get the itos that involved in  the same contracts of that uo
        $result = $this->trip_internaltrip_model->get_ea_contracts_itos();
        $itos = array();
        $itos[""] = lang('global_select_from_menu');
        $name = name();
        foreach ($result as $itos_id => $ito) {
            $itos[$ito->safa_ito_id] = $ito->$name;
        }
        return $itos;
    }

    function change_upload_name() {
        $upload_time = rand(0, 9999) . time();
        session('internaltrip_file_upload_time', $upload_time);
        $file_name = $_FILES['userfile']['name'];
        $uplaod_name = session('internaltrip_file_upload_time') . $file_name;
        $_FILES['userfile']['name'] = $uplaod_name;
    }

    function get_trip_contract_id($trip_id) {
        $contract = $this->trip_internaltrip_model->get_trip_contract_id($trip_id);
        if(count($contract)>0) {
        	return $contract->safa_uo_contract_id;
        }
    }
    
    
}

/* End of file trip_internaltrip.php */
/* Location: ./application/controllers/trip_internaltrip.php */