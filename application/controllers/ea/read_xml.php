<?php

class Read_xml extends Safa_Controller {

    public $data;

    public function __construct() {
        parent::__construct();
        $this->lang->load('read_xml');
        $this->layout = 'new';
        //Side menu session, By Gouda.
        session('side_menu_id', 5);

        
        $this->load->library('xmltoarray');
        $this->load->library('structure');
        $this->load->helper('read_xml');
        $this->load->helper('download');
                
        $this->load->model('Safa_group_passports_model');
        permission();
    }

    public function index() {
        $this->load->library("form_validation");
        $this->form_validation->set_rules('file_type', 'lang:file_type', 'trim|required|callback_check_file_type');
        $this->form_validation->set_rules('safa_group_id', 'lang:group_name', 'trim|required');
        $this->form_validation->set_rules('text_input', 'lang:read_xml_file', 'trim|required');
        $this->form_validation->set_rules('userfile', 'lang:read_xml_file', 'trim|callback_fileupload');
        if ($this->form_validation->run() == false) {
            $this->load->view("ea/upload_xml");
        } else {
            $group_id  = $this->input->post('safa_group_id');
            $file_type = $this->input->post('file_type');
            $file_data = $this->upload->data();
            $file_name = $file_data['file_name'];
            $xmlArray  = getXmlArray($file_name, $file_type);
            if ($xmlArray) {
                $this->save_data_file($xmlArray, $group_id);
                $this->load->view('redirect_message', array('msg' => lang('global_added_successfully'),
                    'url' => site_url('ea/read_xml'),
                    'model_title' => lang('read_xml_upload_file'), 'action' => lang('read_xml_upload_file')));
            }
        }
    }

    function fileupload() {
        $config['upload_path'] = './static/xml';
        $config['allowed_types'] = 'xml|txt';

        if (strpos($this->input->post('text_input'), '.safa') !== false && $this->input->post('file_type') == 4) {
            $config['allowed_types'] = '*';
        }

        $config['overwrite'] = 'true';
        $this->load->library('upload', $config);
        if (!$this->upload->do_upload()) {
            $error = $this->upload->display_errors();
            $this->form_validation->set_message('fileupload', $error);
            return false;
        }
    }

    function download($data, $writeFile_type) {

        if ($writeFile_type == 1) {
            force_download('جاما.XML', $data);
        } elseif ($writeFile_type == 2) {
            force_download('باب العمرة.XML', $data);
        } elseif ($writeFile_type == 3) {
            force_download('طريق  العمرة.txt', $data);
        } elseif ($writeFile_type == 4) {
            force_download('file.safa', $data);
        }
    }

    function save_data_file($xmlArray, $group_id) {

        $structure = array('no' => 'MD_NO',
            'title_id' => 'MD_TITLE_ID',
            'first_name_la' => 'MD_FIRST_NAME_LA',
            'second_name_la' => 'MD_SECOND_NAME_LA',
            'third_name_la' => 'MD_THIRD_NAME_LA',
            'fourth_name_la' => 'MD_FOURTH_NAME_LA',
            'first_name_ar' => 'MD_FIRST_NAME_AR',
            'second_name_ar' => 'MD_SECOND_NAME_AR',
            'third_name_ar' => 'MD_THIRD_NAME_AR',
            'fourth_name_ar' => 'MD_FOURTH_NAME_AR',
            'nationality_id' => 'MD_NATIONALITY_ID',
            'passport_no' => 'MD_PASSPORT_NO',
            'passport_type_id' => 'MD_PASSPORT_TYPE',
            'passport_issue_date' => 'MD_PASSPORT_ISSUE_DATE',
            'passport_expiry_date' => 'MD_PASSPORT_EXPIRY_DATE',
            'passport_issuing_city' => 'MD_PASSPORT_ISSUING_CITY',
            'passport_issuing_country_id' => 'MD_PASSPORT_ISSUING_COUNTRY_ID',
            'passport_dpn_count' => 'MD_PASSPORT_DPN_COUNT',
            'dpn_serial_no' => 'MD_DPN_SERIAL_NO',
            'gender_id' => 'MD_GENDER_ID',
            'marital_status_id' => 'MD_MARITAL_STATUS_ID',
            'date_of_birth' => 'MD_DATE_OF_BIRTH',
            'birth_city' => 'MD_BIRTH_CITY',
            'birth_country_id' => 'MD_BIRTH_COUNTRY_ID',
            'relative_no' => 'MD_RELATIVE_NO',
            'relative_relation_id' => 'MD_RELATIVE_RELATION_ID',
            'educational_level_id' => 'MD_EDUCATIONAL_LEVEL',
            'occupation' => 'MD_OCCUPATION',
            'city' => 'MD_CITY',
            'erp_country_id' => 'MD_COUNTRY_ID', 
            'age' => 'MD_AGE',
            'previous_nationality_id' => 'MD_PREVIOUS_NATIONALITY_ID', 
            'relative_gender_id' => 'MD_RELATIVE_GENDER_ID',
            'mofa' => 'MD_MOFA', 'enumber' => 'MD_ENUMBER',
            'id_number' => 'MD_IDNUMBER',
            'job2' => 'MD_JOB2', 'address' => 'MD_ADDRESS',
        );

        $a = 0;
        $input = $xmlArray['MUTAMERS'];

        foreach ($input as $inputvalue) {
            foreach ($structure as $key => $value) {

                if (isset($inputvalue[$value]))
                    $mutamers[$a][$key] = $inputvalue[$value];
                else
                    $mutamers[$a][$key] = '';
            }$a++;
        }
//        print_r($mutamers);exit();
        for ($i = 0; $i < sizeof($mutamers); $i++) {
            $mutamers[$i]['safa_group_id'] = $group_id;
            $mutamers[$i]['passport_issue_date'] = date('Y-m-d', strtotime($mutamers[$i]['passport_issue_date']));
            $mutamers[$i]['passport_expiry_date'] = date('Y-m-d', strtotime($mutamers[$i]['passport_expiry_date']));
            if (!$mutamers[$i]['age']) {
                if (strpos($mutamers[$i]['date_of_birth'], '/') == TRUE)
                    $birthDate = explode("/", "" . $mutamers[$i]['date_of_birth'] . "");
                else
                    $birthDate = explode("-", "" . $mutamers[$i]['date_of_birth'] . "");
                $mutamers[$i]['age'] = (date("md", date("U", mktime(0, 0, 0, $birthDate[0], $birthDate[1], $birthDate[2]))) > date("md") ? ((date("Y") - $birthDate[2]) - 1) : (date("Y") - $birthDate[2]));
            }
            $mutamers[$i]['date_of_birth'] = date('Y-m-d', strtotime($mutamers[$i]['date_of_birth']));
            $mutamers[$i]['display_order'] = $this->Safa_group_passports_model->calc_next_no_field($group_id, $i + 1);
        }
//        print_r($mutamers);exit();
        foreach ($mutamers as $mutamer) {
            $this->Safa_group_passports_model->safa_group_id = $group_id;
            $this->Safa_group_passports_model->passport_no = $mutamer['passport_no'];
            $this->Safa_group_passports_model->nationality_id = $mutamer['nationality_id'];
            $this->Safa_group_passports_model->ea_id = session('ea_id');
            $this->Safa_group_passports_model->dpn_serial_no = $mutamer['dpn_serial_no'];

            $rows_count = $this->Safa_group_passports_model->get(true);

            //To reset them to default value;
            $this->Safa_group_passports_model->nationality_id = false;
            $this->Safa_group_passports_model->passport_no = false;
            $this->Safa_group_passports_model->dpn_serial_no = false;

            if ($rows_count < 1) {
                $this->db->insert('safa_group_passports', $mutamer);
                $safa_group_passport_id = $this->db->insert_id();


                //------------------------------ By Gouda, To link relations between passports ------------------
                if ($mutamer['relative_no'] != 0) {

                    $this->Safa_group_passports_model->no = $mutamer['relative_no'];
                    $group_passports_rows = $this->Safa_group_passports_model->get();
                    //To reset them to default value;
                    $this->Safa_group_passports_model->no = false;

                    if (count($group_passports_rows) > 0) {

                        $max_safa_group_passport_id = 0;
                        foreach ($group_passports_rows as $group_passports_row) {
                            if ($group_passports_row->safa_group_passport_id > $max_safa_group_passport_id) {
                                $max_safa_group_passport_id = $group_passports_row->safa_group_passport_id;
                            }
                        }
                        $this->Safa_group_passports_model->relative_no = $max_safa_group_passport_id;
                        $this->Safa_group_passports_model->safa_group_passport_id = $safa_group_passport_id;
                        $this->Safa_group_passports_model->save();

                        //To reset them to default value;
                        $this->Safa_group_passports_model->relative_no = false;
                        $this->Safa_group_passports_model->safa_group_passport_id = false;
                    }
                }
                //-----------------------------------------------------------------------------------------------
                //By Gouda. To save modification time.
                $this->db->insert('safa_group_passport_modifications', array(
                    'safa_group_passport_id' => $safa_group_passport_id,
                    'user_type' => 3,
                    'user_id' => session('ea_user_id'),
                    'datetime' => date('Y-m-d H:i'),
                ));
                $c = $this->db->update('safa_groups', array('last_update' => date('Y-m-d H:i')), " `safa_group_id` = '" . $group_id . "'");
            }
        }
    }

    function getDB_Data() {
        $data = $this->Safa_group_passports_model->get();
        $structure = array('MD_NO' => 'no',
            'MD_TITLE_ID' => 'title_id',
            'MD_FIRST_NAME_LA' => 'first_name_la',
            'MD_SECOND_NAME_LA' => 'second_name_la',
            'MD_THIRD_NAME_LA' => 'third_name_la',
            'MD_FOURTH_NAME_LA' => 'fourth_name_la',
            'MD_FIRST_NAME_AR' => 'first_name_ar',
            'MD_SECOND_NAME_AR' => 'second_name_ar',
            'MD_THIRD_NAME_AR' => 'third_name_ar',
            'MD_FOURTH_NAME_AR' => 'fourth_name_ar',
            'MD_NATIONALITY_ID' => 'nationality_id',
            'MD_PASSPORT_NO' => 'passport_no',
            'MD_PASSPORT_TYPE' => 'passport_type_id',
            'MD_PASSPORT_ISSUE_DATE' => 'passport_issue_date',
            'MD_PASSPORT_EXPIRY_DATE' => 'passport_expiry_date',
            'MD_PASSPORT_ISSUING_CITY' => 'passport_issuing_city',
            'MD_PASSPORT_ISSUING_COUNTRY_ID' => 'passport_issuing_country_id',
            'MD_PASSPORT_DPN_COUNT' => 'passport_dpn_count',
            'MD_DPN_SERIAL_NO' => 'dpn_serial_no',
            'MD_GENDER_ID' => 'gender_id',
            'MD_MARITAL_STATUS_ID' => 'marital_status_id',
            'MD_DATE_OF_BIRTH' => 'date_of_birth',
            'MD_BIRTH_CITY' => 'birth_city',
            'MD_BIRTH_COUNTRY_ID' => 'birth_country_id',
            'MD_RELATIVE_NO' => 'relative_no',
            'MD_RELATIVE_RELATION_ID' => 'relative_relation_id',
            'MD_EDUCATIONAL_LEVEL' => 'educational_level_id',
            'MD_OCCUPATION' => 'occupation',
            'MD_CITY' => 'city',
            'MD_COUNTRY_ID' => 'erp_country_id',
            'MD_AGE' => 'age',
            'MD_PREVIOUS_NATIONALITY_ID' => 'previous_nationality_id',
            'MD_RELATIVE_GENDER_ID' => 'relative_gender_id',
            'MD_MOFA' => 'mofa',
            'MD_ENUMBER' => 'enumber',
            'MD_IDNUMBER' => 'id_number',
            'MD_JOB2' => 'job2',
            'MD_ADDRESS' => 'address'
        );
        $a = 0;
        $input = $this->toArray($data);

        foreach ($input as $inputvalue) {
            foreach ($structure as $key => $value) {
                if (isset($inputvalue[$value]))
                    $mutamers[$a][$key] = $inputvalue[$value];
                else
                    $mutamers[$a][$key] = '';
            }$a++;
        }
        return(array('MUTAMERS' => $mutamers));
    }

    function check_file_type($file_type) {
//        print_r($_FILES);
        $this->form_validation->set_message('check_file_type', lang('form_validation_required'));
//        $e = explode('.', $_FILES["userfile"]["name"]);
//        $ext = end($e);
//        if($ext == 'xml' && ($file_type != 1 or $file_type != 2))
//            return FALSE;
//        if($ext == 'txt' && ($file_type != 3))
//            return FALSE;
//        if($ext == 'safa' && ($file_type != 4))
//            return FALSE;
//        if($ext != 'safa' && $ext != 'xml' && $ext != 'txt')
//            return FALSE;
        if($file_type == 0)
            return FALSE;
        
            return TRUE;
    }

    function toArray($obj) {
        if (is_object($obj))
            $obj = (array) $obj;
        if (is_array($obj)) {
            $new = array();
            foreach ($obj as $key => $val) {
                $new[$key] = $this->toArray($val);
            }
        } else {
            $new = $obj;
        }
        return $new;
    }

}
