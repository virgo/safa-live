<?php

class Umrahgroups_passports extends Safa_Controller {

    public $module = "umrahgroups_passports";

    public function __construct() {
        parent::__construct();
        $this->layout = 'new';
        //Side menu session, By Gouda.
        session('side_menu_id', 5);

        $this->load->model('countries_model');

        $this->load->helper('cookie');
        $this->load->model('safa_umrahgroups_passports_model');
        $this->load->model('safa_groups_model');
        $this->load->helper('read_xml');
        $this->load->helper('download');
        $this->lang->load('umrahgroups_passports');
        $this->lang->load('group_passports');
        permission();
        if (session('ea_id'))
            $this->safa_umrahgroups_passports_model->ea_id = session('ea_id');
    }

    public function index() {

        $ed_id = session('ea_id');

        //By Gouda 
        $ea_user_id = session('ea_user_id');

        if (isset($_GET['search'])) {
            $this->search();
            $data["items"] = $this->safa_umrahgroups_passports_model->get_for_table();
        }
        $this->safa_umrahgroups_passports_model->join = TRUE;
//        $data["total_rows"] = $this->safa_umrahgroups_passports_model->get(true);
//        $this->safa_umrahgroups_passports_model->offset = $this->uri->segment("4");
//        $this->safa_umrahgroups_passports_model->limit = $this->config->item('per_page');
//        $config['uri_segment'] = 4;
//        $config['base_url'] = site_url('ea/umrahgroups_passports/index');
//        $config['total_rows'] = $data["total_rows"];
//        $config['per_page'] = $this->safa_umrahgroups_passports_model->limit;
//        $config['suffix'] = $this->input->get() ? '?' . http_build_query($_GET) : NULL;
//        $this->load->library('pagination');
//        $this->pagination->initialize($config);
//        $data['pagination'] = $this->pagination->create_links();

        $cols = array('safa_umrahgroup_id', 'display_order', 'no', 'passport_no', 'passport_type_id', 'name_ar', 'name_la', 'erp_country_id', 'nationality_id', 'date_of_birth', 'age', 'passport_issue_date',
            'passport_expiry_date', 'passport_issuing_country_id', 'passport_issuing_city', 'dpn_serial_no', 'relative_relation_id',
            'occupation', 'city', 'marital_status_id', 'passport_dpn_count', 'educational_level_id', 'relative_no', 'birth_country_id', 'birth_city', 'relative_gender_id', 'name');


        $colums = array();
        foreach ($cols as $col) {

            if ($col == 'safa_umrahgroup_id') {
                if ($this->input->get("safa_umrahgroup_id")) {
                    continue;
                }
            } else if ($col == 'display_order') {
                if (!$this->input->get("safa_umrahgroup_id")) {
                    continue;
                }
            } else if ($col == 'relative_gender_id') {
                continue;
            }

            if ($this->input->cookie('' . $ea_user_id . '_' . $col . ''))
                $colums[] = $this->input->cookie('' . $ea_user_id . '_' . $col . '');
        }

        if (!empty($colums) && count($colums) >= 1 && is_array($colums))
            $data['cols'] = $colums;
        else
            $data['cols'] = $cols;
        $data['safa_umrahgroup_id'] = FALSE;
        $this->load->view('ea/umrahgroups_passports/index', $data);
    }

    public function passports_index($safa_umrahgroup_id = FALSE) {

        $ed_id = session('ea_id');

        //By Gouda 
        $ea_user_id = session('ea_user_id');

        if (isset($_GET['search']))
            $this->search();

        if ($safa_umrahgroup_id != 0 && $safa_umrahgroup_id != false) {
            $this->safa_umrahgroups_passports_model->safa_umrahgroup_id = $safa_umrahgroup_id;
        }

//        $data["total_rows"] = $this->safa_umrahgroups_passports_model->get(true);
//        $this->safa_umrahgroups_passports_model->offset = $this->uri->segment("5");
//        $this->safa_umrahgroups_passports_model->limit = $this->config->item('per_page');
        $data["items"] = $this->safa_umrahgroups_passports_model->get_for_table();
//        $config['uri_segment'] = 5;
//        $config['base_url'] = site_url('ea/umrahgroups_passports/passports_index/' . $safa_umrahgroup_id . '');
//        $config['total_rows'] = $data["total_rows"];
//        $config['per_page'] = $this->safa_umrahgroups_passports_model->limit;
//        $config['suffix'] = $this->input->get() ? '?' . http_build_query($_GET) : NULL;
//        $this->load->library('pagination');
//        $this->pagination->initialize($config);
//        $data['pagination'] = $this->pagination->create_links();

        /*
          $cols = array('safa_umrahgroup_id', 'display_order', 'passport_no', 'name_la', 'no', 'name_ar', 'nationality_id', 'passport_type_id', 'passport_issue_date',
          'passport_expiry_date', 'passport_issuing_city', 'passport_issuing_country_id', 'passport_dpn_count', 'dpn_serial_no', 'relative_relation_id',
          'educational_level_id', 'occupation', 'date_of_birth', 'age', 'birth_city', 'birth_country_id', 'relative_no', 'relative_gender_id', 'marital_status_id', 'city', 'erp_country_id');
         */

        $cols = array('safa_umrahgroup_id', 'display_order', 'no', 'passport_no', 'passport_type_id', 'name_ar', 'name_la', 'erp_country_id', 'nationality_id', 'date_of_birth', 'age', 'passport_issue_date',
            'passport_expiry_date', 'passport_issuing_country_id', 'passport_issuing_city', 'dpn_serial_no', 'relative_relation_id',
            'occupation', 'city', 'marital_status_id', 'passport_dpn_count', 'educational_level_id', 'relative_no', 'birth_country_id', 'birth_city', 'relative_gender_id', 'name');


        $colums = array();
        foreach ($cols as $col) {

            if ($col == 'safa_umrahgroup_id') {
                if ($this->input->get("safa_umrahgroup_id") || ($safa_umrahgroup_id != 0 && $safa_umrahgroup_id != false)) {
                    continue;
                }
            } else if ($col == 'display_order') {
                if (!$this->input->get("safa_umrahgroup_id")) {
                    continue;
                }
            } else if ($col == 'relative_gender_id') {
                continue;
            }

            if ($this->input->cookie('' . $ea_user_id . '_' . $col . '')) {
                $colums[] = $this->input->cookie('' . $ea_user_id . '_' . $col . '');
            }
        }

        if (!empty($colums) && count($colums) >= 1 && is_array($colums))
            $data['cols'] = $colums;
        else {

            if ($this->input->get("safa_umrahgroup_id")) {
                $data['cols'] = array('display_order', 'passport_no', 'name_la', 'no', 'name_ar');
            } else {
                $data['cols'] = array('group_id', 'passport_no', 'name_la', 'no', 'name_ar');
            }
        }
        $data['safa_umrahgroup_id'] = $safa_umrahgroup_id;
        $this->load->view('ea/umrahgroups_passports/index', $data);
    }

    function columns() {
        $ed_id = session('ea_id');

        //By Gouda 
        $ea_user_id = session('ea_user_id');

        $this->layout = 'js';
        $this->load->library("form_validation");
        $this->form_validation->set_rules('cols[]', 'lang:choose_cols', 'trim|required');
        $cols = array('safa_umrahgroup_id', 'display_order', 'passport_no', 'name_la', 'no', 'name_ar', 'nationality_id', 'passport_type_id', 'passport_issue_date',
            'passport_expiry_date', 'passport_issuing_city', 'passport_issuing_country_id', 'passport_dpn_count', 'dpn_serial_no', 'relative_relation_id',
            'educational_level_id', 'occupation', 'date_of_birth', 'age', 'birth_city', 'birth_country_id', 'relative_no', 'relative_gender_id', 'marital_status_id', 'city', 'erp_country_id', 'name');
        if ($this->form_validation->run() == false) {
            $colums = array();
            foreach ($cols as $col) {
                if ($this->input->cookie('' . $ea_user_id . '_' . $col . ''))
                    $colums[] = $this->input->cookie('' . $ea_user_id . '_' . $col . '');
            }
            if (!empty($colums) && count($colums) >= 1 && is_array($colums))
                $data['cols'] = $colums;
            else
                $data['cols'] = $cols;
            $this->load->view("ea/umrahgroups_passports/colum", $data);
        } else {
            if ($this->input->post('cols')) {
                foreach ($cols as $col) {
                    if ($this->input->cookie('' . $ea_user_id . '_' . $col . ''))
                        setcookie('' . $ea_user_id . '_' . $col . '', '' . $col . '', time() - 60 * 60 * 24 * 365);
                }
                $data = $this->input->post('cols');
                foreach ($data as $col) {
                    setcookie('' . $ea_user_id . '_' . $col . '', '' . $col . '', time() + 60 * 60 * 24 * 365);
                }
            }


            //By Gouda
            //$rows_count_per_page=$this->input->post('rows_count_per_page');
            //setcookie($ea_user_id . '_umrahgroups_passports_rows_count_per_page', $rows_count_per_page , time() + 60 * 60 * 24 * 365);

            $this->load->view('redirect_message_popup', array('msg' => lang('uploaded_successfully_message'),
                'url' => site_url("ea/umrahgroups_passports/columns"),
                'id' => $this->uri->segment("4"),
                'model_title' => lang('title'), 'action' => lang('upload_files')));
        }
    }

    #TO Show Mutamer data

    public function show_data($id = FALSE) {

        $this->layout = 'js';

        if (!$id)
            show_404();

        $this->safa_umrahgroups_passports_model->safa_umrahgroups_passport_id = $id;
        $data['item'] = $this->safa_umrahgroups_passports_model->get();
        $this->load->view('ea/umrahgroups_passports/show_data', $data);
    }

    public function edit($id = FALSE) {
        if (!$id)
            show_404();
        $this->safa_umrahgroups_passports_model->safa_umrahgroups_passport_id = $id;
        $data['item'] = $this->safa_umrahgroups_passports_model->get();

        if (!$data['item'])
            show_404();


        $data['relative_rows'] = $this->safa_umrahgroups_passports_model->get_by_relative_no($data['item']->safa_group_passport_id, $data['item']->safa_umrahgroup_id);


        $this->load->library("form_validation");
        $this->form_validation->set_rules('safa_umrahgroup_id', 'lang:umrahgroups_passports_safa_safa_umrahgroup_id', 'trim');
        $this->form_validation->set_rules('no', 'lang:umrahgroups_passports_no', 'trim');
        $this->form_validation->set_rules('title_id', 'lang:umrahgroups_passports_title_id', 'trim');
        $this->form_validation->set_rules('first_name_la', 'lang:umrahgroups_passports_first_name_la', 'trim');
        $this->form_validation->set_rules('second_name_la', 'lang:umrahgroups_passports_second_name_la', 'trim');
        $this->form_validation->set_rules('third_name_la', 'lang:umrahgroups_passports_third_name_la', 'trim');
        $this->form_validation->set_rules('fourth_name_la', 'lang:umrahgroups_passports_fourth_name_la', 'trim');
        $this->form_validation->set_rules('first_name_ar', 'lang:umrahgroups_passports_first_name_ar', 'trim');
        $this->form_validation->set_rules('second_name_ar', 'lang:umrahgroups_passports_second_name_ar', 'trim');
        $this->form_validation->set_rules('third_name_ar', 'lang:umrahgroups_passports_third_name_ar', 'trim');
        $this->form_validation->set_rules('fourth_name_ar', 'lang:umrahgroups_passports_fourth_name_ar', 'trim');
        $this->form_validation->set_rules('nationality_id', 'lang:umrahgroups_passports_nationality_id', 'trim');
        $this->form_validation->set_rules('previous_nationality_id', 'lang:umrahgroups_passports_previous_nationality_id', 'trim');
        $this->form_validation->set_rules('passport_no', 'lang:umrahgroups_passports_passport_no', 'trim');
        $this->form_validation->set_rules('passport_type_id', 'lang:umrahgroups_passports_passport_type_id', 'trim');
        $this->form_validation->set_rules('passport_issue_date', 'lang:umrahgroups_passports_passport_issue_date', 'trim');
        $this->form_validation->set_rules('passport_expiry_date', 'lang:umrahgroups_passports_passport_expiry_date', 'trim');
        $this->form_validation->set_rules('passport_issuing_city', 'lang:umrahgroups_passports_passport_issuing_city', 'trim');
        $this->form_validation->set_rules('passport_issuing_country_id', 'lang:umrahgroups_passports_passport_issuing_country_id', 'trim');
        $this->form_validation->set_rules('passport_dpn_count', 'lang:group_passports_passport_dpn_count', 'trim');
        $this->form_validation->set_rules('dpn_serial_no', 'lang:umrahgroups_passports_dpn_serial_no', 'trim');
        $this->form_validation->set_rules('relative_relation_id', 'lang:umrahgroups_passports_relative_relation_id', 'trim');
        $this->form_validation->set_rules('educational_level_id', 'lang:umrahgroups_passports_educational_level_id', 'trim');
        $this->form_validation->set_rules('occupation', 'lang:umrahgroups_passports_occupation', 'trim');
        $this->form_validation->set_rules('date_of_birth', 'lang:umrahgroups_passports_date_of_birth', 'trim');
        //$this->form_validation->set_rules('age', 'lang:umrahgroups_passports_age', 'trim');
        $this->form_validation->set_rules('birth_city', 'lang:umrahgroups_passports_birth_city', 'trim');
        $this->form_validation->set_rules('birth_country_id', 'lang:umrahgroups_passports_birth_country_id', 'trim');
        $this->form_validation->set_rules('relative_no', 'lang:umrahgroups_passports_relative_no', 'trim');
        $this->form_validation->set_rules('relative_gender_id', 'lang:umrahgroups_passports_relative_gender_id', 'trim');
        $this->form_validation->set_rules('gender_id', 'lang:umrahgroups_passports_gender_id', 'trim');
        $this->form_validation->set_rules('marital_status_id', 'lang:umrahgroups_passports_marital_status_id', 'trim');
        $this->form_validation->set_rules('city', 'lang:umrahgroups_passports_city', 'trim');
        $this->form_validation->set_rules('erp_country_id', 'lang:umrahgroups_passports_erp_country_id', 'trim');

        if ($this->form_validation->run() == false) {
            $this->load->view("ea/umrahgroups_passports/edit", $data);
        } else {
            $this->safa_umrahgroups_passports_model->safa_umrahgroup_id = $this->input->post('safa_umrahgroup_id');
            $this->safa_umrahgroups_passports_model->no = $this->input->post('no');
            $this->safa_umrahgroups_passports_model->title_id = $this->input->post('title_id');
            $this->safa_umrahgroups_passports_model->first_name_la = $this->input->post('first_name_la');
            $this->safa_umrahgroups_passports_model->second_name_la = $this->input->post('second_name_la');
            $this->safa_umrahgroups_passports_model->third_name_la = $this->input->post('third_name_la');
            $this->safa_umrahgroups_passports_model->fourth_name_la = $this->input->post('fourth_name_la');
            $this->safa_umrahgroups_passports_model->first_name_ar = $this->input->post('first_name_ar');
            $this->safa_umrahgroups_passports_model->second_name_ar = $this->input->post('second_name_ar');
            $this->safa_umrahgroups_passports_model->third_name_ar = $this->input->post('third_name_ar');
            $this->safa_umrahgroups_passports_model->fourth_name_ar = $this->input->post('fourth_name_ar');
            $this->safa_umrahgroups_passports_model->nationality_id = $this->input->post('nationality_id');
            $this->safa_umrahgroups_passports_model->previous_nationality_id = $this->input->post('previous_nationality_id');
            $this->safa_umrahgroups_passports_model->passport_no = $this->input->post('passport_no');
            $this->safa_umrahgroups_passports_model->passport_type_id = $this->input->post('passport_type_id');
            $this->safa_umrahgroups_passports_model->passport_issue_date = $this->input->post('passport_issue_date');
            $this->safa_umrahgroups_passports_model->passport_expiry_date = $this->input->post('passport_expiry_date');
            $this->safa_umrahgroups_passports_model->passport_issuing_city = $this->input->post('passport_issuing_city');
            $this->safa_umrahgroups_passports_model->passport_issuing_country_id = $this->input->post('passport_issuing_country_id');
            $this->safa_umrahgroups_passports_model->passport_dpn_count = $this->input->post('passport_dpn_count');
            $this->safa_umrahgroups_passports_model->dpn_serial_no = $this->input->post('dpn_serial_no');
            $this->safa_umrahgroups_passports_model->relative_relation_id = $this->input->post('relative_relation_id');
            $this->safa_umrahgroups_passports_model->educational_level_id = $this->input->post('educational_level_id');
            $this->safa_umrahgroups_passports_model->occupation = $this->input->post('occupation');
            $this->safa_umrahgroups_passports_model->date_of_birth = $this->input->post('date_of_birth');
            //$this->safa_umrahgroups_passports_model->age = $this->input->post('age');
            $this->safa_umrahgroups_passports_model->birth_city = $this->input->post('birth_city');
            $this->safa_umrahgroups_passports_model->birth_country_id = $this->input->post('birth_country_id');
            $this->safa_umrahgroups_passports_model->relative_no = $this->input->post('relative_no');
            $this->safa_umrahgroups_passports_model->relative_gender_id = $this->input->post('relative_gender_id');
            $this->safa_umrahgroups_passports_model->gender_id = $this->input->post('gender_id');
            $this->safa_umrahgroups_passports_model->marital_status_id = $this->input->post('marital_status_id');
            $this->safa_umrahgroups_passports_model->city = $this->input->post('city');
            $this->safa_umrahgroups_passports_model->erp_country_id = $this->input->post('erp_country_id');

            $this->safa_umrahgroups_passports_model->mofa = $this->input->post('mofa');
            $this->safa_umrahgroups_passports_model->mofa_date = $this->input->post('mofa_date');
            $this->safa_umrahgroups_passports_model->enumber = $this->input->post('enumber');
            $this->safa_umrahgroups_passports_model->enumber_date = $this->input->post('enumber_date');
            $this->safa_umrahgroups_passports_model->visa_number = $this->input->post('visa_number');
            $this->safa_umrahgroups_passports_model->visa_date = $this->input->post('visa_date');
            $this->safa_umrahgroups_passports_model->border_number = $this->input->post('border_number');

            $this->safa_umrahgroups_passports_model->address = $this->input->post('address');


            $this->safa_umrahgroups_passports_model->save();
            $this->load->view('redirect_message', array('msg' => lang('global_updated_message'),
                'url' => site_url('ea/umrahgroups_passports/passports_index/' . $this->safa_umrahgroups_passports_model->safa_umrahgroup_id),
                'id' => $id,
                'model_title' => lang('umrahgroups_passports_title'), 'action' => lang('umrahgroups_passports_edit_title')));
        }
    }

    //copy&&cut mutamers from umrah group to another
    public function copy_cut() {
        $mutamers = $this->input->post("mutamers");
        if ($this->input->post('safa_umrahgroup_id'))
            $data['safa_umrahgroup_id'] = $this->input->post('safa_umrahgroup_id');
        else
            $data['safa_umrahgroup_id'] = FALSE;
        if (isset($mutamers) && count($mutamers) > 0 && is_array($mutamers))
            $data['mutamers'] = serialize($mutamers);
        else
            $data['mutamers'] = $mutamers;
        $data['copy_cut'] = $this->input->post("copy_cut_val");
//        $data["uo_contracts"] = $this->create_contracts_array();
        $this->load->library("form_validation");
        if (isset($_POST['add']))
            $this->form_validation->set_rules('safa_umrahgroup', 'lang:umrahgroups_passports_group_name', 'trim|required|callback_check_relative_no|callback_check_mutamers_dpncount');

        if ($this->form_validation->run() == false) {
            $this->load->view('ea/umrahgroups_passports/copy_cut', $data);
        } else {
            if ($this->input->post('copy_cut_val') == 'cut') {

                //************insert into safa_umrahgroups_passports*************************
                $mutamers2 = unserialize($this->input->post("mutamers"));
                $cut_umrahgroup_id = $this->input->post('safa_umrahgroup');
                if ($this->input->post('safa_umrahgroup_id'))
                    $safa_umrahgroup_id = $this->input->post('safa_umrahgroup_id');
                if (isset($mutamers2) && count($mutamers2) > 0) {
                    foreach ($mutamers2 as $mutamer) {

                        $this->safa_umrahgroups_passports_model->safa_umrahgroups_passport_id = $mutamer;
                        $mutamers_info[] = $this->safa_umrahgroups_passports_model->get();
                    }
                    $input = $this->toArray($mutamers_info);
//                    print_r($input);
//                                        exit();
                    for ($i = 0; $i < sizeof($input); $i++) {
                        if ($this->copy_cut_check($cut_umrahgroup_id, $input[$i]['passport_no']))
                            $unique_mutamers[] = $input[$i];
                        else
                            $exist_mutamers[] = $input[$i];
                    }
                    if (isset($unique_mutamers) && count($unique_mutamers) > 0) {
                        for ($i = 0; $i < sizeof($input); $i++) {
                            $this->db->set('safa_umrahgroup_id', $cut_umrahgroup_id, FALSE);
                            $this->db->where('safa_umrahgroups_passport_id', $input[$i]['safa_umrahgroups_passport_id']);
                            $saved = $this->db->update('safa_umrahgroups_passports');
                        }
                        if ($saved >= 1) {
                            if (isset($exist_mutamers) && count($exist_mutamers) > 0) {
                                if (isset($safa_umrahgroup_id)) {
                                    $this->load->view('ea/umrahgroups_passports/redirect_message', array('msg' => lang('cut_umrahgroups_passports_some_mutamerds_exist_msg'),
                                        'url' => site_url('ea/umrahgroups_passports/passports_index/' . $safa_umrahgroup_id . ''),
                                        'model_title' => lang('umrahgroups_passports_title'), 'action' => lang('cut_to_umrahgroups_passports'),
                                        'exist_mutamers' => $exist_mutamers));
                                } else {
                                    $this->load->view('ea/umrahgroups_passports/redirect_message', array('msg' => lang('cut_umrahgroups_passports_some_mutamerds_exist_msg'),
                                        'url' => site_url('ea/umrahgroups_passports'),
                                        'model_title' => lang('umrahgroups_passports_title'), 'action' => lang('cut_to_umrahgroups_passports'),
                                        'exist_mutamers' => $exist_mutamers));
                                }
                            } else {
                                if (isset($safa_umrahgroup_id)) {
                                    $this->load->view('redirect_message', array('msg' => lang('global_cut_message'),
                                        'url' => site_url('ea/umrahgroups_passports/passports_index/' . $safa_umrahgroup_id . ''),
                                        'model_title' => lang('umrahgroups_passports_title'), 'action' => lang('cut_to_umrahgroups_passports')));
                                } else {
                                    $this->load->view('redirect_message', array('msg' => lang('global_cut_message'),
                                        'url' => site_url('ea/umrahgroups_passports'),
                                        'model_title' => lang('umrahgroups_passports_title'), 'action' => lang('cut_to_umrahgroups_passports')));
                                }
                            }
                        } else {
                            if (isset($safa_umrahgroup_id)) {
                                $this->load->view('redirect_message', array('msg' => lang('global_error_save'),
                                    'url' => site_url('ea/umrahgroups_passports/passports_index/' . $safa_umrahgroup_id . ''),
                                    'model_title' => lang('umrahgroups_passports_title'), 'action' => lang('cut_to_umrahgroups_passports')));
                            } else {
                                $this->load->view('redirect_message', array('msg' => lang('global_error_save'),
                                    'url' => site_url('ea/umrahgroups_passports'),
                                    'model_title' => lang('umrahgroups_passports_title'), 'action' => lang('cut_to_umrahgroups_passports')));
                            }
                        }
                    } else {
                        if (isset($safa_umrahgroup_id)) {
                            $this->load->view('redirect_message', array('msg' => lang('copy_cut_umrahgroups_passports_mutamerds_exist_msg'),
                                'url' => site_url('ea/umrahgroups_passports/passports_index/' . $safa_umrahgroup_id . ''),
                                'model_title' => lang('umrahgroups_passports_title'), 'action' => lang('cut_to_umrahgroups_passports')));
                        } else {
                            $this->load->view('redirect_message', array('msg' => lang('copy_cut_umrahgroups_passports_mutamerds_exist_msg'),
                                'url' => site_url('ea/umrahgroups_passports'),
                                'model_title' => lang('umrahgroups_passports_title'), 'action' => lang('cut_to_umrahgroups_passports')));
                        }
                    }
                } else {
                    if (isset($safa_umrahgroup_id))
                        redirect('ea/umrahgroups_passports/passports_index/' . $safa_umrahgroup_id . '');
                    else
                        redirect('ea/umrahgroups_passports/index');
                }
            } elseif ($this->input->post('copy_cut_val') == 'copy') {
                if ($this->input->post('safa_umrahgroup_id'))
                    $safa_umrahgroup_id = $this->input->post('safa_umrahgroup_id');
                $copy_umrahgroup_id = $this->input->post("safa_umrahgroup");
                $mutamers2 = unserialize($this->input->post("mutamers"));
                if (isset($mutamers2) && count($mutamers2) > 0) {
                    foreach ($mutamers2 as $mutamer) {

                        $this->safa_umrahgroups_passports_model->safa_umrahgroups_passport_id = $mutamer;
                        $mutamers_info[] = $this->safa_umrahgroups_passports_model->get();
                    }
                    $input = $this->toArray($mutamers_info);
//                    print_r($input);
//                                        exit();
                    for ($i = 0; $i < sizeof($input); $i++) {
                        if ($this->copy_cut_check($copy_umrahgroup_id, $input[$i]['passport_no']))
                            $unique_mutamers[] = $input[$i];
                        else
                            $exist_mutamers[] = $input[$i];
                    }
                    if (isset($unique_mutamers) && count($unique_mutamers) > 0) {
                        for ($i = 0; $i < sizeof($unique_mutamers); $i++) {
                            unset($unique_mutamers[$i]['safa_umrahgroups_passport_id']);
                            unset($unique_mutamers[$i]['country_name']);
                            unset($unique_mutamers[$i]['gender_name']);
                            $unique_mutamers[$i]['safa_umrahgroup_id'] = $copy_umrahgroup_id;
                            $unique_mutamers[$i]['display_order'] = $this->safa_umrahgroups_passports_model->calc_next_no_field($copy_umrahgroup_id, $i + 1);
                        }
                        $saved = 0;
                        foreach ($unique_mutamers as $a) {

                            if ($this->db->insert('safa_umrahgroups_passports', $a))
                                $saved++;
                        }
                        if ($saved >= 1) {
                            if (isset($exist_mutamers) && count($exist_mutamers) > 0) {
                                if (isset($safa_umrahgroup_id)) {
                                    $this->load->view('ea/umrahgroups_passports/redirect_message', array('msg' => lang('copy_umrahgroups_passports_some_mutamerds_exist_msg'),
                                        'url' => site_url('ea/umrahgroups_passports/passports_index/' . $safa_umrahgroup_id . ''),
                                        'model_title' => lang('umrahgroups_passports_title'), 'action' => lang('copy_to_umrahgroups_passports'),
                                        'exist_mutamers' => $exist_mutamers));
                                } else {
                                    $this->load->view('ea/umrahgroups_passports/redirect_message', array('msg' => lang('copy_umrahgroups_passports_some_mutamerds_exist_msg'),
                                        'url' => site_url('ea/umrahgroups_passports'),
                                        'model_title' => lang('umrahgroups_passports_title'), 'action' => lang('copy_to_umrahgroups_passports'),
                                        'exist_mutamers' => $exist_mutamers));
                                }
                            } else {
                                if (isset($safa_umrahgroup_id)) {
                                    $this->load->view('redirect_message', array('msg' => lang('global_added_message'),
                                        'url' => site_url('ea/umrahgroups_passports/passports_index/' . $safa_umrahgroup_id . ''),
                                        'model_title' => lang('umrahgroups_passports_title'), 'action' => lang('copy_to_umrahgroups_passports')));
                                } else {
                                    $this->load->view('redirect_message', array('msg' => lang('global_added_message'),
                                        'url' => site_url('ea/umrahgroups_passports'),
                                        'model_title' => lang('umrahgroups_passports_title'), 'action' => lang('copy_to_umrahgroups_passports')));
                                }
                            }
                        } else {
                            if (isset($safa_umrahgroup_id)) {
                                $this->load->view('redirect_message', array('msg' => lang('global_error_save'),
                                    'url' => site_url('ea/umrahgroups_passports/passports_index/' . $safa_umrahgroup_id . ''),
                                    'model_title' => lang('umrahgroups_passports_title'), 'action' => lang('copy_to_umrahgroups_passports')));
                            } else {
                                $this->load->view('redirect_message', array('msg' => lang('global_error_save'),
                                    'url' => site_url('ea/umrahgroups_passports'),
                                    'model_title' => lang('umrahgroups_passports_title'), 'action' => lang('copy_to_umrahgroups_passports')));
                            }
                        }
                    } else {
                        if (isset($safa_umrahgroup_id)) {
                            $this->load->view('redirect_message', array('msg' => lang('copy_cut_umrahgroups_passports_mutamerds_exist_msg'),
                                'url' => site_url('ea/umrahgroups_passports/passports_index/' . $safa_umrahgroup_id . ''),
                                'model_title' => lang('umrahgroups_passports_title'), 'action' => lang('copy_to_umrahgroups_passports')));
                        } else {
                            $this->load->view('redirect_message', array('msg' => lang('copy_cut_umrahgroups_passports_mutamerds_exist_msg'),
                                'url' => site_url('ea/umrahgroups_passports'),
                                'model_title' => lang('umrahgroups_passports_title'), 'action' => lang('copy_to_umrahgroups_passports')));
                        }
                    }
                } else {
                    if (isset($safa_umrahgroup_id))
                        redirect('ea/umrahgroups_passports/passports_index/' . $safa_umrahgroup_id . '');
                    else
                        redirect('ea/umrahgroups_passports/index');
                }
            }
        }
    }

    function export_mutamers_file() {
        $structure = array('MD_NO' => 'no',
            'MD_TITLE_ID' => 'title_id',
            'MD_FIRST_NAME_LA' => 'first_name_la',
            'MD_SECOND_NAME_LA' => 'second_name_la',
            'MD_THIRD_NAME_LA' => 'third_name_la',
            'MD_FOURTH_NAME_LA' => 'fourth_name_la',
            'MD_FIRST_NAME_AR' => 'first_name_ar',
            'MD_SECOND_NAME_AR' => 'second_name_ar',
            'MD_THIRD_NAME_AR' => 'third_name_ar',
            'MD_FOURTH_NAME_AR' => 'fourth_name_ar',
            'MD_NATIONALITY_ID' => 'nationality_id',
            'MD_PREVIOUS_NATIONALITY_ID' => 'previous_nationality_id',
            'MD_PASSPORT_NO' => 'passport_no',
            'MD_PASSPORT_TYPE' => 'passport_type_id',
            'MD_PASSPORT_ISSUE_DATE' => 'passport_issue_date',
            'MD_PASSPORT_EXPIRY_DATE' => 'passport_expiry_date',
            'MD_PASSPORT_ISSUING_CITY' => 'passport_issuing_city',
            'MD_PASSPORT_ISSUING_COUNTRY_ID' => 'passport_issuing_country_id',
            'MD_PASSPORT_DPN_COUNT' => 'passport_dpn_count',
            'MD_DPN_SERIAL_NO' => 'dpn_serial_no',
            'MD_GENDER_ID' => 'gender_id',
            'MD_MARITAL_STATUS_ID' => 'marital_status_id',
            'MD_DATE_OF_BIRTH' => 'date_of_birth',
            'MD_AGE' => 'age',
            'MD_BIRTH_CITY' => 'birth_city',
            'MD_BIRTH_COUNTRY_ID' => 'birth_country_id',
            'MD_RELATIVE_NO' => 'relative_no',
            'MD_RELATIVE_GENDER_ID' => 'relative_gender_id',
            'MD_RELATIVE_RELATION_ID' => 'relative_relation_id',
            'MD_EDUCATIONAL_LEVEL' => 'educational_level_id',
            'MD_OCCUPATION' => 'occupation',
            'MD_CITY' => 'city',
            'MD_COUNTRY_ID' => 'erp_country_id',
        );

        $mutamers2 = $this->input->post("mutamers");
        if ($this->input->post('safa_umrahgroup_id'))
            $safa_umrahgroup_id = $this->input->post('safa_umrahgroup_id');
        $file_type = $this->input->post("export_val");
        if (isset($mutamers2) && count($mutamers2) > 0 && is_array($mutamers2)) {
            foreach ($mutamers2 as $mutamer) {
                $this->safa_umrahgroups_passports_model->safa_umrahgroups_passport_id = $mutamer;
                $mutamers_info[] = $this->safa_umrahgroups_passports_model->get();
            }
            $input = $this->toArray($mutamers_info);
            $a = 0;
            foreach ($input as $inputvalue) {
                foreach ($structure as $key => $value) {
                    if (isset($inputvalue[$value])) {
                        $mutamers[$a][$key] = $inputvalue[$value];

                        //Added By Gouda, to get relation and relative NO.
                        //----------------------------------------------------------------------------------------
                        if ($value == 'relative_no') {
                            if (!empty($inputvalue[$value])) {
                                $this->safa_umrahgroups_passports_model->safa_umrahgroups_passport_id = false;
                                $this->safa_umrahgroups_passports_model->safa_group_passport_id = $inputvalue[$value];
                                $export_safa_umrahgroups_passports_rows = $this->safa_umrahgroups_passports_model->get();
                                $this->safa_umrahgroups_passports_model->safa_group_passport_id = false;
                                if (count($export_safa_umrahgroups_passports_rows) > 0) {
                                    $mutamers[$a][$key] = $export_safa_umrahgroups_passports_rows[0]->no;
                                }
                            } else {
                                $mutamers[$a][$key] = '';
                            }
                        }
                        //----------------------------------------------------------------------------------------
                    } else {
                        $mutamers[$a][$key] = '';
                    }
                }$a++;
            }

            $mutamers = (array('MUTAMERS' => $mutamers));
//        print_r($mutamers);
//                exit();
            $write_file = write_xml($mutamers, $file_type);
            $this->download($write_file, $file_type);
            exit();
        } else {
            if (isset($safa_umrahgroup_id))
                redirect('ea/umrahgroups_passports/passports_index/' . $safa_umrahgroup_id . '');
            else
                redirect('ea/umrahgroups_passports/index');
        }
    }

    function delete_old() {
        $this->layout = 'ajax';
        if (!$this->input->is_ajax_request()) {
            redirect('ea/group_passports/index');
        } else {
            $safa_umrahgroups_passport_id = $this->input->post('safa_umrahgroups_passport_id');
            $data_dpncount = $this->safa_umrahgroups_passports_model->check_delete_dpncount_ability($safa_umrahgroups_passport_id);
            if ($data_dpncount == 0) {
                $data = $this->safa_umrahgroups_passports_model->check_delete_mehrem_ability($safa_umrahgroups_passport_id);
                if ($data == 0) {
                    $dpn_serial_no = $this->safa_umrahgroups_passports_model->check_mutamer_dpn_serial_no($safa_umrahgroups_passport_id);
                    if ($dpn_serial_no == 1)
                        $this->safa_umrahgroups_passports_model->update_dpn_count($safa_umrahgroups_passport_id);
                    $mhrem_passport_no = $this->safa_umrahgroups_passports_model->get_his_mehrem_passport_no($safa_umrahgroups_passport_id);
                    $this->safa_umrahgroups_passports_model->safa_umrahgroups_passport_id = $safa_umrahgroups_passport_id;
                    if ($this->safa_umrahgroups_passports_model->delete())
                        if (isset($mhrem_passport_no))
                            echo json_encode(array('response' => TRUE, 'msg' => lang('global_delete_confirm'), 'passno' => lang('delete_his_mehrem_passport_no') . $mhrem_passport_no->passport_no));
                        else
                            echo json_encode(array('response' => TRUE, 'msg' => lang('global_delete_confirm')));
                }
                else {
                    $passports_no = $this->safa_umrahgroups_passports_model->get_mehrem_to_passports_no($safa_umrahgroups_passport_id);
                    if (count($passports_no) > 1) {
                        $msg = lang('not_delete_first_mehrem_to_passports_no');
                        foreach ($passports_no as $passport_no) {
                            $msg.= $passport_no->passport_no . ',';
                        }
                        echo json_encode(array('response' => FALSE, 'msg' => $msg));
                    } else {
                        $msg = lang('not_delete_first_mehrem_to_passports_no');
                        $msg.= $passports_no[0]->passport_no;
                        echo json_encode(array('response' => FALSE, 'msg' => $msg));
                    }
                }
            } else {
                echo json_encode(array('response' => FALSE, 'msg' => lang('not_delete_first_dpn_count_passports_no')));
            }
        }
    }

    function delete($safa_umrahgroup_id = 0, $safa_umrahgroups_passport_id = 0) {
        //By Gouda.	
        $data_dpncount = $this->safa_umrahgroups_passports_model->check_delete_dpncount_ability($safa_umrahgroups_passport_id);
        if ($data_dpncount == 0) {
            $dpn_serial_no = $this->safa_umrahgroups_passports_model->check_mutamer_dpn_serial_no($safa_umrahgroups_passport_id);
            if ($dpn_serial_no == 1)
                $this->safa_umrahgroups_passports_model->update_dpn_count($safa_umrahgroups_passport_id);

            $this->safa_umrahgroups_passports_model->safa_umrahgroups_passport_id = $safa_umrahgroups_passport_id;

            $this->safa_umrahgroups_passports_model->delete();

            $this->load->view('redirect_message', array('msg' => lang('global_deleted_message'),
                'url' => site_url('ea/umrahgroups_passports/passports_index/' . $safa_umrahgroup_id),
                'model_title' => lang('umrahgroups_passports_title'), 'action' => lang('global_delete')));
        } else {

            $this->load->view('confirmation_message', array('msg' => lang('this_passports_has_compains_are_you_sure_delete'),
                'back_url' => site_url('ea/umrahgroups_passports/passports_index/' . $safa_umrahgroup_id),
                'confirmation_url' => site_url('ea/umrahgroups_passports/delete_confirmation/' . $safa_umrahgroup_id . '/' . $safa_umrahgroups_passport_id),
                'model_title' => lang('umrahgroups_passports_title'), 'action' => lang('global_delete'),
            ));
        }
    }

    function delete_confirmation($safa_umrahgroup_id = 0, $safa_umrahgroups_passport_id = 0) {
        $this->safa_umrahgroups_passports_model->safa_umrahgroups_passport_id = $safa_umrahgroups_passport_id;
        $umrahgroups_passports_row = $this->safa_umrahgroups_passports_model->get();
        $safa_group_passport_id = 0;
        if (count($umrahgroups_passports_row) > 0) {
            $safa_group_passport_id = $umrahgroups_passports_row->safa_group_passport_id;
        }
        if ($this->safa_umrahgroups_passports_model->delete()) {
            $this->safa_umrahgroups_passports_model->delete_by_relative_no($safa_group_passport_id, $safa_umrahgroup_id);
        }

        $this->load->view('redirect_message', array('msg' => lang('global_deleted_message'),
            'url' => site_url('ea/umrahgroups_passports/passports_index/' . $safa_umrahgroup_id),
            'model_title' => lang('umrahgroups_passports_title'), 'action' => lang('global_delete')));
    }

    function delete_all() {
        $umrahgroup_passport_ids = $this->input->post('mutamers');
        if ($this->input->post('safa_umrahgroup_id'))
            $safa_umrahgroup_id = $this->input->post('safa_umrahgroup_id');
        if (isset($umrahgroup_passport_ids) && count($umrahgroup_passport_ids) > 0 && is_array($umrahgroup_passport_ids)) {
            foreach ($umrahgroup_passport_ids as $safa_umrahgroup_passport_id) {
                $data_dpncount = $this->safa_umrahgroups_passports_model->check_delete_dpncount_ability($safa_umrahgroup_passport_id);
                if ($data_dpncount == 0) {
                    $dpn_serial_no = $this->safa_umrahgroups_passports_model->check_mutamer_dpn_serial_no($safa_umrahgroup_passport_id);
                    if ($dpn_serial_no == 1)
                        $this->safa_umrahgroups_passports_model->update_dpn_count($safa_umrahgroup_passport_id);
                    $data = $this->safa_umrahgroups_passports_model->check_delete_mehrem_ability($safa_umrahgroup_passport_id);
                    if ($data == 0)
                        $unique_mutamers[] = $safa_umrahgroup_passport_id;
                    else
                        $has_mutamersWith[] = $safa_umrahgroup_passport_id;
                }
                else {
                    $has_mutamersWith[] = $safa_umrahgroup_passport_id;
                }
            }
            if (isset($unique_mutamers) && count($unique_mutamers) > 0) {
                $deleted = 0;
                foreach ($unique_mutamers as $safa_umrahgroup_passport_id) {
                    $this->safa_umrahgroups_passports_model->safa_umrahgroups_passport_id = $safa_umrahgroup_passport_id;
                    if ($this->safa_umrahgroups_passports_model->delete())
                        $deleted++;
                }
                $mutamers_info = array();
                if (isset($has_mutamersWith) && count($has_mutamersWith) > 0 && is_array($has_mutamersWith)) {
                    foreach ($has_mutamersWith as $has_mutamer) {
                        $data_dpncount = $this->safa_umrahgroups_passports_model->check_delete_dpncount_ability($has_mutamer);
                        if ($data_dpncount == 0) {
                            $this->safa_umrahgroups_passports_model->safa_umrahgroups_passport_id = $has_mutamer;
                            $this->safa_umrahgroups_passports_model->delete();
                        } else {
                            $this->safa_umrahgroups_passports_model->safa_umrahgroups_passport_id = $has_mutamer;
                            $mutamers_info[] = $this->safa_umrahgroups_passports_model->get();
                        }
                    }
                }
                if ($deleted >= 1) {
                    if (isset($mutamers_info) && count($mutamers_info) > 0 && is_array($mutamers_info)) {
                        if (isset($safa_umrahgroup_id)) {
                            $this->load->view('ea/umrahgroups_passports/delete_message', array('msg' => lang('delete_group_passports_some_mutamerds_hasMutamers_msg'),
                                'url' => site_url('ea/umrahgroups_passports/passports_index/' . $safa_umrahgroup_id . ''),
                                'model_title' => lang('umrahgroups_passports_title'), 'action' => lang('global_delete'),
                                'exist_mutamers' => $mutamers_info));
                        } else {
                            $this->load->view('ea/umrahgroups_passports/delete_message', array('msg' => lang('delete_group_passports_some_mutamerds_hasMutamers_msg'),
                                'url' => site_url('ea/umrahgroups_passports'),
                                'model_title' => lang('umrahgroups_passports_title'), 'action' => lang('global_delete'),
                                'exist_mutamers' => $mutamers_info));
                        }
                    } else {
                        if (isset($safa_umrahgroup_id)) {
                            $this->load->view('redirect_message', array('msg' => lang('global_deleted_message'),
                                'url' => site_url('ea/umrahgroups_passports/passports_index/' . $safa_umrahgroup_id . ''),
                                'model_title' => lang('umrahgroups_passports_title'), 'action' => lang('global_delete')));
                        } else {
                            $this->load->view('redirect_message', array('msg' => lang('global_deleted_message'),
                                'url' => site_url('ea/umrahgroups_passports'),
                                'model_title' => lang('umrahgroups_passports_title'), 'action' => lang('global_delete')));
                        }
                    }
                } else {
                    if (isset($safa_umrahgroup_id)) {
                        $this->load->view('redirect_message', array('msg' => lang('global_error_save'),
                            'url' => site_url('ea/umrahgroups_passports/passports_index/' . $safa_umrahgroup_id . ''),
                            'model_title' => lang('umrahgroups_passports_title'), 'action' => lang('global_delete')));
                    } else {
                        $this->load->view('redirect_message', array('msg' => lang('global_error_save'),
                            'url' => site_url('ea/umrahgroups_passports'),
                            'model_title' => lang('umrahgroups_passports_title'), 'action' => lang('global_delete')));
                    }
                }
            } else {
                if (isset($safa_umrahgroup_id)) {
                    $this->load->view('redirect_message', array('msg' => lang('not_delete_group_passports_mutamerds_msg'),
                        'url' => site_url('ea/umrahgroups_passports/passports_index/' . $safa_umrahgroup_id . ''),
                        'model_title' => lang('umrahgroups_passports_title'), 'action' => lang('global_delete')));
                } else {
                    $this->load->view('redirect_message', array('msg' => lang('not_delete_group_passports_mutamerds_msg'),
                        'url' => site_url('ea/umrahgroups_passports'),
                        'model_title' => lang('umrahgroups_passports_title'), 'action' => lang('global_delete')));
                }
            }
        } else {
            if (isset($safa_umrahgroup_id))
                redirect('ea/umrahgroups_passports/passports_index/' . $safa_umrahgroup_id . '');
            else
                redirect('ea/umrahgroups_passports/index');
        }
    }

    function search() {


        if ($this->input->get("safa_umrahgroup_id"))
            $this->safa_umrahgroups_passports_model->safa_umrahgroup_id = $this->input->get("safa_umrahgroup_id");

        if ($this->input->get("mutamer_name")) {

            /*
              $first_space = stripos($this->input->get("mutamer_name"), " ");
              $second_space = strrpos($this->input->get("mutamer_name"), " ");
              $first_name = substr($this->input->get("mutamer_name"), 0, $first_space);
              $third_name = substr($this->input->get("mutamer_name"), $second_space);
              if ($this->alpha_dash($first_name)) {
              $name_la = $this->input->get("mutamer_name");
              } else {
              $name_ar = $this->input->get("mutamer_name");
              }
              if (!empty($name_la) || isset($name_la)) {
              $first_space = stripos($name_la, " ");
              $second_space = strrpos($name_la, " ");
              $first_name_la = substr($name_la, 0, $first_space);
              //echo $second_name_la=substr($name_la,$first_space+1,3);
              $third_name_la = substr($name_la, $second_space);
              if (!empty($first_name_la) || isset($first_name_la))
              $this->safa_umrahgroups_passports_model->first_name_la = $first_name_la;
              //if (!empty($second_name_la) || isset($second_name_la))
              //$this->safa_groups_model->second_name_la = $second_name_la;
              if (!empty($third_name_la) || isset($third_name_la))
              $this->safa_umrahgroups_passports_model->third_name_la = $third_name_la;
              }
              elseif (!empty($name_ar) || isset($name_ar)) {

              $first_space = stripos($name_ar, " ");
              $second_space = strrpos($name_ar, " ");
              $first_name_ar = substr($name_ar, 0, $first_space);

              //echo $second_name_ar=substr($name_la,$first_space+1,3);
              $third_name_ar = substr($name_ar, $second_space);
              if (!empty($first_name_ar) || isset($first_name_ar))
              $this->safa_umrahgroups_passports_model->first_name_ar = $first_name_ar;
              //if (!empty($second_name_ar) || isset($second_name_ar))
              //$this->safa_groups_model->second_name_ar = $second_name_ar;
              if (!empty($third_name_ar) || isset($third_name_ar))
              $this->safa_umrahgroups_passports_model->third_name_ar = $third_name_ar;
              }
             */

            $this->safa_umrahgroups_passports_model->db->where("(CONCAT( first_name_ar, ' ', second_name_ar, ' ', third_name_ar, ' ', fourth_name_ar ) LIKE '%" . $this->input->get("mutamer_name") . "%'");
            $this->safa_umrahgroups_passports_model->db->or_where("CONCAT( first_name_la,  ' ', second_name_la,  ' ', third_name_la,  ' ', fourth_name_la ) LIKE '%" . $this->input->get("mutamer_name") . "%')");
        }

        if ($this->input->get("passport_no"))
            $this->safa_umrahgroups_passports_model->passport_no = $this->input->get("passport_no");

        if ($this->input->get("nationality_id"))
            $this->safa_umrahgroups_passports_model->nationality_id = $this->input->get("nationality_id");

        if ($this->input->get("age"))
            $this->safa_umrahgroups_passports_model->age = $this->input->get("age");

        if ($this->input->get("relative_no"))
            $this->safa_umrahgroups_passports_model->relative_no = $this->input->get("relative_no");
    }

    function get_passport_no($passport_no) {
        $this->layout = 'ajax';
        if (!empty($passport_no) || isset($passport_no)) {
            $this->db->select('passport_no');
            $this->db->from('safa_umrahgroups_passports');
            $this->db->like('passport_no', $passport_no);
            echo json_encode($this->db->get()->result());
        }
    }

    function mutamer_name() {
        $this->layout = 'ajax';
        $mutamer_name = $this->input->post('searched');
        if ($this->alpha_dash($mutamer_name))
            $name_la = $mutamer_name;
        else
            $name_ar = $mutamer_name;

        if (!empty($name_la) || isset($name_la)) {
            $mutamer_upper = strtoupper($name_la);
            $mutamer_lower = strtolower($name_la);
            $this->db->select('first_name_la,second_name_la,third_name_la,fourth_name_la');
//            $this->db->like('first_name_la', $mutamer_upper);
//            $this->db->like('first_name_la', $mutamer_lower);
            $this->db->like('first_name_la', $name_la);
            echo json_encode($this->db->get('safa_umrahgroups_passports')->result());
        } elseif (!empty($name_ar) || isset($name_ar)) {
            $this->db->select('first_name_ar,second_name_ar,third_name_ar,fourth_name_ar');
            $this->db->like('first_name_ar', $name_ar);
            echo json_encode($this->db->get('safa_umrahgroups_passports')->result());
        }
    }

    function alpha_dash($str) {
        if (preg_match("/^([-a-z0-9_-]*$)+$/i", $str)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function toArray($obj) {
        if (is_object($obj))
            $obj = (array) $obj;
        if (is_array($obj)) {
            $new = array();
            foreach ($obj as $key => $val) {
                $new[$key] = $this->toArray($val);
            }
        } else {
            $new = $obj;
        }
        return $new;
    }

    function copy_cut_check($safa_umrahgroup_id, $passport_no) {
        $this->db->where('safa_umrahgroup_id', $safa_umrahgroup_id);
        $this->db->where('passport_no', $passport_no);
        if ($this->db->get('safa_umrahgroups_passports')->num_rows()) {
            return FALSE;
        } else {
            return TRUE;
        }
    }

    function download($data, $file_type) {

        if ($file_type == 'gama') {
            force_download('جاما.XML', $data);
        } elseif ($file_type == 'babUmrah') {
            force_download('باب العمرة.XML', $data);
        } elseif ($file_type == 'wayToUmrah') {
            force_download('طريق  العمرة.txt', $data);
        } elseif ($file_type == 'FileSafa') {

            $this->load->library('zip');
            $this->zip->add_data('group.txt', $data);

            // Write the zip file to a folder on your server. Name it "my_backup.zip"
            $filename = 'file.safa';
            $this->zip->archive($filename);

            // Download the file to your desktop. Name it "my_backup.zip"
            //$this->zip->download(substr_replace('file.safa.zip' , 'zip', strrpos('file.safa.zip' , '.') +1)); 

            $get_zip = $this->zip->get_zip();
            $zip_content = & $get_zip;
            force_download($filename, $zip_content);


            //force_download('file.safa', $data);
        }
    }

    function check_relative_no($umrahgroup_id) {
        $mutamers = unserialize($this->input->post('mutamers'));
        $relative_nos = array();
        $flag = 0;
        foreach ($mutamers as $mutamer) {
            $this->db->select('safa_umrahgroups_passports.no,safa_umrahgroups_passports.relative_no,safa_umrahgroups_passports.safa_umrahgroup_id,first_name_ar,second_name_ar,
               safa_umrahgroups_passports.first_name_la,safa_umrahgroups_passports.second_name_la');
            $this->db->where('safa_umrahgroups_passports.safa_umrahgroups_passport_id', $mutamer);
            $row = $this->db->get('safa_umrahgroups_passports')->row();
            if (!empty($row->no) && $row->relative_no == 0) {

                $this->db->select('safa_umrahgroups_passports.safa_umrahgroups_passport_id,');
                $this->db->where('safa_umrahgroups_passports.safa_umrahgroup_id', $row->safa_umrahgroup_id);
                $this->db->where('safa_umrahgroups_passports.relative_no', $row->no);
                $this->db->where('safa_umrahgroups_passports.relative_no!=', 0);
                $passport_ids = $this->db->get('safa_umrahgroups_passports')->result();
                foreach ($passport_ids as $passport_id) {
                    if (!in_array($passport_id->safa_umrahgroups_passport_id, $mutamers)) {
                        ++$flag;
                        if (name() == 'name_ar')
                            $relative_nos[] = $row->first_name_ar . ' ' . $row->second_name_ar;
                        else
                            $relative_nos[] = $row->first_name_la . ' ' . $row->second_name_la;
                        break;
                    }
                }
            }
            elseif ($row->relative_no != 0) {
                $this->db->select('safa_umrahgroups_passports.safa_umrahgroups_passport_id,');
                $this->db->where('safa_umrahgroups_passports.safa_umrahgroup_id', $row->safa_umrahgroup_id);
                $this->db->where('safa_umrahgroups_passports.no', $row->relative_no);
                $passport_id = $this->db->get('safa_umrahgroups_passports')->row();
                if (!in_array($passport_id->safa_umrahgroups_passport_id, $mutamers)) {
                    ++$flag;
                    if (name() == 'name_ar')
                        $nos[] = $row->first_name_ar . ' ' . $row->second_name_ar;
                    else
                        $nos[] = $row->first_name_la . ' ' . $row->second_name_la;
                }
            }
        }
        if ($flag > 0) {
            $msg = '';
            if (isset($relative_nos) && count($relative_nos) == 1) {
                $msg = lang('not_cut') . $relative_nos[0] . lang('without_relative_no');
            } elseif (isset($relative_nos) && count($relative_nos) > 1) {
                $msg = lang('not_cut');
                foreach (isset($relative_nos) && $relative_nos as $relative) {
                    $msg.=$relative . lang('and');
                }
                $msg.=lang('without_relatives_no');
            }
            /////////////////
            if (isset($nos) && count($nos) == 1) {
                $msg = lang('not_cut') . $nos[0] . lang('without_no');
            } elseif (isset($nos) && count($nos) > 1) {
                $msg = lang('not_cut');
                foreach ($nos as $no) {
                    $msg.=$no . lang('and');
                }
                $msg.=lang('without_nos');
            }
            $this->form_validation->set_message('check_relative_no', $msg);
            return FALSE;
        } else {
            return TRUE;
        }
    }

    function check_mutamers_dpncount($group_id) {
        $mutamers = unserialize($this->input->post('mutamers'));
        $relative_nos = array();
        $flag = 0;
        foreach ($mutamers as $mutamer) {
            $this->db->select('safa_umrahgroups_passports.passport_no,safa_umrahgroups_passports.passport_dpn_count,safa_umrahgroups_passports.safa_umrahgroup_id,safa_umrahgroups_passports.dpn_serial_no,
              safa_umrahgroups_passports.first_name_ar,safa_umrahgroups_passports.second_name_ar,safa_umrahgroups_passports.first_name_la,safa_umrahgroups_passports.second_name_la');
            $this->db->where('safa_umrahgroups_passports.safa_umrahgroups_passport_id', $mutamer);
            $query = $this->db->get('safa_umrahgroups_passports')->row();
            if ($query->passport_dpn_count > 0) {

                $this->db->select('safa_umrahgroups_passports.safa_umrahgroups_passport_id,safa_umrahgroups_passports.passport_no,safa_umrahgroups_passports.passport_dpn_count,safa_umrahgroups_passports.dpn_serial_no');
                $this->db->where('safa_umrahgroups_passports.safa_umrahgroup_id', $query->safa_umrahgroup_id);
                $this->db->where('safa_umrahgroups_passports.passport_no', $query->passport_no);
                $dpn_counts = $this->db->get('safa_umrahgroups_passports')->result();
//               print_r($dpn_counts);exit();
                foreach ($dpn_counts as $dpn_count) {
                    if (!in_array($dpn_count->safa_umrahgroups_passport_id, $mutamers)) {
                        ++$flag;
                        if (name() == 'name_ar')
                            $counts[] = $query->first_name_ar . ' ' . $query->second_name_ar;
                        else
                            $counts[] = $query->first_name_la . ' ' . $query->second_name_la;
                        break;
                    }
                }
            }elseif ($query->passport_dpn_count == 0 && $query->dpn_serial_no > 0) {
                $this->db->select('safa_umrahgroups_passports.safa_umrahgroups_passport_id,safa_umrahgroups_passports.passport_no,safa_umrahgroups_passports.passport_dpn_count,safa_umrahgroups_passports.dpn_serial_no');
                $this->db->where('safa_umrahgroups_passports.safa_umrahgroup_id', $query->safa_umrahgroup_id);
                $this->db->where('safa_umrahgroups_passports.passport_no', $query->passport_no);
                $this->db->where('safa_umrahgroups_passports.dpn_serial_no', 0);
                $dpn_counts = $this->db->get('safa_umrahgroups_passports')->row();
                if (!in_array($dpn_counts->safa_umrahgroups_passport_id, $mutamers)) {
                    ++$flag;
                    if (name() == 'name_ar')
                        $traveling_with[] = $query->first_name_ar . ' ' . $query->second_name_ar;
                    else
                        $traveling_with[] = $query->first_name_la . ' ' . $query->second_name_la;
                }
            }
        }
//        print_r($relative_nos);
//                exit();
        if ($flag > 0) {
            $msg = '';
            if (isset($counts) && count($counts) == 1) {
                $msg = lang('not_cut') . $counts[0] . ('without_traveling_with');
            } elseif (isset($counts) && count($counts) > 1) {
                $msg = lang('not_cut');
                foreach ($counts as $count) {
                    $msg.=$count . lang('and');
                }
                $msg.=('without_travelings_with');
            }
            ////////
            if (isset($traveling_with) && count($traveling_with) == 1) {
                $msg = lang('not_cut_traveling_with') . $traveling_with[0] . lang('without_passport_no');
            } elseif (isset($traveling_with) && count($traveling_with) > 1) {
                $msg = lang('not_cut_travelings_with');
                foreach ($traveling_with as $traveling) {
                    $msg.=$traveling . lang('and');
                }
                $msg.=lang('without_passports_no');
            }
            $this->form_validation->set_message('check_mutamers_dpncount', $msg);
            return FALSE;
        } else {
            return TRUE;
        }
    }

    function sortable($umrahgroup_id) {
        $this->layout = 'ajax';
        $data = $this->safa_umrahgroups_passports_model->getDisplay_order($umrahgroup_id);
        foreach ($_POST as $key => $value) {
            $newRank = $value;
            $key = substr($key, 4);
            $currentRank = $data[$key];
            if ($currentRank != $newRank) {
                $this->safa_umrahgroups_passports_model->switchOrder($key, $newRank);
            }
        }
    }

    function setRowsCountCookie() {
        $ea_user_id = session('ea_user_id');
        $rows_count_per_page = $this->input->post('rows_count_per_page');
        setcookie($ea_user_id . '_umrahgroups_passports_rows_count_per_page', $rows_count_per_page, time() + 60 * 60 * 24 * 365);
    }

}

/* End of file port_halls.php */
    /* Location: ./application/controllers/admin/port_halls.php */




    