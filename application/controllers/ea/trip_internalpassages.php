<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Trip_internalpassages extends Safa_Controller {

    public $module = "trip_internaltrip";

    public function __construct() {
        parent::__construct();
        $this->load->model('internalpassages_model');
        $this->load->model('trip_internaltrip_model');
        $this->lang->load('trip_internaltrip');
        $this->lang->load('internalpassages');
        $this->load->helper('form_helper');
        permission();
        if ($this->destination == 'ea')
            $this->trip_internaltrip_model->ea_id = session('ea_id');
        elseif ($this->destination == 'uo')
            $this->trip_internaltrip_model->uo_id = session('uo_id');
    }

    public function add($internaltrip_id = FALSE) {
        $this->layout = 'js'; // to load only javascript/css in the header
        $this->trip_internaltrip_model->custom_select = 'safa_trip_id';
        $this->trip_internaltrip_model->safa_trip_internaltrip_id = $internaltrip_id;
        $intrenaltrip = $this->trip_internaltrip_model->search();
        $trip_id = $intrenaltrip->safa_trip_id;
        /*         * ***num of segments********* */
        $this->internalpassages_model->safa_trip_internaltrip_id = $internaltrip_id;
        $data['num_of_segments'] = $this->internalpassages_model->get(true);
        /*         * *********** */
        $data["erp_hotels"] = $this->get_ea_hotels($trip_id);
        $data["safa_tourismplaces"] = $this->get_tourism_places($trip_id);
        $data["erp_ports"] = $data["erp_ports"] = $this->get_erp_ports();
        $data["internaltrip_id"] = $internaltrip_id;
        $data["safa_internalpassagestatus"] = ddgen("safa_internalsegmentstatus", array("safa_internalsegmentstatus_id", name()));
        $data["safa_internalpassagetypes"] = ddgen("safa_internalsegmenttypes", array("safa_internalsegmenttype_id", name()));
        $this->load->library("form_validation");
        //********* form_validations **/
        $this->form_validation->set_rules('internalpassage_type', 'lang:internalpassage_type', 'trim|required');
        $this->form_validation->set_rules('startdatetime', 'lang:internalpassage_startdatatime', 'trim|required|callback_checkdate');
        //$this->form_validation->set_rules('enddatetime','lang:internalpassage_enddatatime','trim|callback_comparetime');
        $this->form_validation->set_rules('seatscount', 'lang:internalpassage_seatscount', 'trim|required|is_natural_no_zero');

        if ($this->input->post('internalpassage_type') == 1) {
            $this->form_validation->set_rules('internalpassage_port_start', 'lang:internalpassage_starthotel', 'trim|required');
            $this->form_validation->set_rules('internalpassage_hotel_end', 'lang:internalpassage_endhotel', 'trim|required');
        }
        if ($this->input->post('internalpassage_type') == 2) {
            $this->form_validation->set_rules('internalpassage_port_end', 'lang:internalpassage_starthotel', 'trim|required');
            $this->form_validation->set_rules('internalpassage_hotel_start', 'lang:internalpassage_endhotel', 'trim|required');
        }
        if ($this->input->post('internalpassage_type') == 3) {
            $this->form_validation->set_rules('internalpassage_hotel_start', 'lang:internalpassage_starthotel', 'trim|required');
            $this->form_validation->set_rules('internalpassages_torismplace_end', 'lang:internalpassage_endhotel', 'trim|required');
        }
        if ($this->input->post('internalpassage_type') == 4) {
            $this->form_validation->set_rules('internalpassage_hotel_start', 'lang:internalpassage_starthotel', 'trim|required');
            $this->form_validation->set_rules('internalpassage_hotel_end', 'lang:internalpassage_endhotel', 'trim|required');
        }
        if ($this->form_validation->run() == false) {
            $this->load->view("ea/internalpassages/add", $data);
        } else {
            $this->internalpassages_model->safa_internalsegmenttype_id = $this->input->post('internalpassage_type');
            $this->internalpassages_model->start_datetime = $this->input->post('startdatetime');
            $enddatetime = date_create($this->input->post('startdatetime'));
            $this->internalpassages_model->end_datetime = date_format($enddatetime, "Y-m-d");
            $this->internalpassages_model->seats_count = $this->input->post('seatscount');
            $this->internalpassages_model->notes = $this->input->post('internalpassage_notes');
            $this->internalpassages_model->safa_internalsegmentestatus_id = 1;
            $intrenal_trip_id = $this->input->post('internaltrip_id');
            $this->internalpassages_model->safa_trip_internaltrip_id = $intrenal_trip_id;


            if ($this->input->post('internalpassage_type') == 1) {
                $this->internalpassages_model->erp_port_id = $this->input->post('internalpassage_port_start');
                $this->internalpassages_model->erp_end_hotel_id = $this->input->post('internalpassage_hotel_end');
            }
            if ($this->input->post('internalpassage_type') == 2) {
                $this->internalpassages_model->erp_port_id = $this->input->post('internalpassage_port_end');
                $this->internalpassages_model->erp_start_hotel_id = $this->input->post('internalpassage_hotel_start');
            }
            if ($this->input->post('internalpassage_type') == 3) {
                $this->internalpassages_model->erp_start_hotel_id = $this->input->post('internalpassage_hotel_start');
                $this->internalpassages_model->safa_tourism_place_id = $this->input->post('internalpassages_torismplace_end');
            }
            if ($this->input->post('internalpassage_type') == 4) {
                $this->internalpassages_model->erp_start_hotel_id = $this->input->post('internalpassage_hotel_start');
                $this->internalpassages_model->erp_end_hotel_id = $this->input->post('internalpassage_hotel_end');
            }
            $this->internalpassages_model->save();

            /*             * ** impelementing the confirmation */
            $this->load->view('redirect_message_popup', array('msg' => lang('global_added_message'),
                'action' => lang('add_internalpassages'),
                'url' => site_url('ea/trip_internalpassages/add/' . $internaltrip_id))
            );
        }
    }

    public function edit($id = FALSE) {
        $this->layout = 'js';  // to load only javascript/css in the header
        if (!$id)
            show_404();
        $this->internalpassages_model->safa_internalsegment_id = $id;
        $data['item'] = $this->internalpassages_model->get();
        /**         * *get trip_id*********** */
        $this->trip_internaltrip_model->custom_select = 'safa_trip_id';
        $this->trip_internaltrip_model->safa_trip_internaltrip_id = $data['item']->safa_trip_internaltrip_id;
        $intrenaltrip = $this->trip_internaltrip_model->search();
        $trip_id = $intrenaltrip->safa_trip_id;
        /**         * **num of segments********* */
        if (!$data['item'])
            show_404();
        $data["erp_hotels"] = $this->get_ea_hotels($trip_id);
        $data["safa_tourismplaces"] = $this->get_tourism_places($trip_id);
        $data["erp_ports"] = $data["erp_ports"] = $this->get_erp_ports();
        $data["safa_internalpassagestatus"] = ddgen("safa_internalsegmentstatus", array("safa_internalsegmentstatus_id", name()));
        $data["safa_internalpassagetypes"] = ddgen("safa_internalsegmenttypes", array("safa_internalsegmenttype_id", name()));
        $this->load->library("form_validation");
        //********* form_validations **/
        $this->form_validation->set_rules('internalpassage_type', 'lang:internalpassage_type', 'trim|required');
        $this->form_validation->set_rules('startdatetime', 'lang:internalpassage_startdatatime', 'trim|required|callback_checkdate');
        $this->form_validation->set_rules('seatscount', 'lang:internalpassage_seatscount', 'trim|required|is_natural_no_zero');

        if ($this->input->post('internalpassage_type') == 1) {
            $this->form_validation->set_rules('internalpassage_port_start', 'lang:internalpassage_starthotel', 'trim|required');
            $this->form_validation->set_rules('internalpassage_hotel_end', 'lang:internalpassage_endhotel', 'trim|required');
        }
        if ($this->input->post('internalpassage_type') == 2) {
            $this->form_validation->set_rules('internalpassage_port_end', 'lang:internalpassage_starthotel', 'trim|required');
            $this->form_validation->set_rules('internalpassage_hotel_start', 'lang:internalpassage_endhotel', 'trim|required');
        }
        if ($this->input->post('internalpassage_type') == 3) {
            $this->form_validation->set_rules('internalpassage_hotel_start', 'lang:internalpassage_starthotel', 'trim|required');
            $this->form_validation->set_rules('internalpassages_torismplace_end', 'lang:internalpassage_endhotel', 'trim|required');
        }
        if ($this->input->post('internalpassage_type') == 4) {
            $this->form_validation->set_rules('internalpassage_hotel_start', 'lang:internalpassage_starthotel', 'trim|required');
            $this->form_validation->set_rules('internalpassage_hotel_end', 'lang:internalpassage_endhotel', 'trim|required');
        }
        /*         * ************ */
        if ($this->form_validation->run() == false) {
            $this->load->view("ea/internalpassages/edit", $data);
        } else {
            $this->internalpassages_model->safa_internalsegment_id = $id;
            $this->internalpassages_model->seats_count = $this->input->post('seatscount');
            $this->internalpassages_model->notes = $this->input->post('internalpassage_notes');
            $this->internalpassages_model->start_datetime = $this->input->post('startdatetime');
            $enddatetime = date_create($this->input->post('startdatetime'));
            $this->internalpassages_model->end_datetime = date_format($enddatetime, "Y-m-d");
            $this->internalpassages_model->safa_internalsegmenttype_id = $this->input->post('internalpassage_type');
            $this->internalpassages_model->safa_internalsegmentestatus_id = 1;

            if ($this->input->post('internalpassage_type') == 1) {
                $this->internalpassages_model->erp_port_id = $this->input->post('internalpassage_port_start');
                $this->internalpassages_model->erp_end_hotel_id = $this->input->post('internalpassage_hotel_end');
                //------------------------------------for update the type of segment----------------------------------------------------------------//
                $this->internalpassages_model->erp_start_hotel_id = NULL;
                $this->internalpassages_model->safa_tourism_place_id = NULL;
            }
            if ($this->input->post('internalpassage_type') == 2) {
                $this->internalpassages_model->erp_port_id = $this->input->post('internalpassage_port_end');
                $this->internalpassages_model->erp_start_hotel_id = $this->input->post('internalpassage_hotel_start');
                //------------------------------for update the type of segment-----------------------------------------------------------------------------//   
                $this->internalpassages_model->safa_tourism_place_id = NULL;
                $this->internalpassages_model->erp_end_hotel_id = NULL;
            }
            if ($this->input->post('internalpassage_type') == 3) {
                $this->internalpassages_model->erp_start_hotel_id = $this->input->post('internalpassage_hotel_start');
                $this->internalpassages_model->safa_tourism_place_id = $this->input->post('internalpassages_torismplace_end');
                //------------------------------for update the type of segment-----------------------------------------------------------------------------//   
                $this->internalpassages_model->erp_port_id = NULL;
                $this->internalpassages_model->erp_end_hotel_id = NULL;
            }
            if ($this->input->post('internalpassage_type') == 4) {
                $this->internalpassages_model->erp_start_hotel_id = $this->input->post('internalpassage_hotel_start');
                $this->internalpassages_model->erp_end_hotel_id = $this->input->post('internalpassage_hotel_end');
                $this->internalpassages_model->erp_port_id = NULL;
                $this->internalpassages_model->safa_tourism_place_id = NULL;
                //------------------------------for update the type of segment-------------------------------------------------------------------------//
            }

            $internalpassages = $this->internalpassages_model->save();
            $this->load->view('redirect_message_popup', array('msg' => lang('global_updated_message'),
                'id' => $id,
                'action' => lang('edit_internalpassages'),
                'url' => site_url('ea/trip_internalpassages/edit/' . $id))
            );
        }
    }

    function delete($id = FALSE, $internaltrip_id = FALSE) {

        if (!$id)
            show_404();
        $this->internalpassages_model->safa_internalsegment_id = $id;
        if (!$this->internalpassages_model->delete())
            show_404();
        redirect("ea/trip_internaltrip/edit/" . $internaltrip_id);
    }

    function delete_all() {
        if ($this->input->post("delete_items")) {
            foreach ($this->input->post("delete_items") as $item) {
                $this->internalpassages_model->safa_trip_internaltrip_id = $item;

                $this->internalpassages_model->delete();
            }
        } else
            show_404();
        redirect("ea/trip_internaltrip/index");
    }

    function search() {
        if ($this->input->get("safa_trip_internaltrip_id"))
            $this->internalpassages_model->safa_trip_internaltrip_id = $this->input->get("safa_trip_internaltrip_id");
        if ($this->input->get("safa_trip_id"))
            $this->internalpassages_model->safa_trip_id = $this->input->get("safa_trip_id");
        if ($this->input->get("safa_ito_id"))
            $this->internalpassages_model->safa_ito_id = $this->input->get("safa_ito_id");
        if ($this->input->get("safa_internaltripstatus_id"))
            $this->internalpassages_model->safa_internaltripstatus_id = $this->input->get("safa_internaltripstatus_id");
        if ($this->input->get("operator_reference"))
            $this->internalpassages_model->operator_reference = $this->input->get("operator_reference");
        if ($this->input->get("attachement"))
            $this->internalpassages_model->attachement = $this->input->get("attachement");
        if ($this->input->get("datetime"))
            $this->internalpassages_model->datetime = $this->input->get("datetime");
    }

    function get_ea_hotels($trip_id) {
        $result = $this->trip_internaltrip_model->get_trip_hotels($trip_id);
        $hotels = array();
        $name = name();
        $hotels[""] = lang('global_select_from_menu');
        if (isset($result) && count($result) > 0) {
            foreach ($result as $record) {
                $hotels[$record->erp_hotel_id] = $record->$name;
            }
        }
        return $hotels;
    }

    function get_tourism_places($trip_id) {

        $result = $this->trip_internaltrip_model->get_trip_tourismplace($trip_id);
        $name = name();
        $tourism_place[""] = lang('global_select_from_menu');
        if (isset($result) && count($result) > 0) {
            foreach ($result as $record) {
                $tourism_place[$record->safa_tourismplace_id] = $record->$name;
            }
        }
        return $tourism_place;
    }

    function comparetime() {

        $this->form_validation->set_message('comparetime', lang('compare_start_end_time'));
        $start_date = $this->input->post('startdatetime');
        $end_date = $this->input->post('enddatetime');
        if ($start_date && $end_date) {
            $time_diff = strtotime($end_date) - strtotime($start_date);
        }
        if (isset($time_diff) && $time_diff <= 0) {
            return FALSE;
        } else {
            return true;
        }
    }

    function checkdate($value) {
        $this->form_validation->set_message('checkdate', lang('compare_start_end_time'));
        if (date_create($value)) {
            $valid_date = date_create(date('Y-m-d h:i', strtotime("-2 years")));
            $start_date = date_create($value);
            if ($start_date >= $valid_date)
                return true;
            else
                return false;
        }
        else {
            return false;
        }
    }

    function get_erp_ports() {
        $ports = array();
        $name = name();
        $this->db->select("erp_port_id," . $name);
        $this->db->where('code in', "('JED','MED','YNB') ", false);
        $result = $this->db->get('erp_ports')->result();
        foreach ($result as $port) {
            $ports[$port->erp_port_id] = $port->$name;
        }
        return $ports;
    }

}

/* End of file trip_internaltrip.php */
/* Location: ./application/controllers/trip_internaltrip.php */