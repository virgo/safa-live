<?php

class Ea_Ports extends Safa_Controller {

    public $module = "ea_ports";

    public function __construct() {
        parent::__construct();
        $this->layout = 'new';
        //Side menu session, By Gouda.
        session('side_menu_id', 0);
        $this->load->model('ports_model');
        $this->load->model('ea_ports_model');
        $this->lang->load('ea_ports');
        permission();
    }

    public function index() {

        if (isset($_GET['search']))
            $this->search();
        $this->ports_model->join = TRUE;
        $data["total_rows"] = $this->ports_model->get(true);
        $this->ports_model->offset = $this->uri->segment("4");
        $this->ports_model->limit = $this->config->item('per_page');
        $data["items"] = $this->ports_model->get();
        $config['uri_segment'] = 4;
        $config['base_url'] = site_url('ea/ea_ports/index');
        $config['total_rows'] = $data["total_rows"];
        $config['per_page'] = $this->ports_model->limit;
        $config['suffix'] = $this->input->get() ? '?' . http_build_query($_GET) : NULL;
        $this->load->library('pagination');
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();
        $this->load->view('ea/ea_ports/index', $data);
    }

    function add_port() {

        $this->layout = 'ajax';

        if (!$this->input->is_ajax_request()) {
            redirect('ea/ea_ports/index/');
        } else {

            $erp_port_id = $this->input->post('erp_port_id');
            $safa_ea_id = $this->input->post('safa_ea_id');

//       $this->ea_ports_model->erp_port_id =$erp_port_id;
//       $this->ea_ports_model->safa_ea_id =$safa_ea_id;

            if ($this->db->insert('safa_ea_ports', array(
                        'erp_port_id' => $erp_port_id,
                        'safa_ea_id' => $safa_ea_id
                    ))) {

                echo json_encode(array('response' => TRUE, 'msg' => 'added'));
            } else {

                echo json_encode(array('response' => FALSE, 'msg' => 'not added'));
            }
        }
    }

    function delete_port() {

        $this->layout = 'ajax';

        if (!$this->input->is_ajax_request()) {
            redirect('ea/ea_ports/index/');
        } else {

            $erp_port_id = $this->input->post('erp_port_id');
            $safa_ea_id = $this->input->post('safa_ea_id');

            $this->ea_ports_model->erp_port_id = $erp_port_id;
            $this->ea_ports_model->safa_ea_id = $safa_ea_id;

            if ($this->ea_ports_model->delete()) {
                echo json_encode(array('response' => TRUE, 'msg' => 'deleted',
                ));
            } else {
                echo json_encode(array('response' => FALSE, 'msg' => 'not deleted'));
            }
        }
    }

    function delete() {

        $this->layout = 'ajax';

        if (!$this->input->is_ajax_request()) {
            redirect('ea/ea_ports/index');
        } else {
            $safa_ea_port_id = $this->input->post('safa_ea_port_id');
            $this->ea_ports_model->safa_ea_port_id = $safa_ea_port_id;
            if ($this->ea_ports_model->delete())
                echo json_encode(array('response' => TRUE, 'msg' => 'deleted'));
            else {
                echo json_encode(array('response' => FALSE, 'msg' => 'not deleted'));
            }
        }
    }

    function search() {
        if ($this->input->get("erp_country_id"))
            $this->ports_model->erp_country_id = $this->input->get("erp_country_id");
    }

}

/* End of file ea_ports.php */
/* Location: ./application/controllers/admin/port_halls.php */


