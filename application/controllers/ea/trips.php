<?php

class Trips extends Safa_Controller {

	function __construct() {
		parent::__construct();

		$this->layout = 'new';
		//Side menu session, By Gouda.
		session('side_menu_id', 1);


		$this->load->model('safa_trips_model');
		$this->load->model('flight_availabilities_model');
		$this->load->model('trip_supervisors_model');
		$this->load->model('safa_trips_requests_model');
		$this->load->model('safa_group_passports_model');
		$this->load->model('notification_model');
		$this->load->model('ports_model');

		$this->load->model('eas_model');


		$this->lang->load('trips');
		$this->lang->load('flight_availabilities');

		$this->lang->load('group_passports');
		$this->lang->load('trip_details');

		$this->load->helper('form_helper');

		permission();
		if (session('ea_id')) {
			$this->safa_trips_model->safa_ea_id = session('ea_id');
		}
	}

	function index() {
		if (isset($_GET['search'])) {
			$this->search();
		}

		$data["safa_trips"] = ddgen("safa_trips", array("safa_trip_id", 'name'), array('safa_ea_id'=>session('ea_id')));

		$data["safa_tripstatus"] = ddgen("safa_tripstatus", array("safa_tripstatus_id", name()));
		$data["safa_trip_confirm"] = ddgen("safa_trip_confirm", array("safa_trip_confirm_id", name()));
		$data["erp_transportertypes"] = ddgen("erp_transportertypes", array("erp_transportertype_id", name()));
		$data['supervisors'] = $this->trip_supervisors_model->get_ea_supervisor();


		$data["total_rows"] = $this->safa_trips_model->get(true);

		//		$this->safa_trips_model->offset = $this->uri->segment("4");
		$this->safa_trips_model->limit = 1000;
		//$this->safa_trips_model->order_by = array('safa_trip_id');
		$data["items"] = $this->safa_trips_model->get();
		//		$config['uri_segment'] = 4;
		//		$config['base_url'] = site_url('ea/trips/index');
		//		$config['total_rows'] = $data["total_rows"];

		//		$config['per_page'] = $this->config->item('per_page');
		//		$config['suffix'] = $this->input->get() ? '?' . http_build_query($_GET) : NULL;
		//		$this->load->library('pagination');
		//		$this->pagination->initialize($config);
		$data['pagination'] = '';//$this->pagination->create_links();
		$this->load->view('ea/trips/index', $data);
	}

	public function search()
	{
		if ($this->input->get("safa_trip_id"))
		$this->safa_trips_model->safa_trip_id = $this->input->get("safa_trip_id");

		if ($this->input->get("date_from"))
		$this->safa_trips_model->date_from = $this->input->get("date_from");
		if ($this->input->get("date_to"))
		$this->safa_trips_model->date_to = $this->input->get("date_to");

		if ($this->input->get("arrival_date"))
		$this->safa_trips_model->arrival_date = $this->input->get("arrival_date");
		if ($this->input->get("safa_tripstatus_id"))
		$this->safa_trips_model->safa_tripstatus_id = $this->input->get("safa_tripstatus_id");
		if ($this->input->get("safa_trip_confirm_id"))
		$this->safa_trips_model->safa_trip_confirm_id = $this->input->get("safa_trip_confirm_id");
		if ($this->input->get("erp_country_id"))
		$this->safa_trips_model->erp_country_id = $this->input->get("erp_country_id");

		if ($this->input->get("safa_ea_supervisor_ids"))
		$this->safa_trips_model->safa_ea_supervisor_ids = $this->input->get("safa_ea_supervisor_ids");


	}

	function manage($id = FALSE)
	{
		$this->load->library('form_validation');

		$data = array();
		$mode = '';

		$data["safa_tripstatus"] = ddgen("safa_tripstatus", array("safa_tripstatus_id", name()));
		$data["safa_trip_confirm"] = ddgen("safa_trip_confirm", array("safa_trip_confirm_id", name()));
		$data["erp_transportertypes"] = ddgen("erp_transportertypes", array("erp_transportertype_id", name()));

		$data['supervisors'] = $this->trip_supervisors_model->get_ea_supervisor();
		$data['trip_supervisors'] = array();

		$data['going_trip_flights_rows'] = array();
		$data['going_trip_flights_rows_count'] = 0;
		$data['return_trip_flights_rows'] = array();
		$data['return_trip_flights_rows_count'] = 0;

		$data['safa_trips_requests_rows'] = array();
		$data['safa_trips_requests_count'] = 0;
		$uo_services_arr_after_filter = array();
		$uo_services_arr_before_filter = ddgen("safa_uo_services", array("safa_uo_service_id", name()));
		foreach ($uo_services_arr_before_filter as $key => $value) {
			//if($key==1) {
			$uo_services_arr_after_filter[$key] = $value;
			//}
		}
		$data["safa_uo_services"] = $uo_services_arr_after_filter;



		$all_flights_rows = $this->flight_availabilities_model->get_for_trip();
		$data['all_flights_rows'] = $all_flights_rows;

		$data['erp_flight_classes'] = ddgen('erp_flight_classes', array('erp_flight_class_id', 'name'), false, false, true);
		$data['erp_flight_status'] = ddgen('erp_flight_status', array('erp_flight_status_id', 'name'), false, false, true);
		$data['erp_path_types'] = ddgen('erp_path_types', array('erp_path_type_id', name()), false, false, true);

		$erp_ports_rows = $this->ports_model->get_all_have_halls();
		$erp_ports_arr = array();
		foreach ($erp_ports_rows as $erp_ports_row){
			$erp_ports_arr[$erp_ports_row->erp_port_id]= $erp_ports_row->code;
		}
		$data["erp_ports"] = $erp_ports_arr;

		$erp_sa_ports_rows = $this->ports_model->get_all_have_halls('SA');
		$erp_sa_ports_arr = array();
		foreach ($erp_sa_ports_rows as $erp_sa_ports_row){
			$erp_sa_ports_arr[$erp_sa_ports_row->erp_port_id]= $erp_sa_ports_row->code;
		}
		$data["erp_sa_ports"] = $erp_sa_ports_arr;


		$erp_airlines_rows = $this->db->get(FSDB . '.fs_airlines')->result();
		$erp_airlines_arr = array();
		foreach ($erp_airlines_rows as $erp_airlines_row){
			$erp_airlines_arr[$erp_airlines_row->fs_airline_id]= $erp_airlines_row->iata;
		}
		$data["erp_airlines"] = $erp_airlines_arr;

			
		$data['ea_country_id'] = '';
		if (session('ea_id')) {
			$this->eas_model->safa_ea_id = session('ea_id');
			$ea_row = $this->eas_model->get();
			if(count($ea_row)>0) {
				$data['ea_country_id'] = $ea_row->erp_country_id;
			}
		}


		if ($id) {
			$this->safa_trips_model->safa_trip_id = $id;
			$data['item'] = $this->safa_trips_model->get();

			//------------------------ Going Flights ----------------------------------------
			//$this->flight_availabilities_model->erp_path_type_id=1;
			$this->flight_availabilities_model->safa_trip_id = $id;
			$going_trip_flights_rows = $this->flight_availabilities_model->get_for_trip();
			$data['going_trip_flights_rows'] = $going_trip_flights_rows;
			$data['going_trip_flights_rows_count'] = count($going_trip_flights_rows);

			$arr_going_trip_flights = array();
			foreach ($going_trip_flights_rows as $going_trip_flights_row) {
				$arr_going_trip_flights[$going_trip_flights_row->erp_flight_availability_id] = $going_trip_flights_row->erp_flight_availability_id;
			}
			//session('going_trip_flights_session', $arr_going_trip_flights);
			$_SESSION['going_trip_flights_session'] = $arr_going_trip_flights;
				
			//-------------------------------------------------------------------------------
			//------------------------ Return Flights ----------------------------------------
			/*
			 $this->flight_availabilities_model->erp_path_type_id=2;
			 $this->flight_availabilities_model->safa_trip_id=$id;
			 $return_trip_flights_rows = $this->flight_availabilities_model->get_details_for_trip();
			 $data['return_trip_flights_rows'] = $return_trip_flights_rows;
			 $data['return_trip_flights_rows_count'] = count($return_trip_flights_rows);

			 $arr_return_trip_flights = array();
			 foreach ($return_trip_flights_rows as $return_trip_flights_row) {
			 $arr_return_trip_flights[$return_trip_flights_row->erp_flight_availability_id] = $return_trip_flights_row->erp_flight_availability_id;
			 }
			 session('return_trip_flights_session', $arr_return_trip_flights);
			 */
			//---------------------------------------------------------------------------------
			//------------------------ Supervisors ----------------------------------------
			$this->trip_supervisors_model->safa_trip_id = $id;
			$supervisorids = $this->trip_supervisors_model->get();
			foreach ($supervisorids as $supervisorid) {
				$data['trip_supervisors'][] = $supervisorid->safa_ea_supervisor_id;
			}
			//-----------------------------------------------------------------------------
			//------------------------------- Group Passports ------------------------------------
			$data['item_group_passports'] = array();
			$this->safa_group_passports_model->safa_trip_id = $id;
			$item_group_passports = $this->safa_group_passports_model->get();
			$data['item_group_passports_rows'] = $item_group_passports;
			foreach ($item_group_passports as $item_group_passport) {
				$data['item_group_passports'][] = $item_group_passport->safa_group_passport_id;
			}
			//-----------------------------------------------------------------------------
			//------------------------ Trip Uo Requests -----------------------------------

			$safa_trips_requests_rows = $this->safa_trips_requests_model->get_by_trip_id($id);
			$data['safa_trips_requests_rows'] = $safa_trips_requests_rows;
			$data['safa_trips_requests_rows_count'] = count($safa_trips_requests_rows);

			$arr_safa_trips_requests = array();
			foreach ($safa_trips_requests_rows as $safa_trips_requests_row) {
				$arr_safa_trips_requests[$safa_trips_requests_row->safa_trips_requests_id] = $safa_trips_requests_row->safa_trips_requests_id;
			}
			//session('safa_trips_requests_session', $arr_safa_trips_requests);
			$_SESSION['safa_trips_requests_session'] = $arr_safa_trips_requests;
			
			//-----------------------------------------------------------------------------
		}


		$this->load->library("form_validation");

		$this->form_validation->set_rules('name', 'lang:name', 'trim|required');
		$this->form_validation->set_rules('the_date', 'lang:the_date', 'trim|required');
		$this->form_validation->set_rules('erp_country_id', 'lang:erp_country_id', 'trim|required');
		$this->form_validation->set_rules('safa_tripstatus_id', 'lang:safa_tripstatus_id', 'required');
		$this->form_validation->set_rules('erp_transportertype_id', 'lang:erp_transportertype_id', 'required');

		if ($this->form_validation->run() == false) {
			$this->load->view("ea/trips/manage", $data);
		} else {


			$this->safa_trips_model->name = $this->input->post('name');
			$this->safa_trips_model->date = $this->input->post('the_date');
			$this->safa_trips_model->arrival_date = $this->input->post('arrival_date');
			$this->safa_trips_model->safa_tripstatus_id = $this->input->post('safa_tripstatus_id');
			$this->safa_trips_model->erp_transportertype_id = $this->input->post('erp_transportertype_id');
			$this->safa_trips_model->erp_country_id = $this->input->post('erp_country_id');
			$this->safa_trips_model->notes = $this->input->post('notes');




			if ($id) {
				$this->trip_supervisors_model->safa_trip_id = $id;
				$this->trip_supervisors_model->delete();

				$this->safa_trips_model->safa_trip_id = $id;
				$this->safa_trips_model->save();

				$mode = 'edit';
			} else {

				$this->safa_trips_model->safa_trip_confirm_id = 2;
				$id = $this->safa_trips_model->save();
			}


			foreach ($this->input->post('safa_ea_supervisor_id') as $supervisor_id) {

				$this->trip_supervisors_model->safa_ea_supervisor_id = $supervisor_id;
				$this->trip_supervisors_model->safa_trip_id = $id;
				$this->trip_supervisors_model->save();
			}



			/////////////////////// Going Trip Flights ///////////////////////////////////////////////////////////////

			$counter = 1;
			foreach ($_POST AS $field => $value) {
				if ($this->start_with($field, 'trip_going_erp_flight_availability_id')) {
					$counter = substr($field, 37);
					//if ($_POST[$field] == 0) {

					$going_trip_flights_id = $_POST['trip_going_erp_flight_availability_id' . $counter];
					if($going_trip_flights_id>0) {
						$this->flight_availabilities_model->erp_flight_availability_id = $going_trip_flights_id;
						$this->flight_availabilities_model->safa_trip_id = $id;
						$this->flight_availabilities_model->save();
					}
					//}
					//------------- Delete going return updated from session will deleted --------
					$arr_going_trip_flights = $_SESSION['going_trip_flights_session'];
					if ($arr_going_trip_flights) {
						foreach ($arr_going_trip_flights as $key => $value) {
							if ($going_trip_flights_id == $key) {
								unset($arr_going_trip_flights[$key]);
							}
						}
					}
					//session('going_trip_flights_session', $arr_going_trip_flights);
					$_SESSION['going_trip_flights_session'] = $arr_going_trip_flights;
			
					//----------------------------------------------------------------------
				}
			}
			//----------- Delete Trip going -------------------------------
			//			$arr_going_trip_flights = session('going_trip_flights_session');
			//			if ($arr_going_trip_flights) {
			//				foreach ($arr_going_trip_flights as $key => $value) {
			//					$this->flight_availabilities_model->erp_flight_availability_id = $key;
			//					$this->flight_availabilities_model->safa_trip_id = null;
			//					$this->flight_availabilities_model->save();
			//				}
			//			}
			//
			//			$this->session->unset_userdata('going_trip_flights_session');

			///////////////////////////////////////////////////////////////////////////////////////////////////
			/////////////////////// Rturn Trip Flights ///////////////////////////////////////////////////////////////
			/*
			 $counter = 1;
			 foreach ($_POST AS $field => $value) {
			 if ($this->start_with($field, 'trip_return_erp_flight_availability_id')) {
			 $counter = substr($field, 38);
			 //if ($_POST[$field] == 0) {
			 $return_trip_flights_id = $_POST['trip_return_erp_flight_availability_id' . $counter];
			 $this->flight_availabilities_model->erp_flight_availability_id= $return_trip_flights_id ;
			 $this->flight_availabilities_model->safa_trip_id= $id;
			 $this->flight_availabilities_model->save();

			 //}

			 //------------- Delete trip return updated from session will deleted --------
			 $arr_return_trip_flights = session('return_trip_flights_session');
			 if ($arr_return_trip_flights) {
			 foreach ($arr_return_trip_flights as $key => $value) {
			 if ($return_trip_flights_id == $key) {
			 unset($arr_return_trip_flights[$key]);
			 }
			 }
			 }
			 session('return_trip_flights_session', $arr_return_trip_flights);
			 //----------------------------------------------------------------------
			 }
			 }

			 //----------- Delete Trip return -------------------------------
			 $arr_return_trip_flights = session('return_trip_flights_session');
			 if ($arr_return_trip_flights) {
			 foreach ($arr_return_trip_flights as $key => $value) {
			 $this->flight_availabilities_model->erp_flight_availability_id= $key ;
			 $this->flight_availabilities_model->safa_trip_id= null;
			 $this->flight_availabilities_model->save();
			 }
			 }

			 $this->session->unset_userdata('return_trip_flights_session');
			 */
			///////////////////////////////////////////////////////////////////////////////////////////////////
				
				
			/////////////////////// Save Flight Availability ///////////////////////////////////////////////////////////////
			$erp_airline_id_arr = $this->input->post('erp_airline_id');
			if(count($erp_airline_id_arr)>0) {
				$this->save_flight_availability($id);
			}
			////////////////////////////////////////////////////////////////////////////////////////////////////////
				
				
			/////////////////////// Trip Uo Requests ///////////////////////////////////////////////////////////////
			$counter = 1;
			foreach ($_POST AS $field => $value) {
				if ($this->start_with($field, 'safa_trips_requests_safa_trips_requests_id')) {
					$counter = substr($field, 42);
					if ($_POST[$field] == 0) {
						$trips_requests_data = array(
                            'safa_trip_id' => $id,
                            'safa_uo_service_id' => $_POST['safa_trips_requests_safa_uo_service_id' . $counter],
                            'remarks' => $_POST['safa_trips_requests_remarks' . $counter],
						);
						$this->safa_trips_requests_model->insert($trips_requests_data);
					} else {
						$safa_trips_requests_id = $_POST['safa_trips_requests_safa_trips_requests_id' . $counter];
						$trips_requests_data = array(
                            'safa_trip_id' => $id,
                            'safa_uo_service_id' => $_POST['safa_trips_requests_safa_uo_service_id' . $counter],
                            'remarks' => $_POST['safa_trips_requests_remarks' . $counter],
						);
						$this->safa_trips_requests_model->update($safa_trips_requests_id, $trips_requests_data);

						//------------- Delete trip Requests updated from session will deleted --------
						$arr_safa_trips_requests =  $_SESSION['safa_trips_requests_session'];
						if ($arr_safa_trips_requests) {
							foreach ($arr_safa_trips_requests as $key => $value) {
								if ($safa_trips_requests_id == $key) {
									unset($arr_safa_trips_requests[$key]);
								}
							}
						}
						//session('safa_trips_requests_session', $arr_safa_trips_requests);
						$_SESSION['safa_trips_requests_session'] = $arr_safa_trips_requests;
						//----------------------------------------------------------------------
					}
				}
			}
			//----------- Delete Trip Requests -------------------------------
			$arr_safa_trips_requests = $_SESSION['safa_trips_requests_session'];
			if ($arr_safa_trips_requests) {
				foreach ($arr_safa_trips_requests as $key => $value) {
					$this->safa_trips_requests_model->delete_by_trip_and_id($id, $value);
				}
			}
			//session('safa_trips_requests_session', NULL);
			$_SESSION['safa_trips_requests_session'] = NULL;
						

			redirect("ea/trips/finish/" . $id . '/' . $mode);
		}
	}

	function save_flight_availability($safa_trip_id)
	{
		$this->db->set('pnr', post('name'));
		$this->db->set('safa_ea_id', session('ea_id'));
		$this->db->set('safa_trip_id', $safa_trip_id);
		$this->db->set('by_trip', 1);

		$this->db->insert('erp_flight_availabilities');
		$flight_availability_id = $this->db->insert_id();

		$arr = array(
        'erp_airline_id',
        'flight_number',
        'erp_flight_class_id',
        'flight_date',
		'flight_arrival_date',
		'erp_port_id_from',
		'erp_port_id_to',
        'airports',
        'erp_flight_status_id',
        'seats_count',
        'safa_externaltriptype_id',
        'erp_path_type_id'
        );

        foreach ($arr as $item)
        $$item = post($item);

        foreach ($erp_airline_id as $key => $airline) {

        	 
        	$this->db->set('erp_flight_availability_id', $flight_availability_id);
        	$this->db->set('erp_airline_id', $erp_airline_id[$key]);
        	 
        	 
        	$current_flight_datetime = $flight_date[$key];
        	$current_flight_datetime_arr = explode (' ', $current_flight_datetime);
        	$current_flight_date = $current_flight_datetime_arr[0];
        	$current_flight_time='';
        	if(isset($current_flight_datetime_arr[1])) {
        		$current_flight_time = $current_flight_datetime_arr[1];
        	}
        	 
        	$current_arrival_datetime = $flight_arrival_date[$key];
        	$current_arrival_datetime_arr = explode (' ', $current_arrival_datetime);
        	$current_arrival_date = $current_arrival_datetime_arr[0];
        	$current_arrival_time='';
        	if(isset($current_arrival_datetime_arr[1])) {
        		$current_arrival_time = $current_arrival_datetime_arr[1];
        	}
        	 
        	$this->db->set('flight_date', $current_flight_date);
        	$this->db->set('arrival_date', $current_arrival_date);
        	$this->db->set('departure_time', $current_flight_time);
        	$this->db->set('arrival_time', $current_arrival_time);
        	 
        	$this->db->set('flight_number', $flight_number[$key]);
        	$this->db->set('erp_flight_class_id', $erp_flight_class_id[$key]);
        	$this->db->set('erp_port_id_from', $erp_port_id_from[$key]);
        	$this->db->set('erp_port_id_to', $erp_port_id_to[$key]);
        	 
        	$this->db->set('erp_path_type_id', $erp_path_type_id[$key]);
        	$this->db->set('safa_externaltriptype_id', $safa_externaltriptype_id[$key]);
        	 
        	$this->db->set('erp_flight_status_id', $erp_flight_status_id[$key]);
        	$this->db->set('seats_count', $seats_count[$key]);
        	 
        	 
        	 
        	$this->db->insert('erp_flight_availabilities_detail');
        }
	}

	function generate_safa_internaltrip($safa_trip_id)
	{
		$this->trip_internaltrip_model->safa_trip_id = $safa_trip_id;
		if (session('ea_id')) {
			$this->trip_internaltrip_model->safa_ea_id = session('ea_id');
		}
		$this->trip_internaltrip_model->datetime = $this->input->post('the_date');
		

		if ($this->input->post('safa_ito_id'))
		$this->trip_internaltrip_model->safa_ito_id = $this->input->post('safa_ito_id');
		if ($this->input->post('safa_transporter_id'))
		$this->trip_internaltrip_model->safa_transporter_id = $this->input->post('safa_transporter_id');

		$this->trip_internaltrip_model->safa_internaltripstatus_id = $this->input->post('safa_internaltripstatus_id');
		$this->trip_internaltrip_model->operator_reference = $this->input->post('operator_reference');
		
		if (session('ea_id')) {

			$this->contracts_model->safa_uo_contract_id = $this->input->post('safa_uo_contract_id');
			$contracts_row = $this->contracts_model->get();
			if(count($contracts_row)>0) {
				$safa_uo_code='';
				$safa_uo_serial='';

				$this->safa_uos_model->safa_uo_id = $contracts_row->safa_uo_id;
				$safa_uo_row = $this->safa_uos_model->get();
				if(count($safa_uo_row)>0) {
					$safa_uo_code = $safa_uo_row->code;
				}

				$safa_uo_row = $this->trip_internaltrip_model->get_max_serial_by_uo($contracts_row->safa_uo_id);
				if(count($safa_uo_row)>0) {
					$safa_uo_serial = $safa_uo_row->max_serial+1;
				}

				$this->trip_internaltrip_model->trip_title = $safa_uo_code.'-'.$safa_uo_serial;
				$this->trip_internaltrip_model->serial = $safa_uo_serial;
			}
		}

		$this->trip_internaltrip_model->trip_supervisors = $this->input->post('trip_supervisors');
		$this->trip_internaltrip_model->trip_supervisors_phone = $this->input->post('trip_supervisors_phone');
		$this->trip_internaltrip_model->safa_uo_contract_id = $this->input->post('safa_uo_contract_id');

		$this->trip_internaltrip_model->adult_seats = $this->input->post('adult_seats');
		$this->trip_internaltrip_model->child_seats = $this->input->post('child_seats');
		$this->trip_internaltrip_model->baby_seats = $this->input->post('baby_seats');

		$this->trip_internaltrip_model->confirmation_number = $this->input->post('confirmation_number');

		$this->trip_internaltrip_model->erp_transportertype_id = $this->input->post('erp_transportertype_id');
		$this->trip_internaltrip_model->save();
	}

	function delete($id = false) {
		if (!$id)
		show_404();

		$this->safa_trips_model->safa_trip_id = $id;

		$safa_trips_row = $this->safa_trips_model->get();
		if($safa_trips_row->safa_tripstatus_id==3 || $safa_trips_row->safa_tripstatus_id==4) {
			show_404();
		}

		if (!$this->safa_trips_model->delete())
		show_404();

		$this->load->view('redirect_message', array('msg' => lang('global_deleted_message'),
            'url' => site_url('ea/trips'),
            'model_title' => lang('trips'), 'action' => lang('global_delete')));
	}

	function finish($id = FALSE, $mode = 'add') {
		if (!$id)
		show_404();
		$this->safa_trips_model->safa_ea_id = session("ea_id");
		$this->safa_trips_model->safa_trip_id = $id;
		$item = $this->safa_trips_model->get();
		if (!$item) {
			show_404();
		}

		if ($mode == 'add') {
			$action = lang('add_title');
			$msg = lang('global_added_message');
		} else {
			$action = lang('edit_title');
			$msg = lang('global_updated_message');
		}

		$this->load->view('ea/trips/redirect_message', array(
            'msg' => $msg,
            'action' => $action,
            'url' => site_url('ea/trips'),
            'item' => $item,
            'back' => site_url('ea/trips/manage/' . $id)
		));
	}

	function send_to_uo_popup($id) {
		$this->layout = 'js_new';

		$data['trip_id'] = $id;

		//$this->contracts_model->get();
		$uo_services_arr_after_filter = array();
		$uo_services_arr_before_filter = ddgen("safa_uo_services", array("safa_uo_service_id", name()));
		foreach ($uo_services_arr_before_filter as $key => $value) {
			//if($key==1) {
			$uo_services_arr_after_filter[$key] = $value;
			//}
		}
		$data["safa_uo_services"] = $uo_services_arr_after_filter;

		$this->safa_trips_model->safa_ea_id = session('ea_id');
		$this->safa_trips_model->safa_trip_id = $id;
		$trip_row = $this->safa_trips_model->get();

		$safa_trips_requests_rows = $this->safa_trips_requests_model->get_by_trip_id($id);
		$data['safa_trips_requests_rows'] = $safa_trips_requests_rows;
		$data['safa_trips_requests_count'] = count($safa_trips_requests_rows);

		if (!isset($_POST['smt_send'])) {
			$this->load->view('ea/trips/send_to_uo_popup', $data);
		} else {

			/////////////////////// Trip Uo Requests ///////////////////////////////////////////////////////////////
			$counter = 1;
			$arr_safa_trips_requests_inputs = array();
			foreach ($_POST AS $field => $value) {
				if ($this->start_with($field, 'safa_trips_requests_safa_trips_requests_id')) {
					$counter = substr($field, 42);
					if ($_POST[$field] == 0) {
						$trips_requests_data = array(
                            'safa_trip_id' => $id,
                            'safa_uo_service_id' => $_POST['safa_trips_requests_safa_uo_service_id' . $counter],
                            'remarks' => $_POST['safa_trips_requests_remarks' . $counter],
						);
						$this->safa_trips_requests_model->insert($trips_requests_data);
					} else {
						$safa_trips_requests_id = $_POST['safa_trips_requests_safa_trips_requests_id' . $counter];
						$trips_requests_data = array(
                            'safa_trip_id' => $id,
                            'safa_uo_service_id' => $_POST['safa_trips_requests_safa_uo_service_id' . $counter],
                            'remarks' => $_POST['safa_trips_requests_remarks' . $counter],
						);
						$this->safa_trips_requests_model->update($safa_trips_requests_id, $trips_requests_data);

						//------------- Delete trip tourismplace updated from session will deleted --------
						$arr_safa_trips_requests = $_SESSION['safa_trips_requests_session'];
						if ($arr_safa_trips_requests) {
							foreach ($arr_safa_trips_requests as $key => $value) {
								if ($safa_trips_requests_id == $key) {
									unset($arr_safa_trips_requests[$key]);
								}
							}
						}
						//session('safa_trips_requests_session', $arr_safa_trips_requests);
						$_SESSION['safa_trips_requests_session'] = $arr_safa_trips_requests;
						//----------------------------------------------------------------------
					}
				}
			}
			//----------- Delete trip tourismplace -------------------------------
			$arr_safa_trips_requests = $_SESSION['safa_trips_requests_session'];
			if ($arr_safa_trips_requests) {
				foreach ($arr_safa_trips_requests as $key => $value) {
					$this->safa_trips_requests_model->delete_by_trip_and_id($id, $value);
				}
			}
			//session('safa_trips_requests_session', NULL);
			$_SESSION['safa_trips_requests_session'] = NULL;

			//Update status & confirmation of trip
			$this->safa_trips_model->safa_trip_id = $id;
			$this->safa_trips_model->safa_tripstatus_id = 3;
			$this->safa_trips_model->safa_trip_confirm_id = 1;
			$this->safa_trips_model->save();

			$this->db->where('safa_trip_id',$id)->set('safa_internaltripstatus_id',1)->update('safa_trip_internaltrips');


			//----------- Send Notification For Company ------------------------------
			$msg_datetime = date('Y-m-d H:i', time());

			$this->notification_model->notification_type = 'automatic';
			$this->notification_model->erp_importance_id = 1;
			$this->notification_model->sender_type_id = 1;
			$this->notification_model->sender_id = 0;

			$this->notification_model->erp_system_events_id = 23;

			$this->notification_model->language_id = 2;
			$this->notification_model->msg_datetime = $msg_datetime;

			$tags = '';
			if (count($trip_row) > 0) {

				//Link to replace.
				$link_trip = "<a href='" . site_url('trip_details/details/' . $trip_row->safa_trip_id) . "'  target='_blank'> " . $trip_row->safa_trip_id . " </a>";
				$link_trip_name = "<a href='" . site_url('trip_details/details/' . $trip_row->safa_trip_id) . "'  target='_blank'> " . $trip_row->name . " </a>";

				$tags = "#*trip_no#*=" . $link_trip . "*****#*trip#*=" . $link_trip_name . "*****#*date#*=" . $trip_row->date . "*****#*trip_date#*=" . $trip_row->date;
			}
			$this->notification_model->tags = $tags;
			$this->notification_model->parent_id = '';

			$erp_notification_id = $this->notification_model->save();

			//-------- Notification Details ---------------
			$this->notification_model->detail_erp_notification_id = $erp_notification_id;

			$this->notification_model->detail_receiver_type = 2;

			if (count($trip_row) > 0) {

				$uo_rows = $this->safa_group_passports_model->get_uos_by_trip($id);
				foreach ($uo_rows as $uo_row) {
					$this->notification_model->detail_receiver_id = $uo_row->safa_uo_id;
					$this->notification_model->saveDetails();
				}
			}
			//----------------------------------------------
			//-------------------------------------------------------------------



			$this->load->view('redirect_message_popup', array('msg' => lang('send_to_uo_message'),
                'url' => site_url("ea/trips/send_to_uo_popup"),
                'id' => $this->uri->segment("4"),
                'model_title' => lang('title'), 'action' => lang('send_to_uo')));
		}
	}

	public function passports($id)
	{
		$this->safa_group_passports_model->safa_trip_id = $id;
		$data['passports'] = $this->safa_group_passports_model->get_for_accomedation();
		$this->load->view('ea/trips/passports', $data);
	}

	public function start_with($s, $prefix)
	{
		return strpos($s, $prefix) === 0;
	}

}