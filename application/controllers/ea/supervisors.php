<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Supervisors extends Safa_Controller {

    public $module = "supervisors";

    public function __construct() {
        parent::__construct();
        $this->layout = 'new';
        //Side menu session, By Gouda.
        session('side_menu_id', 0);
        
        $this->load->model('safa_ea_supervisors_model');
        $this->lang->load('supervisors');
        $this->load->helper('form_helper');
        $this->load->helper('db_helper');
        $this->load->helper('itconf_helper');
        permission();
        if (session('ea_id')) {
            $this->safa_ea_supervisors_model->safa_ea_id = session('ea_id');
        }
    }

    public function index() {
        if (isset($_GET['search'])) {
            $this->search();
        }
        $data["total_rows"] = $this->safa_ea_supervisors_model->get(true);

        $this->safa_ea_supervisors_model->offset = $this->uri->segment("4");
        $this->safa_ea_supervisors_model->limit = $this->config->item('per_page');
        $data["items"] = $this->safa_ea_supervisors_model->get();
        $config['uri_segment'] = 4;
        $config['base_url'] = site_url('ea/supervisors/index');
        $config['total_rows'] = $data["total_rows"];

        $config['per_page'] = $this->config->item('per_page');
        $config['suffix'] = $this->input->get() ? '?' . http_build_query($_GET) : NULL;
        $this->load->library('pagination');
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();
        $this->load->view('ea/supervisors/index', $data);
    }

    public function add() {
        $data = array();

        $this->load->library("form_validation");

        $this->form_validation->set_rules('name_ar', 'lang:name_ar', 'trim|required');
        $this->form_validation->set_rules('name_la', 'lang:name_la', 'trim|required');
        //$this->form_validation->set_rules('phone', 'lang:phone', 'trim|required');
        $this->form_validation->set_rules('mobile', 'lang:mobile', 'trim|required');

        if ($this->form_validation->run() == false) {
            $this->load->view("ea/supervisors/add", $data);
        } else {
            $this->safa_ea_supervisors_model->safa_ea_id = session('ea_id');
            $this->safa_ea_supervisors_model->name_ar = $this->input->post('name_ar');
            $this->safa_ea_supervisors_model->name_la = $this->input->post('name_la');

            $this->safa_ea_supervisors_model->phone = $this->input->post('phone');
            $this->safa_ea_supervisors_model->mobile = $this->input->post('mobile');
            $this->safa_ea_supervisors_model->mobile_sa = $this->input->post('mobile_sa');
            $this->safa_ea_supervisors_model->notes = $this->input->post('notes');

            // Save Phases
            $safa_ea_supervisor_id = $this->safa_ea_supervisors_model->save();

            if (isset($safa_ea_supervisor_id)) {

                $this->load->view('redirect_message', array('msg' => lang('global_added_message'),
                    'url' => site_url('ea/supervisors'),
                    'id' => $this->uri->segment("4"),
                    'model_title' => lang('title'), 'action' => lang('add_title')));
            }
        }
    }

    public function edit($id = FALSE) {
        $data = array();

        $this->load->library("form_validation");

        $this->form_validation->set_rules('name_ar', 'lang:name_ar', 'trim|required');
        $this->form_validation->set_rules('name_la', 'lang:name_la', 'trim|required');
        //$this->form_validation->set_rules('phone', 'lang:phone', 'trim|required');
        $this->form_validation->set_rules('mobile', 'lang:mobile', 'trim|required');

        if (!$id) {
            show_404();
        }
        $this->safa_ea_supervisors_model->safa_ea_supervisor_id = $id;
        $data['item'] = $this->safa_ea_supervisors_model->get();

        if (!$data['item']) {
            show_404();
        }



        if ($this->form_validation->run() == false) {
            $this->load->view("ea/supervisors/edit", $data);
        } else {
            $this->safa_ea_supervisors_model->safa_ea_id = session('ea_id');
            $this->safa_ea_supervisors_model->name_ar = $this->input->post('name_ar');
            $this->safa_ea_supervisors_model->name_la = $this->input->post('name_la');

            $this->safa_ea_supervisors_model->phone = $this->input->post('phone');
            $this->safa_ea_supervisors_model->mobile = $this->input->post('mobile');
            $this->safa_ea_supervisors_model->mobile_sa = $this->input->post('mobile_sa');
            $this->safa_ea_supervisors_model->notes = $this->input->post('notes');

            // Save Phases
            $this->safa_ea_supervisors_model->save();


            $this->load->view('redirect_message', array('msg' => lang('global_updated_message'),
                'url' => site_url('ea/supervisors'),
                'id' => $this->uri->segment("4"),
                'model_title' => lang('title'), 'action' => lang('edit_title')));
        }
    }

    function delete($id = false) {
        if (!$id) {
            show_404();
        }
        $this->db->trans_start();
        $this->safa_ea_supervisors_model->safa_ea_supervisor_id = $id;
        $this->safa_ea_supervisors_model->delete();
        $this->db->trans_complete();
        $this->load->view('redirect_message', array('msg' => lang('global_deleted_message'),
            'url' => site_url('ea/supervisors'),
            'model_title' => lang('title'), 'action' => lang('delete_title')));
    }

    function delete_all() {
        if ($this->input->post("delete_items")) {
            foreach ($this->input->post("delete_items") as $item) {
                $this->safa_ea_supervisors_model->safa_ea_supervisor_id = $item;
                $this->safa_ea_supervisors_model->delete();
            }
        } else {
            show_404();
        }
        redirect("ea/supervisors/index");
    }

    function search() {
        if ($this->input->get("safa_ea_supervisor_id"))
            $this->safa_ea_supervisors_model->safa_ea_supervisor_id = $this->input->get("safa_ea_supervisor_id");
        if ($this->input->get("safa_trip_id"))
            $this->safa_ea_supervisors_model->safa_trip_id = $this->input->get("safa_trip_id");
        if ($this->input->get("safa_ito_id"))
            $this->safa_ea_supervisors_model->safa_ito_id = $this->input->get("safa_ito_id");
        if ($this->input->get("safa_transporter_id"))
            $this->safa_ea_supervisors_model->safa_transporter_id = $this->input->get("safa_transporter_id");
        if ($this->input->get("safa_tripstatus_id"))
            $this->safa_ea_supervisors_model->safa_tripstatus_id = $this->input->get("safa_tripstatus_id");
        if ($this->input->get("safa_internaltripstatus_id"))
            $this->safa_ea_supervisors_model->safa_internaltripstatus_id = $this->input->get("safa_internaltripstatus_id");
        if ($this->input->get("operator_reference"))
            $this->safa_ea_supervisors_model->operator_reference = $this->input->get("operator_reference");
        if ($this->input->get("attachement"))
            $this->safa_ea_supervisors_model->attachement = $this->input->get("attachement");
        if ($this->input->get("datetime"))
            $this->safa_ea_supervisors_model->datetime = $this->input->get("datetime");
        /* search utility */
        if ($this->input->get("ea_contract_id"))
            $this->safa_ea_supervisors_model->by_contract = $this->input->get("ea_contract_id");
        /* search utility */
    }

}

/* End of file supervisors.php */
/* Location: ./application/controllers/supervisors.php */