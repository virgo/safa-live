<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Trips extends Safa_Controller {

    public $module = "trips";

    public function __construct() {
        parent::__construct();

        $this->layout = 'new';
        //Side menu session, By Gouda.
        session('side_menu_id', 1);


        $this->load->model('trips_model');
        $this->load->model('trip_externalsegments_model');
        $this->load->model('trip_hotel_model');
        $this->load->model('trip_tourismplace_model');
        $this->load->model('trip_internalpassage_model');
        $this->load->model('hotels_model');
        $this->load->model('tourismplaces_model');
        $this->load->model('erp_port_halls_model');
        $this->load->model('ports_model');
        
        //By Gouda
        $this->load->model('trip_travellers_model');
        $this->load->model('hotels_availability_room_details_model');
        $this->lang->load('group_passports');
        $this->load->model('flight_availabilities_model');
        $this->load->model('trip_externalsegments_availabilities_model');
        $this->load->model('safa_trips_requests_model');
        $this->load->model('uos_model');
        $this->load->model('notification_model');
        $this->load->model('contracts_settings_model');
        $this->load->model('safa_uo_packages_model');
        $this->load->model('ea_packages_model');
        $this->load->model('uo_package_hotels_model');
        $this->load->model('uo_package_tourismplaces_model');
        
        
        
        $this->load->library('form_validation');
        $this->lang->load('trips');
        $this->load->helper('form_helper');
        ea_login();
        if (!permission('trips'))
            show_404();
    }

    public function index() {
        $data = array();

        $data["safa_ea_seasons"] = ddgen("safa_ea_seasons", array("safa_ea_season_id", 'name'));
        $data["safa_eas"] = ddgen("safa_eas", array("safa_ea_id", name()));
        $data["safa_tripstatus"] = ddgen("safa_tripstatus", array("safa_tripstatus_id", name()));
        $data['safa_ea_season_id'] = '';
        $data['date_from'] = '';
        $data['date_to'] = '';
        $data['safa_ea_id'] = '';
        $data['name'] = '';
        $data['safa_tripstatus_id'] = '';

//        $this->trips_model->custom_select = 'safa_trips.*, safa_eas.name_ar as safa_ea_ar, safa_trip_confirm.name_ar as safa_trip_confirm_ar';

        if ($this->input->get("safa_ea_season_id")) {
            $this->trips_model->safa_ea_season_id = $this->input->get("safa_ea_season_id");
            $data['safa_ea_season_id'] = $this->input->get("safa_ea_season_id");
        }
        if ($this->input->get("date_from")) {
            $this->trips_model->date_from = $this->input->get("date_from");
            $data['date_from'] = $this->input->get("date_from");
        }
        if ($this->input->get("date_to")) {
            $this->trips_model->date_to = $this->input->get("date_to");
            $data['date_to'] = $this->input->get("date_to");
        }
//        if ($this->input->get("safa_ea_id")) {
//            $this->trips_model->safa_ea_id = $this->input->get("safa_ea_id");
//            $data['safa_ea_id'] = $this->input->get("safa_ea_id");
//        }
        if ($this->input->get("name")) {
            $this->trips_model->name = $this->input->get("name");
            $data['name'] = $this->input->get("name");
        }
        if ($this->input->get("safa_tripstatus_id")) {
            $this->trips_model->safa_tripstatus_id = $this->input->get("safa_tripstatus_id");
            $data['safa_tripstatus_id'] = $this->input->get("safa_tripstatus_id");
        }
        $this->trips_model->safa_ea_id = session("ea_id");
        $this->trips_model->order_by = array('date', 'DESC');
        $data["items"] = $this->trips_model->get();
        $this->load->view("ea/trips/index", $data);
    }

    public function delete($id) {

        if (!$id)
            show_404();

        $this->trips_model->safa_ea_id = session('ea_id');
        $this->trips_model->safa_trip_id = $id;
        $trip = $this->trips_model->get();
        if (!$trip)
            show_404();
        if ($trip->internal_trips)
            show_404();

        $hotels = $this->db->where('safa_trip_id', $id)->get('safa_trip_hotels')->result();
        if ($hotels && is_array($hotels) && count($hotels))
            foreach ($hotels as $hotel) {
                $this->db->where('safa_trip_hotel_id', $hotel->safa_trip_hotel_id)->delete('safa_trip_hotel_rooms');
            }
        // DELETE HOTELS
        $this->db->where('safa_trip_id', $id)->delete('safa_trip_hotels');
        // DELETE Externalsegments
        $this->db->where('safa_trip_id', $id)->delete('safa_trip_externalsegments');
        // DELETE Travellers
        $this->db->where('safa_trip_id', $id)->delete('safa_trip_travellers');
        // DELETE Tourism Places
        $this->db->where('safa_trip_id', $id)->delete('safa_trip_tourismplaces');
        // DELETE Supervisors
        $this->db->where('safa_trip_id', $id)->delete('safa_trip_supervisors');

        if (!$this->trips_model->delete())
            show_404();
        redirect("ea/trips");
    }

    public function search() {
        if ($this->input->get("safa_trips_id"))
            $this->trips_model->safa_trips_id = $this->input->get("safa_trips_id");
        if ($this->input->get("safa_trip_id"))
            $this->trips_model->safa_trip_id = $this->input->get("safa_trip_id");
        if ($this->input->get("safa_ito_id"))
            $this->trips_model->safa_ito_id = $this->input->get("safa_ito_id");
        if ($this->input->get("safa_internaltripstatus_id"))
            $this->trips_model->safa_internaltripstatus_id = $this->input->get("safa_internaltripstatus_id");
        if ($this->input->get("operator_reference"))
            $this->trips_model->operator_reference = $this->input->get("operator_reference");
        if ($this->input->get("attachement"))
            $this->trips_model->attachement = $this->input->get("attachement");
        if ($this->input->get("datetime"))
            $this->trips_model->datetime = $this->input->get("datetime");

        $this->load->view("ea/trips/search", $data);
    }

    public function form($id = 0) 
    {
        $data['item'] = array();
        $data['trip_going_count'] = 0;
        $data['trip_return_count'] = 0;
        $data['trip_hotel_count'] = 0;
        $data['trip_tourismplace_count'] = 0;
        $data['trip_internalpassage_count'] = 0;
        $data["contract_hotels"] = array();
        $data['trip_going_rows'] = array();
        $data['trip_return_rows'] = array();
        $data['trip_hotel_rows'] = array();
        $data['trip_tourismplace_rows'] = array();
        $data['trip_internalpassage_rows'] = array();
        $data['safa_itos'] = array();
        $data['trip_supervisors'] = array();

        $data['safa_trips_requests_rows'] = array();
        $data['safa_trips_requests_count'] =0;
        
        //$this->contracts_model->get();
		$uo_services_arr_after_filter=array();
		$uo_services_arr_before_filter = ddgen("safa_uo_services", array("safa_uo_service_id", name()));
		foreach($uo_services_arr_before_filter as $key=>$value) {
			//if($key==1) {
				$uo_services_arr_after_filter[$key]=$value;
			//}
		}
		$data["safa_uo_services"] = $uo_services_arr_after_filter;
        
        
        
        $this->load->model('trip_supervisors_model');
        if ($id != 0) {
            $this->trips_model->safa_ea_id = session('ea_id');
            $this->trips_model->safa_trip_id = $id;
            $data['item'] = $this->trips_model->get();
            if (!$data['item'])
                redirect('ea/trips');
            //------------------------ Trip going ----------------------------------------
            $trip_going_rows = $this->trip_externalsegments_model->get_trip_externalsegments_by_trip_id(1, $id);
            $data['trip_going_rows'] = $trip_going_rows;
            $data['trip_going_count'] = count($trip_going_rows);

            $this->trip_supervisors_model->safa_trip_id = $id;
            $supervisorids = $this->trip_supervisors_model->get();

            foreach ($supervisorids as $supervisorid)
                $data['trip_supervisors'][] = $supervisorid->safa_ea_supervisor_id;

            $arr_trip_goings = array();
            foreach ($trip_going_rows as $trip_going_row) {
                $arr_trip_goings[$trip_going_row->safa_trip_externalsegment_id] = $trip_going_row->safa_trip_externalsegment_id;
            }
            session('trip_goings_session', $arr_trip_goings);


            //------------------------ Trip return ----------------------------------------
            $trip_return_rows = $this->trip_externalsegments_model->get_trip_externalsegments_by_trip_id(2, $id);
            $data['trip_return_rows'] = $trip_return_rows;
            $data['trip_return_count'] = count($trip_return_rows);

            $arr_trip_returns = array();
            foreach ($trip_return_rows as $trip_return_row) {
                $arr_trip_returns[$trip_return_row->safa_trip_externalsegment_id] = $trip_return_row->safa_trip_externalsegment_id;
            }
            session('trip_returns_session', $arr_trip_returns);


            //------------------------ Trip hotel ----------------------------------------
            $trip_hotel_rows = $this->trip_hotel_model->get_trip_hotels_by_trip_id($id);
            $data['trip_hotel_rows'] = $trip_hotel_rows;
            $data['trip_hotel_count'] = count($trip_hotel_rows);

            $arr_trip_hotels = array();
            foreach ($trip_hotel_rows as $trip_hotel_row) {
                $arr_trip_hotels[$trip_hotel_row->safa_trip_hotel_id] = $trip_hotel_row->safa_trip_hotel_id;
            }
            session('trip_hotels_session', $arr_trip_hotels);


            //------------------------ Trip tourismplace ----------------------------------------
            $trip_tourismplace_rows = $this->trip_tourismplace_model->get_trip_tourismplaces_by_trip_id($id);
            $data['trip_tourismplace_rows'] = $trip_tourismplace_rows;
            $data['trip_tourismplace_count'] = count($trip_tourismplace_rows);

            $arr_trip_tourismplaces = array();
            foreach ($trip_tourismplace_rows as $trip_tourismplace_row) {
                $arr_trip_tourismplaces[$trip_tourismplace_row->safa_trip_tourismplace_id] = $trip_tourismplace_row->safa_trip_tourismplace_id;
            }
            session('trip_tourismplaces_session', $arr_trip_tourismplaces);
            
            
	        //By Gouda
	    	$data['item_travellers']=array();
	        $this->trip_travellers_model->safa_trip_id = $id;
	        $item_travellers = $this->trip_travellers_model->get();
	        $data['item_travellers_rows']=$item_travellers;
	        foreach($item_travellers as $item_traveller) {
	            $data['item_travellers'][] = $item_traveller->safa_group_passport_id;
	        }
	        
	        //------------------------ Trip Uo Requests ----------------------------------------
            $safa_trips_requests_rows = $this->safa_trips_requests_model->get_by_trip_id($id);
            $data['safa_trips_requests_rows'] = $safa_trips_requests_rows;
            $data['safa_trips_requests_rows_count'] = count($safa_trips_requests_rows);

            $arr_safa_trips_requests = array();
            foreach ($safa_trips_requests_rows as $safa_trips_requests_row) {
                $arr_safa_trips_requests[$safa_trips_requests_row->safa_trips_requests_id] = $safa_trips_requests_row->safa_trips_requests_id;
            }
            session('safa_trips_requests_session', $arr_safa_trips_requests);
            
            
        }

        $data["safa_ea_packages"] = ddgen("safa_ea_packages", array("safa_ea_package_id", 'safa_uo_package_id'));
        $data["safa_tripstatus"] = ddgen("safa_tripstatus", array("safa_tripstatus_id", name()));
        $data["safa_trip_confirm"] = ddgen("safa_trip_confirm", array("safa_trip_confirm_id", name()));
        $data["erp_transportertypes"] = ddgen("erp_transportertypes", array("erp_transportertype_id", name()));
        $data["erp_cities"] = ddgen("erp_cities", array("erp_city_id", name()));
        $data["ksa_cities"] = ddgen("erp_cities", array("erp_city_id", name()), array('erp_country_id' => '966'));
        $data["erp_hotels"] = ddgen("erp_hotels", array("erp_hotel_id", name()));

        
        
        //        if(isset($data['item']->safa_uo_contract_id))//ddgen("erp_hotels", array("erp_hotel_id", name()));//
        if ($id != 0)
            $data["contract_hotels"] = ddquery('SELECT erp_hotels.erp_hotel_id, erp_hotels.' . name() . ' 
            FROM erp_hotels 
            WHERE erp_hotel_id IN 
            (SELECT erp_hotel_id 
             FROM safa_uo_contracts_hotels 
             JOIN safa_uo_contracts_eas ON safa_uo_contracts_eas.safa_uo_contract_id = safa_uo_contracts_hotels.safa_uo_contract_id 
             WHERE safa_uo_contracts_eas.safa_uo_contract_ea_id = "' . $data['item']->safa_uo_contract_ea_id . '")
        ', array("erp_hotel_id", name()));

        $data["erp_meals"] = ddgen("erp_meals", array("erp_meal_id", name()));
        
        
        //By Gouda, to change the contract name in EA Module.
        /*
        $data["safa_eas"][''] = lang('global_select_from_menu');
        $contracts = $this->db->query("select safa_uo_contracts_eas.safa_uo_contract_ea_id, safa_uo_contracts." . name() . " from 
                                safa_uo_contracts_eas 
                                left join safa_uo_contracts on safa_uo_contracts.safa_uo_contract_id=safa_uo_contracts_eas.safa_uo_contract_id
                                where safa_ea_id='" . session('ea_id') . "'")->result();
        $data['select_safa_eas'] = NULL;
        if ($contracts && is_array($contracts) && count($contracts)) {
            $i = 0;
            foreach ($contracts as $contract) {
                if ($i == 0)
                    $data['select_safa_eas'] = $contract->safa_uo_contract_ea_id;$i++;
                $data["safa_eas"][$contract->safa_uo_contract_ea_id] = $contract->{name()};
            }
        }
        */
        
        //By Gouda.
        $data['select_safa_eas'] = NULL;
        $data["safa_eas"]= $this->create_contracts_array();
        
        $data["erp_transportertypes"] = ddgen("erp_transportertypes", array("erp_transportertype_id", name()));
        $data["erp_ports"] = $this->ports_model->get_all_have_halls();
        $data["erp_sa_ports"] = $this->ports_model->get_all_have_halls('SA');
        $data["safa_tourismplaces"] = ddgen("safa_tourismplaces", array("safa_tourismplace_id", name()));
        $data["safa_internalsegmenttypes"] = ddgen("safa_internalsegmenttypes",
                array("safa_internalsegmenttype_id", name()));
        $data["safa_internalsegmentstatus"] = ddgen("safa_internalsegmentstatus",
                array("safa_internalsegmentstatus_id", name()));
        $data["safa_transporters"][''] = lang('global_select_from_menu');
        foreach ($this->db->where('erp_transportertype_id', 2)->get('safa_transporters')->result() as $trans)
            $data["safa_transporters"][$trans->safa_transporter_id] = $trans->{name()} . ' - ' . $trans->code;
//        $data["safa_transporters"] = ddgen("safa_transporters", array("safa_transporter_id", name()), array('erp_transportertype_id' => 2));

        $data["safa_externalsegmenttypes"] = ddgen("safa_externalsegmenttypes",
                array("safa_externalsegmenttype_id", name()));


        $data['supervisors'] = $this->trip_supervisors_model->get_ea_supervisor();
		//By Gouda
        $data['travellers'] = $this->trip_travellers_model->get_safa_group_passports();
        
        
        
        //$this->form_validation->set_rules('safa_ea_package_id', 'lang:safa_ea_package_id', 'required');
        
        //By Gouda
        //$this->form_validation->set_rules('safa_trip_confirm_id', 'lang:safa_trip_confirm_id', 'required');
        
        $this->form_validation->set_rules('safa_tripstatus_id', 'lang:safa_tripstatus_id', 'required');
        $this->form_validation->set_rules('erp_transportertype_id', 'lang:erp_transportertype_id', 'required');
        if ($this->form_validation->run() == false) {
            $this->load->view("ea/trips/form", $data);
        } else if (!isset($_POST['smt_save'])) {
            $this->load->view("ea/trips/form", $data);
        } else {

            if ($id) {
                $this->trip_supervisors_model->safa_trip_id = $id;
                $this->trip_supervisors_model->delete();
                $this->trips_model->safa_trip_id = $id;
            } 

            $this->trips_model->name = $this->input->post('name');
            $this->trips_model->date = $this->input->post('date');
            $this->trips_model->safa_ea_season_id = $this->input->post('safa_ea_season_id');
            $this->trips_model->safa_ea_id = $this->input->post('safa_ea_id');
            
            $this->trips_model->safa_ea_package_id = $this->input->post('safa_ea_package_id');
            
            $this->trips_model->safa_tripstatus_id = $this->input->post('safa_tripstatus_id');
            $this->trips_model->notes = $this->input->post('notes');
            
            //By Gouda.
            //$this->trips_model->safa_trip_confirm_id = $this->input->post('safa_trip_confirm_id');
            if($id==0) {
            	$this->trips_model->safa_trip_confirm_id = 2;
            }
            
            $this->trips_model->erp_transportertype_id = $this->input->post('erp_transportertype_id');
            $this->trips_model->travellers_adult_count = $this->input->post('travellers_adult_count');
            $this->trips_model->travellers_child_count = $this->input->post('travellers_child_count');
            $this->trips_model->travellers_infant_count = $this->input->post('travellers_infant_count');
            $this->trips_model->erp_country_id = $this->input->post('erp_country_id');
            $id = $this->trips_model->save();


            foreach ($this->input->post('safa_ea_supervisor_id') as $supervisor_id) {
                $this->trip_supervisors_model->safa_ea_supervisor_id = $supervisor_id;
                $this->trip_supervisors_model->safa_trip_id = $id;
                $this->trip_supervisors_model->save();
            }
            
            // By Gouda, To save trip travellers.
            $this->trip_travellers_model->safa_trip_id = $id;
            $this->trip_travellers_model->delete();
            if($this->input->post('safa_trip_traveller_ids')) {
        	foreach ($this->input->post('safa_trip_traveller_ids') as $safa_trip_traveller_id) {
                $this->trip_travellers_model->safa_group_passport_id = $safa_trip_traveller_id;
                $this->trip_travellers_model->save();
            }
            }
            
            
            /////////////////////// Trip going ///////////////////////////////////////////////////////////////
            $counter = 1;
            $arr_trip_goings_inputs = array();
            foreach ($_POST AS $field => $value) {
                if ($this->start_with($field, 'safa_trip_going_id')) {

                    $counter = substr($field, 18);

                    $this->erp_port_halls_model->erp_port_id = $_POST['trip_going_airport_start' . $counter];
                    $start_port_hall_rows = $this->erp_port_halls_model->get();
                    if (count($start_port_hall_rows) > 0) {
                        $start_port_hall_id = $start_port_hall_rows[0]->erp_port_hall_id;
                    } else {
                        break;
                    }

                    $this->erp_port_halls_model->erp_port_id = $_POST['trip_going_airport_end' . $counter];
                    $end_port_hall_rows = $this->erp_port_halls_model->get();
                    if (count($end_port_hall_rows) > 0) {
                        $end_port_hall_id = $end_port_hall_rows[0]->erp_port_hall_id;
                    } else {
                        break;
                    }
                    if ($this->input->post('erp_transportertype_id') == '2')
                        if ($_POST[$field] == 0) {
                            $__trip_code = $_POST['trip_going_trip_code' . $counter];
                            $__trip_code = strtoupper(substr($__trip_code, 0, 2)) . trim(substr($__trip_code, 2));
                            //$trip_going_data[]=array(
                            $trip_going_data = array(
                                'safa_trip_id' => $id,
                                'safa_externaltriptype_id' => 1,
                                'trip_code' => $__trip_code,
                                'start_port_hall_id' => $start_port_hall_id,
                                'departure_datetime' => $_POST['trip_going_departure_datetime' . $counter],
                                'end_port_hall_id' => $end_port_hall_id,
                                'arrival_datetime' => $_POST['trip_going_arrival_datetime' . $counter],
                                'safa_transporter_id' => $_POST['trip_going_safa_transporter_id' . $counter],
                                'safa_externalsegmenttype_id' => $_POST['trip_going_safa_externalsegmenttype_id' . $counter],
                                'fs_flight_id' => NULL,
                                    //'sort' => $counter,
                            );
                           $trip_going_id= $this->trip_externalsegments_model->insert($trip_going_data);
                        } else {
                            $trip_going_id = $_POST['safa_trip_going_id' . $counter];
                            $__trip_code = $_POST['trip_going_trip_code' . $counter];
                            $__trip_code = strtoupper(substr($__trip_code, 0, 2)) . trim(substr($__trip_code, 2));
                            $trip_going_data = array(
                                'safa_trip_id' => $id,
                                'safa_externaltriptype_id' => 1,
                                'trip_code' => $__trip_code,
                                'start_port_hall_id' => $start_port_hall_id,
                                'departure_datetime' => $_POST['trip_going_departure_datetime' . $counter],
                                'end_port_hall_id' => $end_port_hall_id,
                                'arrival_datetime' => $_POST['trip_going_arrival_datetime' . $counter],
                                'safa_transporter_id' => $_POST['trip_going_safa_transporter_id' . $counter],
                                'safa_externalsegmenttype_id' => $_POST['trip_going_safa_externalsegmenttype_id' . $counter],
                                'fs_flight_id' => NULL,
                            );
                            $this->trip_externalsegments_model->update($trip_going_id, $trip_going_data);

                            //------------- Delete trip going updated from session will deleted --------
                            $arr_trip_goings = session('trip_goings_session');
                            if ($arr_trip_goings) {
                                foreach ($arr_trip_goings as $key => $value) {
                                    if ($trip_going_id == $key) {
                                        unset($arr_trip_goings[$key]);
                                    }
                                }
                            }
                            session('trip_goings_session', $arr_trip_goings);
                        }
                        
                        // Save In trip availabilities to substract from available
                        if(isset($_POST['trip_going_erp_flight_availabilities_detail_id' . $counter])) {
                        $this->flight_availabilities_model->erp_flight_availabilities_detail_id=$_POST['trip_going_erp_flight_availabilities_detail_id' . $counter];
                        $this->flight_availabilities_model->safa_trip_externalsegment_id=$trip_going_id;
                        $this->flight_availabilities_model->seats_used_count=$_POST['trip_going_required_seats_count' . $counter];
                        $this->flight_availabilities_model->save_trip_externalsegments_availabilities();
                        }
                }
            }
            // Delete trip going 
            $arr_trip_goings = session('trip_goings_session');
            if ($arr_trip_goings) {
                foreach ($arr_trip_goings as $key => $value) {
                    $this->trip_externalsegments_model->delete_by_trip_and_id($id, $value);
                }
            }
            $this->session->unset_userdata('trip_goings_session');

            // Trip return
            $counter = 1;
            $arr_trip_returns_inputs = array();
            foreach ($_POST AS $field => $value) {
                if ($this->start_with($field, 'safa_trip_return_id')) {

                    //if(isset($_POST['trip_return_id_'.$counter])) {

                    $counter = substr($field, 19);

                    $this->erp_port_halls_model->erp_port_id = $_POST['trip_return_airport_start' . $counter];
                    $start_port_hall_rows = $this->erp_port_halls_model->get();
                    if (count($start_port_hall_rows) > 0) {
                        $start_port_hall_id = $start_port_hall_rows[0]->erp_port_hall_id;
                    } else {
                        break;
                    }

                    $this->erp_port_halls_model->erp_port_id = $_POST['trip_return_airport_end' . $counter];
                    $end_port_hall_rows = $this->erp_port_halls_model->get();
                    if (count($end_port_hall_rows) > 0) {
                        $end_port_hall_id = $end_port_hall_rows[0]->erp_port_hall_id;
                    } else {
                        break;
                    }
                    if ($this->input->post('erp_transportertype_id') == '2')
                        if ($_POST[$field] == 0) {
                            //$trip_return_data[]=array(
                            $__trip_code = $_POST['trip_return_trip_code' . $counter];
                            $__trip_code = strtoupper(substr($__trip_code, 0, 2)) . trim(substr($__trip_code, 2));
                            $trip_return_data = array(
                                'safa_trip_id' => $id,
                                'safa_externaltriptype_id' => 2,
                                'trip_code' => $__trip_code,
                                'start_port_hall_id' => $start_port_hall_id,
                                'departure_datetime' => $_POST['trip_return_departure_datetime' . $counter],
                                'end_port_hall_id' => $end_port_hall_id,
                                'arrival_datetime' => $_POST['trip_return_arrival_datetime' . $counter],
                                'safa_transporter_id' => $_POST['trip_return_safa_transporter_id' . $counter],
                                'safa_externalsegmenttype_id' => $_POST['trip_return_safa_externalsegmenttype_id' . $counter],
                                'fs_flight_id' => NULL,
                                    //'sort' => $counter,
                            );
                            $trip_return_id = $this->trip_externalsegments_model->insert($trip_return_data);
                        } else {
                            $__trip_code = $_POST['trip_return_trip_code' . $counter];
                            $__trip_code = strtoupper(substr($__trip_code, 0, 2)) . trim(substr($__trip_code, 2));

                            $trip_return_id = $_POST['safa_trip_return_id' . $counter];
                            $trip_return_data = array(
                                'safa_trip_id' => $id,
                                'safa_externaltriptype_id' => 2,
                                'trip_code' => $__trip_code,
                                'start_port_hall_id' => $start_port_hall_id,
                                'departure_datetime' => $_POST['trip_return_departure_datetime' . $counter],
                                'end_port_hall_id' => $end_port_hall_id,
                                'arrival_datetime' => $_POST['trip_return_arrival_datetime' . $counter],
                                'safa_transporter_id' => $_POST['trip_return_safa_transporter_id' . $counter],
                                'safa_externalsegmenttype_id' => $_POST['trip_return_safa_externalsegmenttype_id' . $counter],
                                'fs_flight_id' => NULL,
                            );
                            $this->trip_externalsegments_model->update($trip_return_id, $trip_return_data);

                            //------------- Delete trip return updated from session will deleted --------
                            $arr_trip_returns = session('trip_returns_session');
                            if ($arr_trip_returns) {
                                foreach ($arr_trip_returns as $key => $value) {
                                    if ($trip_return_id == $key) {
                                        unset($arr_trip_returns[$key]);
                                    }
                                }
                            }
                            session('trip_returns_session', $arr_trip_returns);
                        }
                        
                        // Save In trip availabilities to substract from available
                         if(isset($_POST['trip_return_erp_flight_availabilities_detail_id' . $counter])) {
	                        $this->flight_availabilities_model->erp_flight_availabilities_detail_id=$_POST['trip_return_erp_flight_availabilities_detail_id' . $counter];
	                        $this->flight_availabilities_model->safa_trip_externalsegment_id=$trip_return_id;
	                        $this->flight_availabilities_model->seats_used_count=$_POST['trip_return_required_seats_count' . $counter];
	                        $this->flight_availabilities_model->save_trip_externalsegments_availabilities();
                         }
                }
            }
            //----------- Delete trip return -------------------------------
            $arr_trip_returns = session('trip_returns_session');
            if ($arr_trip_returns) {
                foreach ($arr_trip_returns as $key => $value) {
                    $this->trip_externalsegments_model->delete_by_trip_and_id($id, $value);
                }
            }
            $this->session->unset_userdata('trip_returns_session');

            // Trip hotels
            $counter = 1;
            $arr_trip_hotels_inputs = array();
            foreach ($_POST AS $field => $value) {
                if ($this->start_with($field, 'safa_trip_hotel_id')) {

                    //if(isset($_POST['trip_hotel_id_'.$counter])) {

                    $counter = substr($field, 18);
                    if ($_POST[$field] == 0) {
                        //$trip_hotel_data[]=array(
                        $trip_hotel_data = array(
                            'safa_trip_id' => $id,
                            'nights_count' => $_POST['trip_hotel_nights_count' . $counter],
                            'erp_hotel_id' => $_POST['trip_hotel_erp_hotel_id' . $counter],
                            //'erp_city_id' => $_POST['trip_hotel_erp_city_id'.$counter],  
                            'erp_meal_id' => $_POST['trip_hotel_erp_meal_id' . $counter],
                            'trip_hotel_rooms' => $_POST['trip_hotel_rooms' . $counter],
//                            'even_room_count' => $_POST['trip_hotel_even_room_count' . $counter],
                            'checkin_datetime' => $_POST['trip_hotel_checkin_datetime' . $counter],
                            'checkout_datetime' => $_POST['trip_hotel_checkout_datetime' . $counter],
                                //'sort' => $counter,
                        );
                        if (count($trip_hotel_data)) {
                            $safa_trip_hotel_room_id = $this->trip_hotel_model->insert($trip_hotel_data);

                            // Save Hotel availability rooms details
		                    
                            $this->hotels_availability_room_details_model->erp_hotels_availability_sub_room_id= $_POST['hotels_availability_sub_room_id' . $counter];
                            $hotels_availability_room_details_rows = $this->hotels_availability_room_details_model->get();
                            
                            $this->hotels_availability_room_details_model->safa_trip_hotel_room_id= $safa_trip_hotel_room_id;
		                    
		                    
		                    $this->hotels_availability_room_details_model->from_date= $_POST['trip_hotel_checkin_datetime' . $counter];
		                    $this->hotels_availability_room_details_model->to_date= $_POST['trip_hotel_checkout_datetime' . $counter];
		                    $this->hotels_availability_room_details_model->status= 2;
		                    
		                    $rooms_count = $_POST['trip_hotel_rooms' . $counter];
		                    for($i=0; $i<$rooms_count ;$i++) {
		                    	$this->hotels_availability_room_details_model->number= $hotels_availability_room_details_rows[$i]->number;
		                    	$this->hotels_availability_room_details_model->save();
		                    }
                        }
                    } else {
                        $trip_hotel_id = $_POST['safa_trip_hotel_id' . $counter];
                        $trip_hotel_data = array(
                            'safa_trip_id' => $id,
                            'nights_count' => $_POST['trip_hotel_nights_count' . $counter],
                            'erp_hotel_id' => $_POST['trip_hotel_erp_hotel_id' . $counter],
                            //'erp_city_id' => $_POST['trip_hotel_erp_city_id'.$counter],  
                            'erp_meal_id' => $_POST['trip_hotel_erp_meal_id' . $counter],
                            'trip_hotel_rooms' => $_POST['trip_hotel_rooms' . $counter],
//                            'even_room_count' => $_POST['trip_hotel_even_room_count' . $counter],
                            'checkin_datetime' => $_POST['trip_hotel_checkin_datetime' . $counter],
                            'checkout_datetime' => $_POST['trip_hotel_checkout_datetime' . $counter],
                        );
                        if (count($trip_hotel_data))
                            $this->trip_hotel_model->update($trip_hotel_id, $trip_hotel_data);

                        //------------- Delete trip hotel updated from session will deleted --------
                        $arr_trip_hotels = session('trip_hotels_session');
                        if ($arr_trip_hotels) {
                            foreach ($arr_trip_hotels as $key => $value) {
                                if ($trip_hotel_id == $key) {
                                    unset($arr_trip_hotels[$key]);
                                }
                            }
                        }
                        session('trip_hotels_session', $arr_trip_hotels);
                    }
                }
            }
            //----------- Delete trip hotel -------------------------------
            $arr_trip_hotels = session('trip_hotels_session');
            if ($arr_trip_hotels) {
                foreach ($arr_trip_hotels as $key => $value) {
                    $this->trip_hotel_model->delete_by_trip_and_id($id, $value);
                    
                    // Delete Hotel availability rooms details
                    $this->hotels_availability_room_details_model->safa_trip_hotel_room_id= $value;
                    $this->hotels_availability_room_details_model->delete();
                    
                }
            }
            $this->session->unset_userdata('trip_hotels_session');

            //---------------------------------------------------------
            ///////////////////////////////////////////////////////////////////////////////////////////
            /////////////////////// Trip tourismplace ///////////////////////////////////////////////////////////////
            $counter = 1;
            $arr_trip_tourismplaces_inputs = array();
            foreach ($_POST AS $field => $value) {
                if ($this->start_with($field, 'safa_trip_tourismplace_id')) {
                    $counter = substr($field, 25);
                    if ($_POST[$field] == 0) {
                        //$trip_tourismplace_data[]=array(
                        $trip_tourismplace_data = array(
                            'safa_trip_id' => $id,
//                            'erp_city_id' => $_POST['trip_tourismplace_city_id' . $counter],
                            'safa_tourismplace_id' => $_POST['trip_tourismplace_safa_tourismplace_id' . $counter],
                            'datetime' => $_POST['trip_tourismplace_datetime' . $counter],
                        );
                        $this->trip_tourismplace_model->insert($trip_tourismplace_data);
                    } else {
                        $trip_tourismplace_id = $_POST['safa_trip_tourismplace_id' . $counter];
                        $trip_tourismplace_data = array(
                            'safa_trip_id' => $id,
//                            'erp_city_id' => $_POST['trip_tourismplace_city_id' . $counter],
                            'safa_tourismplace_id' => $_POST['trip_tourismplace_safa_tourismplace_id' . $counter],
                            'datetime' => $_POST['trip_tourismplace_datetime' . $counter],
                        );
                        $this->trip_tourismplace_model->update($trip_tourismplace_id, $trip_tourismplace_data);

                        //------------- Delete trip tourismplace updated from session will deleted --------
                        $arr_trip_tourismplaces = session('trip_tourismplaces_session');
                        if ($arr_trip_tourismplaces) {
                            foreach ($arr_trip_tourismplaces as $key => $value) {
                                if ($trip_tourismplace_id == $key) {
                                    unset($arr_trip_tourismplaces[$key]);
                                }
                            }
                        }
                        session('trip_tourismplaces_session', $arr_trip_tourismplaces);
                        //----------------------------------------------------------------------
                    }
                }
            }
            //----------- Delete trip tourismplace -------------------------------
            $arr_trip_tourismplaces = session('trip_tourismplaces_session');
            if ($arr_trip_tourismplaces) {
                foreach ($arr_trip_tourismplaces as $key => $value) {
                    $this->trip_tourismplace_model->delete_by_trip_and_id($id, $value);
                }
            }
            $this->session->unset_userdata('trip_tourismplaces_session');
            
            
            
            
            /////////////////////// Trip Uo Requests ///////////////////////////////////////////////////////////////
            $counter = 1;
            $arr_safa_trips_requests_inputs = array();
            foreach ($_POST AS $field => $value) {
                if ($this->start_with($field, 'safa_trips_requests_safa_trips_requests_id')) {
                    $counter = substr($field, 42);
                    if ($_POST[$field] == 0) {
                        $trips_requests_data = array(
                            'safa_trip_id' => $id,
                            'safa_uo_service_id' => $_POST['safa_trips_requests_safa_uo_service_id' . $counter],
                            'remarks' => $_POST['safa_trips_requests_remarks' . $counter],
                        );
                        $this->safa_trips_requests_model->insert($trips_requests_data);
                    } else {
                        $safa_trips_requests_id = $_POST['safa_trips_requests_safa_trips_requests_id' . $counter];
                        $trips_requests_data = array(
                            'safa_trip_id' => $id,
                            'safa_uo_service_id' => $_POST['safa_trips_requests_safa_uo_service_id' . $counter],
                            'remarks' => $_POST['safa_trips_requests_remarks' . $counter],
                        );
                        $this->safa_trips_requests_model->update($safa_trips_requests_id, $trips_requests_data);

                        //------------- Delete trip tourismplace updated from session will deleted --------
                        $arr_safa_trips_requests = session('safa_trips_requests_session');
                        if ($arr_safa_trips_requests) {
                            foreach ($arr_safa_trips_requests as $key => $value) {
                                if ($safa_trips_requests_id == $key) {
                                    unset($arr_safa_trips_requests[$key]);
                                }
                            }
                        }
                        session('safa_trips_requests_session', $arr_safa_trips_requests);
                        //----------------------------------------------------------------------
                    }
                }
            }
            //----------- Delete trip tourismplace -------------------------------
            $arr_safa_trips_requests = session('safa_trips_requests_session');
            if ($arr_safa_trips_requests) {
                foreach ($arr_safa_trips_requests as $key => $value) {
                    $this->safa_trips_requests_model->delete_by_trip_and_id($id, $value);
                }
            }
            $this->session->unset_userdata('safa_trips_requests_session');
            
           
            
            
            redirect("ea/trips/finish/" . $id);
        }
    }

    public function finish($id = FALSE) {
        if (!$id)
            show_404();
        $this->trips_model->safa_ea_id = session("ea_id");
        $this->trips_model->safa_trip_id = $id;
        $item = $this->trips_model->get();
        if (!$item)
            show_404();
        $this->load->view('ea/trips/redirect_message',
                array(
            'msg' => lang('global_added_message'),
            'url' => site_url('ea/trips'),
            'item' => $item,
            'back' => site_url('ea/trips/form/' . $id)
        ));
    }

    public function confirm($id, $confirm = 0) {
        $this->trips_model->safa_trip_id = $id;

        $this->trips_model->safa_trip_confirm_id = $confirm;

        $id = $this->trips_model->save();

        redirect(base_url() . 'ea/trips');
    }

    function get_hotels_by_city($city_id = false, $safa_uo_contract_id = FALSE) {
        $this->layout = 'ajax';
        
        //By Gouda
        //$con = $this->db->where('safa_uo_contract_ea_id', $counter)->get('safa_uo_contracts_eas')->row();

        
        $drpdwn = "<option value=''></option>";

        $rows = $this->hotels_model->get_by_city($city_id, $safa_uo_contract_id);
        foreach ($rows as $record) {
            $drpdwn = $drpdwn . "<option value='" . $record->erp_hotel_id . "' >" . $record->{name()} . "</option>";
        }
//        $drpdwn = $drpdwn . "</select>";

        echo $drpdwn;
    }

    function get_tourismplaces_by_city($city_id = FALSE, $counter = FALSE) {
        $this->layout = 'ajax';
        $drpdwn = "<select name='trip_tourismplace_safa_tourismplace_id$counter' id='trip_tourismplace_safa_tourismplace_id$counter' class='validate[required]'>";
        $drpdwn = $drpdwn . "<option value=''></option>";

        $rows = $this->tourismplaces_model->get_by_city($city_id);
        foreach ($rows as $record) {
            $drpdwn = $drpdwn . "<option value='" . $record->safa_tourismplace_id . "' >" . $record->name_ar . "</option>";
        }
        $drpdwn = $drpdwn . "</select>";

        echo $drpdwn;
    }

    public function start_with($s, $prefix) {
        return strpos($s, $prefix) === 0;
    }

    // AJAX CALLS
    public function get_ea_packages($safa_uo_contract_ea_id = 0, $selected=0) 
    {
        $this->layout = 'ajax';
//        echo "<option value=''>" . lang('global_select_from_menu') . '</option>';
        
        $this->db->select('safa_uo_packages.name_ar, safa_uo_packages.name_la, safa_ea_packages.safa_ea_package_id ');
        $this->db->from('safa_uo_packages');
        $this->db->join('safa_ea_packages', 'safa_uo_packages.safa_uo_package_id = safa_ea_packages.safa_uo_package_id');
        $this->db->join('safa_uo_contracts_eas', 'safa_uo_contracts_eas.safa_uo_contract_ea_id = safa_ea_packages.safa_uo_contract_ea_id');
        $this->db->where('safa_uo_contracts_eas.safa_uo_contract_ea_id', $safa_uo_contract_ea_id);
        
        $packages = $this->db->get()->result();
        
        //echo $query_last= $this->db->last_query(); exit;
        
        echo '<option value="">' . lang('global_select_from_menu') . '</option>';
        
        foreach ($packages as $row) {
			if ($row->safa_ea_package_id == $selected) {
				echo '<option value="' . $row->safa_ea_package_id . '" selected="selected">' . $row->{name()} . '</option>';
			} else {
				echo '<option value="' . $row->safa_ea_package_id . '">' . $row->{name()} . '</option>';
			}
		}
        
    }
    
    public function get_package_id_by_ea_package($safa_ea_package_id=0)
    {
    	$this->ea_packages_model->safa_ea_package_id = $safa_ea_package_id;
    	$ea_package_row = $this->ea_packages_model->get();
    	if(count($ea_package_row)>0) {
    		echo $ea_package_row->safa_uo_package_id;
    	}
    	exit;
    }

    public function get_saudi_ports($cc = FALSE) {
        $this->layout = 'ajax';
        $this->db->distinct();
        if ($cc)
            $this->db->where('erp_ports.country_code', $cc);
        $this->db->select('erp_ports.*');
        $this->db->where('safa_ea_ports.safa_ea_id', session('ea_id'));
        $this->db->group_by('erp_ports.erp_port_id');
        $this->db->join('erp_port_halls', 'erp_port_halls.erp_port_id = erp_ports.erp_port_id');
        $this->db->join('safa_ea_ports', 'safa_ea_ports.erp_port_id = erp_ports.erp_port_id');
        $ports = $this->db->get('erp_ports')->result();
        foreach ($ports as $row) {
            echo '<option value="' . $row->erp_port_id . '">' . $row->code . '</option>';
        }
    }

    private function generate_internaltrip($trip_id = FALSE, $ito_id = FALSE) {
        $this->load->helper('itconf');
        if (!$ito_id)
            $ito_id = $this->input->post('ito_id');
        // CHECK IF THERE'S INTERNAL TRIP EXISTS
        // TRIP
        $this->trips_model->safa_ea_id = session('ea_id');
        $this->trips_model->safa_trip_id = $trip_id;
        $trip = $this->trips_model->get();
        if (!$trip)
            show_404();
//        $trip = $this->db->where('safa_trip_id', $trip_id)->get('safa_trips')->row();

        $trip_travellers = $this->db->where('date_of_birth', date('Y-m-d', strtotime('-2 years')))
                ->where('safa_trip_id', $trip_id)
                ->join('safa_group_passports', 'safa_group_passport_id')
                ->get('safa_trip_travellers')
                ->num_rows();
        if (!$trip_travellers)
            $trip_travellers = $trip->travellers_adult_count + $trip->travellers_child_count;

        // GET GOING EXTERNAL SEGMENTS
        # ARRIVAL
        $arrival = $this->db->where('safa_trip_id', $trip_id)->where('safa_externalsegmenttype_id', 1)->where('safa_externaltriptype_id',
                        1)->get('safa_trip_externalsegments')->row();

        # DEPARTURE
        $departure = $this->db->where('safa_trip_id', $trip_id)->where('safa_externalsegmenttype_id', 2)->where('safa_externaltriptype_id',
                        2)->get('safa_trip_externalsegments')->row();

        // GET HOTELS
        $hotels = $this->db->order_by('checkin_datetime', 'ASC')->where('safa_trip_id', $trip_id)->get('safa_trip_hotels')->result();

        # CHECK-IN
        $checkin_hotel = $this->db->order_by('checkin_datetime', 'ASC')->where('safa_trip_id', $trip_id)->get('safa_trip_hotels')->row();

        # CHECK-OUT
        $checkout_hotel = $this->db->order_by('checkout_datetime', 'DESC')->where('safa_trip_id', $trip_id)->get('safa_trip_hotels')->row();

        // TOURISM PLACES
        $tourism_places = $this->db->order_by('datetime', 'ASC')->where('safa_trip_id', $trip_id)->get('safa_trip_tourismplaces')->result();

        if ($ito_id)
            $safa_internaltripstatus_id = 2;
        else
            $safa_internaltripstatus_id = 1;

        $trip_internaltrip = array(
            'safa_trip_id' => $trip_id,
            'safa_ito_id' => $ito_id,
            'safa_transporter_id' => NULL,
            'safa_internaltripstatus_id' => $safa_internaltripstatus_id,
            'safa_tripstatus_id' => NULL,
            'operator_reference' => NULL,
            'attachement' => NULL,
            'datetime' => $trip->date,
            'confirmation_number' => gen_itconf()
        );

        $this->db->insert('safa_trip_internaltrips', $trip_internaltrip);
        $internal_trip_id = $this->db->insert_id();

        for ($i = 0; $i <= count($hotels) - 1; $i++) {
            $hotel = $hotels[$i];
            if ($checkin_hotel && $checkin_hotel->safa_trip_hotel_id == $hotel->safa_trip_hotel_id) {
                // GET ARRIVAL PORT ID
                $arrival_port = $this->db->where('erp_port_hall_id', $arrival->end_port_hall_id)->get('erp_port_halls')->row();
                // ARRIVAL SEGMENT 1
                $this->db->insert('safa_internalsegments',
                        array(
                    'safa_internalsegmenttype_id' => 1,
                    'safa_trip_internaltrip_id' => $internal_trip_id,
                    'safa_internalsegmentestatus_id' => 1,
                    'start_datetime' => $arrival->arrival_datetime,
                    'end_datetime' => $hotel->checkin_datetime,
                    'erp_port_id' => $arrival_port->erp_port_id,
                    'erp_start_hotel_id' => NULL,
                    'erp_end_hotel_id' => $checkin_hotel->erp_hotel_id,
                    'safa_tourism_place_id' => NULL,
                    'safa_uo_user_id' => NULL,
                    'seats_count' => $trip_travellers,
                ));
            } elseif ($checkout_hotel && $checkout_hotel->safa_trip_hotel_id == $hotel->safa_trip_hotel_id) {
                // GET DEPARTURE PORT ID
                $departure_port = $this->db->where('erp_port_hall_id', $departure->start_port_hall_id)->get('erp_port_halls')->row();
                // DEPARTURE SEGMENT 2
                $this->db->insert('safa_internalsegments',
                        array(
                    'safa_internalsegmenttype_id' => 2,
                    'safa_trip_internaltrip_id' => $internal_trip_id,
                    'safa_internalsegmentestatus_id' => 1,
                    'start_datetime' => $hotel->checkout_datetime,
                    'end_datetime' => $departure->departure_datetime,
                    'erp_port_id' => $departure_port->erp_port_id,
                    'erp_start_hotel_id' => $checkout_hotel->erp_hotel_id,
                    'erp_end_hotel_id' => NULL,
                    'safa_tourism_place_id' => NULL,
                    'safa_uo_user_id' => NULL,
                    'seats_count' => $trip_travellers,
                ));
            } //else {
            //}
        }


        for ($i = 0; $i <= count($hotels) - 1; $i++) {
            if (!isset($hotels[$i + 1]))
                break;
            $hotel = $hotels[$i];
            // MOVING SEGMENTS 4
            $this->db->insert('safa_internalsegments',
                    array(
                'safa_internalsegmenttype_id' => 4,
                'safa_trip_internaltrip_id' => $internal_trip_id,
                'safa_internalsegmentestatus_id' => 1,
                'start_datetime' => $hotel->checkout_datetime,
                'end_datetime' => $hotels[$i + 1]->checkin_datetime,
                'erp_port_id' => NULL,
                'erp_start_hotel_id' => $hotel->erp_hotel_id,
                'erp_end_hotel_id' => $hotels[$i + 1]->erp_hotel_id,
                'safa_tourism_place_id' => NULL,
                'safa_uo_user_id' => NULL,
                'seats_count' => $trip_travellers,
            ));
        }
        // TOURISM PLACES 3
        foreach ($tourism_places as $place) {
            $hotel = $this->db->where('checkin_datetime <=', $place->datetime)->where('checkout_datetime >=',
                            $place->datetime)->get('safa_trip_hotels', 1)->row();
            if ($hotel)
                $this->db->insert('safa_internalsegments',
                        array(
                    'safa_internalsegmenttype_id' => 3,
                    'safa_trip_internaltrip_id' => $internal_trip_id,
                    'safa_internalsegmentestatus_id' => 1,
                    'start_datetime' => $place->datetime,
                    'end_datetime' => $place->datetime,
                    'erp_port_id' => NULL,
                    'erp_start_hotel_id' => $hotel->erp_hotel_id,
                    'erp_end_hotel_id' => NULL,
                    'safa_tourism_place_id' => $place->safa_tourismplace_id,
                    'safa_uo_user_id' => NULL,
                    'seats_count' => $trip_travellers,
                ));
        }
    }

    public function generate_tit($trip_id = FALSE) {
        if (!$trip_id)
            show_404();
        $this->trips_model->safa_ea_id = session('ea_id');
        $this->trips_model->safa_trip_id = $trip_id;
        $data['item'] = $this->trips_model->get();
        if (!$data['item'])
            show_404();
        if ($data['item']->internal_trips != 0)
            show_404();

        $this->layout = 'js';
        $data['itos'][''] = lang('global_select_from_menu');
        $ITO = $this->db->select('safa_itos.safa_ito_id, safa_itos.' . name())
                ->where('safa_uo_contracts_eas.safa_ea_id', session('ea_id'))
                ->join('safa_ea_itos', 'safa_ea_itos.safa_ito_id = safa_itos.safa_ito_id')
                ->join('safa_uo_contracts', 'safa_ea_itos.safa_uo_contract_id = safa_uo_contracts.safa_uo_contract_id')
                ->join('safa_uo_contracts_eas',
                        'safa_uo_contracts.safa_uo_contract_id = safa_uo_contracts_eas.safa_uo_contract_id')
                ->get('safa_itos')
                ->result();
        foreach ($ITO as $ito)
            $data['itos'][$ito->safa_ito_id] = $ito->{name()};
        $this->load->library('form_validation');
        $this->form_validation->set_rules('ito_id', 'lang:ito_id', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $this->load->view('ea/trips/trip_internaltrip', $data);
        } else {
            $this->generate_internaltrip($trip_id, $this->input->post('ito_id'));
            echo "<script>parent.$.fancybox.close(); parent.Shadowbox.close();</script>";
        }
    }

    public function autocomplete_es($trip_code = FALSE, $arrival_date = FALSE) {
        $this->layout = 'none';
        if ($this->input->post('trip_code'))
            $trip_code = $this->input->post('trip_code');
        if ($this->input->post('arrival_date'))
            $arrival_date = $this->input->post('arrival_date');
        $airline = substr($trip_code, 0, 2);
        $flight_code = substr($trip_code, 2);

        $flight = $this->db->select('fs_flights.flight, fs_flights.airline, fs_resources.*, fs_states.arrivalAirportFsCode, fs_states.departureAirportFsCode, fs_states.scheduledGateArrival, fs_states.scheduledGateDeparture')
                ->where('fs_flights.flight', $flight_code)
                ->where('fs_flights.airline', $airline)
                ->where('fs_resources.departureTerminal IS NOT NULL', FALSE, FALSE)
                ->where('fs_resources.arrivalTerminal IS NOT NULL', FALSE, FALSE)
                ->join(FSDB . '.fs_states', 'fs_states.fs_flight_id = fs_flights.fs_flight_id')
                ->join(FSDB . '.fs_resources', 'fs_resources.fs_flight_id = fs_flights.fs_flight_id')
                ->order_by('fs_flights.fs_flight_id', 'DESC')
                ->get(FSDB . '.fs_flights', 1)
                ->row();
//                   print_r($flight);
//        if (!$flight)
//            return;
        $airline_code = $this->db->where('code', $airline)->get('safa_transporters')->row();
        if ($airline_code)
            $res['airline'] = $airline_code->safa_transporter_id;
        if ($arrival_date && $flight) {
            $time_differ = abs(strtotime($flight->scheduledGateArrival) - strtotime($flight->scheduledGateDeparture));
            $res['arrival_datetime'] = date('Y-m-d h:i', strtotime($arrival_date) + $time_differ);
        }
        // TERMINALS
        // DEPARTURE TERMINAL trip_code
        if ($flight && $flight->departureTerminal && $flight->departureAirportFsCode) {
            $erp_port_hall = $this->db->where('erp_port_halls.code', $flight->departureTerminal)
                    ->where('erp_ports.code', $flight->departureAirportFsCode)
                    ->join('erp_ports', 'erp_ports.erp_port_id = erp_port_halls.erp_port_id')
                    ->get('erp_port_halls')
                    ->row();
            $res['start_port_id'] = $erp_port_hall->erp_port_id;
//             if($erp_port_hall)
//             {
//                 $res['start_port_hall_id'] = $erp_port_hall->erp_port_hall_id;
//             }
//             else
//             {
//                 $erp_port = $this->db->where('erp_ports.code', $flight->departureAirportFsCode)
//                          ->get('erp_ports')
//                          ->row();
//
//                 $this->db->set('erp_port_halls.code', $flight->departureTerminal);
//                 $this->db->set('erp_port_halls.name_ar', $flight->departureTerminal);
//                 $this->db->set('erp_port_halls.name_la', $flight->departureTerminal);
//                 $this->db->set('erp_port_halls.erp_port_id', $erp_port->erp_port_id);
//                 $this->db->insert('erp_port_halls');
//                 $id = $this->db->insert_id();
//                 $res['start_port_hall_id'] = $id;
//             }
        }

        // ARRIVAL TERMINAL
        if ($flight && $flight->arrivalTerminal && $flight->arrivalAirportFsCode) {
            $erp_port_halls = $this->db->where('erp_port_halls.code', $flight->arrivalTerminal)
                    ->where('erp_ports.code', $flight->arrivalAirportFsCode)
                    ->join('erp_ports', 'erp_ports.erp_port_id = erp_port_halls.erp_port_id')
                    ->get('erp_port_halls')
                    ->row();
            $res['end_port_id'] = $erp_port_halls->erp_port_id;
//           if($erp_port_halls)
//           {
//               $res['end_port_hall_id'] = $erp_port_halls->erp_port_hall_id;
//           }
//           else
//           {
//               $erp_port = $this->db->where('erp_ports.code', $flight->arrivalAirportFsCode)
//                        ->get('erp_ports')
//                        ->row();
//
//               $this->db->set('erp_port_halls.code', $flight->arrivalTerminal);
//               $this->db->set('erp_port_halls.name_ar', $flight->arrivalTerminal);
//               $this->db->set('erp_port_halls.name_la', $flight->arrivalTerminal);
//               $this->db->set('erp_port_halls.erp_port_id', $erp_port->erp_port_id);
//               $this->db->insert('erp_port_halls');
//               $id = $this->db->insert_id();
//               $res['end_port_hall_id'] = $id;
//           }
        }
        header('Content-Type: application/json');
        echo json_encode($res);
    }
    
	function create_contracts_array() 
	{
		$this->load->model('trip_internaltrip_model');
		$this->trip_internaltrip_model->ea_id = session('ea_id');
        $result = $this->trip_internaltrip_model->get_ea_contracts();
        $contracts = array();
        $contracts[""] = lang('global_select_from_menu');
        //$name = name();
        foreach ($result as $con_id => $contract) {
           $contracts[$contract->safa_uo_contract_ea_id] = $contract->safa_uo_contracts_eas_name;
        }
        return $contracts;
    }
    
    function get_uo_services_by_contract_for_ajax_table($safa_uo_contract_id)
    {
    	$uo_services_arr=array();
    	$this->contracts_settings_model->safa_uo_contract_id=$safa_uo_contract_id;
    	$contracts_settings_row = $this->contracts_settings_model->get();
    	if(count($contracts_settings_row)>0) {
    		if($contracts_settings_row->services_buy_uo_visa==1) {
    			$uo_services_arr[]= 3;
    		}
    		if($contracts_settings_row->services_buy_uo_hotels==1) {
    			$uo_services_arr[]= 1;
    		}
    		if($contracts_settings_row->services_buy_uo_transporter==1) {
    			$uo_services_arr[]= 2;
    		}
    	}
    	echo json_encode($uo_services_arr);
    	exit;
    }
    
	function get_hotels_by_package_for_ajax_table($safa_ea_package_id=0)
    {
    	$safa_uo_package_id=0;
    	$this->ea_packages_model->safa_ea_package_id = $safa_ea_package_id;
    	$ea_package_row = $this->ea_packages_model->get();
    	if(count($ea_package_row)>0) {
    		$safa_uo_package_id =  $ea_package_row->safa_uo_package_id;
    	}
    	
    	//---------- Special Package ------------------------
    	/*
    	$this->safa_uo_packages_model->safa_uo_package_id= $safa_uo_package_id;
    	$package_row = $this->safa_uo_packages_model->get();
    	if(count($package_row)>0) {
    		if($package_row->private==1) {
    			echo 0;
    			exit;
    		}
    	}
    	*/
    	if($safa_uo_package_id==1) {
    			echo 0;
    			exit;
    	}
    	//----------------------------------------------------
    	
    	$this->uo_package_hotels_model->safa_uo_package_id=$safa_uo_package_id;
    	$package_hotels_rows = $this->uo_package_hotels_model->get();
    	
    	echo json_encode($package_hotels_rows);
    	exit;
    }
	function get_tourismplaces_by_package_for_ajax_table($safa_ea_package_id=0)
    {
    	$safa_uo_package_id=0;
    	$this->ea_packages_model->safa_ea_package_id = $safa_ea_package_id;
    	$ea_package_row = $this->ea_packages_model->get();
    	if(count($ea_package_row)>0) {
    		$safa_uo_package_id =  $ea_package_row->safa_uo_package_id;
    	}
    	
    	//---------- Special Package ------------------------
    	/*
    	$this->safa_uo_packages_model->safa_uo_package_id= $safa_uo_package_id;
    	$package_row = $this->safa_uo_packages_model->get();
    	if(count($package_row)>0) {
    		if($package_row->private==1) {
    			echo 0;
    			exit;
    		}
    	}
    	*/
    	if($safa_uo_package_id==1) {
    			echo 0;
    			exit;
    	}
    	//----------------------------------------------------
    	
    	$this->uo_package_tourismplaces_model->safa_uo_package_id=$safa_uo_package_id;
    	$package_tourismplaces_rows = $this->uo_package_tourismplaces_model->get();
    	
    	echo json_encode($package_tourismplaces_rows);
    	exit;
    }
    
    function get_private_package_id_by_ea_contract_for_ajax_table($safa_uo_contract_ea_id)
    {
    	$current_ea_contract_private_ea_package_id=0;
        $this->ea_packages_model->safa_uo_contract_ea_id = $safa_uo_contract_ea_id;
        $current_ea_package_rows = $this->ea_packages_model->get();
        if(count($current_ea_package_rows)>0) {
        	foreach ($current_ea_package_rows as $current_ea_package_row) {
        		if($current_ea_package_row->safa_uo_package_id==1) {
        			$current_ea_contract_private_ea_package_id = $current_ea_package_row->safa_ea_package_id;
        		}
        	}
        }
        echo $current_ea_contract_private_ea_package_id;
        exit;
    }
    
    function are_package_changed($safa_ea_package_id, $id)
    {
    	$safa_uo_package_id=0;
    	$this->ea_packages_model->safa_ea_package_id = $safa_ea_package_id;
    	$ea_package_row = $this->ea_packages_model->get();
    	if(count($ea_package_row)>0) {
    		$safa_uo_package_id =  $ea_package_row->safa_uo_package_id;
    	}
    	if($safa_uo_package_id==1) {
	    	echo 0;
	    	exit;
    	}
    	
    	//------ Package --------------
    	$this->uo_package_hotels_model->safa_uo_package_id=$safa_uo_package_id;
    	$package_hotels_rows = $this->uo_package_hotels_model->get();
    	$this->uo_package_tourismplaces_model->safa_uo_package_id=$safa_uo_package_id;
    	$package_tourismplaces_rows = $this->uo_package_tourismplaces_model->get();
    	
    	//------ Trip --------------
    	$trip_hotel_rows = $this->trip_hotel_model->get_trip_hotels_by_trip_id($id);
        $trip_tourismplace_rows = $this->trip_tourismplace_model->get_trip_tourismplaces_by_trip_id($id);

        
        if(count($package_hotels_rows) != count($trip_hotel_rows)) {
        	echo 1;
	    	exit;
        }
    	if(count($package_tourismplaces_rows) != count($trip_tourismplace_rows)) {
        	echo 1;
	    	exit;
        }
    	
        //------ Hotels --------------------
        $hotel_changed=false;
        foreach ($trip_hotel_rows as $trip_hotel_row) {
        	$hotel_exist=0;
        	foreach ($package_hotels_rows as $package_hotels_row) {
	        	if($trip_hotel_row->erp_hotel_id == $package_hotels_row->erp_hotel_id) {
	        		if($trip_hotel_row->erp_meal_id == $package_hotels_row->erp_meal_id) {
	        			$hotel_exist=1;
	        		}
	        	}
    		}
    		
    		if($hotel_exist==0) {
				echo 1;
	    		exit;
			} 
        }
        
        //------ Tourismplaces --------------------
        $tourismplace_changed=false;
    	foreach ($trip_tourismplace_rows as $trip_tourismplace_row) {
    		$tourismplace_exist=0;
    		foreach ($package_tourismplaces_rows as $package_tourismplaces_row) {
	        	if($trip_tourismplace_row->safa_tourismplace_id == $package_tourismplaces_row->safa_tourismplace_id) {
	        		$tourismplace_exist=1;
	        	}
    		}
			if($tourismplace_exist==0) {
				echo 1;
	    		exit;
			}   		
        }
        
        
    	echo 0;
    	exit;
    }
    
    function send_to_uo_popup($id)
    {
    	$this->layout = 'js_new';
       
		$data['trip_id'] = $id;
		
		//$this->contracts_model->get();
		$uo_services_arr_after_filter=array();
		$uo_services_arr_before_filter = ddgen("safa_uo_services", array("safa_uo_service_id", name()));
		foreach($uo_services_arr_before_filter as $key=>$value) {
			//if($key==1) {
				$uo_services_arr_after_filter[$key]=$value;
			//}
		}
		$data["safa_uo_services"] = $uo_services_arr_after_filter;
        
		$this->trips_model->safa_ea_id = session('ea_id');
        $this->trips_model->safa_trip_id = $id;
        $trip_row = $this->trips_model->get();
            
        $safa_trips_requests_rows = $this->safa_trips_requests_model->get_by_trip_id($id);
        $data['safa_trips_requests_rows'] = $safa_trips_requests_rows;
        $data['safa_trips_requests_count'] = count($safa_trips_requests_rows);
        
        if (!isset($_POST['smt_send'])) {
        	$this->load->view('ea/trips/send_to_uo_popup', $data);
        } else {
        	
        	/////////////////////// Trip Uo Requests ///////////////////////////////////////////////////////////////
            $counter = 1;
            $arr_safa_trips_requests_inputs = array();
            foreach ($_POST AS $field => $value) {
                if ($this->start_with($field, 'safa_trips_requests_safa_trips_requests_id')) {
                    $counter = substr($field, 42);
                    if ($_POST[$field] == 0) {
                        $trips_requests_data = array(
                            'safa_trip_id' => $id,
                            'safa_uo_service_id' => $_POST['safa_trips_requests_safa_uo_service_id' . $counter],
                            'remarks' => $_POST['safa_trips_requests_remarks' . $counter],
                        );
                        $this->safa_trips_requests_model->insert($trips_requests_data);
                    } else {
                        $safa_trips_requests_id = $_POST['safa_trips_requests_safa_trips_requests_id' . $counter];
                        $trips_requests_data = array(
                            'safa_trip_id' => $id,
                            'safa_uo_service_id' => $_POST['safa_trips_requests_safa_uo_service_id' . $counter],
                            'remarks' => $_POST['safa_trips_requests_remarks' . $counter],
                        );
                        $this->safa_trips_requests_model->update($safa_trips_requests_id, $trips_requests_data);

                        //------------- Delete trip tourismplace updated from session will deleted --------
                        $arr_safa_trips_requests = session('safa_trips_requests_session');
                        if ($arr_safa_trips_requests) {
                            foreach ($arr_safa_trips_requests as $key => $value) {
                                if ($safa_trips_requests_id == $key) {
                                    unset($arr_safa_trips_requests[$key]);
                                }
                            }
                        }
                        session('safa_trips_requests_session', $arr_safa_trips_requests);
                        //----------------------------------------------------------------------
                    }
                }
            }
            //----------- Delete trip tourismplace -------------------------------
            $arr_safa_trips_requests = session('safa_trips_requests_session');
            if ($arr_safa_trips_requests) {
                foreach ($arr_safa_trips_requests as $key => $value) {
                    $this->safa_trips_requests_model->delete_by_trip_and_id($id, $value);
                }
            }
            $this->session->unset_userdata('safa_trips_requests_session');
            
        	
            //Update status & confirmation of trip
            $this->trips_model->safa_trip_id = $id;
            $this->trips_model->safa_tripstatus_id = 3;
            $this->trips_model->safa_trip_confirm_id = 1;
            $this->trips_model->save();
            
            
            
            //----------- Send Notification For Company ------------------------------
            $msg_datetime = date('Y-m-d H:i:s', time());

            $this->notification_model->notification_type = 'automatic';
            $this->notification_model->erp_importance_id = 1;
            $this->notification_model->sender_type_id = 1;
            $this->notification_model->sender_id = 0;

            $this->notification_model->erp_system_events_id = 23;

            $this->notification_model->language_id = 2;
            $this->notification_model->msg_datetime = $msg_datetime;
           
            $tags='';
            if(count($trip_row)>0) {
            	
            	//Link to replace.
	        	$link_trip ="<a href='".site_url('ea/trips/form/'.$trip_row->safa_trip_id)."'  target='_blank'> ".$trip_row->safa_trip_id." </a>";
	        	$link_trip_name ="<a href='".site_url('ea/trips/form/'.$trip_row->safa_trip_id)."'  target='_blank'> ".$trip_row->name." </a>";
	        
            	$tags = "#*trip_no#*=" . $link_trip . "*****#*trip#*=" . $link_trip_name . "*****#*date#*=" . $trip_row->date . "*****#*trip_date#*=".$trip_row->date;
            }
            $this->notification_model->tags = $tags;
            $this->notification_model->parent_id = '';

            $erp_notification_id = $this->notification_model->save();

            //-------- Notification Details ---------------
            $this->notification_model->detail_erp_notification_id = $erp_notification_id;

            $this->notification_model->detail_receiver_type = 2;
            
            if(count($trip_row)>0) {
            	$tri_safa_ea_package_id = $trip_row->safa_ea_package_id;
            	$uo_row = $this->uos_model->get_by_safa_ea_package_id($tri_safa_ea_package_id);
            	
            	$this->notification_model->detail_receiver_id = $uo_row->safa_uo_id;
            }
            
            $this->notification_model->saveDetails();
            //--------------------------------
            //-------------------------------------------------------------------
            
            
            
        	$this->load->view('redirect_message_popup', array('msg' => lang('send_to_uo_message'),
            'url' => site_url("ea/trips/send_to_uo_popup"),
            'id' => $this->uri->segment("4"),
            'model_title' => lang('title'), 'action' => lang('send_to_uo')));
        }	
    }
    
}

/* End of file trips.php */
/* Location: ./application/controllers/trips.php */
