<?php

class Safa_groups extends Safa_Controller {

    public $module = "safa_groups";

    public function __construct() {
        parent::__construct();
        $this->layout = 'new';
        //Side menu session, By Gouda.
        session('side_menu_id', 5);
        $this->load->model('safa_groups_model');
        $this->lang->load('safa_groups');
        $this->lang->load('group_passports');
        permission();
        if (session('ea_id'))
            $this->safa_groups_model->ea_id = session('ea_id');
    }

    public function index() {

        if (isset($_GET['search'])) {
            if ($this->input->get("mutamer_name") || $this->input->get("passport_no") || $this->input->get("nationality_id") || $this->input->get("age_from") || $this->input->get("age_to") || $this->input->get("relative_no")) {

                $mutamer_name = $this->input->get("mutamer_name");
                $passport_no = $this->input->get("passport_no");
                $nationality_id = $this->input->get("nationality_id");
                $age_from = $this->input->get("age_from");
                $age_to = $this->input->get("age_to");

                $relative_no = $this->input->get("relative_no");
                $safa_group_id = $this->input->get("safa_group_id");

                redirect("ea/group_passports/passports_index/0?search=&mutamer_name=$mutamer_name&passport_no=$passport_no&nationality_id=$nationality_id&age_from=$age_from&age_to=$age_to&relative_no=$relative_no&safa_group_id=$safa_group_id");
            } else {
                $this->search();
            }
        }
        $this->safa_groups_model->join = TRUE;
        $data["total_rows"] = $this->safa_groups_model->get(true);
        $this->safa_groups_model->offset = $this->uri->segment("4");
        $this->safa_groups_model->limit = $this->config->item('per_page');
        $data["items"] = $this->safa_groups_model->get();
        $config['uri_segment'] = 4;
        $config['base_url'] = site_url('ea/safa_groups/index');
        $config['total_rows'] = $data["total_rows"];
        $config['per_page'] = $this->safa_groups_model->limit;
        $config['suffix'] = $this->input->get() ? '?' . http_build_query($_GET) : NULL;
        $this->load->library('pagination');
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();
        $this->load->view('ea/safa_groups/index', $data);
    }

    //Added By Gouda, To select Trip travellers
    public function popup() {
        $this->layout = 'js';

        if (isset($_GET['search'])) {
            if ($this->input->get("mutamer_name") || $this->input->get("passport_no") || $this->input->get("nationality_id") || $this->input->get("age_from") || $this->input->get("age_to") || $this->input->get("relative_no")) {

                $mutamer_name = $this->input->get("mutamer_name");
                $passport_no = $this->input->get("passport_no");
                $nationality_id = $this->input->get("nationality_id");
                $age_from = $this->input->get("age_from");
                $age_to = $this->input->get("age_to");

                $relative_no = $this->input->get("relative_no");
                $safa_group_id = $this->input->get("safa_group_id");

                redirect("ea/group_passports/popup/0?search=&mutamer_name=$mutamer_name&passport_no=$passport_no&nationality_id=$nationality_id&age_from=$age_from&age_to=$age_to&relative_no=$relative_no&safa_group_id=$safa_group_id");
            } else {
                $this->search();
            }
        }
        $this->safa_groups_model->join = TRUE;
        $data["total_rows"] = $this->safa_groups_model->get(true);
        $this->safa_groups_model->offset = $this->uri->segment("4");
        $this->safa_groups_model->limit = $this->config->item('per_page');
        $data["items"] = $this->safa_groups_model->get();
        $config['uri_segment'] = 4;
        $config['base_url'] = site_url('ea/safa_groups/popup');
        $config['total_rows'] = $data["total_rows"];
        $config['per_page'] = $this->safa_groups_model->limit;
        $config['suffix'] = $this->input->get() ? '?' . http_build_query($_GET) : NULL;
        $this->load->library('pagination');
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();
        $this->load->view('ea/safa_groups/popup', $data);
    }

    public function add() {

        $this->load->library("form_validation");
        $this->form_validation->set_rules('name', 'lang:safa_groups_name', 'trim|required|unique_col[safa_groups.name]');
        $this->form_validation->set_rules('creation_date', 'lang:safa_groups_creation_date', 'trim|required');



        if ($this->form_validation->run() == false) {
            $this->load->view("ea/safa_groups/add");
        } else {
            $this->safa_groups_model->name = $this->input->post('name');
            $this->safa_groups_model->creation_date = $this->input->post('creation_date');
            $this->safa_groups_model->safa_ea_id = session('ea_id');
            $this->safa_groups_model->save();
            $this->load->view('redirect_message', array('msg' => lang('global_added_message'),
                'url' => site_url('ea/safa_groups'),
                'model_title' => lang('safa_groups_title'), 'action' => lang('safa_groups_add_title')));
        }
    }

    public function edit($id = FALSE) {
        if (!$id)
            show_404();
        $this->safa_groups_model->safa_group_id = $id;
        $data['item'] = $this->safa_groups_model->get();

        if (!$data['item'])
            show_404();
        $this->load->library("form_validation");
        $this->form_validation->set_rules('name', 'lang:safa_groups_name', 'trim|required|callback_check_name');
        $this->form_validation->set_rules('creation_date', 'lang:safa_groups_creation_date', 'trim|required');


        if ($this->form_validation->run() == false) {
            $this->load->view("ea/safa_groups/edit", $data);
        } else {
            $this->safa_groups_model->name = $this->input->post('name');
            $this->safa_groups_model->creation_date = $this->input->post('creation_date');
            $this->safa_groups_model->safa_ea_id = session('ea_id');
            $this->safa_groups_model->save();
            $this->load->view('redirect_message', array('msg' => lang('global_updated_message'),
                'url' => site_url('ea/safa_groups'),
                'id' => $id,
                'model_title' => lang('safa_groups_title'), 'action' => lang('safa_groups_edit_title')));
        }
    }

    function delete($id = FALSE) {
        $this->layout = 'ajax';
        if (!$this->input->is_ajax_request()) {
            redirect('ea/safa_groups/index');
        } else {
            $safa_group_id = $this->input->post('safa_group_id');
            $data = $this->safa_groups_model->check_delete_ability($safa_group_id);

            if ($data == 0) {
                $this->safa_groups_model->safa_group_id = $safa_group_id;
                if ($this->safa_groups_model->delete())
                    echo json_encode(array('response' => TRUE, 'msg' => 'deleted'));
            }
            else {
                echo json_encode(array('response' => FALSE, 'msg' => 'not deleted'));
            }
        }
    }

    function search() {

        if ($this->input->get("from_creation_date"))
            $this->safa_groups_model->from_creation_date = $this->input->get("from_creation_date");

        if ($this->input->get("to_creation_date"))
            $this->safa_groups_model->to_creation_date = $this->input->get("to_creation_date");

        if ($this->input->get("safa_group_id")) {

            $this->safa_groups_model->safa_group_id = $this->input->get("safa_group_id");
            $safa_groups_row = $this->safa_groups_model->get();
            $this->safa_groups_model->safa_group_id = false;

            $this->safa_groups_model->name = $safa_groups_row->name;
        }


        if ($this->input->get("safa_groups_no_of_mutamers_from")) {
            $this->safa_groups_model->no_of_mutamers_from = $this->input->get("safa_groups_no_of_mutamers_from");
        }
        if ($this->input->get("safa_groups_no_of_mutamers_to")) {
            $this->safa_groups_model->no_of_mutamers_to = $this->input->get("safa_groups_no_of_mutamers_to");
        }

        // By Gouda
        /*
          if ($this->input->get("mutamer_name")){
          $first_space=stripos ($this->input->get("mutamer_name")," ");
          $second_space=strrpos ($this->input->get("mutamer_name")," ");
          echo $first_name_la=substr($this->input->get("mutamer_name"),0,$first_space).'<br>';
          //       echo $second_name_la=substr($this->input->get("mutamer_name"),$first_space+1,3);
          echo $third_name_la=substr( $this->input->get("mutamer_name"),$second_space);
          exit();
          $this->safa_groups_model->first_name_la = $first_name_la;
          //            $this->safa_groups_model->second_name_la = $this->input->get("second_name_la");
          $this->safa_groups_model->third_name_la = $third_name_la;
          }

          if ($this->input->get("passport_no"))
          $this->safa_groups_model->passport_no = $this->input->get("passport_no");
         */
    }

    function check_name($name) {
        if ($this->uri->segment('3') == 'edit')
            $this->db->where('safa_group_id !=', $this->uri->segment('4'));
        $this->db->where('name', $name);
        if ($this->db->get('safa_groups')->num_rows()) {
            $this->form_validation->set_message('check_name_ar', lang('safa_groups_name_exist'));
            return FALSE;
        } else {
            return TRUE;
        }
    }

    function get_passport_no($passport_no) {
        $this->layout = 'ajax';
        if (!empty($passport_no) || isset($passport_no)) {
            $this->db->select('passport_no');
            $this->db->from('safa_group_passports');
            $this->db->like('passport_no', $passport_no);
            echo json_encode($this->db->get()->result());
        }
    }

    function mutamer_name($mutamer_name) {
        $this->layout = 'ajax';

        if (!empty($mutamer_name) || isset($mutamer_name)) {
            $mutamer_upper = strtoupper($mutamer_name);
            $mutamer_lower = strtolower($mutamer_name);
//            $this->db->select('passport_no'); 
//        $this->db->from('safa_group_passports');
            $this->db->like('first_name_la', $mutamer_upper);
            $this->db->like('first_name_la', $mutamer_lower);
//          $this->db->like('first_name_ar',$mutamer_name);
            echo json_encode($this->db->get('safa_group_passports')->result());
        }
    }

}

/* End of file safa_groups.php */
/* Location: ./application/controllers/admin/port_halls.php */


