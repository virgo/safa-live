<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Ea_users extends Safa_Controller {

    public $module = "ea_users";

    public function __construct() {
        parent::__construct();
        permission();
        $this->layout = 'new';
        //Side menu session, By Gouda.
        session('side_menu_id', '0');
        $this->load->model('ea_users_model');
        $this->lang->load('safa_ea_users');
    }

    public function index() {

        if (isset($_GET['search']))
            $this->search();

        #to get external agent user that entered (only)  
        $this->ea_users_model->safa_ea_id = session('ea_id');
        ######
        $data["total_rows"] = $this->ea_users_model->search(true);
        $this->ea_users_model->offset = $this->uri->segment("4");
        //$this->ea_users_model->limit = $this->config->item('per_page');
        $data["items"] = $this->ea_users_model->search();
        $config['uri_segment'] = 4;
        $config['base_url'] = site_url('ea/ea_users/index');
        $config['total_rows'] = $data["total_rows"];
        $config['per_page'] = $this->ea_users_model->limit;
        $config['suffix'] = $this->input->get() ? '?' . http_build_query($_GET) : NULL;
        $this->load->library('pagination');
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();
        $this->load->view('ea/ea_users/index', $data);
    }

    public function add() {


        $this->load->library("form_validation");
        $this->form_validation->set_rules('name_ar', 'lang:ea_users_name_ar', 'trim');
        $this->form_validation->set_rules('name_la', 'lang:ea_users_name_la', 'trim');

        if (name() == 'name_la')
            $this->form_validation->set_rules('name_la', 'lang:ea_users_name_la', 'trim|required');
        if (name() == 'name_ar')
            $this->form_validation->set_rules('name_ar', 'lang:ea_users_name_ar', 'trim|required');

        $this->form_validation->set_rules('username', 'lang:ea_users_username', 'trim|required|alpha_dash|unique_ea_user[safa_ea_users.username]|callback_check_username_availability');
        $this->form_validation->set_rules('email', 'lang:ea_users_email', 'trim|required|valid_email|callback_check_email_user_availability');

        $this->form_validation->set_rules('password', 'lang:ea_users_password', 'trim|required');
        $this->form_validation->set_rules('passconf', 'lang:ea_users_confirm_password', 'trim|required|matches[password]');
        $this->form_validation->set_rules('safa_ea_usergroup_id', 'lang:ea_users_safa_ea_usergroup_id', 'trim|required');

        if ($this->form_validation->run() == false) {
            $this->load->view("ea/ea_users/add");
        } else {

            $this->ea_users_model->safa_ea_id = session('ea_id');
            $this->ea_users_model->name_ar = $this->input->post('name_ar');
            $this->ea_users_model->name_la = $this->input->post('name_la');
            $this->ea_users_model->username = $this->input->post('username');
            $this->ea_users_model->email = $this->input->post('email');
            if ($this->input->post('password'))
                $this->ea_users_model->password = md5($this->input->post('password'));
            $this->ea_users_model->safa_ea_usergroup_id = $this->input->post('safa_ea_usergroup_id');
            $this->ea_users_model->save();
            $this->load->view('redirect_message', array('msg' => lang('global_added_message'),
                'url' => site_url('ea/ea_users/index'),
                'model_title' => lang('menu_external_agent_Users'), 'action' => lang('users_add')));
        }
    }

    public function edit($id) {

        if (!$id)
            show_404();
        #To prevent showing data of other users using url id 
        $this->ea_users_model->safa_ea_id = session('ea_id');
        ##########
        $this->ea_users_model->safa_ea_user_id = $id;
        $data['item'] = $this->ea_users_model->get();

        if (!$data['item'])
            show_404();

        $this->load->library("form_validation");
        $this->form_validation->set_rules('name_ar', 'lang:ea_users_name_ar', 'trim');
        $this->form_validation->set_rules('name_la', 'lang:ea_users_name_la', 'trim');

        if (name() == 'name_la')
            $this->form_validation->set_rules('name_la', 'lang:ea_users_name_la', 'trim|required');
        if (name() == 'name_ar')
            $this->form_validation->set_rules('name_ar', 'lang:ea_users_name_ar', 'trim|required');

        $this->form_validation->set_rules('username', 'lang:ea_users_username', 'trim|required|alpha_dash|unique_ea_user[safa_ea_users.username.' . $id . ']|callback_check_username_availability');
        $this->form_validation->set_rules('email', 'lang:ea_users_email', 'trim|required|valid_email|callback_check_email_user_availability');

        $this->form_validation->set_rules('password', 'lang:ea_users_password', 'trim');
        $this->form_validation->set_rules('passconf', 'lang:ea_users_confirm_password', 'trim|matches[password]');
        $this->form_validation->set_rules('safa_ea_usergroup_id', 'lang:ea_users_safa_ea_usergroup_id', 'trim|required');


        if ($this->form_validation->run() == false) {
            $this->load->view("ea/ea_users/edit", $data);
        } else {
            $this->ea_users_model->safa_ea_user_id = $id;
            $this->ea_users_model->name_ar = $this->input->post('name_ar');
            $this->ea_users_model->name_la = $this->input->post('name_la');
            $this->ea_users_model->username = $this->input->post('username');
            $this->ea_users_model->email = $this->input->post('email');
            if ($this->input->post('password'))
                $this->ea_users_model->password = md5($this->input->post('password'));
            $this->ea_users_model->safa_ea_usergroup_id = $this->input->post('safa_ea_usergroup_id');
            $this->ea_users_model->save();
            $this->load->view('redirect_message', array('msg' => lang('global_updated_message'),
                'url' => site_url('ea/ea_users'),
                'id' => $id,
                'model_title' => lang('menu_external_agent_Users'), 'action' => lang('users_edit')));
        }
    }

    function delete($id = false) {
        if (!$id)
            show_404();
        $this->ea_users_model->safa_ea_user_id = $id;
        $this->ea_users_model->delete();
        redirect('ea/ea_users/index');
    }

    function check_email_user_availability($email_value) {
        $id = 0;
        if ($this->input->post('safa_ea_user_id')) {
            $id = $this->input->post('safa_ea_user_id');
        }
        $this->form_validation->set_message('check_email_user_availability', 'This Email Already Registered In Our Database');
        $this->db->select('count(*) as rows_count')->from('safa_eas')->join('safa_ea_users', 'safa_ea_users.safa_ea_id=safa_eas.safa_ea_id')
                ->where('safa_ea_users.email', $email_value)->where("safa_ea_users.safa_ea_user_id!=$id");
        $rows_count = $this->db->get()->row()->rows_count;
        //echo $this->db->last_query(); exit;
        if ($rows_count == 0)
            return TRUE;
        else
            return false;
    }

    function check_username_availability($username_value) {
        $id = 0;
        if ($this->input->post('safa_ea_user_id')) {
            $id = $this->input->post('safa_ea_user_id');
        }
        $this->form_validation->set_message('check_username_availability', 'This Username Already Registered In Our Database');
        $this->db->select('count(*) as rows_count')->from('safa_eas')->join('safa_ea_users', 'safa_ea_users.safa_ea_id=safa_eas.safa_ea_id')
                ->where('safa_ea_users.username', $username_value)->where("safa_ea_users.safa_ea_user_id!=$id");
        $rows_count = $this->db->get()->row()->rows_count;
        if ($rows_count == 0)
            return TRUE;
        else
            return false;
    }

}

/* End of file ea_users.php */
/* Location: ./application/controllers/ea_users.php */