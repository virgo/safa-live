<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class User_personal_settings extends Safa_Controller {

    public function __construct() {
        parent::__construct();

        $this->layout = 'new';

        $this->load->model('ea_user_personal_settings_model');
        $this->lang->load('admins/user_personal_settings');
        if (!session('ea_id'))
            permission();
    }

    public function index() {
        $this->ea_user_personal_settings_model->safa_ea_user_id = session('ea_user_id');
        //echo $this->user_personal_settings_model->id;
        $this->ea_user_personal_settings_model->limit = $this->config->item('per_page');
        $this->ea_user_personal_settings_model->offset = $this->uri->segment('4');
        $config['uri_segment'] = 4;
        $config['base_url'] = site_url('ea/user_personal_settings/index');
        $config['total_rows'] = $this->db->get('safa_ea_users')->num_rows();
        $config['per_page'] = $this->config->item('per_page');
        $this->load->library('pagination');
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();
        $data['items'] = $this->ea_user_personal_settings_model->get();
        $this->load->view('ea/user_personal_settings/index', $data);
    }

    public function edit($id = FALSE) {

        $id = $this->ea_user_personal_settings_model->safa_ea_user_id = session('ea_user_id');

        if (!$id)
            show_404();
        $data['item'] = $this->ea_user_personal_settings_model->get();
        if (!$data['item'])
            if (!$id)
                show_404();

        $data['item'] = $this->ea_user_personal_settings_model->get();
        if (!$data['item'])
            show_404();
        $this->load->library('form_validation');

        $this->form_validation->set_rules('new_password', 'lang:user_personal_settings_new_password', 'trim|matches[passconf]|required');
        $this->form_validation->set_rules('passconf', 'lang:user_personal_settings_resetpassword', 'required');
        $this->form_validation->set_rules('check_user_info', '', 'callback_check_user_info');

        if ($this->form_validation->run() == FALSE) {
            $this->load->view('ea/user_personal_settings/edit', $data);
        } else {

            #new_password from view
            $password = $this->input->post('new_password');

            if ($password != "") {

                $this->ea_user_personal_settings_model->password = md5($password);
            }

            $this->ea_user_personal_settings_model->safa_ea_user_id = $id;
            $this->ea_user_personal_settings_model->save();

//            $this->db->like('user_data', 's:10:"ea_user_id";s:' . strlen($id) .':"' . $id . '";', 'both')
//                     ->like('user_data', 's:5:"ea_id";s:' . strlen($data['item']->safa_ea_id) .':"' . $data['item']->safa_ea_id . '"', 'both')
//                     ->delete('sessions');
            $this->load->view('admin/redirect', array('msg' => lang('global_updated_successfully'), 'url' => site_url('ea/logout')));
        }
    }

    function check_user_info() {

        $this->form_validation->set_message('check_user_info', lang('global_info_not_true'));

        $username = $this->input->post('username');
        $password = md5($this->input->post('password'));

        $this->db->where('username', $username);
        $this->db->where('password', $password);
        $this->db->where("safa_ea_users.safa_ea_user_id", session("ea_user_id"));
        $query = $this->db->get('safa_ea_users');
//          print_r($password);

        if ($query->num_rows > 0) {
            return true;
        } else {
            return false;
        }
    }

    function valid_pass($password) {

        $this->form_validation->set_message('valid_pass', lang('password_error'));

        if ($password) {
            $r2 = '/[a-z]/';
            $r4 = '/[0-9]/';

            if (preg_match_all($r2, $password, $o) < 2)
                return FALSE;

            if (preg_match_all($r4, $password, $o) < 2)
                return FALSE;

            if (strlen($password) < 8)
                return FALSE;
        }
        return TRUE;
    }

    function check_username($username) {

        $this->db->where('id !=', session('user_id'));
        $user = $this->db->where('username', $username)->get('users')->num_rows();
        if ($user) {

            $this->form_validation->set_message('check_username', lang('global_username_exists'));
            return FALSE;
        } else {
            return TRUE;
        }
    }

}

/* End of file User_personal_settings.php */
/* Location: ./application/controllers/admin/User_personal_settings.php */