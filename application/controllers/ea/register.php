<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * @property CI_Loader $load
 * @property CI_Form_validation $form_validation
 * @property CI_Input $input
 * @property CI_Email $email
 * @property CI_DB_active_record $db
 * @property CI_DB_forge $dbforge
 * @property CI_Table $table
 * @property CI_Session $session
 * @property CI_FTP $ftp
 * ....
 */
class Register extends Safa_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('ea_users_model');
        $this->load->model('eas_model');
        $this->load->model('contracts_model');

        $this->lang->load('login');
        $this->load->helper('cookie');
        $this->layout = 'ajax';
    }

    // By Gouda.
    function index() {
        $this->load->library('form_validation');

        $this->form_validation->set_rules('contract_username', 'lang:contract_username', 'required|trim');
        $this->form_validation->set_rules('contract_password', 'lang:contract_password', 'required|trim|min_length[5]');

        $this->form_validation->set_rules('name_ar', 'lang:name_ar', 'required|trim');
        $this->form_validation->set_rules('name_la', 'lang:name_la', 'required|trim');
        $this->form_validation->set_rules('country', 'lang:country', 'required|trim');
        $this->form_validation->set_rules('phone', 'lang:phone', 'required|trim');
        $this->form_validation->set_rules('fax', 'lang:fax', 'required|trim');

        $this->form_validation->set_rules('username', 'lang:username', 'required|trim|callback_check_username_availability');
        $this->form_validation->set_rules('rpassword', 'lang:password', 'required|trim');
        $this->form_validation->set_rules('confpassword', 'lang:repeat_password', 'trim|required|matches[contract_password]');

        $this->form_validation->set_rules('email', 'lang:global_forget_password_enter_email', 'required|trim|valid_email|callback_check_email_user_availability');
        if ($this->form_validation->run() == FALSE) {
            $this->load->view('login/ea');
        } else {

            $this->ea_users_model->username = $this->input->post("username");
            $ea_users_rows_count = $this->ea_users_model->get(true);

            $this->contracts_model->contract_username = $this->input->post('contract_username');
            $this->contracts_model->contract_password = md5($this->input->post('contract_password'));
            $contracts_rows_count = $this->contracts_model->get(true);

            //By Gouda.
            $this->layout = 'js';

            if ($ea_users_rows_count > 0) {
                $this->load->view('error_message', array('msg' => lang('username_not_available'),
                    'url' => site_url('ea'),
                    'model_title' => lang('node_title'), 'action' => lang('registeration')));
            } else if ($contracts_rows_count == 0) {
                $this->load->view('error_message', array('msg' => lang('contract_not_available'),
                    'url' => site_url('ea'),
                    'model_title' => lang('node_title'), 'action' => lang('registeration')));
            } else {
                $ea_id = $this->add_externalagents();
                if ($ea_id > 0) {
                    $this->add_ea_user($ea_id);
                    $this->add_ea_contract($ea_id);

                    /*
                      $ea_contract_id = $this->add_ea_contract($contract_id, $ea_id);
                      $season_id = $this->add_ea_seasons($ea_id);
                      if ($season_id) {
                      $package_id = $this->add_ea_package($ea_contract_id, $season_id);
                      }
                     */
                }

                $this->load->view('redirect_message', array('msg' => lang('global_added_message'),
                    'url' => site_url('ea'),
                    'model_title' => lang('node_title'), 'action' => lang('registeration')));
            }
        }
    }

    function add_externalagents() {

        $this->eas_model->name_ar = $this->input->post("name_ar");
        $this->eas_model->name_la = $this->input->post("name_la");
        $this->eas_model->erp_country_id = $this->input->post("country");
        $this->eas_model->phone = $this->input->post("phone");
        $this->eas_model->email = $this->input->post("email");
        $this->eas_model->fax = $this->input->post("fax");
        $eas_id = $this->eas_model->save();
        return $eas_id;
    }

    function add_ea_user($ea_id) {
        $this->ea_users_model->safa_ea_id = $ea_id;
        $this->ea_users_model->name_ar = $this->input->post("name_ar");
        $this->ea_users_model->name_la = $this->input->post("name_la");
        $this->ea_users_model->email = $this->input->post("email");
        $this->ea_users_model->erp_language_id = 2;
        $this->ea_users_model->safa_ea_usergroup_id = 1;
        $this->ea_users_model->username = $this->input->post("username");
        if ($this->input->post("rpassword")) {
            $this->ea_users_model->password = md5($this->input->post("rpassword"));
        }
        $this->ea_users_model->active = 1;
        $ea_user_id = $this->ea_users_model->save();

        return $ea_user_id;
    }

    function add_ea_contract($ea_id) {
        $this->load->model('contracts_model');

        $username = $this->input->post('contract_username');

        $password = $this->input->post('contract_password');
        $password = md5($password);

        $this->contracts_model->contract_username = $username;
        $this->contracts_model->contract_password = $password;

        $contracts_rows = $this->contracts_model->search();

        if (count($contracts_rows) > 0) {
            $this->contracts_model->safa_uo_contract_id = $contracts_rows[0]->safa_uo_contract_id;

            $total_contracts_rows = $this->contracts_model->get_contract_ea(true);

            $this->contracts_model->by_eas_id = $ea_id;

            $total_ea_contracts_rows = $this->contracts_model->get_contract_ea(true);
            $ea_contracts_rows = $this->contracts_model->search();
            if ($total_ea_contracts_rows > 0) {
                /*

                 */

                if ($ea_contracts_rows[0]->safa_uo_contract_ea_id != '') {
                    $this->contracts_model->uo_contracts_eas_safa_uo_contract_ea_id = $ea_contracts_rows[0]->safa_uo_contract_ea_id;
                    $this->contracts_model->uo_contracts_eas_disabled = 0;

                    if ($total_contracts_rows == 0) {
                        $this->contracts_model->uo_contracts_eas_is_original_owner = 1;
                    }

                    $this->contracts_model->uo_contracts_eas_name_ar = $contracts_rows[0]->safa_uos_name . ' - ' . $contracts_rows[0]->name_ar;
                    $this->contracts_model->uo_contracts_eas_name_la = $contracts_rows[0]->safa_uos_name . ' - ' . $contracts_rows[0]->name_la;

                    $this->contracts_model->save_contract_ea();
                }
            } else {
                $this->contracts_model->uo_contracts_eas_disabled = 0;

                if ($total_contracts_rows == 0) {
                    $this->contracts_model->uo_contracts_eas_is_original_owner = 1;
                }

                $this->contracts_model->uo_contracts_eas_name_ar = $contracts_rows[0]->safa_uos_name . ' - ' . $contracts_rows[0]->name_ar;
                $this->contracts_model->uo_contracts_eas_name_la = $contracts_rows[0]->safa_uos_name . ' - ' . $contracts_rows[0]->name_la;

                $this->contracts_model->save_contract_ea();
            }
        }
    }

    function check_email_user_availability($email_value) {
        $this->form_validation->set_message('check_email_user_availability', 'This Email Already Registered In Our Database');
        $this->db->select('count(*) as rows_count')->from('safa_eas')->join('safa_ea_users', 'safa_ea_users.safa_ea_id=safa_eas.safa_ea_id')
                ->where('safa_ea_users.email', $email_value);
        $rows_count = $this->db->get()->row()->rows_count;
        if ($rows_count == 0)
            return TRUE;
        else
            return false;
    }

    function check_username_availability($username_value) {
        $this->form_validation->set_message('check_username_availability', 'This Username Already Registered In Our Database');
        $this->db->select('count(*) as rows_count')->from('safa_eas')->join('safa_ea_users', 'safa_ea_users.safa_ea_id=safa_eas.safa_ea_id')
                ->where('safa_ea_users.username', $username_value);
        $rows_count = $this->db->get()->row()->rows_count;
        if ($rows_count == 0)
            return TRUE;
        else
            return false;
    }

    function check_username_availability_ajax() {
        $this->ea_users_model->username = $this->input->post("username");
        $ea_users_rows_count = $this->ea_users_model->get(true);
        if ($ea_users_rows_count > 0) {
            echo"<input type='hidden' id='hdn_username_not_available_error' vlaue='1' />" . lang('username_not_available');
        } else {
            echo "<font color='green'>" . lang('username_available') . "</font>";
        }
        exit;
    }

    function set_language($language_id) {
        session(array(
            'language' => $language_id,
        ));

        $this->input->set_cookie('ea_language', $language_id, time() + strtotime("+1 year"));

        $ref = $this->input->server('HTTP_REFERER', TRUE);
        redirect($ref, 'location');
    }

}

/* End of file login.php */
/* Location: ./application/controllers/admin/login.php */