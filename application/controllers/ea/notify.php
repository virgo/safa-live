<?php

class notify extends Safa_Controller {

    public $module = "ea_no";

    public function __construct() {

        parent::__construct();

        //$this->layout='new';
        $this->load->model('notification_model');
        $this->load->model('erp_languages_model');

        permission();
    }

    public function index() {

        $data['notifications'] = $this->notification_model->getByReceiverTypeAndReceiverId('3', session('ea_id'));
        $this->load->view('ea/notify_view', $data);
    }

    public function get($user_type, $user_id) {
        $this->layout = 'ajax';
        $notifications = $this->notification_model->getByReceiverTypeAndReceiverId($user_type, $user_id);
        //echo(count($notifications));        

        echo("<div msgs='" . count($notifications) . "' id='ul_user_msg' >");
        foreach ($notifications as $notification) {

            $erp_system_events_id = $notification->erp_system_events_id;
            $language_id = $notification->language_id;
            $msg_datetime = $notification->msg_datetime;

            $tags = $notification->tags;
            $tags_arr = explode('*****', $tags);


            $notification_data = $this->getNotificationDataFromXml($language_id, $erp_system_events_id);
            $notification_data_arr = explode('*****', $notification_data);

            $notification_subject = '';
            $notification_body = '';

            if (isset($notification_data_arr[0])) {
                $notification_subject = $notification_data_arr[0];
                foreach ($tags_arr as $tag) {
                    $tag_arr = explode('=', $tag);
                    if (isset($tag_arr[0]) && isset($tag_arr[1])) {
                        $notification_subject = str_replace($tag_arr[0], $tag_arr[1], $notification_subject);
                    }
                }

                if (isset($notification_data_arr[1])) {
                    $notification_body = $notification_data_arr[1];
                    foreach ($tags_arr as $tag) {
                        $tag_arr = explode('=', $tag);
                        if (isset($tag_arr[0]) && isset($tag_arr[1])) {
                            $notification_body = str_replace($tag_arr[0], $tag_arr[1], $notification_body);
                        }
                    }
                }
            }

            echo("
            <div class='cls_notification_message_dv'>
            <b>$notification_subject</b>
            <br/>
            $notification_body
            </div>
            ");
        }
        echo("</div>");


        echo("<script>");
        foreach ($notifications as $notification) {
            //echo("alertify.success('$notification->sender_type_id', 5000);");
        }
        echo("</script>");
    }

    function getNotificationDataFromXml($language_id, $event_key) {
        $this->erp_languages_model->erp_language_id = $language_id;
        $erp_languages_row = $this->erp_languages_model->get();

        $lang_suffix = $erp_languages_row->suffix;

        if (file_exists("./static/xml/notifications/notifications_$lang_suffix.xml")) {
            $xml = new DOMDocument('1.0', 'utf-8');
            /**
             * Added to format the xml code, when open with editor.
             */
            $xml->formatOutput = true;
            $xml->preserveWhiteSpace = false;

            $xml->Load("./static/xml/notifications/notifications_$lang_suffix.xml");
            /**
             * 
             * Check if this event inserted before
             */
            $event_key_exist = false;
            $notifications = $xml->getElementsByTagName('notification');
            foreach ($notifications as $notification) {
                $loop_current_key = $notification->getElementsByTagName('key');
                $loop_current_key_value = $loop_current_key->item(0)->nodeValue;

                if ($loop_current_key_value == $event_key) {
                    $event_key_exist = true;

                    $loop_current_subject = $notification->getElementsByTagName('subject');
                    $loop_current_subject_value = $loop_current_subject->item(0)->nodeValue;

                    $loop_current_body = $notification->getElementsByTagName('body');
                    $loop_current_body_value = $loop_current_body->item(0)->nodeValue;

                    /*
                      if(name()=='name_ar') {
                      $loop_current_name= $notification->getElementsByTagName('name_ar');
                      $loop_current_name_value= $loop_current_name->item(0)->nodeValue;
                      } else if(name()=='name_la') {
                      $loop_current_name= $notification->getElementsByTagName('name_la');
                      $loop_current_name_value= $loop_current_name->item(0)->nodeValue;
                      }
                     */

                    return $loop_current_subject_value . '*****' . $loop_current_body_value;
                }
            }

            if (!$event_key_exist) {
                return '';
            }

            //------------------------------------------------------------------
        } else {
            return '';
        }
    }

}
