<?php

class Group_passports extends Safa_Controller {

	public $module = "group_passports";

	public function __construct() {
		parent::__construct();
		$this->layout = 'new';
		//Side menu session, By Gouda.
		session('side_menu_id', 5);
		$this->load->model('countries_model');
		$this->load->helper('cookie');
		$this->load->model('safa_group_passports_model');
		$this->load->model('safa_groups_model');
		$this->load->model('safa_umrahgroups_model');
		$this->load->model('safa_packages_model');
		$this->load->model('contracts_model');
		$this->load->model('safa_package_periods_model');
		$this->load->model('package_hotels_model');
		$this->load->model('flight_availabilities_model');
		$this->load->model('safa_group_passports_flight_availabilties_model');



		$this->lang->load('group_passports');

		permission();
		if (session('ea_id'))
		$this->safa_group_passports_model->ea_id = session('ea_id');
	}

	public function index() {

		$ed_id = session('ea_id');

		//By Gouda
		$ea_user_id = session('ea_user_id');

		if (isset($_GET['search'])) {
			$this->search();
			$data["items"] = $this->safa_group_passports_model->get_for_table();
		}

		$this->safa_group_passports_model->join = TRUE;
		//        $data["total_rows"] = $this->safa_group_passports_model->get(true);
		//        $this->safa_group_passports_model->offset = $this->uri->segment("4");
		//        $this->safa_group_passports_model->limit = $this->config->item('per_page');
		//        $data["items"] = $this->safa_group_passports_model->get();
		//        $config['uri_segment'] = 4;
		//        $config['base_url'] = site_url('ea/group_passports/index');
		//        $config['total_rows'] = $data["total_rows"];
		//        $config['per_page'] = $this->safa_group_passports_model->limit;
		//        $config['suffix'] = $this->input->get() ? '?' . http_build_query($_GET) : NULL;
		//        $this->load->library('pagination');
		//        $this->pagination->initialize($config);
		//        $data['pagination'] = $this->pagination->create_links();
		$cols = array('group_id', 'display_order', 'no', 'passport_no', 'passport_type_id', 'name_ar', 'name_la', 'erp_country_id', 'nationality_id', 'date_of_birth', 'age', 'passport_issue_date',
            'passport_expiry_date', 'passport_issuing_country_id', 'passport_issuing_city', 'passport_dpn_count', 'relative_relation_id',
            'occupation', 'city', 'marital_status_id', 'relative_no', 'dpn_serial_no', 'educational_level_id', 'birth_country_id', 'birth_city', 'relative_gender_id', 'name');
		$colums = array();
		foreach ($cols as $col) {

			if ($col == 'group_id') {
				if ($this->input->get("safa_group_id")) {
					continue;
				}
			} else if ($col == 'display_order') {
				if (!$this->input->get("safa_group_id")) {
					continue;
				}
			} else if ($col == 'relative_gender_id') {
				continue;
			}


			if ($this->input->cookie('' . $ea_user_id . '_' . $col . ''))
			$colums[] = $this->input->cookie('' . $ea_user_id . '_' . $col . '');
		}

		if (!empty($colums) && count($colums) >= 1 && is_array($colums))
		$data['cols'] = $colums;
		else
		$data['cols'] = $cols;
		$data['safa_group_id'] = FALSE;
		$this->load->view('ea/group_passports/index', $data);
	}

	public function passports_index($safa_group_id = FALSE) {
		//print_r($_COOKIE); exit;
		$ed_id = session('ea_id');

		//By Gouda
		$ea_user_id = session('ea_user_id');


		if (isset($_GET['search'])) {
			//By Gouda
			//$data['safa_umrahgroup'] = 'safa_umrahgroup';
			$this->search();
		}

		if ($safa_group_id != 0 && $safa_group_id != false) {
			$this->safa_group_passports_model->safa_group_id = $safa_group_id;
		}
		//        $data["total_rows"] = $this->safa_group_passports_model->get(true);
		//        $this->safa_group_passports_model->offset = $this->uri->segment("5");
		//        $this->safa_group_passports_model->limit = $this->config->item('per_page');
		$data["items"] = $this->safa_group_passports_model->get_for_table();
		//        $config['uri_segment'] = 5;
		//        $config['base_url'] = site_url('ea/group_passports/passports_index/' . $safa_group_id . '');
		//        $config['total_rows'] = $data["total_rows"];
		//        $config['per_page'] = $this->safa_group_passports_model->limit;
		//        $config['suffix'] = $this->input->get() ? '?' . http_build_query($_GET) : NULL;
		//        $this->load->library('pagination');
		//        $this->pagination->initialize($config);
		//        $data['pagination'] = $this->pagination->create_links();
		$cols = array('group_id', 'display_order', 'no', 'passport_no', 'passport_type_id', 'name_ar', 'name_la', 'erp_country_id', 'nationality_id', 'date_of_birth', 'age', 'passport_issue_date',
            'passport_expiry_date', 'passport_issuing_country_id', 'passport_issuing_city', 'passport_dpn_count', 'relative_relation_id',
            'occupation', 'city', 'marital_status_id', 'relative_no', 'dpn_serial_no', 'educational_level_id', 'birth_country_id', 'birth_city', 'relative_gender_id', 'name');
		$colums = array();
		foreach ($cols as $col) {
			if ($col == 'group_id') {
				if ($this->input->get("safa_group_id") || ($safa_group_id != 0 && $safa_group_id != false)) {
					continue;
				}
			} else if ($col == 'display_order') {
				if (!$this->input->get("safa_group_id")) {
					continue;
				}
			} else if ($col == 'relative_gender_id') {
				continue;
			}


			if ($this->input->cookie('' . $ea_user_id . '_' . $col . '')) {
				$colums[] = $this->input->cookie('' . $ea_user_id . '_' . $col . '');
			}
		}

		if (!empty($colums) && count($colums) >= 1 && is_array($colums)) {
			$data['cols'] = $colums;
		} else {
			if ($this->input->get("safa_group_id")) {
				$data['cols'] = array('display_order', 'passport_no', 'name_la', 'no', 'name_ar');
			} else {
				$data['cols'] = array('group_id', 'passport_no', 'name_la', 'no', 'name_ar');
			}
		}
		$data['safa_group_id'] = $safa_group_id;


		$this->load->view('ea/group_passports/index', $data);
	}

	//Added By Gouda, To select Trip travellers
	public function popup($safa_group_id = FALSE) {
		$this->layout = 'js';

		//print_r($_COOKIE); exit;
		$ed_id = session('ea_id');

		//By Gouda
		$ea_user_id = session('ea_user_id');


		if (isset($_GET['search'])) {
			//By Gouda
			//$data['safa_umrahgroup'] = 'safa_umrahgroup';
			$this->search();
		}

		if ($safa_group_id != 0 && $safa_group_id != false) {
			$this->safa_group_passports_model->safa_group_id = $safa_group_id;
		}
		//        $data["total_rows"] = $this->safa_group_passports_model->get(true);
		//        $this->safa_group_passports_model->offset = $this->uri->segment("5");
		//        $this->safa_group_passports_model->limit = $this->config->item('per_page');
		$data["items"] = $this->safa_group_passports_model->get_for_table();
		//        $config['uri_segment'] = 5;
		//        $config['base_url'] = site_url('ea/group_passports/passports_index/' . $safa_group_id . '');
		//        $config['total_rows'] = $data["total_rows"];
		//        $config['per_page'] = $this->safa_group_passports_model->limit;
		//        $config['suffix'] = $this->input->get() ? '?' . http_build_query($_GET) : NULL;
		//        $this->load->library('pagination');
		//        $this->pagination->initialize($config);
		//        $data['pagination'] = $this->pagination->create_links();
		$cols = array('group_id', 'display_order', 'no', 'passport_no', 'passport_type_id', 'name_ar', 'name_la', 'erp_country_id', 'nationality_id', 'date_of_birth', 'age', 'passport_issue_date',
            'passport_expiry_date', 'passport_issuing_country_id', 'passport_issuing_city', 'passport_dpn_count', 'relative_relation_id',
            'occupation', 'city', 'marital_status_id', 'relative_no', 'dpn_serial_no', 'educational_level_id', 'birth_country_id', 'birth_city', 'relative_gender_id');
		$colums = array();
		foreach ($cols as $col) {
			if ($col == 'group_id') {
				if ($this->input->get("safa_group_id") || ($safa_group_id != 0 && $safa_group_id != false)) {
					continue;
				}
			} else if ($col == 'display_order') {
				if (!$this->input->get("safa_group_id")) {
					continue;
				}
			} else if ($col == 'relative_gender_id') {
				continue;
			}


			if ($this->input->cookie('' . $ea_user_id . '_' . $col . '')) {
				$colums[] = $this->input->cookie('' . $ea_user_id . '_' . $col . '');
			}
		}

		if (!empty($colums) && count($colums) >= 1 && is_array($colums)) {
			$data['cols'] = $colums;
		} else {
			if ($this->input->get("safa_group_id")) {
				$data['cols'] = array('display_order', 'passport_no', 'name_la', 'no', 'name_ar');
			} else {
				$data['cols'] = array('group_id', 'passport_no', 'name_la', 'no', 'name_ar');
			}
		}
		$data['safa_group_id'] = $safa_group_id;
		$this->load->view('ea/group_passports/popup', $data);
	}

	//Added By Gouda, To select Trip travellers
	public function show_by_trip_popup($safa_trip_id = FALSE) {
		$this->layout = 'js';

		//print_r($_COOKIE); exit;
		$ed_id = session('ea_id');

		//By Gouda
		$ea_user_id = session('ea_user_id');


		if (isset($_GET['search'])) {
			//By Gouda
			//$data['safa_umrahgroup'] = 'safa_umrahgroup';
			$this->search();
		}

		if ($safa_trip_id != 0 && $safa_trip_id != false) {
			$this->safa_group_passports_model->safa_trip_id = $safa_trip_id;
		}
		$data["items"] = $this->safa_group_passports_model->get_for_table();

		$cols = array('group_id', 'display_order', 'no', 'passport_no', 'passport_type_id', 'name_ar', 'name_la', 'erp_country_id', 'nationality_id', 'date_of_birth', 'age', 'passport_issue_date',
            'passport_expiry_date', 'passport_issuing_country_id', 'passport_issuing_city', 'passport_dpn_count', 'relative_relation_id',
            'occupation', 'city', 'marital_status_id', 'relative_no', 'dpn_serial_no', 'educational_level_id', 'birth_country_id', 'birth_city', 'relative_gender_id');
		$colums = array();
		foreach ($cols as $col) {
			if ($col == 'display_order') {
				if (!$this->input->get("safa_group_id")) {
					continue;
				}
			} else if ($col == 'relative_gender_id') {
				continue;
			}


			if ($this->input->cookie('' . $ea_user_id . '_' . $col . '')) {
				$colums[] = $this->input->cookie('' . $ea_user_id . '_' . $col . '');
			}
		}

		if (!empty($colums) && count($colums) >= 1 && is_array($colums)) {
			$data['cols'] = $colums;
		} else {
			if ($this->input->get("safa_group_id")) {
				$data['cols'] = array('display_order', 'passport_no', 'name_la', 'no', 'name_ar');
			} else {
				$data['cols'] = array('group_id', 'passport_no', 'name_la', 'no', 'name_ar');
			}
		}
		$data['safa_trip_id'] = $safa_trip_id;
		$this->load->view('ea/group_passports/show_by_trip_popup', $data);
	}

	
	//Added By Gouda, To select Trip travellers
	public function add_to_internaltrip_by_trip_popup($safa_trip_id = FALSE) {
		$this->layout = 'js';

		//print_r($_COOKIE); exit;
		$ed_id = session('ea_id');

		//By Gouda
		$ea_user_id = session('ea_user_id');


		if (isset($_GET['search'])) {
			//By Gouda
			//$data['safa_umrahgroup'] = 'safa_umrahgroup';
			$this->search();
		}

		if ($safa_trip_id != 0 && $safa_trip_id != false) {
			//$this->safa_group_passports_model->safa_trip_id = $safa_trip_id;
		}
		$data["items"] = $this->safa_group_passports_model->get_for_table();

		$cols = array('group_id', 'display_order', 'no', 'passport_no', 'passport_type_id', 'name_ar', 'name_la', 'erp_country_id', 'nationality_id', 'date_of_birth', 'age', 'passport_issue_date',
            'passport_expiry_date', 'passport_issuing_country_id', 'passport_issuing_city', 'passport_dpn_count', 'relative_relation_id',
            'occupation', 'city', 'marital_status_id', 'relative_no', 'dpn_serial_no', 'educational_level_id', 'birth_country_id', 'birth_city', 'relative_gender_id');
		$colums = array();
		foreach ($cols as $col) {
			if ($col == 'display_order') {
				if (!$this->input->get("safa_group_id")) {
					continue;
				}
			} else if ($col == 'relative_gender_id') {
				continue;
			}


			if ($this->input->cookie('' . $ea_user_id . '_' . $col . '')) {
				$colums[] = $this->input->cookie('' . $ea_user_id . '_' . $col . '');
			}
		}

		if (!empty($colums) && count($colums) >= 1 && is_array($colums)) {
			$data['cols'] = $colums;
		} else {
			if ($this->input->get("safa_group_id")) {
				$data['cols'] = array('display_order', 'passport_no', 'name_la', 'no', 'name_ar');
			} else {
				$data['cols'] = array('group_id', 'passport_no', 'name_la', 'no', 'name_ar');
			}
		}
		$data['safa_trip_id'] = $safa_trip_id;
		$this->load->view('ea/group_passports/add_to_internaltrip_by_trip_popup', $data);
	}
	
	function columns() {
		$ed_id = session('ea_id');

		//By Gouda
		$ea_user_id = session('ea_user_id');

		$this->layout = 'js';
		$this->load->library("form_validation");
		$this->form_validation->set_rules('cols[]', 'lang:choose_cols', 'trim|required');
		$cols = array('group_id', 'display_order', 'passport_no', 'name_la', 'no', 'name_ar', 'nationality_id', 'passport_type_id', 'passport_issue_date',
            'passport_expiry_date', 'passport_issuing_city', 'passport_issuing_country_id', 'passport_dpn_count', 'dpn_serial_no', 'relative_relation_id',
            'educational_level_id', 'occupation', 'date_of_birth', 'age', 'birth_city', 'birth_country_id', 'relative_no', 'relative_gender_id', 'marital_status_id', 'city', 'erp_country_id', 'name');
		if ($this->form_validation->run() == false) {
			$colums = array();
			foreach ($cols as $col) {
				if ($this->input->cookie('' . $ea_user_id . '_' . $col . ''))
				$colums[] = $this->input->cookie('' . $ea_user_id . '_' . $col . '');
			}
			if (!empty($colums) && count($colums) >= 1 && is_array($colums))
			$data['cols'] = $colums;
			else
			$data['cols'] = $cols;
			$this->load->view("ea/group_passports/colum", $data);
		} else {
			if ($this->input->post('cols')) {
				foreach ($cols as $col) {
					if ($this->input->cookie('' . $ea_user_id . '_' . $col . '')) {
						setcookie('' . $ea_user_id . '_' . $col . '', '' . $col . '', time() - 60 * 60 * 24 * 365);
					}
				}
				$data = $this->input->post('cols');
				foreach ($data as $col) {
					setcookie('' . $ea_user_id . '_' . $col . '', '' . $col . '', time() + 60 * 60 * 24 * 365);
				}
			}

			//By Gouda.
			//$rows_count_per_page=$this->input->post('rows_count_per_page');
			//setcookie('' . $ea_user_id . '_' . 'group_passports_rows_count_per_page' . '', ''. $rows_count_per_page . '' , time() + 60 * 60 * 24 * 365);

			$this->load->view('redirect_message_popup', array('msg' => lang('uploaded_successfully_message'),
                'url' => site_url("ea/group_passports/columns"),
                'id' => $this->uri->segment("4"),
                'model_title' => lang('title'), 'action' => lang('upload_files')));
		}
	}

	public function add_umrah_passports() {
		$mutamers = $this->input->post("mutamers");
		if ($this->input->post('safa_group_id'))
		$data['safa_group_id'] = $this->input->post('safa_group_id');
		else
		$data['safa_group_id'] = FALSE;
		if (isset($mutamers) && count($mutamers) > 0 && is_array($mutamers))
		$data['mutamers'] = serialize($mutamers);
		else
		$data['mutamers'] = $mutamers;
		$data['confirm'] = $this->input->post("confirm_val");

		//By Gouda
		//$data["uo_contracts"] = $this->create_contracts_array();
		$structure = array('contract_id', name());
		$key = $structure['0'];
		if (isset($structure['1'])) {
			$value = $structure['1'];
		}
		$uo_contracts_arr = array();
		$uo_contracts_arr[''] = lang('global_select_from_menu');
		$uo_contracts_rows = $this->safa_group_passports_model->get_ea_contracts();
		foreach ($uo_contracts_rows as $uo_contracts_row) {
			//$uo_contracts_arr[$uo_contracts_row->$key] = $uo_contracts_row->$value;
			$uo_contracts_arr[$uo_contracts_row->$key] = $uo_contracts_row->safa_uo_contracts_eas_name;
		}
		$data["uo_contracts"] = $uo_contracts_arr;



		$this->load->library("form_validation");
		if (isset($_POST['add'])) {
			if (isset($_POST['safa_umrahgroup_id']))
			$this->form_validation->set_rules('safa_umrahgroup_id', 'lang:group_passports_group_name', 'trim|required|callback_check_relative_no|callback_check_mutamers_dpncount');
			else
			$this->form_validation->set_rules('safa_umrahgroup_name', 'lang:group_passports_group_name', 'trim|required|unique_col[safa_umrahgroups.name]|callback_check_relative_no|callback_check_mutamers_dpncount');
			$this->form_validation->set_rules('safa_uo_contract_id', 'lang:group_passports_uo_contract', 'trim|required|callback_check_safa_uo_contract_id');
		}
		if ($this->form_validation->run() == false) {
			$this->load->view('ea/group_passports/umrahgroup', $data);
		} else {
			if ($this->input->post('safa_umrahgroup_name')) {
				$this->safa_umrahgroups_model->name = $this->input->post('safa_umrahgroup_name');
				$this->safa_umrahgroups_model->creation_date = date("Y-m-d H:i", time());
				$this->safa_umrahgroups_model->safa_ea_id = session("ea_id");
				$this->safa_umrahgroups_model->safa_uo_contract_id = $this->input->post('safa_uo_contract_id');
				$safa_umrahgroup_id = $this->safa_umrahgroups_model->save();
				//************insert into safa_umrahgroups_passports*************************
				$mutamers2 = unserialize($this->input->post("mutamers"));
				if ($this->input->post('safa_group_id'))
				$safa_group_id = $this->input->post('safa_group_id');
				if (isset($mutamers2) && count($mutamers2) > 0) {
					foreach ($mutamers2 as $mutamer) {
						$this->safa_group_passports_model->safa_group_passport_id = $mutamer;
						$mutamers_info[] = $this->safa_group_passports_model->get();
					}
					$input = $this->toArray($mutamers_info);

					for ($i = 0; $i < sizeof($input); $i++) {

						//By Gouda, To fill this field also.
						//unset($input[$i]['safa_group_passport_id']);

						unset($input[$i]['safa_group_id']);
						unset($input[$i]['job2']);
						unset($input[$i]['remarks']);
						unset($input[$i]['canceled']);
						unset($input[$i]['mobile']);
						unset($input[$i]['sim_serial']);
						unset($input[$i]['notes']);
						unset($input[$i]['entry_datetime']);
						unset($input[$i]['safa_trip_internaltrip_id']);
						
						$input[$i]['safa_umrahgroup_id'] = $safa_umrahgroup_id;
					}
					$saved = 0;
					foreach ($input as $a) {

						if ($this->db->insert('safa_umrahgroups_passports', $a))
						$saved++;
					}
					if ($saved >= 1) {
						if (isset($safa_group_id)) {
							$this->load->view('redirect_message', array('msg' => lang('global_added_message'),
                                'url' => site_url('ea/group_passports/passports_index/' . $safa_group_id . ''),
                                'model_title' => lang('group_passports_title'), 'action' => lang('create_new_umrahgroup')));
						} else if (isset($safa_umrahgroup_id)) {
							$this->load->view('redirect_message', array('msg' => lang('global_added_message'),
                                'url' => site_url('ea/umrahgroups_passports/passports_index/' . $safa_umrahgroup_id . ''),
                                'model_title' => lang('group_passports_title'), 'action' => lang('move_to_umrahgroup')));
						} else {
							$this->load->view('redirect_message', array('msg' => lang('global_added_message'),
                                'url' => site_url('ea/group_passports'),
                                'model_title' => lang('group_passports_title'), 'action' => lang('create_new_umrahgroup')));
						}
					} else {
						if (isset($safa_group_id)) {
							$this->load->view('redirect_message', array('msg' => lang('global_error_save'),
                                'url' => site_url('ea/group_passports/passports_index/' . $safa_group_id . ''),
                                'model_title' => lang('group_passports_title'), 'action' => lang('create_new_umrahgroup')));
						} else if (isset($safa_umrahgroup_id)) {
							$this->load->view('redirect_message', array('msg' => lang('global_error_save'),
                                'url' => site_url('ea/umrahgroups_passports/passports_index/' . $safa_umrahgroup_id . ''),
                                'model_title' => lang('group_passports_title'), 'action' => lang('move_to_umrahgroup')));
						} else {
							$this->load->view('redirect_message', array('msg' => lang('global_error_save'),
                                'url' => site_url('ea/group_passports'),
                                'model_title' => lang('group_passports_title'), 'action' => lang('create_new_umrahgroup')));
						}
					}
				} else {
					if (isset($safa_group_id))
					redirect('ea/group_passports/passports_index/' . $safa_group_id . '');
					else
					redirect('ea/group_passports/index');
				}
			} elseif ($this->input->post("safa_umrahgroup_id")) {
				if ($this->input->post('safa_group_id'))
				$safa_group_id = $this->input->post('safa_group_id');
				$safa_umrahgroup_id = $this->input->post("safa_umrahgroup_id");
				$mutamers2 = unserialize($this->input->post("mutamers"));
				if (isset($mutamers2) && count($mutamers2) > 0) {
					foreach ($mutamers2 as $mutamer) {
						$this->safa_group_passports_model->safa_group_passport_id = $mutamer;
						$mutamers_info[] = $this->safa_group_passports_model->get();
					}
					$input = $this->toArray($mutamers_info);

					for ($i = 0; $i < sizeof($input); $i++) {
						//By Gouda, To fill this field also.
						//unset($input[$i]['safa_group_passport_id']);

						unset($input[$i]['safa_group_id']);
						unset($input[$i]['job2']);
						unset($input[$i]['remarks']);
						unset($input[$i]['canceled']);
						unset($input[$i]['mobile']);
						unset($input[$i]['sim_serial']);
						unset($input[$i]['notes']);
						unset($input[$i]['entry_datetime']);

						$input[$i]['safa_umrahgroup_id'] = $safa_umrahgroup_id;
					}
					for ($i = 0; $i < sizeof($input); $i++) {
						if ($this->move_toumrahgroup_check($safa_umrahgroup_id, $input[$i]['passport_no']))
						$unique_mutamers[] = $input[$i];
						else
						$exist_mutamers[] = $input[$i];
					}
					if (isset($unique_mutamers) && count($unique_mutamers) > 0) {
						$saved = 0;
						foreach ($unique_mutamers as $a) {

							if ($this->db->insert('safa_umrahgroups_passports', $a))
							$saved++;
						}
						if ($saved >= 1) {
							if (isset($exist_mutamers) && count($exist_mutamers) > 0) {
								if (isset($safa_group_id)) {
									$this->load->view('ea/group_passports/redirect_message', array('msg' => lang('cut_group_passports_some_mutamerds_exist_msg'),
                                        'url' => site_url('ea/group_passports/passports_index/' . $safa_group_id . ''),
                                        'model_title' => lang('group_passports_title'), 'action' => lang('move_to_umrahgroup'),
                                        'exist_mutamers' => $exist_mutamers));
								} else if (isset($safa_umrahgroup_id)) {
									$this->load->view('redirect_message', array('msg' => lang('copy_cut_group_passports_mutamerds_exist_msg'),
                                        'url' => site_url('ea/umrahgroups_passports/passports_index/' . $safa_umrahgroup_id . ''),
                                        'model_title' => lang('group_passports_title'), 'action' => lang('move_to_umrahgroup')));
								} else {
									$this->load->view('ea/group_passports/redirect_message', array('msg' => lang('cut_group_passports_some_mutamerds_exist_msg'),
                                        'url' => site_url('ea/group_passports'),
                                        'model_title' => lang('group_passports_title'), 'action' => lang('move_to_umrahgroup'),
                                        'exist_mutamers' => $exist_mutamers));
								}
							} else {
								if (isset($safa_group_id)) {
									$this->load->view('redirect_message', array('msg' => lang('global_cut_message'),
                                        'url' => site_url('ea/group_passports/passports_index/' . $safa_group_id . ''),
                                        'model_title' => lang('group_passports_title'), 'action' => lang('move_to_umrahgroup')));
								} else if (isset($safa_umrahgroup_id)) {
									$this->load->view('redirect_message', array('msg' => lang('global_cut_message'),
                                        'url' => site_url('ea/umrahgroups_passports/passports_index/' . $safa_umrahgroup_id . ''),
                                        'model_title' => lang('group_passports_title'), 'action' => lang('move_to_umrahgroup')));
								} else {
									$this->load->view('redirect_message', array('msg' => lang('global_cut_message'),
                                        'url' => site_url('ea/group_passports'),
                                        'model_title' => lang('group_passports_title'), 'action' => lang('move_to_umrahgroup')));
								}
							}
						} else {
							if (isset($safa_group_id)) {
								$this->load->view('redirect_message', array('msg' => lang('global_error_save'),
                                    'url' => site_url('ea/group_passports/passports_index/' . $safa_group_id . ''),
                                    'model_title' => lang('group_passports_title'), 'action' => lang('move_to_umrahgroup')));
							} else if (isset($safa_umrahgroup_id)) {
								$this->load->view('redirect_message', array('msg' => lang('global_error_save'),
                                    'url' => site_url('ea/umrahgroups_passports/passports_index/' . $safa_umrahgroup_id . ''),
                                    'model_title' => lang('group_passports_title'), 'action' => lang('move_to_umrahgroup')));
							} else {
								$this->load->view('redirect_message', array('msg' => lang('global_error_save'),
                                    'url' => site_url('ea/group_passports'),
                                    'model_title' => lang('group_passports_title'), 'action' => lang('move_to_umrahgroup')));
							}
						}
					} else {
						if (isset($safa_group_id)) {
							$this->load->view('redirect_message', array('msg' => lang('copy_cut_group_passports_mutamerds_exist_msg'),
                                'url' => site_url('ea/group_passports/passports_index/' . $safa_group_id . ''),
                                'model_title' => lang('group_passports_title'), 'action' => lang('move_to_umrahgroup')));
						} else if (isset($safa_umrahgroup_id)) {
							$this->load->view('redirect_message', array('msg' => lang('copy_cut_group_passports_mutamerds_exist_msg'),
                                'url' => site_url('ea/umrahgroups_passports/passports_index/' . $safa_umrahgroup_id . ''),
                                'model_title' => lang('group_passports_title'), 'action' => lang('move_to_umrahgroup')));
						} else {
							$this->load->view('redirect_message', array('msg' => lang('copy_cut_group_passports_mutamerds_exist_msg'),
                                'url' => site_url('ea/group_passports'),
                                'model_title' => lang('group_passports_title'), 'action' => lang('move_to_umrahgroup')));
						}
					}
				} else {
					if (isset($safa_group_id))
					redirect('ea/group_passports/passports_index/' . $safa_group_id . '');
					else
					redirect('ea/group_passports/index');
				}
			}
		}
	}

	public function copy_cut() {
		$mutamers = $this->input->post("mutamers");
		if ($this->input->post('safa_group_id'))
		$data['safa_group_id'] = $this->input->post('safa_group_id');
		else
		$data['safa_group_id'] = FALSE;
		if (isset($mutamers) && count($mutamers) > 0 && is_array($mutamers))
		$data['mutamers'] = serialize($mutamers);
		else
		$data['mutamers'] = $mutamers;
		$data['copy_cut'] = $this->input->post("copy_cut_val");
		//        $data["uo_contracts"] = $this->create_contracts_array();
		$this->load->library("form_validation");
		if (isset($_POST['add']))
		$this->form_validation->set_rules('safa_group', 'lang:group_passports_group_name', 'trim|required|callback_check_relative_no|callback_check_mutamers_dpncount');

		if ($this->form_validation->run() == false) {
			$this->load->view('ea/group_passports/copy_cut', $data);
		} else {
			if ($this->input->post('copy_cut_val') == 'cut') {

				//************insert into safa_umrahgroups_passports*************************
				$mutamers2 = unserialize($this->input->post("mutamers"));
				$cut_group_id = $this->input->post('safa_group');
				if ($this->input->post('safa_group_id'))
				$safa_group_id = $this->input->post('safa_group_id');
				if (isset($mutamers2) && count($mutamers2) > 0) {
					foreach ($mutamers2 as $mutamer) {

						$this->safa_group_passports_model->safa_group_passport_id = $mutamer;
						$mutamers_info[] = $this->safa_group_passports_model->get();
					}
					$input = $this->toArray($mutamers_info);
					//                    for ($i = 0; $i < sizeof($input); $i++) {
					//                        if ($this->copy_cut_check($cut_group_id, $input[$i]['passport_no']))
					//                            $unique_mutamers[] = $input[$i];
					//                        else
					//                            $exist_mutamers[] = $input[$i];
					//                    }
					if (isset($input) && count($input) > 0) {
						for ($i = 0; $i < sizeof($input); $i++) {
							$this->db->set('safa_group_id', $cut_group_id, FALSE);
							$this->db->where('safa_group_passport_id', $input[$i]['safa_group_passport_id']);
							$saved = $this->db->update('safa_group_passports');
						}
						if ($saved >= 1) {
							if (isset($safa_group_id)) {
								$this->load->view('redirect_message', array('msg' => lang('global_cut_message'),
                                    'url' => site_url('ea/group_passports/passports_index/' . $safa_group_id . ''),
                                    'model_title' => lang('group_passports_title'), 'action' => lang('cut_to_group_passports')));
							} else {
								$this->load->view('redirect_message', array('msg' => lang('global_cut_message'),
                                    'url' => site_url('ea/group_passports'),
                                    'model_title' => lang('group_passports_title'), 'action' => lang('cut_to_group_passports')));
							}
						} else {
							if (isset($safa_group_id)) {
								$this->load->view('redirect_message', array('msg' => lang('global_error_save'),
                                    'url' => site_url('ea/group_passports/passports_index/' . $safa_group_id . ''),
                                    'model_title' => lang('group_passports_title'), 'action' => lang('cut_to_group_passports')));
							} else {
								$this->load->view('redirect_message', array('msg' => lang('global_error_save'),
                                    'url' => site_url('ea/group_passports'),
                                    'model_title' => lang('group_passports_title'), 'action' => lang('cut_to_group_passports')));
							}
						}
					} else {
						if (isset($safa_group_id))
						redirect('ea/group_passports/passports_index/' . $safa_group_id . '');
						else
						redirect('ea/group_passports/index');
					}
				} else {
					if (isset($safa_group_id))
					redirect('ea/group_passports/passports_index/' . $safa_group_id . '');
					else
					redirect('ea/group_passports/index');
				}
			}
		}
	}

	public function show_data($id = FALSE) {

		$this->layout = 'js';

		if (!$id)
		show_404();
		$this->safa_group_passports_model->safa_group_passport_id = $id;
		$data['item'] = $this->safa_group_passports_model->get();
		$this->load->view('ea/group_passports/show_data', $data);
	}

	public function edit($id = FALSE) {
		if (!$id)
		show_404();
		$this->safa_group_passports_model->safa_group_passport_id = $id;
		$data['item'] = $this->safa_group_passports_model->get();

		if (!$data['item'])
		show_404();

		$data['relative_rows'] = $this->safa_group_passports_model->get_by_relative_no($id);

		$this->load->library("form_validation");
		$this->form_validation->set_rules('safa_group_id', 'lang:group_passports_safa_group_id', 'trim');
		$this->form_validation->set_rules('no', 'lang:group_passports_no', 'trim');
		$this->form_validation->set_rules('title_id', 'lang:group_passports_title_id', 'trim');
		$this->form_validation->set_rules('first_name_la', 'lang:group_passports_first_name_la', 'trim');
		$this->form_validation->set_rules('second_name_la', 'lang:group_passports_second_name_la', 'trim');
		$this->form_validation->set_rules('third_name_la', 'lang:group_passports_third_name_la', 'trim');
		$this->form_validation->set_rules('fourth_name_la', 'lang:group_passports_fourth_name_la', 'trim');
		$this->form_validation->set_rules('first_name_ar', 'lang:group_passports_first_name_ar', 'trim');
		$this->form_validation->set_rules('second_name_ar', 'lang:group_passports_second_name_ar', 'trim');
		$this->form_validation->set_rules('third_name_ar', 'lang:group_passports_third_name_ar', 'trim');
		$this->form_validation->set_rules('fourth_name_ar', 'lang:group_passports_fourth_name_ar', 'trim');
		$this->form_validation->set_rules('nationality_id', 'lang:group_passports_nationality_id', 'trim');
		$this->form_validation->set_rules('previous_nationality_id', 'lang:group_passports_previous_nationality_id', 'trim');
		$this->form_validation->set_rules('passport_no', 'lang:group_passports_passport_no', 'trim');
		$this->form_validation->set_rules('passport_type_id', 'lang:group_passports_passport_type_id', 'trim');
		$this->form_validation->set_rules('passport_issue_date', 'lang:group_passports_passport_issue_date', 'trim');
		$this->form_validation->set_rules('passport_expiry_date', 'lang:group_passports_passport_expiry_date', 'trim');
		$this->form_validation->set_rules('passport_issuing_city', 'lang:group_passports_passport_issuing_city', 'trim');
		$this->form_validation->set_rules('passport_issuing_country_id', 'lang:group_passports_passport_issuing_country_id', 'trim');
		$this->form_validation->set_rules('passport_dpn_count', 'lang:group_passports_passport_dpn_count', 'trim');
		$this->form_validation->set_rules('dpn_serial_no', 'lang:group_passports_dpn_serial_no', 'trim');
		$this->form_validation->set_rules('relative_relation_id', 'lang:group_passports_relative_relation_id', 'trim');
		$this->form_validation->set_rules('educational_level_id', 'lang:group_passports_educational_level_id', 'trim');
		$this->form_validation->set_rules('occupation', 'lang:group_passports_occupation', 'trim');
		$this->form_validation->set_rules('date_of_birth', 'lang:group_passports_date_of_birth', 'trim');
		//$this->form_validation->set_rules('age', 'lang:group_passports_age', 'trim');
		$this->form_validation->set_rules('birth_city', 'lang:group_passports_birth_city', 'trim');
		$this->form_validation->set_rules('birth_country_id', 'lang:group_passports_birth_country_id', 'trim');
		$this->form_validation->set_rules('relative_no', 'lang:group_passports_relative_no', 'trim');
		$this->form_validation->set_rules('relative_gender_id', 'lang:group_passports_relative_gender_id', 'trim');
		$this->form_validation->set_rules('gender_id', 'lang:group_passports_gender_id', 'trim');
		$this->form_validation->set_rules('marital_status_id', 'lang:group_passports_marital_status_id', 'trim');
		$this->form_validation->set_rules('city', 'lang:group_passports_city', 'trim');
		$this->form_validation->set_rules('erp_country_id', 'lang:group_passports_erp_country_id', 'trim');

		if ($this->form_validation->run() == false) {
			$this->load->view("ea/group_passports/edit", $data);
		} else {
			$this->safa_group_passports_model->safa_group_id = $this->input->post('safa_group_id');
			$this->safa_group_passports_model->no = $this->input->post('no');
			$this->safa_group_passports_model->title_id = $this->input->post('title_id');
			$this->safa_group_passports_model->first_name_la = $this->input->post('first_name_la');
			$this->safa_group_passports_model->second_name_la = $this->input->post('second_name_la');
			$this->safa_group_passports_model->third_name_la = $this->input->post('third_name_la');
			$this->safa_group_passports_model->fourth_name_la = $this->input->post('fourth_name_la');
			$this->safa_group_passports_model->first_name_ar = $this->input->post('first_name_ar');
			$this->safa_group_passports_model->second_name_ar = $this->input->post('second_name_ar');
			$this->safa_group_passports_model->third_name_ar = $this->input->post('third_name_ar');
			$this->safa_group_passports_model->fourth_name_ar = $this->input->post('fourth_name_ar');
			$this->safa_group_passports_model->nationality_id = $this->input->post('nationality_id');
			$this->safa_group_passports_model->previous_nationality_id = $this->input->post('previous_nationality_id');
			$this->safa_group_passports_model->passport_no = $this->input->post('passport_no');
			$this->safa_group_passports_model->passport_type_id = $this->input->post('passport_type_id');
			$this->safa_group_passports_model->passport_issue_date = $this->input->post('passport_issue_date');
			$this->safa_group_passports_model->passport_expiry_date = $this->input->post('passport_expiry_date');
			$this->safa_group_passports_model->passport_issuing_city = $this->input->post('passport_issuing_city');
			$this->safa_group_passports_model->passport_issuing_country_id = $this->input->post('passport_issuing_country_id');
			$this->safa_group_passports_model->passport_dpn_count = $this->input->post('passport_dpn_count');
			$this->safa_group_passports_model->dpn_serial_no = $this->input->post('dpn_serial_no');
			$this->safa_group_passports_model->relative_relation_id = $this->input->post('relative_relation_id');
			$this->safa_group_passports_model->educational_level_id = $this->input->post('educational_level_id');
			$this->safa_group_passports_model->occupation = $this->input->post('occupation');
			$this->safa_group_passports_model->date_of_birth = $this->input->post('date_of_birth');
			//$this->safa_group_passports_model->age = $this->input->post('age');
			$this->safa_group_passports_model->birth_city = $this->input->post('birth_city');
			$this->safa_group_passports_model->birth_country_id = $this->input->post('birth_country_id');
			$this->safa_group_passports_model->relative_no = $this->input->post('relative_no');
			$this->safa_group_passports_model->relative_gender_id = $this->input->post('relative_gender_id');
			$this->safa_group_passports_model->gender_id = $this->input->post('gender_id');
			$this->safa_group_passports_model->marital_status_id = $this->input->post('marital_status_id');
			$this->safa_group_passports_model->city = $this->input->post('city');
			$this->safa_group_passports_model->erp_country_id = $this->input->post('erp_country_id');

			$this->safa_group_passports_model->id_number = $this->input->post('id_number');
			$this->safa_group_passports_model->region_name = $this->input->post('region_name');

			$this->safa_group_passports_model->address = $this->input->post('address');
			$this->safa_group_passports_model->zip_code = $this->input->post('zip_code');
			$this->safa_group_passports_model->mobile = $this->input->post('mobile');
			$this->safa_group_passports_model->email_address = $this->input->post('email_address');
			$this->safa_group_passports_model->remarks = $this->input->post('remarks');

			$this->upload_image();
			
			$this->safa_group_passports_model->save();

			$this->add_relatives($id);

			//By Gouda. To save modification time.
			$this->db->insert('safa_group_passport_modifications', array(
                'safa_group_passport_id' => $id,
                'user_type' => 3,
                'user_id' => session('ea_user_id'),
                'datetime' => date('Y-m-d H:i'),
			));
			$c = $this->db->update('safa_groups', array('last_update' => date('Y-m-d H:i')), " `safa_group_id` = '" . $this->input->post('safa_group_id') . "'");


			$this->load->view('redirect_message', array('msg' => lang('global_updated_message'),
                'url' => site_url('ea/group_passports/passports_index/' . $this->safa_group_passports_model->safa_group_id),
                'id' => $id,
                'model_title' => lang('group_passports_title'), 'action' => lang('group_passports_edit_title')));
		}
	}

	public function add($safa_group_id=0)
	{
		$data['safa_group_id']=$safa_group_id;

		$this->load->library("form_validation");
		//$this->form_validation->set_rules('safa_group_id', 'lang:group_passports_safa_group_id', 'trim');
		//$this->form_validation->set_rules('no', 'lang:group_passports_no', 'trim');
		$this->form_validation->set_rules('title_id', 'lang:group_passports_title_id', 'trim');
		$this->form_validation->set_rules('first_name_la', 'lang:group_passports_first_name_la', 'trim');
		$this->form_validation->set_rules('second_name_la', 'lang:group_passports_second_name_la', 'trim');
		$this->form_validation->set_rules('third_name_la', 'lang:group_passports_third_name_la', 'trim');
		$this->form_validation->set_rules('fourth_name_la', 'lang:group_passports_fourth_name_la', 'trim');
		$this->form_validation->set_rules('first_name_ar', 'lang:group_passports_first_name_ar', 'trim');
		$this->form_validation->set_rules('second_name_ar', 'lang:group_passports_second_name_ar', 'trim');
		$this->form_validation->set_rules('third_name_ar', 'lang:group_passports_third_name_ar', 'trim');
		$this->form_validation->set_rules('fourth_name_ar', 'lang:group_passports_fourth_name_ar', 'trim');
		$this->form_validation->set_rules('nationality_id', 'lang:group_passports_nationality_id', 'trim');
		$this->form_validation->set_rules('previous_nationality_id', 'lang:group_passports_previous_nationality_id', 'trim');
		$this->form_validation->set_rules('passport_no', 'lang:group_passports_passport_no', 'trim');
		$this->form_validation->set_rules('passport_type_id', 'lang:group_passports_passport_type_id', 'trim');
		$this->form_validation->set_rules('passport_issue_date', 'lang:group_passports_passport_issue_date', 'trim');
		$this->form_validation->set_rules('passport_expiry_date', 'lang:group_passports_passport_expiry_date', 'trim');
		$this->form_validation->set_rules('passport_issuing_city', 'lang:group_passports_passport_issuing_city', 'trim');
		$this->form_validation->set_rules('passport_issuing_country_id', 'lang:group_passports_passport_issuing_country_id', 'trim');
		$this->form_validation->set_rules('passport_dpn_count', 'lang:group_passports_passport_dpn_count', 'trim');
		$this->form_validation->set_rules('dpn_serial_no', 'lang:group_passports_dpn_serial_no', 'trim');
		$this->form_validation->set_rules('relative_relation_id', 'lang:group_passports_relative_relation_id', 'trim');
		$this->form_validation->set_rules('educational_level_id', 'lang:group_passports_educational_level_id', 'trim');
		$this->form_validation->set_rules('occupation', 'lang:group_passports_occupation', 'trim');
		$this->form_validation->set_rules('date_of_birth', 'lang:group_passports_date_of_birth', 'trim');
		//$this->form_validation->set_rules('age', 'lang:group_passports_age', 'trim');
		$this->form_validation->set_rules('birth_city', 'lang:group_passports_birth_city', 'trim');
		$this->form_validation->set_rules('birth_country_id', 'lang:group_passports_birth_country_id', 'trim');
		$this->form_validation->set_rules('relative_no', 'lang:group_passports_relative_no', 'trim');
		$this->form_validation->set_rules('relative_gender_id', 'lang:group_passports_relative_gender_id', 'trim');
		$this->form_validation->set_rules('gender_id', 'lang:group_passports_gender_id', 'trim');
		$this->form_validation->set_rules('marital_status_id', 'lang:group_passports_marital_status_id', 'trim');
		$this->form_validation->set_rules('city', 'lang:group_passports_city', 'trim');
		$this->form_validation->set_rules('erp_country_id', 'lang:group_passports_erp_country_id', 'trim');

		if ($this->form_validation->run() == false) {
			$this->load->view("ea/group_passports/add", $data);
		} else {
			$this->safa_group_passports_model->safa_group_id = $safa_group_id;
			$group_passports_row = $this->safa_group_passports_model->get_max_no_by_group($safa_group_id);

			$this->safa_group_passports_model->no = $group_passports_row->max_no+1;

			$this->safa_group_passports_model->title_id = $this->input->post('title_id');
			$this->safa_group_passports_model->first_name_la = $this->input->post('first_name_la');
			$this->safa_group_passports_model->second_name_la = $this->input->post('second_name_la');
			$this->safa_group_passports_model->third_name_la = $this->input->post('third_name_la');
			$this->safa_group_passports_model->fourth_name_la = $this->input->post('fourth_name_la');
			$this->safa_group_passports_model->first_name_ar = $this->input->post('first_name_ar');
			$this->safa_group_passports_model->second_name_ar = $this->input->post('second_name_ar');
			$this->safa_group_passports_model->third_name_ar = $this->input->post('third_name_ar');
			$this->safa_group_passports_model->fourth_name_ar = $this->input->post('fourth_name_ar');
			$this->safa_group_passports_model->nationality_id = $this->input->post('nationality_id');
			$this->safa_group_passports_model->previous_nationality_id = $this->input->post('previous_nationality_id');
			$this->safa_group_passports_model->passport_no = $this->input->post('passport_no');
			$this->safa_group_passports_model->passport_type_id = $this->input->post('passport_type_id');
			$this->safa_group_passports_model->passport_issue_date = $this->input->post('passport_issue_date');
			$this->safa_group_passports_model->passport_expiry_date = $this->input->post('passport_expiry_date');
			$this->safa_group_passports_model->passport_issuing_city = $this->input->post('passport_issuing_city');
			$this->safa_group_passports_model->passport_issuing_country_id = $this->input->post('passport_issuing_country_id');
			$this->safa_group_passports_model->passport_dpn_count = $this->input->post('passport_dpn_count');
			$this->safa_group_passports_model->dpn_serial_no = $this->input->post('dpn_serial_no');
			$this->safa_group_passports_model->relative_relation_id = $this->input->post('relative_relation_id');
			$this->safa_group_passports_model->educational_level_id = $this->input->post('educational_level_id');
			$this->safa_group_passports_model->occupation = $this->input->post('occupation');
			$this->safa_group_passports_model->date_of_birth = $this->input->post('date_of_birth');
			//$this->safa_group_passports_model->age = $this->input->post('age');
			$this->safa_group_passports_model->birth_city = $this->input->post('birth_city');
			$this->safa_group_passports_model->birth_country_id = $this->input->post('birth_country_id');
			$this->safa_group_passports_model->relative_no = $this->input->post('relative_no');
			$this->safa_group_passports_model->relative_gender_id = $this->input->post('relative_gender_id');
			$this->safa_group_passports_model->gender_id = $this->input->post('gender_id');
			$this->safa_group_passports_model->marital_status_id = $this->input->post('marital_status_id');
			$this->safa_group_passports_model->city = $this->input->post('city');
			$this->safa_group_passports_model->erp_country_id = $this->input->post('erp_country_id');

			$this->safa_group_passports_model->id_number = $this->input->post('id_number');
			$this->safa_group_passports_model->region_name = $this->input->post('region_name');

			$this->safa_group_passports_model->address = $this->input->post('address');
			$this->safa_group_passports_model->zip_code = $this->input->post('zip_code');
			$this->safa_group_passports_model->mobile = $this->input->post('mobile');
			$this->safa_group_passports_model->email_address = $this->input->post('email_address');
			$this->safa_group_passports_model->remarks = $this->input->post('remarks');

			//Relatives
			$relat_safa_group_passport_id_arr = $this->input->post('relat_safa_group_passport_id');
			$this->safa_group_passports_model->passport_dpn_count = count($relat_safa_group_passport_id_arr);

			$this->upload_image();
			
			$id = $this->safa_group_passports_model->save();

			$this->add_relatives($id);

			//By Gouda. To save modification time.
			$this->db->insert('safa_group_passport_modifications', array(
                'safa_group_passport_id' => $id,
                'user_type' => 3,
                'user_id' => session('ea_user_id'),
                'datetime' => date('Y-m-d H:i'),
			));
			$c = $this->db->update('safa_groups', array('last_update' => date('Y-m-d H:i')), " `safa_group_id` = '" . $this->input->post('safa_group_id') . "'");


			$this->load->view('redirect_message', array('msg' => lang('global_updated_message'),
                'url' => site_url('ea/group_passports/passports_index/' . $this->safa_group_passports_model->safa_group_id),
                'id' => $id,
                'model_title' => lang('group_passports_title'), 'action' => lang('group_passports_edit_title')));
		}
	}

	function add_relatives($id = 0)
	{
		$relat_safa_group_passport_id_arr = $this->input->post('relat_safa_group_passport_id');

		$relat_first_name_la_arr = $this->input->post('relat_first_name_la');
		$relat_second_name_la_arr = $this->input->post('relat_second_name_la');
		$relat_third_name_la_arr = $this->input->post('relat_third_name_la');
		$relat_fourth_name_la_arr = $this->input->post('relat_fourth_name_la');

		$relat_first_name_ar_arr = $this->input->post('relat_first_name_ar');
		$relat_second_name_ar_arr = $this->input->post('relat_second_name_ar');
		$relat_third_name_ar_arr = $this->input->post('relat_third_name_ar');
		$relat_fourth_name_ar_arr = $this->input->post('relat_fourth_name_ar');

		$relat_date_of_birth_arr = $this->input->post('relat_date_of_birth');
		$relat_birth_city_arr = $this->input->post('relat_birth_city');
		$relat_birth_country_id_arr = $this->input->post('relat_birth_country_id');
		$relat_gender_id_arr = $this->input->post('relat_gender_id');
		$relat_relative_relation_id_arr = $this->input->post('relat_relative_relation_id');

		if(count($relat_safa_group_passport_id_arr)>0) {
			foreach ($relat_safa_group_passport_id_arr as $key => $value) {

				if($relat_safa_group_passport_id_arr[$key]!=0){
					$this->safa_group_passports_model->safa_group_passport_id = $relat_safa_group_passport_id_arr[$key];
				} else {
					$this->safa_group_passports_model->safa_group_passport_id = false;
				}
					
				$this->safa_group_passports_model->relative_no =$id;
					
					
				$this->safa_group_passports_model->first_name_la = $relat_first_name_la_arr[$key];
				$this->safa_group_passports_model->second_name_la = $relat_second_name_la_arr[$key];
				$this->safa_group_passports_model->third_name_la = $relat_third_name_la_arr[$key];
				$this->safa_group_passports_model->fourth_name_la = $relat_fourth_name_la_arr[$key];
				$this->safa_group_passports_model->first_name_ar = $relat_first_name_ar_arr[$key];
				$this->safa_group_passports_model->second_name_ar = $relat_second_name_ar_arr[$key];
				$this->safa_group_passports_model->third_name_ar = $relat_third_name_ar_arr[$key];
				$this->safa_group_passports_model->fourth_name_ar = $relat_fourth_name_ar_arr[$key];
					
				$this->safa_group_passports_model->date_of_birth = $relat_date_of_birth_arr[$key];
				$this->safa_group_passports_model->birth_city = $relat_birth_city_arr[$key];
				$this->safa_group_passports_model->birth_country_id = $relat_birth_country_id_arr[$key];
				$this->safa_group_passports_model->gender_id = $relat_gender_id_arr[$key];
				$this->safa_group_passports_model->relative_relation_id = $relat_relative_relation_id_arr[$key];
					
				$this->safa_group_passports_model->save();
					
			}
		}

		$relatives_remove = $this->input->post('relatives_remove');
		if (ensure($relatives_remove)) {
			foreach ($relatives_remove as $safa_group_passport_id) {
				$this->safa_group_passports_model->safa_group_passport_id = $safa_group_passport_id;
				$this->safa_group_passports_model->delete();
			}
		}


	}

	function upload_image()
	{
		$now_timestamp = strtotime(date('Y-m-d H:i', time()));
		if (isset($_FILES["picture_file"])) {
			if (count($_FILES["picture_file"]['tmp_name']) > 0) {
				$tmpFilePath = $_FILES["picture_file"]['tmp_name'];

				if ($tmpFilePath != "") {
					
					$file_name = $now_timestamp.'_'.$_FILES["picture_file"]['name'];
					$newFilePath = './static/group_passports/' . $file_name;

					$is_file_uploaded = move_uploaded_file($tmpFilePath, iconv('utf-8', 'windows-1256', $newFilePath));
					
					$this->safa_group_passports_model->picture = $file_name;
				}
			}
		}
	}

	public function delete_old() {
		$this->layout = 'ajax';
		if (!$this->input->is_ajax_request()) {
			redirect('ea/group_passports/index');
		} else {
			$safa_group_passport_id = $this->input->post('safa_group_passport_id');


			$is_umrah_passport = $this->safa_group_passports_model->check_umrah_passports_for_passport($safa_group_passport_id);
			$is_trip_traveller = $this->safa_group_passports_model->check_trip_travellers_for_passport($safa_group_passport_id);

			if ($is_umrah_passport || $is_trip_traveller) {

				if ($is_umrah_passport) {
					echo json_encode(array('response' => TRUE, 'msg' => lang('cannot_delete_this_passport_has_visa')));
				} else if ($is_trip_traveller) {
					echo json_encode(array('response' => TRUE, 'msg' => lang('cannot_delete_this_passport_on_trip')));
				}
			} else {

				//By Gouda.
				$data_dpncount = $this->safa_group_passports_model->check_delete_dpncount_ability($safa_group_passport_id);
				if ($data_dpncount == 0) {
					//    $data = $this->safa_group_passports_model->check_delete_mehrem_ability($safa_group_passport_id);
					//    if ($data == 0) {
					//        $dpn_serial_no = $this->safa_group_passports_model->check_mutamer_dpn_serial_no($safa_group_passport_id);
					//        if ($dpn_serial_no == 1)
					//            $this->safa_group_passports_model->update_dpn_count($safa_group_passport_id);
					//Commented By Gouda.
					//$mhrem_passport_no = $this->safa_group_passports_model->get_his_mehrem_passport_no($safa_group_passport_id);
					$this->safa_group_passports_model->safa_group_passport_id = $safa_group_passport_id;

					if ($this->safa_group_passports_model->delete()) {

						$this->safa_group_passports_model->delete_by_relative_no($safa_group_passport_id);

						/*
						 if (isset($mhrem_passport_no)) {
						 echo json_encode(array('response' => TRUE, 'msg' => lang('global_delete_confirm'), 'passno' => lang('delete_his_mehrem_passport_no') . $mhrem_passport_no->passport_no));
						 } else {
						 echo json_encode(array('response' => TRUE, 'msg' => lang('global_delete_confirm')));
						 }
						 */

						echo json_encode(array('response' => TRUE, 'msg' => lang('global_delete_confirm')));
					}
					/*
					 } else {
					 $passports_no = $this->safa_group_passports_model->get_mehrem_to_passports_no($safa_group_passport_id);
					 if (count($passports_no) > 1) {
					 $msg = lang('not_delete_first_mehrem_to_passports_no');
					 foreach ($passports_no as $passport_no) {
					 $msg.= $passport_no->passport_no . ',';
					 }
					 echo json_encode(array('response' => FALSE, 'msg' => $msg));
					 } else {
					 $msg = lang('not_delete_first_mehrem_to_passports_no');
					 $msg.= $passports_no[0]->passport_no;
					 echo json_encode(array('response' => FALSE, 'msg' => $msg));
					 }
					 }
					 */
				} else {
					//echo json_encode(array('response' => FALSE, 'msg' => lang('not_delete_first_dpn_count_passports_no')));

					$this->load->view('ea/group_passports/delete_message', array('msg' => lang('delete_group_passports_some_mutamerds_hasMutamers_msg'),
                        'url' => site_url('ea/group_passports/delete_confirmation/' . $safa_group_passport_id . ''),
                        'model_title' => lang('group_passports_title'), 'action' => lang('global_delete'),
					));
				}
			}
		}
	}

	public function delete($safa_group_id = 0, $safa_group_passport_id = 0) {
		$is_umrah_passport = $this->safa_group_passports_model->check_umrah_passports_for_passport($safa_group_passport_id);
		$is_trip_traveller = $this->safa_group_passports_model->check_trip_travellers_for_passport($safa_group_passport_id);

		if ($is_umrah_passport || $is_trip_traveller) {

			if ($is_umrah_passport) {
				echo json_encode(array('response' => TRUE, 'msg' => lang('cannot_delete_this_passport_has_visa')));
			} else if ($is_trip_traveller) {
				echo json_encode(array('response' => TRUE, 'msg' => lang('cannot_delete_this_passport_on_trip')));
			}
		} else {

			//By Gouda.
			$data_dpncount = $this->safa_group_passports_model->check_delete_dpncount_ability($safa_group_passport_id);
			if ($data_dpncount == 0) {
				//$data = $this->safa_group_passports_model->check_delete_mehrem_ability($safa_group_passport_id);
				//if ($data == 0) {
				$dpn_serial_no = $this->safa_group_passports_model->check_mutamer_dpn_serial_no($safa_group_passport_id);
				if ($dpn_serial_no == 1)
				$this->safa_group_passports_model->update_dpn_count($safa_group_passport_id);

				//Commented By Gouda.
				//$mhrem_passport_no = $this->safa_group_passports_model->get_his_mehrem_passport_no($safa_group_passport_id);
				$this->safa_group_passports_model->safa_group_passport_id = $safa_group_passport_id;

				$this->safa_group_passports_model->delete();

				//$this->safa_group_passports_model->delete_by_relative_no($safa_group_passport_id);

				/*
				 if (isset($mhrem_passport_no)) {
				 echo json_encode(array('response' => TRUE, 'msg' => lang('global_delete_confirm'), 'passno' => lang('delete_his_mehrem_passport_no') . $mhrem_passport_no->passport_no));
				 } else {
				 echo json_encode(array('response' => TRUE, 'msg' => lang('global_delete_confirm')));
				 }
				 */

				//echo json_encode(array('response' => TRUE, 'msg' => lang('global_delete_confirm')));


				$this->load->view('redirect_message', array('msg' => lang('global_deleted_message'),
                    'url' => site_url('ea/group_passports/passports_index/' . $safa_group_id),
                    'model_title' => lang('group_passports_title'), 'action' => lang('global_delete')));


				/*
				 } else {
				 $passports_no = $this->safa_group_passports_model->get_mehrem_to_passports_no($safa_group_passport_id);
				 if (count($passports_no) > 1) {
				 $msg = lang('not_delete_first_mehrem_to_passports_no');
				 foreach ($passports_no as $passport_no) {
				 $msg.= $passport_no->passport_no . ',';
				 }
				 echo json_encode(array('response' => FALSE, 'msg' => $msg));
				 } else {
				 $msg = lang('not_delete_first_mehrem_to_passports_no');
				 $msg.= $passports_no[0]->passport_no;
				 echo json_encode(array('response' => FALSE, 'msg' => $msg));
				 }
				 }
				 */
			} else {
				//echo json_encode(array('response' => FALSE, 'msg' => lang('not_delete_first_dpn_count_passports_no')));

				$this->load->view('confirmation_message', array('msg' => lang('this_passports_has_compains_are_you_sure_delete'),
                    'back_url' => site_url('ea/group_passports/passports_index/' . $safa_group_id),
                    'confirmation_url' => site_url('ea/group_passports/delete_confirmation/' . $safa_group_id . '/' . $safa_group_passport_id),
                    'model_title' => lang('group_passports_title'), 'action' => lang('global_delete'),
				));
			}
		}
	}

	public function delete_confirmation($safa_group_id = 0, $safa_group_passport_id = 0) {
		$this->safa_group_passports_model->safa_group_passport_id = $safa_group_passport_id;
		if ($this->safa_group_passports_model->delete()) {
			$this->safa_group_passports_model->delete_by_relative_no($safa_group_passport_id);
		}

		$this->load->view('redirect_message', array('msg' => lang('global_deleted_message'),
            'url' => site_url('ea/group_passports/passports_index/' . $safa_group_id),
            'model_title' => lang('group_passports_title'), 'action' => lang('global_delete')));
	}

	public function delete_all() {
		$group_passport_ids = $this->input->post('mutamers');
		if ($this->input->post('safa_group_id'))
		$safa_group_id = $this->input->post('safa_group_id');
		if (isset($group_passport_ids) && count($group_passport_ids) > 0 && is_array($group_passport_ids)) {
			foreach ($group_passport_ids as $safa_group_passport_id) {
				$data_dpncount = $this->safa_group_passports_model->check_delete_dpncount_ability($safa_group_passport_id);
				if ($data_dpncount == 0) {
					$dpn_serial_no = $this->safa_group_passports_model->check_mutamer_dpn_serial_no($safa_group_passport_id);
					if ($dpn_serial_no == 1)
					$this->safa_group_passports_model->update_dpn_count($safa_group_passport_id);
					$data = $this->safa_group_passports_model->check_delete_mehrem_ability($safa_group_passport_id);
					if ($data == 0)
					$unique_mutamers[] = $safa_group_passport_id;
					else
					$has_mutamersWith[] = $safa_group_passport_id;
				}
				else {
					$has_mutamersWith[] = $safa_group_passport_id;
				}
			}
			if (isset($unique_mutamers) && count($unique_mutamers) > 0) {
				$saved = 0;
				foreach ($unique_mutamers as $safa_group_passport_id) {
					$this->safa_group_passports_model->safa_group_passport_id = $safa_group_passport_id;
					if ($this->safa_group_passports_model->delete())
					$saved++;
				}
				$mutamers_info = array();
				if (isset($has_mutamersWith) && count($has_mutamersWith) > 0 && is_array($has_mutamersWith)) {
					foreach ($has_mutamersWith as $has_mutamer) {

						$data_dpncount = $this->safa_group_passports_model->check_delete_dpncount_ability($has_mutamer);
						if ($data_dpncount == 0) {
							$this->safa_group_passports_model->safa_group_passport_id = $has_mutamer;
							$this->safa_group_passports_model->delete();
						} else {
							$this->safa_group_passports_model->safa_group_passport_id = $has_mutamer;
							$mutamers_info[] = $this->safa_group_passports_model->get();
						}
					}
				}
				if ($saved >= 1) {
					if (isset($mutamers_info) && count($mutamers_info) > 0 && is_array($mutamers_info)) {
						if (isset($safa_group_id)) {
							$this->load->view('ea/group_passports/delete_message', array('msg' => lang('delete_group_passports_some_mutamerds_hasMutamers_msg'),
                                'url' => site_url('ea/group_passports/passports_index/' . $safa_group_id . ''),
                                'model_title' => lang('group_passports_title'), 'action' => lang('global_delete'),
                                'exist_mutamers' => $mutamers_info));
						} else {
							$this->load->view('ea/group_passports/delete_message', array('msg' => lang('delete_group_passports_some_mutamerds_hasMutamers_msg'),
                                'url' => site_url('ea/group_passports'),
                                'model_title' => lang('group_passports_title'), 'action' => lang('global_delete'),
                                'exist_mutamers' => $mutamers_info));
						}
					} else {
						if (isset($safa_group_id)) {
							$this->load->view('redirect_message', array('msg' => lang('global_deleted_message'),
                                'url' => site_url('ea/group_passports/passports_index/' . $safa_group_id . ''),
                                'model_title' => lang('group_passports_title'), 'action' => lang('global_delete')));
						} else {
							$this->load->view('redirect_message', array('msg' => lang('global_deleted_message'),
                                'url' => site_url('ea/group_passports'),
                                'model_title' => lang('group_passports_title'), 'action' => lang('global_delete')));
						}
					}
				} else {
					if (isset($safa_group_id)) {
						$this->load->view('redirect_message', array('msg' => lang('global_error_save'),
                            'url' => site_url('ea/group_passports/passports_index/' . $safa_group_id . ''),
                            'model_title' => lang('group_passports_title'), 'action' => lang('global_delete')));
					} else {
						$this->load->view('redirect_message', array('msg' => lang('global_error_save'),
                            'url' => site_url('ea/group_passports'),
                            'model_title' => lang('group_passports_title'), 'action' => lang('global_delete')));
					}
				}
			} else {
				if (isset($safa_group_id)) {
					$this->load->view('redirect_message', array('msg' => lang('not_delete_group_passports_mutamerds_msg'),
                        'url' => site_url('ea/group_passports/passports_index/' . $safa_group_id . ''),
                        'model_title' => lang('group_passports_title'), 'action' => lang('global_delete')));
				} else {
					$this->load->view('redirect_message', array('msg' => lang('not_delete_group_passports_mutamerds_msg'),
                        'url' => site_url('ea/group_passports'),
                        'model_title' => lang('group_passports_title'), 'action' => lang('global_delete')));
				}
			}
		} else {
			if (isset($safa_group_id))
			redirect('ea/group_passports/passports_index/' . $safa_group_id . '');
			else
			redirect('ea/group_passports/index');
		}
	}

	public function search() {

		if ($this->input->get("safa_group_id"))
		$this->safa_group_passports_model->safa_group_id = $this->input->get("safa_group_id");


		if ($this->input->get("mutamer_name")) {

			/*
			 $first_space = stripos($this->input->get("mutamer_name"), " ");
			 $second_space = strrpos($this->input->get("mutamer_name"), " ");
			 $first_name = substr($this->input->get("mutamer_name"), 0, $first_space);
			 $third_name = substr($this->input->get("mutamer_name"), $second_space);
			 if ($this->alpha_dash($first_name)) {
			 $name_la = $this->input->get("mutamer_name");
			 } else {
			 $name_ar = $this->input->get("mutamer_name");
			 }
			 if (!empty($name_la) || isset($name_la)) {
			 $first_space = stripos($name_la, " ");
			 $second_space = strrpos($name_la, " ");
			 $first_name_la = substr($name_la, 0, $first_space);
			 //echo $second_name_la=substr($name_la,$first_space+1,3);
			 $third_name_la = substr($name_la, $second_space);
			 if (!empty($first_name_la) || isset($first_name_la))
			 $this->safa_group_passports_model->first_name_la = $first_name_la;
			 //if (!empty($second_name_la) || isset($second_name_la))
			 //$this->safa_groups_model->second_name_la = $second_name_la;
			 if (!empty($third_name_la) || isset($third_name_la))
			 $this->safa_group_passports_model->third_name_la = $third_name_la;
			 }
			 elseif (!empty($name_ar) || isset($name_ar)) {

			 $first_space = stripos($name_ar, " ");
			 $second_space = strrpos($name_ar, " ");
			 $first_name_ar = substr($name_ar, 0, $first_space);

			 //echo $second_name_ar=substr($name_la,$first_space+1,3);
			 $third_name_ar = substr($name_ar, $second_space);
			 if (!empty($first_name_ar) || isset($first_name_ar))
			 $this->safa_group_passports_model->first_name_ar = $first_name_ar;
			 //if (!empty($second_name_ar) || isset($second_name_ar))
			 //$this->safa_groups_model->second_name_ar = $second_name_ar;
			 if (!empty($third_name_ar) || isset($third_name_ar))
			 $this->safa_group_passports_model->third_name_ar = $third_name_ar;
			 }
			 */


			$this->safa_group_passports_model->db->where("(CONCAT( first_name_ar, ' ', second_name_ar, ' ', third_name_ar, ' ', fourth_name_ar ) LIKE '%" . $this->input->get("mutamer_name") . "%'");
			$this->safa_group_passports_model->db->or_where("CONCAT( first_name_la,  ' ', second_name_la,  ' ', third_name_la,  ' ', fourth_name_la ) LIKE '%" . $this->input->get("mutamer_name") . "%')");
		}
		if ($this->input->get("passport_no"))
		$this->safa_group_passports_model->passport_no = $this->input->get("passport_no");

		if ($this->input->get("nationality_id"))
		$this->safa_group_passports_model->nationality_id = $this->input->get("nationality_id");

		if ($this->input->get("age_from"))
		$this->safa_group_passports_model->age_from = $this->input->get("age_from");

		if ($this->input->get("age_to"))
		$this->safa_group_passports_model->age_to = $this->input->get("age_to");

		if ($this->input->get("relative_no"))
		$this->safa_group_passports_model->relative_no = $this->input->get("relative_no");
	}

	public function check_name($name) {
		if ($this->uri->segment('3') == 'edit')
		$this->db->where('safa_group_id !=', $this->uri->segment('4'));
		$this->db->where('name', $name);
		if ($this->db->get('safa_groups')->num_rows()) {
			$this->form_validation->set_message('check_name_ar', lang('safa_groups_name_exist'));
			return FALSE;
		} else {
			return TRUE;
		}
	}

	public function get_passport_no($passport_no) {
		$this->layout = 'ajax';
		if (!empty($passport_no) || isset($passport_no)) {
			$this->db->select('passport_no');
			$this->db->from('safa_group_passports');
			$this->db->like('passport_no', $passport_no);
			echo json_encode($this->db->get()->result());
		}
	}

	public function mutamer_name() {
		$this->layout = 'ajax';
		$mutamer_name = $this->input->post('searched');
		if ($this->alpha_dash($mutamer_name))
		$name_la = $mutamer_name;
		else
		$name_ar = $mutamer_name;

		if (!empty($name_la) || isset($name_la)) {
			$mutamer_upper = strtoupper($name_la);
			$mutamer_lower = strtolower($name_la);
			$this->db->select('first_name_la,second_name_la,third_name_la,fourth_name_la');
			//            $this->db->like('first_name_la', $mutamer_upper);
			//            $this->db->like('first_name_la', $mutamer_lower);
			$this->db->like('first_name_la', $name_la);
			echo json_encode($this->db->get('safa_group_passports')->result());
		} elseif (!empty($name_ar) || isset($name_ar)) {
			$this->db->select('first_name_ar,second_name_ar,third_name_ar,fourth_name_ar');
			$this->db->like('first_name_ar', $name_ar);
			echo json_encode($this->db->get('safa_group_passports')->result());
		}
	}

	public function toArray($obj) {
		if (is_object($obj))
		$obj = (array) $obj;
		if (is_array($obj)) {
			$new = array();
			foreach ($obj as $key => $val) {
				$new[$key] = $this->toArray($val);
			}
		} else {
			$new = $obj;
		}
		return $new;
	}

	public function alpha_dash($str) {
		if (preg_match("/^([-a-z0-9_-]*$)+$/i", $str)) {
			return TRUE;
		} else {
			return FALSE;
		}
	}

	public function create_contracts_array() {
		$result = $this->safa_group_passports_model->get_ea_contracts();
		$contracts = array();
		$contracts[""] = lang('global_select_from_menu');
		$name = name();
		foreach ($result as $con_id => $contract) {
			$contracts[$contract->contract_id] = $contract->$name;
		}
		return $contracts;
	}

	public function copy_cut_check($safa_group_id, $passport_no) {
		$this->db->where('safa_group_id', $safa_group_id);
		$this->db->where('passport_no', $passport_no);
		if ($this->db->get('safa_group_passports')->num_rows()) {
			return FALSE;
		} else {
			return TRUE;
		}
	}

	public function move_toumrahgroup_check($safa_umrahgroup_id, $passport_no) {
		$this->db->where('safa_umrahgroup_id', $safa_umrahgroup_id);
		$this->db->where('passport_no', $passport_no);
		if ($this->db->get('safa_umrahgroups_passports')->num_rows()) {
			return FALSE;
		} else {
			return TRUE;
		}
	}

	public function get_uo_contract_id($safa_umrahgroup_id) {

		$this->layout = 'ajax';
		$contract_id = $this->safa_group_passports_model->get_safa_uo_contract_id($safa_umrahgroup_id);
		if (isset($contract_id))
		echo $contract_id->safa_uo_contract_id;

		if (!isset($contract_id)) {
			echo "<option value='0'>" . lang('contact_select_from_menu') . "</option>";
		} else {
			echo "<option value='" . $contract_id->safa_uo_contract_id . "'>" . $contract_id->{name()} . "</option>";
		}
	}

	public function getUmrahgroupsByContract() {
		$contract_id = $this->input->post('contract_id');

		$structure = array('safa_umrahgroup_id', 'name');
		$key = $structure['0'];
		if (isset($structure['1'])) {
			$value = $structure['1'];
		}
		$groups_arr = array();
		$groups = $this->safa_group_passports_model->get_safa_umrahgroups($contract_id);
		foreach ($groups as $group) {
			$groups_arr[$group->$key] = $group->$value;
		}


		$form_dropdown = form_dropdown('safa_umrahgroup_id', $groups_arr, set_value('', ''), 'class="chosen-select chosen-rtl input-full"  tabindex="4" ');
		$form_dropdown = str_replace('<select name="safa_umrahgroup_id"  name="s_example" class="select" id="umrahgroup_id" style="width:100%;" >', '', $form_dropdown);
		$form_dropdown = str_replace('</select>', '', $form_dropdown);
		echo $form_dropdown;
		exit;
	}

	public function check_safa_uo_contract_id($safa_uo_contract_id) {
		if ($safa_uo_contract_id == 0) {
			$this->form_validation->set_message('check_safa_uo_contract_id', lang('form_validation_required'));
			return false;
		} else
		return true;
	}

	public function check_relative_no($group_id) {
		$mutamers = unserialize($this->input->post('mutamers'));
		$relative_nos = array();
		$flag = 0;
		foreach ($mutamers as $mutamer) {
			$this->db->select('safa_group_passports.no,safa_group_passports.relative_no,safa_group_passports.safa_group_id,first_name_ar,second_name_ar,
               safa_group_passports.first_name_la,safa_group_passports.second_name_la');
			$this->db->where('safa_group_passports.safa_group_passport_id', $mutamer);
			$row = $this->db->get('safa_group_passports')->row();
			if (!empty($row->no) && $row->relative_no == 0) {

				$this->db->select('safa_group_passports.safa_group_passport_id,');
				$this->db->where('safa_group_passports.safa_group_id', $row->safa_group_id);
				$this->db->where('safa_group_passports.relative_no', $row->no);
				$this->db->where('safa_group_passports.relative_no!=', 0);
				$passport_ids = $this->db->get('safa_group_passports')->result();
				foreach ($passport_ids as $passport_id) {
					if (!in_array($passport_id->safa_group_passport_id, $mutamers)) {
						++$flag;
						if (name() == 'name_ar')
						$relative_nos[] = $row->first_name_ar . ' ' . $row->second_name_ar;
						else
						$relative_nos[] = $row->first_name_la . ' ' . $row->second_name_la;
						break;
					}
				}
			}
			elseif ($row->relative_no != 0) {
				$this->db->select('safa_group_passports.safa_group_passport_id,');
				$this->db->where('safa_group_passports.safa_group_id', $row->safa_group_id);
				$this->db->where('safa_group_passports.no', $row->relative_no);
				$passport_id = $this->db->get('safa_group_passports')->row();

				if (isset($passport_id->safa_group_passport_id)) {
					if (!in_array($passport_id->safa_group_passport_id, $mutamers)) {
						++$flag;
						if (name() == 'name_ar')
						$nos[] = $row->first_name_ar . ' ' . $row->second_name_ar;
						else
						$nos[] = $row->first_name_la . ' ' . $row->second_name_la;
					}
				}
			}
		}
		if ($flag > 0) {
			$msg = '';
			if (isset($relative_nos) && count($relative_nos) == 1) {
				$msg = lang('not_cut') . $relative_nos[0] . lang('without_relative_no');
			} elseif (isset($relative_nos) && count($relative_nos) > 1) {
				$msg = lang('not_cut');
				//foreach (isset($relative_nos) && $relative_nos as $relative) {
				foreach ($relative_nos as $relative) {
					$msg.=$relative . lang('and');
				}
				$msg.=lang('without_relatives_no');
			}
			/////////////////
			if (isset($nos) && count($nos) == 1) {
				$msg = lang('not_cut') . $nos[0] . lang('without_no');
			} elseif (isset($nos) && count($nos) > 1) {
				$msg = lang('not_cut');
				foreach ($nos as $no) {
					$msg.=$no . lang('and');
				}
				$msg.=lang('without_nos');
			}
			$this->form_validation->set_message('check_relative_no', $msg);
			return FALSE;
		} else {
			return TRUE;
		}
	}

	public function check_mutamers_dpncount($group_id) {
		$mutamers = unserialize($this->input->post('mutamers'));
		$relative_nos = array();
		$flag = 0;
		foreach ($mutamers as $mutamer) {
			$this->db->select('safa_group_passports.passport_no,safa_group_passports.passport_dpn_count,safa_group_passports.safa_group_id,safa_group_passports.dpn_serial_no,
              safa_group_passports.first_name_ar,safa_group_passports.second_name_ar,safa_group_passports.first_name_la,safa_group_passports.second_name_la');
			$this->db->where('safa_group_passports.safa_group_passport_id', $mutamer);
			$query = $this->db->get('safa_group_passports')->row();
			if ($query->passport_dpn_count > 0) {

				$this->db->select('safa_group_passports.safa_group_passport_id,safa_group_passports.passport_no,safa_group_passports.passport_dpn_count,safa_group_passports.dpn_serial_no');
				$this->db->where('safa_group_passports.safa_group_id', $query->safa_group_id);
				$this->db->where('safa_group_passports.passport_no', $query->passport_no);
				$dpn_counts = $this->db->get('safa_group_passports')->result();
				//               print_r($dpn_counts);exit();
				foreach ($dpn_counts as $dpn_count) {
					if (!in_array($dpn_count->safa_group_passport_id, $mutamers)) {
						++$flag;
						if (name() == 'name_ar')
						$counts[] = $query->first_name_ar . ' ' . $query->second_name_ar;
						else
						$counts[] = $query->first_name_la . ' ' . $query->second_name_la;
						break;
					}
				}
			}elseif ($query->passport_dpn_count == 0 && $query->dpn_serial_no > 0) {
				$this->db->select('safa_group_passports.safa_group_passport_id,safa_group_passports.passport_no,safa_group_passports.passport_dpn_count,safa_group_passports.dpn_serial_no');
				$this->db->where('safa_group_passports.safa_group_id', $query->safa_group_id);
				$this->db->where('safa_group_passports.passport_no', $query->passport_no);
				$this->db->where('safa_group_passports.dpn_serial_no', 0);
				$dpn_counts = $this->db->get('safa_group_passports')->row();
				if (!in_array($dpn_counts->safa_group_passport_id, $mutamers)) {
					++$flag;
					if (name() == 'name_ar')
					$traveling_with[] = $query->first_name_ar . ' ' . $query->second_name_ar;
					else
					$traveling_with[] = $query->first_name_la . ' ' . $query->second_name_la;
				}
			}
		}
		//        print_r($relative_nos);
		//                exit();
		if ($flag > 0) {
			$msg = '';
			if (isset($counts) && count($counts) == 1) {
				$msg = lang('not_cut') . $counts[0] . ('without_traveling_with');
			} elseif (isset($counts) && count($counts) > 1) {
				$msg = lang('not_cut');
				foreach ($counts as $count) {
					$msg.=$count . lang('and');
				}
				$msg.=('without_travelings_with');
			}
			////////
			if (isset($traveling_with) && count($traveling_with) == 1) {
				$msg = lang('not_cut_traveling_with') . $traveling_with[0] . lang('without_passport_no');
			} elseif (isset($traveling_with) && count($traveling_with) > 1) {
				$msg = lang('not_cut_travelings_with');
				foreach ($traveling_with as $traveling) {
					$msg.=$traveling . lang('and');
				}
				$msg.=lang('without_passports_no');
			}
			$this->form_validation->set_message('check_mutamers_dpncount', $msg);
			return FALSE;
		} else {
			return TRUE;
		}
	}

	public function sortable($group_id) {
		$this->layout = 'ajax';
		$data = $this->safa_group_passports_model->getDisplay_order($group_id);
		foreach ($_POST as $key => $value) {
			$newRank = $value;
			$key = substr($key, 4);
			$currentRank = $data[$key];
			if ($currentRank != $newRank) {
				$this->safa_group_passports_model->switchOrder($key, $newRank);
			}
		}
	}

	public function setRowsCountCookie()
	{
		$ea_user_id = session('ea_user_id');
		$rows_count_per_page = $this->input->post('rows_count_per_page');
		setcookie($ea_user_id . '_group_passports_rows_count_per_page', $rows_count_per_page, time() + 60 * 60 * 24 * 365);
	}

	public function auto_complete() {
		$this->layout = 'ajax';

		$field = $this->input->get('field');
		$field_value = $this->input->get('field_value');

		if (!empty($field_value) || isset($field_value)) {
			$this->db->distinct();
			$this->db->select($field . ' as field_name');
			$this->db->like($field, $field_value, 'after');
			echo json_encode($this->db->get('safa_group_passports')->result());
		}
	}

	function add_flights_popup()
	{
		$this->layout = 'js_new';
			
		$data =array();

		if(session("ea_id")) {
			$this->flight_availabilities_model->safa_ea_id=session("ea_id");
		}

		//$data['results'] = $this->flight_availabilities_model->get_for_trip();


		if (!isset($_POST['smt_save'])) {
			$this->load->view('ea/group_passports/add_flights_popup', $data);
		} else {

			$safa_group_passport_ids_str = $this->input->post('hdn_safa_group_passport_ids');
			$safa_group_passport_ids_arr  = explode(',', $safa_group_passport_ids_str);

			$erp_flight_availability_ids = $this->input->post('erp_flight_availability_ids');


			foreach($safa_group_passport_ids_arr as $safa_group_passport_id) {
				$this->safa_group_passports_flight_availabilties_model->safa_group_passport_id = $safa_group_passport_id;

				foreach ($erp_flight_availability_ids as $erp_flight_availability_id) {
					$this->safa_group_passports_flight_availabilties_model->erp_flight_availabilty_id = $erp_flight_availability_id;

					$safa_group_passports_flight_availabilties_count =$this->safa_group_passports_flight_availabilties_model->get(true);
					if($safa_group_passports_flight_availabilties_count==0) {
						$this->safa_group_passports_flight_availabilties_model->save();
					}
				}
			}

			$this->load->view('redirect_message_popup', array('msg' => lang('flights_added_to_passports_successfully'),
            'url' => site_url("ea/group_passports/add_flights_popup"),
            'id' => $this->uri->segment("4"),
            'model_title' => lang('flights'), 'action' => lang('flights_added')));
		}
	}

	function get_flight_availabilities_by_safa_group_passport_ids()
	{

		$this->layout = 'js_new';
		if(session("ea_id")) {
			$this->flight_availabilities_model->safa_ea_id=session("ea_id");
		}

		$safa_group_passport_ids_str = $this->input->post('safa_group_passport_ids_str');
		$safa_group_passport_ids_arr  = explode(',', $safa_group_passport_ids_str);
		$this->flight_availabilities_model->safa_group_passport_ids_arr = $safa_group_passport_ids_arr;
		$results = $this->flight_availabilities_model->get_for_safa_group_passports();


		echo "
		<table cellpadding='0' class='' cellspacing='0' width='100%' id='tbl_flight_availabilities'>
                <thead>
                    <tr>
                        <th><input type='checkbox' class='checkall' id='checkall' onchange='checkUncheck()'/> </th>
                       
                        <th>". lang('flight_from_to') ."</th>
                        <th>". lang('departure_date') ."</th>
                        <th>". lang('arrival_date') ."</th>
                        <th>". lang('available_seats_count') ."</th>

                    </tr>
                </thead>
                <tbody>";

		if (isset($results)) {
			foreach ($results as $value) {
				$this->flight_availabilities_model->erp_flight_availability_id = $value->erp_flight_availability_id;
				$flight_availabilities_details_rows = $this->flight_availabilities_model->get_details_for_trip();

				$ports='';
				$start_datetime='';
				$end_datetime='';
				$loop_counter=0;

				$erp_port_id_from = 0;
				$erp_port_id_to =0;
				$start_ports_name ='';
				$end_ports_name ='';


				foreach($flight_availabilities_details_rows as $flight_availabilities_details_row) {
					if($loop_counter==0) {

						$erp_port_id_from = $flight_availabilities_details_row->erp_port_id_from;
						$start_ports_name =$flight_availabilities_details_row->start_ports_name;

						$erp_port_id_to = $flight_availabilities_details_row->erp_port_id_to;
						$end_ports_name =$flight_availabilities_details_row->end_ports_name;

						$ports = $flight_availabilities_details_row->start_ports_name.' - '.$flight_availabilities_details_row->end_ports_name;
						$start_datetime = $flight_availabilities_details_row->flight_date .' - '.$flight_availabilities_details_row->departure_time;
						$end_datetime = $flight_availabilities_details_row->flight_date .' - '.$flight_availabilities_details_row->arrival_time;
					} else {
						$erp_port_id_to = $flight_availabilities_details_row->erp_port_id_to;
						$end_ports_name =$flight_availabilities_details_row->end_ports_name;

						$ports = $ports.' <br/> '.$flight_availabilities_details_row->start_ports_name.' - '.$flight_availabilities_details_row->end_ports_name;
						$end_datetime = $flight_availabilities_details_row->flight_date .' - '.$flight_availabilities_details_row->arrival_time;
					}
					$loop_counter++;
				}

				echo "<tr id='row_". $value->erp_flight_availability_id."'>
                            <td>
                            <input type='checkbox' name='erp_flight_availability_ids[]' id='erp_flight_availability_ids[]'  value='". $value->erp_flight_availability_id ."' title=''/>
                           
                            <input type='hidden' name='trip_safa_transporter_id_". $value->erp_flight_availability_id ."'  id='trip_safa_transporter_id_". $value->erp_flight_availability_id ."'  value='". $value->safa_transporter_id ."'/>
                        <input type='hidden' name='trip_safa_transporter_name_". $value->erp_flight_availability_id ."'  id='trip_safa_transporter_name_". $value->erp_flight_availability_id ."'  value='". $value->safa_transporter_name ."'/>
                        
                        
                            </td>
                            
                            
                             
                            <td>
                            
                             
                            <input type='hidden' name='erp_port_id_from_". $value->erp_flight_availability_id ."'  id='erp_port_id_from_". $value->erp_flight_availability_id ."'  value='". $erp_port_id_from ."'/>
                            <input type='hidden' name='erp_port_id_to_". $value->erp_flight_availability_id ."'  id='erp_port_id_to_". $value->erp_flight_availability_id ."'  value='". $erp_port_id_to ."'/>
                            
                            <input type='hidden' name='start_ports_name_". $value->erp_flight_availability_id ."'  id='start_ports_name_". $value->erp_flight_availability_id ."'  value='". $start_ports_name ."'/>
                            <input type='hidden' name='end_ports_name_". $value->erp_flight_availability_id ."'  id='end_ports_name_". $value->erp_flight_availability_id ."'  value='". $end_ports_name ."'/>

                             ". $ports ."
                             <input type='hidden' name='ports_name_". $value->erp_flight_availability_id ."'  id='ports_name_". $value->erp_flight_availability_id ."'  value='". $ports ."'/>
                            
                            </td>
                            
                            
                             <td>
                             
                            <input type='hidden' name='departure_date_". $value->erp_flight_availability_id ."'  id='departure_date_". $value->erp_flight_availability_id ."'  value='". $start_datetime ."'/>
                             ". $start_datetime ."
                            </td>
                            
                            <td>
                            
                            <input type='hidden' name='arrival_date_". $value->erp_flight_availability_id ."'  id='arrival_date_". $value->erp_flight_availability_id ."'  value='". $end_datetime ."'/>
                            ". $end_datetime ."
                             
                            </td>
                            
                            <td>
                            <input type='hidden' name='available_seats_count_". $value->erp_flight_availability_id ."'  id='available_seats_count_". $value->erp_flight_availability_id ."'  value='". $value->available_seats_count ."'/>
                            <input type='text' name='available_seats_count_text_". $value->erp_flight_availability_id ."'  id='available_seats_count_text_". $value->erp_flight_availability_id ."'  value='". $value->available_seats_count ."' readonly='readonly' disabled='disabled'/>
                            
					
                            </td>
                            
                            
                            </tr>";

			}
		}

		echo "</tbody>
            </table>
		";

		exit;
	}



}

/* End of file port_halls.php */
/* Location: ./application/controllers/admin/port_halls.php */



