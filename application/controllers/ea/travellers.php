<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Travellers extends Safa_Controller {

    public $module = "travellers";

    public function __construct() {
        parent::__construct();
        $this->load->model('travellers_model');
        $this->lang->load('travellers');
        permission();
    }

    public function index() {

        if (isset($_POST['search']))
            $this->search();

        $data['travellers'] = $this->travellers_model->get();

        $data["total_rows"] = $this->travellers_model->search(true);

        $this->travellers_model->offset = $this->uri->segment("3");

        $this->travellers_model->limit = 1;

        $data["items"] = $this->travellers_model->search();

        $config['uri_segment'] = 3;

        $config['base_url'] = site_url('travellers/index');

        $config['total_rows'] = $data["total_rows"];

        $config['per_page'] = $this->travellers_model->limit;

        //$config['suffix'] = $this->input->post() ? '?'.http_build_query($_post) : NULL;

        $this->load->library('pagination');

        $this->pagination->initialize($config);

        $data['pagination'] = $this->pagination->create_links();

        $this->load->view('ea/travellers/index', $data);
    }

    public function add_passport($trip_id) {

        $this->load->library("form_validation");

        $this->form_validation->set_rules("erp_title_id", 'lang:travellers_title_id', "required");

        $data = array();

        $data['relative_persons'][''] = lang('global_select_from_menu');

        foreach ($this->travellers_model->post_related_persons($trip_id) as $val) {

            $data['relative_persons'][$val['safa_trip_traveller_id']] = $val['first_' . name()] . ' ' . $val['second_' . name()] . ' ' . $val['third_' . name()] . ' ' . $val['fourth_' . name()];
        }


        if ($this->form_validation->run() == false) {

            $this->load->view("ea/travellers/travellers_view", $data);
        } else {

            $this->travellers_model->safa_trip_id = $trip_id;

            $this->travellers_model->offline_id = NULL;

            $this->travellers_model->no = $this->travellers_model->calc_next_no_field($this->input->post('safa_trip_id'));

            $this->travellers_model->display_order = NULL;

            $this->travellers_model->title_id = $this->input->post('erp_title_id');

            $this->travellers_model->first_name_la = $this->input->post('first_name_la');

            $this->travellers_model->second_name_la = $this->input->post('second_name_la');

            $this->travellers_model->third_name_la = $this->input->post('third_name_la');

            $this->travellers_model->fourth_name_la = $this->input->post('fourth_name_la');

            $this->travellers_model->first_name_ar = $this->input->post('first_name_ar');

            $this->travellers_model->second_name_ar = $this->input->post('second_name_ar');

            $this->travellers_model->third_name_ar = $this->input->post('third_name_ar');

            $this->travellers_model->fourth_name_ar = $this->input->post('fourth_name_ar');



            $this->travellers_model->nationality_id = $this->input->post('nationality_id');

            $this->travellers_model->previous_nationality_id = $this->input->post('previous_nationality_id');

            $this->travellers_model->passport_no = $this->input->post('passport_no');

            $this->travellers_model->passport_type_id = $this->input->post('passport_type_id');

            $this->travellers_model->passport_issue_date = $this->input->post('passport_issue_date');

            $this->travellers_model->passport_expiry_date = $this->input->post('passport_expiry_date');

            $this->travellers_model->passport_issuing_city = $this->input->post('passport_issuing_city');

            $this->travellers_model->passport_issuing_country_id = $this->input->post('passport_issuing_country_id');

            $this->travellers_model->passport_dpn_count = $this->input->post('passport_dpn_count');

            $this->travellers_model->dpn_serial_no = $this->input->post('dpn_serial_no');

            $this->travellers_model->relative_relation_id = $this->input->post('relative_relation_id');

            $this->travellers_model->educational_level_id = $this->input->post('educational_level_id');

            $this->travellers_model->occupation = $this->input->post('occupation');

            $this->travellers_model->email_address = $this->input->post('email_address');

            $this->travellers_model->street_name = $this->input->post('street_name');

            $this->travellers_model->date_of_birth = $this->input->post('date_of_birth');

            $this->travellers_model->age = $this->input->post('age');

            $this->travellers_model->birth_city = $this->input->post('birth_city');

            $this->travellers_model->birth_country_id = $this->input->post('birth_country_id');

            $this->travellers_model->relative_no = $this->input->post('relative_no');

            $this->travellers_model->relative_gender_id = $this->input->post('relative_gender_id');

            $this->travellers_model->gender_id = $this->input->post('gender_id');

            $this->travellers_model->marital_status_id = $this->input->post('marital_status_id');

            $this->travellers_model->religion = $this->input->post('religion');

            $this->travellers_model->zip_code = $this->input->post('zip_code');

            $this->travellers_model->city = $this->input->post('city');

            $this->travellers_model->region_name = $this->input->post('region_name');

            $this->travellers_model->erp_country_id = $this->input->post('erp_country_id');

            $this->travellers_model->mofa = $this->input->post('mofa');

            $this->travellers_model->enumber = $this->input->post('enumber');

            $this->travellers_model->id_number = $this->input->post('id_number');

            $this->travellers_model->job2 = $this->input->post('job2');

            $this->travellers_model->address = $this->input->post('address');

            $this->travellers_model->remarks = $this->input->post('remarks');

            $this->travellers_model->canceled = $this->input->post('canceled');

            $this->travellers_model->mobile = $this->input->post('mobile');

            $this->travellers_model->sim_serial = $this->input->post('sim_serial');

            $this->travellers_model->visa_number = $this->input->post('visa_number');

            $this->travellers_model->notes = $this->input->post('notes');

            $this->travellers_model->entry_datetime = $this->input->post('entry_datetime');

            $this->travellers_model->erp_meal_id = $this->input->post('erp_meal_id');


            $config['upload_path'] = './static/uploads/';
            $config['allowed_types'] = 'gif|jpg|png';
            $this->load->library('upload', $config);

            if ($this->upload->do_upload("picture")) {
                $data = $this->upload->data();
            }
            //print_r($this->upload->display_errors());
            $this->travellers_model->picture = $data['file_name'];

            $this->travellers_model->save();


            redirect("ea/travellers/index");
        }
    }

    public function edit($id) {

        if (!$id)
            show_404();

        $this->travellers_model->safa_trip_traveller_id = $id;
        $data['item'] = $this->travellers_model->post();

        if (!$data['item'])
            show_404();

        $this->load->library("form_validation");



        if ($this->form_validation->run() == false) {
            $this->load->view("ea/travellers/edit", $data);
        } else {
            $this->travellers_model->safa_trip_traveller_id = $id;
            $this->travellers_model->safa_trip_id = $this->input->post('safa_trip_id');
            $this->travellers_model->offline_id = $this->input->post('offline_id');
            $this->travellers_model->no = $this->input->post('no');
            $this->travellers_model->display_order = $this->input->post('display_order');
            $this->travellers_model->title_id = $this->input->post('title_id');
            $this->travellers_model->first_name_la = $this->input->post('first_name_la');
            $this->travellers_model->second_name_la = $this->input->post('second_name_la');
            $this->travellers_model->third_name_la = $this->input->post('third_name_la');
            $this->travellers_model->fourth_name_la = $this->input->post('fourth_name_la');
            $this->travellers_model->first_name_ar = $this->input->post('first_name_ar');
            $this->travellers_model->second_name_ar = $this->input->post('second_name_ar');
            $this->travellers_model->third_name_ar = $this->input->post('third_name_ar');
            $this->travellers_model->fourth_name_ar = $this->input->post('fourth_name_ar');
            $this->travellers_model->picture = $this->input->post('picture');
            $this->travellers_model->nationality_id = $this->input->post('nationality_id');
            $this->travellers_model->previous_nationality_id = $this->input->post('previous_nationality_id');
            $this->travellers_model->passport_no = $this->input->post('passport_no');
            $this->travellers_model->passport_type_id = $this->input->post('passport_type_id');
            $this->travellers_model->passport_issue_date = $this->input->post('passport_issue_date');
            $this->travellers_model->passport_expiry_date = $this->input->post('passport_expiry_date');
            $this->travellers_model->passport_issuing_city = $this->input->post('passport_issuing_city');
            $this->travellers_model->passport_issuing_country_id = $this->input->post('passport_issuing_country_id');
            $this->travellers_model->passport_dpn_count = $this->input->post('passport_dpn_count');
            $this->travellers_model->dpn_serial_no = $this->input->post('dpn_serial_no');
            $this->travellers_model->relative_relation_id = $this->input->post('relative_relation_id');
            $this->travellers_model->educational_level_id = $this->input->post('educational_level_id');
            $this->travellers_model->occupation = $this->input->post('occupation');
            $this->travellers_model->email_address = $this->input->post('email_address');
            $this->travellers_model->street_name = $this->input->post('street_name');
            $this->travellers_model->date_of_birth = $this->input->post('date_of_birth');
            $this->travellers_model->age = $this->input->post('age');
            $this->travellers_model->birth_city = $this->input->post('birth_city');
            $this->travellers_model->birth_country_id = $this->input->post('birth_country_id');
            $this->travellers_model->relative_no = $this->input->post('relative_no');
            $this->travellers_model->relative_gender_id = $this->input->post('relative_gender_id');
            $this->travellers_model->gender_id = $this->input->post('gender_id');
            $this->travellers_model->marital_status_id = $this->input->post('marital_status_id');
            $this->travellers_model->religion = $this->input->post('religion');
            $this->travellers_model->zip_code = $this->input->post('zip_code');
            $this->travellers_model->city = $this->input->post('city');
            $this->travellers_model->region_name = $this->input->post('region_name');
            $this->travellers_model->erp_country_id = $this->input->post('erp_country_id');
            $this->travellers_model->mofa = $this->input->post('mofa');
            $this->travellers_model->enumber = $this->input->post('enumber');
            $this->travellers_model->id_number = $this->input->post('id_number');
            $this->travellers_model->job2 = $this->input->post('job2');
            $this->travellers_model->address = $this->input->post('address');
            $this->travellers_model->remarks = $this->input->post('remarks');
            $this->travellers_model->canceled = $this->input->post('canceled');
            $this->travellers_model->mobile = $this->input->post('mobile');
            $this->travellers_model->sim_serial = $this->input->post('sim_serial');
            $this->travellers_model->visa_number = $this->input->post('visa_number');
            $this->travellers_model->notes = $this->input->post('notes');
            $this->travellers_model->entry_datetime = $this->input->post('entry_datetime');
            $this->travellers_model->erp_meal_id = $this->input->post('erp_meal_id');


            $this->travellers_model->save();

            redirect("travellers/index");
        }
    }

    public function delete($id) {
        if (!$id)
            show_404();

        $this->travellers_model->safa_trip_traveller_id = $id;

        if (!$this->travellers_model->delete())
            show_404();
        redirect("travellers/index");
    }

    public function delete_all() {
        if ($this->input->post("delete_items")) {
            foreach ($this->input->post("delete_items") as $item) {
                $this->travellers_model->safa_trip_traveller_id = $item;

                $this->travellers_model->delete();
            }
        } else
            show_404();
        redirect("travellers/index");
    }

    public function search() {

        if ($this->input->post("safa_trip_traveller_id"))
            $this->travellers_model->safa_trip_traveller_id = $this->input->post("safa_trip_traveller_id");

        if ($this->input->post("safa_trip_id"))
            $this->travellers_model->safa_trip_id = $this->input->post("safa_trip_id");

        if ($this->input->post("offline_id"))
            $this->travellers_model->offline_id = $this->input->post("offline_id");

        if ($this->input->post("no"))
            $this->travellers_model->no = $this->input->post("no");

        if ($this->input->post("display_order"))
            $this->travellers_model->display_order = $this->input->post("display_order");


        if ($this->input->post("title_id"))
            $this->travellers_model->title_id = $this->input->post("title_id");


        if ($this->input->post("first_name_la"))
            $this->travellers_model->first_name_la = $this->input->post("first_name_la");


        if ($this->input->post("second_name_la"))
            $this->travellers_model->second_name_la = $this->input->post("second_name_la");




        if ($this->input->post("third_name_la"))
            $this->travellers_model->third_name_la = $this->input->post("third_name_la");


        if ($this->input->post("fourth_name_la"))
            $this->travellers_model->fourth_name_la = $this->input->post("fourth_name_la");

        if ($this->input->post("first_name_ar")) {
            $this->travellers_model->first_name_ar = $this->input->post("first_name_ar");
        }


        if ($this->input->post("second_name_ar"))
            $this->travellers_model->second_name_ar = $this->input->post("second_name_ar");


        if ($this->input->post("third_name_ar"))
            $this->travellers_model->third_name_ar = $this->input->post("third_name_ar");


        if ($this->input->post("fourth_name_ar"))
            $this->travellers_model->fourth_name_ar = $this->input->post("fourth_name_ar");


        if ($this->input->post("picture"))
            $this->travellers_model->picture = $this->input->post("picture");


        if ($this->input->post("nationality_id"))
            $this->travellers_model->nationality_id = $this->input->post("nationality_id");


        if ($this->input->post("previous_nationality_id"))
            $this->travellers_model->previous_nationality_id = $this->input->post("previous_nationality_id");


        if ($this->input->post("passport_no"))
            $this->travellers_model->passport_no = $this->input->post("passport_no");


        if ($this->input->post("passport_type_id"))
            $this->travellers_model->passport_type_id = $this->input->post("passport_type_id");


        if ($this->input->post("passport_issue_date"))
            $this->travellers_model->passport_issue_date = $this->input->post("passport_issue_date");


        if ($this->input->post("passport_expiry_date"))
            $this->travellers_model->passport_expiry_date = $this->input->post("passport_expiry_date");


        if ($this->input->post("passport_issuing_city"))
            $this->travellers_model->passport_issuing_city = $this->input->post("passport_issuing_city");


        if ($this->input->post("passport_issuing_country_id"))
            $this->travellers_model->passport_issuing_country_id = $this->input->post("passport_issuing_country_id");


        if ($this->input->post("passport_dpn_count"))
            $this->travellers_model->passport_dpn_count = $this->input->post("passport_dpn_count");


        if ($this->input->post("dpn_serial_no"))
            $this->travellers_model->dpn_serial_no = $this->input->post("dpn_serial_no");


        if ($this->input->post("relative_relation_id"))
            $this->travellers_model->relative_relation_id = $this->input->post("relative_relation_id");


        if ($this->input->post("educational_level_id"))
            $this->travellers_model->educational_level_id = $this->input->post("educational_level_id");


        if ($this->input->post("occupation"))
            $this->travellers_model->occupation = $this->input->post("occupation");


        if ($this->input->post("email_address"))
            $this->travellers_model->email_address = $this->input->post("email_address");


        if ($this->input->post("street_name"))
            $this->travellers_model->street_name = $this->input->post("street_name");


        if ($this->input->post("date_of_birth"))
            $this->travellers_model->date_of_birth = $this->input->post("date_of_birth");


        if ($this->input->post("age"))
            $this->travellers_model->age = $this->input->post("age");


        if ($this->input->post("birth_city"))
            $this->travellers_model->birth_city = $this->input->post("birth_city");


        if ($this->input->post("birth_country_id"))
            $this->travellers_model->birth_country_id = $this->input->post("birth_country_id");


        if ($this->input->post("relative_no"))
            $this->travellers_model->relative_no = $this->input->post("relative_no");


        if ($this->input->post("relative_gender_id"))
            $this->travellers_model->relative_gender_id = $this->input->post("relative_gender_id");


        if ($this->input->post("gender_id"))
            $this->travellers_model->gender_id = $this->input->post("gender_id");


        if ($this->input->post("marital_status_id"))
            $this->travellers_model->marital_status_id = $this->input->post("marital_status_id");


        if ($this->input->post("religion"))
            $this->travellers_model->religion = $this->input->post("religion");


        if ($this->input->post("zip_code"))
            $this->travellers_model->zip_code = $this->input->post("zip_code");


        if ($this->input->post("city"))
            $this->travellers_model->city = $this->input->post("city");


        if ($this->input->post("region_name"))
            $this->travellers_model->region_name = $this->input->post("region_name");


        if ($this->input->post("erp_country_id"))
            $this->travellers_model->erp_country_id = $this->input->post("erp_country_id");


        if ($this->input->post("mofa"))
            $this->travellers_model->mofa = $this->input->post("mofa");


        if ($this->input->post("enumber"))
            $this->travellers_model->enumber = $this->input->post("enumber");


        if ($this->input->post("id_number"))
            $this->travellers_model->id_number = $this->input->post("id_number");


        if ($this->input->post("job2"))
            $this->travellers_model->job2 = $this->input->post("job2");


        if ($this->input->post("address"))
            $this->travellers_model->address = $this->input->post("address");


        if ($this->input->post("remarks"))
            $this->travellers_model->remarks = $this->input->post("remarks");


        if ($this->input->post("canceled"))
            $this->travellers_model->canceled = $this->input->post("canceled");


        if ($this->input->post("mobile"))
            $this->travellers_model->mobile = $this->input->post("mobile");


        if ($this->input->post("sim_serial"))
            $this->travellers_model->sim_serial = $this->input->post("sim_serial");


        if ($this->input->post("visa_number"))
            $this->travellers_model->visa_number = $this->input->post("visa_number");


        if ($this->input->post("notes"))
            $this->travellers_model->notes = $this->input->post("notes");


        if ($this->input->post("entry_datetime"))
            $this->travellers_model->entry_datetime = $this->input->post("entry_datetime");


        if ($this->input->post("erp_meal_id"))
            $this->travellers_model->erp_meal_id = $this->input->post("erp_meal_id");
    }

}

/* End of file travellers.php */
/* Location: ./application/controllers/travellers.php */