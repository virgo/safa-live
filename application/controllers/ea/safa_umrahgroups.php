<?php

class Safa_umrahgroups extends Safa_Controller {

    public $module = "safa_umrahgroups";
    public $mutamers = array();
    
    public function __construct() {
        parent::__construct();
        $this->layout = 'new';
        //Side menu session, By Gouda.
        session('side_menu_id', 5);
        $this->load->model('safa_umrahgroups_model');
        $this->lang->load('safa_umrahgroups');
        $this->lang->load('group_passports');
        $this->lang->load('safa_groups');
        permission();
        if (session('ea_id'))
            $this->safa_umrahgroups_model->ea_id = session('ea_id');
    }

    public function index() {
        //By Gouda

        $contracts = $this->create_contracts_array_with_first_option();
        $data["uo_contracts"] = $contracts;
        $data["safa_umrahgroups_status"] = ddgen('safa_umrahgroups_status', array('safa_umrahgroups_status_id', name()), FALSE, FALSE, false);

        if (isset($_GET['search'])) {
            if ($this->input->get("mutamer_name") || $this->input->get("passport_no") || $this->input->get("nationality_id") || $this->input->get("age") || $this->input->get("relative_no")) {

                $mutamer_name = $this->input->get("mutamer_name");
                $passport_no = $this->input->get("passport_no");
                $nationality_id = $this->input->get("nationality_id");
                $age = $this->input->get("age");
                $relative_no = $this->input->get("relative_no");
                $safa_group_id = $this->input->get("safa_group_id");

                redirect("ea/umrahgroups_passports/passports_index/0?search=&mutamer_name=$mutamer_name&passport_no=$passport_no&nationality_id=$nationality_id&age=$age&relative_no=$relative_no&safa_group_id=$safa_group_id");
            } else {
                $this->search();
            }
        }

        $this->safa_umrahgroups_model->join = TRUE;
        $data["total_rows"] = $this->safa_umrahgroups_model->get(true);
        $this->safa_umrahgroups_model->offset = $this->uri->segment("4");
        //$this->safa_umrahgroups_model->limit = $this->config->item('per_page');
        $data["items"] = $this->safa_umrahgroups_model->get();
        $config['uri_segment'] = 4;
        $config['base_url'] = site_url('ea/safa_umrahgroups/index');
        $config['total_rows'] = $data["total_rows"];
        $config['per_page'] = $this->safa_umrahgroups_model->limit;
        $config['suffix'] = $this->input->get() ? '?' . http_build_query($_GET) : NULL;
        $this->load->library('pagination');
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();
        $this->load->view('ea/safa_umrahgroups/index', $data);
    }

    public function add() {
        $data["uo_contracts"] = $this->create_contracts_array();
        $data["safa_umrahgroups_status"] = ddgen('safa_umrahgroups_status', array('safa_umrahgroups_status_id', name()), FALSE, FALSE, TRUE);
        $this->load->library("form_validation");
        $this->form_validation->set_rules('name', 'lang:safa_umrahgroups_name', 'trim|required|unique_col[safa_umrahgroups.name]');
        $this->form_validation->set_rules('creation_date', 'lang:safa_umrahgroups_creation_date', 'trim|required');
        $this->form_validation->set_rules('safa_uo_contract_id', 'lang:safa_umrahgroups_contract_id', 'trim|required');
        $this->form_validation->set_rules('safa_umrahgroups_status_id', 'lang:safa_umrahgroups_status_id', 'trim|required');



        if ($this->form_validation->run() == false) {
            $this->load->view("ea/safa_umrahgroups/add", $data);
        } else {
            $this->safa_umrahgroups_model->name = $this->input->post('name');
            $this->safa_umrahgroups_model->creation_date = $this->input->post('creation_date');
            $this->safa_umrahgroups_model->safa_ea_id = session('ea_id');
            $this->safa_umrahgroups_model->safa_uo_contract_id = $this->input->post('safa_uo_contract_id');
            $this->safa_umrahgroups_model->safa_umrahgroups_status_id = $this->input->post('safa_umrahgroups_status_id');

            $this->safa_umrahgroups_model->save();
            $this->load->view('redirect_message', array('msg' => lang('global_added_message'),
                'url' => site_url('ea/safa_umrahgroups'),
                'model_title' => lang('safa_umrahgroups_title'), 'action' => lang('safa_umrahgroups_add_title')));
        }
    }

    public function edit($id = FALSE) {
        if (!$id)
            show_404();
        $this->safa_umrahgroups_model->safa_umrahgroup_id = $id;
        $data['item'] = $this->safa_umrahgroups_model->get();

        if (!$data['item'])
            show_404();
        $data["uo_contracts"] = $this->create_contracts_array();
        $data["safa_umrahgroups_status"] = ddgen('safa_umrahgroups_status', array('safa_umrahgroups_status_id', name()), FALSE, FALSE, TRUE);

        $this->load->library("form_validation");
        $this->form_validation->set_rules('name', 'lang:safa_umrahgroups_name', 'trim|required|callback_check_name');
        $this->form_validation->set_rules('creation_date', 'lang:safa_umrahgroups_creation_date', 'trim|required');
        $this->form_validation->set_rules('safa_uo_contract_id', 'lang:safa_umrahgroups_contract_id', 'trim|required');
        $this->form_validation->set_rules('safa_umrahgroups_status_id', 'lang:safa_umrahgroups_status_id', 'trim|required');


        if ($this->form_validation->run() == false) {
            $this->load->view("ea/safa_umrahgroups/edit", $data);
        } else {
            $this->safa_umrahgroups_model->name = $this->input->post('name');
            $this->safa_umrahgroups_model->creation_date = $this->input->post('creation_date');
            $this->safa_umrahgroups_model->safa_ea_id = session('ea_id');
            $this->safa_umrahgroups_model->safa_uo_contract_id = $this->input->post('safa_uo_contract_id');
            $this->safa_umrahgroups_model->safa_umrahgroups_status_id = $this->input->post('safa_umrahgroups_status_id');

            $this->safa_umrahgroups_model->save();
            $this->load->view('redirect_message', array('msg' => lang('global_updated_message'),
                'url' => site_url('ea/safa_umrahgroups'),
                'id' => $id,
                'model_title' => lang('safa_umrahgroups_title'), 'action' => lang('safa_umrahgroups_edit_title')));
        }
    }

    public function delete($id = FALSE) {
        $this->layout = 'ajax';
        if (!$this->input->is_ajax_request()) {
            redirect('ea/safa_umrahgroups/index');
        } else {
            $safa_umrahgroup_id = $this->input->post('safa_umrahgroup_id');
            $data = $this->safa_umrahgroups_model->check_delete_ability($safa_umrahgroup_id);

            if ($data == 0) {
                $this->safa_umrahgroups_model->safa_umrahgroup_id = $safa_umrahgroup_id;
                if ($this->safa_umrahgroups_model->delete())
                    echo json_encode(array('response' => TRUE, 'msg' => 'deleted'));
            }
            else {
                echo json_encode(array('response' => FALSE, 'msg' => 'not deleted'));
            }
        }
    }

    public function search() {

        if ($this->input->get("from_creation_date"))
            $this->safa_umrahgroups_model->from_creation_date = $this->input->get("from_creation_date");

        if ($this->input->get("to_creation_date"))
            $this->safa_umrahgroups_model->to_creation_date = $this->input->get("to_creation_date");

        if ($this->input->get("safa_umrahgroup_id")) {

            $this->safa_umrahgroups_model->safa_umrahgroup_id = $this->input->get("safa_umrahgroup_id");
            $safa_umrahgroups_row = $this->safa_umrahgroups_model->get();
            $this->safa_umrahgroups_model->safa_umrahgroup_id = false;
			
            if(count($safa_umrahgroups_row)>0) {
            $this->safa_umrahgroups_model->name = $safa_umrahgroups_row->name;
            }
        }
        if ($this->input->get("safa_uo_contract_id")) {
            $this->safa_umrahgroups_model->safa_uo_contract_id = $this->input->get("safa_uo_contract_id");
        }
        if ($this->input->get("safa_umrahgroups_status_id")) {
            $this->safa_umrahgroups_model->safa_umrahgroups_status_id = $this->input->get("safa_umrahgroups_status_id");
        }
        if ($this->input->get("safa_umrahgroups_no_of_mutamers_from")) {
            $this->safa_umrahgroups_model->no_of_mutamers_from = $this->input->get("safa_umrahgroups_no_of_mutamers_from");
        }
        if ($this->input->get("safa_umrahgroups_no_of_mutamers_to")) {
            $this->safa_umrahgroups_model->no_of_mutamers_to = $this->input->get("safa_umrahgroups_no_of_mutamers_to");
        }
    }

    function check_name($name) {
        if ($this->uri->segment('3') == 'edit')
            $this->db->where('safa_umrahgroup_id !=', $this->uri->segment('4'));
        $this->db->where('name', $name);
        if ($this->db->get('safa_umrahgroups')->num_rows()) {
            $this->form_validation->set_message('check_name', lang('safa_umrahgroups_name_exist'));
            return FALSE;
        } else {
            return TRUE;
        }
    }

    function create_contracts_array() {
        $result = $this->safa_umrahgroups_model->get_ea_contracts();
        $contracts = array();

        //By Gouda.
        //$contracts[""] = lang('global_select_from_menu');
        foreach ($result as $con_id => $contract) {
            $contracts[$contract->contract_id] = $contract->safa_uo_contracts_eas_name;
        }
        return $contracts;
    }

    function create_contracts_array_with_first_option() {
        $result = $this->safa_umrahgroups_model->get_ea_contracts();
        $contracts = array();

        $contracts[""] = lang('global_select_from_menu');
        //$name = name();
        foreach ($result as $con_id => $contract) {
            $contracts[$contract->contract_id] = $contract->safa_uo_contracts_eas_name;
        }
        return $contracts;
    }

    /**
     * Babelumrah WS
     * @author Muhammad El-Saeed <m.elsaeed@virgotel.com>
     * @purpose Bab El-Umrah WS
     */
    public function send_to_babelumrah($id = FALSE) {
        if (!$id)
            show_404();
        $this->layout = 'js_new';
        $this->load->library('babelumrah');
        $group = $this->db->where('safa_umrahgroup_id', $id)->get('safa_umrahgroups')->row();
        $ea = $this->db->where('safa_ea_id', session('ea_id'))->get('safa_eas')->row();
        $data['consulates'] = ddgen('erp_embassies', array('code', name()), array('erp_country_id' => $ea->erp_country_id)); // HIS COUNTRY CONSULATES
        $data['packages'] = ddgen('yahajj_packages', array('yahajj_package_id', strtoupper('PG_' . name()), 'safa_uo_contract_id' => $group->safa_uo_contract_id)); // HIS COUNTRY CONSULATES
        $data['contract'] = $group->safa_uo_contract_id;
        $this->load->library('form_validation');
        $this->form_validation->set_rules('start_date', lang('safa_umrahgroups_start_date'), 'trim|required');
        $this->form_validation->set_rules('end_date', lang('safa_umrahgroups_end_date'), 'trim|required');
        $this->form_validation->set_rules('package', lang('safa_umrahgroups_package'), 'trim|required');
        $this->form_validation->set_rules('consulate', lang('safa_umrahgroups_consulate'), 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $this->load->view('ea/safa_umrahgroups/babelumrah', $data);
        } else {
            // SET UP ENVIRONMENT 
            $this->layout = 'ajax';
            $this->load->library('structure');
            $group_info = $this->safa_umrahgroups_model->get_group_info($id, session('ea_id'));
            if (!$group_info)
                show_404();
            $travellers = $this->db->where('safa_umrahgroup_id', $id)->get('safa_umrahgroups_passports')->result_array();
            $this->relative($travellers);
            $statistics = $this->db->select(
                            "COUNT(*) AS total_passprots"
                            . ", (SELECT SUM(passport_dpn_count) FROM safa_umrahgroups_passports WHERE safa_umrahgroup_id = $id) AS companions"
                            . ", (SELECT COUNT(*) FROM safa_umrahgroups_passports WHERE safa_umrahgroup_id = $id AND gender_id = '1') AS men"
                            . ", (SELECT COUNT(*) FROM safa_umrahgroups_passports WHERE safa_umrahgroup_id = $id AND gender_id = '2') AS women"
                            . ", (SELECT COUNT(*) FROM safa_umrahgroups_passports WHERE safa_umrahgroup_id = $id AND relative_no != '0') AS has_mahram"
                            . ", (SELECT DISTINCT COUNT(relative_no) FROM safa_umrahgroups_passports WHERE safa_umrahgroup_id = $id AND relative_no != '0') AS mahrams"
                    )->where('safa_umrahgroup_id', $id)->get('safa_umrahgroups_passports')->row();
            $consulate = $this->db->where('erp_embassy_id', $this->input->post('consulate'))->get('erp_embassies')->row();
            $package = $this->db->where('yahajj_package_id', $this->input->post('package'))->get('yahajj_packages')->row();

            // NUMBER OF MAHRAMS
            $num_of_mahrams = $statistics->mahrams;
            // NUMBER OF MUTAMERS
            $num_of_mutamers = $statistics->total_passprots;
            // NUMBER OF COMPANIONS
            $num_of_companions = $statistics->companions;
            // NUMBER OF WHO'S HAS MAHRAM
            $num_of_has_mahram = $statistics->has_mahram;
            // NUMBER OF WOMEN
            $num_of_women = $statistics->women;

            // UO ACCESS INFO ON BAB EL UMRAH WEB SERVICE
            $this->babelumrah->auto_setup($group->safa_uo_contract_id);
            if( ! $this->babelumrah->Auth())
                show_error (lang('global_you_dont_have_avalid_uasp_credintials'));

            $GROUP['GR_GROUP_NAME'] = $group_info->name;
            $GROUP['GR_INTERNAL_ID'] = $group_info->safa_umrahgroup_id;
            $GROUP['GR_YEAR'] = date('Y');
            $GROUP['GR_NUMBER_MUHRAM'] = 0;//$num_of_mahrams;
            $GROUP['GR_NUMBER_MUTAMERS'] = $num_of_mutamers;
            $GROUP['GR_NUMBER_COMPANIONS'] = $num_of_companions;
            $GROUP['GR_WOMEN'] = 0;//$num_of_women;
            $GROUP['GR_HASMUHRAM'] = 0;//$num_of_has_mahram;
            $GROUP['GR_ID'] = 0;//$id;
            $GROUP['GR_EXPECTED_DEPARTURE_DATE'] = $this->input->post('end_date');
            $GROUP['GR_EXPECTED_ARRIVAL_DATE'] = $this->input->post('start_date');
            $GROUP['GR_CONSULATE_NAME'] = $consulate->{name()};
            $GROUP['GR_CONSULATE_ID'] = $this->input->post('consulate');
            $GROUP['GR_STARTING_DATE'] = $package->PG_DATE_START;
            $GROUP['GR_ENDING_DATE'] = $package->PG_DATE_END;
            $GROUP['GR_DURATION'] = $package->PG_DURATION;
            $GROUP['GR_GROUP_STATUS'] = 1;
            $GROUP['GR_SEND_HOURS'] = 0;
            $GROUP['GR_PACKAGE_CODE'] = $package->PG_NO;
            $GROUP['GR_PACKAGE_NAME'] = $package->PG_NAME;
            $mutamer = $this->structure->convert($travellers, 4, 1);
            $res = $this->babelumrah->InsertGroup($GROUP, $mutamer);
            $res = Xmltoarray::createArray($res);
            if(isset($res['Result']['Reason']['@attributes']['errDesc']))
                echo $res['Result']['Reason']['@attributes']['errDesc'];
            elseif(isset($res['Result']))
            {
                
                if(isset($res['Result']['G']['@attributes']['OFID']))
                {
                    $this->db->where('safa_umrahgroup_id', $res['Result']['G']['@attributes']['OFID'])->update('safa_umrahgroups', array(
                        'uasp_id' => $res['Result']['G']['@attributes']['ONID']
                    ));
                    if(isset($res['Result']['G']['M']))
                    {
                        foreach($res['Result']['G']['M'] as $mutamer)
                        {
                            $this->db->where('safa_umrahgroup_id', $res['Result']['G']['@attributes']['OFID'])
                                     ->where('no', $mutamer['@attributes']['OFID'])
                                     ->update('safa_umrahgroups_passports', array(
                                         'uasp_id' => $mutamer['@attributes']['ONID']
                                     ));
                        }
                    }
                }
                echo "<script>parent.$.fancybox.close();</script>";
            }
        }
    }

    public function getPackages($contract_id = false) {
        if (!$contract_id)
            show_404();
        $this->layout = 'ajax';
        $this->db->where('safa_ea_id', session('ea_id'))->where('safa_uo_contract_id', $contract_id)->delete('yahajj_packages');
        $this->load->library('babelumrah');
        $this->babelumrah->auto_setup($contract_id); //'ea_admin', 'ea12345#', '88888'
        if (!$this->babelumrah->Auth())
            die(lang('invalid_babelumrah_access_info'));
        $lookup = $this->babelumrah->GetLookups();
//        print_r($lookup['soap:Envelope']['soap:Body']['GetLookupsResponse']['GetLookupsResult']['diffgr:diffgram']['DataSetLookups']['YAHAJJ_PACKAGE_DETAILS']);
        foreach ($lookup['soap:Envelope']['soap:Body']['GetLookupsResponse']['GetLookupsResult']['diffgr:diffgram']['DataSetLookups']['YAHAJJ_PACKAGES'] as $package) {
            $this->db->insert('yahajj_packages', array(
                'PG_NO' => $package['PG_NO'],
                'PG_NAME_AR' => $package['PG_NAME_AR'],
                'PG_DURATION' => $package['PG_DURATION'],
                'PG_DATE_START' => $package['PG_DATE_START'],
                'PG_DATE_END' => $package['PG_DATE_END'],
                'PG_PRICE' => $package['PG_PRICE'],
                'PG_NAME_LA' => $package['PG_NAME_LA'],
                'PG_NAME' => $package['PG_NAME'],
                'safa_ea_id' => session('ea_id'),
                'safa_uo_contract_id' => $contract_id
            ));
        }
        $this->update_menu($contract_id);
    }

    public function update_menu($contract_id) {
        $this->layout = 'ajax';
        echo form_dropdown('package', ddgen('yahajj_packages', array('yahajj_package_id', strtoupper('PG_' . name())), array('safa_ea_id' => session('ea_id'), 'safa_uo_contract_id' => $contract_id)), set_value('package'), 'class="validate[required]"') . '  <span class="icon-1x icon-refresh" onclick="icon_refresh()"></span>';
    }

    public function getmofa($id = false) {
        $this->layout = 'ajax';
        $this->load->helper('simple_html');
        $this->load->library('babelumrah');
        
        $group = $this->db->where('safa_umrahgroup_id', $id)->get('safa_umrahgroups')->row();
        if( ! $group)
            show_404 ();
        $this->babelumrah->auto_setup($group->safa_uo_contract_id);
//        $this->babelumrah->setup('delta', 'm860860', '18921');
        if( ! $this->babelumrah->Auth())
            show_error (lang('global_you_dont_have_avalid_uasp_credintials'));
        
        $mofas = $this->babelumrah->GetMOFA($group->uasp_id);
        $xml = simplexml_load_string($mofas['soap:Envelope']['soap:Body']['GetMOFAResponse']['GetMOFAResult']);
        // GOT MOFA 
        $group_s = $xml->G->attributes();
//        print_r($group_s['HasMofa']);        
        if($group_s['HasMofa'] != true)
            die("You didn't got a MOFA");
        // GROUP ID
//        print_r($group_s['GRID']); 
        // MUTAMERS
        foreach($xml->G->M as $mutamer)
        {
            $mut = $mutamer->attributes();
            $this->db->where('safa_umrahgroup_id', $group->safa_umrahgroup_id)
                     ->where('uasp_id', $mut['ONID'])
                     ->update('safa_umrahgroups_passports', array(
                         'mofa' => $mut['MOFA'],
                         'mofa_date' => date('Y-m-d')
                     ));
        }
    }

    public function getUmrahgroupsByContract() {
        $safa_uo_contract_id = $this->input->post('safa_uo_contract_id');

        $structure = array('safa_uo_contract_id', 'name');
        $key = $structure['0'];
        if (isset($structure['1'])) {
            $value = $structure['1'];
        }
        $umrahgroups_arr = array();
        $this->safa_umrahgroups_model->safa_uo_contract_id = $safa_uo_contract_id;
        $umrahgroups = $this->safa_umrahgroups_model->get();
        foreach ($umrahgroups as $umrahgroup) {
            $umrahgroups_arr[$umrahgroup->$key] = $umrahgroup->$value;
        }
        $form_dropdown = form_dropdown('safa_uo_contract_id', $umrahgroups_arr, set_value('safa_uo_contract_id', ''), 'class="chosen-select chosen-rtl input-full"  tabindex="4" ');
        $form_dropdown = str_replace('<select id="safa_umrahgroup_id" class="select input-huge" style="width: 100%; display: none;" name="safa_umrahgroup_id">', '', $form_dropdown);
        $form_dropdown = str_replace('</select>', '', $form_dropdown);
        echo $form_dropdown;
        exit;
    }

    /**
     * Gama WS
     * @author Muhammad El-Saeed <m.elsaeed@virgotel.com>
     * @param int $id
     */
    public function send_to_gama($id = FALSE) {
        $this->layout = 'js_new';
        $this->load->library('gama');
        $group = $this->db->where('safa_umrahgroup_id', $id)->get('safa_umrahgroups')->row();
        $ea = $this->db->where('safa_ea_id', session('ea_id'))->get('safa_eas')->row();
        $data['consulates'] = ddgen('erp_embassies', array('code', name()), array('erp_country_id' => $ea->erp_country_id)); // HIS COUNTRY CONSULATES
        $data['packages'] = ddgen('gama_packages', array('gama_package_id', strtoupper(name()), 'safa_uo_contract_id' => $group->safa_uo_contract_id)); // PACKAGES
        $data['contract'] = $group->safa_uo_contract_id;
        $this->load->library('form_validation');
        $this->form_validation->set_rules('end_date', lang('safa_umrahgroups_end_date'), 'trim|required');
        $this->form_validation->set_rules('package', lang('safa_umrahgroups_package'), 'trim|required');
        $this->form_validation->set_rules('consulate', lang('safa_umrahgroups_consulate'), 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $this->load->view('ea/safa_umrahgroups/gama', $data);
        } else {
            // SET UP ENVIRONMENT 
            $this->layout = 'ajax';
            $this->load->library('structure');
            $group_info = $this->safa_umrahgroups_model->get_group_info($id, session('ea_id'));
            if (!$group_info)
                show_404();
            $travellers = $this->db->where('safa_umrahgroup_id', $id)->get('safa_umrahgroups_passports')->result_array();
            $travellers = $this->relative($travellers);
            
            $statistics = $this->db->select(
                            "COUNT(*) AS total_passprots"
                            . ", (SELECT SUM(passport_dpn_count) FROM safa_umrahgroups_passports WHERE safa_umrahgroup_id = $id) AS companions"
                            . ", (SELECT COUNT(*) FROM safa_umrahgroups_passports WHERE safa_umrahgroup_id = $id AND gender_id = '1') AS men"
                            . ", (SELECT COUNT(*) FROM safa_umrahgroups_passports WHERE safa_umrahgroup_id = $id AND gender_id = '2') AS women"
                            . ", (SELECT COUNT(*) FROM safa_umrahgroups_passports WHERE safa_umrahgroup_id = $id AND relative_no != '0') AS has_mahram"
                            . ", (SELECT DISTINCT COUNT(relative_no) FROM safa_umrahgroups_passports WHERE safa_umrahgroup_id = $id AND relative_no != '0') AS mahrams"
                    )->where('safa_umrahgroup_id', $id)->get('safa_umrahgroups_passports')->row();
            $consulate = $this->db->where('erp_embassy_id', $this->input->post('consulate'))->get('erp_embassies')->row();
            $package = $this->db->where('gama_package_id', $this->input->post('package'))->get('gama_packages')->row();

            // UO ACCESS INFO ON BAB EL UMRAH WEB SERVICE
            $this->gama->auto_setup($group->safa_uo_contract_id);
            if( ! $this->gama->Auth())
                show_error (lang('global_you_dont_have_avalid_uasp_credintials'));
            $GROUP['safa_umrahgroup_id'] = $group_info->safa_umrahgroup_id;
            $GROUP['GroupName'] = $group_info->name;
            $GROUP['OfflineVersion'] = '4.0.1.0';
            $GROUP['PackageNo'] = $this->input->post('package');
            $GROUP['ConsulateID'] = $this->input->post('consulate');
            $GROUP['PlannedArrivalDate'] = date('Y/m/d', strtotime($this->input->post('end_date')));
            $GROUP['SendtoUO'] = 'false';
            $GROUP['GroupRemarks'] = null;
            $GROUP['ArabicInferface'] = 'true';
            $mutamer = $this->structure->convert($travellers, 4, 3);
            $res = $this->gama->InsertGroupAndMutamersData($GROUP, $mutamer);
            if( isset($res['Envelope']['Body']['InsertGroupAndMutamersData_NewResponse']['InsertGroupAndMutamersData_NewResult']) && preg_match("/GROUPNO#([0-9]+)/", $res['Envelope']['Body']['InsertGroupAndMutamersData_NewResponse']['InsertGroupAndMutamersData_NewResult'], $match)){
                $this->db->where('safa_umrahgroup_id', $group_info->safa_umrahgroup_id)
                         ->update('safa_umrahgroups', array(
                             'uasp_id' => $match['1']
                         ));
                echo "<script>parent.$.fancybox.close();</script>";
            }else
                show_404 ();
        }
    }

    public function getGamaPackages($contract_id = false) {
        if (!$contract_id)
            show_404();
        $this->layout = 'ajax';
        $this->db->where('safa_ea_id', session('ea_id'))->where('safa_uo_contract_id', $contract_id)->delete('gama_packages');
        $this->load->library('gama');
//        $this->gama->auto_setup($contract_id); //'info@skydirecttravel.co.uk', 'abdul1', '212'
        $this->gama->setup('info@skydirecttravel.co.uk', 'abdul1', '212');
        if( ! $this->gama->Auth())
                show_error (lang('global_you_dont_have_avalid_uasp_credintials'));
        $packages = $this->gama->getPackages();
        foreach ($packages as $package) {
            if(isset($package['PKG_NO']))
            $this->db->insert('gama_packages', array(
                'pkg_no' => $package['PKG_NO'],
                'name_ar' => $package['PKG_NAME_AR'],
                'name_la' => $package['PKG_NAME_LA'],
                'trans_arr_dep_type_name_ar' => $package['TRANS_ARR_DEP_TYPE_NAME_AR'],
                'trans_arr_dep_type_name_la' => $package['TRANS_ARR_DEP_TYPE_NAME_LA'],
                'valid_from' => $package['PKG_VALID_FROM'],
                'valid_to' => $package['PKG_VALID_UNTIL'],
                'duration' => $package['PKG_DURATION'],
                'amount' => $package['PKG_AMOUNT'],
                'safa_ea_id' => session('ea_id'),
                'safa_uo_contract_id' => $contract_id
            ));
        }
        $this->updateGamaMenu($contract_id);
    }

    public function updateGamaMenu($contract_id) {
        $this->layout = 'ajax';
        echo form_ajax_dropdown('package', ddgen('gama_packages', array('gama_package_id', name()), array('safa_ea_id' => session('ea_id'), 'safa_uo_contract_id' => $contract_id)), set_value('package'), 'class="validate[required]"');
    }

    public function getgamagroups() {
        $this->layout = 'ajax';
        $this->load->library('gama');
        $this->gama->setup('info@skydirecttravel.co.uk', 'abdul1', '212');
        if( ! $this->babelumrah->Auth())
            show_error (lang('global_you_dont_have_avalid_uasp_credintials'));
        print_r($this->gama->getPackges());
    }

    /**
     * Tawaf WS
     * @purpose Tawaf
     * @author Muhammad El-Saeed <m.elsaeed@virgotel.com>
     */
    public function send_to_tawaf($id = false) {
        $this->layout = 'js_new';
        $this->load->library('tawaf');
        $group = $this->db->where('safa_umrahgroup_id', $id)->get('safa_umrahgroups')->row();
        $ea = $this->db->where('safa_ea_id', session('ea_id'))->get('safa_eas')->row();
        $data['consulates'] = ddgen('erp_embassies', array('code', name()), array('erp_country_id' => $ea->erp_country_id)); // HIS COUNTRY CONSULATES
        $data['contract'] = $group->safa_uo_contract_id;
        $this->load->library('form_validation');
        $this->form_validation->set_rules('end_date', lang('safa_umrahgroups_end_date'), 'trim|required');
        $this->form_validation->set_rules('consulate', lang('safa_umrahgroups_consulate'), 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $this->load->view('ea/safa_umrahgroups/tawaf', $data);
        } else {
            // SET UP ENVIRONMENT 
            $this->layout = 'ajax';
            $this->load->library('structure');
            $group_info = $this->safa_umrahgroups_model->get_group_info($id, session('ea_id'));
            if (!$group_info)
                show_404();
            $travellers = $this->db->where('safa_umrahgroup_id', $id)->get('safa_umrahgroups_passports')->result_array();
            $travellers = $this->relative($travellers);
            
            $statistics = $this->db->select(
                            "COUNT(*) AS total_passprots"
                            . ", (SELECT SUM(passport_dpn_count) FROM safa_umrahgroups_passports WHERE safa_umrahgroup_id = $id) AS companions"
                            . ", (SELECT COUNT(*) FROM safa_umrahgroups_passports WHERE safa_umrahgroup_id = $id AND gender_id = '1') AS men"
                            . ", (SELECT COUNT(*) FROM safa_umrahgroups_passports WHERE safa_umrahgroup_id = $id AND gender_id = '2') AS women"
                            . ", (SELECT COUNT(*) FROM safa_umrahgroups_passports WHERE safa_umrahgroup_id = $id AND relative_no != '0') AS has_mahram"
                            . ", (SELECT DISTINCT COUNT(relative_no) FROM safa_umrahgroups_passports WHERE safa_umrahgroup_id = $id AND relative_no != '0') AS mahrams"
                    )->where('safa_umrahgroup_id', $id)->get('safa_umrahgroups_passports')->row();
            $consulate = $this->db->where('erp_embassy_id', $this->input->post('consulate'))->get('erp_embassies')->row();
            
            $GROUP['GROUP_CODE'] = $group_info->safa_umrahgroup_id;
            $GROUP['GRP_NAME'] = $group_info->name;
            $GROUP['GRP_EMB'] = $this->input->post('consulate');
            $GROUP['GRP_CREATE_DATE'] = $this->input->post('end_date');
            $this->tawaf->setup('ea0476', 'pvs110');
//            $this->tawaf->auto_setup($group->safa_uo_contract_id);
            if( ! $this->tawaf->auth())
                show_error (lang('global_you_dont_have_avalid_uasp_credintials'));
            
            // SEND THE GROUP
//            $res = $this->tawaf->insertOfflineGroups($GROUP);
            $mutamer = $this->structure->convert($travellers, 4, 3);   
            $this->tawaf->insertMutamers($mutamer);
                echo "<script>parent.$.fancybox.close();</script>";
            
        }
    }

    public function getTawafPackages() {
        
    }
    
    /**
     * Update travellers relative id
     * @param type $travellers
     * @return type
     * @author Muhammad El-Saeed <m.elsaeed@virgotel.com>
     */
    public function relative( & $travellers) {
        $arr['0'] = 0;
        $i = 0;
        foreach($travellers as $key => $traveller){
            $arr[$traveller['safa_group_passport_id']] = ++$i;
            $travellers[$key]['no'] = $i;
        }
        foreach($travellers as $key => $traveller){
            if( isset($arr[$traveller['relative_no']]))
                $travellers[$key]['relative_no'] = $arr[$traveller['relative_no']];
            $this->db->where('safa_umrahgroups_passport_id', $traveller['safa_umrahgroups_passport_id'])->update('safa_umrahgroups_passports', array(
                'no' => $traveller['no'],
                'relative_no' => $travellers[$key]['relative_no']
            ));
        }
        return $travellers;
    }
}


/* End of file safa_umrahgroups.php */
/* Location: ./application/controllers/ea/safa_umrahgroups.php */


    
