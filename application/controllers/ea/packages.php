<?php

class Packages extends Safa_Controller {

    function __construct() {
        parent::__construct();

        session('side_menu_id', 6);

        $this->layout = 'new';
        $this->load->library('hijrigregorianconvert');

        $this->lang->load('packages');
        $this->load->model('safa_packages_model');
        $this->load->helper('packages');
        $this->load->model('package_tourismplaces_model');
        $this->load->model('package_hotels_model');
        $this->load->model('safa_package_execlusive_meals_model');
        $this->load->model('safa_package_execlusive_nights_model');
        $this->load->model('safa_package_execlusive_nights_prices_model');
        $this->load->model('safa_package_periods_model');
        $this->load->model('safa_package_periods_prices_model');

        $this->load->model('package_periods_model');
        $this->load->model('hotels_model');
        $this->load->model('tourismplaces_model');
        $this->load->model('hotelroomsizes_model');

        permission();
    }

    function index() {

        $data['erp_mekka_hotels'] = ddgen('erp_hotels', array('erp_hotel_id', name()), array('erp_city_id' => 1), FALSE, array('' => lang('global_all')));
        $data['erp_madina_hotels'] = ddgen('erp_hotels', array('erp_hotel_id', name()), array('erp_city_id' => 2), FALSE, array('' => lang('global_all')));
        $data['erp_hotellevels'] = ddgen('erp_hotellevels', array('erp_hotellevel_id', name()), FALSE, FALSE, array('' => lang('global_all')));
        $data['erp_contracts'] = ddgen('safa_uo_contracts_eas', array('safa_uo_contract_id', name()), array('safa_ea_id' => session('ea_id')), FALSE, array('' => lang('global_all')));
        $data['erp_transportertypes'] = ddgen('safa_packages_transportation_types', array('safa_packages_transportation_type_id', name()), FALSE, FALSE, array('' => lang('global_all')));

        $this->load->library("form_validation");
        $this->form_validation->set_rules('dosearch', 'العقد', 'trim');

        if ($this->form_validation->run()) {
            
            if ($this->input->post('contract_id')) {
                $contractid = $this->input->post('contract_id');
                $this->db->where('safa_uo_contract_id', $contractid);
                $uoid = $this->db->get('safa_uo_contracts')->row();
                $this->safa_packages_model->erp_company_type_id = 2;
                $this->safa_packages_model->erp_company_id = $uoid->safa_uo_id;
                
            } else {
                $eaid = session('ea_id');
                $query = <<<QURY
SELECT safa_uo_contracts.safa_uo_id FROM safa_uo_contracts
INNER JOIN safa_uo_contracts_eas ON safa_uo_contracts_eas.safa_uo_contract_id = safa_uo_contracts.safa_uo_contract_id
WHERE safa_uo_contracts_eas.safa_ea_id = $eaid
QURY;
                $uosobjcts = $this->db->query($query)->result();
                $uos = array();
                foreach ($uosobjcts as $uobj)
                    $uos[] = $uobj->safa_uo_id ;
                
                $this->db->where('erp_company_type_id', 2);
                if(count($uos))
                    $this->db->where_in('erp_company_id',$uos);
                else 
                    $this->db->where('erp_company_id','0');
            }
            

            $data['items'] = $this->safa_packages_model->get();
        }

        $this->load->view('ea/packages/search', $data);
    }

    function view($id = FALSE) {
        if (!$id)
            show_404();
        $data['erp_cities'] = ddgen('erp_cities', array('erp_city_id', name()), FALSE, FALSE, TRUE);
        $data['erp_hotels'] = ddgen('erp_hotels', array('erp_hotel_id', name()), FALSE, FALSE, TRUE);
        $data['erp_currencies'] = ddgen('erp_currencies', array('erp_currency_id', name()), FALSE, FALSE, TRUE);
        $data['erp_meals'] = ddgen('erp_meals', array('erp_meal_id', name()), FALSE, FALSE, TRUE);
        $data['erp_transportertypes'] = ddgen('safa_packages_transportation_types', array('safa_packages_transportation_type_id', name()), FALSE, FALSE, TRUE);
        $data['safa_tourismplaces'] = ddgen('safa_tourismplaces', array('safa_tourismplace_id', name()), FALSE, FALSE, TRUE);
        $data['package_periods'] = ddgen('erp_package_periods', array('erp_package_period_id', name()), FALSE, FALSE, TRUE);

        //$data['erp_hotelroomsizes'] = ddgen('erp_hotelroomsizes', array('erp_hotelroomsize_id', name()), FALSE, FALSE, TRUE);
        $data['erp_hotelroomsizes'] = $this->hotelroomsizes_model->get();

        $this->safa_packages_model->safa_package_id = $id;
        $data['item'] = $this->safa_packages_model->get();

        if (!isset($data['item']))
            show_404();

        //---------- Details --------------------------
        $this->safa_package_execlusive_meals_model->safa_package_id = $id;
        $item_execlusive_meals_prices = $this->safa_package_execlusive_meals_model->get();
        $data['item_execlusive_meals_prices'] = $item_execlusive_meals_prices;

        $this->safa_package_execlusive_nights_model->safa_package_id = $id;
        $item_execlusive_nights_prices = $this->safa_package_execlusive_nights_model->get();
        $data['item_execlusive_nights_prices'] = $item_execlusive_nights_prices;

        $this->safa_package_periods_model->safa_package_id = $id;
        $item_hotels_prices = $this->safa_package_periods_model->get();
        $data['item_hotels_prices'] = $item_hotels_prices;

        $this->package_hotels_model->safa_package_id = $id;
        $item_hotels = $this->package_hotels_model->get();
        $data['item_hotels'] = $item_hotels;

        $this->package_tourismplaces_model->safa_package_id = $id;
        $item_tourism_places = $this->package_tourismplaces_model->get();
        $data['item_tourism_places'] = $item_tourism_places;


        $this->load->view('ea/packages/view', $data);
    }

}
