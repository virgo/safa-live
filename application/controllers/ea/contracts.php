<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Contracts extends Safa_Controller {

    public $module = "contracts";

    public function __construct() {
        parent::__construct();

        $this->layout = 'new';
        //Side menu session, By Gouda.
        session('side_menu_id', 1);

        $this->load->model('contracts_model');
        $this->load->model('uo_contracts_ea_model');
        $this->load->model('eas_model');
        $this->load->model('ea_seasons_model');
        $this->load->model('ea_users_model');
        $this->load->model('ea_hotels_model');
        $this->load->model('currencies_model');
        $this->load->model('transporters_model');
        $this->load->model('contracts_settings_model', 'setting');
        $this->load->model('contracts_settings_visa_prices_model', 'visa_price');
        #to make search in hotels
        $this->load->model('itos_model');
        $this->load->model('uo_contracts_itos_model');
        #to make hotels in contract
        $this->load->model('hotels_model');
        $this->load->model('uo_contracts_hotels_model');
        //By Gouda.
        $this->load->model('erp_languages_model');
        $this->load->helper('db_helper');
        $this->lang->load('contracts');
        
        permission();
        if (session('ea_id'))
            $this->contracts_model->safa_ea_id = session("ea_id");
    }

    public function index() {
        
    }

    public function edit($id = false) {

        //By Gouda.------
        $data['safa_uo_contracts_status'] = ddgen('safa_uo_contracts_status', array('safa_uo_contracts_status_id', name()));
        $data['safa_contract_phases'] = ddgen('safa_contract_phases', array('safa_contract_phases_id', name()), array('safa_uo_id' => session('uo_id')));

        if (!$id)
            show_404();
        $this->contracts_model->safa_uo_contract_id = $id;
        $data['item'] = $this->contracts_model->get();

        if (!$data['item'])
            show_404();

        $this->contracts_model->safa_uo_contract_id = $id;
        $this->contracts_model->by_eas_id = session('ea_id');
        $total_ea_contracts_rows = $this->contracts_model->get_contract_ea();
        $data['contract_row_item'] = $total_ea_contracts_rows;

        $this->setting->safa_uo_contract_id = $id;
        $data['setting'] = $this->setting->get();
        if (!$data['setting']) {
            $this->setting->safa_uo_contract_id = $id;
            $this->setting->save();
            $this->setting->safa_uo_contract_id = $id;
            $data['setting'] = $this->setting->get();
        }

        if ($data['setting']->transporters_us == 'list') {
            $this->load->model('contracts_settings_transporters_model', 'transporter');
            $this->transporter->safa_uo_contract_id = $id;
            $this->transporter->safa_uo_contract_setting_id = $data['setting']->safa_uo_contract_setting_id;
            $transporters = $this->transporter->get();
            $data['list_transporter'] = '[';
            foreach ($transporters as $transporter) {
                $data['list_transporter'] .= '"' . $transporter->safa_transporter_id . '",';
            }
            $data['list_transporter'] .= ']';
        } else
            $data['list_transporter'] = '[]';

        $this->load->library("form_validation");
        $this->form_validation->set_rules('contract_name_ar', 'lang:contracts_name_ar', 'trim');
        $this->form_validation->set_rules('contract_name_la', 'lang:contracts_name_la', 'trim');
        if (name() == "name_la")
            $this->form_validation->set_rules('contract_name_la', 'lang:contracts_name_la', 'trim|required');
        if (name() == "name_ar")
            $this->form_validation->set_rules('contract_name_ar', 'lang:contracts_name_ar', 'trim|required');

        $this->form_validation->set_rules('contarct_address', 'lang:contracts_address', 'trim');
        $this->form_validation->set_rules('contarct_phone', 'lang:contracts_phone', 'trim|required');
        $this->form_validation->set_rules('contarct_ksa_address', 'lang:contracts_ksaaddress', 'trim');
        $this->form_validation->set_rules('contarct_ayata_num', 'lang:contracts_ayata_num', 'trim');
        $this->form_validation->set_rules('contarct_agency_symbol', 'lang:contracts_agencysymbol', 'trim|required');
        //$this->form_validation->set_rules('contarct_agency_name', 'lang:contracts_agencyname', 'trim|required');
        $this->form_validation->set_rules('contarct_email', 'lang:contracts_email', 'trim|required|valid_email');
        $this->form_validation->set_rules('contarct_country', 'lang:contracts_country', 'trim|required|is_natural_no_zero');
        $this->form_validation->set_rules('contarct_fax', 'lang:contracts_fax', 'trim');
        $this->form_validation->set_rules('contarct_nationality', 'lang:contracts_nationality', 'trim|required|is_natural_no_zero');
        //$this->form_validation->set_rules('contarct_username', 'lang:contracts_username', 'trim|required|alpha_dash|unique_col[safa_uo_contracts.contract_username.' . $id . ']'); // to do again to be contained 
        //$this->form_validation->set_rules('contarct_password', 'lang:contracts_password', 'trim');
        //$this->form_validation->set_rules('contarct_confpassword', 'lang:contracts_repeat_password', 'trim|matches[contarct_password]');


        $this->form_validation->set_rules('uasp_username', 'lang:uasp_username', 'trim');
        $this->form_validation->set_rules('uasp_password', 'lang:uasp_password', 'trim');
        $this->form_validation->set_rules('uasp_eacode', 'lang:uasp_eacode', 'trim');

        $this->form_validation->set_rules('contarct_responsible_name', 'lang:contracts_resposible_name', 'trim');
        $this->form_validation->set_rules('contarct_responsible_phone', 'lang:contracts_resposible_phone', 'trim');
        $this->form_validation->set_rules('contarct_responsible_email', 'lang:contracts_resposible_email', 'trim');

        $this->form_validation->set_rules('contract_notes', '', 'trim');


        if ($this->form_validation->run() == false) {
            $currencies = $this->currencies_model->get();
            $data["currencies"] = array('' => '');
            foreach ($currencies as $currency) {
                $data["currencies"][$currency->erp_currency_id] = $currency->symbol;
            }

            $transporters = $this->transporters_model->get();
            $data["transporters"] = array('' => '');
            foreach ($transporters as $transporter) {
                $data["transporters"][$transporter->safa_transporter_id] = $transporter->name_ar;
            }

            $this->visa_price->safa_uo_contract_id = $id;
            $this->visa_price->safa_uo_contract_setting_id = $data['setting']->safa_uo_contract_setting_id;
            $data['prices'] = $this->visa_price->get();

            $this->load->view('ea/contracts/edit', $data);
        } else {

            $now_timestamp = strtotime(date('Y-m-d H:i', time()));
            $allowedExts = array("gif", "jpeg", "jpg", "png");
            $temp = explode(".", $_FILES["responsible_passport_photo_path"]["name"]);
            $extension = end($temp);
            if ((($_FILES["responsible_passport_photo_path"]["type"] == "image/gif") || ($_FILES["responsible_passport_photo_path"]["type"] == "image/jpeg") || ($_FILES["responsible_passport_photo_path"]["type"] == "image/jpg") || ($_FILES["responsible_passport_photo_path"]["type"] == "image/pjpeg") || ($_FILES["responsible_passport_photo_path"]["type"] == "image/x-png") || ($_FILES["responsible_passport_photo_path"]["type"] == "image/png"))
                    //&& ($_FILES["responsible_passport_photo_path"]["size"] < 20000)
                    && in_array($extension, $allowedExts)) {

                if ($_FILES["responsible_passport_photo_path"]["error"] > 0) {
                    echo "Return Code: " . $_FILES["responsible_passport_photo_path"]["error"] . "<br>";
                } else {

                    /*
                      echo "Upload: " . $_FILES["file"]["name"] . "<br>";
                      echo "Type: " . $_FILES["file"]["type"] . "<br>";
                      echo "Size: " . ($_FILES["file"]["size"] / 1024) . " kB<br>";
                      echo "Temp file: " . $_FILES["file"]["tmp_name"] . "<br>";

                      if (file_exists("upload/" . $_FILES["file"]["name"]))
                      {
                      echo $_FILES["file"]["name"] . " already exists. ";
                      }
                      else
                      {


                      move_uploaded_file($_FILES["responsible_passport_photo_path"]["tmp_name"],
                      "upload/" . $_FILES["responsible_passport_photo_path"]["name"]);
                      echo "Stored in: " . "upload/" . $_FILES["responsible_passport_photo_path"]["name"];
                      }
                     */

                    if (file_exists('./static/uploads/passports_photos/' . $data['item']->responsible_passport_photo_path)) {
                        unlink('./static/uploads/passports_photos/' . $data['item']->responsible_passport_photo_path);
                    }

                    move_uploaded_file($_FILES["responsible_passport_photo_path"]["tmp_name"], "./static/uploads/passports_photos/" . $now_timestamp . '_' . $_FILES["responsible_passport_photo_path"]["name"]);

                    $this->contracts_model->responsible_passport_photo_path = $now_timestamp . '_' . $_FILES["responsible_passport_photo_path"]["name"];
                }
            } else {
                //echo "Invalid file";
            }

            //$this->contracts_model->name_ar = $this->input->post("contract_name_ar");
            //$this->contracts_model->name_la = $this->input->post("contract_name_la");

            $this->contracts_model->address = $this->input->post('contarct_address');
            $this->contracts_model->phone = $this->input->post('contarct_phone');
            $this->contracts_model->ksa_address = $this->input->post('contarct_ksa_address');
            $this->contracts_model->iata = $this->input->post('contarct_ayata_num');
            $this->contracts_model->ksa_phone = $this->input->post('contarct_ksa_phone');
            $this->contracts_model->agency_symbol = $this->input->post('contarct_agency_symbol');
            $this->contracts_model->agency_name = $this->input->post('contarct_agency_name');
            $this->contracts_model->email = $this->input->post('contarct_email');

            //By Gouda. --------
            //$this->contracts_model->safa_uo_contracts_status_id = $this->input->post('safa_uo_contracts_status_id');
            //$this->contracts_model->safa_contract_phases_id_current = $this->input->post('safa_contract_phases_id_current');
            //$this->contracts_model->safa_contract_phases_id_next = $this->input->post('safa_contract_phases_id_next');
            //------------------

            $this->contracts_model->country_id = $this->input->post('contarct_country');
            $this->contracts_model->fax = $this->input->post('contarct_fax');
            $this->contracts_model->city_id = $this->input->post('contarct_city');
            $this->contracts_model->responsible_name = $this->input->post('contarct_responsible_name');
            $this->contracts_model->nationality_id = $this->input->post('contarct_nationality');
            $this->contracts_model->responsible_phone = $this->input->post('contarct_responsible_phone');
            $this->contracts_model->responsible_email = $this->input->post('contarct_responsible_email');

            $this->contracts_model->uasp_username = $this->input->post('uasp_username');
            $this->contracts_model->uasp_password = $this->input->post('uasp_password');
            $this->contracts_model->uasp_eacode = $this->input->post('uasp_eacode');

            $this->contracts_model->notes = $this->input->post('contract_notes');
            $contract_id = $this->contracts_model->save();
            $this->contracts_model->save();


            $this->contracts_model->uo_contracts_eas_safa_uo_contract_ea_id = $total_ea_contracts_rows->safa_uo_contract_ea_id;
            $this->contracts_model->uo_contracts_eas_name_ar = $this->input->post('contract_name_ar');
            $this->contracts_model->uo_contracts_eas_name_la = $this->input->post('contract_name_la');

            $this->contracts_model->save_contract_ea();


            $this->load->view('redirect_message', array('msg' => lang('global_updated_message'),
                'url' => site_url('ea/contract_approving_phases/view_all'),
                'id' => $id,
                'model_title' => lang('node_title'), 'action' => lang('contracts_edit')));
        }
    }

}

/* End of file contracts.php */
/* Location: ./application/controllers/http://www.googel.com/contracts.php */