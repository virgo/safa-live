<?php

class Safa_buses_brands extends Safa_Controller {

	function __construct()
	{
		parent::__construct();
		//Side menu session, By Gouda.
		if (session('uo_id')) {
			session('side_menu_id', 0);
		} else if (session('ito_id')) {
			session('side_menu_id', 0);
		}
		$this->layout = 'new';

		$this->lang->load('safa_buses_brands');
		$this->load->model('safa_buses_brands_model');
		$this->load->model('safa_buses_models_model');

		$this->load->model('transporters_buses_model');

		permission('safa_buses_brands');
	}

	function index()
	{
		if (isset($_GET['search'])) {
			$this->search();
		}
		$data["total_rows"] = $this->safa_buses_brands_model->get(true);

		$this->safa_buses_brands_model->offset = $this->uri->segment("3");
		//$this->safa_buses_brands_model->limit = $this->config->item('per_page');
		//$this->safa_buses_brands_model->order_by = array('safa_contract_phases_id');
		$data["items"] = $this->safa_buses_brands_model->get();
		$config['uri_segment'] = 3;
		$config['base_url'] = site_url('safa_buses_brands/index');
		$config['total_rows'] = $data["total_rows"];

		$config['per_page'] = $this->config->item('per_page');
		$config['suffix'] = $this->input->get() ? '?' . http_build_query($_GET) : NULL;
		$this->load->library('pagination');
		$this->pagination->initialize($config);
		$data['pagination'] = $this->pagination->create_links();
		$this->load->view('safa_buses_brands/index', $data);
	}

	function manage($id = FALSE)
	{
		$data = array();

		if ($id) {
			$this->safa_buses_brands_model->safa_buses_brands_id = $id;
			$data['item'] = $this->safa_buses_brands_model->get();
			
			$this->safa_buses_models_model->safa_buses_brands_id = $id;
	        $item_models = $this->safa_buses_models_model->get();
	        $data['item_models'] = $item_models;
		}

		$this->load->library("form_validation");
		$this->form_validation->set_rules('name_ar', 'lang:name_ar', 'trim|required');
		$this->form_validation->set_rules('name_la', 'lang:name_la', 'trim|required');

		if ($this->form_validation->run() == false) {
			$this->load->view('safa_buses_brands/manage', $data);
		} else {

			$this->safa_buses_brands_model->name_ar = $this->input->post('name_ar');
			$this->safa_buses_brands_model->name_la = $this->input->post('name_la');

			if ($id) {
				$this->safa_buses_brands_model->safa_buses_brands_id = $id;
				$this->safa_buses_brands_model->save();

				$this->save_details($id);

				$this->load->view('redirect_new_message', array('msg' => lang('global_updated_message'),
                    'url' => site_url('safa_buses_brands'),
                    'id' => $this->uri->segment("4"),
                    'model_title' => lang('title'), 'action' => lang('edit_title')));
			} else {
				$safa_buses_brands_id = $this->safa_buses_brands_model->save();

				$this->save_details($safa_buses_brands_id);

				if (isset($safa_buses_brands_id)) {
					$this->load->view('redirect_new_message', array('msg' => lang('global_added_message'),
                        'url' => site_url('safa_buses_brands'),
                        'id' => $this->uri->segment("4"),
                        'model_title' => lang('title'), 'action' => lang('add_title')));
				}
			}
		}
	}

	function save_details($id)
	{

		$name_ar = $this->input->post('model_name_ar');
		$name_la = $this->input->post('model_name_la');
			
		if (ensure($name_ar))
		foreach ($name_ar as $key => $value) {

			$safa_buses_models_id = $key;
			$this->safa_buses_models_model->safa_buses_models_id  = $key;
			$current_buses_models_row_count = $this->safa_buses_models_model->get(true);


			$this->safa_buses_models_model->safa_buses_brands_id = $id;
			$this->safa_buses_models_model->name_ar = $name_ar[$key];
			$this->safa_buses_models_model->name_la = $name_la[$key];



			if ($current_buses_models_row_count == 0) {
				$this->safa_buses_models_model->safa_buses_models_id = false;
				$safa_buses_models_id = $this->safa_buses_models_model->save();
				
				//------------------ Save Default Bus For This Model -------------------------
				$this->transporters_buses_model->safa_transporters_id = null;
				
				//$this->transporters_buses_model->bus_no = 'غير محدد';
				$this->transporters_buses_model->bus_no =$name_ar[$key];
				$this->safa_buses_brands_model->safa_buses_brands_id = $id;
				$safa_buses_brands_row = $this->safa_buses_brands_model->get();
				if(count($safa_buses_brands_row)>0) {
				$this->transporters_buses_model->bus_no =$safa_buses_brands_row->{name()}.' - '.$name_ar[$key];
				}
				
				$this->transporters_buses_model->safa_buses_brands_id = $id;
				$this->transporters_buses_model->safa_buses_models_id = $safa_buses_models_id;
				$this->transporters_buses_model->industry_year = '';
				$this->transporters_buses_model->passengers_count = '';
				$this->transporters_buses_model->notes = '';
				$this->transporters_buses_model->auto_generate = 1;
	
				$this->transporters_buses_model->save();
				//-----------------------------------------------------------------------------
			
			} else {
				$this->safa_buses_models_model->save();
			}

			


			// By Gouda
			$this->safa_buses_models_model->safa_buses_models_id = false;
			$this->safa_buses_models_model->safa_buses_brands_id = false;
			$this->safa_buses_models_model->name_ar = false;
			$this->safa_buses_models_model->name_la = false;
		}


		// --- Delete Deleted rooms rows from Database --------------
		$buses_models_remove = $this->input->post('buses_models_remove');
		if (ensure($buses_models_remove)) {
			foreach ($buses_models_remove as $buses_model_remove) {
				$this->safa_buses_models_model->safa_buses_models_id = $buses_model_remove;
				$this->safa_buses_models_model->delete();
			}
		}
	}

	function delete($id = false)
	{
		if (!$id)
		show_404();

		$this->db->trans_start();
		$this->safa_buses_brands_model->safa_buses_brands_id = $id;
		$this->safa_buses_brands_model->delete();
		$this->db->trans_complete();
		$this->load->view('redirect_message', array('msg' => lang('global_deleted_message'),
            'url' => site_url('safa_buses_brands'),
            'model_title' => lang('title'), 'action' => lang('delete_title')));
	}

	function getBrandModels() 
	{
        $safa_buses_brands_id = $this->input->post('safa_buses_brands_id');
        if ($safa_buses_brands_id == 0) {
            $safa_buses_models = ddgen('safa_buses_models', array('safa_buses_models_id', name()), '', array(name(), 'ASC'), TRUE);
        } else {
            $safa_buses_models = ddgen('safa_buses_models', array('safa_buses_models_id', name()), array('safa_buses_brands_id' => $safa_buses_brands_id), array(name(), 'ASC'), TRUE);
        }

        $safa_buses_models_options = '';
        foreach ($safa_buses_models as $key => $value) {
            $safa_buses_models_options .= "<option value='$key'>$value</option>\n";
        }
        
        $this->safa_buses_models_model->order_by = array('safa_buses_models_id', 'desc');
        $this->safa_buses_models_model->limit = 1;
        $safa_buses_models_rows = $this->safa_buses_models_model->get();


        $data_arr[] = array('safa_buses_models_options' => $safa_buses_models_options,
            'safa_buses_models_id_selected' => $safa_buses_models_rows[0]->safa_buses_models_id,
        );
        echo json_encode($data_arr);
        exit;
    }
}
