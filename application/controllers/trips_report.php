<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Trips_report extends Safa_Controller {

    public function __construct() {
        parent::__construct();
        $this->layout = 'new';

        //Side menu session, By Gouda.
        if (session('uo_id')) {
            session('side_menu_id', 3);
        } else if (session('ea_id')) {
            session('side_menu_id', 1);
        }
        
        $this->load->model('safa_trips_model');
        
        $this->lang->load('trips_report');
        $this->lang->load('trips_reports');
        $this->load->helper('form_helper');
        $this->load->helper('db_helper');


        permission();
        if (session('ea_id')) {
        	$this->safa_trips_model->safa_tripstatus_id = 5;
            $this->safa_trips_model->safa_ea_id = session('ea_id');
        } 
        if (session('uo_id')) {
        	$this->safa_trips_model->safa_tripstatus_id = 3;
            $this->safa_trips_model->safa_uo_id = session('uo_id');
        }
    }

    public function index() 
    {
        if (isset($_GET['search'])) {
			$this->search();
		}
		$data["total_rows"] = $this->safa_trips_model->get(true);

		$this->safa_trips_model->offset = $this->uri->segment("3");
		//$this->safa_trips_model->limit = $this->config->item('per_page');
		//$this->safa_trips_model->order_by = array('safa_trip_id');
		$data["items"] = $this->safa_trips_model->get();
		$config['uri_segment'] = 3;
		$config['base_url'] = site_url('trips_report/index');
		$config['total_rows'] = $data["total_rows"];

        $data["items"] = $this->safa_trips_model->get();        
		//echo $this->db->last_query();
        $config['uri_segment'] = 4;
        $config['base_url'] = site_url('trips_report');
        $config['total_rows'] = $data["total_rows"];

        $config['per_page'] = $this->config->item('per_page');
        $config['suffix'] = $this->input->get() ? '?' . http_build_query($_GET) : NULL;
        $this->load->library('pagination');
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();
        $this->load->view('trips_report/index', $data);
    }

}

/* End of file trips_report.php */
/* Location: ./application/controllers/uo/trips_report.php */
