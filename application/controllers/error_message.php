<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Error_message extends Safa_Controller {

    public $module = "Error_message";

    public function __construct() {
        parent::__construct();
        $this->layout = 'new';

        if (session('uo_id')) {
            //Side menu session, By Gouda.
            session('side_menu_id', 5);
        } else if (session('ea_id')) {
            session('side_menu_id', 3);
        }

        permission();
    }

    public function index() {
        $this->load->view('error_message', array('msg' => lang('don_not_have_privilege_to_add_reservation_order'),
            'url' => site_url('hotels_reservation_orders'),
            'model_title' => lang('title'), 'action' => lang('title')));
    }

    public function show() {
        $message = session('error_message');
        $url = session('error_url');
        $title = session('error_title');
        $action = session('error_action');

        $this->load->view('error_message', array('msg' => $message,
            'url' => $url,
            'model_title' => $title, 'action' => $action));
    }

}

/* End of file error_message.php */
/* Location: ./application/controllers/error_message.php */