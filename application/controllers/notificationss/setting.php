<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class setting extends Safa_Controller {

    public $module = "setting";

    public function __construct() {
        parent::__construct();
        permission();
        $this->lang->load("admins/menu");
        $this->load->model("noti_setting_model");
        $this->noti_setting_model->company_id = session($this->destination . "_id");
        $this->noti_setting_model->company_type = $this->system_id;
        $this->noti_setting_model->init();
    }

    function index() {
        $data = array();
        $data['select_from_menu'] = lang('global_select_from_menu');
        if ($this->input->post('save')) {
            if (($this->input->post('send_email'))) {
                $this->noti_setting_model->send_email = $this->input->post('send_email');
            } else {
                $this->noti_setting_model->send_email = 0;
            }

            if (($this->input->post("email"))) {
                $this->noti_setting_model->email = $this->input->post('email');
            }

            if (($this->input->post("send_sms"))) {
                $this->noti_setting_model->send_sms = $this->input->post('send_sms');
            } else {
                $this->noti_setting_model->send_sms = 0;
            }

            if (($this->input->post('mobile'))) {
                $this->noti_setting_model->phone_no = $this->input->post("mobile");
            }
            //set notify type as default
            $this->noti_setting_model->note_me = $this->input->post("note_me");
            $this->noti_setting_model->uos = $this->input->post("safa_uo_id");
            $this->noti_setting_model->eas = $this->input->post("safa_ea_id");
            $this->noti_setting_model->itos = $this->input->post("safa_ito_id");
            $this->noti_setting_model->save();
        }

        $this->load->view("notification/setting_view", $data);
    }
}
