<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Ea_licenses extends Safa_Controller {

    public $module = "ea_licenses";
    public $layout = 'new';
    public $table_pk = 'crm_ea_license_id';

    public function __construct() {
        parent::__construct();
        $this->load->model('ea_licenses_model');
        $this->lang->load('ea_licenses');
        permission();
    }

    public function index($ea_id = false) {
        if (!$ea_id)
            show_404();

        $data['ea_id'] = $ea_id;
        $data['module'] = $this->module;
        $data['table_pk'] = $this->table_pk;
        $this->ea_licenses_model->safa_ea_id = $ea_id;
        if (isset($_GET['search']))
            $this->search();
        $data["total_rows"] = $this->ea_licenses_model->search(true);
        $this->ea_licenses_model->custom_select = 'crm_ea_licenses.*, '
                . '(SELECT name FROM crm_licenses WHERE crm_ea_licenses.crm_license_id = crm_licenses.crm_license_id) AS name,'
                . '(SELECT COUNT(*) FROM `crm_ea_payments` WHERE `crm_ea_payments`.`crm_ea_license_id` = `crm_ea_licenses`.`crm_ea_license_id` ) AS "delete"';
        $this->ea_licenses_model->offset = $this->uri->segment("4");
        $this->ea_licenses_model->limit = config('per_page');
        $data["items"] = $this->ea_licenses_model->search();
        $config['uri_segment'] = 4;
        $config['base_url'] = site_url('ea_licenses/index/' . $ea_id);
        $config['total_rows'] = $data["total_rows"];
        $config['per_page'] = $this->ea_licenses_model->limit;
        $config['suffix'] = $this->input->get() ? '?' . http_build_query($_GET) : NULL;
        $this->load->library('pagination');
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();
        $this->load->view('ea_licenses/index', $data);
    }

    public function insert($ea_id = false) {
        if (!$ea_id)
            show_404();
        $this->manage(false, $ea_id);
    }

    public function manage($id = false, $ea_id = false) {
        $data['ea_id'] = $ea_id;
        $data['module'] = $this->module;
        $data['table_pk'] = $this->table_pk;
        if ($id) {
            $this->ea_licenses_model->crm_ea_license_id = $id;
            $data['item'] = $this->ea_licenses_model->get();
            if (!$data['item'])
                show_404();
        }
        else {
            $data['item'] = new stdClass();
            $data['item']->crm_license_id = null;
            $data['item']->over_credit = null;
            $data['item']->status = null;
        }

        $this->load->library("form_validation");
        $this->form_validation->set_rules('crm_license_id', lang($data['module'] . '_crm_license_id'), 'required');
        $this->form_validation->set_rules('over_credit', lang($data['module'] . '_over_credit'), 'required|numeric');
        $this->form_validation->set_rules('status', lang($data['module'] . '_status'), 'required');
        if ($this->form_validation->run() == false) {
            $this->load->view("ea_licenses/manage", $data);
        } else {
            if ($id)
                $this->ea_licenses_model->crm_ea_license_id = $id;

            if ($ea_id)
                $this->ea_licenses_model->safa_ea_id = $ea_id;
            else
                $ea_id = $data['item']->safa_ea_id;
            $this->ea_licenses_model->crm_license_id = $this->input->post('crm_license_id');
            $this->ea_licenses_model->over_credit = $this->input->post('over_credit');
            $this->ea_licenses_model->status = $this->input->post('status');
            $this->ea_licenses_model->save();
            redirect("ea_licenses/index/" . $ea_id);
        }
    }

    function delete($id) {
        if (!$id)
            show_404();

        $this->ea_licenses_model->crm_ea_license_id = $id;
        $ea_license = $this->ea_licenses_model->get();
        if (!$this->ea_licenses_model->delete())
            show_404();
        redirect("ea_licenses/index/" . $ea_license->safa_ea_id);
    }

    function search() {
        if ($this->input->get("crm_ea_license_id"))
            $this->ea_licenses_model->crm_ea_license_id = $this->input->get("crm_ea_license_id");
        if ($this->input->get("safa_ea_id"))
            $this->ea_licenses_model->safa_ea_id = $this->input->get("safa_ea_id");
        if ($this->input->get("crm_license_id"))
            $this->ea_licenses_model->crm_license_id = $this->input->get("crm_license_id");
        if ($this->input->get("over_credit"))
            $this->ea_licenses_model->over_credit = $this->input->get("over_credit");
        if ($this->input->get("status"))
            $this->ea_licenses_model->status = $this->input->get("status");
    }

}

/* End of file ea_licenses.php */
/* Location: ./application/controllers/ea_licenses.php */