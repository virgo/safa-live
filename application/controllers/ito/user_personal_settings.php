<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class User_personal_settings extends Safa_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('ito_user_personal_settings_model');
        $this->lang->load('admins/user_personal_settings');
        if (!session('ito_id'))
            permission();
    }

    public function index() {

        $this->ito_user_personal_settings_model->safa_ito_user_id = session('ito_user_id');

        $this->ito_user_personal_settings_model->limit = $this->config->item('per_page');
        $this->ito_user_personal_settings_model->offset = $this->uri->segment('4');
        $config['uri_segment'] = 4;
        $config['base_url'] = site_url('ito/user_personal_settings/index');
        $config['total_rows'] = $this->db->get('safa_ito_users')->num_rows();
        $config['per_page'] = $this->config->item('per_page');
        $this->load->library('pagination');
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();
        $data['items'] = $this->ito_user_personal_settings_model->get();
        $this->load->view('ito/user_personal_settings/index', $data);
    }

    public function edit($id = FALSE) {

        $id = $this->ito_user_personal_settings_model->safa_ito_user_id = session('ito_user_id');

        if (!$id)
            show_404();
        $data['item'] = $this->ito_user_personal_settings_model->get();
        if (!$data['item'])
            if (!$id)
                show_404();

        $data['item'] = $this->ito_user_personal_settings_model->get();
        if (!$data['item'])
            show_404();
        $this->load->library('form_validation');

        $this->form_validation->set_rules('new_password', 'lang:user_personal_settings_new_password', 'trim|matches[passconf]|required');
        $this->form_validation->set_rules('passconf', 'lang:user_personal_settings_resetpassword', 'required');
        $this->form_validation->set_rules('check_user_info', '', 'callback_check_user_info');



        if ($this->form_validation->run() == FALSE) {
            $this->load->view('ito/user_personal_settings/edit', $data);
        } else {

            #new_password from view
            $password = $this->input->post('new_password');

            if ($password != "") {

                $this->ito_user_personal_settings_model->password = md5($password);
            }

            $this->ito_user_personal_settings_model->safa_ito_user_id = $id;
            $this->ito_user_personal_settings_model->save();
//            $this->db->like('user_data', 's:11:"ito_user_id";s:' . strlen($id) .':"' . $id . '";', 'both')
//                     ->like('user_data', 's:6:"ito_id";s:' . strlen($data['item']->safa_ito_id) .':"' . $data['item']->safa_ito_id . '"', 'both')
//                     ->delete('sessions');
            $this->load->view('admin/redirect', array('msg' => lang('global_updated_successfully'), 'url' => site_url('ito/logout')));
        }
    }

    function check_user_info() {

        $this->form_validation->set_message('check_user_info', lang('global_info_not_true'));

        $username = $this->input->post('username');
        $password = md5($this->input->post('password'));

        $this->db->where('username', $username);
        $this->db->where('password', $password);
        $this->db->where("safa_ito_users.safa_ito_user_id", session("ito_user_id"));
        $query = $this->db->get('safa_ito_users');
//          print_r($password);

        if ($query->num_rows > 0) {
            return true;
        } else {
            return false;
        }
    }

    function valid_pass($password) {

        $this->form_validation->set_message('valid_pass', lang('password_error'));

        if ($password) {
            $r2 = '/[a-z]/';
            $r4 = '/[0-9]/';

            if (preg_match_all($r2, $password, $o) < 2)
                return FALSE;

            if (preg_match_all($r4, $password, $o) < 2)
                return FALSE;

            if (strlen($password) < 8)
                return FALSE;
        }
        return TRUE;
    }

}

/* End of file User_personal_settings.php */
/* Location: ./application/controllers/admin/User_personal_settings.php */