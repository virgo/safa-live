<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class All_movements_ito extends Safa_Controller {

    public $module = "trips_reports";
    public $ito_id;

    public function __construct() {
        parent::__construct();
        //Side menu session, By Gouda.
        session('side_menu_id', 1);
        $this->layout = 'new';
        $this->load->model('trips_reports_model');
        $this->load->model('internalpassages_model');
        $this->load->model('transporters_drivers_model');
        $this->load->model('transporters_buses_model');
        $this->load->model('trip_internaltrip_model');
        $this->load->model('internalsegments_drivers_and_buses_model');
        $this->lang->load('trips_reports');
        $this->lang->load('admins/transporters');
        $this->load->helper('trip');
        $this->load->helper('array');
        permission();
        if ($this->destination == 'ito')
            $this->ito_id = session('ito_id');
    }

    public function index() {

        if (!$_POST) {
            $this->trips_reports_model->from_date = date("Y-m-d");
            $this->trips_reports_model->to_date = date("Y-m-d");
        }

        //from_date
        if ($this->input->post('from_date')) {
            $this->trips_reports_model->from_date = $this->input->post('from_date');
        }

        //to_date
        if ($this->input->post('to_date')) {
            $this->trips_reports_model->to_date = $this->input->post('to_date');
        }

        //from_time
        if ($this->input->post('from_time')) {
            $this->trips_reports_model->from_time = $this->input->post('from_time');
        }

        //to_time
        if ($this->input->post('to_time')) {
            $this->trips_reports_model->to_time = $this->input->post('to_time');
        }

        //from_count
        if ($this->input->post('from_count')) {
            $this->trips_reports_model->from_count = $this->input->post('from_count');
        }

        //to_count
        if ($this->input->post('to_count')) {
            $this->trips_reports_model->to_count = $this->input->post('to_count');
        }

        //safa_ito_id
        if ($this->input->post('safa_ito_id')) {
            $this->trips_reports_model->safa_ito_id = $this->input->post('safa_ito_id');
        }

        //safa_internalsegmenttype_id
        if ($this->input->post('safa_internalsegmenttype_id')) {
            $this->trips_reports_model->safa_internalsegmenttype_id = $this->input->post('safa_internalsegmenttype_id');
        }

        //operator_reference
        if ($this->input->post('operator_reference')) {
            $this->trips_reports_model->operator_reference = $this->input->post('operator_reference');
        }

     	if ($this->input->post('safa_uo_id')) {
            $this->trips_reports_model->uo_id = $this->input->post('safa_uo_id');
        }
        if ($this->input->post('safa_uo_contract_id')) {
            $this->trips_reports_model->safa_uo_contract_id = $this->input->post('safa_uo_contract_id');
        }

        //------------ By Gouda --------------
        if ($this->input->post('from_city')) {
            $this->trips_reports_model->erp_city_id = $this->input->post('from_city');
        }
        if ($this->input->post('to_city')) {
            $this->trips_reports_model->end_erp_city_id = $this->input->post('to_city');
        }
        //-------------------------------------
        //from_erp_hotel_id
        if ($this->input->post('from_erp_hotel_id')) {
            $this->trips_reports_model->from_erp_hotel_id = $this->input->post('from_erp_hotel_id');
        }

        //to_erp_hotel_id
        if ($this->input->post('to_erp_hotel_id')) {
            $this->trips_reports_model->to_erp_hotel_id = $this->input->post('to_erp_hotel_id');
        }

        //erp_port_id
        if ($this->input->post('erp_port_id')) {
            $this->trips_reports_model->erp_port_id = $this->input->post('erp_port_id');
        }

        $data['uos_options'] = $this->trips_reports_model->uos_options($this->ito_id);

        $data['uo_contracts_options'] = $this->trips_reports_model->uo_contracts_options($this->ito_id);

        //fill data
        //$data['trips_ds'] = $this->trips_reports_model->all_movements_ito($this->ito_id);
		$this->trips_reports_model->ito_id = $this->ito_id;
        $data['trips_ds'] = $this->trips_reports_model->all_movements();
		

        if ($this->input->post('cols'))
            $data['cols'] = $this->input->post('cols');
        else
            $data['cols'] = array('contract', 'operator', 'operation_order_id', 'segment_type', 'starting', 'from', 'to', 'seats');


        $data['hotels'] = $this->trips_reports_model->get_hotels();

        $data['places'] = $this->trips_reports_model->get_places();

        $data['ports'] = $this->trips_reports_model->get_ports();


        if ($this->input->post('excel_export')) {

            $this->layout = 'js';

            $this->load->helper('download');

            $data['print_statement'] = "";

            $data = $this->load->view("ito/trips_reports/all_movements_ito_excel", $data, TRUE);

            $name = 'all-ito-' . date('Y-m-d-His') . '.xls';

            force_download($name, $data);

            $data['mydata'] = $data;
        } elseif ($this->input->post('print_report')) {

            $this->layout = 'js';

            $data['print_statement'] = "<script>print()</script>";

            $this->load->view("ito/trips_reports/all_movements_ito_excel", $data);

            $data['mydata'] = $data;

            $data['count'] = count($data);
        } else {

            $this->load->view("ito/trips_reports/all_movements_ito", $data);
        }
    }

    function add_drivers($id = FALSE) {

        if (!$id)
            show_404();

        $this->internalpassages_model->safa_internalsegment_id = $id;
        $items = $this->internalpassages_model->get();
        if (!$items)
            show_404();
        $data['drivers'] = json_decode($items->ito_driver_info);
        $data['vehicle'] = json_decode($items->ito_vehicle_info);
        if ($data) {
            $this->load->library("form_validation");

            $m = 0;
            $s = 1;
            for ($i = 1; $i < sizeof($_POST) / 2; $i++) {

                if ($_POST["driver" . $i . ""][$m] == "" && $_POST["driver" . $i . ""][$s] == "" && $_POST["car" . $i . ""][$m] == "" && $_POST["car" . $i . ""][$s] == "") {

                    $this->form_validation->set_rules("driver" . $i . "[$m]", 'lang:driver_name', 'trim');
                    $this->form_validation->set_rules("driver" . $i . "[$s]", 'lang:driver_phone', 'trim|integer');
                    $this->form_validation->set_rules("car" . $i . "[$m]", 'lang:driver_vehicle_number', 'trim');
                    $this->form_validation->set_rules("car" . $i . "[$s]", 'lang:driver_vehicle_type', 'trim');
                } else {
                    $this->form_validation->set_rules("driver" . $i . "[$m]", 'lang:driver_name', 'trim|required');
                    $this->form_validation->set_rules("driver" . $i . "[$s]", 'lang:driver_phone', 'trim|integer|required');
                    $this->form_validation->set_rules("car" . $i . "[$m]", 'lang:driver_vehicle_number', 'trim|required');
                    $this->form_validation->set_rules("car" . $i . "[$s]", 'lang:driver_vehicle_type', 'trim|required');
                }
            }

            if ($this->form_validation->run() == false) {
                $this->load->view("ito/trips_reports/drivers", $data);
            } else {
                $i = 1;
                $a = 1;
                foreach ($_POST as $key => $value) {
                    if ($key == "driver" . $i . "" && is_array($value)) {
                        $driver_info[] = $value;
                        $i++;
                    } elseif ($key == "car" . $a . "" && is_array($value)) {
                        $vehicle_info[] = $value;
                        $a++;
                    }
                }
                $w = 1;
                $s = 0;
                for ($i = 0; $i < sizeof($driver_info); $i++) {
                    if ($driver_info[$i][$s] == "" && $driver_info[$i][$w] == "" && $vehicle_info[$i][$s] == "" && $vehicle_info[$i][$w] == "") {
                        
                    } else {
                        $driver_infos[] = $driver_info[$i];
                        $vehicle_infos[] = $vehicle_info[$i];
                    }
                }
                if (!empty($driver_infos) && count($driver_infos) >= 1 && is_array($driver_infos)) {
                    $driver_infos = json_encode($driver_infos);
                    $vehicle_infos = json_encode($vehicle_infos);
                    $this->internalpassages_model->ito_driver_info = $driver_infos;
                    $this->internalpassages_model->ito_vehicle_info = $vehicle_infos;
                    $this->internalpassages_model->safa_internalsegment_id = $id;

                    $this->internalpassages_model->save();
//      $this->load->view('redirect_message', array('msg' => lang('global_added_message'), 'url' => site_url('ito/all_movements_ito')));  
                    redirect("ito/all_movements_ito");
                } else {
                    $this->internalpassages_model->ito_driver_info = NULL;
                    $this->internalpassages_model->ito_vehicle_info = NULL;
                    $this->internalpassages_model->safa_internalsegment_id = $id;


                    $this->internalpassages_model->save();
                    redirect("ito/all_movements_ito");
                }
            }
        } else {
            $this->load->library("form_validation");
            for ($i = 1; $i < sizeof($_POST) / 2; $i++) {
                if ($_POST["driver" . $i . ""][$m] == "" && $_POST["driver" . $i . ""][$s] == "" && $_POST["car" . $i . ""][$m] == "" && $_POST["car" . $i . ""][$s] == "") {

                    $this->form_validation->set_rules("driver" . $i . "[$m]", 'lang:driver_name', 'trim');
                    $this->form_validation->set_rules("driver" . $i . "[$s]", 'lang:driver_phone', 'trim|integer');
                    $this->form_validation->set_rules("car" . $i . "[$m]", 'lang:driver_vehicle_number', 'trim');
                    $this->form_validation->set_rules("car" . $i . "[$s]", 'lang:driver_vehicle_type', 'trim');
                } else {

                    $this->form_validation->set_rules("driver" . $i . "[$m]", 'lang:driver_name', 'trim|required');
                    $this->form_validation->set_rules("driver" . $i . "[$s]", 'lang:driver_phone', 'trim|integer|required');
                    $this->form_validation->set_rules("car" . $i . "[$m]", 'lang:driver_vehicle_number', 'trim|required');
                    $this->form_validation->set_rules("car" . $i . "[$s]", 'lang:driver_vehicle_type', 'trim|required');
                }
            }
            if ($this->form_validation->run() == false) {
                $this->load->view("ito/trips_reports/drivers");
            } else {
                //for Driver_info
                $i = 1;
                $a = 1;
                foreach ($_POST as $key => $value) {
                    if ($key == "driver" . $i . "" && is_array($value)) {
                        $driver_info[] = $value;
                        $i++;
                    } elseif ($key == "car" . $a . "" && is_array($value)) {
                        $vehicle_info[] = $value;
                        $a++;
                    }
                }
                $w = 1;
                $s = 0;
                for ($i = 0; $i < sizeof($driver_info); $i++) {
                    if ($driver_info[$i][$s] == "" && $driver_info[$i][$w] == "" && $vehicle_info[$i][$s] == "" && $vehicle_info[$i][$w] == "") {
                        
                    } else {
                        $driver_infos[] = $driver_info[$i];
                        $vehicle_infos[] = $vehicle_info[$i];
                    }
                }
                if (!empty($driver_infos) && count($driver_infos) >= 1 && is_array($driver_infos)) {
                    $driver_infos = json_encode($driver_infos);
                    $vehicle_infos = json_encode($vehicle_infos);
                    $this->internalpassages_model->ito_driver_info = $driver_infos;
                    $this->internalpassages_model->ito_vehicle_info = $vehicle_infos;
                    $this->internalpassages_model->safa_internalsegment_id = $id;


                    $this->internalpassages_model->save();
//   $this->load->view('redirect_message', array('msg' => lang('global_added_message'), 'url' => site_url('ito/all_movements_ito')));  
                    redirect("ito/all_movements_ito");
                } else {
                    $this->internalpassages_model->ito_driver_info = NULL;
                    $this->internalpassages_model->ito_vehicle_info = NULL;
                    $this->internalpassages_model->safa_internalsegment_id = $id;


                    $this->internalpassages_model->save();
                    redirect("ito/all_movements_ito");
                }
            }
        }
    }

    //By Gouda.
    function drivers_and_buses($id = FALSE) {
        $this->internalpassages_model->safa_internalsegment_id = $id;
        $safa_internalsegment_row = $this->internalpassages_model->get();
        $safa_trip_internaltrip_id = $safa_internalsegment_row->safa_trip_internaltrip_id;

        $this->trip_internaltrip_model->safa_trip_internaltrip_id = $safa_trip_internaltrip_id;
        $safa_internalsegment_row = $this->trip_internaltrip_model->get();
        $safa_transporters_id = $safa_internalsegment_row->safa_transporter_id;

        $structure = array('safa_transporters_drivers_id', name());
        $key = $structure['0'];
        if (isset($structure['1'])) {
            $value = $structure['1'];
        }
        $drivers_arr = array();
        $this->transporters_drivers_model->safa_transporters_id = $safa_transporters_id;
        $drivers = $this->transporters_drivers_model->get();
        $drivers_arr[""] = lang('global_select_from_menu');
        foreach ($drivers as $driver) {
            $drivers_arr[$driver->$key] = $driver->$value;
        }
        $data['safa_transporters_drivers'] = $drivers_arr;


        $structure = array('safa_transporters_buses_id', 'bus_no');
        $key = $structure['0'];
        if (isset($structure['1'])) {
            $value = $structure['1'];
        }
        $buses_arr = array();
        $this->transporters_buses_model->safa_transporters_id = $safa_transporters_id;
        $buses = $this->transporters_buses_model->get();
        $buses_arr[""] = lang('global_select_from_menu');
        foreach ($buses as $bus) {
            $buses_arr[$bus->$key] = $bus->$value;
        }
        $data['safa_transporters_buses'] = $buses_arr;


        $this->internalsegments_drivers_and_buses_model->safa_internalsegments_id = $id;
        $item_drivers_and_buses = $this->internalsegments_drivers_and_buses_model->get();
        $data['item_drivers_and_buses'] = $item_drivers_and_buses;


        $this->load->library("form_validation");
        //$this->form_validation->set_rules('safa_transporters_drivers_id', 'lang:safa_transporters_drivers_id', 'trim|required');
        //$this->form_validation->set_rules('safa_transporters_buses_id', 'lang:safa_transporters_buses_id', 'trim|required');
        //if ($this->form_validation->run() == false) {
        if (!$this->input->post('smt_save')) {
            $this->load->view("ito/trips_reports/drivers_and_buses", $data);
        } else {
            //$this->internalsegments_drivers_and_buses_model->safa_internalsegments_id = $id;
            // Save driver_and_buss
            $drivers_and_buses_safa_transporters_drivers = $this->input->post('drivers_and_buses_safa_transporters_drivers');
            $drivers_and_buses_safa_transporters_buses = $this->input->post('drivers_and_buses_safa_transporters_buses');

            if (ensure($drivers_and_buses_safa_transporters_drivers))
                foreach ($drivers_and_buses_safa_transporters_drivers as $key => $value) {
                    $this->internalsegments_drivers_and_buses_model->safa_transporters_drivers_id = $drivers_and_buses_safa_transporters_drivers[$key];
                    $this->internalsegments_drivers_and_buses_model->safa_transporters_buses_id = $drivers_and_buses_safa_transporters_buses[$key];


                    $this->internalsegments_drivers_and_buses_model->safa_internalsegments_drivers_and_buses_id = $key;
                    $current_driver_and_bus_row_count = $this->internalsegments_drivers_and_buses_model->get(true);
                    if ($current_driver_and_bus_row_count == 0) {
                        $this->internalsegments_drivers_and_buses_model->safa_internalsegments_drivers_and_buses_id = false;
                        $this->internalsegments_drivers_and_buses_model->save();
                    } else {
                        $this->internalsegments_drivers_and_buses_model->save();
                    }
                }
            // --- Delete Deleted driver_and_buss rows from Database --------------
            //By Gouda
            $this->internalsegments_drivers_and_buses_model->safa_transporters_drivers_id = false;
            $this->internalsegments_drivers_and_buses_model->safa_transporters_buses_id = false;

            $drivers_and_buses_remove = $this->input->post('drivers_and_buses_remove');
            if (ensure($drivers_and_buses_remove)) {
                foreach ($drivers_and_buses_remove as $driver_and_bus_remove) {
                    $this->internalsegments_drivers_and_buses_model->safa_internalsegments_drivers_and_buses_id = $driver_and_bus_remove;
                    $this->internalsegments_drivers_and_buses_model->delete();
                }
            }
            //---------------------------------------------------------------



            $this->load->view('redirect_message', array('msg' => lang('global_added_message'), 'url' => site_url('ito/all_movements_ito'), 'model_name' => 'all_movements_ito', 'model_title' => lang('menu_ito_movements'), 'action' => lang('menu_ito_movements')));
        }
    }

    function getDriverData() {
        $safa_transporters_drivers_id = $this->input->post('safa_transporters_drivers_id');


        $this->transporters_drivers_model->safa_transporters_drivers_id = $safa_transporters_drivers_id;
        $driver_row = $this->transporters_drivers_model->get();
        if(count($driver_row)>0) {
        echo lang('phone') . ':' . $driver_row->phone;
        }
        exit;
    }

    function getBusData() {
        $safa_transporters_buses_id = $this->input->post('safa_transporters_buses_id');

        $this->transporters_buses_model->safa_transporters_buses_id = $safa_transporters_buses_id;
        $this->transporters_buses_model->join = true;
        $bus_row = $this->transporters_buses_model->get();

        if(count($bus_row)>0) {
        echo lang('safa_buses_brands_id') . ':' . $bus_row->buses_brands_name . '<br/>';
        echo lang('safa_buses_models_id') . ':' . $bus_row->buses_models_name . '<br/>';
        echo lang('industry_year') . ':' . $bus_row->industry_year . '<br/>';
        echo lang('passengers_count') . ':' . $bus_row->passengers_count . '<br/>';
        }
        exit;
    }

    function alpha_dash($str) {
        if ((!preg_match("/^[\-_ \d\p{Arabic}]*\p{Arabic}[\d\p{Arabic}]*$/ui", $str)) && (!preg_match("/^([-a-z0-9_-]*$)+$/i", $str))) {
            $this->form_validation->set_message('alpha_dash', lang('driver_name_invalid'));
            return FALSE;
        } else {
            return TRUE;
        }
    }

    function alpha_dash_carNumber($str) {
        if ((!preg_match("/^[\-_ \d\p{Arabic}]*\p{Arabic}[\d\p{Arabic}]*$/ui", $str)) && (!preg_match("/^([-a-z0-9_-]*$)+$/i", $str))) {
            $this->form_validation->set_message('alpha_dash_carNumber', lang('driver_vehicle_number_invalid'));
            return FALSE;
        } else {
            return TRUE;
        }
    }

    function alpha_dash_carType($str) {
        if ((!preg_match("/^[\-_ \d\p{Arabic}]*\p{Arabic}[\d\p{Arabic}]*$/ui", $str)) && (!preg_match("/^([-a-z0-9_-]*$)+$/i", $str))) {
            $this->form_validation->set_message('alpha_dash_carType', lang('driver_vehicle_type_invalid'));
            return FALSE;
        } else {
            return TRUE;
        }
    }

}
