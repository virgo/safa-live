<?php

class dashboard extends Safa_Controller {

    public $module = "currently_report";
    public $ito_id;
    public $arriving_pane = "";
    public $departing_pane = "";

    public function __construct() {
        parent::__construct();

        //Side menu session, By Gouda.
        session('side_menu_id', 1);

        $this->layout = 'new';

        $this->load->model('trips_reports_model');
        $this->lang->load('trips_reports');
        $this->load->helper('trip');
        if ($this->destination == 'ito')
            $this->ito_id = session('ito_id');
    }

    public function index() {

        //init value of link
        if (!$this->input->get("arrival")) {
            $_GET['arrival'] = 'today';
            $this->trips_reports_model->arriving_today = true;
        } elseif ($this->input->get("arrival") == "today") {

            $this->trips_reports_model->arriving_today = true;
        } elseif ($this->input->get("arrival") == "twm") {

            $this->trips_reports_model->arriving_twm = true;
        }


        if (!$this->input->get("departure")) {

            $_GET['departure'] = 'today';
            $this->trips_reports_model->departing_today = true;
        } elseif ($this->input->get("departure") == "today") {

            $this->trips_reports_model->departing_today = true;
        } elseif ($this->input->get("departure") == "twm") {

            $this->trips_reports_model->departing_twm = true;
        }





        //add extra title to box of departing
        if ($this->trips_reports_model->departing_twm) {
            $this->departing_pane = " - " . lang('twm');
        } else {
            $this->departing_pane = " - " . lang('today');
        }

        if ($this->trips_reports_model->arriving_twm) {
            $this->arriving_pane = " - " . lang('twm');
        } else {
            $this->arriving_pane = " - " . lang('today');
        }

        $data['uos_options'] = $this->trips_reports_model->uos_options($this->ito_id);

        $data['uo_contracts_options'] = $this->trips_reports_model->uo_contracts_options($this->ito_id);

        $data['arriving'] = $this->trips_reports_model->all_movements_ito($this->ito_id, 1, 'arr_filters', false);
        $data['departing'] = $this->trips_reports_model->all_movements_ito($this->ito_id, 2, false, 'dep_filters');
        
        

        if ($this->input->get('export_arr_report') || $this->input->get('export_dep_report')) {
            $this->layout = 'js';
            $this->load->helper('download');
            $data['print_statement'] = "";
            $data = $this->load->view("ito/trips_reports/currently_report_ito_excel", $data, TRUE);
            $name = 'cur-ito-' . date('Y-m-d-His') . '.xls';
            force_download($name, $data);
        } elseif ($this->input->get('print_arr_report') || $this->input->get('print_dep_report')) {


            $this->layout = 'js';
            $data['print_statement'] = "<script>print()</script>";
            $this->load->view("ito/trips_reports/currently_report_ito_excel", $data);
        } else {

            $this->load->view("ito/trips_reports/currently_report_ito", $data);
        }
    }

}
