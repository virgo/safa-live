<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class arriving_report_ito extends Safa_Controller {

    public $module = "trips_reports";
    public $ito_id;

    public function __construct() {
        parent::__construct();
        //Side menu session, By Gouda.
        session('side_menu_id', 1);
        $this->layout = 'new';
        $this->load->model('trips_reports_model');
        $this->lang->load('trips_reports');
        $this->load->helper('trip');
        if ($this->destination == 'ito')
            $this->ito_id = session('ito_id');
    }

    public function index() {
        //nationality_id
        if ($this->input->post('nationality_id')) {
            $this->trips_reports_model->nationality_id = $this->input->post('nationality_id');
        }

        //erp_hotel_id
        if ($this->input->post('erp_hotel_id')) {
            $this->trips_reports_model->erp_hotel_id = $this->input->post('erp_hotel_id');
        }

        //safa_uo_contract_id
        if ($this->input->post('safa_uo_contract_id')) {
            $this->trips_reports_model->safa_uo_contract_id = $this->input->post('safa_uo_contract_id');
        }



        //erp_port_id
        if ($this->input->post('erp_port_id')) {
            $this->trips_reports_model->erp_port_id = $this->input->post('erp_port_id');
        }


        //erp_city_id
        if ($this->input->post('erp_city_id')) {
            $this->trips_reports_model->erp_city_id = $this->input->post('erp_city_id');
        }

        //safa_transporter_id
        if ($this->input->post('safa_transporter_id')) {
            $this->trips_reports_model->safa_transporter_id = $this->input->post('safa_transporter_id');
        }

        //from_date
        if ($this->input->post('from_date')) {
            $this->trips_reports_model->from_date = $this->input->post('from_date');
        }

        //to_date
        if ($this->input->post('to_date')) {
            $this->trips_reports_model->to_date = $this->input->post('to_date');
        }

        //from_time
        if ($this->input->post('from_time')) {
            $this->trips_reports_model->from_time = $this->input->post('from_time');
        }

        //to_time
        if ($this->input->post('to_time')) {
            $this->trips_reports_model->to_time = $this->input->post('to_time');
        }

        //from_count
        if ($this->input->post('from_count')) {
            $this->trips_reports_model->from_count = $this->input->post('from_count');
        }

        //to_count
        if ($this->input->post('to_count')) {
            $this->trips_reports_model->to_count = $this->input->post('to_count');
        }


        $data['uos_options'] = $this->trips_reports_model->uos_options($this->ito_id);

        $data['uo_contracts_options'] = $this->trips_reports_model->uo_contracts_options($this->ito_id);

        $data['trips_ds'] = $this->trips_reports_model->all_movements_ito($this->ito_id, 1);
		
        
        
        if ($this->input->post('cols'))
            $data['cols'] = $this->input->post('cols');
        else
            $data['cols'] = array('code', 'arrival_date', 'arrival_time', 'hotel', 'contract', 'cn', 'nationality', 'port', 'uo', 'supervisor', 'status');

        if ($this->input->post('excel_export')) {

            $this->layout = 'js';
            $this->load->helper('download');
            $data['print_statement'] = "";
            $data = $this->load->view("ito/trips_reports/arriving_report_ito_excel", $data, TRUE);
            $name = 'arr-ito-' . date('Y-m-d-His') . '.xls';
            force_download($name, $data);
        } elseif ($this->input->post('print_report')) {
            $this->layout = 'js';
            $data['print_statement'] = "<script>print()</script>";
            $this->load->view("ito/trips_reports/arriving_report_ito_excel", $data);
        } else {
            $this->load->view("ito/trips_reports/arriving_report_ito", $data);
        }
    }

}
