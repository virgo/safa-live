<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Trip_internaltrip extends Safa_Controller {

    public $module = "trip_internaltrip";
    public $temp_upload_path;
    public $uploaded_file_name;

    public function __construct() {
        parent::__construct();
        $this->layout = 'new';
        //Side menu session, By Gouda.
        session('side_menu_id', 2);
        $this->load->model('trip_internaltrip_model');
        $this->load->model('internalpassages_model');
        //By Gouda, For Notifications
        $this->load->model('contracts_model');
        $this->load->model('notification_model');
        $this->load->model('internaltrip_status_model');
        $this->load->model('safa_trips_model');
        
        
        
        //$this->load->library('showfields');

        $this->lang->load('trip_internaltrip');
        $this->lang->load('internalpassages');
        $this->load->helper('form_helper');
        $this->load->helper('db_helper');
        $this->load->helper('itconf_helper');
        permission();
    	if (session('ito_id')) {
			$this->trip_internaltrip_model->ito_id = session('ito_id');
			$this->safa_trips_model->safa_ito_id = session('ito_id');
		} else {
            show_404();
		}
    }

    public function index() {
        $data["uo_contracts"] = $this->create_contracts_array();
        $data["transporters"] = $this->create_transportes_array(); /* update after phase 2 */

        $data["safa_intrernaltripstatus"] = ddgen("safa_internaltripstatus", array("safa_internaltripstatus_id", name()));
        $data["safa_intrernaltripstatus_color"] = $this->db->select(name() . ',color,code')->from('safa_internaltripstatus')->get()->result();

        $this->trip_internaltrip_model->safa_trip_id = null;

        if (isset($_GET['search']))
            $this->search();
        $data["total_rows"] = $this->trip_internaltrip_model->get(true);

        $this->trip_internaltrip_model->offset = $this->uri->segment("4");
        //$this->trip_internaltrip_model->limit = $this->config->item('per_page');
        $this->trip_internaltrip_model->order_by = array('safa_trip_internaltrip_id', 'desc');
        $data["items"] = $this->trip_internaltrip_model->get();
        $config['uri_segment'] = 4;
        $config['base_url'] = site_url('ito/trip_internaltrip/index');
        $config['total_rows'] = $data["total_rows"];

        $config['per_page'] = $this->trip_internaltrip_model->limit;
        $config['suffix'] = $this->input->get() ? '?' . http_build_query($_GET) : NULL;
        $this->load->library('pagination');
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();
        $this->load->view('ito/trip_internaltrip/index', $data);
    }

    public function incoming() {

        $data["uo_contracts"] = $this->create_contracts_array();
        $data["transporters"] = $this->create_transportes_array(); /* update after phase 2 */

        $data["safa_intrernaltripstatus"] = ddgen("safa_internaltripstatus", array("safa_internaltripstatus_id", name()));
        $data["safa_intrernaltripstatus_color"] = $this->db->select(name() . ',color,code')->from('safa_internaltripstatus')->get()->result();

        //By Gouda, to filter
        //$this->trip_internaltrip_model->safa_trip_id!=null;


        if (isset($_GET['search']))
            $this->search();
        $data["total_rows"] = $this->trip_internaltrip_model->get(true);

        $this->trip_internaltrip_model->offset = $this->uri->segment("4");
        //$this->trip_internaltrip_model->limit = $this->config->item('per_page');
        $this->trip_internaltrip_model->order_by = array('safa_trip_internaltrip_id', 'desc');
        $data["items"] = $this->trip_internaltrip_model->get();
        $config['uri_segment'] = 4;
        $config['base_url'] = site_url('ito/trip_internaltrip/incoming');
        $config['total_rows'] = $data["total_rows"];

        $config['per_page'] = $this->trip_internaltrip_model->limit;
        $config['suffix'] = $this->input->get() ? '?' . http_build_query($_GET) : NULL;
        $this->load->library('pagination');
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();
        $this->load->view('ito/trip_internaltrip/incoming', $data);
    }

    public function add() {
        /* get the refrance data */
        $data["uo_contracts"] = $this->create_contracts_array();
        $data["transporters"] = $this->create_transportes_array();
        $data["safa_intrernaltripstatus"] = ddgen("safa_internaltripstatus", array("safa_internaltripstatus_id", name()));
        //$data["safa_ito"] = $this->get_ea_contracts_itos();
        $data['uo_companies'] = ddgen('safa_uos', array('safa_uo_id', name()), FALSE, FALSE, TRUE);



        $this->load->library("form_validation");
        /*         * ************* */
        $this->form_validation->set_rules('datetime', 'lang:transport_request_date', 'trim|required');
        //$this->form_validation->set_rules('uo_contract_id', 'lang:transport_request_contract', 'trim|required');
        $this->form_validation->set_rules('safa_internaltripstatus_id', 'lang:transport_request_status', 'trim|required');
        $this->form_validation->set_rules('safa_transporter_id', 'lang:transport_request_transportername', 'trim');
        $this->form_validation->set_rules('operator_reference', 'lang:transport_request_res_code', 'trim|required');

        if ($this->form_validation->run() == false) {
            $this->load->view("ito/trip_internaltrip/add", $data);
        } else {


            if (isset($_FILES["transport_request_res_file"])) {
                if (count($_FILES["transport_request_res_file"]['tmp_name']) > 0) {
                    //Get the temp file path
                    $tmpFilePath = $_FILES["transport_request_res_file"]['tmp_name'];

                    //Make sure we have a filepath
                    if ($tmpFilePath != "") {
                        //Setup our new file path
                        $newFilePath = './static/temp/ea_files/' . $_FILES["transport_request_res_file"]['name'];

                        //Upload the file into the temp dir
                        //To solve arabic files names problem.
                        $is_file_uploaded = move_uploaded_file($tmpFilePath, iconv('utf-8', 'windows-1256', $newFilePath));
                        //$is_file_uploaded=move_uploaded_file($tmpFilePath, $newFilePath);



                        $file_name = $_FILES["transport_request_res_file"]['name'];
                        $this->trip_internaltrip_model->attachement = $file_name;
                    }
                }
            }

            $this->trip_internaltrip_model->safa_trip_id = null;
            $this->trip_internaltrip_model->safa_ito_id = session('ito_id');

            if ($this->input->post('safa_transporter_id'))
                $this->trip_internaltrip_model->safa_transporter_id = $this->input->post('safa_transporter_id');

            $this->trip_internaltrip_model->safa_internaltripstatus_id = $this->input->post('safa_internaltripstatus_id');
            $this->trip_internaltrip_model->operator_reference = $this->input->post('operator_reference');


            $this->trip_internaltrip_model->confirmation_number = gen_itconf();

            $this->trip_internaltrip_model->datetime = $this->input->post('datetime');
            $this->trip_internaltrip_model->ito_notes = $this->input->post('ito_notes');

            //By Gouda.
            $this->trip_internaltrip_model->erp_company_type_id = $this->input->post('company_type');
            if ($this->input->post('company_type') != '') {
                $this->trip_internaltrip_model->erp_company_id = $this->input->post('erp_company_id');
            } else {
                $this->trip_internaltrip_model->erp_company_name = $this->input->post('erp_company_name');
            }


            $internaltrip_id = $this->trip_internaltrip_model->save();

            /*
              if ($file_name)// uploading when the file exist//
              $this->move_upload();
             */

            //By Gouda.
            //----------- Send Notification For UO, EA Company ------------------------------
       		
        	if($this->input->post('company_type')!='') {
        	$msg_datetime = date('Y-m-d H:i', time());
    
       		$this->notification_model->notification_type = 'automatic';
        	$this->notification_model->erp_importance_id = 1;
        	$this->notification_model->sender_type_id = 1;
	        $this->notification_model->sender_id = 0;
	        
	        $this->notification_model->erp_system_events_id = 19;
	        
	        $this->notification_model->language_id = 2;
	        $this->notification_model->msg_datetime = $msg_datetime;
        
	        
	        $internaltripstatus_name='';
	        $safa_internaltripstatus_id=$this->input->post('safa_internaltripstatus_id');
	        $this->internaltrip_status_model->safa_internaltripstatus_id=$safa_internaltripstatus_id;
	        $internaltrip_status_row=$this->internaltrip_status_model->get();
	        if(count($internaltrip_status_row)>0) {
	        	$internaltripstatus_name=$internaltrip_status_row->{name()};
	        }
	        
	        /*
	        $trip_name='';
	        $safa_trip_id=$this->input->post('safa_trip_id');
	        $this->safa_trips_model->safa_trip_id=$safa_trip_id;
	        $trip_row=$this->safa_trips_model->get();
	        if(count($trip_row)>0) {
	        	$trip_name=$trip_row->{name()};
	        }
	        */
	        
	        //Link to replace.
	        if($this->input->post('company_type')==2) {
	        	$link_internaltrip ="<a href='".site_url('uo/trip_internaltrip/edit/'.$internaltrip_id)."'  target='_blank' > $internaltrip_id </a>";
	        } else {
	        	$link_internaltrip ="<a href='".site_url('ea/trip_internaltrip/edit/'.$internaltrip_id)."'  target='_blank' > $internaltrip_id </a>";
	        }
	        $this->notification_model->tags = "#*safa_trip_internaltrip_id#*=$link_internaltrip*****#*internaltripstatus#*=$internaltripstatus_name*****#*the_date#*=$msg_datetime*****#*trip#*=";
	        $this->notification_model->parent_id = '';
	        	        
	        $erp_notification_id=$this->notification_model->save();
	        
	        //-------- Notification Details ---------------
	        $this->notification_model->detail_erp_notification_id = $erp_notification_id;
         	$this->notification_model->detail_receiver_type = $this->input->post('company_type');
         	$this->notification_model->detail_receiver_id = $this->input->post('erp_company_id');
        	$this->notification_model->saveDetails();
         	
	        //--------------------------------
        	}
	        
	        //-------------------------------------------------------------------
       		
            	
            	$this->load->view('redirect_message', array('msg' => lang('global_added_message'),
                'url' => site_url('ito/trip_internaltrip/edit/'.$internaltrip_id),
                'model_title' => lang('node_title'), 'action' => lang('add_transport_request')));

        }
    }

    public function edit($id = FALSE) {
        if (!$id)
            show_404();
        $this->trip_internaltrip_model->safa_trip_internaltrip_id = $id;
        $data['item'] = $this->trip_internaltrip_model->get();
        if (!$data['item'])
            show_404();

        $data["safa_intrernaltripstatus"] = ddgen("safa_internaltripstatus", array("safa_internaltripstatus_id", name()));
        $data["transporters"] = $this->create_transportes_array();

        $data["safa_internalpassagetypes"] = ddgen("safa_internalsegmenttypes", array("safa_internalsegmenttype_id", name()));

        $data["erp_hotels"] = ddgen("erp_hotels", array("erp_hotel_id", name()), '', '', true);
        $data["safa_tourismplaces"] = ddgen("safa_tourismplaces", array("safa_tourismplace_id", name()), '', '', true);
        $data["erp_ports"] = ddgen("erp_ports", array("erp_port_id", name()), '', '', true);

        $item = $data['item'];
        if ($item->erp_company_type_id == 3) {
            $data['companies'] = ddgen('safa_eas', array('safa_ea_id', name()), FALSE, FALSE, TRUE);
        } else if ($item->erp_company_type_id == 2) {
            $data['companies'] = ddgen('safa_uos', array('safa_uo_id', name()), FALSE, FALSE, TRUE);
        } else {
            $data['companies'] = ddgen('safa_uos', array('safa_uo_id', name()), FALSE, FALSE, TRUE);
        }


        /* --------- get the internal passage ----------------------------------------------------- */;
        $this->internalpassages_model->order_by = array("safa_internalsegments.start_datetime", 'ASC');
        $this->internalpassages_model->safa_trip_internaltrip_id = $id;
        $data["internalpassages"] = $this->internalpassages_model->get();

        $data["trip_internal_id"] = $id;

        /* ---------------------------------------------------------------------------------------------------------------------------------------- */
        $this->load->library("form_validation");

        $this->form_validation->set_rules('datetime', 'lang:transport_request_date', 'trim|required');
        $this->form_validation->set_rules('safa_internaltripstatus_id', 'lang:transport_request_status', 'trim|required');
        $this->form_validation->set_rules('safa_transporter_id', 'lang:transport_request_transportername', 'trim');
        $this->form_validation->set_rules('operator_reference', 'lang:transport_request_res_code', 'trim|required');

        if ($this->form_validation->run() == false) {
            $this->load->view("ito/trip_internaltrip/edit", $data);
        } else {
            if ($this->input->post('safa_transporter_id'))
                $this->trip_internaltrip_model->safa_transporter_id = $this->input->post('safa_transporter_id');
            else
                $this->trip_internaltrip_model->safa_transporter_id = NULL;

            $this->trip_internaltrip_model->safa_internaltripstatus_id = $this->input->post('safa_internaltripstatus_id');
            $this->trip_internaltrip_model->operator_reference = $this->input->post('operator_reference');
            $this->trip_internaltrip_model->ito_notes = $this->input->post('ito_notes');
            $this->trip_internaltrip_model->datetime = $this->input->post('datetime');

            if (isset($_FILES["transport_request_res_file"])) {
                if (count($_FILES["transport_request_res_file"]['tmp_name']) > 0) {
                    //Get the temp file path
                    $tmpFilePath = $_FILES["transport_request_res_file"]['tmp_name'];

                    //Make sure we have a filepath
                    if ($tmpFilePath != "") {
                        //Setup our new file path
                        $newFilePath = './static/temp/ea_files/' . $_FILES["transport_request_res_file"]['name'];

                        //Upload the file into the temp dir
                        //To solve arabic files names problem.
                        $is_file_uploaded = move_uploaded_file($tmpFilePath, iconv('utf-8', 'windows-1256', $newFilePath));
                        //$is_file_uploaded=move_uploaded_file($tmpFilePath, $newFilePath);

                        $file_name = $_FILES["transport_request_res_file"]['name'];
                        $this->trip_internaltrip_model->attachement = $file_name;
                    }
                }
            }

            $this->trip_internaltrip_model->erp_company_type_id = $this->input->post('company_type');
            if ($this->input->post('company_type') != '') {
                $this->trip_internaltrip_model->erp_company_id = $this->input->post('erp_company_id');
            } else {
                $this->trip_internaltrip_model->erp_company_name = $this->input->post('erp_company_name');
            }

            $this->trip_internaltrip_model->save();



            // Save internalpassages
            $safa_internalsegmenttype_id = $this->input->post('internalpassage_type');
            $start_datetime = $this->input->post('internalpassage_startdatatime');
            $end_datetime = $this->input->post('internalpassage_enddatatime');
            $seats_count = $this->input->post('seatscount');
            $notes = $this->input->post('internalpassage_notes');
            $safa_internalsegmentestatus_id = 1;


            $erp_end_hotel_id = $this->input->post('internalpassage_hotel_end');
            $erp_start_hotel_id = $this->input->post('internalpassage_hotel_start');

            $erp_port_start_id = $this->input->post('internalpassage_port_start');
            $erp_port_end_id = $this->input->post('internalpassage_port_end');

            //$safa_tourism_place_start_id = $this->input->post('internalpassages_torismplace_start');
            $safa_tourism_place_end_id = $this->input->post('internalpassages_torismplace_end');


            if (ensure($safa_internalsegmenttype_id))
                foreach ($safa_internalsegmenttype_id as $key => $value) {

                    $this->internalpassages_model->safa_internalsegment_id = $key;
                    $current_internalpassage_row_count = $this->internalpassages_model->get(true);


                    // Save internalpassages
                    $this->internalpassages_model->safa_trip_internaltrip_id = $id;
                    $this->internalpassages_model->safa_internalsegmenttype_id = $safa_internalsegmenttype_id[$key];
                    $this->internalpassages_model->start_datetime = $start_datetime[$key];
                    $this->internalpassages_model->end_datetime = $end_datetime[$key];
                    $this->internalpassages_model->seats_count = $seats_count[$key];
                    $this->internalpassages_model->notes = $notes[$key];

                    //$this->internalpassages_model->safa_internalsegmentestatus_id = 1;



                    if ($safa_internalsegmenttype_id[$key] == 1) {
                        $this->internalpassages_model->erp_port_id = $erp_port_start_id[$key];
                        $this->internalpassages_model->erp_end_hotel_id = $erp_end_hotel_id[$key];
                    } else if ($safa_internalsegmenttype_id[$key] == 2) {
                        $this->internalpassages_model->erp_start_hotel_id = $erp_start_hotel_id[$key];
                        $this->internalpassages_model->erp_port_id = $erp_port_end_id[$key];
                    } else if ($safa_internalsegmenttype_id[$key] == 3) {
                        $this->internalpassages_model->erp_start_hotel_id = $erp_start_hotel_id[$key];
                        $this->internalpassages_model->safa_tourism_place_id = $safa_tourism_place_end_id[$key];
                    } else if ($safa_internalsegmenttype_id[$key] == 4) {
                        $this->internalpassages_model->erp_start_hotel_id = $erp_start_hotel_id[$key];
                        $this->internalpassages_model->erp_end_hotel_id = $erp_end_hotel_id[$key];
                    }


                    if ($current_internalpassage_row_count == 0) {
                        $this->internalpassages_model->safa_internalsegment_id = false;
                        $this->internalpassages_model->save();
                    } else {
                        $this->internalpassages_model->save();
                    }


                    // By Gouda
                    $this->internalpassages_model->safa_trip_internaltrip_id = false;
                    $this->internalpassages_model->safa_internalsegmenttype_id = false;
                    $this->internalpassages_model->start_datetime = false;
                    $this->internalpassages_model->end_datetime = false;
                    $this->internalpassages_model->seats_count = false;
                    $this->internalpassages_model->notes = false;

                    $this->internalpassages_model->erp_port_id = false;
                    $this->internalpassages_model->erp_start_hotel_id = false;
                    $this->internalpassages_model->erp_end_hotel_id = false;
                    $this->internalpassages_model->safa_tourism_place_id = false;
                }
            // --- Delete Deleted rooms rows from Database --------------

            $internalpassages_remove = $this->input->post('passages_remove');
            if (ensure($internalpassages_remove)) {
                foreach ($internalpassages_remove as $internalpassage_remove) {
                    $this->internalpassages_model->safa_internalsegment_id = $internalpassage_remove;
                    $this->internalpassages_model->delete();
                }
            }
            //---------------------------------------------------------------
            //By Gouda.
            //----------- Send Notification For UO, EA Company ------------------------------
       		
        	if($this->input->post('company_type')!='') {
        	$msg_datetime = date('Y-m-d H:i', time());
    
       		$this->notification_model->notification_type = 'automatic';
        	$this->notification_model->erp_importance_id = 1;
        	$this->notification_model->sender_type_id = 1;
	        $this->notification_model->sender_id = 0;
	        
	        $this->notification_model->erp_system_events_id = 20;
	        
	        $this->notification_model->language_id = 2;
	        $this->notification_model->msg_datetime = $msg_datetime;
        
	        
	        $internaltripstatus_name='';
	        $safa_internaltripstatus_id=$this->input->post('safa_internaltripstatus_id');
	        $this->internaltrip_status_model->safa_internaltripstatus_id=$safa_internaltripstatus_id;
	        $internaltrip_status_row=$this->internaltrip_status_model->get();
	        if(count($internaltrip_status_row)>0) {
	        	$internaltripstatus_name=$internaltrip_status_row->{name()};
	        }
	        
	        /*
	        $trip_name='';
	        $safa_trip_id=$this->input->post('safa_trip_id');
	        $this->safa_trips_model->safa_trip_id=$safa_trip_id;
	        $trip_row=$this->safa_trips_model->get();
	        if(count($trip_row)>0) {
	        	$trip_name=$trip_row->{name()};
	        }
	        */
	        
	        //Link to replace.
        	if($this->input->post('company_type')==2) {
	        	$link_internaltrip ="<a href='".site_url('uo/trip_internaltrip/edit/'.$id)."'  target='_blank' > $id </a>";
	        } else {
	        	$link_internaltrip ="<a href='".site_url('ea/trip_internaltrip/edit/'.$id)."'  target='_blank' > $id </a>";
	        }
	        $this->notification_model->tags = "#*safa_trip_internaltrip_id#*=$link_internaltrip*****#*internaltripstatus#*=$internaltripstatus_name*****#*the_date#*=$msg_datetime*****#*trip#*=";
	        $this->notification_model->parent_id = '';
	        	        
	        $erp_notification_id=$this->notification_model->save();
	        
	        //-------- Notification Details ---------------
	        $this->notification_model->detail_erp_notification_id = $erp_notification_id;
         	$this->notification_model->detail_receiver_type = $this->input->post('company_type');
         	$this->notification_model->detail_receiver_id = $this->input->post('erp_company_id');
        	$this->notification_model->saveDetails();
         	
	        //--------------------------------
        	}
	        
	        //-------------------------------------------------------------------
       		
	        

            $this->load->view('redirect_message', array('msg' => lang('global_updated_message'),
                'url' => site_url('ito/trip_internaltrip'),
                'id' => $id,
                'model_title' => lang('node_title'), 'action' => lang('edit_trip_internaltrip')));
        }
    }

    public function edit_incoming($id = FALSE) {
        if (!$id)
            show_404();
        $this->trip_internaltrip_model->safa_trip_internaltrip_id = $id;
        $data['item'] = $this->trip_internaltrip_model->get();
        if (!$data['item'])
            show_404();

        $data["safa_intrernaltripstatus"] = ddgen("safa_internaltripstatus", array("safa_internaltripstatus_id", name()));
        $data["transporters"] = $this->create_transportes_array();

        /* --------- get the internal passage ----------------------------------------------------- */;
        $this->internalpassages_model->order_by = array("safa_internalsegments.start_datetime", 'ASC');
        $this->internalpassages_model->safa_trip_internaltrip_id = $id;
        $data["internalpassages"] = $this->internalpassages_model->get();
        /* ---------------------------------------------------------------------------------------------------------------------------------------- */
        $this->load->library("form_validation");
        $this->form_validation->set_rules('safa_internaltripstatus_id', 'lang:transport_request_status', 'trim|required');
        $this->form_validation->set_rules('safa_transporter_id', 'lang:transport_request_transportername', 'trim');
        $this->form_validation->set_rules('operator_reference', 'lang:transport_request_res_code', 'trim|required');

        if ($this->form_validation->run() == false) {
            $this->load->view("ito/trip_internaltrip/edit_incoming", $data);
        } else {
            if ($this->input->post('safa_transporter_id'))
                $this->trip_internaltrip_model->safa_transporter_id = $this->input->post('safa_transporter_id');
            else
                $this->trip_internaltrip_model->safa_transporter_id = NULL;

            $this->trip_internaltrip_model->safa_internaltripstatus_id = $this->input->post('safa_internaltripstatus_id');
            $this->trip_internaltrip_model->operator_reference = $this->input->post('operator_reference');
            $this->trip_internaltrip_model->ito_notes = $this->input->post('ito_notes');
            $this->trip_internaltrip_model->save();
            $this->load->view('redirect_message', array('msg' => lang('global_updated_message'),
                'url' => site_url('ito/trip_internaltrip/incoming'),
                'id' => $id,
                'model_title' => lang('node_title'), 'action' => lang('edit_trip_internaltrip')));
        }
    }

    function search() {
        if ($this->input->get("safa_trip_internaltrip_id"))
            $this->trip_internaltrip_model->safa_trip_internaltrip_id = $this->input->get("safa_trip_internaltrip_id");
        if ($this->input->get("safa_trip_id"))
            $this->trip_internaltrip_model->safa_trip_id = $this->input->get("safa_trip_id");
        if ($this->input->get("safa_ito_id"))
            $this->trip_internaltrip_model->safa_ito_id = $this->input->get("safa_ito_id");
        if ($this->input->get("safa_transporter_id"))
            $this->trip_internaltrip_model->safa_transporter_id = $this->input->get("safa_transporter_id");
        if ($this->input->get("safa_tripstatus_id"))
            $this->trip_internaltrip_model->safa_tripstatus_id = $this->input->get("safa_tripstatus_id");
        if ($this->input->get("safa_internaltripstatus_id"))
            $this->trip_internaltrip_model->safa_internaltripstatus_id = $this->input->get("safa_internaltripstatus_id");
        if ($this->input->get("operator_reference"))
            $this->trip_internaltrip_model->operator_reference = $this->input->get("operator_reference");
        if ($this->input->get("attachement"))
            $this->trip_internaltrip_model->attachement = $this->input->get("attachement");
        if ($this->input->get("datetime"))
            $this->trip_internaltrip_model->datetime = $this->input->get("datetime");
        if ($this->input->get('uo_contract_id'))
            $this->trip_internaltrip_model->by_contract = $this->input->get("uo_contract_id");
    }

    function create_contracts_array() {
        $result = $this->trip_internaltrip_model->get_ito_contracts();
        $contracts = array();
        $contracts[""] = lang('global_select_from_menu');
        $name = name();
        foreach ($result as $con_id => $contract) {
            $contracts[$contract->contract_id] = $contract->$name;
        }
        return $contracts;
    }

    /* get transporters update after phase 2 */

    function create_transportes_array() {
        $result = $this->trip_internaltrip_model->get_ito_transporters_withContracts();
        $transporters = array();
        $transporters[""] = lang('global_select_from_menu');
        $name = name();
        foreach ($result as $transporter_id => $transporter) {
            $transporters[$transporter->transporter_id] = $transporter->$name;
        }
        return $transporters;
    }

    function get_ea_trips($contract_id = FALSE) {
        $this->layout = 'ajax';
        $trips_ea = $this->trip_internaltrip_model->get_ea_trips($contract_id);
        $trips = array();
        echo "<option value=''>" . lang('global_select_from_menu') . "</option>";
        if (isset($trips_ea) && count($trips_ea) > 0) {
            foreach ($trips_ea as $trip) {
                echo "<option value='" . $trip->safa_trip_id . "'>" . $trip->name . "</option>";
            }
        }
    }

    function edit_ito_notes() {
        $this->layout = 'ajax';
        $internal_trip_notes = $this->input->post('internaltrip_notes');
        $internal_trip_id = $this->input->post('internal_trip_id');
        $this->db->set('ito_notes', $internal_trip_notes);
        $this->db->where('safa_trip_internaltrip_id', $internal_trip_id);
        $update_id = $this->db->update('safa_trip_internaltrips');
        echo $update_id;
    }

    public function getCompaniesByCompanyType() {
        $erp_company_type_id = $this->input->post('erp_company_type_id');

        $structure = array('id', name());
        $key = $structure['0'];
        if (isset($structure['1'])) {
            $value = $structure['1'];
        }
        $companies_arr = array();
        $companies = $this->trip_internaltrip_model->getCompaniesByCompanyType($erp_company_type_id);
        foreach ($companies as $company) {
            $companies_arr[$company->$key] = $company->$value;
        }


        $form_dropdown = form_dropdown('erp_company_id', $companies_arr, set_value('erp_company_id', ''), 'class="chosen-select chosen-rtl input-full"  tabindex="4" ');
        $form_dropdown = str_replace('<select id="erp_company_id" class="validate[required] chosen-select chosen-rtl input-huge" data-placeholder="الكل" tabindex="-1" name="erp_company_id" style="display: none;" >', '', $form_dropdown);
        $form_dropdown = str_replace('</select>', '', $form_dropdown);
        echo $form_dropdown;
        exit;
    }

    function delete($id = false) {
        if (!$id) {
            show_404();
        }
        $this->trip_internaltrip_model->safa_trip_internaltrip_id = $id;
        if (!$this->trip_internaltrip_model->delete())
            show_404();
        $this->load->view('redirect_message', array('msg' => lang('global_deleted_message'),
            'url' => site_url('ito/trip_internaltrip'),
            'model_title' => lang('node_title'), 'action' => lang('delete_title')));
    }

}

/* End of file trip_internaltrip.php */
/* Location: ./application/controllers/trip_internaltrip.php */