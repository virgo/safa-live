<?php

class Errors extends CI_Controller {
    public $layout = 'ajax';
    public function fourohfour() {
        $this->load->view('error', array(
            'title' => "404",
            'message' => "Sorry, The page you requested cannot be found"
            ));
    }
}