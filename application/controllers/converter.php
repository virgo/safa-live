<?php

class Converter extends Safa_Controller {

    public $layout = 'ajax';
    public $file = FALSE;
    public $group_name = FALSE;
    public $upload_path = './static/uploads/safa_files/';
    public $mutamers = array();
    public $current_mutamer = false;
    
    public function __construct() {
        parent::__construct();
        $this->load->library('Structure');
        $this->load->library('arraytoxml');
    }

    public function index() {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('hardware_id', '', 'trim|required');
        $this->form_validation->set_rules('version', '', 'trim|required');
        $this->form_validation->set_rules('username', '', 'trim|required');
        $this->form_validation->set_rules('password', '', 'trim|required');
        $this->form_validation->set_rules('destination', '', 'trim|required|less_than[4]');
        $this->form_validation->set_rules("file", '', "trim|callback_file");
        if($this->input->post('destination') == 2) {
            $this->form_validation->set_rules('ea_code', '', 'trim|required');
            $this->form_validation->set_rules('uo_code', '', 'trim|required');
        }
        if ($this->form_validation->run() == FALSE) {
            $this->load->view('converter/index');
        } else {
            $passports = $this->import();
            $group_id = $this->databaseImport($passports);
            $this->export($passports, $this->input->post('destination'));
        }
    }

    private function import() {
        $zip = new ZipArchive;
        $res = $zip->open($this->upload_path . $this->file);
        if ($res !== TRUE)
            show_404();
        $zip->extractTo($this->upload_path . $this->file . '_files');
        $zip->close();
        $file = $this->upload_path . $this->file . '_files/group.txt';
        $fp = @fopen($file, 'r');
        if ($fp)
            $lines = explode("\n", fread($fp, filesize($file)));
        $titles = $lines[0];
        unset($lines[0]);
        foreach ($lines as $line) {
            $arr = explode(",", $line);
            if (!isset($arr[1]))
                continue;
            $fields = array_combine(explode(",", $titles), $arr);
            // WAY TO UMRAH FORMAT
            $passports[] = $fields;
        }
        $this->mutamers = $passports;
        return $passports;
    }

    private function databaseImport($passports) {
        $passports = $this->structure->convert($passports, 2, 4);
        $num = $this->db->select("MAX(name) as num")->where('safa_ea_id', 1)->get('safa_groups')->row()->num++;
        $this->group_name = $num;
        $this->db->insert('safa_groups', array(
            'name' => $num,
            'creation_date' => date('Y-m-d'),
            'safa_ea_id' => 1,
            'last_update' => date('Y-m-d'),
            'import_data' => json_encode(array(
                'hardware_id' => $this->input->post('hardware_id'),
                'version' => $this->input->post('version'),
                'username' => $this->input->post('username'),
                'password' => $this->input->post('password'),
                'destination' => $this->input->post('destination'),
            ))
        ));
        $group_id = $this->db->insert_id();
        
        foreach ($passports as $pass) {
            $pass['safa_group_id'] = $group_id;
            foreach ($pass as $key => $val)
            {
                $d[$key] = iconv ("windows-1256" , "UTF-8" , $val);
            }
            $this->db->insert('safa_group_passports', $d);
        }
        return $group_id;
    }

    private function export($passports, $format) {
        // FROM DATABASE FORMAT TO ANY
        $pass = $this->structure->convert($passports, 2, $format);
//        print_r($pass);
        if($format == 1 or $format == 3)
            $this->xmlExport($pass);
        else
            $this->textExport($pass, $format);
    }
    
    private function textExport($pass, $format) {
        header('Content-type: text/txt');
        header('Content-Disposition: attachment; filename="' . $this->file . '.txt"');
        if($format == 2)
            echo $this->array_to_text_wtu($pass);
        else
            echo $this->array_to_text($pass);
    }
    
    private function xmlExport($pass) {
        header('Content-type: text/xml');
        header('Content-Disposition: attachment; filename="' . $this->file . '.xml"');
        echo $this->array_to_xml($pass);
    }
    
    private function array_to_text_wtu($mutamers) {
        $attr = 'UO_CODE,EA_CODE,PilgrimID,Title,FirstName,AFirstName,FatherName,AFatherName,LastName,ALastName,MotherName,AMotherName,MiddleName,AMiddleName,Sex,BirthDate,BirthCountry,BirthCity,MaritalStatus,Religion,Education,job,GroupCode,PackageCode,SponserID,RelationWithSponser,No_Of_Relatives,Dpn_Serial_No,Emb_City,CNationality,PrevNationality,PPNo,PPType,PPIssueCity,PPIssueDate,PPExpDate,PPIssueCountry,CAddStreet,CAddAStreet,CAddCity,CAddaCity,CAddState,CAddAstate,CAddCountry,CAddZip,CAddMobile,CAddTel,CAddFax,FemaleId';
        $txt = "B1O1R1," . $attr . ",E1O1R1\n";
        
        foreach ($mutamers as $mtamer) {
            $mtamer['UO_CODE'] = $this->input->post('uo_code');
            $mtamer['EA_CODE'] = $this->input->post('ea_code');
            $mtamer['Religion'] = 1;
//            $mtamer['GroupCode'] = '1';
            $mtamer['PackageCode'] = 1;
            $mtamer['Emb_City'] = '0';
//            $mtamer['GroupCode'] = 'OFFLN';
            if(isset($mtamer['Job']))
                $mtamer['job'] = $mtamer['Job'];
            
            $txt .= "B1O1R1,";
            foreach (explode(',', $attr) as $key) 
                if(isset( $mtamer[$key] ))
                    $txt .= "$mtamer[$key],";
                else
                    $txt .= ",";
            $txt .= "E1O1R1\n";
        }
        $txt .= "E1O1F1,UO_CODE,EA_CODE,PilgrimID,Title,FirstName,AFirstName,FatherName,AFatherName,LastName,ALastName,MotherName,AMotherName,MiddleName,AMiddleName,Sex,BirthDate,BirthCountry,BirthCity,MaritalStatus,Religion,Education,job,GroupCode,PackageCode,SponserID,RelationWithSponser,No_Of_Relatives,Dpn_Serial_No,Emb_City,CNationality,PrevNationality,PPNo,PPType,PPIssueCity,PPIssueDate,PPExpDate,PPIssueCountry,CAddStreet,CAddAStreet,CAddCity,CAddaCity,CAddState,CAddAstate,CAddCountry,CAddZip,CAddMobile,CAddTel,CAddFax,FemaleId,E1O1R1";
        return $txt;
    }
    
    private function array_to_text($mutamers) {
        $txt = '';
        foreach ($mtamers as $mtamer) {
            foreach ($mtamer as $key => $value) 
                $txt .= "$key,";
            $txt .= "UO_CODE,EA_CODE\n";
            break;
        }
        
        foreach ($mtamers as $mtamer) {
            if($this->input->post('destination') == 2) {
                $txt .= "<UO_CODE>" . $this->input->post('uo_code') . "</UO_CODE>";
                $txt .= "<EA_CODE>" . $this->input->post('ea_code') . "</EA_CODE>";
            }
            foreach ($mtamer as $key => $value) 
                if($key)
                     $txt .= "$value,";
            $txt .= "\n";
        }
        return $txt;
    }
    
    private function array_to_xml($mtamers, $level = 1) {
        $xml = '';
        if ($level == 1) {
            if($this->input->post('destination') == 3)
            $xml .= '<?xml version="1.0" standalone="yes"?>' .
                    "\n<DataSetMutamers>\n";
            if($this->input->post('destination') == 1)
            $xml .= "<?xml version=\"1.0\" standalone=\"yes\"?>\n<DataSetMutamers xmlns=\"http://www.babalumra.com/DataSetMutamers.xsd\">";
        }
        $i = 0;
        if($this->input->post('destination') == 1) {
            $babelumrah = "SN,FN,LN,MN,TC,G,MS,N,CAC,CACO,PN,PT,CIA,DI,DE,COB,CB,DB,FAN,MNR,DSN,EL,II,MIG,IM,CIAT,CACN,CIAN,CBN,J,AFN,ALN,AFAN,AMN,TL";
            foreach ($mtamers as $k => $mtamer) {
                $this->current_mutamer = $mtamer;
                $xml .= "<MUTAMERS>";
                if($this->input->post('destination') == 1) {
                    if(isset($this->mutamers[$i]['Job']))
                    $mtamer['J'] =  $this->mutamers[$i]['Job'];
                    $mtamer['II'] = ++$i;
                }
                
                foreach(explode(',', $babelumrah) as $key)
                    if(isset($mtamer[$key]))
                        if ($key == "DI" or $key == "DE" or $key == "DB")
                            $xml .= "<$key>". date('d/m/Y', strtotime($mtamer[$key])) ."</$key>";
                        elseif($key == "MD_PASSPORT_ISSUE_DATE" or $key == "MD_PASSPORT_EXPIRY_DATE" or $key == "MD_DATE_OF_BIRTH")
                            $xml .= "<$key>". date('Y/m/d', strtotime($mtamer[$key])) ."</$key>";
                        else
                            $xml .= "<$key>".iconv ('windows-1256', 'utf-8', $mtamer[$key]) ."</$key>";
                    else
                        $xml .= $this->babelumrah_exceptions($key);
                         
                $xml .= "</MUTAMERS>";
            }
        }
        else
        {
            foreach ($mtamers as $k => $mtamer) {
                $xml .= "<MUTAMERS>";
                if($this->input->post('destination') == 1) {
                    if(isset($this->mutamers[$i]['Job']))
                    $mtamer['J'] =  $this->mutamers[$i]['Job'];
                    $mtamer['II'] = $i++;
                }
                foreach ($mtamer as $key => $value) 
                    if($key)
                        if ($key == "DI" or $key == "DE" or $key == "DB")
                            $xml .= "<$key>". date('d/m/Y', strtotime($value)) ."</$key>";
                        elseif($key == "MD_PASSPORT_ISSUE_DATE" or $key == "MD_PASSPORT_EXPIRY_DATE" or $key == "MD_DATE_OF_BIRTH")
                            $xml .= "<$key>". date('Y/m/d', strtotime($value)) ."</$key>";
                        else
                            $xml .= "<$key>".iconv ('windows-1256', 'utf-8', $value) ."</$key>";
                $xml .= "</MUTAMERS>";
            }
        }
        if ($level == 1) {
            $xml .= "</DataSetMutamers>";
        }
        return $xml;
    }
    private function babelumrah_exceptions($key) {
        if($key == 'MIG')
            return "<$key>778</$key>";
        elseif($key == 'IM')
            return "<$key>false</$key>";
        elseif($key == 'CIAN')
            return "<$key>".iconv ('windows-1256', 'utf-8', $this->current_mutamer['CIAT']) ."</$key>";
        return "<$key />";
    }
    // CALLBACK
    public function file($var) {
        $config['upload_path'] = $this->upload_path;
        $config['allowed_types'] = '*';
        $config['detect_mime'] = FALSE;
        $this->load->library('upload', $config);
        if (!$this->upload->do_upload('file')) {
            $this->form_validation->set_message('file', $this->upload->display_errors());
            return FALSE;
        } else {
            $data = $this->upload->data();
            if ($data['file_name'])
                $this->file = $data['file_name'];
        }
    }

}
