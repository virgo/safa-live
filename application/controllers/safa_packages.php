<?php

class Safa_packages extends Safa_Controller {

    function __construct() {
        parent::__construct();

        //Side menu session, By Gouda.
        if (session('uo_id')) {
            session('side_menu_id', 7);
        } else if (session('ea_id')) {
            session('side_menu_id', 6);
        }

        $this->layout = 'new';

        $this->load->library('hijrigregorianconvert');

        $this->lang->load('packages');
        $this->load->model('safa_packages_model');
        $this->load->helper('packages');

        $this->load->model('package_tourismplaces_model');
        $this->load->model('package_hotels_model');
        $this->load->model('safa_package_execlusive_meals_model');
        $this->load->model('safa_package_execlusive_nights_model');
        $this->load->model('safa_package_execlusive_nights_prices_model');
        $this->load->model('safa_package_periods_model');
        $this->load->model('safa_package_periods_prices_model');
        $this->load->model('package_periods_cities_model');
        

        $this->load->model('package_periods_model');
        $this->load->model('hotels_model');
        $this->load->model('tourismplaces_model');
        $this->load->model('hotelroomsizes_model');


        permission();

        if (session('uo_id')) {
            $this->safa_packages_model->erp_company_type_id = 2;
            $this->safa_packages_model->erp_company_id = session('uo_id');
        } if (session('ea_id')) {
            $this->safa_packages_model->erp_company_type_id = 3;
            $this->safa_packages_model->erp_company_id = session('ea_id');
        }
    }

    function index() {
        $data = array();

        $this->safa_packages_model->order_by=array('safa_package_id','desc');
        $data['items'] = $this->safa_packages_model->get();

        $this->load->view('safa_packages/index', $data);
    }

    function view($id = FALSE) {
        if (!$id)
            show_404();
        $data['erp_cities'] = ddgen('erp_cities', array('erp_city_id', name()), FALSE, FALSE, TRUE);
        $data['erp_hotels'] = ddgen('erp_hotels', array('erp_hotel_id', name()), FALSE, FALSE, TRUE);
        $data['erp_currencies'] = ddgen('erp_currencies', array('erp_currency_id', name()), FALSE, FALSE, TRUE);
        $data['erp_meals'] = ddgen('erp_meals', array('erp_meal_id', name()), FALSE, FALSE, TRUE);
        $data['erp_transportertypes'] = ddgen('safa_packages_transportation_types', array('safa_packages_transportation_type_id', name()), FALSE, FALSE, TRUE);
        $data['safa_tourismplaces'] = ddgen('safa_tourismplaces', array('safa_tourismplace_id', name()), FALSE, FALSE, TRUE);


        $erp_company_type_id = 0;
        $erp_company_id = 0;
        if (session('uo_id')) {
            $erp_company_type_id = 2;
            $erp_company_id = session('uo_id');
        } if (session('ea_id')) {
            $erp_company_type_id = 3;
            $erp_company_id = session('ea_id');
        }
        $data['package_periods'] = ddgen('erp_package_periods', array('erp_package_period_id', name()), array('erp_company_type_id' => $erp_company_type_id, 'erp_company_id' => $erp_company_id), FALSE, TRUE);

        //$data['erp_hotelroomsizes'] = ddgen('erp_hotelroomsizes', array('erp_hotelroomsize_id', name()), FALSE, FALSE, TRUE);
        $data['erp_hotelroomsizes'] = $this->hotelroomsizes_model->get();




        $this->safa_packages_model->safa_package_id = $id;
        $data['item'] = $this->safa_packages_model->get();

        if (!isset($data['item']))
            show_404();

        //---------- Details --------------------------
        $this->safa_package_execlusive_meals_model->safa_package_id = $id;
        $item_execlusive_meals_prices = $this->safa_package_execlusive_meals_model->get();
        $data['item_execlusive_meals_prices'] = $item_execlusive_meals_prices;

        $this->safa_package_execlusive_nights_model->safa_package_id = $id;
        $item_execlusive_nights_prices = $this->safa_package_execlusive_nights_model->get();
        $data['item_execlusive_nights_prices'] = $item_execlusive_nights_prices;

        $this->safa_package_periods_model->safa_package_id = $id;
        $item_hotels_prices = $this->safa_package_periods_model->get();
        $data['item_hotels_prices'] = $item_hotels_prices;

        $this->package_hotels_model->safa_package_id = $id;
        $item_hotels = $this->package_hotels_model->get();
        $data['item_hotels'] = $item_hotels;

        $this->package_tourismplaces_model->safa_package_id = $id;
        $item_tourism_places = $this->package_tourismplaces_model->get();
        $data['item_tourism_places'] = $item_tourism_places;
        //-----------------------------------------------



        $this->load->view('safa_packages/view', $data);
    }

    function manage($id = FALSE) {
        $data['erp_cities'] = ddgen('erp_cities', array('erp_city_id', name()), array('erp_country_id'=>966), FALSE, TRUE);
        $data['erp_hotels'] = ddgen('erp_hotels', array('erp_hotel_id', name()), FALSE, FALSE, TRUE);
        $data['erp_currencies'] = ddgen('erp_currencies', array('erp_currency_id', name()), FALSE, FALSE, TRUE);
        $data['erp_meals'] = ddgen('erp_meals', array('erp_meal_id', name()), FALSE, FALSE, TRUE);
        $data['erp_transportertypes'] = ddgen('safa_packages_transportation_types', array('safa_packages_transportation_type_id', name()), FALSE, FALSE, TRUE);
        $data['safa_tourismplaces'] = ddgen('safa_tourismplaces', array('safa_tourismplace_id', name()), FALSE, FALSE, TRUE);

        $erp_company_type_id = 0;
        $erp_company_id = 0;
        if (session('uo_id')) {
            $erp_company_type_id = 2;
            $erp_company_id = session('uo_id');
        } if (session('ea_id')) {
            $erp_company_type_id = 3;
            $erp_company_id = session('ea_id');
        }
        $data['package_periods'] = ddgen('erp_package_periods', array('erp_package_period_id', name()), array('erp_company_type_id' => $erp_company_type_id, 'erp_company_id' => $erp_company_id), FALSE, TRUE);

        //$data['erp_hotelroomsizes'] = ddgen('erp_hotelroomsizes', array('erp_hotelroomsize_id', name()), FALSE, FALSE, TRUE);
        $data['erp_hotelroomsizes'] = $this->hotelroomsizes_model->get();


        if ($id) {

            $this->safa_packages_model->safa_package_id = $id;
            $data['item'] = $this->safa_packages_model->get();

            //---------- Details --------------------------
            $this->safa_package_execlusive_meals_model->safa_package_id = $id;
            $item_execlusive_meals_prices = $this->safa_package_execlusive_meals_model->get();
            $data['item_execlusive_meals_prices'] = $item_execlusive_meals_prices;
 
            $this->safa_package_execlusive_nights_model->safa_package_id = $id;
            $item_execlusive_nights_prices = $this->safa_package_execlusive_nights_model->get();
            $data['item_execlusive_nights_prices'] = $item_execlusive_nights_prices;

            $this->safa_package_periods_model->safa_package_id = $id;
            $item_hotels_prices = $this->safa_package_periods_model->get();
            $data['item_hotels_prices'] = $item_hotels_prices;

            $this->package_hotels_model->safa_package_id = $id;
            $item_hotels = $this->package_hotels_model->get();
            $data['item_hotels'] = $item_hotels;

            $this->package_tourismplaces_model->safa_package_id = $id;
            $item_tourism_places = $this->package_tourismplaces_model->get();
            $data['item_tourism_places'] = $item_tourism_places;
            //-----------------------------------------------
        }

        $this->load->library("form_validation");
        $this->form_validation->set_rules('name_ar', 'lang:name_ar', 'trim|required');
        $this->form_validation->set_rules('name_la', 'lang:name_la', 'trim|required');
        $this->form_validation->set_rules('package_code', 'lang:package_code', 'trim|required');

        if ($this->form_validation->run() == false) {
            $this->load->view('safa_packages/manage', $data);
        } else {
            //$this->safa_packages_model->erp_company_type_id = 2;
            //$this->safa_packages_model->erp_company_id = session('uo_id');

            $this->safa_packages_model->package_code = $this->input->post('package_code');
            $this->safa_packages_model->date_type = $this->input->post('date_type');


            //---------For Hijri Date ----------------------------------------------------
            if ($this->input->post('date_type') == 'islamic') {
                $this->safa_packages_model->start_date = $this->hijrigregorianconvert->HijriToGregorian($this->input->post('start_date'), 'YYYY-MM-DD');
                $this->safa_packages_model->end_date = $this->hijrigregorianconvert->HijriToGregorian($this->input->post('end_date'), 'YYYY-MM-DD');
            } else {
                $this->safa_packages_model->start_date = $this->input->post('start_date');
                $this->safa_packages_model->end_date = $this->input->post('end_date');
            }
            //-----------------------------------------------------------------------------

            $this->safa_packages_model->name_ar = $this->input->post('name_ar');
            $this->safa_packages_model->name_la = $this->input->post('name_la');
            
            $this->safa_packages_model->local_currency_id = $this->input->post('local_currency_id');
            $this->safa_packages_model->service_cost = $this->input->post('service_cost');
            $this->safa_packages_model->round_num = $this->input->post('round_num');
            $this->safa_packages_model->convert = $this->input->post('convert');
            
            $this->safa_packages_model->sale_currency_id = $this->input->post('sale_currency_id');
            $this->safa_packages_model->safa_packages_transportation_type_id = $this->input->post('safa_packages_transportation_type_id');
            $this->safa_packages_model->remarks = $this->input->post('remarks');
            if (isset($_POST['active'])) {
                $this->safa_packages_model->active = 1;
            } else {
                $this->safa_packages_model->active = 0;
            }

            if ($id) {
                $this->safa_packages_model->safa_package_id = $id;
                $safa_package_id = $id;
                $this->safa_packages_model->save();
            } else {
                $safa_package_id = $this->safa_packages_model->save();
            }




            //-------------------- Package Hotels -----------------------------------
            $this->package_hotels_model->safa_package_id = $safa_package_id;

            $hotels_erp_hotel_id_arr = $this->input->post('hotels_erp_hotel_id');
            $hotels_erp_meal_id_arr = $this->input->post('hotels_erp_meal_id');
            $hotels_day_price = $this->input->post('hotels_day_price');

            if (ensure($hotels_erp_hotel_id_arr))
                foreach ($hotels_erp_hotel_id_arr as $key => $value) {

                    $this->package_hotels_model->erp_meal_id = false;
                    $this->package_hotels_model->erp_hotel_id = false;
                    if (strpos($key, 'N') === false)
                        $this->package_hotels_model->safa_package_hotel_id = $key;
                    else
                        $this->package_hotels_model->safa_package_hotel_id = false;

                    $this->package_hotels_model->safa_package_id = $safa_package_id;
                    $this->package_hotels_model->erp_hotel_id = $hotels_erp_hotel_id_arr[$key];
                    $this->package_hotels_model->erp_meal_id = $hotels_erp_meal_id_arr[$key];
                    $this->package_hotels_model->save();

                    if (strpos($key, 'N') === false)
                        $package_hote_id = $key;
                    else
                        $package_hote_id = $this->db->insert_id();

                    if (ensure($hotels_day_price[$key])) {
                        foreach ($hotels_day_price[$key] as $roomsize => $roomprice) {
                            $this->db->set('safa_package_hotel_id', $package_hote_id);
                            $this->db->set('erp_hotelroomsize_id', $roomsize);
                            $this->db->set('erp_meal_id', $hotels_erp_meal_id_arr[$key]);
                            $this->db->set('price', $roomprice);
                            if (strpos($key, 'N') === false) {
                                $this->db->where('safa_package_hotel_id', $package_hote_id);
                                $this->db->where('erp_hotelroomsize_id', $roomsize);
                                if ($this->db->from('safa_package_hotels_prices')->count_all_results()) {
                                    $this->db->where('safa_package_hotel_id', $package_hote_id);
                                    $this->db->where('erp_hotelroomsize_id', $roomsize);
                                    $this->db->set('erp_meal_id', $hotels_erp_meal_id_arr[$key]);
                                    $this->db->set('price', $roomprice);
                                    $this->db->update('safa_package_hotels_prices');
                                } else {
                                    $this->db->set('safa_package_hotel_id', $package_hote_id);
                                    $this->db->set('erp_hotelroomsize_id', $roomsize);
                                    $this->db->set('erp_meal_id', $hotels_erp_meal_id_arr[$key]);
                                    $this->db->set('price', $roomprice);
                                    $this->db->insert('safa_package_hotels_prices');
                                }
                            } else {
                                $this->db->set('safa_package_hotel_id', $package_hote_id);
                                $this->db->set('erp_hotelroomsize_id', $roomsize);
                                $this->db->set('erp_meal_id', $hotels_erp_meal_id_arr[$key]);
                                $this->db->set('price', $roomprice);
                                $this->db->insert('safa_package_hotels_prices');
                            }
                        }
                    }
                }
            // --- Delete Deleted rows from Database --------------
            $this->package_hotels_model->erp_meal_id = false;
            $this->package_hotels_model->erp_hotel_id = false;

            $hotels_remove = $this->input->post('hotels_remove');
            if (ensure($hotels_remove)) {
                foreach ($hotels_remove as $hotel_remove) {
                    $this->package_hotels_model->safa_package_hotel_id = $hotel_remove;
                    $this->package_hotels_model->delete();
                }
            }
            //---------------------------------------------------------------
            //-------------------- Package Hotels Prices -----------------------------------
            $this->safa_package_periods_prices_model->safa_package_id = $safa_package_id;

            $hotels_prices_safa_package_id = $this->input->post('hotels_prices_safa_package_id');
            $hotels_prices_price = $this->input->post('hotels_prices_price');


            if (ensure($hotels_prices_safa_package_id))
                foreach ($hotels_prices_safa_package_id as $key => $value) {
                    if (strpos($key, 'N') === false)
                        $this->safa_package_periods_model->safa_package_periods_id = $key;
                    else
                        $this->safa_package_periods_model->safa_package_periods_id = FALSE;

                    $this->safa_package_periods_model->safa_package_id = $safa_package_id;
                    $this->safa_package_periods_model->erp_package_period_id = $value;

                    if (strpos($key, 'N') === false) {
                        $this->safa_package_periods_model->save();
                        $periodid = $key;
                    } else
                        $periodid = $this->safa_package_periods_model->save();

                    $this->db->where('safa_package_periods_id',$periodid)->delete('safa_package_periods_prices');
                    foreach ($hotels_prices_price[$key] as $roomsize => $price) {
                        if ($price > 0) {
                            $this->safa_package_periods_prices_model->safa_package_periods_id = $periodid;
                            $this->safa_package_periods_prices_model->erp_hotelroomsize_id = $roomsize;
                            $this->safa_package_periods_prices_model->price = $price;
                            $this->safa_package_periods_prices_model->save();
                        }
                    }
                }
            // --- Delete Deleted rows from Database --------------
            $this->safa_package_periods_prices_model->erp_meal_id = false;
            $this->safa_package_periods_prices_model->erp_hotel_id = false;

            $hotels_prices_remove = $this->input->post('hotels_prices_remove');
            if (ensure($hotels_prices_remove)) {
                foreach ($hotels_prices_remove as $hotel_price_remove) {
                    $this->safa_package_periods_prices_model->safa_package_id = $hotel_price_remove;
                    $this->safa_package_periods_prices_model->delete();
                }
            }
            //---------------------------------------------------------------
            //-------------------- Package execlusive nights -----------------------------------
            $this->safa_package_execlusive_nights_model->safa_package_id = $safa_package_id;

            $hotels_nights_erp_hotel_id = $this->input->post('execlusive_nights_prices_erp_hotel_id');
            $hotels_nights_erp_meal_id = $this->input->post('execlusive_nights_prices_erp_meal_id');
            $hotels_nights_price = $this->input->post('execlusive_nights_prices_price');

            if (ensure($hotels_nights_erp_hotel_id))
                foreach ($hotels_nights_erp_hotel_id as $key => $value) {
                    if (strpos($key, 'N') === false)
                        $this->safa_package_execlusive_nights_model->safa_package_execlusive_night_id = $key;
                    else
                        $this->safa_package_execlusive_nights_model->safa_package_execlusive_night_id = FALSE;

                    $this->safa_package_execlusive_nights_model->safa_package_id = $safa_package_id;
                    $this->safa_package_execlusive_nights_model->hotel_id = $value;
                    $this->safa_package_execlusive_nights_model->erp_meal_id = $hotels_nights_erp_meal_id[$key];

                    if (strpos($key, 'N') === false) {
                        $this->safa_package_execlusive_nights_model->save();
                        $nightid = $key;
                    } else
                        $nightid = $this->safa_package_execlusive_nights_model->save();

                    $this->safa_package_execlusive_nights_prices_model->safa_package_execlusive_night_id = $nightid;
                    $this->safa_package_execlusive_nights_prices_model->delete();
                    foreach ($hotels_nights_price[$key] as $roomsize => $price) {
                        if ($price > 0) {
                            $this->safa_package_execlusive_nights_prices_model->safa_package_execlusive_night_id = $nightid;
                            $this->safa_package_execlusive_nights_prices_model->erp_hotelroomsize_id = $roomsize;
                            $this->safa_package_execlusive_nights_prices_model->price = $price;
                            $this->safa_package_execlusive_nights_prices_model->save();
                        }
                    }
                }
            // --- Delete Deleted rows from Database --------------
            $this->safa_package_execlusive_nights_model->erp_meal_id = false;
            $this->safa_package_execlusive_nights_model->erp_hotel_id = false;

            $hotels_nights_remove = $this->input->post('execlusive_nights_prices_remove');
            if (ensure($hotels_nights_remove)) {
                foreach ($hotels_nights_remove as $hotel_night_remove) {
                    $this->safa_package_execlusive_nights_model->safa_package_id = $hotel_night_remove;
                    $this->safa_package_execlusive_nights_model->delete();
                }
            }
            //---------------------------------------------------------------
            //-------------------- Package Tourismplaces -----------------------------------
            $this->package_tourismplaces_model->safa_package_id = $safa_package_id;

            $tourismplaces_safa_tourismplace_id_arr = $this->input->post('tourism_places_safa_tourismplace_id');

            if (ensure($tourismplaces_safa_tourismplace_id_arr))
                foreach ($tourismplaces_safa_tourismplace_id_arr as $key => $value) {

                    if (strpos($key, 'N') === false)
                        $this->package_tourismplaces_model->safa_package_tourismplace_id = $key;
                    else
                        $this->package_tourismplaces_model->safa_package_tourismplace_id = FALSE;

                    $this->package_tourismplaces_model->safa_tourismplace_id = $value;
                    $this->package_tourismplaces_model->safa_package_id = $safa_package_id;

                    if (strpos($key, 'N') === false) {
                        $this->package_tourismplaces_model->save();
                        $tourismplace = $key;
                    } else {
                        $tourismplace = $this->package_tourismplaces_model->save();
                    }
                }
            // --- Delete Deleted rows from Database --------------
            $this->package_tourismplaces_model->safa_tourismplace_id = false;

            $tourismplaces_remove = $this->input->post('tourism_places_remove');
            if (ensure($tourismplaces_remove)) {
                foreach ($tourismplaces_remove as $tourismplace_remove) {
                    $this->package_tourismplaces_model->safa_package_tourismplace_id = $tourismplace_remove;
                    $this->package_tourismplaces_model->delete();
                }
            }
            //---------------------------------------------------------------
            //-------------------- Package Execlusive Meals Prices -----------------------------------
            $this->safa_package_execlusive_meals_model->safa_package_id = $safa_package_id;

            $execlusive_meals_prices_erp_hotel_id_arr = $this->input->post('execlusive_meals_prices_erp_hotel_id');
            $execlusive_meals_prices_erp_meal_id_arr = $this->input->post('execlusive_meals_prices_erp_meal_id');
            $execlusive_meals_prices_price_arr = $this->input->post('execlusive_meals_prices_price');

            if (ensure($execlusive_meals_prices_erp_hotel_id_arr))
                foreach ($execlusive_meals_prices_erp_hotel_id_arr as $key => $value) {

                    $this->safa_package_execlusive_meals_model->erp_meal_id = false;
                    $this->safa_package_execlusive_meals_model->hotel_id = false;
                    $this->safa_package_execlusive_meals_model->price = false;
                    $this->safa_package_execlusive_meals_model->safa_package_execlusive_meal_id = $key;
                    $current_hotel_row_count = $this->safa_package_execlusive_meals_model->get(true);

                    $this->safa_package_execlusive_meals_model->safa_package_id = $safa_package_id;
                    $this->safa_package_execlusive_meals_model->hotel_id = $execlusive_meals_prices_erp_hotel_id_arr[$key];
                    $this->safa_package_execlusive_meals_model->erp_meal_id = $execlusive_meals_prices_erp_meal_id_arr[$key];
                    $this->safa_package_execlusive_meals_model->price = $execlusive_meals_prices_price_arr[$key];

                    if ($current_hotel_row_count == 0) {
                        $this->safa_package_execlusive_meals_model->safa_package_execlusive_meal_id = false;
                        $this->safa_package_execlusive_meals_model->save();
                    } else {
                        $this->safa_package_execlusive_meals_model->save();
                    }
                }
            // --- Delete Deleted rows from Database --------------
            $this->safa_package_execlusive_meals_model->erp_meal_id = false;
            $this->safa_package_execlusive_meals_model->hotel_id = false;
            $this->safa_package_execlusive_meals_model->price = false;

            $execlusive_meals_prices_remove = $this->input->post('execlusive_meals_prices_remove');
            if (ensure($execlusive_meals_prices_remove)) {
                foreach ($execlusive_meals_prices_remove as $hotel_remove) {
                    $this->safa_package_execlusive_meals_model->safa_package_execlusive_meal_id = $hotel_remove;
                    $this->safa_package_execlusive_meals_model->delete();
                }
            }
            //---------------------------------------------------------------

            if ($id) {
                $this->load->view('redirect_new_message', array('msg' => lang('global_updated_message'),
                    'url' => site_url('safa_packages'),
                    'id' => $this->uri->segment("4"),
                    'model_title' => lang('title'), 'action' => lang('edit_title')));
            } else {
                $this->load->view('redirect_new_message', array('msg' => lang('global_added_message'),
                    'url' => site_url('safa_packages'),
                    'id' => $this->uri->segment("4"),
                    'model_title' => lang('title'), 'action' => lang('add_title')));
            }
        }
    }

    function get_hotels_by_city($city_id = false) {
        $this->layout = 'ajax';
        $drpdwn = "<option value=''></option>";
        $rows = $this->hotels_model->get_by_city_id($city_id);
        foreach ($rows as $record) {
            $drpdwn = $drpdwn . "<option value='" . $record->erp_hotel_id . "' >" . $record->{name()} . "</option>";
        }
        echo $drpdwn;
    }

    function get_torism_places_by_city_id($city_id = false) {
        $this->layout = 'ajax';
        $drpdwn = "<option value=''></option>";
        $rows = $this->tourismplaces_model->get_by_city($city_id);
        foreach ($rows as $record) {
            $drpdwn = $drpdwn . "<option value='" . $record->safa_tourismplace_id . "' >" . $record->{name()} . "</option>";
        }
        echo $drpdwn;
    }

	public function get_days_by_package_period_ajax()
	{
		$erp_package_period_id = $this->input->post('erp_package_period_id');
		
		$this->package_periods_cities_model->erp_package_period_id =$erp_package_period_id;
		$package_periods_cities_rows = $this->package_periods_cities_model->get();
		
		$data_arr = array();
		foreach($package_periods_cities_rows as $package_periods_cities_row) {
			$data_arr[$package_periods_cities_row->erp_city_id]=$package_periods_cities_row->city_days ;
		}
		

		echo json_encode($data_arr);
		
		exit;
	}
	
	
}
