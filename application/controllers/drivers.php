<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Drivers extends Safa_Controller {

    public $module = "drivers";

    public function __construct() {
        parent::__construct();
        $this->load->model('trips_reports_model');
        $this->load->model('internalpassages_model');
        $this->lang->load('trips_reports');
        $this->load->helper('trip');
        $this->load->helper('array');
        permission();
        if ($this->destination == 'uo')
            $this->uo_id = session('uo_id');
    }

    function index() {
        
    }

    function getDrivers($id) {
        if (!$id)
            show_404();

        $this->layout = 'js';

        $this->internalpassages_model->safa_internalsegment_id = $id;
        $items = $this->internalpassages_model->get();
        if (!$items)
            show_404();
        $data['drivers'] = json_decode($items->ito_driver_info);
        $data['vehicle'] = json_decode($items->ito_vehicle_info);
        if ($data) {
            $this->load->view("uo/trips_reports/drivers", $data);
        } else {
            echo 'لا يوجد سائقين هنا';
        }
    }

}
