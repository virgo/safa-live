<?php

class Style extends Safa_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index($style = FALSE) {
        set_cookie('style', $style,  time()+ 3600*24*7);
        redirect();
    }
    
    public function remove($style = FALSE) {
        set_cookie('style','');
                redirect();
    }
}
