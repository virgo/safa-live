<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Hotels_availabilty_offers extends Safa_Controller {

    public $module = "hotels_availabilty_offers";

    public function __construct() {
        parent::__construct();
        $this->layout = 'new';
        if (session('uo_id')) {
            //Side menu session, By Gouda.
            session('side_menu_id', 5);
        } else if (session('ea_id')) {
            session('side_menu_id', 3);
        }
        $this->load->model('erp_hotels_availabilty_offers_model');
        $this->load->model('notification_model');
        $this->load->model('contracts_model');
        $this->lang->load('hotels_availabilty_offers');
        $this->load->helper('form_helper');
        $this->load->helper('db_helper');
        $this->load->helper('itconf_helper');

        permission();
    }

    public function index() {
        if (session('uo_id')) {
            $this->erp_hotels_availabilty_offers_model->availabilty_offer_owner_type = 2;
            $this->erp_hotels_availabilty_offers_model->availabilty_offer_owner_id = session('uo_id');
        } else if (session('ea_id')) {
            $this->erp_hotels_availabilty_offers_model->availabilty_offer_owner_type = 3;
            $this->erp_hotels_availabilty_offers_model->availabilty_offer_owner_id = session('ea_id');
        } else {
            $this->erp_hotels_availabilty_offers_model->availabilty_offer_owner_type = 0;
            $this->erp_hotels_availabilty_offers_model->availabilty_offer_owner_id = 0;
        }

        $data["total_rows"] = $this->erp_hotels_availabilty_offers_model->get(true);

        $this->erp_hotels_availabilty_offers_model->offset = $this->uri->segment("3");
        //$this->erp_hotels_availabilty_offers_model->limit = $this->config->item('per_page');
        $this->erp_hotels_availabilty_offers_model->order_by = array('erp_hotels_availabilty_offers_id', 'desc');
        $data["items"] = $this->erp_hotels_availabilty_offers_model->get();
        $config['uri_segment'] = 3;
        $config['base_url'] = site_url('hotels_availabilty_offers/index');
        $config['total_rows'] = $data["total_rows"];

        $config['per_page'] = $this->config->item('per_page');
        $config['suffix'] = $this->input->get() ? '?' . http_build_query($_GET) : NULL;
        $this->load->library('pagination');
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();
        $this->load->view('hotels_availabilty_offers/index', $data);
    }

    public function add() {
        $data = array();

        $this->load->library("form_validation");

        $this->form_validation->set_rules('name_la', 'lang:name_la', 'trim|required');
        $this->form_validation->set_rules('name_ar', 'lang:name_ar', 'trim|required');
        $this->form_validation->set_rules('nights_count', 'lang:nights_count', 'trim|required');

        if ($this->form_validation->run() == false) {
            $this->load->view("hotels_availabilty_offers/add", $data);
        } else {

            if (session('uo_id')) {
                $this->erp_hotels_availabilty_offers_model->availabilty_offer_owner_type = 2;
                $this->erp_hotels_availabilty_offers_model->availabilty_offer_owner_id = session('uo_id');
            } else if (session('ea_id')) {
                $this->erp_hotels_availabilty_offers_model->availabilty_offer_owner_type = 3;
                $this->erp_hotels_availabilty_offers_model->availabilty_offer_owner_id = session('ea_id');
            } else {
                permission();
            }


            $this->erp_hotels_availabilty_offers_model->name_la = $this->input->post('name_la');
            $this->erp_hotels_availabilty_offers_model->name_ar = $this->input->post('name_ar');
            $this->erp_hotels_availabilty_offers_model->nights_count = $this->input->post('nights_count');

            $this->erp_hotels_availabilty_offers_model->pricing_methodology = $this->input->post('pricing_methodology');

            $this->erp_hotels_availabilty_offers_model->discount_percent = $this->input->post('discount_percent');
            $this->erp_hotels_availabilty_offers_model->discount_fixed = $this->input->post('discount_fixed');
            $this->erp_hotels_availabilty_offers_model->rounding_value = $this->input->post('rounding_value');

            /*
              if(isset($_POST['look_to_haram'])){
              $this->erp_hotels_availabilty_offers_model->look_to_haram = 1;
              } else {
              $this->erp_hotels_availabilty_offers_model->look_to_haram = 0;
              }
             */



            // Save Order
            $erp_hotels_availabilty_offers_id = $this->erp_hotels_availabilty_offers_model->save();

            if (isset($erp_hotels_availabilty_offers_id)) {
                $this->load->view('redirect_message', array('msg' => lang('global_added_message'),
                    'url' => site_url('hotels_availabilty_offers'),
                    'model_title' => lang('title'), 'action' => lang('add_title')));
            }
        }
    }

    public function edit($id = FALSE) {
        $data = array();

        if (!$id) {
            show_404();
        }
        $this->erp_hotels_availabilty_offers_model->erp_hotels_availabilty_offers_id = $id;
        $item = $this->erp_hotels_availabilty_offers_model->get();
        $data['item'] = $item;

        if (!$data['item']) {
            show_404();
        }


        $this->load->library("form_validation");

        $this->form_validation->set_rules('name_la', 'lang:name_la', 'trim|required');
        $this->form_validation->set_rules('name_ar', 'lang:name_ar', 'trim|required');
        $this->form_validation->set_rules('nights_count', 'lang:nights_count', 'trim|required');

        if ($this->form_validation->run() == false) {
            $this->load->view("hotels_availabilty_offers/edit", $data);
        } else {
            if (session('uo_id')) {
                $this->erp_hotels_availabilty_offers_model->availabilty_offer_owner_type = 2;
                $this->erp_hotels_availabilty_offers_model->availabilty_offer_owner_id = session('uo_id');
            } else if (session('ea_id')) {
                $this->erp_hotels_availabilty_offers_model->availabilty_offer_owner_type = 3;
                $this->erp_hotels_availabilty_offers_model->availabilty_offer_owner_id = session('ea_id');
            } else {
                permission();
            }

            $this->erp_hotels_availabilty_offers_model->name_la = $this->input->post('name_la');
            $this->erp_hotels_availabilty_offers_model->name_ar = $this->input->post('name_ar');
            $this->erp_hotels_availabilty_offers_model->nights_count = $this->input->post('nights_count');

            $this->erp_hotels_availabilty_offers_model->pricing_methodology = $this->input->post('pricing_methodology');

            $this->erp_hotels_availabilty_offers_model->discount_percent = $this->input->post('discount_percent');
            $this->erp_hotels_availabilty_offers_model->discount_fixed = $this->input->post('discount_fixed');
            $this->erp_hotels_availabilty_offers_model->rounding_value = $this->input->post('rounding_value');


            // Save Order
            $this->erp_hotels_availabilty_offers_model->save();

            $this->load->view('redirect_message', array('msg' => lang('global_updated_message'),
                'url' => site_url('hotels_availabilty_offers'),
                'id' => $this->uri->segment("4"),
                'model_title' => lang('title'), 'action' => lang('edit_title')));
        }
    }

    public function getCompaniesByCompanyType() {
        $availabilty_offer_owner_type = $this->input->post('availabilty_offer_owner_type');

        $structure = array('id', name());
        $key = $structure['0'];
        if (isset($structure['1'])) {
            $value = $structure['1'];
        }
        $companies_arr = array();
        $companies = $this->erp_hotels_availabilty_offers_model->getCompaniesByCompanyType($availabilty_offer_owner_type);
        foreach ($companies as $company) {
            $companies_arr[$company->$key] = $company->$value;
        }


        $form_dropdown = form_dropdown('availabilty_offer_owner_id', $companies_arr, set_value('availabilty_offer_owner_id', ''), 'class="chosen-select chosen-rtl input-full"  tabindex="4" ');
        $form_dropdown = str_replace('<select name="availabilty_offer_owner_id" class="chosen-select chosen-rtl input-full" multiple tabindex="4" >', '', $form_dropdown);
        $form_dropdown = str_replace('</select>', '', $form_dropdown);
        echo $form_dropdown;
        exit;
    }

    function delete($id = false) {
        if (!$id) {
            show_404();
        }
        $this->erp_hotels_availabilty_offers_model->erp_hotels_availabilty_offers_id = $id;
        if (!$this->erp_hotels_availabilty_offers_model->delete())
            show_404();
        $this->load->view('redirect_message', array('msg' => lang('global_deleted_message'),
            'url' => site_url('hotels_availabilty_offers'),
            'model_title' => lang('title'), 'action' => lang('delete_title')));
    }

    function delete_all() {
        if ($this->input->post("delete_items")) {
            foreach ($this->input->post("delete_items") as $item) {
                $this->erp_hotels_availabilty_offers_model->erp_hotels_availabilty_offers_id = $item;

                $this->erp_hotels_availabilty_offers_model->delete();
            }
        } else {
            show_404();
        }
        redirect("hotels_availabilty_offers");
    }

}

/* End of file hotels_availabilty_offers.php */
/* Location: ./application/controllers/hotels_availabilty_offers.php */