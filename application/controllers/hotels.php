<?php

class Hotels extends Safa_Controller {

    public $module = "hotels";

    public function __construct() {
        parent::__construct();
        $this->layout = 'new';

        if (session('uo_id')) {
            session('side_menu_id', 5);
        } else if (session('ea_id')) {
            session('side_menu_id', 3);
        }

        $this->load->model('hotels_model');
        $this->load->model('erp_hotels_contact_officials_model');
        $this->load->model('erp_hotels_policies_details_model');
        $this->load->model('erp_hotel_advantages_details_model');
        $this->load->model('erp_hotel_speakinglanguage_details_model');
        $this->load->model('erp_hotels_photos_model');
        $this->load->model('erp_companies_hotels_model');
        $this->load->helper('db_helper');
        $this->lang->load('build_hotel');
        $this->lang->load('hotels');
        $this->lang->load('search_hotels');
        permission();
    }

    public function index() {
        /* search_hotels */
        if (($this->destination != 'admin') && ($this->destination != 'ea') && ($this->destination != 'uo'))
            show_404();
        if (isset($_GET['search']))
            $this->search();
        $this->hotels_model->join = TRUE;
        $data["total_rows"] = $this->hotels_model->get(true);
        $this->hotels_model->offset = $this->uri->segment("3");
        $this->hotels_model->limit = 10;
        $this->hotels_model->order_by = array('0' => 'erp_hotel_id', '1' => 'desc');
        $data["items"] = $this->hotels_model->get();
        $config['uri_segment'] = 3;
        $config['base_url'] = site_url('hotels/index');
        $config['total_rows'] = $data["total_rows"];
        $config['per_page'] = $this->hotels_model->limit;
        $config['suffix'] = $this->input->get() ? '?' . http_build_query($_GET) : NULL;
        $this->load->library('pagination');
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();
        $data['country'] = $this->hotels_model->get_countery_ksa();
        /* check for if the hotel exist */
        if ($this->destination == 'ea')
            $data['company_id'] = session('ea_id');
        if ($this->destination == 'uo')
            $data['company_id'] = session('uo_id');

        $this->load->view('hotels/index', $data);
    }

    function search() {
        if (($this->destination != 'admin') && ($this->destination != 'ea') && ($this->destination != 'uo'))
            show_404();
        if ($this->input->get("erp_city_id"))
            $this->hotels_model->erp_city_id = $this->input->get("erp_city_id");

        if ($this->input->get("erp_hotellevel_id"))
            $this->hotels_model->erp_hotellevel_id = $this->input->get("erp_hotellevel_id");

        if ($this->input->get("name_ar"))
            $this->hotels_model->name_ar = $this->input->get("name_ar");

        if ($this->input->get("name_la"))
            $this->hotels_model->name_la = $this->input->get("name_la");
    }

    function hotel_details($id = false) {
        if ($this->destination != 'admin' && $this->destination != 'uo' && $this->destination != 'ea')
            if (!$id)
                show_404();
        $this->hotels_model->erp_hotel_id = $id;
        $this->hotels_model->join = TRUE;
        $data['item'] = $this->hotels_model->get();

        /* get the advantages of the hotel */
        $this->erp_hotel_advantages_details_model->erp_hotel_id = $id;
        $data['hotel_advantages'] = $this->erp_hotel_advantages_details_model->get();

        /* get the speaklanguages of the hotel */
        $this->erp_hotel_speakinglanguage_details_model->erp_hotel_id = $id;
        $data['hotel_speaklanguage'] = $this->erp_hotel_speakinglanguage_details_model->get();


        /* get the polices of the hotel */
        $this->erp_hotels_policies_details_model->erp_hotels_id = $id;
        $data['hotel_polices'] = $this->erp_hotels_policies_details_model->get();

        /* get the hotels potos */
        $this->erp_hotels_photos_model->erp_hotel_id = $id;
        $data['hotel_photos'] = $this->erp_hotels_photos_model->get();
        $this->load->view('hotels/hotel_details', $data);
    }

    /* search utilities */

    function map($hotel_id = false) {
        if ($this->destination != 'admin' && $this->destination != 'uo' && $this->destination != 'ea')
            show_404();
        $this->layout = 'ajax';
        if (!$hotel_id)
            show_404();

        $this->load->helper('hotel_availability');
        $this->load->model('hotels_model');
        $this->hotels_model->erp_hotel_id = $hotel_id;
        $this->hotels_model->join = TRUE;
        $data['hotel'] = $this->hotels_model->get();
        if (!$data['hotel'])
            show_404();

        $this->load->view('hotels/map', $data);
    }

    /*     * ***************** */

    public function add() {
        if ($this->destination != 'admin' && $this->destination != 'uo')
            show_404();

        $data['country'] = $this->hotels_model->get_countery_ksa();
        $this->load->library("form_validation");
        /* first Step */
        $this->form_validation->set_rules('name_ar', 'lang:hotels_name_ar', 'trim');
        $this->form_validation->set_rules('name_la', 'lang:hotels_name_la', 'trim');
        $this->form_validation->set_rules('erp_city_id', 'lang:hotels_erp_city_id', 'trim');

        $this->form_validation->set_rules('geoarea_id', 'lang:geoarea', 'trim');
        $this->form_validation->set_rules('street_id', 'lang:street', 'trim');

        $this->form_validation->set_rules('address_ar', 'lang:hotels_address_ar', 'trim');
        $this->form_validation->set_rules('address_la', 'lang:hotels_address_la', 'trim');
        $this->form_validation->set_rules('floors_count', 'lang:floors_count', 'trim');
        $this->form_validation->set_rules('room_count', 'lang:room_count', 'trim');
        $this->form_validation->set_rules('bed_count', 'lang:room_count', 'trim');

        /*         * **** */
        $this->form_validation->set_rules('rooms_count_for_disabled', 'lang:rooms_count_for_disabled', 'trim');
        $this->form_validation->set_rules('flats_count', 'lang:flats_count', 'trim');
        $this->form_validation->set_rules('suites_count', 'lang:suites_count', 'trim');
        $this->form_validation->set_rules('lifts_count', 'lang:lifts_count', 'trim');
        $this->form_validation->set_rules('hotel_capacity', 'lang:hotel_capacity', 'trim');
        $this->form_validation->set_rules('hotel_creation_date', 'lang:hotel_creation_date', 'trim');
        $this->form_validation->set_rules('website', 'lang:website', 'trim');
        /* scound Step */

        $this->form_validation->set_rules('phone', 'lang:phone', 'trim');
        $this->form_validation->set_rules('fax', 'lang:fax', 'trim');
        $this->form_validation->set_rules('email', 'lang:email', 'trim');
        /* third Step */
        $this->form_validation->set_rules('hotellevel', 'lang:hotels_th_hotellevel', 'trim');
        $this->form_validation->set_rules('hotel_sub_level', 'lang:hotel_sublevel', 'trim');
        $this->form_validation->set_rules('map_distance', 'lang:map_distance', 'trim');
        /* fourth Step */

        /* fifth step */
        $this->form_validation->set_rules('langtitude', 'lang:longtitude', 'trim');
        $this->form_validation->set_rules('latitude', 'lang:latitude', 'trim');

        if ($this->form_validation->run() == false) {

            $this->load->view("hotels/add", $data);
        } else {
            /* first step */
            $this->hotels_model->name_ar = $this->input->post('name_ar');
            $this->hotels_model->name_la = $this->input->post('name_la');
            $this->hotels_model->erp_city_id = $this->input->post('erp_city_id');

            $this->hotels_model->erp_geoarea_id = $this->input->post('geoarea_id');
            $this->hotels_model->erp_street_id = $this->input->post('street_id');

            $this->hotels_model->address_ar = $this->input->post('address_ar');
            $this->hotels_model->address_la = $this->input->post('address_la');
            $this->hotels_model->floors_number = $this->input->post('floors_count');
            $this->hotels_model->rooms_number = $this->input->post('room_count');
            $this->hotels_model->beds_number = $this->input->post('bed_count');
            $this->hotels_model->remark = $this->input->post('remark');

            $this->hotels_model->rooms_count_for_disabled = $this->input->post('rooms_count_for_disabled');
            $this->hotels_model->flats_count = $this->input->post('flats_count');
            $this->hotels_model->suites_count = $this->input->post('suites_count');
            $this->hotels_model->lifts_count = $this->input->post('lifts_count');
            $this->hotels_model->hotel_capacity = $this->input->post('hotel_capacity');
            $this->hotels_model->hotel_creation_date = $this->input->post('hotel_creation_date');
            $this->hotels_model->website = $this->input->post('website');

            /* scound step */
            $this->hotels_model->phone = $this->input->post('phone');
            $this->hotels_model->email = $this->input->post('email');
            $this->hotels_model->fax = $this->input->post('fax');
            /* third step */
            $this->hotels_model->erp_hotellevel_id = $this->input->post('hotellevel');
            $this->hotels_model->erp_hotel_sub_levels_id = $this->input->post('hotel_sub_level');
            $this->hotels_model->distance = $this->input->post('map_distance');
            /* forth step */
            $this->hotels_model->googel_map_langtitude = $this->input->post('langtitude');
            $this->hotels_model->googel_map_latitude = $this->input->post('latitude');
            $this->hotels_model->distance = $this->input->post('map_distance');

            $hotel_id = $this->hotels_model->save();
            /* hotel offecial contacts */
            if ($this->input->post('official_work') && count($this->input->post('official_work')) > 0) {
                foreach ($this->input->post('official_work') as $index => $contact_work) {
                    $hotel = 0;
                    if ($contact_work != '') {
                        $this->db->set('position', $contact_work);
                        $hotel = 1;
                    }
                    if ($_POST['official_name'][$index] != '') {
                        $this->db->set('name', $_POST['official_name'][$index]);
                        $hotel = 1;
                    }
                    if ($_POST['official_email'][$index] != '') {
                        $this->db->set('email', $_POST['official_email'][$index]);
                        $hotel = 1;
                    }
                    if ($_POST['official_phone'][$index] != '') {
                        $this->db->set('phone', $_POST['official_phone'][$index]);
                        $hotel = 1;
                    }
                    if ($_POST['official_skype'][$index] != '') {
                        $this->db->set('skype', $_POST['official_skype'][$index]);
                        $hotel = 1;
                    }
                    if ($_POST['official_messenger'][$index] != '') {
                        $this->db->set('messenger', $_POST['official_messenger'][$index]);
                        $hotel = 1;
                    }
                    if ($_POST['official_phonetype'][$index] != '') {
                        $this->db->set('erp_phone_type_id', $_POST['official_phonetype'][$index]);
                        $hotel = 1;
                    }
                    if ($hotel == 1) {
                        $this->db->set('erp_hotel_id', $hotel_id);
                        $this->db->insert('erp_hotels_contact_officials');
                    }
                };
            }
            /* hotel policy */
            if ($this->input->post('hotels_policy') && count($this->input->post('hotels_policy')) > 0) {
                foreach ($this->input->post('hotels_policy') as $index => $policy) {
                    $flag = 0;
                    if ($_POST['hotels_policy'][$index] != '') {
                        $this->db->set('erp_hotels_policies_id', $_POST['hotels_policy'][$index]);
                        $flag = 1;
                    }
                    if ($_POST['policy_description'][$index] != '') {
                        $this->db->set('policy_description', $_POST['policy_description'][$index]);
                        $flag = 1;
                    }
                    if ($flag == 1) {
                        $this->db->set('erp_hotels_id', $hotel_id);
                        $this->db->insert('erp_hotels_policies_details');
                    }
                }
            }
            /* inserting hotel advantages */
            if ($this->input->post('hotels_advantages') && count($this->input->post('hotels_advantages')) > 0) {
                foreach ($this->input->post('hotels_advantages') as $index => $advantage) {
                    $this->db->set('erp_hotel_advantages_id', $advantage)
                            ->set('erp_hotel_id', $hotel_id);
                    $this->db->insert('erp_hotel_advantages_details');
                }
            }
            /* insertinghotellanguages */
            if ($this->input->post('hotels_languages') && count($this->input->post('hotels_languages')) > 0) {
                foreach ($this->input->post('hotels_languages') as $index => $value) {
                    $this->erp_hotel_speakinglanguage_details_model->erp_hotelspeakinglanguage_id = $value;
                    $this->erp_hotel_speakinglanguage_details_model->erp_hotel_id = $hotel_id;
                    $this->erp_hotel_speakinglanguage_details_model->save();
                }
            }
            /* set_images */
            if ($this->input->post('image_info') && count($this->input->post('image_info')) > 0) {
                /* inserting the images into a folder that holds hotel_id */
                $this->set_hotel_images($this->input->post('image_info'), $this->input->post('image_name'), $hotel_id);
            }
            redirect("hotels/index");
        }
    }

    public function edit($id = false) {
        if ($this->destination != 'admin' && $this->destination != 'uo')
            show_404();
        if (!$id)
            show_404();
        $this->hotels_model->erp_hotel_id = $id;
        $this->hotels_model->join = TRUE;
        $data['items'] = $this->hotels_model->get();
        if (!$data['items'])
            show_404();

        /* hotel officials */
        $this->erp_hotels_contact_officials_model->erp_hotel_id = $id;
        $data['hotel_contact_officials'] = $this->erp_hotels_contact_officials_model->get();
        /* hotel policies */
        $this->erp_hotels_policies_details_model->erp_hotels_id = $id;
        $data['hotel_policies'] = $this->erp_hotels_policies_details_model->get();
        /* hotels advantages */
        $this->erp_hotel_advantages_details_model->erp_hotel_id = $id;
        $data['hotel_advantages'] = $this->erp_hotel_advantages_details_model->get();
        /* hotel_languages */
        $this->erp_hotel_speakinglanguage_details_model->erp_hotel_id = $id;
        $data['hotel_languages'] = $this->erp_hotel_speakinglanguage_details_model->get();
        /* hotel images */
        $data['hotel_imgs'] = $this->GetimgAsBinary($id);
        $this->load->library('form_validation');
        /* first Step */
        $this->form_validation->set_rules('name_ar', 'lang:hotels_name_ar', 'trim');
        $this->form_validation->set_rules('name_la', 'lang:hotels_name_la', 'trim');
        $this->form_validation->set_rules('erp_city_id', 'lang:hotels_erp_city_id', 'trim');

        $this->form_validation->set_rules('geoarea_id', 'lang:geoarea', 'trim');
        $this->form_validation->set_rules('street_id', 'lang:street', 'trim');

        $this->form_validation->set_rules('address_ar', 'lang:hotels_address_ar', 'trim');
        $this->form_validation->set_rules('address_la', 'lang:hotels_address_la', 'trim');
        $this->form_validation->set_rules('floors_count', 'lang:floors_count', 'trim');
        $this->form_validation->set_rules('room_count', 'lang:room_count', 'trim');
        $this->form_validation->set_rules('bed_count', 'lang:room_count', 'trim');
        $this->form_validation->set_rules('remark', 'lang:remark', 'trim');
        /*         * **** */
        $this->form_validation->set_rules('rooms_count_for_disabled', 'lang:rooms_count_for_disabled', 'trim');
        $this->form_validation->set_rules('flats_count', 'lang:flats_count', 'trim');
        $this->form_validation->set_rules('suites_count', 'lang:suites_count', 'trim');
        $this->form_validation->set_rules('lifts_count', 'lang:lifts_count', 'trim');
        $this->form_validation->set_rules('hotel_capacity', 'lang:hotel_capacity', 'trim');
        $this->form_validation->set_rules('hotel_creation_date', 'lang:hotel_creation_date', 'trim');
        $this->form_validation->set_rules('website', 'lang:website', 'trim');
        /*         * ** */
        /* scound Step */
        $this->form_validation->set_rules('phone', 'lang:phone', 'trim');
        $this->form_validation->set_rules('fax', 'lang:fax', 'trim');
        $this->form_validation->set_rules('email', 'lang:email', 'trim');
        /* third Step */
        $this->form_validation->set_rules('hotellevel', 'lang:hotels_th_hotellevel', 'trim');
        $this->form_validation->set_rules('hotel_sub_level', 'lang:hotel_sublevel', 'trim');
        $this->form_validation->set_rules('map_distance', 'lang:map_distance', 'trim');
        /* fourth Step */


        /* fifth step */
        $this->form_validation->set_rules('langtitude', 'lang:longtitude', 'trim');
        $this->form_validation->set_rules('latitude', 'lang:latitude', 'trim');


        if ($this->form_validation->run() == false) {

            $this->load->view("hotels/edit", $data);
        } else {


            /* first step */
            $this->hotels_model->name_ar = $this->input->post('name_ar');
            $this->hotels_model->name_la = $this->input->post('name_la');
            $this->hotels_model->erp_city_id = $this->input->post('erp_city_id');

            $this->hotels_model->erp_geoarea_id = $this->input->post('geoarea_id');
            $this->hotels_model->erp_street_id = $this->input->post('street_id');

            $this->hotels_model->address_ar = $this->input->post('address_ar');
            $this->hotels_model->address_la = $this->input->post('address_la');
            $this->hotels_model->floors_number = $this->input->post('floors_count');
            $this->hotels_model->rooms_number = $this->input->post('room_count');
            $this->hotels_model->beds_number = $this->input->post('bed_count');
            $this->hotels_model->remark = $this->input->post('remark');

            $this->hotels_model->rooms_count_for_disabled = $this->input->post('rooms_count_for_disabled');
            $this->hotels_model->flats_count = $this->input->post('flats_count');
            $this->hotels_model->suites_count = $this->input->post('suites_count');
            $this->hotels_model->lifts_count = $this->input->post('lifts_count');
            $this->hotels_model->hotel_capacity = $this->input->post('hotel_capacity');
            $this->hotels_model->hotel_creation_date = $this->input->post('hotel_creation_date');
            $this->hotels_model->website = $this->input->post('website');

            /* scound step */
            $this->hotels_model->phone = $this->input->post('phone');
            $this->hotels_model->email = $this->input->post('email');
            $this->hotels_model->fax = $this->input->post('fax');
            /* third step */
            $this->hotels_model->erp_hotellevel_id = $this->input->post('hotellevel');
            $this->hotels_model->erp_hotel_sub_levels_id = $this->input->post('hotel_sub_level');
            $this->hotels_model->distance = $this->input->post('map_distance');

            /* forth step */
            $this->hotels_model->googel_map_langtitude = $this->input->post('langtitude');
            $this->hotels_model->googel_map_latitude = $this->input->post('latitude');
            $this->hotels_model->save();
            /* edit hotel policy */

            if ($this->input->post('hotels_policy')) {
                /*                 * * */
                $this->erp_hotels_policies_details_model->delete();
                foreach ($this->input->post('hotels_policy') as $index => $value) {
                    $flag = 0;
                    if ($value != '') {
                        $this->erp_hotels_policies_details_model->erp_hotels_policies_id = $value;
                        $flag = 1;
                    }
                    if ($_POST['policy_description'][$index] != '') {
                        $this->erp_hotels_policies_details_model->policy_description = $_POST['policy_description'][$index];
                        $flag = 1;
                    }
                    if ($flag == 1) {
                        $this->erp_hotels_policies_details_model->save();
                    }
                }
            }
            /* edit official info */

            if ($this->input->post('official_work')) {
                $this->erp_hotels_contact_officials_model->delete();
                foreach ($this->input->post('official_work') as $index => $value) {
                    $flag = 0;
                    if ($_POST['official_name'][$index] != '') {
                        $this->erp_hotels_contact_officials_model->name = $_POST['official_name'][$index];
                        $flag = 1;
                    }
                    if ($value != '') {
                        $this->erp_hotels_contact_officials_model->position = $value;
                        $flag = 1;
                    }
                    if ($_POST['official_email'][$index] != '') {
                        $this->erp_hotels_contact_officials_model->email = $_POST['official_email'][$index];
                        $flag = 1;
                    }
                    if ($_POST['official_phone'][$index] != '') {
                        $this->erp_hotels_contact_officials_model->phone = $_POST['official_phone'][$index];
                        $flag = 1;
                    }
                    if ($_POST['official_phonetype'][$index] != '') {
                        $this->erp_hotels_contact_officials_model->erp_phone_type_id = $_POST['official_phonetype'][$index];
                        $flag = 1;
                    }
                    if ($_POST['official_skype'][$index] != '') {
                        $this->erp_hotels_contact_officials_model->skype = $_POST['official_skype'][$index];
                        $flag = 1;
                    }
                    if ($_POST['official_messenger'][$index] != '') {
                        $this->erp_hotels_contact_officials_model->messenger = $_POST['official_messenger'][$index];
                        $flag = 1;
                    }
                    if ($flag == 1) {
                        $this->erp_hotels_contact_officials_model->save();
                    }
                }
            }
            /* edit hotel advantages */
            if ($this->input->post('hotels_advantages')) {
                $this->erp_hotel_advantages_details_model->delete();
                foreach ($this->input->post('hotels_advantages') as $index => $value) {
                    $this->erp_hotel_advantages_details_model->erp_hotel_advantages_id = $value;
                    $this->erp_hotel_advantages_details_model->save();
                }
            }
            /* edit hotel languages */
            if ($this->input->post('hotels_languages')) {
                $this->erp_hotel_speakinglanguage_details_model->delete();
                foreach ($this->input->post('hotels_languages') as $index => $value) {
                    $this->erp_hotel_speakinglanguage_details_model->erp_hotelspeakinglanguage_id = $value;
                    $this->erp_hotel_speakinglanguage_details_model->save();
                }
            }

            /* set_images */
            if (!$this->input->post('image_info') && !$this->input->post('image_name')) {
                /* inserting the images into a folder that holds hotel_id */
                $this->delete_hotel_images($id);
                $this->erp_hotels_photos_model->erp_hotel_id = $id;
                $this->erp_hotels_photos_model->delete();
            } else {
                $this->delete_hotel_images($id);
                $this->erp_hotels_photos_model->erp_hotel_id = $id;
                $this->erp_hotels_photos_model->delete();
                $this->set_hotel_images($this->input->post('image_info'), $this->input->post('image_name'), $id);
            }
            /* check for details */

            redirect("hotels/index");
        }
    }

    function delete($hotel_id = false) {
        if ($this->destination != 'admin')
            show_404();
        $this->layout = 'ajax';
        if (!$this->input->is_ajax_request()) {
            redirect('hotels/index');
        } else {
            $data = $this->hotels_model->check_delete_ability($hotel_id);
            if ($data == 0) {
                $this->hotels_model->erp_hotel_id = $hotel_id;
                echo $this->hotels_model->delete();
            } else {
                echo '0'; // TODO AFTRE CHECK DATABASE SCHEMA.
            }
        }
    }

    function alpha_dash_space($str) {
        return (!preg_match("/^([-a-z_ ])+$/i", $str)) ? FALSE : TRUE;
    }

    function check_name_ar($name_ar) {
        $city_id = $this->input->post('erp_city_id');
        $this->db->where('name_ar', $name_ar);
        $this->db->where('erp_city_id', $city_id);
        if ($this->db->get('erp_hotels')->num_rows()) {
            $this->form_validation->set_message('check_name_ar', lang('hotels_name_in_city_exist'));
            return FALSE;
        } else
            return true;
    }

    function check_name_la($name_la) {
        $city_id = $this->input->post('erp_city_id');
        $this->db->where('name_la', $name_la);
        $this->db->where('erp_city_id', $city_id);
        if ($this->db->get('erp_hotels')->num_rows()) {
            $this->form_validation->set_message('check_name_la', lang('hotels_name_in_city_exist'));
            return FALSE;
        } else
            return true;
    }

    function is_unique_ar($value, $id = false) {
        $city_id = $this->input->post('erp_city_id');
        if ($id == false) {
            $query = $this->db->limit(1)->where("name_ar", $value)->get("erp_hotels");
        } else {
            $this->db->limit(1);
            $this->db->where("name_ar", $value);
            $this->db->where("erp_city_id", $city_id);
            $query = $this->db->where("hotel_id!=" . $id)->get("erp_hotels");
        }
        if ($query->num_rows()) {
            $this->form_validation->set_message('is_unique_ar', lang('hotels_name_in_city_exist'));
            return false;
        } else
            return true;
    }

    function is_unique_la($value, $id = false) {
        $city_id = $this->input->post('erp_city_id');
        if ($id == false) {
            $query = $this->db->limit(1)->where("name_la", $value)->get("erp_hotels");
        } else {
            $this->db->limit(1);
            $this->db->where("name_la", $value);
            $this->db->where("erp_city_id", $city_id);
            $query = $this->db->where("hotel_id!=" . $id)->get("erp_hotels");
        }
        if ($query->num_rows()) {
            $this->form_validation->set_message('is_unique_la', lang('hotels_name_in_city_exist'));
            return false;
        } else
            return true;
    }

    function get_erp_city($erp_country_id = '') {
        $this->layout = 'ajax';
        $er_cities = $this->hotels_model->get_cities($erp_country_id);
        echo "<option value=>" . lang('global_select_from_menu') . "</option>";
        if (isset($er_cities) && count($er_cities) > 0) {
            foreach ($er_cities as $city) {
                echo "<option value='" . $city->erp_city_id . "'>" . $city->{name()} . "</option>";
            }
        }
    }

    function get_geoarea($city_id = " ") {
        $this->layout = 'ajax';
        $this->load->model('erp_geoareas_model');
        $this->erp_geoareas_model->erp_city_id = $city_id;
        $areas = $this->erp_geoareas_model->get();
        echo "<option value=''>" . lang('global_select_from_menu') . "</option>";
        if (count($areas) > 0) {
            foreach ($areas as $index => $area) {
                echo "<option value='" . $area->geoarea_id . "'>" . $area->{name()} . "</option>";
            }
        } else
            echo " ";
    }

    function get_street($geo_id = " ") {
        $this->layout = 'ajax';
        $this->load->model('erp_streets_model');
        $this->erp_streets_model->geoarea_id = $geo_id;
        $streets = $this->erp_streets_model->get();
        echo "<option value=''>" . lang('global_select_from_menu') . "</option>";
        if (count($streets) > 0) {
            foreach ($streets as $index => $street) {
                echo "<option value='" . $street->erp_street_id . "'>" . $street->{name()} . "</option>";
            }
        }
    }

    function check_city_id($city_id) {
        if ($city_id == 0) {
            $this->form_validation->set_message('check_city_id', lang('hotels_select_from_menu_false'));
            return false;
        } else
            return true;
    }

    function GetimgAsBinary($hotel_folder_num) {
        $this->load->helper('file');
        /*
         * takes folder number
         * returns the array of file as base64encoded  
         */

        //By Gouda
        $folder_path = './static/hotel_album/' . $hotel_folder_num;

        $binary_arr = array();
        if (file_exists($folder_path)) {
            $files_info_arr = get_filenames($folder_path);

            foreach ($files_info_arr as $index => $file_name) {
                if (strpos($file_name, 'thumb') !== false) {
                    
                } else {
                    $file_path = $folder_path . '/' . $file_name;
                    $file_asbinary = read_file($file_path);
                    $mime = get_mime_by_extension($file_path);
                    $base64 = base64_encode($file_asbinary);
                    $binary_arr[] = array(
                        'src' => "data:$mime;base64,$base64",
                        'name' => $file_name
                    );
                }
            }
        }
        return $binary_arr;
    }

    function set_hotel_images($img_info = false, $img_name = false, $hotel_id = false) {
        $this->load->helper('file');
        /* first make adirectory */
        $folder_path = './static/hotel_album/' . $hotel_id;
        if (!file_exists($folder_path))
            mkdir($folder_path);
        foreach ($img_info as $index => $img_binary) {
            $file_path = $folder_path . '/' . $img_name[$index];
            $file_data = base64_decode(substr($img_binary, strpos($img_binary, ',') + 1));
            $insertd = write_file($file_path, $file_data);
            if ($insertd) {
                /* inserting the images_names into database */
                /* select if the image notexsist in the image */
                $this->erp_hotels_photos_model->erp_hotel_id = $hotel_id;
                $this->erp_hotels_photos_model->hotel_photospath = $img_name[$index];
                $row = $this->erp_hotels_photos_model->get(true);
                if ($row == 0) {
                    $this->erp_hotels_photos_model->erp_hotel_id = $hotel_id;
                    $this->erp_hotels_photos_model->hotel_photospath = $img_name[$index];
                    $this->erp_hotels_photos_model->save();
                }
                /* creating thumbnails */
                $this->createthumb($file_path, $folder_path, $img_name[$index]);
            }
        }
    }

    function delete_hotel_images($hotel_id = false) {
        $this->load->helper('file');
        $folder_path = './static/hotel_album/' . $hotel_id;
        delete_files($folder_path);
    }

    function createthumb($img_src, $folder_path, $img_name) {
        $folder_path.='/thumbs';
        if (!file_exists($folder_path))
            mkdir($folder_path);

        $configs['image_library'] = 'gd2';
        $config['create_thumb'] = TRUE;
        $config['maintain_ratio'] = TRUE;
        $config['thumb_marker'] = '';
        $config['source_image'] = $img_src;

        //By Gouda
        //$config['width'] = 96;
        //$config['height'] = 96;
        $config['width'] = 150;
        $config['height'] = 150;

        $config['new_image'] = $folder_path . '/thumb_' . $img_name;
        $this->load->library('image_lib', $config);
        $this->image_lib->resize();
    }

}

// end class
