<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Payments extends Safa_Controller {

    public $module = "payments";
    public $layout = 'new';
    public $table_pk = 'erp_payment_id';

    public function __construct() {
        parent::__construct();
        
    	//Side menu session, By Gouda.
        if (session('uo_id')) {
            session('side_menu_id', 8);
        } else if (session('ea_id')) {
            session('side_menu_id', 7);
        }else if (session('ito_id')) {
            session('side_menu_id', 3);
        }
        
        $this->load->model('payments_model');
        $this->lang->load('ea_payments');
        $this->lang->load('payments');
        
        permission();
    }

    public function index() {
        $data['erp_company_types'] = ddgen('erp_company_types', array('erp_company_type_id', name()));
        $data['erp_companies'] = array();
        $this->payments_model->owner_erp_company_type_id = $this->system_id;
        $this->payments_model->owner_erp_company_id = session($this->destination . '_id');
        $this->payments_model->custom_select = 'erp_payments.*'
                . ', (SELECT ' . name() . ' FROM erp_currencies WHERE erp_payments.erp_currency_id = erp_currencies.erp_currency_id) AS currency'
                . ', (SELECT ' . name() . ' FROM crm_payment_types WHERE erp_payments.payment_type = crm_payment_types.crm_payment_type_id ) AS payment'
                . ', (CASE '
                . ' WHEN erp_payments.erp_company_type_id = 2 THEN (SELECT '.name().' FROM safa_uos WHERE safa_uos.safa_uo_id = erp_payments.erp_company_id)'
                . ' WHEN erp_payments.erp_company_type_id = 3 THEN (SELECT '.name().' FROM safa_eas WHERE safa_eas.safa_ea_id = erp_payments.erp_company_id)'
                . ' WHEN erp_payments.erp_company_type_id = 4 THEN (SELECT '.name().' FROM safa_hms WHERE safa_hms.safa_hm_id = erp_payments.erp_company_id)'
                . ' WHEN erp_payments.erp_company_type_id = 5 THEN (SELECT '.name().' FROM safa_itos WHERE safa_itos.safa_ito_id = erp_payments.erp_company_id)'
                . ' END)  AS company';
//                . ', (SELECT name FROM  WHERE erp_payments.crm_cashbox_id = crm_cashbox.crm_cashbox_id ) AS cashbox';
        $data['module'] = $this->module;
        $data['destination'] = $this->destination;
        $data['table_pk'] = $this->table_pk;
        if (isset($_GET['search']))
            $this->search();
        $data["total_rows"] = $this->payments_model->get(true);
        $this->payments_model->offset = $this->uri->segment("3");
       // $this->payments_model->limit = config('per_page');
        $data["items"] = $this->payments_model->get();
        $config['uri_segment'] = 3;
        $config['base_url'] = site_url('payments/index/');
        $config['total_rows'] = $data["total_rows"];
        $config['per_page'] = $this->payments_model->limit;
        $config['suffix'] = $this->input->get() ? '?' . http_build_query($_GET) : NULL;
        $this->load->library('pagination');
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();
        $this->load->view('payments/index', $data);
    }

    public function manage($id = false) {
        $this->load->library("form_validation");
        $data['id'] = $id;
        $data['module'] = $this->module;
        $data['destination'] = $this->destination;
        $data['erp_company_types'] = ddgen('erp_company_types', array('erp_company_type_id', name()));
        unset($data['erp_company_types']['1']);

        if ($id) {
            $this->payments_model->owner_erp_company_type_id = $this->system_id;
            $this->payments_model->owner_erp_company_id = session($this->destination . '_id');
            $this->payments_model->erp_payment_id = $id;
            $data['item'] = $this->payments_model->get();
            if (!$data['item'])
                show_404();
            $op = $this->db->where('erp_company_type_id', $data['item']->erp_company_type_id)->get('erp_company_types')->row();
            $data['erp_companies'] = iddgen($op->table);
        }
        else {
            $data['item'] = new stdClass();
            $data['item']->payment_type = 1;
            $data['item']->amount = NULL;
            $data['item']->erp_currency_id = NULL;
            $data['item']->erp_company_type_id = NULL;
            $data['item']->erp_company_id = NULL;
            $data['item']->rate = NULL;
            $data['item']->order_no = NULL;
            $data['item']->notes = NULL;
            $data['item']->date = date('Y-m-d H:i');
            $data['erp_companies'] = array(' ');
            $voucher = $this->db->select('MAX(voucher) as voucher')
                                ->where('owner_erp_company_type_id', $this->system_id)
                                ->where('owner_erp_company_id', $this->{$this->destination.'_id'})
                                ->get('erp_payments')
                                ->row()->voucher;
            if( ! $voucher)
                $voucher = 0;
             $data['item']->voucher = $voucher + 1;
        }
        $this->form_validation->set_rules('amount', lang($data['module'] . '_amount'), 'trim|required|numeric');
        $this->form_validation->set_rules('erp_currency_id', lang($data['module'] . '_erp_currency_id'), $this->input->post('crm_package_id') ? 'trim' : 'trim|required');
        $this->form_validation->set_rules('payment_type', lang($data['module'] . '_payment_type'), 'required');
        $this->form_validation->set_rules('voucher', lang($data['module'] . '_voucher'), 'required|callback_voucher');
        $this->form_validation->set_rules('date', lang($data['module'] . '_date'), 'required');
        $this->form_validation->set_rules('notes', lang($data['module'] . '_notes'), 'trim');
        if ($this->form_validation->run() == false) {
            $this->load->view("payments/manage", $data);
        } else {
            if ($id)
                $this->payments_model->erp_payment_id = $id;
            $this->payments_model->voucher = $this->input->post('voucher');
            $this->payments_model->notes = $this->input->post('notes');
            $this->payments_model->date = $this->input->post('date');
            $this->payments_model->erp_currency_id = $this->input->post('erp_currency_id');
            $this->payments_model->order_no = $this->input->post('order_no');
            $this->payments_model->amount = $this->input->post('amount');
            $this->payments_model->payment_type = $this->input->post('payment_type');
            $this->payments_model->erp_company_type_id = $this->input->post('erp_company_type_id');
            $this->payments_model->erp_company_id = $this->input->post('erp_company_id');
            $this->payments_model->owner_erp_company_type_id = $this->system_id;
            $this->payments_model->owner_erp_company_id = session($this->destination . '_id');
            $this->payments_model->rate = $this->input->post('rate');
            $this->payments_model->submission_datetime = date('Y-m-d H:i');
            $id = $this->payments_model->save();

//            $this->db->insert('erp_payments_log', array(
//                'crm_ea_payment_id' => $id,
//                'crm_cashbox_id' => $this->payments_model->crm_cashbox_id,
//                'crm_ea_license_id' => $license_id,
//                'crm_package_id' => $this->payments_model->crm_package_id,
//                'erp_admin_id' => $this->payments_model->erp_admin_id,
//                'payment_type' => $this->payments_model->payment_type,
//                'amount' => $this->payments_model->amount,
//                'erp_currency_id' => $this->payments_model->erp_currency_id,
//                'voucher' => $this->payments_model->voucher,
//                'notes' => $this->payments_model->notes,
//                'date' => $this->payments_model->date,
//                'credit' => $this->payments_model->credit,
//                'submission_datetime' => $this->payments_model->submission_datetime
//            ));
            redirect("payments/index/");
        }
    }

    public function log($id = false) {
        if (!$id)
            show_404();
        $this->layout = 'js_new';
        $data['module'] = $this->module;
        $data['destination'] = $this->destination;
        $payment = $this->db->where('crm_ea_payment_id', $id)->get('crm_ea_payments')->row();
        if (!$payment)
            show_404();

        $data['items'] = $this->db->select('crm_ea_payments_log.*,'
                        . '(SELECT ' . name() . ' FROM erp_currencies WHERE crm_ea_payments_log.erp_currency_id = erp_currencies.erp_currency_id) AS currency,'
                        . '(SELECT username FROM erp_admins WHERE crm_ea_payments_log.erp_admin_id = erp_admins.erp_admin_id ) AS admin,'
                        . '(SELECT ' . name() . ' FROM crm_payment_types WHERE crm_ea_payments_log.payment_type = crm_payment_types.crm_payment_type_id ) AS payment')
                ->where('crm_ea_payment_id', $id)
                ->get('crm_ea_payments_log')
                ->result();

        $this->load->view('payments/log', $data);
    }

    public function search() {
        if ($this->input->get("crm_ea_payment_id"))
            $this->payments_model->crm_ea_payment_id = $this->input->get("crm_ea_payment_id");
        if ($this->input->get("crm_cashbox_id"))
            $this->payments_model->crm_cashbox_id = $this->input->get("crm_cashbox_id");
        if ($this->input->get("crm_ea_license_id"))
            $this->payments_model->crm_ea_license_id = $this->input->get("crm_ea_license_id");
        if ($this->input->get("crm_package_id"))
            $this->payments_model->crm_package_id = $this->input->get("crm_package_id");
        if ($this->input->get("erp_admin_id"))
            $this->payments_model->erp_admin_id = $this->input->get("erp_admin_id");
        if ($this->input->get("payment_type"))
            $this->payments_model->payment_type = $this->input->get("payment_type");
        if ($this->input->get("amount"))
            $this->payments_model->amount = $this->input->get("amount");
        if ($this->input->get("erp_currency_id"))
            $this->payments_model->erp_currency_id = $this->input->get("erp_currency_id");
        if ($this->input->get("voucher"))
            $this->payments_model->voucher = $this->input->get("voucher");
        if ($this->input->get("notes"))
            $this->payments_model->notes = $this->input->get("notes");
        if ($this->input->get("date"))
            $this->payments_model->date = $this->input->get("date");
        if ($this->input->get("credit"))
            $this->payments_model->credit = $this->input->get("credit");
        if ($this->input->get("submition_datetime"))
            $this->payments_model->submition_datetime = $this->input->get("submition_datetime");
         if ($this->input->get('from_date'))
            $this->payments_model->from_date = $this->input->get('from_date');
        if ($this->input->get('to_date'))
            $this->payments_model->to_date = $this->input->get('to_date');
         if ($this->input->get('from_amount'))
            $this->payments_model->from_amount = $this->input->get('from_amount');
        if ($this->input->get('to_amount'))
            $this->payments_model->to_amount = $this->input->get('to_amount');
        
    }

    public function update_companies() {
        $this->layout = 'ajax';
        if (!$this->input->post('id'))
            show_404();
        $op = $this->db->where('erp_company_type_id', $this->input->post('id'))->get('erp_company_types')->row();
        echo form_ajax_dropdown('', iddgen($op->table), set_value('erp_company_id'));
    }

    public function report() {
        $data['destination'] = $this->destination;
        $data['erp_company_types'] = ddgen('erp_company_types', array('erp_company_type_id', name()));
        $data['erp_companies'] = array(' ');
        unset($data['erp_company_types']['1']);
        $data['module'] = $this->module;
        $this->payments_model->custom_select = 'erp_payments.*,'
                . '(SELECT ' . name() . ' FROM erp_currencies WHERE erp_payments.erp_currency_id = erp_currencies.erp_currency_id) AS currency,';
        $this->load->library('form_validation');
        $data['item'] = new stdClass();
        $data['item']->erp_company_type_id = NULL;
        $data['item']->erp_company_id = NULL;
        $data['item']->from_date = NULL;
        $data['item']->to_date = NULL;
        $this->form_validation->set_rules('erp_company_type_id', lang($data['module'] . '_erp_company_type_id'), 'required');
        $this->form_validation->set_rules('erp_company_id', lang($data['module'] . '_erp_company_id'), 'required');
        $this->form_validation->set_rules('from_date', lang($data['module'] . '_from_date'), 'trim');
        $this->form_validation->set_rules('to_date', lang($data['module'] . '_to_date'), 'trim');
        if ($this->form_validation->run() == false) {
            $this->load->view("payments/report", $data);
        } else {
            $this->payments_model->owner_erp_company_type_id = $this->system_id;
            $this->payments_model->owner_erp_company_id = session($this->destination . '_id');
            $this->payments_model->erp_company_type_id = $this->input->post('erp_company_type_id');
            $this->payments_model->erp_company_id = $this->input->post('erp_company_id');
            if ($this->input->post('from_date'))
                $this->payments_model->from_date = $this->input->post('from_date');
            if ($this->input->post('to_date'))
                $this->payments_model->to_date = $this->input->post('to_date');
            $data['items'] = $this->payments_model->get();
            $this->load->view("payments/report", $data);
        }
    }

    public function voucher($var) {
        $row = $this->db->where('owner_erp_company_type_id', $this->system_id)
                        ->where('owner_erp_company_id', $this->{$this->destination.'_id'})
                        ->where('voucher', $var)
                        ->get('erp_payments')
                        ->row();
        if($row)
        {
            $this->form_validation->set_message('voucher', lang('payments_voucher_must_be_unique'));
            return FALSE;
        }
        else
            return TRUE;
    }
}

/* End of file ea_payments.php */
/* Location: ./application/controllers/ea_payments.php */