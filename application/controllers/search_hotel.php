<?php

class search_hotel extends Safa_Controller {

    public function index() {
        $this->layout = 'new';
        // $this->lang->load('screen1');
        $data['nationalities'] = ddgen('erp_nationalities', array('erp_nationality_id', name()));
        $this->load->view('search_hotel/search_hotel', $data);
    }

}
