<?php

class language_update extends Safa_Controller {

    public function __construct() {
        parent::__construct();
        $this->layout = 'new';
        $this->load->model('erp_languages_model');
        $this->load->model('safa_language_keys_model');
        $this->load->model('safa_languages_files_model');
        $this->load->model('safa_languages_phrases_model');
        $this->lang->load('language');
//        permission();
    }

    public function index() {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('from', lang('lang_from'), 'required');
        $this->form_validation->set_rules('to', lang('lang_to'), 'required');

        if ($this->form_validation->run()) {
            $from = $this->input->post('from');
            $to = $this->input->post('to');
            redirect(site_url('language_update/update_lang/' . $from . '/' . $to));
        } else {
            $languages = $this->erp_languages_model->get();
            $data['from_lang'] = array('' => lang('lang_select'));
            $data['to_lang'] = array('' => lang('lang_select'));
            foreach ($languages as $lang) {
                $data['from_lang'][$lang->erp_language_id] = $lang->path;
                $data['to_lang'][$lang->erp_language_id] = $lang->path;
            }
            $this->load->view('languages/index', $data);
        }
    }

    public function update_lang($fromlang = false, $tolang = false) {
        if (!$fromlang || !$tolang)
            show_404();

        $this->load->library('form_validation');
        $this->form_validation->set_rules('file_id', lang('lang_to'), 'required');
        if ($this->form_validation->run()) {
            $tolangrows = $this->input->post('row');
            foreach ($tolangrows as $key => $phrase) {
                if (strlen($phrase)) {
                    $this->safa_languages_phrases_model->safa_language_key_id = $key;
                    $this->safa_languages_phrases_model->safa_language_id = $tolang;
                    $this->safa_languages_phrases_model->phrase = $phrase;
                    $this->safa_languages_phrases_model->save();
                }
            }

            $data['datasaved'] = lang('lang_saved');
        }
        $data['files'] = array('0' => lang('global_all'));
        $files = $this->safa_languages_files_model->get();
        foreach ($files as $file)
            $data['files'][$file->safa_file_id] = $file->file_name;

        if ($this->input->post('file_id'))
            $fileselect = $this->input->post('file_id');
        else
            $fileselect = $files[0]->safa_file_id;

        $data['fromvalues'] = $this->erp_languages_model->get_file_lang($fromlang, $fileselect);
        $this->erp_languages_model->erp_language_id = $fromlang;
        $data['fromlang'] = $this->erp_languages_model->get();

        $data['tovalues'] = $this->erp_languages_model->get_file_lang($tolang, $fileselect);
        $this->erp_languages_model->erp_language_id = $tolang;
        $data['tolang'] = $this->erp_languages_model->get();

        $this->load->view('languages/translate', $data);
    }

    public function getfilerows($file_id = false, $fromlang = false, $tolang = false) {
        $this->layout = 'ajax';
        $data['fromvalues'] = $this->erp_languages_model->get_file_lang($fromlang, $file_id);
        $this->erp_languages_model->erp_language_id = $fromlang;
        $data['fromlang'] = $this->erp_languages_model->get();

        $data['tovalues'] = $this->erp_languages_model->get_file_lang($tolang, $file_id);
        $this->erp_languages_model->erp_language_id = $tolang;
        $data['tolang'] = $this->erp_languages_model->get();

        $this->load->view('languages/row', $data);
    }

    public function fetch_lang() {
        $this->load->helper('directory');
        /*
          $this->db->truncate('safa_languages_files');
          $this->db->truncate('safa_language_keys');
          $this->db->truncate('safa_languages_phrases');
         */
        $language_files = directory_map('./application/language/arabic/');

        foreach ($language_files as $key => $file) {
            if (is_array($file)) {
                $key = str_replace('\\', '/', $key);
                foreach ($file as $filename) {
                    $filetbname = $key . str_replace('_lang.php', '', $filename);
                    if ($table = $this->db->where('file_name', $filetbname)->get('safa_languages_files')->row()) {
                        $file_id = $table->safa_file_id;
                    } else {
                        $this->db->set('file_name', $filetbname)->insert('safa_languages_files');
                        $file_id = $this->db->insert_id();
                    }
                    $filerows = $this->lang->load($filetbname, '', TRUE);
                    foreach ($filerows as $rowkey => $rowvalue) {
                        $rowarray = array(
                            'safa_languages_file_id' => $file_id,
                            'safa_language_key_name' => $rowkey
                        );
                        if (!$this->db->where($rowarray)->from('safa_language_keys')->count_all_results()) {
                            $this->db->insert('safa_language_keys', $rowarray);
                            $rowid = $this->db->insert_id();
                            $phrasesrow = array(
                                'safa_language_key_id' => $rowid,
                                'phrase' => $rowvalue,
                                'safa_language_id' => 2
                            );
                            $this->db->insert('safa_languages_phrases', $phrasesrow);
                        }
                    }
                }
            } else {
                $filetbname = str_replace('_lang.php', '', $file);
                if ($table = $this->db->where('file_name', $filetbname)->get('safa_languages_files')->row()) {
                    $file_id = $table->safa_file_id;
                } else {
                    $this->db->set('file_name', $filetbname)->insert('safa_languages_files');
                    $file_id = $this->db->insert_id();
                }

                $filerows = $this->lang->load($filetbname, '', TRUE);
                foreach ($filerows as $rowkey => $rowvalue) {
                    $rowarray = array(
                        'safa_languages_file_id' => $file_id,
                        'safa_language_key_name' => $rowkey
                    );
                    if (!$this->db->where($rowarray)->from('safa_language_keys')->count_all_results()) {
                        $this->db->insert('safa_language_keys', $rowarray);
                        $rowid = $this->db->insert_id();
                        $phrasesrow = array(
                            'safa_language_key_id' => $rowid,
                            'phrase' => $rowvalue,
                            'safa_language_id' => 2
                        );
                        $this->db->insert('safa_languages_phrases', $phrasesrow);
                    }
                }
            }
        }
        die('done');
    }

    public function add_row() {
        $this->layout = 'ajax';

        if ($this->input->post()) {
            $data['existkeys'] = '';
            $file_id = $this->input->post('file_id');
            $filerows = $this->input->post('key');
            $filevalues = $this->input->post('value');
            foreach ($filerows as $keyid => $keyvalue) {
                $rowarray = array(
                    'safa_languages_file_id' => $file_id,
                    'safa_language_key_name' => $keyvalue
                );
                if (!$this->db->where($rowarray)->from('safa_language_keys')->count_all_results()) {
                    $this->db->insert('safa_language_keys', $rowarray);
                    $add_key = $this->db->last_query();
                    echo $add_key.";\n\r"."<br />";
                    $rowid = $this->db->insert_id();
                    $phrasesrow = array(
                        'safa_language_key_id' => $rowid,
                        'phrase' => $filevalues[$keyid],
                        'safa_language_id' => 2
                    );
                    $this->db->insert('safa_languages_phrases', $phrasesrow);
                    $add_prase = $this->db->last_query();
                    $add_prase = str_replace($rowid, 'LAST_INSERT_ID()', $add_prase);
                    echo $add_prase.";\n\r"."<br />/* ---------------------------------------------------- */<br />";
                } else {
                    $data['existkeys'] .= ' , ' . $rowvalue;
                }
            }
        }


        $data['files'] = array();
        $files = $this->safa_languages_files_model->get();
        foreach ($files as $file)
            $data['files'][$file->safa_file_id] = $file->file_name;

        $this->load->view('languages/newrow', $data);
    }

}
