<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Sql_queries extends Safa_Controller {


    public function __construct() {
        parent::__construct();
    }

    function index() {
        //$columns_rows = $this->db->get('columns')->result();
        //$tables_rows = $this->db->get('tables')->result();
        $unknowntable_rows = $this->db->get('unknowntable')->result();
    	
//        foreach ($tables_rows as $tables_row) {
//        	
//        	$newperiod = array(
//			'table_name' => $tables_row->id,
//			);
//									
//        	$this->db->where('table_name', $tables_row->name)->update('columns', $newperiod);
//        }

        
    foreach ($unknowntable_rows as $unknowntable_row) {
        	
    		$pk_field_row = $this->db->select('fields.id as field_id')->join('tables','tables.id=fields.tables_id')->where('tables.name', $unknowntable_row->pk_table)->where('fields.name', $unknowntable_row->pk_field_id)->get('fields')->result();
    		$pk_field_id = $pk_field_row[0]->field_id;
    		
    		$fk_field_row = $this->db->select('fields.id as field_id')->join('tables','tables.id=fields.tables_id')->where('tables.name', $unknowntable_row->fk_table)->where('fields.name', $unknowntable_row->fk_field_id)->get('fields')->result();
    		$fk_field_id = $fk_field_row[0]->field_id;
    		
    		$relation_on_delete = $unknowntable_row->relation_on_delete;
    		$relation_on_update = $unknowntable_row->relation_on_update;
    		
    	
        	$relations_arr = array(
			'pk_field_id' => $pk_field_id,
        	'fk_field_id' => $fk_field_id,
        	'relation_on_delete' => $relation_on_delete,
        	'relation_on_update' => $relation_on_update,
			);
									
        	$this->db->insert('tables_relations', $relations_arr);
        }
    }
    
    function ea_ports()
    {
    	$eas_rows = $this->db->from('safa_eas')->where('erp_country_id', 20)->get()->result();
    	$ports_arr = array(6070,6056,6053,2733,2729,2739,2731,2730,2741,2745,2727);
    		
    	foreach ($eas_rows as $eas_row) {
        	
    		foreach ($ports_arr as $port_id) {
	    		$row = $this->db->select('*')->from('safa_ea_ports')->where('safa_ea_id', $eas_row->safa_ea_id)->where('erp_port_id', $port_id)->get()->result();
	    		
	    		if(count($row)==0) {
	        	$values_arr = array(
				'safa_ea_id' => $eas_row->safa_ea_id,
	        	'erp_port_id' => $port_id,
				);
										
	        	$this->db->insert('safa_ea_ports', $values_arr);
	    		}
    		}
        }
    }
    
	function uo_contracts()
    {
    	$uos_rows = $this->db->from('safa_uos')->get()->result();
    		
    	foreach ($uos_rows as $uos_row) {
        		$uo_counter=001;
	    		$contract_rows = $this->db->select('*')->from('safa_uo_contracts')->where('safa_uo_id', $uos_row->safa_uo_id)->get()->result();
	    		foreach ($contract_rows as $contract_row) {
		        	
					$this->db->set('contract_username', substr($uos_row->name_la,0,4).'_'.$uo_counter);
                	$this->db->set('contract_password', 'e10adc3949ba59abbe56e057f20f883e');
											
		        	$this->db->where('safa_uo_contract_id',$contract_row->safa_uo_contract_id)->update('safa_uo_contracts');

		        	$uo_counter++;
	    		}
        }
    }
    

}
