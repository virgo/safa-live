<?php

if (!defined('BASEPATH'))
exit('No direct script access allowed');

class Contracts extends Safa_Controller {

	public $module = "contracts";

	public function __construct() {
		parent::__construct();
		$this->layout = 'new';
		//Side menu session, By Gouda.
		session('side_menu_id', 1);
		$this->load->model('contracts_model');
		$this->load->model('uo_contracts_ea_model');
		$this->load->model('eas_model');
		$this->load->model('ea_seasons_model');
		$this->load->model('ea_users_model');
		$this->load->model('ea_hotels_model');
		$this->load->model('currencies_model');
		$this->load->model('transporters_model');
		$this->load->model('contracts_settings_model', 'setting');
		$this->load->model('contracts_settings_visa_prices_model', 'visa_price');
		#to make search in hotels
		$this->load->model('itos_model');
		$this->load->model('uo_contracts_itos_model');
		#to make hotels in contract
		$this->load->model('hotels_model');
		$this->load->model('uo_contracts_hotels_model');
		//By Gouda.
		$this->load->model('erp_languages_model');
		$this->load->model('countries_model');
		$this->load->helper('db_helper');
		$this->lang->load('contracts');

		permission();
		if (session('uo_id'))
		$this->contracts_model->safa_uo_id = session("uo_id");
	}

	public function index() {
		$data['user_id'] = session('users_id');
		$data['module_name'] = $this->module;
		if (isset($_GET['search']))
		$this->search();
		$data["total_rows"] = $this->contracts_model->get(true);
		$this->contracts_model->offset = $this->uri->segment("4");
		//$this->contracts_model->limit = $this->config->item('per_page');
		//$this->contracts_model->limit = 1000;
		$data["items"] = $this->contracts_model->get();
		$config['uri_segment'] = 4;
		$config['base_url'] = site_url('uo/contracts/index');
		$config['total_rows'] = $data["total_rows"];
		$config['per_page'] = $this->config->item('per_page');
		$config['suffix'] = $this->input->get() ? '?' . http_build_query($_GET) : NULL;
		$this->load->library('pagination');
		$this->pagination->initialize($config);
		$data['pagination'] = $this->pagination->create_links();
		$this->load->view('uo/contracts/index', $data);
	}

	public function add() {

		//By Gouda.------
		$data['safa_uo_contracts_status'] = ddgen('safa_uo_contracts_status', array('safa_uo_contracts_status_id', name()));
		$data['safa_contract_phases'] = ddgen('safa_contract_phases', array('safa_contract_phases_id', name()), array('safa_uo_id' => session('uo_id')));
		//---------------
		//        if (!permission('uo_contracts_add'))
		//                    show_message(lang('global_you_dont_have_permission'));
		$this->load->library("form_validation");
		$this->form_validation->set_rules('contract_name_ar', 'lang:contracts_name_ar', 'trim');
		$this->form_validation->set_rules('contract_name_la', 'lang:contracts_name_la', 'trim');
		if (name() == "name_la")
		$this->form_validation->set_rules('contract_name_la', 'lang:contracts_name_la', 'trim|required');
		if (name() == "name_ar")
		$this->form_validation->set_rules('contract_name_ar', 'lang:contracts_name_ar', 'trim|required');

		$this->form_validation->set_rules('contarct_address', 'lang:contracts_address', 'trim');
		$this->form_validation->set_rules('contarct_phone', 'lang:contracts_phone', 'trim|required');
		$this->form_validation->set_rules('contarct_ksa_address', 'lang:contracts_ksaaddress', 'trim');
		$this->form_validation->set_rules('contarct_ayata_num', 'lang:contracts_ayata_num', 'trim');
		$this->form_validation->set_rules('contarct_agency_symbol', 'lang:contracts_agencysymbol', 'trim|required');
		$this->form_validation->set_rules('contarct_agency_name', 'lang:contracts_agencyname', 'trim|required');
		$this->form_validation->set_rules('contarct_email', 'lang:contracts_email', 'trim|required|valid_email|unique_col[safa_uo_contracts.email]');
		$this->form_validation->set_rules('contarct_country', 'lang:contracts_country', 'trim|required|is_natural_no_zero');
		$this->form_validation->set_rules('contarct_fax', 'lang:contracts_fax', 'trim');
		$this->form_validation->set_rules('contarct_nationality', 'lang:contracts_nationality', 'trim|required|is_natural_no_zero');
		$this->form_validation->set_rules('contarct_username', 'lang:contracts_username', 'trim|required|alpha_dash|unique_col[safa_uo_contracts.contract_username]'); // to do again to be contained

		$this->form_validation->set_rules('contarct_password', 'lang:contracts_password', 'trim|required');
		$this->form_validation->set_rules('contarct_confpassword', 'lang:contracts_repeat_password', 'required|trim|matches[contarct_password]');
		$this->form_validation->set_rules('uasp_username', 'lang:uasp_username', 'trim');
		$this->form_validation->set_rules('uasp_password', 'lang:uasp_password', 'trim');
		$this->form_validation->set_rules('uasp_eacode', 'lang:uasp_eacode', 'trim');
		$this->form_validation->set_rules('safa_uo_contracts_status_id', 'lang:safa_uo_contracts_status_id', 'trim');

		$this->form_validation->set_rules('contarct_responsible_name', 'lang:contracts_resposible_name', 'trim');
		$this->form_validation->set_rules('contarct_responsible_phone', 'lang:contracts_resposible_phone', 'trim');
		$this->form_validation->set_rules('contarct_responsible_email', 'lang:contracts_resposible_email', 'trim');

		$this->form_validation->set_rules('contract_notes', '', 'trim');
		$this->form_validation->set_rules('visa_price[]', 'lang:contracts_visa_price', 'trim|required');
		$this->form_validation->set_rules('visa_price_currency_id[]', 'lang:contracts_currency', 'trim|required');
		//$this->form_validation->set_rules('credit_balance', 'lang:contracts_credit_balance', 'trim|required');
		//$this->form_validation->set_rules('credit_balance_currency_id', 'lang:contracts_currency', 'trim|required');

		$this->form_validation->set_rules('services', '', 'trim|callback_sevices_check');
		if ($this->form_validation->run() == false) {
			$currencies = $this->currencies_model->get();
			$data["currencies"] = array('' => '');
			foreach ($currencies as $currency) {
				$data["currencies"][$currency->erp_currency_id] = $currency->symbol;
			}

			$this->transporters_model->erp_transportertype_id = 1;
			$transporters = $this->transporters_model->get();
			$data["transporters"] = array('' => '');
			foreach ($transporters as $transporter) {
				$data["transporters"][$transporter->safa_transporter_id] = $transporter->name_ar;
			}

			$this->load->view('uo/contracts/add', $data);
		} else {

			$now_timestamp = strtotime(date('Y-m-d H:i', time()));
			$allowedExts = array("gif", "jpeg", "jpg", "png");
			$temp = explode(".", $_FILES["responsible_passport_photo_path"]["name"]);
			$extension = end($temp);
			if ((($_FILES["responsible_passport_photo_path"]["type"] == "image/gif") || ($_FILES["responsible_passport_photo_path"]["type"] == "image/jpeg") || ($_FILES["responsible_passport_photo_path"]["type"] == "image/jpg") || ($_FILES["responsible_passport_photo_path"]["type"] == "image/pjpeg") || ($_FILES["responsible_passport_photo_path"]["type"] == "image/x-png") || ($_FILES["responsible_passport_photo_path"]["type"] == "image/png"))
			//&& ($_FILES["responsible_passport_photo_path"]["size"] < 20000)
			&& in_array($extension, $allowedExts)) {

				if ($_FILES["responsible_passport_photo_path"]["error"] > 0) {
					echo "Return Code: " . $_FILES["responsible_passport_photo_path"]["error"] . "<br>";
				} else {

					/*
					 echo "Upload: " . $_FILES["file"]["name"] . "<br>";
					 echo "Type: " . $_FILES["file"]["type"] . "<br>";
					 echo "Size: " . ($_FILES["file"]["size"] / 1024) . " kB<br>";
					 echo "Temp file: " . $_FILES["file"]["tmp_name"] . "<br>";

					 if (file_exists("upload/" . $_FILES["file"]["name"]))
					 {
					 echo $_FILES["file"]["name"] . " already exists. ";
					 }
					 else
					 {


					 move_uploaded_file($_FILES["responsible_passport_photo_path"]["tmp_name"],
					 "upload/" . $_FILES["responsible_passport_photo_path"]["name"]);
					 echo "Stored in: " . "upload/" . $_FILES["responsible_passport_photo_path"]["name"];
					 }
					 */

					move_uploaded_file($_FILES["responsible_passport_photo_path"]["tmp_name"], "./static/uploads/passports_photos/" . $now_timestamp . '_' . $_FILES["responsible_passport_photo_path"]["name"]);

					$this->contracts_model->responsible_passport_photo_path = $now_timestamp . '_' . $_FILES["responsible_passport_photo_path"]["name"];
				}
			} else {
				//echo "Invalid file";
			}

			$this->contracts_model->name_ar = $this->input->post("contract_name_ar");
			$this->contracts_model->name_la = $this->input->post("contract_name_la");
			$this->contracts_model->contract_username = $this->input->post('contarct_username');
			if ($this->input->post('contarct_password'))
			$this->contracts_model->contract_password = md5($this->input->post('contarct_password'));

			$this->contracts_model->address = $this->input->post('contarct_address');

			//By Gouda
			$contarct_phone = preg_replace('/^00/', '', $this->input->post('contarct_phone'));
			//$contarct_phone = preg_replace('/^+/', '', $this->input->post('contarct_phone'));
			$this->contracts_model->phone = $contarct_phone;


			$this->contracts_model->uasp_username = $this->input->post('uasp_username');
			$this->contracts_model->uasp_password = $this->input->post('uasp_password');
			$this->contracts_model->uasp_eacode = $this->input->post('uasp_eacode');

			$this->contracts_model->ksa_address = $this->input->post('contarct_ksa_address');
			$this->contracts_model->iata = $this->input->post('contarct_ayata_num');

			//By Gouda
			$contarct_ksa_phone = preg_replace('/^00/', '', $this->input->post('contarct_ksa_phone'));
			//$contarct_ksa_phone = preg_replace('/^+/', '', $this->input->post('contarct_ksa_phone'));
			$this->contracts_model->ksa_phone = $contarct_ksa_phone;

			$this->contracts_model->agency_symbol = $this->input->post('contarct_agency_symbol');
			$this->contracts_model->agency_name = $this->input->post('contarct_agency_name');
			$this->contracts_model->email = $this->input->post('contarct_email');

			//By Gouda. --------
			$this->contracts_model->safa_uo_contracts_status_id = $this->input->post('safa_uo_contracts_status_id');
			//$this->contracts_model->safa_contract_phases_id_current = $this->input->post('safa_contract_phases_id_current');
			//$this->contracts_model->safa_contract_phases_id_next = $this->input->post('safa_contract_phases_id_next');
			//------------------


			$this->contracts_model->country_id = $this->input->post('contarct_country');

			//By Gouda
			$contarct_fax = preg_replace('/^00/', '', $this->input->post('contarct_fax'));
			//$contarct_fax = preg_replace('/^+/', '', $this->input->post('contarct_fax'));
			$this->contracts_model->fax = $contarct_fax;

			$this->contracts_model->responsible_name = $this->input->post('contarct_responsible_name');
			$this->contracts_model->nationality_id = $this->input->post('contarct_nationality');

			//By Gouda
			$contarct_responsible_phone = preg_replace('/^00/', '', $this->input->post('contarct_responsible_phone'));
			//$contarct_responsible_phone = preg_replace('/^+/', '', $this->input->post('contarct_responsible_phone'));
			$this->contracts_model->responsible_phone = $contarct_responsible_phone;

			$this->contracts_model->responsible_email = $this->input->post('contarct_responsible_email');

			$this->contracts_model->notes = $this->input->post('contract_notes');
			$contract_id = $this->contracts_model->save();
			if ($contract_id > 0) {
				/* ADD IN THE HOTELS FOR THE CONTRACT */
				$this->add_hotels($contract_id);
				/* add tourismplaces */
				$this->add_tourismplaces($contract_id);
				/* add_itos */
				$this->add_itos($contract_id);
				/* add_ea_itos */
				$this->add_ea_itos($contract_id);
				/* add_defualt_umrah_package */
				$this->add_default_umrahpackage($contract_id);


				$this->setting->safa_uo_contract_id = $contract_id;

				if ($this->input->post('services_visa'))
				$this->setting->services_buy_uo_visa = $this->input->post('services_visa');
				if ($this->input->post('services_hostel'))
				$this->setting->services_buy_uo_hotels = $this->input->post('services_hostel');
				if ($this->input->post('services_travel'))
				$this->setting->services_buy_uo_transporter = $this->input->post('services_travel');
				if ($this->input->post('out_services_hostel'))
				$this->setting->services_buy_other_hotels = $this->input->post('out_services_hostel');
				if ($this->input->post('out_services_travel'))
				$this->setting->services_buy_other_transporter = $this->input->post('out_services_travel');
				if ($this->input->post('out_services_other_services'))
				$this->setting->services_buy_other_services = $this->input->post('out_services_other_services');

				$this->setting->transport_cycle_from = $this->input->post('transport_cycle_from');
				if ($this->input->post('transport_cycle_from') == 2) {
					$this->setting->transporters_us = $this->input->post('transporters_us');
				}

				if ($this->input->post('can_use_other_uo'))
				$this->setting->can_use_other_uo = $this->input->post('can_use_other_uo');
				if ($this->input->post('can_use_hotel_marking'))
				$this->setting->can_use_hotel_marking = 1;
				if ($this->input->post('can_book_couch'))
				$this->setting->can_book_couch = $this->input->post('can_book_couch');


				$this->setting->max_limit_visa = $this->input->post('max_limit_visa');

				$this->setting->credit_balance = $this->input->post('credit_balance');
				$this->setting->credit_balance_currency_id = $this->input->post('credit_balance_currency_id');

				if ($this->input->post('can_over_limit'))
				$this->setting->can_over_limit = $this->input->post('can_over_limit');
				else
				$this->setting->can_over_limit = '0';

					
				if ($this->input->post('add_passports_accomodation_without_visa'))
				$this->setting->add_passports_accomodation_without_visa = $this->input->post('add_passports_accomodation_without_visa');
				else
				$this->setting->add_passports_accomodation_without_visa = '0';

				$setting_id = $this->setting->save();

				if ($setting_id && ($this->input->post('transporters_us') == 'list')) {
					$transporterslist = $this->input->post('transporters_us_values');
					if (is_array($transporterslist) && count($transporterslist)) {
						$this->load->model('contracts_settings_transporters_model', 'transporter');
						foreach ($transporterslist as $transporterid) {
							$this->transporter->safa_uo_contract_id = $contract_id;
							$this->transporter->safa_uo_contract_setting_id = $setting_id;
							$this->transporter->safa_transporter_id = $transporterid;
							$this->transporter->save();
						}
					}
				}

				$visa_prices = $this->input->post('visa_price');
				$fromdate = $this->input->post('visa_date_from');
				$todate = $this->input->post('visa_date_to');
				$currency_ids = $this->input->post('visa_price_currency_id');

				if (is_array($visa_prices) && count($visa_prices)) {
					foreach ($visa_prices as $vkey => $price) {
						if ($price) {
							$this->visa_price->safa_uo_contract_id = $contract_id;
							$this->visa_price->safa_uo_contract_setting_id = $setting_id;
							$this->visa_price->price = $price;
							$this->visa_price->erp_currency_id = $currency_ids[$vkey];
							$this->visa_price->fromdate = $fromdate[$vkey];
							$this->visa_price->todate = $todate[$vkey];
							$this->visa_price->save();
						}
					}
				}



				// Added By Gouda, To send email with contract login data to EA email.

				$subject = lang('new_contract_data_email_subject');

				$username = $this->input->post('contarct_username');
				$password = $this->input->post('contarct_password');

				$_SESSION['sesssion_contarct_password'] = 	$password;

				$link = site_url('ea/login/');

				// By Gouda.
				/*
				 $message = str_replace('{link}', $link, lang('new_contract_data_email_body'));
				 $message = str_replace('{username}', $username, $message);
				 $message = str_replace('{password}', $password, $message);
				 */



				// Get Message Body From XML.
				$this->erp_languages_model->erp_language_id = 2;
				$erp_languages_row = $this->erp_languages_model->get();
				$lang_suffix = $erp_languages_row->suffix;
				if (file_exists("./static/xml/notifications/notifications_$lang_suffix.xml")) {
					$event_key = 18;
					$xml = new DOMDocument('1.0', 'utf-8');
					/**
					 * Added to format the xml code, when open with editor.
					 */
					$xml->formatOutput = true;
					$xml->preserveWhiteSpace = false;

					$xml->Load("./static/xml/notifications/notifications_$lang_suffix.xml");
					/**
					 *
					 * Check if this event inserted before
					 */
					$event_key_exist = false;
					$notifications = $xml->getElementsByTagName('notification');
					foreach ($notifications as $notification) {
						$loop_current_key = $notification->getElementsByTagName('key');
						$loop_current_key_value = $loop_current_key->item(0)->nodeValue;


						if ($loop_current_key_value == $event_key) {
							$event_key_exist = true;

							$loop_current_subject = $notification->getElementsByTagName('subject');
							$loop_current_subject_value = $loop_current_subject->item(0)->nodeValue;
							$subject = $loop_current_subject_value;

							$loop_current_body = $notification->getElementsByTagName('body');
							$loop_current_body_value = $loop_current_body->item(0)->nodeValue;

							//Not remove Username and password.
							$message = $message . '<br\>' . $loop_current_body_value . '<br/>';
						}
					}
				}

				$this->eas_model->email = $this->input->post('contarct_email');
				$eas_rows = $this->eas_model->get();
				if(count($eas_rows)>0) {

					$this->load->view('confirmation_message', array('msg' => lang('there_is_ea_registered_with_email_are_you_add_another'),
                    'back_url' => site_url('uo/contracts'),
                    'confirmation_url' => site_url('uo/contracts/add_another_ea/' . $contract_id ),
                    'model_title' => lang('node_title'), 'action' => lang('contracts_add'),
					));

				} else {
					$ea_id = $this->add_externalagents();
					if ($ea_id > 0) {
						$this->add_ea_user($ea_id);
						$ea_contract_id = $this->add_ea_contract($contract_id, $ea_id);
					}

					$this->load->view('redirect_message', array('msg' => lang('global_added_message'),
		            'url' => site_url('uo/contracts'),
		            'model_title' => lang('node_title'), 'action' => lang('contracts_add')));
				}

				$this->send_email($contract_id,'add');

				//                $this->load->view('confirmation_message', array('msg' => lang('are_you_want_to_send_contract_data_email'),
				//                    'back_url' => site_url('uo/contracts'),
				//                    'confirmation_url' => site_url('uo/contracts/send_email/' . $contract_id . '/add'),
				//                    'model_title' => lang('node_title'), 'action' => lang('contracts_add'),
				//                ));


					
			}

		}
	}

	public function edit($id = false) {

		//By Gouda.------
		$data['safa_uo_contracts_status'] = ddgen('safa_uo_contracts_status', array('safa_uo_contracts_status_id', name()));
		$data['safa_contract_phases'] = ddgen('safa_contract_phases', array('safa_contract_phases_id', name()), array('safa_uo_id' => session('uo_id')));
		//echo session('uo_id'); exit;
		//---------------
		//       if (!permission('uo_contracts_edit'))
		//                    show_message(lang('global_you_dont_have_permission'));
		if (!$id)
		show_404();
		$this->contracts_model->safa_uo_contract_id = $id;
		$data['item'] = $this->contracts_model->get();

		if (!$data['item'])
		show_404();

		$this->setting->safa_uo_contract_id = $id;
		$data['setting'] = $this->setting->get();
		if (!$data['setting']) {
			$this->setting->safa_uo_contract_id = $id;
			$this->setting->save();
			$this->setting->safa_uo_contract_id = $id;
			$data['setting'] = $this->setting->get();
		}

		if ($data['setting']->transporters_us == 'list') {
			$this->load->model('contracts_settings_transporters_model', 'transporter');
			$this->transporter->safa_uo_contract_id = $id;
			$this->transporter->safa_uo_contract_setting_id = $data['setting']->safa_uo_contract_setting_id;
			$transporters = $this->transporter->get();
			$data['list_transporter'] = '[';
			foreach ($transporters as $transporter) {
				$data['list_transporter'] .= '"' . $transporter->safa_transporter_id . '",';
			}
			$data['list_transporter'] .= ']';
		} else
		$data['list_transporter'] = '[]';

		$this->load->library("form_validation");
		$this->form_validation->set_rules('contract_name_ar', 'lang:contracts_name_ar', 'trim');
		$this->form_validation->set_rules('contract_name_la', 'lang:contracts_name_la', 'trim');
		if (name() == "name_la")
		$this->form_validation->set_rules('contract_name_la', 'lang:contracts_name_la', 'trim|required');
		if (name() == "name_ar")
		$this->form_validation->set_rules('contract_name_ar', 'lang:contracts_name_ar', 'trim|required');
		$this->form_validation->set_rules('uasp_username', 'lang:uasp_username', 'trim');
		$this->form_validation->set_rules('uasp_password', 'lang:uasp_password', 'trim');
		$this->form_validation->set_rules('uasp_eacode', 'lang:uasp_eacode', 'trim');
		$this->form_validation->set_rules('contarct_address', 'lang:contracts_address', 'trim');
		$this->form_validation->set_rules('contarct_phone', 'lang:contracts_phone', 'trim|required');
		$this->form_validation->set_rules('contarct_ksa_address', 'lang:contracts_ksaaddress', 'trim');
		$this->form_validation->set_rules('contarct_ayata_num', 'lang:contracts_ayata_num', 'trim');
		$this->form_validation->set_rules('contarct_agency_symbol', 'lang:contracts_agencysymbol', 'trim|required');
		$this->form_validation->set_rules('contarct_agency_name', 'lang:contracts_agencyname', 'trim|required');
		$this->form_validation->set_rules('contarct_email', 'lang:contracts_email', 'trim|required|valid_email');
		$this->form_validation->set_rules('contarct_country', 'lang:contracts_country', 'trim|required|is_natural_no_zero');
		$this->form_validation->set_rules('contarct_fax', 'lang:contracts_fax', 'trim');
		$this->form_validation->set_rules('contarct_nationality', 'lang:contracts_nationality', 'trim|required|is_natural_no_zero');
		$this->form_validation->set_rules('contarct_username', 'lang:contracts_username', 'trim|required|alpha_dash|unique_col[safa_uo_contracts.contract_username.' . $id . ']'); // to do again to be contained
		$this->form_validation->set_rules('contarct_password', 'lang:contracts_password', 'trim');
		$this->form_validation->set_rules('contarct_confpassword', 'lang:contracts_repeat_password', 'trim|matches[contarct_password]');

		$this->form_validation->set_rules('contarct_responsible_name', 'lang:contracts_resposible_name', 'trim');
		$this->form_validation->set_rules('contarct_responsible_phone', 'lang:contracts_resposible_phone', 'trim');
		$this->form_validation->set_rules('contarct_responsible_email', 'lang:contracts_resposible_email', 'trim');

		$this->form_validation->set_rules('contract_notes', '', 'trim');

		$this->form_validation->set_rules('safa_uo_contracts_status_id', 'lang:safa_uo_contracts_status_id', 'trim');

		$this->form_validation->set_rules('visa_price[]', 'lang:contracts_visa_price', 'trim|required');
		$this->form_validation->set_rules('visa_price_currency_id[]', 'lang:contracts_currency', 'trim|required');
		//$this->form_validation->set_rules('credit_balance', 'lang:contracts_credit_balance', 'trim|required');
		//$this->form_validation->set_rules('credit_balance_currency_id', 'lang:contracts_currency', 'trim|required');
		$this->form_validation->set_rules('services', '', 'trim|callback_sevices_check');
		if ($this->form_validation->run() == false) {
			$currencies = $this->currencies_model->get();
			$data["currencies"] = array('' => '');
			foreach ($currencies as $currency) {
				$data["currencies"][$currency->erp_currency_id] = $currency->symbol;
			}

			$this->transporters_model->erp_transportertype_id = 1;
			$transporters = $this->transporters_model->get();
			$data["transporters"] = array('' => '');
			foreach ($transporters as $transporter) {
				$data["transporters"][$transporter->safa_transporter_id] = $transporter->name_ar;
			}

			$this->visa_price->safa_uo_contract_id = $id;
			$this->visa_price->safa_uo_contract_setting_id = $data['setting']->safa_uo_contract_setting_id;
			$data['prices'] = $this->visa_price->get();

			$this->load->view('uo/contracts/edit', $data);
		} else {

			$now_timestamp = strtotime(date('Y-m-d H:i', time()));
			$allowedExts = array("gif", "jpeg", "jpg", "png");
			$temp = explode(".", $_FILES["responsible_passport_photo_path"]["name"]);
			$extension = end($temp);
			if ((($_FILES["responsible_passport_photo_path"]["type"] == "image/gif") || ($_FILES["responsible_passport_photo_path"]["type"] == "image/jpeg") || ($_FILES["responsible_passport_photo_path"]["type"] == "image/jpg") || ($_FILES["responsible_passport_photo_path"]["type"] == "image/pjpeg") || ($_FILES["responsible_passport_photo_path"]["type"] == "image/x-png") || ($_FILES["responsible_passport_photo_path"]["type"] == "image/png"))
			//&& ($_FILES["responsible_passport_photo_path"]["size"] < 20000)
			&& in_array($extension, $allowedExts)) {

				if ($_FILES["responsible_passport_photo_path"]["error"] > 0) {
					echo "Return Code: " . $_FILES["responsible_passport_photo_path"]["error"] . "<br>";
				} else {

					/*
					 echo "Upload: " . $_FILES["file"]["name"] . "<br>";
					 echo "Type: " . $_FILES["file"]["type"] . "<br>";
					 echo "Size: " . ($_FILES["file"]["size"] / 1024) . " kB<br>";
					 echo "Temp file: " . $_FILES["file"]["tmp_name"] . "<br>";

					 if (file_exists("upload/" . $_FILES["file"]["name"]))
					 {
					 echo $_FILES["file"]["name"] . " already exists. ";
					 }
					 else
					 {


					 move_uploaded_file($_FILES["responsible_passport_photo_path"]["tmp_name"],
					 "upload/" . $_FILES["responsible_passport_photo_path"]["name"]);
					 echo "Stored in: " . "upload/" . $_FILES["responsible_passport_photo_path"]["name"];
					 }
					 */

					if (file_exists('./static/uploads/passports_photos/' . $data['item']->responsible_passport_photo_path)) {
						unlink('./static/uploads/passports_photos/' . $data['item']->responsible_passport_photo_path);
					}

					move_uploaded_file($_FILES["responsible_passport_photo_path"]["tmp_name"], "./static/uploads/passports_photos/" . $now_timestamp . '_' . $_FILES["responsible_passport_photo_path"]["name"]);

					$this->contracts_model->responsible_passport_photo_path = $now_timestamp . '_' . $_FILES["responsible_passport_photo_path"]["name"];
				}
			} else {
				//echo "Invalid file";
			}

			$this->contracts_model->name_ar = $this->input->post("contract_name_ar");
			$this->contracts_model->name_la = $this->input->post("contract_name_la");
			$this->contracts_model->contract_username = $this->input->post('contarct_username');

			if ($this->input->post('contarct_password')) {
				$this->contracts_model->contract_password = md5($this->input->post('contarct_password'));

				$this->contracts_model->safa_uo_contract_id = $id;
				$this->contracts_model->uo_contracts_eas_disabled = 1;
				$this->contracts_model->disable_contract_ea_by_contract_id();

				// Added By Gouda, To send email with contract login data to EA email.

				$subject = lang('new_contract_data_email_subject');

				$username = $this->input->post('contarct_username');
				$password = $this->input->post('contarct_password');

				$link = site_url('ea/login/');
				$message = str_replace('{link}', $link, lang('new_contract_data_email_body'));
				$message = str_replace('{username}', $username, $message);
				$message = str_replace('{password}', $password, $message);

				// Get Message Body From XML.
				$this->erp_languages_model->erp_language_id = 2;
				$erp_languages_row = $this->erp_languages_model->get();
				$lang_suffix = $erp_languages_row->suffix;
				if (file_exists("./static/xml/notifications/notifications_$lang_suffix.xml")) {
					$event_key = 18;
					$xml = new DOMDocument('1.0', 'utf-8');
					/**
					 * Added to format the xml code, when open with editor.
					 */
					$xml->formatOutput = true;
					$xml->preserveWhiteSpace = false;

					$xml->Load("./static/xml/notifications/notifications_$lang_suffix.xml");
					/**
					 *
					 * Check if this event inserted before
					 */
					$event_key_exist = false;
					$notifications = $xml->getElementsByTagName('notification');
					foreach ($notifications as $notification) {
						$loop_current_key = $notification->getElementsByTagName('key');
						$loop_current_key_value = $loop_current_key->item(0)->nodeValue;


						if ($loop_current_key_value == $event_key) {
							$event_key_exist = true;

							$loop_current_subject = $notification->getElementsByTagName('subject');
							$loop_current_subject_value = $loop_current_subject->item(0)->nodeValue;
							$subject = $loop_current_subject_value;

							$loop_current_body = $notification->getElementsByTagName('body');
							$loop_current_body_value = $loop_current_body->item(0)->nodeValue;

							//Not remove Username and password.
							$message = $message . '<br\>' . $loop_current_body_value . '<br/>';
						}
					}
				}


				/*
				 $this->load->library('email');

				 $this->email->from(config('webmaster_email'), config('title'));
				 $this->email->to($this->input->post('contarct_email'));


				 $this->email->subject($subject);
				 $this->email->message($message);

				 $this->email->attach('./static/new_template/contract_email_form/img/icon_02_facebook.png', 'inline');
				 $this->email->attach('./static/new_template/contract_email_form/img/icon_02_linkedin.png', 'inline');
				 $this->email->attach('./static/new_template/contract_email_form/img/icon_02_twitter.png', 'inline');
				 $this->email->attach(getcwd() . '/static/new_template/contract_email_form/img/Safa-ContractMail-Final_03.png', 'inline');
				 $this->email->attach(getcwd() . '/static/new_template/contract_email_form/img/Safa-ContractMail-Final_07.png', 'inline');
				 $this->email->attach(getcwd() . '/static/new_template/contract_email_form/img/Safa-ContractMail-Final_11.png', 'inline');

				 $sending_email_result = $this->email->send();
				 //echo $this->email->print_debugger(); exit;
				 */
			}
			$this->contracts_model->uasp_username = $this->input->post('uasp_username');
			$this->contracts_model->uasp_password = $this->input->post('uasp_password');
			$this->contracts_model->uasp_eacode = $this->input->post('uasp_eacode');

			$this->contracts_model->address = $this->input->post('contarct_address');

			//By Gouda
			$contarct_phone = preg_replace('/^00/', '', $this->input->post('contarct_phone'));
			//$contarct_phone = preg_replace('/^+/', '', $this->input->post('contarct_phone'));
			$this->contracts_model->phone = $contarct_phone;

			$this->contracts_model->ksa_address = $this->input->post('contarct_ksa_address');
			$this->contracts_model->iata = $this->input->post('contarct_ayata_num');

			//By Gouda
			$contarct_ksa_phone = preg_replace('/^00/', '', $this->input->post('contarct_ksa_phone'));
			//$contarct_ksa_phone = preg_replace('/^+/', '', $this->input->post('contarct_ksa_phone'));
			$this->contracts_model->ksa_phone = $contarct_ksa_phone;

			$this->contracts_model->agency_symbol = $this->input->post('contarct_agency_symbol');
			$this->contracts_model->agency_name = $this->input->post('contarct_agency_name');
			$this->contracts_model->email = $this->input->post('contarct_email');

			//By Gouda. --------
			$this->contracts_model->safa_uo_contracts_status_id = $this->input->post('safa_uo_contracts_status_id');
			//$this->contracts_model->safa_contract_phases_id_current = $this->input->post('safa_contract_phases_id_current');
			//$this->contracts_model->safa_contract_phases_id_next = $this->input->post('safa_contract_phases_id_next');
			//------------------

			$this->contracts_model->country_id = $this->input->post('contarct_country');

			//By Gouda
			$contarct_fax = preg_replace('/^00/', '', $this->input->post('contarct_fax'));
			//$contarct_fax = preg_replace('/^+/', '', $this->input->post('contarct_fax'));
			$this->contracts_model->fax = $contarct_fax;


			$this->contracts_model->city_id = $this->input->post('contarct_city');
			$this->contracts_model->responsible_name = $this->input->post('contarct_responsible_name');
			$this->contracts_model->nationality_id = $this->input->post('contarct_nationality');

			//By Gouda
			$contarct_responsible_phone = preg_replace('/^00/', '', $this->input->post('contarct_responsible_phone'));
			//$contarct_responsible_phone = preg_replace('/^+/', '', $this->input->post('contarct_responsible_phone'));
			$this->contracts_model->responsible_phone = $contarct_responsible_phone;

			$this->contracts_model->responsible_email = $this->input->post('contarct_responsible_email');

			$this->contracts_model->notes = $this->input->post('contract_notes');
			$contract_id = $this->contracts_model->save();
			$this->contracts_model->save();


			$this->setting->safa_uo_contract_setting_id = $data['setting']->safa_uo_contract_setting_id;
			$this->setting->safa_uo_contract_id = $id;

			if ($this->input->post('services_visa'))
			$this->setting->services_buy_uo_visa = $this->input->post('services_visa');
			else
			$this->setting->services_buy_uo_visa = '0';
			if ($this->input->post('services_hostel'))
			$this->setting->services_buy_uo_hotels = $this->input->post('services_hostel');
			else
			$this->setting->services_buy_uo_hotels = '0';
			if ($this->input->post('services_travel'))
			$this->setting->services_buy_uo_transporter = $this->input->post('services_travel');
			else
			$this->setting->services_buy_uo_transporter = '0';
			if ($this->input->post('out_services_hostel'))
			$this->setting->services_buy_other_hotels = $this->input->post('out_services_hostel');
			else
			$this->setting->services_buy_other_hotels = '0';
			if ($this->input->post('out_services_travel'))
			$this->setting->services_buy_other_transporter = $this->input->post('out_services_travel');
			else
			$this->setting->services_buy_other_transporter = '0';
			if ($this->input->post('out_services_other_services'))
			$this->setting->services_buy_other_services = $this->input->post('out_services_other_services');
			else
			$this->setting->services_buy_other_services = '0';

			$this->setting->transport_cycle_from = $this->input->post('transport_cycle_from');
			if ($this->input->post('transport_cycle_from') == 2)
			$this->setting->transporters_us = $this->input->post('transporters_us');
			else
			$this->setting->transporters_us = 'all';

			if ($this->input->post('can_use_other_uo')) {
				$this->setting->can_use_other_uo = $this->input->post('can_use_other_uo');
			} else {
				$this->setting->can_use_other_uo = '0';
			}

			if ($this->input->post('can_use_hotel_marking')) {
				$this->setting->can_use_hotel_marking = $this->input->post('can_use_hotel_marking');
			} else {
				$this->setting->can_use_hotel_marking = '0';
			}

			if ($this->input->post('can_book_couch'))
			$this->setting->can_book_couch = $this->input->post('can_book_couch');
			else
			$this->setting->can_book_couch = '0';



			$this->setting->max_limit_visa = $this->input->post('max_limit_visa');

			$this->setting->credit_balance = $this->input->post('credit_balance');
			$this->setting->credit_balance_currency_id = $this->input->post('credit_balance_currency_id');

			if ($this->input->post('can_over_limit'))
			$this->setting->can_over_limit = $this->input->post('can_over_limit');
			else
			$this->setting->can_over_limit = '0';


			if ($this->input->post('add_passports_accomodation_without_visa'))
			$this->setting->add_passports_accomodation_without_visa = $this->input->post('add_passports_accomodation_without_visa');
			else
			$this->setting->add_passports_accomodation_without_visa = '0';


			$this->setting->save();


			$visa_prices = $this->input->post('visa_price');
			$fromdate = $this->input->post('visa_date_from');
			$todate = $this->input->post('visa_date_to');
			$currency_ids = $this->input->post('visa_price_currency_id');


			if (is_array($visa_prices) && count($visa_prices)) {
				$this->visa_price->safa_uo_contract_id = $id;
				$this->visa_price->safa_uo_contract_setting_id = $data['setting']->safa_uo_contract_setting_id;
				$this->visa_price->delete();
				foreach ($visa_prices as $vkey => $price) {
					if ($price) {
						$this->visa_price->safa_uo_contract_id = $id;
						$this->visa_price->safa_uo_contract_setting_id = $data['setting']->safa_uo_contract_setting_id;
						$this->visa_price->price = $price;
						$this->visa_price->erp_currency_id = $currency_ids[$vkey];
						$this->visa_price->fromdate = $fromdate[$vkey];
						$this->visa_price->todate = $todate[$vkey];
						$this->visa_price->save();
					}
				}
			}
			$this->load->model('contracts_settings_transporters_model', 'transporter');
			$this->transporter->safa_uo_contract_id = $id;
			$this->transporter->safa_uo_contract_setting_id = $data['setting']->safa_uo_contract_setting_id;
			$this->transporter->delete();
			if ($this->input->post('transporters_us') == 'list' && $this->input->post('transport_cycle_from') == 2) {
				$transporterslist = $this->input->post('transporters_us_values');
				if (is_array($transporterslist) && count($transporterslist)) {
					$this->load->model('contracts_settings_transporters_model', 'transporter');
					foreach ($transporterslist as $transporterid) {
						$this->transporter->safa_uo_contract_id = $id;
						$this->transporter->safa_uo_contract_setting_id = $data['setting']->safa_uo_contract_setting_id;
						$this->transporter->safa_transporter_id = $transporterid;
						$this->transporter->save();
					}
				}
			}


			$this->load->view('redirect_message', array('msg' => lang('global_updated_message'),
                'url' => site_url('uo/contracts'),
                'model_title' => lang('node_title'), 'action' => lang('contracts_edit')));

			//            $this->load->view('confirmation_message', array('msg' => lang('are_you_want_to_send_contract_data_email'),
			//                'back_url' => site_url('uo/contracts'),
			//                'confirmation_url' => site_url('uo/contracts/send_email/' . $id . '/edit'),
			//                'model_title' => lang('node_title'), 'action' => lang('contracts_edit'),
			//            ));

		}
	}

	function get_contracts_username_by_country_ajax($country_id = false)
	{
		$this->layout = 'ajax';

		$contracts_username='';

		$this->countries_model->erp_country_id = $country_id;
		$countries_row= $this->countries_model->get();
		$country_code = $countries_row->country_code;

		$this->contracts_model->country_id = $country_id;
		$country_serial = $this->contracts_model->get(true);
		$country_serial++;

		$contracts_username = $country_code.'_'.$country_serial;

		echo $contracts_username;
		exit();
	}

	function sevices_check() {
		if (!$this->input->post('services_visa') && !$this->input->post('services_hostel') && !$this->input->post('services_travel')) {
			$this->form_validation->set_message('sevices_check', lang('contract_have_to_check_one'));
			return FALSE;
		}

		return TRUE;
	}

	function search() {
		//          if (!permission('uo_contracts_search'))
		//                    show_message(lang('global_you_dont_have_permission'));
		if ($this->input->get("safa_uo_contract_id"))
		$this->contracts_model->safa_uo_contract_id = $this->input->get("safa_uo_contract_id");
		if ($this->input->get("safa_uo_id"))
		$this->contracts_model->safa_uo_id = $this->input->get("safa_uo_id");
		if ($this->input->get("contarct_username"))
		$this->contracts_model->contract_username = $this->input->get("contarct_username");
		if ($this->input->get("contarct_password"))
		$this->contracts_model->contract_password = $this->input->get("contarct_password");
		if ($this->input->get("contarct_name_ar"))
		$this->contracts_model->name_ar = $this->input->get("contarct_name_ar");
		if ($this->input->get("contarct_name_la"))
		$this->contracts_model->name_la = $this->input->get("contarct_name_la");
		if ($this->input->get("contarct_address"))
		$this->contracts_model->address = $this->input->get("contarct_address");
		if ($this->input->get("contarct_phone"))
		$this->contracts_model->phone = $this->input->get("contarct_phone");
		if ($this->input->get("contarct_ksa_address"))
		$this->contracts_model->ksa_address = $this->input->get("contarct_ksa_address");
		if ($this->input->get("contarct_ayata_num"))
		$this->contracts_model->iata = $this->input->get("contarct_ayata_num");
		if ($this->input->get("ksa_phone"))
		$this->contracts_model->ksa_phone = $this->input->get("ksa_phone");
		if ($this->input->get("contarct_agency_symbol"))
		$this->contracts_model->agency_symbol = $this->input->get("contarct_agency_symbol");
		if ($this->input->get("contarct_agency_name"))
		$this->contracts_model->agency_name = $this->input->get("contarct_agency_name");
		if ($this->input->get("contarct_email"))
		$this->contracts_model->email = $this->input->get("contarct_email");
		if ($this->input->get("contarct_country"))
		$this->contracts_model->country_id = $this->input->get("contarct_country");
		if ($this->input->get("contarct_fax"))
		$this->contracts_model->fax = $this->input->get("contarct_fax");
		if ($this->input->get("contarct_city"))
		$this->contracts_model->city_id = $this->input->get("contarct_city");
		if ($this->input->get("contarct_responsible_name"))
		$this->contracts_model->responsible_name = $this->input->get("contarct_responsible_name");
		if ($this->input->get("contarct_nationality"))
		$this->contracts_model->nationality_id = $this->input->get("contarct_nationality");
		if ($this->input->get("contarct_responsible_phone"))
		$this->contracts_model->responsible_phone = $this->input->get("contarct_responsible_phone");
		if ($this->input->get("contarct_responsible_email"))
		$this->contracts_model->responsible_email = $this->input->get("contarct_responsible_email");
	}

	function delete($id) {
		//        if (!permission('uo_contracts_delete'))
		//                    show_message(lang('global_you_dont_have_permission'));
		if (!$id)
		show_404();
		$this->contracts_model->safa_uo_contract_id = $id;
		$this->contracts_model->delete();
		$this->load->view('redirect_message', array('msg' => lang('global_deleted_message'),
            'url' => site_url('uo/contracts'),
            'model_title' => lang('node_title'), 'action' => lang('contracts_add')));
		redirect("uo/contracts/index");
	}

	function delete_all() {
		if (!$_POST['delete_items'])
		show_404();
		foreach ($_POST['delete_items'] as $item) {
			$this->contracts_model->safa_uo_contract_id = $item;
			$this->contracts_model->delete();
		}
		redirect("uo/contracts/index");
	}

	function add_another_ea($id = false)
	{
		$this->contracts_model->safa_uo_contract_id = $id;
		$contracts_row = $this->contracts_model->get();
		if(count($contracts_row)>0) {

			//----------------------- Save EA -------------------------
			$this->eas_model->name_ar = $contracts_row->name_ar;
			$this->eas_model->name_la = $contracts_row->name_la;
			$this->eas_model->erp_country_id = $contracts_row->country_id;
			$this->eas_model->phone = $contracts_row->phone;
			$this->eas_model->email = $contracts_row->email;
			$this->eas_model->fax = $contracts_row->fax;
			$ea_id = $this->eas_model->save();

			if ($ea_id > 0) {

				//----------------------- Save EA User -------------------------
				$this->ea_users_model->safa_ea_id = $ea_id;
				$this->ea_users_model->name_ar = $contracts_row->name_ar;
				$this->ea_users_model->name_la = $contracts_row->name_la;
				$this->ea_users_model->email = $contracts_row->email;
				$this->ea_users_model->erp_language_id = 2;
				$this->ea_users_model->safa_ea_usergroup_id = 1;
				$this->ea_users_model->username = $contracts_row->contract_username;
				$this->ea_users_model->password = $contracts_row->contract_password;
				$this->ea_users_model->active = 1;
				$this->ea_users_model->save();

				//----------------------- Save EA Contract -------------------------
				$this->uo_contracts_ea_model->safa_uo_contract_id = $id;
				$this->uo_contracts_ea_model->safa_ea_id = $ea_id;
				$this->uo_contracts_ea_model->name_ar =  session('uo_name').' - '.$contracts_row->name_ar;
				$this->uo_contracts_ea_model->name_la =  session('uo_name').' - '.$contracts_row->name_la;
				$this->uo_contracts_ea_model->save();

			}
		}

		$this->load->view('redirect_message', array('msg' => lang('global_added_message'),
        'url' => site_url('uo/contracts'),
        'model_title' => lang('node_title'), 'action' => lang('contracts_add')));
	}

	function add_externalagents()
	{
		//         if (!permission('uo_contracts_add_externalagents'))
		//                    show_message(lang('global_you_dont_have_permission'));
		//--- this function targets the external agents table------//
		$this->eas_model->name_ar = $this->input->post("contract_name_ar");
		$this->eas_model->name_la = $this->input->post("contract_name_la");
		$this->eas_model->erp_country_id = $this->input->post("contarct_country");
		$this->eas_model->phone = $this->input->post("contarct_phone");
		$this->eas_model->email = $this->input->post("contarct_email");
		$this->eas_model->fax = $this->input->post("contarct_fax");
		$eas_id = $this->eas_model->save();
		return $eas_id;
	}

	function add_ea_contract($contract_id, $ea_id) {
		//        if (!permission('uo_contracts_add_ea_contract'))
		//                    show_message(lang('global_you_dont_have_permission'));
		$this->uo_contracts_ea_model->safa_uo_contract_id = $contract_id;
		$this->uo_contracts_ea_model->safa_ea_id = $ea_id;
		$this->uo_contracts_ea_model->name_ar =  session('uo_name').' - '.$this->input->post("contract_name_ar");
		$this->uo_contracts_ea_model->name_la =  session('uo_name').' - '.$this->input->post("contract_name_la");
		$ea_contract_id = $this->uo_contracts_ea_model->save();
		return $ea_contract_id;
	}

	function add_ea_user($ea_id) {
		//        if (!permission('uo_contracts_add_ea_user'))
		//                    show_message(lang('global_you_dont_have_permission'));
		$this->ea_users_model->safa_ea_id = $ea_id;
		$this->ea_users_model->name_ar = $this->input->post("contract_name_ar");
		$this->ea_users_model->name_la = $this->input->post("contract_name_la");
		$this->ea_users_model->email = $this->input->post("contarct_email");
		$this->ea_users_model->erp_language_id = 2;
		$this->ea_users_model->safa_ea_usergroup_id = 1;
		$this->ea_users_model->username = $this->input->post("contarct_username");
		if ($this->input->post("contarct_password"))
		$this->ea_users_model->password = md5($this->input->post("contarct_password"));
		$this->ea_users_model->active = 1;

		$this->ea_users_model->save();
	}

	function add_hotels($contract_id) {
		//         if (!permission('uo_contracts_add_hotels'))
		//                    show_message(lang('global_you_dont_have_permission'));
		/* get the all hotels and applay all of them to the external agents */
		$this->contracts_model->safa_uo_contract_id = $contract_id;
		$affected_rows = $this->contracts_model->add_hotels();
		return $affected_rows;
	}

	function add_tourismplaces($contract_id) {
		//       if (!permission('uo_contracts_add_tourismplaces'))
		//                    show_message(lang('global_you_dont_have_permission'));
		/* get the all tourismplaces and applay all of them to the external agents */
		$this->contracts_model->safa_uo_contract_id = $contract_id;
		$affected_rows = $this->contracts_model->add_tourismplaces();
		return $affected_rows;
	}

	function add_itos($contract_id) {

		$this->contracts_model->safa_uo_contract_id = $contract_id;
		$this->contracts_model->add_itos();
	}

	function add_ea_itos($contract_id) {
		$this->contracts_model->safa_uo_contract_id = $contract_id;
		$num_rows = $this->contracts_model->add_ea_itos();
	}

	function add_default_umrahpackage($contract_id) {
		$this->contracts_model->safa_uo_contract_id = $contract_id;
		$rows = $this->contracts_model->add_defulat_umrahpackage();
	}

	function is_unique($value, $bool) {

		$boolean = (bool) $bool;
		if ($boolean == false)
		$query = $this->db->limit(1)->where("contract_username", $value)->where('safa_uo_contracts.safa_uo_id', $this->contracts_model->safa_uo_id)->get("safa_uo_contracts");
		else
		$query = $this->db->limit(1)->where("contract_username", $value)->where("safa_uo_contract_id!=" . $this->uri->segment('4'))->where('safa_uo_contracts.safa_uo_id', $this->contracts_model->safa_uo_id)->get("safa_uo_contracts");
		if ($query->num_rows() == 0)
		return true;
		else
		return false;
	}

	function validate_username($value) { // to validate a username.
		$this->form_validation->set_message('validate_username', lang('username_validation'));
		$bool = preg_match('/^[a-zA-Z0-9.,_-]{0,100}$/', $value);
		if ($bool == 0)
		return false;
		else
		return true;
	}

	function get_external_agents($uo_id = FALSE) {
		$name = name();
		$result = $this->contracts_model->get_external_agents();
		$externalagents = array();
		$externalagents[''] = lang('global_select_from_menu');
		foreach ($result as $record) {
			$externalagents[$record->safa_ea_id] = $record->$name;
		}
		return $externalagents;
	}

	function add_ea_seasons($ea_id = false) {
		$this->ea_seasons_model->safa_ea_id = $ea_id;
		$season_id = $this->ea_seasons_model->save();
		return $season_id;
	}

	function add_ea_package($uo_ea_contract_id = false, $season_id = false) {
		$ea_package_id = $this->contracts_model->add_ea_packages($uo_ea_contract_id, $season_id);
		return $ea_package_id;
	}

	function itos() {

		//     if (isset($_GET['search']))
		//          $data['contract_id']=  $this->input->post('contract_id');
		//      else {
		//          $data['contract_id']= $this->uri->segment("4");
		//          echo $data['contract_id'];
		//      }
		#to prevent join with country
		$this->itos_model->join = FALSE;

		//       $this->itos_model->safa_ito_id = $id;

		$data['itos'] = $this->itos_model->get();
		$this->load->view("uo/contracts/itos", $data);
	}

	function add_ito() {

		$this->layout = 'ajax';

		if (!$this->input->is_ajax_request()) {
			redirect('uo/contracts/itos/' . $safa_uo_contract_id);
		} else {

			$safa_ito_id = $this->input->post('safa_ito_id');
			$safa_uo_contract_id = $this->input->post('safa_uo_contract_id');

			$this->uo_contracts_itos_model->safa_ito_id = $safa_ito_id;
			$this->uo_contracts_itos_model->safa_uo_contract_id = $safa_uo_contract_id;

			if ($this->db->insert('safa_uo_contracts_itos', array(
                        'safa_ito_id' => $safa_ito_id,
                        'safa_uo_contract_id' => $safa_uo_contract_id
			))) {

				echo json_encode(array('response' => TRUE, 'msg' => 'added'));
			} else {

				echo json_encode(array('response' => FALSE, 'msg' => 'not added'));
			}
		}
	}

	function delete_ito() {
		$this->layout = 'ajax';
		if (!$this->input->is_ajax_request()) {
			redirect('uo/contracts/itos/' . $safa_uo_contract_id);
		} else {

			$safa_ito_id = $this->input->post('safa_ito_id');
			$safa_uo_contract_id = $this->input->post('safa_uo_contract_id');

			$this->uo_contracts_itos_model->safa_ito_id = $safa_ito_id;
			$this->uo_contracts_itos_model->safa_uo_contract_id = $safa_uo_contract_id;
			if ($this->uo_contracts_itos_model->delete()) {
				echo json_encode(array('response' => TRUE, 'msg' => 'deleted',
				));
			} else {
				echo json_encode(array('response' => FALSE, 'msg' => 'not deleted'));
			}
		}
	}

	function hotels($safa_uo_contract_id) {

		#to get name of city and hotel level
		$this->hotels_model->join = TRUE;

		$data["total_rows"] = $this->hotels_model->get(true);
		$this->hotels_model->offset = $this->uri->segment("5");
		$this->hotels_model->limit = 20;
		$config['uri_segment'] = 4;
		$config['base_url'] = site_url('uo/contracts/hotels/' . $safa_uo_contract_id);
		$config['total_rows'] = $data["total_rows"];
		$config['per_page'] = $this->hotels_model->limit;

		$config['suffix'] = $this->input->get() ? '?' . http_build_query($_GET) : NULL;
		$this->load->library('pagination');
		$this->pagination->initialize($config);
		$data['pagination'] = $this->pagination->create_links();


		$data['hotels'] = $this->hotels_model->get();
		$this->load->view("uo/contracts/hotels", $data);
	}

	function add_hotel() {

		$this->layout = 'ajax';

		if (!$this->input->is_ajax_request()) {
			redirect('uo/contracts/hotels/' . $safa_uo_contract_id);
		} else {

			$erp_hotel_id = $this->input->post('erp_hotel_id');
			$safa_uo_contract_id = $this->input->post('safa_uo_contract_id');

			$this->uo_contracts_hotels_model->erp_hotel_id = $erp_hotel_id;
			$this->uo_contracts_hotels_model->safa_uo_contract_id = $safa_uo_contract_id;

			if ($this->db->insert('safa_uo_contracts_hotels', array(
                        'erp_hotel_id' => $erp_hotel_id,
                        'safa_uo_contract_id' => $safa_uo_contract_id
			))) {

				echo json_encode(array('response' => TRUE, 'msg' => 'added'));
			} else {

				echo json_encode(array('response' => FALSE, 'msg' => 'not added'));
			}
		}
	}

	function delete_hotel() {

		$this->layout = 'ajax';

		if (!$this->input->is_ajax_request()) {
			redirect('uo/contracts/hotels/' . $safa_uo_contract_id);
		} else {

			$erp_hotel_id = $this->input->post('erp_hotel_id');
			$safa_uo_contract_id = $this->input->post('safa_uo_contract_id');

			$this->uo_contracts_hotels_model->erp_hotel_id = $erp_hotel_id;
			$this->uo_contracts_hotels_model->safa_uo_contract_id = $safa_uo_contract_id;

			if ($this->uo_contracts_hotels_model->delete()) {
				echo json_encode(array('response' => TRUE, 'msg' => 'deleted',
				));
			} else {
				echo json_encode(array('response' => FALSE, 'msg' => 'not deleted'));
			}
		}
	}

	function delete_() {
		if (!$this->input->is_ajax_request()) {
			redirect('uo/contracts/itos');
		} else {
			$safa_ito_id = $this->input->post('safa_itos_id');
			$data = $this->contracts_model->check_contract_ito($safa_ito_id);
			if ($data == 0) {
				$this->contracts_model->safa_ito_id = $safa_ito_id;
				if ($this->contracts_model->delete())
				echo json_encode(array('response' => TRUE, 'msg' => 'deleted'));
			}
			else {
				echo json_encode(array('response' => FALSE, 'msg' => 'not deleted'));
			}
		}
	}

	function search_ito() {
		if ($this->input->get('myselect') == 1) {

			//              $contract_id=$this->input->get('contract_id');
			//              $this->uo_contracts_itos_model->safa_uo_contract_id=$contract_id;
			//              $this->uo_contracts_itos_model->safa_uo_contract_id=$contract_id;
		}
	}

	function ajax_contract_files() {
		$this->layout = 'ajax';
		$contract_id = $this->input->post('contract_id');
		if (!$contract_id)
		die('Invalid Contract Id');

		$data['data'] = array();
		$this->load->model('safa_contract_approving_phases_model', 'contract_phases');
		$phases = $this->contract_phases->get_contract_phases($contract_id);
		foreach ($phases as $phase) {
			$files = $this->contract_phases->get_contract_phase_file($contract_id, $phase->safa_contract_phases_id);
			$data['data'][] = array('phase' => $phase, 'files' => $files);
		}


		$this->load->view('uo/contracts/files', $data);
	}

	function sending($safa_uo_contracts_id = false) {

		if (!$safa_uo_contracts_id) {
			show_404();
		}
		$this->layout = 'js';


		$this->contracts_model->safa_uo_contract_id = $safa_uo_contracts_id;
		$item = $this->contracts_model->get();
		$data['item'] = $item;



		$erp_sending_companies_arr = ddgen('erp_sending_companies', array('erp_sending_companies_id', name()));
		$erp_sending_companies_arr['-'] = lang('other');
		$data['erp_sending_companies'] = $erp_sending_companies_arr;


		$this->load->library("form_validation");
		$this->form_validation->set_rules('sending_number', 'lang:sending_number', 'trim|required');
		$this->form_validation->set_rules('sending_date', 'lang:sending_date', 'trim|required');
		$this->form_validation->set_rules('erp_sending_companies_id', 'lang:erp_sending_companies_id', 'trim|required');


		if (!isset($_POST['smt_save']) || $this->form_validation->run() == false) {
			$this->load->view('uo/contracts/sending', $data);
		} else {

			$this->contracts_model->safa_uo_contract_id = $safa_uo_contracts_id;
			$this->contracts_model->sending_number = $this->input->post('sending_number');
			$this->contracts_model->sending_date = $this->input->post('sending_date');
			if ($this->input->post('erp_sending_companies_id') == '-') {
				$this->contracts_model->erp_sending_companies_id = 0;
			} else {
				$this->contracts_model->erp_sending_companies_id = $this->input->post('erp_sending_companies_id');
			}

			if ($this->input->post('sending_others_text') == 0) {
				$this->contracts_model->sending_others_text = $this->input->post('sending_others_text');
			}

			$this->contracts_model->save();

			$this->load->view('redirect_message', array('msg' => lang('sending_message'),
                'url' => site_url("uo/contracts/sending/$safa_uo_contracts_id"),
                'id' => $this->uri->segment("4"),
                'model_title' => lang('send_contract'), 'action' => lang('send_contract')));
		}
	}

	function contact_email_html($item) {
		$this->layout = 'ajax';

		$data['ea_login_link'] = site_url('ea/login/');

		//$this->contracts_model->safa_uo_contract_id = $id;
		//$item = $this->contracts_model->get();

		$data['name_ar'] = $item->name_ar;
		$data['name_la'] = $item->name_la;
		$data['contract_username'] = $item->contract_username;
		//$data['contract_password'] = $item->contract_username;
		$data['contract_password'] = $_SESSION['sesssion_contarct_password'];


		$data['address'] = $item->address;
		$data['phone'] = $item->phone;

		$data['ksa_address'] = $item->ksa_address;
		$data['iata'] = $item->iata;
		$data['ksa_phone'] = $item->ksa_phone;
		$data['agency_symbol'] = $item->agency_symbol;
		$data['agency_name'] = $item->agency_name;
		$data['email'] = $item->email;

		$data['uo_name'] = session('uo_name');



		$this->countries_model->erp_country_id = $item->country_id;
		$country_row = $this->countries_model->get();

		if (count($country_row) > 0) {
			$dir = $country_row->orientation;

			if ($dir == 'ltr') {
				return $this->load->view('uo/contracts/contact_email_ltr', $data, true);
			} else if ($dir == 'rtl') {
				return $this->load->view('uo/contracts/contact_email_rtl', $data, true);
			}
		}
	}

	function send_email($id, $mode = '') {
		$this->contracts_model->safa_uo_contract_id = $id;
		$item = $this->contracts_model->get();

		if (count($item) > 0) {
			$this->load->library('email');
			$this->email->from(config('webmaster_email'), config('title'));
			
			$this->email->to($item->email.','.$item->responsible_email);


			//            $this->email->attach('./static/new_template/contract_email_form/img/icon_02_facebook.png', 'inline', 'icon_02_facebook.png');
			//            $this->email->attach('./static/new_template/contract_email_form/img/icon_02_linkedin.png', 'inline');
			//            $this->email->attach('./static/new_template/contract_email_form/img/icon_02_twitter.png', 'inline');
			//            $this->email->attach(getcwd() . '/static/new_template/contract_email_form/img/Safa-ContractMail-Final_03.png', 'inline');
			//            $this->email->attach(getcwd() . '/static/new_template/contract_email_form/img/Safa-ContractMail-Final_07.png', 'inline');
			//            $this->email->attach(getcwd() . '/static/new_template/contract_email_form/img/Safa-ContractMail-Final_11.png', 'inline');



			$email_message = $this->contact_email_html($item);
			$this->layout = 'new';

			$subject = lang('new_contract_data_email_subject');
			$this->email->subject($subject);
			$this->email->message($email_message);

			$sending_email_result = $this->email->send();
			unset($_SESSION['sesssion_contarct_password']);
		}

		if ($mode == 'add') {

		} else if ($mode == 'edit') {
			$this->load->view('redirect_message', array('msg' => lang('global_updated_message'),
                'url' => site_url('uo/contracts'),
                'model_title' => lang('node_title'), 'action' => lang('contracts_edit')));
		} else if ($mode == 'resend') {
			$this->load->view('redirect_message', array('msg' => lang('resend_message'),
                'url' => site_url('uo/contracts'),
                'model_title' => lang('node_title'), 'action' => lang('resend_message')));
		}
	}

	function resend_email_popup($id)
	{
		$this->layout = 'js_new';
		
		$this->contracts_model->safa_uo_contract_id = $id;
		$item = $this->contracts_model->get();

		$data['safa_uo_contract_id'] = $id;
		
		$this->load->library('form_validation');
		$this->form_validation->set_rules('contarct_password', 'lang:contracts_password', 'trim|required');
		$this->form_validation->set_rules('contarct_confpassword', 'lang:contracts_repeat_password', 'required|trim|matches[contarct_password]');
		if ($this->form_validation->run() == FALSE) {
			$this->load->view('uo/contracts/resend_email_popup', $data);
		} else {
			$this->ea_users_model->email = $item->email;
			$ea_users_row = $this->ea_users_model->get();
			if(count($ea_users_row)>0) {
				$this->ea_users_model->safa_ea_user_id = $ea_users_row[0]->safa_ea_user_id;
				$this->ea_users_model->password = md5($this->input->post("contarct_password"));
				//$this->ea_users_model->ver_code = $this->gencode();
				$this->ea_users_model->save();
					
				$this->contracts_model->contract_password = md5($this->input->post('contarct_password'));
				$this->contracts_model->save();
			}

			$_SESSION['sesssion_contarct_password'] = 	$this->input->post('contarct_password');

			if (count($item) > 0) {
				$this->load->library('email');
				$this->email->from(config('webmaster_email'), config('title'));
				$this->email->to($item->email.','.$item->responsible_email);
				$email_message = $this->contact_email_html($item);
				$this->layout = 'new';
				$subject = lang('new_contract_data_email_subject');
				$this->email->subject($subject);
				$this->email->message($email_message);
				$sending_email_result = $this->email->send();
			}
	
			$this->layout = 'js_new';
			$this->load->view('message_popup', array('msg' => lang('resend_message'),
	            'url' => site_url("uo/contracts"),
	            'id' => $this->uri->segment("4"),
	            'model_title' => lang('node_title'), 'action' => lang('resend_message')));
		}
	}


}

/* End of file contracts.php */
/* Location: ./application/controllers/http://www.googel.com/contracts.php */