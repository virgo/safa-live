<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Contract_phases extends Safa_Controller {

    public $module = "contract_phases";

    public function __construct() {
        parent::__construct();
        $this->layout = 'new';
        //Side menu session, By Gouda.
        session('side_menu_id', 2);
        $this->load->model('safa_contract_phases_model');
        $this->load->model('erp_nationalities_model');
        $this->lang->load('contract_phases');
        $this->load->helper('form_helper');
        $this->load->helper('db_helper');
        $this->load->helper('itconf_helper');
        permission();
        if (session('uo_id')) {
            $this->safa_contract_phases_model->safa_uo_id = session('uo_id');
        }
    }

    public function index() {
        if (isset($_GET['search'])) {
            $this->search();
        }
        $data["total_rows"] = $this->safa_contract_phases_model->get(true);

        $this->safa_contract_phases_model->offset = $this->uri->segment("4");
        //$this->safa_contract_phases_model->limit = $this->config->item('per_page');
        //$this->safa_contract_phases_model->order_by = array('safa_contract_phases_id');
        $data["items"] = $this->safa_contract_phases_model->get();
        $config['uri_segment'] = 4;
        $config['base_url'] = site_url('uo/contract_phases/index');
        $config['total_rows'] = $data["total_rows"];

        $config['per_page'] = $this->config->item('per_page');
        $config['suffix'] = $this->input->get() ? '?' . http_build_query($_GET) : NULL;
        $this->load->library('pagination');
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();
        $this->load->view('uo/contract_phases/index', $data);
    }

    public function add() {
        $data = array();
        $data['nationalities_rows'] = $this->erp_nationalities_model->get();

        $data['nationalities'] = ddgen('erp_nationalities', array('erp_nationality_id', name()), FALSE, FALSE, TRUE);

        $this->load->library("form_validation");

        $this->form_validation->set_rules('name_ar', 'lang:name_ar', 'trim|required');
        $this->form_validation->set_rules('name_la', 'lang:name_la', 'trim|required');
        $this->form_validation->set_rules('nationalities[]', 'lang:nationalities', 'trim|required');

        if ($this->form_validation->run() == false) {
            $this->load->view("uo/contract_phases/add", $data);
        } else {
            $this->safa_contract_phases_model->safa_uo_id = session('uo_id');
            $this->safa_contract_phases_model->name_ar = $this->input->post('name_ar');
            $this->safa_contract_phases_model->name_la = $this->input->post('name_la');

            // Save Phases
            $safa_contract_phases_id = $this->safa_contract_phases_model->save();

            // Save nationalities
            $nationalities = $this->input->post('nationalities');
            foreach ($nationalities as $nationality_id) {
                $this->safa_contract_phases_model->nationalities_safa_contract_phases_id = $safa_contract_phases_id;
                $this->safa_contract_phases_model->nationalities_erp_nationalities_id = $nationality_id;
                $this->safa_contract_phases_model->saveNationalities();
            }

            // Save documents
            $documents_name_ar = $this->input->post('documents_name_ar');
            $documents_name_la = $this->input->post('documents_name_la');
            $documents_nationalities = $this->input->post('documents_nationalities');

            if (ensure($documents_name_ar))
                foreach ($documents_name_ar as $key => $value) {
                    $this->safa_contract_phases_model->documents_safa_contract_phases_id = $safa_contract_phases_id;
                    $this->safa_contract_phases_model->documents_name_ar = $documents_name_ar[$key];
                    $this->safa_contract_phases_model->documents_name_la = $documents_name_la[$key];

                    $safa_contract_phases_documents_id = $this->safa_contract_phases_model->saveDocuments();

                    // Save documents nationalities
                    foreach ($documents_nationalities[$key] as $documents_nationality_id) {
                        $this->safa_contract_phases_model->documents_nationalities_safa_contract_phases_documents_id = $safa_contract_phases_documents_id;
                        $this->safa_contract_phases_model->documents_nationalities_erp_nationalities_id = $documents_nationality_id;
                        $this->safa_contract_phases_model->saveDocumentsNationalities();
                    }
                }


            if (isset($safa_contract_phases_id)) {

                $this->load->view('redirect_message', array('msg' => lang('global_added_message'),
                    'url' => site_url('uo/contract_phases'),
                    'id' => $this->uri->segment("4"),
                    'model_title' => lang('title'), 'action' => lang('add_title')));
            }
        }
    }

    public function edit($id = FALSE) {
        $data = array();
        $data['nationalities_rows'] = $this->erp_nationalities_model->get();

        $data['nationalities'] = ddgen('erp_nationalities', array('erp_nationality_id', name()), FALSE, FALSE, TRUE);

        $this->load->library("form_validation");

        $this->form_validation->set_rules('name_ar', 'lang:name_ar', 'trim|required');
        $this->form_validation->set_rules('name_la', 'lang:name_la', 'trim|required');
        $this->form_validation->set_rules('nationalities[]', 'lang:nationalities', 'trim|required');

        if (!$id) {
            show_404();
        }
        $this->safa_contract_phases_model->safa_contract_phases_id = $id;
        $data['item'] = $this->safa_contract_phases_model->get();

        if (!$data['item']) {
            show_404();
        }

        $data['item_nationalities'] = array();
        $this->safa_contract_phases_model->nationalities_safa_contract_phases_id = $id;
        $item_nationalities = $this->safa_contract_phases_model->getNationalities();
        foreach ($item_nationalities as $item_nationality) {
            $data['item_nationalities'][] = $item_nationality->erp_nationalities_id;
        }

        $this->safa_contract_phases_model->documents_safa_contract_phases_id = $id;
        $item_documents = $this->safa_contract_phases_model->getDocuments();
        $data['item_documents'] = $item_documents;

        $data['item_documents_nationalities'] = array();
        foreach ($item_documents as $item_document) {
            $this->safa_contract_phases_model->documents_nationalities_safa_contract_phases_documents_id = $item_document->safa_contract_phases_documents_id;
            $item_documents_nationalities = $this->safa_contract_phases_model->getDocumentsNationalities();
            foreach ($item_documents_nationalities as $item_documents_nationality) {
                $data['item_documents_nationalities'][$item_document->safa_contract_phases_documents_id][] = $item_documents_nationality->erp_nationalities_id;
            }
        }



        if ($this->form_validation->run() == false) {
            $this->load->view("uo/contract_phases/edit", $data);
        } else {
            $this->safa_contract_phases_model->safa_uo_id = session('uo_id');
            $this->safa_contract_phases_model->name_ar = $this->input->post('name_ar');
            $this->safa_contract_phases_model->name_la = $this->input->post('name_la');

            // Save Phases
            $this->safa_contract_phases_model->save();

            // Save nationalities
            $nationalities = $this->input->post('nationalities');

            $this->safa_contract_phases_model->nationalities_safa_contract_phases_id = $id;
            $this->safa_contract_phases_model->deleteNationalities();

            foreach ($nationalities as $nationality_id) {

                $this->safa_contract_phases_model->nationalities_safa_contract_phases_id = $id;
                $this->safa_contract_phases_model->nationalities_erp_nationalities_id = $nationality_id;
                $this->safa_contract_phases_model->saveNationalities();
            }

            // Save documents

            $this->safa_contract_phases_model->documents_safa_contract_phases_id = $id;
            //$this->safa_contract_phases_model->deleteDocuments();

            $documents_name_ar = $this->input->post('documents_name_ar');
            $documents_name_la = $this->input->post('documents_name_la');
            $documents_nationalities = $this->input->post('documents_nationalities');

            if (ensure($documents_name_ar))
                foreach ($documents_name_ar as $key => $value) {
                    $this->safa_contract_phases_model->documents_safa_contract_phases_id = $id;
                    $this->safa_contract_phases_model->documents_name_ar = $documents_name_ar[$key];
                    $this->safa_contract_phases_model->documents_name_la = $documents_name_la[$key];

                    $this->safa_contract_phases_model->documents_safa_contract_phases_documents_id = $key;
                    $current_document_row_count = $this->safa_contract_phases_model->getDocuments(true);
                    if ($current_document_row_count == 0) {
                        $this->safa_contract_phases_model->documents_safa_contract_phases_documents_id = false;
                        $safa_contract_phases_documents_id = $this->safa_contract_phases_model->saveDocuments();
                    } else {
                        $safa_contract_phases_documents_id = $key;
                        $this->safa_contract_phases_model->saveDocuments();
                    }



                    // Save documents nationalities
                    $this->safa_contract_phases_model->documents_nationalities_safa_contract_phases_documents_id = $key;
                    $this->safa_contract_phases_model->documents_nationalities_erp_nationalities_id = false;
                    $this->safa_contract_phases_model->deleteDocumentsNationalities();
                    if (isset($documents_nationalities[$key])) {
                        foreach ($documents_nationalities[$key] as $documents_nationality_id) {
                            $this->safa_contract_phases_model->documents_nationalities_safa_contract_phases_documents_id = $safa_contract_phases_documents_id;
                            $this->safa_contract_phases_model->documents_nationalities_erp_nationalities_id = $documents_nationality_id;
                            $this->safa_contract_phases_model->saveDocumentsNationalities();
                        }
                    }
                }

            // --- Delete Deleted documents rows from Database --------------
            $documents_remove = $this->input->post('documents_remove');
            if (ensure($documents_remove)) {
                foreach ($documents_remove as $document_remove) {
                    $this->safa_contract_phases_model->documents_safa_contract_phases_documents_id = $document_remove;
                    $this->safa_contract_phases_model->deleteDocuments();
                }
            }
            //---------------------------------------------------------------


            $this->load->view('redirect_message', array('msg' => lang('global_updated_message'),
                'url' => site_url('uo/contract_phases'),
                'id' => $this->uri->segment("4"),
                'model_title' => lang('title'), 'action' => lang('edit_title')));
        }
    }

    function delete($id = false) {
        if (!$id)
            show_404();

        $this->safa_contract_phases_model->safa_contract_phases_id = $id;
        if (!$this->safa_contract_phases_model->delete())
            show_404();
        $this->load->view('redirect_message', array('msg' => lang('global_deleted_message'),
            'url' => site_url('uo/contract_phases'),
            'model_title' => lang('title'), 'action' => lang('delete_title')));
    }

    function delete_all() {
        if ($this->input->post("delete_items")) {
            foreach ($this->input->post("delete_items") as $item) {
                $this->safa_contract_phases_model->safa_contract_phases_id = $item;

                $this->safa_contract_phases_model->delete();
            }
        } else
            show_404();
        redirect("uo/contract_phases/index");
    }

    function search() {
        if ($this->input->get("safa_contract_phases_id"))
            $this->safa_contract_phases_model->safa_contract_phases_id = $this->input->get("safa_contract_phases_id");
        if ($this->input->get("safa_trip_id"))
            $this->safa_contract_phases_model->safa_trip_id = $this->input->get("safa_trip_id");
        if ($this->input->get("safa_ito_id"))
            $this->safa_contract_phases_model->safa_ito_id = $this->input->get("safa_ito_id");
        if ($this->input->get("safa_transporter_id"))
            $this->safa_contract_phases_model->safa_transporter_id = $this->input->get("safa_transporter_id");
        if ($this->input->get("safa_tripstatus_id"))
            $this->safa_contract_phases_model->safa_tripstatus_id = $this->input->get("safa_tripstatus_id");
        if ($this->input->get("safa_internaltripstatus_id"))
            $this->safa_contract_phases_model->safa_internaltripstatus_id = $this->input->get("safa_internaltripstatus_id");
        if ($this->input->get("operator_reference"))
            $this->safa_contract_phases_model->operator_reference = $this->input->get("operator_reference");
        if ($this->input->get("attachement"))
            $this->safa_contract_phases_model->attachement = $this->input->get("attachement");
        if ($this->input->get("datetime"))
            $this->safa_contract_phases_model->datetime = $this->input->get("datetime");
        /* search utility */
        if ($this->input->get("uo_contract_id"))
            $this->safa_contract_phases_model->by_contract = $this->input->get("uo_contract_id");
        /* search utility */
    }

    function create_contracts_array() {
        $result = $this->safa_contract_phases_model->get_uo_contracts();
        $contracts = array();
        $contracts[""] = lang('global_select_from_menu');
        $name = name();
        foreach ($result as $con_id => $contract) {
            $contracts[$contract->contract_id] = $contract->$name;
        }
        return $contracts;
    }

    function get_uo_contracts_itos() { // get the itos that involved in  the same contracts of that uo
        $result = $this->safa_contract_phases_model->get_uo_contracts_itos();
        $itos = array();
        $itos[""] = lang('global_select_from_menu');
        $name = name();
        foreach ($result as $itos_id => $ito) {
            $itos[$ito->safa_ito_id] = $ito->$name;
        }
        return $itos;
    }

    function create_transportes_array() {
        $result = $this->safa_contract_phases_model->get_uo_transporters_withContracts();
        $transporters = array();
        $transporters[""] = lang('global_select_from_menu');
        $name = name();
        foreach ($result as $transporter_id => $transporter) {
            $transporters[$transporter->transporter_id] = $transporter->$name;
        }
        return $transporters;
    }

    function get_ea_trips($contract_id = FALSE) {
        $this->layout = 'ajax'; //  to prevent the hook in loading the header in the response//
        $trips_ea = $this->safa_contract_phases_model->get_ea_trips($contract_id);
        $trips = array();
        echo "<option value=''>" . lang('global_select_from_menu') . "</option>";
        if (isset($trips_ea) && count($trips_ea) > 0) {

            foreach ($trips_ea as $trip) {
                echo "<option value='" . $trip->safa_trip_id . "'>" . $trip->name . "</option>";
            }
        }
    }

    function fileupload() {
        $this->change_upload_name();
        $config['upload_path'] = './static/temp/uo_files';
        $config['allowed_types'] = 'pdf|jpg|jpeg';
        $config['max_size'] = '1024';
        $config['max_width'] = '1024';
        $config['max_height'] = '768';
        $config['overwrite'] = 'true';
        $config['encrypt_name'] = 'true';
        $this->load->library('upload', $config);
        if (!$this->upload->do_upload()) {
            $error = $this->upload->display_errors();
            $this->form_validation->set_message('fileupload', $error);
            return false;
        }
    }

    function move_upload() {
        if (file_exists('./static/temp/uo_files/' . session('internaltrip_file_upload_time') . $this->input->post('text_input'))) {
            if (!copy('./static/temp/uo_files/' . session('internaltrip_file_upload_time') . $this->input->post('text_input'), './static/uo_files/' . session('internaltrip_file_upload_time') . $this->input->post('text_input')))
                if (!move_uploaded_file('./static/temp/uo_files/' . session('internaltrip_file_upload_time') . $this->input->post('text_input'), './static/uo_files/' . session('internaltrip_file_upload_time') . $this->input->post('text_input')))
                    echo "<script>alert('error uploading a file')</script>";
        } else
            echo "<script>alert('error uploading a file')</script>";
    }

    function get_ito_refcode($ito_id = false) {
        $this->layout = 'ajax';
        $this->load->model('itos_model');
        $this->itos_model->custom_select = 'reference_code';
        $this->itos_model->safa_ito_id = $ito_id;
        $result = $this->itos_model->get();
        if (isset($result))
            echo $result->reference_code;
    }

    function change_upload_name() {
        $upload_time = rand(0, 9999) . time();
        session('internaltrip_file_upload_time', $upload_time);
        $file_name = $_FILES['userfile']['name'];
        $uplaod_name = session('internaltrip_file_upload_time') . $file_name;
        $_FILES['userfile']['name'] = $uplaod_name;
    }

}

/* End of file contract_phases.php */
/* Location: ./application/controllers/contract_phases.php */