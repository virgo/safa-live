<?php

if (!defined('BASEPATH'))
exit('No direct script access allowed');

class Trip_internaltrip extends Safa_Controller {

	public $module = "trip_internaltrip";
	public $temp_upload_path;
	public $uploaded_file_name;

	public function __construct() {
		parent::__construct();
		$this->layout = 'new';
		//Side menu session, By Gouda.
		session('side_menu_id', 4);
		//By Gouda, For Notifications
		$this->load->model('contracts_model');
		$this->load->model('notification_model');
		$this->load->model('internaltrip_status_model');
		$this->load->model('safa_trips_model');
		$this->load->model('trip_details_model');
		$this->load->model('safa_uo_users_model');
		$this->load->model('safa_uos_model');
		$this->load->model('hotels_model');
		$this->load->model('transporters_model');
		
		$this->load->model('trip_internaltrip_model');
		$this->load->model('internalpassages_model');
		$this->lang->load('trip_internaltrip');
		$this->lang->load('internalpassages');
		$this->lang->load('flight_availabilities');

		$this->lang->load('trip_details');

		$this->load->helper('form_helper');
		$this->load->helper('db_helper');
		$this->load->helper('itconf_helper');
		$this->load->helper('trip');


		permission();
		if (session('uo_id')) {
			$this->trip_internaltrip_model->uo_id = session('uo_id');
			$this->safa_trips_model->safa_uo_id = session('uo_id');
			
		}
	}

	public function index() {

		//$data["uo_contracts"] = $this->create_contracts_array();
		$data["uo_eas"] = $this->create_eas_array();
		$data["safa_uo_contracts"] = ddgen('safa_uo_contracts', array('safa_uo_contract_id', name()), array('safa_uo_id' => session('uo_id')), FALSE, TRUE);

		$data["transporters"] = $this->create_transportes_array();

		$data["safa_intrernaltripstatus"] = ddgen("safa_internaltripstatus", array("safa_internaltripstatus_id", name()));
		$data["safa_intrernaltripstatus_color"] = $this->db->select(name() . ',color,code')->from('safa_internaltripstatus')->get()->result();
		$data["safa_ito"] = $this->get_uo_contracts_itos();
		if (isset($_GET['search']))
		$this->search();
		$data["total_rows"] = $this->trip_internaltrip_model->get(true);

		$this->trip_internaltrip_model->offset = $this->uri->segment("4");
		//$this->trip_internaltrip_model->limit = $this->config->item('per_page');
		$this->trip_internaltrip_model->order_by = array('serial', 'desc');
		$data["items"] = $this->trip_internaltrip_model->get();
		$config['uri_segment'] = 4;
		$config['base_url'] = site_url('uo/trip_internaltrip/index');
		$config['total_rows'] = $data["total_rows"];

		$config['per_page'] = $this->config->item('per_page');
		$config['suffix'] = $this->input->get() ? '?' . http_build_query($_GET) : NULL;
		$this->load->library('pagination');
		$this->pagination->initialize($config);
		$data['pagination'] = $this->pagination->create_links();
		$this->load->view('uo/trip_internaltrip/index', $data);
	}

	public function reserved_from_ea() 
	{

		$this->trip_internaltrip_model->where_condition = "(safa_trip_internaltrips.safa_internaltripstatus_id=3 OR safa_trip_internaltrips.safa_internaltripstatus_id=5)";
		
		//$data["uo_contracts"] = $this->create_contracts_array();
		$data["uo_eas"] = $this->create_eas_array();
		$data["safa_uo_contracts"] = ddgen('safa_uo_contracts', array('safa_uo_contract_id', name()), array('safa_uo_id' => session('uo_id')), FALSE, TRUE);

		$data["transporters"] = $this->create_transportes_array();

		$data["safa_intrernaltripstatus"] = ddgen("safa_internaltripstatus", array("safa_internaltripstatus_id", name()));
		$data["safa_intrernaltripstatus_color"] = $this->db->select(name() . ',color,code')->from('safa_internaltripstatus')->get()->result();
		$data["safa_ito"] = $this->get_uo_contracts_itos();
		if (isset($_GET['search']))
		$this->search();
		$data["total_rows"] = $this->trip_internaltrip_model->get(true);

		$this->trip_internaltrip_model->offset = $this->uri->segment("4");
		//$this->trip_internaltrip_model->limit = $this->config->item('per_page');
		$this->trip_internaltrip_model->order_by = array('serial', 'desc');
		$data["items"] = $this->trip_internaltrip_model->get();
		$config['uri_segment'] = 4;
		$config['base_url'] = site_url('uo/trip_internaltrip/index');
		$config['total_rows'] = $data["total_rows"];

		$config['per_page'] = $this->config->item('per_page');
		$config['suffix'] = $this->input->get() ? '?' . http_build_query($_GET) : NULL;
		$this->load->library('pagination');
		$this->pagination->initialize($config);
		$data['pagination'] = $this->pagination->create_links();
		$this->load->view('uo/trip_internaltrip/index', $data);
	}
	
	public function requested_from_ea() 
	{

		$this->trip_internaltrip_model->where_condition = "(safa_trip_internaltrips.safa_internaltripstatus_id=2 OR safa_trip_internaltrips.safa_internaltripstatus_id=4)";
		
		//$data["uo_contracts"] = $this->create_contracts_array();
		$data["uo_eas"] = $this->create_eas_array();
		$data["safa_uo_contracts"] = ddgen('safa_uo_contracts', array('safa_uo_contract_id', name()), array('safa_uo_id' => session('uo_id')), FALSE, TRUE);

		$data["transporters"] = $this->create_transportes_array();

		$data["safa_intrernaltripstatus"] = ddgen("safa_internaltripstatus", array("safa_internaltripstatus_id", name()));
		$data["safa_intrernaltripstatus_color"] = $this->db->select(name() . ',color,code')->from('safa_internaltripstatus')->get()->result();
		$data["safa_ito"] = $this->get_uo_contracts_itos();
		if (isset($_GET['search']))
		$this->search();
		$data["total_rows"] = $this->trip_internaltrip_model->get(true);

		$this->trip_internaltrip_model->offset = $this->uri->segment("4");
		//$this->trip_internaltrip_model->limit = $this->config->item('per_page');
		$this->trip_internaltrip_model->order_by = array('serial', 'desc');
		$data["items"] = $this->trip_internaltrip_model->get();
		$config['uri_segment'] = 4;
		$config['base_url'] = site_url('uo/trip_internaltrip/index');
		$config['total_rows'] = $data["total_rows"];

		$config['per_page'] = $this->config->item('per_page');
		$config['suffix'] = $this->input->get() ? '?' . http_build_query($_GET) : NULL;
		$this->load->library('pagination');
		$this->pagination->initialize($config);
		$data['pagination'] = $this->pagination->create_links();
		$this->load->view('uo/trip_internaltrip/index', $data);
	}
	
	public function with_confirmation_number() 
	{

		$this->trip_internaltrip_model->where_condition = "(safa_trip_internaltrips.safa_internaltripstatus_id=3 AND safa_trip_internaltrips.confirmation_number!=0)";
		
		//$data["uo_contracts"] = $this->create_contracts_array();
		$data["uo_eas"] = $this->create_eas_array();
		$data["safa_uo_contracts"] = ddgen('safa_uo_contracts', array('safa_uo_contract_id', name()), array('safa_uo_id' => session('uo_id')), FALSE, TRUE);

		$data["transporters"] = $this->create_transportes_array();

		$data["safa_intrernaltripstatus"] = ddgen("safa_internaltripstatus", array("safa_internaltripstatus_id", name()));
		$data["safa_intrernaltripstatus_color"] = $this->db->select(name() . ',color,code')->from('safa_internaltripstatus')->get()->result();
		$data["safa_ito"] = $this->get_uo_contracts_itos();
		if (isset($_GET['search']))
		$this->search();
		$data["total_rows"] = $this->trip_internaltrip_model->get(true);

		$this->trip_internaltrip_model->offset = $this->uri->segment("4");
		//$this->trip_internaltrip_model->limit = $this->config->item('per_page');
		$this->trip_internaltrip_model->order_by = array('serial', 'desc');
		$data["items"] = $this->trip_internaltrip_model->get();
		$config['uri_segment'] = 4;
		$config['base_url'] = site_url('uo/trip_internaltrip/index');
		$config['total_rows'] = $data["total_rows"];

		$config['per_page'] = $this->config->item('per_page');
		$config['suffix'] = $this->input->get() ? '?' . http_build_query($_GET) : NULL;
		$this->load->library('pagination');
		$this->pagination->initialize($config);
		$data['pagination'] = $this->pagination->create_links();
		$this->load->view('uo/trip_internaltrip/index', $data);
	}
	
	public function add() {
		/* get the refrance data */
		//        if (!permission('trip_internaltrip_add'))
		//                    show_message(lang('global_you_dont_have_permission'));
		//$data["uo_contracts"] = $this->create_contracts_array();
		$data["uo_eas"] = $this->create_eas_array();

		$data["safa_uo_contracts"] = ddgen('safa_uo_contracts', array('safa_uo_contract_id', name()), array('safa_uo_id' => session('uo_id')), FALSE, FALSE);

		$data["transporters"] = $this->create_transportes_array();


		//$data["safa_intrernaltripstatus"] = ddgen("safa_internaltripstatus", array("safa_internaltripstatus_id", name()));
		$safa_intrernaltripstatus_rows = $this->internaltrip_status_model->get();
		$safa_intrernaltripstatus_arr = array();
		foreach ($safa_intrernaltripstatus_rows as $safa_intrernaltripstatus_row) {
			if(session('ea_id')) {
				if($safa_intrernaltripstatus_row->safa_internaltripstatus_id==1 || $safa_intrernaltripstatus_row->safa_internaltripstatus_id==2) {
					$safa_intrernaltripstatus_arr[$safa_intrernaltripstatus_row->safa_internaltripstatus_id] = $safa_intrernaltripstatus_row->{name()};
				}
			} else if(session('uo_id')) {
				if($safa_intrernaltripstatus_row->safa_internaltripstatus_id==4) {
					$safa_intrernaltripstatus_arr[$safa_intrernaltripstatus_row->safa_internaltripstatus_id] = $safa_intrernaltripstatus_row->{name()};
				}
			}
		}
		$data["safa_intrernaltripstatus"] =$safa_intrernaltripstatus_arr;
		
		
		$data["safa_ito"] = $this->get_uo_contracts_itos();

		$data["confirmation_number"] = gen_itconf();

		$this->safa_uos_model->safa_uo_id = session('uo_id');
		$safa_uo_row = $this->safa_uos_model->get();
		$data['safa_uo_code'] = $safa_uo_row->code;
		
		$safa_uo_row = $this->trip_internaltrip_model->get_max_serial_by_uo(session('uo_id'));
		$data['safa_uo_serial'] = $safa_uo_row->max_serial+1;
		
		
		
		$this->load->library("form_validation");
		/*         * ************* */
		$this->form_validation->set_rules('datetime', 'lang:transport_request_date', 'trim|required');
		//$this->form_validation->set_rules('uo_ea_id', 'lang:transport_request_contract', 'trim|required');
		//$this->form_validation->set_rules('safa_trip_id', 'lang:transport_request_trip_id', 'trim|required');
		$this->form_validation->set_rules('safa_internaltripstatus_id', 'lang:transport_request_status', 'trim|required');
		if (PHASE > 1):
		$this->form_validation->set_rules('safa_ito_id', 'lang:transport_request_opertator', 'trim');
		else:
		$this->form_validation->set_rules('safa_ito_id', 'lang:transport_request_opertator', 'trim|required');
		endif;

		$this->form_validation->set_rules('safa_transporter_id', 'lang:transport_request_transportername', 'trim');
		$this->form_validation->set_rules('safa_uo_contract_id', 'lang:transport_request_contract', 'trim');

		$this->form_validation->set_rules('serial', 'lang:serial', 'trim|required|unique_col_by_uo[safa_trip_internaltrips.serial]');
		$this->form_validation->set_rules('buses_count', 'lang:buses_count', 'trim|required');

		//$this->form_validation->set_rules('operator_reference', 'lang:transport_request_res_code', 'trim|required');
		$this->form_validation->set_rules('text_input', '', 'trim');
		if ($this->input->post('text_input') && $_FILES['userfile']['name']) {
			$this->form_validation->set_rules('userfile', 'lang:userfile', 'trim|callback_fileupload');
		}


		if ($this->form_validation->run() == false) {
			$this->load->view("uo/trip_internaltrip/add", $data);
		} else {

			//$file_name = $this->input->post('text_input');

			if (isset($_FILES["transport_request_res_file"])) {
				if (count($_FILES["transport_request_res_file"]['tmp_name']) > 0) {
					//Get the temp file path
					$tmpFilePath = $_FILES["transport_request_res_file"]['tmp_name'];

					//Make sure we have a filepath
					if ($tmpFilePath != "") {
						//Setup our new file path
						$newFilePath = './static/temp/uo_files/' . $_FILES["transport_request_res_file"]['name'];

						//Upload the file into the temp dir
						//To solve arabic files names problem.
						$is_file_uploaded = move_uploaded_file($tmpFilePath, iconv('utf-8', 'windows-1256', $newFilePath));
						//$is_file_uploaded=move_uploaded_file($tmpFilePath, $newFilePath);



						$file_name = $_FILES["transport_request_res_file"]['name'];
						$this->trip_internaltrip_model->attachement = $file_name;
					}
				}
			}

			if ($this->input->post('safa_trip_id')) {
				$this->trip_internaltrip_model->safa_trip_id = $this->input->post('safa_trip_id');
			} else {
				$this->trip_internaltrip_model->safa_trip_id = null;
			}
			$this->trip_internaltrip_model->safa_uo_id = session('uo_id');



			if ($this->input->post('safa_ito_id'))
			$this->trip_internaltrip_model->safa_ito_id = $this->input->post('safa_ito_id');
			if ($this->input->post('safa_transporter_id'))
			$this->trip_internaltrip_model->safa_transporter_id = $this->input->post('safa_transporter_id');

			$this->trip_internaltrip_model->safa_internaltripstatus_id = $this->input->post('safa_internaltripstatus_id');
			$this->trip_internaltrip_model->operator_reference = $this->input->post('operator_reference');
			//$this->trip_internaltrip_model->attachement = $file_name;
			$this->trip_internaltrip_model->datetime = $this->input->post('datetime');

			//$this->trip_internaltrip_model->erp_company_type_id = 3;
			//$this->trip_internaltrip_model->erp_company_id = $this->input->post('uo_ea_id');

			$this->trip_internaltrip_model->safa_uo_contract_id = $this->input->post('safa_uo_contract_id');

			$this->trip_internaltrip_model->serial = $this->input->post('serial');
			
			$this->trip_internaltrip_model->trip_title = $this->input->post('safa_uo_code').'-'.$this->input->post('serial');
			
			$this->trip_internaltrip_model->buses_count = $this->input->post('buses_count');

			$this->trip_internaltrip_model->confirmation_number = $this->input->post('confirmation_number');



			$internaltrip_id = $this->trip_internaltrip_model->save();

			//if($file_name)// uploading when the file exist//
			//$this->move_upload();

			if (isset($internaltrip_id)) {


				//By Gouda.
				//----------- Send Notification For EA Company ------------------------------
				$msg_datetime = date('Y-m-d H:i', time());

				$this->notification_model->notification_type = 'automatic';
				$this->notification_model->erp_importance_id = 1;
				$this->notification_model->sender_type_id = 1;
				$this->notification_model->sender_id = 0;

				$this->notification_model->erp_system_events_id = 19;

				$this->notification_model->language_id = 2;
				$this->notification_model->msg_datetime = $msg_datetime;


				$internaltripstatus_name = '';
				$safa_internaltripstatus_id = $this->input->post('safa_internaltripstatus_id');
				$this->internaltrip_status_model->safa_internaltripstatus_id = $safa_internaltripstatus_id;
				$internaltrip_status_row = $this->internaltrip_status_model->get();
				if (count($internaltrip_status_row) > 0) {
					$internaltripstatus_name = $internaltrip_status_row->{name()};
				}
				$trip_name = '';
				$safa_trip_id = $this->input->post('safa_trip_id');
				$this->safa_trips_model->safa_trip_id = $safa_trip_id;
				$trip_row = $this->safa_trips_model->get();
				if (count($trip_row) > 0) {
					$trip_name = $trip_row->name;
				}

				//Link to replace.
				$link_internaltrip = "<a href='" . site_url('ea/trip_internaltrip/edit/' . $internaltrip_id) . "'  target='_blank'> $internaltrip_id </a>";

				$this->notification_model->tags = "#*safa_trip_internaltrip_id#*=$link_internaltrip*****#*internaltripstatus#*=$internaltripstatus_name*****#*the_date#*=$msg_datetime*****#*trip#*=$trip_name";
				$this->notification_model->parent_id = '';

				$erp_notification_id = $this->notification_model->save();

				//-------- Notification Details ---------------
				$this->notification_model->detail_erp_notification_id = $erp_notification_id;
				$this->notification_model->detail_receiver_type = 3;

				$this->contracts_model->safa_uo_contract_id = $this->input->post('uo_contract_id');
				$contracts_row=$this->contracts_model->search();
				if(count($contracts_row)>0) {
					$this->notification_model->detail_receiver_id = $contracts_row[0]->safa_ea_id ;
					//$this->notification_model->detail_receiver_id = $this->input->post('uo_ea_id');
					$this->notification_model->saveDetails();
				}
				//--------------------------------
				//-------------------------------------------------------------------
				//----------- Send Notification For ITO Company ------------------------------
				if ($this->input->post('safa_ito_id') != '') {
					$msg_datetime = date('Y-m-d H:i', time());

					$this->notification_model->notification_type = 'automatic';
					$this->notification_model->erp_importance_id = 1;
					$this->notification_model->sender_type_id = 1;
					$this->notification_model->sender_id = 0;

					$this->notification_model->erp_system_events_id = 19;

					$this->notification_model->language_id = 2;
					$this->notification_model->msg_datetime = $msg_datetime;


					$internaltripstatus_name = '';
					$safa_internaltripstatus_id = $this->input->post('safa_internaltripstatus_id');
					$this->internaltrip_status_model->safa_internaltripstatus_id = $safa_internaltripstatus_id;
					$internaltrip_status_row = $this->internaltrip_status_model->get();
					if (count($internaltrip_status_row) > 0) {
						$internaltripstatus_name = $internaltrip_status_row->{name()};
					}
					$trip_name = '';
					$safa_trip_id = $this->input->post('safa_trip_id');
					$this->safa_trips_model->safa_trip_id = $safa_trip_id;
					$trip_row = $this->safa_trips_model->get();
					if (count($trip_row) > 0) {
						$trip_name = $trip_row->name;
					}

					//Link to replace.
					$link_internaltrip = "<a href='" . site_url('ito/trip_internaltrip/edit_incoming/' . $internaltrip_id) . "'  target='_blank' > $internaltrip_id </a>";

					$this->notification_model->tags = "#*safa_trip_internaltrip_id#*=$link_internaltrip*****#*internaltripstatus#*=$internaltripstatus_name*****#*the_date#*=$msg_datetime*****#*trip#*=$trip_name";
					$this->notification_model->parent_id = '';

					$erp_notification_id = $this->notification_model->save();

					//-------- Notification Details ---------------
					$this->notification_model->detail_erp_notification_id = $erp_notification_id;
					$this->notification_model->detail_receiver_type = 5;
					$this->notification_model->detail_receiver_id = $this->input->post('safa_ito_id');
					$this->notification_model->saveDetails();

					//--------------------------------
				}
				//-------------------------------------------------------------------



				$this->load->view('redirect_message', array('msg' => lang('global_added_message'),
                    'url' => site_url('uo/trip_internaltrip/edit/' . $internaltrip_id),
                    'model_title' => lang('node_title'), 'action' => lang('add_transport_request')));
			}
		}
	}

	public function addfull() {

		$this->db->trans_start();

		$this->load->model('flight_availabilities_model');
		$this->load->model('trip_supervisors_model');
		$this->load->model('safa_trips_requests_model');
		$this->load->model('safa_group_passports_model');
		$this->load->model('notification_model');
		$this->load->model('ports_model');
		$this->load->model('safa_uos_model');
		
		$this->safa_uos_model->safa_uo_id = session('uo_id');
		$safa_uo_row = $this->safa_uos_model->get();
		$data['safa_uo_code'] = $safa_uo_row->code;
		
		$safa_uo_row = $this->trip_internaltrip_model->get_max_serial_by_uo(session('uo_id'));
		$data['safa_uo_serial'] = $safa_uo_row->max_serial+1;
		
		
		
		$data['cities'] = ddgen('erp_cities', array('erp_city_id', name()), array('erp_country_id' => 966), FALSE, TRUE);
		$data['hotels'] = ddgen('erp_hotels', array('erp_hotel_id', name()), array('erp_city_id' => 1), array(name(), 'ASC'), TRUE);

		$all_flights_rows = $this->flight_availabilities_model->get_for_trip();
		$data['all_flights_rows'] = $all_flights_rows;

		$data['erp_flight_classes'] = ddgen('erp_flight_classes', array('erp_flight_class_id', 'name'), false, false, true);
		$data['erp_flight_status'] = ddgen('erp_flight_status', array('erp_flight_status_id', 'name'), false, false, true);
		$data['erp_path_types'] = ddgen('erp_path_types', array('erp_path_type_id', name()), false, false, true);
		$data['safa_tourismplaces'] = ddgen('safa_tourismplaces', array('safa_tourismplace_id', name()), FALSE, FALSE, TRUE);

		$erp_ports_rows = $this->ports_model->get_all_have_halls();
		$erp_ports_arr = array();
		foreach ($erp_ports_rows as $erp_ports_row) {
			$erp_ports_arr[$erp_ports_row->erp_port_id] = $erp_ports_row->code;
		}
		$data["erp_ports"] = $erp_ports_arr;

		$erp_sa_ports_rows = $this->ports_model->get_all_have_halls('SA');
		$erp_sa_ports_arr = array();
		foreach ($erp_sa_ports_rows as $erp_sa_ports_row) {
			$erp_sa_ports_arr[$erp_sa_ports_row->erp_port_id] = $erp_sa_ports_row->code;
		}
		$data["erp_sa_ports"] = $erp_sa_ports_arr;


		$erp_airlines_query = $this->db->query('select ' . FSDB . '.fs_airlines.fs_airline_id, CONCAT(safa_transporters.'.name().',\' \', ' . FSDB . '.`fs_airlines`.`iata`) as transporter_code_name from  ' . FSDB . '.`fs_airlines`, safa_transporters where  safa_transporters.code=' . FSDB . '.fs_airlines.iata order by  transporter_code_name');
		$erp_airlines_rows = $erp_airlines_query->result();  
		//$erp_airlines_rows = $this->db->order_by('iata', 'asc')->where("iata <> ''")->get(FSDB . '.fs_airlines')->result();
		$erp_airlines_arr = array();
		foreach ($erp_airlines_rows as $erp_airlines_row) {
			$erp_airlines_arr[$erp_airlines_row->fs_airline_id] = $erp_airlines_row->transporter_code_name;
		}
		$data["erp_airlines"] = $erp_airlines_arr;

		$data["uo_eas"] = $this->create_eas_array();

		$data["safa_uo_contracts"] = ddgen('safa_uo_contracts', array('safa_uo_contract_id', name()), array('safa_uo_id' => session('uo_id')), FALSE, TRUE);

		$data["transporters"] = $this->create_transportes_array();


		//$data["safa_intrernaltripstatus"] = ddgen("safa_internaltripstatus", array("safa_internaltripstatus_id", name()));
		$safa_intrernaltripstatus_rows = $this->internaltrip_status_model->get();
		$safa_intrernaltripstatus_arr = array();
		foreach ($safa_intrernaltripstatus_rows as $safa_intrernaltripstatus_row) {
			if(session('ea_id')) {
				if($safa_intrernaltripstatus_row->safa_internaltripstatus_id==1 || $safa_intrernaltripstatus_row->safa_internaltripstatus_id==2) {
					$safa_intrernaltripstatus_arr[$safa_intrernaltripstatus_row->safa_internaltripstatus_id] = $safa_intrernaltripstatus_row->{name()};
				}
			} else if(session('uo_id')) {
				if($safa_intrernaltripstatus_row->safa_internaltripstatus_id==4) {
					$safa_intrernaltripstatus_arr[$safa_intrernaltripstatus_row->safa_internaltripstatus_id] = $safa_intrernaltripstatus_row->{name()};
				}
			}
		}
		$data["safa_intrernaltripstatus"] =$safa_intrernaltripstatus_arr;
		
		
		$data["safa_ito"] = $this->get_uo_contracts_itos();
		$this->load->library("form_validation");
		/*         * ************* */
		$this->form_validation->set_rules('datetime', 'lang:transport_request_date', 'trim|required');
		$this->form_validation->set_rules('safa_uo_contract_id', 'lang:transport_request_contract', 'trim|required');
		$this->form_validation->set_rules('uo_ea_id', 'lang:transport_request_contract', 'trim');
		//$this->form_validation->set_rules('safa_trip_id', 'lang:transport_request_trip_id', 'trim|required');
		$this->form_validation->set_rules('safa_internaltripstatus_id', 'lang:transport_request_status', 'trim|required');
		if (PHASE > 1):
		$this->form_validation->set_rules('safa_ito_id', 'lang:transport_request_opertator', 'trim');
		else:
		$this->form_validation->set_rules('safa_ito_id', 'lang:transport_request_opertator', 'trim|required');
		endif;

		$this->form_validation->set_rules('safa_transporter_id', 'lang:transport_request_transportername', 'trim');

		$this->form_validation->set_rules('operator_reference', 'lang:transport_request_res_code', 'trim');
		$this->form_validation->set_rules('text_input', '', 'trim');
		$this->form_validation->set_rules('serial', 'lang:serial', 'trim|required|unique_col_by_uo[safa_trip_internaltrips.serial]');
		
		if ($this->input->post('text_input') && $_FILES['userfile']['name']) {
			$this->form_validation->set_rules('userfile', 'lang:userfile', 'trim|callback_fileupload');
		}


		if ($this->form_validation->run() == false) {
			$this->load->view("uo/trip_internaltrip/addfull", $data);
		} else {

			if (isset($_FILES["transport_request_res_file"])) {
				if (count($_FILES["transport_request_res_file"]['tmp_name']) > 0) {
					//Get the temp file path
					$tmpFilePath = $_FILES["transport_request_res_file"]['tmp_name'];

					//Make sure we have a filepath
					if ($tmpFilePath != "") {
						//Setup our new file path
						$newFilePath = './static/temp/uo_files/' . $_FILES["transport_request_res_file"]['name'];

						//Upload the file into the temp dir
						//To solve arabic files names problem.
						$is_file_uploaded = move_uploaded_file($tmpFilePath, iconv('utf-8', 'windows-1256', $newFilePath));
						//$is_file_uploaded=move_uploaded_file($tmpFilePath, $newFilePath);



						$file_name = $_FILES["transport_request_res_file"]['name'];
						$this->trip_internaltrip_model->attachement = $file_name;
					}
				}
			}

			if ($this->input->post('safa_trip_id')) {
				$this->trip_internaltrip_model->safa_trip_id = $this->input->post('safa_trip_id');
			} else {
				$this->trip_internaltrip_model->safa_trip_id = null;
			}
			$this->trip_internaltrip_model->safa_uo_id = session('uo_id');



			if ($this->input->post('safa_ito_id'))
			$this->trip_internaltrip_model->safa_ito_id = $this->input->post('safa_ito_id');
			if ($this->input->post('safa_transporter_id'))
			$this->trip_internaltrip_model->safa_transporter_id = $this->input->post('safa_transporter_id');

			$this->trip_internaltrip_model->safa_internaltripstatus_id = $this->input->post('safa_internaltripstatus_id');
			$this->trip_internaltrip_model->operator_reference = $this->input->post('operator_reference');
			//$this->trip_internaltrip_model->attachement = $file_name;
			$this->trip_internaltrip_model->datetime = $this->input->post('datetime');

			$this->trip_internaltrip_model->erp_company_type_id = 3;
			$this->trip_internaltrip_model->erp_company_id = $this->input->post('uo_ea_id');

			//$this->trip_internaltrip_model->trip_title = $this->input->post('trip_title');
			$this->trip_internaltrip_model->trip_title = $this->input->post('safa_uo_code').'-'.$this->input->post('serial');
			$this->trip_internaltrip_model->serial = $this->input->post('serial');
			
			$this->trip_internaltrip_model->trip_supervisors = $this->input->post('trip_supervisors');
			$this->trip_internaltrip_model->trip_supervisors_phone = $this->input->post('trip_supervisors_phone');
			$this->trip_internaltrip_model->safa_uo_contract_id = $this->input->post('safa_uo_contract_id');

			$this->trip_internaltrip_model->adult_seats = $this->input->post('adult_seats');
			$this->trip_internaltrip_model->child_seats = $this->input->post('child_seats');
			$this->trip_internaltrip_model->baby_seats = $this->input->post('baby_seats');

			if (PHASE > 1)
			$this->trip_internaltrip_model->confirmation_number = $this->input->post('confirmation_number');

			$internaltrip_id = $this->trip_internaltrip_model->save();

			if (isset($internaltrip_id)) {


				$flight_availability_id = $this->save_flight_availability();

				$flight_seats = (int)  $this->input->post('adult_seats') +(int)  $this->input->post('child_seats') +(int)  $this->input->post('baby_seats') ;
				$internal_seats = (int)  $this->input->post('adult_seats') +(int)  $this->input->post('child_seats');

				$this->db->where('safa_trip_internaltrip_id', $internaltrip_id)
				->set('erp_flight_availability_id', $flight_availability_id)
				->update('safa_trip_internaltrips');

				$safa_externaltriptype_id = post('safa_externaltriptype_id');
				$erp_port_id_to = post('erp_port_id_to');
				$erp_port_id_from = post('erp_port_id_from');
				$erp_path_type_id = post('erp_path_type_id');
				$flight_arrival_date = post('flight_arrival_date');
				$flight_date = post('flight_date');
				$seatcont = post('seats_count');
				
				$passengers = false;
				$portto = false;
				$portfrom = false;
				
				$backdate='';
				
				if($erp_port_id_to) {
					foreach ($erp_port_id_to as $key => $port_id) {
						if ($safa_externaltriptype_id[$key] == 1 && $erp_path_type_id[$key] == 2) {
							$portto = $port_id;
							$passengers = $seatcont[$key];
							$flight_arrival = $flight_arrival_date[$key];
						}
						if ($safa_externaltriptype_id[$key] == 2 && $erp_path_type_id[$key] == 1) {
							$portfrom = $erp_port_id_from[$key];
							$backdate = $flight_date[$key];
						}
					}
				}


				$erp_city_id = post('erp_city_id');
				$erp_hotel_id = post('erp_hotel_id');
				$arrival_date = post('arrival_date');
				$nights = post('nights');
				$exit_date = post('exit_date');
				$oldhotel = false;

				$movetime = 0;
				
				if($erp_hotel_id=='' ) {
					$erp_hotel_id=array();
				}
				
				if(count($erp_hotel_id)>0) {
				foreach ($erp_hotel_id as $hkey => $hotelid) {
					if (!$oldhotel) {
						$movetime = 0;
						switch ($portto) {
							case 6053:
								if(isset($erp_city_id[$key])) {
								if ($erp_city_id[$key] == 1)
								$movetime = 3;
								elseif ($erp_city_id[$key] == 2)
								$movetime = 7;
								}
								break;
							case 6056:
								if(isset($erp_city_id[$key])) {
								if ($erp_city_id[$key] == 1)
								$movetime = 7;
								elseif ($erp_city_id[$key] == 2)
								$movetime = 2;
								}
								break;
							case 6070:
								if(isset($erp_city_id[$key])) {
								if ($erp_city_id[$key] == 1)
								$movetime = 6;
								elseif ($erp_city_id[$key] == 2)
								$movetime = 6;
								}
								break;
						}
						$arrivestamp = strtotime($flight_arrival) + (3600 * $movetime);
						$arrivaldate = date('Y-m-d h:i', $arrivestamp);
						$this->db->insert('safa_internalsegments', array(
                            'safa_internalsegmenttype_id' => 1,
                            'safa_trip_internaltrip_id' => $internaltrip_id,
                            'safa_internalsegmentestatus_id' => 1,
                            'start_datetime' => $flight_arrival,
                            'end_datetime' => $arrivaldate,
                            'erp_port_id' => $portto,
                            'erp_start_hotel_id' => NULL,
                            'erp_end_hotel_id' => $hotelid,
                            'safa_tourism_place_id' => NULL,
                            'safa_uo_user_id' => NULL,
                            'seats_count' => $internal_seats
						));
					} else {
						$startdate = $arrival_date[$hkey] . " " . $this->config->item('start_moving');
						$arrivestamp = strtotime($startdate) + (3600 * 7);
						$arrivaldate = date('Y-m-d h:i', $arrivestamp);
						$this->db->insert('safa_internalsegments', array(
                            'safa_internalsegmenttype_id' => 4,
                            'safa_trip_internaltrip_id' => $internaltrip_id,
                            'safa_internalsegmentestatus_id' => 1,
                            'start_datetime' => $startdate,
                            'end_datetime' => $arrivaldate,
                            'erp_port_id' => NULL,
                            'erp_start_hotel_id' => $oldhotel,
                            'erp_end_hotel_id' => $hotelid,
                            'safa_tourism_place_id' => NULL,
                            'safa_uo_user_id' => NULL,
                            'seats_count' => $internal_seats
						));
					}

					$oldhotel = $hotelid;
				}
				

				
				switch ($portfrom) {
					case 6053:
						if (end($erp_city_id) == 1)
						$movetime = 3;
						elseif (end($erp_city_id) == 2)
						$movetime = 7;

						break;
					case 6056:
						if (end($erp_city_id) == 1)
						$movetime = 7;
						elseif (end($erp_city_id) == 2)
						$movetime = 2;
						break;
					case 6070:
						if (end($erp_city_id) == 1)
						$movetime = 6;
						elseif (end($erp_city_id) == 2)
						$movetime = 6;
						break;
				}
				$arrivestamp = strtotime($backdate) - (3600 * $movetime);
				$arrivaldate = date('Y-m-d h:i', $arrivestamp);
				
				if(count($erp_hotel_id)>0) {
				$this->db->insert('safa_internalsegments', array(
                    'safa_internalsegmenttype_id' => 2,
                    'safa_trip_internaltrip_id' => $internaltrip_id,
                    'safa_internalsegmentestatus_id' => 1,
                    'start_datetime' => $arrivaldate,
                    'end_datetime' => $backdate,
                    'erp_port_id' => $portfrom,
                    'erp_start_hotel_id' => end($erp_hotel_id),
                    'erp_end_hotel_id' => NULL,
                    'safa_tourism_place_id' => NULL,
                    'safa_uo_user_id' => NULL,
                    'seats_count' => $internal_seats
				));
				}

				$safa_tourismplace_id = post('safa_tourismplace_id');
				$tourism_date = post('tourism_date');

				if($safa_tourismplace_id) {
					if(count($safa_tourismplace_id)>0){
				foreach ($safa_tourismplace_id as $tkey => $tourism) {
					$tourismdata = $this->db->where('safa_tourismplace_id', $tourism)->get('safa_tourismplaces')->row();
					$hotelkey = array_search($tourismdata->erp_city_id, $erp_city_id);
					$movestamp = strtotime($tourism_date[$tkey]) - (3600 * 2);
					$movedate = date('Y-m-d h:i', $movestamp);
					$this->db->insert('safa_internalsegments', array(
                        'safa_internalsegmenttype_id' => 3,
                        'safa_trip_internaltrip_id' => $internaltrip_id,
                        'safa_internalsegmentestatus_id' => 1,
                        'start_datetime' => $movedate,
                        'end_datetime' => $tourism_date[$tkey],
                        'erp_port_id' => NULL,
                        'erp_start_hotel_id' => $erp_hotel_id[$hotelkey],
                        'erp_end_hotel_id' => NULL,
                        'safa_tourism_place_id' => $tourism,
                        'safa_uo_user_id' => NULL,
                        'seats_count' => $internal_seats
					));
				}
					}
				}
				}

				if ($this->input->post('uo_ea_id')) {
					//By Gouda.
					//----------- Send Notification For EA Company ------------------------------
					$msg_datetime = date('Y-m-d H:i', time());

					$this->notification_model->notification_type = 'automatic';
					$this->notification_model->erp_importance_id = 1;
					$this->notification_model->sender_type_id = 1;
					$this->notification_model->sender_id = 0;

					$this->notification_model->erp_system_events_id = 19;

					$this->notification_model->language_id = 2;
					$this->notification_model->msg_datetime = $msg_datetime;


					$internaltripstatus_name = '';
					$safa_internaltripstatus_id = $this->input->post('safa_internaltripstatus_id');
					$this->internaltrip_status_model->safa_internaltripstatus_id = $safa_internaltripstatus_id;
					$internaltrip_status_row = $this->internaltrip_status_model->get();
					if (count($internaltrip_status_row) > 0) {
						$internaltripstatus_name = $internaltrip_status_row->{name()};
					}
					$trip_name = '';
					$safa_trip_id = $this->input->post('safa_trip_id');
					$this->safa_trips_model->safa_trip_id = $safa_trip_id;
					$trip_row = $this->safa_trips_model->get();
					if (count($trip_row) > 0) {
						$trip_name = $trip_row->name;
					}

					//Link to replace.
					$link_internaltrip = "<a href='" . site_url('ea/trip_internaltrip/edit/' . $internaltrip_id) . "'  target='_blank'> $internaltrip_id </a>";

					$this->notification_model->tags = "#*safa_trip_internaltrip_id#*=$link_internaltrip*****#*internaltripstatus#*=$internaltripstatus_name*****#*the_date#*=$msg_datetime*****#*trip#*=$trip_name";
					$this->notification_model->parent_id = '';

					$erp_notification_id = $this->notification_model->save();

					//-------- Notification Details ---------------
					$this->notification_model->detail_erp_notification_id = $erp_notification_id;
					$this->notification_model->detail_receiver_type = 3;

					//$this->contracts_model->safa_uo_contract_id = $this->input->post('uo_contract_id');
					//$contracts_row=$this->contracts_model->search();
					//if(count($contracts_row)>0) {
					//	$this->notification_model->detail_receiver_id = $contracts_row[0]->safa_ea_id ;
					$this->notification_model->detail_receiver_id = $this->input->post('uo_ea_id');
					$this->notification_model->saveDetails();
					//}
					//--------------------------------
					//-------------------------------------------------------------------
					//----------- Send Notification For ITO Company ------------------------------
					if ($this->input->post('safa_ito_id') != '') {
						$msg_datetime = date('Y-m-d H:i', time());

						$this->notification_model->notification_type = 'automatic';
						$this->notification_model->erp_importance_id = 1;
						$this->notification_model->sender_type_id = 1;
						$this->notification_model->sender_id = 0;

						$this->notification_model->erp_system_events_id = 19;

						$this->notification_model->language_id = 2;
						$this->notification_model->msg_datetime = $msg_datetime;


						$internaltripstatus_name = '';
						$safa_internaltripstatus_id = $this->input->post('safa_internaltripstatus_id');
						$this->internaltrip_status_model->safa_internaltripstatus_id = $safa_internaltripstatus_id;
						$internaltrip_status_row = $this->internaltrip_status_model->get();
						if (count($internaltrip_status_row) > 0) {
							$internaltripstatus_name = $internaltrip_status_row->{name()};
						}
						$trip_name = '';
						$safa_trip_id = $this->input->post('safa_trip_id');
						$this->safa_trips_model->safa_trip_id = $safa_trip_id;
						$trip_row = $this->safa_trips_model->get();
						if (count($trip_row) > 0) {
							$trip_name = $trip_row->name;
						}

						//Link to replace.
						$link_internaltrip = "<a href='" . site_url('ito/trip_internaltrip/edit_incoming/' . $internaltrip_id) . "'  target='_blank' > $internaltrip_id </a>";

						$this->notification_model->tags = "#*safa_trip_internaltrip_id#*=$link_internaltrip*****#*internaltripstatus#*=$internaltripstatus_name*****#*the_date#*=$msg_datetime*****#*trip#*=$trip_name";
						$this->notification_model->parent_id = '';

						$erp_notification_id = $this->notification_model->save();

						//-------- Notification Details ---------------
						$this->notification_model->detail_erp_notification_id = $erp_notification_id;
						$this->notification_model->detail_receiver_type = 5;
						$this->notification_model->detail_receiver_id = $this->input->post('safa_ito_id');
						$this->notification_model->saveDetails();

						//--------------------------------
					}
					//-------------------------------------------------------------------
				}
			}
			$this->db->trans_complete();

			$this->load->view('redirect_message', array('msg' => lang('global_added_message'),
                'url' => site_url('uo/trip_internaltrip'),
                'model_title' => lang('node_title'), 'action' => lang('add_transport_request')));
		}
	}

	function save_flight_availability() {
		$this->db->set('pnr', post('name'));
		// $this->db->set('safa_ea_id', session('ea_id'));
		$this->db->set('by_trip', 1);
		$this->db->insert('erp_flight_availabilities');
		$flight_availability_id = $this->db->insert_id();

		$arr = array(
            'erp_airline_id',
            'flight_number',
            'erp_flight_class_id',
            'flight_date',
            'flight_arrival_date',
            'erp_port_id_from',
            'erp_port_id_to',
            'airports',
            'erp_flight_status_id',
            'seats_count',
            'safa_externaltriptype_id',
            'erp_path_type_id'
            );

            foreach ($arr as $item)
            $$item = post($item);

            if($erp_airline_id) {
            	foreach ($erp_airline_id as $key => $airline) {


            		$this->db->set('erp_flight_availability_id', $flight_availability_id);
            		$this->db->set('erp_airline_id', $erp_airline_id[$key]);


            		$current_flight_datetime = $flight_date[$key];
            		$current_flight_datetime_arr = explode(' ', $current_flight_datetime);
            		$current_flight_date = $current_flight_datetime_arr[0];
            		$current_flight_time = '';
            		if (isset($current_flight_datetime_arr[1])) {
            			$current_flight_time = $current_flight_datetime_arr[1];
            		}

            		$current_arrival_datetime = $flight_arrival_date[$key];
            		$current_arrival_datetime_arr = explode(' ', $current_arrival_datetime);
            		$current_arrival_date = $current_arrival_datetime_arr[0];
            		$current_arrival_time = '';
            		if (isset($current_arrival_datetime_arr[1])) {
            			$current_arrival_time = $current_arrival_datetime_arr[1];
            		}

            		$this->db->set('flight_date', $current_flight_date);
            		$this->db->set('arrival_date', $current_arrival_date);
            		$this->db->set('departure_time', $current_flight_time);
            		$this->db->set('arrival_time', $current_arrival_time);

            		$this->db->set('flight_number', $flight_number[$key]);
            		$this->db->set('erp_flight_class_id', $erp_flight_class_id[$key]);
            		$this->db->set('erp_port_id_from', $erp_port_id_from[$key]);
            		$this->db->set('erp_port_id_to', $erp_port_id_to[$key]);

            		$this->db->set('erp_path_type_id', $erp_path_type_id[$key]);
            		$this->db->set('safa_externaltriptype_id', $safa_externaltriptype_id[$key]);

            		$this->db->set('erp_flight_status_id', $erp_flight_status_id[$key]);
            		$this->db->set('seats_count', $seats_count[$key]);



            		$this->db->insert('erp_flight_availabilities_detail');
            	}
            }

            return $flight_availability_id;
	}

	public function edit($id = FALSE) {

		if (!$id)
		show_404();
		$this->trip_internaltrip_model->safa_trip_internaltrip_id = $id;

		$data['item'] = $item = $this->trip_internaltrip_model->get();
		if (!$data['item'])
		show_404();

		$trip_id = $data['item']->safa_trip_id;
		//$data["contract_id"] = $this->trip_internaltrip_model->get_trip_contracts($trip_id);

		
		$this->safa_uos_model->safa_uo_id = session('uo_id');
		$safa_uo_row = $this->safa_uos_model->get();
		$data['safa_uo_code'] = $safa_uo_row->code;
		
		//$safa_uo_row = $this->trip_internaltrip_model->get_max_serial_by_uo(session('uo_id'));
		//$data['safa_uo_serial'] = $safa_uo_row->max_serial+1;
		
		

		if ($item->safa_uo_id != '') {
			$this->contracts_model->by_eas_id = $item->erp_company_id;
			$this->contracts_model->safa_uo_id = session('uo_id');
			$contracts_rows = $this->contracts_model->search();
		} else {
			$this->contracts_model->by_eas_id = $item->safa_ea_id;
			$this->contracts_model->safa_uo_id = session('uo_id');

			$contracts_rows = $this->contracts_model->search();
		}

		$data["uo_ea_id"] = 0;
		if (count($contracts_rows) > 0) {
			$data["uo_ea_id"] = $contracts_rows[0]->safa_ea_id;
		}

		//$data["uo_contracts"] = $this->create_contracts_array();
		$data["uo_eas"] = $this->create_eas_array();

		$data["safa_uo_contracts"] = ddgen('safa_uo_contracts', array('safa_uo_contract_id', name()), array('safa_uo_id' => session('uo_id')), FALSE, FALSE);

		$data["ea_trips"] = $this->create_trips_array($data['item']->safa_uo_contract_id);

		$data["transporters"] = $this->create_transportes_array();

		$data["safa_intrernaltripstatus"] = ddgen("safa_internaltripstatus", array("safa_internaltripstatus_id", name()));
		$data["safa_ito"] = $data["safa_ito"] = $this->get_uo_contracts_itos();
		/* --------- get the internal passage ----------------------------------------------------- */
		$this->internalpassages_model->safa_trip_internaltrip_id = $id;
		$this->internalpassages_model->order_by = array("safa_internalsegments.start_datetime", 'ASC');
		$data["internalpassages"] = $this->internalpassages_model->get();
		$data["trip_internal_id"] = $id;
		/* ---------------------------------------------------------------------------------------------------------------------------------------- */
		$this->load->library("form_validation");
		/*         * ************* */
		$this->form_validation->set_rules('datetime', 'lang:transport_request_date', 'trim|required');
		//$this->form_validation->set_rules('uo_ea_id', 'lang:transport_request_contract', 'trim|required');
		//$this->form_validation->set_rules('safa_trip_id', 'lang:transport_request_trip_id', 'trim|required');
		$this->form_validation->set_rules('safa_internaltripstatus_id', 'lang:transport_request_status', 'trim|required');
		if (PHASE > 1):
		$this->form_validation->set_rules('safa_ito_id', 'lang:transport_request_opertator', 'trim');
		else:
		$this->form_validation->set_rules('safa_ito_id', 'lang:transport_request_opertator', 'trim|required');
		endif;
		$this->form_validation->set_rules('safa_transporter_id', 'lang:transport_request_transportername', 'trim');
		$this->form_validation->set_rules('safa_uo_contract_id', 'lang:transport_request_contract', 'trim');

		//$this->form_validation->set_rules('serial', 'lang:serial', 'trim|required|unique_col[safa_trip_internaltrips.serial]');
		$this->form_validation->set_rules('buses_count', 'lang:buses_count', 'trim|required');

		$this->form_validation->set_rules('operator_reference', 'lang:transport_request_res_code', 'trim');
		$this->form_validation->set_rules('text_input', '', 'trim');
		if ($this->input->post('text_input') && $_FILES['userfile']['name']) {
			$this->form_validation->set_rules('userfile', 'lang:userfile', 'trim|callback_fileupload');
		}
		if ($this->form_validation->run() == false) {
			$this->load->view("uo/trip_internaltrip/edit", $data);
		} else {

			if (isset($_FILES["transport_request_res_file"])) {
				if (count($_FILES["transport_request_res_file"]['tmp_name']) > 0) {
					$tmpFilePath = $_FILES["transport_request_res_file"]['tmp_name'];

					if ($tmpFilePath != "") {
						$file_name = $_FILES["transport_request_res_file"]['name'];
						$newFilePath = './static/temp/uo_files/' . $file_name;

						$is_file_uploaded = move_uploaded_file($tmpFilePath, iconv('utf-8', 'windows-1256', $newFilePath));


						$this->trip_internaltrip_model->attachement = $file_name;
					}
				}
			}

			$this->trip_internaltrip_model->safa_trip_internaltrip_id = $id;

			if ($this->input->post('safa_trip_id')) {
				$this->trip_internaltrip_model->safa_trip_id = $this->input->post('safa_trip_id');
			} else {
				$this->trip_internaltrip_model->safa_trip_id = null;
			}
			$this->trip_internaltrip_model->safa_uo_id = session('uo_id');

			if ($this->input->post('safa_ito_id'))
			$this->trip_internaltrip_model->safa_ito_id = $this->input->post('safa_ito_id');
			else
			$this->trip_internaltrip_model->safa_ito_id = NULL;
			if ($this->input->post('safa_transporter_id'))
			$this->trip_internaltrip_model->safa_transporter_id = $this->input->post('safa_transporter_id');
			else
			$this->trip_internaltrip_model->safa_transporter_id = NULL;

			$this->trip_internaltrip_model->safa_internaltripstatus_id = $this->input->post('safa_internaltripstatus_id');
			$this->trip_internaltrip_model->operator_reference = $this->input->post('operator_reference');
			//$this->trip_internaltrip_model->attachement = $file_name;
			$this->trip_internaltrip_model->datetime = $this->input->post('datetime');

			//$this->trip_internaltrip_model->erp_company_type_id = 3;
			//$this->trip_internaltrip_model->erp_company_id = $this->input->post('uo_ea_id');

			$this->trip_internaltrip_model->safa_uo_contract_id = $this->input->post('safa_uo_contract_id');

			//$this->trip_internaltrip_model->serial = $this->input->post('serial');
			$this->trip_internaltrip_model->buses_count = $this->input->post('buses_count');

			//if ($file_name)// uploading when the file exist//
			//$this->move_upload();
			$internaltrip_id = $this->trip_internaltrip_model->save();


			//By Gouda.
			//----------- Send Notification For EA Company ------------------------------
			$msg_datetime = date('Y-m-d H:i', time());

			$this->notification_model->notification_type = 'automatic';
			$this->notification_model->erp_importance_id = 1;
			$this->notification_model->sender_type_id = 1;
			$this->notification_model->sender_id = 0;

			$this->notification_model->erp_system_events_id = 20;

			$this->notification_model->language_id = 2;
			$this->notification_model->msg_datetime = $msg_datetime;


			$internaltripstatus_name = '';
			$safa_internaltripstatus_id = $this->input->post('safa_internaltripstatus_id');
			$this->internaltrip_status_model->safa_internaltripstatus_id = $safa_internaltripstatus_id;
			$internaltrip_status_row = $this->internaltrip_status_model->get();
			if (count($internaltrip_status_row) > 0) {
				$internaltripstatus_name = $internaltrip_status_row->{name()};
			}
			$trip_name = '';
			$safa_trip_id = $this->input->post('safa_trip_id');
			$this->safa_trips_model->safa_trip_id = $safa_trip_id;
			$trip_row = $this->safa_trips_model->get();
			if (count($trip_row) > 0) {
				$trip_name = $trip_row->name;
			}

			//Link to replace.
			$link_internaltrip = "<a href='" . site_url('ea/trip_internaltrip/edit/' . $id) . "'  target='_blank'> $id </a>";

			$this->notification_model->tags = "#*safa_trip_internaltrip_id#*=$link_internaltrip*****#*internaltripstatus#*=$internaltripstatus_name*****#*the_date#*=$msg_datetime*****#*trip#*=$trip_name";
			$this->notification_model->parent_id = '';

			$erp_notification_id = $this->notification_model->save();

			//-------- Notification Details ---------------
			$this->notification_model->detail_erp_notification_id = $erp_notification_id;
			$this->notification_model->detail_receiver_type = 3;

			$this->contracts_model->safa_uo_contract_id = $this->input->post('uo_contract_id');
			$contracts_row=$this->contracts_model->search();
			if(count($contracts_row)>0) {
				$this->notification_model->detail_receiver_id = $contracts_row[0]->safa_ea_id ;
				//$this->notification_model->detail_receiver_id = $this->input->post('uo_ea_id');
				$this->notification_model->saveDetails();
			}
			//--------------------------------
			//-------------------------------------------------------------------
			//----------- Send Notification For ITO Company ------------------------------
			if ($this->input->post('safa_ito_id') != '') {
				$msg_datetime = date('Y-m-d H:i', time());

				$this->notification_model->notification_type = 'automatic';
				$this->notification_model->erp_importance_id = 1;
				$this->notification_model->sender_type_id = 1;
				$this->notification_model->sender_id = 0;

				$this->notification_model->erp_system_events_id = 20;

				$this->notification_model->language_id = 2;
				$this->notification_model->msg_datetime = $msg_datetime;


				$internaltripstatus_name = '';
				$safa_internaltripstatus_id = $this->input->post('safa_internaltripstatus_id');
				$this->internaltrip_status_model->safa_internaltripstatus_id = $safa_internaltripstatus_id;
				$internaltrip_status_row = $this->internaltrip_status_model->get();
				if (count($internaltrip_status_row) > 0) {
					$internaltripstatus_name = $internaltrip_status_row->{name()};
				}
				$trip_name = '';
				$safa_trip_id = $this->input->post('safa_trip_id');
				$this->safa_trips_model->safa_trip_id = $safa_trip_id;
				$trip_row = $this->safa_trips_model->get();
				if (count($trip_row) > 0) {
					$trip_name = $trip_row->name;
				}

				//Link to replace.
				$link_internaltrip = "<a href='" . site_url('ito/trip_internaltrip/edit_incoming/' . $id) . "'  target='_blank'> $id </a>";

				$this->notification_model->tags = "#*safa_trip_internaltrip_id#*=$link_internaltrip*****#*internaltripstatus#*=$internaltripstatus_name*****#*the_date#*=$msg_datetime*****#*trip#*=$trip_name";
				$this->notification_model->parent_id = '';

				$erp_notification_id = $this->notification_model->save();

				//-------- Notification Details ---------------
				$this->notification_model->detail_erp_notification_id = $erp_notification_id;
				$this->notification_model->detail_receiver_type = 5;
				$this->notification_model->detail_receiver_id = $this->input->post('safa_ito_id');
				$this->notification_model->saveDetails();

				//--------------------------------
			}
			//-------------------------------------------------------------------



			//            $this->load->view('redirect_message', array('msg' => lang('global_updated_message'),
			//                'url' => site_url('uo/trip_internaltrip'),
			//                'id' => $this->uri->segment("4"),
			//                'model_title' => lang('node_title'), 'action' => lang('edit_trip_internaltrip')));

			$this->load->view('confirmation_message', array('msg' => lang('are_you_want_to_print_report'),
                    'back_url' => site_url('uo/trip_internaltrip/edit/'. $id),
                    'confirmation_url' => site_url('uo/trip_internaltrip/internalsegments_report/' . $id ),
                    'model_title' => lang('node_title'), 'action' => lang('add_transport_request'),
			));

		}
	}

	function delete_($id = false) {
		if (!$id)
		show_404();

		$this->trip_internaltrip_model->safa_trip_internaltrip_id = $id;
		if (!$this->trip_internaltrip_model->delete())
		show_404();
		$this->load->view('redirect_message', array('msg' => lang('global_deleted_message'),
            'url' => site_url('uo/trip_internaltrip'),
            'model_title' => lang('node_title'), 'action' => ''));
	}

	function delete($id = false)
	{
		if (!$id)
		show_404();

		$this->trip_internaltrip_model->safa_trip_internaltrip_id = $id;
		$this->trip_internaltrip_model->deleted = 1;
		$this->trip_internaltrip_model->save();
		
		$this->load->view('redirect_message', array('msg' => lang('global_deleted_message'),
            'url' => site_url('uo/trip_internaltrip'),
            'model_title' => lang('node_title'), 'action' => ''));
	}
	
	function delete_all() {
		if ($this->input->post("delete_items")) {
			foreach ($this->input->post("delete_items") as $item) {
				$this->trip_internaltrip_model->safa_trip_internaltrip_id = $item;

				$this->trip_internaltrip_model->delete();
			}
		} else
		show_404();
		redirect("uo/trip_internaltrip/index");
	}

	function search() {
		if ($this->input->get("safa_trip_internaltrip_id"))
		$this->trip_internaltrip_model->safa_trip_internaltrip_id = $this->input->get("safa_trip_internaltrip_id");
		if ($this->input->get("safa_trip_id"))
		$this->trip_internaltrip_model->safa_trip_id = $this->input->get("safa_trip_id");
		if ($this->input->get("safa_ito_id"))
		$this->trip_internaltrip_model->safa_ito_id = $this->input->get("safa_ito_id");
		if ($this->input->get("safa_transporter_id"))
		$this->trip_internaltrip_model->safa_transporter_id = $this->input->get("safa_transporter_id");
		if ($this->input->get("safa_tripstatus_id"))
		$this->trip_internaltrip_model->safa_tripstatus_id = $this->input->get("safa_tripstatus_id");
		if ($this->input->get("safa_internaltripstatus_id"))
		$this->trip_internaltrip_model->safa_internaltripstatus_id = $this->input->get("safa_internaltripstatus_id");
		if ($this->input->get("operator_reference"))
		$this->trip_internaltrip_model->operator_reference = $this->input->get("operator_reference");
		if ($this->input->get("attachement"))
		$this->trip_internaltrip_model->attachement = $this->input->get("attachement");
		if ($this->input->get("datetime"))
		$this->trip_internaltrip_model->datetime = $this->input->get("datetime");
		/* search utility */
		if ($this->input->get("uo_ea_id")) {
			$this->trip_internaltrip_model->erp_company_type_id = 3;
			$this->trip_internaltrip_model->erp_company_id = $this->input->get("uo_ea_id");
		}

		if ($this->input->get("safa_uo_contract_id")) {
			$this->trip_internaltrip_model->safa_uo_contract_id = $this->input->get("safa_uo_contract_id");
		}
			
		/* search utility */
	}

	function create_contracts_array() {
		$result = $this->trip_internaltrip_model->get_uo_contracts();
		$contracts = array();
		$contracts[""] = lang('global_select_from_menu');
		$name = name();
		foreach ($result as $con_id => $contract) {
			$contracts[$contract->contract_id] = $contract->$name;
		}
		return $contracts;
	}

	function create_eas_array() {
		$result = $this->trip_internaltrip_model->get_uo_eas();
		$eas = array();
		$eas[""] = lang('global_select_from_menu');
		foreach ($result as $row) {
			$eas[$row->safa_ea_id] = $row->safa_ea_name;
		}
		return $eas;
	}

	function create_trips_array($safa_uo_contract_id) {
		$result = $this->trip_internaltrip_model->get_uo_contract_trips($safa_uo_contract_id);
		$eas = array();
		$eas[""] = lang('global_select_from_menu');
		foreach ($result as $key => $value) {
			$eas[$value->safa_trip_id] = $value->name;
		}
		return $eas;
	}

	function get_uo_contracts_itos() { // get the itos that involved in  the same contracts of that uo
		$result = $this->trip_internaltrip_model->get_uo_contracts_itos();
		$itos = array();
		$itos[""] = lang('global_select_from_menu');
		$name = name();
		foreach ($result as $itos_id => $ito) {
			$itos[$ito->safa_ito_id] = $ito->$name;
		}
		return $itos;
	}

	function create_transportes_array() {
		$result = $this->trip_internaltrip_model->get_uo_transporters_withContracts();
		$transporters = array();
		$transporters[""] = lang('global_select_from_menu');
		$name = name();
		foreach ($result as $transporter_id => $transporter) {
			$transporters[$transporter->transporter_id] = $transporter->$name;
		}
		return $transporters;
	}

	function get_uo_contract_trips($safa_uo_contract_id = FALSE) {
		$this->layout = 'ajax'; //  to prevent the hook in loading the header in the response//
		$trips_ea = $this->trip_internaltrip_model->get_uo_contract_trips($safa_uo_contract_id);
		$trips = array();
		echo "<option value=''>" . lang('global_select_from_menu') . "</option>";
		if (isset($trips_ea) && count($trips_ea) > 0) {

			foreach ($trips_ea as $trip) {
				echo "<option value='" . $trip->safa_trip_id . "'>" . $trip->name . "</option>";
			}
		}
	}

	function fileupload() {
		$this->change_upload_name();
		$config['upload_path'] = './static/temp/uo_files';
		$config['allowed_types'] = 'pdf|jpg|jpeg';
		$config['max_size'] = '1024';
		$config['max_width'] = '1024';
		$config['max_height'] = '768';
		$config['overwrite'] = 'true';
		$config['encrypt_name'] = 'true';
		$this->load->library('upload', $config);
		if (!$this->upload->do_upload()) {
			$error = $this->upload->display_errors();
			$this->form_validation->set_message('fileupload', $error);
			return false;
		}
	}

	function move_upload() {
		if (file_exists('./static/temp/uo_files/' . session('internaltrip_file_upload_time') . $this->input->post('text_input'))) {
			if (!copy('./static/temp/uo_files/' . session('internaltrip_file_upload_time') . $this->input->post('text_input'), './static/uo_files/' . session('internaltrip_file_upload_time') . $this->input->post('text_input')))
			if (!move_uploaded_file('./static/temp/uo_files/' . session('internaltrip_file_upload_time') . $this->input->post('text_input'), './static/uo_files/' . session('internaltrip_file_upload_time') . $this->input->post('text_input')))
			echo "<script>alert('error uploading a file')</script>";
		} else
		echo "<script>alert('error uploading a file')</script>";
	}

	function get_ito_refcode($ito_id = false) {
		$this->layout = 'ajax';
		$this->load->model('itos_model');
		$this->itos_model->custom_select = 'reference_code';
		$this->itos_model->safa_ito_id = $ito_id;
		$result = $this->itos_model->get();
		if (isset($result))
		echo $result->reference_code;
	}

	function change_upload_name() {
		$upload_time = rand(0, 9999) . time();
		session('internaltrip_file_upload_time', $upload_time);
		$file_name = $_FILES['userfile']['name'];
		$uplaod_name = session('internaltrip_file_upload_time') . $file_name;
		$_FILES['userfile']['name'] = $uplaod_name;
	}

	function get_hotel_by_city($cityid = false) {
		$hotels = ddgen('erp_hotels', array('erp_hotel_id', name()), array('erp_city_id' => $cityid), array(name(), 'ASC'), TRUE);
		$return = '';
		foreach ($hotels as $key => $value)
		$return .= "<option value='$key'>$value</option>\n";

		$this->layout = 'ajax';
		echo $return;
		die();
	}

	function internalsegments_report($id)
	{
		$this->layout = 'ajax';
			
		$this->trip_details_model->safa_trip_internaltrip_id = $id;
		$data['internalsegments'] = $this->trip_details_model->get_internal_segments();

		$data['safa_uo_code'] ='';
    	if (session('uo_id')) {
	    	$this->safa_uos_model->safa_uo_id = session('uo_id');
			$safa_uo_row = $this->safa_uos_model->get();
			$data['safa_uo_code'] = $safa_uo_row->code;
    	} else if (session('ea_id')) {
    			$this->contracts_model->safa_uo_contract_id =$data['internalsegments'][0]->safa_uo_contract_id;
				$contracts_row = $this->contracts_model->get();
				if(count($contracts_row)>0) {
					$safa_uo_code='';
					$safa_uo_serial='';

					$this->safa_uos_model->safa_uo_id = $contracts_row->safa_uo_id;
					$safa_uo_row = $this->safa_uos_model->get();
					if(count($safa_uo_row)>0) {
						$data['safa_uo_code'] = $safa_uo_row->code;
					}

				}
    	}
		$this->load->view('uo/trips/internalsegments', $data);
	}

	function add_hotel_popup($hotel_counter)
	{
		$this->layout = 'js_new';
			
		$data =array();
		$data['hotel_counter'] = $hotel_counter;
		if (!isset($_POST['smt_save'])) {
			$this->load->view('uo/trip_internaltrip/add_hotel_popup', $data);
		} else {
			
			$this->load->library("form_validation");
			$this->form_validation->set_rules('name_ar', 'lang:hotels_name_ar', 'trim');
        	$this->form_validation->set_rules('name_la', 'lang:hotels_name_la', 'trim');
        	//$this->form_validation->set_rules('erp_city_id', 'lang:hotels_erp_city_id', 'trim');
			
			if ($this->form_validation->run() == false) {

            	$this->load->view("uo/trip_internaltrip/add_hotel_popup", $data);
	        } else {
	            $this->hotels_model->name_ar = $this->input->post('name_ar');
	            $this->hotels_model->name_la = $this->input->post('name_la');
	            $this->hotels_model->erp_city_id = $this->input->post('erp_city_id');
	            
	            $hotel_id = $this->hotels_model->save();
	            
	       
	        //----------- Send Notification For Company ------------------------------
            $msg_datetime = date('Y-m-d H:i', time());

            $this->notification_model->notification_type = 'automatic';
            $this->notification_model->erp_importance_id = 1;
            $this->notification_model->sender_type_id = 1;
            $this->notification_model->sender_id = 0;

            $this->notification_model->erp_system_events_id = 25;

            $this->notification_model->language_id = 2;
            $this->notification_model->msg_datetime = $msg_datetime;

            /*
              $current_phase_name=$this->input->post('current_phase');
              $next_phase_id=$this->input->post('next_phase');
              $this->safa_contract_phases_model->safa_contract_phases_id=$next_phase_id;
              $next_phase_row=$this->safa_contract_phases_model->get();
              $next_phase_name=$next_phase_row->{name()};

              $tags="#*current_contract_phase#*=$current_phase_name*****#*next_contract_phase#*=$next_phase_name";
             */



            //Link to replace.
            $link_hotels = "<a href='" . site_url('hotels/hotel_details/' . $hotel_id) . "'  target='_blank'> " . $this->input->post('name_ar') . " </a>";

            $tags = "#*the_date#*=" . date('Y-m-d H:i', time()) . "*****#*hotel_id#*=$link_hotels*****#*safa_uo#*=".session('uo_name');

            $this->notification_model->tags = $tags;
            $this->notification_model->parent_id = '';

            $erp_notification_id = $this->notification_model->save();

            //-------- Notification Details ---------------
            $this->notification_model->detail_erp_notification_id = $erp_notification_id;

            $this->notification_model->detail_receiver_type = 1;
            
            $this->notification_model->detail_receiver_id = 1;
            $this->notification_model->saveDetails();
            //--------------------------------
            //-------------------------------------------------------------------
	            
	            
	            
	            $this->load->view('message_popup', array('msg' => lang('hotel_added_successfully'),
	            'url' => site_url("uo/trip_internaltrip/add_hotel_popup"),
	            'id' => $this->uri->segment("4"),
	            'model_title' => lang('add_hotel'), 'action' => lang('add_hotel')));
	        }
	        
			
		}
	}

	function add_transporter_popup()
	{
		$this->layout = 'js_new';
			
		$data =array();
		
		$data['erp_countries'] = ddgen('erp_countries', array('erp_country_id', name()));
		
		if (!isset($_POST['smt_save'])) {
			$this->load->view('uo/trip_internaltrip/add_transporter_popup', $data);
		} else {
			
			$this->load->library("form_validation");
			$this->form_validation->set_rules('erp_country_id', 'lang:safa_transporter_erp_country_id', 'trim|required');
	        $this->form_validation->set_rules('name_ar', 'lang:safa_transporter_name_ar', 'trim|required|is_unique[safa_transporters.name_ar]');
	        $this->form_validation->set_rules('name_la', 'lang:safa_transporter_name_la', 'trim|required|is_unique[safa_transporters.name_la]');
	        $this->form_validation->set_rules('code', 'lang:safa_transporter_code', 'trim|required|max_length[15]');
			
			if ($this->form_validation->run() == false) {
            	$this->load->view("uo/trip_internaltrip/add_transporter_popup", $data);
	        } else {
	        	
	        	$this->transporters_model->erp_country_id = $this->input->post('erp_country_id');
	            $this->transporters_model->name_ar = $this->input->post('name_ar');
	            $this->transporters_model->name_la = $this->input->post('name_la');
	            $this->transporters_model->erp_transportertype_id = 2;
	            $this->transporters_model->code = $this->input->post('code');
	            $transporter_id = $this->transporters_model->save();
	            
	       
	        //----------- Send Notification For Company ------------------------------
//            $msg_datetime = date('Y-m-d H:i', time());
//
//            $this->notification_model->notification_type = 'automatic';
//            $this->notification_model->erp_importance_id = 1;
//            $this->notification_model->sender_type_id = 1;
//            $this->notification_model->sender_id = 0;
//
//            $this->notification_model->erp_system_events_id = 25;
//
//            $this->notification_model->language_id = 2;
//            $this->notification_model->msg_datetime = $msg_datetime;
//
//            /*
//              $current_phase_name=$this->input->post('current_phase');
//              $next_phase_id=$this->input->post('next_phase');
//              $this->safa_contract_phases_model->safa_contract_phases_id=$next_phase_id;
//              $next_phase_row=$this->safa_contract_phases_model->get();
//              $next_phase_name=$next_phase_row->{name()};
//
//              $tags="#*current_contract_phase#*=$current_phase_name*****#*next_contract_phase#*=$next_phase_name";
//             */
//
//
//
//            //Link to replace.
//            $link_hotels = "<a href='" . site_url('hotels/hotel_details/' . $hotel_id) . "'  target='_blank'> " . $this->input->post('name_ar') . " </a>";
//
//            $tags = "#*the_date#*=" . date('Y-m-d H:i', time()) . "*****#*hotel_id#*=$link_hotels*****#*safa_uo#*=".session('uo_name');
//
//            $this->notification_model->tags = $tags;
//            $this->notification_model->parent_id = '';
//
//            $erp_notification_id = $this->notification_model->save();
//
//            //-------- Notification Details ---------------
//            $this->notification_model->detail_erp_notification_id = $erp_notification_id;
//
//            $this->notification_model->detail_receiver_type = 1;
//            
//            $this->notification_model->detail_receiver_id = 1;
//            $this->notification_model->saveDetails();
            //--------------------------------
            //-------------------------------------------------------------------
	            
	            
	            
	            $this->load->view('message_popup', array('msg' => lang('transporter_added_successfully'),
	            'url' => site_url("uo/trip_internaltrip/add_transporter_popup"),
	            'id' => $this->uri->segment("4"),
	            'model_title' => lang('add_transporter'), 'action' => lang('add_transporter')));
	        }
	        
			
		}
	}
	
	function get_transporters() 
	{
		$erp_airlines_query = $this->db->query('select ' . FSDB . '.fs_airlines.fs_airline_id, CONCAT(safa_transporters.'.name().',\' \', ' . FSDB . '.`fs_airlines`.`iata`) as transporter_code_name from  ' . FSDB . '.`fs_airlines`, safa_transporters where  safa_transporters.code=' . FSDB . '.fs_airlines.iata order by  transporter_code_name');
		$erp_airlines_rows = $erp_airlines_query->result();  
		//$erp_airlines_rows = $this->db->order_by('iata', 'asc')->where("iata <> ''")->get(FSDB . '.fs_airlines')->result();
		$erp_airlines_arr = array();
		foreach ($erp_airlines_rows as $erp_airlines_row) {
			$erp_airlines_arr[$erp_airlines_row->fs_airline_id] = $erp_airlines_row->transporter_code_name;
		}
		
		
		$return = '';
		foreach ($erp_airlines_arr as $key => $value)
		$return .= "<option value='$key'>$value</option>\n";

		$this->layout = 'ajax';
		echo $return;
		die();
	}
	
}

/* End of file trip_internaltrip.php */
/* Location: ./application/controllers/trip_internaltrip.php */
