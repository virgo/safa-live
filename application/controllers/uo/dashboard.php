<?php

class Dashboard extends Safa_Controller {

    public $module = "currently_report";
    public $uo_id;
    public $arriving_pane = "";
    public $departing_pane = "";

    public function __construct() {
        parent::__construct();
        permission();
        $this->layout = 'new';
        //Side menu session, By Gouda.
        session('side_menu_id', 3);

        $this->load->library('gcharts');
        $this->load->model('safa_trips_model');
        
        $this->uo_id = session('uo_id');
        $this->load->model('trips_reports_model');
        $this->lang->load('trips_reports');
        $this->load->helper('trip');
        
        $this->load->library('flight_states');
        
    }

    public function index() {
        $data = array();
        
        /*
        $this->load->view("uo/charts/index", $data);
        return true;
        */
        
        $data['cur_year'] = date('Y'); 
        
        $this->safa_trips_model->safa_uo_id=  $this->uo_id ;
        $data['eas_rows']= $this->safa_trips_model->get_eas();
        
        $data['total_mofa_count']='';
        $data['total_mofa_count_for_ksa']='';
        $data['leave_in_24_hours_count']='';
        
        $total_mofa_count_row= $this->safa_trips_model->get_total_mofa_count();
        if(count($total_mofa_count_row)>0) {
        	$data['total_mofa_count']= $total_mofa_count_row->mofa_count;
        }
        $total_mofa_count_for_ksa_row= $this->safa_trips_model->get_total_mofa_count_in_KSA();
        if(count($total_mofa_count_for_ksa_row)>0) {
        	$data['total_mofa_count_for_ksa']= $total_mofa_count_for_ksa_row->mofa_count;
        }
        
        $leave_in_24_hours_count_row= $this->safa_trips_model->get_total_mofa_count_in_KSA(true);
        if(count($leave_in_24_hours_count_row)>0) {
        	$data['leave_in_24_hours_count']= $leave_in_24_hours_count_row->mofa_count;
        }
        
        $data['uo_trips_countries']=$this->safa_trips_model->get_contries();
        
        //init value of link
        if (!$this->input->get("arrival")) {
            $_GET['arrival'] = 'today';
            $this->trips_reports_model->arriving_today = true;
        } elseif ($this->input->get("arrival") == "today") {
            $this->trips_reports_model->arriving_today = true;
        } elseif ($this->input->get("arrival") == "twm") {
            $this->trips_reports_model->arriving_twm = true;
        }

        if (!$this->input->get("departure")) {
            $_GET['departure'] = 'today';
            $this->trips_reports_model->departing_today = true;
        } elseif ($this->input->get("departure") == "today") {
            $this->trips_reports_model->departing_today = true;
        } elseif ($this->input->get("departure") == "twm") {
            $this->trips_reports_model->departing_twm = true;
        }
        //add extra title to box of departing
        if ($this->trips_reports_model->departing_twm) {
            $this->departing_pane = " - " . lang('twm');
        } else {
            $this->departing_pane = " - " . lang('today');
        }

        if ($this->trips_reports_model->arriving_twm) {
            $this->arriving_pane = " - " . lang('twm');
        } else {
            $this->arriving_pane = " - " . lang('today');
        }

        $this->trips_reports_model->uo_id= session('uo_id');
        $data['departing'] = $this->trips_reports_model->departing_reports($this->uo_id, 'filters');
        $data['arriving'] = $this->trips_reports_model->arriving_reports($this->uo_id, 'filters');

        if ($this->input->get('export_arr_report') || $this->input->get('export_dep_report')) {
            $this->layout = 'js';
            $this->load->helper('download');
            $data['print_statement'] = "";
            $data = $this->load->view("uo/trips_reports/currently_report_excel", $data, TRUE);
            $name = 'cur-' . date('Y-m-d-His') . '.xls';
            force_download($name, $data);
        } elseif ($this->input->get('print_arr_report') || $this->input->get('print_dep_report')) {
            $this->layout = 'js';
            $data['print_statement'] = "<script>print()</script>";
            $this->load->view("uo/trips_reports/currently_report_excel", $data);
        } else {

            $this->load->view("uo/dashboard", $data);

        }
        
    }
    
    function charts() 
    {
    	$this->load->view("gcharts/index", $data);
    }

}
