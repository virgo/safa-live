<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Contracts_reports extends Safa_Controller {

    public $module = "contract_approving_phases";

    public function __construct() {
        parent::__construct();
        $this->layout = 'new';
        //Side menu session, By Gouda.
        session('side_menu_id', 2);
        $this->load->model('contracts_model');
        $this->load->model('safa_contract_phases_model');
        $this->load->model('safa_contract_approving_phases_model');
        $this->load->model('safa_contract_approving_phases_logs_model');
        $this->lang->load('contract_approving_phases');
        $this->lang->load('contracts');
        $this->load->helper('form_helper');
        $this->load->helper('db_helper');
        $this->load->helper('itconf_helper');
        $this->load->library("form_validation");
        permission();
    }

    public function index() {
        $data['user_id'] = session('users_id');
        $data['module_name'] = $this->module;
        if (isset($_GET['search'])) {
            $this->search();
        }
        $this->contracts_model->safa_uo_id = session('uo_id');
        $data["total_rows"] = $this->contracts_model->search(true);
        $this->contracts_model->offset = $this->uri->segment("4");
        $this->contracts_model->limit = $this->config->item('per_page');
        $data["items"] = $this->contracts_model->search();
        $config['uri_segment'] = 4;
        $config['base_url'] = site_url('uo/contracts_reports/index');
        $config['total_rows'] = $data["total_rows"];
        $config['per_page'] = $this->config->item('per_page');
        $config['suffix'] = $this->input->get() ? '?' . http_build_query($_GET) : NULL;
        $this->load->library('pagination');
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();



        $data["contract_phases_count"] = $this->safa_contract_phases_model->get(true);
        $data["contract_phases"] = $this->safa_contract_phases_model->get();

        $this->load->view('uo/contracts_reports/index', $data);
    }

}

/* End of file contracts_reports.php */
/* Location: ./application/controllers/contracts_reports.php */