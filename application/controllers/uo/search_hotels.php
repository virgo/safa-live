<?php

class Search_hotels extends Safa_Controller {

    
    public function __construct(){
        parent::__construct();
        $this->layout='new';      
        $this->load->model('hotels_model');
        $this->lang->load('search_hotels');
        permission();
    }

    public function index() {        

        if (isset($_GET['search']))
            $this->search();
        $this->hotels_model->join = TRUE;
        $data["total_rows"] = $this->hotels_model->get(true);
        $this->hotels_model->offset = $this->uri->segment("4");
        $this->hotels_model->limit = 20;
        $data["items"] = $this->hotels_model->get() ;
       
        $config['uri_segment'] = 4;
        $config['base_url'] = site_url('uo/search_hotels/index');
        $config['total_rows'] = $data["total_rows"];
        
        $config['per_page'] = $this->hotels_model->limit;
        
        
        $config['suffix'] = $this->input->get() ? '?' . http_build_query($_GET) : NULL;
        $this->load->library('pagination');
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();
        $data['country'] = $this->hotels_model->get_countery_ksa();
        $this->load->view('uo/search_hotels/index',$data);
        
    }


    function search() {

        if ($this->input->get("erp_country_id"))
            $this->hotels_model->erp_country_id = $this->input->get("erp_country_id");
            
        if ($this->input->get("erp_hotellevel_id"))
            $this->hotels_model->erp_hotellevel_id = $this->input->get("erp_hotellevel_id");
        
        if ($this->input->get("name_ar"))
            $this->hotels_model->name_ar = $this->input->get("name_ar");
                
        if ($this->input->get("name_la"))
            $this->hotels_model->name_la = $this->input->get("name_la");
        
    }
 
    #to show map of hotel
    function map(){

        $this->layout='ajax';
        $long = $this->input->get("googel_map_langtitude"); 
        $lat = $this->input->get("googel_map_latitude");
        $this->load->view('uo/search_hotels/map');
        
    }
    
    
   function hotel_details($id) {

         if ( ! $id)
            show_404();
         
        # to get images of hotel
        $data['images'] = $this->hotels_model->get_hotel_photo_path($id);
        
        #to get policies of hotel
        $data['hotel_polices'] = $this->hotels_model->get_hotel_policy($id);
        
        #to get advantages of hotel
        $data['hotel_advantages'] = $this->hotels_model->get_hotel_advantages($id);
        
        
        $this->hotels_model->join = TRUE; 

        $this->hotels_model->erp_hotel_id = $id;
        
        $data['item'] = $this->hotels_model->get();
        $this->load->view('uo/search_hotels/hotel_details',$data);
       
    }
    
    
  
    
    
    



}
