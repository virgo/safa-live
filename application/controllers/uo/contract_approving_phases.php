<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Contract_approving_phases extends Safa_Controller {

    public $module = "contract_approving_phases";

    public function __construct() {
        parent::__construct();
        $this->layout = 'new';
        $this->load->model('contracts_model');
        $this->load->model('safa_contract_phases_model');
        $this->load->model('safa_contract_approving_phases_model');
        $this->load->model('safa_contract_approving_phases_logs_model');
        $this->load->model('notification_model');
        $this->lang->load('contract_approving_phases');
        $this->load->helper('form_helper');
        $this->load->helper('db_helper');
        $this->load->helper('itconf_helper');
        $this->load->library("form_validation");
        permission();
    }

    public function index($safa_uo_contracts_id = false) {

        if (!$safa_uo_contracts_id) {
            show_404();
        }
        $this->layout = 'js';


        $data['contract_phases'] = ddgen('safa_contract_phases', array('safa_contract_phases_id', name()), FALSE, FALSE, TRUE);

        $this->contracts_model->safa_uo_contract_id = $safa_uo_contracts_id;
        $items = $this->contracts_model->search();
        $data['item'] = $items[0];

        //--------------------------------- Next contract phases documents

        $next_phase_id = $items[0]->safa_contract_phases_id_next;

        $this->safa_contract_approving_phases_model->safa_uo_contracts_id = $safa_uo_contracts_id;
        $this->safa_contract_approving_phases_model->safa_contract_phases_id = $next_phase_id;
        $required_documents_rows_for_contract = $this->safa_contract_approving_phases_model->get();


        $structure = array('safa_contract_phases_documents_id', name());
        $key = $structure['0'];
        if (isset($structure['1'])) {
            $value = $structure['1'];
        }
        $phase_documents_arr = array();
        $phase_documents_selected = array();
        $this->safa_contract_phases_model->documents_safa_contract_phases_id = $next_phase_id;
        $phase_documents = $this->safa_contract_phases_model->getDocuments();
        foreach ($phase_documents as $phase_document) {
            $phase_documents_arr[$phase_document->$key] = $phase_document->$value;

            foreach ($required_documents_rows_for_contract as $required_documents_row_for_contract) {
                if ($phase_document->$key == $required_documents_row_for_contract->safa_contract_phases_documents_id) {
                    $phase_documents_selected[] = $phase_document->$key;
                }
            }
        }

        $data['required_documents'] = $phase_documents_arr;
        $data['required_documents_selected'] = $phase_documents_selected;

        //--------------------------------

        $this->form_validation->set_rules('next_phase', 'lang:next_phase', 'trim|required');
        $this->form_validation->set_rules('required_documents[]', 'lang:required_documents', 'trim|required');

        if ($this->form_validation->run() == false) {
            $this->load->view('uo/contract_approving_phases/index', $data);
        } else {
            $this->safa_contract_approving_phases_model->safa_uo_contracts_id = $safa_uo_contracts_id;

            $required_documents = $this->input->post('required_documents');

            //------------ Update Contract Next Phase --------------------
            $this->contracts_model->safa_uo_contract_id = $safa_uo_contracts_id;
            $this->contracts_model->safa_contract_phases_id_next = $this->input->post('next_phase');
            $this->contracts_model->save();
            //------------------------------------------------------------





            $this->safa_contract_approving_phases_model->safa_uo_contracts_id = $safa_uo_contracts_id;
            $this->safa_contract_approving_phases_model->safa_contract_phases_id = $this->input->post('next_phase');

            $this->safa_contract_phases_model->documents_safa_contract_phases_id = $this->input->post('next_phase');
            $phase_documents = $this->safa_contract_phases_model->getDocuments();

            foreach ($phase_documents as $phase_document) {
                $this->safa_contract_approving_phases_model->safa_contract_phases_documents_id = $phase_document->safa_contract_phases_documents_id;
                $required_documents_selected = false;
                foreach ($required_documents as $required_documents_id) {
                    if ($phase_document->safa_contract_phases_documents_id == $required_documents_id) {
                        $required_documents_selected = true;
                        break;
                    }
                }


                if ($required_documents_selected) {
                    $required_documents_rows_count = $this->safa_contract_approving_phases_model->get(true);
                    if ($required_documents_rows_count == 0) {
                        $this->safa_contract_approving_phases_model->save();
                    }
                } else {
                    $this->safa_contract_approving_phases_model->delete();
                }
            }






            /*
              $this->safa_contract_approving_phases_model->safa_uo_contracts_id = $safa_uo_contracts_id;
              $this->safa_contract_approving_phases_model->safa_contract_phases_id =$this->input->post('next_phase');

              $contract_approving_phases_rows=$this->safa_contract_approving_phases_model->get();

              foreach($contract_approving_phases_rows as $contract_approving_phases_row) {
              $this->safa_contract_approving_phases_model->safa_contract_approving_phases_id = $contract_approving_phases_row->safa_contract_approving_phases_id;

              $file_name=iconv('utf-8','windows-1256',$contract_approving_phases_row->document_path);
              if (file_exists($file_name)) {
              unlink($file_name);
              }
              $this->safa_contract_approving_phases_model->delete();
              }

              $this->safa_contract_approving_phases_model->safa_contract_approving_phases_id=false;
              foreach($required_documents as $required_documents_id) {
              $this->safa_contract_approving_phases_model->safa_contract_phases_id = $this->input->post('next_phase');
              $this->safa_contract_approving_phases_model->safa_contract_phases_documents_id = $required_documents_id;
              $this->safa_contract_approving_phases_model->save();
              }
             */



            //----------- Send Notification For EA ------------------------------
            $msg_datetime = date('Y-m-d H:i', time());

            $this->notification_model->notification_type = 'automatic';
            $this->notification_model->erp_importance_id = 1;
            $this->notification_model->sender_type_id = 1;
            $this->notification_model->sender_id = 0;
            $this->notification_model->erp_system_events_id = 12;
            $this->notification_model->language_id = 2;
            $this->notification_model->msg_datetime = $msg_datetime;

            $current_phase_name = $this->input->post('current_phase');
            $next_phase_id = $this->input->post('next_phase');
            $this->safa_contract_phases_model->safa_contract_phases_id = $next_phase_id;
            $next_phase_row = $this->safa_contract_phases_model->get();
            $next_phase_name = $next_phase_row->{name()};

            $tags = "#*current_contract_phase#*=$current_phase_name*****#*next_contract_phase#*=$next_phase_name";
            $this->notification_model->tags = $tags;
            $this->notification_model->parent_id = '';


            $erp_notification_id = $this->notification_model->save();

            //-------- Notification Details ---------------
            $this->notification_model->detail_erp_notification_id = $erp_notification_id;
            $this->notification_model->detail_receiver_type = 3;
            $this->notification_model->detail_receiver_id = $items[0]->safa_ea_id;
            $this->notification_model->saveDetails();
            //--------------------------------
            //-------------------------------------------------------------------


            $this->load->view('redirect_message_popup', array('msg' => lang('global_updated_message'),
                'url' => site_url("uo/contract_approving_phases/index/$safa_uo_contracts_id"),
                'id' => $this->uri->segment("4"),
                'model_title' => lang('title'), 'action' => lang('edit_title')));
        }
    }

    public function getPhaseDocuments() {
        $safa_uo_contracts_id = $this->input->post('safa_uo_contracts_id');
        $next_phase_id = $this->input->post('next_phase_id');

        $this->safa_contract_approving_phases_model->safa_uo_contracts_id = $safa_uo_contracts_id;
        $this->safa_contract_approving_phases_model->safa_contract_phases_id = $next_phase_id;
        $required_documents_rows_for_contract = $this->safa_contract_approving_phases_model->get();


        $structure = array('safa_contract_phases_documents_id', name());
        $key = $structure['0'];
        if (isset($structure['1'])) {
            $value = $structure['1'];
        }
        $phase_documents_arr = array();
        $this->safa_contract_phases_model->documents_safa_contract_phases_id = $next_phase_id;
        $phase_documents = $this->safa_contract_phases_model->getDocuments();
        foreach ($phase_documents as $phase_document) {
            $phase_documents_arr[$phase_document->$key] = $phase_document->$value;
            //$phase_documents_selected[] = $phase_document->$key;

            foreach ($required_documents_rows_for_contract as $required_documents_row_for_contract) {
                if ($phase_document->$key == $required_documents_row_for_contract->safa_contract_phases_documents_id) {
                    $phase_documents_selected[] = $phase_document->$key;
                }
            }
        }


        $form_multiselect = form_multiselect('required_documents[]', $phase_documents_arr, set_value('required_documents', $phase_documents_selected), 'class="chosen-select chosen-rtl input-full" multiple tabindex="4" ');
        $form_multiselect = str_replace('<select name="required_documents[]" class="chosen-select chosen-rtl input-full" multiple tabindex="4" >', '', $form_multiselect);
        $form_multiselect = str_replace('</select>', '', $form_multiselect);
        echo $form_multiselect;
        exit;
    }

    public function approve($safa_uo_contracts_id = false) {

        if (!$safa_uo_contracts_id) {
            show_404();
        }
        $this->layout = 'js';


        $this->contracts_model->safa_uo_contract_id = $safa_uo_contracts_id;
        $items = $this->contracts_model->search();
        $data['item'] = $items[0];

        $next_phase_id = $items[0]->safa_contract_phases_id_next;

        //--------------------------------- Next contract phases documents
        $this->safa_contract_approving_phases_model->safa_uo_contracts_id = $safa_uo_contracts_id;
        $this->safa_contract_approving_phases_model->safa_contract_phases_id = $next_phase_id;

        $required_documents_rows_for_contract = $this->safa_contract_approving_phases_model->get();

        $data['required_documents'] = $required_documents_rows_for_contract;
        $data['safa_contract_approving_phases_status'] = ddgen('safa_contract_approving_phases_status', array('safa_contract_approving_phases_status_id', name()), FALSE, FALSE, TRUE);

        //--------------------------------

        if (!isset($_POST['smt_save'])) {
            $this->load->view('uo/contract_approving_phases/approve', $data);
        } else {

            $all_ea_documents_accepted = true;
            foreach ($required_documents_rows_for_contract as $required_documents_row_for_contract) {
                $safa_contract_approving_phases_id = $required_documents_row_for_contract->safa_contract_approving_phases_id;
                $this->safa_contract_approving_phases_model->safa_contract_approving_phases_id = $safa_contract_approving_phases_id;

                $this->safa_contract_approving_phases_model->safa_contract_approving_phases_status_id = $this->input->post("safa_contract_approving_phases_status_id_$safa_contract_approving_phases_id");
                $this->safa_contract_approving_phases_model->refuse_reason = $this->input->post("refuse_reason_$safa_contract_approving_phases_id");

                $this->safa_contract_approving_phases_model->save();

                if ($this->safa_contract_approving_phases_model->safa_contract_approving_phases_status_id != 2) {
                    $all_ea_documents_accepted = false;
                }
            }

            //------------ Update Contract Next Phase & Register log row --------------------------------------------
            if ($all_ea_documents_accepted) {
                //--- Save Log ----
                $current_datetime_for_log_row = date('Y-m-d H:i', time());
                $this->safa_contract_approving_phases_logs_model->safa_uo_contracts_id = $safa_uo_contracts_id;
                $this->safa_contract_approving_phases_logs_model->safa_contract_phases_id_from = $items[0]->safa_contract_phases_id_current;
                $this->safa_contract_approving_phases_logs_model->safa_contract_phases_id_to = $next_phase_id;
                $this->safa_contract_approving_phases_logs_model->the_date = $current_datetime_for_log_row;
                $this->safa_contract_approving_phases_logs_model->save();
                //-----------------	
                //---- Update Contract ---------
                $this->contracts_model->safa_uo_contract_id = $safa_uo_contracts_id;
                $this->contracts_model->safa_contract_phases_id_current = $next_phase_id;
                $this->contracts_model->save();
                //------------------------------
            }
            //------------------------------------------------------------------------------------------------------
            //----------- Send Notification For EA ------------------------------
            $msg_datetime = date('Y-m-d H:i', time());

            $this->notification_model->notification_type = 'automatic';
            $this->notification_model->erp_importance_id = 1;
            $this->notification_model->sender_type_id = 1;
            $this->notification_model->sender_id = 0;
            if ($all_ea_documents_accepted) {
                $this->notification_model->erp_system_events_id = 15;
            } else {
                $this->notification_model->erp_system_events_id = 14;
            }
            $this->notification_model->language_id = 2;
            $this->notification_model->msg_datetime = $msg_datetime;

            /*
              $current_phase_name=$this->input->post('current_phase');
              $next_phase_id=$this->input->post('next_phase');
              $this->safa_contract_phases_model->safa_contract_phases_id=$next_phase_id;
              $next_phase_row=$this->safa_contract_phases_model->get();
              $next_phase_name=$next_phase_row->{name()};

              $tags="#*current_contract_phase#*=$current_phase_name*****#*next_contract_phase#*=$next_phase_name";
             */
            $this->notification_model->tags = '';
            $this->notification_model->parent_id = '';


            $erp_notification_id = $this->notification_model->save();

            //-------- Notification Details ---------------
            $this->notification_model->detail_erp_notification_id = $erp_notification_id;
            $this->notification_model->detail_receiver_type = 3;
            $this->notification_model->detail_receiver_id = $items[0]->safa_ea_id;
            $this->notification_model->saveDetails();
            //--------------------------------
            //-------------------------------------------------------------------


            $this->load->view('redirect_message_popup', array('msg' => lang('global_updated_message'),
                'url' => site_url("uo/contract_approving_phases/approve/$safa_uo_contracts_id"),
                'id' => $this->uri->segment("4"),
                'model_title' => lang('title'), 'action' => lang('edit_title')));
        }
    }

    public function history($safa_uo_contracts_id = false) {

        if (!$safa_uo_contracts_id) {
            show_404();
        }
        $this->layout = 'js';


        $data['contract_phases'] = ddgen('safa_contract_phases', array('safa_contract_phases_id', name()), FALSE, FALSE, TRUE);

        $this->contracts_model->safa_uo_contract_id = $safa_uo_contracts_id;
        $items = $this->contracts_model->search();
        $data['item'] = $items[0];

        //--------------------------------- Next contract phases documents
        $next_phase_id = $items[0]->safa_contract_phases_id_next;

        $this->safa_contract_approving_phases_model->safa_uo_contracts_id = $safa_uo_contracts_id;
        $this->safa_contract_approving_phases_model->safa_contract_phases_id = $next_phase_id;
        $required_documents_rows_for_contract = $this->safa_contract_approving_phases_model->get();

        $data['required_documents'] = $required_documents_rows_for_contract;
        $data['safa_contract_approving_phases_status'] = ddgen('safa_contract_approving_phases_status', array('safa_contract_approving_phases_status_id', name()), FALSE, FALSE, TRUE);

        //--------------------------------



        if (!isset($_POST['smt_save'])) {
            $this->load->view('uo/contract_approving_phases/history', $data);
        } else {
            $all_ea_documents_accepted = true;

            $this->safa_contract_approving_phases_model->safa_contract_phases_id = $this->input->post('previous_phases');
            $required_documents_rows_for_contract = $this->safa_contract_approving_phases_model->get();


            foreach ($required_documents_rows_for_contract as $required_documents_row_for_contract) {


                $safa_contract_approving_phases_id = $required_documents_row_for_contract->safa_contract_approving_phases_id;

                $this->safa_contract_approving_phases_model->safa_contract_approving_phases_id = $this->input->post("safa_contract_approving_phases_id_$safa_contract_approving_phases_id");
                $this->safa_contract_approving_phases_model->safa_contract_approving_phases_status_id = $this->input->post("safa_contract_approving_phases_status_id_$safa_contract_approving_phases_id");
                $this->safa_contract_approving_phases_model->refuse_reason = $this->input->post("refuse_reason_$safa_contract_approving_phases_id");

                $this->safa_contract_approving_phases_model->save();

                if ($this->safa_contract_approving_phases_model->safa_contract_approving_phases_status_id != 2) {
                    $all_ea_documents_accepted = false;
                }
            }

            //------------ Update Contract Next Phase & Register log row --------------------------------------------
            //---- Update Contract ---------
            $this->contracts_model->safa_uo_contract_id = $safa_uo_contracts_id;
            $this->contracts_model->safa_contract_phases_id_next = $this->input->post('previous_phases');

            $this->safa_contract_approving_phases_logs_model->safa_contract_phases_id_to = $this->input->post('previous_phases');
            $approving_phases_logs_rows = $this->safa_contract_approving_phases_logs_model->get();
            if (count($approving_phases_logs_rows) > 0) {
                $this->contracts_model->safa_contract_phases_id_current = $approving_phases_logs_rows[0]->safa_contract_phases_id_from;
            }
            $this->contracts_model->save();
            //------------------------------




            if ($all_ea_documents_accepted) {
                //--- Save Log ----
                $current_datetime_for_log_row = date('Y-m-d H:i', time());
                $this->safa_contract_approving_phases_logs_model->safa_uo_contracts_id = $safa_uo_contracts_id;
                $this->safa_contract_approving_phases_logs_model->safa_contract_phases_id_from = $items[0]->safa_contract_phases_id_current;
                $this->safa_contract_approving_phases_logs_model->safa_contract_phases_id_to = $this->input->post('previous_phases');
                $this->safa_contract_approving_phases_logs_model->the_date = $current_datetime_for_log_row;
                $this->safa_contract_approving_phases_logs_model->save();
                //-----------------	
                //---- Update Contract ---------
                $this->contracts_model->safa_uo_contract_id = $safa_uo_contracts_id;
                $this->contracts_model->safa_contract_phases_id_current = $this->input->post('previous_phases');
                $this->contracts_model->save();
                //------------------------------
            }
            //------------------------------------------------------------------------------------------------------
            //----------- Send Notification For EA ------------------------------
            $msg_datetime = date('Y-m-d H:i', time());

            $this->notification_model->notification_type = 'automatic';
            $this->notification_model->erp_importance_id = 1;
            $this->notification_model->sender_type_id = 1;
            $this->notification_model->sender_id = 0;
            if ($all_ea_documents_accepted) {
                $this->notification_model->erp_system_events_id = 15;
            } else {
                $this->notification_model->erp_system_events_id = 14;
            }
            $this->notification_model->language_id = 2;
            $this->notification_model->msg_datetime = $msg_datetime;

            /*
              $current_phase_name=$this->input->post('current_phase');
              $next_phase_id=$this->input->post('next_phase');
              $this->safa_contract_phases_model->safa_contract_phases_id=$next_phase_id;
              $next_phase_row=$this->safa_contract_phases_model->get();
              $next_phase_name=$next_phase_row->{name()};

              $tags="#*current_contract_phase#*=$current_phase_name*****#*next_contract_phase#*=$next_phase_name";
             */
            $this->notification_model->tags = '';
            $this->notification_model->parent_id = '';


            $erp_notification_id = $this->notification_model->save();

            //-------- Notification Details ---------------
            $this->notification_model->detail_erp_notification_id = $erp_notification_id;
            $this->notification_model->detail_receiver_type = 3;
            $this->notification_model->detail_receiver_id = $items[0]->safa_ea_id;
            $this->notification_model->saveDetails();
            //--------------------------------
            //-------------------------------------------------------------------

            $this->load->view('redirect_message_popup', array('msg' => lang('global_updated_message'),
                'url' => site_url("uo/contract_approving_phases/history/$safa_uo_contracts_id"),
                'id' => $this->uri->segment("4"),
                'model_title' => lang('title'), 'action' => lang('edit_title')));
        }
    }

    public function reportPhase($safa_uo_contracts_id = false, $next_phase_id = false) {

        if (!$safa_uo_contracts_id) {
            show_404();
        }
        $this->layout = 'js';


        $data['contract_phases'] = ddgen('safa_contract_phases', array('safa_contract_phases_id', name()), FALSE, FALSE, TRUE);

        $this->contracts_model->safa_uo_contract_id = $safa_uo_contracts_id;
        $items = $this->contracts_model->search();
        $data['item'] = $items[0];

        //--------------------------------- Next contract phases documents
        //$next_phase_id=$items[0]->safa_contract_phases_id_next; 
        $data['previous_phase_selected'] = $next_phase_id;

        $this->safa_contract_approving_phases_model->safa_uo_contracts_id = $safa_uo_contracts_id;
        $this->safa_contract_approving_phases_model->safa_contract_phases_id = $next_phase_id;
        $required_documents_rows_for_contract = $this->safa_contract_approving_phases_model->get();

        $data['required_documents'] = $required_documents_rows_for_contract;
        $data['safa_contract_approving_phases_status'] = ddgen('safa_contract_approving_phases_status', array('safa_contract_approving_phases_status_id', name()), FALSE, FALSE, TRUE);

        //--------------------------------



        if (!isset($_POST['smt_save'])) {
            $this->load->view('uo/contract_approving_phases/report_phase', $data);
        } else {
            $all_ea_documents_accepted = true;

            $this->safa_contract_approving_phases_model->safa_contract_phases_id = $this->input->post('previous_phases');
            $required_documents_rows_for_contract = $this->safa_contract_approving_phases_model->get();


            foreach ($required_documents_rows_for_contract as $required_documents_row_for_contract) {


                $safa_contract_approving_phases_id = $required_documents_row_for_contract->safa_contract_approving_phases_id;

                $this->safa_contract_approving_phases_model->safa_contract_approving_phases_id = $this->input->post("safa_contract_approving_phases_id_$safa_contract_approving_phases_id");
                $this->safa_contract_approving_phases_model->safa_contract_approving_phases_status_id = $this->input->post("safa_contract_approving_phases_status_id_$safa_contract_approving_phases_id");
                $this->safa_contract_approving_phases_model->refuse_reason = $this->input->post("refuse_reason_$safa_contract_approving_phases_id");

                $this->safa_contract_approving_phases_model->save();

                if ($this->safa_contract_approving_phases_model->safa_contract_approving_phases_status_id != 2) {
                    $all_ea_documents_accepted = false;
                }
            }

            //------------ Update Contract Next Phase & Register log row --------------------------------------------
            //---- Update Contract ---------
            $this->contracts_model->safa_uo_contract_id = $safa_uo_contracts_id;
            $this->contracts_model->safa_contract_phases_id_next = $this->input->post('previous_phases');

            $this->safa_contract_approving_phases_logs_model->safa_contract_phases_id_to = $this->input->post('previous_phases');
            $approving_phases_logs_rows = $this->safa_contract_approving_phases_logs_model->get();
            if (count($approving_phases_logs_rows) > 0) {
                $this->contracts_model->safa_contract_phases_id_current = $approving_phases_logs_rows[0]->safa_contract_phases_id_from;
            }
            $this->contracts_model->save();
            //------------------------------




            if ($all_ea_documents_accepted) {
                //--- Save Log ----
                $current_datetime_for_log_row = date('Y-m-d H:i', time());
                $this->safa_contract_approving_phases_logs_model->safa_uo_contracts_id = $safa_uo_contracts_id;
                $this->safa_contract_approving_phases_logs_model->safa_contract_phases_id_from = $items[0]->safa_contract_phases_id_current;
                $this->safa_contract_approving_phases_logs_model->safa_contract_phases_id_to = $this->input->post('previous_phases');
                $this->safa_contract_approving_phases_logs_model->the_date = $current_datetime_for_log_row;
                $this->safa_contract_approving_phases_logs_model->save();
                //-----------------	
                //---- Update Contract ---------
                $this->contracts_model->safa_uo_contract_id = $safa_uo_contracts_id;
                $this->contracts_model->safa_contract_phases_id_current = $this->input->post('previous_phases');
                $this->contracts_model->save();
                //------------------------------
            }
            //------------------------------------------------------------------------------------------------------
            //----------- Send Notification For EA ------------------------------
            $msg_datetime = date('Y-m-d H:i', time());

            $this->notification_model->notification_type = 'automatic';
            $this->notification_model->erp_importance_id = 1;
            $this->notification_model->sender_type_id = 1;
            $this->notification_model->sender_id = 0;
            if ($all_ea_documents_accepted) {
                $this->notification_model->erp_system_events_id = 15;
            } else {
                $this->notification_model->erp_system_events_id = 14;
            }
            $this->notification_model->language_id = 2;
            $this->notification_model->msg_datetime = $msg_datetime;

            /*
              $current_phase_name=$this->input->post('current_phase');
              $next_phase_id=$this->input->post('next_phase');
              $this->safa_contract_phases_model->safa_contract_phases_id=$next_phase_id;
              $next_phase_row=$this->safa_contract_phases_model->get();
              $next_phase_name=$next_phase_row->{name()};

              $tags="#*current_contract_phase#*=$current_phase_name*****#*next_contract_phase#*=$next_phase_name";
             */
            $this->notification_model->tags = '';
            $this->notification_model->parent_id = '';


            $erp_notification_id = $this->notification_model->save();

            //-------- Notification Details ---------------
            $this->notification_model->detail_erp_notification_id = $erp_notification_id;
            $this->notification_model->detail_receiver_type = 3;
            $this->notification_model->detail_receiver_id = $items[0]->safa_ea_id;
            $this->notification_model->saveDetails();
            //--------------------------------
            //-------------------------------------------------------------------

            $this->load->view('redirect_message', array('msg' => lang('global_updated_message'),
                'url' => site_url("uo/contract_approving_phases/reportPhase/$safa_uo_contracts_id/$next_phase_id"),
                'id' => $this->uri->segment("4"),
                'model_title' => lang('title'), 'action' => lang('edit_title')));
        }
    }

    public function getPhaseDocumentsTable() {
        $safa_uo_contracts_id = $this->input->post('safa_uo_contracts_id');
        $previous_phases_id = $this->input->post('previous_phases_id');

        $this->safa_contract_approving_phases_model->safa_uo_contracts_id = $safa_uo_contracts_id;
        $this->safa_contract_approving_phases_model->safa_contract_phases_id = $previous_phases_id;
        $required_documents_rows_for_contract = $this->safa_contract_approving_phases_model->get();

        $safa_contract_approving_phases_status = ddgen('safa_contract_approving_phases_status', array('safa_contract_approving_phases_status_id', name()), FALSE, FALSE, TRUE);


        $table_html = "
       
       			<table cellpadding='0' cellspacing='0'>
                <thead>
                    <tr>
                        <th>" . lang('document_name') . "</th>
                        <th>" . lang('upload_file') . "</th>
                        <th>" . lang('status') . "</th>
                        <th>" . lang('refuse_reason') . "</th>
                    </tr>
                </thead>
                <tbody id='had'>";
        if (ensure($required_documents_rows_for_contract)) {
            foreach ($required_documents_rows_for_contract as $required_document) {

                $safa_contract_approving_phases_id = $required_document->safa_contract_approving_phases_id;
                $safa_contract_phases_documents_id = $required_document->safa_contract_phases_documents_id;
                $refuse_reason = $required_document->refuse_reason;
                $safa_contract_approving_phases_status_id = $required_document->safa_contract_approving_phases_status_id;

                $table_html = $table_html . "
                    <tr rel='" . $safa_contract_approving_phases_id . "'>

                        <td>" . form_input(array('name' => "safa_contract_approving_phases_id_$safa_contract_approving_phases_id", 'type' => 'hidden', 'value' => $safa_contract_approving_phases_id, 'id' => "safa_contract_approving_phases_id_$safa_contract_approving_phases_id")) . "
                            " . $required_document->{name()} . "
                        </td>
                        <td>
                          ";

                if ($required_document->document_path != '') {
                    $document_path = $required_document->document_path;
                    $document_file_name_arr = explode('/', $document_path);
                    $document_file_name = end($document_file_name_arr);

                    $table_html = $table_html . "<a target='_blank' href='" . base_url() . $document_path . "'>$document_file_name</a>";
                } else {
                    //echo form_upload('document_file_$safa_contract_phases_documents_id', '','  style='' id='document_file_$safa_contract_phases_documents_id'  ') ;
                }

                $table_html = $table_html . " 
                        </td>
                        <td>
                            " . form_dropdown("safa_contract_approving_phases_status_id_$safa_contract_approving_phases_id", $safa_contract_approving_phases_status, set_value("safa_contract_approving_phases_status_id_$safa_contract_approving_phases_id", $safa_contract_approving_phases_status_id), "class='validate[required] input-full chosen-rtl ' style='width:100%;' id=safa_contract_approving_phases_status_id_'$safa_contract_approving_phases_id'") . "
                        </td>
                        <td>
                            " . form_input("refuse_reason_$safa_contract_approving_phases_id", set_value("refuse_reason_$safa_contract_approving_phases_id", $refuse_reason), " style='' id='refuse_reason_$safa_contract_approving_phases_id' class='input-full' ") . "
                        </td>
                        
                    </tr>";
            }
        }
        $table_html = $table_html . " </tbody>
            </table>
       ";

        echo $table_html;
        exit;
    }

}

/* End of file contract_approving_phases.php */
/* Location: ./application/controllers/contract_approving_phases.php */