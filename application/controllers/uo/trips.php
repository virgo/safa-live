<?php

class Trips extends Safa_Controller {

    function __construct() {
        parent::__construct();
        $this->layout = 'new';

        //Side menu session, By Gouda.
        if (session('uo_id')) {
            session('side_menu_id', 3);
        } else if (session('ea_id')) {
            session('side_menu_id', 1);
        }

        $this->load->model('safa_trips_model');
        $this->load->model('trip_supervisors_model');
        $this->load->model('safa_group_passports_model');
        $this->load->model('trip_details_model');
        $this->load->model('safa_uos_model');
        $this->load->model('contracts_model');
		$this->load->model('internalsegments_drivers_and_buses_model');
        

        $this->lang->load('trips_report');
        $this->lang->load('trips_reports');
        $this->lang->load('group_passports');
        $this->lang->load('trip_details');

        $this->lang->load('trips');

        $this->load->helper('form_helper');
        $this->load->helper('db_helper');
        $this->load->helper('trip');


        permission();
        if (session('ea_id')) {
            $this->safa_trips_model->safa_tripstatus_id_not = 1;
            $this->safa_trips_model->safa_ea_id = session('ea_id');

            $this->trip_details_model->safa_ea_id = session('ea_id');
        }
        if (session('uo_id')) {
            $this->safa_trips_model->safa_tripstatus_id_not = 1;
            $this->safa_trips_model->safa_uo_id = session('uo_id');

            $this->trip_details_model->safa_uo_id = session('uo_id');
        }
    }

    function index() {
        $this->safa_trips_model->safa_tripstatus_id_not = 1;
        $result = $this->safa_trips_model->get();
        $trips = array();
        $trips[''] = lang('global_select_from_menu');
        foreach ($result as $row) {
            $trips[$row->safa_trip_id] = $row->name;
        }
        $data["safa_trips"] = $trips;


        $data["safa_tripstatus"] = ddgen("safa_tripstatus", array("safa_tripstatus_id", name()), array('safa_tripstatus_id!=' => 1));
        $data["safa_trip_confirm"] = ddgen("safa_trip_confirm", array("safa_trip_confirm_id", name()));
        $data["erp_transportertypes"] = ddgen("erp_transportertypes", array("erp_transportertype_id", name()));
        $data['supervisors'] = $this->trip_supervisors_model->get_ea_supervisor();


        if (isset($_GET['search'])) {
            $this->search();
        }
        $data["total_rows"] = $this->safa_trips_model->get(true);

        $this->safa_trips_model->offset = $this->uri->segment("3");
        //$this->safa_trips_model->limit = $this->config->item('per_page');
        //$this->safa_trips_model->order_by = array('safa_trip_id');
        $data["items"] = $this->safa_trips_model->get();
        $config['uri_segment'] = 3;
        $config['base_url'] = site_url('uo/trips/index');
        $config['total_rows'] = $data["total_rows"];

        $data["items"] = $this->safa_trips_model->get();
        //echo $this->db->last_query();
        $config['uri_segment'] = 4;
        $config['base_url'] = site_url('trips_report');
        $config['total_rows'] = $data["total_rows"];

        $data["safa_reservation_forms_rows"] = $this->safa_trips_model->get_packages();

        $config['per_page'] = $this->config->item('per_page');
        $config['suffix'] = $this->input->get() ? '?' . http_build_query($_GET) : NULL;
        $this->load->library('pagination');
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();
        $this->load->view('uo/trips/index', $data);
    }

    public function search() {
        if ($this->input->get("safa_trip_id"))
            $this->safa_trips_model->safa_trip_id = $this->input->get("safa_trip_id");

        if ($this->input->get("date_from"))
            $this->safa_trips_model->date_from = $this->input->get("date_from");
        if ($this->input->get("date_to"))
            $this->safa_trips_model->date_to = $this->input->get("date_to");

        if ($this->input->get("arrival_date"))
            $this->safa_trips_model->arrival_date = $this->input->get("arrival_date");
        if ($this->input->get("safa_tripstatus_id"))
            $this->safa_trips_model->safa_tripstatus_id = $this->input->get("safa_tripstatus_id");
        if ($this->input->get("safa_trip_confirm_id"))
            $this->safa_trips_model->safa_trip_confirm_id = $this->input->get("safa_trip_confirm_id");
        if ($this->input->get("erp_country_id"))
            $this->safa_trips_model->erp_country_id = $this->input->get("erp_country_id");

        if ($this->input->get("safa_ea_supervisor_ids"))
            $this->safa_trips_model->safa_ea_supervisor_ids = $this->input->get("safa_ea_supervisor_ids");
    }

    public function passports($id) {
        $this->safa_group_passports_model->safa_trip_id = $id;
        $data['passports'] = $this->safa_group_passports_model->get_for_accomedation();
        $this->load->view('uo/trips/passports', $data);
    }

    public function internalsegments($id = FALSE, $pdfview = false) {
        $this->layout = 'ajax';
        if (!$id)
            show_404();

        $this->trip_details_model->safa_trip_id = $id;
        $data['internalsegments'] = $this->trip_details_model->get_internal_segments();

        $data['safa_uo_code'] = '';

        if (count($data['internalsegments']) > 0) {
        	$data['buses'] = $this->internalsegments_drivers_and_buses_model->get_distinct_bus_brands_by_safa_internalsegment($data['internalsegments'][0]->safa_internalsegment_id);
			
            if (session('uo_id')) {
                $this->safa_uos_model->safa_uo_id = session('uo_id');
                $data['uo_dir'] = session('uo_id');
                $safa_uo_row = $this->safa_uos_model->get();
                $data['safa_uo_code'] = $safa_uo_row->code;
            } else if (session('ea_id')) {
                $this->contracts_model->safa_uo_contract_id = $data['internalsegments'][0]->safa_uo_contract_id;
                $contracts_row = $this->contracts_model->get();
                if (count($contracts_row) > 0) {
                    $safa_uo_code = '';
                    $safa_uo_serial = '';

                    $this->safa_uos_model->safa_uo_id = $contracts_row->safa_uo_id;
                    $data['uo_dir'] = $contracts_row->safa_uo_id;
                    $safa_uo_row = $this->safa_uos_model->get();
                    if (count($safa_uo_row) > 0) {
                        $data['safa_uo_code'] = $safa_uo_row->code;
                    }
                }
            }
        }

        if (strtolower($pdfview) == 'pdf') {
            $page = $this->load->view('uo/trips/internalsegments', $data,TRUE);
            $this->load->helper('pdf_helper');
		$pdf_orientation = 'P';
		$pdf_unit = 'px';
		$pdf_pageformat = 'A4';
		$pdf_unicode = true;
		$pdf_encoding = 'UTF-8';

		$pdf = new virgo_pdf($pdf_orientation, $pdf_unit, $pdf_pageformat, $pdf_unicode, $pdf_encoding, false);
		$tahoma = $pdf->addTTFfont('static/fonts/arial.ttf', 'TrueTypeUnicode', '', 96);
		$pdf->SetFont($tahoma, '', 10, '', false);
		$pdf->SetCreator('Safa Live');
		$pdf->SetAuthor('Safa Live');
		$pdf->SetTitle('Safa Live Reports');
		$pdf->SetSubject('Safa Live');

		$pdf->setPrintHeader(false);

                $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(20, 20, 40);
		$pdf->SetHeaderMargin(20);
$pdf->SetFooterMargin(0);

// remove default footer
$pdf->setPrintFooter(false);
			
// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

		$pdf->setRTL(true);
		$pdf->AddPage();
                
                $pdf->writeHTML($page, true, false, true, false, '');

			
		$this->load->helper('download');
		$pdf->Output("internalsegments_$id.pdf", 'D');
                
        } else {
            $this->load->view('uo/trips/internalsegments', $data);
        }
    }

}
