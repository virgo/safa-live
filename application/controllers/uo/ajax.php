<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Ajax extends Safa_Controller {

    public function __construct() {
        parent::__construct();
        $this->layout = "ajax";
        $this->load->model('trips_reports_model');
//        permission();
    }

    public function get_city_hotels($city_id) {
        $hotels = $this->trips_reports_model->get_related_hotels_to_contracts(session('uo_id'), $city_id);
        foreach ($hotels as $key => $val) {
            echo "<option value='$key'>$val</option>";
        }
    }
}
