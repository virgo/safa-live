<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class departing_report extends Safa_Controller {

    public $module = "trips_reports";
    public $uo_id; //TODO: get $uo_id from session

    public function __construct() {
        parent::__construct();
        
        $this->layout = 'new';
        //Side menu session, By Gouda.
        session('side_menu_id',3);
        
        $this->uo_id = session('uo_id');

        $this->load->model('trips_reports_model');
        $this->lang->load('trips_reports');
        $this->load->helper('trip');
                
        $this->load->library('flight_states');
        
        permission();

    }

    public function index() {

    	$this->trips_reports_model->uo_id= session('uo_id');

    	//nationality_id
        if ($this->input->post('nationality_id')) {
            $this->trips_reports_model->nationality_id = $this->input->post('nationality_id');
        }

        //erp_hotel_id
        if ($this->input->post('erp_hotel_id')) {
            $this->trips_reports_model->erp_hotel_id = $this->input->post('erp_hotel_id');
        }

        //safa_uo_contract_id
        if ($this->input->post('safa_uo_contract_id')) {
            $this->trips_reports_model->safa_uo_contract_id = $this->input->post('safa_uo_contract_id');
        }

        //erp_port_id
        if ($this->input->post('erp_port_id')) {
            $this->trips_reports_model->erp_port_id = $this->input->post('erp_port_id');
        }

        //nationality_id
        if ($this->input->post('nationality_id')) {
            $this->trips_reports_model->nationality_id = $this->input->post('nationality_id');
        }

        //erp_city_id
        if ($this->input->post('erp_city_id')) {
            $this->trips_reports_model->erp_city_id = $this->input->post('erp_city_id');
        }
        //safa_transporter_id
        if ($this->input->post('safa_transporter_id')) {
            $this->trips_reports_model->safa_transporter_id = $this->input->post('safa_transporter_id');
        }

        //from_date
        if ($this->input->post('from_date')) {
            $this->trips_reports_model->from_date = $this->input->post('from_date');
        }

        //to_date
        if ($this->input->post('to_date')) {
            $this->trips_reports_model->to_date = $this->input->post('to_date');
        }

        //from_time
        if ($this->input->post('from_time')) {
            $this->trips_reports_model->from_time = $this->input->post('from_time');
        }

        //to_time
        if ($this->input->post('to_time')) {
            $this->trips_reports_model->to_time = $this->input->post('to_time');
        }

        //from_count
        if ($this->input->post('from_count')) {
            $this->trips_reports_model->from_count = $this->input->post('from_count');
        }

        //to_count
        if ($this->input->post('to_count')) {
            $this->trips_reports_model->to_count = $this->input->post('to_count');
        }


        if ($this->input->post('safa_trip_confirm_id')) {
            $this->trips_reports_model->safa_trip_confirm_id = $this->input->post('safa_trip_confirm_id');
        }


        $data['trips_ds'] = $this->trips_reports_model->departing_reports($this->uo_id);

        //$uo_id = session('uo_id');
        $cols = array();
        
		$uo_user_id = session('uo_user_id');
        if ($this->input->post('cols')) {

        	$cols_uo_unset = array('code', 'arrival_date', 'arrival_time', 'hotel', 'contract', 'cn', 'nationality', 'port', 'trans', 'supervisor', 'status');
            foreach($cols_uo_unset as $col_uo_unset) {
				setcookie(''.'departing_' . $uo_user_id . '_' . $col_uo_unset . '', '' . $col_uo_unset . '', time()-3600);
        	}
        	
            $cols = $this->input->post('cols');
            
            foreach($cols as $col) {
            	setcookie(''.'departing_' . $uo_user_id . '_' . $col . '', '' . $col . '', time() + 60 * 60 * 24 * 365);
            }
        } else {
        	$cols = array('code', 'arrival_date', 'arrival_time', 'hotel', 'contract', 'cn', 'nationality', 'port', 'trans', 'supervisor', 'status');
        }
        
        
        
        $colums = array();        
    	foreach ($cols as $col) {
			if ($this->input->cookie(''.'departing_' . $uo_user_id . '_' . $col . ''))
			$colums[] = $this->input->cookie(''.'departing_' . $uo_user_id . '_' . $col . '');
		}
        
		if (!empty($colums) && count($colums) >= 1 && is_array($colums)) {
			$data['cols'] = $colums;
		} else {
			$data['cols'] = $cols;
		}
		
        if ($this->input->post('excel_export')) {
            
            $this->layout = 'js';
            $this->load->helper('download');
            $data['print_statement'] = "";
            $data = $this->load->view("uo/trips_reports/departing_report_excel", $data, TRUE);
            $name = 'dep-' . date('Y-m-d-His') . '.xls';
            force_download($name, $data);
            
        } elseif ($this->input->post('print_report')) {

            $this->layout = 'js';
            $data['print_statement'] = "<script></script>";
            $this->load->view("uo/trips_reports/departing_report_excel", $data);
            
        } else {

            $this->load->view("uo/trips_reports/departing_report.php", $data);
        }
    }

}
