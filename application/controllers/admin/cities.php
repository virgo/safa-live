<?php


class Cities extends Safa_Controller {

    public $module = "cities";

    public function __construct() {
        parent::__construct();
        $this->load->model('cities_model');
        $this->lang->load('admins/cities'); 
        $this->layout='new';
//        
        permission();
    }

    public function index() {        
        if(isset($_GET['search'])) $this->search();
        $this->cities_model->join = TRUE;
        $data["total_rows"] = $this->cities_model->get(true);
        $this->cities_model->offset = $this->uri->segment("4");
        $this->cities_model->limit = 10;
        $data["items"] = $this->cities_model->get();
        $config['uri_segment'] = 4;
        $config['base_url'] = site_url('admin/cities/index');
        $config['total_rows'] = $data["total_rows"];
        $config['per_page'] = $this->config->item('per_page');
        $config['suffix'] = $this->input->get() ? '?'.http_build_query($_GET) : NULL;
        $this->load->library('pagination');
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();
        $this->load->view('admin/cities/index', $data);
    }

    public function add() {
        

        $this->load->library("form_validation");
        $this->form_validation->set_rules('erp_country_id', 'lang:cities_erp_countery_id', 'trim|required');
        $this->form_validation->set_rules('name_ar', 'lang:cities_name_ar', 'trim|required|is_unique[erp_cities.name_ar]|min_length[3]|max_length[50]');
        $this->form_validation->set_rules('name_la', 'lang:cities_name_la', 'trim|required|is_unique[erp_cities.name_la]|min_length[3]|max_length[50]');
        if ($this->form_validation->run() == false) {
            $this->load->view("admin/cities/add");
        } else {
            $this->cities_model->erp_country_id = $this->input->post('erp_country_id');
            $this->cities_model->name_ar = $this->input->post('name_ar');
            $this->cities_model->name_la = $this->input->post('name_la');
            
          
          $this->cities_model->save();
   $this->load->view('redirect_message', array('msg' => lang('global_added_message'), 'url' => site_url('admin/cities'),'model_title'=> lang('menu_main_cities'),'action'=>lang('cities_add_title')));  
           // redirect("admin/cities/index");
        }
    }

    public function edit($id) {

        if ( ! $id)
            show_404();
        
        $this->cities_model->erp_city_id = $id;
        $data['items'] = $this->cities_model->get();
        
        if ( ! $data['items'])
            show_404();
        
        $this->load->library("form_validation");
        $this->form_validation->set_rules('erp_country_id', 'lang:cities_erp_countery_id', 'trim|required');
        $this->form_validation->set_rules('name_ar', 'lang:cities_name_ar', 'trim|required|callback_is_unique_ar['.$this->uri->segment("4").']|min_length[3]|max_length[50]');
        $this->form_validation->set_rules('name_la', 'lang:cities_name_la', 'trim|required|callback_is_unique_la['.$this->uri->segment("4").']|min_length[3]|max_length[50]');
        if ($this->form_validation->run() == false) {
            $this->load->view("admin/cities/edit", $data);
        } else {
            $this->cities_model->erp_country_id = $this->input->post('erp_country_id');
            $this->cities_model->name_ar = $this->input->post('name_ar');
            $this->cities_model->name_la = $this->input->post('name_la');
            $this->cities_model->save();
            $this->load->view('redirect_message', array('msg' => lang('global_updated_message'), 'url' => site_url('admin/cities'), 'id'=>$this->uri->segment("4"),'model_title'=> lang('menu_main_cities'),'action'=>lang('cities_edit_title')));
            //redirect("admin/cities/index");
        }
    }

          function delete($id=false) {
            if(!$id)
                show_404 ();
                $this->cities_model->erp_city_id = $id;
                $this->cities_model->delete();
                redirect('admin/cities/index');
                
           
        }
    
//    function delete() {
//         $this->layout='ajax';
//        if (!$this->input->is_ajax_request()) {
//            redirect('admin/cities/index');
//        } else {
//            $erp_city_id =$this->input->post('erp_city_id');
//            $data = $this->cities_model->check_delete_ability($erp_city_id);
//
//            if ($data==0) {
//                $this->cities_model->erp_city_id = $erp_city_id;
//                if ($this->cities_model->delete())
//                    echo json_encode(array('response' => TRUE, 'msg' => 'deleted'));
//            }
//            else {
//                echo json_encode(array('response' => FALSE, 'msg' => 'not deleted'));
//            }
//        }
//    }

    function delete_all() {
        if ($this->input->post("delete_cities")) {
            foreach ($this->input->post("delete_cities") as $item) {
                    $this->cities_model->erp_city_id = $item;
                
                $this->cities_model->delete();
            }
        }
        else
            show_404 ();
        redirect("admin/cities/index");
    }

    function search() {

        if ($this->input->get("name_ar"))
            $this->cities_model->name_ar = $this->input->get("name_ar");

        if ($this->input->get("name_la"))
            $this->cities_model->name_la = $this->input->get("name_la");
        
         if ($this->input->get("erp_country_id"))
            $this->cities_model->erp_country_id = $this->input->get("erp_country_id");

    }
    
      function alpha_dash_space($name_la) {
         if(!preg_match("/^([-a-z_ ])+$/i", $name_la)){
              $this->form_validation->set_message('alpha_dash_space', lang('cities_name_invalid'));
            return FALSE;
         }
         else {
           return true;  
         }
                 
    }   
    ////will be used  later
        function check_name_ar($name_ar) {
            if($this->uri->segment('3')=='edit')

        $this->db->where('name_ar', $name_ar);

        if ($this->db->get('erp_cities')->num_rows()) {
            $this->form_validation->set_message('check_name_ar', lang('cities_name_exist'));
            return FALSE;
        }
        else
            return true;
    }
////will be used  later
    function check_name_la($name_la) {
       $this->db->where('erp', $name_la);
        $this->db->where('name_ar', $name_la);
        if ($this->db->get('erp_cities')->num_rows()) {
            $this->form_validation->set_message('check_name_la', lang('cities_name_exist'));
            return FALSE;
        }
        else
        return true;
    }

        function is_unique_ar($value,$id= false){
           if($id==false){
               $query=$this->db->limit(1)->where("name_ar",$value)->get("erp_cities");
           }
           else{
                $query=$this->db->limit(1)->where("name_ar",$value)->where("erp_city_id!=".$id)->get("erp_cities");
           }
           if ($query->num_rows()) { 
           $this->form_validation->set_message('is_unique_ar', lang('cities_name_exist'));
           return false;
           }
           else
               return true;
  }
  
  
        function is_unique_la($value,$id= false){
           if($id==false){
               $query=$this->db->limit(1)->where("name_la",$value)->get("erp_cities");
           }
           else{
                $query=$this->db->limit(1)->where("name_la",$value)->where("erp_city_id!=".$id)->get("erp_cities");
           }
           if ($query->num_rows()) { 
           $this->form_validation->set_message('is_unique_la', lang('cities_name_exist'));
           return false;
           }
           else
               return true;
       }
}
/* End of file cities.php */
/* Location: ./application/controllers/admin/cities.php */

