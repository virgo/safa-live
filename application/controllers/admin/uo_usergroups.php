<?php

class Uo_usergroups extends Safa_Controller {

    public $module = "uo_usergroups";

    public function __construct() {
        parent::__construct();
        $this->layout = 'new';
		
        $this->load->helper('cookie');
        $this->load->model('uo_usergroups_model');
        $this->load->model('uoprivileges_model');
        $this->lang->load('admins/uo_usergroups');
//        
        permission();
        if (!admin_permission())
            show_404();
    }

    public function index() {
//        echo $this->input->cookie("admin_language");        
        if (isset($_GET['search']))
            $this->search();
//        $this->uo_usergroups_model->join = TRUE;
        $data["total_rows"] = $this->uo_usergroups_model->get(true);
        $this->uo_usergroups_model->offset = $this->uri->segment("4");
        $this->uo_usergroups_model->limit = 10;
        $data["items"] = $this->uo_usergroups_model->get();
        $config['uri_segment'] = 4;
        $config['base_url'] = site_url('admin/uo_usergroups/index');
        $config['total_rows'] = $data["total_rows"];
        $config['per_page'] = $this->uo_usergroups_model->limit;
        $config['suffix'] = $this->input->get() ? '?' . http_build_query($_GET) : NULL;
        $this->load->library('pagination');
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();
        $this->load->view('admin/uo_usergroups/index', $data);
    }

    public function add() {

        $data['items'] = $this->uoprivileges_model->get();
        $this->load->library("form_validation");
        $this->form_validation->set_rules('name_ar', 'lang:uo_usergroups_name_ar', 'trim|required|is_unique[safa_uo_usergroups.name_ar]|min_length[3]|max_length[50]');
        $this->form_validation->set_rules('name_la', 'lang:uo_usergroups_name_la', 'trim|required|is_unique[safa_uo_usergroups.name_la]|min_length[3]|max_length[50]');
        $this->form_validation->set_rules('privilege[]', 'lang:uo_usergroups_privilege', 'trim');
        if ($this->form_validation->run() == false) {
            $this->load->view("admin/uo_usergroups/add", $data);
        } else {

            $this->uo_usergroups_model->name_ar = $this->input->post('name_ar');
            $this->uo_usergroups_model->name_la = $this->input->post('name_la');


            $id = $this->uo_usergroups_model->save();
            if ($this->input->post('privilege'))
                foreach ($this->input->post('privilege') as $privilege) {
                    $this->db->insert('safa_uo_usergroups_uoprivileges', array(
                        'safa_uo_usergroup_id' => $id,
                        'safa_uoprivilege_id' => $privilege
                    ));
                }
            $this->load->view('redirect_message', array('msg' => lang('global_added_message'), 'url' => site_url('admin/uo_usergroups'),'model_title' => lang('uo_usergroups_title'), 'action' => lang('uo_usergroups_add_title')));
            // redirect("admin/cities/index");
        }
    }

    public function edit($id) {

        if (!$id)
            show_404();

        $this->uo_usergroups_model->safa_uo_usergroup_id = $id;
        $data['items'] = $this->uo_usergroups_model->get();
        $data['privileges'] = $this->uoprivileges_model->get();
        $data['user_privileges'] = array();
        foreach ($this->db->where('safa_uo_usergroup_id', $id)->get('safa_uo_usergroups_uoprivileges')->result() as $privileges)
            $data['user_privileges'][] = $privileges->safa_uoprivilege_id;
        if (!$data['items'])
            show_404();

        $this->load->library("form_validation");
        $this->form_validation->set_rules('name_ar', 'lang:uo_usergroups_name_la', 'trim|required|callback_is_unique_ar[' . $this->uri->segment("4") . ']|min_length[3]|max_length[50]');
        $this->form_validation->set_rules('name_la', 'lang:uo_usergroups_name_la', 'trim|required|callback_is_unique_la[' . $this->uri->segment("4") . ']|min_length[3]|max_length[50]');
        $this->form_validation->set_rules('privilege[]', 'lang:uo_usergroups_privilege', 'trim');
        if ($this->form_validation->run() == false) {
            $this->load->view("admin/uo_usergroups/edit", $data);
        } else {

            $this->uo_usergroups_model->name_ar = $this->input->post('name_ar');
            $this->uo_usergroups_model->name_la = $this->input->post('name_la');
            $this->uo_usergroups_model->save();
            $this->db->where('safa_uo_usergroup_id', $id)->delete('safa_uo_usergroups_uoprivileges');
            if ($this->input->post('privilege'))
                foreach ($this->input->post('privilege') as $privilege) {
                    $this->db->insert('safa_uo_usergroups_uoprivileges', array(
                        'safa_uo_usergroup_id' => $id,
                        'safa_uoprivilege_id' => $privilege
                    ));
                }
            $this->load->view('redirect_message', array('msg' => lang('global_updated_message'), 'url' => site_url('admin/uo_usergroups'), 'id' => $this->uri->segment("4"),'model_title' => lang('uo_usergroups_title'), 'action' => lang('uo_usergroups_edit_title')));
            //redirect("admin/cities/index");
        }
    }

    function delete() {
        $this->layout='ajax';
        if (!$this->input->is_ajax_request()) {
            redirect('admin/uo_usergroups/index');
        } else {
            $safa_uo_usergroup_id = $this->input->post('safa_uo_usergroup_id');
            $data = $this->uo_usergroups_model->check_delete_ability($safa_uo_usergroup_id);

            if ($data == 0) {
                $this->uo_usergroups_model->safa_uo_usergroup_id = $safa_uo_usergroup_id;
                if ($this->uo_usergroups_model->delete())
                    echo json_encode(array('response' => TRUE, 'msg' => 'deleted'));
            }
            else {
                echo json_encode(array('response' => FALSE, 'msg' => 'not deleted'));
            }
        }
    }

    function search() {

        if ($this->input->get("name_ar"))
            $this->uo_usergroups_model->name_ar = $this->input->get("name_ar");

        if ($this->input->get("name_la"))
            $this->uo_usergroups_model->name_la = $this->input->get("name_la");
    }

    function alpha_dash_space($name_la) {
        if (!preg_match("/^([-a-z_ ])+$/i", $name_la)) {
            $this->form_validation->set_message('alpha_dash_space', lang('cities_name_invalid'));
            return FALSE;
        } else {
            return true;
        }
    }

    function is_unique_ar($value, $id = false) {
        if ($id == false) {
            $query = $this->db->limit(1)->where("name_ar", $value)->get("safa_uo_usergroups");
        } else {
            $query = $this->db->limit(1)->where("name_ar", $value)->where("safa_uo_usergroup_id!=" . $id)->get("safa_uo_usergroups");
        }
        if ($query->num_rows()) {
            $this->form_validation->set_message('is_unique_ar', lang('uo_usergroups_name_exist'));
            return false;
        }
        else
            return true;
    }

    function is_unique_la($value, $id = false) {
        if ($id == false) {
            $query = $this->db->limit(1)->where("name_la", $value)->get("safa_uo_usergroups");
        } else {
            $query = $this->db->limit(1)->where("name_la", $value)->where("safa_uo_usergroup_id!=" . $id)->get("safa_uo_usergroups");
        }
        if ($query->num_rows()) {
            $this->form_validation->set_message('is_unique_la', lang('uo_usergroups_name_exist'));
            return false;
        }
        else
            return true;
    }

}

/* End of file cities.php */
/* Location: ./application/controllers/admin/cities.php */


