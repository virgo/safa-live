<?php

class Hotels_meals extends Safa_Controller {

    public $module = "hotels_meals";

    public function __construct() {
        parent::__construct();
        
        $this->layout='new';
        $this->load->model('hotels_meals_model');
        $this->lang->load('admins/hotels_meals');

        permission();
    }
    
    public function index() {      
        
        if(isset($_GET['search'])) $this->search();
        $this->hotels_meals_model->join = TRUE;
        $data["total_rows"] = $this->hotels_meals_model->get(true);
        $this->hotels_meals_model->offset = $this->uri->segment("4");
        $this->hotels_meals_model->limit = 10;
        $data["items"] = $this->hotels_meals_model->get();
        $config['uri_segment'] = 4;
        $config['base_url'] = site_url('admin/hotels_meals/index');
        $config['total_rows'] = $data["total_rows"];
        $config['per_page'] = $this->config->item('per_page');
        $config['suffix'] = $this->input->get() ? '?'.http_build_query($_GET) : NULL;
        $this->load->library('pagination');
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();
        $this->load->view('admin/hotels_meals/index', $data);
    }

    public function add() {

        $this->load->library("form_validation");
        $this->form_validation->set_rules('name_ar', 'lang:hotels_meals_name_ar', 'trim|required');
        $this->form_validation->set_rules('name_la', 'lang:hotels_meals_name_la', 'trim|required');
        $this->form_validation->set_rules('description_ar', 'lang:hotels_meals_description_ar', 'trim');
        $this->form_validation->set_rules('description_la', 'lang:hotels_meals_description_la', 'trim');
        $this->form_validation->set_rules('code', 'lang:hotels_meals_code', 'trim|required');
        
        if ($this->form_validation->run() == false) {

            $this->load->view("admin/hotels_meals/add");
        } else {
            
            $this->hotels_meals_model->name_ar = $this->input->post('name_ar');
            $this->hotels_meals_model->name_la = $this->input->post('name_la');
            $this->hotels_meals_model->description_ar = $this->input->post('description_ar');
            $this->hotels_meals_model->description_la = $this->input->post('description_la');
            $this->hotels_meals_model->code = $this->input->post('code');
            
            $this->hotels_meals_model->save();
            $this->load->view('redirect_message', array('msg' => lang('global_added_message'), 'url' => site_url('admin/hotels_meals/index'),'model_title'=> lang('menu_main_hotels_meals'),'action'=>lang('hotels_meals_add_title')));
    
        }
    }
    public function edit($id) {

        if ( ! $id)
            show_404();
        
        $this->hotels_meals_model->erp_meal_id = $id;
        $data['items'] = $this->hotels_meals_model->get();
        
        $data['item']=$this->hotels_meals_model->get();
        
        
        if ( ! $data['items'])
            show_404();
        
        $this->load->library("form_validation");
        $this->form_validation->set_rules('name_ar', 'lang:hotels_meals_name_ar', 'trim|required');
        $this->form_validation->set_rules('name_la', 'lang:hotels_meals_name_la', 'trim|required');
        $this->form_validation->set_rules('description_ar', 'lang:hotels_meals_description_ar', 'trim');
        $this->form_validation->set_rules('description_la', 'lang:hotels_meals_description_la', 'trim');
        $this->form_validation->set_rules('code', 'lang:hotels_meals_code', 'trim|required');
        
        if ($this->form_validation->run() == false) {
            $this->load->view("admin/hotels_meals/edit", $data);
        } else {
            
            $this->hotels_meals_model->name_ar = $this->input->post('name_ar');
            $this->hotels_meals_model->name_la = $this->input->post('name_la');
            $this->hotels_meals_model->description_ar = $this->input->post('description_ar');
            $this->hotels_meals_model->description_la = $this->input->post('description_la');
            $this->hotels_meals_model->code = $this->input->post('code');
           
            $this->hotels_meals_model->save();
            $this->load->view('redirect_message', array('msg' => lang('global_updated_message'), 'url' => site_url('admin/hotels_meals/index'), 'id'=>$this->uri->segment("4"),'model_title'=> lang('menu_main_hotels_meals'),'action'=>lang('hotels_meals_edit_title')));
        }
    }
    
    
     function delete() {
        $this->layout='ajax';
        if (!$this->input->is_ajax_request()) {
           //redirect('admin/sub_hotels_levels/index')
        } else {
            
                $erp_meal_id = $this->input->post('erp_meal_id');
           
                $this->hotels_meals_model->erp_meal_id = $erp_meal_id;
              
                if ($this->hotels_meals_model->delete()) {
                    echo json_encode(array('response' => TRUE, 'msg' => 'deleted'));
                } else {
                    echo json_encode(array('response' => FALSE, 'msg' => 'not deleted user'));
                }
           
        }
    }
    


 



}

/* End of file hotels_meals.php */
/* Location: ./application/controllers/hotels_meals.php */
