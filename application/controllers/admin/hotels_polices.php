<?php

class Hotels_polices extends Safa_Controller {

    public $module = "hotels_polices";

    public function __construct() {
        parent::__construct();

        $this->load->model('hotels_polices_model');
        $this->lang->load('admins/hotels_polices');

        permission();
    }

    public function index() {   
        
        if(isset($_GET['search'])) $this->search();
        $this->hotels_polices_model->join = TRUE;
        $data["total_rows"] = $this->hotels_polices_model->get(true);
        $this->hotels_polices_model->offset = $this->uri->segment("4");
        $this->hotels_polices_model->limit = 10;
        $data["items"] = $this->hotels_polices_model->get();
        $config['uri_segment'] = 4;
        $config['base_url'] = site_url('admin/hotels_polices/index');
        $config['total_rows'] = $data["total_rows"];
        $config['per_page'] = $this->config->item('per_page');
        $config['suffix'] = $this->input->get() ? '?'.http_build_query($_GET) : NULL;
        $this->load->library('pagination');
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();
        $this->load->view('admin/hotels_polices/index', $data);
    }

    public function add() {

        $this->load->library("form_validation");
        $this->form_validation->set_rules('name_ar', 'lang:hotels_polices_name_ar', 'trim|required');
        $this->form_validation->set_rules('name_la', 'lang:hotels_polices_name_la', 'trim|required');
        
        if ($this->form_validation->run() == false) {

            $this->load->view("admin/hotels_polices/add");
        } else {
            
            $this->hotels_polices_model->name_ar = $this->input->post('name_ar');
            $this->hotels_polices_model->name_la = $this->input->post('name_la');

            $this->hotels_polices_model->save();
            $this->load->view('redirect_message', array('msg' => lang('global_added_message'), 'url' => site_url('admin/hotels_polices/index'),'model_title'=> lang('menu_main_hotels_polices'),'action'=>lang('hotels_polices_add_title')));
    
        }
    }
    public function edit($id) {

        if ( ! $id)
            show_404();
        
        $this->hotels_polices_model->erp_hotels_policies_id = $id;
        $data['items'] = $this->hotels_polices_model->get();
        
        if ( ! $data['items'])
            show_404();
        
        $this->load->library("form_validation");
        $this->form_validation->set_rules('name_ar', 'lang:hotels_polices_name_ar', 'trim|required');
        $this->form_validation->set_rules('name_la', 'lang:hotels_polices_name_la', 'trim|required');
       
        if ($this->form_validation->run() == false) {
            $this->load->view("admin/hotels_polices/edit", $data);
        } else {
            $this->hotels_polices_model->name_ar = $this->input->post('name_ar');
            $this->hotels_polices_model->name_la = $this->input->post('name_la');
           
            $this->hotels_polices_model->save();
            $this->load->view('redirect_message', array('msg' => lang('global_updated_message'), 'url' => site_url('admin/hotels_polices/index'), 'id'=>$this->uri->segment("4"),'model_title'=> lang('menu_main_hotels_polices'),'action'=>lang('hotels_polices_edit_title')));
        }
    }



    
    function delete() {
        $this->layout='ajax';
        if (!$this->input->is_ajax_request()) {
       
        } else {
            $erp_hotels_policies_id = $this->input->post('erp_hotels_policies_id');
           
                $this->hotels_polices_model->erp_hotels_policies_id = $erp_hotels_policies_id;
                if ($this->hotels_polices_model->delete()) {
                    echo json_encode(array('response' => TRUE, 'msg' => 'deleted'));
                } else {
                    echo json_encode(array('response' => FALSE, 'msg' => 'not deleted user'));
                }
           
        }
    }


 



}

/* End of file hotels_polices.php */
/* Location: ./application/controllers/hotels_polices.php */
