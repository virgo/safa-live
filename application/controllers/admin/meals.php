<?php
class meals extends Safa_Controller {

    public $module = "meals";
    public function __construct() {
        parent::__construct();
        $this->load->model('meals_model');
        $this->lang->load('admins/meals'); 
        permission();
    }
    public function index() {        
        if(isset($_GET['search'])) $this->search();
        $data["total_rows"] = $this->meals_model->get(true);
        $this->meals_model->offset = $this->uri->segment("4");
        $this->meals_model->limit =$this->config->item('per_page');
        $data["items"] = $this->meals_model->get();
        $config['uri_segment'] = 4;
        $config['base_url'] = site_url('admin/meals/index');
        $config['total_rows'] = $data["total_rows"];
        $config['per_page'] = $this->config->item('per_page');
        $config['suffix'] = $this->input->get() ? '?'.http_build_query($_GET) : NULL;
        $this->load->library('pagination');
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();
        $this->load->view('admin/meals/index', $data);
    }

    public function add() {
        
        $this->load->library("form_validation");
        
        $this->form_validation->set_rules('name_ar', 'lang:meals_namear', 'trim');
        $this->form_validation->set_rules('name_la', 'lang:meals_namela', 'trim');
        $this->form_validation->set_rules('des_namear', 'lang:meals_description_ar', 'trim');
        $this->form_validation->set_rules('des_namela', 'lang:meals_description_la', 'trim');
       
        
        if(name()=='name_ar')
            $this->form_validation->set_rules('name_ar', 'lang:meals_namear', 'trim|required');
        else
           $this->form_validation->set_rules('name_la', 'lang:meals_namela', 'trim|required');
        
        if ($this->form_validation->run() == false) {
              $this->load->view("admin/meals/add");
        } else {

            $this->meals_model->name_ar = $this->input->post('name_ar');
            $this->meals_model->name_la = $this->input->post('name_la');
            $this->meals_model->description_ar = $this->input->post('des_namear');
            $this->meals_model->description_la = $this->input->post('des_namela');
          $this->meals_model->save();
          $this->load->view('redirect_message', 
              array('msg' => lang('global_added_message'), 
             'url' => site_url('admin/meals/index'),
             'model_title'=> lang('parent_child_title'),
             'action'=>lang('add_meal')));  
        }
    }

    public function edit($id) {
        if(!$id)
            show_404 ();
        $this->meals_model->erp_meal_id=$id;
        $data['items']=$this->meals_model->get();
        $this->load->library("form_validation");
         $this->form_validation->set_rules('des_namear', 'lang:meals_description_ar', 'trim');
         $this->form_validation->set_rules('des_namela', 'lang:meals_description_la', 'trim');
        
        $this->form_validation->set_rules('name_ar', 'lang:meals_namear', 'trim');
        $this->form_validation->set_rules('name_la', 'lang:meals_namela', 'trim');
            
        if(name()=='name_ar')
            $this->form_validation->set_rules('name_ar', 'lang:meals_namear', 'trim|required');
        else
           $this->form_validation->set_rules('name_la', 'lang:meals_namela', 'trim|required');
 
        if ($this->form_validation->run() == false) {
              $this->load->view("admin/meals/edit",$data);
        } else {
            $this->meals_model->name_ar = $this->input->post('name_ar');
            $this->meals_model->name_la = $this->input->post('name_la');
            $this->meals_model->description_ar = $this->input->post('des_namear');
            $this->meals_model->description_la = $this->input->post('des_namela');
          $this->meals_model->save();
          $this->load->view('redirect_message', 
              array('msg' => lang('global_updated_message'), 
             'url' => site_url('admin/meals'),
             'id'=>$id,     
             'model_title'=> lang('parent_child_title'),
             'action'=>lang('edit_meal')));  
        }
    }
    function delete($id) {
        if(!$id)
        show_404();
                $this->meals_model->erp_meal_id= $id;
                $this->meals_model->delete();
                redirect('admin/meals/index');
                
               
        }
}
/* End of file meals.php */
/* Location: ./application/controllers/admin/meals.php */

