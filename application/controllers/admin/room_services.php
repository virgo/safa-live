<?php

class Room_services extends Safa_Controller {

    public $module = "sub_hotels_levels";

    public function __construct() {
        parent::__construct();
        $this->load->model('room_services_model');
        $this->lang->load('admins/room_services');
        permission();
    }

    public function index() {   
        
        if(isset($_GET['search'])) $this->search();
        $this->room_services_model->join = TRUE;
        $data["total_rows"] = $this->room_services_model->get(true);
        $this->room_services_model->offset = $this->uri->segment("4");
        $this->room_services_model->limit = 10;
        $data["items"] = $this->room_services_model->get();
        $config['uri_segment'] = 4;
        $config['base_url'] = site_url('admin/room_services/index');
        $config['total_rows'] = $data["total_rows"];
        $config['per_page'] = $this->config->item('per_page');
        $config['suffix'] = $this->input->get() ? '?'.http_build_query($_GET) : NULL;
        $this->load->library('pagination');
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();
        $this->load->view('admin/room_services/index', $data);
    }

    public function add() {

        $this->load->library("form_validation");
        $this->form_validation->set_rules('name_ar', 'lang:room_services_name_ar', 'trim|required');
        $this->form_validation->set_rules('name_la', 'lang:room_services_name_la', 'trim|required');
        
        if ($this->form_validation->run() == false) {

            $this->load->view("admin/room_services/add");
        } else {
            
            $this->room_services_model->name_ar = $this->input->post('name_ar');
            $this->room_services_model->name_la = $this->input->post('name_la');

            $this->room_services_model->save();
            $this->load->view('redirect_message', array('msg' => lang('global_added_message'), 'url' => site_url('admin/room_services/index'),'model_title'=> lang('menu_main_room_services'),'action'=>lang('room_services_add_title')));
    
        }
    }
    public function edit($id) {

        if ( !$id)
            show_404();
        
        $this->room_services_model->erp_room_service_id = $id;
        $data['items'] = $this->room_services_model->get();
        
        if ( ! $data['items'])
            show_404();
        
        $this->load->library("form_validation");
        $this->form_validation->set_rules('name_ar', 'lang:hotels_levels_name_ar', 'trim|required');
        $this->form_validation->set_rules('name_la', 'lang:hotels_levels_name_la', 'trim|required');
       
        if ($this->form_validation->run() == false) {
            $this->load->view("admin/room_services/edit", $data);
        } else {
            $this->room_services_model->name_ar = $this->input->post('name_ar');
            $this->room_services_model->name_la = $this->input->post('name_la');
           
            $this->room_services_model->save();
            $this->load->view('redirect_message', array('msg' => lang('global_updated_message'), 'url' => site_url('admin/room_services/index'), 'id'=>$this->uri->segment("4"),'model_title'=> lang('menu_main_room_services'),'action'=>lang('room_services_edit_title')));
        }
    }



    
    function delete() {
        $this->layout='ajax';
        if (!$this->input->is_ajax_request()) {
           
        } else {
            $erp_room_service_id = $this->input->post('erp_room_service_id');
           
                $this->room_services_model->erp_room_service_id = $erp_room_service_id;
                if ($this->room_services_model->delete()) {
                    echo json_encode(array('response' => TRUE, 'msg' => 'deleted'));
                } else {
                    echo json_encode(array('response' => FALSE, 'msg' => 'not deleted user'));
                }
           
        }
    }


 



}

/* End of file sub_hotels_levels.php */
/* Location: ./application/controllers/sub_hotels_levels.php */
