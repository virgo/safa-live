<?php

class Port_halls extends Safa_Controller {

    public $module = "port_halls";

    public function __construct() {
        parent::__construct();

        $this->layout = 'new';
        $this->load->model('port_halls_model');
        $this->lang->load('admins/port_halls');
        permission();
    }

    public function index() {

        if (isset($_GET['search']))
            $this->search();
        $data["total_rows"] = $this->port_halls_model->search(true);
        $this->port_halls_model->offset = $this->uri->segment("4");
        $this->port_halls_model->limit = 10;
        $this->port_halls_model->join = TRUE;
        $data["items"] = $this->port_halls_model->get();
        $config['uri_segment'] = 4;
        $config['base_url'] = site_url('admin/port_halls/index');
        $config['total_rows'] = $data["total_rows"];
        $config['per_page'] = $this->port_halls_model->limit;
        $config['suffix'] = $this->input->get() ? '?' . http_build_query($_GET) : NULL;
        $this->load->library('pagination');
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();
        $this->load->view('admin/port_halls/index', $data);
    }

    public function add() {
        $data["erp_ports"] = $this->create_erp_ports_array();
        $this->load->library("form_validation");
        $this->form_validation->set_rules('erp_port_id', 'lang:port_halls_erp_port_id', 'trim|required');
        $this->form_validation->set_rules('name_ar', 'lang:port_halls_name_ar', 'trim|required|is_unique[erp_port_halls.name_ar]');
        $this->form_validation->set_rules('name_la', 'lang:port_halls_name_la', 'trim|required|is_unique[erp_port_halls.name_la]');
        $this->form_validation->set_rules('code', 'lang:port_halls_code', 'trim|required');

        if ($this->form_validation->run() == false) {
            $this->load->view("admin/port_halls/add", $data);
        } else {

            $this->port_halls_model->erp_port_id = $this->input->post('erp_port_id');
            $this->port_halls_model->name_ar = $this->input->post('name_ar');
            $this->port_halls_model->name_la = $this->input->post('name_la');
            $this->port_halls_model->code = $this->input->post('code');

            $this->port_halls_model->save();
            $this->load->view('admin/redirect_message', array('msg' => lang('global_added_message'), 'url' => site_url('admin/port_halls'), 'model_name' => 'port_halls', 'model_title' => lang('menu_main_hole_airports'), 'action' => lang('port_halls_add_title')));
        }
    }

    public function edit($id) {

        if (!$id)
            show_404();

        $this->port_halls_model->erp_port_hall_id = $id;
        $data['items'] = $this->port_halls_model->get();
        $data["erp_ports"] = $this->create_erp_ports_array();
        if (!$data['items'])
            show_404();

        $this->load->library("form_validation");
        $this->form_validation->set_rules('erp_port_id', 'lang:port_halls_erp_port_id', 'trim|required');
        $this->form_validation->set_rules('name_ar', 'lang:port_halls_name_ar', 'trim|required');
        $this->form_validation->set_rules('name_la', 'lang:port_halls_name_la', 'trim|required');
        $this->form_validation->set_rules('code', 'lang:port_halls_code', 'trim|required');

        if ($this->form_validation->run() == false) {
            $this->load->view("admin/port_halls/edit", $data);
        } else {
            $this->port_halls_model->erp_port_id = $this->input->post('erp_port_id');
            $this->port_halls_model->name_ar = $this->input->post('name_ar');
            $this->port_halls_model->name_la = $this->input->post('name_la');
            $this->port_halls_model->code = $this->input->post('code');

            $this->port_halls_model->save();

            $this->load->view('admin/redirect_message', array('msg' => lang('global_updated_message'), 'url' => site_url('admin/port_halls'), 'id' => $this->uri->segment("4"), 'model_name' => 'port_halls', 'model_title' => lang('menu_main_hole_airports'), 'action' => lang('port_halls_edit_title')));
        }
    }

    function delete() {

        $this->layout = 'ajax';

        if (!$this->input->is_ajax_request()) {
            redirect('admin/port_halls/index');
        } else {
            $erp_port_hall_id = $this->input->post('erp_port_hall_id');
            $data = $this->port_halls_model->check_delete_ability($erp_port_hall_id);
            if ($data == 0) {
                $this->port_halls_model->erp_port_hall_id = $erp_port_hall_id;
                if ($this->port_halls_model->delete())
                    echo json_encode(array('response' => TRUE, 'msg' => 'deleted'));
            }
            else {
                echo json_encode(array('response' => FALSE, 'msg' => 'not deleted'));
            }
        }
    }

    function search() {

        if ($this->input->get("name_ar"))
            $this->port_halls_model->name_ar = $this->input->get("name_ar");

        if ($this->input->get("name_la"))
            $this->port_halls_model->name_la = $this->input->get("name_la");

        if ($this->input->get("erp_country_id"))
            $this->port_halls_model->erp_country_id = $this->input->get("erp_country_id");
    }

    function create_erp_ports_array() {
        $result = $this->port_halls_model->get_erp_ports();
        $ports = array();
        $ports[""] = lang('global_select_from_menu');
        if (isset($result) && count($result) > 0) {
            foreach ($result as $record) {
                $ports[$record->erp_port_id] = $record->name_code;
            }
        }
        return $ports;
    }

}

/* End of file port_halls.php */
/* Location: ./application/controllers/admin/port_halls.php */

