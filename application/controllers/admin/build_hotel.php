<?php
class Build_hotel extends Safa_Controller {
    public function __construct() {
        parent:: __construct();
        $this->load->model('hotels_model');
        $this->load->model('erp_companies_hotels_model');
        $this->load->model('erp_companies_hotels_officials_model');
        $this->load->model('erp_companies_hotels_policies_model');
        $this->load->model('erp_companies_hotels_rooms_model');
        $this->load->model('erp_hotel_floor_model');
        $this->load->helper('db_helper');
        $this->lang->load('hotels');
        $this->lang->load('build_hotel'); 
        $this->layout='new';
        permission();
   }
    public function add($hotel_id=false){ 
       if(!$hotel_id)
           show_404 (); 
            $data['hotel_name']=item('erp_hotels',name(),array('erp_hotel_id'=>$hotel_id));
            $data['hotel_city']=  item('erp_cities',name(),array('erp_city_id'=>item('erp_hotels','erp_city_id',array('erp_hotel_id'=>$hotel_id))));
            /*companyname*/
           if($this->destination == 'admin'){
               $data['company_name']= item('erp_company_types',name(),array('erp_company_type_id'=>'1'));
               $data['company_type']= item('erp_company_types',name(),array('erp_company_type_id'=>'1'));
               $data['company_type_id']='1';
               $company_id=-1;
               
           }
           if($this->destination == 'ea'){
               $data['company_name']= item('erp_company_types',name(),array('erp_company_type_id'=>'3'));
               $data['company_type']= item('erp_company_types',name(),array('erp_company_type_id'=>'3'));
               $data['company_type_id']='3';
               $company_id= session('ea_id');
           }
          if($this->destination == 'uo'){
               $data['company_name']= item('erp_company_types',name(),array('erp_company_type_id'=>'2'));
               $data['company_type']= item('erp_company_types',name(),array('erp_company_type_id'=>'2'));
               $data['company_type_id']='2';
               $company_id=session('uo_id');
           }
           $this->load->library("form_validation");
           /*hotels rooms*/
           if($this->input->post('rooms_floor') && count($this->input->post('rooms_floor'))>0 ){
               foreach($this->input->post('rooms_floor') as $index=>$item){
                    $this->form_validation->set_rules('rooms_floor['.$index.']','lang:rooms_floor','trim|required');
                    $this->form_validation->set_rules('rooms_section['.$index.']','lang:rooms_section','trim|required');
                    $this->form_validation->set_rules('rooms_count['.$index.']','lang:rooms_count','trim|required|integer|is_natural_no_zero');
                    $this->form_validation->set_rules('beds_count['.$index.']','lang:beds_count','trim|required|integer|is_natural_no_zero');
                    $this->form_validation->set_rules('date_from['.$index.']','lang:date_from','trim|required');
                    $this->form_validation->set_rules('date_to['.$index.']','lang:date_to','trim|required|callback_compare_date['.$index.']');
               }
               /*hotels officials*/
               if($this->input->post('official_name') && count($this->input->post('official_name')>0)){
                    foreach($this->input->post('official_name') as $index=>$item){
                        $this->form_validation->set_rules('official_name['.$index.']','lang:official_name','trim|required');
                        $this->form_validation->set_rules('official_email['.$index.']','lang:official_email','trim|valid_email');
                        $this->form_validation->set_rules('official_work['.$index.']','lang:official_work','trim|required');
                        $this->form_validation->set_rules('official_phone['.$index.']','lang:official_phone','trim|required');/*callback todo*/
                        $this->form_validation->set_rules('official_phonetype['.$index.']','lang:official_phonetype','trim|required');
                        $this->form_validation->set_rules('official_skype['.$index.']','lang:official_skype','trim');
                        $this->form_validation->set_rules('official_messenger['.$index.']','lang:official_massenger','trim');
                    }
               }
               /*hotels policies*/
               if($this->input->post('hotels_policy') && count($this->input->post('hotels_policy'))>0){
                   foreach($this->input->post('hotels_policy') as $index =>$value){
                       $this->form_validation->set_rules('hotels_policy['.$index.']','lang:hotel_policy','trim');
                       $this->form_validation->set_rules('policy_fieldtype['.$index.']','lang:hotel_policy','trim');
                       $this->form_validation->set_rules('policy_description['.$index.']','lang:hotel_policy','trim');
                   }
               }
               
            }
            if($this->form_validation->run()== false)
            {       
                $this->load->view('admin/build_hotel/build_hotel',$data);
                 
            }
            else{
                 /*inserting the data into database*/
                  $this->erp_companies_hotels_model->erp_company_type_id=$data['company_type_id'];
                  $this->erp_companies_hotels_model->company_id=$company_id;
                  $this->erp_companies_hotels_model->erp_hotel_id=$hotel_id;
                  $hotel_company_id=$this->erp_companies_hotels_model->save();
                  
                  if(isset($hotel_company_id)){
                     /*inserting the rooms details*/
                     if(($this->input->post('rooms_floor')&& count($this->input->post('rooms_floor'))>0 )){
                         foreach($this->input->post('rooms_floor') as $index=>$value){
                                   $this->erp_companies_hotels_rooms_model->section =$_POST['rooms_section'][$index];
                                   $this->erp_companies_hotels_rooms_model->rooms_count =$_POST['rooms_count'][$index];
                                   $this->erp_companies_hotels_rooms_model->available_from=$_POST['date_from'][$index];
                                   $this->erp_companies_hotels_rooms_model->available_to=$_POST['date_to'][$index];
                                   $this->erp_companies_hotels_rooms_model->beds_count=$_POST['beds_count'][$index];
                                   $this->erp_companies_hotels_rooms_model->erp_hotel_floor_id=$_POST['rooms_floor'][$index]; 
                                   $this->erp_companies_hotels_rooms_model->erp_companies_hotels_id=$hotel_company_id; 
                                   $this->erp_companies_hotels_rooms_model->save(); 
                         }
                      }
                  }
                  /*inserting the contact offecial*/
                  if(isset($hotel_company_id)){
                       if($this->input->post('official_name')&& count($this->input->post('official_name'))){
                           foreach($this->input->post('official_name') as $index=>$value){
                                   $this->erp_companies_hotels_officials_model->erp_companies_hotels_id=$hotel_company_id;
                                   $this->erp_companies_hotels_officials_model->position=$_POST['official_work'][$index];
                                   $this->erp_companies_hotels_officials_model->name=$_POST['official_name'][$index];
                                   $this->erp_companies_hotels_officials_model->email=$_POST['official_email'][$index];
                                   $this->erp_companies_hotels_officials_model->phone=$_POST['official_phone'][$index];
                                   $this->erp_companies_hotels_officials_model->skype=$_POST['official_skype'][$index];
                                   $this->erp_companies_hotels_officials_model->messenger=$_POST['official_messenger'][$index];
                                   $this->erp_companies_hotels_officials_model->phone_type_id=$_POST['official_phonetype'][$index];
                                   $this->erp_companies_hotels_officials_model->save();
                           }
                       } 
                  }
                  /*inserting  hotel_company_policy*/
                  if(isset($hotel_company_id)){
                      if($this->input->post('hotels_policy') && count($this->input->post('hotels_policy'))>0){
                          foreach($this->input->post('hotels_policy') as $index=>$value){
                              $this->erp_companies_hotels_policies_model->erp_hotels_policies_id=$_POST['hotels_policy'][$index];
                              $this->erp_companies_hotels_policies_model->policy_description=$_POST['policy_description'][$index];
                              $this->erp_companies_hotels_policies_model->policy_field_type=$_POST['policy_fieldtype'][$index];
                              $this->erp_companies_hotels_policies_model->erp_companies_hotels_id=$hotel_company_id;
                              $this->erp_companies_hotels_policies_model->save();
                          }
                      }
                  }
            }
    }
    
   /* callback validation method*/
    function compare_date($value=false,$index=0){
        $this->form_validation->set_message('compare_date',lang('compare_dates'));
        $date_to= date_create($_POST['date_to'][$index]);
        $date_from=date_create($_POST['date_from'][$index]);
        if($date_to<=$date_from)
            return false;
        else
           return true; 
       
    }

  }
