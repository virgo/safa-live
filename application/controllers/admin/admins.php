<?php

class Admins extends Safa_Controller {

    public $module = "admins";

    public function __construct() {
        parent::__construct();

        $this->layout='new';
        $this->load->model('admins_model');
        $this->lang->load('admins/admins');
//        
        permission();
    }

    public function index() {
        if (isset($_GET['search']))
            $this->search();
        $data["total_rows"] = $this->admins_model->search(true);
        $this->admins_model->offset = $this->uri->segment("4");
        $this->admins_model->limit = 10;
        $data["admins"] = $this->admins_model->search();
        $config['uri_segment'] = 4;
        $config['base_url'] = site_url('admin/admins/index');
        $config['total_rows'] = $data["total_rows"];
        $config['per_page'] = $this->admins_model->limit;
        $config['suffix'] = $this->input->get() ? '?' . http_build_query($_GET) : NULL;
        $this->load->library('pagination');
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();
        $this->load->view('admin/admins/index', $data);
    }

    public function add() {
        $this->load->library("form_validation");
        $this->form_validation->set_rules('username', 'lang:admins_username', 'trim|required|max_dblength[erp_admins.username]|is_unique[erp_admins.username]');
        $this->form_validation->set_rules('password', 'lang:admins_password', 'trim|required|matches[passconf]');
        $this->form_validation->set_rules('passconf', 'lang:admins_repeat_password', 'trim|required');
        $this->form_validation->set_rules('email', 'lang:admins_email', 'trim|required|valid_email|is_unique[erp_admins.email]');
        if ($this->form_validation->run() == false) {

            $this->load->view("admin/admins/add");
        } else {
            $this->admins_model->username = $this->input->post('username');
       if (strlen($this->input->post('password'))> 1)
             $this->admins_model->password = md5($this->input->post('password'));
            
       $this->admins_model->email = $this->input->post('email');
            $this->admins_model->save();
        $this->load->view('redirect_message', array('msg' => lang('global_added_message'), 'url' => site_url('admin/admins'), 'model_title'=> lang('menu_management_users'),'action'=>lang('admins_add_title')));
            //redirect("admin/admins/index");
        }
    }

    public function edit($id) {
        if (!$id)
            show_404();

        $this->admins_model->erp_admin_id = $id;
        $data['admins'] = $this->admins_model->get();

        if (!$data['admins'])
            show_404();

        $this->load->library("form_validation");
        $this->form_validation->set_rules('username', 'username', 'trim|required|callback_check_username|min_length[5]|max_length[12]');
        $this->form_validation->set_rules('password', 'lang:admins_password', 'trim|matches[passconf]');
        $this->form_validation->set_rules('passconf', 'lang:admins_repeat_password', 'trim');
        $this->form_validation->set_rules('email', 'lang:admins_email', 'trim|required|valid_email');
        if ($this->form_validation->run() == false) {
            $this->load->view("admin/admins/edit", $data);
        } else {
            $this->admins_model->username = $this->input->post('username');
            if (strlen($this->input->post('password'))> 1)
                $this->admins_model->password = md5($this->input->post('password'));
            
            $this->admins_model->email = $this->input->post('email');
            $this->admins_model->save();
            $this->load->view('redirect_message', array('msg' => lang('global_updated_message'), 'url' => site_url('admin/admins'), 'id'=>$this->uri->segment("4"),'model_title'=> lang('menu_management_users'),'action'=>lang('admins_edit_title')));
            //redirect("admin/admins/index");
        }
    }

         function delete($id=false) {
            if(!$id)
                show_404 ();
                $this->admins_model->erp_admin_id = $id;
                $this->admins_model->delete();
                redirect('admin/admins/index');
                
           
        }
    
//    
//    function delete() {
//        $this->layout='ajax';
//        if (!$this->input->is_ajax_request()) {
//            redirect('admin/admins/index');
//        } else {
//            $id = $this->input->post('erp_admin_id');
//            if (($id == 1) || (session('admin_user_id')) == $id) {
//                echo json_encode(array('response' => FALSE, 'msg' => 'can not delete admin or logIn user'));
//            } else {
//                $this->admins_model->erp_admin_id = $id;
//                if ($this->admins_model->delete()) {
//                    echo json_encode(array('response' => TRUE, 'msg' => 'deleted'));
//                } else {
//                    echo json_encode(array('response' => FALSE, 'msg' => 'not deleted'));
//                }
//            }
//        }
//    }

    function delete_all() {
        if ($this->input->post("delete_admins")) {
            $erp_admins_id = $this->input->post("delete_admins");

            foreach ($erp_admins_id as $admin) {
                if (($admin == 1) || (session('erp_admin_id') == $admin)) {
                    echo 'Cantnot delete admin';
                } else {
                    $this->admins_model->erp_admin_id = $admin;
                    $this->admins_model->delete();
                }
            }
        }
        else
            show_404();
        redirect("admin/admins/index");
    }

    function search() {

        if ($this->input->get("username"))
            $this->admins_model->username = $this->input->get("username");

        if ($this->input->get("email"))
            $this->admins_model->email = $this->input->get("email");
    }

    function check_username($username) {
        if ($this->uri->segment('3') == 'edit')
            $this->db->where('erp_admin_id !=', $this->uri->segment('4'));
        $this->db->where('username', $username);
        if ($this->db->get('erp_admins')->num_rows()) {
            $this->form_validation->set_message('check_username', lang('global_this_username_exists'));
            return FALSE;
        } else {
            return TRUE;
        }
    }

}

/* End of file poll.php */
/* Location: ./application/controllers/poll.php */