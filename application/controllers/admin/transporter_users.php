<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Transporter_users extends Safa_Controller {

    public $module = "transporter_users";

    public function __construct() {
        parent::__construct();
        $this->load->model('transporter_users_model');
        $this->lang->load('admins/transporter_users');
        permission();
    }

    public function index() {

        if (isset($_GET['search']))
            $this->search();
        $data["total_rows"] = $this->transporter_users_model->get(true);
        $this->transporter_users_model->offset = $this->uri->segment("4");
        $this->transporter_users_model->limit = 10;
        $this->transporter_users_model->join = TRUE;
        $data["items"] = $this->transporter_users_model->get();
        $config['uri_segment'] = 4;
        $config['base_url'] = site_url('admin/transporter_users/index');
        $config['total_rows'] = $data["total_rows"];
        $config['per_page'] = $this->transporter_users_model->limit;
        $config['suffix'] = $this->input->get() ? '?' . http_build_query($_GET) : NULL;
        $this->load->library('pagination');
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();
        $this->load->view('admin/transporter_users/index', $data);
    }

    public function add() {

        $this->load->library("form_validation");
        $this->form_validation->set_rules('safa_transporter_id', 'lang:safa_transporter_users_safa_transporter_id', 'trim|required');
        $this->form_validation->set_rules('name_ar', 'lang:safa_transporter_users_name_ar', 'trim|required');
        $this->form_validation->set_rules('name_la', 'lang:safa_transporter_users_name_la', 'trim|required');
        $this->form_validation->set_rules('username', 'lang:safa_transporter_users_username', 'trim|required');
        $this->form_validation->set_rules('password', 'lang:safa_transporter_users_password', 'trim|required');
        $this->form_validation->set_rules('passconf', 'lang:safa_transporter_users_repeat_password', 'trim|required|matches[password]');


        if ($this->form_validation->run() == false) {
            $this->load->view("admin/transporter_users/add");
        } else {

            $this->transporter_users_model->safa_transporter_id = $this->input->post('safa_transporter_id');
            $this->transporter_users_model->name_ar = $this->input->post('name_ar');
            $this->transporter_users_model->name_la = $this->input->post('name_la');
            $this->transporter_users_model->username = $this->input->post('username');
            $this->transporter_users_model->password = md5($this->input->post('password'));


            $this->transporter_users_model->save();
            $this->load->view('admin/redirect_message', array('msg' => lang('global_added_message'), 'url' => site_url('admin/transporter_users'), 'model_name' => 'transporter_users', 'model_title' => lang('menu_transport_companies_users'), 'action' => lang('safa_transporter_users_add')));
        }
    }

    public function edit($id) {

        if (!$id)
            show_404();

        $this->transporter_users_model->safa_transporter_user_id = $id;
        $data['item'] = $this->transporter_users_model->get();

        if (!$data['item'])
            show_404();

        $this->load->library("form_validation");
        $this->form_validation->set_rules('safa_transporter_id', 'lang:safa_transporter_users_safa_transporter_id', 'trim|required');
        $this->form_validation->set_rules('name_ar', 'lang:safa_transporter_users_name_ar', 'trim|required');
        $this->form_validation->set_rules('name_la', 'lang:safa_transporter_users_name_la', 'trim|required');
        $this->form_validation->set_rules('username', 'lang:safa_transporter_users_username', 'trim|required');
        $this->form_validation->set_rules('password', 'lang:safa_transporter_users_password', 'trim');
        $this->form_validation->set_rules('passconf', 'lang:safa_transporter_users_repeat_password', 'trim|matches[password]');


        if ($this->form_validation->run() == false) {
            $this->load->view("admin/transporter_users/edit", $data);
        } else {
            $this->transporter_users_model->safa_transporter_user_id = $id;
            $this->transporter_users_model->safa_transporter_id = $this->input->post('safa_transporter_id');
            $this->transporter_users_model->name_ar = $this->input->post('name_ar');
            $this->transporter_users_model->name_la = $this->input->post('name_la');
            $this->transporter_users_model->username = $this->input->post('username');
            $this->transporter_users_model->password = md5($this->input->post('password'));


            $this->transporter_users_model->save();

            $this->load->view('admin/redirect_message', array('msg' => lang('global_updated_message'), 'url' => site_url('admin/transporter_users'), 'id' => $this->uri->segment("4"), 'model_name' => 'transporter_users', 'model_title' => lang('menu_transport_companies_users'), 'action' => lang('safa_transporter_users_edit')));
        }
    }

    function delete() {
        if (!$this->input->is_ajax_request()) {
            redirect('admin/transporter_users/index');
        } else {
            $id = $this->input->post('safa_transporter_user_id');

            //echo json_encode(array('response' => FALSE, 'msg' => 'can not delete admin or logIn user'));

            $this->transporter_users_model->safa_transporter_user_id = $id;
            if ($this->transporter_users_model->delete()) {
                echo json_encode(array('response' => TRUE, 'msg' => 'deleted'));
            } else {
                echo json_encode(array('response' => FALSE, 'msg' => 'not deleted user'));
            }
        }
    }

//    function delete_all() {
//        if ($this->input->post("delete_items")) {
//            foreach ($this->input->post("delete_items") as $item) {
//                $this->transporter_users_model->safa_transporter_user_id = $item;
//
//                $this->transporter_users_model->delete();
//            }
//        }
//        else
//            show_404();
//        redirect("admin/transporter_users/index");
//    }
}

/* End of file transporter_users.php */
/* Location: ./application/controllers/transporter_users.php */