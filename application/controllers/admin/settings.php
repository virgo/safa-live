<?php

class settings extends Safa_Controller {

    function __construct() {
        parent::__construct();
    }

    function index(){
        
        
        $this->load->library('form_validation');
        $this->form_validation->set_rules('', '', '');
        if($this->form_validation->run() == FALSE)
            $this->load->view('admin/settings', $data);
        else
        {
            $this->input->post();
        }
    }
}
