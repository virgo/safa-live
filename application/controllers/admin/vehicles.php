<?php

class Vehicles extends Safa_Controller {

    public $module = "vehicles";

    public function __construct() {
        parent::__construct();
        $this->layout='new';
        $this->load->model('vehicles_model');
        $this->lang->load('admins/vehicles');
       permission();
    }

    public function index() {

        if (isset($_GET['search']))
            $this->search();
        $data["total_rows"] = $this->vehicles_model->get(true);
        $this->vehicles_model->offset = $this->uri->segment("4");
        $this->vehicles_model->limit = $this->config->item('per_page');
        $data["items"] = $this->vehicles_model->get();
        
        $config['uri_segment'] = 4;
        $config['base_url'] = site_url('admin/vehicles/index');
        $config['total_rows'] = $data["total_rows"];
        $config['per_page'] = $this->vehicles_model->limit;
        $config['suffix'] = $this->input->get() ? '?' . http_build_query($_GET) : NULL;
        $this->load->library('pagination');
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();
        $this->load->view('admin/vehicles/index', $data);
    }

    public function add() {

        $this->load->library("form_validation");
        $this->form_validation->set_rules('name_ar', 'lang:vehicles_name_ar', 'trim|unique_col[safa_vehicles.name_ar]');
        $this->form_validation->set_rules('name_la', 'lang:vehicles_name_la', 'trim|unique_col[safa_vehicles.name_la]');
         if(name()=='name_ar')
            $this->form_validation->set_rules('name_ar', 'lang:vehicles_name_ar', 'trim|required|unique_col[safa_vehicles.name_ar]');
      if(name()=='name_la')
         $this->form_validation->set_rules('name_la', 'lang:vehicles_name_la', 'trim|required|unique_col[safa_vehicles.name_la]');
        
         if ($this->form_validation->run() == false) {
            $this->load->view("admin/vehicles/add");
        } else {
                 $this->vehicles_model->name_ar = $this->input->post('name_ar');
                 $this->vehicles_model->name_la = $this->input->post('name_la');
                 $this->vehicles_model->save();
                 $this->load->view('redirect_message',
                   array('msg' => lang('global_added_message'), 
                  'url' => site_url('admin/vehicles'),
                  'model_title'=> lang('safa_vehicles_title'),'action'=>lang('add_vehicles_title')));
            }
    }

    public function edit($id = FALSE) {
        if (!$id)
            show_404();
        $this->vehicles_model->safa_vehicle_id = $id;
        $data['item'] = $this->vehicles_model->get();
        
        if (!$data['item'])
            show_404();
        $this->load->library("form_validation");
        $this->form_validation->set_rules('name_ar', 'lang:vehicles_name_ar', 'trim|unique_col[safa_vehicles.name_ar.'.$id.']');
        $this->form_validation->set_rules('name_la', 'lang:vehicles_name_la', 'trim|unique_col[safa_vehicles.name_la.'.$id.']');
         if(name()=='name_ar')
            $this->form_validation->set_rules('name_ar', 'lang:vehicles_name_ar', 'trim|required|unique_col[safa_vehicles.name_ar.'.$id.']');
      if(name()=='name_la')
         $this->form_validation->set_rules('name_la', 'lang:vehicles_name_la', 'trim|required|unique_col[safa_vehicles.name_la.'.$id.']');
        
        if ($this->form_validation->run() == false) {
            $this->load->view("admin/vehicles/edit", $data);
        } else {
            $this->vehicles_model->name_ar = $this->input->post('name_ar');
            $this->vehicles_model->name_la = $this->input->post('name_la');
            $this->vehicles_model->save();
            $this->load->view('redirect_message',
                   array('msg' => lang('global_added_message'), 
                  'url' => site_url('admin/vehicles'),
                   'id'=>$id,
                  'model_title'=> lang('safa_vehicles_title'),'action'=>lang('add_vehicles_title')));
            
        }
    }

    function delete($id = FALSE) {
        $this->vehicles_model->safa_vehicle_id =$id;
        $this->vehicles_model->delete();
        redirect('admin/vehicles/index');
    }

    function search() {

        if ($this->input->get("name_ar"))
            $this->v_model->name_ar = $this->input->get("name_ar");

        if ($this->input->get("name_la"))
            $this->vehicles_model->name_la = $this->input->get("name_la");
    }

}

/* End of file port_halls.php */
/* Location: ./application/controllers/admin/port_halls.php */

