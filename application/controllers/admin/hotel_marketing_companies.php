<?php

class Hotel_marketing_companies extends Safa_Controller {

    public $module = "hotel_marketing_companies";

    public function __construct() {
        parent::__construct();
        $this->layout = 'new';
        $this->load->model('hotel_marketing_companies_model');
        $this->lang->load('admins/hotel_marketing_companies');

        permission();
    }

    public function index() {  
        
        if(isset($_GET['search'])) $this->search();
        $this->hotel_marketing_companies_model->join = TRUE;
        $data["total_rows"] = $this->hotel_marketing_companies_model->get(true);
        $this->hotel_marketing_companies_model->offset = $this->uri->segment("4");
        $this->hotel_marketing_companies_model->limit = 10;
        $data["items"] = $this->hotel_marketing_companies_model->get();
        $config['uri_segment'] = 4;
        $config['base_url'] = site_url('admin/hotel_marketing_companies/index');
        $config['total_rows'] = $data["total_rows"];
        $config['per_page'] = $this->config->item('per_page');
        $config['suffix'] = $this->input->get() ? '?'.http_build_query($_GET) : NULL;
        $this->load->library('pagination');
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();
        $this->load->view('admin/hotel_marketing_companies/index', $data);
        
    }

    public function add() {

        $this->load->library("form_validation");
        $this->form_validation->set_rules('name_ar', 'lang:hotel_marketing_companies_name_ar', 'trim|required');
        $this->form_validation->set_rules('name_la', 'lang:hotel_marketing_companies_name_la', 'trim|required');
        $this->form_validation->set_rules('phone', 'lang:hotel_marketing_companies_phone', 'trim|required|numeric');
        $this->form_validation->set_rules('fax','lang:hotel_marketing_companies_fax','trim|required');
        $this->form_validation->set_rules('email', 'lang:hotel_marketing_comapnies_email','trim|required|valid_email');
        $this->form_validation->set_rules('remarks','lang:hotel_marketing_companies_remarks','trim');
        
        if ($this->form_validation->run() == false) {

            $this->load->view("admin/hotel_marketing_companies/add");
        } else {
            
            $this->hotel_marketing_companies_model->name_ar = $this->input->post('name_ar');
            $this->hotel_marketing_companies_model->name_la = $this->input->post('name_la');
            $this->hotel_marketing_companies_model->phone = $this->input->post('phone');
            $this->hotel_marketing_companies_model->fax = $this->input->post('fax');
            $this->hotel_marketing_companies_model->email = $this->input->post('email');
            $this->hotel_marketing_companies_model->remarks = $this->input->post('remarks');

            $this->hotel_marketing_companies_model->save();
            $this->load->view('redirect_message', array('msg' => lang('global_added_message'), 'url' => site_url('admin/hotel_marketing_companies/index'),'model_title'=> lang('menu_main_hotel_marketing_companies'),'action'=>lang('hotel_marketing_companies_add_title')));
    
        }
    }
    public function edit($id) {

        if ( ! $id)
            show_404();
        
        $this->hotel_marketing_companies_model->safa_hm_id = $id;
        $data['items'] = $this->hotel_marketing_companies_model->get();
        
        if ( ! $data['items'])
            show_404();
        
        $this->load->library("form_validation");
        $this->form_validation->set_rules('name_ar', 'lang:hotel_marketing_companies_name_ar', 'trim|required');
        $this->form_validation->set_rules('name_la', 'lang:hotel_marketing_companies_name_la', 'trim|required');
        $this->form_validation->set_rules('phone', 'lang:hotel_marketing_companies_phone', 'trim|required|numeric');
        $this->form_validation->set_rules('fax','lang:hotel_marketing_companies_fax','trim|required');
        $this->form_validation->set_rules('email', 'lang:hotel_marketing_companies_email','trim|required|valid_email');
        $this->form_validation->set_rules('remarks','lang:hotel_marketing_companies_remarks','trim');
       
        if ($this->form_validation->run() == false) {
            $this->load->view("admin/hotel_marketing_companies/edit", $data);
        } else {
            $this->hotel_marketing_companies_model->name_ar = $this->input->post('name_ar');
            $this->hotel_marketing_companies_model->name_la = $this->input->post('name_la');
            $this->hotel_marketing_companies_model->phone = $this->input->post('phone');
            $this->hotel_marketing_companies_model->fax = $this->input->post('fax');
            $this->hotel_marketing_companies_model->email = $this->input->post('email');
            $this->hotel_marketing_companies_model->remarks = $this->input->post('remarks');
            
            $this->hotel_marketing_companies_model->save();
            $this->load->view('redirect_message', array('msg' => lang('global_updated_message'), 'url' => site_url('admin/hotel_marketing_companies/index'), 'id'=>$this->uri->segment("4"),'model_title'=> lang('menu_main_hotel_marketing_companies'),'action'=>lang('hotel_marketing_companies_edit_title')));
        }
    }



    
    function delete() {
        $this->layout='ajax';
        if (!$this->input->is_ajax_request()) {
         
        } else {
            $safa_hm_id = $this->input->post('safa_hm_id');
           
                $this->hotel_marketing_companies_model->safa_hm_id = $safa_hm_id;
                if ($this->hotel_marketing_companies_model->delete()) {
                    echo json_encode(array('response' => TRUE, 'msg' => 'deleted'));
                } else {
                    echo json_encode(array('response' => FALSE, 'msg' => 'not deleted user'));
                }
           
        }
    }


 



}

/* End of file hotel_marketing_companies.php */
/* Location: ./application/controllers/hotel_marketing_companies.php */
