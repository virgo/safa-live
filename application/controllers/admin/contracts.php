<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Contracts extends safa_Controller {

    public $module = "contracts";

    public function __construct() {
        parent:: __construct();
        $this->load->model('contracts_model');
        $this->load->model('uo_contracts_ea_model');
        $this->load->model('eas_model');
        $this->load->model('ea_seasons_model');
        $this->load->model('ea_users_model');
        $this->load->model('ea_hotels_model');
        $this->load->model('uos_model');
        $this->load->helper('db_helper');
        $this->lang->load('contracts');
//        
        permission();
        if (!admin_permission() && !uo_permission())
            show_404();
    }
    public function index() {
        $this->contracts_model->safa_uo_id = FALSE;
        $data['user_id'] = session('users_id');
        $data['module_name'] = $this->module;
        if (isset($_GET['search']))
            $this->search();
        $data["total_rows"] = $this->contracts_model->search(true);
        $this->contracts_model->offset = $this->uri->segment("4");
        $this->contracts_model->limit = $this->config->item('per_page');
        $data["items"] = $this->contracts_model->search();
        $config['uri_segment'] = 4;
        $config['base_url'] = site_url('admin/contracts/index');
        $config['total_rows'] = $data["total_rows"];
        $config['per_page'] = $this->config->item('per_page');
        $config['suffix'] = $this->input->get() ? '?' . http_build_query($_GET) : NULL;
        $this->load->library('pagination');
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();
        /* get the uos to filter with them */
        $OUS = $this->get_uos();
        /* get the eas to filter with them */
        $eas = $this->get_external_agents();
        $data['eas'] = $eas;
        $data['uos'] = $OUS;
        $this->load->view('admin/contracts/index', $data);
    }

    public function add() {
        $this->load->library("form_validation");
        $this->form_validation->set_rules('contarct_name_la', 'lang:contracts_name_la', 'trim');
        $this->form_validation->set_rules('contarct_name_la', 'lang:contracts_name_la', 'trim');
        if (name() == "name_la")
            $this->form_validation->set_rules('contarct_name_la', 'lang:contracts_name_la', 'trim|required');
        if (name() == "name_ar")
            $this->form_validation->set_rules('contarct_name_ar', 'lang:contracts_name_ar', 'trim|required');
        $this->form_validation->set_rules('uasp_username', 'lang:uasp_username', 'trim');
        $this->form_validation->set_rules('uasp_password', 'lang:uasp_password', 'trim');
        $this->form_validation->set_rules('uasp_eacode', 'lang:uasp_eacode', 'trim');
        $this->form_validation->set_rules('contract_uo', 'lang:uo_contract', 'trim|required');
        $this->form_validation->set_rules('contarct_address', 'lang:contracts_address', 'trim');
        $this->form_validation->set_rules('contarct_phone', 'lang:contracts_phone', 'trim|required|integer');
        $this->form_validation->set_rules('contarct_ksa_address', 'lang:contracts_ksaaddress', 'trim');
        $this->form_validation->set_rules('contarct_ayata_num', 'lang:contracts_ayata_num', 'trim');
        $this->form_validation->set_rules('contarct_agency_symbol', 'lang:contracts_agencysymbol', 'trim|required');
        $this->form_validation->set_rules('contarct_agency_name', 'lang:contracts_agencyname', 'trim|required');
        $this->form_validation->set_rules('contarct_email', 'lang:contracts_email', 'trim|required|valid_email');
        $this->form_validation->set_rules('contarct_country', 'lang:contracts_country', 'trim|required|is_natural_no_zero');
        $this->form_validation->set_rules('contarct_fax', 'lang:contracts_fax', 'trim');
        $this->form_validation->set_rules('contarct_nationality', 'lang:contracts_nationality', 'trim|required|is_natural_no_zero');
        $this->form_validation->set_rules('contarct_username', 'lang:contracts_username', 'trim|required|alpha_dash|unique_col[safa_uo_contracts.contract_username]');

        $this->form_validation->set_rules('contarct_password', 'lang:contracts_password', 'trim|required');
        $this->form_validation->set_rules('contarct_confpassword', 'lang:contracts_repeat_password', 'trim|required|matches[contarct_password]');
        $this->form_validation->set_rules('contarct_responsible_name', 'lang:contracts_resposible_name', 'trim');
        $this->form_validation->set_rules('contarct_responsible_phone', 'lang:contracts_resposible_phone', 'trim');
        $this->form_validation->set_rules('contract_notes', '', 'trim');
        if ($this->form_validation->run() == false) {
            $this->load->view('admin/contracts/add');
        } else {

            $this->contracts_model->name_ar = $this->input->post("contarct_name_ar");
            $this->contracts_model->name_la = $this->input->post("contarct_name_la");
            $this->contracts_model->contract_username = $this->input->post('contarct_username');
          if($this->input->post('contarct_password'))
                $this->contracts_model->contract_password = md5($this->input->post('contarct_password'));
                
          
            $this->contracts_model->uasp_username = $this->input->post('uasp_username');
            $this->contracts_model->uasp_password = $this->input->post('uasp_password');
            $this->contracts_model->uasp_eacode = $this->input->post('uasp_eacode');
            
          $this->contracts_model->address = $this->input->post('contarct_address');
            $this->contracts_model->phone = $this->input->post('contarct_phone');
            $this->contracts_model->ksa_address = $this->input->post('contarct_ksa_address');
            $this->contracts_model->iata = $this->input->post('contarct_ayata_num');
            $this->contracts_model->ksa_phone = $this->input->post('contarct_ksa_phone');
            $this->contracts_model->agency_symbol = $this->input->post('contarct_agency_symbol');
            $this->contracts_model->safa_uo_id = $this->input->post("contract_uo");
            $this->contracts_model->agency_name = $this->input->post('contarct_agency_name');
            $this->contracts_model->email = $this->input->post('contarct_email');
            $this->contracts_model->country_id = $this->input->post('contarct_country');
            $this->contracts_model->fax = $this->input->post('contarct_fax');
            $this->contracts_model->responsible_name = $this->input->post('contarct_responsible_name');
            $this->contracts_model->nationality_id = $this->input->post('contarct_nationality');
            $this->contracts_model->responsible_phone = $this->input->post('contarct_responsible_phone');
            $this->contracts_model->notes = $this->input->post('contract_notes');
            $contract_id = $this->contracts_model->save();
            if ($contract_id > 0) {
                /* ADD IN THE HOTELS FOR THE CONTRACT */
                //$this->add_hotels($contract_id);
                /* add tourismplaces */
                $this->add_tourismplaces($contract_id);
                /* add_ea_itos */
                $this->add_ea_itos($contract_id);
                /* add_itos */
                $this->add_itos($contract_id);
                /* add_defualt_umrah_package */
                $this->add_default_umrahpackage($contract_id);
                /* add extenlaagent */
                $ea_id = $this->add_externalagents();
                /* add ea_user */
                if ($ea_id > 0) {
                    $this->add_ea_user($ea_id);
                    /* include the relation */
                    $ea_contract_id = $this->add_ea_contract($contract_id, $ea_id);
                    /* adding ea_season */
                    $season_id = $this->add_ea_seasons($ea_id);
                    /* adding ea package */
                    if ($season_id)
                        $package_id = $this->add_ea_package($ea_contract_id, $season_id);
                }
            }
            $this->load->view('redirect_message', array('msg' => lang('global_added_message'),
                'url' => site_url('admin/contracts'),
                'model_title' => lang('node_title'), 'action' => lang('contracts_add')));
        }
    }

    public function edit($id = false) {
        if (!$id)
            show_404();
        $this->contracts_model->safa_uo_contract_id = $id;
        $data['item'] = $this->contracts_model->get();
        $this->load->library("form_validation");
        $this->form_validation->set_rules('contarct_name_la', 'lang:contracts_name_la', 'trim');
        $this->form_validation->set_rules('contarct_name_la', 'lang:contracts_name_la', 'trim');
        if (name() == "name_la")
            $this->form_validation->set_rules('contarct_name_la', 'lang:contracts_name_la', 'trim|required');
        if (name() == "name_ar")
            $this->form_validation->set_rules('contarct_name_ar', 'lang:contracts_name_ar', 'trim|required');
        $this->form_validation->set_rules('uasp_username', 'lang:uasp_username', 'trim');
        $this->form_validation->set_rules('uasp_password', 'lang:uasp_password', 'trim');
        $this->form_validation->set_rules('uasp_eacode', 'lang:uasp_eacode', 'trim');
        $this->form_validation->set_rules('contract_uo', 'lang:uo_contract', 'trim|required');
        $this->form_validation->set_rules('contarct_address', 'lang:contracts_address', 'trim');
        $this->form_validation->set_rules('contarct_phone', 'lang:contracts_phone', 'trim|required|integer');
        $this->form_validation->set_rules('contarct_ksa_address', 'lang:contracts_ksaaddress', 'trim');
        $this->form_validation->set_rules('contarct_ayata_num', 'lang:contracts_ayata_num', 'trim');
        $this->form_validation->set_rules('contarct_agency_symbol', 'lang:contracts_agencysymbol', 'trim|required');
        $this->form_validation->set_rules('contarct_agency_name', 'lang:contracts_agencyname', 'trim|required');
        $this->form_validation->set_rules('contarct_email', 'lang:contracts_email', 'trim|required|valid_email');
        $this->form_validation->set_rules('contarct_country', 'lang:contracts_country', 'trim|required|is_natural_no_zero');
        $this->form_validation->set_rules('contarct_fax', 'lang:contracts_fax', 'trim');
        $this->form_validation->set_rules('contarct_nationality', 'lang:contracts_nationality', 'trim|required|is_natural_no_zero');
        $this->form_validation->set_rules('contarct_username', 'lang:contracts_username', 'trim|required|alpha_dash|unique_col[safa_uo_contracts.contract_username.' . $id . ']'); // to do again to be contained 
        $this->form_validation->set_rules('contarct_password', 'lang:contracts_password', 'trim');
        $this->form_validation->set_rules('contarct_confpassword', 'lang:contracts_repeat_password', 'trim|matches[contarct_password]');

        $this->form_validation->set_rules('contarct_responsible_name', 'lang:contracts_resposible_name', 'trim');
        $this->form_validation->set_rules('contarct_responsible_phone', 'lang:contracts_resposible_phone', 'trim');
        $this->form_validation->set_rules('contract_notes', '', 'trim');

        if ($this->form_validation->run() == false) {
            $this->load->view('admin/contracts/edit', $data);
        } else {
            $this->contracts_model->name_ar = $this->input->post("contarct_name_ar");
            $this->contracts_model->name_la = $this->input->post("contarct_name_la");
            $this->contracts_model->contract_username = $this->input->post('contarct_username');
           if($this->input->post('contarct_password'))
                $this->contracts_model->contract_password = md5($this->input->post('contarct_password'));
            $this->contracts_model->address = $this->input->post('contarct_address');
            $this->contracts_model->phone = $this->input->post('contarct_phone');
            $this->contracts_model->ksa_address = $this->input->post('contarct_ksa_address');
            $this->contracts_model->iata = $this->input->post('contarct_ayata_num');
            $this->contracts_model->ksa_phone = $this->input->post('contarct_ksa_phone');
            $this->contracts_model->agency_symbol = $this->input->post('contarct_agency_symbol');

            $this->contracts_model->safa_uo_id = $this->contracts_model->safa_uo_id = $this->input->post("contract_uo");

            $this->contracts_model->uasp_username = $this->input->post('uasp_username');
            $this->contracts_model->uasp_password = $this->input->post('uasp_password');
            $this->contracts_model->uasp_eacode = $this->input->post('uasp_eacode');
            
            $this->contracts_model->agency_name = $this->input->post('contarct_agency_name');
            $this->contracts_model->email = $this->input->post('contarct_email');
            $this->contracts_model->country_id = $this->input->post('contarct_country');
            $this->contracts_model->fax = $this->input->post('contarct_fax');
            $this->contracts_model->city_id = $this->input->post('contarct_city');
            $this->contracts_model->responsible_name = $this->input->post('contarct_responsible_name');
            $this->contracts_model->nationality_id = $this->input->post('contarct_nationality');
            $this->contracts_model->responsible_phone = $this->input->post('contarct_responsible_phone');
            $this->contracts_model->notes = $this->input->post('contract_notes');
            $contract_id = $this->contracts_model->save();
            $this->contracts_model->save();
            $this->load->view('redirect_message', array('msg' => lang('global_updated_message'),
                'url' => site_url('admin/contracts'),
                'id' => $id,
                'model_title' => lang('node_title'), 'action' => lang('contracts_edit')));
        }
    }

    function search() {

        if ($this->input->get("safa_uo_contract_id"))
            $this->contracts_model->safa_uo_contract_id = $this->input->get("safa_uo_contract_id");

        if ($this->input->get("safa_uos"))
            $this->contracts_model->safa_uo_id = $this->input->get("safa_uos");

        if ($this->input->get("contarct_username"))
            $this->contracts_model->contract_username = $this->input->get("contarct_username");
        if ($this->input->get("contarct_password"))
            $this->contracts_model->contract_password = $this->input->get("contarct_password");
        if ($this->input->get("contarct_name_ar"))
            $this->contracts_model->name_ar = $this->input->get("contarct_name_ar");
        if ($this->input->get("contarct_name_la"))
            $this->contracts_model->name_la = $this->input->get("contarct_name_la");
        if ($this->input->get("contarct_address"))
            $this->contracts_model->address = $this->input->get("contarct_address");
        if ($this->input->get("contarct_phone"))
            $this->contracts_model->phone = $this->input->get("contarct_phone");
        if ($this->input->get("contarct_ksa_address"))
            $this->contracts_model->ksa_address = $this->input->get("contarct_ksa_address");
        if ($this->input->get("contarct_ayata_num"))
            $this->contracts_model->iata = $this->input->get("contarct_ayata_num");
        if ($this->input->get("ksa_phone"))
            $this->contracts_model->ksa_phone = $this->input->get("ksa_phone");
        if ($this->input->get("contarct_agency_symbol"))
            $this->contracts_model->agency_symbol = $this->input->get("contarct_agency_symbol");
        if ($this->input->get("contarct_agency_name"))
            $this->contracts_model->agency_name = $this->input->get("contarct_agency_name");
        if ($this->input->get("contarct_email"))
            $this->contracts_model->email = $this->input->get("contarct_email");
        if ($this->input->get("contarct_country"))
            $this->contracts_model->country_id = $this->input->get("contarct_country");
        if ($this->input->get("contarct_fax"))
            $this->contracts_model->fax = $this->input->get("contarct_fax");
        if ($this->input->get("contarct_city"))
            $this->contracts_model->city_id = $this->input->get("contarct_city");
        if ($this->input->get("contarct_responsible_name"))
            $this->contracts_model->responsible_name = $this->input->get("contarct_responsible_name");
        if ($this->input->get("contarct_nationality"))
            $this->contracts_model->nationality_id = $this->input->get("contarct_nationality");
        if ($this->input->get("contarct_responsible_phone"))
            $this->contracts_model->responsible_phone = $this->input->get("contarct_responsible_phone");
        if ($this->input->get('safa_uo_eas'))
            $this->contracts_model->by_eas_id = $this->input->get('safa_uo_eas');
    }

    function delete($id) {
        if (!$id)
            show_404();
        $this->contracts_model->safa_uo_contract_id = $id;
        $this->contracts_model->delete();
        $this->load->view('redirect_message', array('msg' => lang('global_updated_message'),
            'url' => site_url('admin/contracts'),
            'model_title' => lang('node_title'), 'action' => ''));
    }

    function delete_all() {
        if (!$_POST['delete_items'])
            show_404();
        foreach ($_POST['delete_items'] as $item) {
            $this->contracts_model->safa_uo_contract_id = $item;
            $this->contracts_model->delete();
        }
        redirect("admin/contracts/index");
    }

    function add_externalagents() {
        //--- this function targets the external agents table------//
        $this->eas_model->name_ar = $this->input->post("contarct_name_ar");
        $this->eas_model->name_la = $this->input->post("contarct_name_la");
        $this->eas_model->erp_country_id = $this->input->post("contarct_country");
        $this->eas_model->phone = $this->input->post("contarct_phone");
        $this->eas_model->email = $this->input->post("contarct_email");
        $this->eas_model->fax = $this->input->post("contarct_fax");
        $this->eas_model->mobile = $this->input->post("contarct_fax");
        $eas_id = $this->eas_model->save();
        return $eas_id;
    }

    function add_ea_contract($contract_id, $ea_id) {
        $this->uo_contracts_ea_model->safa_uo_contract_id = $contract_id;
        $this->uo_contracts_ea_model->safa_ea_id = $ea_id;
        $ea_contract_id = $this->uo_contracts_ea_model->save();
        return $ea_contract_id;
    }

    function add_ea_user($ea_id) {
        $this->ea_users_model->safa_ea_id = $ea_id;
        $this->ea_users_model->name_ar = $this->input->post("contarct_name_ar");
        $this->ea_users_model->name_la = $this->input->post("contarct_name_la");
        $this->ea_users_model->username = $this->input->post("contarct_username");
        if($this->input->post("contarct_password"))
            $this->ea_users_model->password = md5($this->input->post("contarct_password"));
        $this->ea_users_model->email = $this->input->post("contarct_email");
        $this->ea_users_model->erp_language_id = 2;
        $this->ea_users_model->safa_ea_usergroup_id = 1;
        $this->ea_users_model->save();
    }

    function add_hotels($contract_id) {
        /* get the all hotels and applay all of them to the external agents */
        $this->contracts_model->safa_uo_contract_id = $contract_id;
        $affected_rows = $this->contracts_model->add_hotels();
        return $affected_rows;
    }

    function add_tourismplaces($contract_id) {
        /* get the all tourismplaces and applay all of them to the external agents */
        $this->contracts_model->safa_uo_contract_id = $contract_id;
        $affected_rows = $this->contracts_model->add_tourismplaces();
        return $affected_rows;
    }

    function add_itos($contract_id) {
        $this->contracts_model->safa_uo_contract_id = $contract_id;
        $this->contracts_model->add_itos();
    }

    function add_ea_itos($contract_id) {
        $this->contracts_model->safa_uo_contract_id = $contract_id;
        $num_rows = $this->contracts_model->add_ea_itos();
    }

    function get_external_agents() {
        $eas = $this->contracts_model->get_externalagents();
        $eas_arr = array();
        $name = name();
        $eas_arr[''] = lang('global_select_from_menu');
        foreach ($eas as $record) {
            $eas_arr[$record->safa_ea_id] = $record->$name;
        }
        return $eas_arr;
    }

    function get_uos() {
        $uos = $this->uos_model->get();
        $uos_arr = array();
        $name = name();
        $uos_arr['0'] = lang('global_select_from_menu');
        foreach ($uos as $record) {
            $uos_arr[$record->safa_uo_id] = $record->$name;
        }
        return $uos_arr;
    }

    function add_ea_seasons($ea_id = false) {
        $this->ea_seasons_model->safa_ea_id = $ea_id;
        $season_id = $this->ea_seasons_model->save();
        return $season_id;
    }

    function add_ea_package($uo_ea_contract_id = false, $season_id = false) {
        $ea_package_id = $this->contracts_model->add_ea_packages($uo_ea_contract_id, $season_id);
        return $ea_package_id;
    }
    function add_default_umrahpackage($contract_id) {
        $this->contracts_model->safa_uo_contract_id = $contract_id;
        $rows = $this->contracts_model->add_defulat_umrahpackage();
    }

    /*     * ****validate the username**** */
  function is_unique($value,$bool) {

        $boolean = (bool) $bool;
        if ($boolean == false)
            $query = $this->db->limit(1)->where("contract_username", $value)->where('safa_uo_contracts.safa_uo_id', $this->contracts_model->safa_uo_id)->get("safa_uo_contracts");
        else
            $query = $this->db->limit(1)->where("contract_username", $value)->where("safa_uo_contract_id!=" . $this->uri->segment('4'))->where('safa_uo_contracts.safa_uo_id', $this->contracts_model->safa_uo_id)->get("safa_uo_contracts");
        if ($query->num_rows() == 0)
            return true;
        else
            return false;
    }
} 
/* End of file contracts.php */
/* Location: ./application/controllers/http://www.googel.com/contracts.php */