<?php

class Hotels_rooms_types extends Safa_Controller {

    public $module = "hotels_rooms_types";

    public function __construct() {
        parent::__construct();

        $this->load->model('hotels_rooms_types_model');
        $this->lang->load('admins/hotels_rooms_types');

        permission();
    }

    public function index() {      
        
        if(isset($_GET['search'])) $this->search();
        $this->hotels_rooms_types_model->join = TRUE;
        $data["total_rows"] = $this->hotels_rooms_types_model->get(true);
        $this->hotels_rooms_types_model->offset = $this->uri->segment("4");
        $this->hotels_rooms_types_model->limit = 10;
        $data["items"] = $this->hotels_rooms_types_model->get();
        $config['uri_segment'] = 4;
        $config['base_url'] = site_url('admin/hotels_rooms_types/index');
        $config['total_rows'] = $data["total_rows"];
        $config['per_page'] = $this->config->item('per_page');
        $config['suffix'] = $this->input->get() ? '?'.http_build_query($_GET) : NULL;
        $this->load->library('pagination');
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();
        $this->load->view('admin/hotels_rooms_types/index', $data);
    }

    public function add() {

        $this->load->library("form_validation");
        $this->form_validation->set_rules('name_ar', 'lang:hotels_rooms_types_name_ar', 'trim|required');
        $this->form_validation->set_rules('name_la', 'lang:hotels_rooms_types_name_la', 'trim|required');
        $this->form_validation->set_rules('beds_number', 'lang:hotels_rooms_types_beds_number', 'required|integer');
        $this->form_validation->set_rules('overload', 'lang:hotels_rooms_types_overload', 'required|integer');
        
        
        if ($this->form_validation->run() == false) {

            $this->load->view("admin/hotels_rooms_types/add");
        } else {
            
            $this->hotels_rooms_types_model->name_ar = $this->input->post('name_ar');
            $this->hotels_rooms_types_model->name_la = $this->input->post('name_la');
            $this->hotels_rooms_types_model->beds_number = $this->input->post('beds_number');
            $this->hotels_rooms_types_model->overload = $this->input->post('overload');
            $this->hotels_rooms_types_model->timestamp = time();
            
            $this->hotels_rooms_types_model->save();
            $this->load->view('redirect_message', array('msg' => lang('global_added_message'), 'url' => site_url('admin/hotels_rooms_types/index'),'model_title'=> lang('menu_main_hotels_levels'),'action'=>lang('hotels_levels_add_title')));
    
        }
    }
    public function edit($id) {

        if ( ! $id)
            show_404();
        
        $this->hotels_rooms_types_model->erp_room_id = $id;
        $data['items'] = $this->hotels_rooms_types_model->get();
        
        if ( ! $data['items'])
            show_404();
        
        $this->load->library("form_validation");
        $this->form_validation->set_rules('name_ar', 'lang:hotels_rooms_types_name_ar', 'trim|required');
        $this->form_validation->set_rules('name_la', 'lang:hotels_rooms_types_name_la', 'trim|required');
        $this->form_validation->set_rules('beds_number', 'lang:hotels_rooms_types_beds_number', 'required|integer');
        $this->form_validation->set_rules('overload', 'lang:hotels_rooms_types_overload', 'required|integer');
        
        if ($this->form_validation->run() == false) {
            $this->load->view("admin/hotels_rooms_types/edit", $data);
        } else {
            
            $this->hotels_rooms_types_model->name_ar = $this->input->post('name_ar');
            $this->hotels_rooms_types_model->name_la = $this->input->post('name_la');
            $this->hotels_rooms_types_model->beds_number = $this->input->post('beds_number');
            $this->hotels_rooms_types_model->overload = $this->input->post('overload');
            $this->hotels_rooms_types_model->timestamp = time();
           
            $this->hotels_rooms_types_model->save();
            $this->load->view('redirect_message', array('msg' => lang('global_updated_message'), 'url' => site_url('admin/hotels_rooms_types/index'), 'id'=>$this->uri->segment("4"),'model_title'=> lang('menu_main_hotels_levels'),'action'=>lang('hotels_levels_edit_title')));
        }
    }
    
    
     function delete() {
        $this->layout='ajax';
        if (!$this->input->is_ajax_request()) {
           //redirect('admin/sub_hotels_levels/index')
        } else {
            
            $erp_room_id = $this->input->post('erp_room_id');
           
                $this->hotels_rooms_types_model->erp_room_id = $erp_room_id;
              
                if ($this->hotels_rooms_types_model->delete()) {
                    echo json_encode(array('response' => TRUE, 'msg' => 'deleted'));
                } else {
                    echo json_encode(array('response' => FALSE, 'msg' => 'not deleted user'));
                }
           
        }
    }
    


 



}

/* End of file rooms_types.php */
/* Location: ./application/controllers/rooms_typesphp */
