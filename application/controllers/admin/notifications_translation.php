<?php

class Notifications_translation extends Safa_Controller {

    public $module = "notifications_translation";

    public function __construct() {
        parent::__construct();
        $this->layout = 'new';

        $this->load->model('erp_system_events_model');
        $this->load->model('erp_languages_model');
        $this->load->model('erp_notifications_tags_model');
        $this->lang->load('admins/notifications_translation');
        permission();
    }

    public function index() {

        $this->load->library("form_validation");

        $this->form_validation->set_rules('language_1', 'lang:language_1', 'trim|required');
        $this->form_validation->set_rules('language_2', 'lang:language_2', 'trim|required');
        $this->form_validation->set_rules('subject_lang1', 'lang:subject_lang1', 'trim|required');

        $this->form_validation->set_rules('subject_lang2', 'lang:subject_lang2', 'trim|required');
        $this->form_validation->set_rules('body_lang2', 'lang:body_lang2', 'trim|required');

        if ($this->form_validation->run() == false) {
            $languages_arr1 = array('' => lang('select_from_menu'));
            $languages_arr2 = ddgen('erp_languages', FALSE, FALSE, FALSE, TRUE);
            $languages_arr = $languages_arr1 + $languages_arr2;
            $data['languages'] = $languages_arr;

            $erp_notifications_tags_rows = $this->erp_notifications_tags_model->get();
            $data['erp_notifications_tags_rows'] = $erp_notifications_tags_rows;

            $this->load->view("admin/notifications_translation/index", $data);
        } else {

            $language_2 = $this->input->post('language_2');
            $this->erp_languages_model->erp_language_id = $language_2;
            $erp_languages_row = $this->erp_languages_model->get();
            $lang_suffix = $erp_languages_row->suffix;

            $event_key = $this->input->post('subject_lang1');
            $this->erp_system_events_model->erp_system_events_id = $event_key;
            $erp_system_events_row = $this->erp_system_events_model->get();
            $name_ar = $erp_system_events_row->name_ar;
            $name_la = $erp_system_events_row->name_la;

            $subject_lang2 = $this->input->post('subject_lang2');
            $body_lang2 = $this->input->post('body_lang2');


            if (!file_exists("./static/xml/notifications/notifications_$lang_suffix.xml")) {

                $xml = new DOMDocument('1.0', 'utf-8');

                $root = $xml->createElement("notifications");
                $xml->appendChild($root);

                $system_event_key = $xml->createElement("key");
                $system_event_key_text = $xml->createTextNode($event_key);
                $system_event_key->appendChild($system_event_key_text);

                $system_event_name_ar = $xml->createElement("name_ar");
                $system_event_name_ar_text = $xml->createTextNode($name_ar);
                $system_event_name_ar->appendChild($system_event_name_ar_text);

                $system_event_name_la = $xml->createElement("name_la");
                $system_event_name_la_text = $xml->createTextNode($name_la);
                $system_event_name_la->appendChild($system_event_name_la_text);

                $notification_subject = $xml->createElement("subject");
                $notification_subject_text = $xml->createTextNode($subject_lang2);
                $notification_subject->appendChild($notification_subject_text);

                $notification_body = $xml->createElement("body");
                $notification_body_text = $xml->createTextNode($body_lang2);
                $notification_body->appendChild($notification_body_text);


                $notification = $xml->createElement("notification");
                $notification->appendChild($system_event_key);
                $notification->appendChild($system_event_name_ar);
                $notification->appendChild($system_event_name_la);
                $notification->appendChild($notification_subject);
                $notification->appendChild($notification_body);

                $root->appendChild($notification);

                $xml->formatOutput = true;
                //echo "<xmp>". $xml->saveXML() ."</xmp>";

                $xml->save("./static/xml/notifications/notifications_$lang_suffix.xml") or die("Error");
            } else {

                $xml = new DOMDocument('1.0', 'utf-8');

                /**
                 * Added to format the xml code, when open with editor.
                 */
                $xml->formatOutput = true;
                $xml->preserveWhiteSpace = false;

                $xml->Load("./static/xml/notifications/notifications_$lang_suffix.xml");

                /**
                 * 
                 * Check if this event inserted before
                 */
                $notifications = $xml->getElementsByTagName('notification');
                foreach ($notifications as $notification) {
                    $loop_current_key = $notification->getElementsByTagName('key');
                    $loop_current_key_value = $loop_current_key->item(0)->nodeValue;

                    if ($loop_current_key_value == $event_key) {
                        $xml->documentElement->removeChild($notification);
                        break;
                    }
                }
                //------------------------------------------------------------------

                $root = $xml->documentElement;
                $xml->appendChild($root);

                $system_event_key = $xml->createElement("key");
                $system_event_key_text = $xml->createTextNode($event_key);
                $system_event_key->appendChild($system_event_key_text);

                $system_event_name_ar = $xml->createElement("name_ar");
                $system_event_name_ar_text = $xml->createTextNode($name_ar);
                $system_event_name_ar->appendChild($system_event_name_ar_text);

                $system_event_name_la = $xml->createElement("name_la");
                $system_event_name_la_text = $xml->createTextNode($name_la);
                $system_event_name_la->appendChild($system_event_name_la_text);

                $notification_subject = $xml->createElement("subject");
                $notification_subject_text = $xml->createTextNode($subject_lang2);
                $notification_subject->appendChild($notification_subject_text);

                $notification_body = $xml->createElement("body");
                $notification_body_text = $xml->createTextNode($body_lang2);
                $notification_body->appendChild($notification_body_text);


                $notification = $xml->createElement("notification");
                $notification->appendChild($system_event_key);
                $notification->appendChild($system_event_name_ar);
                $notification->appendChild($system_event_name_la);
                $notification->appendChild($notification_subject);
                $notification->appendChild($notification_body);

                $root->appendChild($notification);

                $xml->save("./static/xml/notifications/notifications_$lang_suffix.xml") or die("Error");
            }
            $this->load->view('admin/redirect_message', array('msg' => lang('global_added_message'), 'url' => site_url('admin/notifications_translation'), 'model_name' => 'notifications_translation', 'model_title' => lang('menu_title'), 'action' => lang('notifications_translate')));
        }
    }

    function getTagElementsForAjax() {
        $erp_language_id = $_POST['erp_language_id'];

        $this->erp_languages_model->erp_language_id = $erp_language_id;
        $erp_languages_row = $this->erp_languages_model->get();

        $lang_suffix = $erp_languages_row->suffix;

        if (file_exists("./static/xml/notifications/notifications_$lang_suffix.xml")) {

            $xml = new DOMDocument('1.0', 'utf-8');
            /**
             * Added to format the xml code, when open with editor.
             */
            $xml->formatOutput = true;
            $xml->preserveWhiteSpace = false;

            $xml->Load("./static/xml/notifications/notifications_$lang_suffix.xml");


            $notifications = $xml->getElementsByTagName('notification');

            foreach ($notifications as $notification) {
                $loop_current_key = $notification->getElementsByTagName('key');
                $loop_current_key_value = $loop_current_key->item(0)->nodeValue;

                $loop_current_subject = $notification->getElementsByTagName('subject');
                $loop_current_subject_value = $loop_current_subject->item(0)->nodeValue;

                //$loop_current_body= $notification->getElementsByTagName('body');
                //$loop_current_body_value= $loop_current_body->item(0)->nodeValue;

                echo"<option value='$loop_current_key_value'>$loop_current_subject_value</option>";
            }
            exit;
        } else {
            echo "
    	 	";
            exit;
        }
    }

    function fillLanguage2DropdownByAjax() {
        $erp_language_id = $_POST['erp_language_id'];

        $erp_languages_rows = $this->erp_languages_model->get();

        echo"<option value=''>" . lang('select_from_menu') . "</option>";

        foreach ($erp_languages_rows as $erp_languages_row) {
            $loop_current_erp_language_id = $erp_languages_row->erp_language_id;
            $loop_current_language_name = $erp_languages_row->name;
            if ($loop_current_erp_language_id != $erp_language_id) {
                echo"<option value='$loop_current_key_value'>$loop_current_language_name</option>";
            }
        }
        exit;
    }

    function getTagDataForAjax() {
        if (isset($_POST['language_1'])) {
            if ($_POST['language_1'] != '') {

                $language_1 = $_POST['language_1'];

                $this->erp_languages_model->erp_language_id = $language_1;
                $erp_languages_row = $this->erp_languages_model->get();

                $lang_suffix = $erp_languages_row->suffix;

                if (file_exists("./static/xml/notifications/notifications_$lang_suffix.xml")) {
                    $event_key = $_POST['event_key'];
                    $xml = new DOMDocument('1.0', 'utf-8');
                    /**
                     * Added to format the xml code, when open with editor.
                     */
                    $xml->formatOutput = true;
                    $xml->preserveWhiteSpace = false;

                    $xml->Load("./static/xml/notifications/notifications_$lang_suffix.xml");
                    /**
                     * 
                     * Check if this event inserted before
                     */
                    $event_key_exist = false;
                    $notifications = $xml->getElementsByTagName('notification');
                    foreach ($notifications as $notification) {
                        $loop_current_key = $notification->getElementsByTagName('key');
                        $loop_current_key_value = $loop_current_key->item(0)->nodeValue;

                        if ($loop_current_key_value == $event_key) {
                            $event_key_exist = true;
                            /*
                              $loop_current_subject= $notification->getElementsByTagName('subject');
                              $loop_current_subject_value= $loop_current_subject->item(0)->nodeValue;
                             */
                            $loop_current_body = $notification->getElementsByTagName('body');
                            $loop_current_body_value = $loop_current_body->item(0)->nodeValue;


                            if (name() == 'name_ar') {
                                $loop_current_name = $notification->getElementsByTagName('name_ar');
                                $loop_current_name_value = $loop_current_name->item(0)->nodeValue;
                            } else if (name() == 'name_la') {
                                $loop_current_name = $notification->getElementsByTagName('name_la');
                                $loop_current_name_value = $loop_current_name->item(0)->nodeValue;
                            }


                            //------------------------------- Lang 2 -------------------------------------------------

                            $loop_current_subject_value_2 = '';
                            $loop_current_body_value_2 = '';

                            if (isset($_POST['language_2'])) {
                                if ($_POST['language_2'] != '') {

                                    $language_2 = $_POST['language_2'];

                                    $this->erp_languages_model->erp_language_id = $language_2;
                                    $erp_languages_row_2 = $this->erp_languages_model->get();

                                    $lang_suffix_2 = $erp_languages_row_2->suffix;

                                    if (file_exists("./static/xml/notifications/notifications_$lang_suffix_2.xml")) {
                                        $xml_2 = new DOMDocument('1.0', 'utf-8');
                                        /**
                                         * Added to format the xml code, when open with editor.
                                         */
                                        $xml_2->formatOutput = true;
                                        $xml_2->preserveWhiteSpace = false;

                                        $xml_2->Load("./static/xml/notifications/notifications_$lang_suffix_2.xml");
                                        /**
                                         * 
                                         * Check if this event inserted before
                                         */
                                        $notifications_2 = $xml_2->getElementsByTagName('notification');
                                        foreach ($notifications_2 as $notification_2) {
                                            $loop_current_key_2 = $notification_2->getElementsByTagName('key');
                                            $loop_current_key_value_2 = $loop_current_key_2->item(0)->nodeValue;

                                            if ($loop_current_key_value_2 == $event_key) {

                                                $loop_current_subject_2 = $notification_2->getElementsByTagName('subject');
                                                $loop_current_subject_value_2 = $loop_current_subject_2->item(0)->nodeValue;

                                                $loop_current_body_2 = $notification_2->getElementsByTagName('body');
                                                $loop_current_body_value_2 = $loop_current_body_2->item(0)->nodeValue;
                                                break;
                                            }
                                        }
                                    }
                                }
                            }
                            //----------------------------------------------------------------------------------------


                            echo $loop_current_name_value . '#$@&*#@#$#' . $loop_current_body_value . '#$@&*#@#$#' . $loop_current_subject_value_2 . '#$@&*#@#$#' . $loop_current_body_value_2;
                            exit;
                        }
                    }

                    if (!$event_key_exist) {
                        echo '';
                        exit;
                    }

                    //------------------------------------------------------------------
                } else {
                    echo '';
                    exit;
                }
            } else {
                echo '';
                exit;
            }
        } else {
            echo '';
            exit;
        }
    }

}
