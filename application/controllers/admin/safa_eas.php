<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Safa_eas extends Safa_Controller {

    public $module = "safa_eas";
    public function __construct() {
        parent::__construct();
        $this->layout = 'new';
	
        $this->load->model('eas_model');
        $this->load->model('ea_users_model');
        $this->load->model('ea_seasons_model');
        $this->load->model('ea_branches_model');
        $this->lang->load('admins/safa_eas');
        $this->lang->load('safa_ea_users');
        permission();
    }

    public function index() {

        #To get direct contracts 
        $this->eas_model->direct_contracts = TRUE;

        #To get indirect contracts
        $this->eas_model->indirect_contracts = TRUE;

        #To get num of trips
        $this->eas_model->trips = TRUE;

        if (isset($_GET['search']))
            $this->search();
        $data["total_rows"] = $this->eas_model->search(true);
        $this->eas_model->offset = $this->uri->segment("4");
        $this->eas_model->limit =$this->config->item('per_page');
        $this->eas_model->join = TRUE;
        $data["items"] = $this->eas_model->search();
        $config['uri_segment'] = 4;
        $config['base_url'] = site_url('admin/safa_eas/index');
        $config['total_rows'] = $data["total_rows"];
        $config['per_page'] = $this->eas_model->limit;
        $config['suffix'] = $this->input->get() ? '?' . http_build_query($_GET) : NULL;
        $this->load->library('pagination');
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();
        $this->load->view('admin/safa_eas/index', $data);
    }

    public function add() {

        $this->load->library("form_validation");
        $this->form_validation->set_rules('name_ar', 'lang:safa_eas_name_ar', 'trim|required|is_unique[safa_eas.name_ar]');
        $this->form_validation->set_rules('name_la', 'lang:safa_eas_name_la', 'trim|required|is_unique[safa_eas.name_la]');
        $this->form_validation->set_rules('erp_country_id', 'lang:safa_eas_erp_country_id', 'trim|required');
//        $this->form_validation->set_rules('logo', 'lang:safa_eas_logo', 'trim|required');
        
        //By Gouda
        //$this->form_validation->set_rules('phone', 'lang:safa_eas_phone', 'trim|required|valid_phone');
        //$this->form_validation->set_rules('mobile', 'lang:safa_eas_mobile', 'trim|required|valid_phone');
        $this->form_validation->set_rules('phone', 'lang:safa_eas_phone', 'trim|required');
        $this->form_validation->set_rules('mobile', 'lang:safa_eas_mobile', 'trim|required');
        
        
        $this->form_validation->set_rules('fax', 'lang:safa_eas_fax', 'trim|required');
        $this->form_validation->set_rules('email', 'lang:safa_eas_email', 'trim|required|valid_email');

        #validation on records that found in table ea_users
        $this->form_validation->set_rules('user_username', 'lang:safa_eas_user_username', 'trim|required|is_unique[safa_ea_users.username]');
        $this->form_validation->set_rules('user_password', 'lang:safa_eas_user_password', 'trim|required');
        $this->form_validation->set_rules('user_passconf', 'lang:safa_eas_user_repeat_password', 'trim|required|matches[user_password]');


        $data['erp_cities'] = ddgen('erp_cities', array('erp_city_id', name()), FALSE, FALSE, TRUE);
        
        if ($this->form_validation->run() == false) {
            $this->load->view("admin/safa_eas/add", $data);
        } else {

            #Upoad Logo
            $config['upload_path'] = './static/ea_files/';
            $config['allowed_types'] = 'gif|jpg|jpeg|png';
            $this->load->library('upload', $config);

            if ($this->upload->do_upload('logo')) {

                $upload_data = $this->upload->data();
//                print_r($upload_data);
                $this->eas_model->logo = $upload_data['file_name'];
            }
            # End Of Upload

            $this->eas_model->name_ar = $this->input->post('name_ar');
            $this->eas_model->name_la = $this->input->post('name_la');
            $this->eas_model->erp_country_id = $this->input->post('erp_country_id');
            $this->eas_model->phone = $this->input->post('phone');
            $this->eas_model->mobile = $this->input->post('mobile');
            $this->eas_model->fax = $this->input->post('fax');
            $this->eas_model->email = $this->input->post('email');
            $safa_ea_id = $this->eas_model->save();
            if ($safa_ea_id) {
                #insert in table ea_users by id
                $this->ea_users_model->name_ar = $this->input->post('name_ar');
                $this->ea_users_model->name_la = $this->input->post('name_la');
                $this->ea_users_model->email = $this->input->post('email');
                $this->ea_users_model->username = $this->input->post('user_username');
                $this->ea_users_model->password = md5($this->input->post('user_password'));
                $this->ea_users_model->safa_ea_usergroup_id = 1;
                $this->ea_users_model->safa_ea_id = $safa_ea_id;
                $this->ea_users_model->save();

                #insert in table ea_seasons by id
                $this->ea_seasons_model->safa_ea_id = $safa_ea_id;
                $this->ea_seasons_model->start_date = date('Y-m-d H:i');
                $expiry = date('Y-m-d H:i:s', strtotime('+1 year'));
                $this->ea_seasons_model->end_date = $expiry;
                $this->ea_seasons_model->name = 'الموسم الافتراضى';
                $this->ea_seasons_model->save();
                
                
            // Save branches
	       
	        $branches_name_la = $this->input->post('branches_name_la');
	        $branches_name_ar = $this->input->post('branches_name_ar');
	        $branches_erp_city_id = $this->input->post('branches_erp_city_id');
	        $branches_address = $this->input->post('branches_address');
	        $branches_phone = $this->input->post('branches_phone');
	        $branches_mobile = $this->input->post('branches_mobile');
	        $branches_fax = $this->input->post('branches_fax');
	        $branches_email = $this->input->post('branches_email');
	        
	        
	        if(ensure($branches_name_la)) {
                    foreach($branches_name_la as $key => $value) {
        		$this->ea_branches_model->safa_ea_id = $safa_ea_id;
	        	
        		
        		$this->ea_branches_model->name_la = $branches_name_la[$key];
        		$this->ea_branches_model->name_ar = $branches_name_ar[$key];
        		$this->ea_branches_model->erp_city_id = $branches_erp_city_id[$key];
        		$this->ea_branches_model->address = $branches_address[$key];
        		$this->ea_branches_model->phone = $branches_phone[$key];
        		$this->ea_branches_model->mobile = $branches_mobile[$key];
        		$this->ea_branches_model->fax = $branches_fax[$key];
        		$this->ea_branches_model->email = $branches_email[$key];
        		
        		$this->ea_branches_model->save();
                    }
	        }
                
            }

                $this->load->view('admin/redirect_message', array('msg' => lang('global_added_message'), 'url' => site_url('admin/safa_eas'),'model_name'=>'safa_eas', 'model_title'=> lang('menu_external_agent'),'action'=>lang('safa_eas_add_title')));  
        }
    }

    public function edit($id) {

        if (!$id)
            show_404();

        $this->eas_model->join = TRUE;

        $this->eas_model->safa_ea_id = $id;
        $data['items'] = $this->eas_model->get();

        if (!$data['items'])
            show_404();

        $this->load->library("form_validation");
        $this->form_validation->set_rules('name_ar', 'lang:safa_eas_name_ar', 'trim|required');
        $this->form_validation->set_rules('name_la', 'lang:safa_eas_name_la', 'trim|required');
//        $this->form_validation->set_rules('erp_country_id', 'lang:safa_eas_erp_country_id', 'trim|required');
//        $this->form_validation->set_rules('logo', 'lang:safa_eas_logo', 'trim|required');
        
        //By Gouda
        //$this->form_validation->set_rules('phone', 'lang:safa_eas_phone', 'trim|required|valid_phone');
        //$this->form_validation->set_rules('mobile', 'lang:safa_eas_mobile', 'trim|required|valid_phone');
        $this->form_validation->set_rules('phone', 'lang:safa_eas_phone', 'trim|required');
        $this->form_validation->set_rules('mobile', 'lang:safa_eas_mobile', 'trim|required');
        
        $this->form_validation->set_rules('fax', 'lang:safa_eas_fax', 'trim|required');
        $this->form_validation->set_rules('email', 'lang:safa_eas_email', 'trim|required|valid_email');


        $data['erp_cities'] = ddgen('erp_cities', array('erp_city_id', name()), FALSE, FALSE, TRUE);
        
        $this->ea_branches_model->safa_ea_id = $id;
        $item_branches = $this->ea_branches_model->get();
    	$data['item_branches'] = $item_branches;
    	
    	
        if ($this->form_validation->run() == false) {
            $this->load->view("admin/safa_eas/edit", $data);
        } else {

    
         
            #Upoad Logo
            $config['upload_path'] = './static/ea_files/';
            $config['allowed_types'] = 'gif|jpg|jpeg|png';
            $this->load->library('upload', $config);

            if ($this->upload->do_upload('logo')) {

                $upload_data = $this->upload->data();
//                print_r($upload_data);
//                $error = array('error' => $this->upload->display_errors());
//               echo $error;
                $this->eas_model->logo = $upload_data['file_name'];
            }
            # End Of Upload


            $this->eas_model->name_ar = $this->input->post('name_ar');
            $this->eas_model->name_la = $this->input->post('name_la');
            $this->eas_model->erp_country_id = $this->input->post('erp_country_id');
//            $this->eas_model->logo = $this->input->post('logo');
            $this->eas_model->phone = $this->input->post('phone');
            $this->eas_model->mobile = $this->input->post('mobile');
            $this->eas_model->fax = $this->input->post('fax');
            $this->eas_model->email = $this->input->post('email');


            $this->eas_model->save();

            
            // Save branches
	        $this->ea_branches_model->safa_ea_id = $id;	        
        	$branches_name_la = $this->input->post('branches_name_la');
	        $branches_name_ar = $this->input->post('branches_name_ar');
	        $branches_erp_city_id = $this->input->post('branches_erp_city_id');
	        $branches_address = $this->input->post('branches_address');
	        $branches_phone = $this->input->post('branches_phone');
	        $branches_mobile = $this->input->post('branches_mobile');
	        $branches_fax = $this->input->post('branches_fax');
	        $branches_email = $this->input->post('branches_email');
	        
	        
	        if(ensure($branches_name_la)) {
	        	foreach($branches_name_la as $key => $value) {
        		$this->ea_branches_model->safa_ea_id = $id;
	        	
        		
        		
        		        		
	        	$this->ea_branches_model->safa_ea_branch_id = $key;
	        	
	        	// By Gouda
			    $this->ea_branches_model->name_la = false;
	        	$this->ea_branches_model->name_ar = false;
	        	$this->ea_branches_model->erp_city_id = false;
	        	$this->ea_branches_model->address = false;
	        	$this->ea_branches_model->phone = false;
	        	$this->ea_branches_model->mobile = false;
	        	$this->ea_branches_model->fax = false;
	        	$this->ea_branches_model->email = false; 
		        	
        		$current_room_row_count = $this->ea_branches_model->get(true);
        		if($current_room_row_count==0) {
        			
        			$this->ea_branches_model->name_la = $branches_name_la[$key];
	        		$this->ea_branches_model->name_ar = $branches_name_ar[$key];
	        		$this->ea_branches_model->erp_city_id = $branches_erp_city_id[$key];
	        		$this->ea_branches_model->address = $branches_address[$key];
	        		$this->ea_branches_model->phone = $branches_phone[$key];
	        		$this->ea_branches_model->mobile = $branches_mobile[$key];
	        		$this->ea_branches_model->fax = $branches_fax[$key];
	        		$this->ea_branches_model->email = $branches_email[$key];
        		
	        		$this->ea_branches_model->safa_ea_branch_id = false;
	        		$this->ea_branches_model->save();
        		} else {
        			
        			$this->ea_branches_model->name_la = $branches_name_la[$key];
	        		$this->ea_branches_model->name_ar = $branches_name_ar[$key];
	        		$this->ea_branches_model->erp_city_id = $branches_erp_city_id[$key];
	        		$this->ea_branches_model->address = $branches_address[$key];
	        		$this->ea_branches_model->phone = $branches_phone[$key];
	        		$this->ea_branches_model->mobile = $branches_mobile[$key];
	        		$this->ea_branches_model->fax = $branches_fax[$key];
	        		$this->ea_branches_model->email = $branches_email[$key];
        			
        			$this->ea_branches_model->save();
        		}
	        }
	        }
                
	       	// --- Delete Deleted rooms rows from Database --------------
	        
	       	// By Gouda
		    $this->ea_branches_model->name_la = false;
        	$this->ea_branches_model->name_ar = false;
        	$this->ea_branches_model->erp_city_id = false;
        	$this->ea_branches_model->address = false;
        	$this->ea_branches_model->phone = false;
        	$this->ea_branches_model->mobile = false;
        	$this->ea_branches_model->fax = false;
        	$this->ea_branches_model->email = false; 
	        
	        $branches_remove = $this->input->post('branches_remove');
	        if(ensure($branches_remove)) {    
	           	foreach($branches_remove as $branch_remove) {
	          		$this->ea_branches_model->safa_ea_branch_id = $branch_remove;
			        	$this->ea_branches_model->delete();
	         	}
	        }
	      	//---------------------------------------------------------------
	      	
            
            
            $this->load->view('admin/redirect_message', array('msg' => lang('global_updated_message'), 'url' => site_url('admin/safa_eas'), 'id'=>$this->uri->segment("4"),'model_name'=>'safa_eas','model_title'=> lang('menu_external_agent'),'action'=>lang('safa_eas_edit_title')));
        }
    }

//    function delete($id) {
//        if ( ! $id)
//            show_404();
//
//        
//        if( ! $this->eas_model->delete())
//            show_404 ();
//        redirect("admin/safa_eas/index");
//    }

    function delete() {
        
        $this->layout = 'ajax';
        
        if (!$this->input->is_ajax_request()) {
            redirect('admin/safa_eas/index');
        } else {
            $safa_ea_id = $this->input->post('safa_ea_id');
            $data = $this->eas_model->check_delete_ability($safa_ea_id);
//              echo $data; //die();
            if ($data == 0) {
                $this->eas_model->safa_ea_id = $safa_ea_id;
                if ($this->eas_model->delete())
                    echo json_encode(array('response' => TRUE, 'msg' => 'deleted'));
            }
            else {
                echo json_encode(array('response' => FALSE, 'msg' => 'not deleted'));
            }
        }
    }

    function delete_all() {
        if ($this->input->post("delete_items")) {
            foreach ($this->input->post("delete_items") as $item) {

                $this->eas_model->delete();
            }
        }
        else
            show_404();
        redirect("admin/safa_eas/index");
    }

    function search() {
        if ($this->input->get("erp_country_id"))
            $this->eas_model->erp_country_id = $this->input->get("erp_country_id");
        if ($this->input->get("name")){
            $this->eas_model->name = $this->input->get("name");
        }
    }

    public function users_index($safa_ea_id) {

        if (isset($_GET['search']))
            $this->search();
        $this->ea_users_model->offset = $this->uri->segment("3");
        $this->ea_users_model->limit = $this->config->item('per_page');
        $this->ea_users_model->safa_ea_id = $safa_ea_id;
        $this->ea_users_model->join = TRUE;
        $data["items"] = $this->ea_users_model->get();
        $config['uri_segment'] = 3;
        $config['base_url'] = site_url('admin/ea_users/index');
        $config['per_page'] = $this->ea_users_model->limit;
        $config['suffix'] = $this->input->get() ? '?' . http_build_query($_GET) : NULL;
        $this->load->library('pagination');
        $this->pagination->initialize($config);
        
        
        $data['safa_ea_id']=$safa_ea_id;
        $data['pagination'] = $this->pagination->create_links();
        $this->load->view('admin/ea_users/index', $data);
    }

    public function users_add($safa_ea_id = FALSE) {
        if(!$safa_ea_id)
              show_404 ();
        $data['safa_ea_id']=$safa_ea_id;
        $this->load->library("form_validation");
        $this->form_validation->set_rules('name_ar', 'lang:ea_users_name_ar', 'trim|required');
        $this->form_validation->set_rules('name_la', 'lang:ea_users_name_la', 'trim|required');
        $this->form_validation->set_rules('username', 'lang:ea_users_username', 'trim|required');
        $this->form_validation->set_rules('password', 'lang:ea_users_password', 'trim|required');
        $this->form_validation->set_rules('passconf', 'lang:ea_users_confirm_password', 'trim|required|matches[password]');
        if ($this->form_validation->run() == false) {
            $this->load->view("admin/ea_users/add" ,$data);
        } else {
            $this->ea_users_model->name_ar = $this->input->post('name_ar');
            $this->ea_users_model->name_la = $this->input->post('name_la');
            $this->ea_users_model->username = $this->input->post('username');
            $this->ea_users_model->password = md5($this->input->post('password'));
            $this->ea_users_model->safa_ea_id = $safa_ea_id;
            $this->ea_users_model->safa_ea_usergroup_id = 1;

            if(isset($_POST['active'])) {
            	$this->ea_users_model->active = 1;
            } else {
            	$this->ea_users_model->active = 0;
            }
            
            $this->ea_users_model->save();
            $this->load->view('redirect_message',
                   array('msg' => lang('global_added_message'), 
                  'url' => site_url('admin/safa_eas/users_index/'.$safa_ea_id),
                  'model_title'=> lang('child_node_title_ea_users'),'action'=>lang('safa_eas_users_add_title')));
        }
    }
    // for activation and de activatation the external agent // 
    public function cancel_ea($ea_id=false){
        if(!$ea_id)
            show_404 ();
      $this->eas_model->safa_ea_id=$ea_id;
      $this->eas_model->disabled=1;
      $updated=$this->eas_model->save();
    if($updated)
         redirect('admin/safa_eas/index');
      
    }
    public function active_ea($ea_id=false){
         if(!$ea_id)
            show_404 ();
      $this->eas_model->safa_ea_id=$ea_id;
      $this->eas_model->disabled=0;
      $updated=$this->eas_model->save();
    if($updated)
         redirect('admin/safa_eas/index'); 
    }

    public function users_edit($id, $safa_ea_id) {
        if (!$id)
            show_404();
        $this->ea_users_model->safa_ea_user_id = $id;
        $data['item'] = $this->ea_users_model->get();
        if (!$data['item'])
            show_404();
        $this->load->library("form_validation");
        $this->form_validation->set_rules('name_ar', 'lang:ea_users_name_ar', 'trim|required');
        $this->form_validation->set_rules('name_la', 'lang:ea_users_name_la', 'trim|required');
        $this->form_validation->set_rules('username', 'lang:ea_users_username', 'trim|required');
        $this->form_validation->set_rules('password', 'lang:ea_users_password', 'trim');
        $this->form_validation->set_rules('passconf', 'lang:ea_users_confirm_password', 'trim|matches[password]');
        if ($this->form_validation->run() == false) {
            $this->load->view("admin/ea_users/edit", $data);
        } else {
            
            $this->ea_users_model->safa_ea_user_id = $id;
            $this->ea_users_model->name_ar = $this->input->post('name_ar');
            $this->ea_users_model->name_la = $this->input->post('name_la');
            $this->ea_users_model->username = $this->input->post('username');
            $this->ea_users_model->password = md5($this->input->post('password'));
            
            if(isset($_POST['active'])) {
            	$this->ea_users_model->active = 1;
            } else {
            	$this->ea_users_model->active = 0;
            }
            
            $this->ea_users_model->save();
            redirect("admin/safa_eas/users_index/" . $safa_ea_id);
        }
    }
    
       function users_delete($id,$safa_ea_id) {
        if (!$id)
            show_404();
            $this->ea_users_model->safa_ea_user_id = $id;
            $this->ea_users_model->delete();
            redirect("admin/safa_eas/users_index/" . $safa_ea_id);
       }

}

/* End of file safa_eas.php */
/* Location: ./application/controllers/safa_eas.php */