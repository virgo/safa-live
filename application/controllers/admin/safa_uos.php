<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Safa_uos extends Safa_Controller {

    protected $safa_ous_name = '';

    public function __construct() {
        parent::__construct();
        $this->layout = 'new';
        $this->load->model('safa_uos_model');
        $this->load->helper('date');
        $this->load->model('safa_uo_users_model');
        $this->load->model('packages_model');
        $this->lang->load('admins/safa_uos');
        $this->lang->load('admins/safa_uo_users');
        permission();
    }

    public function index() {
        $this->safa_uos_model->deleted = 0;
        if (isset($_GET['search']))
            $this->search();
        $data["total_rows"] = $this->safa_uos_model->get(true);
        $this->safa_uos_model->offset = $this->uri->segment("4");
        $this->safa_uos_model->limit = 50;
        $this->safa_uos_model->join = true;

        $data["items"] = $this->safa_uos_model->get();
        $config['uri_segment'] = 4;
        $config['base_url'] = site_url('admin/safa_uos/index');
        $config['total_rows'] = $data["total_rows"];
        $config['per_page'] = $this->config->item('per_page');
        $config['suffix'] = $this->input->get() ? '?' . http_build_query($_GET) : NULL;
        $this->load->library('pagination');
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();
        $this->load->view('admin/safa_uos/index', $data);
    }

    public function add() {
        $this->load->library("form_validation");
        $this->form_validation->set_rules('name_ar', 'lang:safa_uos_name_ar', 'trim');
        $this->form_validation->set_rules('name_la', 'lang:safa_uos_name_la', 'trim');
        if (name() == 'name_ar')
            $this->form_validation->set_rules('name_ar', 'lang:safa_uos_name_ar', 'trim|required');
        if (name() == 'name_la')
            $this->form_validation->set_rules('name_la', 'lang:safa_uos_name_la', 'trim|required');

        $this->form_validation->set_rules('erp_uasp_id', 'lang:erp_uasp_id', 'trim|required');
        $this->form_validation->set_rules('code', 'lang:safa_uos_code', 'trim|required|alpha_numeric|unique_col[safa_uos.code]');
        $this->form_validation->set_rules('phone', 'lang:safa_uos_phone', 'trim');
        $this->form_validation->set_rules('mobile', 'lang:safa_uos_mobile', 'trim');
        $this->form_validation->set_rules('email', 'lang:safa_uos_email', 'trim|valid_email');
        $this->form_validation->set_rules('fax', 'lang:safa_uos_fax', 'trim');
        $this->form_validation->set_rules('user_username', 'lang:safa_uos_user_username'
                , 'trim|required|alpha_dash|unique_col[safa_uo_users.username]');

        $this->form_validation->set_rules('user_password', 'lang:safa_uos_user_password', 'trim|required');
        $this->form_validation->set_rules('user_passconf', 'lang:safa_uos_user_repeat_password', 'trim|required|matches[user_password]');

        if ($this->form_validation->run() == false) {
            $this->load->view("admin/safa_uos/add");
        } else {

            $this->safa_uos_model->name_ar = $this->input->post('name_ar');
            $this->safa_uos_model->name_la = $this->input->post('name_la');
            $this->safa_uos_model->code = $this->input->post('code');
            $this->safa_uos_model->phone = $this->input->post('phone');
            $this->safa_uos_model->mobile = $this->input->post('mobile');
            $this->safa_uos_model->email = $this->input->post('email');
            $this->safa_uos_model->fax = $this->input->post('fax');
            $safa_uo_id = $this->safa_uos_model->save();
            if ($safa_uo_id) {
                $this->safa_uo_users_model->name_ar = $this->input->post('name_ar');
                $this->safa_uo_users_model->name_la = $this->input->post('name_la');
                $this->safa_uo_users_model->username = $this->input->post('user_username');
                if (strlen($this->input->post('user_password') > 1))
                    $this->safa_uo_users_model->password = md5($this->input->post('user_password'));
                $this->safa_uos_model->erp_uasp_id = $this->input->post('erp_uasp_id');
                $this->safa_uo_users_model->safa_uo_usergroup_id = 1;
                $this->safa_uo_users_model->safa_uo_id = $safa_uo_id;
                $this->safa_uo_users_model->email = $this->input->post('email');
                $this->safa_uo_users_model->save();
                $this->packages_model->safa_uo_id = $safa_uo_id;
                $expiry = date('Y-m-d H:i:s', strtotime('+1 year'));
                $this->packages_model->start_date = date('Y-m-d H:i');
                $this->packages_model->end_date = $expiry;
                $this->packages_model->name_ar = 'باكج افتراضى';
                $this->packages_model->name_la = 'default package';
                $this->packages_model->erp_country_id = 20;
                $this->packages_model->save();
            }
            $this->load->view('redirect_message', array('msg' => lang('global_added_message'),
                'url' => site_url('admin/safa_uos'),
                'model_title' => lang('parent_child_title'), 'action' => lang('safa_uos_add_title')));
        }
    }

    public function edit($id = FALSE) {
        if (!$id)
            show_404();
        $this->safa_uos_model->safa_uo_id = $id;
        $data['item'] = $this->safa_uos_model->get();
        if (!$data['item'])
            show_404();


        $this->load->library("form_validation");

        $this->form_validation->set_rules('name_ar', 'lang:safa_uos_name_ar', 'trim');
        $this->form_validation->set_rules('name_la', 'lang:safa_uos_name_la', 'trim');

        if (name() == 'name_ar')
            $this->form_validation->set_rules('name_ar', 'lang:safa_uos_name_ar', 'trim|required');
        if (name() == 'name_la')
            $this->form_validation->set_rules('name_la', 'lang:safa_uos_name_la', 'trim|required');
        $this->form_validation->set_rules('erp_uasp_id', 'lang:erp_uasp_id', 'trim|required');
        $this->form_validation->set_rules('code', 'lang:safa_uos_code', 'trim|required|alpha_numeric|unique_col[safa_uos.code.' . $id . ']');
        $this->form_validation->set_rules('phone', 'lang:safa_uos_phone', 'trim');
        $this->form_validation->set_rules('mobile', 'lang:safa_uos_mobile', 'trim');
        $this->form_validation->set_rules('email', 'lang:safa_uos_email', 'trim|valid_email');
        $this->form_validation->set_rules('fax', 'lang:safa_uos_fax', 'trim');
        if ($this->form_validation->run() == false) {
            $this->load->view("admin/safa_uos/edit", $data);
        } else {
            $this->safa_uos_model->name_ar = $this->input->post('name_ar');
            $this->safa_uos_model->name_la = $this->input->post('name_la');
            $this->safa_uos_model->erp_uasp_id = $this->input->post('erp_uasp_id');
            $this->safa_uos_model->code = $this->input->post('code');
            $this->safa_uos_model->phone = $this->input->post('phone');
            $this->safa_uos_model->mobile = $this->input->post('mobile');
            $this->safa_uos_model->email = $this->input->post('email');
            $this->safa_uos_model->fax = $this->input->post('fax');
            $safa_uo_id = $this->safa_uos_model->save();
            $this->load->view('redirect_message', array('msg' => lang('global_updated_message'),
                'url' => site_url('admin/safa_uos'),
                'id' => $id,
                'model_title' => lang('parent_child_title'), 'action' => lang('safa_uos_edit_title')));
        }
    }

    function delete_disable($id) {
        $this->layout = 'ajax';
        if (!$id)
            show_404();
        $this->safa_uos_model->safa_uo_id = $id;
        $deleted = $this->safa_uos_model->delete_disable();
        echo $deleted;
    }

    function delete_enable($id) {
        $this->layout = 'ajax';
        if (!$id)
            show_404();
        $this->safa_uos_model->safa_uo_id = $id;
        $deleted = $this->safa_uos_model->delete_enable();
        echo $deleted;
    }

    function search() {

        if ($this->input->get("safa_uo_id"))
            $this->safa_uos_model->safa_uo_id = $this->input->get("safa_uo_id");


        if ($this->input->get("uo_deleted") != '')
            $this->safa_uos_model->deleted = $this->input->get("uo_deleted");
        else
            $this->safa_uos_model->deleted = false;

        if ($this->input->get("name_ar"))
            $this->safa_uos_model->name_ar = $this->input->get("name_ar");
        if ($this->input->get("name_la"))
            $this->safa_uos_model->name_la = $this->input->get("name_la");
        if ($this->input->get("code"))
            $this->safa_uos_model->code = $this->input->get("code");
        if ($this->input->get("phone"))
            $this->safa_uos_model->phone = $this->input->get("phone");
        if ($this->input->get("mobile"))
            $this->safa_uos_model->mobile = $this->input->get("mobile");
        if ($this->input->get("email"))
            $this->safa_uos_model->email = $this->input->get("email");
        if ($this->input->get("fax"))
            $this->safa_uos_model->fax = $this->input->get("fax");
    }

    public function users_index($safa_uo_id = FALSE) {

        if (isset($_GET['search']))
            $this->search();
        $this->safa_uo_users_model->offset = $this->uri->segment("3");
        $this->safa_uo_users_model->limit = $this->config->item('per_page');
        $this->safa_uo_users_model->safa_uo_id = $safa_uo_id;
        $this->safa_uo_users_model->join = TRUE;
        $data["items"] = $this->safa_uo_users_model->get();
        $config['uri_segment'] = 3;
        $config['base_url'] = site_url('admin/safa_uo_users/index');
        $config['per_page'] = $this->safa_uo_users_model->limit;
        $config['suffix'] = $this->input->get() ? '?' . http_build_query($_GET) : NULL;
        $this->load->library('pagination');
        $this->pagination->initialize($config);
        /*         * **get uos_name************ */
        $data['safa_uo_id'] = $safa_uo_id;
        $data['pagination'] = $this->pagination->create_links();
        $this->load->view('admin/safa_uo_users/index', $data);
    }

    public function users_add($safa_uo_id = FALSE) {
        if (!$safa_uo_id)
            show_404();
        $data['safa_uo_id'] = $safa_uo_id;
        $this->load->library("form_validation");
        $this->form_validation->set_rules('name_ar', 'lang:safa_uos_user_name_ar', 'trim');
        $this->form_validation->set_rules('name_la', 'lang:safa_uos_user_name_la', 'trim');
        if (name() == 'name_ar')
            $this->form_validation->set_rules('name_ar', 'lang:safa_uos_user_name_ar', 'trim|required');
        if (name() == 'name_la')
            $this->form_validation->set_rules('name_la', 'lang:safa_uos_user_name_la', 'trim|required');
        $this->form_validation->set_rules('username', 'lang:safa_uos_user_username', 'trim|required|alpha_dash|unique_col[safa_uo_users.username]');
        $this->form_validation->set_rules('password', 'lang:safa_uos_user_password', 'trim|required');
        $this->form_validation->set_rules('passconf', 'lang:safa_uos_user_repeat_password', 'trim|required|matches[password]');
        $this->form_validation->set_rules('uo_user_mobile', 'lang:safa_uo_users_mobile', 'trim|integer');
        $this->form_validation->set_rules('uo_user_email', 'lang:safa_uo_users_email', 'trim|valid_email');
        $this->form_validation->set_rules('safa_uo_usergroup_id', 'lang:safa_uo_users_usersgroup', 'trim|required');

        if ($this->form_validation->run() == false) {
            $this->load->view("admin/safa_uo_users/add", $data);
        } else {
            $this->safa_uo_users_model->name_ar = $this->input->post('name_ar');
            $this->safa_uo_users_model->name_la = $this->input->post('name_la');
            $this->safa_uo_users_model->username = $this->input->post('username');
            if (strlen($this->input->post('password') > 1))
                $this->safa_uo_users_model->password = md5($this->input->post('password'));

            $this->safa_uo_users_model->safa_uo_id = $safa_uo_id;
            $this->safa_uo_users_model->safa_uo_usergroup_id = $this->input->post('safa_uo_usergroup_id');
            $this->safa_uo_users_model->email = $this->input->post('uo_user_email');
            $this->safa_uo_users_model->mobile = $this->input->post('uo_user_mobile');

            $this->safa_uo_users_model->save();
            $this->load->view('redirect_message', array('msg' => lang('global_added_message'),
                'url' => site_url('admin/safa_uos/users_index/' . $safa_uo_id),
                'model_title' => lang('child_node_title_ou_users'), 'action' => lang('safa_uos_users_add_title')));
        }
    }

    public function users_edit($id, $safa_uo_id) {

        if (!$id)
            show_404();
        if (!$safa_uo_id)
            show_404();
        $this->safa_uo_users_model->safa_uo_user_id = $id;
        $data['safa_uo_id'] = $safa_uo_id;
        $data['item'] = $this->safa_uo_users_model->get();
        if (!$data['item'])
            show_404();
        $this->load->library("form_validation");
        $this->form_validation->set_rules('name_ar', 'lang:safa_uos_user_name_ar', 'trim');
        $this->form_validation->set_rules('name_la', 'lang:safa_uos_user_name_la', 'trim');
        if (name() == 'name_ar')
            $this->form_validation->set_rules('name_ar', 'lang:safa_uos_user_name_ar', 'trim|required');
        if (name() == 'name_la')
            $this->form_validation->set_rules('name_la', 'lang:safa_uos_user_name_la', 'trim|required');
        $this->form_validation->set_rules('username', 'lang:safa_uos_user_username', 'trim|required|alpha_dash|unique_col[safa_uo_users.username.' . $id . ']');
        $this->form_validation->set_rules('password', 'lang:safa_uos_user_password', 'trim');
        $this->form_validation->set_rules('passconf', 'lang:safa_uos_user_repeat_password', 'trim|matches[password]');
        $this->form_validation->set_rules('uo_user_mobile', 'lang:safa_uo_users_mobile', 'trim|integer');
        $this->form_validation->set_rules('uo_user_email', 'lang:safa_uo_users_email', 'trim|valid_email');
        $this->form_validation->set_rules('safa_uo_usergroup_id', 'lang:safa_uo_users_usersgroup', 'trim|required');

        if ($this->form_validation->run() == false) {
            $this->load->view("admin/safa_uo_users/edit", $data);
        } else {
            $this->safa_uo_users_model->safa_uo_user_id = $id;
            $this->safa_uo_users_model->name_ar = $this->input->post('name_ar');
            $this->safa_uo_users_model->name_la = $this->input->post('name_la');
            $this->safa_uo_users_model->username = $this->input->post('username');
            if (strlen($this->input->post('password')))
                $this->safa_uo_users_model->password = md5($this->input->post('password'));

            $this->safa_uo_users_model->safa_uo_usergroup_id = $this->input->post('safa_uo_usergroup_id');
            $this->safa_uo_users_model->email = $this->input->post('uo_user_email');
            $this->safa_uo_users_model->mobile = $this->input->post('uo_user_mobile');
            $this->safa_uo_users_model->save();
            $this->load->view('redirect_message', array('msg' => lang('global_updated_message'),
                'url' => site_url('admin/safa_uos/users_index/' . $safa_uo_id),
                'model_title' => lang('child_node_title_ou_users'), 'action' => lang('safa_uos_users_edit_title')));
        }
    }

    function users_delete($id, $safa_uo_id) {
        if (!$id)
            show_404();
        $this->safa_uo_users_model->safa_uo_user_id = $id;
        $this->safa_uo_users_model->delete();
        redirect("admin/safa_uos/users_index/" . $safa_uo_id);
    }

//    function users_delete_all() {
//        if ($this->input->post("delete_items")) {
//            $safa_uo_id = $this->input->post("safa_uo_id");
//            foreach ($this->input->post("delete_items") as $item) {
//                $data['item'] = $this->safa_uos_model->check_users_delete_ability($safa_uo_id);
//                if (sizeof($data['item']) > 1) {
//                    $this->safa_uo_users_model->safa_uo_user_id = $item;
//                    $this->safa_uo_users_model->delete();
//                    redirect("admin/safa_uos/users_index/" . $safa_uo_id);
//                } else {
//                    echo"not delete allowed";
//                }
//            }
//        }
//        else
//            show_404();
//    }

    function users_search() {
        if ($this->input->get("safa_uo_user_id"))
            $this->safa_uo_users_model->safa_uo_user_id = $this->input->get("safa_uo_user_id");
        if ($this->input->get("name_ar"))
            $this->safa_uo_users_model->name_ar = $this->input->get("name_ar");
        if ($this->input->get("name_la"))
            $this->safa_uo_users_model->name_la = $this->input->get("name_la");
        if ($this->input->get("username"))
            $this->safa_uo_users_model->username = $this->input->get("username");
        if ($this->input->get("password"))
            $this->safa_uo_users_model->password = $this->input->get("password");
        if ($this->input->get("safa_uo_id"))
            $this->safa_uo_users_model->safa_uo_id = $this->input->get("safa_uo_id");
        if ($this->input->get("safa_uo_usergroup_id"))
            $this->safa_uo_users_model->safa_uo_usergroup_id = $this->input->get("safa_uo_usergroup_id");
    }

    /* callbackvalidation FOR THE TEXT WITH SPACE  */

    // for activation and de activatation the external agent // 
    public function cancel_uo($uo_id = FALSE) {
        if (!$uo_id)
            show_404();
        $this->safa_uos_model->safa_uo_id = $uo_id;
        $this->safa_uos_model->disabled = 1;
        $updated = $this->safa_uos_model->save();
        if ($updated)
            redirect('admin/safa_uos/index');
    }

    public function active_uo($uo_id = FALSE) {
        if (!$uo_id)
            show_404();
        $this->safa_uos_model->safa_uo_id = $uo_id;
        $this->safa_uos_model->disabled = 0;
        $updated = $this->safa_uos_model->save();
        if ($updated)
            redirect('admin/safa_uos/index');
    }

}

/* End of file safa_uos.php */
/* Location: ./application/controllers/admin/safa_uos.php */
