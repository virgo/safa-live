<?php

class Tourismplaces extends Safa_Controller {

    public $module = "tourismplaces";

    public function __construct() {
        parent::__construct();

        $this->layout='new';
        
        $this->load->model('tourismplaces_model');
        $this->lang->load('admins/tourismplaces');
//        
        permission();
        if (!admin_permission())
            show_404();
    }

    public function index() {

        if (isset($_GET['search']))
            $this->search();
        $this->tourismplaces_model->join = TRUE;
        $data["total_rows"] = $this->tourismplaces_model->get(true);
        $this->tourismplaces_model->offset = $this->uri->segment("4");
        $this->tourismplaces_model->limit = 10;
        $data["items"] = $this->tourismplaces_model->get();
        $config['uri_segment'] = 4;
        $config['base_url'] = site_url('admin/tourismplaces/index');
        $config['total_rows'] = $data["total_rows"];
        $config['per_page'] = $this->tourismplaces_model->limit;
        $config['suffix'] = $this->input->get() ? '?' . http_build_query($_GET) : NULL;
        $this->load->library('pagination');
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();
        $data['country'] = $this->tourismplaces_model->get_countery_ksa();
        $this->load->view('admin/tourismplaces/index', $data);
    }

    public function add() {

        $data['country'] = $this->tourismplaces_model->get_countery_ksa();
        $this->load->library("form_validation");
        $this->form_validation->set_rules('erp_city_id', 'lang:tourismplaces_erp_city_id', 'trim|required|callback_check_city_id');
        $this->form_validation->set_rules('name_ar', 'lang:tourismplaces_name_ar', 'trim|required|callback_check_name_ar|min_length[3]|max_length[100]');
        $this->form_validation->set_rules('name_la', 'lang:tourismplaces_name_la', 'trim|required|callback_check_name_la|min_length[3]|max_length[100]');
        if ($this->form_validation->run() == false) {
            $this->load->view("admin/tourismplaces/add", $data);
        } else {
            $this->tourismplaces_model->erp_city_id = $this->input->post('erp_city_id');
            $this->tourismplaces_model->name_ar = $this->input->post('name_ar');
            $this->tourismplaces_model->name_la = $this->input->post('name_la');


            $this->tourismplaces_model->save();
            $this->load->view('redirect_message', array('msg' => lang('global_added_message'), 'url' => site_url('admin/tourismplaces'), 'model_title' => lang('menu_main_sights'), 'action' => lang('tourismplaces_add_title')));

            //redirect("admin/tourismplaces/index");
        }
    }

    public function edit($id) {

        if (!$id)
            show_404();

        $this->tourismplaces_model->safa_tourismplace_id = $id;
        $city_id = $this->tourismplaces_model->get_city_id($id);
        $asd = $city_id->erp_city_id;
        $data['country'] = $this->tourismplaces_model->get_countery_id($asd);
        $data['items'] = $this->tourismplaces_model->get();

        if (!$data['items'])
            show_404();

        $this->load->library("form_validation");
        $this->form_validation->set_rules('erp_city_id', 'lang:tourismplaces_erp_city_id', 'trim|required|callback_check_city_id');
        $this->form_validation->set_rules('name_ar', 'lang:tourismplaces_name_ar', 'trim|required|callback_is_unique_ar[' . $this->uri->segment("4") . ']|min_length[3]|max_length[100]');
        $this->form_validation->set_rules('name_la', 'lang:tourismplaces_name_la', 'trim|required|callback_is_unique_la[' . $this->uri->segment("4") . ']|min_length[3]|max_length[100]');
        if ($this->form_validation->run() == false) {
            $this->load->view("admin/tourismplaces/edit", $data);
        } else {
            $this->tourismplaces_model->erp_city_id = $this->input->post('erp_city_id');
            $this->tourismplaces_model->name_ar = $this->input->post('name_ar');
            $this->tourismplaces_model->name_la = $this->input->post('name_la');
            $this->tourismplaces_model->save();
            $this->load->view('redirect_message', array('msg' => lang('global_updated_message'), 'url' => site_url('admin/tourismplaces'), 'id' => $this->uri->segment("4"), 'model_title' => lang('menu_main_sights'), 'action' => lang('tourismplaces_edit_title')));
            // redirect("admin/tourismplaces/index");
        }
    }

    function delete() {
        $this->layout = 'ajax';
        if (!$this->input->is_ajax_request()) {
            redirect('admin/tourismplaces/index');
        } else {
            $safa_tourismplace_id = $this->input->post('safa_tourismplace_id');
            $data = $this->tourismplaces_model->check_delete_ability($safa_tourismplace_id);

            if ($data == 0) {
                $this->tourismplaces_model->safa_tourismplace_id = $safa_tourismplace_id;
                if ($this->tourismplaces_model->delete())
                    echo json_encode(array('response' => TRUE, 'msg' => 'deleted'));
            }
            else {
                echo json_encode(array('response' => FALSE, 'msg' => 'not deleted'));
            }
        }
    }

    function search() {
        if ($this->input->get("name_ar"))
            $this->tourismplaces_model->name_ar = $this->input->get("name_ar");

        if ($this->input->get("name_la"))
            $this->tourismplaces_model->name_la = $this->input->get("name_la");
        if ($this->input->get("erp_country_id"))
            $this->tourismplaces_model->erp_country_id = $this->input->get("erp_country_id");
        if ($this->input->get("erp_city_id"))
            $this->tourismplaces_model->erp_city_id = $this->input->get("erp_city_id");
    }

    function alpha_dash_space($name_la) {
        if (!preg_match("/^([-a-z_ ])+$/i", $name_la)) {
            $this->form_validation->set_message('alpha_dash_space', lang('tourismplaces_name_invalid'));
            return FALSE;
        } else {
            return true;
        }
    }

    function check_name_ar($name_ar) {
        $city_id = $this->input->post('erp_city_id');
        $this->db->where('name_ar', $name_ar);
        $this->db->where('erp_city_id', $city_id);
        if ($this->db->get('safa_tourismplaces')->num_rows()) {
            $this->form_validation->set_message('check_name_ar', lang('tourismplaces_name_in_city_exist'));
            return FALSE;
        }
        else
            return true;
    }

    function check_name_la($name_la) {
        $city_id = $this->input->post('erp_city_id');
        $this->db->where('name_la', $name_la);
        $this->db->where('erp_city_id', $city_id);
        if ($this->db->get('safa_tourismplaces')->num_rows()) {
            $this->form_validation->set_message('check_name_la', lang('tourismplaces_name_in_city_exist'));
            return FALSE;
        }
        else
            return true;
    }

    function is_unique_ar($value, $id = false) {
        $city_id = $this->input->post('erp_city_id');
        if ($id == false) {
            $query = $this->db->limit(1)->where("name_ar", $value)->get("safa_tourismplaces");
        } else {
            $this->db->limit(1);
            $this->db->where("name_ar", $value);
            $this->db->where("erp_city_id", $city_id);
            $query = $this->db->where("safa_tourismplace_id!=" . $id)->get("safa_tourismplaces");
        }
        if ($query->num_rows()) {
            $this->form_validation->set_message('is_unique_ar', lang('tourismplaces_name_in_city_exist'));
            return false;
        }
        else
            return true;
    }

    function is_unique_la($value, $id = false) {
        $city_id = $this->input->post('erp_city_id');
        if ($id == false) {
            $query = $this->db->limit(1)->where("name_la", $value)->get("safa_tourismplaces");
        } else {
            $this->db->limit(1);
            $this->db->where("name_la", $value);
            $this->db->where("erp_city_id", $city_id);
            $query = $this->db->where("safa_tourismplace_id!=" . $id)->get("safa_tourismplaces");
        }
        if ($query->num_rows()) {
            $this->form_validation->set_message('is_unique_la', lang('tourismplaces_name_in_city_exist'));
            return false;
        }
        else
            return true;
    }

    function check_city_id($city_id) {
        if ($city_id == 0) {
            $this->form_validation->set_message('check_city_id', lang('tourismplaces_select_from_menu_false'));
            return false;
        }
        else
            return true;
    }

    function get_erp_cities($erp_country_id, $a) {
        $this->layout = 'ajax';
        $er_cities = $this->tourismplaces_model->get_cities($erp_country_id);
        if (!(count($er_cities) > 0)) {
            echo "<option value='0'>" . lang('tourismplaces_select_from_menu_no_cities') . "</option>";
        } else {
//         echo "<option value='0'>".lang('tourismplaces_select_from_menu_cities')."</option>";
            if (isset($er_cities) && count($er_cities) > 0) {
                if ($this->config->item('language') == 'arabic') {
                    echo "<option value=''>الكل</option>";
                } else {
                    echo "<option value=''>All Tourismplaces</option>";
                }
                foreach ($er_cities as $city) {
                    echo "<option value='" . $city->erp_city_id . "'>" . $city->{name()} . "</option>";
                }
            }
        }
    }

    function get_erp_city($erp_country_id, $def = false) {
        $this->layout = 'ajax';
        $er_cities = $this->tourismplaces_model->get_cities($erp_country_id);
        if (!(count($er_cities) > 0)) {
            echo "<option value='0'>" . lang('tourismplaces_select_from_menu_no_cities') . "</option>";
        } else {
//         echo "<option value='0'>".lang('tourismplaces_select_from_menu_cities')."</option>";
            if (isset($er_cities) && count($er_cities) > 0) {

                foreach ($er_cities as $city) {

                    if ($city->erp_city_id == $def) {
                    
                        echo "<option value='" . $city->erp_city_id . "' selected>" . $city->{name()} . "</option>";                        
                    } else {
                        echo "<option value='" . $city->erp_city_id . "'>" . $city->{name()} . "</option>";                        
                    }
                }
            }
        }
    }

}

/* End of file tourismplaces.php */
/* Location: ./application/controllers/admin/tourismplaces.php */


