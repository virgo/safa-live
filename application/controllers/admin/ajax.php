<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Ajax extends Safa_Controller {

    function __construct() {
        parent::__construct();
        $this->layout = "ajax";
        $this->load->model('admins_model');
    }

    function cities($country_id) {

        if (config("language") == "english") {

            $this->db->select("erp_city_id.name_la as name");
            $this->db->order_by("name_la", "asc");
        } else {

            $this->db->select("erp_city_id.name_ar as name");
            $this->db->order_by("name_ar", "asc");
        }
        $this->db->where("erp_country_id", $country_id);
        $query = $this->db->get("erp_cities");
        if ($query->num_rows) {
            $html = "<option value='0' >" . lang('اختار دوله الاول ') . "</option>";
            foreach ($query->result() as $row) {
                $html.="<option value='$row->erp_city_id'>" . $row->name . "</option>";
            }
            echo $html;
        }
    }
}
