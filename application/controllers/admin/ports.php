<?php

class Ports extends Safa_Controller {

    public $module = "ports";

    public function __construct() {
        parent::__construct();

        $this->layout = 'new';

        $this->load->model('ports_model');
        $this->lang->load('admins/ports');
        permission();
    }

    public function index() {

        if (isset($_GET['search']))
            $this->search();
        $this->ports_model->join = TRUE;
        $data["total_rows"] = $this->ports_model->get(true);
        $this->ports_model->offset = $this->uri->segment("4");
        $this->ports_model->limit = 10;
        $data["items"] = $this->ports_model->get();
        $config['uri_segment'] = 4;
        $config['base_url'] = site_url('admin/ports/index');
        $config['total_rows'] = $data["total_rows"];
        $config['per_page'] = $this->ports_model->limit;
        $config['suffix'] = $this->input->get() ? '?' . http_build_query($_GET) : NULL;
        $this->load->library('pagination');
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();
        $this->load->view('admin/ports/index', $data);
    }

    public function add() {

        $this->load->library("form_validation");
        $this->form_validation->set_rules('erp_city_id', 'lang:ports_erp_city_id', 'trim|required|callback_check_city_id');
        $this->form_validation->set_rules('name_ar', 'lang:ports_name_ar', 'trim|required|is_unique[erp_ports.name_ar]');
        $this->form_validation->set_rules('name_la', 'lang:ports_name_la', 'trim|required|is_unique[erp_ports.name_la]');
        $this->form_validation->set_rules('code', 'lang:ports_code', 'trim|required');
        $this->form_validation->set_rules('country_code', 'lang:ports_country_code', 'trim|required');
        $this->form_validation->set_rules('erp_country_id', 'lang:ports_country_name', 'trim|required');
        $this->form_validation->set_rules('world_area_code', 'lang:ports_world_area_code', 'trim|required|integer');
        if ($this->form_validation->run() == false) {
            $this->load->view("admin/ports/add");
        } else {

            $this->ports_model->erp_city_id = $this->input->post('erp_city_id');
            $this->ports_model->name_ar = $this->input->post('name_ar');
            $this->ports_model->name_la = $this->input->post('name_la');
            $this->ports_model->code = $this->input->post('code');

            $this->ports_model->world_area_code = $this->input->post('world_area_code');
            $this->ports_model->safa_transportertype_id = 2;
            if ($this->input->post('erp_country_id')) {
                $this->ports_model->erp_country_id = $this->input->post('erp_country_id');
                $country_row = $this->ports_model->get_country_name($this->input->post('erp_country_id'));
                $this->ports_model->country_name = $country_row->{name()};
                $this->ports_model->country_code = $country_row->country_code;
            }
            $this->ports_model->save();
            $this->load->view('admin/redirect_message', array('msg' => lang('global_added_message'), 'url' => site_url('admin/ports'), 'model_name' => 'ports', 'model_title' => lang('menu_main_ports'), 'action' => lang('ports_add_title')));
        }
    }

    public function edit($id) {

        if (!$id)
            show_404();

        $this->ports_model->erp_port_id = $id;
        $data['items'] = $this->ports_model->get();
        $data['country_id'] = $this->ports_model->get_country_id($data['items']->country_name);
        if (!$data['items'])
            show_404();

        $this->load->library("form_validation");
        $this->form_validation->set_rules('erp_city_id', 'lang:ports_erp_city_id', 'trim|required|callback_check_city_id');
        $this->form_validation->set_rules('name_ar', 'lang:ports_name_ar', 'trim|required|callback_unique_name_ar');
        $this->form_validation->set_rules('name_la', 'lang:ports_name_la', 'trim|required|callback_unique_name_la');
        $this->form_validation->set_rules('code', 'lang:ports_code', 'trim|required');
        $this->form_validation->set_rules('country_code', 'lang:ports_country_code', 'trim|required');
        $this->form_validation->set_rules('erp_country_id', 'lang:ports_country_name', 'trim|required');
        $this->form_validation->set_rules('world_area_code', 'lang:ports_world_area_code', 'trim|required|integer');

        if ($this->form_validation->run() == false) {
            $this->load->view("admin/ports/edit", $data);
        } else {
            $this->ports_model->erp_city_id = $this->input->post('erp_city_id');
            $this->ports_model->name_ar = $this->input->post('name_ar');
            $this->ports_model->name_la = $this->input->post('name_la');
            $this->ports_model->code = $this->input->post('code');

            $this->ports_model->world_area_code = $this->input->post('world_area_code');
            $this->ports_model->safa_transportertype_id = 2;
            if ($this->input->post('erp_country_id')) {
                $this->ports_model->erp_country_id = $this->input->post('erp_country_id');
                $country_row = $this->ports_model->get_country_name($this->input->post('erp_country_id'));
                $this->ports_model->country_name = $country_row->{name()};
                $this->ports_model->country_code = $country_row->country_code;
            }
            $this->ports_model->save();

            $this->load->view('admin/redirect_message', array('msg' => lang('global_updated_message'), 'url' => site_url('admin/ports'), 'id' => $this->uri->segment("4"), 'model_name' => 'ports', 'model_title' => lang('menu_main_ports'), 'action' => lang('ports_edit_title')));
        }
    }

    function delete() {

        $this->layout = 'ajax';

        if (!$this->input->is_ajax_request()) {
            redirect('admin/ports/index');
        } else {
            $erp_port_id = $this->input->post('erp_port_id');
            $data = $this->ports_model->check_delete_ability($erp_port_id);
            if ($data == 0) {
                $this->ports_model->erp_port_id = $erp_port_id;
                if ($this->ports_model->delete())
                    echo json_encode(array('response' => TRUE, 'msg' => 'deleted'));
            }
            else {
                echo json_encode(array('response' => FALSE, 'msg' => 'not deleted'));
            }
        }
    }

    function search() {

        if ($this->input->get("name_ar"))
            $this->ports_model->name_ar = $this->input->get("name_ar");

        if ($this->input->get("name_la"))
            $this->ports_model->name_la = $this->input->get("name_la");

        if ($this->input->get("erp_country_id"))
            $this->ports_model->erp_country_id = $this->input->get("erp_country_id");
    }

    function unique_name_ar($name_ar) {
        if ($this->uri->segment('3') == 'edit')
            $this->db->where('erp_port_id !=', $this->uri->segment('4'));
        $this->db->where('name_ar', $name_ar);
        if ($this->db->get('erp_ports')->num_rows()) {
            $this->form_validation->set_message('unique_name_ar', lang('ports_name_exist'));
            return FALSE;
        } else {
            return TRUE;
        }
    }

    function unique_name_la($name_la) {
        if ($this->uri->segment('3') == 'edit')
            $this->db->where('erp_port_id !=', $this->uri->segment('4'));
        $this->db->where('name_la', $name_la);
        if ($this->db->get('erp_ports')->num_rows()) {
            $this->form_validation->set_message('unique_name_la', lang('ports_name_exist'));
            return FALSE;
        } else {
            return TRUE;
        }
    }

    function get_erp_city($erp_country_id) {

        $this->layout = 'ajax';
        $er_cities = $this->ports_model->get_cities($erp_country_id);
        if (!(count($er_cities) > 0)) {
            echo "<option value='0'>" . lang('ports_select_from_menu_no_cities') . "</option>";
        } else {
//         echo "<option value='0'>".lang('hotels_select_from_menu_cities')."</option>";
            if (isset($er_cities) && count($er_cities) > 0) {
                foreach ($er_cities as $city) {
                    echo "<option value='" . $city->erp_city_id . "'>" . $city->{name()} . "</option>";
                }
            }
        }
    }

    function get_country_code($erp_country_id = FALSE) {
        $this->layout = 'ajax';
        $country_row = $this->ports_model->get_country_name($erp_country_id);
        if (isset($country_row))
            echo $country_row->country_code;
    }

    function check_city_id($city_id) {
        if ($city_id == 0) {
            $this->form_validation->set_message('check_city_id', lang('ports_select_from_menu_false'));
            return false;
        } else
            return true;
    }

}

/* End of file port_halls.php */
/* Location: ./application/controllers/admin/port_halls.php */

