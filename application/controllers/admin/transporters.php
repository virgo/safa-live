<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Transporters extends Safa_Controller {

    public $module = "transporters";

    public function __construct() {
        parent::__construct();
        $this->layout = 'new';

        $this->load->model('transporters_model');
        $this->lang->load('admins/transporters');
        permission();
    }

    public function index() {

        if (isset($_GET['search']))
            $this->search();
        $data["total_rows"] = $this->transporters_model->search(true);
        $this->transporters_model->offset = $this->uri->segment("4");
        $this->transporters_model->limit = $this->config->item('per_page');
        $this->transporters_model->join = TRUE;
        $data["items"] = $this->transporters_model->search();
        $config['uri_segment'] = 4;
        $config['base_url'] = site_url('admin/transporters/index');
        $config['total_rows'] = $data["total_rows"];
        $config['per_page'] = $this->transporters_model->limit;
        $config['suffix'] = $this->input->get() ? '?' . http_build_query($_GET) : NULL;
        $this->load->library('pagination');
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();
        $this->load->view('admin/transporters/index', $data);
    }

    public function add() {

        $this->load->library("form_validation");
        $this->form_validation->set_rules('erp_country_id', 'lang:safa_transporter_erp_country_id', 'trim|required');
        $this->form_validation->set_rules('erp_transportertype_id', 'lang:safa_transporter_erp_transportertype_id', 'trim|required');
        $this->form_validation->set_rules('name_ar', 'lang:safa_transporter_name_ar', 'trim|required|is_unique[safa_transporters.name_ar]');
        $this->form_validation->set_rules('name_la', 'lang:safa_transporter_name_la', 'trim|required|is_unique[safa_transporters.name_la]');
        $this->form_validation->set_rules('code', 'lang:safa_transporter_code', 'trim|required|max_length[15]');

        if ($this->form_validation->run() == false) {
            $this->load->view("admin/transporters/add");
        } else {

            $this->transporters_model->erp_country_id = $this->input->post('erp_country_id');
            $this->transporters_model->erp_transportertype_id = $this->input->post('erp_transportertype_id');
            $this->transporters_model->name_ar = $this->input->post('name_ar');
            $this->transporters_model->name_la = $this->input->post('name_la');
            $this->transporters_model->code = $this->input->post('code');
            $this->transporters_model->save();
            $this->load->view('admin/redirect_message', array('msg' => lang('global_added_message'), 'url' => site_url('admin/transporters'), 'model_name' => 'transporters', 'model_title' => lang('menu_main_transport_companies'), 'action' => lang('safa_transporters_add')));
        }
    }

    public function edit($id) {

        if (!$id)
            show_404();

        $this->transporters_model->safa_transporter_id = $id;
        $data['item'] = $this->transporters_model->get();

        if (!$data['item'])
            show_404();

        $this->load->library("form_validation");
        $this->form_validation->set_rules('erp_country_id', 'lang:safa_transporter_erp_country_id', 'trim|required');
        $this->form_validation->set_rules('erp_transportertype_id', 'lang:safa_transporter_erp_transportertype_id', 'trim|required');
        $this->form_validation->set_rules('name_ar', 'lang:safa_transporter_name_ar', 'trim|required');
        $this->form_validation->set_rules('name_la', 'lang:safa_transporter_name_la', 'trim|required');
        $this->form_validation->set_rules('code', 'lang:safa_transporter_code', 'trim|required|max_length[15]');

        if ($this->form_validation->run() == false) {
            $this->load->view("admin/transporters/edit", $data);
        } else {
            $this->transporters_model->safa_transporter_id = $id;
            $this->transporters_model->erp_country_id = $this->input->post('erp_country_id');
            $this->transporters_model->erp_transportertype_id = $this->input->post('erp_transportertype_id');
            $this->transporters_model->name_ar = $this->input->post('name_ar');
            $this->transporters_model->name_la = $this->input->post('name_la');
            $this->transporters_model->code = $this->input->post('code');


            $this->transporters_model->save();

            $this->load->view('admin/redirect_message', array('msg' => lang('global_updated_message'), 'url' => site_url('admin/transporters'), 'id' => $this->uri->segment("4"), 'model_name' => 'transporters', 'model_title' => lang('menu_main_transport_companies'), 'action' => lang('safa_transporters_edit')));
        }
    }

//    function delete($id) {
//        if (!$id)
//            show_404();
//
//        $this->transporters_model->safa_transporter_id = $id;
//
//        if (!$this->transporters_model->delete())
//            show_404();
//        redirect("admin/transporters/index");
//    }

    function delete() {

        $this->layout = 'ajax';

        if (!$this->input->is_ajax_request()) {
            redirect('admin/transporters/index');
        } else {
            $safa_transporter_id = $this->input->post('safa_transporter_id');
            $data = $this->transporters_model->check_delete_ability($safa_transporter_id);
            if ($data == 0) {
                $this->transporters_model->safa_transporter_id = $safa_transporter_id;
                if ($this->transporters_model->delete())
                    echo json_encode(array('response' => TRUE, 'msg' => 'deleted'));
            }
            else {
                echo json_encode(array('response' => FALSE, 'msg' => 'not deleted'));
            }
        }
    }

    function delete_all() {
        if ($this->input->post("delete_items")) {
            foreach ($this->input->post("delete_items") as $item) {
                $this->transporters_model->safa_transporter_id = $item;

                $this->transporters_model->delete();
            }
        } else
            show_404();
        redirect("admin/transporters/index");
    }

    function search() {
        if ($this->input->get("safa_transporter_id"))
            $this->transporters_model->safa_transporter_id = $this->input->get("safa_transporter_id");
        if ($this->input->get("erp_country_id"))
            $this->transporters_model->erp_country_id = $this->input->get("erp_country_id");
        if ($this->input->get("erp_transportertype_id"))
            $this->transporters_model->erp_transportertype_id = $this->input->get("erp_transportertype_id");
        if ($this->input->get("name_ar"))
            $this->transporters_model->name_ar = $this->input->get("name_ar");
        if ($this->input->get("name_la"))
            $this->transporters_model->name_la = $this->input->get("name_la");
        if ($this->input->get("code"))
            $this->transporters_model->code = $this->input->get("code");
    }

}

/* End of file transporters.php */
/* Location: ./application/controllers/transporters.php */