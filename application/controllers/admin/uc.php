<?php

class Uc extends Safa_Controller {

    public function index() {
        $this->load->library('user_agent');
        $data['url'] = $this->agent->referrer();
        $data['msg'] = lang('global_this_page_is_under_construction');
        $this->load->view('admin/redirect', $data);
    }
}

/* End of file uc.php */
/* Location: ./application/controllers/admin/uc.php */