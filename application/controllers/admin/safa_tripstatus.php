<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Safa_tripstatus extends Safa_Controller {

    public $module = "safa_tripstatus";

    public function __construct() {
        parent::__construct();

        $this->layout = 'new';
        $this->load->model('tripstatus_model');
        $this->lang->load('admins/safa_tripstatus');
        permission();
    }

    public function index() {
        if (isset($_GET['search']))
            $this->search();
        $data["total_rows"] = $this->tripstatus_model->search(true);
        $this->tripstatus_model->offset = $this->uri->segment("3");
        $this->tripstatus_model->limit = 10;
        $data["items"] = $this->tripstatus_model->search();
        $config['uri_segment'] = 3;
        $config['base_url'] = site_url('admin/safa_tripstatus/index');
        $config['total_rows'] = $data["total_rows"];
        $config['per_page'] = $this->tripstatus_model->limit;
        $config['suffix'] = $this->input->get() ? '?' . http_build_query($_GET) : NULL;
        $this->load->library('pagination');
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();
        $this->load->view('admin/safa_tripstatus/index', $data);
    }

    public function add() {

        $this->load->library("form_validation");
        $this->form_validation->set_rules('name_ar', 'lang:safa_tripstatus_name_ar', 'trim|required|is_unique[safa_tripstatus.name_ar]');
        $this->form_validation->set_rules('name_la', 'lang:safa_tripstatus_name_la', 'trim|required|is_unique[safa_tripstatus.name_la]');
        $this->form_validation->set_rules('code', 'lang:safa_tripstatus_code', 'trim|required|is_unique[safa_tripstatus.code]|max_length[10]');

        if ($this->form_validation->run() == false) {
            $this->load->view("admin/safa_tripstatus/add");
        } else {
            $this->tripstatus_model->name_ar = $this->input->post('name_ar');
            $this->tripstatus_model->name_la = $this->input->post('name_la');
            $this->tripstatus_model->description_ar = $this->input->post('description_ar');
            $this->tripstatus_model->description_la = $this->input->post('description_la');
            $this->tripstatus_model->code = $this->input->post('code');

            $this->tripstatus_model->save();
            $this->load->view('admin/redirect_message', array('msg' => lang('global_added_message'), 'url' => site_url('admin/safa_tripstatus'), 'model_name' => 'safa_tripstatus', 'model_title' => lang('menu_techn_flight_cases'), 'action' => lang('safa_tripstatus_add')));
        }
    }

    public function edit($id) {

        if (!$id)
            show_404();

        $this->tripstatus_model->safa_tripstatus_id = $id;
        $data['item'] = $this->tripstatus_model->get();

        if (!$data['item'])
            show_404();

        $this->load->library("form_validation");
        $this->form_validation->set_rules('name_ar', 'lang:safa_tripstatus_name_ar', 'trim|required');
        $this->form_validation->set_rules('name_la', 'lang:safa_tripstatus_name_la', 'trim|required');
        $this->form_validation->set_rules('code', 'lang:safa_tripstatus_code', 'trim|required|max_length[10]');

        if ($this->form_validation->run() == false) {
            $this->load->view("admin/safa_tripstatus/edit", $data);
        } else {

            $this->tripstatus_model->name_ar = $this->input->post('name_ar');
            $this->tripstatus_model->name_la = $this->input->post('name_la');
            $this->tripstatus_model->description_ar = $this->input->post('description_ar');
            $this->tripstatus_model->description_la = $this->input->post('description_la');
            $this->tripstatus_model->code = $this->input->post('code');

            $this->tripstatus_model->save();

            $this->load->view('admin/redirect_message', array('msg' => lang('global_updated_message'), 'url' => site_url('admin/safa_tripstatus'), 'id' => $this->uri->segment("4"), 'model_name' => 'safa_tripstatus', 'model_title' => lang('menu_techn_flight_cases'), 'action' => lang('safa_tripstatus_title_edit')));
        }
    }

    function delete() {

        $this->layout = 'ajax';

        if (!$this->input->is_ajax_request()) {
            redirect('admin/safa_tripstatus/index');
        } else {
            $safa_tripstatus_id = $this->input->post('safa_tripstatus_id');
            $data = $this->tripstatus_model->check_delete_ability($safa_tripstatus_id);
            if ($data == 0) {
                $this->tripstatus_model->safa_tripstatus_id = $safa_tripstatus_id;
                if ($this->tripstatus_model->delete())
                    echo json_encode(array('response' => TRUE, 'msg' => 'deleted'));
            }
            else {
                echo json_encode(array('response' => FALSE, 'msg' => 'not deleted'));
            }
        }
    }

    function delete_all() {
        if ($this->input->post("delete_items")) {
            foreach ($this->input->post("delete_items") as $item) {

                $this->tripstatus_model->delete();
            }
        } else
            show_404();
        redirect("admin/safa_tripstatus/index");
    }

    function search() {
        
    }

}

/* End of file safa_tripstatus.php */
/* Location: ./application/controllers/safa_tripstatus.php */