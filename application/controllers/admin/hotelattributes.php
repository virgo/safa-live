<?php
class Hotelattributes extends Safa_Controller {

    public $module = "hotelattributes";
    public function __construct() {
        parent::__construct();
        $this->layout='new';
        $this->load->model('hotelattributes_model');
        $this->lang->load('admins/hotelattributes'); 
        permission();
    }
    public function index() {        
        if(isset($_GET['search'])) $this->search();
        $data["total_rows"] = $this->hotelattributes_model->get(true);
        $this->hotelattributes_model->offset = $this->uri->segment("4");
        $this->hotelattributes_model->limit =$this->config->item('per_page');
        $data["items"] = $this->hotelattributes_model->get();
        $config['uri_segment'] = 4;
        $config['base_url'] = site_url('admin/hotelattributes/index');
        $config['total_rows'] = $data["total_rows"];
        $config['per_page'] = $this->config->item('per_page');
        $config['suffix'] = $this->input->get() ? '?'.http_build_query($_GET) : NULL;
        $this->load->library('pagination');
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();
        $this->load->view('admin/hotelattributes/index', $data);
    }

    public function add() {
        $this->load->library("form_validation");  
        $this->form_validation->set_rules('name_ar', 'lang:hotelattributes_namear', 'trim');
        $this->form_validation->set_rules('name_la', 'lang:hotelattributes_namela', 'trim');
      
        
        if(name()=='name_ar')
            $this->form_validation->set_rules('name_ar', 'lang:hotelattributes_namear', 'trim|required');
        else
           $this->form_validation->set_rules('name_la', 'lang:hotelattributes_namela', 'trim|required');
        
        if ($this->form_validation->run() == false) {
              $this->load->view("admin/hotelattributes/add");
        } else {

            $this->hotelattributes_model->name_ar = $this->input->post('name_ar');
            $this->hotelattributes_model->name_la = $this->input->post('name_la');
          $this->hotelattributes_model->save();
          $this->load->view('redirect_message', 
              array('msg' => lang('global_added_message'), 
             'url' => site_url('admin/hotelattributes/index'),
             'model_title'=> lang('parent_child_title'),
             'action'=>lang('add_hotelattribute')));  
        }
    }

    public function edit($id) {
        if(!$id)
            show_404 ();
        $this->hotelattributes_model->safa_hotel_attribute_id =$id;
        $data['items']=$this->hotelattributes_model->get();
        $this->load->library("form_validation");
        $this->form_validation->set_rules('name_ar', 'lang:hotelattributes_namear', 'trim');
        $this->form_validation->set_rules('name_la', 'lang:hotelattributes_namela', 'trim');
        
        if(name()=='name_ar')
            $this->form_validation->set_rules('name_ar', 'lang:hotelattributes_namear', 'trim|required');
        else
           $this->form_validation->set_rules('name_la', 'lang:hotelattributes_namela', 'trim|required');
        
        
     
        if ($this->form_validation->run() == false) {
              $this->load->view("admin/hotelattributes/edit",$data);
        } else {
            $this->hotelattributes_model->name_ar = $this->input->post('name_ar');
            $this->hotelattributes_model->name_la = $this->input->post('name_la');
          $this->hotelattributes_model->save();
          $this->load->view('redirect_message', 
              array('msg' => lang('global_updated_message'), 
             'url' => site_url('admin/hotelattributes'),
             'id'=>$id,     
             'model_title'=> lang('parent_child_title'),
             'action'=>lang('edit_hotelattribute')));  
        }
    }
    function delete($id) {
        if(!$id)
        show_404();
                $this->hotelattributes_model->safa_hotel_attribute_id= $id;
                $this->hotelattributes_model->delete();
                redirect('admin/hotelattributes/index');
        }
        
}
/* End of file hotelattributes.php */
/* Location: ./application/controllers/admin/hotelattributes.php */

