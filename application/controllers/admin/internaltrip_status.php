<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
class Internaltrip_status extends Safa_Controller {
    public $module = "internaltrip_status";
    public function __construct() {
            parent::__construct();
            $this->load->model('internaltrip_status_model');
            $this->lang->load('admins/internaltrip_status');
//            
            permission();
    }
    public function index(){
        
        $data["items"] = $this->internaltrip_status_model->get();
        $this->load->view('admin/internaltrip_status/index', $data);
         // no search 
        //no pagination
    }
    public function add(){
        $this->load->library("form_validation");
        $this->form_validation->set_rules('internaltripstatus_name_ar"', 'lang:internaltripstatus_name_ar', 'trim|required|is_unique[]');
        $this->form_validation->set_rules('internaltripstatus_name_la', 'lang:internaltripstatus_name_la', 'trim|required');
        $this->form_validation->set_rules('internaltripstatus_code', 'lang:internaltripstatus_code', 'trim|alpha_numeric');
        $this->form_validation->set_rules('internaltripstatus_color', 'lang:internaltripstatus_color', 'trim|required');
        $this->form_validation->set_rules('internaltripstatus_description_ar', 'lang:internaltripstatus_description_ar', 'trim');
        $this->form_validation->set_rules('internaltripstatus_description_ar', 'lang:internaltripstatus_description_la', 'trim');
        
       if($this->form_validation->run()==false)
             $this->load->view("admin/internaltrip_status/add");
       else{
          // insert into the database 
       }
   }
    
}