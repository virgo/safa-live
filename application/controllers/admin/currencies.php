<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Currencies extends Safa_Controller {
       
    public $module = "currencies";
    public function __construct() {
        parent::__construct();
        $this->layout='new';
        
        $this->load->model('currencies_model');
        $this->lang->load('admins/currencies');
        permission();
    }

    public function index() {
        $this->currencies_model->limit = $this->config->item('per_page');
        $this->currencies_model->offset = $this->uri->segment('4');
        $config['uri_segment'] = 4;
        $config['base_url'] = site_url('admin/currencies/index');
        $config['total_rows'] = $this->db->get('erp_currencies')->num_rows();
        $config['per_page'] = $this->config->item('per_page');
        $this->load->library('pagination');
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();
        $data['items'] = $this->currencies_model->get();
        $this->load->view('admin/currencies/index', $data);
    }

    public function add() {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('name_la', 'lang:currencies_name_ar', 'trim');
        $this->form_validation->set_rules('name_ar', 'lang:currencies_name_la', 'trim');
        $this->form_validation->set_rules('short_name_la', 'lang:currencies_short_name_la', 'trim');
        $this->form_validation->set_rules('short_name_ar', 'lang:currencies_short_name_ar', 'trim');
        $this->form_validation->set_rules('symbol', 'lang:currencies_symbol', 'trim|required');
        if (name() == 'name_ar') {
            $this->form_validation->set_rules('name_ar', 'lang:currencies_name_ar', 'trim|required');
            $this->form_validation->set_rules('short_name_ar', 'lang:currencies_short_name_ar', 'trim|required');
        }
        if (name() == 'name_la') {
            $this->form_validation->set_rules('name_la', 'lang:currencies_name_la', 'trim|required');
            $this->form_validation->set_rules('short_name_la', 'lang:currencies_short_name_la', 'trim|required');
        }

        if ($this->form_validation->run() == FALSE) {
            $this->load->view('admin/currencies/add');
        } else {
            $this->currencies_model->name_ar = $this->input->post('name_ar');
            $this->currencies_model->name_la = $this->input->post('name_la');
            $this->currencies_model->short_name = $this->input->post('short_name_la');
            $this->currencies_model->short_name_ar = $this->input->post('short_name_ar');
            $this->currencies_model->symbol = $this->input->post('symbol');
            $this->currencies_model->timestamp = time();
            $this->currencies_model->save();
            $this->load->view('redirect_message', array('msg' => lang('global_added_message'),
                'url' => site_url('admin/currencies/index'),
                'model_title' => lang('parent_child_title'),
                'action' => lang('add_meal')));
        }
    }

    public function edit($id = FALSE) {
        if (!$id)
            show_404();
        $this->currencies_model->erp_currency_id = $id;
        $data['item'] = $this->currencies_model->get();
        if (!$data['item'])
            show_404();
        $this->load->library('form_validation');
        $this->form_validation->set_rules('name_la', 'lang:currencies_name_ar', 'trim');
        $this->form_validation->set_rules('name_ar', 'lang:currencies_name_la', 'trim');
        $this->form_validation->set_rules('short_name_la', 'lang:currencies_short_name_la', 'trim');
        $this->form_validation->set_rules('short_name_ar', 'lang:currencies_short_name_ar', 'trim');
        $this->form_validation->set_rules('symbol', 'lang:currencies_symbol', 'trim|required');
        if (name() == 'name_ar') {
            $this->form_validation->set_rules('name_ar', 'lang:currencies_name_ar', 'trim|required');
            $this->form_validation->set_rules('short_name_ar', 'lang:currencies_short_name_ar', 'trim|required');
        }
        if (name() == 'name_la') {
            $this->form_validation->set_rules('name_la', 'lang:currencies_name_la', 'trim|required');
            $this->form_validation->set_rules('short_name_la', 'lang:currencies_short_name_la', 'trim|required');
        }

        if ($this->form_validation->run() == FALSE) {
            $this->load->view('admin/currencies/edit',$data);
        } else {
            $this->currencies_model->name_ar = $this->input->post('name_ar');
            $this->currencies_model->name_la = $this->input->post('name_la');
            $this->currencies_model->short_name = $this->input->post('short_name_la');
            $this->currencies_model->short_name_ar = $this->input->post('short_name_ar');
            $this->currencies_model->symbol = $this->input->post('symbol');
            $this->currencies_model->timestamp = time();
            $this->currencies_model->save();
            $this->load->view('redirect_message', array('msg' => lang('global_added_message'),
                'url' => site_url('admin/currencies'),
                'erp_currency_id'=>$id,
                'model_title' => lang('parent_child_title'),
                'action' => lang('edit_currency')));
        }
    }

    function delete($id = FALSE) {
        if (!permission('currencies_delete'))
            show_error(lang('global_insufficient_permission'));
        if (!$id)
            show_404();
        $this->common->report('{report_currencies_deleted}');
        $this->currencies_model->erp_currency_id = $id;
        $this->currencies_model->delete();
        $this->load->view('admin/redirect', array('msg' => lang('global_deleted_successfully'), 'url' => site_url('admin/currencies/index')));
    }

}

/* End of file currencies.php */
/* Location: ./application/controllers/admin/currencies.php */