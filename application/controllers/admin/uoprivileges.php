<?php

class Uoprivileges extends Safa_Controller {

    public $module = "uoprivileges";

    public function __construct() {
        parent::__construct();
		$this->layout = 'new';
		
        $this->load->model('uoprivileges_model');
        $this->lang->load('admins/privileges');
//        
        permission();
    }

    public function index() {
        if (isset($_GET['search']))
            $this->search();
        $data["total_rows"] = $this->uoprivileges_model->search(true);
        $this->uoprivileges_model->offset = $this->uri->segment("4");
        $this->uoprivileges_model->limit = 10;
        $data["items"] = $this->uoprivileges_model->search();
        $config['uri_segment'] = 4;
        $config['base_url'] = site_url('admin/uoprivileges/index');
        $config['total_rows'] = $data["total_rows"];
        $config['per_page'] = $this->uoprivileges_model->limit;
        $config['suffix'] = $this->input->get() ? '?' . http_build_query($_GET) : NULL;
        $this->load->library('pagination');
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();
        $this->load->view('admin/uoprivileges/index', $data);
    }

    public function add() {
       
        $this->load->library("form_validation");
        $this->form_validation->set_rules('name_ar', 'lang:privileges_name_ar', 'trim|required|is_unique[safa_uoprivileges.name_ar]|min_length[3]|max_length[50]');
        $this->form_validation->set_rules('name_la', 'lang:privileges_name_la', 'trim|required|is_unique[safa_uoprivileges.name_la]|min_length[3]|max_length[50]');
        $this->form_validation->set_rules('description_ar', 'lang:privileges_description_ar', 'trim|required|min_length[3]|max_length[50]');
        $this->form_validation->set_rules('description_la', 'lang:privileges_description_la', 'trim|required|min_length[3]|max_length[50]');
        $this->form_validation->set_rules('role_name', 'lang:privileges_role_name', 'trim|required|min_length[3]|max_length[50]');
        if ($this->form_validation->run() == false) {
            $this->load->view("admin/uoprivileges/add");
        } else {
            $this->uoprivileges_model->name_ar = $this->input->post('name_ar');
            $this->uoprivileges_model->name_la = $this->input->post('name_la');
            $this->uoprivileges_model->description_ar = $this->input->post('description_ar');
            $this->uoprivileges_model->description_la = $this->input->post('description_la');
            $this->uoprivileges_model->role_name = $this->input->post('role_name');
            $this->uoprivileges_model->save();
        $this->load->view('redirect_message', array('msg' => lang('global_added_message'), 'url' => site_url('admin/uoprivileges'), 'model_title'=> lang('menu_management_users'),'action'=>lang('privileges_add_title')));
            //redirect("admin/admins/index");
        }
    }

    public function edit($id) {
        if (!$id)
            show_404();

        $this->uoprivileges_model->safa_uoprivilege_id = $id;
        $data['items'] = $this->uoprivileges_model->get();

        if (!$data['items'])
            show_404();

        $this->load->library("form_validation");
        $this->form_validation->set_rules('name_ar', 'lang:privileges_name_ar', 'trim|required|callback_check_name_ar|min_length[3]|max_length[50]');
        $this->form_validation->set_rules('name_la', 'lang:privileges_name_la', 'trim|required|callback_check_name_la|min_length[3]|max_length[50]');
        $this->form_validation->set_rules('description_ar', 'lang:privileges_description_ar', 'trim|required|min_length[3]|max_length[50]');
        $this->form_validation->set_rules('description_la', 'lang:privileges_description_la', 'trim|required|min_length[3]|max_length[50]');
        $this->form_validation->set_rules('role_name', 'lang:privileges_role_name', 'trim|required|min_length[3]|max_length[50]');
        if ($this->form_validation->run() == false) {
            $this->load->view("admin/uoprivileges/edit", $data);
        } else {
            $this->uoprivileges_model->name_ar = $this->input->post('name_ar');
            $this->uoprivileges_model->name_la = $this->input->post('name_la');
            $this->uoprivileges_model->description_ar = $this->input->post('description_ar');
            $this->uoprivileges_model->description_la = $this->input->post('description_la');
            $this->uoprivileges_model->role_name = $this->input->post('role_name');
            $this->uoprivileges_model->save();
            $this->load->view('redirect_message', array('msg' => lang('global_updated_message'), 'url' => site_url('admin/uoprivileges'), 'id'=>$this->uri->segment("4"),'model_title'=> lang('menu_management_users'),'action'=>lang('privileges_edit_title')));
            //redirect("admin/admins/index");
        }
    }

    function delete() {
         $this->layout='ajax';
        if (!$this->input->is_ajax_request()) {
            redirect('admin/uoprivileges/index');
        } else {
            $safa_uoprivilege_id =$this->input->post('safa_uoprivilege_id');
            $data = $this->uoprivileges_model->check_delete_ability($safa_uoprivilege_id);

            if ($data==0) {
                $this->uoprivileges_model->safa_uoprivilege_id = $safa_uoprivilege_id;
                if ($this->uoprivileges_model->delete())
                    echo json_encode(array('response' => TRUE, 'msg' => 'deleted'));
            }
            else {
                echo json_encode(array('response' => FALSE, 'msg' => 'not deleted'));
            }
        }
    }

    

    function search() {

        if ($this->input->get("name_ar"))
            $this->uoprivileges_model->name_ar = $this->input->get("name_ar");

        if ($this->input->get("name_la"))
            $this->uoprivileges_model->name_la = $this->input->get("name_la");
    }

    function check_name_ar($name_ar) {
        if ($this->uri->segment('3') == 'edit')
            $this->db->where('safa_uoprivilege_id !=', $this->uri->segment('4'));
        $this->db->where('name_ar', $name_ar);
        if ($this->db->get('safa_uoprivileges')->num_rows()) {
            $this->form_validation->set_message('check_name_ar', lang('privileges_name_exist'));
            return FALSE;
        } else {
            return TRUE;
        }
    }
    
    function check_name_la($name_la) {
        if ($this->uri->segment('3') == 'edit')
            $this->db->where('safa_uoprivilege_id !=', $this->uri->segment('4'));
        $this->db->where('name_la', $name_la);
        if ($this->db->get('safa_uoprivileges')->num_rows()) {
            $this->form_validation->set_message('check_name_la', lang('privileges_name_exist'));
            return FALSE;
        } else {
            return TRUE;
        }
    }

}

/* End of file poll.php */
/* Location: ./application/controllers/poll.php */