<?php

class Sub_hotels_levels extends Safa_Controller {

    public $module = "sub_hotels_levels";

    public function __construct() {
        parent::__construct();

        $this->load->model('sub_hotels_levels_model');
        $this->lang->load('admins/sub_hotels_levels');

        permission();
    }

    public function index() {   
        if(isset($_GET['search'])) $this->search();
        $this->sub_hotels_levels_model->join = TRUE;
        $data["total_rows"] = $this->sub_hotels_levels_model->get(true);
        $this->sub_hotels_levels_model->offset = $this->uri->segment("4");
        $this->sub_hotels_levels_model->limit = 10;
        $data["items"] = $this->sub_hotels_levels_model->get();
        $config['uri_segment'] = 4;
        $config['base_url'] = site_url('admin/sub_hotels_levels/index');
        $config['total_rows'] = $data["total_rows"];
        $config['per_page'] = $this->config->item('per_page');
        $config['suffix'] = $this->input->get() ? '?'.http_build_query($_GET) : NULL;
        $this->load->library('pagination');
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();
        $this->load->view('admin/sub_hotels_levels/index', $data);
    }

    public function add() {

        $this->load->library("form_validation");
        $this->form_validation->set_rules('name_ar', 'lang:sub_hotels_levels_name_ar', 'trim|required');
        $this->form_validation->set_rules('name_la', 'lang:sub_hotels_levels_name_la', 'trim|required');
        
        if ($this->form_validation->run() == false) {

            $this->load->view("admin/sub_hotels_levels/add");
        } else {
            
            $this->sub_hotels_levels_model->name_ar = $this->input->post('name_ar');
            $this->sub_hotels_levels_model->name_la = $this->input->post('name_la');

            $this->sub_hotels_levels_model->save();
            $this->load->view('redirect_message', array('msg' => lang('global_added_message'), 'url' => site_url('admin/sub_hotels_levels/index'),'model_title'=> lang('menu_main_hotels_levels'),'action'=>lang('hotels_levels_add_title')));
    
        }
    }
    public function edit($id) {

        if ( ! $id)
            show_404();
        
        $this->sub_hotels_levels_model->erp_hotel_sub_levels_id = $id;
        $data['items'] = $this->sub_hotels_levels_model->get();
        
        if ( ! $data['items'])
            show_404();
        
        $this->load->library("form_validation");
        $this->form_validation->set_rules('name_ar', 'lang:hotels_levels_name_ar', 'trim|required');
        $this->form_validation->set_rules('name_la', 'lang:hotels_levels_name_la', 'trim|required');
       
        if ($this->form_validation->run() == false) {
            $this->load->view("admin/sub_hotels_levels/edit", $data);
        } else {
            $this->sub_hotels_levels_model->name_ar = $this->input->post('name_ar');
            $this->sub_hotels_levels_model->name_la = $this->input->post('name_la');
           
            $this->sub_hotels_levels_model->save();
            $this->load->view('redirect_message', array('msg' => lang('global_updated_message'), 'url' => site_url('admin/sub_hotels_levels/index'), 'id'=>$this->uri->segment("4"),'model_title'=> lang('menu_main_hotels_levels'),'action'=>lang('sub_hotels_levels_edit_title')));
        }
    }



    
    function delete() {
        $this->layout='ajax';
        if (!$this->input->is_ajax_request()) {
           //redirect('admin/sub_hotels_levels/index')
        } else {
            $erp_hotel_sub_levels_id = $this->input->post('erp_hotel_sub_levels_id');
           
                $this->sub_hotels_levels_model->erp_hotel_sub_levels_id = $erp_hotel_sub_levels_id;
                if ($this->sub_hotels_levels_model->delete()) {
                    echo json_encode(array('response' => TRUE, 'msg' => 'deleted'));
                } else {
                    echo json_encode(array('response' => FALSE, 'msg' => 'not deleted user'));
                }
           
        }
    }


 



}

/* End of file sub_hotels_levels.php */
/* Location: ./application/controllers/sub_hotels_levels.php */
