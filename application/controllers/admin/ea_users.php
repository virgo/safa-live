<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Ea_users extends Safa_Controller {

    public $module = "ea_users";

    public function __construct() {
        parent::__construct();
        $this->load->model('ea_users_model');
        $this->lang->load('safa_ea_users');
        permission();
    }

    public function index() {

        if (isset($_GET['search']))
            $this->search();
        $data["total_rows"] = $this->ea_users_model->search(true);
        $this->ea_users_model->offset = $this->uri->segment("4");
        $this->ea_users_model->limit = 10;
        $data["items"] = $this->ea_users_model->search();
        $config['uri_segment'] = 4;
        $config['base_url'] = site_url('admin/ea_users/index');
        $config['total_rows'] = $data["total_rows"];
        $config['per_page'] = $this->ea_users_model->limit;
        $config['suffix'] = $this->input->get() ? '?' . http_build_query($_GET) : NULL;
        $this->load->library('pagination');
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();
        $this->load->view('admin/ea_users/index', $data);
    }

    public function add() {
//        echo "<pre>";
//        echo $this->input->post('safa_ea_usergroup_id');
//        echo '</pre>';

        $this->load->library("form_validation");
        $this->form_validation->set_rules('safa_ea_id', 'lang:ea_users_safa_ea_id', 'trim|required');
        $this->form_validation->set_rules('name_ar', 'lang:ea_users_name_ar', 'trim|required');
        $this->form_validation->set_rules('name_la', 'lang:ea_users_name_la', 'trim|required');
        $this->form_validation->set_rules('username', 'lang:ea_users_username', 'trim|required');
        $this->form_validation->set_rules('password', 'lang:ea_users_password', 'trim|required|matches[passconf]');
        $this->form_validation->set_rules('passconf', 'lang:ea_users_confirm_password', 'trim|required');
//        $this->form_validation->set_rules('safa_ea_usergroup_id','lang:ea_users_safa_ea_usergroup_id','trim|required');

        if ($this->form_validation->run() == false) {
            $this->load->view("admin/ea_users/add");
        } else {

            $this->ea_users_model->safa_ea_id = session('admin_safa_ea_id');
            $this->ea_users_model->name_ar = $this->input->post('name_ar');
            $this->ea_users_model->name_la = $this->input->post('name_la');
            $this->ea_users_model->username = $this->input->post('username');
            $this->ea_users_model->password = md5($this->input->post('password'));
            $this->ea_users_model->safa_ea_usergroup_id = $this->input->post('safa_ea_usergroup_id');


            $this->ea_users_model->save();
            redirect("admin/ea_users/index");
        }
    }

    public function edit($id) {

        if (!$id)
            show_404();

        $this->ea_users_model->safa_ea_user_id = $id;
        $data['item'] = $this->ea_users_model->get();

        if (!$data['item'])
            show_404();

        $this->load->library("form_validation");
        $this->form_validation->set_rules('name_ar', 'lang:ea_users_name_ar', 'trim|required');
        $this->form_validation->set_rules('name_la', 'lang:ea_users_name_la', 'trim|required');
        $this->form_validation->set_rules('password', 'lang:ea_users_password', 'trim|matches[passconf]');
        $this->form_validation->set_rules('passconf', 'lang:ea_users_confirm_password', 'trim');

        if ($this->form_validation->run() == false) {
            $this->load->view("admin/ea_users/edit", $data);
        } else {
            $this->ea_users_model->safa_ea_user_id = $id;
            $this->ea_users_model->safa_ea_id = session('admin_safa_ea_id');
            $this->ea_users_model->name_ar = $this->input->post('name_ar');
            $this->ea_users_model->name_la = $this->input->post('name_la');
            $this->ea_users_model->username = $this->input->post('username');
            $this->ea_users_model->password = md5($this->input->post('password'));
            $this->ea_users_model->safa_ea_usergroup_id = $this->input->post('safa_ea_usergroup_id');


            $this->ea_users_model->save();

            redirect("admin/ea_users/index");
        }
    }

    function delete() {
        if (!$this->input->is_ajax_request()) {
            redirect('admin/ea_users/index');
        } else {
            $safa_ea_user_id = $this->input->post('safa_ea_user_id');
//            $data = $this->ea_users_model->check_delete_ability($safa_ea_user_id);
//            if ($data==0) {
            $this->ea_users_model->safa_ea_user_id = $safa_ea_user_id;
            if ($this->ea_users_model->delete())
                echo json_encode(array('response' => TRUE, 'msg' => 'deleted'));
//            }
//            else {
//                echo json_encode(array('response' => FALSE, 'msg' => 'not deleted'));
//            }
        }
    }

    function delete_all() {
        if ($this->input->post("delete_items")) {
            foreach ($this->input->post("delete_items") as $item) {
                $this->ea_users_model->safa_ea_user_id = $item;

                $this->ea_users_model->delete();
            }
        } else
            show_404();
        redirect("admin/ea_users/index");
    }

    function search() {

        if ($this->input->get("safa_ea_usergroup_id"))
            $this->ea_users_model->safa_ea_usergroup_id = $this->input->get("safa_ea_usergroup_id");
    }

}

/* End of file ea_users.php */
/* Location: ./application/controllers/ea_users.php */