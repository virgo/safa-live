<?php

class Notifications extends Safa_Controller {

    public $module = "notifications";

    public function __construct() {
        parent::__construct();
        $this->layout = 'new';

        $this->load->model('erp_system_events_model');
        $this->load->model('erp_notifications_tags_model');
        $this->lang->load('admins/notifications');
        permission();
    }

    public function index() {

        $this->load->library("form_validation");
        $this->form_validation->set_rules('event_type', 'lang:event_type', 'trim|required');
        $this->form_validation->set_rules('event', 'lang:event', 'trim|required');
        $this->form_validation->set_rules('subject', 'lang:subject', 'trim|required');
        $this->form_validation->set_rules('body', 'lang:body', 'trim|required');

        if ($this->form_validation->run() == false) {

            $erp_notifications_tags_rows = $this->erp_notifications_tags_model->get();
            $data['erp_notifications_tags_rows'] = $erp_notifications_tags_rows;

            $this->load->view("admin/notifications/index", $data);
        } else {

            $event_key = $this->input->post('event');

            $this->erp_system_events_model->erp_system_events_id = $event_key;
            $erp_system_events_row = $this->erp_system_events_model->get();

            $name_ar = $erp_system_events_row->name_ar;
            $name_la = $erp_system_events_row->name_la;

            $subject = $this->input->post('subject');
            $body = $this->input->post('body');


            if (!file_exists('./static/xml/notifications/notifications_ar.xml')) {

                $xml = new DOMDocument('1.0', 'utf-8');

                $root = $xml->createElement("notifications");
                $xml->appendChild($root);

                $system_event_key = $xml->createElement("key");
                $system_event_key_text = $xml->createTextNode($event_key);
                $system_event_key->appendChild($system_event_key_text);

                $system_event_name_ar = $xml->createElement("name_ar");
                $system_event_name_ar_text = $xml->createTextNode($name_ar);
                $system_event_name_ar->appendChild($system_event_name_ar_text);

                $system_event_name_la = $xml->createElement("name_la");
                $system_event_name_la_text = $xml->createTextNode($name_la);
                $system_event_name_la->appendChild($system_event_name_la_text);

                $notification_subject = $xml->createElement("subject");
                $notification_subject_text = $xml->createTextNode($subject);
                $notification_subject->appendChild($notification_subject_text);

                $notification_body = $xml->createElement("body");
                $notification_body_text = $xml->createTextNode($body);
                $notification_body->appendChild($notification_body_text);


                $notification = $xml->createElement("notification");
                $notification->appendChild($system_event_key);
                $notification->appendChild($system_event_name_ar);
                $notification->appendChild($system_event_name_la);
                $notification->appendChild($notification_subject);
                $notification->appendChild($notification_body);

                $root->appendChild($notification);

                $xml->formatOutput = true;
                //echo "<xmp>". $xml->saveXML() ."</xmp>";

                $xml->save('./static/xml/notifications/notifications_ar.xml') or die("Error");
            } else {

                $xml = new DOMDocument('1.0', 'utf-8');

                /**
                 * Added to format the xml code, when open with editor.
                 */
                $xml->formatOutput = true;
                $xml->preserveWhiteSpace = false;

                $xml->Load('./static/xml/notifications/notifications_ar.xml');

                /**
                 * 
                 * Check if this event inserted before
                 */
                $notifications = $xml->getElementsByTagName('notification');
                foreach ($notifications as $notification) {
                    $loop_current_key = $notification->getElementsByTagName('key');
                    $loop_current_key_value = $loop_current_key->item(0)->nodeValue;

                    if ($loop_current_key_value == $event_key) {
                        $xml->documentElement->removeChild($notification);
                        //$xml->save('./static/xml/notifications/notifications_ar.xml') or die("Error");
                        break;
                    }
                }
                //------------------------------------------------------------------

                $root = $xml->documentElement;
                $xml->appendChild($root);

                $system_event_key = $xml->createElement("key");
                $system_event_key_text = $xml->createTextNode($event_key);
                $system_event_key->appendChild($system_event_key_text);

                $system_event_name_ar = $xml->createElement("name_ar");
                $system_event_name_ar_text = $xml->createTextNode($name_ar);
                $system_event_name_ar->appendChild($system_event_name_ar_text);

                $system_event_name_la = $xml->createElement("name_la");
                $system_event_name_la_text = $xml->createTextNode($name_la);
                $system_event_name_la->appendChild($system_event_name_la_text);

                $notification_subject = $xml->createElement("subject");
                $notification_subject_text = $xml->createTextNode($subject);
                $notification_subject->appendChild($notification_subject_text);

                $notification_body = $xml->createElement("body");
                $notification_body_text = $xml->createTextNode($body);
                $notification_body->appendChild($notification_body_text);


                $notification = $xml->createElement("notification");
                $notification->appendChild($system_event_key);
                $notification->appendChild($system_event_name_ar);
                $notification->appendChild($system_event_name_la);
                $notification->appendChild($notification_subject);
                $notification->appendChild($notification_body);

                $root->appendChild($notification);

                $xml->save('./static/xml/notifications/notifications_ar.xml') or die("Error");
            }
            $this->load->view('admin/redirect_message', array('msg' => lang('global_added_message'), 'url' => site_url('admin/notifications'), 'model_name' => 'notifications', 'model_title' => lang('menu_title'), 'action' => lang('notifications_add')));
        }
    }

    function getTagDataForAjax() {
        if (file_exists('./static/xml/notifications/notifications_ar.xml')) {
            $event_key = $_POST['event_key'];
            $xml = new DOMDocument('1.0', 'utf-8');
            /**
             * Added to format the xml code, when open with editor.
             */
            $xml->formatOutput = true;
            $xml->preserveWhiteSpace = false;

            $xml->Load('./static/xml/notifications/notifications_ar.xml');
            /**
             * 
             * Check if this event inserted before
             */
            $event_key_exist = false;
            $notifications = $xml->getElementsByTagName('notification');
            foreach ($notifications as $notification) {
                $loop_current_key = $notification->getElementsByTagName('key');
                $loop_current_key_value = $loop_current_key->item(0)->nodeValue;

                if ($loop_current_key_value == $event_key) {
                    $event_key_exist = true;

                    $loop_current_subject = $notification->getElementsByTagName('subject');
                    $loop_current_subject_value = $loop_current_subject->item(0)->nodeValue;

                    $loop_current_body = $notification->getElementsByTagName('body');
                    $loop_current_body_value = $loop_current_body->item(0)->nodeValue;


                    echo $loop_current_subject_value . '#$@&*#@#$#' . $loop_current_body_value;
                    exit;
                }
            }

            if (!$event_key_exist) {
                echo '';
                exit;
            }

            //------------------------------------------------------------------
        } else {
            echo '';
            exit;
        }
    }

    public function getEventsByEventType() {
        $event_type = $this->input->post('event_type');

        $structure = array('erp_system_events_id', name());
        $key = $structure['0'];
        if (isset($structure['1'])) {
            $value = $structure['1'];
        }
        $events_arr = array();
        $events_arr[''] = lang('global_select_from_menu');
        $this->erp_system_events_model->type = $event_type;
        $events = $this->erp_system_events_model->get();
        foreach ($events as $event) {
            $events_arr[$event->$key] = $event->$value;
        }


        $form_dropdown = form_dropdown('event', $events_arr, set_value('event', ''), 'class="chosen-select chosen-rtl input-full"  tabindex="4" ');
        $form_dropdown = str_replace("<select name='event'  id='event' name='event' class='select' style='width:100%;' >", '', $form_dropdown);
        $form_dropdown = str_replace('</select>', '', $form_dropdown);
        echo $form_dropdown;
        exit;
    }

}
