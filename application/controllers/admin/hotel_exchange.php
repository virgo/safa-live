<?php

class Hotel_exchange extends Safa_Controller {

	function __construct() {
		parent::__construct();

		//Side menu session, By Gouda.
		session('side_menu_id', 8);

		$this->layout = 'new';

		$this->load->library("form_validation");
		
		$this->lang->load('hotels');
		$this->load->model('hotel_exchange_model');

		permission();
			
	}

	function index()
	{
			
	}

	function manage()
	{
		$data['erp_cities'] = ddgen('erp_cities', array('erp_city_id', name()), FALSE, FALSE, TRUE);
        $data['erp_hotels'] = ddgen('erp_hotels', array('erp_hotel_id', name()), FALSE, FALSE, TRUE);
        
		$this->form_validation->set_rules('old_erp_hotel_id', 'lang:old_erp_hotel_id', 'trim|required');
		$this->form_validation->set_rules('new_erp_hotel_id', 'lang:new_erp_hotel_id', 'trim|required');

		if ($this->form_validation->run() == false) {
			$this->load->view("admin/hotel_exchange/manage", $data);
		} else {
			
			$this->hotel_exchange_model->old_erp_hotel_id = $this->input->post('old_erp_hotel_id');
			$this->hotel_exchange_model->new_erp_hotel_id = $this->input->post('new_erp_hotel_id');
			$this->hotel_exchange_model->save();
				
			$this->load->view('redirect_new_message', array('msg' => lang('global_updated_message'),
                    'url' => site_url('admin/hotel_exchange/manage'),
                    'model_title' => lang('hotel_exchange'), 'action' => lang('hotel_exchange')));
		}
	}
	
	function get_hotels_by_city_ajax($erp_city_id = false)
	{
		$hotels = ddgen('erp_hotels', array('erp_hotel_id', name()), array('erp_city_id' => $erp_city_id), array(name(), 'ASC'), TRUE);
		$return = '';
		foreach ($hotels as $key => $value)
		$return .= "<option value='$key'>$value</option>\n";

		$this->layout = 'ajax';
		echo $return;
		die();
	}


}
