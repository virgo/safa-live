<?php
class Roomattributes extends Safa_Controller {

    public $module = "roomattributes";
    public function __construct() {
        parent::__construct();
        $this->layout='new';
        $this->load->model('roomattributes_model');
        $this->lang->load('admins/roomattributes'); 
        permission();
    }
    public function index() {        
        if(isset($_GET['search'])) $this->search();
        $data["total_rows"] = $this->roomattributes_model->get(true);
        $this->roomattributes_model->offset = $this->uri->segment("4");
        $this->roomattributes_model->limit =$this->config->item('per_page');
        $data["items"] = $this->roomattributes_model->get();
        $config['uri_segment'] = 4;
        $config['base_url'] = site_url('admin/roomattributes/index');
        $config['total_rows'] = $data["total_rows"];
        $config['per_page'] = $this->config->item('per_page');
        $config['suffix'] = $this->input->get() ? '?'.http_build_query($_GET) : NULL;
        $this->load->library('pagination');
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();
        $this->load->view('admin/roomattributes/index', $data);
    }

    public function add() {
        $this->load->library("form_validation");  
        $this->form_validation->set_rules('name_ar', 'lang:roomattributes_namear', 'trim');
        $this->form_validation->set_rules('name_la', 'lang:roomattributes_namela', 'trim');
      
        
        if(name()=='name_ar')
            $this->form_validation->set_rules('name_ar', 'lang:roomattributes_namear', 'trim|required');
        else
           $this->form_validation->set_rules('name_la', 'lang:roomattributes_namela', 'trim|required');
        
        if ($this->form_validation->run() == false) {
              $this->load->view("admin/roomattributes/add");
        } else {

            $this->roomattributes_model->name_ar = $this->input->post('name_ar');
            $this->roomattributes_model->name_la = $this->input->post('name_la');
          $this->roomattributes_model->save();
          $this->load->view('redirect_message', 
              array('msg' => lang('global_added_message'), 
             'url' => site_url('admin/roomattributes/index'),
             'model_title'=> lang('parent_child_title'),
             'action'=>lang('add_roomattribute')));  
        }
    }

    public function edit($id) {
        if(!$id)
            show_404 ();
        $this->roomattributes_model->safa_room_attribute_id =$id;
        $data['items']=$this->roomattributes_model->get();
        $this->load->library("form_validation");
        $this->form_validation->set_rules('name_ar', 'lang:roomattributes_namear', 'trim');
        $this->form_validation->set_rules('name_la', 'lang:roomattributes_namela', 'trim');
        
        if(name()=='name_ar')
            $this->form_validation->set_rules('name_ar', 'lang:roomattributes_namear', 'trim|required');
        else
           $this->form_validation->set_rules('name_la', 'lang:roomattributes_namela', 'trim|required');
        
        
     
        if ($this->form_validation->run() == false) {
              $this->load->view("admin/roomattributes/edit",$data);
        } else {
            $this->roomattributes_model->name_ar = $this->input->post('name_ar');
            $this->roomattributes_model->name_la = $this->input->post('name_la');
          $this->roomattributes_model->save();
          $this->load->view('redirect_message', 
              array('msg' => lang('global_updated_message'), 
             'url' => site_url('admin/roomattributes'),
             'id'=>$id,     
             'model_title'=> lang('parent_child_title'),
             'action'=>lang('edit_roomattribute')));  
        }
    }
    function delete($id) {
        if(!$id)
        show_404();
                $this->roomattributes_model->safa_room_attribute_id= $id;
                $this->roomattributes_model->delete();
                redirect('admin/roomattributes/index');
        }
        
}
/* End of file roomattributes.php */
/* Location: ./application/controllers/admin/roomattributes.php */

