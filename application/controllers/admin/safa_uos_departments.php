<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Safa_uos_departments extends Safa_Controller {

    public function __construct() 
    {
        parent::__construct();
        $this->layout = 'new';
        $this->load->model('safa_uos_departments_model');
        $this->lang->load('safa_uos_departments');
        permission();
    }

    public function index($safa_uo_id=0) 
    {
        if (session('uo_id')) {
			$this->safa_uos_departments_model->safa_uo_id = session('uo_id');
			$data["safa_uo_id"] = session('uo_id');
		} else {
			$this->safa_uos_departments_model->safa_uo_id = $safa_uo_id;
			$data["safa_uo_id"] = $safa_uo_id;
		}
		
		if (isset($_GET['search'])) {
			$this->search();
		}

		$data["total_rows"] = $this->safa_uos_departments_model->get(true);

		//$this->safa_uos_departments_model->offset = $this->uri->segment("4");
		//$this->safa_uos_departments_model->limit = $this->config->item('per_page');
		$data["items"] = $this->safa_uos_departments_model->get();
		//$config['uri_segment'] = 4;
		//$config['base_url'] = site_url('admins/safa_uos_departments/index');
		//$config['total_rows'] = $data["total_rows"];

		//$config['per_page'] = $this->config->item('per_page');
		//$config['suffix'] = $this->input->get() ? '?' . http_build_query($_GET) : NULL;
		//$this->load->library('pagination');
		//$this->pagination->initialize($config);
		//$data['pagination'] = $this->pagination->create_links();
        $this->load->view('admin/safa_uos_departments/index', $data);

    }

    public function manage($safa_uo_id=0, $id=0) 
    {
    	$data["safa_uo_id"] = $safa_uo_id;
    	
		if($id!=0) {
	        $this->safa_uos_departments_model->safa_uos_department_id = $id;
	        $data['item'] = $this->safa_uos_departments_model->get();
	        if (!$data['item'])
            show_404();
		}
		
        $this->load->library("form_validation");
        $this->form_validation->set_rules('name_ar', 'lang:name_ar', 'trim|required');
        $this->form_validation->set_rules('name_la', 'lang:name_la', 'trim|required');
        
        $this->form_validation->set_rules('email', 'lang:email', 'trim|valid_email');

        if ($this->form_validation->run() == false) {
            $this->load->view("admin/safa_uos_departments/manage", $data);
        } else {

        	if (session('uo_id')) {
				$this->safa_uos_departments_model->safa_uo_id = session('uo_id');
			} else {
				$this->safa_uos_departments_model->safa_uo_id = $safa_uo_id;
			}
            $this->safa_uos_departments_model->name_ar = $this->input->post('name_ar');
            $this->safa_uos_departments_model->name_la = $this->input->post('name_la');
            $this->safa_uos_departments_model->email = $this->input->post('email');
                        
        	if ($id) {
				$this->safa_uos_departments_model->safa_uos_department_id = $id;
				$safa_uos_department_id = $id;
				$this->safa_uos_departments_model->save();		

				$this->load->view('redirect_new_message', array('msg' => lang('global_updated_message'),
                    'url' => site_url('admin/safa_uos_departments/index/'.$safa_uo_id),
                    'id' => $safa_uos_department_id,
                    'model_title' => lang('safa_uos_departments'), 'action' => lang('edit_safa_uos_departments')));
				
			} else {
				$safa_uos_department_id = $this->safa_uos_departments_model->save();
				
				$this->load->view('redirect_new_message', array('msg' => lang('global_added_message'),
                    'url' => site_url('admin/safa_uos_departments/index/'.$safa_uo_id),
                    'id' => $safa_uos_department_id,
                    'model_title' => lang('safa_uos_departments'), 'action' => lang('add_safa_uos_departments')));
			}
        }
    }

    function delete($safa_uo_id=0, $id) 
    {
        if (!$id)
		show_404();

		$this->safa_uos_departments_model->safa_uos_department_id = $id;
        $deleted = $this->safa_uos_departments_model->delete();

		$this->load->view('redirect_message', array('msg' => lang('global_deleted_message'),
            'url' => site_url('admin/safa_uos_departments/index/'.$safa_uo_id),
			'model_title' => lang('safa_uos_departments'), 'action' => lang('delete_safa_uos_departments')));
    }

}

/* End of file safa_uos_departments.php */
/* Location: ./application/controllers/admin/safa_uos_departments.php */
