<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Itos_users extends Safa_Controller {

    public $module = "itos_users";

    public function __construct() {
        parent::__construct();
        $this->load->model('safa_ito_users_model');
        $this->lang->load('admins/itos_users');
        permission();
    }

    public function index() {

        if (isset($_GET['search']))
            $this->search();
        $data["total_rows"] = $this->safa_ito_users_model->search(true);
        $this->safa_ito_users_model->offset = $this->uri->segment("3");
        $this->safa_ito_users_model->limit = 10;
        $data["items"] = $this->safa_ito_users_model->search();
        $config['uri_segment'] = 3;
        $config['base_url'] = site_url('admin/safa_ito_users/index');
        $config['total_rows'] = $data["total_rows"];
        $config['per_page'] = $this->safa_ito_users_model->limit;
        $config['suffix'] = $this->input->get() ? '?' . http_build_query($_GET) : NULL;
        $this->load->library('pagination');
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();
        $this->load->view('admin/itos_users/index', $data);
    }

    public function add() {

        $this->load->library("form_validation");
        $this->form_validation->set_rules('name_ar', 'lang:itos_users_name_ar', 'trim|required|is_unique[safa_ito_users.name_ar]');
        $this->form_validation->set_rules('name_la', 'lang:itos_users_name_la', 'trim|required|is_unique[safa_ito_users.name_la]');
        $this->form_validation->set_rules('username', 'lang:itos_users_username', 'trim|required|is_unique[safa_ito_users.username]');
        $this->form_validation->set_rules('email', 'lang:itos_users_email', 'trim|required|valid_email');
        $this->form_validation->set_rules('ver_code', 'lang:itos_users_ver_code', 'trim|required');
        $this->form_validation->set_rules('safa_ito_usergroup_id', 'lang:itos_users_safa_ito_usergroup_id', 'trim|required');
        $this->form_validation->set_rules('safa_ito_id', 'lang:itos_users_safa_ito_id', 'trim|required');
        $this->form_validation->set_rules('erp_language_id', 'lang:itos_users_safa_erp_language_id', 'trim|required');
        $this->form_validation->set_rules('password', 'lang:itos_users_password', 'trim|required|matches[passconf]');
        $this->form_validation->set_rules('passconf', 'lang:itos_users_repeat_password', 'trim|required');


        if ($this->form_validation->run() == false) {
            $this->load->view("admin/itos_users/add");
        } else {
            $this->safa_ito_users_model->name_ar = $this->input->post('name_ar');
            $this->safa_ito_users_model->name_la = $this->input->post('name_la');
            $this->safa_ito_users_model->username = $this->input->post('username');
            $this->safa_ito_users_model->password = $this->input->post('password');
            $this->safa_ito_users_model->email = $this->input->post('email');
            $this->safa_ito_users_model->safa_ito_id = $this->input->post('safa_ito_id');
            $this->safa_ito_users_model->safa_ito_usergroup_id = $this->input->post('safa_ito_usergroup_id');
            $this->safa_ito_users_model->ver_code = $this->input->post('ver_code');
            $this->safa_ito_users_model->erp_language_id = $this->input->post('erp_language_id');
            $this->safa_ito_users_model->timestamp = time();

            $this->safa_ito_users_model->save();
            redirect("admin/itos_users/index");
        }
    }

    public function edit($id) {

        if (!$id)
            show_404();

        $this->safa_ito_users_model->safa_ito_user_id = $id;
        $data['item'] = $this->safa_ito_users_model->get();

        if (!$data['item'])
            show_404();

        $this->load->library("form_validation");
        $this->form_validation->set_rules('name_ar', 'lang:itos_users_name_ar', 'trim|required');
        $this->form_validation->set_rules('name_la', 'lang:itos_users_name_la', 'trim|required');
        $this->form_validation->set_rules('username', 'lang:itos_users_username', 'trim|required');
        $this->form_validation->set_rules('email', 'lang:itos_users_email', 'trim|required|valid_email');
        $this->form_validation->set_rules('ver_code', 'lang:itos_users_ver_code', 'trim|required');
        $this->form_validation->set_rules('safa_ito_usergroup_id', 'lang:itos_users_safa_ito_usergroup_id', 'trim|required');
        $this->form_validation->set_rules('safa_ito_id', 'lang:itos_users_safa_ito_id', 'trim|required');
        $this->form_validation->set_rules('erp_language_id', 'lang:itos_users_safa_erp_language_id', 'trim|required');
        $this->form_validation->set_rules('password', 'lang:itos_users_password', 'trim|matches[passconf]');
        $this->form_validation->set_rules('passconf', 'lang:itos_users_repeat_password', 'trim');

        if ($this->form_validation->run() == false) {
            $this->load->view("admin/itos_users/edit", $data);
        } else {
            $this->safa_ito_users_model->name_ar = $this->input->post('name_ar');
            $this->safa_ito_users_model->name_la = $this->input->post('name_la');
            $this->safa_ito_users_model->username = $this->input->post('username');
            $this->safa_ito_users_model->password = $this->input->post('password');
            $this->safa_ito_users_model->email = $this->input->post('email');
            $this->safa_ito_users_model->safa_ito_id = $this->input->post('safa_ito_id');
            $this->safa_ito_users_model->safa_ito_usergroup_id = $this->input->post('safa_ito_usergroup_id');
            $this->safa_ito_users_model->ver_code = $this->input->post('ver_code');
            $this->safa_ito_users_model->erp_language_id = $this->input->post('erp_language_id');
            $this->safa_ito_users_model->timestamp = time();

            $this->safa_ito_users_model->save();

            redirect("admin/itos_users/index");
        }
    }

    function delete($id) {
        if (!$id)
            show_404();

        $this->safa_ito_users_model->safa_ito_user_id = $id;
        if (!$this->safa_ito_users_model->delete())
            show_404();
        redirect("admin/itos_users/index");
    }

//     function delete() {
//        if (!$this->input->is_ajax_request()) {
//            redirect('admin/safa_tripstatus/index');
//        } else {
//            $safa_tripstatus_id =$this->input->post('safa_tripstatus_id');
//            $data = $this->tripstatus_model->check_delete_ability($safa_tripstatus_id);
//            if ($data==0) {
//                $this->tripstatus_model->safa_tripstatus_id = $safa_tripstatus_id;
//                if ($this->tripstatus_model->delete())
//                    echo json_encode(array('response' => TRUE, 'msg' => 'deleted'));
//            }
//            else {
//                echo json_encode(array('response' => FALSE, 'msg' => 'not deleted'));
//            }
//        }
//    }


    function delete_all() {
        if ($this->input->post("delete_items")) {
            foreach ($this->input->post("delete_items") as $item) {

                $this->tripstatus_model->delete();
            }
        } else
            show_404();
        redirect("admin/safa_tripstatus/index");
    }

    function search() {
        
    }

}

/* End of file safa_tripstatus.php */
/* Location: ./application/controllers/safa_tripstatus.php */