<?php

class Seasons extends Safa_Controller {

    public $module = "seasons";
    public $layout = "new";
    public function __construct() {
        parent::__construct();

        $this->load->model('seasons_model');
        $this->lang->load('admins/seasons');

        permission();
    }
    
    public function index() {      
        
        if(isset($_GET['search'])) $this->search();
        $this->seasons_model->join = TRUE;
        $data["total_rows"] = $this->seasons_model->get(true);
        $this->seasons_model->offset = $this->uri->segment("4");
        $this->seasons_model->limit = 10;
        $data["items"] = $this->seasons_model->get();
        $config['uri_segment'] = 4;
        $config['base_url'] = site_url('admin/seasons/index');
        $config['total_rows'] = $data["total_rows"];
        $config['per_page'] = $this->config->item('per_page');
        $config['suffix'] = $this->input->get() ? '?'.http_build_query($_GET) : NULL;
        $this->load->library('pagination');
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();
        $this->load->view('admin/seasons/index', $data);
    }

    public function add() {

        $this->load->library("form_validation");
        $this->form_validation->set_rules('name_ar', 'lang:seasons_name_ar', 'trim|required');
        $this->form_validation->set_rules('name_la', 'lang:seasons_name_la', 'trim|required');
        $this->form_validation->set_rules('start_date', 'lang:seasons_start_date', 'trim|required');
        $this->form_validation->set_rules('end_date', 'lang:seasons_end_date', 'trim|required');
        
        if ($this->form_validation->run() == false) {

            $this->load->view("admin/seasons/add");
        } else {
            
            $this->seasons_model->name_ar = $this->input->post('name_ar');
            $this->seasons_model->name_la = $this->input->post('name_la');
            $this->seasons_model->start_date = $this->input->post('start_date');
            $this->seasons_model->end_date = $this->input->post('end_date');
            
            $this->seasons_model->save();
            $this->load->view('redirect_message', array('msg' => lang('global_added_message'), 'url' => site_url('admin/seasons/index'),'model_title'=> lang('menu_main_seasons'),'action'=>lang('seasons_add_title')));
    
        }
    }
    public function edit($id) {

        if ( ! $id)
            show_404();
        
        $this->seasons_model->erp_season_id = $id;
        $data['items'] = $this->seasons_model->get();
        
        $data['item']=$this->seasons_model->get();
        
        
        if ( ! $data['items'])
            show_404();
        
        $this->load->library("form_validation");
       
        $this->form_validation->set_rules('name_ar', 'lang:seasons_name_ar', 'trim|required');
        $this->form_validation->set_rules('name_la', 'lang:seasons_name_la', 'trim|required');
        $this->form_validation->set_rules('start_date', 'lang:seasons_start_date', 'trim|required');
        $this->form_validation->set_rules('end_date', 'lang:seasons_end_date', 'trim|required');
       
        
        if ($this->form_validation->run() == false) {
            $this->load->view("admin/seasons/edit", $data);
        } else {
           
            $this->seasons_model->name_ar = $this->input->post('name_ar');
            $this->seasons_model->name_la = $this->input->post('name_la');
            $this->seasons_model->start_date = $this->input->post('start_date');
            $this->seasons_model->end_date = $this->input->post('end_date');
           
           
            $this->seasons_model->save();
            $this->load->view('redirect_message', array('msg' => lang('global_updated_message'), 'url' => site_url('admin/seasons/index'), 'id'=>$this->uri->segment("4"),'model_title'=> lang('menu_main_seasons'),'action'=>lang('seasons_edit_title')));
        }
    }
    
    
     function delete() {
        $this->layout='ajax';
            
        $erp_season_id = $this->input->post('erp_season_id');

        $this->seasons_model->erp_season_id = $erp_season_id;

        $this->seasons_model->delete();
        redirect('admin/seasons');
           
    }


}

/* End of file seasons.php */
/* Location: ./application/controllers/seasons.php */
