<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Safa_itos extends Safa_Controller {

    public $module = "safa_itos";

    public function __construct() {
        parent::__construct();
        $this->layout = 'new';

        $this->load->model('itos_model');
        $this->load->model('safa_ito_users_model');
        $this->lang->load('admins/safa_itos');
        $this->lang->load('admins/itos_users');
        permission();
    }

    public function index() {


        if (isset($_GET['search']))
            $this->search();
        $data["total_rows"] = $this->itos_model->search(true);
        $this->itos_model->offset = $this->uri->segment("4");
        $this->itos_model->limit = $this->config->item('per_page');

        $data["items"] = $this->itos_model->search();
        $config['uri_segment'] = 4;
        $config['base_url'] = site_url('admin/safa_itos/index');
        $config['total_rows'] = $data["total_rows"];
        $config['per_page'] = $this->itos_model->limit;
        $config['suffix'] = $this->input->get() ? '?' . http_build_query($_GET) : NULL;
        $this->load->library('pagination');
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();
        $this->load->view('admin/safa_itos/index', $data);
    }

    public function add() {

        $this->load->library("form_validation");
        $this->form_validation->set_rules('name_ar', 'lang:safa_itos_name_ar', 'trim|required|is_unique[safa_eas.name_ar]');
        $this->form_validation->set_rules('name_la', 'lang:safa_itos_name_la', 'trim|required|is_unique[safa_eas.name_la]');
        $this->form_validation->set_rules('erp_country_id', 'lang:safa_itos_erp_country_id', 'trim|required');
        $this->form_validation->set_rules('reference_code', 'lang:safa_itos_reference_code', 'trim|required');

        #validation on records that found in table ea_users
        $this->form_validation->set_rules('user_username', 'lang:safa_itos_user_username', 'trim|required|alpha|is_unique[safa_ito_users.username]');
        $this->form_validation->set_rules('user_password', 'lang:safa_itos_user_password', 'trim|required');
        $this->form_validation->set_rules('user_passconf', 'lang:safa_itos_user_repeat_password', 'trim|required|matches[user_password]');


        if ($this->form_validation->run() == false) {
            $this->load->view("admin/safa_itos/add");
        } else {

            $this->itos_model->name_ar = $this->input->post('name_ar');
            $this->itos_model->name_la = $this->input->post('name_la');
            $this->itos_model->erp_country_id = $this->input->post('erp_country_id');
            $this->itos_model->reference_code = $this->input->post('reference_code');

            $safa_ito_id = $this->itos_model->save();
            if ($safa_ito_id) {
                #insert in table ito_users by id
                $this->safa_ito_users_model->name_ar = $this->input->post('name_ar');
                $this->safa_ito_users_model->name_la = $this->input->post('name_la');
                $this->safa_ito_users_model->username = $this->input->post('user_username');
                $this->safa_ito_users_model->password = md5($this->input->post('user_password'));
                $this->safa_ito_users_model->safa_ito_usergroup_id = 1;
                $this->safa_ito_users_model->safa_ito_id = $safa_ito_id;
                $this->safa_ito_users_model->save();
                
                /* add_contracts */
                $this->add_contracts($safa_ito_id);
            }

            $this->load->view('admin/redirect_message', array('msg' => lang('global_added_message'), 'url' => site_url('admin/safa_itos'), 'model_name' => 'safa_itos', 'model_title' => lang('menu_safa_itos'), 'action' => lang('safa_itos_add_title')));
        }
    }

    //By Gouda.
	function add_contracts($safa_ito_id) {

        $this->itos_model->safa_ito_id = $safa_ito_id;
        $this->itos_model->add_contracts();
    }
    
    public function edit($id) {

        if (!$id)
            show_404();

        $this->itos_model->join = TRUE;

        $this->itos_model->safa_ito_id = $id;
        $data['items'] = $this->itos_model->get();

        if (!$data['items'])
            show_404();

        $this->load->library("form_validation");
        $this->form_validation->set_rules('name_ar', 'lang:safa_itos_name_ar', 'trim|required');
        $this->form_validation->set_rules('name_la', 'lang:safa_itos_name_la', 'trim|required');
        $this->form_validation->set_rules('erp_country_id', 'lang:safa_itos_erp_country_id', 'trim|required');
        $this->form_validation->set_rules('reference_code', 'lang:safa_itos_reference_code', 'trim|required');


        if ($this->form_validation->run() == false) {
            $this->load->view("admin/safa_itos/edit", $data);
        } else {

            $this->itos_model->name_ar = $this->input->post('name_ar');
            $this->itos_model->name_la = $this->input->post('name_la');
            $this->itos_model->erp_country_id = $this->input->post('erp_country_id');
            $this->itos_model->reference_code = $this->input->post('reference_code');

            $this->itos_model->save();

            $this->load->view('admin/redirect_message', array('msg' => lang('global_updated_message'), 'url' => site_url('admin/safa_itos'), 'id' => $this->uri->segment("4"), 'model_name' => 'safa_itos', 'model_title' => lang('menu_safa_itos'), 'action' => lang('safa_itos_edit_title')));
        }
    }

    function delete() {

        $this->layout = 'ajax';

        if (!$this->input->is_ajax_request()) {
            redirect('admin/safa_itos/index');
        } else {
            $safa_ito_id = $this->input->post('safa_ito_id');
            $data = $this->itos_model->check_delete_ability($safa_ito_id);
//              echo $data; //die();
            if ($data == 0) {
                $this->itos_model->safa_ito_id = $safa_ito_id;
                if ($this->itos_model->delete())
                    echo json_encode(array('response' => TRUE, 'msg' => 'deleted'));
            }
            else {
                echo json_encode(array('response' => FALSE, 'msg' => 'not deleted'));
            }
        }
    }

    function delete_all() {
        if ($this->input->post("delete_items")) {
            foreach ($this->input->post("delete_items") as $item) {

                $this->eas_model->delete();
            }
        } else
            show_404();
        redirect("admin/safa_itos/index");
    }

    public function users_index($safa_ito_id) {

        if (isset($_GET['search']))
            $this->search();
        $this->safa_ito_users_model->offset = $this->uri->segment("3");
        $this->safa_ito_users_model->limit = $this->config->item('per_page');
        $this->safa_ito_users_model->safa_ito_id = $safa_ito_id;
        $data["items"] = $this->safa_ito_users_model->get();
        $config['uri_segment'] = 3;
        $config['base_url'] = site_url('admin/itos_users/index');
        $config['per_page'] = $this->safa_ito_users_model->limit;
        $config['suffix'] = $this->input->get() ? '?' . http_build_query($_GET) : NULL;
        $this->load->library('pagination');
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();
        $this->load->view('admin/itos_users/index', $data);
    }

    public function users_add($safa_ito_id = FALSE) {

        $this->load->library("form_validation");
        $this->form_validation->set_rules('name_ar', 'lang:safa_itos_name_ar', 'trim|required');
        $this->form_validation->set_rules('name_la', 'lang:safa_itos_name_la', 'trim|required');
        $this->form_validation->set_rules('username', 'lang:itos_users_username', 'trim|required');
        $this->form_validation->set_rules('email', 'lang:itos_users_email', 'trim|required|valid_email');
        $this->form_validation->set_rules('ver_code', 'lang:itos_users_ver_code', 'trim|required');
        $this->form_validation->set_rules('password', 'lang:itos_users_password', 'trim|required');
        $this->form_validation->set_rules('passconf', 'lang:itos_users_repeat_password', 'trim|required|matches[password]');

        if ($this->form_validation->run() == false) {
            $this->load->view("admin/itos_users/add");
        } else {
            $this->safa_ito_users_model->name_ar = $this->input->post('name_ar');
            $this->safa_ito_users_model->name_la = $this->input->post('name_la');
            $this->safa_ito_users_model->username = $this->input->post('username');
            $this->safa_ito_users_model->email = $this->input->post('email');
            $this->safa_ito_users_model->ver_code = $this->input->post('ver_code');
            $this->safa_ito_users_model->password = md5($this->input->post('password'));
            $this->safa_ito_users_model->safa_ito_id = $safa_ito_id;
            $this->safa_ito_users_model->safa_ito_usergroup_id = 1;

            $this->safa_ito_users_model->save();
            redirect("admin/safa_itos/users_index/" . $safa_ito_id);
        }
    }

    public function users_edit($id, $safa_ito_id) {

        if (!$id)
            show_404();
        $this->safa_ito_users_model->safa_ito_user_id = $id;
        $data['item'] = $this->safa_ito_users_model->get();
        if (!$data['item'])
            show_404();
        $this->load->library("form_validation");
        $this->form_validation->set_rules('name_ar', 'lang:safa_itos_name_ar', 'trim|required');
        $this->form_validation->set_rules('name_la', 'lang:safa_itos_name_la', 'trim|required');
        $this->form_validation->set_rules('username', 'lang:itos_users_username', 'trim|required');
        $this->form_validation->set_rules('email', 'lang:itos_users_email', 'trim|required|valid_email');
        $this->form_validation->set_rules('ver_code', 'lang:itos_users_ver_code', 'trim|required');
        $this->form_validation->set_rules('password', 'lang:itos_users_password', 'trim');
        $this->form_validation->set_rules('passconf', 'lang:itos_users_repeat_password', 'trim|matches[password]');

        if ($this->form_validation->run() == false) {
            $this->load->view("admin/itos_users/edit", $data);
        } else {

            $this->safa_ito_users_model->safa_ito_user_id = $id;
            $this->safa_ito_users_model->name_ar = $this->input->post('name_ar');
            $this->safa_ito_users_model->name_la = $this->input->post('name_la');
            $this->safa_ito_users_model->username = $this->input->post('username');
            $this->safa_ito_users_model->email = $this->input->post('email');
            $this->safa_ito_users_model->ver_code = $this->input->post('ver_code');
            $this->safa_ito_users_model->password = md5($this->input->post('password'));
            $this->safa_ito_users_model->safa_ito_id = $safa_ito_id;
            $this->safa_ito_users_model->safa_ito_usergroup_id = 1;

            $this->safa_ito_users_model->save();
            redirect("admin/safa_itos/users_index/" . $safa_ito_id);
        }
    }

    function users_delete($id, $safa_ito_id) {
        if (!$id)
            show_404();
        $this->safa_ito_users_model->safa_ito_user_id = $id;
        $this->safa_ito_users_model->delete();
        redirect("admin/safa_itos/users_index/" . $safa_ito_id);
    }

    // for activation and de activatation the external agent // 
    public function cancel_ito($ito_id = FALSE) {
        if (!$ito_id)
            show_404();
        $this->itos_model->safa_ito_id = $ito_id;
        $this->itos_model->disabled = 1;
        $updated = $this->itos_model->save();
        if ($updated)
            redirect('admin/safa_itos/index');
    }

    public function active_ito($ito_id = FALSE) {
        if (!$ito_id)
            show_404();
        $this->itos_model->safa_ito_id = $ito_id;
        $this->itos_model->disabled = 0;
        $updated = $this->itos_model->save();
        if ($updated)
            redirect('admin/safa_itos/index');
    }

}

/* End of file safa_itos.php */
/* Location: ./application/controllers/safa_itos.php */