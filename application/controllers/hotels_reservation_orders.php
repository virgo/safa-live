<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Hotels_reservation_orders extends Safa_Controller {

    public $module = "hotels_reservation_orders";
    private $sub_criteria = array(
        'date_from' => 'entry_date',
        'date_to' => 'exit_date',
        'hotel_id' => 'erp_hotels_id',
        'housing_type_id' => 'erp_housingtypes_id',
        'room_type_id' => 'erp_hotelroomsizes_id',
    );
    private $post_sub_criteria = array(
        'date_from' => 'rooms_entry_date',
        'date_to' => 'rooms_exit_date',
        'hotel_id' => 'rooms_erp_hotels_id',
        'housing_type_id' => 'rooms_erp_housingtypes_id',
        'room_type_id' => 'rooms_erp_hotelroomsizes_id',
    );
    private $sub_criteria2 = array(
        'date_from' => 'entry_date',
        'date_to' => 'exit_date',
        'housing_type_id' => 'erp_housingtypes_id',
        'price_from' => 'price_from',
        'price_to' => 'price_to',
        'room_type_id' => 'erp_hotelroomsizes_id',
        'currency' => 'erp_currencies_id',
    );

    public function __construct() {
        parent::__construct();
        $this->layout = 'new';
        if (session('uo_id')) {
            //Side menu session, By Gouda.
            session('side_menu_id', 5);
        } else if (session('ea_id')) {
            session('side_menu_id', 3);
        }else if (session('hm_id')) {
            session('side_menu_id', 1);
        }
        $this->load->model('erp_hotels_reservation_orders_model');
        $this->load->model('erp_hotels_reservation_orders_log_model');
        $this->load->model('erp_nationalities_model');
        $this->load->model('room_services_model');
        $this->load->model('hotels_model');
        $this->load->model('notification_model');
        $this->load->model('safa_umrahgroups_model');
        $this->load->model('contracts_model');
        $this->load->model('hotel_availability_search_model');
        $this->lang->load('hotels_reservation_orders');
        $this->load->helper('form_helper');
        $this->load->helper('db_helper');
        $this->load->helper('packages_helper');
        $this->load->helper('itconf_helper');
        permission();
    }

    public function index() {
        if (session('uo_id')) {
            $this->erp_hotels_reservation_orders_model->owner_erp_company_types_id = 2;
            $this->erp_hotels_reservation_orders_model->owner_erp_company_id = session('uo_id');
        } else if (session('ea_id')) {
            $this->erp_hotels_reservation_orders_model->owner_erp_company_types_id = 3;
            $this->erp_hotels_reservation_orders_model->owner_erp_company_id = session('ea_id');
        } else if (session('hm_id')) {
            $this->erp_hotels_reservation_orders_model->owner_erp_company_types_id = 5;
            $this->erp_hotels_reservation_orders_model->owner_erp_company_id = session('hm_id');
        } else {
            $this->erp_hotels_reservation_orders_model->owner_erp_company_types_id = 0;
            $this->erp_hotels_reservation_orders_model->owner_erp_company_id = 0;
        }

        $data["total_rows"] = $this->erp_hotels_reservation_orders_model->get(true);

        $this->erp_hotels_reservation_orders_model->offset = $this->uri->segment("3");
        //$this->erp_hotels_reservation_orders_model->limit = $this->config->item('per_page');
        $this->erp_hotels_reservation_orders_model->order_by = array('erp_hotels_reservation_orders_id', 'desc');
        $data["items"] = $this->erp_hotels_reservation_orders_model->get();
        $config['uri_segment'] = 3;
        $config['base_url'] = site_url('hotels_reservation_orders/index');
        $config['total_rows'] = $data["total_rows"];

        $config['per_page'] = $this->config->item('per_page');
        $config['suffix'] = $this->input->get() ? '?' . http_build_query($_GET) : NULL;
        $this->load->library('pagination');
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();
        $this->load->view('hotels_reservation_orders/index', $data);
    }

    public function incoming() {
        if (session('uo_id')) {
            $this->erp_hotels_reservation_orders_model->erp_company_types_id = 2;
            $this->erp_hotels_reservation_orders_model->erp_company_id = session('uo_id');
        } else if (session('ea_id')) {
            $this->erp_hotels_reservation_orders_model->erp_company_types_id = 3;
            $this->erp_hotels_reservation_orders_model->erp_company_id = session('ea_id');
        } else if (session('hm_id')) {
            $this->erp_hotels_reservation_orders_model->erp_company_types_id = 5;
            $this->erp_hotels_reservation_orders_model->erp_company_id = session('hm_id');
        }  else {
            $this->erp_hotels_reservation_orders_model->erp_company_types_id = 0;
            $this->erp_hotels_reservation_orders_model->erp_company_id = 0;
        }

        $data["total_rows"] = $this->erp_hotels_reservation_orders_model->get(true);

        $this->erp_hotels_reservation_orders_model->offset = $this->uri->segment("3");
        //$this->erp_hotels_reservation_orders_model->limit = $this->config->item('per_page');
        $this->erp_hotels_reservation_orders_model->order_by = array('sort', 'asc');
        $data["items"] = $this->erp_hotels_reservation_orders_model->get();

        $config['uri_segment'] = 3;
        $config['base_url'] = site_url('hotels_reservation_orders/incoming');
        $config['total_rows'] = $data["total_rows"];

        $config['per_page'] = $this->config->item('per_page');
        $config['suffix'] = $this->input->get() ? '?' . http_build_query($_GET) : NULL;
        $this->load->library('pagination');
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();
        $this->load->view('hotels_reservation_orders/incoming', $data);
    }

    public function add() {
        $data = array();
        $data['nationalities_rows'] = $this->erp_nationalities_model->get();

        $data['nationalities'] = ddgen('erp_nationalities', array('erp_nationality_id', name()), FALSE, FALSE, TRUE);


        $structure = array('erp_room_service_id', name());
        $key = $structure['0'];
        if (isset($structure['1'])) {
            $value = $structure['1'];
        }
        $room_services_arr = array();
        $room_services_rows = $this->room_services_model->get();
        foreach ($room_services_rows as $room_services_row) {
            $room_services_arr[$room_services_row->$key] = $room_services_row->$value;
        }
        $data['room_services_multiselect'] = form_multiselect('room_services[]', $room_services_arr, set_value('room_services', ''), 'class="chosen-select chosen-rtl input-full" multiple tabindex="4" id="room_services" data-placeholder="' . lang('global_all') . '"');


        $data['advantages'] = ddgen('erp_hotel_advantages', array('erp_hotel_advantages_id', name()), FALSE, FALSE, TRUE);

        $data['erp_cities'] = ddgen('erp_cities', array('erp_city_id', name()), array('erp_country_id' => 966), FALSE, TRUE);
        $data['erp_hotels'] = ddgen('erp_hotels', array('erp_hotel_id', name()), FALSE, FALSE, TRUE);
        $data['erp_currencies'] = ddgen('erp_currencies', array('erp_currency_id', name()), FALSE, FALSE, TRUE);

        $data['erp_meals'] = ddgen('erp_meals', array('erp_meal_id', name()), FALSE, FALSE, TRUE);

        $data['erp_housingtypes'] = ddgen('erp_housingtypes', array('erp_housingtype_id', name()), FALSE, FALSE, TRUE);
        $data['erp_hotelroomsizes'] = ddgen('erp_hotelroomsizes', array('erp_hotelroomsize_id', name()), FALSE, FALSE, TRUE);

        $data["uo_contracts"] = $this->create_contracts_array();

        $max_order_no = $this->erp_hotels_reservation_orders_model->getMaxId();
        $data['current_order_no'] = $max_order_no + 1;

        if (session('ea_id')) {
            $this->contracts_model->safa_ea_id = session('ea_id');
            $contract_row = $data['contract_row'] = $this->contracts_model->search();
        }

        //By Gouda
        //---------------------------------------------------------
        $structure = array('safa_uo_id', 'safa_uos_name');
        $key = $structure['0'];
        if (isset($structure['1'])) {
            $value = $structure['1'];
        }
        $companies_arr = array();
        if (isset($contract_row)) {
            if (count($contract_row) > 0) {
                $this->contracts_model->safa_uo_contract_id = $contract_row[0]->safa_uo_contract_id;
            }
        }
        $companies = $this->contracts_model->search();
        $companies_arr[''] = lang('global_select');
        foreach ($companies as $company) {
            $companies_arr[$company->$key] = $company->$value;
        }
        //$data['uo_companies'] = ddgen('safa_uos', array('safa_uo_id', name()), FALSE, FALSE, TRUE);
        $data['uo_companies'] = $companies_arr;
        //---------------------------------------------------------


        $this->load->library("form_validation");

        $this->form_validation->set_rules('erp_company_id', 'lang:company', 'trim|required');
        $this->form_validation->set_rules('order_date', 'lang:order_date', 'trim|required');

        //$this->form_validation->set_rules('erp_cities_id[]', 'lang:city', 'trim|required');

        if ($this->form_validation->run() == false) {
            if ($this->input->post('tripid')) {
                $this->layout = 'ajax';
                $data['tripid'] = $this->input->post('tripid');
                $this->load->model('safa_trips_model');
                $data['item_rooms'] = $this->safa_trips_model->get_hotels_rooms($data['tripid']);
            }

            if ($this->input->post('packageid')) {
                $this->layout = 'ajax';
                $data['packageid'] = $this->input->post('packageid');
                $this->load->model('safa_packages_model');
                $package = $this->db->where('safa_package_id', $data['packageid'])->get('safa_packages')->row();
                $data['companytype'] = $package->erp_company_type_id;
                $data['companyid'] = $package->erp_company_id;
                $p_hotels = $this->safa_packages_model->get_package_hotels($data['packageid']);
                $cities = array('');
                $hotels = array('');
                foreach ($p_hotels as $htl) {
                    $cities[] = $htl->erp_city_id;
                    $hotels[] = $htl->erp_hotel_id;
                }
                foreach ($data['erp_cities'] as $ctykey => $name) {
                    if (!in_array($ctykey, $cities)) {
                        unset($data['erp_cities'][$ctykey]);
                    }
                }
                foreach ($data['erp_hotels'] as $htlkey => $name) {
                    if (!in_array($htlkey, $hotels)) {
                        unset($data['erp_hotels'][$htlkey]);
                    }
                }
            }

            $this->load->view("hotels_reservation_orders/add", $data);
        } else {

            if (session('uo_id')) {
                $this->erp_hotels_reservation_orders_model->owner_erp_company_types_id = 2;
                $this->erp_hotels_reservation_orders_model->owner_erp_company_id = session('uo_id');
            } else if (session('ea_id')) {
                $this->erp_hotels_reservation_orders_model->owner_erp_company_types_id = 3;
                $this->erp_hotels_reservation_orders_model->owner_erp_company_id = session('ea_id');
            } else if (session('hm_id')) {
                $this->erp_hotels_reservation_orders_model->owner_erp_company_types_id = 5;
                $this->erp_hotels_reservation_orders_model->owner_erp_company_id = session('hm_id');
            }
            if ($this->input->post('company_type')) {
                $this->erp_hotels_reservation_orders_model->erp_company_types_id = $this->input->post('company_type');
            } else {
                $this->erp_hotels_reservation_orders_model->erp_company_types_id = 2;
            }

            $this->erp_hotels_reservation_orders_model->erp_company_id = $this->input->post('erp_company_id');
            $this->erp_hotels_reservation_orders_model->the_date = date('Y-m-d H:i', time());
            $this->erp_hotels_reservation_orders_model->order_date = $this->input->post('order_date');
            $this->erp_hotels_reservation_orders_model->arrival_date = $this->input->post('arrival_date');
            $this->erp_hotels_reservation_orders_model->departure_date = $this->input->post('departure_date');
            $this->erp_hotels_reservation_orders_model->contact_person = $this->input->post('contact_person');
            $this->erp_hotels_reservation_orders_model->safa_trip_id = $this->input->post('tripid');
            $this->erp_hotels_reservation_orders_model->safa_package_id = $this->input->post('packageid');
            //            $this->erp_hotels_reservation_orders_model->erp_hotels_id = $this->input->post('erp_hotels_id');
            //            $this->erp_hotels_reservation_orders_model->erp_hotels_id_alternative = $this->input->post('erp_hotels_id_alternative');
            //            $this->erp_hotels_reservation_orders_model->distance_from = $this->input->post('distance_from');
            //            $this->erp_hotels_reservation_orders_model->distance_to = $this->input->post('distance_to');
            $this->erp_hotels_reservation_orders_model->order_status = 3;

            if (isset($_POST['look_to_haram'])) {
                $this->erp_hotels_reservation_orders_model->look_to_haram = 1;
            } else {
                $this->erp_hotels_reservation_orders_model->look_to_haram = 0;
            }

            if (isset($_POST['safa_uo_contract_id'])) {
                $this->erp_hotels_reservation_orders_model->safa_uo_contract_id = $this->input->post('safa_uo_contract_id');
            }

            /*
              $this->erp_hotels_reservation_orders_model->price_from = $this->input->post('price_from');
              $this->erp_hotels_reservation_orders_model->price_to = $this->input->post('price_to');
              $this->erp_hotels_reservation_orders_model->erp_currencies_id = $this->input->post('erp_currencies_id');
             */

            // Save Order
            $erp_hotels_reservation_orders_id = $this->erp_hotels_reservation_orders_model->save();



            // Save nationalities
            $nationalities = $this->input->post('nationalities');
            if (ensure($nationalities)) {
                foreach ($nationalities as $nationality_id) {
                    $this->erp_hotels_reservation_orders_model->nationalities_erp_hotels_reservation_orders_id = $erp_hotels_reservation_orders_id;
                    $this->erp_hotels_reservation_orders_model->nationalities_erp_nationalities_id = $nationality_id;
                    $this->erp_hotels_reservation_orders_model->saveNationalities();
                }
            }
            // Save hotel_advantages
            $advantages = $this->input->post('advantages');
            if (ensure($advantages)) {
                foreach ($advantages as $advantage_id) {
                    $this->erp_hotels_reservation_orders_model->advantages_erp_hotels_reservation_orders_id = $erp_hotels_reservation_orders_id;
                    $this->erp_hotels_reservation_orders_model->advantages_erp_hotel_advantages_id = $advantage_id;
                    $this->erp_hotels_reservation_orders_model->saveHotelAdvantages();
                }
            }
            // Save room_services
            $room_services = $this->input->post('room_services');

            if (ensure($room_services)) {
                foreach ($room_services as $room_service_id) {
                    $this->erp_hotels_reservation_orders_model->room_services_erp_hotels_reservation_orders_id = $erp_hotels_reservation_orders_id;
                    $this->erp_hotels_reservation_orders_model->room_services_erp_room_services_id = $room_service_id;
                    $this->erp_hotels_reservation_orders_model->saveRoomServices();
                }
            }



            // Save meals
            $meals_erp_meals = $this->input->post('meals_erp_meals');
            $meals_meals_count = $this->input->post('meals_meals_count');
            $meals_erp_hotel_id = $this->input->post('meals_erp_hotel_id');
            if (ensure($meals_erp_meals))
                foreach ($meals_erp_meals as $key => $value) {
                    $this->erp_hotels_reservation_orders_model->meals_erp_hotels_reservation_orders_id = $erp_hotels_reservation_orders_id;
                    $this->erp_hotels_reservation_orders_model->meals_erp_meals_id = $meals_erp_meals[$key];
                    $this->erp_hotels_reservation_orders_model->meals_meals_count = $meals_meals_count[$key];
                    //$this->erp_hotels_reservation_orders_model->meals_price = $meals_price[$key];
                    $this->erp_hotels_reservation_orders_model->meals_erp_hotel_id = $meals_erp_hotel_id[$key];
                    $safa_hotels_reservation_orders_meals_id = $this->erp_hotels_reservation_orders_model->saveMeals();
                }


            $hotel_name = '';


            // Save rooms
            $rooms_housingtypes = $this->input->post('rooms_erp_housingtypes_id');
            $rooms_hotelroomsizes = $this->input->post('rooms_erp_hotelroomsizes_id');
            $rooms_city_id = $this->input->post('rooms_erp_cities_id');
            $rooms_hotels_id = $this->input->post('rooms_erp_hotels_id');
            $rooms_rooms_count = $this->input->post('rooms_rooms_count');
            $rooms_nights_count = $this->input->post('rooms_nights_count');
            $rooms_entry_date = $this->input->post('rooms_entry_date');
            $rooms_exit_date = $this->input->post('rooms_exit_date');
            $rooms_erp_meals_id = $this->input->post('rooms_erp_meals_id');
            $rooms_notes = $this->input->post('rooms_notes');

            if (ensure($rooms_housingtypes))
                foreach ($rooms_housingtypes as $key => $value) {
                    $this->erp_hotels_reservation_orders_model->rooms_erp_hotels_reservation_orders_id = $erp_hotels_reservation_orders_id;

                    $this->erp_hotels_reservation_orders_model->rooms_erp_cities_id = $rooms_city_id[$key];
                    $this->erp_hotels_reservation_orders_model->rooms_erp_hotels_id = $rooms_hotels_id[$key];
                    $this->erp_hotels_reservation_orders_model->rooms_erp_housingtypes_id = $rooms_housingtypes[$key];
                    $this->erp_hotels_reservation_orders_model->rooms_erp_hotelroomsizes_id = $rooms_hotelroomsizes[$key];
                    $this->erp_hotels_reservation_orders_model->rooms_rooms_count = $rooms_rooms_count[$key];
                    $this->erp_hotels_reservation_orders_model->rooms_nights_count = $rooms_nights_count[$key];
                    $this->erp_hotels_reservation_orders_model->rooms_entry_date = $rooms_entry_date[$key];
                    $this->erp_hotels_reservation_orders_model->rooms_exit_date = $rooms_exit_date[$key];
                    $this->erp_hotels_reservation_orders_model->rooms_erp_meals_id = $rooms_erp_meals_id[$key];

                    $this->erp_hotels_reservation_orders_model->rooms_notes = $rooms_notes[$key];

                    $safa_hotels_reservation_orders_rooms_id = $this->erp_hotels_reservation_orders_model->saveRooms();


                    //-------------- For Notification --------------------------------------------
                    $this->hotels_model->erp_hotel_id = $rooms_hotels_id[$key];
                    $this->hotels_model->join = true;
                    $erp_hotels_row = $this->hotels_model->get();
                    if (count($erp_hotels_row) > 0) {
                        $hotel_name = $hotel_name . ',' . $erp_hotels_row->hotel_name;
                    }
                    //----------------------------------------------------------------------------
                }



            //----------- Send Notification For Company ------------------------------
            $msg_datetime = date('Y-m-d H:i', time());

            $this->notification_model->notification_type = 'automatic';
            $this->notification_model->erp_importance_id = 1;
            $this->notification_model->sender_type_id = 1;
            $this->notification_model->sender_id = 0;

            $this->notification_model->erp_system_events_id = 16;

            $this->notification_model->language_id = 2;
            $this->notification_model->msg_datetime = $msg_datetime;

            /*
              $current_phase_name=$this->input->post('current_phase');
              $next_phase_id=$this->input->post('next_phase');
              $this->safa_contract_phases_model->safa_contract_phases_id=$next_phase_id;
              $next_phase_row=$this->safa_contract_phases_model->get();
              $next_phase_name=$next_phase_row->{name()};

              $tags="#*current_contract_phase#*=$current_phase_name*****#*next_contract_phase#*=$next_phase_name";
             */



            if (session('uo_id')) {
                $hotel_reservation_order_owner = session('uo_name');
            } else if (session('ea_id')) {
                $hotel_reservation_order_owner = session('ea_name');
            }

            //Link to replace.
            $link_hotels_reservation_orders = "<a href='" . site_url('hotels_reservation_orders/view/' . $erp_hotels_reservation_orders_id) . "'  target='_blank'> " . $erp_hotels_reservation_orders_id . " </a>";

            $tags = "#*the_date#*=" . date('Y-m-d H:i', time()) . "*****#*hotel_reservation_order_no#*=$link_hotels_reservation_orders*****#*hotel_reservation_order_owner#*=$hotel_reservation_order_owner*****#*hotel_name#*=$hotel_name";

            $this->notification_model->tags = $tags;
            $this->notification_model->parent_id = '';

            $erp_notification_id = $this->notification_model->save();

            //-------- Notification Details ---------------
            $this->notification_model->detail_erp_notification_id = $erp_notification_id;

            if ($this->input->post('company_type')) {
                $this->notification_model->detail_receiver_type = $this->input->post('company_type');
            } else {
                $this->notification_model->detail_receiver_type = 2;
            }
            $this->notification_model->detail_receiver_id = $this->input->post('erp_company_id');
            $this->notification_model->saveDetails();
            //--------------------------------
            //-------------------------------------------------------------------


            if (isset($erp_hotels_reservation_orders_id)) {

                // Insert Log Copy. For Reservation Order.
                //--------------------------------------------------
                $this->insert_log($erp_hotels_reservation_orders_id);
                //--------------------------------------------------


                $this->load->view('redirect_message', array('msg' => lang('global_added_message'),
                    //'url' => site_url('hotels_reservation_orders/edit/'.$erp_hotels_reservation_orders_id),
                    'url' => site_url('hotels_reservation_orders'),
                    'model_title' => lang('title'), 'action' => lang('add_title')));
            }
        }
    }

    public function addfrompkg($pkgid = FALSE) {
        //$this->layout = 'ajax';
        if (!$pkgid)
            show_404();
        $data['packageid'] = $pkgid;

        $this->load->library("form_validation");

        $this->form_validation->set_rules('fromdate', '', 'trim|required');
        $this->form_validation->set_rules('oreder_date', 'lang:order_date', 'trim|required');
        if ($this->form_validation->run() == false) {
            $this->load->model('safa_packages_model');
            $package = $this->db->where('safa_package_id', $data['packageid'])->get('safa_packages')->row();
            $data['packagename'] = $package->{name()};
            $data['package_periods'] = ddgen('erp_package_periods', array('erp_package_period_id', name()), array('erp_company_type_id' => $package->erp_company_type_id, 'erp_company_id' => $package->erp_company_id), FALSE, TRUE);
            $data['erp_hotelroomsizes'] = ddgen('erp_hotelroomsizes', array('erp_hotelroomsize_id', name()), FALSE, FALSE, TRUE);

            if (!$package)
                show_404();
            $data['companytype'] = $package->erp_company_type_id;
            $data['companyid'] = $package->erp_company_id;
            $data['rows'] = $this->safa_packages_model->get_package_hotels($data['packageid']);



            $this->load->model('safa_package_execlusive_meals_model');
            $this->safa_package_execlusive_meals_model->safa_package_id = $pkgid;
            $this->safa_package_execlusive_meals_model->join = TRUE;
            $data['item_execlusive_meals_prices'] = $this->safa_package_execlusive_meals_model->get();

            $data['orderid'] = $this->db->select_max('erp_hotels_reservation_orders_id')->get('erp_hotels_reservation_orders', 1)->row();
            //print_r($data['orderid']);

            $this->load->view('hotels_reservation_orders/pkgadd', $data);
        } else {

            //print_r($_POST);die();

            if (session('uo_id')) {
                $this->erp_hotels_reservation_orders_model->owner_erp_company_types_id = 2;
                $this->erp_hotels_reservation_orders_model->owner_erp_company_id = session('uo_id');
            } else if (session('ea_id')) {
                $this->erp_hotels_reservation_orders_model->owner_erp_company_types_id = 3;
                $this->erp_hotels_reservation_orders_model->owner_erp_company_id = session('ea_id');
            } else if (session('hm_id')) {
                $this->erp_hotels_reservation_orders_model->owner_erp_company_types_id = 5;
                $this->erp_hotels_reservation_orders_model->owner_erp_company_id = session('hm_id');
            }

            $package = $this->db->where('safa_package_id', $data['packageid'])->get('safa_packages')->row();
            if ($package) {
                $this->erp_hotels_reservation_orders_model->erp_company_types_id = $package->erp_company_type_id;
                $this->erp_hotels_reservation_orders_model->erp_company_id = $package->erp_company_id;
            }

            $this->erp_hotels_reservation_orders_model->the_date = date('Y-m-d H:i', time());
            $this->erp_hotels_reservation_orders_model->order_date = date('Y-m-d');
            $this->erp_hotels_reservation_orders_model->arrival_date = $this->input->post('fromdate');
            $this->erp_hotels_reservation_orders_model->departure_date = $this->input->post('departure_date');
            $this->erp_hotels_reservation_orders_model->safa_trip_id = $this->input->post('tripid');
            $this->erp_hotels_reservation_orders_model->safa_package_id = $pkgid;
            $this->erp_hotels_reservation_orders_model->order_status = 3;

            if (isset($_POST['look_to_haram'])) {
                $this->erp_hotels_reservation_orders_model->look_to_haram = 1;
            } else {
                $this->erp_hotels_reservation_orders_model->look_to_haram = 0;
            }

            $erp_hotels_reservation_orders_id = $this->erp_hotels_reservation_orders_model->save();
            $rooms_housingtypes = $this->input->post('rooms');
            $hotel_name = '';
            $entry_date = $this->input->post('fromdate');
            $departure_date = '';
            if (ensure($rooms_housingtypes) && $erp_hotels_reservation_orders_id)
                foreach ($rooms_housingtypes as $key => $value) {
                    $this->erp_hotels_reservation_orders_model->rooms_erp_hotels_reservation_orders_id = $erp_hotels_reservation_orders_id;
                    $this->erp_hotels_reservation_orders_model->rooms_erp_cities_id = $value['cityid_0'];
                    $this->erp_hotels_reservation_orders_model->rooms_erp_hotels_id = $value['hotelid_0'];
                    $this->erp_hotels_reservation_orders_model->rooms_erp_housingtypes_id = 1;
                    $this->erp_hotels_reservation_orders_model->rooms_erp_hotelroomsizes_id = $value['erp_hotelroomsize'];
                    $this->erp_hotels_reservation_orders_model->rooms_rooms_count = $value['roomscount'];
                    $this->erp_hotels_reservation_orders_model->rooms_nights_count = $value['nights_0'];
                    $this->erp_hotels_reservation_orders_model->rooms_entry_date = $entry_date;
                    $this->erp_hotels_reservation_orders_model->rooms_exit_date = $value['hotel0exit'];
                    $safa_hotels_reservation_orders_rooms_id = $this->erp_hotels_reservation_orders_model->saveRooms();

                    //-------------- For Notification --------------------------------------------
                    $this->hotels_model->erp_hotel_id = $value['hotelid_0'];
                    $this->hotels_model->join = true;
                    $erp_hotels_row = $this->hotels_model->get();
                    if (count($erp_hotels_row) > 0) {
                        $hotel_name = $hotel_name . ',' . $erp_hotels_row->hotel_name;
                    }
                    //----------------------------------------------------------------------------

                    $this->erp_hotels_reservation_orders_model->rooms_erp_hotels_reservation_orders_id = $erp_hotels_reservation_orders_id;
                    $this->erp_hotels_reservation_orders_model->rooms_erp_cities_id = $value['cityid_1'];
                    $this->erp_hotels_reservation_orders_model->rooms_erp_hotels_id = $value['hotelid_1'];
                    $this->erp_hotels_reservation_orders_model->rooms_erp_housingtypes_id = 1;
                    $this->erp_hotels_reservation_orders_model->rooms_erp_hotelroomsizes_id = $value['erp_hotelroomsize'];
                    $this->erp_hotels_reservation_orders_model->rooms_rooms_count = $value['roomscount'];
                    $this->erp_hotels_reservation_orders_model->rooms_nights_count = $value['nights_1'];
                    $this->erp_hotels_reservation_orders_model->rooms_entry_date = $value['hotel0exit'];
                    $this->erp_hotels_reservation_orders_model->rooms_exit_date = $value['hotel1exit'];
                    $safa_hotels_reservation_orders_rooms_id = $this->erp_hotels_reservation_orders_model->saveRooms();

                    if (strtotime($departure_date) < strtotime($value['hotel1exit']))
                        $departure_date = $value['hotel1exit'];
                    //-------------- For Notification --------------------------------------------
                    $this->hotels_model->erp_hotel_id = $value['hotelid_1'];
                    $this->hotels_model->join = true;
                    $erp_hotels_row = $this->hotels_model->get();
                    if (count($erp_hotels_row) > 0) {
                        $hotel_name = $hotel_name . ',' . $erp_hotels_row->hotel_name;
                    }
                    //----------------------------------------------------------------------------
                }

            $this->erp_hotels_reservation_orders_model->erp_hotels_reservation_orders_id = $erp_hotels_reservation_orders_id;
            $this->erp_hotels_reservation_orders_model->departure_date = $departure_date;
            $this->erp_hotels_reservation_orders_model->save();


            $meals_erp_meals = $this->input->post('meals_erp_meals');
            $meals_meals_count = $this->input->post('mealscount');
            $meals_erp_hotel_id = $this->input->post('meal_hotel_id');
            $meals_price = $this->input->post('mealsprice');
            if (ensure($meals_erp_meals))
                foreach ($meals_erp_meals as $key => $value) {
                    $this->erp_hotels_reservation_orders_model->meals_erp_hotels_reservation_orders_id = $erp_hotels_reservation_orders_id;
                    $this->erp_hotels_reservation_orders_model->meals_erp_meals_id = $meals_erp_meals[$key];
                    $this->erp_hotels_reservation_orders_model->meals_meals_count = $meals_meals_count[$key];
                    $this->erp_hotels_reservation_orders_model->meals_price = $meals_price[$key];
                    $this->erp_hotels_reservation_orders_model->meals_erp_hotel_id = $meals_erp_hotel_id[$key];
                    if ($meals_meals_count[$key] && $meals_meals_count[$key] > 0)
                        $safa_hotels_reservation_orders_meals_id = $this->erp_hotels_reservation_orders_model->saveMeals();
                }

            //----------- Send Notification For Company ------------------------------
            $msg_datetime = date('Y-m-d H:i', time());

            $this->notification_model->notification_type = 'automatic';
            $this->notification_model->erp_importance_id = 1;
            $this->notification_model->sender_type_id = 1;
            $this->notification_model->sender_id = 0;

            $this->notification_model->erp_system_events_id = 16;

            $this->notification_model->language_id = 2;
            $this->notification_model->msg_datetime = $msg_datetime;

            if (session('uo_id')) {
                $hotel_reservation_order_owner = session('uo_name');
            } else if (session('ea_id')) {
                $hotel_reservation_order_owner = session('ea_name');
            } else if (session('hm_id')) {
                $hotel_reservation_order_owner = session('hm_name');
            }

            //Link to replace.
            $link_hotels_reservation_orders = "<a href='" . site_url('hotels_reservation_orders/view/' . $erp_hotels_reservation_orders_id) . "'  target='_blank'> " . $erp_hotels_reservation_orders_id . " </a>";

            $tags = "#*the_date#*=" . date('Y-m-d H:i', time()) . "*****#*hotel_reservation_order_no#*=$link_hotels_reservation_orders*****#*hotel_reservation_order_owner#*=$hotel_reservation_order_owner*****#*hotel_name#*=$hotel_name";

            $this->notification_model->tags = $tags;
            $this->notification_model->parent_id = '';

            $erp_notification_id = $this->notification_model->save();

            //-------- Notification Details ---------------
            $this->notification_model->detail_erp_notification_id = $erp_notification_id;

            if ($package) {
                $this->notification_model->detail_receiver_type = $package->erp_company_type_id;
                $this->notification_model->detail_receiver_id = $package->erp_company_id;
            }

            $this->notification_model->saveDetails();
            //--------------------------------
            //-------------------------------------------------------------------

            if (isset($erp_hotels_reservation_orders_id)) {

                // Insert Log Copy. For Reservation Order.
                //--------------------------------------------------
                $this->insert_log($erp_hotels_reservation_orders_id);
                //--------------------------------------------------


                $this->load->view('redirect_message', array('msg' => lang('global_added_message'),
                    //'url' => site_url('hotels_reservation_orders/edit/'.$erp_hotels_reservation_orders_id),
                    'url' => site_url('hotels_reservation_orders'),
                    'model_title' => lang('title'), 'action' => lang('add_title')));
            }
        }
    }

    public function edit($id = FALSE) {
        $data = array();

        if (!$id) {
            show_404();
        }
        $this->erp_hotels_reservation_orders_model->owner_erp_company_id = $this->{$this->destination . '_id'};
        $this->erp_hotels_reservation_orders_model->owner_erp_company_types_id = $this->system_id;
        $this->erp_hotels_reservation_orders_model->erp_hotels_reservation_orders_id = $id;
        $item = $this->erp_hotels_reservation_orders_model->get();

        if (!$item) {
            show_404();
        }

        if ($item->order_status == 1) {
            session('error_message', lang('you_cannot_edit_or_delete_order_after_confirmation'));
            session('error_url', site_url('hotels_reservation_orders'));
            session('error_title', lang('title'));
            session('error_action', lang('edit_title'));
            redirect("error_message/show");
        }

        $data['item'] = $item;
        $data['nationalities_rows'] = $this->erp_nationalities_model->get();
        $data['nationalities'] = ddgen('erp_nationalities', array('erp_nationality_id', name()), FALSE, FALSE, TRUE);
        $data['room_services'] = ddgen('erp_room_services', array('erp_room_service_id', name()), FALSE, FALSE, TRUE);
        $data['advantages'] = ddgen('erp_hotel_advantages', array('erp_hotel_advantages_id', name()), FALSE, FALSE, TRUE);

        if (session('ea_id')) {
            $this->contracts_model->safa_ea_id = session('ea_id');
            $data['contract_row'] = $this->contracts_model->search();
        }

        if ($item->erp_company_types_id == 3) {
            $data['companies'] = ddgen('safa_eas', array('safa_ea_id', name()), FALSE, FALSE, TRUE);
        } else if ($item->erp_company_types_id == 5) {
            $data['companies'] = ddgen('safa_hms', array('safa_hm_id', name()), FALSE, FALSE, TRUE);
        } else {

            //------------------------------------------------------------------
            $structure = array('safa_uo_id', 'safa_uos_name');
            $key = $structure['0'];
            if (isset($structure['1'])) {
                $value = $structure['1'];
            }
            $companies_arr = array();
            $this->contracts_model->safa_uo_contract_id = $item->safa_uo_contract_id;
            $companies = $this->contracts_model->search();
            $companies_arr[''] = lang('global_select');
            foreach ($companies as $company) {
                $companies_arr[$company->$key] = $company->$value;
            }
            $data['companies'] = $companies_arr;
            //$data['companies'] = ddgen('safa_uos', array('safa_uo_id', name()), FALSE, FALSE, TRUE);
            //------------------------------------------------------------------
        }
        $data['erp_cities'] = ddgen('erp_cities', array('erp_city_id', name()), array('erp_country_id' => 966), FALSE, TRUE);
        $data['erp_hotels'] = ddgen('erp_hotels', array('erp_hotel_id', name()), FALSE, FALSE, TRUE);
        $data['erp_currencies'] = ddgen('erp_currencies', array('erp_currency_id', name()), FALSE, FALSE, TRUE);
        $data['erp_meals'] = ddgen('erp_meals', array('erp_meal_id', name()), FALSE, FALSE, TRUE);
        $data['erp_housingtypes'] = ddgen('erp_housingtypes', array('erp_housingtype_id', name()), FALSE, FALSE, TRUE);
        $data['erp_hotelroomsizes'] = ddgen('erp_hotelroomsizes', array('erp_hotelroomsize_id', name()), FALSE, FALSE, TRUE);
        $data["uo_contracts"] = $this->create_contracts_array();

        //$max_order_no = $this->erp_hotels_reservation_orders_model->getMaxId();
        $data['current_order_no'] = $id;

        $this->load->library("form_validation");

        $this->form_validation->set_rules('erp_company_id', 'lang:company', 'trim|required');
        $this->form_validation->set_rules('order_date', 'lang:order_date', 'trim|required');

        $data['item_nationalities'] = array();
        $this->erp_hotels_reservation_orders_model->nationalities_erp_hotels_reservation_orders_id = $id;
        $item_nationalities = $this->erp_hotels_reservation_orders_model->getNationalities();
        foreach ($item_nationalities as $item_nationality) {
            $data['item_nationalities'][] = $item_nationality->erp_nationalities_id;
        }

        $data['item_advantages'] = array();
        $this->erp_hotels_reservation_orders_model->advantages_erp_hotels_reservation_orders_id = $id;
        $item_advantages = $this->erp_hotels_reservation_orders_model->getHotelAdvantages();
        foreach ($item_advantages as $item_advantage) {
            $data['item_advantages'][] = $item_advantage->erp_hotel_advantages_id;
        }

        $data['item_room_services'] = array();
        $this->erp_hotels_reservation_orders_model->room_services_erp_hotels_reservation_orders_id = $id;
        $item_room_services = $this->erp_hotels_reservation_orders_model->getRoomServices();
        foreach ($item_room_services as $item_room_service) {
            $data['item_room_services'][] = $item_room_service->erp_room_services_id;
        }


        $this->erp_hotels_reservation_orders_model->rooms_erp_hotels_reservation_orders_id = $id;
        $item_rooms = $this->erp_hotels_reservation_orders_model->getRooms();
        $data['item_rooms'] = $item_rooms;

        $this->erp_hotels_reservation_orders_model->meals_erp_hotels_reservation_orders_id = $id;
        $item_meals = $this->erp_hotels_reservation_orders_model->getMeals();
        $data['item_meals'] = $item_meals;





        if ($this->form_validation->run() == false) {
            $this->load->view("hotels_reservation_orders/edit", $data);
        } else {
            if (session('uo_id')) {
                $this->erp_hotels_reservation_orders_model->owner_erp_company_types_id = 2;
                $this->erp_hotels_reservation_orders_model->owner_erp_company_id = session('uo_id');
            } else if (session('ea_id')) {
                $this->erp_hotels_reservation_orders_model->owner_erp_company_types_id = 3;
                $this->erp_hotels_reservation_orders_model->owner_erp_company_id = session('ea_id');
            } else if (session('hm_id')) {
                $this->erp_hotels_reservation_orders_model->owner_erp_company_types_id = 5;
                $this->erp_hotels_reservation_orders_model->owner_erp_company_id = session('hm_id');
            }
            if ($this->input->post('company_type')) {
                $this->erp_hotels_reservation_orders_model->erp_company_types_id = $this->input->post('company_type');
            } else {
                $this->erp_hotels_reservation_orders_model->erp_company_types_id = 2;
            }

            $this->erp_hotels_reservation_orders_model->erp_company_id = $this->input->post('erp_company_id');
            $this->erp_hotels_reservation_orders_model->the_date = date('Y-m-d H:i', time());
            $this->erp_hotels_reservation_orders_model->order_date = $this->input->post('order_date');
            $this->erp_hotels_reservation_orders_model->arrival_date = $this->input->post('arrival_date');
            $this->erp_hotels_reservation_orders_model->departure_date = $this->input->post('departure_date');
            $this->erp_hotels_reservation_orders_model->contact_person = $this->input->post('contact_person');
            $this->erp_hotels_reservation_orders_model->order_status = 3;
            $this->erp_hotels_reservation_orders_model->refused_reason = '';

            if (isset($_POST['look_to_haram'])) {
                $this->erp_hotels_reservation_orders_model->look_to_haram = 1;
            } else {
                $this->erp_hotels_reservation_orders_model->look_to_haram = 0;
            }

            if (isset($_POST['safa_uo_contract_id'])) {
                $this->erp_hotels_reservation_orders_model->safa_uo_contract_id = $this->input->post('safa_uo_contract_id');
            }

            /*
              $this->erp_hotels_reservation_orders_model->price_from = $this->input->post('price_from');
              $this->erp_hotels_reservation_orders_model->price_to = $this->input->post('price_to');
              $this->erp_hotels_reservation_orders_model->erp_currencies_id = $this->input->post('erp_currencies_id');
             */

            // Save Order
            $this->erp_hotels_reservation_orders_model->save();

            // Save nationalities
            $nationalities = $this->input->post('nationalities');
            $this->erp_hotels_reservation_orders_model->nationalities_erp_hotels_reservation_orders_id = $id;
            $this->erp_hotels_reservation_orders_model->deleteNationalities();
            if (ensure($nationalities)) {
                foreach ($nationalities as $nationality_id) {
                    $this->erp_hotels_reservation_orders_model->nationalities_erp_hotels_reservation_orders_id = $id;
                    $this->erp_hotels_reservation_orders_model->nationalities_erp_nationalities_id = $nationality_id;
                    $this->erp_hotels_reservation_orders_model->saveNationalities();
                }
            }

            // Save hotel_advantages
            $advantages = $this->input->post('advantages');
            $this->erp_hotels_reservation_orders_model->advantages_erp_hotels_reservation_orders_id = $id;
            $this->erp_hotels_reservation_orders_model->deleteHotelAdvantages();
            if (ensure($advantages)) {
                foreach ($advantages as $advantage_id) {
                    $this->erp_hotels_reservation_orders_model->advantages_erp_hotels_reservation_orders_id = $id;
                    $this->erp_hotels_reservation_orders_model->advantages_erp_hotel_advantages_id = $advantage_id;
                    $this->erp_hotels_reservation_orders_model->saveHotelAdvantages();
                }
            }
            // Save room_services
            $room_services = $this->input->post('room_services');
            $this->erp_hotels_reservation_orders_model->room_services_erp_hotels_reservation_orders_id = $id;
            $this->erp_hotels_reservation_orders_model->deleteRoomServices();

            if (ensure($room_services)) {
                foreach ($room_services as $room_service_id) {
                    $this->erp_hotels_reservation_orders_model->room_services_erp_hotels_reservation_orders_id = $id;
                    $this->erp_hotels_reservation_orders_model->room_services_erp_room_services_id = $room_service_id;
                    $this->erp_hotels_reservation_orders_model->saveRoomServices();
                }
            }


            // Save meals
            $this->erp_hotels_reservation_orders_model->meals_erp_hotels_reservation_orders_id = $id;
            $meals_erp_meals = $this->input->post('meals_erp_meals');
            $meals_meals_count = $this->input->post('meals_meals_count');
            $meals_erp_hotel_id = $this->input->post('meals_erp_hotel_id');
            if (ensure($meals_erp_meals))
                foreach ($meals_erp_meals as $key => $value) {
                    $this->erp_hotels_reservation_orders_model->meals_erp_hotels_reservation_orders_id = $id;
                    $this->erp_hotels_reservation_orders_model->meals_erp_meals_id = $meals_erp_meals[$key];
                    $this->erp_hotels_reservation_orders_model->meals_meals_count = $meals_meals_count[$key];
                    $this->erp_hotels_reservation_orders_model->meals_erp_hotel_id = $meals_erp_hotel_id[$key];

                    $this->erp_hotels_reservation_orders_model->meals_erp_hotels_reservation_orders_meals_id = $key;
                    $current_meal_row_count = $this->erp_hotels_reservation_orders_model->getMeals(true);
                    if ($current_meal_row_count == 0) {
                        $this->erp_hotels_reservation_orders_model->meals_erp_hotels_reservation_orders_meals_id = false;
                        $erp_hotels_reservation_orders_meals_id = $this->erp_hotels_reservation_orders_model->saveMeals();
                    } else {
                        $erp_hotels_reservation_orders_meals_id = $key;
                        $this->erp_hotels_reservation_orders_model->saveMeals();
                    }
                }
            // --- Delete Deleted meals rows from Database --------------
            //By Gouda
            $this->erp_hotels_reservation_orders_model->meals_erp_meals_id = false;
            $this->erp_hotels_reservation_orders_model->meals_meals_count = false;

            $meals_remove = $this->input->post('meals_remove');
            if (ensure($meals_remove)) {
                foreach ($meals_remove as $meal_remove) {
                    $this->erp_hotels_reservation_orders_model->meals_erp_hotels_reservation_orders_meals_id = $meal_remove;
                    $this->erp_hotels_reservation_orders_model->deleteMeals();
                }
            }
            //---------------------------------------------------------------
            // Save rooms
            $this->erp_hotels_reservation_orders_model->rooms_erp_hotels_reservation_orders_id = $id;
            $rooms_housingtypes = $this->input->post('rooms_erp_housingtypes_id');
            $rooms_hotelroomsizes = $this->input->post('rooms_erp_hotelroomsizes_id');
            $rooms_city_id = $this->input->post('rooms_erp_cities_id');
            $rooms_hotels_id = $this->input->post('rooms_erp_hotels_id');
            $rooms_rooms_count = $this->input->post('rooms_rooms_count');
            $rooms_nights_count = $this->input->post('rooms_nights_count');
            $rooms_entry_date = $this->input->post('rooms_entry_date');
            $rooms_exit_date = $this->input->post('rooms_exit_date');
            $rooms_erp_meals_id = $this->input->post('rooms_erp_meals_id');
            $rooms_notes = $this->input->post('rooms_notes');

            //            $rooms_price_from = $this->input->post('rooms_price_from');
            //            $rooms_price_to = $this->input->post('rooms_price_to');
            //            $rooms_erp_currencies_id = $this->input->post('rooms_erp_currencies_id');

            $hotel_name = '';

            if (ensure($rooms_housingtypes))
                foreach ($rooms_housingtypes as $key => $value) {
                    $this->erp_hotels_reservation_orders_model->rooms_erp_hotels_reservation_orders_id = $id;

                    $this->erp_hotels_reservation_orders_model->rooms_erp_cities_id = $rooms_city_id[$key];
                    $this->erp_hotels_reservation_orders_model->rooms_erp_hotels_id = $rooms_hotels_id[$key];
                    $this->erp_hotels_reservation_orders_model->rooms_erp_housingtypes_id = $rooms_housingtypes[$key];
                    $this->erp_hotels_reservation_orders_model->rooms_erp_hotelroomsizes_id = $rooms_hotelroomsizes[$key];
                    $this->erp_hotels_reservation_orders_model->rooms_rooms_count = $rooms_rooms_count[$key];
                    $this->erp_hotels_reservation_orders_model->rooms_nights_count = $rooms_nights_count[$key];
                    $this->erp_hotels_reservation_orders_model->rooms_entry_date = $rooms_entry_date[$key];
                    $this->erp_hotels_reservation_orders_model->rooms_exit_date = $rooms_exit_date[$key];
                    $this->erp_hotels_reservation_orders_model->rooms_erp_meals_id = $rooms_erp_meals_id[$key];

                    //                    $this->erp_hotels_reservation_orders_model->rooms_price_from = $rooms_price_from[$key];
                    //                    $this->erp_hotels_reservation_orders_model->rooms_price_to = $rooms_price_to[$key];
                    //                    $this->erp_hotels_reservation_orders_model->rooms_erp_currencies_id = $rooms_erp_currencies_id[$key];

                    $this->erp_hotels_reservation_orders_model->rooms_erp_hotels_reservation_orders_rooms_id = $key;
                    $current_room_row_count = $this->erp_hotels_reservation_orders_model->getRooms(true);
                    if ($current_room_row_count == 0) {
                        $this->erp_hotels_reservation_orders_model->rooms_erp_hotels_reservation_orders_rooms_id = false;
                        $erp_hotels_reservation_orders_rooms_id = $this->erp_hotels_reservation_orders_model->saveRooms();
                    } else {
                        $erp_hotels_reservation_orders_rooms_id = $key;
                        $this->erp_hotels_reservation_orders_model->saveRooms();
                    }

                    //-------------- For Notification --------------------------------------------
                    $this->hotels_model->erp_hotel_id = $rooms_hotels_id[$key];
                    $this->hotels_model->join = true;
                    $erp_hotels_row = $this->hotels_model->get();
                    if (count($erp_hotels_row) > 0) {
                        $hotel_name = $hotel_name . ',' . $erp_hotels_row->hotel_name;
                    }
                    //----------------------------------------------------------------------------
                }
            // --- Delete Deleted rooms rows from Database --------------
            // By Gouda
            $this->erp_hotels_reservation_orders_model->rooms_erp_housingtypes_id = false;
            $this->erp_hotels_reservation_orders_model->rooms_erp_hotelroomsizes_id = false;
            $this->erp_hotels_reservation_orders_model->rooms_rooms_count = false;
            $this->erp_hotels_reservation_orders_model->rooms_nights_count = false;
            $this->erp_hotels_reservation_orders_model->rooms_entry_date = false;
            $this->erp_hotels_reservation_orders_model->rooms_exit_date = false;
            $this->erp_hotels_reservation_orders_model->rooms_erp_meals_id = false;

            $this->erp_hotels_reservation_orders_model->rooms_price_from = false;
            $this->erp_hotels_reservation_orders_model->rooms_price_to = false;
            $this->erp_hotels_reservation_orders_model->rooms_erp_currencies_id = false;

            $rooms_remove = $this->input->post('rooms_remove');
            if (ensure($rooms_remove)) {
                foreach ($rooms_remove as $room_remove) {
                    $this->erp_hotels_reservation_orders_model->rooms_erp_hotels_reservation_orders_rooms_id = $room_remove;
                    $this->erp_hotels_reservation_orders_model->deleteRooms();
                }
            }
            //---------------------------------------------------------------
            // Edit Log Copy. For Reservation Order.
            //--------------------------------------------------
            $this->edit_log($id);
            //--------------------------------------------------
            //----------- Send Notification For Company ------------------------------
            $msg_datetime = date('Y-m-d H:i', time());

            $this->notification_model->notification_type = 'automatic';
            $this->notification_model->erp_importance_id = 1;
            $this->notification_model->sender_type_id = 1;
            $this->notification_model->sender_id = 0;

            $this->notification_model->erp_system_events_id = 17;

            $this->notification_model->language_id = 2;
            $this->notification_model->msg_datetime = $msg_datetime;

            /*
              $current_phase_name=$this->input->post('current_phase');
              $next_phase_id=$this->input->post('next_phase');
              $this->safa_contract_phases_model->safa_contract_phases_id=$next_phase_id;
              $next_phase_row=$this->safa_contract_phases_model->get();
              $next_phase_name=$next_phase_row->{name()};

              $tags="#*current_contract_phase#*=$current_phase_name*****#*next_contract_phase#*=$next_phase_name";
             */


            if (session('uo_id')) {
                $hotel_reservation_order_owner = session('uo_name');
            } else if (session('ea_id')) {
                $hotel_reservation_order_owner = session('ea_name');
            } else if (session('hm_id')) {
                $hotel_reservation_order_owner = session('hm_name');
            }

            //Link to replace.
            $link_hotels_reservation_orders = "<a href='" . site_url('hotels_reservation_orders/view/' . $id) . "'  target='_blank'> " . $id . " </a>";

            $tags = "#*the_date#*=" . date('Y-m-d H:i', time()) . "*****#*hotel_reservation_order_no#*=$link_hotels_reservation_orders*****#*hotel_reservation_order_owner#*=$hotel_reservation_order_owner*****#*hotel_name#*=$hotel_name";

            $this->notification_model->tags = $tags;
            $this->notification_model->parent_id = '';

            $erp_notification_id = $this->notification_model->save();

            //-------- Notification Details ---------------
            $this->notification_model->detail_erp_notification_id = $erp_notification_id;
            if ($this->input->post('company_type')) {
                $this->notification_model->detail_receiver_type = $this->input->post('company_type');
            } else {
                $this->notification_model->detail_receiver_type = 2;
            }
            $this->notification_model->detail_receiver_id = $this->input->post('erp_company_id');
            $this->notification_model->saveDetails();
            //--------------------------------
            //-------------------------------------------------------------------


            $this->load->view('redirect_message', array('msg' => lang('global_updated_message'),
                'url' => site_url('hotels_reservation_orders'),
                'id' => $this->uri->segment("4"),
                'model_title' => lang('title'), 'action' => lang('edit_title')));
        }
    }

    public function view($id = FALSE) {
        $data = array();
        if (!$id)
            show_404();
        //        $this->erp_hotels_reservation_orders_model->order_status = 0;
        $this->erp_hotels_reservation_orders_model->erp_company_id = $this->{$this->destination . '_id'};
        $this->erp_hotels_reservation_orders_model->erp_company_types_id = $this->system_id;
        $this->erp_hotels_reservation_orders_model->erp_hotels_reservation_orders_id = $id;
        $item = $this->erp_hotels_reservation_orders_model->get();
        $data['item'] = $item;
        if (!$data['item'])
            show_404();

        $data['nationalities_rows'] = $this->erp_nationalities_model->get();
        $data['nationalities'] = ddgen('erp_nationalities', array('erp_nationality_id', name()), FALSE, FALSE, TRUE);
        $data['room_services'] = ddgen('erp_room_services', array('erp_room_service_id', name()), FALSE, FALSE, TRUE);
        $data['advantages'] = ddgen('erp_hotel_advantages', array('erp_hotel_advantages_id', name()), FALSE, FALSE, TRUE);
        if (session('ea_id')) {
            $this->contracts_model->safa_ea_id = session('ea_id');
            $data['contract_row'] = $this->contracts_model->search();
        }

        if ($item->owner_erp_company_types_id == 3) {
            $data['companies'] = ddgen('safa_eas', array('safa_ea_id', name()), FALSE, FALSE, TRUE);
        } else if ($item->owner_erp_company_types_id == 5) {
            $data['companies'] = ddgen('safa_hms', array('safa_hm_id', name()), FALSE, FALSE, TRUE);
        } else {
            $data['companies'] = ddgen('safa_uos', array('safa_uo_id', name()), FALSE, FALSE, TRUE);
        }
        $data['erp_cities'] = ddgen('erp_cities', array('erp_city_id', name()), FALSE, FALSE, TRUE);
        $data['erp_hotels'] = ddgen('erp_hotels', array('erp_hotel_id', name()), FALSE, FALSE, TRUE);
        $data['erp_currencies'] = ddgen('erp_currencies', array('erp_currency_id', name()), FALSE, FALSE, TRUE);
        $data['erp_meals'] = ddgen('erp_meals', array('erp_meal_id', name()), FALSE, FALSE, TRUE);
        $data['erp_housingtypes'] = ddgen('erp_housingtypes', array('erp_housingtype_id', name()), FALSE, FALSE, TRUE);
        $data['erp_hotelroomsizes'] = ddgen('erp_hotelroomsizes', array('erp_hotelroomsize_id', name()), FALSE, FALSE, TRUE);
        $data["uo_contracts"] = $this->create_contracts_array();
        //$max_order_no = $this->erp_hotels_reservation_orders_model->getMaxId();
        $data['current_order_no'] = $id;
        $this->load->library("form_validation");
        $this->form_validation->set_rules('erp_company_id', 'lang:company', 'trim|required');
        $this->form_validation->set_rules('order_date', 'lang:order_date', 'trim|required');
        //$this->form_validation->set_rules('erp_cities_id', 'lang:city', 'trim|required');
        //$this->form_validation->set_rules('erp_hotels_id', 'lang:hotel', 'trim|required');
        $this->form_validation->set_rules('arrival_date', 'lang:arrival_date', 'trim|required');
        $this->form_validation->set_rules('departure_date', 'lang:departure_date', 'trim|required');
        //$this->form_validation->set_rules('nationalities[]', 'lang:nationalities', 'trim|required');
        $data['item_nationalities'] = array();
        $this->erp_hotels_reservation_orders_model->nationalities_erp_hotels_reservation_orders_id = $id;
        $item_nationalities = $this->erp_hotels_reservation_orders_model->getNationalities();
        foreach ($item_nationalities as $item_nationality) {
            $data['item_nationalities'][] = $item_nationality->erp_nationalities_id;
        }
        $data['item_advantages'] = array();
        $this->erp_hotels_reservation_orders_model->advantages_erp_hotels_reservation_orders_id = $id;
        $item_advantages = $this->erp_hotels_reservation_orders_model->getHotelAdvantages();
        foreach ($item_advantages as $item_advantage) {
            $data['item_advantages'][] = $item_advantage->erp_hotel_advantages_id;
        }

        $data['item_room_services'] = array();
        $this->erp_hotels_reservation_orders_model->room_services_erp_hotels_reservation_orders_id = $id;
        $item_room_services = $this->erp_hotels_reservation_orders_model->getRoomServices();
        foreach ($item_room_services as $item_room_service) {
            $data['item_room_services'][] = $item_room_service->erp_room_services_id;
        }

        $this->erp_hotels_reservation_orders_model->rooms_erp_hotels_reservation_orders_id = $id;
        $item_rooms = $this->erp_hotels_reservation_orders_model->getRooms();
        $data['item_rooms'] = $item_rooms;



        $this->erp_hotels_reservation_orders_model->meals_erp_hotels_reservation_orders_id = $id;
        $item_meals = $this->erp_hotels_reservation_orders_model->getMeals();
        $data['item_meals'] = $item_meals;

        if ($this->form_validation->run() == false) {
            $this->load->view("hotels_reservation_orders/view", $data);
        } else {
            $this->db->trans_start();
            // TODO: REMOVE THIS COMMENT
            //            if ($item->order_status != '0')
            //                show_404();

            $this->confirm_availability_deduction($id);
            $this->erp_hotels_reservation_orders_model->erp_hotels_reservation_orders_id = $id;
            $this->erp_hotels_reservation_orders_model->order_status = 1;
            $this->erp_hotels_reservation_orders_model->save();

            /**
             * NOTIFICATION
             */
            $this->notification_model->notification_type = 'automatic';
            $this->notification_model->erp_importance_id = 1;
            $this->notification_model->sender_type_id = 1;
            $this->notification_model->sender_id = 0;
            $this->notification_model->erp_system_events_id = 21;
            $this->notification_model->language_id = 2;
            $this->notification_model->msg_datetime = date('Y-m-d H:i', time());
            $this->hotels_model->erp_hotel_id = $this->input->post('erp_hotels_id');
            $this->hotels_model->join = true;
            $erp_hotels_row = $this->hotels_model->get();
            if (count($erp_hotels_row) > 0) {
                $hotel_name = $erp_hotels_row->hotel_name;
            }

            if (session('uo_id')) {
                $hotel_reservation_order_owner = session('uo_name');
            } else if (session('ea_id')) {
                $hotel_reservation_order_owner = session('ea_name');
            } else if (session('hm_id')) {
                $hotel_reservation_order_owner = session('hm_name');
            }

            $this->db->trans_complete();
            //-------------- For Notification --------------------------------------------
            $hotel_name = '';
            foreach ($item_rooms as $item_room) {
                $this->hotels_model->erp_hotel_id = $item_room->erp_hotels_id;
                $this->hotels_model->join = true;
                $erp_hotels_row = $this->hotels_model->get();
                if (count($erp_hotels_row) > 0) {
                    $hotel_name = $hotel_name . ',' . $erp_hotels_row->hotel_name;
                }
            }
            //----------------------------------------------------------------------------
            //Link to replace.
            $link_hotels_reservation_orders = "<a href='" . site_url('hotels_reservation_orders/view_owner/' . $id) . "'  target='_blank'> " . $id . " </a>";
            $tags = "#*the_date#*=" . date('Y-m-d H:i', time()) . "*****#*hotel_reservation_order_no#*=$link_hotels_reservation_orders*****#*hotel_reservation_order_owner#*=$hotel_reservation_order_owner*****#*hotel_name#*=$hotel_name";

            $this->notification_model->tags = $tags;
            $this->notification_model->parent_id = '';
            $erp_notification_id = $this->notification_model->save();
            //-------- Notification Details ---------------
            $this->notification_model->detail_erp_notification_id = $erp_notification_id;
            $this->notification_model->detail_receiver_type = $data['item']->owner_erp_company_types_id;
            $this->notification_model->detail_receiver_id = $data['item']->owner_erp_company_id;
            $this->notification_model->saveDetails();


            $this->load->view('redirect_message', array('msg' => lang('confirmed_successfully'),
                'url' => site_url('hotels_reservation_orders'),
                'id' => $this->uri->segment("4"),
                'model_title' => lang('title'), 'action' => lang('edit_title')));
        }
    }

    public function view_owner($id = FALSE) {
        $data = array();
        if (!$id)
            show_404();
        //        $this->erp_hotels_reservation_orders_model->order_status = 0;
        $this->erp_hotels_reservation_orders_model->owner_erp_company_id = $this->{$this->destination . '_id'};
        $this->erp_hotels_reservation_orders_model->owner_erp_company_types_id = $this->system_id;
        $this->erp_hotels_reservation_orders_model->erp_hotels_reservation_orders_id = $id;
        $item = $this->erp_hotels_reservation_orders_model->get();
        $data['item'] = $item;
        if (!$data['item'])
            show_404();

        $data['nationalities_rows'] = $this->erp_nationalities_model->get();
        $data['nationalities'] = ddgen('erp_nationalities', array('erp_nationality_id', name()), FALSE, FALSE, TRUE);
        $data['room_services'] = ddgen('erp_room_services', array('erp_room_service_id', name()), FALSE, FALSE, TRUE);
        $data['advantages'] = ddgen('erp_hotel_advantages', array('erp_hotel_advantages_id', name()), FALSE, FALSE, TRUE);
        if (session('ea_id')) {
            $this->contracts_model->safa_ea_id = session('ea_id');
            $data['contract_row'] = $this->contracts_model->search();
        }

        if ($item->owner_erp_company_types_id == 3) {
            $data['companies'] = ddgen('safa_eas', array('safa_ea_id', name()), FALSE, FALSE, TRUE);
        } else if ($item->owner_erp_company_types_id == 5) {
            $data['companies'] = ddgen('safa_hms', array('safa_hm_id', name()), FALSE, FALSE, TRUE);
        } else {
            $data['companies'] = ddgen('safa_uos', array('safa_uo_id', name()), FALSE, FALSE, TRUE);
        }
        $data['erp_cities'] = ddgen('erp_cities', array('erp_city_id', name()), FALSE, FALSE, TRUE);
        $data['erp_hotels'] = ddgen('erp_hotels', array('erp_hotel_id', name()), FALSE, FALSE, TRUE);
        $data['erp_currencies'] = ddgen('erp_currencies', array('erp_currency_id', name()), FALSE, FALSE, TRUE);
        $data['erp_meals'] = ddgen('erp_meals', array('erp_meal_id', name()), FALSE, FALSE, TRUE);
        $data['erp_housingtypes'] = ddgen('erp_housingtypes', array('erp_housingtype_id', name()), FALSE, FALSE, TRUE);
        $data['erp_hotelroomsizes'] = ddgen('erp_hotelroomsizes', array('erp_hotelroomsize_id', name()), FALSE, FALSE, TRUE);
        $data["uo_contracts"] = $this->create_contracts_array();
        //$max_order_no = $this->erp_hotels_reservation_orders_model->getMaxId();
        $data['current_order_no'] = $id;
        $this->load->library("form_validation");
        $this->form_validation->set_rules('erp_company_id', 'lang:company', 'trim|required');
        $this->form_validation->set_rules('order_date', 'lang:order_date', 'trim|required');
        //$this->form_validation->set_rules('erp_cities_id', 'lang:city', 'trim|required');
        //$this->form_validation->set_rules('erp_hotels_id', 'lang:hotel', 'trim|required');
        $this->form_validation->set_rules('arrival_date', 'lang:arrival_date', 'trim|required');
        $this->form_validation->set_rules('departure_date', 'lang:departure_date', 'trim|required');
        //$this->form_validation->set_rules('nationalities[]', 'lang:nationalities', 'trim|required');
        $data['item_nationalities'] = array();
        $this->erp_hotels_reservation_orders_model->nationalities_erp_hotels_reservation_orders_id = $id;
        $item_nationalities = $this->erp_hotels_reservation_orders_model->getNationalities();
        foreach ($item_nationalities as $item_nationality) {
            $data['item_nationalities'][] = $item_nationality->erp_nationalities_id;
        }
        $data['item_advantages'] = array();
        $this->erp_hotels_reservation_orders_model->advantages_erp_hotels_reservation_orders_id = $id;
        $item_advantages = $this->erp_hotels_reservation_orders_model->getHotelAdvantages();
        foreach ($item_advantages as $item_advantage) {
            $data['item_advantages'][] = $item_advantage->erp_hotel_advantages_id;
        }

        $data['item_room_services'] = array();
        $this->erp_hotels_reservation_orders_model->room_services_erp_hotels_reservation_orders_id = $id;
        $item_room_services = $this->erp_hotels_reservation_orders_model->getRoomServices();
        foreach ($item_room_services as $item_room_service) {
            $data['item_room_services'][] = $item_room_service->erp_room_services_id;
        }

        $this->erp_hotels_reservation_orders_model->rooms_erp_hotels_reservation_orders_id = $id;
        $item_rooms = $this->erp_hotels_reservation_orders_model->getRooms();
        $data['item_rooms'] = $item_rooms;



        $this->erp_hotels_reservation_orders_model->meals_erp_hotels_reservation_orders_id = $id;
        $item_meals = $this->erp_hotels_reservation_orders_model->getMeals();
        $data['item_meals'] = $item_meals;

        if ($this->form_validation->run() == false) {
            $this->load->view("hotels_reservation_orders/view_owner", $data);
        } else {

            // TODO: REMOVE THIS COMMENT
            //            if ($item->order_status != '0')
            //                show_404();

            $this->confirm_availability_deduction($id);
            $this->erp_hotels_reservation_orders_model->erp_hotels_reservation_orders_id = $id;
            $this->erp_hotels_reservation_orders_model->order_status = 1;
            $this->erp_hotels_reservation_orders_model->save();

            /**
             * NOTIFICATION
             */
            $this->notification_model->notification_type = 'automatic';
            $this->notification_model->erp_importance_id = 1;
            $this->notification_model->sender_type_id = 1;
            $this->notification_model->sender_id = 0;
            $this->notification_model->erp_system_events_id = 21;
            $this->notification_model->language_id = 2;
            $this->notification_model->msg_datetime = date('Y-m-d H:i', time());
            $this->hotels_model->erp_hotel_id = $this->input->post('erp_hotels_id');
            $this->hotels_model->join = true;
            $erp_hotels_row = $this->hotels_model->get();
            if (count($erp_hotels_row) > 0) {
                $hotel_name = $erp_hotels_row->hotel_name;
            }

            if (session('uo_id')) {
                $hotel_reservation_order_owner = session('uo_name');
            } else if (session('ea_id')) {
                $hotel_reservation_order_owner = session('ea_name');
            }


            //-------------- For Notification --------------------------------------------
            $hotel_name = '';
            foreach ($item_rooms as $item_room) {
                $this->hotels_model->erp_hotel_id = $item_room->erp_hotels_id;
                $this->hotels_model->join = true;
                $erp_hotels_row = $this->hotels_model->get();
                if (count($erp_hotels_row) > 0) {
                    $hotel_name = $hotel_name . ',' . $erp_hotels_row->hotel_name;
                }
            }
            //----------------------------------------------------------------------------
            //Link to replace.
            $link_hotels_reservation_orders = "<a href='" . site_url('hotels_reservation_orders/view/' . $id) . "'  target='_blank'> " . $id . " </a>";
            $tags = "#*the_date#*=" . date('Y-m-d H:i', time()) . "*****#*hotel_reservation_order_no#*=$link_hotels_reservation_orders*****#*hotel_reservation_order_owner#*=$hotel_reservation_order_owner*****#*hotel_name#*=$hotel_name";

            $this->notification_model->tags = $tags;
            $this->notification_model->parent_id = '';
            $erp_notification_id = $this->notification_model->save();
            //-------- Notification Details ---------------
            $this->notification_model->detail_erp_notification_id = $erp_notification_id;
            $this->notification_model->detail_receiver_type = $data['item']->owner_erp_company_types_id;
            $this->notification_model->detail_receiver_id = $data['item']->owner_erp_company_id;
            $this->notification_model->saveDetails();

            $this->load->view('redirect_message', array('msg' => lang('confirmed_successfully'),
                'url' => site_url('hotels_reservation_orders'),
                'id' => $this->uri->segment("4"),
                'model_title' => lang('title'), 'action' => lang('edit_title')));
        }
    }

    public function rooms_details_popup($id = FALSE) {
        $data = array();

        if (!$id) {
            show_404();
        }
        $this->layout = 'js';

        $this->erp_hotels_reservation_orders_model->rooms_erp_hotels_reservation_orders_id = $id;
        $item_rooms = $this->erp_hotels_reservation_orders_model->getRooms();
        $data['items'] = $item_rooms;

        $this->load->view("hotels_reservation_orders/rooms_details_popup", $data);
    }

    public function available_rooms_popup($id = FALSE, $master_erp_hotelroomsizes_id = FALSE) {
        $data = array();

        if (!$id) {
            show_404();
        }
        $this->layout = 'js';


        $this->erp_hotels_reservation_orders_model->erp_hotels_reservation_orders_id = $id;
        $this->erp_hotels_reservation_orders_model->master_erp_hotelroomsizes_id = $master_erp_hotelroomsizes_id;
        $data["item"] = $this->erp_hotels_reservation_orders_model->get();

        //echo $this->erp_hotels_reservation_orders_model->db->last_query(); exit;

        $this->load->view("hotels_reservation_orders/available_rooms_popup", $data);
    }

    public function getCompaniesByCompanyType() {
        $erp_company_type_id = $this->input->post('erp_company_type_id');

        $structure = array('id', name());
        $key = $structure['0'];
        if (isset($structure['1'])) {
            $value = $structure['1'];
        }
        $companies_arr = array();
        $companies = $this->erp_hotels_reservation_orders_model->getCompaniesByCompanyType($erp_company_type_id);
        foreach ($companies as $company) {
            $companies_arr[$company->$key] = $company->$value;
        }


        $form_dropdown = form_dropdown('erp_company_id', $companies_arr, set_value('erp_company_id', ''), 'class="chosen-select chosen-rtl input-full"  tabindex="4" ');
        $form_dropdown = str_replace('<select name="erp_company_id" class="chosen-select chosen-rtl input-full" multiple tabindex="4" >', '', $form_dropdown);
        $form_dropdown = str_replace('</select>', '', $form_dropdown);
        echo $form_dropdown;
        exit;
    }

    public function getHotelsByCities($packageid = false) {
        $erp_city_id = $this->input->post('erp_cities_id');

        $structure = array('erp_hotel_id', name());
        $key = $structure['0'];
        if (isset($structure['1'])) {
            $value = $structure['1'];
        }
        $hotels_arr = array();
        $this->hotels_model->erp_city_id = $erp_city_id;
        $hotels = $this->hotels_model->get();
        $hotelid = FALSE;
        if ($packageid) {
            $this->load->model('safa_packages_model');
            $p_hotels = $this->safa_packages_model->get_package_hotels($packageid);
            foreach ($p_hotels as $htl) {
                if ($htl->erp_city_id == $erp_city_id)
                    $hotelid = $htl->erp_hotel_id;
            }
        }

        foreach ($hotels as $hotel) {
            if ($hotelid) {
                if ($hotelid == $hotel->$key)
                    $hotels_arr[$hotel->$key] = $hotel->$value;
            }else {
                $hotels_arr[$hotel->$key] = $hotel->$value;
            }
        }


        $form_dropdown = form_dropdown('erp_hotels_id', $hotels_arr, set_value('erp_hotels_id', ''), 'class="chosen-select chosen-rtl input-full"  tabindex="4" ');
        $form_dropdown = str_replace('<select name="erp_hotels_id" class="chosen-select chosen-rtl input-full"  tabindex="4" id="erp_hotels_id" >', '', $form_dropdown);
        $form_dropdown = str_replace('</select>', '', $form_dropdown);
        echo $form_dropdown;
        exit;
    }

    function delete($id = false) {
        if (!$id) {
            show_404();
        }

        $this->erp_hotels_reservation_orders_model->erp_hotels_reservation_orders_id = $id;
        $item = $this->erp_hotels_reservation_orders_model->get();
        if ($item->order_status == 1) {
            session('error_message', lang('you_cannot_edit_or_delete_order_after_confirmation'));
            session('error_url', site_url('hotels_reservation_orders'));
            session('error_title', lang('title'));
            session('error_action', lang('delete_title'));
            redirect("error_message/show");
        }

        if (!$this->erp_hotels_reservation_orders_model->delete())
            show_404();
        $this->load->view('redirect_message', array('msg' => lang('global_deleted_message'),
            'url' => site_url('hotels_reservation_orders'),
            'model_title' => lang('title'), 'action' => lang('delete_title')));
    }

    function delete_all() {
        if ($this->input->post("delete_items")) {
            foreach ($this->input->post("delete_items") as $item) {
                $this->erp_hotels_reservation_orders_model->erp_hotels_reservation_orders_id = $item;

                $this->erp_hotels_reservation_orders_model->delete();
            }
        } else {
            show_404();
        }
        redirect("hotels_reservation_orders");
    }

    function create_contracts_array() {
        $this->safa_umrahgroups_model->ea_id = session('ea_id');
        $result = $this->safa_umrahgroups_model->get_ea_contracts();
//                echo $this->db->last_query();die();
        $contracts = array();

        //By Gouda.
        //$contracts[""] = lang('global_select_from_menu');
        foreach ($result as $con_id => $contract) {
            $contracts[$contract->contract_id] = $contract->safa_uo_contracts_eas_name;
        }
        return $contracts;
    }

    public function check_availability($ajax = TRUE) {
        $this->layout = 'ajax';
        $result = array();
        foreach (post('rooms_rooms_count') as $key => $value) {
            foreach ($this->post_sub_criteria as $sub_key => $sub_value) {
                $var = post($sub_value);
                if ($var[$key])
                    $this->hotel_availability_search_model->$sub_key = $var[$key];
            }

            $available_count = 0;
            $res = $this->hotel_availability_search_model->get();
            $romcount = 0;
            foreach ($res as $av) {
                $romcount += $av->available_count;
            }
            if ($romcount)
                $result[$key] = $romcount - $value;
            else
                $result[$key] = - $value;
        }
        echo json_encode($result);
    }

    public function cancel($id = FALSE) {
        if (!$id)
            show_404();
        $this->erp_hotels_reservation_orders_model->erp_company_id = $this->{$this->destination . '_id'};
        $this->erp_hotels_reservation_orders_model->erp_company_types_id = $this->system_id;
        $this->erp_hotels_reservation_orders_model->erp_hotels_reservation_orders_id = $id;
        $item = $this->erp_hotels_reservation_orders_model->get();
        $data['item'] = $item;
        if (!$data['item'])
            show_404();
        $this->db->where('erp_hotels_reservation_orders_id', $id);
        $this->db->update('erp_hotels_reservation_orders', array(
            'order_status' => 2,
            'refused_reason' => post('reason')
        ));

        $this->db->where('erp_hotels_reservation_orders_id', $id);
        $this->db->update('erp_hotels_reservation_orders_log', array(
            'order_status' => 2,
            'refused_reason' => post('reason')
        ));

        /**
         * NOTIFICATION
         */
        $this->notification_model->notification_type = 'automatic';
        $this->notification_model->erp_importance_id = 1;
        $this->notification_model->sender_type_id = 1;
        $this->notification_model->sender_id = 0;
        $this->notification_model->erp_system_events_id = 22;
        $this->notification_model->language_id = 2;
        $this->notification_model->msg_datetime = date('Y-m-d H:i', time());

        /*
          $current_phase_name=$this->input->post('current_phase');
          $next_phase_id=$this->input->post('next_phase');
          $this->safa_contract_phases_model->safa_contract_phases_id=$next_phase_id;
          $next_phase_row=$this->safa_contract_phases_model->get();
          $next_phase_name=$next_phase_row->{name()};

          $tags="#*current_contract_phase#*=$current_phase_name*****#*next_contract_phase#*=$next_phase_name";
         */

        //-------------- For Notification --------------------------------------------
        $this->erp_hotels_reservation_orders_model->rooms_erp_hotels_reservation_orders_id = $id;
        $item_rooms = $this->erp_hotels_reservation_orders_model->getRooms();
        $hotel_name = '';
        foreach ($item_rooms as $item_room) {
            $this->hotels_model->erp_hotel_id = $item_room->erp_hotels_id;
            $this->hotels_model->join = true;
            $erp_hotels_row = $this->hotels_model->get();
            if (count($erp_hotels_row) > 0) {
                $hotel_name = $hotel_name . ',' . $erp_hotels_row->hotel_name;
            }
        }
        //----------------------------------------------------------------------------


        if (session('uo_id')) {
            $hotel_reservation_order_owner = session('uo_name');
        } else if (session('ea_id')) {
            $hotel_reservation_order_owner = session('ea_name');
        } else if (session('hm_id')) {
            $hotel_reservation_order_owner = session('hm_name');
        }

        //Link to replace.
        $link_hotels_reservation_orders = "<a href='" . site_url('hotels_reservation_orders/edit/' . $id) . "'  target='_blank'> " . $id . " </a>";
        $tags = "#*the_date#*=" . date('Y-m-d H:i', time()) . "*****#*hotel_reservation_order_no#*=$link_hotels_reservation_orders*****#*hotel_reservation_order_owner#*=$hotel_reservation_order_owner*****#*hotel_name#*=$hotel_name";


        $this->notification_model->tags = $tags;

        $this->notification_model->parent_id = '';
        $erp_notification_id = $this->notification_model->save();
        //-------- Notification Details ---------------
        $this->notification_model->detail_erp_notification_id = $erp_notification_id;
        $this->notification_model->detail_receiver_type = $item->owner_erp_company_types_id;
        $this->notification_model->detail_receiver_id = $item->owner_erp_company_id;
        $this->notification_model->saveDetails();
        redirect('hotels_reservation_orders/incoming');
    }

    private function confirm_availability_deduction($id = FALSE) {
        $result = array();
        //        $main_criteria = $this->main_criteria;
        $sub_criteria = $this->sub_criteria;

        $this->hotel_availability_search_model->get_rooms = true;
        $this->hotel_availability_search_model->erp_company_type_id = $this->system_id;
        $this->hotel_availability_search_model->safa_company_id = $this->{$this->destination . '_id'};
        /**
         * CREATE HOTEL AVAILABILITY
         */
        $order = $this->db->where('erp_hotels_reservation_orders_id', $id)->get('erp_hotels_reservation_orders')->row();
        $res_order_rooms = $this->db->where('erp_hotels_reservation_orders_id', $id)->get('erp_hotels_reservation_orders_rooms')->result();

        foreach ($res_order_rooms as $res_order_room) {
            //             foreach ($main_criteria as $key => $value)
            //                if ($order->$value)
            //                    $this->hotel_availability_search_model->$key = $order->$value;

            foreach ($sub_criteria as $key => $value)
                if ($res_order_room->$value)
                    $this->hotel_availability_search_model->$key = $res_order_room->$value;



            $key = $res_order_room->erp_hotels_reservation_orders_rooms_id;
            foreach ($this->sub_criteria2 as $sub_key => $sub_value) {
                if (isset($res_order_room->$sub_value) && $res_order_room->$sub_value && $res_order_room->$sub_value != 0.00)
                    $this->hotel_availability_search_model->$sub_key = $res_order_room->$sub_value;
            }

            $res = $this->hotel_availability_search_model->get();

            if (!$res)
                show_error(lang('global_insufficient_hotels'));

            if ($order->owner_erp_company_id)
                $erp_hotels_availability_master_id = $this->create_new_availability($order, 966, $res_order_room);

            $available_count = 0;
            $i = 0;
            foreach ($res as $r) {
                $sub_room = $this->db->where('erp_hotels_availability_sub_room_id', $r->erp_hotels_availability_sub_room_id)->get('erp_hotels_availability_sub_rooms')->row();
                $room = $this->db->where('erp_hotels_availability_rooms_id', $sub_room->erp_hotels_availability_rooms_id)->get('erp_hotels_availability_rooms')->row();
                $room_details = $this->db->query("SELECT available.*
                                FROM erp_hotels_availability_room_details available
                                LEFT JOIN erp_hotels_availability_room_details reserved 
                                ON (
                                        reserved.erp_hotels_availability_sub_room_id = available.erp_hotels_availability_sub_room_id AND
                                        reserved.status = 2 AND
                                        reserved.number = available.number AND
                                        reserved.from_date <= '$res_order_room->entry_date' AND
                                        reserved.to_date >= '$res_order_room->exit_date'
                                )
                                WHERE 
                                available.erp_hotels_availability_sub_room_id = '$r->erp_hotels_availability_sub_room_id' AND
                                available.status = 1 AND
                                available.from_date <= '$res_order_room->entry_date' AND
                                available.to_date >= '$res_order_room->exit_date' AND
                                reserved.erp_hotels_availability_room_detail_id IS NULL")->result();
                //                print_r($room_details);
                /**
                 * CREATE HOTEL AVAILABILITY ROOMS
                 */
                if ($order->owner_erp_company_id && $erp_hotels_availability_master_id) {
                    $erp_hotels_availability_rooms = array(
                        'erp_hotelroomsize_id' => $res_order_room->erp_hotelroomsizes_id,
                        'rooms_beds_count' => $res_order_room->rooms_count,
                        'max_beds_count' => $room->max_beds_count,
                        'closed_rooms_beds_count' => $room->closed_rooms_beds_count,
                        'erp_hotels_availability_view_id' => $room->erp_hotels_availability_view_id,
                        'can_spiltting' => $room->can_spiltting,
                        'purchase_price' => $room->purchase_price,
                        'offer_status' => $room->offer_status,
                        'erp_hotels_availability_master_id' => $erp_hotels_availability_master_id,
                        'erp_housingtype_id' => $res_order_room->erp_housingtypes_id,
                    );
                    $this->db->insert('erp_hotels_availability_rooms', $erp_hotels_availability_rooms);
                    $erp_hotels_availability_rooms_id = $this->db->insert_id();
                }
                /**
                 * CREATE HOTEL AVAILABILITY DETAIL
                 */
                if ($erp_hotels_availability_master_id) {
                    $erp_hotels_availability_detail = array(
                        'erp_period_id' => 1,
                        'erp_housingtype_id' => $res_order_room->erp_housingtypes_id,
                        'erp_hotelroomsize_id' => $res_order_room->erp_hotelroomsizes_id,
                        'sale_price' => 0,
                        'offer_status' => 1,
                        'start_date' => $res_order_room->entry_date,
                        'end_date' => $res_order_room->exit_date,
                        'erp_hotels_availability_master_id' => $erp_hotels_availability_master_id,
                    );
                    $this->db->insert('erp_hotels_availability_detail', $erp_hotels_availability_detail);
                    $erp_hotels_availability_detail_id = $this->db->insert_id();
                }
                if (isset($r->erp_hotels_availability_sub_room_id) && $erp_hotels_availability_rooms_id) {
                    $sub_room = $this->db->where('erp_hotels_availability_sub_room_id', $r->erp_hotels_availability_sub_room_id)->get('erp_hotels_availability_sub_rooms')->row();
                    $erp_hotels_availability_sub_rooms = array(
                        'erp_hotels_availability_rooms_id' => $erp_hotels_availability_rooms_id,
                        'erp_floor_id' => $sub_room->erp_floor_id,
                        'section' => $sub_room->section,
                        'num_of_rooms' => $sub_room->num_of_rooms,
                        'room_nums' => $sub_room->room_nums
                    );
                    $this->db->insert('erp_hotels_availability_sub_rooms', $erp_hotels_availability_sub_rooms);
                    $erp_hotels_availability_sub_rooms_id = $this->db->insert_id();
                    foreach ($room_details as $room_detail) {
                        if ($i < $res_order_room->rooms_count) {
                            $i++;
                            /**
                             * RESERVING ROOMS
                             */
                            $insert = array(
                                'erp_hotels_availability_sub_room_id' => $r->erp_hotels_availability_sub_room_id,
                                'number' => $room_detail->number,
                                'from_date' => $res_order_room->entry_date,
                                'to_date' => $res_order_room->exit_date,
                                'status' => 2,
                                'safa_trip_hotel_room_id' => null,
                                'erp_hotels_reservation_orders_rooms_id' => $key,
                            );
                            $this->db->insert('erp_hotels_availability_room_details', $insert);
                            /**
                             * NEW AVAILABILITY ROOM DETAILS
                             */
                            $insert = array(
                                'erp_hotels_availability_sub_room_id' => $erp_hotels_availability_sub_rooms_id,
                                'number' => $room_detail->number,
                                'from_date' => $res_order_room->entry_date,
                                'to_date' => $res_order_room->exit_date,
                                'status' => 1,
                                'safa_trip_hotel_room_id' => null,
                                'erp_hotels_reservation_orders_rooms_id' => NULL,
                            );
                            $this->db->insert('erp_hotels_availability_room_details', $insert);
                        }
                    }
                }
            }
        }
    }

    private function create_new_availability($order, $erp_country_id = NULL, $res_order_room) {
        $erp_hotels_availability_master = array(
            'safa_company_id' => $order->owner_erp_company_id,
            'erp_company_type_id' => $order->owner_erp_company_types_id,
            'erp_country_id' => 966,
            'erp_city_id' => $res_order_room->erp_cities_id,
            'erp_hotel_id' => $res_order_room->erp_hotels_id,
            'season_id' => NULL,
            'date_from' => $order->arrival_date,
            'date_to' => $order->departure_date,
            'purchase_currency_id' => 3,
            'sale_currency_id' => 3,
            'notes' => NULL,
            'invoice_number' => NULL,
            'invoice_image' => NULL,
            'availability_status' => 3
        );
        $this->db->insert('erp_hotels_availability_master', $erp_hotels_availability_master);
        return $this->db->insert_id();
    }

    function insert_log($id) {
        if (session('uo_id')) {
            $this->erp_hotels_reservation_orders_log_model->owner_erp_company_types_id = 2;
            $this->erp_hotels_reservation_orders_log_model->owner_erp_company_id = session('uo_id');
        } else if (session('ea_id')) {
            $this->erp_hotels_reservation_orders_log_model->owner_erp_company_types_id = 3;
            $this->erp_hotels_reservation_orders_log_model->owner_erp_company_id = session('ea_id');
        } else if (session('hm_id')) {
            $this->erp_hotels_reservation_orders_log_model->owner_erp_company_types_id = 5;
            $this->erp_hotels_reservation_orders_log_model->owner_erp_company_id = session('hm_id');
        }
        if ($this->input->post('company_type')) {
            $this->erp_hotels_reservation_orders_log_model->erp_company_types_id = $this->input->post('company_type');
        } else {
            $this->erp_hotels_reservation_orders_log_model->erp_company_types_id = 2;
        }

        $this->erp_hotels_reservation_orders_log_model->erp_hotels_reservation_orders_id = $id;

        $this->erp_hotels_reservation_orders_log_model->erp_company_id = $this->input->post('erp_company_id');
        $this->erp_hotels_reservation_orders_log_model->the_date = date('Y-m-d H:i', time());
        $this->erp_hotels_reservation_orders_log_model->order_date = $this->input->post('order_date');
        $this->erp_hotels_reservation_orders_log_model->arrival_date = $this->input->post('arrival_date');
        $this->erp_hotels_reservation_orders_log_model->departure_date = $this->input->post('departure_date');
        $this->erp_hotels_reservation_orders_log_model->contact_person = $this->input->post('contact_person');
        $this->erp_hotels_reservation_orders_log_model->erp_cities_id = $this->input->post('erp_cities_id');
        $this->erp_hotels_reservation_orders_log_model->erp_hotels_id = $this->input->post('erp_hotels_id');
        $this->erp_hotels_reservation_orders_log_model->erp_hotels_id_alternative = $this->input->post('erp_hotels_id_alternative');
        $this->erp_hotels_reservation_orders_log_model->distance_from = $this->input->post('distance_from');
        $this->erp_hotels_reservation_orders_log_model->distance_to = $this->input->post('distance_to');
        if (isset($_POST['look_to_haram'])) {
            $this->erp_hotels_reservation_orders_log_model->look_to_haram = 1;
        } else {
            $this->erp_hotels_reservation_orders_log_model->look_to_haram = 0;
        }

        if (isset($_POST['safa_uo_contract_id'])) {
            $this->erp_hotels_reservation_orders_log_model->safa_uo_contract_id = $this->input->post('safa_uo_contract_id');
        }

        /*
          $this->erp_hotels_reservation_orders_log_model->price_from = $this->input->post('price_from');
          $this->erp_hotels_reservation_orders_log_model->price_to = $this->input->post('price_to');
          $this->erp_hotels_reservation_orders_log_model->erp_currencies_id = $this->input->post('erp_currencies_id');
         */

        // Save Order
        $erp_hotels_reservation_orders_log_id = $this->erp_hotels_reservation_orders_log_model->save();



        // Save nationalities
        $nationalities = $this->input->post('nationalities');
        if (ensure($nationalities)) {
            foreach ($nationalities as $nationality_id) {
                $this->erp_hotels_reservation_orders_log_model->nationalities_erp_hotels_reservation_orders_log_id = $erp_hotels_reservation_orders_log_id;
                $this->erp_hotels_reservation_orders_log_model->nationalities_erp_nationalities_id = $nationality_id;
                $this->erp_hotels_reservation_orders_log_model->saveNationalities();
            }
        }
        // Save hotel_advantages
        $advantages = $this->input->post('advantages');
        if (ensure($advantages)) {
            foreach ($advantages as $advantage_id) {
                $this->erp_hotels_reservation_orders_log_model->advantages_erp_hotels_reservation_orders_log_id = $erp_hotels_reservation_orders_log_id;
                $this->erp_hotels_reservation_orders_log_model->advantages_erp_hotel_advantages_id = $advantage_id;
                $this->erp_hotels_reservation_orders_log_model->saveHotelAdvantages();
            }
        }
        // Save room_services
        $room_services = $this->input->post('room_services');
        if (ensure($room_services)) {
            foreach ($room_services as $room_service_id) {
                $this->erp_hotels_reservation_orders_log_model->room_services_erp_hotels_reservation_orders_log_id = $erp_hotels_reservation_orders_log_id;
                $this->erp_hotels_reservation_orders_log_model->room_services_erp_room_services_id = $room_service_id;
                $this->erp_hotels_reservation_orders_log_model->saveRoomServices();
            }
        }



        // Save meals
        $meals_erp_meals = $this->input->post('meals_erp_meals');
        $meals_meals_count = $this->input->post('meals_meals_count');
        $meals_erp_hotel_id = $this->input->post('meals_erp_hotel_id');
        if (ensure($meals_erp_meals))
            foreach ($meals_erp_meals as $key => $value) {
                $this->erp_hotels_reservation_orders_log_model->meals_erp_hotels_reservation_orders_log_id = $erp_hotels_reservation_orders_log_id;
                $this->erp_hotels_reservation_orders_log_model->meals_erp_meals_id = $meals_erp_meals[$key];
                $this->erp_hotels_reservation_orders_log_model->meals_meals_count = $meals_meals_count[$key];

                $safa_hotels_reservation_orders_meals_id = $this->erp_hotels_reservation_orders_log_model->saveMeals();
            }

        // Save rooms
        $rooms_housingtypes = $this->input->post('rooms_erp_housingtypes_id');
        $rooms_hotelroomsizes = $this->input->post('rooms_erp_hotelroomsizes_id');
        $rooms_rooms_count = $this->input->post('rooms_rooms_count');
        $rooms_nights_count = $this->input->post('rooms_nights_count');
        $rooms_entry_date = $this->input->post('rooms_entry_date');
        $rooms_exit_date = $this->input->post('rooms_exit_date');
        $rooms_erp_meals_id = $this->input->post('rooms_erp_meals_id');

        $rooms_price_from = $this->input->post('rooms_price_from');
        $rooms_price_to = $this->input->post('rooms_price_to');
        $rooms_erp_currencies_id = $this->input->post('rooms_erp_currencies_id');

        if (ensure($rooms_housingtypes))
            foreach ($rooms_housingtypes as $key => $value) {
                $this->erp_hotels_reservation_orders_log_model->rooms_erp_hotels_reservation_orders_log_id = $erp_hotels_reservation_orders_log_id;

                $this->erp_hotels_reservation_orders_log_model->rooms_erp_housingtypes_id = $rooms_housingtypes[$key];
                $this->erp_hotels_reservation_orders_log_model->rooms_erp_hotelroomsizes_id = $rooms_hotelroomsizes[$key];
                $this->erp_hotels_reservation_orders_log_model->rooms_rooms_count = $rooms_rooms_count[$key];
                $this->erp_hotels_reservation_orders_log_model->rooms_nights_count = $rooms_nights_count[$key];
                $this->erp_hotels_reservation_orders_log_model->rooms_entry_date = $rooms_entry_date[$key];
                $this->erp_hotels_reservation_orders_log_model->rooms_exit_date = $rooms_exit_date[$key];
                $this->erp_hotels_reservation_orders_log_model->rooms_erp_meals_id = $rooms_erp_meals_id[$key];

                $this->erp_hotels_reservation_orders_log_model->rooms_price_from = $rooms_price_from[$key];
                $this->erp_hotels_reservation_orders_log_model->rooms_price_to = $rooms_price_to[$key];
                $this->erp_hotels_reservation_orders_log_model->rooms_erp_currencies_id = $rooms_erp_currencies_id[$key];

                $safa_hotels_reservation_orders_rooms_id = $this->erp_hotels_reservation_orders_log_model->saveRooms();
            }
    }

    function edit_log($id) {
        $this->erp_hotels_reservation_orders_log_model->erp_hotels_reservation_orders_id = $id;
        $erp_hotels_reservation_orders_log_rows = $this->erp_hotels_reservation_orders_log_model->get();

        if (count($erp_hotels_reservation_orders_log_rows) > 0) {

            $erp_hotels_reservation_orders_log_id = $erp_hotels_reservation_orders_log_rows[0]->erp_hotels_reservation_orders_log_id;
            $this->erp_hotels_reservation_orders_log_model->erp_hotels_reservation_orders_log_id = $erp_hotels_reservation_orders_log_id;

            if (session('uo_id')) {
                $this->erp_hotels_reservation_orders_log_model->owner_erp_company_types_id = 2;
                $this->erp_hotels_reservation_orders_log_model->owner_erp_company_id = session('uo_id');
            } else if (session('ea_id')) {
                $this->erp_hotels_reservation_orders_log_model->owner_erp_company_types_id = 3;
                $this->erp_hotels_reservation_orders_log_model->owner_erp_company_id = session('ea_id');
            } else if (session('hm_id')) {
                $this->erp_hotels_reservation_orders_log_model->owner_erp_company_types_id = 5;
                $this->erp_hotels_reservation_orders_log_model->owner_erp_company_id = session('hm_id');
            }

            if ($this->input->post('company_type')) {
                $this->erp_hotels_reservation_orders_log_model->erp_company_types_id = $this->input->post('company_type');
            } else {
                $this->erp_hotels_reservation_orders_log_model->erp_company_types_id = 2;
            }

            $this->erp_hotels_reservation_orders_log_model->erp_company_id = $this->input->post('erp_company_id');
            $this->erp_hotels_reservation_orders_log_model->the_date = date('Y-m-d H:i', time());
            $this->erp_hotels_reservation_orders_log_model->order_date = $this->input->post('order_date');
            $this->erp_hotels_reservation_orders_log_model->arrival_date = $this->input->post('arrival_date');
            $this->erp_hotels_reservation_orders_log_model->departure_date = $this->input->post('departure_date');
            $this->erp_hotels_reservation_orders_log_model->contact_person = $this->input->post('contact_person');
            $this->erp_hotels_reservation_orders_log_model->erp_cities_id = $this->input->post('erp_cities_id');
            $this->erp_hotels_reservation_orders_log_model->erp_hotels_id = $this->input->post('erp_hotels_id');
            $this->erp_hotels_reservation_orders_log_model->erp_hotels_id_alternative = $this->input->post('erp_hotels_id_alternative');
            $this->erp_hotels_reservation_orders_log_model->distance_from = $this->input->post('distance_from');
            $this->erp_hotels_reservation_orders_log_model->distance_to = $this->input->post('distance_to');
            if (isset($_POST['look_to_haram'])) {
                $this->erp_hotels_reservation_orders_log_model->look_to_haram = 1;
            } else {
                $this->erp_hotels_reservation_orders_log_model->look_to_haram = 0;
            }

            if (isset($_POST['safa_uo_contract_id'])) {
                $this->erp_hotels_reservation_orders_log_model->safa_uo_contract_id = $this->input->post('safa_uo_contract_id');
            }

            /*
              $this->erp_hotels_reservation_orders_log_model->price_from = $this->input->post('price_from');
              $this->erp_hotels_reservation_orders_log_model->price_to = $this->input->post('price_to');
              $this->erp_hotels_reservation_orders_log_model->erp_currencies_id = $this->input->post('erp_currencies_id');
             */

            // Save Order
            $this->erp_hotels_reservation_orders_log_model->save();

            // Save nationalities
            $nationalities = $this->input->post('nationalities');
            $this->erp_hotels_reservation_orders_log_model->nationalities_erp_hotels_reservation_orders_log_id = $erp_hotels_reservation_orders_log_id;
            $this->erp_hotels_reservation_orders_log_model->deleteNationalities();
            if (ensure($nationalities)) {
                foreach ($nationalities as $nationality_id) {
                    $this->erp_hotels_reservation_orders_log_model->nationalities_erp_hotels_reservation_orders_log_id = $erp_hotels_reservation_orders_log_id;
                    $this->erp_hotels_reservation_orders_log_model->nationalities_erp_nationalities_id = $nationality_id;
                    $this->erp_hotels_reservation_orders_log_model->saveNationalities();
                }
            }

            // Save hotel_advantages
            $advantages = $this->input->post('advantages');
            $this->erp_hotels_reservation_orders_log_model->advantages_erp_hotels_reservation_orders_log_id = $erp_hotels_reservation_orders_log_id;
            $this->erp_hotels_reservation_orders_log_model->deleteHotelAdvantages();
            if (ensure($advantages)) {
                foreach ($advantages as $advantage_id) {
                    $this->erp_hotels_reservation_orders_log_model->advantages_erp_hotels_reservation_orders_log_id = $erp_hotels_reservation_orders_log_id;
                    $this->erp_hotels_reservation_orders_log_model->advantages_erp_hotel_advantages_id = $advantage_id;
                    $this->erp_hotels_reservation_orders_log_model->saveHotelAdvantages();
                }
            }
            // Save room_services
            $room_services = $this->input->post('room_services');
            $this->erp_hotels_reservation_orders_log_model->room_services_erp_hotels_reservation_orders_log_id = $erp_hotels_reservation_orders_log_id;
            $this->erp_hotels_reservation_orders_log_model->deleteRoomServices();
            if (ensure($room_services)) {
                foreach ($room_services as $room_service_id) {
                    $this->erp_hotels_reservation_orders_log_model->room_services_erp_hotels_reservation_orders_log_id = $erp_hotels_reservation_orders_log_id;
                    $this->erp_hotels_reservation_orders_log_model->room_services_erp_room_services_id = $room_service_id;
                    $this->erp_hotels_reservation_orders_log_model->saveRoomServices();
                }
            }


            // Save meals
            $this->erp_hotels_reservation_orders_log_model->meals_erp_hotels_reservation_orders_log_id = $erp_hotels_reservation_orders_log_id;
            $this->erp_hotels_reservation_orders_log_model->deleteMeals();
            $meals_erp_meals = $this->input->post('meals_erp_meals');
            $meals_meals_count = $this->input->post('meals_meals_count');
            $meals_erp_hotel_id = $this->input->post('meals_erp_hotel_id');
            if (ensure($meals_erp_meals))
                foreach ($meals_erp_meals as $key => $value) {
                    $this->erp_hotels_reservation_orders_log_model->meals_erp_hotels_reservation_orders_log_id = $erp_hotels_reservation_orders_log_id;
                    $this->erp_hotels_reservation_orders_log_model->meals_erp_meals_id = $meals_erp_meals[$key];
                    $this->erp_hotels_reservation_orders_log_model->meals_meals_count = $meals_meals_count[$key];


                    //$this->erp_hotels_reservation_orders_log_model->erp_hotels_reservation_orders_meals_log_id = $key;
                    //$current_meal_row_count = $this->erp_hotels_reservation_orders_log_model->getMeals(true);
                    //if($current_meal_row_count==0) {
                    //$this->erp_hotels_reservation_orders_log_model->erp_hotels_reservation_orders_meals_log_id = false;
                    $this->erp_hotels_reservation_orders_log_model->saveMeals();
                    //} else {
                    //$erp_hotels_reservation_orders_meals_id=$key;
                    //$this->erp_hotels_reservation_orders_log_model->saveMeals();
                    //}
                }
            // --- Delete Deleted meals rows from Database --------------
            //By Gouda
            /*
              $this->erp_hotels_reservation_orders_log_model->meals_erp_meals_id = false;
              $this->erp_hotels_reservation_orders_log_model->meals_meals_count = false;

              $meals_remove = $this->input->post('meals_remove');
              if(ensure($meals_remove)) {
              foreach($meals_remove as $meal_remove) {
              $this->erp_hotels_reservation_orders_log_model->erp_hotels_reservation_orders_meals_log_id = $meal_remove;
              $this->erp_hotels_reservation_orders_log_model->deleteMeals();
              }
              }
             */
            //---------------------------------------------------------------
            // Save rooms
            $this->erp_hotels_reservation_orders_log_model->rooms_erp_hotels_reservation_orders_log_id = $erp_hotels_reservation_orders_log_id;
            $this->erp_hotels_reservation_orders_log_model->deleteRooms();

            $rooms_housingtypes = $this->input->post('rooms_erp_housingtypes_id');
            $rooms_hotelroomsizes = $this->input->post('rooms_erp_hotelroomsizes_id');
            $rooms_rooms_count = $this->input->post('rooms_rooms_count');
            $rooms_nights_count = $this->input->post('rooms_nights_count');
            $rooms_entry_date = $this->input->post('rooms_entry_date');
            $rooms_exit_date = $this->input->post('rooms_exit_date');
            $rooms_erp_meals_id = $this->input->post('rooms_erp_meals_id');

            $rooms_price_from = $this->input->post('rooms_price_from');
            $rooms_price_to = $this->input->post('rooms_price_to');
            $rooms_erp_currencies_id = $this->input->post('rooms_erp_currencies_id');


            if (ensure($rooms_housingtypes))
                foreach ($rooms_housingtypes as $key => $value) {
                    $this->erp_hotels_reservation_orders_log_model->rooms_erp_hotels_reservation_orders_log_id = $erp_hotels_reservation_orders_log_id;
                    $this->erp_hotels_reservation_orders_log_model->rooms_erp_housingtypes_id = $rooms_housingtypes[$key];
                    $this->erp_hotels_reservation_orders_log_model->rooms_erp_hotelroomsizes_id = $rooms_hotelroomsizes[$key];
                    $this->erp_hotels_reservation_orders_log_model->rooms_rooms_count = $rooms_rooms_count[$key];
                    $this->erp_hotels_reservation_orders_log_model->rooms_nights_count = $rooms_nights_count[$key];
                    $this->erp_hotels_reservation_orders_log_model->rooms_entry_date = $rooms_entry_date[$key];
                    $this->erp_hotels_reservation_orders_log_model->rooms_exit_date = $rooms_exit_date[$key];
                    $this->erp_hotels_reservation_orders_log_model->rooms_erp_meals_id = $rooms_erp_meals_id[$key];

                    $this->erp_hotels_reservation_orders_log_model->rooms_price_from = $rooms_price_from[$key];
                    $this->erp_hotels_reservation_orders_log_model->rooms_price_to = $rooms_price_to[$key];
                    $this->erp_hotels_reservation_orders_log_model->rooms_erp_currencies_id = $rooms_erp_currencies_id[$key];

                    //$this->erp_hotels_reservation_orders_log_model->rooms_erp_hotels_reservation_orders_rooms_log_id = $key;
                    //$current_room_row_count = $this->erp_hotels_reservation_orders_log_model->getRooms(true);
                    //if($current_room_row_count==0) {
                    //$this->erp_hotels_reservation_orders_log_model->rooms_erp_hotels_reservation_orders_rooms_log_id = false;
                    $this->erp_hotels_reservation_orders_log_model->saveRooms();
                    //} else {
                    //$erp_hotels_reservation_orders_rooms_id=$key;
                    //$this->erp_hotels_reservation_orders_log_model->saveRooms();
                    //}
                }
            // --- Delete Deleted rooms rows from Database --------------
            // By Gouda
            /*
              $this->erp_hotels_reservation_orders_log_model->rooms_erp_housingtypes_id = false;
              $this->erp_hotels_reservation_orders_log_model->rooms_erp_hotelroomsizes_id = false;
              $this->erp_hotels_reservation_orders_log_model->rooms_rooms_count = false;
              $this->erp_hotels_reservation_orders_log_model->rooms_nights_count = false;
              $this->erp_hotels_reservation_orders_log_model->rooms_entry_date = false;
              $this->erp_hotels_reservation_orders_log_model->rooms_exit_date = false;
              $this->erp_hotels_reservation_orders_log_model->rooms_erp_meals_id = false;

              $this->erp_hotels_reservation_orders_log_model->rooms_price_from = false;
              $this->erp_hotels_reservation_orders_log_model->rooms_price_to = false;
              $this->erp_hotels_reservation_orders_log_model->rooms_erp_currencies_id = false;

              $rooms_remove = $this->input->post('rooms_remove');
              if(ensure($rooms_remove)) {
              foreach($rooms_remove as $room_remove) {
              $this->erp_hotels_reservation_orders_log_model->rooms_erp_hotels_reservation_orders_rooms_log_id = $room_remove;
              $this->erp_hotels_reservation_orders_log_model->deleteRooms();
              }
              }
             */
            //---------------------------------------------------------------
        } else {
            $this->insert_log($id);
        }
    }

    function pdf_export($id) {
        $data = array();
        if (!$id)
            show_404();
        $this->erp_hotels_reservation_orders_model->owner_erp_company_id = $this->{$this->destination . '_id'};
        $this->erp_hotels_reservation_orders_model->owner_erp_company_types_id = $this->system_id;
        $this->erp_hotels_reservation_orders_model->erp_hotels_reservation_orders_id = $id;
        $item = $this->erp_hotels_reservation_orders_model->get();
        $data['item'] = $item;
        if (!$data['item']) {
            $this->erp_hotels_reservation_orders_model->owner_erp_company_id = false;
            $this->erp_hotels_reservation_orders_model->owner_erp_company_types_id = false;

            $this->erp_hotels_reservation_orders_model->erp_company_id = $this->{$this->destination . '_id'};
            $this->erp_hotels_reservation_orders_model->erp_company_types_id = $this->system_id;

            $item = $this->erp_hotels_reservation_orders_model->get();
            $data['item'] = $item;
            if (!$data['item']) {
                show_404();
            }
        }

        if (session('ea_id')) {
            $this->contracts_model->safa_ea_id = session('ea_id');
            $data['contract_row'] = $this->contracts_model->search();
        }


        $data['current_order_no'] = $id;
        
        $data['erp_hotelroomsizes'] = $this->db->select('erp_hotelroomsize_id,'.name())->where_in('erp_hotelroomsize_id', explode(',', $data['item']->room_sizes_string))->get('erp_hotelroomsizes')->result();


        $this->erp_hotels_reservation_orders_model->nationalities_erp_hotels_reservation_orders_id = $id;
        $data['item_nationalities'] = $this->erp_hotels_reservation_orders_model->getNationalities();

        $this->erp_hotels_reservation_orders_model->advantages_erp_hotels_reservation_orders_id = $id;
        $data['item_advantages'] = $this->erp_hotels_reservation_orders_model->getHotelAdvantages();


        $this->erp_hotels_reservation_orders_model->room_services_erp_hotels_reservation_orders_id = $id;
        $data['item_room_services'] = $this->erp_hotels_reservation_orders_model->getRoomServices();


        $this->erp_hotels_reservation_orders_model->rooms_erp_hotels_reservation_orders_id = $id;
        $item_rooms = $this->erp_hotels_reservation_orders_model->getRooms();
        $hotels = array();
        foreach ($item_rooms as $room){
            $hotels[$room->erp_hotels_id]['data'] = $room;
            $hotels[$room->erp_hotels_id]['rooms'][$room->erp_hotelroomsizes_id] = $room->rooms_count;
        }
        $data['item_rooms'] = $hotels;

        $this->erp_hotels_reservation_orders_model->meals_erp_hotels_reservation_orders_id = $id;
        $item_meals = $this->erp_hotels_reservation_orders_model->getMeals();
        $data['item_meals'] = $item_meals;

        // DOMPDF
        /*
          $this->load->library('pdf');
          $this->layout = 'ajax';
          $html = $this->load->view("hotels_reservation_orders/pdf", $data, true);
          //echo $html; exit;
          $this->pdf->load_html($html);
          $this->pdf->render();
          $this->pdf->stream("hotels_reservation_orders.pdf");
         */

//        print_r($data);exit();
        

        $this->load->helper('pdf_helper');
        $pdf_orientation = 'P';
        $pdf_unit = 'px';
        $pdf_pageformat = 'A4';
        $pdf_unicode = true;
        $pdf_encoding = 'UTF-8';

        $pdf = new virgo_pdf($pdf_orientation, $pdf_unit, $pdf_pageformat, $pdf_unicode, $pdf_encoding, false);
        $tahoma = $pdf->addTTFfont('static/fonts/arial.ttf', 'TrueTypeUnicode', '', 96);
        $pdf->SetFont($tahoma, '', 10, '', false);
        //$pdf->SetFont('dejavusans', '', 10, '', false);
        // set document information
        $pdf->SetCreator('Safa Live');
        $pdf->SetAuthor('Safa Live');
        $pdf->SetTitle('Safa Live Reports');
        $pdf->SetSubject('Safa Live');

        $pdf->SetMargins(40, PDF_MARGIN_TOP, 40);
        $pdf->SetHeaderMargin(0);
        $pdf->SetFooterMargin(0);

        $pdf->setPrintFooter(false);

        // set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

        // set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
$pdf->setRTL(true);
        $pdf->AddPage();

        /* NOTE:
         * *********************************************************
         * You can load external XHTML using :
         *
         * $html = file_get_contents('/path/to/your/file.html');
         *
         * External CSS files will be automatically loaded.
         * Sometimes you need to fix the path of the external CSS.
         * *********************************************************
         */

        // define some HTML content with style
//print_r($data);die();

        $html = $this->load->view("hotels_reservation_orders/pdf", $data, true);
        //echo $html; exit;
        // output the HTML content
        $pdf->writeHTML($html, true, false, true, false, '');

        // - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        // reset pointer to the last page
        $pdf->lastPage();

        // ---------------------------------------------------------
        //Close and output PDF document
        ob_end_clean();
        $this->load->helper('download');
        $pdf->Output("hotels_reservation_orders_$id.pdf", 'D');

        //exit();
    }

    public function hotel_search() {
        $this->layout = 'js_new';
        $attributes = array(
            'city_id',
            'rooms',
            'nights',
            'date_from',
            'date_to',
            'housing_type_id',
            'room_type_id',
            'price_from',
            'price_to',
            'currency',
            'distance_from_haram',
            'distance_to_haram',
            'level',
            'nationalities',
            'hotel_features',
            'inclusive_service',
            'exclusive_service',
        );
        if ($this->input->post('date_from'))
            $this->hotel_availability_search_model->main_date_from = $this->input->post('date_from');
        if ($this->input->post('date_to'))
            $this->hotel_availability_search_model->main_date_to = $this->input->post('date_to');
        $this->hotel_availability_search_model->ha_type = 1;

        foreach ($attributes as $attribute)
            if ($this->input->post($attribute))
                $this->hotel_availability_search_model->$attribute = $this->input->post($attribute);
        $data['results'][] = $this->hotel_availability_search_model->get();
        $data['co_type'] = $this->system_id;
        $data['co_id'] = $this->{$this->destination . '_id'};
        $this->load->view('hotels_reservation_orders/result', $data);
    }

    function incoming_sort() {
        $this->layout = 'ajax';

        if (session('uo_id')) {
            $this->erp_hotels_reservation_orders_model->erp_company_types_id = 2;
            $this->erp_hotels_reservation_orders_model->erp_company_id = session('uo_id');
        } else if (session('ea_id')) {
            $this->erp_hotels_reservation_orders_model->erp_company_types_id = 3;
            $this->erp_hotels_reservation_orders_model->erp_company_id = session('ea_id');
        } else if (session('hm_id')) {
            $this->erp_hotels_reservation_orders_model->erp_company_types_id = 5;
            $this->erp_hotels_reservation_orders_model->erp_company_id = session('hm_id');
        } else {
            $this->erp_hotels_reservation_orders_model->erp_company_types_id = 0;
            $this->erp_hotels_reservation_orders_model->erp_company_id = 0;
        }

        $data = $this->erp_hotels_reservation_orders_model->get_sort();
        foreach ($_POST as $key => $value) {
            $new_sort = $value;
            $key = substr($key, 4);
            $current_sort = $data[$key];
            if ($current_sort != $new_sort) {
                $this->erp_hotels_reservation_orders_model->switch_order($key, $new_sort);
            }
        }
    }

    public function get_uos_companies_by_safa_uo_contract_id() {
        $safa_uo_contract_id = $this->input->post('safa_uo_contract_id');

        $structure = array('safa_uo_id', 'safa_uos_name');
        $key = $structure['0'];
        if (isset($structure['1'])) {
            $value = $structure['1'];
        }
        $companies_arr = array();
        $companies = $this->contracts_model->safa_uo_contract_id = $safa_uo_contract_id;
        $companies = $this->contracts_model->search();
        $companies_arr[''] = lang('global_select');
        foreach ($companies as $company) {
            $companies_arr[$company->$key] = $company->$value;
        }

        $form_dropdown = form_dropdown('erp_company_id', $companies_arr, set_value('erp_company_id', ''), 'class="chosen-select chosen-rtl input-full"  tabindex="4" ');
        $form_dropdown = str_replace('<select name="erp_company_id" class="chosen-select chosen-rtl input-full" multiple tabindex="4" >', '', $form_dropdown);
        $form_dropdown = str_replace('</select>', '', $form_dropdown);
        echo $form_dropdown;
        exit;
    }

    public function get_nights($peroid_id = false, $city_id = false) {
        $this->layout = 'ajax';
        if (!$peroid_id || !$city_id)
            die('0');

        $dayscount = get_peroid_night_count($peroid_id, $city_id);
        die($dayscount);
    }

    public function get_price($package_id = false, $peroid_id = false, $roomsize_id = false) {
        $this->layout = 'ajax';
        if (!$package_id || !$peroid_id || !$roomsize_id)
            die('0');

        $query = <<<QURY
SELECT safa_package_periods_prices.price FROM safa_package_periods
INNER JOIN safa_package_periods_prices ON safa_package_periods_prices.safa_package_periods_id = safa_package_periods.safa_package_periods_id
WHERE
safa_package_periods.safa_package_id = $package_id AND
safa_package_periods.erp_package_period_id = $peroid_id AND
safa_package_periods_prices.erp_hotelroomsize_id = $roomsize_id
QURY;
        $prices = $this->db->query($query)->row();
        echo $prices->price;
        die();
    }

    public function pkg_check_availability($pkgid = TRUE) {
        $this->layout = 'ajax';
        $package = $this->db->where('safa_package_id', $pkgid)->get('safa_packages')->row();
        $result = array();
        foreach (post('rooms') as $key => $value) {
            $leavetime = strtotime($value['hotel0exit']);
            $entrytime = $leavetime - ((int) $value['nights_0'] * 24 * 3600);
            $this->hotel_availability_search_model->date_from = date('Y-m-d', $entrytime);
            $this->hotel_availability_search_model->date_to = $value['hotel0exit'];
            $this->hotel_availability_search_model->hotel_id = $value['hotelid_0'];
            $this->hotel_availability_search_model->housing_type_id = NULL;
            $this->hotel_availability_search_model->room_type_id = $value['erp_hotelroomsize'];

            $this->hotel_availability_search_model->ha_type = 3;
            $this->hotel_availability_search_model->erp_company_type_id = $package->erp_company_type_id;
            $this->hotel_availability_search_model->safa_company_id = $package->erp_company_id;

            $available_count = 0;
            $res = $this->hotel_availability_search_model->get();
            $romcount = 0;
            foreach ($res as $av) {
                $romcount += $av->available_count;
            }
            if ($romcount)
                $result['h0'] = $romcount - $value['roomscount'];
            else
                $result['h0'] = - $value['roomscount'];

            $this->hotel_availability_search_model->date_from = $value['hotel0exit'];
            $this->hotel_availability_search_model->date_to = $value['hotel1exit'];
            $this->hotel_availability_search_model->hotel_id = $value['hotelid_1'];
            $this->hotel_availability_search_model->housing_type_id = 1;
            $this->hotel_availability_search_model->room_type_id = $value['erp_hotelroomsize'];

            $this->hotel_availability_search_model->ha_type = 3;
            $this->hotel_availability_search_model->erp_company_type_id = $package->erp_company_type_id;
            $this->hotel_availability_search_model->safa_company_id = $package->erp_company_id;

            $available_count = 0;
            $res = $this->hotel_availability_search_model->get();
            $romcount = 0;
            foreach ($res as $av) {
                $romcount += $av->available_count;
            }
            if ($romcount)
                $result['h1'] = $romcount - $value['roomscount'];
            else
                $result['h1'] = - $value['roomscount'];
        }
        echo json_encode($result);
    }

    public function add_availability() {
        $this->layout = 'ajax';
        $cities = $this->input->post('rooms_erp_cities_id');
        $hotels = $this->input->post('rooms_erp_hotels_id');
        $housingtypes = $this->input->post('rooms_erp_housingtypes_id');
        $hotelroomsizes = $this->input->post('rooms_erp_hotelroomsizes_id');
        $rooms = $this->input->post('rooms_rooms_count');
        $nights = $this->input->post('rooms_nights_count');
        $entries = $this->input->post('rooms_entry_date');
        $exits = $this->input->post('rooms_exit_date');
        $meals = $this->input->post('rooms_erp_meals_id');

        $availabilites = array();

        foreach ($cities as $key => $cityid) {
            $availabilites[$hotels[$key]][] = array(
                'rooms_erp_cities_id' => $cities[$key],
                'rooms_erp_housingtypes_id' => $housingtypes[$key],
                'rooms_erp_hotelroomsizes_id' => $hotelroomsizes[$key],
                'rooms_rooms_count' => $rooms[$key],
                'rooms_nights_count' => $nights[$key],
                'rooms_entry_date' => $entries[$key],
                'rooms_exit_date' => $exits[$key],
                'rooms_erp_meals_id' => $meals[$key]
            );
        }

        if (session('ea_id')) {
            $comp_id = session('ea_id');
            $comp_type = 3;
        } else if (session('uo_id')) {
            $comp_id = session('uo_id');
            $comp_type = 2;
        } else if (session('hm_id')) {
            $comp_id = session('hm_id');
            $comp_type = 5;
        } else {
            $comp_id = 0;
            $comp_type = 0;
        }
        $this->db->trans_start();
        foreach ($availabilites as $hotel_id => $availabilitydata) {
            $erp_hotels_availability_master = array(
                'safa_company_id' => $comp_id,
                'erp_company_type_id' => $comp_type,
                'erp_country_id' => 966,
                'erp_city_id' => $availabilitydata[0]['rooms_erp_cities_id'],
                'erp_hotel_id' => $hotel_id,
                'season_id' => NULL,
                'date_from' => $availabilitydata[0]['rooms_entry_date'],
                'date_to' => $availabilitydata[0]['rooms_exit_date'],
                'purchase_currency_id' => 3,
                'sale_currency_id' => 3,
                'notes' => NULL,
                'invoice_number' => NULL,
                'invoice_image' => NULL,
                'availability_status' => 3
            );
            $this->db->insert('erp_hotels_availability_master', $erp_hotels_availability_master);
            $erp_hotels_availability_master_id = $this->db->insert_id();

            foreach ($availabilitydata as $roomsarray) {
                $erp_hotels_availability_rooms = array(
                    'erp_hotelroomsize_id' => $roomsarray['rooms_erp_hotelroomsizes_id'],
                    'rooms_beds_count' => $roomsarray['rooms_rooms_count'],
                    'max_beds_count' => 0,
                    'closed_rooms_beds_count' => 0,
                    'erp_hotels_availability_view_id' => 0,
                    'can_spiltting' => 0,
                    'purchase_price' => 0,
                    'offer_status' => 1,
                    'erp_hotels_availability_master_id' => $erp_hotels_availability_master_id,
                    'erp_housingtype_id' => $roomsarray['rooms_erp_housingtypes_id'],
                );
                $this->db->insert('erp_hotels_availability_rooms', $erp_hotels_availability_rooms);
                $erp_hotels_availability_rooms_id = $this->db->insert_id();

                /**
                 * CREATE HOTEL AVAILABILITY DETAIL
                 */
                if ($erp_hotels_availability_master_id) {
                    $erp_hotels_availability_detail = array(
                        'erp_period_id' => 1,
                        'erp_housingtype_id' => $roomsarray['rooms_erp_housingtypes_id'],
                        'erp_hotelroomsize_id' => $roomsarray['rooms_erp_hotelroomsizes_id'],
                        'sale_price' => 0,
                        'offer_status' => 1,
                        'start_date' => $roomsarray['rooms_entry_date'],
                        'end_date' => $roomsarray['rooms_exit_date'],
                        'erp_hotels_availability_master_id' => $erp_hotels_availability_master_id,
                    );
                    $this->db->insert('erp_hotels_availability_detail', $erp_hotels_availability_detail);
                    $erp_hotels_availability_detail_id = $this->db->insert_id();
                }
                if ($erp_hotels_availability_rooms_id) {
                    $erp_hotels_availability_sub_rooms = array(
                        'erp_hotels_availability_rooms_id' => $erp_hotels_availability_rooms_id,
                        'erp_floor_id' => 1,
                        'section' => '',
                        'num_of_rooms' => $roomsarray['rooms_rooms_count'],
                        'room_nums' => ''
                    );
                    $this->db->insert('erp_hotels_availability_sub_rooms', $erp_hotels_availability_sub_rooms);
                    $erp_hotels_availability_sub_rooms_id = $this->db->insert_id();
                    for ($rn = 1; $rn <= $roomsarray['rooms_rooms_count']; $rn++) {
                        $insert = array(
                            'erp_hotels_availability_sub_room_id' => $erp_hotels_availability_sub_rooms_id,
                            'number' => "10" . $rn,
                            'from_date' => $roomsarray['rooms_entry_date'],
                            'to_date' => $roomsarray['rooms_exit_date'],
                            'status' => 1,
                            'safa_trip_hotel_room_id' => null,
                            'erp_hotels_reservation_orders_rooms_id' => NULL,
                        );
                        $this->db->insert('erp_hotels_availability_room_details', $insert);
                    }
                }
            }
        }
        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE) {
            echo 'Error';
        } else {
            echo 'Done';
        }
    }

}

/* End of file hotels_reservation_orders.php */
/* Location: ./application/controllers/hotels_reservation_orders.php */
