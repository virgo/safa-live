<?php

class Welcome extends Safa_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $this->layout = 'ajax';
        $this->load->view('welcome');
    }
  
    public function fourohfour() {
        show_404();
    }

    public function updatelang() {
        $this->load->helper('directory');
        $this->db->truncate('safa_languages_files');
        $this->db->truncate('safa_language_keys');
        $this->db->truncate('safa_languages_phrases');

        $language_files = directory_map('./application/language/arabic/');

        foreach ($language_files as $key => $file) {
            if (is_array($file)) {
                $key = str_replace('\\', '/', $key);
                foreach ($file as $filename) {
                    $filetbname = $key . str_replace('_lang.php', '', $filename);
                    $this->db->set('file_name', $filetbname)->insert('safa_languages_files');
                    $file_id = $this->db->insert_id();
                    $filerows = $this->lang->load($filetbname, '', TRUE);
                    foreach ($filerows as $rowkey => $rowvalue) {
                        $rowarray = array(
                            'safa_languages_file_id' => $file_id,
                            'safa_language_key_name' => $rowkey
                        );
                        $this->db->insert('safa_language_keys', $rowarray);
                        $rowid = $this->db->insert_id();
                        $phrasesrow = array(
                            'safa_language_key_id' => $rowid,
                            'phrase' => $rowvalue,
                            'safa_language_id' => 2
                        );
                        $this->db->insert('safa_languages_phrases', $phrasesrow);
                    }
                }
            } else {
                $filetbname = str_replace('_lang.php', '', $file);
                $this->db->set('file_name', $filetbname)->insert('safa_languages_files');
                $file_id = $this->db->insert_id();
                $filerows = $this->lang->load($filetbname, '', TRUE);
                foreach ($filerows as $rowkey => $rowvalue) {
                    $rowarray = array(
                        'safa_languages_file_id' => $file_id,
                        'safa_language_key_name' => $rowkey
                    );
                    $this->db->insert('safa_language_keys', $rowarray);
                    $rowid = $this->db->insert_id();
                    $phrasesrow = array(
                        'safa_language_key_id' => $rowid,
                        'phrase' => $rowvalue,
                        'safa_language_id' => 2
                    );
                    $this->db->insert('safa_languages_phrases', $phrasesrow);
                }
            }
        }
        die('done');
    }

    public function test() {
        $this->layout = 'ajax';
        $this->load->helper('simple_html');

        $this->load->library('curl');
        $login_url = 'http://gyh1.eumra.com/umra1435_2014/U_Login/Login.aspx?ln=ar';
        $mofa_url = 'http://gyh1.eumra.com/umra1435_2014/U_GRP_Managements/GroupsOperations.aspx';
        $html = file_get_html($login_url);
        foreach ($html->find('input') as $element) {
            if ($element->name == '__VIEWSTATE')
                $__VIEWSTATE = $element->value;
            elseif ($element->name == '__EVENTVALIDATION')
                $__EVENTVALIDATION = $element->value;
        }

        $data['LoginName'] = 'info@skydirecttravel.co.uk';
        $data['txtPassword'] = 'abdul1';
        $data['RadScriptManager1_TSM'] = '';
        $data['__EVENTTARGET'] = '';
        $data['__EVENTARGUMENT'] = '';
        $data['__LASTFOCUS'] = '';
        $data['btnLogin'] = 'Sign In';
        $data['RM'] = '';
        $data['FLD'] = '';
        $data['__VIEWSTATE'] = $__VIEWSTATE;
        $data['__EVENTVALIDATION'] = $__EVENTVALIDATION;
//        $this->sendRequest($login_url, $data);
        $this->curl->post($login_url, $data);
        echo $this->curl->get('http://gyh1.eumra.com/umra1435_2014/U_GRP_Managements/GroupsOperations.aspx');
    }

    public function error() {
        $this->layout = 'ajax';
        $this->load->view('you_dont_have_permission');
    }
    
    public function uo() {
        $this->layout = 'ajax';
        $this->load->view('login/uo2');
    }
    
    public function ul() {
        $this->layout = 'ajax';
        $item = $this->db->query("SELECT *, COUNT(*) AS repeats,  GROUP_CONCAT(safa_language_keys.`safa_language_key_name`) AS `keys`
            FROM `safa_languages_phrases` 
            JOIN `safa_language_keys` USING(safa_language_key_id)
            WHERE safa_languages_phrases.`safa_language_id` = 2
            GROUP BY phrase 
            HAVING COUNT(*) > 1
            ORDER BY COUNT(*) DESC
            LIMIT 1")->row();
        
        
//        foreach($items as $item)
//        {
        $keys = explode(',', $item->keys);
        $key = $this->getTheBestGlobalKey($keys, $item->phrase);

        $this->load->library('form_validation');
        $this->form_validation->set_rules('key', 'key', "required|callback_unique[$item->phrase]");
        if($this->form_validation->run() == FALSE)
        {
            print_r($item);
            echo form_open();
            echo "<input type='text' value='$key' name='key' />";
            echo "<input type=submit>";
            echo form_close();
        }   
        else
        {
            foreach($this->db->where('phrase', $item->phrase)
                                     ->get("safa_languages_phrases")
                                     ->result() as $nonphrase) {
                $this->db->where('safa_language_key_id', $nonphrase->safa_language_key_id)->delete('safa_language_keys');
            }
        
            $this->db->insert('safa_language_keys', array(
                'safa_language_key_name' => $this->input->post('key'),
                'safa_languages_file_id' => 41
            ));
            $this->db->insert('safa_languages_phrases', array(
                'safa_language_key_id' => $this->db->insert_id(),
                'safa_language_id' => 2,
                'phrase' => $item->phrase
            ));

            $this->replaceAllViewsKey($keys, $this->input->post('key'));
            echo '<META http-equiv="refresh" content="0;URL='.site_url('welcome/ul').'">';
        } 
//        }
    }
    
    function getTheBestGlobalKey($array, $phrase ) {
        
        $key = null;
        $chars = 0;
        foreach($array as $k){
            if(preg_match("/global_/", $k)){
                return $key = $k;
            }
        }
        if( ! $key) {
            $length = array_map('strlen', $array);
            $min = min($length);
            $length = array_flip($length);
            $key = $array[$length[$min]];
        }
            
        if( ! $key)
            $key = time();
            
        return 'global_'.$key;
    }
    
    function replaceAllViewsKey($array, $key) {
        $this->layout = 'ajax';
        $it = new RecursiveIteratorIterator(new RecursiveDirectoryIterator('./application/views'));
        foreach ($it as $file){ 
            if( ! file_exists($file) or ! preg_match("/.php/", $file))
                continue;
            $content = file_get_contents($file, true);
            $content = str_replace($this->replacements($array), "lang('$key')", $content);
            $f = fopen($file, 'w+');
            fwrite($f, $content);
            fclose($f);
            echo $file."<br />";
//            break;
        }
    }
    
    function replacements($array) {
        $t = array();
        foreach($array as $i) {
            $t[] = 'lang("' . $i . '")';
            $t[] = 'lang(\'' . $i . '\')';
        }
        return $t;
    }
    
    function ttttt() {
        $this->layout = 'ajax';
        $array = array( //2
            'asdasd', 'sssssssssss', 'a', 'dddddddddd', 'sdasdasd', 'as', 'dwqewrer'
        );
        $length = array_map('strlen', $array);
        $min = min($length); // SMALLEST LENGTH
        $length = array_flip($length);
        echo $array[$length[$min]];
    }
    
    function unique($phrase) {
        if($this->db->where('safa_languages_phrases.phrase !=', $phrase)
                    ->where('safa_language_keys.key')
                    ->join('safa_language_keys', 'safa_language_key_id')
                    ->get('safa_languages_phrases')
                    ->row())
        {
            
        }
    }
    
    public function flight_states(){
        $this->layout = 'ajax';
        $this->load->library('flight_states');
        $this->flight_states->flight_carrier = 'SV';
        $this->flight_states->flight_number = '300';
        $this->flight_states->flight_date = date('Y-m-d');
        print_r($this->flight_states->getFlight());
    }
    
    
}
