<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class counts extends Safa_Controller { 
    public $module = "trips_reports";

    public function __construct() {
        parent::__construct();

        //Side menu session, By Gouda.
        session('side_menu_id', 1);

        $this->layout = 'new';

        $this->load->model('trips_reports_model');       

        
        
        $this->lang->load('trips_reports');
        $this->load->helper('trip');
        $this->load->helper('form');
        $this->load->helper('array');

        
        permission();

        define('AGENT_USERGROUP', 3);
    }
    
    public function index(){
        $data = array();
        $data['types'] = array(
                'ea'=> lang('ea'),
                'uo'=> lang('uo_companies'),
                'trans'=> lang('trans_type')
            );
        $data['results'] = array();
        $this->load->library("form_validation");
        
        $this->form_validation->set_rules('from', lang('from'), 'required');
	$this->form_validation->set_rules('to', lang('to'), 'required');
        $this->form_validation->set_rules('type', lang('type'), 'required');
        if($this->form_validation->run()) {
            $from = $this->input->post('from');
            $to = $this->input->post('to');
            $type = $this->input->post('type');
            $data['type'] = $type;
            $data['results'] = $this->trips_reports_model->counts($type,$from,$to);
        }
        
        $this->load->view('gov/counts',$data);
    }

}

