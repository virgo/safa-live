<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Hm_users extends Safa_Controller {

    public $module = "safa_hm_users";

    public function __construct() {
        parent::__construct();
        $this->layout = 'new';
        //Side menu session, By Gouda.
        session('side_menu_id', '0');

        $this->load->model('hm_users_model');
        $this->load->model('safa_hm_user_info_model');
        
        $this->lang->load('safa_uo_users');
        $this->lang->load('uo_users');
//        
        permission();
        if (session('hm_id'))
            $this->hm_users_model->safa_hm_id = session('hm_id');
    }

    public function index() {
        if (isset($_GET['search']))
            $this->search();
        $data["total_rows"] = $this->hm_users_model->get(true);
        $this->hm_users_model->offset = $this->uri->segment("4");
        //$this->hm_users_model->limit = $this->config->item('per_page');
        $data["items"] = $this->hm_users_model->get();
        $config['uri_segment'] = 4;
        $config['base_url'] = site_url('hm/hm_users/index');
        $config['total_rows'] = $data["total_rows"];
        $config['per_page'] = $this->config->item('per_page');
        $config['suffix'] = $this->input->get() ? '?' . http_build_query($_GET) : NULL;
        $this->load->library('pagination');
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();
        $this->load->view('hm/hm_users/index', $data);
    }

    public function add() {

    	//$data['safa_uo_user_types'] = ddgen('safa_uo_user_types', array('safa_uo_user_type_id', name()), FALSE, FALSE, FALSE);
    	$data['erp_cities'] = ddgen('erp_cities', array('erp_city_id', name()), array('erp_country_id' => 966), FALSE, TRUE);
		$data['port_types'] = array('all'=>lang('all'), 'air'=>lang('air'), 'sea'=>lang('sea'));
    	
        $this->load->library("form_validation");

        $this->form_validation->set_rules('name_ar', 'lang:uo_users_name_ar', 'trim');
        $this->form_validation->set_rules('name_la', 'lang:uo_users_name_la', 'trim');
        if (name() == 'name_ar')
            $this->form_validation->set_rules('name_ar', 'lang:uo_users_name_ar', 'trim|required');
        else
            $this->form_validation->set_rules('name_la', 'lang:uo_users_name_la', 'trim|required');

        $this->form_validation->set_rules('email', 'lang:uo_users_email', 'trim|valid_email|is_unique[safa_hm_users.email]');
        $this->form_validation->set_rules('mobile', 'lang:uo_users_mobile', 'trim|integer');
        $this->form_validation->set_rules('password', 'lang:uo_users_password', 'trim|required');
        $this->form_validation->set_rules('passconf', 'lang:uo_users_repeat_password', 'trim|required|matches[password]');
        $this->form_validation->set_rules('safa_hm_usergroup_id', 'lang:uo_users_safa_uo_usergroup_id', 'trim|required');

        if ($this->form_validation->run() == false) {
            $this->load->view("hm/hm_users/add", $data);
        } else {

            $this->hm_users_model->name_ar = $this->input->post('name_ar');
            $this->hm_users_model->name_la = $this->input->post('name_la');
            if ($this->input->post('password'))
                $this->hm_users_model->password = md5($this->input->post('password'));

            $this->hm_users_model->email = $this->input->post('email');
            $this->hm_users_model->mobile = $this->input->post('mobile');
            $this->hm_users_model->safa_hm_usergroup_id = $this->input->post('safa_hm_usergroup_id');
            
            if($this->input->post('safa_hm_user_type_id')!='') {
            	$this->hm_users_model->safa_hm_user_type_id = $this->input->post('safa_hm_user_type_id');
            }
            
            $safa_hm_user_id= $this->hm_users_model->save();
            
            if($this->input->post('safa_hm_user_type_id')!='') {
	            $this->safa_hm_user_info_model->safa_hm_user_id = $safa_hm_user_id;
	            if($this->input->post('erp_city_id')) {
	            	$this->safa_hm_user_info_model->erp_city_id = $this->input->post('erp_city_id');
	            }
	            if($this->input->post('safa_hm_user_type_id')==2 || $this->input->post('safa_hm_user_type_id')==3) {
	            	$this->safa_hm_user_info_model->port_type = $this->input->post('port_type');
	            } else {
	            	$this->safa_hm_user_info_model->port_type = null;
	            }
	            $this->safa_hm_user_info_model->save();
            }
            
            
            $this->load->view('redirect_message', array('msg' => lang('global_added_message'),
                'url' => site_url('hm/hm_users/'),
                'model_title' => lang('uo_users'), 'action' => lang('add_uo_users')));
            //redirect("uo/uo_users/index");
        }
    }

    public function edit($id) {
//         if (!permission('uo_users_edit'))
//                    show_message(lang('global_you_dont_have_permission'));
        if (!$id)
            show_404();

        //$data['safa_hm_user_types'] = ddgen('safa_hm_user_types', array('safa_hm_user_type_id', name()), FALSE, FALSE, FALSE);
    	$data['erp_cities'] = ddgen('erp_cities', array('erp_city_id', name()), array('erp_country_id' => 966), FALSE, TRUE);
		$data['port_types'] = array('all'=>lang('all'), 'air'=>lang('air'), 'sea'=>lang('sea'));
    	
		$this->hm_users_model->join = true;
        $this->hm_users_model->safa_hm_user_id = $id;
        $data['item'] = $this->hm_users_model->get();
        if (!$data['item'])
            show_404();
        $this->load->library("form_validation");
        $this->form_validation->set_rules('name_ar', 'lang:uo_users_name_ar', 'trim');
        $this->form_validation->set_rules('name_la', 'lang:uo_users_name_la', 'trim');
        if (name() == 'name_ar')
            $this->form_validation->set_rules('name_ar', 'lang:uo_users_name_ar', 'trim|required');
        else
            $this->form_validation->set_rules('name_la', 'lang:uo_users_name_la', 'trim|required');

        $this->form_validation->set_rules('email', 'lang:uo_users_email', 'trim|valid_email|unique_col[safa_hm_users.email.' . $id . ']');
        $this->form_validation->set_rules('mobile', 'lang:uo_users_mobile', 'trim|integer');
        $this->form_validation->set_rules('password', 'lang:uo_users_password', 'trim');
        $this->form_validation->set_rules('passconf', 'lang:uo_users_repeat_password', 'trim|matches[password]');
        $this->form_validation->set_rules('safa_hm_usergroup_id', 'lang:uo_users_safa_uo_usergroup_id', 'trim|required');

        if ($this->form_validation->run() == false) {
            $this->load->view("hm/hm_users/edit", $data);
        } else {
            $this->hm_users_model->name_ar = $this->input->post('name_ar');
            $this->hm_users_model->name_la = $this->input->post('name_la');
            if ($this->input->post('password'))
                $this->hm_users_model->password = md5($this->input->post('password'));

            $this->hm_users_model->email = $this->input->post('email');
            $this->hm_users_model->mobile = $this->input->post('mobile');
            $this->hm_users_model->safa_hm_usergroup_id = $this->input->post('safa_hm_usergroup_id');
            
            if($this->input->post('safa_hm_user_type_id')!='') {
        		$this->hm_users_model->safa_hm_user_type_id = $this->input->post('safa_hm_user_type_id');
            } else {
            	$this->hm_users_model->safa_hm_user_type_id = null;
            }
            $this->hm_users_model->save();
            
            
            $this->safa_hm_user_info_model->safa_hm_user_id = $id;
        	$safa_uo_user_info_row = $this->safa_uo_user_info_model->get();
        	if(count($safa_uo_user_info_row)>0) {
        		$this->safa_uo_user_info_model->safa_uo_user_info_id = $safa_uo_user_info_row->safa_uo_user_info_id;
        	}
            if($this->input->post('safa_uo_user_type_id')!='') {
	            if($this->input->post('erp_city_id')) {
	            	$this->safa_uo_user_info_model->erp_city_id = $this->input->post('erp_city_id');
	            }
	            if($this->input->post('safa_uo_user_type_id')==2 || $this->input->post('safa_uo_user_type_id')==3) {
	            	$this->safa_uo_user_info_model->port_type = $this->input->post('port_type');
	            } else {
	            	$this->safa_uo_user_info_model->port_type = null;
	            }
	            $this->safa_uo_user_info_model->save();
            }
            
            
            $this->load->view('redirect_message', array('msg' => lang('global_updated_message'),
                'url' => site_url('uo/uo_users/'),
                'id' => $id,
                'model_title' => lang('uo_users'), 'action' => lang('edit_uo_users')));
        }
    }

    function delete($id) {
//        if (!permission('uo_users_delete'))
//                    show_message(lang('global_you_dont_have_permission'));
        if (!$id)
            show_404();

        $this->hm_users_model->safa_hm_user_id = $id;

        if (!$this->hm_users_model->delete())
            show_404();
        redirect("uo/uo_users/index");
    }

    function search() {
//        if (!permission('uo_users_serach'))
//                    show_message(lang('global_you_dont_have_permission'));
        if ($this->input->get("safa_hm_user_id"))
            $this->hm_users_model->safa_hm_user_id = $this->input->get("safa_hm_user_id");
        if ($this->input->get("name_ar"))
            $this->hm_users_model->name_ar = $this->input->get("name_ar");
        if ($this->input->get("name_la"))
            $this->hm_users_model->name_la = $this->input->get("name_la");
        if ($this->input->get("username"))
            $this->hm_users_model->username = $this->input->get("username");
        if ($this->input->get("password"))
            $this->hm_users_model->password = $this->input->get("password");
        if ($this->input->get("safa_hm_id"))
            $this->hm_users_model->safa_hm_id = $this->input->get("safa_hm_id");
        if ($this->input->get("safa_hm_usergroup_id"))
            $this->hm_users_model->safa_hm_usergroup_id = $this->input->get("safa_hm_usergroup_id");
    }

}

/* End of file safa_uo_users.php */
/* Location: ./application/controllers/safa_uo_users.php */
