<?php

class Hotel_availability extends Safa_Controller {

	public function __construct() {
		parent::__construct();
		$this->layout = 'new';
		if (session('uo_id')) {
			session('side_menu_id', 5);
		} else if (session('ea_id')) {
			session('side_menu_id', 3);
		} else if (session('hm_id')) {
			session('side_menu_id', 1);
		}

		$this->lang->load('hotel_availability');
		$this->load->helper('hotel_availability');
		$this->load->model('hotels_availability_room_details_model');
		permission();
	}

	function reserve($save = false) {
		$this->layout = 'ajax';
		$this->load->model('hotels_availability_model');
		$this->load->model('hotels_availability_details_model');
		$this->load->model('notification_model');
		$data = array();
		if ($save) {
			$this->load->model('erp_hotels_reservation_orders_model');
			$datas = json_decode($this->input->post('roomcount'));
			$fromdate = json_decode($this->input->post('fromdate'), TRUE);
			$todate = json_decode($this->input->post('todate'), TRUE);
			$customertype = $this->input->post('customertype');
			$systemcustomertype = $this->input->post('systemcustomertype');
			$systemcustomerid = $this->input->post('systemcustomerid');

			foreach ($datas as $key => $value) {
				$add_result = false;
				foreach ($value as $detail_id => $roomcount) {
					if ($roomcount)
					$add_result = true;
				}
				if ($add_result) {
					$availabilityarray = array();
					$this->hotels_availability_model->erp_hotels_availability_master_id = $key;
					$this->hotels_availability_model->join = TRUE;
					$availability = $this->hotels_availability_model->get();

					if (isset($customertype[$key])) {
						if ($customertype[$key] == 1) {
							$this->erp_hotels_reservation_orders_model->owner_erp_company_types_id = NULL;
							$this->erp_hotels_reservation_orders_model->owner_erp_company_id = NULL;
						} else {
							$this->erp_hotels_reservation_orders_model->owner_erp_company_types_id = $systemcustomertype[$key];
							$this->erp_hotels_reservation_orders_model->owner_erp_company_id = $systemcustomerid[$key];
						}
					} else {
						if (session('uo_id')) {
							$this->erp_hotels_reservation_orders_model->owner_erp_company_types_id = 2;
							$this->erp_hotels_reservation_orders_model->owner_erp_company_id = session('uo_id');
						} else if (session('ea_id')) {
							$this->erp_hotels_reservation_orders_model->owner_erp_company_types_id = 3;
							$this->erp_hotels_reservation_orders_model->owner_erp_company_id = session('ea_id');
						} else if (session('hm_id')) {
							$this->erp_hotels_reservation_orders_model->owner_erp_company_types_id = 5;
							$this->erp_hotels_reservation_orders_model->owner_erp_company_id = session('hm_id');
						}
					}

					$this->erp_hotels_reservation_orders_model->erp_company_types_id = $availability->erp_company_type_id;
					$this->erp_hotels_reservation_orders_model->erp_company_id = $availability->safa_company_id;

					$this->erp_hotels_reservation_orders_model->erp_cities_id = $availability->erp_city_id;
					$this->erp_hotels_reservation_orders_model->erp_hotels_id = $availability->erp_hotel_id;
					$this->erp_hotels_reservation_orders_model->order_date = date('Y-m-d');

					$this->erp_hotels_reservation_orders_model->order_status = '3';
					$res_id = $this->erp_hotels_reservation_orders_model->save();
					foreach ($value as $detail_id => $roomcount) {
						if ($roomcount) {
							$this->hotels_availability_details_model->erp_hotels_availability_detail_id = $detail_id;
							$this->hotels_availability_details_model->join = TRUE;
							$details = $this->hotels_availability_details_model->get();
							$this->erp_hotels_reservation_orders_model->rooms_erp_hotels_reservation_orders_id = $res_id;
							$this->erp_hotels_reservation_orders_model->rooms_erp_housingtypes_id = $details->erp_housingtype_id;
							$this->erp_hotels_reservation_orders_model->rooms_erp_hotelroomsizes_id = $details->erp_hotelroomsize_id;
							$this->erp_hotels_reservation_orders_model->rooms_erp_currencies_id = $availability->sale_currency_id;
							$this->erp_hotels_reservation_orders_model->rooms_rooms_count = $roomcount;
							$this->erp_hotels_reservation_orders_model->rooms_erp_cities_id = $availability->erp_city_id;
							$this->erp_hotels_reservation_orders_model->rooms_erp_hotels_id = $availability->erp_hotel_id;

							$this->erp_hotels_reservation_orders_model->rooms_price_from = $details->sale_price;
							$this->erp_hotels_reservation_orders_model->rooms_entry_date = $fromdate[$key][$detail_id];
							$this->erp_hotels_reservation_orders_model->rooms_exit_date = $todate[$key][$detail_id];
							$this->erp_hotels_reservation_orders_model->saveRooms();
						}
					}

					$msg_datetime = date('Y-m-d H:i', time());
					$this->notification_model->notification_type = 'automatic';
					$this->notification_model->erp_importance_id = 1;
					$this->notification_model->sender_type_id = 1;
					$this->notification_model->sender_id = 0;
					$this->notification_model->erp_system_events_id = 16;
					$this->notification_model->language_id = 2;
					$this->notification_model->msg_datetime = $msg_datetime;
					$this->notification_model->tags = '';
					$this->notification_model->parent_id = '';
					$erp_notification_id = $this->notification_model->save();

					//-------- Notification Details ---------------
					$this->notification_model->detail_erp_notification_id = $erp_notification_id;
					if (isset($customertype[$key])) {
						if ($customertype[$key] == 1) {
							$this->notification_model->detail_receiver_type = NULL;
							$this->notification_model->detail_receiver_id = NULL;
						} else {
							$this->notification_model->detail_receiver_type = $systemcustomertype[$key];
							$this->notification_model->detail_receiver_id = $systemcustomerid[$key];
							$this->notification_model->saveDetails();
						}
					} else {
						$this->notification_model->detail_receiver_type = $availability->erp_company_type_id;
						$this->notification_model->detail_receiver_id = $availability->safa_company_id;
						$this->notification_model->saveDetails();
					}
				}
			}
			redirect('hotels_reservation_orders');
		} else {
			$datas = $this->input->post('roomcount');
			$data['datas'] = json_encode($datas);
			$avcount = $this->input->post('avcount');
			$data['fromdate'] = json_encode($this->input->post('fromdate'));
			$data['todate'] = json_encode($this->input->post('todate'));
			$data['companies'] = ddgen('safa_uos', array('safa_uo_id', name()), FALSE, FALSE, TRUE);
			foreach ($datas as $key => $value) {
				$availabilityarray = array();
				$add_result = false;
				$this->hotels_availability_model->erp_hotels_availability_master_id = $key;
				$this->hotels_availability_model->join = TRUE;
				$availabilityarray['availability'] = $this->hotels_availability_model->get();
				foreach ($value as $detail_id => $roomcount) {
					if ($roomcount)
					$add_result = TRUE;
					$this->hotels_availability_details_model->erp_hotels_availability_detail_id = $detail_id;
					$this->hotels_availability_details_model->join = TRUE;
					if ($roomcount) {
						$availabilityarray['avdetails'][] = array(
                            'details' => $this->hotels_availability_details_model->get(),
                            'available' => $avcount[$key][$detail_id],
                            'count' => $roomcount
						);
					}
				}
				if ($add_result)
				$data['results'][] = $availabilityarray;
			}
			$data['co_type'] = $this->system_id;
			$data['co_id'] = $this->{$this->destination . '_id'};
			$this->load->view('hotel_availability/reserve', $data);
		}
	}

	public function ajax_get_companies() {
		if (!$this->input->post('type'))
		die();
		$this->layout = 'ajax';
		$company = 0;
		$ctype = 0;
		if (session('uo_id')) {
			$ctype = 2;
			$company = session('uo_id');
		} else if (session('ea_id')) {
			$ctype = 3;
			$company = session('ea_id');
		} else if (session('hm_id')) {
			$ctype = 5;
			$company = session('hm_id');
		}
		$companiesarray = array();
		if ($this->input->post('type') == 2) {
			$companies = $this->db->get('safa_uos')->result();
			foreach ($companies as $company) {
				if ($company == $company->safa_uo_id && $ctype == 2)
				continue;
				$companiesarray[] = array('id' => $company->safa_uo_id, 'name' => $company->{name()});
			}
		} elseif($this->input->post('type') == 3) {
			$companies = $this->db->get('safa_eas')->result();
			foreach ($companies as $company) {
				if ($company == $company->safa_ea_id && $ctype == 3)
				continue;
				$companiesarray[] = array('id' => $company->safa_ea_id, 'name' => $company->{name()});
			}
		} elseif($this->input->post('type') == 5) {
			$companies = $this->db->get('safa_hms')->result();
			foreach ($companies as $company) {
				if ($company == $company->safa_hm_id && $ctype == 5)
				continue;
				$companiesarray[] = array('id' => $company->safa_hm_id, 'name' => $company->{name()});
			}
		}


		echo json_encode($companiesarray);
	}

	public function index() {
		$this->load->model('hotels_availability_model');
		$data = array('module' => 'hotel_availability');
		if (session("uo_id")) {
			$companyid = session("uo_id");
			$typeid = 2;
		} elseif (session("ea_id")) {
			$companyid = session("ea_id");
			$typeid = 3;
		} elseif (session("hm_id")) {
			$companyid = session("hm_id");
			$typeid = 5;
		}

		$this->hotels_availability_model->safa_company_id = $companyid;
		$this->hotels_availability_model->erp_company_type_id = $typeid;
		$this->hotels_availability_model->join = TRUE;
		$this->hotels_availability_model->order_by = array('erp_hotels_availability_master_id', 'DESC');
		$data['items'] = $this->hotels_availability_model->get();

		$this->load->view('hotel_availability/index', $data);
	}

	public function search() {
		$this->load->model('hotel_availability_search_model');
		$data['countries'] = ddgen('erp_countries', array('erp_country_id', name()));
		$data['cities'] = ddgen('erp_cities', array('erp_city_id', name()), array('erp_country_id' => 966), FALSE, TRUE);
		//$data['hotels'] = array(); //ddgen('erp_hotels', array('erp_hotel_id', name()));

		$this->load->model('cities_model');
		$this->cities_model->erp_country_id = 966;
		$KSA_cities_rows = $this->cities_model->get();
		if (count($KSA_cities_rows) > 0) {
			$data['hotels'] = ddgen('erp_hotels', array('erp_hotel_id', name()), array('erp_city_id' => $KSA_cities_rows[0]->erp_city_id), FALSE, TRUE);
		} else {
			$data['hotels'] = array();
		}

		$data['housing'] = ddgen('erp_housingtypes', array('erp_housingtype_id', name()), FALSE, FALSE, lang('global_all'));
		$data['additions'] = ddgen('erp_room_services', array('erp_room_service_id', name()), FALSE, FALSE, TRUE);
		$data['nationalities'] = ddgen('erp_nationalities', array('erp_nationality_id', name()), FALSE, FALSE, TRUE);
		$data['hotel_advantages'] = ddgen('erp_hotel_advantages', array('erp_hotel_advantages_id', name()), FALSE, FALSE, TRUE);
		$data['room_types'] = ddgen('erp_hotelroomsizes', array('erp_hotelroomsize_id', name()), FALSE, FALSE, lang('global_all'));
		$data['currencies'] = ddgen('erp_currencies', array('erp_currency_id', name()), FALSE, FALSE, lang('global_all'));
		$data['levels'] = ddgen('erp_hotellevels', array('erp_hotellevel_id', name()), FALSE, FALSE, TRUE);
		$data['items'] = array();
		$this->load->library('form_validation');
		$this->form_validation->set_rules('search[]', '', 'required');
		if ($this->form_validation->run() == false) {
			$this->load->view('hotel_availability/search_index', $data);
		} else {

			//By Gouda, To filter by contracts options for this EA with its UO.
			if (session("ea_id")) {
				$this->load->model('contracts_model');
				$this->contracts_model->safa_ea_id = session("ea_id");
				$contract_rows = $this->contracts_model->search();
				if (count($contract_rows) > 0) {
					if ($contract_rows[0]->services_buy_other_hotel != 1) {
						$contract_safa_uo_id = $contract_rows[0]->safa_uo_id;

						$this->hotel_availability_search_model->erp_company_type_id = 2;
						$this->hotel_availability_search_model->safa_company_id = $contract_safa_uo_id;
					}
				}
			}


			$attributes = array(
                'city_id',
                'hotel_id',
                'rooms',
                'nights',
                'date_from',
                'date_to',
                'housing_type_id',
                'room_type_id',
                'price_from',
                'price_to',
                'currency',
                'distance_from_haram',
                'distance_to_haram',
                'level',
                'nationalities',
                'hotel_features',
                'inclusive_service',
                'exclusive_service',
                'ha_type');
			if ($this->input->post('date_from'))
			$this->hotel_availability_search_model->main_date_from = $this->input->post('date_from');
			if ($this->input->post('date_to'))
			$this->hotel_availability_search_model->main_date_to = $this->input->post('date_to');
			foreach ($this->input->post('search') as $key => $search) {
				parse_str($search, $output);
				foreach ($attributes as $attribute)
				if (isset($output[$attribute]) && !empty($output[$attribute]))
				$this->hotel_availability_search_model->$attribute = $output[$attribute];

				//print_r($this->hotel_availability_search_model);
				//die();
				//                $this->hotel_availability_search_model->city_id = $output['city_id'];
				//                $this->hotel_availability_search_model->hotel_id = $output['hotel_id']; // ARRAY
				//                $this->hotel_availability_search_model->rooms = $output['rooms'];
				//                $this->hotel_availability_search_model->nights = $output['nights'];
				//                $this->hotel_availability_search_model->date_from = $output['date_from'];
				//                $this->hotel_availability_search_model->date_to = $output['date_to'];
				//                $this->hotel_availability_search_model->housing_type_id = $output['housing_type_id'];
				//                $this->hotel_availability_search_model->room_type_id = $output['room_type_id'];
				//                $this->hotel_availability_search_model->price_from = $output['price_from'];
				//                $this->hotel_availability_search_model->price_to = $output['price_to'];
				//                $this->hotel_availability_search_model->currency = $output['currency'];
				//                $this->hotel_availability_search_model->distance_from_haram = $output['distance_from_haram'];
				//                $this->hotel_availability_search_model->distance_to_haram = $output['distance_to_haram'];
				//                $this->hotel_availability_search_model->level = $output['level']; // ARRAY
				//                $this->hotel_availability_search_model->nationalities = $output['nationalities']; // ARRAY
				//                $this->hotel_availability_search_model->hotel_features = $output['hotel_features']; // ARRAY
				//                $this->hotel_availability_search_model->inclusive_service = $output['inclusive_service']; // ARRAY
				//                $this->hotel_availability_search_model->exclusive_service = $output['exclusive_service']; // ARRAY

				$data['results'][$key] = $this->hotel_availability_search_model->get();
			}

			//print_r($data['results']);

			$data['co_type'] = $this->system_id;
			$data['co_id'] = $this->{$this->destination . '_id'};
			$this->load->view('hotel_availability/result', $data);
		}
	}

	public function search_popup_for_trip($trip_id = 0) {
		$this->layout = 'js';
		$this->load->model('hotel_availability_search_model');
		$this->load->model('hotels_model');


		$data['countries'] = ddgen('erp_countries', array('erp_country_id', name()));
		$data['cities'] = ddgen('erp_cities', array('erp_city_id', name()), array('erp_country_id' => 966));
		//$data['hotels'] = ddgen('erp_hotels', array('erp_hotel_id', name()), array('erp_country_id' => 966));

		$structure = array('erp_hotel_id', name());
		$key = $structure['0'];
		if (isset($structure['1'])) {
			$value = $structure['1'];
		}
		$hotels_arr = array();
		$hotels = $this->hotels_model->get_by_country(966);
		foreach ($hotels as $hotel) {
			$hotels_arr[$hotel->$key] = $hotel->$value;
		}
		$data['hotels'] = $hotels_arr;


		$this->load->model('trip_hotel_model');
		$trip_hotel_rows = $this->trip_hotel_model->get_trip_hotels_by_trip_id($trip_id);
		$data['trip_hotel_rows'] = $trip_hotel_rows;
		$data['trip_hotel_count'] = count($trip_hotel_rows);

		$data["ksa_cities"] = ddgen("erp_cities", array("erp_city_id", name()), array('erp_country_id' => '966'));
		$data["erp_meals"] = ddgen("erp_meals", array("erp_meal_id", name()));


		$data['items'] = array();

		//By Gouda, To filter by contracts options for this EA with its UO.
		if (session("ea_id")) {
			$this->load->model('contracts_model');
			$this->contracts_model->safa_ea_id = session("ea_id");
			$contract_rows = $this->contracts_model->search();
			if (count($contract_rows) > 0) {
				if ($contract_rows[0]->services_buy_other_hotel != 1) {
					$contract_safa_uo_id = $contract_rows[0]->safa_uo_id;

					$this->hotel_availability_search_model->erp_company_type_id = 2;
					$this->hotel_availability_search_model->safa_company_id = $contract_safa_uo_id;
				}
			}
		}

		if (!$this->input->get('search')) {
			$data['results'] = $this->hotel_availability_search_model->get_rooms_details();
		} else {
			$attributes = array(
                'city_id',
                'hotel_id',
                'date_from',
                'date_to');

			if ($this->input->get('city_id')) {
				$this->hotel_availability_search_model->city_id = $this->input->get('city_id');
			} if ($this->input->get('hotel_id')) {
				$this->hotel_availability_search_model->hotel_id = $this->input->get('hotel_id');
			} if ($this->input->get('date_from')) {
				$this->hotel_availability_search_model->date_from = $this->input->get('date_from');
			} if ($this->input->get('date_to')) {
				$this->hotel_availability_search_model->date_to = $this->input->get('date_to');
			}

			$this->hotel_availability_search_model->rooms = true;

			$data['results'] = $this->hotel_availability_search_model->get_rooms_details();
		}

		$this->load->view('hotel_availability/search_popup_for_trip', $data);
	}

	public function calendar() {
		$data['results'] = array();
		$data['hotels'] = ddgen('erp_hotels', array('erp_hotel_id', name()), FALSE, FALSE, TRUE);
		$this->load->library('form_validation');
		$this->form_validation->set_rules('filter', '', 'required');
		if ($this->form_validation->run()) {

			if (session("uo_id")) {
				$companyid = session("uo_id");
				$typeid = 2;
			} elseif (session("ea_id")) {
				$companyid = session("ea_id");
				$typeid = 3;
			} elseif (session("hm_id")) {
				$companyid = session("hm_id");
				$typeid = 5;
			} else {
				$companyid = 18;
				$typeid = 2;
			}
			$filter = array();
			$filter['safa_company_id'] = $companyid;
			$filter['erp_company_type_id'] = $typeid;
			$startdata = '';
			$enddate = '';
			$this->load->library('hijrigregorianconvert');
			if ($this->input->post('datetype') == 'H') {
				if ($this->input->post('datefrom')) {
					$startdata = $this->hijrigregorianconvert->HijriToGregorian($this->input->post('datefrom'), 'DD-MM-YYYY');
					$filter['date_from >='] = $this->hijrigregorianconvert->HijriToGregorian($this->input->post('datefrom'), 'DD-MM-YYYY');
				}
				if ($this->input->post('dateto')) {
					$enddate = $this->hijrigregorianconvert->HijriToGregorian($this->input->post('dateto'), 'DD-MM-YYYY');
					$filter['date_to <='] = $this->hijrigregorianconvert->HijriToGregorian($this->input->post('dateto'), 'DD-MM-YYYY');
				}
			} else {
				if ($this->input->post('datefrom')) {
					$startdata = $this->input->post('datefrom');
					$filter['date_from >='] = $this->input->post('datefrom');
				}
				if ($this->input->post('dateto')) {
					$enddate = $this->input->post('dateto');
					$filter['date_to <='] = $this->input->post('dateto');
				}
			}
			$this->db->where($filter);
			$data['startdate'] = $startdata;
			$data['enddate'] = $enddate;
			if ($this->input->post('hotels'))
			$this->db->where_in('erp_hotel_id', $this->input->post('hotels'));

			$hotels_masters = $this->db->get('erp_hotels_availability_master')->result();
			foreach ($hotels_masters as $hotel_master) {
				if (!isset($data['results'][$hotel_master->erp_hotel_id])) {
					$hotel = $this->db->where('erp_hotel_id', $hotel_master->erp_hotel_id)->get('erp_hotels')->row();
					$data['results'][$hotel_master->erp_hotel_id] = array('hotel' => $hotel);
					$this->db->select('erp_hotels_availability_rooms.erp_hotelroomsize_id,erp_hotels_availability_sub_rooms.erp_hotels_availability_sub_room_id,erp_hotels_availability_sub_rooms.erp_floor_id,erp_floors.' . name() . '');
					$this->db->where('erp_hotels_availability_master_id', $hotel_master->erp_hotels_availability_master_id);
					$this->db->join('erp_hotels_availability_sub_rooms', 'erp_hotels_availability_sub_rooms.erp_hotels_availability_rooms_id = erp_hotels_availability_rooms.erp_hotels_availability_rooms_id');
					$this->db->join('erp_floors', 'erp_hotels_availability_sub_rooms.erp_floor_id = erp_floors.erp_floor_id');
					$rooms = $this->db->get('erp_hotels_availability_rooms')->result();

					$hotelrooms = array();
					foreach ($rooms as $room) {
						$numdays = $this->db->where(array(
                                    'erp_hotels_availability_sub_room_id' => $room->erp_hotels_availability_sub_room_id,
                                    'to_date >=' => $startdata,
                                    'from_date <' => $enddate
						))->order_by('number', 'ASC')->order_by('erp_hotels_availability_room_detail_id')->get('erp_hotels_availability_room_details')->result();
						//echo $this->db->last_query();
						$hotelrooms[$room->erp_hotels_availability_sub_room_id]['floor'] = $room;
						$roomnum = '';
						$daystatus = array();
						//print_r($numdays);echo "<br /><br />";
						foreach ($numdays as $numday) {
							if ($roomnum != $numday->number)
							$roomnum = $numday->number;
							for ($datetime = strtotime($startdata); $datetime <= strtotime($enddate); $datetime += (3600 * 24)) {
								if ($datetime >= strtotime($numday->from_date) && $datetime <= strtotime($numday->to_date))
								$daystatus[$roomnum][date('d', $datetime)] = $numday->status;
							}
						}
						//print_r($daystatus);echo "<br /><br />";die();
						$hotelrooms[$room->erp_hotels_availability_sub_room_id]['room'] = $daystatus;
					}

					$data['results'][$hotel_master->erp_hotel_id]['rooms'] = $hotelrooms;
				}
			}
		}

		//die();
		$this->load->view('hotel_availability/avialibilty_calender', $data);
	}

	public function calendar_advanced() {
		$data['results'] = array();
		$data['cities'] = ddgen('erp_cities', array('erp_city_id', name()), array('erp_country_id' => 966));
		$data['hotels'] = ddgen('erp_hotels', array('erp_hotel_id', name()), FALSE, FALSE, TRUE);
		$this->load->library('form_validation');
		$this->form_validation->set_rules('filter', '', 'required');
		if ($this->form_validation->run()) {

			if (session("uo_id")) {
				$companyid = session("uo_id");
				$typeid = 2;
			} elseif (session("ea_id")) {
				$companyid = session("ea_id");
				$typeid = 3;
			} elseif (session("hm_id")) {
				$companyid = session("hm_id");
				$typeid = 5;
			} else {
				$companyid = 18;
				$typeid = 2;
			}
			$filter = array();
			$filter['safa_company_id'] = $companyid;
			$filter['erp_company_type_id'] = $typeid;
			$startdata = '';
			$enddate = '';
			$this->load->library('hijrigregorianconvert');
			if ($this->input->post('datetype') == 'H') {
				if ($this->input->post('datefrom')) {
					$startdata = $this->hijrigregorianconvert->HijriToGregorian($this->input->post('datefrom'), 'DD-MM-YYYY');
					$filter['date_from >='] = $this->hijrigregorianconvert->HijriToGregorian($this->input->post('datefrom'), 'DD-MM-YYYY');
				}
				if ($this->input->post('dateto')) {
					$enddate = $this->hijrigregorianconvert->HijriToGregorian($this->input->post('dateto'), 'DD-MM-YYYY');
					$filter['date_to <='] = $this->hijrigregorianconvert->HijriToGregorian($this->input->post('dateto'), 'DD-MM-YYYY');
				}
			} else {
				if ($this->input->post('datefrom')) {
					$startdata = $this->input->post('datefrom');
					$filter['date_from >='] = $this->input->post('datefrom');
				}
				if ($this->input->post('dateto')) {
					$enddate = $this->input->post('dateto');
					$filter['date_to <='] = $this->input->post('dateto');
				}
			}
			$this->db->where($filter);
			$data['startdate'] = $startdata;
			$data['enddate'] = $enddate;
			if ($this->input->post('hotels'))
			$this->db->where_in('erp_hotel_id', $this->input->post('hotels'));

			$hotels_masters = $this->db->get('erp_hotels_availability_master')->result();
			foreach ($hotels_masters as $hotel_master) {
				if (!isset($data['results'][$hotel_master->erp_hotel_id])) {
					$hotel = $this->db->where('erp_hotel_id', $hotel_master->erp_hotel_id)->get('erp_hotels')->row();
					$data['results'][$hotel_master->erp_hotel_id] = array('hotel' => $hotel);
					$this->db->select('erp_hotels_availability_rooms.erp_hotelroomsize_id,erp_hotels_availability_sub_rooms.erp_hotels_availability_sub_room_id,erp_hotels_availability_sub_rooms.erp_floor_id,erp_floors.' . name() . '');
					$this->db->where('erp_hotels_availability_master_id', $hotel_master->erp_hotels_availability_master_id);
					$this->db->join('erp_hotels_availability_sub_rooms', 'erp_hotels_availability_sub_rooms.erp_hotels_availability_rooms_id = erp_hotels_availability_rooms.erp_hotels_availability_rooms_id');
					$this->db->join('erp_floors', 'erp_hotels_availability_sub_rooms.erp_floor_id = erp_floors.erp_floor_id');
					$rooms = $this->db->get('erp_hotels_availability_rooms')->result();

					$hotelrooms = array();
					foreach ($rooms as $room) {
						$numdays = $this->db->where(array(
                                    'erp_hotels_availability_sub_room_id' => $room->erp_hotels_availability_sub_room_id,
                                    'to_date >=' => $startdata,
                                    'from_date <' => $enddate
						))->order_by('number')->get('erp_hotels_availability_room_details')->result();
						$hotelrooms[$room->erp_hotels_availability_sub_room_id]['floor'] = $room;
						$roomnum = '';
						$daystatus = array();
						foreach ($numdays as $numday) {
							if ($roomnum != $numday->number)
							$roomnum = $numday->number;
							for ($datetime = strtotime($startdata); $datetime <= strtotime($enddate); $datetime += (3600 * 24)) {
								if ($datetime >= strtotime($numday->from_date) && $datetime <= strtotime($numday->to_date))
								$daystatus[$roomnum][date('d', $datetime)] = $numday->status;
							}
						}
						$hotelrooms[$room->erp_hotels_availability_sub_room_id]['room'] = $daystatus;
					}

					$data['results'][$hotel_master->erp_hotel_id]['rooms'] = $hotelrooms;
				}
			}
		}
		$this->load->view('hotel_availability/avialibilty_calendar_advanced', $data);
	}

	public function ajax_calendar() {
		$this->layout = 'ajax';
		if (session("uo_id")) {
			$companyid = session("uo_id");
			$typeid = 2;
		} elseif (session("ea_id")) {
			$companyid = session("ea_id");
			$typeid = 3;
		} elseif (session("hm_id")) {
			$companyid = session("hm_id");
			$typeid = 5;
		} else {
			$companyid = 18;
			$typeid = 2;
		}

		$filter = array();
		$filter['safa_company_id'] = $companyid;
		$filter['erp_company_type_id'] = $typeid;
		$startdata = '';
		$enddate = false;
		$this->load->library('hijrigregorianconvert');
		if ($this->input->post('erp_cities_id')) {
			$datefrom = $this->input->post('rooms_entry_date');
			$roomskey = key($datefrom);

			$dateto = $this->input->post('rooms_exit_date');
			$filter['date_from >='] = $datefrom[$roomskey];
			$startdata = $datefrom[$roomskey];
			$enddate = $dateto[$roomskey];
			$filter['erp_hotel_id'] = $this->input->post('erp_hotels_id');
			$this->db->where($filter);
		} else {

			if ($this->input->post('datetype') == 'H') {
				if ($this->input->post('datefrom')) {
					$startdata = $this->hijrigregorianconvert->HijriToGregorian($this->input->post('datefrom'), 'DD-MM-YYYY');
					$filter['date_from >='] = $this->hijrigregorianconvert->HijriToGregorian($this->input->post('datefrom'), 'DD-MM-YYYY');
				}
				if ($this->input->post('dateto')) {
					$enddate = $this->hijrigregorianconvert->HijriToGregorian($this->input->post('dateto'), 'DD-MM-YYYY');
					//$filter['date_to <='] = $this->hijrigregorianconvert->HijriToGregorian($this->input->post('dateto'),'DD-MM-YYYY');
				}
			} else {
				if ($this->input->post('datefrom')) {
					$startdata = $this->input->post('datefrom');
					//$filter['date_from >='] = $this->input->post('datefrom');
				}
				if ($this->input->post('dateto')) {
					$enddate = $this->input->post('dateto');
					//$filter['date_to <='] = $this->input->post('dateto');
				}
			}
			if (!$enddate) {
				$enddate = date('Y-m-d', strtotime($startdata) + (3600 * 24 * 29));
				//$filter['date_to <='] = $enddate;
			}
			$this->db->where($filter);
			if ($this->input->post('hotels'))
			$this->db->where_in('erp_hotel_id', $this->input->post('hotels'));
		}

		$data['startdate'] = $startdata;
		$data['enddate'] = $enddate;

		$hotels_masters = $this->db->get('erp_hotels_availability_master')->result();
		//print_r($hotels_masters);
		$data['results'] = array();
		foreach ($hotels_masters as $hotel_master) {
			if (!isset($data['results'][$hotel_master->erp_hotel_id])) {
				$hotel = $this->db->where('erp_hotel_id', $hotel_master->erp_hotel_id)->get('erp_hotels')->row();
				$data['results'][$hotel_master->erp_hotel_id] = array('hotel' => $hotel);
				$this->db->select('erp_hotels_availability_rooms.erp_hotelroomsize_id,erp_hotels_availability_sub_rooms.erp_hotels_availability_sub_room_id,erp_hotels_availability_sub_rooms.erp_floor_id,erp_floors.' . name() . '');
				$this->db->where('erp_hotels_availability_master_id', $hotel_master->erp_hotels_availability_master_id);
				if ($this->input->post('erp_cities_id')) {
					$rooms_erp_hotelroomsizes_id = $this->input->post('rooms_erp_hotelroomsizes_id');
					$roomkey = key($rooms_erp_hotelroomsizes_id);
					$this->db->where('erp_hotelroomsize_id', $rooms_erp_hotelroomsizes_id[$roomkey]);
				}
				$this->db->join('erp_hotels_availability_sub_rooms', 'erp_hotels_availability_sub_rooms.erp_hotels_availability_rooms_id = erp_hotels_availability_rooms.erp_hotels_availability_rooms_id');
				$this->db->join('erp_floors', 'erp_hotels_availability_sub_rooms.erp_floor_id = erp_floors.erp_floor_id');
				$rooms = $this->db->get('erp_hotels_availability_rooms')->result();

				$hotelrooms = array();
				foreach ($rooms as $room) {
					$numdays = $this->db->where(array(
                                'erp_hotels_availability_sub_room_id' => $room->erp_hotels_availability_sub_room_id,
                                'to_date >=' => $startdata,
                                'from_date <' => $enddate
					))->order_by('number')->get('erp_hotels_availability_room_details')->result();
					$hotelrooms[$room->erp_hotels_availability_sub_room_id]['floor'] = $room;
					$roomnum = '';
					$daystatus = array();
					foreach ($numdays as $numday) {
						if ($roomnum != $numday->number)
						$roomnum = $numday->number;
						for ($datetime = strtotime($startdata); $datetime <= strtotime($enddate); $datetime += (3600 * 24)) {
							if ($datetime >= strtotime($numday->from_date) && $datetime <= strtotime($numday->to_date))
							$daystatus[$roomnum][date('d', $datetime)] = $numday->status;
						}
					}
					$hotelrooms[$room->erp_hotels_availability_sub_room_id]['room'] = $daystatus;
				}

				$data['results'][$hotel_master->erp_hotel_id]['rooms'] = $hotelrooms;
			}
		}
		$this->load->view('hotel_availability/calender_result', $data);
	}

	public function manage($id = FALSE) 
	{
		$this->load->model('hotels_availability_model');
		$this->load->model('hotels_nationalities_model');
		// ADDITIONS [ ROOM SERVICES ]
		$this->load->model('room_services_model');
		$this->load->model('room_inclusive_services_model');
		// ROOMS
		$this->load->model('hotels_availability_rooms_model');
		// DETAILS
		$this->load->model('hotels_availability_details_model');
		// ADDITIONAL SERVICES
		$this->load->model('room_exclusive_services_model');
		// VALIDATION LIBRARY
		$this->load->library('form_validation');

		/**
		 * =====================================================================
		 * DEFAULT VALUES
		 * =====================================================================
		 */
		$data['main'] = new stdClass;
		$data['main']->erp_country_id = 966;
		$data['main']->erp_city_id = 1;
		$data['main']->erp_hotel_id = null;
		$data['main']->season_id = null;
		$data['main']->date_from = null;
		$data['main']->date_to = null;
		$data['main']->purchase_currency_id = 3;
		$data['main']->sale_currency_id = 3;
		$data['main']->invoice_number = null;
		$data['main']->notes = '';
		$data['main']->availability_status = 2;
		$data['selected_nationalities'] = array();
		$data['selected_additions'] = array();
		$data['exclusive'] = array();
		$data['rooms'] = array();
		$data['details'] = array();

		/**
		 * =====================================================================
		 * GET FORM DATA FROM DATABASE
		 * =====================================================================
		 */
		if ($id) {
			$this->hotels_nationalities_model->erp_hotels_availability_master_id = $id;
			$this->hotels_availability_rooms_model->erp_hotels_availability_master_id = $id;
			$this->room_inclusive_services_model->erp_hotels_availability_master_id = $id;
			$this->room_exclusive_services_model->erp_hotels_availability_master_id = $id;
			$this->hotels_availability_model->erp_hotels_availability_master_id = $id;
			$this->hotels_availability_details_model->erp_hotels_availability_master_id = $id;

			$additions = $this->room_inclusive_services_model->get();
			$nationalities = $this->hotels_nationalities_model->get();
			$data['main'] = $this->hotels_availability_model->get();
			$data['rooms'] = $this->hotels_availability_rooms_model->get();
			$data['details'] = $this->hotels_availability_details_model->get();
			$data['exclusive'] = $this->room_exclusive_services_model->get();
			$data['peroids'] = $this->db->where('erp_hotels_availability_master_id', $id)->get('erp_hotels_availability_peroids')->result();
			if (!$data['main'])
			show_404();
			foreach ($nationalities as $nationality)
			$data['selected_nationalities'][] = $nationality->erp_nationality_id;

			foreach ($additions as $addition)
			$data['selected_additions'][] = $addition->erp_room_service_id;
		}

		/**
		 * =====================================================================
		 * DROPDOWNS
		 * =====================================================================
		 */
		// MAIN DROPDOWNS
		$data['currencies'] = ddgen('erp_currencies', array('erp_currency_id', name()));
		$data['countries'] = ddgen('erp_countries', array('erp_country_id', name()));
		$data['uo_companies'] = ddgen('safa_uos', array('safa_uo_id', name()), FALSE, FALSE, TRUE);
		$data['hm_companies'] = ddgen('safa_hms', array('safa_hm_id', name()), FALSE, FALSE, TRUE);
		$data['ea_countries'] = ddgen('erp_countries', array('erp_country_id', name()), FALSE, FALSE, TRUE);

		$data['cities'] = array(); //ddgen('erp_cities', array('erp_city_id', name()));
		$data['seasons'] = ddgen('erp_seasons', array('erp_season_id', name()));
		$data['hotels'] = array(); //ddgen('erp_hotels', array('erp_hotel_id', name()));
		$data['additions'] = ddgen('erp_room_services', array('erp_room_service_id', name()));
		$data['nationalities'] = ddgen('erp_nationalities', array('erp_nationality_id', name()), FALSE, FALSE, TRUE);

		// ROOMS DROPDOWNS
		$data['har_floors'] = ddgen('erp_floors', array('erp_floor_id', name()));

		// DETAILS FROPDOWNS
		$data['had_housing'] = ddgen('erp_housingtypes', array('erp_housingtype_id', name()));
		$data['had_room_types'] = ddgen('erp_hotelroomsizes', array('erp_hotelroomsize_id', name()), false, false, lang('ha_without'));
		$data['erp_hotels_availability_views'] = ddgen('erp_hotels_availability_views', array('erp_hotels_availability_view_id', name()), false, false, true);

		$periods = $this->db->query("SELECT erp_period_id, " . name() . " FROM erp_periods WHERE erp_period_id != 3
            UNION SELECT erp_hotels_availabilty_offers_id as erp_period_id, " . name() . " FROM erp_hotels_availabilty_offers WHERE availabilty_offer_owner_type = '" . $this->system_id . "' AND availabilty_offer_owner_id = '" . $this->{$this->destination . '_id'} . "'")->result();
		foreach ($periods as $period)
		$data['had_periods']['of' . $period->erp_period_id] = $period->{name()};

		/**
		 * =====================================================================
		 * VALIDATION RULES
		 * =====================================================================
		 */
		// MAIN FORM
		// ---------------------------
		$this->form_validation->set_rules('country_id', lang('ha_country_id'), 'trim|required');
		$this->form_validation->set_rules('city_id', lang('ha_city_id'), 'trim|required');
		$this->form_validation->set_rules('hotel_id', lang('ha_hotel_id'), 'trim|required');
		$this->form_validation->set_rules('date_from', lang('ha_date_from'), 'trim|required');
		$this->form_validation->set_rules('date_to', lang('ha_date_to'), 'trim|required');
		$this->form_validation->set_rules('purchase_currency_id', lang('ha_purchase_currency_id'), 'trim|required');
		$this->form_validation->set_rules('sales_currency_id', lang('ha_sales_currency_id'), 'trim|required');

		//  ROOMS FORM
		// ---------------------------
		$ha_housing_type_id = $this->input->post('ha_housing_type');
		$har_room_type_id = $this->input->post('har_room_type_id');
		$har_beds = $this->input->post('har_beds');
		$har_closed_rooms = $this->input->post('har_closed_rooms');
		$har_etlal = $this->input->post('har_etlal');
		$har_max_beds_count = $this->input->post('har_max_beds_count');
		$har_can_splitting = $this->input->post('har_can_splitting');
		$har_purchase_price = $this->input->post('har_purchase_price');
		$har_offer_status = $this->input->post('har_offer_status');

		//  SUB ROOMS FORM
		// ---------------------------
		$sub_har_floor = $this->input->post('sub_har_floor');
		$sub_har_section = $this->input->post('sub_har_section');
		$sub_har_res_num_of_rooms = $this->input->post('sub_har_res_num_of_rooms');
		$sub_har_room_nums = $this->input->post('sub_har_room_nums');

		//  DETAILS FORM
		// ---------------------------
		$had_period_id = $this->input->post('had_period_id');
		$had_housing_type_id = $this->input->post('had_housing_type_id');
		$had_room_type_id = $this->input->post('had_room_type_id');
		$had_etlal = $this->input->post('had_etlal');
		$had_sale_price = $this->input->post('had_sale_price');
		$had_offer_status = $this->input->post('had_offer_status');
		$had_notes = $this->input->post('had_notes');
		
		//By Gouda.
		$erp_hotels_availability_peroid_id = $this->input->post('erp_hotels_availability_peroid_id');
		
		$peroid_from = $this->input->post('peroid_from');
		$peroid_to = $this->input->post('peroid_to');

		$had_uo_allow = $this->input->post('had_uo_allow');
		$had_hm_allow = $this->input->post('had_hm_allow');
		$had_ea_allow = $this->input->post('had_ea_allow');
		$had_eaco_allow = $this->input->post('had_eaco_allow');
		$had_individual = $this->input->post('had_individual');
		$had_uo_disallow = $this->input->post('had_uo_disallow');
		$had_hm_disallow = $this->input->post('had_hm_disallow');
		$had_ea_disallow = $this->input->post('had_ea_disallow');
		$had_eaco_disallow = $this->input->post('had_eaco_disallow');
		$had_disindividual = $this->input->post('had_disindividual');

		$had_start_date = $this->input->post('had_start_date');
		$had_end_date = $this->input->post('had_end_date');
		$had_room_service_id = $this->input->post('had_room_service_id');

		//  ADDITIONAL OPTIONS FORM
		// ---------------------------
		$res_purchase_price = $this->input->post('res_purchase_price');
		$res_sale_price = $this->input->post('res_sale_price');
		$res_service_id = $this->input->post('res_service_id');
		$res_notes = $this->input->post('res_notes');

		$res_uo_allow = $this->input->post('res_uo_allow');
		$res_hm_allow = $this->input->post('res_hm_allow');
		$res_ea_allow = $this->input->post('res_ea_allow');
		$res_eaco_allow = $this->input->post('res_eaco_allow');
		$res_individual = $this->input->post('res_individual');
		$res_uo_disallow = $this->input->post('res_uo_disallow');
		$res_hm_disallow = $this->input->post('res_hm_disallow');
		$res_ea_disallow = $this->input->post('res_ea_disallow');
		$res_eaco_disallow = $this->input->post('res_aco_disallow');
		$res_disindividual = $this->input->post('res_disindividual');

		/**
		 * =====================================================================
		 * RUNNING VALIDATION
		 * =====================================================================
		 */
		if ($this->form_validation->run() == FALSE) {
			$this->load->view('hotel_availability/manage', $data);
		} else {

			if ($id) {
				if (!$this->allowed_edit($id)) {
					session('error_message', lang('you_cannot_update_availability_becouse_reserved_more_than_available'));
					session('error_url', site_url('hotel_availability'));
					session('error_title', lang('menu_hotel_availabilty'));
					session('error_action', lang('global_update'));
					redirect("error_message/show");
				}
			}


			/**
			 * =====================================================================
			 * INSERT/UPDATE TO DATABASE
			 * =====================================================================
			 */
			$this->layout = 'ajax';
			//  MAIN FORM -> STEP1
			// ---------------------------
			//            print_r($peroid_from);print_r($peroid_to);die();
			$this->db->trans_start();

			$erp_hotels_availability_master_id = $this->save_step1($id);
			
			//By Gouda.
			//$this->db->where('erp_hotels_availability_master_id', $erp_hotels_availability_master_id)->delete('erp_hotels_availability_peroids');
			if (count($erp_hotels_availability_peroid_id)) {
				for ($p = 0; $p < count($erp_hotels_availability_peroid_id); $p++) {
					
					// By Gouda
					
//					$newperiod = array(
//                        'erp_hotels_availability_master_id' => $erp_hotels_availability_master_id,
//                        'peroid_from' => $peroid_from[$p],
//                        'peroid_to' => $peroid_to[$p]
//					);
//						
//					if($erp_hotels_availability_peroid_id[$p]==0) {
//						$this->db->insert('erp_hotels_availability_peroids', $newperiod);
//					} else {
//						$this->db->where('erp_hotels_availability_peroid_id', $erp_hotels_availability_peroid_id[$p])->update('erp_hotels_availability_peroids', $newperiod);
//					}
					
				}
			}
			//  ROOMS FORM -> STEP2
			// ---------------------------
			if (ensure($ha_housing_type_id)) {
				foreach ($ha_housing_type_id as $har => $housing_type_id) {
					$this->hotels_availability_rooms_model->erp_hotels_availability_rooms_id = false;
					if (strpos($har, 'N') === false)
					$this->hotels_availability_rooms_model->erp_hotels_availability_rooms_id = $har;


					$this->hotels_availability_rooms_model->erp_hotelroomsize_id = strlen($har_room_type_id[$har]) ? $har_room_type_id[$har] : null;

					$this->hotels_availability_rooms_model->max_beds_count = isset($har_max_beds_count[$har]) ? $har_max_beds_count[$har] : 0;
					$this->hotels_availability_rooms_model->rooms_beds_count = $har_beds[$har];
					$this->hotels_availability_rooms_model->closed_rooms_beds_count = $har_closed_rooms[$har];
					$this->hotels_availability_rooms_model->erp_hotels_availability_view_id = $har_etlal[$har];
					$this->hotels_availability_rooms_model->can_spiltting = isset($har_can_splitting[$har]) ? 1 : 0;
					$this->hotels_availability_rooms_model->purchase_price = $har_purchase_price[$har];
					$this->hotels_availability_rooms_model->offer_status = $har_offer_status[$har];
					$this->hotels_availability_rooms_model->erp_hotels_availability_master_id = $erp_hotels_availability_master_id;
					$this->hotels_availability_rooms_model->erp_housingtype_id = $ha_housing_type_id[$har];

					$har_id = $this->hotels_availability_rooms_model->save();

					if (!$har_id)
					$har_id = $har;

					if (ensure($sub_har_floor[$har])) {
						foreach ($sub_har_floor[$har] as $sub_har => $sub_har_floor_value) {
							$vars = array(
                                'erp_floor_id' => $sub_har_floor[$har][$sub_har],
                                'section' => $sub_har_section[$har][$sub_har],
                                'num_of_rooms' => $sub_har_res_num_of_rooms[$har][$sub_har],
                                'room_nums' => $sub_har_room_nums[$har][$sub_har],
                                'erp_hotels_availability_rooms_id' => $har_id);
							if (strpos($sub_har, 'N') === false) {
								$this->db->where('erp_hotels_availability_sub_room_id', $sub_har)
								->update('erp_hotels_availability_sub_rooms', $vars);

								//By Gouda.
								//$this->db->where('erp_hotels_availability_sub_room_id', $sub_har)->delete('erp_hotels_availability_room_details');

								if (strlen($sub_har_room_nums[$har][$sub_har])) {
									$roomnums = explode('-', $sub_har_room_nums[$har][$sub_har]);
									$startroom = $roomnums[0];
								} else {
									$startroom = $sub_har_floor[$har][$sub_har] . '01';
								}
								$endroom = (int) $startroom + $sub_har_res_num_of_rooms[$har][$sub_har];
								$addfulldate = true;

								if (count($erp_hotels_availability_peroid_id)) {
									for ($p = 0; $p < count($erp_hotels_availability_peroid_id); $p++) {
									
									// By Gouda
									$this->hotels_availability_room_details_model->status = false;
									$this->hotels_availability_room_details_model->erp_hotels_availability_sub_room_id = $sub_har;
									$this->hotels_availability_room_details_model->erp_hotels_availability_peroid_id = null;
									$this->hotels_availability_room_details_model->delete();

									$addfulldate = false;
									
									$newperiod = array(
				                        'erp_hotels_availability_master_id' => $erp_hotels_availability_master_id,
				                        'peroid_from' => $peroid_from[$p],
				                        'peroid_to' => $peroid_to[$p]
									);
										
									if($erp_hotels_availability_peroid_id[$p]==0) {
										$this->db->insert('erp_hotels_availability_peroids', $newperiod);
										$current_erp_hotels_availability_peroid_id =  $this->db->insert_id();
									} else {
										$this->db->where('erp_hotels_availability_peroid_id', $erp_hotels_availability_peroid_id[$p])->update('erp_hotels_availability_peroids', $newperiod);
										$current_erp_hotels_availability_peroid_id = $erp_hotels_availability_peroid_id[$p];
									}

									// By Gouda

									//										$closed = 0;
									//										if (strlen($peroid_from[$p]) && strlen($peroid_to[$p])) {
									//											$addfulldate = false;
									//											for ($i = (int) $startroom; $i < $endroom; $i++) {
									//												$this->db->set('erp_hotels_availability_sub_room_id', $sub_har);
									//												$this->db->set('number', $i);
									//												$this->db->set('from_date', $peroid_from[$p]);
									//												$this->db->set('to_date', $peroid_to[$p]);
									//												$this->db->set('status', ($har_closed_rooms[$har] && (int) $har_closed_rooms[$har] > $closed) ? 0 : 1);
									//												$this->db->insert('erp_hotels_availability_room_details');
									//												$closed++;
									//											}
									//										}


									//By Gouda
									//-------------------------------------------------------------------------------------------
									$this->hotels_availability_room_details_model->status_not_equal=2;
									$this->hotels_availability_room_details_model->erp_hotels_availability_sub_room_id = $sub_har;
									$this->hotels_availability_room_details_model->erp_hotels_availability_peroid_id = $current_erp_hotels_availability_peroid_id;
									$this->hotels_availability_room_details_model->erp_hotels_availability_room_detail_id = false;
									//$this->hotels_availability_room_details_model->order_by=array('erp_hotels_availability_room_detail_id','desc');
									$saved_room_details_rows = $this->hotels_availability_room_details_model->get_joined();
									$this->hotels_availability_room_details_model->status=false;
									$this->hotels_availability_room_details_model->erp_hotels_availability_sub_room_id=false;
									$this->hotels_availability_room_details_model->erp_hotels_availability_peroid_id = false;
									
									$saved_room_details_rows_count = count($saved_room_details_rows);

									//$current_room_details_rows_count = ((int)$endroom-(int)$startroom)+1;
									$current_room_details_rows_count = ((int)$endroom-(int)$startroom);

									$saved_room_details_counter=0;
									$closed=0;
									$room_number=$startroom;
									foreach($saved_room_details_rows as $saved_room_details_row) {
										
										$erp_hotels_availability_room_detail_id = $saved_room_details_row->erp_hotels_availability_room_detail_id;
										

										$details_vars = array(
		                                'from_date' => $peroid_from[$p],
		                                'to_date' => $peroid_to[$p],
	                                	'number' => $room_number,
		                                'status' => ($har_closed_rooms[$har] && (int) $har_closed_rooms[$har] > $closed) ? 0 : 1,
										);
										$this->db->where('erp_hotels_availability_room_detail_id', $erp_hotels_availability_room_detail_id)->update('erp_hotels_availability_room_details', $details_vars);
										$closed++;
										
										$saved_room_details_counter++;
										$room_number = $startroom+$saved_room_details_counter;
										
									}
									if($current_room_details_rows_count > $saved_room_details_rows_count) {
										
										for ($i = (int) $room_number; $i < $endroom; $i++) {
											$this->db->set('erp_hotels_availability_sub_room_id', $sub_har);
											$this->db->set('number', $room_number);
											$this->db->set('from_date', $peroid_from[$p]);
											$this->db->set('to_date', $peroid_to[$p]);
											$this->db->set('status', ($har_closed_rooms[$har] && (int) $har_closed_rooms[$har] > $closed) ? 0 : 1);
											$this->db->set('erp_hotels_availability_peroid_id', $current_erp_hotels_availability_peroid_id);
											
											$this->db->insert('erp_hotels_availability_room_details');
											$closed++;
											
											$room_number++;
										}

									} else if($current_room_details_rows_count < $saved_room_details_rows_count){

										$saved_room_details_deleted_count = (int)($saved_room_details_counter-$current_room_details_rows_count);

										$saved_room_details_deleted_counter=0;
										foreach($saved_room_details_rows as $saved_room_details_row) {

											$this->hotels_availability_room_details_model->erp_hotels_availability_room_detail_id = $saved_room_details_row->erp_hotels_availability_room_detail_id;
											$current_room_details_row = $this->hotels_availability_room_details_model->get();

											if($saved_room_details_deleted_counter<$saved_room_details_deleted_count) {
												if(count($current_room_details_row)>0){
													if($current_room_details_row->status!=2) {

														$this->hotels_availability_room_details_model->erp_hotels_availability_room_detail_id = false;

														$this->hotels_availability_room_details_model->erp_hotels_availability_sub_room_id = $current_room_details_row->erp_hotels_availability_sub_room_id;
														$this->hotels_availability_room_details_model->erp_hotels_availability_peroid_id = $current_room_details_row->erp_hotels_availability_peroid_id;
														$this->hotels_availability_room_details_model->number = $current_room_details_row->number;
														$this->hotels_availability_room_details_model->status = 2;

														$current_reserved_room_details_row = $this->hotels_availability_room_details_model->get();

														$this->hotels_availability_room_details_model->erp_hotels_availability_sub_room_id = false;
														$this->hotels_availability_room_details_model->erp_hotels_availability_peroid_id = false;
														$this->hotels_availability_room_details_model->number = false;
														$this->hotels_availability_room_details_model->status = false;

														if(count($current_reserved_room_details_row)==0) {
															$this->hotels_availability_room_details_model->erp_hotels_availability_room_detail_id = $current_room_details_row->erp_hotels_availability_room_detail_id;
															$this->hotels_availability_room_details_model->delete();
															$saved_room_details_deleted_counter++;
														}
													}
												}
											}

										}
									}
									//-------------------------------------------------------------------------------------------
									



									}
								}

								if ($addfulldate) {

									//By Gouda
									//-------------------------------------------------------------------------------------------
									$this->hotels_availability_room_details_model->status=1;
									$this->hotels_availability_room_details_model->erp_hotels_availability_sub_room_id = $sub_har;
									$saved_room_details_rows = $this->hotels_availability_room_details_model->get_joined();
									$this->hotels_availability_room_details_model->status=false;
									$this->hotels_availability_room_details_model->erp_hotels_availability_sub_room_id=false;

									$saved_room_details_rows_count = count($saved_room_details_rows);

									//$current_room_details_rows_count = ((int)$endroom-(int)$startroom)+1;
									$current_room_details_rows_count = ((int)$endroom-(int)$startroom);

									$saved_room_details_counter=0;
									$closed=0;
									$room_number=$startroom;
									foreach($saved_room_details_rows as $saved_room_details_row) {
										$erp_hotels_availability_room_detail_id = $saved_room_details_row->erp_hotels_availability_room_detail_id;
										

										$details_vars = array(
		                                'from_date' => $this->input->post('date_from'),
		                                'to_date' => $this->input->post('date_to'),
	                                	'number' => $room_number,
										'erp_hotels_availability_peroid_id' => null,
		                                'status' => ($har_closed_rooms[$har] && (int) $har_closed_rooms[$har] > $closed) ? 0 : 1,
										);
										$this->db->where('erp_hotels_availability_room_detail_id', $erp_hotels_availability_room_detail_id)->update('erp_hotels_availability_room_details', $details_vars);
										
										$saved_room_details_counter++;
										$room_number = $startroom+$saved_room_details_counter;
										
									}
									if($current_room_details_rows_count > $saved_room_details_counter) {

										for ($i = (int) $room_number; $i < $endroom; $i++) {
											$this->db->set('erp_hotels_availability_sub_room_id', $sub_har);
											$this->db->set('number', $i);
											$this->db->set('from_date', $this->input->post('date_from'));
											$this->db->set('to_date', $this->input->post('date_to'));
											$this->db->set('status', ($har_closed_rooms[$har] && (int) $har_closed_rooms[$har] > $closed) ? 0 : 1);
											$this->db->insert('erp_hotels_availability_room_details');
											$closed++;
											
											$room_number++;
										}

									} else if($current_room_details_rows_count < $saved_room_details_counter){

										$saved_room_details_deleted_count = (int)($saved_room_details_counter-$current_room_details_rows_count);

										$saved_room_details_deleted_counter=0;
										foreach($saved_room_details_rows as $saved_room_details_row) {

											$this->hotels_availability_room_details_model->erp_hotels_availability_room_detail_id = $saved_room_details_row->erp_hotels_availability_room_detail_id;
											$current_room_details_row = $this->hotels_availability_room_details_model->get();

											if($saved_room_details_deleted_counter<$saved_room_details_deleted_count) {
												if(count($current_room_details_row)>0){
													if($current_room_details_row->status!=2) {

														$this->hotels_availability_room_details_model->erp_hotels_availability_room_detail_id = false;

														$this->hotels_availability_room_details_model->erp_hotels_availability_sub_room_id = $current_room_details_row->erp_hotels_availability_sub_room_id;
														$this->hotels_availability_room_details_model->number = $current_room_details_row->number;
														//$this->hotels_availability_room_details_model->from_date = $current_room_details_row->from_date;
														//$this->hotels_availability_room_details_model->to_date = $current_room_details_row->to_date;
														$this->hotels_availability_room_details_model->status = 2;

														$current_reserved_room_details_row = $this->hotels_availability_room_details_model->get();

														$this->hotels_availability_room_details_model->erp_hotels_availability_sub_room_id = false;
														$this->hotels_availability_room_details_model->number = false;
														$this->hotels_availability_room_details_model->status = false;

														if(count($current_reserved_room_details_row)==0) {
															$this->hotels_availability_room_details_model->erp_hotels_availability_room_detail_id = $current_room_details_row->erp_hotels_availability_room_detail_id;
															$this->hotels_availability_room_details_model->delete();
															$saved_room_details_deleted_counter++;
														}
													}
												}
											}

										}
									}
									//-------------------------------------------------------------------------------------------

									//                                    $closed = 0;
									//                                    for ($i = (int) $startroom; $i < $endroom; $i++) {
									//                                        $this->db->set('erp_hotels_availability_sub_room_id', $sub_har);
									//                                        $this->db->set('number', $i);
									//                                        $this->db->set('from_date', $this->input->post('date_from'));
									//                                        $this->db->set('to_date', $this->input->post('date_to'));
									//                                        $this->db->set('status', ($har_closed_rooms[$har] && (int) $har_closed_rooms[$har] > $closed) ? 0 : 1);
									//                                        $this->db->insert('erp_hotels_availability_room_details');
									//                                        $closed++;
									//                                    }
								}
							} else {
								$this->db->insert('erp_hotels_availability_sub_rooms', $vars);
								$subroomid = $this->db->insert_id();

								//By Gouda.
								//$this->db->where('erp_hotels_availability_sub_room_id', $subroomid)->delete('erp_hotels_availability_room_details');

								if (strlen($sub_har_room_nums[$har][$sub_har])) {
									$roomnums = explode('-', $sub_har_room_nums[$har][$sub_har]);
									$startroom = $roomnums[0];
								} else {
									$startroom = $sub_har_floor[$har][$sub_har] . '01';
								}
								$endroom = (int) $startroom + $sub_har_res_num_of_rooms[$har][$sub_har];

								$addfulldate = true;
								if (count($peroid_from)) {
									for ($p = 0; $p < count($peroid_from); $p++) {
										if (strlen($peroid_from[$p]) && strlen($peroid_to[$p])) {
											
											// By Gouda
											$newperiod = array(
						                        'erp_hotels_availability_master_id' => $erp_hotels_availability_master_id,
						                        'peroid_from' => $peroid_from[$p],
						                        'peroid_to' => $peroid_to[$p]
											);
												
											if($erp_hotels_availability_peroid_id[$p]==0) {
												$this->db->insert('erp_hotels_availability_peroids', $newperiod);
												$current_erp_hotels_availability_peroid_id = $this->db->insert_id();
											} else {
												$this->db->where('erp_hotels_availability_peroid_id', $erp_hotels_availability_peroid_id[$p])->update('erp_hotels_availability_peroids', $newperiod);
												$current_erp_hotels_availability_peroid_id = $erp_hotels_availability_peroid_id[$p];
											}
											
											$addfulldate = false;
											$closed = 0;
											for ($i = (int) $startroom; $i < $endroom; $i++) {
												$this->db->set('erp_hotels_availability_sub_room_id', $subroomid);
												$this->db->set('number', $i);
												$this->db->set('from_date', $peroid_from[$p]);
												$this->db->set('to_date', $peroid_to[$p]);
												$this->db->set('status', ($har_closed_rooms[$har] && (int) $har_closed_rooms[$har] > $closed) ? 0 : 1);
												$this->db->set('erp_hotels_availability_peroid_id', $current_erp_hotels_availability_peroid_id);
												
												$this->db->insert('erp_hotels_availability_room_details');
												$closed++;
											}
										}
									}
								}

								if ($addfulldate) {
									$closed = 0;
									for ($i = (int) $startroom; $i < $endroom; $i++) {
										$this->db->set('erp_hotels_availability_sub_room_id', $subroomid);
										$this->db->set('number', $i);
										$this->db->set('from_date', $this->input->post('date_from'));
										$this->db->set('to_date', $this->input->post('date_to'));
										$this->db->set('status', ($har_closed_rooms[$har] && (int) $har_closed_rooms[$har] > $closed) ? 0 : 1);
										$this->db->insert('erp_hotels_availability_room_details');
										$closed++;
									}
								}
							}
						}
					} else {
						$vars = array(
                            'erp_floor_id' => '1',
                            'section' => '',
                            'num_of_rooms' => $har_beds[$har],
                            'room_nums' => '101-' . (100 + $har_beds[$har]),
                            'erp_hotels_availability_rooms_id' => $har_id);
						$this->db->insert('erp_hotels_availability_sub_rooms', $vars);
						$subroomid = $this->db->insert_id();
						$startroom = 101;
						$endroom = 101 + (int) $har_beds[$har];
						$addfulldate = true;
						if (count($peroid_from)) {
							for ($p = 0; $p < count($peroid_from); $p++) {
								if (strlen($peroid_from[$p]) && strlen($peroid_to[$p])) {
									
									// By Gouda
									$newperiod = array(
				                        'erp_hotels_availability_master_id' => $erp_hotels_availability_master_id,
				                        'peroid_from' => $peroid_from[$p],
				                        'peroid_to' => $peroid_to[$p]
									);
										
									if($erp_hotels_availability_peroid_id[$p]==0) {
										$this->db->insert('erp_hotels_availability_peroids', $newperiod);
										$current_erp_hotels_availability_peroid_id = $this->db->insert_id();
									} else {
										$this->db->where('erp_hotels_availability_peroid_id', $erp_hotels_availability_peroid_id[$p])->update('erp_hotels_availability_peroids', $newperiod);
										$current_erp_hotels_availability_peroid_id = $erp_hotels_availability_peroid_id[$p];
									}
											
									$addfulldate = false;
									$closed = 0;
									for ($i = (int) $startroom; $i < $endroom; $i++) {
										$this->db->set('erp_hotels_availability_sub_room_id', $subroomid);
										$this->db->set('number', $i);
										$this->db->set('from_date', $peroid_from[$p]);
										$this->db->set('to_date', $peroid_to[$p]);
										$this->db->set('status', ($har_closed_rooms[$har] && (int) $har_closed_rooms[$har] > $closed) ? 0 : 1);
										$this->db->set('erp_hotels_availability_peroid_id', $current_erp_hotels_availability_peroid_id);
												
										$this->db->insert('erp_hotels_availability_room_details');
										$closed++;
									}
								}
							}
						}

						if ($addfulldate) {
							$closed = 0;
							for ($i = (int) $startroom; $i < $endroom; $i++) {
								$this->db->set('erp_hotels_availability_sub_room_id', $subroomid);
								$this->db->set('number', $i);
								$this->db->set('from_date', $this->input->post('date_from'));
								$this->db->set('to_date', $this->input->post('date_to'));
								$this->db->set('status', ($har_closed_rooms[$har] && (int) $har_closed_rooms[$har] > $closed) ? 0 : 1);
								$this->db->insert('erp_hotels_availability_room_details');
								$closed++;
							}
						}
					}
				}
			}
			
			// --- Delete Deleted meals rows from Database --------------
            //By Gouda
            $periods_remove = $this->input->post('periods_remove');
            if (ensure($periods_remove)) {
                foreach ($periods_remove as $period_remove) {
                    $this->db->where('erp_hotels_availability_peroid_id', $period_remove)->delete('erp_hotels_availability_peroids');
                }
            }
            //---------------------------------------------------------------
           

			//  DETAILS FORM -> STEP3
			// ---------------------------
			if (ensure($had_period_id)) {
				$this->load->model('hotels_availability_details_hm_company_model');
				$this->load->model('hotels_availability_details_uo_company_model');
				$this->load->model('hotels_availability_details_country_model');
				$this->load->model('hotels_availability_details_ea_company_model');
				foreach ($had_period_id as $had => $period_id) {
					$this->hotels_availability_details_model->erp_hotels_availability_detail_id = false;
					if (strpos($had, 'N') === false)
					$this->hotels_availability_details_model->erp_hotels_availability_detail_id = $had;
					$this->hotels_availability_details_model->erp_period_id = $had_period_id[$had];
					$this->hotels_availability_details_model->erp_housingtype_id = $had_housing_type_id[$had];

					$this->hotels_availability_details_model->erp_hotelroomsize_id = strlen($had_room_type_id[$had]) ? $had_room_type_id[$had] : null;
					if (isset($had_etlal[$had]))
					$this->hotels_availability_details_model->erp_hotels_availability_view_id = $had_etlal[$had];
					$this->hotels_availability_details_model->sale_price = $had_sale_price[$had];
					if (isset($had_offer_status[$had]))
					$this->hotels_availability_details_model->offer_status = $had_offer_status[$had];
					$this->hotels_availability_details_model->erp_hotels_availability_master_id = $erp_hotels_availability_master_id;
					$this->hotels_availability_details_model->individual = $had_individual[$had];
					$this->hotels_availability_details_model->start_date = $had_start_date[$had];
					$this->hotels_availability_details_model->end_date = $had_end_date[$had];
					$this->hotels_availability_details_model->erp_room_service_id = $had_room_service_id[$had];
					$this->hotels_availability_details_model->notes = $had_notes[$had];

					$detail_id = $this->hotels_availability_details_model->save();

					if (strpos($had, 'N') === false)
					$detail_id = $had;

					if (isset($had_uo_allow[$had]) && $had_uo_allow[$had] != 'all' && $had_uo_allow[$had] != 'none') {
						$allowed_companies = explode(',', $had_uo_allow[$had]);
						$this->db->where('status', '1')->where('erp_hotels_availability_detail_id', $detail_id)->delete('erp_hotels_availability_detail_uo_companies');
						//echo $this->db->last_query();
						foreach ($allowed_companies as $acompany) {
							if ($acompany && $acompany != 'none' && $acompany != 'null') {
								$this->hotels_availability_details_uo_company_model->erp_hotels_availability_detail_id = $detail_id;
								$this->hotels_availability_details_uo_company_model->safa_uo_id = $acompany;
								$this->hotels_availability_details_uo_company_model->status = '1';
								$this->hotels_availability_details_uo_company_model->save();
							}
						}
					}

					if (isset($had_hm_allow[$had]) && $had_hm_allow[$had] != 'all' && $had_hm_allow[$had] != 'none') {
						$allowed_companies = explode(',', $had_hm_allow[$had]);
						$this->db->where('status', '1')->where('erp_hotels_availability_detail_id', $detail_id)->delete('erp_hotels_availability_detail_hm_companies');
						foreach ($allowed_companies as $acompany) {
							if ($acompany && $acompany != 'none' && $acompany != 'null') {
								$this->hotels_availability_details_hm_company_model->erp_hotels_availability_detail_id = $detail_id;
								$this->hotels_availability_details_hm_company_model->safa_hm_id = $acompany;
								$this->hotels_availability_details_hm_company_model->status = '1';
								$this->hotels_availability_details_hm_company_model->save();
							}
						}
					}

					if (isset($had_ea_allow[$had]) && $had_ea_allow[$had] != 'all' && $had_ea_allow[$had] != 'none') {
						$allowed_ea_countries = explode(',', $had_ea_allow[$had]);
						$this->db->where('status', '1')->where('erp_hotels_availability_detail_id', $detail_id)->delete('erp_hotels_availability_detail_countries');
						foreach ($allowed_ea_countries as $eacountry) {
							if ($eacountry && $eacountry != 'none' && $eacountry != 'null') {
								$this->hotels_availability_details_country_model->erp_hotels_availability_detail_id = $detail_id;
								$this->hotels_availability_details_country_model->erp_country_id = $eacountry;
								$this->hotels_availability_details_country_model->status = '1';
								$this->hotels_availability_details_country_model->save();
							}
						}

						$allowed_ea_companies = explode(',', $had_eaco_allow[$had]);
						$this->db->where('status', '1')->where('erp_hotels_availability_detail_id', $detail_id)->delete('erp_hotels_availability_detail_ea_companies');
						foreach ($allowed_ea_companies as $acompany) {
							if ($acompany && $acompany != 'none' && $acompany != 'null') {
								$this->hotels_availability_details_ea_company_model->erp_hotels_availability_detail_id = $detail_id;
								$this->hotels_availability_details_ea_company_model->safa_ea_id = $acompany;
								$this->hotels_availability_details_ea_company_model->status = '1';
								$this->hotels_availability_details_ea_company_model->save();
							}
						}
					}

					if (isset($had_uo_disallow[$had]) && $had_uo_disallow[$had] != 'all' && $had_uo_disallow[$had] != 'none') {
						$disallowed_companies = explode(',', $had_uo_disallow[$had]);
						$this->db->where('status', '0')->where('erp_hotels_availability_detail_id', $detail_id)->delete('erp_hotels_availability_detail_uo_companies');
						//echo $this->db->last_query();
						foreach ($disallowed_companies as $acompany) {
							if ($acompany && $acompany != 'none' && $acompany != 'null') {
								$this->hotels_availability_details_uo_company_model->erp_hotels_availability_detail_id = $detail_id;
								$this->hotels_availability_details_uo_company_model->safa_uo_id = $acompany;
								$this->hotels_availability_details_uo_company_model->status = '0';
								$this->hotels_availability_details_uo_company_model->save();
							}
						}
					}

					if (isset($had_hm_disallow[$had]) && $had_hm_disallow[$had] != 'all' && $had_hm_disallow[$had] != 'none') {
						$disallowed_companies = explode(',', $had_hm_disallow[$had]);
						$this->db->where('status', '0')->where('erp_hotels_availability_detail_id', $detail_id)->delete('erp_hotels_availability_detail_hm_companies');
						foreach ($disallowed_companies as $acompany) {
							if ($acompany && $acompany != 'none' && $acompany != 'null') {
								$this->hotels_availability_details_hm_company_model->erp_hotels_availability_detail_id = $detail_id;
								$this->hotels_availability_details_hm_company_model->safa_hm_id = $acompany;
								$this->hotels_availability_details_hm_company_model->status = '0';
								$this->hotels_availability_details_hm_company_model->save();
							}
						}
					}

					if (isset($had_ea_disallow[$had]) && $had_ea_disallow[$had] != 'all' && $had_ea_disallow[$had] != 'none') {
						$disallowed_ea_countries = explode(',', $had_ea_disallow[$had]);
						$this->db->where('status', '0')->where('erp_hotels_availability_detail_id', $detail_id)->delete('erp_hotels_availability_detail_countries');
						foreach ($disallowed_ea_countries as $eacountry) {
							if ($eacountry && $eacountry != 'none' && $eacountry != 'null') {
								$this->hotels_availability_details_country_model->erp_hotels_availability_detail_id = $detail_id;
								$this->hotels_availability_details_country_model->erp_country_id = $eacountry;
								$this->hotels_availability_details_country_model->status = '0';
								$this->hotels_availability_details_country_model->save();
							}
						}

						$disallowed_ea_companies = explode(',', $had_eaco_disallow[$had]);
						$this->db->where('status', '0')->where('erp_hotels_availability_detail_id', $detail_id)->delete('erp_hotels_availability_detail_ea_companies');
						foreach ($disallowed_ea_companies as $acompany) {
							if ($acompany && $acompany != 'none' && $acompany != 'null') {
								$this->hotels_availability_details_ea_company_model->erp_hotels_availability_detail_id = $detail_id;
								$this->hotels_availability_details_ea_company_model->safa_ea_id = $acompany;
								$this->hotels_availability_details_ea_company_model->status = '0';
								$this->hotels_availability_details_ea_company_model->save();
							}
						}
					}
				}
			}

			//  ADDITIONAL OPTIONS FORM -> STEP4
			// ---------------------------
			if (ensure($res_service_id)) {
				$this->load->model('erp_room_execlusive_services_hm_company_model');
				$this->load->model('erp_room_execlusive_services_uo_company_model');
				$this->load->model('erp_room_execlusive_services_country_model');
				$this->load->model('erp_room_execlusive_services_ea_company_model');
				foreach ($res_service_id as $res => $service_id) {
					$this->room_exclusive_services_model->erp_room_exclusive_service_id = false;
					if (strpos($res, 'N') === false)
					$this->room_exclusive_services_model->erp_room_exclusive_service_id = $res;

					$this->room_exclusive_services_model->erp_room_service_id = $service_id;
					$this->room_exclusive_services_model->purchase_price = $res_purchase_price[$res];
					$this->room_exclusive_services_model->sale_price = $res_sale_price[$res];
					$this->room_exclusive_services_model->erp_hotels_availability_master_id = $erp_hotels_availability_master_id;
					$res_id = $this->room_exclusive_services_model->save();

					if (strpos($res, 'N') === false)
					$res_id = $res;

					if (isset($res_uo_allow[$res]) && $res_uo_allow[$res] != 'all' && $res_uo_allow[$res] != 'none') {
						$allowed_companies = explode(',', $res_uo_allow[$res]);
						$this->db->where('status', '1')->where('erp_room_exclusive_service_id', $res_id)->delete('erp_room_execlusive_services_uo_companies');
						foreach ($allowed_companies as $acompany) {
							if ($acompany) {
								$this->erp_room_execlusive_services_uo_company_model->erp_room_exclusive_service_id = $res_id;
								$this->erp_room_execlusive_services_uo_company_model->safa_uo_id = $acompany;
								$this->erp_room_execlusive_services_uo_company_model->status = '1';
								$this->erp_room_execlusive_services_uo_company_model->save();
							}
						}
					}

					if (isset($res_hm_allow[$res]) && $res_hm_allow[$res] != 'all' && $res_hm_allow[$res] != 'none') {
						$allowed_companies = explode(',', $res_hm_allow[$res]);
						$this->db->where('status', '1')->where('erp_room_exclusive_service_id', $res_id)->delete('erp_room_execlusive_services_hm_companies');
						foreach ($allowed_companies as $acompany) {
							if ($acompany) {
								$this->erp_room_execlusive_services_hm_company_model->erp_room_exclusive_service_id = $res_id;
								$this->erp_room_execlusive_services_hm_company_model->safa_hm_id = $acompany;
								$this->erp_room_execlusive_services_hm_company_model->status = '1';
								$this->erp_room_execlusive_services_hm_company_model->save();
							}
						}
					}

					if (isset($res_ea_allow[$res]) && $res_ea_allow[$res] != 'all' && $res_ea_allow[$res] != 'none') {
						$allowed_ea_countries = explode(',', $res_ea_allow[$res]);
						$this->db->where('status', '1')->where('erp_room_exclusive_service_id', $res_id)->delete('erp_room_execlusive_services_countries');
						foreach ($allowed_ea_countries as $eacountry) {
							if ($eacountry) {
								$this->erp_room_execlusive_services_country_model->erp_room_exclusive_service_id = $res_id;
								$this->erp_room_execlusive_services_country_model->erp_country_id = $eacountry;
								$this->erp_room_execlusive_services_country_model->status = '1';
								$this->erp_room_execlusive_services_country_model->save();
							}
						}

						$allowed_ea_companies = explode(',', $res_eaco_allow[$res]);
						$this->db->where('status', '1')->where('erp_room_exclusive_service_id', $res_id)->delete('erp_room_execlusive_services_ea_companies');
						foreach ($allowed_ea_companies as $acompany) {
							if ($acompany) {
								$this->erp_room_execlusive_services_ea_company_model->erp_room_exclusive_service_id = $res_id;
								$this->erp_room_execlusive_services_ea_company_model->safa_ea_id = $acompany;
								$this->erp_room_execlusive_services_ea_company_model->status = '1';
								$this->erp_room_execlusive_services_ea_company_model->save();
							}
						}
					}

					if (isset($res_uo_disallow[$res]) && $res_uo_disallow[$res] != 'all' && $res_uo_disallow[$res] != 'none') {
						$disallowed_companies = explode(',', $res_uo_disallow[$res]);
						$this->db->where('status', '0')->where('erp_room_exclusive_service_id', $res_id)->delete('erp_room_execlusive_services_uo_companies');
						foreach ($disallowed_companies as $acompany) {
							if ($acompany) {
								$this->erp_room_execlusive_services_uo_company_model->erp_room_exclusive_service_id = $res_id;
								$this->erp_room_execlusive_services_uo_company_model->safa_uo_id = $acompany;
								$this->erp_room_execlusive_services_uo_company_model->status = '0';
								$this->erp_room_execlusive_services_uo_company_model->save();
							}
						}
					}

					if (isset($res_hm_disallow[$res]) && $res_hm_disallow[$res] != 'all' && $res_hm_disallow[$res] != 'none') {
						$disallowed_companies = explode(',', $res_hm_disallow[$res]);
						$this->db->where('status', '0')->where('erp_room_exclusive_service_id', $res_id)->delete('erp_room_execlusive_services_hm_companies');
						foreach ($disallowed_companies as $acompany) {
							if ($acompany) {
								$this->erp_room_execlusive_services_hm_company_model->erp_room_exclusive_service_id = $res_id;
								$this->erp_room_execlusive_services_hm_company_model->safa_hm_id = $acompany;
								$this->erp_room_execlusive_services_hm_company_model->status = '0';
								$this->erp_room_execlusive_services_hm_company_model->save();
							}
						}
					}

					if (isset($res_ea_disallow[$res]) && $res_ea_disallow[$res] != 'all' && $res_ea_disallow[$res] != 'none') {
						$disallowed_ea_countries = explode(',', $res_ea_disallow[$res]);
						$this->db->where('status', '0')->where('erp_room_exclusive_service_id', $res_id)->delete('erp_room_execlusive_services_hm_companies');
						foreach ($disallowed_ea_countries as $eacountry) {
							if ($eacountry) {
								$this->erp_room_execlusive_services_country_model->erp_room_exclusive_service_id = $res_id;
								$this->erp_room_execlusive_services_country_model->erp_country_id = $eacountry;
								$this->erp_room_execlusive_services_country_model->status = '0';
								$this->erp_room_execlusive_services_country_model->save();
							}
						}

						$disallowed_ea_companies = explode(',', $res_eaco_disallow[$res]);
						$this->db->where('status', '0')->where('erp_room_exclusive_service_id', $res_id)->delete('erp_room_execlusive_services_ea_companies');
						foreach ($disallowed_ea_companies as $acompany) {
							if ($acompany) {
								$this->erp_room_execlusive_services_ea_company_model->erp_room_exclusive_service_id = $res_id;
								$this->erp_room_execlusive_services_ea_company_model->safa_ea_id = $acompany;
								$this->erp_room_execlusive_services_ea_company_model->status = '0';
								$this->erp_room_execlusive_services_ea_company_model->save();
							}
						}
					}
				}
			}

			// DELETE ENTRIES PROCESSING
			$har_remove = $this->input->post('har_remove');
			$had_remove = $this->input->post('had_remove');
			$res_remove = $this->input->post('res_remove');
			if (ensure($har_remove)) {
				foreach ($har_remove as $har) {
					$this->db->where('erp_hotels_availability_rooms_id', $har)->delete('erp_hotels_availability_rooms');
				}
			}
			if (ensure($had_remove)) {
				foreach ($had_remove as $had) {
					$this->db->where('erp_hotels_availability_detail_id', $had)->delete('erp_hotels_availability_detail');
				}
			}
			if (ensure($res_remove)) {
				foreach ($res_remove as $res) {
					$this->db->where('erp_room_exclusive_service_id', $res)->delete('erp_room_execlusive_services');
				}
			}
			// REDIRECT
			$this->db->trans_complete();
			if ($this->db->trans_status() === FALSE) {

			} else {
				redirect('hotel_availability');
			}
		}
	}

	public function ajax_get_cities() {
		echo '<option value="">' . lang('global_select_from_menu') . '</option>';
		if ($this->input->post('country_id'))
		$this->db->where('erp_country_id', $this->input->post('country_id'));
		else
		die();
		$citiesq = $this->db->get('erp_cities');
		$cities = $citiesq->result();
		$this->layout = 'ajax';
		foreach ($cities as $city)
		if ($city->erp_city_id == $this->input->post('id'))
		echo '<option selected="selected" value="' . $city->erp_city_id . '">' . $city->{name()} . '</option>';
		else
		echo '<option value="' . $city->erp_city_id . '">' . $city->{name()} . '</option>';
	}

	public function ajax_get_ea_companies() {
		if ($this->input->post('country_id'))
		$this->db->where('erp_country_id', $this->input->post('country_id'));
		elseif ($this->input->post('comp_id')) {
			$compids = explode(',', $this->input->post('comp_id'));
			$this->db->where_in('safa_ea_id', $compids);
		} else
		die();
		$companies = $this->db->get('safa_eas')->result();
		$this->layout = 'ajax';
		$companiesarray = array();
		foreach ($companies as $company) {
			$companiesarray[] = array('id' => $company->safa_ea_id, 'name' => $company->{name()});
		}
		echo json_encode($companiesarray);
	}

	public function ajax_get_hotels() {
		if (!$this->input->post('no_select'))
		echo '<option value="">' . lang('global_select_from_menu') . '</option>';
		if ($this->input->post('city_id'))
		$this->db->where('erp_city_id', $this->input->post('city_id'));
		else
		die();

		$q = $this->db->get('erp_hotels');
		$items = $q->result();
		$this->layout = 'ajax';
		foreach ($items as $item)
		if ($item->erp_hotel_id == $this->input->post('id'))
		echo '<option selected="selected" value="' . $item->erp_hotel_id . '">' . $item->{name()} . '</option>';
		else
		echo '<option value="' . $item->erp_hotel_id . '">' . $item->{name()} . '</option>';
	}

	public function ajax_get_hotel_rooms() {
		$this->layout = 'ajax';
		if (!$this->input->post('hotel_id')) {
			echo json_encode(array('status' => FALSE));
			die();
		}

		$this->db->select('rooms_count,beds_count,erp_hotel_floor_id');
		$this->db->where('erp_hotel_id', $this->input->post('hotel_id'));
		$this->db->join('erp_companies_hotels', 'erp_companies_hotels.erp_companies_hotels_id = erp_companies_hotels_rooms.erp_companies_hotels_id');
		$hotelcompanies = $this->db->get('erp_companies_hotels_rooms')->result();
		if (!count($hotelcompanies)) {
			echo json_encode(array('status' => FALSE));
			die();
		}
		$hotel_floors_able = array();
		foreach ($hotelcompanies as $company) {
			$hotel_floors_able[$company->erp_hotel_floor_id] = array(
                'rooms' => $company->rooms_count,
                'beds' => $company->beds_count,
			);
		}

		$this->output->set_output(json_encode(array('floors' => $hotel_floors_able, 'status' => TRUE)));
	}

	public function map($hotel_id = false) {
		$this->layout = 'ajax';
		if (!$hotel_id)
		show_404();

		$this->load->model('hotels_model');
		$this->hotels_model->erp_hotel_id = $hotel_id;
		$this->hotels_model->join = TRUE;
		$data['hotel'] = $this->hotels_model->get();
		if (!$data['hotel'])
		show_404();

		$this->load->view('hotel_availability/maps', $data);
	}

	public function hotel($hotel_id = false) {
		$this->layout = 'ajax';
		if (!$hotel_id)
		show_404();

		$this->load->model('hotels_model');
		$this->hotels_model->erp_hotel_id = $hotel_id;
		$this->hotels_model->join = TRUE;
		$data['hotel'] = $this->hotels_model->get();
		if (!$data['hotel'])
		show_404();

		$this->load->view('hotel_availability/hotel_data', $data);
	}

	public function policies($hotel_id = false) {
		$this->layout = 'ajax';
		if (!$hotel_id)
		show_404();

		$this->load->model('erp_hotels_policies_details_model');
		$this->erp_hotels_policies_details_model->erp_hotels_id = $hotel_id;
		$this->erp_hotels_policies_details_model->join = TRUE;
		$data['hotel_details'] = $this->erp_hotels_policies_details_model->get();
		if (!$data['hotel_details'])
		echo lang('hotel_not_have_polices');
		else
		$this->load->view('hotel_availability/policies', $data);
	}

	public function advantages($hotel_id = false) {
		$this->layout = 'ajax';
		if (!$hotel_id)
		show_404();

		$this->load->model('erp_hotel_advantages_details_model');
		$this->erp_hotel_advantages_details_model->erp_hotel_id = $hotel_id;
		$this->erp_hotel_advantages_details_model->join = TRUE;
		$data['hotel_advantages'] = $this->erp_hotel_advantages_details_model->get();
		if (!$data['hotel_advantages'])
		echo lang('hotel_not_have_advantage');
		else
		$this->load->view('hotel_availability/advantages', $data);
	}

	public function availability_detail($av_master_id = false) {
		$this->layout = 'ajax';
		if (!$av_master_id)
		show_404();
		$data = array();
		$this->load->model('hotels_availability_model');
		$this->hotels_availability_model->erp_hotels_availability_master_id = $av_master_id;
		$av_master = $this->hotels_availability_model->get();

		if ($av_master->erp_company_type_id == 2) {
			$this->load->model('uos_model');
			$this->uos_model->safa_uo_id = $av_master->safa_company_id;
			$data['company'] = $this->uos_model->get();
		} elseif ($av_master->erp_company_type_id == 3) {
			$this->load->model('eas_model');
			$this->eas_model->safa_ea_id = $av_master->safa_company_id;
			$data['company'] = $this->eas_model->get();
		} elseif ($av_master->erp_company_type_id == 5) {
			$this->load->model('hms_model');
			$this->hms_model->safa_hm_id = $av_master->safa_company_id;
			$data['company'] = $this->hms_model->get();
		} elseif ($av_master->erp_company_type_id == 4) {
			$this->load->model('hotel_marketing_companies_model');
			$this->hotel_marketing_companies_model->safa_hm_id = $av_master->safa_company_id;
			$data['company'] = $this->hotel_marketing_companies_model->get();
		}

		if (!$data['company'])
		echo lang('availability_user_data_unavailable');
		else
		$this->load->view('hotel_availability/availability_detail', $data);
	}

	private function save_step1($id = false) {
		if ($id)
		$this->hotels_availability_model->erp_hotels_availability_master_id = $id;

		if (!is_dir('static/uploads/availability'))
		mkdir('static/uploads/availability');

		$config['upload_path'] = './static/uploads/availability/';
		$config['allowed_types'] = 'gif|jpg|png';

		$this->load->library('upload', $config);
		if ($this->upload->do_upload('invoice_image')) {
			$file_data = $this->upload->data();
			$this->hotels_availability_model->invoice_image = $config['upload_path'] . $file_data['file_name'];
		}

		$this->hotels_availability_model->safa_company_id = session($this->destination . '_id');
		$this->hotels_availability_model->erp_company_type_id = $this->system_id;
		$this->hotels_availability_model->erp_country_id = $this->input->post('country_id');
		$this->hotels_availability_model->erp_city_id = $this->input->post('city_id');
		$this->hotels_availability_model->erp_hotel_id = $this->input->post('hotel_id');
		$this->hotels_availability_model->season_id = $this->input->post('season_id');
		$this->hotels_availability_model->date_from = $this->input->post('date_from');
		$this->hotels_availability_model->date_to = $this->input->post('date_to');
		$this->hotels_availability_model->purchase_currency_id = $this->input->post('purchase_currency_id');
		$this->hotels_availability_model->sale_currency_id = $this->input->post('sales_currency_id');
		$this->hotels_availability_model->invoice_number = $this->input->post('invoice_number');
		$this->hotels_availability_model->availability_status = $this->input->post('availability_status');
		$this->hotels_availability_model->notes = $this->input->post('note');
		$erp_hotels_availability_master_id = $this->hotels_availability_model->save();

		if ($id) {
			// DELETE OLD NATIONALITIES
			$this->hotels_nationalities_model->erp_hotels_availability_master_id = $id;
			$this->hotels_nationalities_model->delete();
			// ADDITIONAL [INCLUSIVE] SERVICES
			$this->room_inclusive_services_model->erp_hotels_availability_master_id = $id;
			$this->room_inclusive_services_model->delete();
		}
		// NATIONALITIES
		if (ensure($this->input->post('nationalities'))) {
			foreach ($this->input->post('nationalities') as $nationality) {
				$this->hotels_nationalities_model->erp_nationality_id = $nationality;
				$this->hotels_nationalities_model->erp_hotels_availability_master_id = $erp_hotels_availability_master_id;
				$this->hotels_nationalities_model->save();
			}
		}
		// INCLUSIVE SERVICES
		if (ensure($this->input->post('additions'))) {
			foreach ($this->input->post('additions') as $addition) {
				$this->room_inclusive_services_model->erp_room_service_id = $addition;
				$this->room_inclusive_services_model->erp_hotels_availability_master_id = $erp_hotels_availability_master_id;
				$this->room_inclusive_services_model->save();
			}
		}
		return $erp_hotels_availability_master_id;
	}

	function allowed_edit($id) {
		$this->load->model('hotels_availability_room_details_model');

		$ha_housing_type_id = $this->input->post('ha_housing_type');
		$har_beds = $this->input->post('har_beds');
		$har_closed_rooms = $this->input->post('har_closed_rooms');

		if (ensure($ha_housing_type_id)) {
			foreach ($ha_housing_type_id as $har => $housing_type_id) {
				if (strpos($har, 'N') === false) {
					$erp_hotels_availability_rooms_id = $har;
					$rooms_beds_count = $har_beds[$har];
					$closed_rooms_beds_count = $har_closed_rooms[$har];
					$current_availability_rooms_count = $rooms_beds_count - $closed_rooms_beds_count;
					$reserved_availability_rooms_count = 0;
					$sub_rooms_rows = $this->hotels_availability_rooms_model->get_sub_rooms($erp_hotels_availability_rooms_id);
					foreach ($sub_rooms_rows as $sub_rooms_row) {
						$erp_hotels_availability_sub_room_id = $sub_rooms_row->erp_hotels_availability_sub_room_id;
						$this->hotels_availability_room_details_model->erp_hotels_availability_sub_room_id = $erp_hotels_availability_sub_room_id;
						$this->hotels_availability_room_details_model->status = 2;
						$sub_rooms_count = $this->hotels_availability_room_details_model->get(true);
						$reserved_availability_rooms_count = $reserved_availability_rooms_count + $sub_rooms_count;
					}

					if ($reserved_availability_rooms_count > $current_availability_rooms_count) {
						return false;
					}
				}
			}
		}


		$this->hotels_availability_room_details_model->erp_hotels_availability_sub_room_id= false;
		$this->hotels_availability_room_details_model->erp_hotels_availability_master_id = $id;
		$this->hotels_availability_room_details_model->status = 2;
		$reserved_availability_rooms_rows = $this->hotels_availability_room_details_model->get_joined();

		$had_room_type_id = $this->input->post('had_room_type_id');
		$had_start_date = $this->input->post('had_start_date');
		$had_end_date = $this->input->post('had_end_date');

		$save_dates=false;
		foreach ($reserved_availability_rooms_rows as $reserved_availability_rooms_row) {

			/*
			 if (ensure($had_room_type_id)) {
				foreach ($had_room_type_id as $key => $value) {
				if (strpos($key, 'N') === false) {

				$room_type_id = $had_room_type_id[$key];
				$start_date = $had_start_date[$key];
				$end_date = $had_end_date[$key];


				if($reserved_availability_rooms_row->erp_hotelroomsize_id == $room_type_id) {
				if($start_date <= $reserved_availability_rooms_row->from_date) {
				if($end_date >= $reserved_availability_rooms_row->to_date) {
				$save_dates = true;
				}
				}

				}
				}
				}
				}
				if($save_dates==false) {
				//exit;
				return false;
				}
				*/


			//------------------------------------------------------------------------
			// Main Dates
			$save_dates=false;
			$master_start_date = $this->input->post('date_from');
			$master_end_date = $this->input->post('date_to');
			if($master_start_date <= $reserved_availability_rooms_row->from_date) {
				if($master_end_date >= $reserved_availability_rooms_row->to_date) {
					$save_dates = true;
				}
			}
			if($save_dates==false) {
				return false;
			}
			//------------------------------------------------------------------------


			//------------------------------------------------------------------------
			// Periods Dates
			$peroid_from = $this->input->post('peroid_from');
			$peroid_to = $this->input->post('peroid_to');

			if (count($peroid_from)) {
				$save_dates=false;
				for ($p = 0; $p < count($peroid_from); $p++) {
					if (strlen($peroid_from[$p]) && strlen($peroid_to[$p])) {
						$peroid_from_date= $peroid_from[$p];
						$peroid_to_date= $peroid_to[$p];

						if($peroid_from_date <= $reserved_availability_rooms_row->from_date) {
							if($peroid_to_date >= $reserved_availability_rooms_row->to_date) {
								$save_dates = true;
							}
						}
					}
				}
			}
				
			if($save_dates==false) {
				return false;
			}
			//------------------------------------------------------------------------


		}



		return true;
	}

	function pdf_export($id = false) {
		if (!$id)
		show_404();

		$data = array();
		$this->load->model('hotels_availability_model');
		$this->load->model('hotels_nationalities_model');
		$this->load->model('hotels_availability_rooms_model');

		$this->hotels_availability_model->join = true;
		$this->hotels_availability_model->erp_hotels_availability_master_id = $id;
		$this->hotels_availability_model->erp_company_type_id = $this->system_id;
		$this->hotels_availability_model->safa_company_id = $this->{$this->destination . '_id'};
		$data['item'] = $this->hotels_availability_model->get();

		if (!$data['item']) {
			show_404();
		}

		$data['current_hotel_availability_id'] = $id;

		$this->hotels_nationalities_model->erp_hotels_availability_master_id = $id;
		$this->hotels_availability_rooms_model->erp_hotels_availability_master_id = $id;
		$nationalities = $this->hotels_nationalities_model->get();
		$data['item_nationalities'] = $nationalities;
		$data['item_rooms'] = $this->hotels_availability_rooms_model->get();
		$data['item_peroids'] = $this->db->where('erp_hotels_availability_master_id', $id)->get('erp_hotels_availability_peroids')->result();
		$this->load->helper('pdf_helper');
		$pdf_orientation = 'P';
		$pdf_unit = 'px';
		$pdf_pageformat = 'A4';
		$pdf_unicode = true;
		$pdf_encoding = 'UTF-8';

		$pdf = new virgo_pdf($pdf_orientation, $pdf_unit, $pdf_pageformat, $pdf_unicode, $pdf_encoding, false);
		$tahoma = $pdf->addTTFfont('static/fonts/tradbdo.ttf', 'TrueTypeUnicode', '', 96);
		$pdf->SetFont($tahoma, '', 10, '', false);
		$pdf->SetCreator('Safa Live');
		$pdf->SetAuthor('Safa Live');
		$pdf->SetTitle('Safa Live Reports');
		$pdf->SetSubject('Safa Live');

		$pdf->setPrintHeader(false);

		// set default monospaced font
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

		// set margins
		$pdf->SetMargins(20, 20, 40);
		$pdf->SetHeaderMargin(20);
		$pdf->SetFooterMargin(0);

		// remove default footer
		$pdf->setPrintFooter(false);

		// set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

		// set image scale factor
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

		$pdf->setRTL(true);
		$pdf->AddPage();
//print_r($data);die();
		$html = $this->load->view("hotel_availability/pdf", $data, true);
		$pdf->writeHTML($html, true, false, true, false, '');

		$pdf->lastPage();
		ob_end_clean();
                $this->load->helper('download');
		$pdf->Output("hotel_reservation_$id.pdf", 'D');
	}

	function delete($id = false)
	{
		$this->load->model('hotels_availability_model');
			
		if (!$id) {
			show_404();
		}

		$this->hotels_availability_room_details_model->erp_hotels_availability_master_id = $id;
		$this->hotels_availability_room_details_model->status = 2;
		$item = $this->hotels_availability_room_details_model->get_joined();
		if (count($item)> 0) {
			session('error_message', lang('you_cannot_delete_availability_after_reservation'));
			session('error_url', site_url('hotel_availability'));
			session('error_title', lang('menu_hotel_availabilty'));
			session('error_action', lang('global_delete'));
			redirect("error_message/show");
		}

		$this->hotels_availability_model->erp_hotels_availability_master_id = $id;
		if (!$this->hotels_availability_model->delete())
		show_404();
		$this->load->view('redirect_message', array('msg' => lang('global_deleted_message'),
            'url' => site_url('hotel_availability'),
            'model_title' => lang('menu_hotel_availabilty'), 'action' => lang('global_delete')));
	}


}
