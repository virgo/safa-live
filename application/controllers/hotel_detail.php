<?php

class hotel_detail extends Safa_Controller {

    public function index() {
        $this->layout = 'new';
        // $this->lang->load('screen1');
        $data['nationalities'] = ddgen('erp_nationalities', array('erp_nationality_id', name()));
        $this->load->view('hotel_detail/hotel_detail', $data);
    }

}
