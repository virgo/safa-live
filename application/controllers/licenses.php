<?php

class Licenses extends Safa_Controller {

    var $module = 'licenses';
    var $table = 'crm_licenses';
    var $table_pk = 'crm_license_id';

    public function __construct() {
        parent::__construct();
        $this->layout = 'new';
        $this->lang->load('licenses');
        permission();
    }

    public function index() {
        $data['module'] = $this->module;
        $data['table_pk'] = $this->table_pk;
        $data['destination'] = $this->destination;
        $data['items'] = $this->db->select($this->table . ".*, "
                        . "(SELECT COUNT(*) FROM crm_ea_licenses WHERE crm_ea_licenses.crm_license_id = " . $this->table . ".crm_license_id) AS 'delete'")->get($this->table)->result();
        $this->load->view('licenses/index', $data);
    }

    public function manage($id = false) {
        $data['module'] = $this->module;
        $data['table_pk'] = $this->module;
        $data['destination'] = $this->destination;
        if ($id) {
            $data['item'] = $this->db->where($this->table_pk, $id)->get($this->table)->row();
        } else {
            $data['item'] = new stdClass();
            $data['item']->code = null;
            $data['item']->name = null;
        }
        $this->load->library('form_validation');
        $this->form_validation->set_rules('code', lang($this->module . '_code'), 'required|callback_check_code');
        $this->form_validation->set_rules('name', lang($this->module . 'name'), 'required');
        if ($this->form_validation->run() == false) {
            $this->load->view($this->module . '/manage', $data);
        } else {
            $this->db->set('name', $this->input->post('name'));
            $this->db->set('code', $this->input->post('code'));
            if ($id) {
                $this->db->where($this->table_pk, $id);
                $this->db->update($this->table);
            } else {
                $this->db->insert($this->table);
            }
            redirect($this->module . '/index/');
        }
    }

    public function delete($id = false) {
        if (!$id)
            show_404();

        $this->db->where($this->table_pk, $id)->delete($this->table);
        redirect($this->module . '/index/');
    }

    // CALLBACK
    public function check_code($value) {
        if ($this->uri->segment(3))
            $this->db->where($this->table_pk . ' !=', $this->uri->segment(3));
        if (!$this->db->where('code', $value)->get($this->table)->num_rows())
            return true;
        else {
            $this->form_validation->set_message('check_code', lang($this->module . '_check_code'));
            return false;
        }
    }

}
