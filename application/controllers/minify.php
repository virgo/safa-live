<?php


//require_once APPPATH."libraries/css_minifier";

class Minify extends CI_Controller {
    public $layout = 'ajax';
    private $css;
    private $js;
    public function js(){
        header("content-type: application/x-javascript");
//        require_once APPPATH."libraries/js_minifier";
        $this->js = array(
            NEW_CSS_JS . '_' . lang('DIR') .'/jquery-1.9.1.js',
            NEW_CSS_JS . '_' . lang('DIR') .'/jquery-ui.js',
            NEW_CSS_JS . '_' . lang('DIR') .'/jquery.tipsy.js',
            NEW_CSS_JS . '_' . lang('DIR') .'/slide-menu.js',
            NEW_CSS_JS . '/wizard/formToWizard.js',
            NEW_JS .'/plugins/jquery/jquery-ui-1.10.1.custom.min.js',
            NEW_JS .'/plugins/jquery/jquery-migrate-1.1.1.min.js',
            NEW_JS .'/plugins/bootstrap/bootstrap.min.js',
            NEW_JS .'/plugins/bootstrap/bootstrap-dropdown.js',
            NEW_JS .'/plugins/bootstrap/bootstrap-fileupload.min.js',
            NEW_JS .'/plugins/pnotify/jquery.pnotify.min.js',
            NEW_JS .'/plugins/datatables/jquery.dataTables.min.js',
            NEW_JS .'/plugins/uniform/jquery.uniform.min.js',
            NEW_JS .'/plugins/scroll/jquery.custom-scrollbar.js',
            NEW_JS .'/plugins.js',
            NEW_JS .'/plugins/lang-ar.js',
            NEW_JS .'/jquery.validationEngine-'. name(true) .'.js',
            NEW_JS .'/jquery.validationEngine.js',
            NEW_JS .'/jquery.equalheights.js',
            NEW_JS .'/plugins/select/select2.min.js',
            NEW_CSS_JS . '_' . lang('DIR') .'/multiselect/jquery.multi-select.min.js',
            NEW_CSS_JS . '_' . lang('DIR') .'/multi-select-chosen/chosen.jquery.js',
            NEW_CSS_JS . '_' . lang('DIR') .'/fancybox/jquery.fancybox.pack.js',
            NEW_CSS_JS . '_' . lang('DIR') .'/datetimepicker/jquery-ui.min.js',
            NEW_CSS_JS . '_' . lang('DIR') .'/datetimepicker/jquery-ui-sliderAccess.js',
            NEW_CSS_JS . '_' . lang('DIR') .'/datetimepicker/jquery-ui-timepicker-addon.js'
        );
        foreach($this->js as $url)
            if(ENVIRONMENT == 'production')
                echo file_get_contents($url).' ';
            else
                echo file_get_contents($url).' ';
    }
    public function css(){
        header("Content-type: text/css");
        $this->css = array(
            NEW_CSS_JS . '_' . lang('DIR') .'/common-css.css',
            NEW_CSS_JS . '_' . lang('DIR') .'/slide-menu.css',
            NEW_CSS_JS .'/font-awesome.css',
            NEW_CSS_JS . '_' . lang('DIR') .'/glyphicons_filetypes.css',
            NEW_CSS_JS . '_' . lang('DIR') .'/glyphicons_regular.css',
            NEW_CSS_JS . '_' . lang('DIR') .'/glyphicons_social.css',
            NEW_CSS_JS . '_' . lang('DIR') .'/jquery-ui.css'.
            NEW_CSS_JS . '_' . lang('DIR') .'/validationEngine.jquery.css',
            NEW_CSS_JS . '_' . lang('DIR') .'/tipsy.css',
            NEW_CSS_JS . '_' . lang('DIR') .'/accordionmenu.css',
            NEW_CSS_JS . '_' . lang('DIR') .'/wizard/formToWizard.css',
            NEW_JS .'/plugins/scroll/jquery.custom-scrollbar.css',
            NEW_CSS_JS . '_' . lang('DIR') .'/top-dropdown/css/styles.css',
            NEW_CSS_JS . '_' . lang('DIR') .'/multiselect/multiselect.css',
            NEW_CSS_JS . '_' . lang('DIR') .'/multi-select-chosen/chosen.css',
            NEW_CSS_JS . '_' . lang('DIR') .'/fancybox/jquery.fancybox.css',
            NEW_CSS_JS . '_' . lang('DIR') .'/datetimepicker/jquery-ui.css',
            NEW_CSS_JS . '_' . lang('DIR') .'/datetimepicker/jquery-ui-timepicker-addon.css',
            base_url("static/alertify") .'/alertify.core.css',
            base_url("static/alertify") .'/alertify.default.css',
        );
        foreach($this->css as $url)
            if(ENVIRONMENT == 'production')
                echo file_get_contents($url).' ';
            else
                echo file_get_contents($url).' ';
    }
    
    
    function get_css() {
        header("content-type: application/x-javascript");
        
    }
    
}

