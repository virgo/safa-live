<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Packages extends Safa_Controller {

    public $module = "packages";
    public $layout = 'new';
    public $table_pk = 'crm_package_id';

    public function __construct() {
        parent::__construct();
        $this->load->model('crm_packages', 'packages_model');
        $this->lang->load('packages');
        
        permission();
    }

    public function index() {
        $data['table_pk'] = $this->table_pk;
        $data['module'] = $this->module;
        $this->packages_model->custom_select = 'crm_packages.*'
                . ', (SELECT ' . name() . ' FROM erp_currencies WHERE erp_currencies.erp_currency_id = crm_packages.erp_currency_id) AS currency'
                . ', (SELECT ' . name() . ' FROM erp_countries WHERE erp_countries.erp_country_id = crm_packages.erp_country_id) AS country'
                . ', (SELECT COUNT(*) FROM `crm_ea_payments` WHERE `crm_ea_payments`.`crm_package_id` = `crm_packages`.`crm_package_id` ) AS "delete"';
        if (isset($_GET['search']))
            $this->search();
        $data["total_rows"] = $this->packages_model->search(true);
        $this->packages_model->offset = $this->uri->segment("3");
        $this->packages_model->limit = config('per_page');
        $data["items"] = $this->packages_model->search();
        $config['uri_segment'] = 3;
        $config['base_url'] = site_url('packages/index');
        $config['total_rows'] = $data["total_rows"];
        $config['per_page'] = $this->packages_model->limit;
        $config['suffix'] = $this->input->get() ? '?' . http_build_query($_GET) : NULL;
        $this->load->library('pagination');
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();
        $this->load->view('packages/index', $data);
    }

    public function manage($id = false) {
        $data['module'] = $this->module;
        if ($id) {
            $this->packages_model->crm_package_id = $id;
            $data['item'] = $this->packages_model->get();

            if (!$data['item'])
                show_404();
        }
        else {
            $data['item'] = new stdClass;
            $data['item']->erp_country_id = NULL;
            $data['item']->erp_currency_id = NULL;
            $data['item']->name = NULL;
            $data['item']->amount = NULL;
            $data['item']->credit = NULL;
            $data['item']->additional_credit = NULL;
        }
        $this->load->library("form_validation");
        $this->form_validation->set_rules('erp_country_id', lang($data['module'] . '_erp_country_id'), 'required');
        $this->form_validation->set_rules('erp_currency_id', lang($data['module'] . '_erp_currency_id'), 'required');
        $this->form_validation->set_rules('name', lang($data['module'] . '_name'), 'required');
        $this->form_validation->set_rules('amount', lang($data['module'] . '_amount'), 'required|numeric');
        $this->form_validation->set_rules('credit', lang($data['module'] . '_credit'), 'required|integer');
        $this->form_validation->set_rules('additional_credit', lang($data['module'] . '_additional_credit'), 'required|integer');
//        $this->form_validation->set_rules('', lang($data['module'].'_'), 'required');
        if ($this->form_validation->run() == false) {
            $this->load->view("packages/manage", $data);
        } else {
            $this->packages_model->erp_country_id = $this->input->post('erp_country_id');
            $this->packages_model->name = $this->input->post('name');
            $this->packages_model->amount = $this->input->post('amount');
            $this->packages_model->credit = $this->input->post('credit');
            $this->packages_model->erp_currency_id = $this->input->post('erp_currency_id');
            $this->packages_model->additional_credit = $this->input->post('additional_credit');
            if ($id)
                $this->packages_model->crm_package_id = $id;
            $this->packages_model->save();
            redirect("packages/index");
        }
    }

    function delete($id) {
        if (!$id)
            show_404();

        $this->packages_model->crm_package_id = $id;
        if (!$this->packages_model->delete())
            show_404();
        redirect("packages/index");
    }

    function search() {
        if ($this->input->get("crm_package_id"))
            $this->packages_model->crm_package_id = $this->input->get("crm_package_id");
        if ($this->input->get("erp_country_id"))
            $this->packages_model->erp_country_id = $this->input->get("erp_country_id");
        if ($this->input->get("name"))
            $this->packages_model->name = $this->input->get("name");
        if ($this->input->get("amount"))
            $this->packages_model->amount = $this->input->get("amount");
        if ($this->input->get("credit"))
            $this->packages_model->credit = $this->input->get("credit");
        if ($this->input->get("erp_currency_id"))
            $this->packages_model->erp_currency_id = $this->input->get("erp_currency_id");
        if ($this->input->get("additional_credit"))
            $this->packages_model->additional_credit = $this->input->get("additional_credit");
    }

}

/* End of file packages.php */
/* Location: ./application/controllers/packages.php */