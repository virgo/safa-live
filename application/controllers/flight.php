<?php

/**
 * Flight Status Class
 * @author Muhammad El-Saeed <m.elsaeed@virgotel.com>
 */
class Flight extends Safa_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('fs_model');
        $this->load->model('notification_model');
        $this->layout = 'ajax';
        date_default_timezone_set('Etc/Universal');
        echo "<pre>";
    }

    private function trip_details($date, $airline, $flight) {
        list($year, $month, $day) = explode('-', $date);
        $url = "https://api.flightstats.com/flex/flightstatus/rest/v2/json/flight/status/$airline/$flight/arr/$year/$month/$day?appId=" . FS_APPID . "&appKey=" . FS_APPKEY . "&utc=true";
        return json_decode(file_get_contents($url));
    }

    private function getFlightData($date, $airline, $flight, $purpose = "Initial request") {
        $trip_details = $this->trip_details($date, $airline, $flight);
        if (!$trip_details)
            return FALSE;
        if (isset($trip_details)) {
            $this->fs_model->request = $trip_details;
            if ($trip_details->flightStatuses && is_array($trip_details->flightStatuses) && count($trip_details->flightStatuses)) {
                $states = $trip_details->flightStatuses['0'];
                if ($states->status) {
                    return $this->fs_model->save();
                }
            }
        }
        return FALSE;
    }

    public function cronjob($debug = FALSE) {
        if($debug)
            $this->output->enable_profiler(TRUE);
        
//        echo "CheckUnknownFlights";
        $this->checkUnknownFlights();
//        echo "ScheduledFlights";
        $this->scheduledFlights();
//        echo "ENRouteFlights";
        $this->ENRouteFlights();
//        echo "LandedFlights";
        $this->LandedFlights();
    }

    private function scheduledFlights() {
        // SCHEDULED FLIGHTS
        $flights = $this->db->select('fs_flights.fs_flight_id, fs_flights.fs_status_id, fs_flights.airline, fs_flights.flight, fs_flights.date, fs_flights.last_update, fs_states.*')
                ->where('fs_flights.fs_status_id', 'S')
                ->where('fs_states.scheduledGateDepartureUTC <=', date('Y-m-d H:i:s', strtotime('+5 minutes')))
                            ->where('fs_states.scheduledGateDepartureUTC >= fs_flights.last_update', FALSE, FALSE)
                ->join(FSDB . '.fs_states', 'fs_states.fs_flight_id = fs_flights.fs_flight_id')
                ->get(FSDB . '.fs_flights')
                ->result();
        if ($flights)
            foreach ($flights as $flight) {
                $this->fs_model->fs_flight_id = $flight->fs_flight_id;
                $this->getFlightData($flight->date, $flight->airline, $flight->flight);
            }
    }

    private function ENRouteFlights() {
        // EN-ROUTE
        $route = $this->db->select('fs_flights.fs_flight_id, fs_flights.fs_status_id, fs_flights.airline, fs_flights.flight, fs_flights.date, fs_flights.last_update, fs_states.*')
                ->where('fs_flights.fs_status_id', 'A')
                ->where('fs_states.scheduledGateArrivalUTC <=', date('Y-m-d H:i', strtotime('+5 minutes')))
                ->where('fs_states.scheduledGateArrivalUTC >= fs_flights.last_update', FALSE, FALSE)
                ->join(FSDB . '.fs_states', 'fs_states.fs_flight_id = fs_flights.fs_flight_id')
                ->get(FSDB . '.fs_flights')
                ->result();
//        echo $this->db->last_query();
        if ($route)
            foreach ($route as $flight) {
                $this->fs_model->fs_flight_id = $flight->fs_flight_id;
                $this->getFlightData($flight->date, $flight->airline, $flight->flight);
            }
    }

    private function LandedFlights() {
        // EN-ROUTE
        $flights = $this->db->select('fs_flights.fs_flight_id, fs_flights.fs_status_id, fs_flights.airline, fs_flights.flight, fs_flights.date, fs_flights.last_update, fs_states.*')
                ->where('fs_flights.fs_status_id', 'L')
                ->where('fs_flights.last_update IS NOT NULL', false, false)
                ->where('TIMEDIFF(fs_flights.last_update,actualRunwayArrivalUTC) < "00:30:00"', false, false)
//                ->where('fs_states.scheduledGateArrivalUTC <=', date('Y-m-d H:i', strtotime('+5 minutes')))
//                          ->where('fs_states.scheduledGateDeparture <= fs_flights.last_update', FALSE, FALSE)
                ->join(FSDB . '.fs_states', 'fs_states.fs_flight_id = fs_flights.fs_flight_id')
                ->get(FSDB . '.fs_flights')
                ->result(); 
//        echo $this->db->last_query();
        if ($flights)
            foreach ($flights as $flight) {
                $this->fs_model->fs_flight_id = $flight->fs_flight_id;
                $this->getFlightData($flight->date, $flight->airline, $flight->flight);
            }
    }

    private function checkUnknownFlights() {
        $segments = $this->fs_model->get_null_segments();
        if($segments)
        $this->updateUnknownflights($segments);
        $segments = $this->fs_model->get_hashed_segments();
        if($segments)
        $this->updateUnknownflights($segments);
    }
    
    private function updateUnknownflights($fds) {
        foreach ($fds as $fd) {
            $flight = $this->fs_model->get_flight($fd->code, $fd->flight_number, $fd->flight_date);
            if ($flight)
                $this->fs_model->update_fd_info($fd->erp_flight_availabilities_detail_id, $flight->fs_flight_id);
            else
            {
                // SEND REQUEST AND GET IT'S ID
                $id = $this->getFlightData($fd->flight_date, $fd->code, $fd->flight_number);
                if ($id)
                    $this->fs_model->update_fd_info($fd->erp_flight_availabilities_detail_id, $id);
                else
                    $this->db->where('erp_flight_availabilities_detail_id', $fd->erp_flight_availabilities_detail_id)->update('erp_flight_availabilities_detail', array(
                        'fs_flight_id' => '0',
                        'fs_latest_check' => date('Y-m-d'),
                    ));
            }
        }
    }

    /**
     * Notifications
     * @author Gouda Galal <gouda.galal@virgotel.com>
     */
    function notification() {
        foreach ($this->fs_model->request->flightStatuses as $flight_status) {
            $erp_system_events_id = 0;
            if (isset($flight_status->status)) {
                if ($flight_status->status == 'A') {
                    if ($flight_status->scheduledGateDeparture == $flight_status->actualGateDeparture) {
                        $erp_system_events_id = 2;
                    } else {
                        $erp_system_events_id = 4;
                    }
                } else if ($flight_status->status == 'L') {
                    if ($flight_status->blockMinutes == '') {
                        $erp_system_events_id = 1;
                    } else if ($flight_status->blockMinutes > $flight_status->scheduledBlockMinutes) {
                        $erp_system_events_id = 3;
                    }
                }
            }

            // Notification
            if ($erp_system_events_id != 0) {
                $msg_datetime = date('Y-m-d H:i', time());
                $this->notification_model->notification_type = 'automatic';
                $this->notification_model->erp_importance_id = 1;
                $this->notification_model->sender_type_id = 1;
                $this->notification_model->sender_id = 0;
                $this->notification_model->erp_system_events_id = $erp_system_events_id;
                $this->notification_model->language_id = 2;
                $this->notification_model->msg_datetime = $msg_datetime;
                $airline = $flight_status->airline;
                $flight = $flight_status->flight;
                $flight_date = $flight_status->date;
                $the_date = $msg_datetime;
                $actual_arrival = $flight_status->actualGateArrival;
                $actual_departure = $flight_status->actualGateDeparture;

                $this->notification_model->tags = "#*trip_no#*=$airline-$flight*****#*trip_date#*=$flight_date*****#*the_date#*=$the_date*****#*trip_arrival_datetime#*=$actual_arrival*****#*trip_leaving_datetime#*=$actual_departure";
                $this->notification_model->parent_id = '';

                $erp_notification_id = $this->notification_model->save();

                //-------- Notification Details ---------------
                $this->notification_model->detail_erp_notification_id = $erp_notification_id;
                $this->notification_model->detail_receiver_type = 2;
                $this->notification_model->detail_receiver_id = $flight_status->safa_ea_id;
                $this->notification_model->saveDetails();

                //-------- Notification Details ---------------
                $this->notification_model->detail_erp_notification_id = $erp_notification_id;
                $this->notification_model->detail_receiver_type = 3;
                $this->notification_model->detail_receiver_id = $flight_status->safa_uo_id;
                $this->notification_model->saveDetails();
            }
        }
    }

}
