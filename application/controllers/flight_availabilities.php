<?php
/**
 * @class Flight Availabilities
 * @author Muhammad El-Saeed <m.elsaeed@virgotel.com>
 */
class Flight_availabilities extends Safa_Controller {

    public $layout = 'new';
    public $module = 'flight_availabilities';
    public $direction = 1;

    public function __construct() {
        parent::__construct();

        //Side menu session, By Gouda.
        session('side_menu_id', 1);

        $this->load->model('flight_availabilities_model');
        $this->load->model('safa_group_passports_flight_availabilties_model');


        $this->lang->load($this->module);
        permission();
    }

    public function index() {
        $this->load->library('pagination');
        $data['module'] = $this->module;

        $num = $this->db->select('COUNT(*) as num')
                          ->where('safa_ea_id', session('ea_id'))
                          ->get('erp_flight_availabilities')->row()->num;
        $data["total_rows"] = $num;
        $data["items"] = $this->db->select('erp_flight_availabilities.*')
                          //->limit(config('per_page'), $this->uri->segment("3"))
                          ->where('safa_ea_id', session('ea_id'))
                          ->get('erp_flight_availabilities')->result();
        $config['uri_segment'] = 3;
        $config['base_url'] = site_url($this->module . '/index/');
        $config['total_rows'] = $data["total_rows"];
        $config['per_page'] = config('per_page');
        $config['suffix'] = $this->input->get() ? '?' . http_build_query($_GET) : NULL;

        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();
        $this->load->view('flight_availabilities/index', $data);
    }

    public function manage($id = false) {
        $d['id'] = $id;
        $d['module'] = $this->module;
        $d['erp_flight_classes'] = ddgen('erp_flight_classes', array('erp_flight_class_id', 'name'));
        $d['erp_flight_status'] = ddgen('erp_flight_status', array('erp_flight_status_id', 'name'));
        if (!$id) {
            $d['item'] = new stdClass();
            $d['item']->amadeus = NULL;
            $d['item']->pay_deposit_value = NULL;
            $d['item']->pnr = NULL;
            $d['item']->pay_deposit = NULL;
            $d['item']->to_get_names = NULL;
            $d['item']->check_names = NULL;
            $d['item']->to_issue_ticket = NULL;
            $d['details'] = new stdClass();
        } else {
            $d['item'] = $this->db->where('erp_flight_availability_id', $id)->where('safa_ea_id', session('ea_id'))->get('erp_flight_availabilities')->row();
            $d['details'] = $this->db->select('erp_flight_availabilities_detail.*'
                                    . ',(SELECT iata FROM ' . FSDB . '.`fs_airlines` WHERE ' . FSDB . '.fs_airlines.fs_airline_id = erp_flight_availabilities_detail.erp_airline_id) AS airline'
                                    . ',(SELECT code FROM `erp_ports` WHERE erp_ports.erp_port_id = erp_flight_availabilities_detail.erp_port_id_from) AS erp_port_from'
                                    . ',(SELECT code FROM `erp_ports` WHERE erp_ports.erp_port_id = erp_flight_availabilities_detail.erp_port_id_to) AS erp_port_to')
                            ->where('erp_flight_availability_id', $id)
                            ->get('erp_flight_availabilities_detail')->result();
        }

        $this->load->library('form_validation');
        $this->form_validation->set_rules('amadeus', lang($this->module . '_amadeus'), 'trim');
        $this->form_validation->set_rules('pay_deposit_value', lang($this->module . '_' . 'pay_deposit_value'), 'trim');
        $this->form_validation->set_rules('pnr', lang($this->module . '_' . 'pnr'), 'trim');
        $this->form_validation->set_rules('check_names', lang($this->module . '_' . 'check_names'), 'trim');
        $this->form_validation->set_rules('pay_deposit', lang($this->module . '_' . 'pay_deposit'), 'trim');
        $this->form_validation->set_rules('to_get_names', lang($this->module . '_' . 'to_get_names'), 'trim');
        $this->form_validation->set_rules('to_issue_ticket', lang($this->module . '_' . 'to_issue_ticket'), 'trim');
        if ($this->form_validation->run() == false) {
            $this->load->view('flight_availabilities/manage', $d);
        } else {
            $this->db->set('amadeus', post('amadeus'));
            $this->db->set('pay_deposit_value', post('pay_deposit_value'));
            $this->db->set('pnr', post('pnr'));
            if (!preg_match('/(\d{4})-(\d{2})-(\d){2}/', post('check_names')))
                $this->db->set('check_names', $this->convert_date(post('check_names')));
            else
                $this->db->set('check_names', post('check_names'));
            if (!preg_match('/(\d{4})-(\d{2})-(\d){2}/', post('pay_deposit')))
                $this->db->set('pay_deposit', $this->convert_date(post('pay_deposit')));
            else
                $this->db->set('pay_deposit', post('pay_deposit'));
            if (!preg_match('/(\d{4})-(\d{2})-(\d){2}/', post('to_get_names')))
                $this->db->set('to_get_names', $this->convert_date(post('to_get_names')));
            else
                $this->db->set('to_get_names', post('to_get_names'));
            $this->db->set('to_issue_ticket', post('to_issue_ticket'));

            if (!$id) {
                $this->db->set('safa_ea_id', session('ea_id'));

                //Added By Gouda, For Private flight for specific safa_group_passports
                //-----------------------------------------------------------------------------------------------
                $safa_group_passport_ids_str = $this->input->post('hdn_safa_group_passport_ids');
                if ($safa_group_passport_ids_str != '') {
                    $this->db->set('private', 1);
                }

                $this->db->insert('erp_flight_availabilities');
                $erp_flight_availabilities_id = $this->db->insert_id();


                //Added By Gouda, For Private flight for specific safa_group_passports
                //-----------------------------------------------------------------------------------------------
                //$safa_group_passport_ids_str = $this->input->post('hdn_safa_group_passport_ids');
                if ($safa_group_passport_ids_str != '') {
                    $safa_group_passport_ids_arr = explode(',', $safa_group_passport_ids_str);

                    foreach ($safa_group_passport_ids_arr as $safa_group_passport_id) {
                        $this->safa_group_passports_flight_availabilties_model->safa_group_passport_id = $safa_group_passport_id;
                        $this->safa_group_passports_flight_availabilties_model->erp_flight_availabilty_id = $erp_flight_availabilities_id;
                        $safa_group_passports_flight_availabilties_count = $this->safa_group_passports_flight_availabilties_model->get(true);
                        if ($safa_group_passports_flight_availabilties_count == 0) {
                            $this->safa_group_passports_flight_availabilties_model->save();
                        }
                    }
                }
                //-----------------------------------------------------------------------------------------------
                
            } else {
                $this->db->where('erp_flight_availability_id', $id);
                $this->db->update('erp_flight_availabilities');
            }

            $arr = array(
                'erp_airline_id',
                'flight_number',
                'erp_flight_class_id',
                'flight_date',
                'airports',
                'erp_flight_status_id',
                'seats_count',
                'departure_time',
                'arrival_time'
            );

            foreach ($arr as $item)
                $$item = post($item);

            if ($id) {
                $erp_flight_availabilities_id = $id;
            }
            foreach ($erp_airline_id as $key => $airline) {
                $airport = $this->get_airports($airports[$key]);
//                $_erp_airline_id = $this->flight_availabilities_model->get_airlines($erp_airline_id[$key]);
                
                $this->db->set('airline_code', $erp_airline_id[$key]);
//                $this->db->set('erp_airline_id', $_erp_airline_id);
                $this->db->set('erp_flight_availability_id', $erp_flight_availabilities_id);
//                $this->db->set('remain', $seats_count[$key]);
                if (!preg_match('/(\d{4})-(\d{2})-(\d){2}/', $flight_date[$key]))
                    $this->db->set('flight_date', $flight_date[$key]);
                else
                    $this->db->set('flight_date', $flight_date[$key]);
                $this->db->set('flight_number', $flight_number[$key]);
                $this->db->set('erp_flight_class_id', $erp_flight_class_id[$key]);
                $this->db->set('erp_port_id_from', $airport['from']);
                $this->db->set('erp_path_type_id', $airport['path'][0]);
                $this->db->set('safa_externaltriptype_id', $airport['path'][1]);
                $this->db->set('erp_port_id_to', $airport['to']);
                $this->db->set('erp_flight_status_id', $erp_flight_status_id[$key]);
                $this->db->set('seats_count', $seats_count[$key]);
                $this->db->set('departure_time', $this->translate_time($departure_time[$key]));
                $this->db->set('arrival_time', $this->translate_time($arrival_time[$key]));
                if (!$id)
                    $this->db->insert('erp_flight_availabilities_detail');
                else
                    $this->db->where('erp_flight_availabilities_detail_id', $key)->update('erp_flight_availabilities_detail');
            }
            redirect('flight_availabilities');
        }
    }

    public function delete($id = false) {
        if( ! $id )
            show_404 ();
        
        $item = $this->db->where('erp_flight_availability_id', $id)
                         ->where('safa_ea_id', session('ea_id'))
                         ->where('safa_trip_id')
                         ->get('erp_flight_availabilities')->row();
        if($item)
            $this->db->where('erp_flight_availability_id', $id)
                         ->where('safa_ea_id', session('ea_id'))
                         ->where('safa_trip_id')
                         ->delete('erp_flight_availabilities');
        redirect('flight_availabilities');
    }
    
    private function convert_date($date) {
        $d = substr($date, 0, 2);
        $month_short = substr($date, 2);
        $months = array(
            'JAN' => 1,
            'FEB' => 2,
            'MAR' => 3,
            'APR' => 4,
            'MAY' => 5,
            'JUN' => 6,
            'JUL' => 7,
            'AUG' => 8,
            'SEP' => 9,
            'OCT' => 10,
            'NOV' => 11,
            'DEC' => 12
        );
        return date('Y') . '-' . $months[$month_short] . '-' . $d;
    }

    private function get_airports($airports) {
        $erp_port_code_from = substr($airports, 0, 3);
        $erp_port_code_to = substr($airports, 3);
        $from_airport = $this->flight_availabilities_model->get_airport($erp_port_code_from);
        $to_airport = $this->flight_availabilities_model->get_airport($erp_port_code_to);
        
        return array(
            'from' => $from_airport->erp_port_id,
            'to' => $to_airport->erp_port_id,
            'path' => $this->get_path_type($erp_port_code_from, $erp_port_code_to, $from_airport, $to_airport)
        );
    }

    private function get_path_type($erp_port_code_from, $erp_port_code_to, $from_airport, $to_airport) {
        // GET THE EA COUNTRY ID
        $ea = $this->db->where('safa_ea_id', session('ea_id'))->get('safa_eas')->row();
        $ea_country_id = $ea->erp_country_id;
        // SAUDI ARABIA COUNTRY_ID
        $ksa_country_id = 966;
        // GET THE FIRST AIRPORT COUNTRY_ID
        $from_country_id = $from_airport->erp_country_id;
        // GET THE SECOND AIRPORT COUNTRY_ID
        $to_country_id = $to_airport->erp_country_id;

        /**
         * DIRECTION
         */ 
        if($from_country_id == $ksa_country_id)
            $this->direction = 2;
        if($from_country_id == $ea_country_id)
            $this->direction = 1;
        
        /**
         * PATH
         */
        // FROM KSA
        if($from_country_id == $ksa_country_id ) {
            if($to_country_id == $ksa_country_id ) {
                return array(4, $this->direction); // Path Type - External Segment Type
            }
            return array(1, $this->direction);
        }
        // TO KSA
        if($to_country_id == $ksa_country_id ) {
            return array(2, $this->direction);
        }
        return array(3, $this->direction);
    }

    /**
     * Added By Gouda, To used on trip screen.
     */
    public function popup_trip($flight_trip_type, $trip_id = 0, $arrival_date='') 
    {
        $this->layout = 'js_new';

        $data['flight_trip_type'] = $flight_trip_type;
        $data['trip_id'] = $trip_id;
		$data['arrival_date'] = $arrival_date;
        

        $this->flight_availabilities_model->safa_trip_id = null;

        if (session("ea_id")) {
            $this->flight_availabilities_model->safa_ea_id = session("ea_id");
        }
        /*
          if($flight_trip_type==1) {
          $this->flight_availabilities_model->end_erp_country_id = 966;
          } else if($flight_trip_type==2) {
          $this->flight_availabilities_model->start_erp_country_id = 966;
          }
         */

        //Commented By Gouda.
        //$this->flight_availabilities_model->erp_path_type_id = $flight_trip_type;

        if (!$this->input->get('search')) {
            //$data['results'] = $this->flight_availabilities_model->get_details_for_trip();
            $data['results'] = $this->flight_availabilities_model->get_for_trip();
        } else {
            if ($this->input->get('flight_number')) {
                $this->flight_availabilities_model->flight_number = $this->input->get('flight_number');
            } 
            if ($this->input->get('departure_date')) {
                $this->flight_availabilities_model->flight_date = $this->input->get('departure_date');
            }
        	if ($this->input->get('pnr')) {
                $this->flight_availabilities_model->pnr = $this->input->get('pnr');
            }

            //$data['results'] = $this->flight_availabilities_model->get_details_for_trip();
            $data['results'] = $this->flight_availabilities_model->get_for_trip();
        }

        $this->load->view('flight_availabilities/search_popup_for_trip', $data);
    }
    
    private function translate_time($time) {
        $time = str_replace(':', '', $time);
        if(strlen($time) == 4)
            $time = $time.'00';
        return $time;
    }
    
    
}
