<?php

if (!defined('BASEPATH'))
exit('No direct script access allowed');

class Trip_details extends Safa_Controller {

	public function __construct() {
		parent::__construct();
		$this->layout = 'new';
		$this->load->model('safa_trips_requests_model');
		$this->load->model('trip_details_model');
		$this->load->model('tripstatus_model');

		$this->load->model('safa_trips_model');
		$this->load->model('safa_group_passports_model');
		$this->load->model('flight_availabilities_model');
		$this->load->model('safa_passport_accommodation_rooms_model');
		$this->load->model('safa_group_passport_accommodation_model');
		
		$this->load->model('contracts_model');

		$this->load->model('notification_model');
		$this->load->model('tripstatus_model');
		$this->load->model('safa_uos_model');
		$this->load->model('trip_internaltrip_model');
		$this->load->model('safa_reservation_forms_model');

		$this->load->helper('trip');

		$this->lang->load('trip_details');
		$this->lang->load('travellers');
		$this->lang->load('trips');
		$this->lang->load('group_passports');

		$this->load->library('flight_states');


		permission();
	}

	public function details($id) {
		if (!$id)
		show_404();
		if ($this->destination == 'ea') {
			$this->safa_group_passports_model->ea_id = session('ea_id');
			$this->safa_trips_model->safa_ea_id = session('ea_id');
		} elseif ($this->destination == 'uo') {
			$this->safa_group_passports_model->uo_id = session('uo_id');
			$this->safa_trips_model->safa_uo_id = session('uo_id');
		} elseif ($this->destination == 'ito') {
			$this->safa_group_passports_model->ea_id = session('ito_id');
			$this->safa_trips_model->safa_ito_id = session('ito_id');
		}
		$this->safa_trips_model->safa_trip_id = $id;
		$data['trip'] = $trip_row = $this->safa_trips_model->get();
		if (!$data['trip'])
		show_404();

		if ($this->input->post('delete')) {
			if ($this->input->post('order') && is_array($this->input->post('order')) && count($this->input->post('order')))
			foreach ($this->input->post('order') as $traveller) {
				$this->trip_details_model->safa_trip_id = $id;
				$this->trip_details_model->safa_trip_traveller_id = $traveller;
				$trave = $this->trip_details_model->get_travellers();
				if ($trave) {
					$this->db->where('safa_trip_traveller_id', $traveller)->delete('safa_trip_travellers');
				}
			}
			redirect('/trip_details/details/' . $id . '#tabs-4');
		} else if ($this->input->post('smt_save')) {
			$this->safa_trips_model->safa_trip_id = $id;
			$this->safa_trips_model->safa_tripstatus_id = $this->input->post('safa_tripstatus_id');
			$this->safa_trips_model->uo_refusing_reason = $this->input->post('uo_refusing_reason');

			$this->safa_trips_model->save();


			//----------- Send Notification For Company ------------------------------
			$msg_datetime = date('Y-m-d H:i', time());

			$this->notification_model->notification_type = 'automatic';
			$this->notification_model->erp_importance_id = 1;
			$this->notification_model->sender_type_id = 1;
			$this->notification_model->sender_id = 0;

			$this->notification_model->erp_system_events_id = 24;

			$this->notification_model->language_id = 2;
			$this->notification_model->msg_datetime = $msg_datetime;

			$tags = '';
			if (count($trip_row) > 0) {

				//Link to replace.
				$link_trip = "<a href='" . site_url('ea/trips/manage/' . $trip_row->safa_trip_id) . "'  target='_blank'> " . $trip_row->safa_trip_id . " </a>";
				$link_trip_name = "<a href='" . site_url('ea/trips/manage/' . $trip_row->safa_trip_id) . "'  target='_blank'> " . $trip_row->name . " </a>";

				$this->tripstatus_model->safa_tripstatus_id = $trip_row->safa_tripstatus_id;
				$tripstatus_row = $this->tripstatus_model->get();
				$trip_status = '';
				if (count($tripstatus_row) > 0) {
					$trip_status = $tripstatus_row->{name()};
				}

				$tags = "#*trip_no#*=" . $link_trip . "*****#*trip#*=" . $link_trip_name . "*****#*date#*=" . $trip_row->date . "*****#*trip_date#*=" . $trip_row->date . "*****#*trip_status#*=" . $trip_status;
			}
			$this->notification_model->tags = $tags;
			$this->notification_model->parent_id = '';

			$erp_notification_id = $this->notification_model->save();

			//-------- Notification Details ---------------
			$this->notification_model->detail_erp_notification_id = $erp_notification_id;

			$this->notification_model->detail_receiver_type = 3;
			$this->notification_model->detail_receiver_id = $trip_row->safa_ea_id;
			$this->notification_model->saveDetails();
			//----------------------------------------------
			//-------------------------------------------------------------------




			$this->load->view('redirect_message', array('msg' => lang('global_updated_message'),
                'url' => site_url('/trips_report/'),
                'model_title' => lang('trip_status'), 'action' => lang('edit_trip_status')));
		} else {

			$this->safa_group_passports_model->safa_trip_id = $id;
			$data['travellers'] = $this->safa_group_passports_model->get_for_table();

			$this->trip_details_model->safa_trip_id = $id;
			$data['movable_trips'] = $this->trip_details_model->movable_trips();
			$data['supervisors'] = $this->trip_details_model->get_supervisors();
			//$data['going_trips'] = $this->trip_details_model->trip_path(1);
			//$data['returning_trips'] = $this->trip_details_model->trip_path(2);
			//------------------------ Flights ----------------------------------------
			//$this->flight_availabilities_model->erp_path_type_id=1;
			$this->flight_availabilities_model->safa_trip_id = $id;
			$going_trip_flights_rows = $this->flight_availabilities_model->get_for_trip();
			$data['going_trip_flights_rows'] = $going_trip_flights_rows;
			$data['going_trip_flights_rows_count'] = count($going_trip_flights_rows);
			//------------------------ End Flights ----------------------------------------
			//$data['hotels'] = $this->trip_details_model->trip_hotels();
			$data['hotels'] = $this->safa_trips_model->get_hotels_rooms_for_trip_details($id);
			//            echo $this->db->last_query();die();
			$this->trip_details_model->safa_trip_id = $id;
			$data['internal_trip'] = $this->trip_details_model->get_internal_segments();

			$data['ito_rows'] = $this->safa_trips_model->get_itos_by_trip($id);

			$data['destination'] = $this->destination;

			$safa_trips_requests_rows = $this->safa_trips_requests_model->get_by_trip_id($id);
			$data['safa_trips_requests_rows'] = $safa_trips_requests_rows;

			//-----------------tripstatus combo--------------------------
			$this->tripstatus_model->where = ' safa_tripstatus_id!=1 ';
			$tripstatus_result = $this->tripstatus_model->get();
			$safa_tripstatus_arr = array();
			foreach ($tripstatus_result as $safa_tripstatus => $safa_tripstatus_row) {
				$safa_tripstatus_arr[$safa_tripstatus_row->safa_tripstatus_id] = $safa_tripstatus_row->{name()};
			}
			$data["safa_tripstatus"] = $safa_tripstatus_arr;
			//------------------------------------------------------------
			
			//--------------- Accomodation Forms -------------------------------------------------------
			$this->safa_reservation_forms_model->safa_trip_id = $id;
			$item_safa_reservation_forms_rows = $this->safa_reservation_forms_model->get();
			$data['item_safa_reservation_forms_rows'] = $item_safa_reservation_forms_rows;
			//-----------------------------------------------------------------------------------------
			
			//-------------- Accomedation Available ------------------------
			$this->safa_passport_accommodation_rooms_model->safa_trip_id = $id;
			$safa_passport_accommodation_rooms_rows_count_with_availability = $this->safa_passport_accommodation_rooms_model->get_linked_with_availability(true);
			$data["safa_passport_accommodation_rooms_rows_count_with_availability"] = $safa_passport_accommodation_rooms_rows_count_with_availability;

			$safa_passport_accommodation_rooms_rows_count_without_availability = $this->safa_passport_accommodation_rooms_model->get_without_availability(true);
			$data["safa_passport_accommodation_rooms_rows_count_without_availability"] = $safa_passport_accommodation_rooms_rows_count_without_availability;

			//------------------------------------------------------------

			

			$this->load->view("trip_details/details", $data);
		}
	}

	public function traveller_details($id) {
		if (!$id)
		show_404();
		$this->layout = 'js';
		$this->load->model('travellers_model');
		$this->travellers_model->safa_trip_traveller_id = $id;
		$data['item'] = $this->travellers_model->get();
		if (!$data['item'])
		show_404();

		$data['reative_persons'][] = NULL;
		foreach ($this->travellers_model->get_related_persons($data['item']->safa_trip_id) as $val) {
			$data['reative_persons'][$val['safa_trip_traveller_id']] = $val['first_' . name()] . ' ' . $val['second_' . name()] . ' ' . $val['third_' . name()] . ' ' . $val['fourth_' . name()];
		}

		$this->load->view("trip_details/traveller", $data);
	}

	public function generate_tit($trip_id = FALSE) {
		if (!$trip_id)
		show_404();

		$this->safa_trips_model->safa_trip_id = $trip_id;
		$data['item'] = $this->safa_trips_model->get();
		if (!$data['item'])
		show_404();

		$this->layout = 'ajax';
		$data['itos'][''] = lang('global_select_from_menu');
		$ITO = $this->db->select('safa_itos.safa_ito_id, safa_itos.' . name())
		->where('safa_uo_contracts_eas.safa_ea_id', session('ea_id'))
		->join('safa_ea_itos', 'safa_ea_itos.safa_ito_id = safa_itos.safa_ito_id')
		->join('safa_uo_contracts', 'safa_ea_itos.safa_uo_contract_id = safa_uo_contracts.safa_uo_contract_id')
		->join('safa_uo_contracts_eas', 'safa_uo_contracts.safa_uo_contract_id = safa_uo_contracts_eas.safa_uo_contract_id')
		->get('safa_itos')
		->result();
		foreach ($ITO as $ito)
		$data['itos'][$ito->safa_ito_id] = $ito->{name()};

		//$this->load->library('form_validation');
		//$this->form_validation->set_rules('ito_id', 'lang:ito_id', 'trim|required');
		//By Gouda.
		$contract_safa_uo_id = '';
		$safa_uo_contract_id = '';
		$services_buy_uo_transporter = 0;
		$this->contracts_model->by_eas_id = session('ea_id');
		$this->contracts_model->uo_contracts_eas_disabled = 0;
		$contracts_rows = $this->contracts_model->search();
		if (count($contracts_rows) > 0) {
			$services_buy_uo_transporter = $contracts_rows[0]->services_buy_uo_transporter;
			$contract_safa_uo_id = $contracts_rows[0]->safa_uo_id;
			$safa_uo_contract_id = $contracts_rows[0]->safa_uo_contract_id;
		}
		$data['services_buy_uo_transporter'] = $services_buy_uo_transporter;


		if (!$this->input->post('smt_save_tit')) {
                        $this->layout = 'js_new';
			$this->load->view('ea/trips/trip_internaltrip', $data);
		} else {

			if ($this->input->post('ito_id')) {
				$posted_ito_id = $this->input->post('ito_id');
			} else {
				$posted_ito_id = null;
			}

			$this->safa_trips_model->safa_trip_id = $trip_id;
			$packages_rows = $this->safa_trips_model->get_packages();
			$previous_safa_uo_id=0;
			foreach($packages_rows as $packages_row) {
				$safa_reservation_form_id = $packages_row->safa_reservation_form_id ;
				$erp_transportertype_id = $packages_row->erp_transportertype_id ;

				$this->contracts_model->by_eas_id = session('ea_id');
				$this->contracts_model->uo_contracts_eas_disabled = 0;
				if($packages_row->erp_company_type_id==2){
					$this->contracts_model->safa_uo_id = $packages_row->erp_company_id;
					$contract_safa_uo_id = $packages_row->erp_company_id;
				}
				$contracts_rows = $this->contracts_model->search();
				if (count($contracts_rows) > 0) {
					//$contract_safa_uo_id = $contracts_rows[0]->safa_uo_id;
					$safa_uo_contract_id = $contracts_rows[0]->safa_uo_contract_id;
				}
				
				$repeated_uo=true;
				if($previous_safa_uo_id!=$contract_safa_uo_id) {
					$repeated_uo = false;
				}
				
				$trip_travellers_rows = $this->safa_group_passport_accommodation_model->get_distinct_passports_by_trip_uo_form($trip_id, $contract_safa_uo_id);
				$trip_travellers = count($trip_travellers_rows);
				
				$this->generate_internaltrip($trip_id, $posted_ito_id, $contract_safa_uo_id, $safa_uo_contract_id, $safa_reservation_form_id, $erp_transportertype_id, $repeated_uo, $trip_travellers);
				$previous_safa_uo_id = $contract_safa_uo_id;
			}
			unset($_SESSION['internal_trip_id']);
			echo "<script>parent.$.fancybox.close(); parent.Shadowbox.close();</script>";
		}
	}

	private function generate_internaltrip($trip_id = FALSE, $ito_id = FALSE, $contract_safa_uo_id = FALSE, $safa_uo_contract_id = FALSE, $safa_reservation_form_id = FALSE, $erp_transportertype_id = FALSE, $repeated_uo = FALSE, $trip_travellers = FALSE) 
	{
		$this->load->helper('itconf');
		if (!$ito_id)
		$ito_id = null;

		$this->safa_trips_model->safa_trip_id = $trip_id;
		$trip = $this->safa_trips_model->get();
		if (!$trip)
		show_404();

		
		// GET GOING EXTERNAL SEGMENTS
		# ARRIVAL
		$arrival = $this->db->select('erp_flight_availabilities_detail.*')->join('erp_flight_availabilities', 'erp_flight_availabilities.erp_flight_availability_id = erp_flight_availabilities_detail.erp_flight_availability_id')->where('erp_flight_availabilities.safa_trip_id', $trip_id)->where('erp_flight_availabilities_detail.safa_externaltriptype_id', 1)->get('erp_flight_availabilities_detail')->row();

		# DEPARTURE
		$departure = $this->db->select('erp_flight_availabilities_detail.*')->join('erp_flight_availabilities', 'erp_flight_availabilities.erp_flight_availability_id = erp_flight_availabilities_detail.erp_flight_availability_id')->where('erp_flight_availabilities.safa_trip_id', $trip_id)->where('erp_flight_availabilities_detail.safa_externaltriptype_id', 2)->get('erp_flight_availabilities_detail')->row();

		// GET HOTELS
		//By Gouda
		//$hotels = $this->safa_trips_model->get_trip_hotels($trip_id);
		$hotels = $this->safa_trips_model->get_trip_hotels_for_internaltrip($trip_id, $safa_reservation_form_id);

		# CHECK-IN
		$checkin_hotel = $hotels[0];

		# CHECK-OUT
		$checkout_hotel = end($hotels);

		// TOURISM PLACES
		$tourism_places = $this->db->order_by('datetime', 'ASC')->where('safa_trip_id', $trip_id)->get('safa_trip_tourismplaces')->result();

		if ($ito_id)
		$safa_internaltripstatus_id = 2;
		else
		$safa_internaltripstatus_id = 1;


		if($arrival->erp_flight_availability_id!='') {
			$trip_internaltrip_erp_flight_availability_id = $arrival->erp_flight_availability_id;
		}   else {
			$trip_internaltrip_erp_flight_availability_id = $departure->erp_flight_availability_id;
		}


		//----------------------- By Gouda -------------------------------------
		$safa_uo_code='';
		$safa_uo_serial='';
		$this->contracts_model->safa_uo_contract_id = $safa_uo_contract_id;
		$contracts_row = $this->contracts_model->get();

		if(count($contracts_row)>0) {
			$this->safa_uos_model->safa_uo_id = $contracts_row->safa_uo_id;
			$safa_uo_row = $this->safa_uos_model->get();
			if(count($safa_uo_row)>0) {
				$safa_uo_code = $safa_uo_row->code;
			}

			$safa_uo_row = $this->trip_internaltrip_model->get_max_serial_by_uo($contracts_row->safa_uo_id);
			if(count($safa_uo_row)>0) {
				$safa_uo_serial = $safa_uo_row->max_serial+1;
			}
		}
		//----------------------------------------------------------------------
		if(!$repeated_uo) {
			$trip_internaltrip = array(
	            'safa_trip_id' => $trip_id,
	            'safa_ea_id' => $trip->safa_ea_id,
	
				'trip_title' => $safa_uo_code.'-'.$safa_uo_serial,
				'serial' => $safa_uo_serial,
				'safa_uo_contract_id' => $safa_uo_contract_id,
				'erp_transportertype_id' => $erp_transportertype_id,
	
				// By Gouda.
				//'erp_flight_availability_id' => $arrival->erp_flight_availability_id,
	            'erp_flight_availability_id' => $trip_internaltrip_erp_flight_availability_id,
	            'erp_company_type_id' => '2',
	            'erp_company_id' => $contract_safa_uo_id,
	            'safa_ito_id' => $ito_id,
	            'safa_transporter_id' => NULL,
	            'safa_internaltripstatus_id' => $safa_internaltripstatus_id,
	            'safa_tripstatus_id' => NULL,
	            'operator_reference' => NULL,
	            'attachement' => NULL,
	            'datetime' => $trip->date,
	            'confirmation_number' => gen_itconf()
			);

		$this->db->insert('safa_trip_internaltrips', $trip_internaltrip);
		$internal_trip_id = $this->db->insert_id();
		$_SESSION['internal_trip_id'] = $internal_trip_id;
		} 
		
		//--------- By Gouda ------------
		$this->safa_group_passport_accommodation_model->safa_reservation_form_id = $safa_reservation_form_id;
		$safa_group_passports_rows = $this->safa_group_passport_accommodation_model->get();
		$this->update_safa_group_passports($safa_group_passports_rows, $_SESSION['internal_trip_id']);
		//-------------------------------

		for ($i = 0; $i < count($hotels); $i++) {
			$hotel = $hotels[$i];
			if ($checkin_hotel && $checkin_hotel->erp_hotel_id == $hotel->erp_hotel_id) {
				// GET ARRIVAL PORT ID
				// ARRIVAL SEGMENT 1
				
				if(!$repeated_uo) {
				$this->db->insert('safa_internalsegments', array(
                    'safa_internalsegmenttype_id' => 1,
                    'safa_trip_internaltrip_id' => $_SESSION['internal_trip_id'],
                    'safa_internalsegmentestatus_id' => 1,
                    'start_datetime' => $arrival->flight_date . ' ' . $arrival->arrival_time,
                    'end_datetime' => $checkin_hotel->from_date,
                    'erp_port_id' => $arrival->erp_port_id_to,
                    'erp_start_hotel_id' => NULL,
                    'erp_end_hotel_id' => $checkin_hotel->erp_hotel_id,
                    'safa_tourism_place_id' => NULL,
                    'safa_uo_user_id' => NULL,
                    'seats_count' => $trip_travellers,
				));
				}
				
			} elseif ($checkout_hotel && $checkout_hotel->erp_hotel_id == $hotel->erp_hotel_id) {
				// GET DEPARTURE PORT ID
				// DEPARTURE SEGMENT 2

				// By Gouda
				$backdate = $departure->flight_date. ' ' . $departure->departure_time;
				$portfrom = $departure->erp_port_id_from;
				$movetime=0;
				switch ($portfrom) {
					case 6053:
						if ($hotel->erp_city_id == 1)
						$movetime = 6;
						elseif ($hotel->erp_city_id == 2)
						$movetime = 12;

						break;
					case 6056:
						if ($hotel->erp_city_id == 1)
						$movetime = 7;
						elseif ($hotel->erp_city_id == 2)
						$movetime = 4;
						break;
					case 6070:
						if ($hotel->erp_city_id == 1)
						$movetime = 6;
						elseif ($hotel->erp_city_id == 2)
						$movetime = 6;
						break;
				}
					
				$arrivestamp = strtotime($backdate) - (3600 * $movetime);
				$arrivaldate = date('Y-m-d h:i', $arrivestamp);
					
				$end_datetime = strtotime($backdate) - (3600 * ($movetime-3));
				$end_datetime = date('Y-m-d h:i', $end_datetime);
				
				$trip_travellers_rows = $this->safa_group_passport_accommodation_model->get_distinct_passports_by_trip_uo_form($trip_id, $contract_safa_uo_id, $safa_reservation_form_id);
				$trip_form_travellers = count($trip_travellers_rows);
				
				$this->db->insert('safa_internalsegments', array(
                    'safa_internalsegmenttype_id' => 2,
                    'safa_trip_internaltrip_id' => $_SESSION['internal_trip_id'],
                    'safa_internalsegmentestatus_id' => 1,
                    'start_datetime' => $arrivaldate,
                    'end_datetime' => $end_datetime,
                    'erp_port_id' => $departure->erp_port_id_from,
                    'erp_start_hotel_id' => $checkout_hotel->erp_hotel_id,
                    'erp_end_hotel_id' => NULL,
                    'safa_tourism_place_id' => NULL,
                    'safa_uo_user_id' => NULL,
                    'seats_count' => $trip_form_travellers,
				));
			} //else {
			//}
		}


		for ($i = 0; $i <= count($hotels) - 1; $i++) {
			if (!isset($hotels[$i + 1]))
			break;
			$hotel = $hotels[$i];
			// MOVING SEGMENTS 4
			
			if(!$repeated_uo) {
			$this->db->insert('safa_internalsegments', array(
                'safa_internalsegmenttype_id' => 4,
                'safa_trip_internaltrip_id' => $_SESSION['internal_trip_id'],
                'safa_internalsegmentestatus_id' => 1,
                'start_datetime' => $hotel->to_date,
                'end_datetime' => $hotels[$i + 1]->from_date,
                'erp_port_id' => NULL,
                'erp_start_hotel_id' => $hotel->erp_hotel_id,
                'erp_end_hotel_id' => $hotels[$i + 1]->erp_hotel_id,
                'safa_tourism_place_id' => NULL,
                'safa_uo_user_id' => NULL,
                'seats_count' => $trip_travellers,
			));
			}
		}
		// TOURISM PLACES 3
		foreach ($tourism_places as $place) {
			$hotel = $this->db->where('checkin_datetime <=', $place->datetime)->where('checkout_datetime >=', $place->datetime)->get('safa_trip_hotels', 1)->row();
			if ($hotel)
			$this->db->insert('safa_internalsegments', array(
                    'safa_internalsegmenttype_id' => 3,
                    'safa_trip_internaltrip_id' => $_SESSION['internal_trip_id'],
                    'safa_internalsegmentestatus_id' => 1,
                    'start_datetime' => $place->datetime,
                    'end_datetime' => $place->datetime,
                    'erp_port_id' => NULL,
                    'erp_start_hotel_id' => $hotel->erp_hotel_id,
                    'erp_end_hotel_id' => NULL,
                    'safa_tourism_place_id' => $place->safa_tourismplace_id,
                    'safa_uo_user_id' => NULL,
                    'seats_count' => $trip_travellers,
			));
		}
		//echo "<script>parent.jQuery.fancybox.close();  </script>";
		//die();
	}

	function update_safa_group_passports($safa_group_passports_rows, $safa_trip_internaltrip_id)
	{
		foreach($safa_group_passports_rows as $safa_group_passports_row){
			$this->safa_group_passports_model->safa_group_passport_id = $safa_group_passports_row->safa_group_passport_id ;
			$this->safa_group_passports_model->safa_trip_internaltrip_id = $safa_trip_internaltrip_id;
			$this->safa_group_passports_model->save();
		}
	}

	function view_available_rooms($safa_trip_id = FALSE) {
		$data['safa_trip_id'] = $safa_trip_id;

		$this->safa_passport_accommodation_rooms_model->safa_trip_id = $safa_trip_id;
		$data['safa_passport_accommodation_rooms_hotels_rows'] = $this->safa_passport_accommodation_rooms_model->get_hotels();

		$this->load->view("trip_details/view_available_rooms", $data);
	}

	function trip_hotel_report($safa_trip_id = false) {
		$this->layout = 'ajax';
		$data['roomsizes'] = ddgen('erp_hotelroomsizes', array('erp_hotelroomsize_id', name()), FALSE, FALSE, TRUE);
		$data['safa_trip_id'] = $safa_trip_id;
		$data['hotels'] = array();
		$hotels = $this->safa_trips_model->get_hotels_rooms_for_trip($safa_trip_id);
		foreach ($hotels as $hotel) {
			if (isset($data['hotels'][$hotel->erp_hotels_id])) {
				$data['hotels'][$hotel->erp_hotels_id]['rooms']["$hotel->erp_hotelroomsizes_id"] = $hotel->rooms_count;
			} else {
				$data['hotels'][$hotel->erp_hotels_id] = array('hotel' => $hotel, 'rooms' => array("$hotel->erp_hotelroomsizes_id" => $hotel->rooms_count));
			}
		}

		/**
		 * @author Muhammad El-Saeed <m.elsaeed@virgotel.com>
		 */
		$this->trip_details_model->safa_trip_id = $safa_trip_id;
		$data['internalsegments'] = $this->trip_details_model->get_internal_segments();
		$this->load->view("trip_details/trip_hotel_report", $data);
	}

	function trip_voucher($safa_trip_id = false) {
		$this->layout = 'ajax';
		$data['roomsizes'] = ddgen('erp_hotelroomsizes', array('erp_hotelroomsize_id', name()), FALSE, FALSE, TRUE);
		$data['safa_trip_id'] = $safa_trip_id;
		$data['hotels'] = array();
		$data['groups'] = $this->safa_trips_model->get_trip_groups($safa_trip_id);
		$data['ea'] = $this->safa_trips_model->get_trip_ea($safa_trip_id);

		$this->db->where('safa_group_passports.safa_trip_id', $safa_trip_id);
		$this->db->where('safa_group_passports.date_of_birth > DATE_SUB(CURDATE(), INTERVAL 12 YEAR)');
		$data['childs'] = $this->db->from('safa_group_passports')->count_all_results();

		$hotels = $this->safa_trips_model->trip_voucher($safa_trip_id);

		$this->load->library('hijrigregorianconvert');
		$data['dayhijri'] = $this->hijrigregorianconvert->GregorianToHijri(date('d-m-Y'), 'DD-MM-YYYY');
		$peoplecount = 0;
		$totalamount = 0;
		foreach ($hotels as $hotel) {
			if (isset($data['hotels'][$hotel->safa_package_periods_id][$hotel->erp_hotelroomsizes_id])) {
				$data['hotels'][$hotel->safa_package_periods_id]["$hotel->erp_hotelroomsizes_id"]['hotel'][] = $hotel;
			} else {
				$data['hotels'][$hotel->safa_package_periods_id]["$hotel->erp_hotelroomsizes_id"] = array(
                    'safa_packages_name' => $hotel->safa_packages_name, 'hotel' => array($hotel),
                    'roomtype' => $hotel->erp_hotelroomsize_name, 'roomcount' => $hotel->rooms_count,
                    'people' => $hotel->people, 'people_price' => number_format($hotel->price, 0), 'total' => number_format($hotel->total, 0)
				);
				$peoplecount += (int) $hotel->people;
				$totalamount += $hotel->total;
			}
		}
		$data['peoplecount'] = $peoplecount;
		$data['totalamount'] = number_format($totalamount, 0);
		$this->load->view("trip_details/trip_voucher", $data);
	}

	function trip_packages_rooms($safa_trip_id = false) {
		$this->layout = 'ajax';
		$data['roomsizes'] = ddgen('erp_hotelroomsizes', array('erp_hotelroomsize_id', name()), FALSE, FALSE, TRUE);
		$data['safa_trip_id'] = $safa_trip_id;
		$data['hotels'] = array();
		$data['ea'] = $this->safa_trips_model->get_trip_ea($safa_trip_id);
		$data['trip'] = $this->db->where('safa_trip_id',$safa_trip_id)->get('safa_trips')->row();

		$hotels = $this->safa_trips_model->trip_voucher($safa_trip_id);

		$this->load->library('hijrigregorianconvert');
		$data['dayhijri'] = $this->hijrigregorianconvert->GregorianToHijri(date('d-m-Y'), 'DD-MM-YYYY');
		foreach ($hotels as $hotel) {
			if (isset($data['hotels'][$hotel->safa_package_periods_id])) {
				$data['hotels'][$hotel->safa_package_periods_id]['hotel'][$hotel->erp_hotels_id] = $hotel;
				$data['hotels'][$hotel->safa_package_periods_id]['rooms'][$hotel->erp_hotelroomsizes_id] = $hotel->rooms_count;
			} else {
				$data['hotels'][$hotel->safa_package_periods_id] = array(
                    'safa_packages_name' => $hotel->safa_packages_name, 'hotel' => array($hotel->erp_hotels_id => $hotel),
                    'rooms' => array($hotel->erp_hotelroomsizes_id => $hotel->rooms_count)
				);
			}
		}

		$this->load->view("trip_details/trip_packages_rooms", $data);
	}

}
