<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Ea_payments extends Safa_Controller {

    public $module = "ea_payments";
    public $layout = 'new';
    public $table_pk = 'crm_ea_payment_id';

    public function __construct() {
        parent::__construct();
        $this->load->model('ea_payments_model');
        $this->lang->load('ea_payments');
        
        permission();
    }

    public function index($ea_license_id = false) {
        if (!$ea_license_id)
            show_404();
        $license = $this->db->where('crm_ea_license_id', $ea_license_id)->get('crm_ea_licenses')->row();
        if (!$license)
            show_404();
        $data['ea_license_id'] = $ea_license_id;
        $this->ea_payments_model->custom_select = 'crm_ea_payments.*,'
                . '(SELECT ' . name() . ' FROM erp_currencies WHERE crm_ea_payments.erp_currency_id = erp_currencies.erp_currency_id) AS currency,'
                . '(SELECT username FROM erp_admins WHERE crm_ea_payments.erp_admin_id = erp_admins.erp_admin_id ) AS admin,'
                . '(SELECT ' . name() . ' FROM crm_payment_types WHERE crm_ea_payments.payment_type = crm_payment_types.crm_payment_type_id ) AS payment';
        $data['module'] = $this->module;
        $data['table_pk'] = $this->table_pk;
        $this->ea_payments_model->crm_ea_license_id = $ea_license_id;
        if (isset($_GET['search']))
            $this->search();
        $data["total_rows"] = $this->ea_payments_model->search(true);
        $this->ea_payments_model->offset = $this->uri->segment("4");
        $this->ea_payments_model->limit = config('per_page');
        $data["items"] = $this->ea_payments_model->search();
        $config['uri_segment'] = 4;
        $config['base_url'] = site_url('ea_payments/index/' . $ea_license_id);
        $config['total_rows'] = $data["total_rows"];
        $config['per_page'] = $this->ea_payments_model->limit;
        $config['suffix'] = $this->input->get() ? '?' . http_build_query($_GET) : NULL;
        $this->load->library('pagination');
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();
        $this->load->view('ea_payments/index', $data);
    }

    public function manage($license_id = false, $id = false) {
        if (!$license_id)
            show_404();
        $license = $this->db->where('crm_ea_license_id', $license_id)->get('crm_ea_licenses')->row();
        if (!$license)
            show_404();
        $this->load->library("form_validation");

        $data['id'] = $id;
        $data['module'] = $this->module;
        $data['country_packages'] = ddquery('SELECT crm_packages.crm_package_id, crm_packages.name '
                . ' FROM `crm_packages` '
                . 'INNER JOIN `safa_eas` ON `safa_eas`.`erp_country_id` = `crm_packages`.`erp_country_id`'
                . 'WHERE safa_eas.safa_ea_id = "' . $license->safa_ea_id . '"', array('crm_package_id', 'name'));
        if ($id) {
            $this->ea_payments_model->crm_ea_payment_id = $id;
            $data['item'] = $this->ea_payments_model->get();
            if (!$data['item'])
                show_404();
        }
        else {
            $data['item'] = new stdClass();
            $data['item']->crm_cashbox_id = NULL;
            $data['item']->crm_package_id = NULL;
            $data['item']->payment_type = 1;
            $data['item']->amount = NULL;
            $data['item']->erp_currency_id = NULL;
            $data['item']->voucher = NULL;
            $data['item']->notes = NULL;
            $data['item']->date = date('Y-m-d H:i');
            $data['item']->credit = NULL;
        }
        $this->form_validation->set_rules('amount', lang($data['module'] . '_amount'), 'trim|required|numeric');
        $this->form_validation->set_rules('erp_currency_id', lang($data['module'] . '_erp_currency_id'), $this->input->post('crm_package_id') ? 'trim' : 'trim|required');
        $this->form_validation->set_rules('credit', lang($data['module'] . '_credit'), $this->input->post('crm_package_id') ? 'trim' : 'trim|required');
        $this->form_validation->set_rules('payment_type', lang($data['module'] . '_payment_type'), 'required');
        $this->form_validation->set_rules('crm_cashbox_id', lang($data['module'] . '_crm_cashbox_id'), 'trim|required');
        $this->form_validation->set_rules('crm_package_id', lang($data['module'] . '_crm_package_id'), 'trim');
        $this->form_validation->set_rules('voucher', lang($data['module'] . '_voucher'), 'required');
        $this->form_validation->set_rules('date', lang($data['module'] . '_date'), 'required');
        $this->form_validation->set_rules('notes', lang($data['module'] . '_notes'), 'trim');
        if ($this->form_validation->run() == false) {
            $this->load->view("ea_payments/manage", $data);
        } else {
            if ($id)
                $this->ea_payments_model->crm_ea_payment_id = $id;
            $this->ea_payments_model->crm_ea_license_id = $license_id;
            $cashbox = $this->db->where('crm_cashbox_id', $this->input->post('crm_cashbox_id'))->get('crm_cashbox')->row();
            if ($this->input->post('crm_package_id')) {
                $this->load->model('crm_packages');
                $this->crm_packages->crm_package_id = $this->input->post('crm_package_id');
                $package = $this->crm_packages->get();
                $this->ea_payments_model->crm_package_id = $this->input->post('crm_package_id');
                $this->ea_payments_model->credit = $package->credit + $package->additional_credit;
            } else {
                $this->ea_payments_model->credit = $this->input->post('credit');
            }
            $this->ea_payments_model->voucher = $this->input->post('voucher');
            $this->ea_payments_model->notes = $this->input->post('notes');
            $this->ea_payments_model->date = $this->input->post('date');
            $this->ea_payments_model->erp_currency_id = $cashbox->erp_currency_id;
            $this->ea_payments_model->amount = $this->input->post('amount');
            $this->ea_payments_model->payment_type = $this->input->post('payment_type');
            $this->ea_payments_model->erp_admin_id = session('admin_user_id');
            $this->ea_payments_model->crm_cashbox_id = $this->input->post('crm_cashbox_id');
            if (!$this->input->post('crm_package_id'))
                $this->ea_payments_model->crm_package_id = NULL;
            $this->ea_payments_model->submission_datetime = date('Y-m-d H:i');
            $id = $this->ea_payments_model->save();

            $this->db->insert('crm_ea_payments_log', array(
                'crm_ea_payment_id' => $id,
                'crm_cashbox_id' => $this->ea_payments_model->crm_cashbox_id,
                'crm_ea_license_id' => $license_id,
                'crm_package_id' => $this->ea_payments_model->crm_package_id,
                'erp_admin_id' => $this->ea_payments_model->erp_admin_id,
                'payment_type' => $this->ea_payments_model->payment_type,
                'amount' => $this->ea_payments_model->amount,
                'erp_currency_id' => $this->ea_payments_model->erp_currency_id,
                'voucher' => $this->ea_payments_model->voucher,
                'notes' => $this->ea_payments_model->notes,
                'date' => $this->ea_payments_model->date,
                'credit' => $this->ea_payments_model->credit,
                'submission_datetime' => $this->ea_payments_model->submission_datetime
            ));
            redirect("ea_payments/index/" . $license_id);
        }
    }

    public function log($id = false) {
        if (!$id)
            show_404();
        $this->layout = 'js_new';
        $data['module'] = $this->module;
        $payment = $this->db->where('crm_ea_payment_id', $id)->get('crm_ea_payments')->row();
        if (!$payment)
            show_404();

        $data['items'] = $this->db->select('crm_ea_payments_log.*,'
                        . '(SELECT ' . name() . ' FROM erp_currencies WHERE crm_ea_payments_log.erp_currency_id = erp_currencies.erp_currency_id) AS currency,'
                        . '(SELECT username FROM erp_admins WHERE crm_ea_payments_log.erp_admin_id = erp_admins.erp_admin_id ) AS admin,'
                        . '(SELECT ' . name() . ' FROM crm_payment_types WHERE crm_ea_payments_log.payment_type = crm_payment_types.crm_payment_type_id ) AS payment')
                ->where('crm_ea_payment_id', $id)
                ->get('crm_ea_payments_log')
                ->result();

        $this->load->view('ea_payments/log', $data);
    }

    public function package() {
        $this->layout = 'ajax';
        if ($this->input->post('package_id')) {
            $this->db->where('crm_packages.crm_package_id', $this->input->post('package_id'));
        } else {
            echo json_encode(array('release' => 1));
        }

//        $cashbox = $this->db->select('crm_cashbox.crm_cashbox_id, crm_cashbox.name')
//                          ->join('crm_packages', 'crm_packages.erp_currency_id = crm_cashbox.erp_currency_id')
//                          ->get('crm_cashbox')
//                          ->result();
        $package = $this->db->where('crm_packages.crm_package_id', $this->input->post('package_id'))->get('crm_packages')->row();
        echo json_encode(array(
            'release' => 0,
//            'crm_cashbox_id' => (int) $cashbox->crm_cashbox_id,
            'amount' => $package->amount,
            'erp_currency_id' => $package->erp_currency_id,
            'credit' => $package->credit + $package->additional_credit,
        ));
    }

    public function cashbox($id = false) {
        $this->layout = 'ajax';
        if (!$id)
            show_404();
        echo json_encode(array('erp_currency_id' => $this->db->where('crm_cashbox_id', $id)->get('crm_cashbox')->row()->erp_currency_id));
    }

    function search() {
        if ($this->input->get("crm_ea_payment_id"))
            $this->ea_payments_model->crm_ea_payment_id = $this->input->get("crm_ea_payment_id");
        if ($this->input->get("crm_cashbox_id"))
            $this->ea_payments_model->crm_cashbox_id = $this->input->get("crm_cashbox_id");
        if ($this->input->get("crm_ea_license_id"))
            $this->ea_payments_model->crm_ea_license_id = $this->input->get("crm_ea_license_id");
        if ($this->input->get("crm_package_id"))
            $this->ea_payments_model->crm_package_id = $this->input->get("crm_package_id");
        if ($this->input->get("erp_admin_id"))
            $this->ea_payments_model->erp_admin_id = $this->input->get("erp_admin_id");
        if ($this->input->get("payment_type"))
            $this->ea_payments_model->payment_type = $this->input->get("payment_type");
        if ($this->input->get("amount"))
            $this->ea_payments_model->amount = $this->input->get("amount");
        if ($this->input->get("erp_currency_id"))
            $this->ea_payments_model->erp_currency_id = $this->input->get("erp_currency_id");
        if ($this->input->get("voucher"))
            $this->ea_payments_model->voucher = $this->input->get("voucher");
        if ($this->input->get("notes"))
            $this->ea_payments_model->notes = $this->input->get("notes");
        if ($this->input->get("date"))
            $this->ea_payments_model->date = $this->input->get("date");
        if ($this->input->get("credit"))
            $this->ea_payments_model->credit = $this->input->get("credit");
        if ($this->input->get("submition_datetime"))
            $this->ea_payments_model->submition_datetime = $this->input->get("submition_datetime");
    }

}

/* End of file ea_payments.php */
/* Location: ./application/controllers/ea_payments.php */