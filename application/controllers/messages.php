<?php

class Messages extends Safa_Controller {

    public function __construct() {
        parent::__construct();
        $this->layout = 'new';
        $this->lang->load('messages');
        $this->load->model('messages_model');
        $this->load->model('erp_messages_user_seen_model');

        $this->load->model('erp_importances_model');
        $this->load->model('hotels_model');

        $this->load->model('uos_model');
        $this->load->model('eas_model');
        $this->load->model('hotel_marketing_companies_model');
        $this->load->model('countries_model');
        $this->load->library('form_validation');
        permission();
    }

    public function index() {
        // MAIN DROPDOWNS
        $data['company_types'] = ddgen('erp_company_types', array('erp_company_type_id', name()), false, false, true);
        //$data['countries'] = ddgen('erp_countries', array('erp_country_id', name()), false, false, true);
        $data['importances'] = ddgen('erp_importances', array('erp_importances_id', name()));


        $structure = array('erp_country_id', name());
        $key = $structure['0'];
        if (isset($structure['1'])) {
            $value = $structure['1'];
        }
        $erp_countries_arr = array();
        $erp_countries_arr['all'] = lang('global_all');
        $erp_countries = $this->countries_model->get();
        $data['countries_rows'] = $erp_countries;
        foreach ($erp_countries as $erp_country) {
            $erp_countries_arr[$erp_country->$key] = $erp_country->$value;
        }
        $data['countries'] = $erp_countries_arr;



        $structure = array('safa_uo_id', name());
        $key = $structure['0'];
        if (isset($structure['1'])) {
            $value = $structure['1'];
        }
        $safa_uos_arr = array();
        $safa_uos_arr['all'] = lang('global_all');
        $safa_uos = $this->uos_model->get();
        $data['uos_rows'] = $safa_uos;
        foreach ($safa_uos as $safa_uo) {
            $safa_uos_arr[$safa_uo->$key] = $safa_uo->$value;
        }
        $data['uos'] = $safa_uos_arr;





        $structure = array('safa_ea_id', name());
        $key = $structure['0'];
        if (isset($structure['1'])) {
            $value = $structure['1'];
        }
        $safa_eas_arr = array();

        $safa_eas = $this->eas_model->get();
        foreach ($safa_eas as $safa_ea) {
            $safa_eas_arr[$safa_ea->$key] = $safa_ea->$value;
        }
        $data['eas'] = $safa_eas_arr;




        $structure = array('safa_hm_id', name());
        $key = $structure['0'];
        if (isset($structure['1'])) {
            $value = $structure['1'];
        }
        $safa_hms_arr = array();
        $safa_hms_arr['all'] = lang('global_all');
        $safa_hms = $this->hotel_marketing_companies_model->get();
        $data['hms_rows'] = $safa_hms;
        foreach ($safa_hms as $safa_hm) {
            $safa_hms_arr[$safa_hm->$key] = $safa_hm->$value;
        }
        $data['hms'] = $safa_hms_arr;




        if (isset($_POST['smt_send'])) {
            //$this->form_validation->set_rules('erp_company_type_id', lang('company_type'), 'trim|required');
            $this->form_validation->set_rules('message_subject', lang('message_subject'), 'trim|required');
            $this->form_validation->set_rules('message_body', lang('message_body'), 'trim|required');

            if ($this->form_validation->run() == true) {

                // By Gouda.
                /*
                  $erp_company_type_id = $this->input->post('erp_company_type_id');
                  $countries = $this->input->post('countries');
                  $companies = $this->input->post('companies');
                 */

                $message_subject = $this->input->post('message_subject');
                $message_body = $this->input->post('message_body');
                $erp_importance_id = $this->input->post('erp_importance_id');

                $attachement = $this->input->post('attachement');

                /**
                 * Get current session of current user login
                 */
                $current_user_type_id = 0;
                $current_loin_sender_id = 0;
                $current_loin_user_id = 0;

                if (session('admin_user_id')) {
                    $current_loin_sender_id = session('admin_user_id');
                    $current_loin_user_id = session('admin_user_id');
                    $current_user_type_id = 1;
                } else if (session('uo_id')) {
                    $current_loin_sender_id = session('uo_id');
                    $current_loin_user_id = session('uo_user_id');
                    $current_user_type_id = 2;
                } else if (session('ea_id')) {
                    $current_loin_sender_id = session('ea_id');
                    $current_loin_user_id = session('ea_user_id');
                    $current_user_type_id = 3;
                } else if (session('hm_id')) {
                    $current_loin_sender_id = session('hm_id');
                    $current_loin_user_id = session('hm_user_id');
                    $current_user_type_id = 4;
                } else if (session('ito_id')) {
                    $current_loin_sender_id = session('ito_id');
                    $current_loin_user_id = session('ito_user_id');
                    $current_user_type_id = 5;
                }
                //--------------------------------------------------------

                $message_datetime = date('Y-m-d H:i', time());
                $this->messages_model->erp_importance_id = $erp_importance_id;
                $this->messages_model->sender_type_id = $current_user_type_id;
                $this->messages_model->sender_id = $current_loin_sender_id;
                $this->messages_model->sender_user_id = $current_loin_user_id;
                $this->messages_model->message_subject = $message_subject;
                $this->messages_model->message_body = $message_body;
                $this->messages_model->message_datetime = $message_datetime;
                $erp_messages_id = $this->messages_model->save();
                $erp_messages_id_folder = './static/uploads/messages_attachments/' . $erp_messages_id;
                if (!file_exists($erp_messages_id_folder)) {
                    mkdir($erp_messages_id_folder, 0777, true);
                }

                //---------------------------------------- Attachments --------------------------------------------
                $newFilePath = '';
                mb_internal_encoding("UTF-8");
                if (isset($_FILES['attachement'])) {
                    if (count($_FILES['attachement']['tmp_name']) > 0) {
                        for ($i = 0; $i < count($_FILES['attachement']['tmp_name']); $i++) {
                            //Get the temp file path
                            $tmpFilePath = $_FILES['attachement']['tmp_name'][$i];

                            //Make sure we have a filepath
                            if ($tmpFilePath != "") {
                                //Setup our new file path
                                $newFilePath = './static/uploads/messages_attachments/' . $erp_messages_id . '/' . $_FILES['attachement']['name'][$i];
                                //Upload the file into the temp dir
                                if (move_uploaded_file($tmpFilePath, $newFilePath)) {
                                    //Handle other code here
                                    $this->messages_model->attachements_erp_messages_id = $erp_messages_id;
                                    $this->messages_model->attachements_path = $newFilePath;
                                    $this->messages_model->saveAttachements();
                                }
                            }
                        }
                    }
                }
                //-------------------------------------------------------------------------------------------------
                //---------------------------------------- Detail -------------------------------------------------
                //By Gouda.
                /*
                  $receiver_type = $erp_company_type_id;
                  $receiver_ids_arr = array();

                  $receiver_type_table = 'erp_admins';
                  if ($receiver_type == 1) {
                  $receiver_type_table = 'erp_admins';
                  $receiver_type_id_field = 'erp_admin_id';
                  } else if ($receiver_type == 2) {
                  $receiver_type_table = 'safa_uos';
                  $receiver_type_id_field = 'safa_uo_id';
                  } else if ($receiver_type == 3) {
                  $receiver_type_table = 'safa_eas';
                  $receiver_type_id_field = 'safa_ea_id';
                  } else if ($receiver_type == 4) {
                  $receiver_type_table='safa_hms';
                  $receiver_type_id_field='safa_hm_id';
                  } else if ($receiver_type == 5) {
                  $receiver_type_table = 'safa_itos';
                  $receiver_type_id_field = 'safa_ito_id';
                  }

                  if (count($companies) == 0) {
                  if (count($countries) == 0) {
                  $this->messages_model->detail_erp_messages_id = $erp_messages_id;
                  $this->messages_model->detail_receiver_type = $receiver_type;
                  $receiver_query = "select * from $receiver_type_table ";
                  if ($receiver_type == 2 || $receiver_type == 3 || $receiver_type == 5) {
                  $receiver_query = $receiver_query . " where `disabled`=0";
                  }
                  $receiver_rows_query = $this->messages_model->db->query($receiver_query);
                  $receiver_rows = $receiver_rows_query->result();
                  foreach ($receiver_rows as $receiver_row) {
                  $this->messages_model->detail_receiver_id = $receiver_row->$receiver_type_id_field;
                  $this->messages_model->saveDetails();
                  }
                  } else {
                  foreach ($countries as $country_id) {
                  $this->messages_model->detail_erp_messages_id = $erp_messages_id;
                  $this->messages_model->detail_receiver_type = $receiver_type;
                  $receiver_query = "select * from $receiver_type_table ";
                  if ($receiver_type == 2 || $receiver_type == 3 || $receiver_type == 5) {
                  $receiver_query = $receiver_query . " where `disabled`=0 ";
                  }
                  if ($receiver_type == 3 || $receiver_type == 5) {
                  $receiver_query = $receiver_query . " and `erp_country_id`='$country_id' ";
                  }

                  $receiver_rows_query = $this->messages_model->db->query($receiver_query);
                  $receiver_rows = $receiver_rows_query->result();
                  foreach ($receiver_rows as $receiver_row) {
                  $this->messages_model->detail_receiver_id = $receiver_row->$receiver_type_id_field;
                  $this->messages_model->saveDetails();
                  }
                  }
                  }
                  } else {
                  foreach ($companies as $company_id) {
                  $this->messages_model->detail_erp_messages_id = $erp_messages_id;
                  $this->messages_model->detail_receiver_type = $receiver_type;
                  $this->messages_model->detail_receiver_id = $company_id;
                  $this->messages_model->saveDetails();
                  }
                  }
                 */


                //------------- Admins ----------------------------------------------
                if (isset($_POST['chk_administrators'])) {
                    $this->messages_model->detail_erp_messages_id = $erp_messages_id;
                    $this->messages_model->detail_receiver_type = 1;
                    $receiver_query = "select * from erp_admins ";

                    $receiver_rows_query = $this->messages_model->db->query($receiver_query);
                    $receiver_rows = $receiver_rows_query->result();
                    foreach ($receiver_rows as $receiver_row) {
                        $this->messages_model->detail_receiver_id = $receiver_row->erp_admin_id;
                        $this->messages_model->saveDetails();
                    }

                    //By Gouda.
                    $this->load->library('email');
                    $this->email->from(config('webmaster_email'), config('title'));
                    $this->email->to('safa.support@virgotel.com');
                    if ($newFilePath != '') {
                        $this->email->attach($newFilePath);
                    }
                    $this->email->subject($message_subject);
                    $this->email->message($message_body);
                    $this->email->send();
                }

                //------------- UOS ----------------------------------------------
                $uos = $this->input->post('uos');
                if (count($uos) != 0) {
                    foreach ($uos as $uo_id) {
                        $this->messages_model->detail_erp_messages_id = $erp_messages_id;
                        $this->messages_model->detail_receiver_type = 2;
                        $this->messages_model->detail_receiver_id = $uo_id;
                        $this->messages_model->saveDetails();
                    }
                }

                //------------- HMS ----------------------------------------------
                $hms = $this->input->post('hms');
                if (count($hms) != 0) {
                    foreach ($hms as $hm_id) {
                        $this->messages_model->detail_erp_messages_id = $erp_messages_id;
                        $this->messages_model->detail_receiver_type = 4;
                        $this->messages_model->detail_receiver_id = $hm_id;
                        $this->messages_model->saveDetails();
                    }
                }

                //------------- EAS ----------------------------------------------
                $eas = $this->input->post('eas');
                if (count($eas) != 0) {
                    foreach ($eas as $ea_id) {
                        $this->messages_model->detail_erp_messages_id = $erp_messages_id;
                        $this->messages_model->detail_receiver_type = 3;
                        $this->messages_model->detail_receiver_id = $ea_id;
                        $this->messages_model->saveDetails();
                    }
                }

                //------------- ITOS ----------------------------------------------
                $itos = $this->input->post('itos');
                if (count($itos) != 0) {
                    foreach ($itos as $ito_id) {
                        $this->messages_model->detail_erp_messages_id = $erp_messages_id;
                        $this->messages_model->detail_receiver_type = 5;
                        $this->messages_model->detail_receiver_id = $ito_id;
                        $this->messages_model->saveDetails();
                    }
                }


                //-------------------------------------------------------------------------------------------------------
            }

            $this->load->view('redirect_message', array('msg' => lang('success_message'),
                'url' => site_url('messages'),
                'model_title' => lang('messages/index'), 'action' => lang('new_message')));
        } else {
            $this->load->view('messages/index', $data);
        }
    }

    public function getCompaniesByCompanyTypeAndCountries() {
        $erp_company_type_id = $this->input->post('erp_company_type_id');
        $countries = $this->input->post('countries');
        $countries_arr = explode(',', $countries);

        $structure = array('id', name());
        $key = $structure['0'];
        if (isset($structure['1'])) {
            $value = $structure['1'];
        }
        $companies_arr = array();
        $companies = $this->messages_model->getCompaniesByCompanyTypeAndCountries($erp_company_type_id, $countries_arr);
        foreach ($companies as $company) {
            $companies_arr[$company->$key] = $company->$value;
        }


        $form_multiselect = form_multiselect('companies[]', $companies_arr, set_value('companies', ''), 'class="chosen-select chosen-rtl input-full" multiple tabindex="4" ');
        $form_multiselect = str_replace('<select name="companies[]" class="chosen-select chosen-rtl input-full" multiple tabindex="4" >', '', $form_multiselect);
        $form_multiselect = str_replace('</select>', '', $form_multiselect);
        echo $form_multiselect;
        exit;
    }

    public function getEasByCompanyTypeAndCountries() {
        $erp_company_type_id = $this->input->post('erp_company_type_id');
        $countries = $this->input->post('countries');
        $countries_arr = explode(',', $countries);

        $structure = array('id', name());
        $key = $structure['0'];
        if (isset($structure['1'])) {
            $value = $structure['1'];
        }
        $companies_arr = array();
        //$companies_arr['all']= lang('global_all');
        $all_countries = false;
        foreach ($countries_arr as $country) {
            if ($country == 'all') {
                $all_countries = true;
            }
        }
        if ($all_countries) {
            $companies = $this->messages_model->getCompaniesByCompanyType($erp_company_type_id);
        } else {
            $companies = $this->messages_model->getCompaniesByCompanyTypeAndCountries($erp_company_type_id, $countries_arr);
        }

        if (count($companies) > 0) {
            $companies_arr['all'] = lang('global_all');
        }
        foreach ($companies as $company) {
            $companies_arr[$company->$key] = $company->$value;
        }


        $form_multiselect = form_multiselect('eas[]', $companies_arr, set_value('eas', ''), 'class="chosen-select chosen-rtl input-full" multiple tabindex="4" ');
        $form_multiselect = str_replace('<select name="eas[]" class="chosen-select chosen-rtl input-full" multiple tabindex="4" >', '', $form_multiselect);
        $form_multiselect = str_replace('</select>', '', $form_multiselect);
        echo $form_multiselect;
        exit;
    }

    public function getItosByCompanyTypeAndCountries() {
        $erp_company_type_id = $this->input->post('erp_company_type_id');
        $countries = $this->input->post('countries');
        $countries_arr = explode(',', $countries);

        $structure = array('id', name());
        $key = $structure['0'];
        if (isset($structure['1'])) {
            $value = $structure['1'];
        }
        $companies_arr = array();

        $all_countries = false;
        foreach ($countries_arr as $country) {
            if ($country == 'all') {
                $all_countries = true;
            }
        }
        if ($all_countries) {
            $companies = $this->messages_model->getCompaniesByCompanyType($erp_company_type_id);
        } else {
            $companies = $this->messages_model->getCompaniesByCompanyTypeAndCountries($erp_company_type_id, $countries_arr);
        }
        if (count($companies) > 0) {
            $companies_arr['all'] = lang('global_all');
        }
        foreach ($companies as $company) {
            $companies_arr[$company->$key] = $company->$value;
        }


        $form_multiselect = form_multiselect('itos[]', $companies_arr, set_value('itos', ''), 'class="chosen-select chosen-rtl input-full" multiple tabindex="4" ');
        $form_multiselect = str_replace('<select name="itos[]" class="chosen-select chosen-rtl input-full" multiple tabindex="4" >', '', $form_multiselect);
        $form_multiselect = str_replace('</select>', '', $form_multiselect);
        echo $form_multiselect;
        exit;
    }

    public function get($user_type, $receiver_id, $user_id) {
        $this->layout = 'ajax';
        $messages = $this->messages_model->getByReceiverTypeAndReceiverIdAndUserId($user_type, $receiver_id, $user_id);
        $unreaded_messages = $this->messages_model->getUnreadedByReceiverTypeAndReceiverIdAndUserId($user_type, $receiver_id, $user_id, true);
        $unreaded_messages_count = $unreaded_messages[0]->unreaded_messages_count;
        if ($unreaded_messages_count == 0) {
            $unreaded_messages_count = '';
        }


        //echo "<li> <div msgs='$unreaded_messages_count' id='ul_user_messages' ></li>";
        // By Gouda, to reduce traffic on website.
        /*
          echo "<li> <!-- <div class='cls_message_top_dv'>".lang('messages')." </div> -->
          <a href='". base_url().'messages/index'."' style='float:left; ' > ".lang('new_message')."</a>
          <br/><br/><br/>
          </li>";
        */

        
        $messages_arr=array();
        foreach ($messages as $message) {
            $mark_as_read = $message->mark_as_read;
            $mark_as_important = $message->mark_as_important;
            $erp_messages_detail_id = $message->erp_messages_detail_id;
            $message_datetime = $message->message_datetime;
            $message_subject = $message->message_subject;
            $message_body = $message->message_body;
            if ($mark_as_important == 1) {
                $cls_message_message_dv = 'cls_message_message_important_dv';
                $mark_as_important_value = 0;
                $mark_as_important_link_text = lang('mark_as_un_important');
            } else {
                if ($mark_as_read == 1) {
                    $cls_message_message_dv = 'cls_message_message_readed_dv';
                } else {
                    $cls_message_message_dv = 'cls_message_message_dv';
                }
                $mark_as_important_value = 1;
                $mark_as_important_link_text = lang('mark_as_important');
            }

            $message_subject_brief = substr($message_subject, 0, 90);
            $message_body_brief = substr($message_body, 0, 90) . '...';

            // By Gouda, to reduce traffic on website.
            /*
              echo("<li>
              <span id='$erp_messages_detail_id'  class='icon-trash message_del' title='" . lang('delete_message') . "' style='float:left; cursor: pointer; margin: 3px 3px 3px 3px;'  ></span>
              <span id='$erp_messages_detail_id' alt='$mark_as_important_value' class='icon-star message_star' title='$mark_as_important_link_text' style='float:left; cursor: pointer; margin: 3px 3px 3px 3px;'  ></span>
              <a class='fancybox fancybox.iframe' href='" . site_url("messages/read/$erp_messages_detail_id") . "'>
              <b>$message_subject</b>
              <br/>
              $message_body_brief
              </a></li>");
             */

            $messages_arr[] = array('unreaded_messages_count' => $unreaded_messages_count,
                'cls_message_message_dv' => $cls_message_message_dv,
                'erp_message_detail_id' => $erp_messages_detail_id,
                'mark_as_important_value' => $mark_as_important_value,
                'mark_as_important_link_text' => $mark_as_important_link_text,
                'message_subject' => $message_subject,
                'message_body_brief' => $message_body_brief);
        }
        echo json_encode($messages_arr);


        $now = time();
        // Commented By Gouda, Untill discuss with Mohamed Attia.
        /*
          foreach ($messages as $message) {
          $message_datetime = $message->message_datetime;
          $message_datetime = strtotime($message_datetime);
          $diff_with_second = $now - $message_datetime;
          if ($diff_with_second == 1) {
          $erp_messages_detail_id = $message->erp_messages_detail_id;
          $message_datetime = $message->message_datetime;
          $message_subject = $message->message_subject;
          $message_body = $message->message_body;
          $message_subject_brief = substr($message_subject, 0, 100);
          $message_body_brief = substr($message_body, 0, 180) . '...';
          $html_alert = " <div id=\"main_message_alert_dv\">       <span class=\"icos-cancel1\" title=\"" . lang('close') . "\" style=\"float:left; cursor: pointer;\" onclick=\"document.getElementById(\"main_message_alert_dv\").style.display = \"none\" \"></span>      <a class=\"fancybox fancybox.iframe\" href=\"" . site_url("messages/read/$erp_messages_detail_id") . "\"><b>$message_subject</b><br/>$message_body_brief</a> </div>";
          }
          }
         */
    }

    function read($erp_messages_detail_id = 0) {
        $this->layout = 'ajax';

        $current_loin_user_id = 0;
        if (session('admin_user_id')) {
            $current_loin_user_id = session('admin_user_id');
        } else if (session('uo_user_id')) {
            $current_loin_user_id = session('uo_user_id');
        } else if (session('ea_user_id')) {
            $current_loin_user_id = session('ea_user_id');
        } else if (session('hm_user_id')) {
            $current_loin_user_id = session('hm_user_id');
        } else if (session('ito_user_id')) {
            $current_loin_user_id = session('ito_user_id');
        }



        $this->erp_messages_user_seen_model->erp_messages_detail_id = $erp_messages_detail_id;
        $this->erp_messages_user_seen_model->user_id = $current_loin_user_id;


        $erp_message_user_seen_rows_count = $this->erp_messages_user_seen_model->get(true);
        $erp_message_user_seen_rows = $this->erp_messages_user_seen_model->get();

        $mark_as_important = 0;

        $this->erp_messages_user_seen_model->mark_as_read = 1;
        //$this->erp_messages_user_seen_model->is_hide = 0;
        $this->erp_messages_user_seen_model->read_datetime = date('Y-m-d H:i', time());

        if ($erp_message_user_seen_rows_count == 0) {
            $this->erp_messages_user_seen_model->save();
        } else {
            $this->erp_messages_user_seen_model->erp_messages_user_seen_id = $erp_message_user_seen_rows[0]->erp_messages_user_seen_id;
            $this->erp_messages_user_seen_model->save();

            $mark_as_important = $erp_message_user_seen_rows[0]->mark_as_important;
        }


        $message_row = $this->messages_model->getByDetailId($erp_messages_detail_id);

        $erp_messages_id = $message_row->erp_messages_id;


        $sender_type_id = $message_row->sender_type_id;
        $sender_id = $message_row->sender_id;

        $sender_type_table = 'erp_admins';
        $sender_type_id_field = 'erp_admin_id';
        $sender_type_name_field = 'username';
        if ($sender_type_id == 1) {
            $sender_type_table = 'erp_admins';
            $sender_type_id_field = 'erp_admin_id';
            $sender_type_name_field = 'username';
        } else if ($sender_type_id == 2) {
            $sender_type_table = 'safa_uos';
            $sender_type_id_field = 'safa_uo_id';
            $sender_type_name_field = name();
        } else if ($sender_type_id == 3) {
            $sender_type_table = 'safa_eas';
            $sender_type_id_field = 'safa_ea_id';
            $sender_type_name_field = name();
        } else if ($sender_type_id == 4) {
            $sender_type_table = 'safa_hms';
            $sender_type_id_field = 'safa_hm_id';
            $sender_type_name_field = name();
        } else if ($sender_type_id == 5) {
            $sender_type_table = 'safa_itos';
            $sender_type_id_field = 'safa_ito_id';
            $sender_type_name_field = name();
        }



        $sender_query = "select * from $sender_type_table ";
        $sender_query = $sender_query . " where $sender_type_id_field=$sender_id";
        $sender_rows_query = $this->messages_model->db->query($sender_query);
        $sender_rows = $sender_rows_query->result();
        $from_name = $sender_rows[0]->$sender_type_name_field;
        $data['from_name'] = $from_name;


        $erp_importance_id = $message_row->erp_importance_id;
        $this->erp_importances_model->erp_importances_id = $erp_importance_id;
        $erp_importances_row = $this->erp_importances_model->get();

        $data['erp_importance_name'] = '';
        if (count($erp_importances_row) > 0) {
            $erp_importance_name = $erp_importances_row->{name()};
            $data['erp_importance_name'] = $erp_importance_name;
        }

        $message_datetime = $message_row->message_datetime;
        $message_subject = $message_row->message_subject;
        $message_body = $message_row->message_body;

        $data['mark_as_important'] = $mark_as_important;
        $data['erp_messages_detail_id'] = $erp_messages_detail_id;
        $data['message_from'] = lang('system');
        $data['message_subject'] = $message_subject;
        $data['message_body'] = $message_body;
        $data['message_datetime'] = $message_datetime;

        $data['attachements'] = $this->messages_model->getAttachementsByMessageId($erp_messages_id);

        $this->load->view('messages/message', $data);
    }

    function hide($erp_messages_detail_id = 0, $mark_as_important_value = 1) {
        $this->layout = 'js';

        $current_loin_user_id = 0;
        if (session('admin_user_id')) {
            $current_loin_user_id = session('admin_user_id');
        } else if (session('uo_user_id')) {
            $current_loin_user_id = session('uo_user_id');
        } else if (session('ea_user_id')) {
            $current_loin_user_id = session('ea_user_id');
        } else if (session('ito_user_id')) {
            $current_loin_user_id = session('ito_user_id');
        } else if (session('hm_user_id')) {
            $current_loin_user_id = session('hm_user_id');
        }

        /*
          echo "
          <script>
          alert('$current_loin_user_id, $erp_messages_detail_id');
          </script>";
         */

        $this->erp_messages_user_seen_model->erp_messages_detail_id = $erp_messages_detail_id;
        $this->erp_messages_user_seen_model->user_id = $current_loin_user_id;

        $erp_message_user_seen_rows_count = $this->erp_messages_user_seen_model->get(true);
        $erp_message_user_seen_rows = $this->erp_messages_user_seen_model->get();

        //$this->erp_messages_user_seen_model->mark_as_read = 0;
        $this->erp_messages_user_seen_model->is_hide = $mark_as_important_value;



        if ($erp_message_user_seen_rows_count == 0) {
            $this->erp_messages_user_seen_model->save();
        } else {
            $this->erp_messages_user_seen_model->erp_messages_user_seen_id = $erp_message_user_seen_rows[0]->erp_messages_user_seen_id;
            $this->erp_messages_user_seen_model->save();
        }
    }

    function markAsImportant($erp_messages_detail_id = 0, $mark_as_important_value = 0) {
        $this->layout = 'js';

        $current_loin_user_id = 0;
        if (session('admin_user_id')) {
            $current_loin_user_id = session('admin_user_id');
        } else if (session('uo_user_id')) {
            $current_loin_user_id = session('uo_user_id');
        } else if (session('ea_user_id')) {
            $current_loin_user_id = session('ea_user_id');
        } else if (session('ito_user_id')) {
            $current_loin_user_id = session('ito_user_id');
        } else if (session('hm_user_id')) {
            $current_loin_user_id = session('hm_user_id');
        }

        /*
          echo "
          <script>
          alert('$current_loin_user_id, $erp_messages_detail_id');
          </script>";
         */

        $this->erp_messages_user_seen_model->erp_messages_detail_id = $erp_messages_detail_id;
        $this->erp_messages_user_seen_model->user_id = $current_loin_user_id;

        $erp_message_user_seen_rows_count = $this->erp_messages_user_seen_model->get(true);
        $erp_message_user_seen_rows = $this->erp_messages_user_seen_model->get();

        //$this->erp_messages_user_seen_model->mark_as_read = 0;
        $this->erp_messages_user_seen_model->mark_as_important = $mark_as_important_value;



        if ($erp_message_user_seen_rows_count == 0) {
            $this->erp_messages_user_seen_model->save();
        } else {
            $this->erp_messages_user_seen_model->erp_messages_user_seen_id = $erp_message_user_seen_rows[0]->erp_messages_user_seen_id;
            $this->erp_messages_user_seen_model->save();
        }
    }

    public function viewAll() {

        $current_user_type_id = 0;
        $current_loin_receiver_id = 0;
        $current_loin_user_id = 0;
        if (session('admin_user_id')) {
            $current_loin_receiver_id = session('admin_user_id');
            $current_loin_user_id = session('admin_user_id');
            $current_user_type_id = 1;
        } else if (session('uo_id')) {
            $current_loin_receiver_id = session('uo_id');
            $current_loin_user_id = session('uo_user_id');
            $current_user_type_id = 2;
        } else if (session('ea_id')) {
            $current_loin_receiver_id = session('ea_id');
            $current_loin_user_id = session('ea_user_id');
            $current_user_type_id = 3;
        } else if (session('hm_id')) {
            $current_loin_receiver_id = session('hm_id');
            $current_loin_user_id = session('hm_user_id');
            $current_user_type_id = 4;
        } else if (session('ito_id')) {
            $current_loin_receiver_id = session('ito_id');
            $current_loin_user_id = session('ito_user_id');
            $current_user_type_id = 5;
        }

        $this->messages_model->offset = $this->uri->segment("3");
        $this->messages_model->limit = 20;

        $data["items"] = $this->messages_model->getByReceiverTypeAndReceiverIdAndUserIdForDisplay($current_user_type_id, $current_loin_receiver_id, false);

        $data["total_rows"] = $this->messages_model->getByReceiverTypeAndReceiverIdAndUserIdForDisplay($current_user_type_id, $current_loin_receiver_id, false, true);

        $config['uri_segment'] = 3;
        $config['base_url'] = site_url('messages/viewAll');
        $config['total_rows'] = $data["total_rows"];
        $config['per_page'] = $this->messages_model->limit;
        $config['suffix'] = $this->input->get() ? '?' . http_build_query($_GET) : NULL;
        $this->load->library('pagination');
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();
        $this->load->view('messages/view_all', $data);
    }

    public function viewAllImportant() {

        $current_user_type_id = 0;
        $current_loin_receiver_id = 0;
        $current_loin_user_id = 0;
        if (session('admin_user_id')) {
            $current_loin_receiver_id = session('admin_user_id');
            $current_loin_user_id = session('admin_user_id');
            $current_user_type_id = 1;
        } else if (session('uo_id')) {
            $current_loin_receiver_id = session('uo_id');
            $current_loin_user_id = session('uo_user_id');
            $current_user_type_id = 2;
        } else if (session('ea_id')) {
            $current_loin_receiver_id = session('ea_id');
            $current_loin_user_id = session('ea_user_id');
            $current_user_type_id = 3;
        } else if (session('hm_id')) {
            $current_loin_receiver_id = session('hm_id');
            $current_loin_user_id = session('hm_user_id');
            $current_user_type_id = 4;
        } else if (session('ito_id')) {
            $current_loin_receiver_id = session('ito_id');
            $current_loin_user_id = session('ito_user_id');
            $current_user_type_id = 5;
        }

        $this->messages_model->offset = $this->uri->segment("3");
        $this->messages_model->limit = 20;

        $data["items"] = $this->messages_model->getByReceiverTypeAndReceiverIdAndUserIdForDisplay($current_user_type_id, $current_loin_receiver_id, false, false, 1);

        $data["total_rows"] = $this->messages_model->getByReceiverTypeAndReceiverIdAndUserIdForDisplay($current_user_type_id, $current_loin_receiver_id, false, true, 1);

        $config['uri_segment'] = 3;
        $config['base_url'] = site_url('messages/viewAll');
        $config['total_rows'] = $data["total_rows"];
        $config['per_page'] = $this->messages_model->limit;
        $config['suffix'] = $this->input->get() ? '?' . http_build_query($_GET) : NULL;
        $this->load->library('pagination');
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();
        $this->load->view('messages/view_all', $data);
    }

    public function viewAllDeleted() {

        $current_user_type_id = 0;
        $current_loin_receiver_id = 0;
        $current_loin_user_id = 0;
        if (session('admin_user_id')) {
            $current_loin_receiver_id = session('admin_user_id');
            $current_loin_user_id = session('admin_user_id');
            $current_user_type_id = 1;
        } else if (session('uo_id')) {
            $current_loin_receiver_id = session('uo_id');
            $current_loin_user_id = session('uo_user_id');
            $current_user_type_id = 2;
        } else if (session('ea_id')) {
            $current_loin_receiver_id = session('ea_id');
            $current_loin_user_id = session('ea_user_id');
            $current_user_type_id = 3;
        } else if (session('hm_id')) {
            $current_loin_receiver_id = session('hm_id');
            $current_loin_user_id = session('hm_user_id');
            $current_user_type_id = 4;
        } else if (session('ito_id')) {
            $current_loin_receiver_id = session('ito_id');
            $current_loin_user_id = session('ito_user_id');
            $current_user_type_id = 5;
        }

        $this->messages_model->offset = $this->uri->segment("3");
        $this->messages_model->limit = 20;

        $data["items"] = $this->messages_model->getByReceiverTypeAndReceiverIdAndUserIdForDisplay($current_user_type_id, $current_loin_receiver_id, false, false, false, 1);
        $data["total_rows"] = $this->messages_model->getByReceiverTypeAndReceiverIdAndUserIdForDisplay($current_user_type_id, $current_loin_receiver_id, false, true, false, 1);

        $config['uri_segment'] = 3;
        $config['base_url'] = site_url('messages/viewAll');
        $config['total_rows'] = $data["total_rows"];
        $config['per_page'] = $this->messages_model->limit;
        $config['suffix'] = $this->input->get() ? '?' . http_build_query($_GET) : NULL;
        $this->load->library('pagination');
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();
        $this->load->view('messages/view_all', $data);
    }

    function popup_new($object_name = '', $object_id = 0) {
        $this->layout = 'js';
        $this->lang->load('hotels');

        // MAIN DROPDOWNS
        //$data['company_types'] = ddgen('erp_company_types', array('erp_company_type_id', name()), false, false, true);
        //$data['countries'] = ddgen('erp_countries', array('erp_country_id', name()));
        $data['importances'] = ddgen('erp_importances', array('erp_importances_id', name()));

        $data['success_message'] = '';

        $data['message_subject'] = '';
        $data['message_body'] = '';
        if ($object_name == 'hotel') {
            if ($object_id == 0) {
                $data['message_subject_text'] = lang('message_subject_text_for_add_hotel');
                $data['message_body_text'] = lang('message_body_text_for_add_hotel');
            } else {
                $this->hotels_model->join = true;
                $this->hotels_model->erp_hotel_id = $object_id;
                $hotel_row = $this->hotels_model->get();
                $data['message_subject_text'] = lang('message_subject_text_for_edit_hotel') . $hotel_row->{name()};
                $data['message_body_text'] = lang('hotel_data') . ':' . $hotel_row->{name()} . "\n" .
                        lang('hotels_address_ar') . ': ' . $hotel_row->country_name . ', ' . $hotel_row->city_name . ', ' . $hotel_row->address_ar . "\n" .
                        lang('hotels_address_la') . ': ' . $hotel_row->address_la . "\n" .
                        lang('floors_count') . ': ' . $hotel_row->floors_number . "\n" .
                        lang("ROOM_COUNT") . ': ' . $hotel_row->rooms_number . "\n" .
                        '----------------------------------------------------------------------------------------------------' . "\n" .
                        lang('message_body_text_for_edit_hotel');
            }
        }

        if (isset($_POST['smt_send'])) {
            //$this->form_validation->set_rules('erp_company_type_id', lang('company_type'), 'trim|required');
            $this->form_validation->set_rules('message_subject', lang('message_subject'), 'trim|required');
            $this->form_validation->set_rules('message_body', lang('message_body'), 'trim|required');

            if ($this->form_validation->run() == true) {
                //$erp_company_type_id = $this->input->post('erp_company_type_id');
                $erp_company_type_id = 1;

                //$countries = $this->input->post('countries');

                $companies = $this->input->post('companies');

                $message_subject = $this->input->post('message_subject');
                $message_body = $this->input->post('message_body');
                $erp_importance_id = $this->input->post('erp_importance_id');

                $attachement = $this->input->post('attachement');

                /**
                 * Get current session of current user login
                 */
                $current_user_type_id = 0;
                $current_loin_sender_id = 0;
                $current_loin_user_id = 0;

                if (session('admin_user_id')) {
                    $current_loin_sender_id = session('admin_user_id');
                    $current_loin_user_id = session('admin_user_id');
                    $current_user_type_id = 1;
                } else if (session('uo_id')) {
                    $current_loin_sender_id = session('uo_id');
                    $current_loin_user_id = session('uo_user_id');
                    $current_user_type_id = 2;
                } else if (session('ea_id')) {
                    $current_loin_sender_id = session('ea_id');
                    $current_loin_user_id = session('ea_user_id');
                    $current_user_type_id = 3;
                } else if (session('hm_id')) {
                    $current_loin_sender_id = session('hm_id');
                    $current_loin_user_id = session('hm_user_id');
                    $current_user_type_id = 4;
                } else if (session('ito_id')) {
                    $current_loin_sender_id = session('ito_id');
                    $current_loin_user_id = session('ito_user_id');
                    $current_user_type_id = 5;
                }
                //--------------------------------------------------------

                $message_datetime = date('Y-m-d H:i', time());


                $this->messages_model->erp_importance_id = $erp_importance_id;
                $this->messages_model->sender_type_id = $current_user_type_id;
                $this->messages_model->sender_id = $current_loin_sender_id;
                $this->messages_model->sender_user_id = $current_loin_user_id;
                $this->messages_model->message_subject = $message_subject;
                $this->messages_model->message_body = $message_body;
                $this->messages_model->message_datetime = $message_datetime;

                $erp_messages_id = $this->messages_model->save();

                $erp_messages_id_folder = './static/uploads/messages_attachments/' . $erp_messages_id;
                if (!file_exists($erp_messages_id_folder)) {
                    mkdir($erp_messages_id_folder, 0777, true);
                }

                //---------------------------------------- Attachments --------------------------------------------
                mb_internal_encoding("UTF-8");
                $newFilePath = '';
                if (isset($_FILES['attachement'])) {
                    if (count($_FILES['attachement']['tmp_name']) > 0) {
                        for ($i = 0; $i < count($_FILES['attachement']['tmp_name']); $i++) {
                            //Get the temp file path
                            $tmpFilePath = $_FILES['attachement']['tmp_name'][$i];

                            //Make sure we have a filepath
                            if ($tmpFilePath != "") {
                                //Setup our new file path
                                $newFilePath = './static/uploads/messages_attachments/' . $erp_messages_id . '/' . $_FILES['attachement']['name'][$i];

                                //Upload the file into the temp dir
                                if (move_uploaded_file($tmpFilePath, $newFilePath)) {

                                    //Handle other code here

                                    $this->messages_model->attachements_erp_messages_id = $erp_messages_id;
                                    $this->messages_model->attachements_path = $newFilePath;
                                    $this->messages_model->saveAttachements();
                                }
                            }
                        }
                    }
                }
                //-------------------------------------------------------------------------------------------------
                //---------------------------------------- Detail -------------------------------------------------
                $receiver_type = $erp_company_type_id;
                $receiver_ids_arr = array();

                $receiver_type_table = 'erp_admins';
                if ($receiver_type == 1) {
                    $receiver_type_table = 'erp_admins';
                    $receiver_type_id_field = 'erp_admin_id';
                } else if ($receiver_type == 2) {
                    $receiver_type_table = 'safa_uos';
                    $receiver_type_id_field = 'safa_uo_id';
                } else if ($receiver_type == 3) {
                    $receiver_type_table = 'safa_eas';
                    $receiver_type_id_field = 'safa_ea_id';
                } else if ($receiver_type == 4) {
                    $receiver_type_table = 'safa_hms';
                    $receiver_type_id_field = 'safa_hm_id';
                } else if ($receiver_type == 5) {
                    $receiver_type_table = 'safa_itos';
                    $receiver_type_id_field = 'safa_ito_id';
                }

                //if(count($companies)==0) {
                //    if(count($countries)==0) {
                $this->messages_model->detail_erp_messages_id = $erp_messages_id;
                $this->messages_model->detail_receiver_type = $receiver_type;


                $receiver_query = "select * from $receiver_type_table ";
                /*
                  if($receiver_type==2 || $receiver_type==3 || $receiver_type==4) {
                  $receiver_query=$receiver_query." where `disabled`=0";
                  }
                 */
                $receiver_rows_query = $this->messages_model->db->query($receiver_query);
                $receiver_rows = $receiver_rows_query->result();

                foreach ($receiver_rows as $receiver_row) {
                    $this->messages_model->detail_receiver_id = $receiver_row->$receiver_type_id_field;
                    $this->messages_model->saveDetails();
                }

                //   } 
                //} 
                //-------------------------------------------------------------------------------------------------------
                //By Gouda.
                $this->load->library('email');
                $this->email->from(config('webmaster_email'), config('title'));
                $this->email->to('safa.support@virgotel.com');
                if ($newFilePath != '') {
                    $this->email->attach($newFilePath);
                }
                $this->email->subject($message_subject);
                $this->email->message($message_body);
                $this->email->send();


                $data['success_message'] = lang('success_message');
            }

            $this->load->view('redirect_message_popup', array('msg' => lang('success_message'),
                'model_title' => lang('title'), 'action' => lang('')));
        } else {
            $this->load->view('messages/popup_new', $data);
        }
    }

}
