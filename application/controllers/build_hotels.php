<?php

class Build_hotels extends Safa_Controller {

    public $module = "hotels";

    public function __construct() {
        parent::__construct();
        $this->layout = 'new';
        //******build your hotel model***********************//
        $this->load->model('erp_companies_hotels_model');
        $this->load->model('erp_companies_hotels_officials_model');
        $this->load->model('erp_companies_hotels_policies_model');
        $this->load->model('erp_companies_hotels_rooms_model');
        $this->load->model('erp_hotel_floor_model');
        /*         * **************************************** */
        $this->load->helper('db_helper');
        $this->lang->load('build_hotel');
        $this->lang->load('hotels');
        permission();
    }

    /* build your hotels */

    public function add($hotel_id = false) {
        if ($this->destination != 'hm' && $this->destination != 'ea' && $this->destination != 'uo')
            show_404();
        if (!$hotel_id)
            show_404();
        $data['hotel_name'] = item('erp_hotels', name(), array('erp_hotel_id' => $hotel_id));
        $data['hotel_city'] = item('erp_cities', name(), array('erp_city_id' => item('erp_hotels', 'erp_city_id', array('erp_hotel_id' => $hotel_id))));
        /* companyname */
        if ($this->destination == 'admin') {
            $data['company_name'] = item('erp_company_types', name(), array('erp_company_type_id' => '1'));
            $data['company_type'] = item('erp_company_types', name(), array('erp_company_type_id' => '1'));
            $data['company_type_id'] = '1';
            $company_id = -1;
        }
        if ($this->destination == 'ea') {
            $company_id = session('ea_id');
            $data['company_name'] = item('safa_eas', name(), array('safa_ea_id' => $company_id));
            $data['company_type'] = item('erp_company_types', name(), array('erp_company_type_id' => '3'));
            $data['company_type_id'] = '3';
        }
        if ($this->destination == 'uo') {
            $company_id = session('uo_id');
            $data['company_name'] = item('safa_uos', name(), array('safa_uo_id' => $company_id));
            $data['company_type'] = item('erp_company_types', name(), array('erp_company_type_id' => '2'));
            $data['company_type_id'] = '2';
        }
        $this->load->library("form_validation");
        /* hotels rooms */
        if ($this->input->post('rooms_floor') && count($this->input->post('rooms_floor')) > 0) {
            foreach ($this->input->post('rooms_floor') as $index => $item) {
                $this->form_validation->set_rules('rooms_floor[' . $index . ']', 'lang:rooms_floor', 'trim|integer');
                $this->form_validation->set_rules('rooms_section[' . $index . ']', 'lang:rooms_section', 'trim');
                $this->form_validation->set_rules('rooms_count[' . $index . ']', 'lang:rooms_count', 'trim|integer');
                $this->form_validation->set_rules('beds_count[' . $index . ']', 'lang:beds_count', 'trim|integer');
                $this->form_validation->set_rules('date_from[' . $index . ']', 'lang:date_from', 'trim');
                $this->form_validation->set_rules('date_to[' . $index . ']', 'lang:date_to', 'trim|callback_compare_date[' . $index . ']');
            }
        }
        /* hotels officials */
        if ($this->input->post('official_name') && count($this->input->post('official_name') > 0)) {
            foreach ($this->input->post('official_name') as $index => $item) {
                $this->form_validation->set_rules('official_name[' . $index . ']', 'lang:official_name', 'trim');
                $this->form_validation->set_rules('official_email[' . $index . ']', 'lang:official_email', 'trim|valid_email');
                $this->form_validation->set_rules('official_work[' . $index . ']', 'lang:official_work', 'trim');
                $this->form_validation->set_rules('official_phone[' . $index . ']', 'lang:official_phone', 'trim'); /* callback todo */
                $this->form_validation->set_rules('official_phonetype[' . $index . ']', 'lang:official_phonetype', 'trim');
                $this->form_validation->set_rules('official_skype[' . $index . ']', 'lang:official_skype', 'trim');
                $this->form_validation->set_rules('official_messenger[' . $index . ']', 'lang:official_massenger', 'trim');
            }
        }
        /* hotels policies */
        if ($this->input->post('hotels_policy') && count($this->input->post('hotels_policy')) > 0) {
            foreach ($this->input->post('hotels_policy') as $index => $value) {
                $this->form_validation->set_rules('hotels_policy[' . $index . ']', 'lang:hotel_policy', 'trim');
                $this->form_validation->set_rules('policy_fieldtype[' . $index . ']', 'lang:hotel_policy', 'trim');
                $this->form_validation->set_rules('policy_description[' . $index . ']', 'lang:hotel_policy', 'trim');
            }
        }
        if ($this->form_validation->run() == false) {
            $this->load->view('build_hotels/add', $data);
        } else {
            /* inserting the data into database */
            $this->erp_companies_hotels_model->erp_company_type_id = $data['company_type_id'];
            $this->erp_companies_hotels_model->company_id = $company_id;
            $this->erp_companies_hotels_model->erp_hotel_id = $hotel_id;
            $hotel_company_id = $this->erp_companies_hotels_model->save();

            if (isset($hotel_company_id)) {
                /* inserting the rooms details */
                if (($this->input->post('rooms_floor') && count($this->input->post('rooms_floor')) > 0)) {

                    $this->erp_companies_hotels_rooms_model->erp_companies_hotels_id = $hotel_company_id;
                    foreach ($this->input->post('rooms_floor') as $index => $value) {
                        $record = 0;
                        if ($_POST['rooms_section'][$index] != '') {
                            $this->erp_companies_hotels_rooms_model->section = $_POST['rooms_section'][$index];
                            $record = 1;
                        }
                        if ($_POST['rooms_count'][$index] != '') {
                            $this->erp_companies_hotels_rooms_model->rooms_count = $_POST['rooms_count'][$index];
                            $record = 1;
                        }
                        if ($_POST['date_from'][$index] != '') {
                            $this->erp_companies_hotels_rooms_model->available_from = $_POST['date_from'][$index];
                            $record = 1;
                        }

                        if ($_POST['date_to'][$index] != '') {
                            $this->erp_companies_hotels_rooms_model->available_to = $_POST['date_to'][$index];
                            $record = 1;
                        }
                        if ($_POST['beds_count'][$index] != '') {
                            $this->erp_companies_hotels_rooms_model->beds_count = $_POST['beds_count'][$index];
                            $record = 1;
                        }
                        if ($_POST['rooms_floor'][$index] != '') {
                            $this->erp_companies_hotels_rooms_model->erp_hotel_floor_id = $_POST['rooms_floor'][$index];
                            $record = 1;
                        }
                        if ($record == 1)
                            $this->erp_companies_hotels_rooms_model->save();
                    }
                }
            }
            /* inserting the contact offecial */
            if (isset($hotel_company_id)) {
                if ($this->input->post('official_name') && count($this->input->post('official_name')) > 0) {
                    $this->erp_companies_hotels_officials_model->erp_companies_hotels_id = $hotel_company_id;
                    foreach ($this->input->post('official_name') as $index => $value) {
                        $record = 0;
                        if ($_POST['official_work'][$index] != '') {
                            $this->erp_companies_hotels_officials_model->position = $_POST['official_work'][$index];
                            $record = 1;
                        }
                        if ($_POST['official_name'][$index] != '') {
                            $this->erp_companies_hotels_officials_model->name = $_POST['official_name'][$index];
                            $record = 1;
                        }
                        if ($_POST['official_email'][$index] != '') {
                            $this->erp_companies_hotels_officials_model->email = $_POST['official_email'][$index];
                            $record = 1;
                        }
                        if ($_POST['official_phone'][$index] != '') {
                            $this->erp_companies_hotels_officials_model->phone = $_POST['official_phone'][$index];
                            $record = 1;
                        }
                        if ($_POST['official_skype'][$index] != '') {
                            $this->erp_companies_hotels_officials_model->skype = $_POST['official_skype'][$index];
                            $record = 1;
                        }
                        if ($_POST['official_messenger'][$index] != '') {
                            $this->erp_companies_hotels_officials_model->messenger = $_POST['official_messenger'][$index];
                            $record = 1;
                        }
                        if ($_POST['official_phonetype'][$index] != '') {
                            $this->erp_companies_hotels_officials_model->phone_type_id = $_POST['official_phonetype'][$index];
                            $record = 1;
                        }
                        if ($record == 1)
                            $this->erp_companies_hotels_officials_model->save();
                    }
                }
            }
            /* inserting  hotel_company_policy */
            if (isset($hotel_company_id)) {
                if ($this->input->post('hotels_policy') && count($this->input->post('hotels_policy')) > 0) {
                    $this->erp_companies_hotels_policies_model->erp_companies_hotels_id = $hotel_company_id;
                    foreach ($this->input->post('hotels_policy') as $index => $value) {
                        $record = 0;
                        if ($_POST['hotels_policy'][$index] != '') {
                            $this->erp_companies_hotels_policies_model->erp_hotels_policies_id = $_POST['hotels_policy'][$index];
                            $record = 1;
                        }
                        if ($_POST['policy_description'][$index] != '') {
                            $this->erp_companies_hotels_policies_model->policy_description = $_POST['policy_description'][$index];
                            $record = 1;
                        }
                        if ($_POST['policy_fieldtype'][$index] != '') {
                            $this->erp_companies_hotels_policies_model->policy_field_type = $_POST['policy_fieldtype'][$index];
                            $record = 1;
                        }
                        if ($record == 1)
                            $this->erp_companies_hotels_policies_model->save();
                    }
                }
            }
            $detail_polcy = $this->erp_companies_hotels_policies_model->count_details();
            $detail_official = $this->erp_companies_hotels_officials_model->count_details();
            $detail_rooms = $this->erp_companies_hotels_rooms_model->count_details();
            if ($detail_polcy == 0 && $detail_official == 0 && $detail_rooms == 0)
                $this->erp_companies_hotels_model->delete();
            redirect('hotels/index');
        }
    }

    function edit($company_hotel_id = false) {
        if ($this->destination != 'hm' && $this->destination != 'ea' && $this->destination != 'uo')
            show_404();
        if (!$company_hotel_id)
            show_404();
        $this->erp_companies_hotels_model->erp_companies_hotels_id = $company_hotel_id;
        $company_hotel = $this->erp_companies_hotels_model->get();
        $hotel_id = $company_hotel->erp_hotel_id;
        if (is_ea())
            $data['company_name'] = item('safa_eas', name(), array('safa_ea_id' => $company_hotel->company_id));
        if (is_uo())
            $data['company_name'] = item('safa_uos', name(), array('safa_uo_id' => $company_hotel->company_id));

        $data['company_type_id'] = $company_hotel->erp_company_type_id;
        $data['company_type'] = item('erp_company_types', name(), array('erp_company_type_id' => $company_hotel->erp_company_type_id));
        $data['hotel_name'] = item('erp_hotels', name(), array('erp_hotel_id' => $hotel_id));
        $data['hotel_city'] = item('erp_cities', name(), array('erp_city_id' => item('erp_hotels', 'erp_city_id', array('erp_hotel_id' => $hotel_id))));
        /* assign the object */
        $this->erp_companies_hotels_officials_model->erp_companies_hotels_id = $company_hotel_id;
        $this->erp_companies_hotels_rooms_model->erp_companies_hotels_id = $company_hotel_id;
        $this->erp_companies_hotels_policies_model->erp_companies_hotels_id = $company_hotel_id;


        /* validation */
        $this->load->library("form_validation");
        /* hotel_rooms_info_validation */
        if ($this->input->post('rooms_floor') && count($this->input->post('rooms_floor')) > 0) {
            foreach ($this->input->post('rooms_floor') as $index => $item) {
                $this->form_validation->set_rules('rooms_floor[' . $index . ']', 'lang:rooms_floor', 'trim|integer');
                $this->form_validation->set_rules('rooms_section[' . $index . ']', 'lang:rooms_section', 'trim');
                $this->form_validation->set_rules('rooms_count[' . $index . ']', 'lang:rooms_count', 'trim|integer');
                $this->form_validation->set_rules('beds_count[' . $index . ']', 'lang:beds_count', 'trim|integer');
                $this->form_validation->set_rules('date_from[' . $index . ']', 'lang:date_from', 'trim');
                $this->form_validation->set_rules('date_to[' . $index . ']', 'lang:date_to', 'trim|callback_compare_date[' . $index . ']');
            }
        }
        /* hotels_policies_validation */
        if ($this->input->post('hotels_policy') && count($this->input->post('hotels_policy')) > 0) {
            foreach ($this->input->post('hotels_policy') as $index => $value) {
                $this->form_validation->set_rules('hotels_policy[' . $index . ']', 'lang:hotel_policy', 'trim');
                $this->form_validation->set_rules('policy_fieldtype[' . $index . ']', 'lang:hotel_policy', 'trim');
                $this->form_validation->set_rules('policy_description[' . $index . ']', 'lang:hotel_policy', 'trim');
            }
        }
        /* hotels officials */
        if ($this->input->post('official_name') && count($this->input->post('official_name') > 0)) {
            foreach ($this->input->post('official_name') as $index => $item) {
                $this->form_validation->set_rules('official_name[' . $index . ']', 'lang:official_name', 'trim');
                $this->form_validation->set_rules('official_email[' . $index . ']', 'lang:official_email', 'trim|valid_email');
                $this->form_validation->set_rules('official_work[' . $index . ']', 'lang:official_work', 'trim');
                $this->form_validation->set_rules('official_phone[' . $index . ']', 'lang:official_phone', 'trim'); /* callback todo */
                $this->form_validation->set_rules('official_phonetype[' . $index . ']', 'lang:official_phonetype', 'trim');
                $this->form_validation->set_rules('official_skype[' . $index . ']', 'lang:official_skype', 'trim');
                $this->form_validation->set_rules('official_messenger[' . $index . ']', 'lang:official_massenger', 'trim');
            }
        }
        if ($this->form_validation->run() == false) {
            $data['contact_official'] = $this->erp_companies_hotels_officials_model->get();
            $data['rooms_info'] = $this->erp_companies_hotels_rooms_model->get();
            $data['policies_info'] = $this->erp_companies_hotels_policies_model->get();
            $this->load->view('build_hotels/edit', $data);
        } else {
            if (($this->input->post('rooms_floor') && count($this->input->post('rooms_floor')) > 0)) {

                $this->erp_companies_hotels_rooms_model->delete();
                foreach ($this->input->post('rooms_floor') as $index => $value) {
                    $record = 0;
                    if ($_POST['rooms_section'][$index] != '') {
                        $this->erp_companies_hotels_rooms_model->section = $_POST['rooms_section'][$index];
                        $record = 1;
                    }
                    if ($_POST['rooms_count'][$index] != '') {
                        $this->erp_companies_hotels_rooms_model->rooms_count = $_POST['rooms_count'][$index];
                        $record = 1;
                    }
                    if ($_POST['date_from'][$index] != '') {
                        $this->erp_companies_hotels_rooms_model->available_from = $_POST['date_from'][$index];
                        $record = 1;
                    }

                    if ($_POST['date_to'][$index] != '') {
                        $this->erp_companies_hotels_rooms_model->available_to = $_POST['date_to'][$index];
                        $record = 1;
                    }
                    if ($_POST['beds_count'][$index] != '') {
                        $this->erp_companies_hotels_rooms_model->beds_count = $_POST['beds_count'][$index];
                        $record = 1;
                    }
                    if ($_POST['rooms_floor'][$index] != '') {
                        $this->erp_companies_hotels_rooms_model->erp_hotel_floor_id = $_POST['rooms_floor'][$index];
                        $record = 1;
                    }
                    if ($record == 1)
                        $this->erp_companies_hotels_rooms_model->save();
                }
            }




            /*             * * inserting contact officials ** */
            if ($this->input->post('official_name') && count($this->input->post('official_name')) > 0) {

                $this->erp_companies_hotels_officials_model->delete();
                foreach ($this->input->post('official_name') as $index => $value) {
                    $record = 0;
                    if ($_POST['official_work'][$index] != '') {
                        $this->erp_companies_hotels_officials_model->position = $_POST['official_work'][$index];
                        $record = 1;
                    }
                    if ($_POST['official_name'][$index] != '') {
                        $this->erp_companies_hotels_officials_model->name = $_POST['official_name'][$index];
                        $record = 1;
                    }
                    if ($_POST['official_email'][$index] != '') {
                        $this->erp_companies_hotels_officials_model->email = $_POST['official_email'][$index];
                        $record = 1;
                    }
                    if ($_POST['official_phone'][$index] != '') {
                        $this->erp_companies_hotels_officials_model->phone = $_POST['official_phone'][$index];
                        $record = 1;
                    }
                    if ($_POST['official_skype'][$index] != '') {
                        $this->erp_companies_hotels_officials_model->skype = $_POST['official_skype'][$index];
                        $record = 1;
                    }
                    if ($_POST['official_messenger'][$index] != '') {
                        $this->erp_companies_hotels_officials_model->messenger = $_POST['official_messenger'][$index];
                        $record = 1;
                    }
                    if ($_POST['official_phonetype'][$index] != '') {
                        $this->erp_companies_hotels_officials_model->phone_type_id = $_POST['official_phonetype'][$index];
                        $record = 1;
                    }
                    if ($record == 1)
                        $this->erp_companies_hotels_officials_model->save();
                }
            }

            /*             * **inserting**** */
            if ($this->input->post('hotels_policy') && count($this->input->post('hotels_policy')) > 0) {

                $this->erp_companies_hotels_policies_model->delete();
                foreach ($this->input->post('hotels_policy') as $index => $value) {
                    $record = 0;
                    if ($_POST['hotels_policy'][$index] != '') {
                        $this->erp_companies_hotels_policies_model->erp_hotels_policies_id = $_POST['hotels_policy'][$index];
                        $record = 1;
                    }
                    if ($_POST['policy_description'][$index] != '') {
                        $this->erp_companies_hotels_policies_model->policy_description = $_POST['policy_description'][$index];
                        $record = 1;
                    }
                    if ($_POST['policy_fieldtype'][$index] != '') {
                        $this->erp_companies_hotels_policies_model->policy_field_type = $_POST['policy_fieldtype'][$index];
                        $record = 1;
                    }
                    if ($record == 1)
                        $this->erp_companies_hotels_policies_model->save();
                }
                $detail_polcy = $this->erp_companies_hotels_policies_model->count_details();
                $detail_official = $this->erp_companies_hotels_officials_model->count_details();
                $detail_rooms = $this->erp_companies_hotels_rooms_model->count_details();
                if ($detail_polcy == 0 && $detail_official == 0 && $detail_rooms == 0)
                    $this->erp_companies_hotels_model->delete();
            }
            redirect('hotels/index');
        }
    }

    function compare_date($value = false, $index = 0) {
        $this->form_validation->set_message('compare_date', lang('compare_dates'));
        if ($_POST['date_to'][$index] != '' && $_POST['date_from'][$index] != '') {
            $date_to = date_create($_POST['date_to'][$index]);
            $date_from = date_create($_POST['date_from'][$index]);
        } else {
            return true;
        }
        if (isset($date_to) && isset($date_from)) {
            if ($date_to <= $date_from)
                return false;
            else
                return true;
        }
    }

}

// end class
