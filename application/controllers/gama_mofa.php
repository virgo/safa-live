<?php

class Gama_mofa extends CI_Controller {
    public function __construct() {
        parent::__construct();
        
    }
    public function index() {
        $this->load->helper('simple_html');
        $this->load->library('form_validation');
        $this->load->library('upload', array(
            'upload_path' => './static/uploads/',
            'allowed_types' => 'html|html|txt'
        ));
        if ( ! $this->upload->do_upload('file'))
        {
            $this->form_validation->set_message('uploadfile', $this->upload->display_errors());
            $this->form_validation->set_rules('w', '', 'required|callback_uploadfile');
        }
        else
        {
            $this->form_validation->set_rules('w', '', 'trim');
        }
        if ($this->form_validation->run() === FALSE) {
            $this->load->view('gama_mofa');
        }
        else
        {
            $file = $this->upload->data();
            $html = file_get_html(base_url().'/static/uploads/'.$file['file_name']);
            $tables = $html->find('table');
            $group = $tables[1]->find('tr');
            $group = $group[1]->find('td');
            // GROUP UASP_ID
            $group_uasp_id = $this->parseNumbers($group[1]);
            $group = $this->db->where('uasp_id', $group_uasp_id)->get('safa_umrahgroups')->row();
            if( ! $group)
                show_404 ();
            $passports = $tables[2]->find('tr');
            unset($passports[0]);
            // PASSPORTS MOFAS
            foreach($passports as $passport) {
                $passport = $passport->find('td');
                $this->db->where('safa_umrahgroup_id', $group->safa_umrahgroup_id)
                         ->where('passport_no', $this->parseNumbers($passport[6]))
                         ->update('safa_umrahgroups_passports', array(
                             'mofa' => $this->parseNumbers($passport[1]),
                         ));
            }
        }
    }
    function uploadfile($file) {
        return FALSE;
    }
    protected function parseNumbers($code) {
        preg_match('/([0-9]+)/', $code, $number);
        return trim($number['1']);
    }
}