<?php

class Machines extends Safa_Controller {

    var $table = 'crm_ea_machines';
    var $table_pk = 'crm_ea_machine_id';

    public function __construct() {
        parent::__construct();
        $this->layout = 'new';
        $this->lang->load('machines');
        permission();
    }

    public function index($ea_id = false) {
        if ($this->system_id == 3)
            $ea_id = $this->ea_id;

        if (!$ea_id)
            show_404();

        $data['destination'] = $this->destination;
        $this->db->where('safa_ea_id', $ea_id);
        $data['items'] = $this->db->select($this->table . '.*')->get($this->table)->result();
        $this->load->view('machines/index', $data);
    }

    public function manage($ea_id = false, $id = false) {
        if ($this->system_id == 1)
            if (!$ea_id)
                show_404();

        if ($this->system_id == 3){
            $id = $ea_id;
            $ea_id = $this->ea_id;
        }

        $ea = $this->db->where('safa_ea_id', $ea_id)->get('safa_eas')->row();
        if (!$ea)
            show_404();

        $data['ea_id'] = $ea_id;
        $data['update_code'] = null;
        if (isset($_POST['code']))
            $_POST['code'] = str_replace(' ', '', $this->input->post('code'));

        if ($id) {
            $data['item'] = $this->db->where($this->table_pk, $id)->get($this->table)->row();
            if ($this->system_id == 3)
                if ($data['item']->safa_ea_id != session('ea_id'))
                    show_404();
            $data['update_code'] = 'readonly="readonly"';
        }
        else {
            if (!$this->system_id == 3)
                show_404();

            $data['item'] = new stdClass();
            $data['item']->code = null;
            $data['item']->name = null;
        }
        $data['destination'] = $this->destination;

        $this->load->library('form_validation');

        if (!$id)
            $this->form_validation->set_rules('code', 'lang:machines_code', 'required|callback_check_code');
        else
            $this->form_validation->set_rules('code', 'lang:machines_code', 'required');

        $this->form_validation->set_rules('name', 'lang:machines_name', 'required');
        if ($this->form_validation->run() == false) {
            $this->load->view('machines/manage', $data);
        } else {
            $this->db->set('safa_ea_id', $ea_id);
            $this->db->set('name', $this->input->post('name'));
            if ($id)
                $this->db->where($this->table_pk, $id)
                        ->where('code', $_POST['code']);
            else
                $this->db->where('code', $_POST['code'])
                        ->where('safa_ea_id');
            $this->db->update($this->table);
            if ($this->destination == 'admin')
                redirect('machines/index/' . $ea_id);
            else
                redirect('machines');
        }
    }

    public function delete($id = false) {
        if (!$id)
            show_404();
        $data['item'] = $this->db->where($this->table_pk, $id)->get($this->table)->row();
        $this->db->where($this->table_pk, $id)->update($this->table, array(
            'safa_ea_id' => NULL,
            'name' => NULL,
        ));
        if ($this->system_id == 1)
            redirect('machines/index/' . $data['item']->safa_ea_id);
        else
            redirect('machines');
    }

    // CALLBACK
    public function check_code($value) {
        if ($this->db->where('code', $value)->where('safa_ea_id')->get($this->table)->num_rows())
            return true;
        else {
            $this->form_validation->set_message('check_code', lang('machines_check_code'));
            return false;
        }
    }

}
