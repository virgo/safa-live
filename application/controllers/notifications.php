<?php

class notifications extends Safa_Controller {

    public $module = "ea_no";

    public function __construct() {

        parent::__construct();

        //$this->layout='new';
        $this->load->model('notification_model');
        $this->load->model('erp_languages_model');
        $this->load->model('erp_notification_user_seen_model');

        $this->lang->load('admins/notifications');
        $this->layout = 'new';
        permission();
    }

    public function index() {
        /*
          $data['notifications'] = $this->notification_model->getByReceiverTypeAndReceiverId('3', session('ea_id'));
          $this->load->view('notifications/notifications_view', $data);
         */
    }

    public function get($user_type, $receiver_id, $user_id) {
        $this->layout = 'ajax';

        //-------------- Get Datetime From Server ------------------------------
        //$serverd_datetime_rows=$this->notification_model->getServerDatetime();
        //$now=$serverd_datetime_rows[0]->current_datetime;
        //$now = strtotime($now);
        $now = date('Y-m-d H:i', time());
        $now = strtotime($now);
        //----------------------------------------------------------------------


        $notifications = $this->notification_model->getByReceiverTypeAndReceiverIdAndUserId($user_type, $receiver_id, $user_id);

        $unreaded_notifications = $this->notification_model->getUnreadedByReceiverTypeAndReceiverIdAndUserId($user_type, $receiver_id, $user_id, true);
        $unreaded_notifications_count = $unreaded_notifications[0]->unreaded_notifications_count;
        if ($unreaded_notifications_count == 0) {
            $unreaded_notifications_count = '';
        }

        $notifications_arr = array();

        foreach ($notifications as $notification) {
            $mark_as_read = $notification->mark_as_read;
            $mark_as_important = $notification->mark_as_important;

            $erp_notification_detail_id = $notification->erp_notification_detail_id;

            $erp_system_events_id = $notification->erp_system_events_id;
            $language_id = $notification->language_id;
            $msg_datetime = $notification->msg_datetime;

            $tags = $notification->tags;
            $tags_arr = explode('*****', $tags);

            $notification_data = $this->getNotificationDataFromXml($language_id, $erp_system_events_id);
            $notification_data_arr = explode('*****', $notification_data);

            $notification_subject = '';
            $notification_body = '';

            if (isset($notification_data_arr[0])) {
                $notification_subject = $notification_data_arr[0];
                foreach ($tags_arr as $tag) {
                    $tag_arr = explode('=', $tag);
                    if (isset($tag_arr[0]) && isset($tag_arr[1])) {
                        $notification_subject = str_replace($tag_arr[0], $tag_arr[1], $notification_subject);
                    }
                }

                if (isset($notification_data_arr[1])) {
                    $notification_body = $notification_data_arr[1];
                    foreach ($tags_arr as $tag) {
                        $tag_arr = explode('#*=', $tag);
                        if (isset($tag_arr[0]) && isset($tag_arr[1])) {
                            $notification_body = str_replace($tag_arr[0] . '#*', '<b>' . $tag_arr[1] . '</b>', $notification_body);
                        }
                    }
                }
            }

            if ($mark_as_important == 1) {
                $cls_notification_message_dv = 'cls_notification_message_important_dv';
                $mark_as_important_value = 0;
                $mark_as_important_link_text = lang('mark_as_un_important');
            } else {
                if ($mark_as_read == 1) {
                    $cls_notification_message_dv = 'cls_notification_message_readed_dv';
                } else {
                    $cls_notification_message_dv = 'cls_notification_message_dv';
                }

                $mark_as_important_value = 1;
                $mark_as_important_link_text = lang('mark_as_important');
            }

            $notification_subject_brief = substr($notification_subject, 0, 90);
            $notification_body_brief = strstr($notification_body, '</a>', true).'</a> ....';
            //$notification_body_brief = $notification_body;
   
            
            // By Gouda, to reduce traffic on website.
            /*
              echo("
              <li alt='$cls_notification_message_dv'>
              <span id='$erp_notification_detail_id' class='icon-trash notification_del' title='" . lang('delete_message') . "' style='float:left; cursor: pointer; margin: 3px 3px 3px 3px;'  ></span>
              <span id='$erp_notification_detail_id' alt='$mark_as_important_value' class='icon-star notification_star' title='$mark_as_important_link_text' style='float:left; cursor: pointer; margin: 3px 3px 3px 3px;'  ></span>
              <a class='fancybox fancybox.iframe' href='" . site_url("notifications/read/$erp_notification_detail_id") . "'>
              <b>$notification_subject</b>
              <br/>
              $notification_body_brief
              </a></li>");
             */


            $msg_datetime = $notification->msg_datetime;
            $msg_datetime = strtotime($msg_datetime);
            $diff_with_second = $now - $msg_datetime;

            $notifications_arr[] = array('unreaded_notifications_count' => $unreaded_notifications_count,
                'cls_notification_message_dv' => $cls_notification_message_dv,
                'erp_notification_detail_id' => $erp_notification_detail_id,
                'mark_as_important_value' => $mark_as_important_value,
                'mark_as_important_link_text' => $mark_as_important_link_text,
                'notification_subject' => $notification_subject,
                'notification_body_brief' => $notification_body_brief,
                'diff_with_second' => $diff_with_second,
            );
        }

        echo json_encode($notifications_arr);

        /*
          foreach ($notifications as $notification) {
          echo("alertify.success('$notification->sender_type_id', 5000);");
          }
         */

        //$now = strtotime(date('Y-m-d H:i', time()))+30;
        //$now = strtotime(date('Y-m-d H:i', time()));
        // Commented By Gouda, Untill discuss with Mohamed Attia.
        /*
          $serverd_datetime_rows = $this->notification_model->getServerDatetime();
          $now = $serverd_datetime_rows[0]->current_datetime;
          $now = strtotime($now);

          foreach ($notifications as $notification) {
          $msg_datetime = $notification->msg_datetime;
          $msg_datetime = strtotime($msg_datetime);
          $diff_with_second = $now - $msg_datetime;
          if ($diff_with_second == 1) {

          $erp_notification_detail_id = $notification->erp_notification_detail_id;

          $erp_system_events_id = $notification->erp_system_events_id;
          $language_id = $notification->language_id;
          $msg_datetime = $notification->msg_datetime;

          $tags = $notification->tags;
          $tags_arr = explode('*****', $tags);

          $notification_data = $this->getNotificationDataFromXml($language_id, $erp_system_events_id);
          $notification_data_arr = explode('*****', $notification_data);

          $notification_subject = '';
          $notification_body = '';

          if (isset($notification_data_arr[0])) {
          $notification_subject = $notification_data_arr[0];
          foreach ($tags_arr as $tag) {
          $tag_arr = explode('=', $tag);
          if (isset($tag_arr[0]) && isset($tag_arr[1])) {
          $notification_subject = str_replace($tag_arr[0], $tag_arr[1], $notification_subject);
          }
          }

          if (isset($notification_data_arr[1])) {
          $notification_body = $notification_data_arr[1];
          foreach ($tags_arr as $tag) {
          $tag_arr = explode('=', $tag);
          if (isset($tag_arr[0]) && isset($tag_arr[1])) {
          $notification_body = str_replace($tag_arr[0], $tag_arr[1], $notification_body);
          }
          }
          }
          }


          $notification_subject_brief = substr($notification_subject, 0, 100);
          $notification_body_brief = substr($notification_body, 0, 180) . '...';

          }
          }
         */
    }

    function getNotificationDataFromXml($language_id, $event_key) {
        $this->erp_languages_model->erp_language_id = $language_id;
        $erp_languages_row = $this->erp_languages_model->get();

        $lang_suffix = $erp_languages_row->suffix;

        if (file_exists("./static/xml/notifications/notifications_$lang_suffix.xml")) {
            $xml = new DOMDocument('1.0', 'utf-8');
            /**
             * Added to format the xml code, when open with editor.
             */
            $xml->formatOutput = true;
            $xml->preserveWhiteSpace = false;

            $xml->Load("./static/xml/notifications/notifications_$lang_suffix.xml");
            /**
             * 
             * Check if this event inserted before
             */
            $event_key_exist = false;
            $notifications = $xml->getElementsByTagName('notification');
            foreach ($notifications as $notification) {
                $loop_current_key = $notification->getElementsByTagName('key');
                $loop_current_key_value = $loop_current_key->item(0)->nodeValue;

                if ($loop_current_key_value == $event_key) {
                    $event_key_exist = true;

                    $loop_current_subject = $notification->getElementsByTagName('subject');
                    $loop_current_subject_value = $loop_current_subject->item(0)->nodeValue;

                    $loop_current_body = $notification->getElementsByTagName('body');
                    $loop_current_body_value = $loop_current_body->item(0)->nodeValue;

                    /*
                      if(name()=='name_ar') {
                      $loop_current_name= $notification->getElementsByTagName('name_ar');
                      $loop_current_name_value= $loop_current_name->item(0)->nodeValue;
                      } else if(name()=='name_la') {
                      $loop_current_name= $notification->getElementsByTagName('name_la');
                      $loop_current_name_value= $loop_current_name->item(0)->nodeValue;
                      }
                     */

                    return $loop_current_subject_value . '*****' . $loop_current_body_value;
                }
            }

            if (!$event_key_exist) {
                return '';
            }

            //------------------------------------------------------------------
        } else {
            return '';
        }
    }

    function read($erp_notification_detail_id = 0) {
        $this->layout = 'ajax';

        $current_loin_user_id = 0;
        if (session('admin_user_id')) {
            $current_loin_user_id = session('admin_user_id');
        } else if (session('uo_user_id')) {
            $current_loin_user_id = session('uo_user_id');
        } else if (session('ea_user_id')) {
            $current_loin_user_id = session('ea_user_id');
        } else if (session('ito_user_id')) {
            $current_loin_user_id = session('ito_user_id');
        } else if (session('hm_user_id')) {
            $current_loin_user_id = session('hm_user_id');
        }



        $this->erp_notification_user_seen_model->erp_notification_detail_id = $erp_notification_detail_id;
        $this->erp_notification_user_seen_model->user_id = $current_loin_user_id;


        $erp_notification_user_seen_rows_count = $this->erp_notification_user_seen_model->get(true);
        $erp_notification_user_seen_rows = $this->erp_notification_user_seen_model->get();
        //if(count($erp_notification_user_seen_rows)>0) {
        $mark_as_important = 0;

        $this->erp_notification_user_seen_model->mark_as_read = 1;
        //$this->erp_notification_user_seen_model->is_hide = 0;
        $this->erp_notification_user_seen_model->read_datetime = date('Y-m-d H:i', time());

        if ($erp_notification_user_seen_rows_count == 0) {
            $this->erp_notification_user_seen_model->save();
        } else {
            $this->erp_notification_user_seen_model->erp_notification_user_seen_id = $erp_notification_user_seen_rows[0]->erp_notification_user_seen_id;
            $this->erp_notification_user_seen_model->save();

            $mark_as_important = $erp_notification_user_seen_rows[0]->mark_as_important;
        }


        $notification_row = $this->notification_model->getByDetailId($erp_notification_detail_id);
        $language_id = $notification_row->language_id;
        $erp_system_events_id = $notification_row->erp_system_events_id;

        $msg_datetime = $notification_row->msg_datetime;
        $tags = $notification_row->tags;
        $tags_arr = explode('*****', $tags);

        $notification_data = $this->getNotificationDataFromXml($language_id, $erp_system_events_id);
        $notification_data_arr = explode('*****', $notification_data);

        $notification_subject = '';
        $notification_body = '';

        if (isset($notification_data_arr[0])) {
            $notification_subject = $notification_data_arr[0];
            foreach ($tags_arr as $tag) {
                $tag_arr = explode('=', $tag);
                if (isset($tag_arr[0]) && isset($tag_arr[1])) {
                    $notification_subject = str_replace($tag_arr[0], $tag_arr[1], $notification_subject);
                }
            }

            if (isset($notification_data_arr[1])) {
                $notification_body = $notification_data_arr[1];
                foreach ($tags_arr as $tag) {
                    $tag_arr = explode('#*=', $tag);
                    if (isset($tag_arr[0]) && isset($tag_arr[1])) {
                        $notification_body = str_replace($tag_arr[0] . '#*', '<b>' . $tag_arr[1] . '</b>', $notification_body);
                    }
                }
            }
        }

        $data['mark_as_important'] = $mark_as_important;
        $data['erp_notification_detail_id'] = $erp_notification_detail_id;
        $data['notification_from'] = lang('system');
        $data['notification_subject'] = $notification_subject;
        $data['notification_body'] = $notification_body;
        $data['msg_datetime'] = $msg_datetime;

        $this->load->view('notifications/message', $data);
        //}
    }

    function hide($erp_notification_detail_id = 0, $mark_as_important_value = 1) {
        $this->layout = 'ajax';

        $current_loin_user_id = 0;
        if (session('admin_user_id')) {
            $current_loin_user_id = session('admin_user_id');
        } else if (session('uo_user_id')) {
            $current_loin_user_id = session('uo_user_id');
        } else if (session('ea_user_id')) {
            $current_loin_user_id = session('ea_user_id');
        } else if (session('ito_user_id')) {
            $current_loin_user_id = session('ito_user_id');
        } else if (session('hm_user_id')) {
            $current_loin_user_id = session('hm_user_id');
        }

        /*
          echo "
          <script>
          alert('$current_loin_user_id, $erp_notification_detail_id');
          </script>";
         */

        $this->erp_notification_user_seen_model->erp_notification_detail_id = $erp_notification_detail_id;
        $this->erp_notification_user_seen_model->user_id = $current_loin_user_id;

        $erp_notification_user_seen_rows_count = $this->erp_notification_user_seen_model->get(true);
        $erp_notification_user_seen_rows = $this->erp_notification_user_seen_model->get();

        //$this->erp_notification_user_seen_model->mark_as_read = 0;
        $this->erp_notification_user_seen_model->is_hide = $mark_as_important_value;



        if ($erp_notification_user_seen_rows_count == 0) {
            $this->erp_notification_user_seen_model->save();
        } else {
            $this->erp_notification_user_seen_model->erp_notification_user_seen_id = $erp_notification_user_seen_rows[0]->erp_notification_user_seen_id;
            $this->erp_notification_user_seen_model->save();
        }
    }

    function markAsImportant($erp_notification_detail_id = 0, $mark_as_important_value = 0) {
        $this->layout = 'ajax';

        $current_loin_user_id = 0;
        if (session('admin_user_id')) {
            $current_loin_user_id = session('admin_user_id');
        } else if (session('uo_user_id')) {
            $current_loin_user_id = session('uo_user_id');
        } else if (session('ea_user_id')) {
            $current_loin_user_id = session('ea_user_id');
        } else if (session('ito_user_id')) {
            $current_loin_user_id = session('ito_user_id');
        } else if (session('hm_user_id')) {
            $current_loin_user_id = session('hm_user_id');
        }


        $this->erp_notification_user_seen_model->erp_notification_detail_id = $erp_notification_detail_id;
        $this->erp_notification_user_seen_model->user_id = $current_loin_user_id;

        $erp_notification_user_seen_rows_count = $this->erp_notification_user_seen_model->get(true);
        $erp_notification_user_seen_rows = $this->erp_notification_user_seen_model->get();

        //$this->erp_notification_user_seen_model->mark_as_read = 0;
        $this->erp_notification_user_seen_model->mark_as_important = $mark_as_important_value;



        if ($erp_notification_user_seen_rows_count == 0) {
            $this->erp_notification_user_seen_model->save();
        } else {
            $this->erp_notification_user_seen_model->erp_notification_user_seen_id = $erp_notification_user_seen_rows[0]->erp_notification_user_seen_id;
            $this->erp_notification_user_seen_model->save();
        }
    }

    public function viewAll() {

        $current_user_type_id = 0;
        $current_loin_receiver_id = 0;
        $current_loin_user_id = 0;
        if (session('admin_user_id')) {
            $current_loin_receiver_id = session('admin_user_id');
            $current_loin_user_id = session('admin_user_id');
            $current_user_type_id = 1;
        } else if (session('uo_id')) {
            $current_loin_receiver_id = session('uo_id');
            $current_loin_user_id = session('uo_user_id');
            $current_user_type_id = 2;
        } else if (session('ea_id')) {
            $current_loin_receiver_id = session('ea_id');
            $current_loin_user_id = session('ea_user_id');
            $current_user_type_id = 3;
        } else if (session('ito_id')) {
            $current_loin_receiver_id = session('ito_id');
            $current_loin_user_id = session('ito_user_id');
            $current_user_type_id = 4;
        } else if (session('hm_id')) {
            $current_loin_receiver_id = session('hm_id');
            $current_loin_user_id = session('hm_user_id');
            $current_user_type_id = 5;
        }

        $this->notification_model->offset = $this->uri->segment("3");
        $this->notification_model->limit = 20;

        $data["items"] = $this->notification_model->getByReceiverTypeAndReceiverIdAndUserIdForDisplay($current_user_type_id, $current_loin_receiver_id, false);

        $data["total_rows"] = $this->notification_model->getByReceiverTypeAndReceiverIdAndUserIdForDisplay($current_user_type_id, $current_loin_receiver_id, false, true);

        $config['uri_segment'] = 3;
        $config['base_url'] = site_url('notifications/viewAll');
        $config['total_rows'] = $data["total_rows"];
        $config['per_page'] = $this->notification_model->limit;
        $config['suffix'] = $this->input->get() ? '?' . http_build_query($_GET) : NULL;
        $this->load->library('pagination');
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();
        $this->load->view('notifications/index', $data);
    }

    public function viewAllImportant() {

        $current_user_type_id = 0;
        $current_loin_receiver_id = 0;
        $current_loin_user_id = 0;
        if (session('admin_user_id')) {
            $current_loin_receiver_id = session('admin_user_id');
            $current_loin_user_id = session('admin_user_id');
            $current_user_type_id = 1;
        } else if (session('uo_id')) {
            $current_loin_receiver_id = session('uo_id');
            $current_loin_user_id = session('uo_user_id');
            $current_user_type_id = 2;
        } else if (session('ea_id')) {
            $current_loin_receiver_id = session('ea_id');
            $current_loin_user_id = session('ea_user_id');
            $current_user_type_id = 3;
        } else if (session('ito_id')) {
            $current_loin_receiver_id = session('ito_id');
            $current_loin_user_id = session('ito_user_id');
            $current_user_type_id = 4;
        } else if (session('hm_id')) {
            $current_loin_receiver_id = session('hm_id');
            $current_loin_user_id = session('hm_user_id');
            $current_user_type_id = 5;
        }

        $this->notification_model->offset = $this->uri->segment("3");
        $this->notification_model->limit = 20;

        $data["items"] = $this->notification_model->getByReceiverTypeAndReceiverIdAndUserIdForDisplay($current_user_type_id, $current_loin_receiver_id, false, false, 1);

        $data["total_rows"] = $this->notification_model->getByReceiverTypeAndReceiverIdAndUserIdForDisplay($current_user_type_id, $current_loin_receiver_id, false, true, 1);

        $config['uri_segment'] = 3;
        $config['base_url'] = site_url('notifications/viewAll');
        $config['total_rows'] = $data["total_rows"];
        $config['per_page'] = $this->notification_model->limit;
        $config['suffix'] = $this->input->get() ? '?' . http_build_query($_GET) : NULL;
        $this->load->library('pagination');
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();
        $this->load->view('notifications/index', $data);
    }

    public function viewAllDeleted() {

        $current_user_type_id = 0;
        $current_loin_receiver_id = 0;
        $current_loin_user_id = 0;
        if (session('admin_user_id')) {
            $current_loin_receiver_id = session('admin_user_id');
            $current_loin_user_id = session('admin_user_id');
            $current_user_type_id = 1;
        } else if (session('uo_id')) {
            $current_loin_receiver_id = session('uo_id');
            $current_loin_user_id = session('uo_user_id');
            $current_user_type_id = 2;
        } else if (session('ea_id')) {
            $current_loin_receiver_id = session('ea_id');
            $current_loin_user_id = session('ea_user_id');
            $current_user_type_id = 3;
        } else if (session('ito_id')) {
            $current_loin_receiver_id = session('ito_id');
            $current_loin_user_id = session('ito_user_id');
            $current_user_type_id = 4;
        } else if (session('hm_id')) {
            $current_loin_receiver_id = session('hm_id');
            $current_loin_user_id = session('hm_user_id');
            $current_user_type_id = 5;
        }

        $this->notification_model->offset = $this->uri->segment("3");
        $this->notification_model->limit = 20;

        $data["items"] = $this->notification_model->getByReceiverTypeAndReceiverIdAndUserIdForDisplay($current_user_type_id, $current_loin_receiver_id, false, false, false, 1);
        $data["total_rows"] = $this->notification_model->getByReceiverTypeAndReceiverIdAndUserIdForDisplay($current_user_type_id, $current_loin_receiver_id, false, true, false, 1);

        $config['uri_segment'] = 3;
        $config['base_url'] = site_url('notifications/viewAll');
        $config['total_rows'] = $data["total_rows"];
        $config['per_page'] = $this->notification_model->limit;
        $config['suffix'] = $this->input->get() ? '?' . http_build_query($_GET) : NULL;
        $this->load->library('pagination');
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();
        $this->load->view('notifications/index', $data);
    }

}
