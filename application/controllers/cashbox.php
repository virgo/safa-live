<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Cashbox extends Safa_Controller {

    public $module = "cashbox";
    public $layout = 'new';
    public $table_pk = 'crm_cashbox_id';

    public function __construct() {
        parent::__construct();
        $this->load->model('cashbox_model');
        $this->lang->load('cashbox');
        
        permission();
    }

    public function index() {
        $data['table_pk'] = $this->table_pk;
        $data['module'] = $this->module;
        if (isset($_GET['search']))
            $this->search();
        $data["total_rows"] = $this->cashbox_model->search(true);
        $this->cashbox_model->offset = $this->uri->segment("3");
        $this->cashbox_model->limit = config('per_page');
        $data["items"] = $this->cashbox_model->search();
        $config['uri_segment'] = 3;
        $config['base_url'] = site_url('cashbox/index');
        $config['total_rows'] = $data["total_rows"];
        $config['per_page'] = $this->cashbox_model->limit;
        $config['suffix'] = $this->input->get() ? '?' . http_build_query($_GET) : NULL;
        $this->load->library('pagination');
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();
        $this->load->view('cashbox/index', $data);
    }

    public function manage($id = false) {
        $data['module'] = $this->module;
        if ($id) {
            $this->cashbox_model->crm_cashbox_id = $id;
            $data['item'] = $this->cashbox_model->get();
            if (!$data['item'])
                show_404();
        }
        else {
            $data['item'] = new stdClass();
            $data['item']->name = NULL;
            $data['item']->erp_currency_id = NULL;
        }
        $this->load->library("form_validation");
        $this->form_validation->set_rules('name', lang($data['module'] . '_name'), 'required');
        $this->form_validation->set_rules('erp_currency_id', lang($data['module'] . '_erp_currency_id'), 'required');
        if ($this->form_validation->run() == false) {
            $this->load->view("cashbox/manage", $data);
        } else {
            if ($id)
                $this->cashbox_model->crm_cashbox_id = $id;
            $this->cashbox_model->erp_currency_id = $this->input->post('erp_currency_id');
            $this->cashbox_model->name = $this->input->post('name');
            $this->cashbox_model->save();
            redirect("cashbox/index");
        }
    }

    function delete($id = false) {
        if (!$id)
            show_404();

        $this->cashbox_model->crm_cashbox_id = $id;

        if (!$this->cashbox_model->delete())
            show_404();

        redirect("cashbox/index");
    }

    function search() {
        if ($this->input->get("crm_cashbox_id"))
            $this->cashbox_model->crm_cashbox_id = $this->input->get("crm_cashbox_id");
        if ($this->input->get("erp_currency_id"))
            $this->cashbox_model->erp_currency_id = $this->input->get("erp_currency_id");
        if ($this->input->get("name"))
            $this->cashbox_model->name = $this->input->get("name");
    }

}

/* End of file cashbox.php */
/* Location: ./application/controllers/cashbox.php */