<?php

class Usergroups extends Safa_Controller {
    public $tables = array();
    public $keys = array();
    public function __construct() {
        parent::__construct();
        permission();
        if($this->destination == 'ea')
        {
            $this->tables = array('safa_ea_usergroups', 'safa_eaprivileges', 'safa_ea_usergroups_eaprivileges', 'safa_ea_users');
            $this->keys = array('safa_ea_id', 'safa_eaprivilege_id', 'safa_ea_usergroup_id', 'safa_ea_user_id');
        }
        elseif($this->destination == 'uo')
        {
        	//Side menu session, By Gouda.
        	session('side_menu_id', '0');
        	
            $this->tables = array('safa_uo_usergroups', 'safa_uoprivileges', 'safa_uo_usergroups_uoprivileges', 'safa_uo_users');
            $this->keys = array('safa_uo_id', 'safa_uoprivilege_id', 'safa_uo_usergroup_id', 'safa_uo_user_id');
        }
        elseif($this->destination == 'hm')
        {
        	//Side menu session, By Gouda.
        	session('side_menu_id', '0');
        	
            $this->tables = array('safa_hm_usergroups', 'safa_hmprivileges', 'safa_hm_usergroups_uoprivileges', 'safa_hm_users');
            $this->keys = array('safa_hm_id', 'safa_hmprivilege_id', 'safa_hm_usergroup_id', 'safa_hm_user_id');
        }
        elseif($this->destination == 'admin')
        {
            $this->tables = array('erp_admin_usergroups', 'erp_adminprivileges', 'erp_admin_usergroups_adminprivileges', 'safa_admin_users');
            $this->keys = array('erp_admin_id', 'erp_adminprivilege_id', 'erp_admin_usergroup_id', 'safa_admin_user_id');
        }
        elseif($this->destination == 'ito')
        {
            $this->tables = array('safa_ito_usergroups', 'safa_itoprivileges', 'safa_ito_usergroups_itoprivileges', 'safa_ito_users');
            $this->keys = array('safa_ito_id', 'safa_itoprivilege_id', 'safa_ito_usergroup_id', 'safa_ito_user_id');
        }
        elseif($this->destination == 'gov')
        {
            $this->tables = array('safa_gov_usergroups', 'safa_govprivileges', 'safa_gov_usergroups_govprivileges', 'safa_gov_users');
            $this->keys = array('safa_gov_id', 'safa_govprivilege_id', 'safa_gov_usergroup_id', 'safa_gov_user_id');
        }
    }
    public function index() {
        $data['items'] = $this->db->where($this->keys['0'], session($this->destination.'_id'))->get($this->tables['0'])->result();
        $data['destination'] = $this->destination;
        $data['tables'] = $this->tables;
        $data['keys'] = $this->keys;
        $this->load->view('usergroups/index', $data);
    }
    public function manage($id = FALSE) {
        $data['privileges'] = $this->db->where('visible', 1)->get($this->tables[1])->result();
        $data['destination'] = $this->destination;
        $data['tables'] = $this->tables;
        $data['keys'] = $this->keys;
        $data['permissions'] = array();
        if($id) 
        {
        	//---------------- By Gouda ----------------------
            //$data['item'] = $this->db->where($this->keys['0'], session(str_replace(array('safa_', 'erp_'), '', $this->keys['0'])))->get($this->tables['0'])->row();
            $data['item'] = $this->db->where($this->keys[2], $id)->get($this->tables['0'])->row();
            
            if( ! $data['item'])
                show_404 ();
            foreach($this->db->where($this->keys['2'], $id)->get($this->tables['2'])->result() as $per)
                $data['permissions'][] = $per->{$this->keys['1']};
        }
        else
        {
            $data['item'] = new stdClass();
            $data['item']->name_ar = NULL;
            $data['item']->name_la = NULL;
        }
        $this->load->library('form_validation');
        $this->form_validation->set_rules('name_ar', 'lang:global_name_ar', 'trim|required');
        $this->form_validation->set_rules('name_la', 'lang:global_name_la', 'trim|required');
        if($this->form_validation->run() == FALSE) {
            $this->load->view('usergroups/manage', $data);
        }
        else 
        {
            $name = $this->input->post('name');
            $permissions = $this->input->post('permissions');
            if($id) {
                $this->db->where($this->keys[2], $id)->update($this->tables['0'], array(
                    'name_ar' => $this->input->post('name_ar'),
                    'name_la' => $this->input->post('name_la'),
                ));
                $usergroup_id = $id;
                $this->db->where($this->keys[2], $id)->delete($this->tables[2]);
            }
            else
            {
                $this->db->insert($this->tables[0], array(
                    'name_ar' => $this->input->post('name_ar'),
                    'name_la' => $this->input->post('name_la'),
                    $this->keys[0] => session($this->destination.'_id'),
                ));
                $usergroup_id = $this->db->insert_id();
            }
            foreach($permissions as $permission => $val)
                $this->db->insert($this->tables[2], array(
                    $this->keys[1] => $permission,
                    $this->keys[2] => $usergroup_id
                ));
            redirect('usergroups');
        }
    }
    public function delete($id = FALSE) {
        if($this->db->where($this->keys[2], $id)->get($this->tables[3])->row()) {
            //redirect('usergroups');
        
            session('error_message', lang('you_cannot_delete_this_row_becouse_relations_with_other'));
            session('error_url', site_url('usergroups'));
            session('error_title', lang('global_usergroups'));
            session('error_action', lang('global_delete'));
			redirect("error_message/show");
         
        }
        
        $this->db->where($this->keys[2], $id)
                 ->delete($this->tables[2]);
        
        $this->db->where($this->keys[2], $id)
                 ->where($this->keys[0], session($this->destination.'_id'))
                 ->delete($this->tables[0]);
        redirect('usergroups');
    }
}