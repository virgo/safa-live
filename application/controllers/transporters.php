<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Transporters extends Safa_Controller {

    public $module = "transporters";
    public $temp_upload_path;
    public $uploaded_file_name;

    public function __construct() {
        parent::__construct();
        //Side menu session, By Gouda.
        session('side_menu_id', 0);
        $this->layout = 'new';
        $this->load->model('transporters_model');
        $this->load->model('transporters_drivers_model');
        $this->load->model('transporters_buses_model');
        $this->lang->load('admins/transporters');
        permission();
        
        
//        if (session('ito_id'))
//            $this->transporters_model->ito_id = session('ito_id');
//        else
//            show_404();
    }

    public function index() {

        if (isset($_GET['search'])) {
            $this->search();
        }
        $this->transporters_model->erp_transportertype_id = 1;
        $data["total_rows"] = $this->transporters_model->search(true);
        $this->transporters_model->offset = $this->uri->segment("3");
        //$this->transporters_model->limit = $this->config->item('per_page');
        $this->transporters_model->join = TRUE;
        $data["items"] = $this->transporters_model->search();
        $config['uri_segment'] = 3;
        $config['base_url'] = site_url('transporters/index');
        $config['total_rows'] = $data["total_rows"];
        $config['per_page'] = $this->transporters_model->limit;
        $config['suffix'] = $this->input->get() ? '?' . http_build_query($_GET) : NULL;
        $this->load->library('pagination');
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();
        $this->load->view('transporters/index', $data);
    }

    public function drivers($safa_transporters_id = false) {
        $data['safa_transporters_id'] = $safa_transporters_id;

        $this->transporters_drivers_model->safa_transporters_id = $safa_transporters_id;
        $data["total_rows"] = $this->transporters_drivers_model->get(true);
        $this->transporters_drivers_model->offset = $this->uri->segment("3");
        //$this->transporters_drivers_model->limit = $this->config->item('per_page');
        $this->transporters_drivers_model->join = TRUE;
        $items = $this->transporters_drivers_model->get();
        $data["items"] = $items;

        $config['uri_segment'] = 3;
        $config['base_url'] = site_url('transporters/drivers/' . $safa_transporters_id);
        $config['total_rows'] = $data["total_rows"];
        $config['per_page'] = $this->transporters_drivers_model->limit;
        $config['suffix'] = $this->input->get() ? '?' . http_build_query($_GET) : NULL;
        $this->load->library('pagination');
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();
        $this->load->view('transporters/drivers', $data);
    }

    public function add_driver($safa_transporters_id = false) {
        $data['safa_transporters_id'] = $safa_transporters_id;

        $this->load->library("form_validation");
        $this->form_validation->set_rules('name_ar', 'lang:safa_transporter_name_ar', 'trim|required');
        $this->form_validation->set_rules('name_la', 'lang:safa_transporter_name_la', 'trim|required');
        $this->form_validation->set_rules('phone', 'lang:phone', 'trim|required|max_length[15]|unique_col[safa_transporters_drivers.phone]');

        if ($this->form_validation->run() == false) {
            $this->load->view("transporters/add_driver", $data);
        } else {
            $this->transporters_drivers_model->safa_transporters_id = $safa_transporters_id;
            $this->transporters_drivers_model->name_ar = $this->input->post('name_ar');
            $this->transporters_drivers_model->name_la = $this->input->post('name_la');
            $this->transporters_drivers_model->phone = $this->input->post('phone');
            $this->transporters_drivers_model->save();
            $this->load->view('redirect_message', array('msg' => lang('global_added_message'), 'url' => site_url('transporters/drivers/' . $safa_transporters_id), 'model_name' => 'drivers', 'model_title' => lang('drivers'), 'action' => lang('add_driver')));
        }
    }

    public function edit_driver($safa_transporters_id, $id) {

        if (!$id)
            show_404();

        $this->transporters_drivers_model->safa_transporters_drivers_id = $id;
        $data['item'] = $this->transporters_drivers_model->get();

        if (!$data['item'])
            show_404();

        $data['safa_transporters_id'] = $safa_transporters_id;

        $this->load->library("form_validation");
        $this->form_validation->set_rules('name_ar', 'lang:safa_transporter_name_ar', 'trim|required');
        $this->form_validation->set_rules('name_la', 'lang:safa_transporter_name_la', 'trim|required');
        $this->form_validation->set_rules('phone', 'lang:phone', 'trim|required|max_length[15]');

        if ($this->form_validation->run() == false) {
            $this->load->view("transporters/edit_driver", $data);
        } else {
            $this->transporters_drivers_model->safa_transporters_drivers_id = $id;
            $this->transporters_drivers_model->name_ar = $this->input->post('name_ar');
            $this->transporters_drivers_model->name_la = $this->input->post('name_la');
            $this->transporters_drivers_model->phone = $this->input->post('phone');

            $this->transporters_drivers_model->save();

            $this->load->view('redirect_message', array('msg' => lang('global_updated_message'),
                'url' => site_url('transporters/drivers/' . $safa_transporters_id),
                'model_name' => 'drivers',
                'model_title' => lang('drivers'), 'action' => lang('edit_driver')));
        }
    }

    function delete_driver($safa_transporters_id, $id) {
        if (!$id) {
            show_404();
        }
        $this->transporters_drivers_model->safa_transporters_drivers_id = $id;
        if (!$this->transporters_drivers_model->delete()) {
            show_404();
        }

        $this->load->view('redirect_message', array('msg' => lang('global_deleted_message'),
            'url' => site_url('transporters/drivers/' . $safa_transporters_id),
            'model_name' => 'drivers',
            'model_title' => lang('drivers'), 'action' => lang('delete_driver')));
    }

    public function buses($safa_transporters_id = false) {
        $data['safa_transporters_id'] = $safa_transporters_id;

        $this->transporters_buses_model->safa_transporters_id = $safa_transporters_id;
        $data["total_rows"] = $this->transporters_buses_model->get(true);
        $this->transporters_buses_model->offset = $this->uri->segment("3");
        //$this->transporters_buses_model->limit = $this->config->item('per_page');
        $this->transporters_buses_model->join = TRUE;
        $items = $this->transporters_buses_model->get();
        $data["items"] = $items;

        $config['uri_segment'] = 3;
        $config['base_url'] = site_url('transporters/buses/' . $safa_transporters_id);
        $config['total_rows'] = $data["total_rows"];
        $config['per_page'] = $this->transporters_buses_model->limit;
        $config['suffix'] = $this->input->get() ? '?' . http_build_query($_GET) : NULL;
        $this->load->library('pagination');
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();
        $this->load->view('transporters/buses', $data);
    }

    public function add_bus($safa_transporters_id = false) {
        $data['safa_transporters_id'] = $safa_transporters_id;

        $data['safa_buses_brands'] = ddgen('safa_buses_brands', array('safa_buses_brands_id', name()), FALSE, FALSE, FALSE);
        //$data['safa_buses_models'] = ddgen('safa_buses_models', array('safa_buses_models_id', name()), FALSE, FALSE, TRUE);
		$data['safa_buses_models'] = array();
        
        $this->load->library("form_validation");
        $this->form_validation->set_rules('bus_no', 'lang:bus_no', 'trim|required|unique_col[safa_transporters_buses.bus_no]');
        $this->form_validation->set_rules('safa_buses_brands_id', 'lang:safa_buses_brands_id', 'trim|required');
        $this->form_validation->set_rules('safa_buses_models_id', 'lang:safa_buses_models_id', 'trim|required');
        $this->form_validation->set_rules('passengers_count', 'lang:passengers_count', 'trim|required');

        if ($this->form_validation->run() == false) {
            $this->load->view("transporters/add_bus", $data);
        } else {
            $this->transporters_buses_model->safa_transporters_id = $safa_transporters_id;

            $this->transporters_buses_model->bus_no = $this->input->post('bus_no');
            $this->transporters_buses_model->safa_buses_brands_id = $this->input->post('safa_buses_brands_id');
            $this->transporters_buses_model->safa_buses_models_id = $this->input->post('safa_buses_models_id');
            $this->transporters_buses_model->industry_year = $this->input->post('industry_year');
            $this->transporters_buses_model->passengers_count = $this->input->post('passengers_count');
            $this->transporters_buses_model->notes = $this->input->post('notes');

            $this->transporters_buses_model->save();
            $this->load->view('redirect_message', array('msg' => lang('global_added_message'), 'url' => site_url('transporters/buses/' . $safa_transporters_id), 'model_name' => 'buses', 'model_title' => lang('buses'), 'action' => lang('add_bus')));
        }
    }

    public function edit_bus($safa_transporters_id, $id) {

        if (!$id)
            show_404();

        $this->transporters_buses_model->safa_transporters_buses_id = $id;
        $data['item'] = $this->transporters_buses_model->get();

        if (!$data['item'])
            show_404();

        $data['safa_transporters_id'] = $safa_transporters_id;

        $data['safa_buses_brands'] = ddgen('safa_buses_brands', array('safa_buses_brands_id', name()), FALSE, FALSE, TRUE);
        $data['safa_buses_models'] = ddgen('safa_buses_models', array('safa_buses_models_id', name()), FALSE, FALSE, TRUE);
		if(isset($data['item']->safa_buses_brands_id)) {
			$data['safa_buses_models'] = ddgen('safa_buses_models', array('safa_buses_models_id', name()),  array('safa_buses_brands_id' => $data['item']->safa_buses_brands_id), FALSE, TRUE);
		}
        
        
        $this->load->library("form_validation");
        $this->form_validation->set_rules('bus_no', 'lang:bus_no', 'trim|required');
        $this->form_validation->set_rules('safa_buses_brands_id', 'lang:safa_buses_brands_id', 'trim|required');
        $this->form_validation->set_rules('safa_buses_models_id', 'lang:safa_buses_models_id', 'trim|required');
        $this->form_validation->set_rules('passengers_count', 'lang:passengers_count', 'trim|required');

        if ($this->form_validation->run() == false) {
            $this->load->view("transporters/edit_bus", $data);
        } else {
            $this->transporters_buses_model->safa_transporters_buses_id = $id;

            $this->transporters_buses_model->bus_no = $this->input->post('bus_no');
            $this->transporters_buses_model->safa_buses_brands_id = $this->input->post('safa_buses_brands_id');
            $this->transporters_buses_model->safa_buses_models_id = $this->input->post('safa_buses_models_id');
            $this->transporters_buses_model->industry_year = $this->input->post('industry_year');
            $this->transporters_buses_model->passengers_count = $this->input->post('passengers_count');
            $this->transporters_buses_model->notes = $this->input->post('notes');

            $this->transporters_buses_model->save();

            $this->load->view('redirect_message', array('msg' => lang('global_updated_message'),
                'url' => site_url('transporters/buses/' . $safa_transporters_id),
                'model_name' => 'buses',
                'model_title' => lang('buses'), 'action' => lang('edit_bus')));
        }
    }

    function delete_bus($safa_transporters_id, $id) {
        if (!$id) {
            show_404();
        }
        $this->transporters_buses_model->safa_transporters_buses_id = $id;
        if (!$this->transporters_buses_model->delete()) {
            show_404();
        }

        $this->load->view('redirect_message', array('msg' => lang('global_deleted_message'),
            'url' => site_url('transporters/buses/' . $safa_transporters_id),
            'model_name' => 'buses',
            'model_title' => lang('buses'), 'action' => lang('delete_bus')));
    }

     public function getBusesDriversByTransporterIdAjax() 
	 {
	 	$safa_transporters_id = $this->input->post('safa_transporter_id');
	 	
        $structure = array('safa_transporters_drivers_id', name());
        $key = $structure['0'];
        if (isset($structure['1'])) {
            $value = $structure['1'];
        }
        //$drivers_arr = array();
        $drivers_dd_options='';
        $this->transporters_drivers_model->safa_transporters_id = $safa_transporters_id;
        $drivers = $this->transporters_drivers_model->get();
        //$drivers_arr[""] = lang('global_select_from_menu');
        foreach ($drivers as $driver) {
            //$drivers_arr[$driver->$key] = $driver->$value;
            $drivers_dd_options=$drivers_dd_options."<option value='".$driver->$key."'>".$driver->$value."</option>";
        }


        $structure = array('safa_transporters_buses_id', 'bus_no');
        $key = $structure['0'];
        if (isset($structure['1'])) {
            $value = $structure['1'];
        }
        //$buses_arr = array();
        $buses_dd_options='';
        $this->transporters_buses_model->safa_transporters_id = $safa_transporters_id;
        $buses = $this->transporters_buses_model->get();
        //$buses_arr[""] = lang('global_select_from_menu');
        foreach ($buses as $bus) {
            //$buses_arr[$bus->$key] = $bus->$value;
            $buses_dd_options=$buses_dd_options."<option value='".$bus->$key."'>".$bus->$value."</option>";
        }

        $data_arr[] = array('drivers' => $drivers_dd_options,
            'buses' => $buses_dd_options,
        );
        echo json_encode($data_arr);
        exit;
    }
     
    function add_driver_popup($safa_transporters_id)
	{
		$this->layout = 'js_new';
			
		$data =array();
		$data['safa_transporters_id'] = $safa_transporters_id;
		
		if (!isset($_POST['smt_save'])) {
			$this->load->view('transporters/add_driver_popup', $data);
		} else {

			$this->load->library("form_validation");
	        $this->form_validation->set_rules('name_ar', 'lang:safa_transporter_name_ar', 'trim|required');
	        $this->form_validation->set_rules('name_la', 'lang:safa_transporter_name_la', 'trim|required');
	        $this->form_validation->set_rules('phone', 'lang:phone', 'trim|required|max_length[15]|unique_col[safa_transporters_drivers.phone]');
			
			if ($this->form_validation->run() == false) {

				$this->load->view("transporters/add_driver_popup", $data);
			} else {
				
				$this->transporters_drivers_model->safa_transporters_id = $safa_transporters_id;
	            $this->transporters_drivers_model->name_ar = $this->input->post('name_ar');
	            $this->transporters_drivers_model->name_la = $this->input->post('name_la');
	            $this->transporters_drivers_model->phone = $this->input->post('phone');
	            $this->transporters_drivers_model->save();
	            	
	            $this->load->view('message_popup', array('msg' => lang('global_added_message'),
	            'url' => site_url("transporters/add_driver_popup"),
	            'id' => $this->uri->segment("4"),
	            'model_title' => lang('drivers'), 'action' => lang('add_driver')));
			}


		}
	}
	
	function add_bus_popup($safa_transporters_id)
	{
		$this->layout = 'js_new';
			
		$data['safa_buses_brands'] = ddgen('safa_buses_brands', array('safa_buses_brands_id', name()), FALSE, FALSE, TRUE);
        $data['safa_buses_models'] = ddgen('safa_buses_models', array('safa_buses_models_id', name()), FALSE, FALSE, TRUE);

        $data['safa_transporters_id'] = $safa_transporters_id;
		
		if (!isset($_POST['smt_save'])) {
			$this->load->view('transporters/add_bus_popup', $data);
		} else {

			$this->load->library("form_validation");
	        $this->form_validation->set_rules('bus_no', 'lang:bus_no', 'trim|required|unique_col[safa_transporters_buses.bus_no]');
	        $this->form_validation->set_rules('safa_buses_brands_id', 'lang:safa_buses_brands_id', 'trim|required');
	        $this->form_validation->set_rules('safa_buses_models_id', 'lang:safa_buses_models_id', 'trim|required');
	        $this->form_validation->set_rules('passengers_count', 'lang:passengers_count', 'trim|required');
			
			if ($this->form_validation->run() == false) {

				$this->load->view("transporters/add_bus_popup", $data);
			} else {
				
				$this->transporters_buses_model->safa_transporters_id = $safa_transporters_id;

	            $this->transporters_buses_model->bus_no = $this->input->post('bus_no');
	            $this->transporters_buses_model->safa_buses_brands_id = $this->input->post('safa_buses_brands_id');
	            $this->transporters_buses_model->safa_buses_models_id = $this->input->post('safa_buses_models_id');
	            $this->transporters_buses_model->industry_year = $this->input->post('industry_year');
	            $this->transporters_buses_model->passengers_count = $this->input->post('passengers_count');
	            $this->transporters_buses_model->notes = $this->input->post('notes');
	
	            $this->transporters_buses_model->save();
	            	
	            $this->load->view('message_popup', array('msg' => lang('global_added_message'),
	            'url' => site_url("transporters/add_bus_popup"),
	            'id' => $this->uri->segment("4"),
	            'model_title' => lang('buses'), 'action' => lang('add_bus')));
			}


		}
	}
    
}

/* End of file transporters.php */
/* Location: ./application/controllers/transporters.php */