<?php

class Offline_backup extends Safa_Controller {

    public $layout = 'ajax';
    public $file = FALSE;
    public $group_name = FALSE;
    public $upload_path = '../offline_files/';
    public $mutamers = array();

    public function __construct() {
        parent::__construct();
        $this->load->library('Structure');
        $this->load->library('arraytoxml');
    }
    
    public function index(){
        $this->load->library('form_validation');
        $this->form_validation->set_rules('hardware_id', '', 'trim|required');
        $this->form_validation->set_rules("file", '', "trim|callback_file");

        if ($this->form_validation->run() == FALSE) {
            $this->load->view('offline_backup');
        } else {
            $row = array(
                'hardware_id' => $this->input->post('hardware_id'),
                'creation_date' => date('Y-m-d H:i:s'),
                'file_name' => $this->file
            );
            $this->db->insert('offline_backup',$row);
            echo "File Saved";
        }
    }
    
    public function file($var) {
        $config['upload_path'] = $this->upload_path;
        $config['allowed_types'] = '*';
        $config['detect_mime'] = FALSE;
        $this->load->library('upload', $config);
        if (!$this->upload->do_upload('file')) {
            $this->form_validation->set_message('file', $this->upload->display_errors());
            return FALSE;
        } else {
            $data = $this->upload->data();
            if ($data['file_name'])
                $this->file = $data['file_name'];
        }
    }
}