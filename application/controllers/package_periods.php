<?php

class Package_periods extends Safa_Controller {

    function __construct() {
        parent::__construct();
        //Side menu session, By Gouda.
        if (session('uo_id')) {
            session('side_menu_id', 7);
        } else if (session('ea_id')) {
            session('side_menu_id', 6);
        }
        $this->layout = 'new';

        $this->lang->load('package_periods');
        $this->load->model('package_periods_model');

        permission('package_periods');
        if (session('uo_id')) {
            $this->package_periods_model->erp_company_type_id = 2;
            $this->package_periods_model->erp_company_id = session('uo_id');
        } else if (session('ea_id')) {
            $this->package_periods_model->erp_company_type_id = 3;
            $this->package_periods_model->erp_company_id = session('ea_id');
        }
    }

    function index() {
        if (isset($_GET['search'])) {
            $this->search();
        }
        $data["total_rows"] = $this->package_periods_model->get(true);

        $this->package_periods_model->offset = $this->uri->segment("4");
        //$this->package_periods_model->limit = $this->config->item('per_page');
        //$this->package_periods_model->order_by = array('safa_contract_phases_id');
        $data["items"] = $this->package_periods_model->get();
        $config['uri_segment'] = 4;
        $config['base_url'] = site_url('uo/package_periods/index');
        $config['total_rows'] = $data["total_rows"];

        $config['per_page'] = $this->config->item('per_page');
        $config['suffix'] = $this->input->get() ? '?' . http_build_query($_GET) : NULL;
        $this->load->library('pagination');
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();
        $this->load->view('package_periods/index', $data);
    }

    function manage($id = FALSE) {
        $data = array();

        if ($id) {
            $this->package_periods_model->erp_package_period_id = $id;
            $data['item'] = $this->package_periods_model->get();
        }

        $this->load->library("form_validation");
        $this->form_validation->set_rules('name_ar', 'lang:name_ar', 'trim|required');
        $this->form_validation->set_rules('name_la', 'lang:name_la', 'trim|required');
        $this->form_validation->set_rules('makkah_days', 'lang:makkah_days', 'trim|required');
        $this->form_validation->set_rules('madinah_days', 'lang:madinah_days', 'trim|required');

        if ($this->form_validation->run() == false) {
            $this->load->view('package_periods/manage', $data);
        } else {
            if (session('uo_id')) {
                $this->package_periods_model->erp_company_type_id = 2;
                $this->package_periods_model->erp_company_id = session('uo_id');
            } else if (session('ea_id')) {
                $this->package_periods_model->erp_company_type_id = 3;
                $this->package_periods_model->erp_company_id = session('ea_id');
            }
            $this->package_periods_model->name_ar = $this->input->post('name_ar');
            $this->package_periods_model->name_la = $this->input->post('name_la');
            $makkah_days = $this->input->post('makkah_days');
            $madinah_days = $this->input->post('madinah_days');

            if ($id) {
                $this->package_periods_model->erp_package_period_id = $id;
                $this->package_periods_model->save();
                $this->db->where('erp_city_id',1)->where('erp_package_period_id',$id)->set('city_days',$makkah_days)->update('erp_package_periods_cities');
                $this->db->where('erp_city_id',2)->where('erp_package_period_id',$id)->set('city_days',$madinah_days)->update('erp_package_periods_cities');
                $this->load->view('redirect_new_message', array('msg' => lang('global_updated_message'),
                    'url' => site_url('package_periods'),
                    'id' => $this->uri->segment("4"),
                    'model_title' => lang('title'), 'action' => lang('edit_title')));
            } else {
                $package_period_id = $this->package_periods_model->save();
                $this->db->set('erp_city_id',1)->set('erp_package_period_id',$package_period_id)->set('city_days',$makkah_days)->insert('erp_package_periods_cities');
                $this->db->set('erp_city_id',2)->set('erp_package_period_id',$package_period_id)->set('city_days',$madinah_days)->insert('erp_package_periods_cities');
                if (isset($package_period_id)) {
                    $this->load->view('redirect_new_message', array('msg' => lang('global_added_message'),
                        'url' => site_url('package_periods'),
                        'id' => $this->uri->segment("4"),
                        'model_title' => lang('title'), 'action' => lang('add_title')));
                }
            }
        }
    }

    function delete($id = false) {
        if (!$id)
            show_404();

        $this->db->trans_start();
        $this->package_periods_model->erp_package_period_id = $id;
        $this->package_periods_model->delete();
        $this->db->trans_complete();
        $this->load->view('redirect_message', array('msg' => lang('global_deleted_message'),
            'url' => site_url('package_periods'),
            'model_title' => lang('title'), 'action' => lang('delete_title')));
    }

}
