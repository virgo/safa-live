<!-- By Gouda, To Close menu On this screen -->
<script>
jQuery(function() { 
	menu1.toggle();
})
</script> 

<?= form_open() ?>


<div class="span12">
    <div class="widget">
    
     
    <div class="path-container Fright">
        <div class="icon"><i class="icos-pencil2"></i></div> 
         <div class="path-name Fright">
               <a href="<?= site_url('uo/dashboard') ?>"><?= lang('global_system_management') ?></a>
        </div>    
        <div class="path-arrow Fright">
            </div>
            <div class="path-name Fright">
                <span><?= lang('menu_uos_arrival_report') ?></span>
        </div>
        
        
    </div> 
    </div> 
</div>

<div class="row-fluid">              
    <div class="widget">
        
        
<div class="widget-header">
	
	            <div class="widget-header-icon Fright">
	                <span class="icos-pencil2"></span>
	            </div>
	
	            <div class="widget-header-title Fright">
	                <?= lang('search') ?>
	            </div>
	
	        </div>
        
                               
        <div class="block-fluid">
            <div class="row-form">
                <div class="span4">
                    <div class="span4"><?= lang('date') ?>:</div>
                    <div class="span8">
                        <input name="from_date" value="<?= ($this->input->post('from_date'))?$this->input->post('from_date'):date("Y-m-d"); ?>" class="validate[required] datepicker from_date" style=" width:48%; margin:0 1%" placeholder="<?= lang('from') ?>" type="text">
                        <input name="to_date"  value="<?= $this->input->post('to_date'); ?>"  class="validate[required] datepicker to_date" style=" width:48%; margin:0 1%" placeholder="<?= lang('to') ?>" type="text">
                    
                    <script>
                        $('.from_date').datepicker({
                            dateFormat: "yy-mm-dd",
                            controlType: 'select',
                            timeFormat: 'HH:mm'
                        });
                    </script>
                    <script>
                        $('.to_date').datepicker({
                            dateFormat: "yy-mm-dd",
                            controlType: 'select',
                            timeFormat: 'HH:mm'
                        });
                    </script>
                    
                    </div>
                </div>
                <div class="span4">
                    <div class="span4"><?= lang('time') ?>:</div>
                    <div class="span8">
                        <input name="from_time"  value="<?= $this->input->post('from_time'); ?>"  id="basic_example_1" value="" style=" width:48%; margin:0 1%" placeholder="<?= lang('from') ?>" type="text">
                        <input name="to_time"  value="<?= $this->input->post('to_time'); ?>"  id="basic_example_2" value="" style=" width:48%; margin:0 1%" placeholder="<?= lang('to') ?>" type="text">
                    </div>
                </div>
                <div class="span4">
                    <div class="span4"><?= lang('count') ?>:</div>
                    <div class="span8">

                        <input name="from_count"  value="<?= $this->input->post('from_count'); ?>"  style=" width:48%; margin:0 1%" placeholder="<?= lang('from') ?>" type="text">
                        <input name="to_count"  value="<?= $this->input->post('to_count'); ?>"  style=" width:48%; margin:0 1%" placeholder="<?= lang('to') ?>" type="text">

                    </div>
                </div>
            </div>

            <div class="row-form">
                <div class="span4">
                    <div class="span4"><?= lang('contract') ?>:</div>
                    <div class="span8">

                        <?= form_dropdown('safa_uo_contract_id', ddgen('safa_uo_contracts', array('safa_uo_contract_id', name()), array('safa_uo_id' => $this->uo_id)), set_value('safa_uo_contract_id', '0'), "class='input-huge' ") ?>

                    </div>
                </div>
                <div class="span4">
                    <div class="span4"><?= lang('trans_company') ?>:</div>
                    <div class="span8">
                        <?= form_dropdown('safa_transporter_id', ddgen('safa_transporters', array('safa_transporter_id', name())), set_value('safa_transporter_id', '0'),"  class='select' style='width:100%;' ") ?>
                    </div>
                </div>
                <div class="span4">
                    <div class="span4"><?= lang('nationality') ?>:</div>
                    <div class="span8">

                        <?= form_dropdown('nationality_id', ddgen('erp_countries', array('erp_country_id', name()), false, array(name(), 'asc')), set_value('nationality_id', '0')," name='s_example' class='select' style='width:100%;' ") ?>
                    </div>
                </div>
            </div>

            <div class="row-form">
                <div class="span4">
                    <div class="span4"><?= lang('port') ?>:</div>
                    <div class="span8">
                        <?= form_dropdown('erp_port_id', ddgen('erp_ports', array('erp_port_id', name()), array("country_code" => "SA"), array(name(), 'asc')), set_value('erp_port_id', '0'),"class='input-huge'") ?>
                    </div>
                </div>
                <div class="span4">
                    <div class="span4"><?= lang('city') ?>:</div>
                    <div class="span8">

                        <?= form_dropdown('erp_city_id', ddgen('erp_cities', array('erp_city_id', name()), array('erp_country_id' => '966'), array(name(), 'asc')), set_value('erp_city_id', '0'), "id='city'","class='input-huge'") ?>

                    </div>
                </div>
                <div class="span4">
                    <div class="span4"><?= lang('hotel') ?>:</div>
                    <div class="span8">
                        <? 
                        $city_filter = false;
                        if ( $this->trips_reports_model->erp_city_id ) {
                            $city_filter = array('erp_city_id' => $this->trips_reports_model->erp_city_id);
                        } 
                        //ddgen('erp_hotels', array('erp_hotel_id', name()), $city_filter, array(name(), 'asc'), false)
                        ?>
                        <?= form_dropdown('erp_hotel_id', $this->trips_reports_model->get_related_hotels_to_contracts(session('uo_id')) , set_value('erp_hotel_id', '0'), "id='hotel'") ?>
                    </div>
                </div>
            </div>

            
            
            <div class="row-form">
                <div class="span4">
                    <div class="span4"><?= lang('status') ?>:</div>
                    <div class="span8">
                        <?= form_dropdown('safa_trip_confirm_id', ddgen('safa_trip_confirm', array('safa_trip_confirm_id', name()), array("safa_trip_confirm_id <>" => '2'), array(name(), 'asc')), set_value('safa_trip_confirm_id', '0'),"class='input-huge'") ?>
                    </div>
                </div>
            </div>
            
            
            
            
            

            <div class="row-form">
                <div class="span1"><?= lang('choose_cols') ?></div>
                <div class="span11">
                    <input name="cols[]" value="uo_name" type="checkbox" <?= (in_array('uo_name', $cols)) ? "checked=checked" : "" ?> > <?= lang('uo_name') ?> &nbsp;&nbsp; 
                    <input name="cols[]" value="code" type="checkbox" <?= (in_array('code', $cols)) ? "checked=checked" : "" ?> > <?= lang('trip_id') ?> &nbsp;&nbsp; 
                    <input name="cols[]" value="arrival_date" type="checkbox" <?= (in_array('arrival_date', $cols)) ? "checked=checked" : "" ?> > <?= lang('arr_date') ?> &nbsp;&nbsp; 
                    <input name="cols[]" value="arrival_time" type="checkbox" <?= (in_array('arrival_time', $cols)) ? "checked=checked" : "" ?> > <?= lang('arr_time') ?> &nbsp;&nbsp; 
                    <input name="cols[]" value="contract" type="checkbox" <?= (in_array('contract', $cols)) ? "checked=checked" : "" ?> > <?= lang('contract') ?> &nbsp;&nbsp; 
                    <input name="cols[]" value="cn" type="checkbox" <?= (in_array('cn', $cols)) ? "checked=checked" : "" ?> > <?= lang('count') ?> &nbsp;&nbsp; 
                    <input name="cols[]" value="hotel" type="checkbox" <?= (in_array('hotel', $cols)) ? "checked=checked" : "" ?> > <?= lang('hotel') ?> &nbsp;&nbsp; 
                    <input name="cols[]" value="trans" type="checkbox" <?= (in_array('trans', $cols)) ? "checked=checked" : "" ?> > <?= lang('trans_company') ?> &nbsp;&nbsp; 
                    <input name="cols[]" value="port" type="checkbox" <?= (in_array('port', $cols)) ? "checked=checked" : "" ?> > <?= lang('port') ?> &nbsp;&nbsp; 
                    <input name="cols[]" value="nationality" type="checkbox" <?= (in_array('nationality', $cols)) ? "checked=checked" : "" ?> > <?= lang('nationality') ?> &nbsp;&nbsp; 
                    <input name="cols[]" value="supervisor" type="checkbox" <?= (in_array('supervisor', $cols)) ? "checked=checked" : "" ?> > <?= lang('supervisor') ?> &nbsp;&nbsp; 
                    <input name="cols[]" value="status" type="checkbox" <?= (in_array('status', $cols)) ? "checked=checked" : "" ?> > <?= lang('status') ?> &nbsp;&nbsp; 

                </div>
            </div>

            <div class="toolbar bottom TAC">
                <button class="btn btn-primary"><?= lang('search') ?></button>          
            </div>
        </div>
    </div>


    <div class="widget">
        
        
        
        
        
        <div class="widget-header">
	
	            <div class="widget-header-icon Fright">
	                <span class="icos-pencil2"></span>
	            </div>
	
	            <div class="widget-header-title Fright">

			    <div class="path-container Fright">
			        <div class="icon"><i class="icos-pencil2"></i></div> 
			         <div class="path-name Fright">
			                <?= lang('arriving_form_name') ?>
			        </div>    
			        
			       
			    </div>
	         </div>
	         
	          <div class="btn-group Fleft" style=" margin:1px 0 0 1px;color:#fff">
	                <input type="submit" name="print_report" value="<?= lang('print') ?>" class="btn" style="height: 28px"/>
	                <input type="submit" name="excel_export" value="<?= lang('export') ?>" class="btn" style="height: 28px"/>
	            	</div>
	                

	    </div>
        
        
                       
        <div class="widget-container">
            <div class='table-responsive' >
            <table class="fsTable" cellpadding="0" cellspacing="0">
                <thead>
                    <tr>
                        
                        <th><?= lang('trip') ?></th>
                        
                        <? if (in_array('uo_name', $cols)): ?>
                            <th><?= lang('uo_name') ?></th>
                        <? endif; ?>
                        
                        <? if (in_array('code', $cols)): ?>
                            <th><?= lang('trip_id') ?></th>
                        <? endif; ?>

                        <? if (in_array('arrival_date', $cols)): ?>
                            <th><?= lang('arr_date') ?></th>
                        <? endif; ?>

                        <? if (in_array('arrival_time', $cols)): ?>
                            <th><?= lang('arr_time') ?></th>
                        <? endif; ?>

                        <? if (in_array('contract', $cols)): ?>
                            <th><?= lang('contract') ?></th>
                        <? endif; ?>

                        <? if (in_array('cn', $cols)): ?>
                            <th><?= lang('count') ?></th>
                        <? endif; ?>

                        <? if (in_array('hotel', $cols)): ?>
                            <th><?= lang('hotel') ?></th>
                        <? endif; ?>

                        <? if (in_array('trans', $cols)): ?>
                            <th><?= lang('trans_company') ?></th>
                        <? endif; ?>

                        <? if (in_array('port', $cols)): ?>
                            <th><?= lang('port') ?></th>
                        <? endif; ?>

                        <? if (in_array('nationality', $cols)): ?>
                            <th><?= lang('nationality') ?></th>
                        <? endif; ?>

                        <? if (in_array('supervisor', $cols)): ?>
                            <th><?= lang('supervisor') ?></th>
                        <? endif; ?>   
                            
                        <? if (in_array('status', $cols)): ?>
                            <th><?= lang('status') ?></th>
                        <? endif; ?>
                            
                    </tr>
                </thead>
                <tbody>

                    <? foreach ($trips_ds as $val): ?>
                        <tr > 
                            
                            <td>
                            <?php 
                            if($val['safa_trip_internaltrip_id']=='') {
                            
								if($val['trip_name']!='') {
									echo $val['trip_name'];
								} else {
									echo $val['trip_title'];
								}
                            } else {
                            	
                            	if($val['trip_name']!='') {
									$safa_trip_internaltrip_link_text= $val['trip_name'];
								} else {
									$safa_trip_internaltrip_link_text=  $val['trip_title'];
								}
                            ?>
                            <a href="<?= site_url("safa_trip_internaltrips/view/". $val['safa_trip_internaltrip_id']) ?>" target="_blank"> <?php echo $safa_trip_internaltrip_link_text; ?> </a>
                            <?php 
                            }
                            
							?>
                            </td>
                            <? if (in_array('uo_name', $cols)): ?>
				<td><?= $val['uo_name'] ?></td>
                            <? endif; ?>
                            
                            <? if (in_array('code', $cols)): ?>
                                 <?php 
						$status_color = $this->flight_states->getFlightColor($val['fs_flight_id']);
						?>
						<td class=""><span style="background-color:#<?php echo $status_color;?>" class="label">
						<?= $val['flight_number'] ?> </span></td>
                            <? endif; ?>

                            <? if (in_array('arrival_date', $cols)): ?>
                                <td><?= get_date($val['arrival_date']) ?></td>
                            <? endif; ?>

                            <? if (in_array('arrival_time', $cols)): ?>
                                <td><?= get_time($val['arrival_time']) ?></td>
                            <? endif; ?>

                            <? if (in_array('contract', $cols)): ?>
                                <td><?= $val['cn'] ?></td>
                            <? endif; ?>

                            <? if (in_array('cn', $cols)): ?>
                                <td>
                                    <!--<a href="<?= site_url("trip_details/details/". $val['trip_id']) ?>" target="_blank">
                                        <?= ($val['travellers'])?($val['travellers']):($val['travellers_adult_count']+$val['travellers_child_count']+$val['travellers_infant_count']) ?>
                                    </a>
                                -->
                                <?= $val['seats_count'] ?>
                                </td>
                            <? endif; ?>

                            <? if (in_array('hotel', $cols)): ?>
                                <td><?= $val['hotel_name'] ?></td>
                            <? endif; ?>

                            <? if (in_array('trans', $cols)): ?>
                                <td><?php echo $val['transporter']; ?></td>
                            <? endif; ?>

                            <? if (in_array('port', $cols)): ?>
                                <td>
                                <?= $val['start_port_name'] ?>
								 - 
								<?= $val['end_port_name'] ?>
								
							(<?= $val['end_port_hall_name'] ?>)
                                
                                </td>
                            <? endif; ?>

                            <? if (in_array('nationality', $cols)): ?>
                                <td><?= $val['nationality'] ?></td>
                            <? endif; ?>

                            <? if (in_array('supervisor', $cols)): ?>
                                <td>
                                <?php 
			if($val['supervisor']!='') {
				echo $val['supervisor'];
			} else {
				echo $val['trip_supervisors'];
			}
			?>
                                </td>
                            <? endif; ?>

                            <? if (in_array('status', $cols)): ?>
                                 <?php 
			$status_name='';
			$status_row = $this->flight_states->getStatus($val['fs_flight_id']);
			if(count($status_row)>0) {
				$status_name = $status_row->{name()} ;
			}
			?>
			<td class=" "><?= $status_name ?></td>
                            <? endif; ?>
                                
                        </tr>
                    <? endforeach; ?>

                </tbody>
            </table>      
        </div>
        </div>
    </div>

</div>

<script>
    $('#city').change(function() {
        $.get('<?= site_url('uo/ajax/get_city_hotels') ?>/<?=session('uo_id')?>/'+ $(this).val(), function(data) {        
            $('#hotel').html(data);
        });
    });
</script>

<?= form_close() ?>   



<script>
 if ($(".fsTable").length > 0) {

	 	//By Gouda, TO remove the warning message.
	 	//$.fn.dataTableExt.sErrMode = 'throw';

        $(".fsTable").dataTable({

            bSort: false,
            bAutoWidth: true,
            "iDisplayLength":50, 
            "aLengthMenu": [5, 10, 25, 50, 100, 200, 500], // can be removed for basic 10 items per page
            "bFilter": true,
            "sPaginationType": "full_numbers",
            "aoColumnDefs": [{"bSortable": false,
            "aTargets": [-1, 0]}],
			"aDataSort": [3]
            });
    }
</script>
