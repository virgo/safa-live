
<style>input {margin-bottom : 4px !important;}</style>
<div class="span12">
    <div class="widget">
        <div class="path-container Fright">
            <div class="path-name Fright">
                <a href=""><?= lang('global_system_management') ?></a>
            </div>
            <div class="path-arrow Fright">
            </div>
            <div class="path-name Fright">
                <a href="<?= site_url('gov/gov_users/index') ?>"><?= lang('menu_external_agent_Users') ?></a>
            </div>
            <div class='path-arrow Fright'>
            </div>
            <div class="path-name Fright">
                <?= lang('add_uo_users') ?>
            </div>
        </div>
    </div>
</div>


<div class="widget">
    <div class="widget-header">

        <div class="widget-header-icon Fright">
            <span class="icos-pencil2"></span>
        </div>

        <div class="widget-header-title Fright">
            <?= lang('add_uo_users') ?> 
        </div>

    </div>  

    <div class="widget-container">                               
        <div class="block-fluid">
            <?= form_open("", "autocomplete='off' id='users_gov'" ) ?>

            <div class="row-form">
                <div class="span6">
                    <div class="span4 TAL Pleft10"><?= lang('uo_users_name_ar') ?><? if (name() == 'name_ar'): ?><font style="color:red" >*</font><? endif; ?></div>
                    <div class="span8" >
                        <?= form_input('name_ar', set_value("name_ar"),"class='input-huge validate[required]' ") ?>
                        <?= form_error('name_ar', '<div class="bottom" style="color:red" >', '</div>'); ?>
                    </div>
                </div>
                <div class="span6">
                    <div class="span4 TAL Pleft10"><?= lang('uo_users_name_la') ?><? if (name() == 'name_la'): ?><font style="color:red" >*</font><? endif; ?></div>
                    <div class="span8" >
                        <?= form_input('name_la', set_value("name_la"),"class='input-huge ' ") ?>
                        <?= form_error('name_la', '<div class="bottom" style="color:red" >', '</div>'); ?>
                    </div>    
                </div>
            </div>

            <div class="row-form">
                <div class="span6">
                    <div class="span4 TAL Pleft10"><?= lang('uo_users_username') ?> ( <?= lang('uo_users_email') ?> )<font style="color:red" >*</font></div>
                    <div class="span8" >
                        <?= form_input('email', set_value('email'),"class='input-huge validate[required]' ") ?>
                        <?= form_error('email', '<div class="bottom" style="color:red" >', '</div>'); ?>
                    </div>
                </div>
                <div class="span6" >
                    <div class="span4 TAL Pleft10"><?= lang('uo_users_safa_uo_usergroup_id') ?><font style="color:red" >*</font></div>
                    <div class="span8">
                        <?= form_dropdown('safa_gov_usergroup_id', ddgen('safa_gov_usergroups', array('safa_gov_usergroup_id', name()), array('safa_gov_id'=>session('gov_id')))+ddgen('safa_gov_usergroups', array('safa_gov_usergroup_id', name()), array('safa_gov_usergroup_id'=>3),FALSE,TRUE), set_value('safa_gov_usergroup_id'),"class=' select input-huge' style='width:90%'") ?>
                        <?= form_error('safa_gov_usergroup_id', '<div class="bottom" style="color:red" >', '</div>'); ?>
                    </div>
                </div>
            </div>
            <div class="row-form">
                <div class="span6">
                    <div class="span4 TAL Pleft10"><?= lang('uo_users_password') ?><font style="color:red" >*</font></div>
                    <div class="span8" >
                        <?= form_password('password', set_value('password'),"class='input-huge validate[required]' ") ?>
                        <?= form_error('password', '<div class="bottom" style="color:red" >', '</div>'); ?>
                    </div>
                </div>
                <div class="span6">
                    <div class="span4 TAL Pleft10"><?= lang('uo_users_confirm_password') ?><font style="color:red" >*</font></div>
                    <div class="span8">
                        <?= form_password('passconf', set_value('passconf'),"class='input-huge validate[required]' ") ?>
                        <?= form_error('passconf', '<div class="bottom" style="color:red" >', '</div>'); ?>
                    </div>
                </div>
            </div>

            <div class="toolbar bottom TAC">
                <input type="submit" name="submit" value="<?= lang('global_submit') ?>" class="btn btn-primary"> &nbsp;
                <input  type ="button" value="<?= lang('global_back') ?>"class="btn btn-primary" onclick="window.location = '<?= site_url('uo/uo_users/index') ?>'">
            </div>

            <?= form_close() ?> 
        </div>
    </div>
</div>

<script>
        $("#users_gov").validationEngine({
        prettySelect: true,
        useSuffix: "_chosen",
        promptPosition: "topRight:-150"
//promptPosition : "bottomLeft"
    });
    
</script>


    

