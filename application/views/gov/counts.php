<div class="span12">
    <div class="widget">


        <div class="path-container Fright">
            <div class="icon"><i class="icos-pencil2"></i></div> 
            <div class="path-name Fright">
                <a href="<?= site_url('gov/dashboard') ?>"><?= lang('global_system_management') ?></a>
            </div>    
            <div class="path-arrow Fright">
            </div>
            <div class="path-name Fright">
                <span><?= lang('counts_report') ?></span>
            </div>


        </div> 
    </div> 
</div>

<div class="row-fluid">              
    <div class="widget">
        <div class="block-fluid">
            <form method="post">
                <?php if (validation_errors()) : ?>
                    <div class="row-form">
                        <div class="span3"><?php echo form_error('from') ?></div>
                        <div class="span3"><?php echo form_error('to') ?></div>
                        <div class="span3"><?php echo form_error('type') ?></div>
                        <div class="span3">

                        </div>
                    </div>
                <?php endif ?>
                <div class="row-form">
                    <div class="span3">
                        <span class="span4"><?php echo lang('from') ?></span>
                        <span class="span8"><?php echo form_input('from', set_value_old('from', date('Y-m-d')), 'class="validate[required] datepicker "'); ?></span>
                    </div>
                    <div class="span3">
                        <span class="span4"><?php echo lang('to') ?></span>
                        <span class="span8"><?php echo form_input('to', set_value_old('to', date('Y-m-d')), 'class="validate[required] datepicker "'); ?></span>
                    </div>
                    <div class="span3">
                        <span class="span4"><?php echo lang('type') ?></span>
                        <span class="span8"><?php echo form_dropdown('type', $types, set_value_old('type')); ?></span>
                    </div>
                    <div class="span3">
                        <span class="span12"><?php echo form_submit('dosearch', lang('search'), 'class="btn"'); ?></span>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<?php if (count($results)) : ?>
    <?php $total_trip = 0;
    $total_passport = 0; ?>
<div class="widget">
    <div class="widget-container">
        <div class='table-responsive' >
            <table class="" cellpadding="0" cellspacing="0">
                <thead>
                    <tr>
                        <th><?php echo $types[$type] ?></th>
                        <th><?php echo lang('trip_count') ?></th>
                        <th><?php echo lang('individuals_count') ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($results as $result) : ?>
        <?php $total_trip += $result->trip_count;
        $total_passport += $result->total_passports; ?>
                        <tr>
                            <td><?php echo $result->name ?></td>
                            <td><?php echo $result->trip_count ?></td>
                            <td><?php echo $result->total_passports ?></td>
                        </tr>
    <?php endforeach; ?>
                </tbody>
                <thead>
                    <tr>
                        <td><?php echo lang('total') ?></td>
                        <td><?php echo $total_trip ?></td>
                        <td><?php echo $total_passport ?></td>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>
<?php else : ?>
    <?php echo lang('global_no_data'); ?>
<?php endif; ?>
