<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="<?= lang('global_lang') ?>" xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title><?= lang('global_title') ?></title>
        <link rel="shortcut icon" href="<?= base_url() ?>/static/favicon.ico" type="image/x-icon" />
        <link href="<?= NEW_CSS_JS . '_' . lang('DIR') ?>/common-css.css" rel="stylesheet" type="text/css" />
        <link href="<?= NEW_CSS_JS . '_' . lang('DIR') ?>/slide-menu.css" rel="stylesheet" type="text/css" />
        <link href="<?= NEW_CSS_JS ?>/font-awesome.css" rel="stylesheet" type="text/css" />
        <link href="<?= NEW_CSS_JS . '_' . lang('DIR') ?>/glyphicons_filetypes.css" rel="stylesheet" type="text/css" />
        <link href="<?= NEW_CSS_JS . '_' . lang('DIR') ?>/glyphicons_regular.css" rel="stylesheet" type="text/css" />
        <link href="<?= NEW_CSS_JS . '_' . lang('DIR') ?>/glyphicons_social.css" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="<?= NEW_CSS_JS . '_' . lang('DIR') ?>/jquery-ui.css" class="adly" rel="stylesheet" type="text/css" />
        <link href="<?= NEW_CSS_JS . '_' . lang('DIR') ?>/validationEngine.jquery.css" rel="stylesheet" type="text/css" />
        <link href="<?= NEW_CSS_JS . '_' . lang('DIR') ?>/tipsy.css" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="<?= NEW_CSS_JS . '_' . lang('DIR') ?>/accordionmenu.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="<?= NEW_CSS_JS . '_' . lang('DIR') ?>/wizard/formToWizard.css" />
        <?= js() ?>
        <link rel="stylesheet" href="<?= NEW_JS ?>/plugins/scroll/jquery.custom-scrollbar.css" />
        <link rel="stylesheet" href="<?= NEW_CSS_JS . '_' . lang('DIR') ?>/top-dropdown/css/styles.css" />
        <!--[if lt IE 9]>
                  <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
                <![endif]-->
        <!-- hide and show -->
        <link href="<?= NEW_CSS_JS . '_' . lang('DIR') ?>/multiselect/multiselect.css" rel="stylesheet" type="text/css" media="screen" />
        <link rel="stylesheet" href="<?= NEW_CSS_JS . '_' . lang('DIR') ?>/multi-select-chosen/chosen.css" />
        <link rel="stylesheet" href="<?= NEW_CSS_JS . '_' . lang('DIR') ?>/fancybox/jquery.fancybox.css" type="text/css" media="screen" />
        <link rel="stylesheet" media="all" type="text/css" href="<?= NEW_CSS_JS . '_' . lang('DIR') ?>/datetimepicker/jquery-ui.css" class="adly" />
        <link rel="stylesheet" media="all" type="text/css" href="<?= NEW_CSS_JS . '_' . lang('DIR') ?>/datetimepicker/jquery-ui-timepicker-addon.css" />
        <link rel="stylesheet" href="<?= base_url("static/alertify") ?>/alertify.core.css" />
        <link rel="stylesheet" href="<?= base_url("static/alertify") ?>/alertify.default.css" />
        <? $this->load->view("analytics") ?>
        <script type="text/javascript">
            if (typeof String.prototype.startsWith != 'function') {
                // see below for better implementation!
                String.prototype.startsWith = function(str) {
                    return this.indexOf(str) == 0;
                };
            }
            $(document).ready(function() {
                var accordion_head = $('.accordion > li > a'),
                        accordion_body = $('.accordion li > .sub-menu');
                //accordion_head.first().addClass('active').next().slideDown('normal');
                accordion_head.eq(<?php
if (session('side_menu_id')) {
    echo session('side_menu_id');
} else {
    echo '0';
}
?>).addClass('active').next().slideDown('normal');
                accordion_head.on('click', function(event) {
                    // Disable header links
                    event.preventDefault();
                    // Show and hide the tabs on click
                    if ($(this).attr('class') != 'active') {
                        accordion_body.slideUp('normal');
                        $(this).next().stop(true, true).slideToggle('normal');
                        accordion_head.removeClass('active');
                        $(this).addClass('active');
                    }
                });
            });
            jQuery(function() { // on DOM load
                menu1 = new sidetogglemenu({// initialize first menu example
                    id: 'togglemenu1',
                    pushcontent: true // <- No comma after last option!
                })

                menu1.toggle();
            })

            $(document).ready(function() {
                $(" .chosen-container, select").addClass("input-huge");
                $(".slidingDiv").show();
                $(".show_hide").show();
                $('.show_hide').click(function() {
                $(".slidingDiv").slideToggle();
                });
            });
        
            $(function() {
                $('#tabs').tabs();
                $('.example-container > pre').each(function(i) {
                    eval($(this).text());
                });
            });
            $(function() {
                $(".checkbox").change(function() {
                    $(this).closest("tr").toggleClass("highlight", this.checked)
                })
            })
            function updateButton() {
                if ($('form .group1:checked').length == 0) {
                    $('form .group2')
                            .prop('disabled', true);
                } else {
                    $('form .group2').prop('disabled', false);
                }
            }
            $('form .group1').change(function() {
                updateButton();
            });
            function showDiv(toggle) {
                document.getElementById(toggle).style.display = 'block';
            }
            function logout() {
                input_box = confirm("<?= lang('global_are_you_sure_you_want_to_logout') ?>");
                if (input_box == true)
                {
                    self.parent.location.replace("<?= site_url('/logout') ?>")
                }
            }
            $(document).ready(function() {
                $(".slidingDiv").show();
                $(".show_hide").show();
                $('.show_hide').click(function() {
                    $(".slidingDiv").slideToggle();
                });
            });
        </script>
    </head>
    <body>
        <div class="header"><a href="<?= site_url() ?>"> <img
                    src="<?= NEW_IMAGES ?>/safa_logo_beta.svg" width="" height=""
                    class="logo" /> </a>

            <div class="header-icons">
                <style>
                    #colorNav li ul {
                        top: 9px;
                    }

                    #colorNav>ul>li {
                        margin: 6px;
                    }

                    .count-num {
                        float: right;
                        font-size: 11px;
                        font-weight: 900;
                        margin-left: 2px;
                        margin-top: 8px;
                    }
                    .viewport .overview {
                        width: 300px !important;

                    }

                    .logout {
                        width: 180px !important;
                    }
                    .logout .viewport .overview {
                        width: 180px !important;

                    }

                </style>

                <nav id="colorNav" >
                    <ul style="display: run-in; ">

                        <!-- By Gouda , Display Username Current Account, and His Company-->
                        <li><?php
                            if ($destination == 'uo') {
                                $company_id = session('uo_id');
                                $company_name = session('uo_name');
                                $user_id = session('uo_user_id');
                                $username = session('uo_user_name');
                                echo '<b>' . lang('welcome') . ':</b> ' . $username . ' - <b>' . lang($destination) . ':</b> ' . $company_name;
                            } else if ($destination == 'ea') {
                                $company_id = session('ea_id');
                                $company_name = session('ea_name');
                                $user_id = session('ea_user_id');
                                $username = session('ea_user_name');
                                echo '<b>' . lang('welcome') . ':</b> ' . $username . ' - <b>' . lang($destination) . ':</b> ' . $company_name;
                            } else if ($destination == 'hm') {
                                $company_id = session('hm_id');
                                $company_name = session('hm_name');
                                $user_id = session('hm_user_id');
                                $username = session('hm_user_name');
                                echo '<b>' . lang('welcome') . ':</b> ' . $username . ' - <b>' . lang($destination) . ':</b> ' . $company_name;
                            } else if ($destination == 'gov') {
                                $company_id = session('gov_id');
                                $company_name = session('gov_name');
                                $user_id = session('gov_user_id');
                                $username = session('gov_user_name');
                                echo '<b>' . lang('welcome') . ':</b> ' . $username . ' - <b>' . lang($destination) . ':</b> ' . $company_name;
                            } else if ($destination == 'ito') {
                                $company_id = session('ito_id');
                                $company_name = session('ito_name');
                                $user_id = session('ito_user_id');
                                $username = session('ito_user_name');
                                echo '<b>' . lang('welcome') . ':</b> ' . $username . ' - <b>' . lang($destination) . ':</b> ' . $company_name;
                            } else if ($destination == 'admin') {
                                $company_id = session('admin_id');
                                $company_name = session('admin_name');
                                $user_id = session('admin_user_id');
                                $username = session('admin_user_name');
                                echo '<b>' . lang('welcome') . ':</b> ' . $username . ' - <b>' . lang($destination) . '</b>';
                            }
                            ?></li>


                        <li><a href="<?php echo site_url('notifications/viewAll'); ?>"><span
                                    class="icon-exclamation-sign"></span>
                                <div class="count-num" id="dv_new_notification_count"></div>
                            </a>
                            <ul id="dv_new_notification" class="default-skin">

                            </ul>
                        </li>
                        <li class=""><a href="<?php echo site_url('messages/viewAll'); ?>"><span
                                    class="icon-envelope"></span>
                                <div class="count-num" id="dv_new_message_count"></div>
                            </a>
                            <ul id="dv_new_message" class="default-skin">

                            </ul>
                        </li>

                        <li><a href="#" style="padding: 3px 5px;display:block;"><span class="icon-user"></span></a>
                            <ul class="logout">
                                <li><a 
                                        href="<?= site_url($destination . '/user_personal_settings/edit') ?>"><span
                                            class="icon-cog icon-white"></span><?= lang('global_settings') ?></a>
                                </li>
                                <li><a href="javascript:void(0)" onClick="logout()"
                                       ><span
                                            class="icon-off icon-white"></span><?= lang('global_logout') ?></a></li>
                            </ul>
                        </li>
                    </ul>
                </nav> 
                <script>
<? if ($_SERVER['REMOTE_ADDR'] != '127.0.0.1') : ?>
var myVar = setInterval(function(){get_notifications()},30000);
<? endif ?>
                    function get_notifications() {

<?php
$current_user_type_id = 0;
$current_loin_receiver_id = 0;
$current_loin_user_id = 0;
if (session('admin_user_id')) {
    $current_loin_receiver_id = session('admin_user_id');
    $current_loin_user_id = session('admin_user_id');
    $current_user_type_id = 1;
} else if (session('uo_id')) {
    $current_loin_receiver_id = session('uo_id');
    $current_loin_user_id = session('uo_user_id');
    $current_user_type_id = 2;
} else if (session('ea_id')) {
    $current_loin_receiver_id = session('ea_id');
    $current_loin_user_id = session('ea_user_id');
    $current_user_type_id = 3;
} else if (session('hm_id')) {
    $current_loin_receiver_id = session('hm_id');
    $current_loin_user_id = session('hm_user_id');
    $current_user_type_id = 4;
} else if (session('ito_id')) {
    $current_loin_receiver_id = session('ito_id');
    $current_loin_user_id = session('ito_user_id');
    $current_user_type_id = 5;
}
?>

                        $.get("<?php echo site_url('notifications/get'); ?>/<?php echo $current_user_type_id; ?>/<?php echo $current_loin_receiver_id; ?>/<?php echo $current_loin_user_id; ?>", function(data) {

                                    var inner_html = '';
                                    var notifications_arr = JSON.parse(data);

                                    for (var i = 0; i < notifications_arr.length; i++) {

                                        inner_html = inner_html + "<li class='" + notifications_arr[i]['cls_notification_message_dv'] + "' id='li_" + notifications_arr[i]['erp_notification_detail_id'] + "' style='margin:1px; '>" +
                                                "<span id='" + notifications_arr[i]['erp_notification_detail_id'] + "' class='icon-trash notification_del del' title='<?php echo lang('delete_message'); ?>' style=''  ></span>" +
                                                "<span id='" + notifications_arr[i]['erp_notification_detail_id'] + "' alt='" + notifications_arr[i]['mark_as_important_value'] + "' class='icon-star notification_star fav' title='" + notifications_arr[i]['mark_as_important_link_text'] + "' style=''  ></span>" +
                                                "<a class='fancybox fancybox.iframe' href='<?php echo site_url("notifications/read") ?>" + "/" + notifications_arr[i]['erp_notification_detail_id'] + "' >" +
                                                "<h5 class='mass-title' >" + notifications_arr[i]['notification_subject'] + "</h5>" +
                                                //"<p>" + notifications_arr[i]['notification_body_brief'] + "</p>" +
                                                notifications_arr[i]['notification_body_brief'] + "</a>" +
                                                "</li>";


                                        //By Gouda.
                                        //alert(notifications_arr[i]['diff_with_second']);
                                        if (notifications_arr[i]['diff_with_second'] > 1 && notifications_arr[i]['diff_with_second'] < 6) {
                                            var html_alert = " <div id='main_notification_alert_dv'>       <span class='icon-cancel' title='<?php echo lang('close'); ?>' style='float:left; cursor: pointer;' onclick='document.getElementById(\"main_notification_alert_dv\").style.display = \"none\" '></span>      <a class='fancybox fancybox.iframe'   href='<?php echo site_url("notifications/read"); ?>/" + notifications_arr[i]['erp_notification_detail_id'] + "' ><b>" + notifications_arr[i]['notification_subject'] + "</b><br/>" + notifications_arr[i]['notification_body_brief'] + "</a> </div>";

                                            html_alert = html_alert + "<script>";
                                            html_alert = html_alert + "    $('.fancybox').fancybox({ afterClose: function() {}});";
                                            html_alert = html_alert + "<\/script>";

                                            alertify.success(html_alert, 10000);

                                            /*		
                                             var ping = new Audio('<?php echo site_url(); ?>/static/alertify/sounds/drip.mp3');
                                             ping.play();
                                             var audioElement = document.createElement('audio');
                                             audioElement.setAttribute('src', '<?php echo site_url(); ?>/static/alertify/sounds/drip.mp3');
                                             audioElement.play();
                                             */

                                            if (window.HTMLAudioElement) {
                                                var snd = new Audio('');

                                                if (snd.canPlayType('audio/ogg')) {
                                                    snd = new Audio('<?php echo site_url(); ?>/static/alertify/sounds/drip.ogg');
                                                }
                                                else if (snd.canPlayType('audio/mp3')) {
                                                    snd = new Audio('<?php echo site_url(); ?>/static/alertify/sounds/drip.mp3');
                                                }

                                                snd.play();
                                            } else {
                                                alert('HTML5 Audio is not supported by your browser!');
                                            }

                                        }
                                    }

                                    $("#dv_new_notification").html(inner_html);
                                    $("#dv_new_notification_count").html(notifications_arr[0]['unreaded_notifications_count']);

                                });

                                $.get("<?php echo site_url('messages/get'); ?>/<?php echo $current_user_type_id; ?>/<?php echo $current_loin_receiver_id; ?>/<?php echo $current_loin_user_id; ?>", function(data) {

                                            var inner_html = "<li><a  href='<?php echo site_url("messages/index") ?>' ><i style='font-size:20px; color: #000;' class='icon-edit-sign '></i> <?php echo lang('new_message'); ?></a></li>";

                                            var messages_arr = JSON.parse(data);

                                            for (var i = 0; i < messages_arr.length; i++) {

                                                inner_html = inner_html + "<li class='" + messages_arr[i]['cls_message_message_dv'] + "' id='li_" + messages_arr[i]['erp_message_detail_id'] + "' style='margin:1px; '>" +
                                                        "<span id='" + messages_arr[i]['erp_message_detail_id'] + "' class='icon-trash message_del' title='<?php echo lang('delete_message'); ?>' style='float:left; cursor: pointer; margin: 3px 3px 3px 3px;color:#000;'  ></span>" +
                                                        "<span id='" + messages_arr[i]['erp_message_detail_id'] + "' alt='" + messages_arr[i]['mark_as_important_value'] + "' class='icon-star message_star' title='" + messages_arr[i]['mark_as_important_link_text'] + "' style='color:#EECE04; float:left; cursor: pointer; margin: 3px 3px 3px 3px;'  ></span>" +
                                                        "<a class='fancybox fancybox.iframe' href='<?php echo site_url("messages/read") ?>" + "/" + messages_arr[i]['erp_message_detail_id'] + "'>" +
                                                        "<h5 class='mass-title'>" + messages_arr[i]['message_subject'] + "</h5>" +
                                                        "<p>" + messages_arr[i]['message_body_brief'] + "</p>" +
                                                        "</a></li>";

                                            }

                                            $("#dv_new_message").html(inner_html);
                                            $("#dv_new_message_count").html(messages_arr[0]['unreaded_messages_count']);

                                        });


                                    }
                    $(document).ready(function() {
                                        get_notifications();
                                        $('#colorNav').on('click', '.notification_del', function(data) {
                                            var notification = $(this);
                                            $.get('<?= site_url('notifications/hide') ?>/' + $(this).attr('id'), function(data) {
                                                notification.parent().remove();

                                            });
                                        });

                                        $('#colorNav').on('click', '.notification_star', function(data) {
                                            //alert($('#li_'+$(this).attr('id')).attr('id'));
                                            if ($(this).attr('alt') == 1) {
                                                $('#li_' + $(this).attr('id')).attr('class', 'cls_notification_message_important_dv');
                                                $(this).attr('alt', 0);
                                            } else {
                                                $('#li_' + $(this).attr('id')).attr('class', 'cls_notification_message_readed_dv');
                                                $(this).attr('alt', 1);
                                            }
                                            $.get('<?= site_url('notifications/markAsImportant') ?>/' + $(this).attr('id') + '/' + $(this).attr('alt'), function(data) {
                                            });
                                        });

                                        $('#colorNav').on('click', '.message_del', function(data) {
                                            var notification = $(this);
                                            $.get('<?= site_url('messages/hide') ?>/' + $(this).attr('id'), function(data) {
                                                notification.parent().remove();
                                            });
                                        });

                                        $('#colorNav').on('click', '.message_star', function(data) {

                                            if ($(this).attr('alt') == 1) {
                                                $('#li_' + $(this).attr('id')).attr('class', 'cls_notification_message_important_dv');
                                                $(this).attr('alt', 0);
                                            } else {
                                                $('#li_' + $(this).attr('id')).attr('class', 'cls_notification_message_readed_dv');
                                                $(this).attr('alt', 1);
                                            }

                                            $.get('<?= site_url('messages/markAsImportant') ?>/' + $(this).attr('id') + '/' + $(this).attr('alt'), function(data) {
                                            });
                                        });

                                        $('#colorNav').on('click', '.fancybox', function() {
                                            $.get($(this).attr('href'), function(data) {
                                                $.fancybox({
                                                    content: data
                                                });
                                            });
                                            return false;
                                        });


                                    });
                    $(document).ready(function() {
                        if ($('.block-fluid').find('table').length) {
                            $('.block-fluid').addClass('table-responsive');

                            $('.block-fluid').removeClass('block-fluid');
                        }
                        ;

                        //$(".table-responsive table").css('height','100px').css('width','900px').customScrollbar({preventDefaultScroll: true});
                        $("#colorNav li").hover(function() {
                            $("#colorNav li ul").customScrollbar({preventDefaultScroll: true});
                        });
                    });
                </script>
            </div>
        </div>
        <div id="togglemenu1" class="sidetogglemenu"><?php if (lang('DIR') == 'rtl'): ?>
                <img src="<?= NEW_IMAGES ?>/arrow.png" onClick="menu1.toggle();"
                     class="sideviewtoggle" style="cursor: pointer" /> <?php else: ?> <img
                     src="<?= NEW_IMAGES ?>/arrow_ltr.png" onClick="menu1.toggle();"
                     class="sideviewtoggle" style="cursor: pointer" /> <?php endif; ?> 
                <?php $this->load->view("layout/new_menu/" . $destination) ?>
        </div>
        <div class="ads">
            <div class="ad"><img width="120" height="240" src="<?= NEW_IMAGES ?>/ads.gif" /></div>
        </div>
        <div class="content">{layout}</div>
    </body>
    <script type="text/javascript">
        $(function() {
            $('body').on('change', '.chosen-rtl', function() {
                if ($(this).val())
                    $(this).parent().find('.formError').remove();
            });
            $('body').on('change', '[class*=datepicker]', function() {
                if ($(this).val())
                    $(this).parent().find('.formError').remove();
            });
            $('body').on('change', '[class*=validate]', function() {
                if ($(this).val())
                    $(this).parent().find('.formError').remove();
            });
        });
        </script>
<script>
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

            ga('create', 'UA-49628000-1', 'safalive.com');
            ga('send', 'pageview');
        </script>
    <div id="proactivechatcontainermee6uhw03d"></div><div id="swifttagcontainermee6uhw03d" style="display: none;"><div id="swifttagdatacontainermee6uhw03d"></div></div> <script type="text/javascript">var swiftscriptelemmee6uhw03d=document.createElement("script");swiftscriptelemmee6uhw03d.type="text/javascript";var swiftrandom = Math.floor(Math.random()*1001); var swiftuniqueid = "mee6uhw03d"; var swifttagurlmee6uhw03d="http://support.figoria.com/safa/visitor/index.php?/LiveChat/HTML/Monitoring/cHJvbXB0dHlwZT1jaGF0JnVuaXF1ZWlkPW1lZTZ1aHcwM2QmdmVyc2lvbj00LjYxLjAuNDA4OCZwcm9kdWN0PUZ1c2lvbiZ2YXJpYWJsZVswXVswXT1TYWZhK0xpdmUmdmFyaWFibGVbMF1bMV09JmN1c3RvbW9ubGluZT0mY3VzdG9tb2ZmbGluZT0mY3VzdG9tYXdheT0mY3VzdG9tYmFja3Nob3J0bHk9CmEwNmNhYmEwNDFiZTQxZDE4NGJkMzk5NzJjMTkwM2JmZTA3ZDIzZDU=";setTimeout("swiftscriptelemmee6uhw03d.src=swifttagurlmee6uhw03d;document.getElementById('swifttagcontainermee6uhw03d').appendChild(swiftscriptelemmee6uhw03d);",1);</script>
</html>
