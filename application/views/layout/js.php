<!DOCTYPE html>
<html lang="<?= lang('global_lang') ?>" dir="<?= lang('global_direction') ?>">
    <head>  
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
        <meta http-equiv="cache-control" content="no-cache">
        <meta http-equiv="expires" content="Mon, 22 Jul 2002 11:12:01 GMT">
        <link rel="icon" type="image/ico" href="<?= base_url() ?>/favicon.ico"/>
        <title><?= lang('global_title') ?></title>
        <link href="<?= CSS ?>/stylesheets.css" rel="stylesheet" type="text/css" />
        <link href="<?= JS ?>/css-js/lang-bar.css" rel="stylesheet" type="text/css" />
        <?if(lang('global_lang') == 'ar'): ?>
          <link href="<?= CSS ?>/stylesheet-ar.css" rel="stylesheet" type="text/css" />
        <? endif ?>
        <script type='text/javascript' src='<?= JS ?>/plugins/jquery/jquery-1.9.1.min.js'></script>
        <script type='text/javascript' src='<?= JS ?>/plugins/jquery/jquery-ui-1.10.1.custom.min.js'></script>
        <!-- Preloader -->
        <script type="text/javascript">// <![CDATA[
        $(window).load(function() { $("#loading_layer").fadeOut("slow"); });
        // ]]>
        </script>
        <style>
        #loading_layer{
            position:absolute;
            width:100%;
            height:100%;
            top:0;
            left:0;
            z-index:999999;
            background-color: #FFF;
            text-align: center;
            margin: 0 auto;
            background:#e8e5db url(<?= IMAGES ?>/bg_default.jpg);
        }
        </style>
        <script type='text/javascript' src='<?= JS ?>/plugins/jquery/jquery-migrate-1.1.1.min.js'></script>
        <script type='text/javascript' src='<?= JS ?>/plugins/jquery/globalize.js'></script>
        <script type='text/javascript' src='<?= JS ?>/plugins/other/excanvas.js'></script>
        <script type='text/javascript' src='<?= JS ?>/plugins/other/jquery.mousewheel.min.js'></script>
        <script type='text/javascript' src='<?= JS ?>/plugins/bootstrap/bootstrap.min.js'></script>
        <script type='text/javascript' src='<?= JS ?>/plugins/bootstrap/bootstrap-fileupload.min.js'></script>
        <script type='text/javascript' src='<?= JS ?>/plugins/cookies/jquery.cookies.2.2.0.min.js'></script>
        <script type='text/javascript' src='<?= JS ?>/plugins/fancybox/jquery.fancybox.pack.js'></script>
        <script type='text/javascript' src='<?= JS ?>/plugins/jflot/jquery.flot.js'></script>    
        <script type='text/javascript' src='<?= JS ?>/plugins/jflot/jquery.flot.stack.js'></script>    
        <script type='text/javascript' src='<?= JS ?>/plugins/jflot/jquery.flot.pie.js'></script>
        <script type='text/javascript' src='<?= JS ?>/plugins/jflot/jquery.flot.resize.js'></script>
        <script type='text/javascript' src='<?= JS ?>/plugins/epiechart/jquery.easy-pie-chart.js'></script>
        <script type='text/javascript' src='<?= JS ?>/plugins/knob/jquery.knob.js'></script>
        <script type='text/javascript' src='<?= JS ?>/plugins/sparklines/jquery.sparkline.min.js'></script>    
        <script type='text/javascript' src='<?= JS ?>/plugins/pnotify/jquery.pnotify.min.js'></script>
        <script type='text/javascript' src='<?= JS ?>/plugins/fullcalendar/fullcalendar.min.js'></script>        
        <script type='text/javascript' src='<?= JS ?>/plugins/datatables/jquery.dataTables.min.js'></script>    
        <script type='text/javascript' src='<?= JS ?>/plugins/wookmark/jquery.wookmark.js'></script>        
        <script type='text/javascript' src='<?= JS ?>/plugins/jbreadcrumb/jquery.jBreadCrumb.1.1.js'></script>
        <script type='text/javascript' src='<?= JS ?>/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js'></script>
        <script type='text/javascript' src="<?= JS ?>/plugins/uniform/jquery.uniform.min.js"></script>
        <script type='text/javascript' src="<?= JS ?>/plugins/select/select2.min.js"></script>
        <script type='text/javascript' src='<?= JS ?>/plugins/tagsinput/jquery.tagsinput.min.js'></script>
        <script type='text/javascript' src='<?= JS ?>/plugins/maskedinput/jquery.maskedinput-1.3.min.js'></script>
        <script type='text/javascript' src='<?= JS ?>/plugins/multiselect/jquery.multi-select.min.js'></script>    
        <?if(lang('global_lang')=='ar'):?>
            <script type='text/javascript' src='<?= JS ?>/plugins/validationEngine/languages/jquery.validationEngine-ar.js'></script>
        <?else:?>
              <script type='text/javascript' src='<?= JS ?>/plugins/validationEngine/languages/jquery.validationEngine-en.js'></script>
        <?endif;?>
        <script type='text/javascript' src='<?= JS ?>/plugins/validationEngine/jquery.validationEngine.js'></script>        
        <script type='text/javascript' src='<?= JS ?>/plugins/stepywizard/jquery.stepy.js'></script>
        <script type='text/javascript' src='<?= JS ?>/plugins/animatedprogressbar/animated_progressbar.js'></script>
        <script type='text/javascript' src='<?= JS ?>/plugins/hoverintent/jquery.hoverIntent.minified.js'></script>
        <script type='text/javascript' src='<?= JS ?>/plugins/media/mediaelement-and-player.min.js'></script>    
        <script type='text/javascript' src='<?= JS ?>/plugins/cleditor/jquery.cleditor.js'></script>
        <!-- only editor.html -->
        <script type='text/javascript' src='<?= JS ?>/plugins/ckeditor/ckeditor.js'></script>
        <!-- eof only editor.html -->
        <script type='text/javascript' src='<?= JS ?>/plugins/shbrush/XRegExp.js'></script>
        <script type='text/javascript' src='<?= JS ?>/plugins/shbrush/shCore.js'></script>
        <script type='text/javascript' src='<?= JS ?>/plugins/shbrush/shBrushXml.js'></script>
        <script type='text/javascript' src='<?= JS ?>/plugins/shbrush/shBrushJScript.js'></script>
        <script type='text/javascript' src='<?= JS ?>/plugins/shbrush/shBrushCss.js'></script>    
        <script type='text/javascript' src='<?= JS ?>/plugins/filetree/jqueryFileTree.js'></script>        
        <script type='text/javascript' src='<?= JS ?>/plugins/slidernav/slidernav-min.js'></script>    
        <script type='text/javascript' src='<?= JS ?>/plugins/isotope/jquery.isotope.min.js'></script>  
        <script type='text/javascript' src='<?= JS ?>/plugins/jnotes/jquery-notes_1.0.8_min.js'></script>
        <script type='text/javascript' src='<?= JS ?>/plugins/jcrop/jquery.Jcrop.min.js'></script>
        <script type='text/javascript' src='<?= JS ?>/plugins/ibutton/jquery.ibutton.min.js'></script>
        <script type='text/javascript' src='<?= JS ?>/plugins.js'></script>
        <script type='text/javascript' src='<?= JS ?>/charts.js'></script>
        <script type='text/javascript' src='<?= JS ?>/actions.js'></script>
        <script type='text/javascript' src='<?= JS ?>/custom/create_plugins.js'></script>
        <!-- color picker -->
        <script language="javascript" src="<?= JS ?>/css-js/cp/picker.js"></script>
        <link href="<?= JS ?>/css-js/cp/picker.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript">
            function showDiv(toggle) {
                document.getElementById(toggle).style.display = 'block';
            }
        </script>
        <script type="text/javascript">
            function logout()
            {
                input_box = confirm("<?= lang('global_are_you_sure_you_want_to_logout') ?>");
                if(input_box == true)
                {
                    self.parent.location.replace("<?= site_url($destination.'/logout') ?>")
                }
            }
            $(document).ready(function(){
                $(".slidingDiv").show();
                    $(".show_hide").show();
                    $('.show_hide').click(function(){
                        $(".slidingDiv").slideToggle();
                    });
            });
        </script>
        <style>
        @media only screen and (max-width: 600px) {
            .ads {
                display: none;
            }
            .content{
                    margin-left:0 !important;
                    margin-right:0 !important;
                    min-width: 300px !important;
            }
        }
        .updated_msg{
           padding:8px 35px 8px 14px !important;
           margin-bottom:20px !important;
           margin-left: 0px !important;
           margin-right: 0px !important;
           color:#c09853 !important;
           text-shadow:0 1px 0 rgba(255,255,255,0.5) !important;
           background-color:#fcf8e3 !important;
           border:1px solid #fbeed5 !important;
           -webkit-border-radius:4px !important;
           -moz-border-radius:4px !important;
           border-radius:4px !important;
           color:#468847 !important;
           background-color:#dff0d8 !important;
           border-color:#d6e9c6 !important;
           border: 3px solid rgb(255, 255, 255) !important;
           text-align: center !important;
        }
        </style>
    </head>
    <body>
    {layout}
    </body>
</html>
