<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="<?= lang('global_lang') ?>" xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title><?= lang('global_title') ?></title>
        <link href="<?= NEW_CSS_JS .'_'. lang('DIR') ?>/common-css.css" rel="stylesheet" type="text/css" />
        <link href="<?= NEW_CSS_JS .'_'. lang('DIR') ?>/slide-menu.css" rel="stylesheet" type="text/css" />

         <link rel="stylesheet" href="<?= NEW_CSS_JS .'_'. lang('DIR') ?>/jquery-ui.css" class="adly"  rel="stylesheet" type="text/css"/>

<link href="<?= NEW_CSS_JS ?>/font-awesome.css" rel="stylesheet" type="text/css" />

        <script src="<?= NEW_CSS_JS .'_'. lang('DIR') ?>/jquery-1.9.1.js"></script>
        <script src="<?= NEW_CSS_JS .'_'. lang('DIR') ?>/jquery-ui.js"></script>
    <!--<script type='text/javascript' src='<?= NEW_JS ?>/plugins/jquery/jquery.1.9.js'></script>-->
        <link href="<?= NEW_CSS_JS . '_' . lang('DIR') ?>/validationEngine.jquery.css" rel="stylesheet" type="text/css" />
        <? $this->load->view("analytics") ?>
        <script type="text/javascript">
            $(document).ready(function() {
                var accordion_head = $('.accordion > li > a'),
                        accordion_body = $('.accordion li > .sub-menu');
                //accordion_head.first().addClass('active').next().slideDown('normal');
                accordion_head.eq(<?php if (session('side_menu_id')) {    echo session('side_menu_id');} else {    echo '0';} ?>).addClass('active').next().slideDown('normal');
                accordion_head.on('click', function(event) {
                    // Disable header links
                    event.preventDefault();
                    // Show and hide the tabs on click
                    if ($(this).attr('class') != 'active') {
                        accordion_body.slideUp('normal');
                        $(this).next().stop(true, true).slideToggle('normal');
                        accordion_head.removeClass('active');
                        $(this).addClass('active');
                    }
                });
            });
            jQuery(function() { // on DOM load
                menu1 = new sidetogglemenu({// initialize first menu example
                    id: 'togglemenu1',
                    pushcontent: true // <- No comma after last option!
                })

                menu1.toggle();
            })
        </script>
        <script type='text/javascript' src='<?= NEW_CSS_JS . '_' . lang('DIR') ?>/slide-menu.js'></script>
        <!-- accordion menu -->
        <link rel="stylesheet" href="<?= NEW_CSS_JS . '_' . lang('DIR') ?>/accordionmenu.css" type="text/css" media="screen" />
        <script type='text/javascript' src='<?= NEW_JS ?>/plugins/jquery/jquery-ui-1.10.1.custom.min.js'></script>
        <script type='text/javascript' src='<?= NEW_JS ?>/plugins/jquery/jquery-migrate-1.1.1.min.js'></script>
        <script type='text/javascript' src='<?= NEW_JS ?>/plugins/bootstrap/bootstrap.min.js'></script>
        <script type='text/javascript' src='<?= NEW_JS ?>/plugins/bootstrap/bootstrap-fileupload.min.js'></script>
        <script type='text/javascript' src='<?= NEW_JS ?>/plugins/pnotify/jquery.pnotify.min.js'></script>
        <script type='text/javascript' src='<?= NEW_JS ?>/plugins/datatables/jquery.dataTables.min.js'></script>
        <script type='text/javascript' src="<?= NEW_JS ?>/plugins/uniform/jquery.uniform.min.js"></script>
        <script type='text/javascript' src="<?= NEW_JS ?>/plugins/scroll/jquery.mCustomScrollbar.concat.min.js"></script>

        <!-- 
        <script type='text/javascript' src='<?= NEW_JS ?>/plugins_1.js'></script>
        -->
		
        <script type='text/javascript' src='<?= NEW_JS ?>/plugins.js'></script>
        <script type='text/javascript' src='<?= JS ?>/plugins/lang-ar.js'></script>
        <link rel="stylesheet" href="<?= NEW_CSS_JS . '_' . lang('DIR') ?>/wizard/formToWizard.css"/>
        <link rel="stylesheet" href="<?= NEW_JS ?>/scroll/jquery.mCustomScrollbar.css"/>
        <script language="javascript" type="text/javascript" src="<?= NEW_CSS_JS ?>/wizard/formToWizard.js"></script>
        <script src="<?= NEW_JS ?>/jquery.validationEngine-<?= name(true) ?>.js" type="text/javascript" charset="utf-8"></script>
        <script src="<?= NEW_JS ?>/jquery.validationEngine.js" type="text/javascript" charset="utf-8"></script>
        <script src="<?= NEW_JS ?>/jquery.equalheights.js" type="text/javascript" charset="utf-8"></script>
        <script type='text/javascript' src="<?= NEW_JS ?>/plugins/select/select2.min.js"></script>
        <link rel="stylesheet" href="<?= NEW_CSS_JS . '_' . lang('DIR') ?>/top-dropdown/css/styles.css" />
        <!--[if lt IE 9]>
          <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
        <!-- hide and show -->
        <script type="text/javascript">
            $(document).ready(function() {
                $("input:text, .chosen-container, select").addClass("input-huge");
                $(".slidingDiv").show();
                $(".show_hide").show();
                $('.show_hide').click(function() {
                    $(".slidingDiv").slideToggle();
                });
            });
        </script>
        <!-- multi-select -->
        <link href="<?= NEW_CSS_JS . '_' . lang('DIR') ?>/multiselect/multiselect.css" rel="stylesheet" type="text/css" media="screen" />
        <script src="<?= NEW_CSS_JS . '_' . lang('DIR') ?>/multiselect/jquery.multi-select.min.js"></script>
        <!-- sGallery -->
        <script type='text/javascript' src="<?= NEW_CSS_JS . '_' . lang('DIR') ?>/fancybox/jquery.fancybox.pack.js"></script>
        <link rel="stylesheet" href="<?= NEW_CSS_JS . '_' . lang('DIR') ?>/fancybox/jquery.fancybox.css" type="text/css" media="screen" />
        <!-- date and time picker -->


        <link rel="stylesheet" media="all" type="text/css" href="<?= NEW_CSS_JS .'_'. lang('DIR') ?>/datetimepicker/jquery-ui.css" class="adly" />

        <link rel="stylesheet" media="all" type="text/css" href="<?= NEW_CSS_JS .'_'. lang('DIR') ?>/datetimepicker/jquery-ui-timepicker-addon.css" />
        <script type="text/javascript" src="<?= NEW_CSS_JS .'_'. lang('DIR') ?>/datetimepicker/jquery-ui.min.js"></script>
        <script type="text/javascript" src="<?= NEW_CSS_JS .'_'. lang('DIR') ?>/datetimepicker/jquery-ui-sliderAccess.js"></script>
        <script type="text/javascript" src="<?= NEW_CSS_JS .'_'. lang('DIR') ?>/datetimepicker/jquery-ui-timepicker-addon.js"></script>
		
		<link href="<?= NEW_CSS_JS . '_' . lang('DIR') ?>/glyphicons_filetypes.css" rel="stylesheet" type="text/css" />
        <link href="<?= NEW_CSS_JS . '_' . lang('DIR') ?>/glyphicons_regular.css" rel="stylesheet" type="text/css" />
        <link href="<?= NEW_CSS_JS . '_' . lang('DIR') ?>/glyphicons_social.css" rel="stylesheet" type="text/css" />
       
       
	   <script type="text/javascript">
            $(function() {
                $('#tabs').tabs();
                $('.example-container > pre').each(function(i) {
                    eval($(this).text());
                });
            });
        </script>


        <link rel="stylesheet" href="<?= NEW_CSS_JS . '_' . lang('DIR') ?>/multi-select-chosen/chosen.css" />
        <script src="<?= NEW_CSS_JS . '_' . lang('DIR') ?>/multi-select-chosen/chosen.jquery.js" type="text/javascript"></script>

        <script language="javascript" type="text/javascript">
            $(function() {
                $(".checkbox").change(function() {
                    $(this).closest("tr").toggleClass("highlight", this.checked)
                })
            })
            function updateButton() {
                if ($('form .group1:checked').length == 0) {
                    $('form .group2')
                            .prop('disabled', true);
                } else {
                    $('form .group2').prop('disabled', false);
                }
            }
            $('form .group1').change(function() {
                updateButton();
            });
        </script>


        <!-- By Gouda -->
        <script type="text/javascript">
            function showDiv(toggle) {
                document.getElementById(toggle).style.display = 'block';
            }
            function logout() {
                input_box = confirm("<?= lang('global_are_you_sure_you_want_to_logout') ?>");
                if (input_box == true)
                {
                    self.parent.location.replace("<?= site_url($destination . '/logout') ?>")
                }
            }
            $(document).ready(function() {
                $(".slidingDiv").show();
                $(".show_hide").show();
                $('.show_hide').click(function() {
                    $(".slidingDiv").slideToggle();
                });
            });
            
        </script>
    </head>
    <body>
        {layout}
    </body>
</html>