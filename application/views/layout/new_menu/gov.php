<ul class="accordion">
    <li id="one" class="files">
        <a href="#one"><?= lang('menu_management_users_eas')?><i class="icon-cogs icon-2x pull-left"></i></a>
        <ul class="sub-menu">
            <li><a href="<?= site_url('gov/gov_users') ?>"><?= lang('menu_management_uos_users') ?></a></li>
            <li><a href="<?= site_url('usergroups') ?>"><?= lang('global_usergroups') ?></a></li>
        </ul>
    </li>

    <li id="two" class="cloud">
        <a href="#two"><?= lang('menu_uos_reports') ?><i class="icon-book icon-2x pull-left"></i></a>
        <ul class="sub-menu">
            <li><a href="<?= site_url('gov/dashboard') ?>"><?= lang('menu_uos_current_status') ?></a></li>
            <li><a href="<?= site_url('gov/arriving_report') ?>"><?= lang('menu_uos_arrival_report') ?></a></li>
            <li><a href="<?= site_url('gov/departing_report') ?>"><?= lang('menu_uos_departure_report') ?></a></li>
            <li><a href="<?= site_url('gov/all_movements') ?>"><?= lang('menu_uos_movements') ?></a></li>
            <li><a href="<?= site_url('gov/counts') ?>"><?= lang('counts_report') ?></a></li>
        </ul>
    </li>
</ul>