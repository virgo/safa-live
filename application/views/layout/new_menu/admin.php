<ul class="accordion">
    <? if(check('admins')
       or check('usergroups')
       or check('language_update')): ?>
    <li id="one" class="files">
        <a href="#one"><?= lang('menu_management_users') ?></a>
        <ul class="sub-menu">
            <? if(check('admins')): ?><li><a href="<?= site_url('admin/admins/index')?>"><?= lang('menu_management_users') ?></a></li><? endif ?>
            <? if(check('usergroups')): ?><li><a href="<?= site_url('usergroups') ?>"><?= lang('global_usergroups') ?></a></li><? endif ?>
            <? if(check('language_update')): ?><li><a href="<?= site_url('language_update') ?>"><?= lang('menu_management_language_update') ?></a></li><? endif ?>
        </ul>
    </li>
    <? endif ?>
    <? if(check('packages')
       or check('licenses')
       or check('admin/cities')
       or check('admin/ports')
       or check('admin/port_halls')
       or check('admin/tourismplaces')
       or check('hotels')
       or check('admin/hotels_levels')
       or check('admin/currencies')
       or check('admin/hotelattributes')
       or check('admin/roomattributes')
       or check('admin/vehicles')
       or check('admin/seasons')): ?>
    <li id="two" class="mail">
        <a href="#two"><?= lang('menu_main_data') ?></a>
        <ul class="sub-menu">
            <? if(check('packages')): ?><li><a href="<?= site_url('packages/index') ?>"><?= lang('global_packages') ?></a></li><? endif ?>
            <? if(check('licenses')): ?><li><a href="<?= site_url('licenses/index') ?>"><?= lang('global_ea_licenses') ?></a></li><? endif ?>
            <? if(check('admin/cities')): ?><li><a href="<?= site_url('admin/cities/index') ?>"><?= lang('menu_main_cities') ?></a></li><? endif ?>
            <? if(check('admin/ports')): ?><li><a href="<?= site_url('admin/ports/index') ?>"><?= lang('menu_main_ports') ?></a></li><? endif ?>
            <? if(check('admin/port_halls')): ?><li><a href="<?= site_url('admin/port_halls/index') ?>"><?= lang('menu_main_hole_airports') ?></a></li><? endif ?>
            <? if(check('admin/tourismplaces')): ?><li><a href="<?= site_url('admin/tourismplaces/index') ?>"><?= lang('menu_main_sights') ?></a></li><? endif ?>
            <? if(check('hotels')): ?><li><a href="<?= site_url('hotels/index') ?>"><?= lang('menu_main_hotels') ?></a></li><? endif ?>
            <? if(check('admin/hotels_levels')): ?><li><a href="<?= site_url('admin/hotels_levels/index') ?>"><?= lang('menu_main_hotels_levels') ?></a></li><? endif ?>
            <? if(check('admin/currencies')): ?><li><a href="<?= site_url('admin/currencies/index') ?>"><?= lang('menu_main_types_of_currencies') ?></a></li><? endif ?>
            <? if(check('admin/hotelattributes')): ?><li><a href="<?= site_url('admin/hotelattributes/index') ?>"><?= lang('menu_main_hotel_attributes') ?></a></li><? endif ?>
            <? if(check('admin/roomattributes')): ?><li><a href="<?= site_url('admin/roomattributes/index') ?>"><?= lang('menu_main_rooms_attributes') ?></a></li><? endif ?>
            <? if(check('admin/vehicles')): ?><li><a href="<?= site_url('admin/vehicles/index') ?>"><?= lang('menu_main_types_of_vehcles') ?></a></li><? endif ?>
            <? if(check('admin/seasons')): ?><li><a href="<?= site_url('admin/seasons/index') ?>"><?= lang('menu_main_seasons') ?></a></li><? endif ?>
            <? if(check('admin/hotels_polices')): ?><li><a href="<?= site_url('admin/hotels_polices/index') ?>"><?= lang('menu_hotels_polices') ?></a></li><? endif ?>
        </ul>
    </li>
    <? endif ?>
    <? if(check('admin/safa_tripstatus')): ?>
    <li id="three" class="cloud">
        <a href="#three"><?= lang('menu_technical_data') ?></a>
        <ul class="sub-menu">
            <? if(check('admin/safa_tripstatus')): ?><li><a href="<?= site_url('admin/safa_tripstatus/index') ?>"><?= lang('menu_techn_flight_cases') ?></a></li><? endif ?>
            <li><a href="<?= site_url('admin/uc/index') ?>"><?= lang('menu_techn_types_of_transport') ?></a></li>
            <li><a href="<?= site_url('admin/uc/index') ?>"><?= lang('menu_techn_types_of_internal_transport_clips') ?></a></li>
            <li><a href="<?= site_url('admin/uc/index') ?>"><?= lang('menu_techn_cases_of_transport_clips') ?></a></li>
            <li><a href="<?= site_url('admin/uc/index') ?>"><?= lang('menu_techn_users_checks_virgo') ?></a></li>
        </ul>
    </li>
    <? endif ?>
    <? if(check('admin/transporters')): ?>
    <li id="four" class="cloud">
        <a href="#four"><?= lang('menu_main_transport_companies') ?></a>
        <ul class="sub-menu">
            <? if(check('admin/transporters')): ?><li><a href="<?= site_url('admin/transporters/index') ?>"><?= lang('menu_transport_companies_info') ?></a></li><? endif ?>
        </ul>
    </li>
    <? endif ?>
    <? if(check('admin/safa_uos')
       or check('admin/uoprivileges')
       or check('admin/uo_usergroups')): ?>
    <li id="five" class="cloud">
        <a href="#five"><?= lang('menu_ksa_agent') ?></a>
        <ul class="sub-menu">
            <? if(check('admin/safa_uos')): ?><li><a href="<?= site_url('admin/safa_uos/index') ?>"><?= lang('menu_ksa_agent') ?></a></li><? endif ?>
            <? if(check('admin/uoprivileges')): ?><li><a href="<?= site_url('admin/uoprivileges/index') ?>"><?= lang('menu_ksa_agent_privileges') ?></a></li><? endif ?>
            <? if(check('admin/uo_usergroups')): ?><li><a href="<?= site_url('admin/uo_usergroups/index') ?>"><?= lang('menu_ksa_agent_usergroups') ?></a></li><? endif ?>
        </ul>
    </li>
    <? endif ?>
    <? if(check('admin/safa_eas')
       or check('admin/eaprivileges')
       or check('uc')): ?>
    <li id="six" class="cloud">
        <a href="#six"><?= lang('menu_external_agent') ?></a>
        <ul class="sub-menu">
            <? if(check('admin/safa_eas')): ?><li><a href="<?= site_url('admin/safa_eas/index') ?>"><?= lang('menu_external_agent') ?></a></li><? endif ?>
            <? if(check('admin/eaprivileges')): ?><li><a href="<?= site_url('admin/eaprivileges/index') ?>"><?= lang('menu_external_agent_privileges') ?></a></li><? endif ?>
            <li><a href="<?= site_url('admin/uc/index') ?>"><?= lang('menu_external_agent_usergroups') ?></a></li>
        </ul>
    </li>
    <? endif ?>
    <? if(check('safa_itos')
       or check('itoprivileges')
       or check('uc')): ?>
    <li id="seven" class="cloud">
        <a href="#seven"><?= lang('menu_safa_itos') ?></a>
        <ul class="sub-menu">
            <? if(check('admin/safa_itos')): ?><li><a href="<?= site_url('admin/safa_itos/index') ?>"><?= lang('menu_safa_itos') ?></a></li><? endif ?>
            <? if(check('admin/itoprivileges')): ?><li><a href="<?= site_url('admin/itoprivileges/index') ?>"><?= lang('menu_itos_privileges') ?></a></li><? endif ?>
            <li><a href="<?= site_url('admin/uc/index') ?>"><?= lang('menu_itos_usergroups') ?></a></li>
        </ul>
    </li>
    <? endif ?>
    <? if(check('admin/notifications')
        or check('admin/notifications_translation')): ?>
    <li id="seven" class="cloud">
        <a href="#seven"><?= lang('menu_notifications') ?></a>
        <ul class="sub-menu">
            <? if(check('admin/notifications')): ?><li><a href="<?= site_url('admin/notifications') ?>"><?= lang('menu_add_notifications_data') ?></a></li><? endif ?>
            <? if(check('admin/notifications_translation')): ?><li><a href="<?= site_url('admin/notifications_translation') ?>"><?= lang('menu_add_notifications_translation') ?></a></li><? endif ?>
        </ul>
    </li>
    <? endif ?>
    <? if(check('hotels')
       or check('admin/hotel_marketing_companies')
       or check('hotel_availability')): ?>
    <li id="nine" class="cloud">
        <a href="#nine"><?= lang('menu_hotels')?></a>
        <ul class="sub-menu">
            <? if(check('hotels')): ?><li><a href="<?= site_url('hotels/index')?>"><?= lang('menu_search_hotels')?></a></li><? endif ?>
            <? if(check('admin/hotel_marketing_companies')): ?><li><a href="<?= site_url('admin/hotel_marketing_companies')?>"><?= lang('menu_main_hotel_marketing_companies')?></a></li><? endif ?>
            <? if(check('hotel_availability')): ?><li><a href="<?= site_url('hotel_availability')?>"><?= lang('menu_hotel_availabilty')?></a></li><? endif ?>
            <? if(check('hotel_availability')): ?><li><a href="<?= site_url('hotel_availability/search')?>"><?= lang('menu_hotel_availabilty_search')?></a></li><? endif ?>
            <? if(check('hotel_availability')): ?><li><a href="<?= site_url('hotel_availability/calendar')?>"><?= lang('menu_hotel_availabilty_calendar')?></a></li><? endif ?>
            
            <? if(check('admin/hotel_exchange')): ?><li><a href="<?= site_url('admin/hotel_exchange/manage')?>"><?= lang('menu_hotel_transactions_exchange')?></a></li><? endif ?>
        </ul>
    </li>
    <? endif ?>
</ul>