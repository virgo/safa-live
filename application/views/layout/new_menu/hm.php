<ul class="accordion">
    <li id="one" class="files">
        <a href="#one"><?= lang('menu_management_users_eas')?><i class="icon-cogs icon-2x pull-left"></i></a>
        <ul class="sub-menu">
            <li><a href="<?= site_url('hm/gov_users') ?>"><?= lang('menu_management_uos_users') ?></a></li>
            <li><a href="<?= site_url('usergroups') ?>"><?= lang('global_usergroups') ?></a></li>
        </ul>
    </li>

    <? if(check('hotels')
    or check('hotels_reservation_orders')
    or check('hotels_availabilty_offers')
    or check('hotel_availability')): ?>
    <li id="seven" class="cloud">
        <a href="#seven"><?= lang('menu_hotels')?><i class="icon-building icon-2x pull-left"></i></a>
        <ul class="sub-menu">
            <? if(check('hotels')): ?><li><a href="<?= site_url('hotels/index')?>"><?= lang('menu_search_hotels')?></a></li><? endif ?>
            <? if(check('hotels_reservation_orders')): ?><li><a href="<?= site_url('hotels_reservation_orders')?>"><?= lang('menu_hotels_reservation_orders')?></a></li><? endif ?>
            <? if(check('hotels_reservation_orders')): ?><li><a href="<?= site_url('hotels_reservation_orders/incoming')?>"><?= lang('menu_incoming_hotels_reservation_orders')?></a></li><? endif ?>
            <? if(check('hotels_availabilty_offers')): ?><li><a href="<?= site_url('hotels_availabilty_offers')?>"><?= lang('menu_hotels_availabilty_offers')?></a></li><? endif ?>
            <? if(check('hotel_availability')): ?><li><a href="<?= site_url('hotel_availability')?>"><?= lang('menu_hotel_availabilty')?></a></li><? endif ?>
            <? if(check('hotel_availability')): ?><li><a href="<?= site_url('hotel_availability/search')?>"><?= lang('menu_hotel_availabilty_search')?></a></li><? endif ?>
            <? if(check('hotel_availability')): ?><li><a href="<?= site_url('hotel_availability/calendar')?>"><?= lang('menu_hotel_availabilty_calendar')?></a></li><? endif ?>
        </ul>
    </li>
    <? endif ?>
</ul>