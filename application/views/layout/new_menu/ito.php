<ul class="accordion">
    <? if(check('transporters') or check('usergroups') || check('safa_buses_brands')): ?>
    <li id="one" class="files">
        <a href="#one"><?= lang('menu_management_users_eas')?><i class="icon-cogs icon-2x pull-left"></i></a>
        <ul class="sub-menu">
            <? if(check('transporters')): ?><li><a href="<?= site_url('transporters') ?>"><?= lang('menu_transport_companies_drivers_buses') ?></a></li><? endif ?>
            <? if(check('safa_buses_brands')): ?><li><a href="<?= site_url('safa_buses_brands') ?>"><?= lang('menu_safa_buses_brands') ?></a></li><? endif ?>
            <? if(check('usergroups')): ?><li><a href="<?= site_url('usergroups') ?>"><?= lang('global_usergroups') ?></a></li><? endif ?>
        </ul>
    </li>
    <? endif ?>
    
    <? if(check('ito/dashboard')||check('ito/arriving_report_ito') || check('ito/departing_report_ito')||check('ito/all_movements_ito') ||check('ito/all_movementsv_ito')): ?>
    <li id="two" class="mail">
        <a href="#two"><?= lang('menu_reports')?><i class="icon-book icon-2x pull-left"></i></a>
        <ul class="sub-menu">
            <? if(check('ito/dashboard')): ?><li><a href="<?= site_url('ito/dashboard')?>"><?= lang('menu_ito_currently_report')?></a></li><? endif ?>
            
            
            <? if(check('ito/arriving_report_ito')): ?><li><a href="<?= site_url('ito/arriving_report_ito')?>"><?= lang('menu_ito_arriving_report')?></a></li><? endif ?>
            <? if(check('ito/departing_report_ito')): ?><li><a href="<?= site_url('ito/departing_report_ito')?>"><?= lang('menu_ito_departing_report')?></a></li><? endif ?>
            
            <? if(check('ito/all_movements_ito')): ?><li><a href="<?= site_url('ito/all_movements_ito')?>"><?= lang('menu_ito_movements')?></a></li>   <? endif ?>
            <? if(check('ito/all_movementsv_ito')): ?><li><a href="<?= site_url('ito/all_movementsv_ito')?>"><?= lang('menu_ito_movementsv') ?></a></li><? endif ?>
        </ul>
    </li>
    <? endif ?>
    <? if(check('ito/trip_internaltrip')): ?>
    <li id="three" class="cloud">
        <a href="#three"><?= lang('menu_internal_transportation')?><i class="icon-road icon-2x pull-left"></i></a>
        <ul class="sub-menu"> 
            <? if(check('ito/trip_internaltrip')): ?><li><a href="<?= site_url('ito/trip_internaltrip/index')?>"><?= lang('menu_internal_transportation_order')?></a></li><? endif ?>
            <? if(check('ito/trip_internaltrip')): ?><li><a href="<?= site_url('ito/trip_internaltrip/incoming')?>"><?= lang('menu_incoming_internal_transportation')?></a></li><? endif ?>
        </ul>
    </li>
    <? endif ?>
    
    <? if( check('safa_trip_internaltrips') || check('safa_trip_internaltrips/manage')): ?>
    <li id="six" class="cloud">
        <a href="#six"><?= lang('menu_safa_trip_internaltrips') ?><i class="icon-road icon-2x pull-left"></i></a>
        <ul class="sub-menu">

            <? if(check('safa_trip_internaltrips')): ?><li><a href="<?= site_url('safa_trip_internaltrips')?>"><?= lang('menu_safa_trip_internaltrips')?></a></li><? endif ?>
            <? if(check('safa_trip_internaltrips/manage')): ?><li><a href="<?= site_url('safa_trip_internaltrips/manage')?>"><?= lang('menu_internal_transportation_addfull')?></a></li><? endif ?>
           
        </ul>
    </li>
    <? endif ?>
    
    <? if(check('payments')): ?>
    <li id="nine" class="cloud">
        <a href="#ten"><?= lang('menu_accounting')?><i class="icon-desktop icon-2x pull-left"></i></a>
        <ul class="sub-menu">
            <? if(check('payments')): ?><li><a href="<?= site_url('payments') ?>"><?= lang('menu_payments') ?></a></li><? endif ?>
            <? if(check('payments')): ?><li><a href="<?= site_url('payments/report') ?>"><?= lang('menu_payments_report') ?></a></li><? endif ?>
    	</ul>
    </li>
    <? endif ?>
</ul>