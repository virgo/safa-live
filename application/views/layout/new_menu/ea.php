<ul class="accordion">
    <? if(check('machines')
       or check('ea/ea_users')
       or check('ea/ea_ports')
       or check('usergroups')
       or check('ea/supervisors')): ?>
    <li id="one" class="files">
        <a href="#one"><?= lang('menu_management_users_eas')?><i class="icon-cogs icon-2x pull-left"></i></a>
        <ul class="sub-menu">
            <? if(check('machines')): ?><li><a href="<?= site_url('machines')?>"><?= lang('global_machines')?></a></li><? endif ?>
            <? if(check('ea/ea_users')): ?><li><a href="<?= site_url('ea/ea_users')?>"><?= lang('menu_management_users')?></a></li><? endif ?>
            <? if(check('ea/ea_users')): ?><li><a href="<?= site_url('ea/ea_users/add')?>"><?= lang('menu_management_users_add')?></a></li><? endif ?>
            <? if(check('ea/ea_ports')): ?><li><a href="<?= site_url('ea/ea_ports')?>"><?= lang('menu_main_ports')?></a></li><? endif ?>
            <? if(check('usergroups')): ?><li><a href="<?= site_url('usergroups') ?>"><?= lang('global_usergroups') ?></a></li><? endif ?>
        </ul>
    </li>
    <? endif ?>
    <?  if(check('ea/trips')
       or check('trips_report')
       or check('flight_availabilities')): ?>
    <li id="three" class="">
        <a href="#three"><?= lang('menu_trips_eas')?><i class="icon-briefcase icon-2x pull-left"></i></a>
        <ul class="sub-menu">
        	<!-- 
            <li><a href="<?= site_url('ea/trips/form')?>"><?= lang('menu_trip_add')?></a></li>
            <li><a href="<?= site_url('ea/trips/index')?>"><?= lang('menu_trip_search')?></a></li>
             -->
        	
        	<? if(check('ea/passport_accommodation_rooms')): ?><li><a href="<?= site_url('ea/passport_accommodation_rooms')?>"><?= lang('passport_accommodation_rooms')?></a></li><? endif ?>
                <? if(check('ea/trips')): ?><li><a href="<?= site_url('ea/trips')?>"><?= lang('trips')?></a></li><? endif ?>
            <? if(check('trips_report')): ?><li><a href="<?= site_url('trips_report') ?>"><?= lang('menu_eas_trips_report') ?></a></li><? endif ?>
            <? if(check('flight_availabilities')): ?><li><a href="<?= site_url('flight_availabilities/')?>"><?= lang('menu_flight_availability')?></a></li><? endif ?>
        </ul>
    </li>
    <? endif  ?>
    <? if(check('ea/trip_internaltrip') ): ?>
    <li id="four" class="cloud">
        <a href="#four"><?= lang('menu_internal_transportation_eas')?><i class="icon-road icon-2x pull-left"></i></a>
        <ul class="sub-menu">
            <? if(check('ea/trip_internaltrip')): ?><li><a href="<?= site_url('ea/trip_internaltrip/add')?>"><?= lang('menu_internal_transportation_add')?></a></li><? endif ?>
            <? if(check('ea/trip_internaltrip')): ?><li><a href="<?= site_url('ea/trip_internaltrip')?>"><?= lang('menu_internal_transportation_search')?></a></li><? endif ?>
          
        </ul>
    </li>
    <? endif ?>
    
     <? if( check('safa_trip_internaltrips')): ?>
    <li id="four" class="cloud">
        <a href="#four"><?= lang('menu_safa_trip_internaltrips')?><i class="icon-road icon-2x pull-left"></i></a>
        <ul class="sub-menu">
            
            <? if(check('safa_trip_internaltrips')): ?><li><a href="<?= site_url('safa_trip_internaltrips')?>"><?= lang('menu_safa_trip_internaltrips')?></a></li><? endif ?>
           <? if(check('safa_trip_internaltrips')): ?><li><a href="<?= site_url('safa_trip_internaltrips/manage')?>"><?= lang('menu_internal_transportation_addfull')?></a></li><? endif ?>
           
        </ul>
    </li>
    <? endif ?>
    
    <? if(check('ea/all_movements')): ?>
    <li id="eight" class="cloud">
        <a href="#eight"><?= lang('menu_ea_processes_room')?><i class="icon-bar-chart icon-2x pull-left"></i></a>
        <ul class="sub-menu">
            <? if(check('ea/all_movements')): ?><li><a href="<?= site_url('ea/all_movements') ?>"><?= lang('menu_eas_movements') ?></a></li><? endif ?>
            <? if(check('ea/arriving_report')): ?><li><a href="<?= site_url('ea/arriving_report') ?>"><?= lang('menu_uos_arrival_report') ?></a></li><? endif ?>
            <? if(check('ea/departing_report')): ?><li><a href="<?= site_url('ea/departing_report') ?>"><?= lang('menu_uos_departure_report') ?></a></li><? endif ?>
        </ul>
    </li>
    <? endif ?>
    
    
    <?  if(check('hotels_reservation_orders')
       or check('hotels')
       or check('hotels_availabilty_offers')
       or check('hotel_availability')): ?>
    <li id="five" class="cloud">
        <a href="five"><?= lang('menu_hotels')?><i class="icon-building icon-2x pull-left"></i></a>
        <ul class="sub-menu">
            <? if(check('hotels/index')): ?><li><a href="<?= site_url('hotels/index')?>"><?= lang('menu_search_hotels')?></a></li><? endif ?>
            <? if(check('hotels_reservation_orders')): ?><li><a href="<?= site_url('hotels_reservation_orders')?>"><?= lang('menu_hotels_reservation_orders')?></a></li><? endif ?>
            <? if(check('hotels_reservation_orders')): ?><li><a href="<?= site_url('hotels_reservation_orders/incoming')?>"><?= lang('menu_incoming_hotels_reservation_orders')?></a></li><? endif ?>
            <? if(check('hotels_availabilty_offers')): ?><li><a href="<?= site_url('hotels_availabilty_offers')?>"><?= lang('menu_hotels_availabilty_offers')?></a></li><? endif ?>
            <? if(check('hotel_availability')): ?><li><a href="<?= site_url('hotel_availability')?>"><?= lang('menu_hotel_availabilty')?></a></li><? endif ?>
            <? if(check('hotel_availability')): ?><li><a href="<?= site_url('hotel_availability/search')?>"><?= lang('menu_hotel_availabilty_search')?></a></li><? endif ?>
            <? if(check('hotel_availability')): ?><li><a href="<?= site_url('hotel_availability/calendar')?>"><?= lang('menu_hotel_availabilty_calendar')?></a></li><? endif ?>
        </ul>
        
    </li>
    <? endif  ?>
    <? if(check('ea/contract_approving_phases')): ?>
    <li id="six" class="cloud">
        <a href="#six"><?= lang('menu_contracts_eas')?><i class="icon-file-text icon-2x pull-left"></i></a>
        <ul class="sub-menu">
            <? if(check('ea/contract_approving_phases')): ?><li><a href="<?= site_url('ea/contract_approving_phases/view_all')?>"><?= lang('menu_contracts_eas')?></a></li><? endif ?>
        </ul>
    </li>
    <? endif ?>
    
    <?  if(check('ea/read_xml')
       or check('ea/safa_groups')
       or check('ea/safa_umrahgroups')): ?>
    <li id="seven" class="cloud">
        <a href="#seven"><?= lang('menu_safa_umrah_and_mutamers')?><i class="icon-group icon-2x pull-left"></i></a>
        <ul class="sub-menu">
            <? if(check('ea/read_xml')): ?><li><a href="<?= site_url('ea/read_xml')?>"><?= lang('menu_mutamers_import')?></a></li><? endif ?>
            <? if(check('ea/safa_groups')): ?><li><a href="<?= site_url('ea/safa_groups')?>"><?= lang('menu_safa_groups')?></a></li><? endif ?>
            <? if(check('ea/safa_umrahgroups')): ?><li><a href="<?= site_url('ea/safa_umrahgroups')?>"><?= lang('menu_safa_umrah')?></a></li><? endif ?>
        </ul>
    </li>
    <? endif  ?>
    <?  if(check('safa_packages')): ?>
    <li id="eight" class="cloud">
        <a href="#eight"><?= lang('menu_packages')?><i class="icon-desktop icon-2x pull-left"></i></a>
        <ul class="sub-menu">
            <? if(check('safa_packages')): ?><li><a href="<?= site_url('safa_packages') ?>"><?= lang('menu_packages') ?></a></li><? endif ?>
            <? if(check('package_periods')): ?><li><a href="<?= site_url('package_periods') ?>"><?= lang('package_periods') ?></a></li><? endif ?>
            <? if(check('safa_packages')): ?><li><a href="<?= site_url('ea/packages') ?>"><?= lang('menu_packages_search') ?></a></li><? endif ?>
    	</ul>
    </li>
    <? endif  ?>
    <?  if(check('payments')): ?>
    <li id="nine" class="cloud">
        <a href="#ten"><?= lang('menu_accounting')?><i class="icon-money icon-2x pull-left"></i></a>
        <ul class="sub-menu">
            <? if(check('payments')): ?><li><a href="<?= site_url('payments') ?>"><?= lang('menu_payments') ?></a></li><? endif ?>
            <? if(check('payments')): ?><li><a href="<?= site_url('payments/report') ?>"><?= lang('menu_payments_report') ?></a></li><? endif ?>
    	</ul>
    </li>
    <? endif ?>
</ul>
