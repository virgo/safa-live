<ul class="accordion">
    <? if(check('uo/uo_users') or check('usergroups') or check('transporters')): ?>
    <li id="one" class="files">
        <a href="#one"><?= lang('menu_management_users_uos') ?><i class="icon-cogs icon-2x pull-left"></i></a>
        <ul class="sub-menu">
            <? if(check('usergroups')): ?><li><a href="<?= site_url('usergroups') ?>"><?= lang('global_usergroups') ?></a></li><? endif ?>
            <? if(check('uo/uo_users')): ?><li><a href="<?= site_url('uo/uo_users') ?>"><?= lang('menu_management_uos_users') ?></a></li><? endif ?>
            <? if(check('safa_uos_departments')): ?><li><a href="<?= site_url('admin/safa_uos_departments/index/'.session('uo_id')) ?>"><?= lang('global_safa_uos_departments') ?></a></li><? endif ?>
            <? if(check('transporters')): ?><li><a href="<?= site_url('transporters') ?>"><?= lang('menu_transport_companies_drivers_buses') ?></a></li><? endif ?>
            <? if(check('safa_buses_brands')): ?><li><a href="<?= site_url('safa_buses_brands') ?>"><?= lang('menu_safa_buses_brands') ?></a></li><? endif ?>
            
        </ul>
    </li>
    <? endif ?>
    <? if(check('uo/contracts')): ?>
    <li id="two" class="mail">
        <a href="#two"><?= lang('menu_contracts_uos') ?><i class="icon-briefcase icon-2x pull-left"></i></a>
        <ul class="sub-menu">
            <? if(check('uo/contracts')): ?><li><a href="<?= site_url('uo/contracts/add') ?>"><?= lang('menu_uos_contracts_add') ?></a></li><? endif ?>
            <? if(check('uo/contracts')): ?><li><a href="<?= site_url('uo/contracts') ?>"><?= lang('menu_uos_contracts_search') ?></a></li><? endif ?>
        </ul>
    </li>
    <? endif ?>
    <? if(check('uo/contract_phases') or check('uo/contracts_reports')): ?>
    <li id="tree" class="cloud">
        <a href="#tree"><?= lang('menu_contracts_eas')?><i class="icon-file-text icon-2x pull-left"></i></a>
        <ul class="sub-menu">
            <? if(check('uo/contract_phases')): ?><li><a href="<?= site_url('uo/contract_phases') ?>"><?= lang('menu_contract_phases') ?></a></li><? endif ?>
            <? if(check('uo/contracts_reports')): ?><li><a href="<?= site_url('uo/contracts_reports') ?>"><?= lang('menu_contracts_report') ?></a></li><? endif ?>
        </ul>
    </li>
    <? endif ?>
    <? if(check('uo/dashboard') or check('uo/arriving_report') or check('uo/departing_report') or check('trips_report')  ): ?>
    <li id="five" class="cloud">
        <a href="#five"><?= lang('menu_uos_reports') ?><i class="icon-book icon-2x pull-left"></i></a>
        <ul class="sub-menu">
            <? if(check('uo/dashboard')): ?><li><a href="<?= site_url('uo/dashboard') ?>"><?= lang('menu_uos_current_status') ?></a></li><? endif ?>
            <? if(check('uo/arriving_report')): ?><li><a href="<?= site_url('uo/arriving_report') ?>"><?= lang('menu_uos_arrival_report') ?></a></li><? endif ?>
            <? if(check('uo/departing_report')): ?><li><a href="<?= site_url('uo/departing_report') ?>"><?= lang('menu_uos_departure_report') ?></a></li><? endif ?>
            <? if(check('trips_report')): ?><li><a href="<?= site_url('trips_report') ?>"><?= lang('menu_uos_trips_report') ?></a></li><? endif ?>
            <? if(1==1): ?><li><a href="<?= site_url('uo/trips')?>"><?= lang('trips')?></a></li><? endif ?>
            
        </ul>
    </li>
    <? endif ?>
    <? if(check('uo/trip_internaltrip') ): ?>
    <li id="six" class="cloud">
        <a href="#six"><?= lang('menu_uos_internal_transportation') ?><i class="icon-road icon-2x pull-left"></i></a>
        <ul class="sub-menu">
<!--            <? if(check('uo/trip_internaltrip')): ?><li><a href="<?= site_url('uo/trip_internaltrip/addfull') ?>"><?= lang('menu_internal_transportation_addfull') ?></a></li><? endif ?>-->
            <? if(check('uo/trip_internaltrip')): ?><li><a href="<?= site_url('uo/trip_internaltrip/add') ?>"><?= lang('menu_internal_transportation_add') ?></a></li><? endif ?>
            <? if(check('uo/trip_internaltrip')): ?><li><a href="<?= site_url('uo/trip_internaltrip') ?>"><?= lang('menu_internal_transportation_search') ?></a></li><? endif ?>
            <!--
			<? if(check('uo/trip_internaltrip')): ?><li><a href="<?= site_url('uo/trip_internaltrip/incoming')?>"><?= lang('menu_incoming_internal_transportation')?></a></li><? endif ?>
            -->
            
           
        </ul>
    </li>
    <? endif ?>
    
    <? if( check('safa_trip_internaltrips')): ?>
    <li id="six" class="cloud">
        <a href="#six"><?= lang('menu_safa_trip_internaltrips') ?><i class="icon-road icon-2x pull-left"></i></a>
        <ul class="sub-menu">

            <? if(check('safa_trip_internaltrips')): ?><li><a href="<?= site_url('safa_trip_internaltrips')?>"><?= lang('menu_safa_trip_internaltrips')?></a></li><? endif ?>
            <? if(check('safa_trip_internaltrips')): ?><li><a href="<?= site_url('safa_trip_internaltrips/manage')?>"><?= lang('menu_internal_transportation_addfull')?></a></li><? endif ?>
           
        </ul>
    </li>
    <? endif ?>
    
    <? if( check('uo/trip_internaltrip')) { ?>
    <li id="six" class="cloud" > 
        <a href="#six"><?= lang('menu_transportation_management') ?><i class="icon-road icon-2x pull-left"></i></a>
        <ul class="sub-menu">
        	<!-- By Gouda, To update the transporter management internaltrips counts -->
            <script>
            setInterval(function(){get_safa_trip_internaltrips_counts()},60000);
            function get_safa_trip_internaltrips_counts() 
            {

		 		$.get("<?php echo site_url('safa_trip_internaltrips/get_counts_ajax'); ?>", function(data) {
				var counts_arr = JSON.parse(data);
				var trip_internaltrip_all = counts_arr[0]['all'];
				var trip_internaltrip_reserved_from_ea = counts_arr[0]['reserved_from_ea'];
				var trip_internaltrip_requested_from_ea = counts_arr[0]['requested_from_ea'];
				var trip_internaltrip_with_confirmation_number = counts_arr[0]['with_confirmation_number'];
				
	            $("#spn_trip_internaltrip_all_count").html('<a href="<?= site_url('uo/trip_internaltrip')?>"> <?= lang('menu_safa_trip_internaltrips_all')?>'+' - <span style="background-color: #C7B270; right: 125px;"> '+trip_internaltrip_all+' </span> </a>');
	            $("#spn_trip_internaltrip_reserved_from_ea_count").html('<a href="<?= site_url('uo/trip_internaltrip/reserved_from_ea')?>"><?= lang('menu_safa_trip_internaltrips_reserved_from_ea')?>'+' - <span style="background-color: #C7B270; right: 125px;"> '+trip_internaltrip_reserved_from_ea+'  </span> </a>');
	            $("#spn_trip_internaltrip_requested_from_ea_count").html('<a href="<?= site_url('uo/trip_internaltrip/requested_from_ea')?>"><?= lang('menu_safa_trip_internaltrips_requested_from_ea')?>'+' - <span style="background-color: #C7B270; right: 125px;"> '+trip_internaltrip_requested_from_ea+'  </span> </a>');
	            $("#spn_trip_internaltrip_with_confirmation_number_count").html('<a href="<?= site_url('uo/trip_internaltrip/with_confirmation_number')?>"><?= lang('menu_safa_trip_internaltrips_with_confirmation_number')?>'+' - <span style="background-color: #C7B270; right: 125px;"> '+trip_internaltrip_with_confirmation_number+'  </span> </a>');
	            
				});
			}
			</script>
    		
    		<? if(check('uo/trip_internaltrip')): ?><li id="spn_trip_internaltrip_all_count"> <a href="<?= site_url('uo/trip_internaltrip')?>"><?= lang('menu_safa_trip_internaltrips_all')?></a></li><? endif ?>
            <? if(check('uo/trip_internaltrip')): ?><li id="spn_trip_internaltrip_reserved_from_ea_count"><a href="<?= site_url('uo/trip_internaltrip/reserved_from_ea')?>"><?= lang('menu_safa_trip_internaltrips_reserved_from_ea')?></a></li><? endif ?>
            <? if(check('uo/trip_internaltrip')): ?><li id="spn_trip_internaltrip_requested_from_ea_count"><a href="<?= site_url('uo/trip_internaltrip/requested_from_ea')?>"><?= lang('menu_safa_trip_internaltrips_requested_from_ea')?></a></li><? endif ?>
           	<? if(check('uo/trip_internaltrip')): ?><li id="spn_trip_internaltrip_with_confirmation_number_count"><a href="<?= site_url('uo/trip_internaltrip/with_confirmation_number')?>"><?= lang('menu_safa_trip_internaltrips_with_confirmation_number')?></a></li><? endif ?>
           
        </ul>
    </li>
    
    <? } ?>
    
    <? if(check('hotels')
    or check('hotels_reservation_orders')
    or check('hotels_availabilty_offers')
    or check('hotel_availability')): ?>
    <li id="seven" class="cloud">
        <a href="#seven"><?= lang('menu_hotels')?><i class="icon-building icon-2x pull-left"></i></a>
        <ul class="sub-menu">
            <? if(check('hotels')): ?><li><a href="<?= site_url('hotels/index')?>"><?= lang('menu_search_hotels')?></a></li><? endif ?>
            <? if(check('hotels_reservation_orders')): ?><li><a href="<?= site_url('hotels_reservation_orders')?>"><?= lang('menu_hotels_reservation_orders')?></a></li><? endif ?>
            <? if(check('hotels_reservation_orders')): ?><li><a href="<?= site_url('hotels_reservation_orders/incoming')?>"><?= lang('menu_incoming_hotels_reservation_orders')?></a></li><? endif ?>
            <? if(check('hotels_availabilty_offers')): ?><li><a href="<?= site_url('hotels_availabilty_offers')?>"><?= lang('menu_hotels_availabilty_offers')?></a></li><? endif ?>
            <? if(check('hotel_availability')): ?><li><a href="<?= site_url('hotel_availability')?>"><?= lang('menu_hotel_availabilty')?></a></li><? endif ?>
            <? if(check('hotel_availability')): ?><li><a href="<?= site_url('hotel_availability/search')?>"><?= lang('menu_hotel_availabilty_search')?></a></li><? endif ?>
            <? if(check('hotel_availability')): ?><li><a href="<?= site_url('hotel_availability/calendar')?>"><?= lang('menu_hotel_availabilty_calendar')?></a></li><? endif ?>
        </ul>
    </li>
    <? endif ?>
    
    <? if(check('uo/all_movements')): ?>
    <li id="eight" class="cloud">
        <a href="#eight"><?= lang('menu_processes_room')?><i class="icon-bar-chart icon-2x pull-left"></i></a>
        <ul class="sub-menu">
            <? if(check('uo/all_movements')): ?><li><a href="<?= site_url('uo/all_movements') ?>"><?= lang('menu_uos_movements') ?></a></li><? endif ?>
    	</ul>
    </li>
    <? endif ?>
    
    <? if(check('safa_packages')): ?>
    <li id="nine" class="cloud">
        <a href="#nine"><?= lang('menu_packages')?><i class="icon-dropbox icon-2x pull-left"></i></a>
        <ul class="sub-menu">
        <? if(check('safa_packages')): ?><li><a href="<?= site_url('safa_packages') ?>"><?= lang('menu_packages') ?></a></li><? endif ?>
        <? if(check('package_periods')): ?><li><a href="<?= site_url('package_periods') ?>"><?= lang('package_periods') ?></a></li><? endif ?>
    	</ul>
    </li>
    <? endif ?>
    <? if(check('payments')): ?>
    <li id="nine" class="cloud">
        <a href="#ten"><?= lang('menu_accounting')?><i class="icon-money icon-2x pull-left"></i></a>
        <ul class="sub-menu">
        <? if(check('payments')): ?><li><a href="<?= site_url('payments') ?>"><?= lang('menu_payments') ?></a></li><? endif ?>
        <? if(check('payments')): ?><li><a href="<?= site_url('payments/report') ?>"><?= lang('menu_payments_report') ?></a></li><? endif ?>
    	</ul>
    </li>
    <? endif ?>
</ul>
