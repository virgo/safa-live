<!-- By Gouda, To Close menu On this screen -->
<script>
jQuery(function() { 
	menu1.toggle();
})
</script> 

<div class="widget">
    <div class="path-container Fright">
    	<div class="path-name Fright">
            <a href="<?php echo  site_url().'ito/dashboard'; 
            
            ?>"><?php echo  lang('global_system_management') ?></a>
        </div>
        <div class="path-arrow Fright"></div>
        <div class="path-name Fright">  <?= lang('menu_ito_departing_report') ?></div>
    </div>
            
</div>

<?= form_open() ?>
<div class="row-fluid">              
    <div class="widget">
          
        
        <div class="widget-header">
        <div class="widget-header-icon Fright"> <span class="icos-pencil2"></span> </div>
        <div class="widget-header-title Fright">
            <?php echo  lang('search') ?>
        </div>
    	</div>
                              
        <div class="block-fluid">
            <div class="row-form">
                <div class="span4">
                    <div class="span4"><?= lang('date') ?>:</div>
                    <div class="span8">
                        <input name="from_date" value="<?=$this->input->post('from_date');?>"  class="validate[required] datepicker from_date" style=" width:48%; margin:0 1%" placeholder="<?= lang('from')?>" type="text">
                        <input name="to_date" value="<?=$this->input->post('to_date');?>"  class="validate[required] datepicker to_date" style=" width:48%; margin:0 1%" placeholder="<?= lang('to')?>" type="text">
                        
                         <script>
                        $('.from_date').datepicker({
                            dateFormat: "yy-mm-dd",
                            controlType: 'select',
                            timeFormat: 'HH:mm'
                        });
                    </script>
                    <script>
                        $('.to_date').datepicker({
                            dateFormat: "yy-mm-dd",
                            controlType: 'select',
                            timeFormat: 'HH:mm'
                        });
                    </script>
                    </div>
                </div>
                <div class="span4">
                    <div class="span4"><?= lang('time') ?>:</div>
                    <div class="span8">
                        <input name="from_time"  value="<?=$this->input->post('from_time');?>"  id="basic_example_1" value="" style=" width:48%; margin:0 1%" placeholder="<?= lang('from')?>" type="text">
                        <input name="to_time" value="<?=$this->input->post('to_time');?>"   id="basic_example_2" value="" style=" width:48%; margin:0 1%" placeholder="<?= lang('to')?>" type="text">
                    </div>
                </div>
                <div class="span4">
                    <div class="span4"><?= lang('count') ?>:</div>
                    <div class="span8">
                        <input name="from_count"  value="<?=$this->input->post('from_count');?>"   style=" width:48%; margin:0 1%" placeholder="<?= lang('from')?>" type="text">
                        <input name="to_count"  value="<?=$this->input->post('to_count');?>"   style=" width:48%; margin:0 1%" placeholder="<?= lang('to')?>" type="text">
                    </div>
                </div>
            </div>

            <div class="row-form">
                <div class="span4">
                    <div class="span4"><?= lang('uo_companies') ?>:</div>
                    <div class="span8">
  
                        <?= form_dropdown('safa_uo_contract_id',  $uos_options , set_value('safa_uo_contract_id', '0')) ?>

                    </div>
                </div>
                <div class="span4">
                    <div class="span4"><?= lang('contract') ?>:</div>
                    <div class="span8">
<?= form_dropdown('safa_transporter_id', $uo_contracts_options,  set_value('safa_transporter_id', '0')) ?>
                    </div>
                </div>
                <div class="span4">
                    <div class="span4"><?= lang('nationality') ?>:</div>
                    <div class="span8">

<?= form_dropdown('nationality_id', ddgen('erp_countries', array('erp_country_id', name()), false, array(name(), 'asc')), set_value('nationality_id', '0')," name='s_example' class='select' style='width:100%;' ") ?>
                    </div>
                </div>
            </div>

            <div class="row-form">
                <div class="span4">
                    <div class="span4"><?= lang('port') ?>:</div>
                    <div class="span8">
<?= form_dropdown('erp_port_id', ddgen('erp_ports', array('erp_port_id', name()), array("country_code" => "SA"), array(name(), 'asc')), set_value('erp_port_id', '0')) ?>
                    </div>
                </div>
                <div class="span4">
                    <div class="span4"><?= lang('city') ?>:</div>
                    <div class="span8">

<?= form_dropdown('erp_city_id', ddgen('erp_cities', array('erp_city_id', name()), array('erp_country_id' => '966'), array(name(), 'asc')), set_value('erp_city_id', '0'),"id=city") ?>

                    </div>
                </div>
                <div class="span4">
                    <div class="span4"><?= lang('hotel') ?>:</div>
                    <div class="span8">

<?= form_dropdown('erp_hotel_id', $this->trips_reports_model->get_related_hotels_to_itos( session('ito_id') , $this->input->post('erp_city_id')), set_value('erp_hotel_id', '0'),"id=hotel") ?>

                    </div>
                </div>
            </div>


            <div class="row-form">
                <div class="span1"><?= lang('choose_cols') ?></div>
                <div class="span11">

                    <input name="cols[]" value="code" type="checkbox" <?= (in_array('code', $cols)) ? "checked=checked" : "" ?> > <?= lang('trip_id') ?> &nbsp;&nbsp; 
                    <input name="cols[]" value="departure_date" type="checkbox" <?= (in_array('departure_date', $cols)) ? "checked=checked" : "" ?> > <?= lang('dep_date') ?> &nbsp;&nbsp; 
                    <input name="cols[]" value="departure_time" type="checkbox" <?= (in_array('departure_time', $cols)) ? "checked=checked" : "" ?> > <?= lang('dep_time') ?> &nbsp;&nbsp; 
                    <input name="cols[]" value="contract" type="checkbox" <?= (in_array('contract', $cols)) ? "checked=checked" : "" ?> > <?= lang('contract') ?> &nbsp;&nbsp; 
                    <input name="cols[]" value="cn" type="checkbox" <?= (in_array('cn', $cols)) ? "checked=checked" : "" ?> > <?= lang('count') ?> &nbsp;&nbsp; 
                    <input name="cols[]" value="hotel" type="checkbox" <?= (in_array('hotel', $cols)) ? "checked=checked" : "" ?> > <?= lang('hotel') ?> &nbsp;&nbsp; 
                    <input name="cols[]" value="uo" type="checkbox" <?= (in_array('uo', $cols)) ? "checked=checked" : "" ?> > <?= lang('uo_companies') ?> &nbsp;&nbsp; 
                    <input name="cols[]" value="port" type="checkbox"  <?= (in_array('port', $cols)) ? "checked=checked" : "" ?> > <?= lang('port') ?> &nbsp;&nbsp; 
                    <input name="cols[]" value="nationality" type="checkbox" <?= (in_array('nationality', $cols)) ? "checked=checked" : "" ?> > <?= lang('nationality') ?> &nbsp;&nbsp; 
                    <input name="cols[]" value="supervisor" type="checkbox" c<?= (in_array('supervisor', $cols)) ? "checked=checked" : "" ?> > <?= lang('supervisor') ?> &nbsp;&nbsp; 


                </div>
            </div>

            <div class="toolbar bottom TAC">
                <button class="btn btn-primary"><?= lang('search') ?></button>
            </div>
        </div>
    </div>


    <div class="widget">
       
        
        <div class="widget-header">
        <div class="widget-header-icon Fright"> <span class="icos-pencil2"></span> </div>
        <div class="widget-header-title Fright">
            <?php echo  lang('departing_form_name') ?>
        </div>
        
        <div class="btn-group Fleft" style=" margin:1px 0 0 1px;color:#fff">
                <input type="submit" name="print_report" value="<?= lang('print') ?>" class="btn" />
                <input type="submit" name="excel_export" value="<?= lang('export') ?>" class="btn" />
            </div>
            
    	</div>
        
                     
        <div class="table-responsive">
            <table class="fsTable" cellpadding="0" cellspacing="0">
                <thead>
                    <tr>
                        
                        
                        <th><?= lang('trip') ?></th>
                        
                        <? if (in_array('code', $cols)): ?>
                            <th><?= lang('trip_id') ?></th>
                        <? endif; ?>

                        <? if (in_array('departure_date', $cols)): ?>
                            <th><?= lang('dep_date') ?></th>
                        <? endif; ?>

                        <? if (in_array('departure_time', $cols)): ?>
                            <th> <?= lang('dep_time') ?></th>
                        <? endif; ?>                                   

                        <? if (in_array('contract', $cols)): ?>
                            <th><?= lang('contract') ?></th>
                        <? endif; ?>

                        <? if (in_array('cn', $cols)): ?>
                            <th><?= lang('count') ?></th>
                        <? endif; ?>

                        <? if (in_array('hotel', $cols)): ?>
                            <th><?= lang('hotel') ?></th>
                        <? endif; ?>

                        <? if (in_array('uo', $cols)): ?>
                            <th><?= lang('uo_companies') ?></th>
                        <? endif; ?>

                        <? if (in_array('port', $cols)): ?>
                            <th><?= lang('port') ?></th>
                        <? endif; ?>

                        <? if (in_array('nationality', $cols)): ?>
                            <th><?= lang('nationality') ?></th>
                        <? endif; ?>

                        <? if (in_array('supervisor', $cols)): ?>
                            <th><?= lang('supervisor') ?></th>
                        <? endif; ?>                             


                    </tr>
                </thead>



                <tbody>                        


<? foreach ($trips_ds as $val): ?>
                        <tr>
                            <td><?php 
			if($val['trip_name']!='') {
				echo $val['trip_name'];
			} else {
				echo $val['trip_title'];
			}
			?></td>
                        <? if (in_array('code', $cols)): ?>
                                <td>

                                    <?= $val['flight_number'] ?>

                                </td>
                            <? endif; ?>

                            <? if (in_array('departure_date', $cols)): ?>
                                <td><?= get_date($val['departure_datetime']) ?></td>
                            <? endif; ?>

                            <? if (in_array('departure_time', $cols)): ?>
                                <td><?= get_time($val['departure_datetime']) ?></td>
                            <? endif; ?>

                            <? if (in_array('contract', $cols)): ?>
                                <td><?= $val['contract_name'] ?></td>
                            <? endif; ?>

                            <? if (in_array('cn', $cols)): ?>
                                <td>
                                    <a href="<?= site_url("trip_details/details/".$val['trip_id']) ?>" target="_blank">
                                        <?= $val['seats_count'] ?>
                                    </a>
                                </td>
                            <? endif; ?>

                            <? if (in_array('hotel', $cols)): ?>
                                <td><?= $val['hotel_name'] ?></td>
                            <? endif; ?>

                            <? if (in_array('uo', $cols)): ?>
                                <td><?= $val['uo_name'] ?></td>
                            <? endif; ?>

                            <? if (in_array('port', $cols)): ?>
                                <td><?= $val['start_port_name'] ?>
			(<?= $val['start_port_hall_name'] ?>)
			 - 
			<?= $val['end_port_name'] ?></td>
                            <? endif; ?>

                            <? if (in_array('nationality', $cols)): ?>
                                <td><?= $val['nationality'] ?></td>
                            <? endif; ?>

                            <? if (in_array('supervisor', $cols)): ?>
                               <td><?php 
			if($val['supervisor']!='') {
				echo $val['supervisor'];
			} else {
				echo $val['trip_supervisors'];
			}
			?></td>
                            <? endif; ?>

                        </tr>
<? endforeach; ?>

                </tbody>
            </table>      
        </div>
    </div>
<script>
$("#city").change(function(){    
    $.get('<?= site_url('ito/ajax/get_city_hotels') ?>/'+$(this).val(), function(data){
        $("#hotel").html(data);
    });
});
</script>
</div>
<?= form_close() ?>   
