<!-- By Gouda, To Close menu On this screen -->
<script>
jQuery(function() { 
	menu1.toggle();
})
</script> 

<div class="widget">
    <div class="path-container Fright">
    	<div class="path-name Fright">
            <a href="<?php echo  site_url().'ito/dashboard'; 
            
            ?>"><?php echo  lang('global_system_management') ?></a>
        </div>
        <div class="path-arrow Fright"></div>
        <div class="path-name Fright">  <?= lang('menu_ito_currently_report') ?></div>
    </div>
            
</div>


<!-- arraving trips section -->


<?= form_open('', array('method' => 'GET')) ?>

<!-- this field to hold values (today, twm) -->
<input type="hidden" name="departure" value="<?=$this->input->get('departure')?>" class="btn " />
<input type="hidden" name="arrival" value="<?=$this->input->get('arrival')?>" class="btn " />

<div class="widget">
      
    
    	<div class="widget-header">
        <div class="widget-header-icon Fright"> <span class="icos-pencil2"></span> </div>
        <div class="widget-header-title Fright">
            <?= lang('arriving_form_name').$this->arriving_pane ?>
        </div>
        
        <div class="btn-group fleftfar" style=" margin:1px 0 0 1px;">
            
            <? if ( count($arriving)) : ?>
            <input type="submit" name="print_arr_report" value="<?=lang('print')?>" class="btn Fleft" />
            <input type="submit" name="export_arr_report" value="<?=lang('export')?>" class="btn Fleft" />
            <? endif;?>
            
            <a href="<?= site_url("ito/dashboard/?arrival=today&departure=".$this->input->get('departure')) ?>" class="btn Fleft"><?= lang('today') ?></a>
            <a href="<?= site_url("ito/dashboard/?arrival=twm&departure=".$this->input->get('departure')) ?>"  class="btn Fleft" ><?= lang('twm') ?></a>                                                    
            
        </div>
    	</div>
    
                  
    <div class="table-responsive">
        <table class="fpTable" cellpadding="0" cellspacing="0">
            <thead>
                <tr>
                    
                    <th><?= lang('trip') ?></th>

                    <th><?= lang('trip_id') ?></th>

                    <th><?= lang('arr_date') ?></th>

                    <th><?= lang('arr_time') ?></th>

                    <th><?= lang('contract') ?></th>

                    <th><?= lang('count') ?></th>

                    <th><?= lang('hotel') ?></th>

                    <th><?= lang('trans_company') ?></th>

                    <th><?= lang('port') ?></th>

                    <th><?= lang('nationality') ?></th>

                    <th><?= lang('supervisor') ?></th>
                    
                    <th><?= lang('status') ?></th>                    


                </tr>
            </thead>
            <tbody>

                <? foreach ($arriving as $val): ?>
                    <tr>
                        
                  
                        
                        <td><?php 
						if($val['trip_name']!='') {
							echo $val['trip_name'];
						} else {
							echo $val['trip_title'];
						}
						?></td>
                        
                        <td><?= $val['flight_number'] ?></td>

                        <td><?= get_date($val['arrival_datetime']) ?></td>

                        <td><?= get_time($val['arrival_datetime']) ?></td>

                        <td><?= $val['contract_name'] ?></td>

                        <td><?= $val['travellers'] ?></td>

                        <td><?= $val['hotel_name'] ?></td>

                        <td><?= $val['operator_name'] ?></td>

                        <td><?= $val['start_port_name'] ?>
								 - 
								<?= $val['end_port_name'] ?>
								(<?= $val['end_port_hall_name'] ?>)</td>

                        <td><?= $val['nationality'] ?></td>

                         <td><?php 
			if($val['supervisor']!='') {
				echo $val['supervisor'];
			} else {
				echo $val['trip_supervisors'];
			}
			?></td>                    
      
                        
                        <td><?= $val['status'] ?></td>

                    </tr>
                <? endforeach; ?>

            </tbody>
        </table>      
    </div>
</div>

<!-- end arraving trips section -->

<!-- depatrual trip section  -->

<div class="widget">

    <div class="widget-header">
        <div class="widget-header-icon Fright"> <span class="icos-pencil2"></span> </div>
        <div class="widget-header-title Fright">
            <?= lang('departing_form_name').$this->arriving_pane ?>
        </div>
        
        <div class="btn-group fleftfar" style="margin:1px 0 0 1px;">
            
            <? if ( count($departing)) : ?>           
            <input type="submit" name="print_dep_report" value="<?=lang('print')?>" class="btn Fleft" />
            <input type="submit" name="export_dep_report" value="<?=lang('export')?>" class="btn Fleft" />            
            <? endif;?>
            
            <a href="<?= site_url("ito/dashboard/?arrival=".$this->input->get('arrival')."&departure=today") ?>" class="btn Fleft"><?= lang('today') ?></a>
            <a href="<?= site_url("ito/dashboard/?arrival=".$this->input->get('arrival')."&departure=twm") ?>"  class="btn Fleft" ><?= lang('twm') ?></a>                                                                
            
        </div>
    	</div>
    
       
    <div class="table-responsive">
        <table class="fpTable" cellpadding="0" cellspacing="0">
            <thead>
                <tr>
                    <th><?= lang('trip') ?></th>
                    
                    <th><?= lang('trip_id') ?></th>

                    <th><?= lang('dep_date') ?></th>

                    <th> <?= lang('dep_time') ?></th>

                    <th><?= lang('contract') ?></th>

                    <th><?= lang('count') ?></th>

                    <th><?= lang('hotel') ?></th>

                    <th><?= lang('trans_company') ?></th>

                    <th><?= lang('port') ?></th>

                    <th><?= lang('nationality') ?></th>

                    <th><?= lang('supervisor') ?></th>  
                    
                    <th><?= lang('status') ?></th>

                </tr>
            </thead>


            <tbody>                        


                <? foreach ($departing as $val): ?>
                    <tr>
                        
                         <td><?php 
			if($val['trip_name']!='') {
				echo $val['trip_name'];
			} else {
				echo $val['trip_title'];
			}
			?></td>
                        <td>
                            <?= $val['flight_number'] ?>
                        </td>

                        <td><?= get_date($val['departure_datetime']) ?></td>

                        <td><?= get_time($val['departure_datetime']) ?></td>

                        <td><?= $val['contract_name'] ?></td>

                        <td>                    
                        <?= $val['seats_count'] ?>
                        </td>

                        <td><?= $val['hotel_name'] ?></td>

                        <td><?= $val['operator_name'] ?></td>

                        <td><?= $val['start_port_name'] ?>
			(<?= $val['start_port_hall_name'] ?>)
			 - 
			<?= $val['end_port_name'] ?></td>

                        <td><?= $val['nationality'] ?></td>

                          <td><?php 
			if($val['supervisor']!='') {
				echo $val['supervisor'];
			} else {
				echo $val['trip_supervisors'];
			}
			?></td>
                        
                        <td><?= $val['status'] ?></td>

                    </tr>
                <? endforeach; ?>

            </tbody>
        </table>      
    </div>
</div>
<?= form_close() ?>
<!-- end departual trip section -->