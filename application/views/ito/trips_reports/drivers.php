
<style>
    .sep-fluid{
        border-bottom: 10px solid #fff;
    }

    .driver-titles{
        font-size: 13px;
        font-weight: bold;
        color: #602525;
    }   
</style>

<div class="row-fluid">


    <div class="widget">
        
        
        <div class="widget-header">
        <div class="widget-header-icon Fright"> <span class="icos-pencil2"></span> </div>
        <div class="widget-header-title Fright">
            <?php echo  lang('driver_title') ?>
        </div>
    	</div>
                               
        <div class="block-fluid">
            <?= form_open() ?>
           <? // for ($i = 1; $i <11; $i++) { ?>
            <?  if (!empty($drivers) && count($drivers) >= 1 && is_array($drivers)) { ?> 
            <?// print_r($drivers);die ?>
                 <? $i = 0; ?>
            <?$a=0;?>
            <?$c=1;?>
                <?  foreach ($drivers as $driver) {
//                      foreach ($driver as $key=>$value){?>    
            <div class="sep-fluid" id="reset_<?=$c?>">
                        <div class="row-form TAC"><span class="driver-titles"> <?= lang('driver_info') ?></span></div>
                        <div class="row-form">
                            <div class="span6">
                                <div class="span4"> <?= lang('driver_name') ?>:</div>
                                <div class="span8">
                                    <?= form_input("driver".$c."[0]", set_value("driver".$c."[0]",$driver[$i]), " class='alpha'") ?>
                                    <?= form_error("driver".$c."[0]", '<div class="bottom" style="color:red" >', '</div>'); ?>
                                    <?$i++;?>
                                </div>
                            </div>
                            <div class="span6">
                                <div class="span4"> <?= lang('driver_phone') ?>:</div>
                                <div class="span8">
                                    <?= form_input("driver".$c."[1]", set_value("driver".$c."[1]", $driver[$i]),  " class='numeric'") ?>
                                    <?= form_error("driver".$c."[1]", '<div class="bottom" style="color:red" >', '</div>'); ?>
                                    <?$i=0;?>
                                </div>
                            </div>
                        </div>
                        <div class="row-form">
                            <div class="span6">
                                <div class="span4"> <?= lang('driver_vehicle_number') ?>:</div>
                                <div class="span8">
                                    <?= form_input("car".$c."[0]", set_value("car".$c."[0]", $vehicle[$a][$i]), " class='alpha_numeric'") ?>
                                    <?= form_error("car".$c."[0]", '<div class="bottom" style="color:red" >', '</div>'); ?>
                                    <?$i++;?>
                                </div>
                            </div>
                            <div class="span6">
                                <div class="span4"> <?= lang('driver_vehicle_type') ?>:</div>
                                <div class="span8">
                                    <?//= form_input("car".$c."[1]", set_value("car".$c."[1]", $vehicle[$a][$i]), " class='alpha_numeric'") ?>
                                    <?//= form_error("car".$c."[1]", '<div class="bottom" style="color:red" >', '</div>'); ?>
                                    <?= form_dropdown("car".$c."[1]", ddgen('safa_vehicles', array('safa_vehicle_id', name())), set_value("car".$c."[1]",$vehicle[$a][$i]), "   id='car_type_$c' style='width:100%;' ") ?>
                                    <?= form_error("car".$c."[1]", '<div class="bottom" style="color:red" >', '</div>'); ?>
                                    <?$i=0;?>
                                </div>
                            </div>

                        </div>
                        <div class="toolbar bottom TAC">
                            <input  class="btn btn-primary reset" type="button"   id="<?=$c?>" value="<?= lang('global_reset') ?>"/>
                        </div>
                        
                    </div>
           <?$c++;?>
            <?$a++;?>
<?}?>
            <?//php $i++; ?>
                <? // } ?>
            <?  $w = sizeof($drivers)+1;     ?>
           <? for ($d = 1; $d <11-sizeof($drivers); $d++) { ?>
               
                    <div class="sep-fluid"  id="reset_<?=$c?>">
                        <div class="row-form TAC"><span class="driver-titles"> <?= lang('driver_info') ?></span></div>
                        <div class="row-form">
                            <div class="span6">
                                <div class="span4"> <?= lang('driver_name') ?>:</div>
                                <div class="span8">
                                    <?= form_input("driver".$w."[0]", set_value("driver".$w."[0]"), " class='alpha'") ?>
                                    <?= form_error("driver".$w."[0]", '<div class="bottom" style="color:red" >', '</div>'); ?>
                                </div>
                            </div>
                            <div class="span6">
                                <div class="span4"> <?= lang('driver_phone') ?>:</div>
                                <div class="span8">
                                    <?= form_input("driver".$w."[1]", set_value("driver".$w."[1]"), " class='numeric'") ?>
                                    <?= form_error("driver".$w."[1]", '<div class="bottom" style="color:red" >', '</div>'); ?>
                                </div>
                            </div>
                        </div>
                        <div class="row-form">
                            <div class="span6">
                                <div class="span4"> <?= lang('driver_vehicle_number') ?>:</div>
                                <div class="span8">
                                    <?= form_input("car".$w."[0]", set_value("car".$w."[0]"), " class='alpha_numeric'") ?>
                                    <?= form_error("car".$w."[0]", '<div class="bottom" style="color:red" >', '</div>'); ?>
                                </div>
                            </div>
                            <div class="span6">
                                <div class="span4"> <?= lang('driver_vehicle_type') ?>:</div>
                                <div class="span8">
                                    <?= form_dropdown("car".$w."[1]", ddgen('safa_vehicles', array('safa_vehicle_id', name())), set_value("car".$w."[1]"), "  id='car_type_$c'  style='width:100%;' ") ?>
                                    <?= form_error("car".$w."[1]", '<div class="bottom" style="color:red" >', '</div>'); ?>
                                </div>
                            </div>

                        </div>
                        <div class="toolbar bottom TAC">
                            <input  class="btn btn-primary reset" type="button"   id="<?=$c?>" value="<?= lang('global_reset') ?>"/>
                        </div>
                    </div>
            <?$w++;?>
            <?$c++;?>
                <? } ?>
            <?  } else {
                ?>
            <?$c=1;?>
                <? for ($i = 1; $i <11; $i++) { ?>
                    <div class="sep-fluid"  id="reset_<?=$c?>">
                        <div class="row-form TAC"><span class="driver-titles"> <?= lang('driver_info') ?></span></div>
                        <div class="row-form">
                            <div class="span6">
                                <div class="span4"> <?= lang('driver_name') ?>:</div>
                                <div class="span8">
                                    <?= form_input("driver".$i."[0]", set_value("driver".$i."[0]"), " class='alpha'") ?>
                                    <?= form_error("driver".$i."[0]", '<div class="bottom" style="color:red" >', '</div>'); ?>
                                </div>
                            </div>
                            <div class="span6">
                                <div class="span4"> <?= lang('driver_phone') ?>:</div>
                                <div class="span8">
                                    <?= form_input("driver".$i."[1]", set_value("driver".$i."[1]"), " class='numeric'") ?>
                                    <?= form_error("driver".$i."[1]", '<div class="bottom" style="color:red" >', '</div>'); ?>
                                </div>
                            </div>
                        </div>
                        <div class="row-form">
                            <div class="span6">
                                <div class="span4"> <?= lang('driver_vehicle_number') ?>:</div>
                                <div class="span8">
                                    <?= form_input("car".$i."[0]", set_value("car".$i."[0]"), " class='alpha_numeric'") ?>
                                    <?= form_error("car".$i."[0]", '<div class="bottom" style="color:red" >', '</div>'); ?>
                                </div>
                            </div>
                            <div class="span6">
                                <div class="span4"> <?= lang('driver_vehicle_type') ?>:</div>
                                <div class="span8">
                                    <?= form_dropdown("car".$i."[1]", ddgen('safa_vehicles', array('safa_vehicle_id', name())), set_value("car".$i."[1]"), "  id='car_type_$c'  style='width:100%;' ") ?>
                                    <?= form_error("car".$i."[1]", '<div class="bottom" style="color:red" >', '</div>'); ?>
                                </div>
                            </div>

                        </div>
                        <div class="toolbar bottom TAC">
                            <input  class="btn btn-primary reset" type="button"   id="<?=$c?>" value="<?= lang('global_reset') ?>"/>
                        </div>
                    </div>
            <?$c++;?>
                <? } ?>
            <?  } ?>


            <div class="toolbar bottom TAC"><input type="submit" name="submit" value="<?= lang('global_submit') ?>" class="btn btn-primary"> &nbsp;
                <?if($this->uri->segment('2')=='all_movements_ito'):?>
                  <input  type ="button" value="<?= lang('global_back') ?>"class="btn btn-primary" onclick="window.location = '<?= site_url('ito/all_movements_ito') ?>'">
                <? elseif ($this->uri->segment('2')=='all_movementsv_ito'):?>
                  <input  type ="button" value="<?= lang('global_back') ?>"class="btn btn-primary" onclick="window.location = '<?= site_url('ito/all_movementsv_ito') ?>'">
                <?  endif;?>
            </div>
            <?= form_close() ?>     
        </div>
    </div>
</div>
<script>
    $(document).ready(function(){
      $(".reset").click(function() {
         var id= $(this).attr('id');
         $('#reset_'+id).find("input[type=text], textarea").val("");
         $('#car_type_'+id).val(0);
      });
   });
        

</script>