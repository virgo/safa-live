<!-- By Gouda, To Close menu On this screen -->
<script>
jQuery(function() { 
	menu1.toggle();
})
</script> 

<div class="widget">
    <div class="path-container Fright">
    	<div class="path-name Fright">
            <a href="<?php echo  site_url().'ito/dashboard'; 
            
            ?>"><?php echo  lang('global_system_management') ?></a>
        </div>
        <div class="path-arrow Fright"></div>
        <div class="path-name Fright">  <?= lang('menu_ito_movementsv') ?></div>
    </div>
            
</div>

<?= form_open() ?>

<div class="row-fluid">              
    <div class="widget">
        <div class="widget-header">
        <div class="widget-header-icon Fright"> <span class="icos-pencil2"></span> </div>
        <div class="widget-header-title Fright">
            <?php echo  lang('search') ?>
        </div>
    	</div>
    	                       
        <div class="block-fluid">
            <div class="row-form">
                <div class="span4">
                    <div class="span4"><?= lang('date') ?>:</div>
                    <div class="span8">
                        <input name="from_date"  value="<?= ($this->input->post('from_date')) ? $this->input->post('from_date') : date("Y-m-d"); ?>"  class="validate[required] datepicker from_date" style=" width:48%; margin:0 1%" placeholder="<?= lang('from') ?>" type="text">
                        <input name="to_date"  value="<?= ($this->input->post('to_date'))?$this->input->post('to_date'):date("Y-m-d"); ?>"  class="validate[required] datepicker to_date" style=" width:48%; margin:0 1%" placeholder="<?= lang('to') ?>" type="text">
                        
                         <script>
                        $('.from_date').datepicker({
                            dateFormat: "yy-mm-dd",
                            controlType: 'select',
                            timeFormat: 'HH:mm'
                        });
                    </script>
                    <script>
                        $('.to_date').datepicker({
                            dateFormat: "yy-mm-dd",
                            controlType: 'select',
                            timeFormat: 'HH:mm'
                        });
                    </script>
                    </div>
                </div>
                <div class="span4">
                    <div class="span4"><?= lang('time') ?>:</div>
                    <div class="span8">
                        <input name="from_time"  value="<?= $this->input->post('from_time'); ?>"  id="basic_example_1" value="" style=" width:48%; margin:0 1%" placeholder="<?= lang('from') ?>" type="text">
                        <input name="to_time"  value="<?= $this->input->post('to_time'); ?>"  id="basic_example_2" value="" style=" width:48%; margin:0 1%" placeholder="<?= lang('to') ?>" type="text">
                    </div>
                </div>
                <div class="span4">
                    <div class="span4"><?= lang('count') ?>:</div>
                    <div class="span8">

                        <input name="from_count"  value="<?= $this->input->post('from_count'); ?>"  style=" width:48%; margin:0 1%" placeholder="<?= lang('from') ?>" type="text">
                        <input name="to_count"  value="<?= $this->input->post('to_count'); ?>"  style=" width:48%; margin:0 1%" placeholder="<?= lang('to') ?>" type="text">

                    </div>
                </div>
            </div>

            <div class="row-form">
                <div class="span4">
                    <div class="span4"><?= lang('uo_companies') ?>:</div>
                    <div class="span8">

                        <?= form_dropdown('safa_uo_id', $uos_options, set_value('safa_uo_id', '0'),"style='width:100%'") ?>

                    </div>
                </div>
                <div class="span4">
                    <div class="span4"><?= lang('segment_type') ?>:</div>
                    <div class="span8">
                        <?= form_dropdown('safa_internalsegmenttype_id', ddgen('safa_internalsegmenttypes', array('safa_internalsegmenttype_id', name())), set_value('safa_internalsegmenttype_id', '0')) ?>
                    </div>
                </div>
                <div class="span4">
                    <div class="span4"><?= lang('operation_order_id') ?>:</div>
                    <div class="span8">
                        <input type="text" name="operator_reference" value="<?= $this->input->post('operator_reference') ?>" class="" />
                    </div>
                </div>
            </div>

            <div class="row-form">
                <div class="span4">
                    <div class="span4"><?= lang('contract') ?>:</div>
                    <div class="span8">

                        <?= form_dropdown('safa_uo_contract_id', $uo_contracts_options, set_value('safa_uo_contract_id', '0'),"style='width:100%'") ?>

                    </div>
                </div>
                <div class="span4">
                    <div class="span4"><?= lang('from_city') ?>:</div>
                    <div class="span8">
                        <?= form_dropdown('from_city', ddgen('erp_cities', array('erp_city_id', name()), array('erp_country_id' => '966'), array(name(), 'asc')), set_value('from_city', '0'), "id=from_city") ?>
                    </div>
                </div>
                <div class="span4">
                    <div class="span4"><?= lang('from_hotel') ?>:</div>                    
                    <div class="span8">
                        <?= form_dropdown('from_erp_hotel_id', $this->trips_reports_model->get_related_hotels_to_itos(session('ito_id'), $this->input->post('from_city')), set_value('from_erp_hotel_id', '0'), "id=from_hotel") ?>
                    </div>
                </div>
            </div>

            <div class="row-form">
                <div class="span4">
                    <div class="span4"><?= lang('port') ?>:</div>
                    <div class="span8">
                        <?= form_dropdown('erp_port_id', ddgen('erp_ports', array('erp_port_id', name()), array("country_code" => "SA"), array(name(), 'asc')), set_value('erp_port_id', '0')) ?>
                    </div>
                </div>
                <div class="span4">
                    <div class="span4"><?= lang('to_city') ?>:</div>
                    <div class="span8">

                        <?= form_dropdown('to_city', ddgen('erp_cities', array('erp_city_id', name()), array('erp_country_id' => '966'), array(name(), 'asc')), set_value('to_city', '0'), "id=to_city") ?>

                    </div>
                </div>
                <div class="span4">
                    <div class="span4"><?= lang('to_hotel') ?>:</div>
                    <div class="span8">

                        <?= form_dropdown('to_erp_hotel_id', $this->trips_reports_model->get_related_hotels_to_itos(session('ito_id'), $this->input->post('to_city')), set_value('to_erp_hotel_id', '0'), "id=to_hotel") ?>

                    </div>
                </div>
            </div>


            <div class="row-form">
                <div class="span1"><?= lang('choose_cols') ?></div>
                <div class="span11">
                    <input name="cols[]" value="contract" type="checkbox" <?= (in_array('contract', $cols)) ? "checked=checked" : "" ?> > <?= lang('uo_companies') ?> &nbsp;&nbsp; 
                    <input name="cols[]" value="operator" type="checkbox" <?= (in_array('operator', $cols)) ? "checked=checked" : "" ?> > <?= lang('contract') ?> &nbsp;&nbsp; 
                    <input name="cols[]" value="operation_order_id" type="checkbox" <?= (in_array('operation_order_id', $cols)) ? "checked=checked" : "" ?> > <?= lang('operation_order_id') ?> &nbsp;&nbsp; 
                    <input name="cols[]" value="segment_type" type="checkbox" <?= (in_array('segment_type', $cols)) ? "checked=checked" : "" ?> > <?= lang('segment_type') ?> &nbsp;&nbsp; 
                    <input name="cols[]" value="starting" type="checkbox" <?= (in_array('starting', $cols)) ? "checked=checked" : "" ?> > <?= lang('starting') ?> &nbsp;&nbsp; 
                    <!-- <input name="cols[]" value="ending" type="checkbox" <?= (in_array('ending', $cols)) ? "checked=checked" : "" ?> > <?= lang('ending') ?> &nbsp;&nbsp; -->
                    <input name="cols[]" value="from" type="checkbox" <?= (in_array('from', $cols)) ? "checked=checked" : "" ?> > <?= lang('from') ?> &nbsp;&nbsp; 
                    <input name="cols[]" value="to" type="checkbox" <?= (in_array('to', $cols)) ? "checked=checked" : "" ?> > <?= lang('to') ?> &nbsp;&nbsp;                     
                    <input name="cols[]" value="seats" type="checkbox" <?= (in_array('seats', $cols)) ? "checked=checked" : "" ?> > <?= lang('seats') ?> &nbsp;&nbsp; 
                </div>
            </div>

            <div class="toolbar bottom TAC">
                <button class="btn btn-primary"><?= lang('search') ?></button>
            </div>
        </div>
    </div>


    <div class="widget">
        <div class="head dark">
           
            
            
            <div class="widget-header">
        <div class="widget-header-icon Fright"> <span class="icos-pencil2"></span> </div>
        <div class="widget-header-title Fright">
            <?php echo  lang('all_movements') ?>
        </div>
        
        <div class="btn-group Fleft" style=" margin:1px 0 0 1px;color:#fff">
                <input type="submit" name="print_report" value="<?= lang('print') ?>" class="btn" />
                <input type="submit" name="excel_export" value="<?= lang('export') ?>" class="btn" />
            </div>
            
    	</div>
                            
            <div class="table-responsive">
                <table class="fsTable" cellpadding="0" cellspacing="0">
                    <thead>
                        <tr>
                            <th><?= lang('trip') ?></th>

                            <? if (in_array('contract', $cols)): ?>
                                <th><?= lang('uo_companies') ?></th>
                            <? endif; ?>

                            <? if (in_array('operator', $cols)): ?>
                                <th> <?= lang('contract') ?></th>
                            <? endif; ?>                                   

                            <? if (in_array('operation_order_id', $cols)): ?>
                                <th><?= lang('operation_order_id') ?></th>
                            <? endif; ?>

                            <? if (in_array('segment_type', $cols)): ?>
                                <th><?= lang('segment_type') ?></th>
                            <? endif; ?>


                            <? if (in_array('from', $cols)): ?>
                                <th><?= lang('from') ?></th>
                            <? endif; ?>  


                            <? if (in_array('to', $cols)): ?>
                                <th><?= lang('to') ?></th>
                            <? endif; ?>


                            <? if (in_array('starting', $cols)): ?>
                                <th><?= lang('starting') ?></th>
                            <? endif; ?>

                            <!--
                            <? if (in_array('ending', $cols)): ?>
                                            <th><?= lang('ending') ?></th>
                            <? endif; ?>
                            -->

                            <? if (in_array('seats', $cols)): ?>
                                <th><?= lang('seats') ?></th>
                            <? endif; ?> 
                            <?php if (PHASE > 1): ?>
                                <th><?= lang('drivers') ?></th>
                            <? endif; ?>

                        </tr>
                    </thead>

                    <tbody>             


                        <? foreach ($trips_ds as $val): ?>
                            <tr>
                                <td><?= $val['trip_name'] ?></td>

                                <? if (in_array('contract', $cols)): ?>
                                    <td><?= $val['uo_name'] ?></td>
                                <? endif; ?>

                                <? if (in_array('operator', $cols)): ?>
                                    <td><?= $val['contract_name'] ?></td>
                                <? endif; ?>

                                <? if (in_array('operation_order_id', $cols)): ?>
                                    <td>
                                        <a href=<?= site_url("trip_details/details") ?><?= "/" . $val['trip_id'] ?>#tabs-3" target="_blank">
                                            <?= $val['operator_reference'] ?>
                                        </a>
                                    </td>
                                <? endif; ?>

                                <? if (in_array('segment_type', $cols)): ?>
                                    <td>
                                        <?
                                        switch ($val['safa_internalsegmenttype_id']):
                                            
                                            case 1:
                                                    $segment_name = "arr.png";
                                                break;

                                            case 2: 
                                                    $segment_name = "dep.png";
                                                break;

                                            case 3: 
                                                    $segment_name = "place.png";
                                                break;

                                            case 4:
                                                    $segment_name = "mov.png";
                                                break;

                                        endswitch;
                                        ?>
                                        <img src="<?= IMAGES2."/".$segment_name ?>" width="30" >
                                        <!--
                                        <?= $val['segment_name'] . $val['segment_id'] ?>
                                        -->
                                    </td>
                                <? endif; ?>


                                <? if (in_array('from', $cols)): ?>
                                    <td><?
                                        switch ($val['safa_internalsegmenttype_id']):

                                            case 1:
                                                echo(lang('port').': '.element($val['erp_port_id'], $ports));
                                                break;

                                            case 2: echo("<font class=city_color>" . $val['start_city_name'] . "</font><br>");
                                                echo(element($val['erp_start_hotel_id'], $hotels));
                                                break;

                                            case 3: echo("<font class=city_color>" . $val['start_city_name'] . "</font><br>");
                                                echo(element($val['erp_start_hotel_id'], $hotels));
                                                break;

                                            case 4:
                                                echo("<font class=city_color>" . $val['start_city_name'] . "</font><br>");
                                                echo(element($val['erp_start_hotel_id'], $hotels));
                                                break;

                                        endswitch;
                                        ?>
                                    </td>
                                <? endif; ?>                                


                                <? if (in_array('to', $cols)): ?>
                                    <td><?
                                        switch ($val['safa_internalsegmenttype_id']):

                                            case 1:
                                                echo("<font class=city_color>" . $val['end_city_name'] . "</font><br>");
                                                echo(element($val['erp_end_hotel_id'], $hotels));
                                                break;

                                            case 2:
                                                echo(lang('port').': '.element($val['erp_port_id'], $ports));
                                                break;

                                            case 3:
                                                echo(element($val['safa_tourism_place_id'], $places));
                                                break;

                                            case 4:
                                                echo("<font class=city_color>" . $val['end_city_name'] . "</font><br>");
                                                echo(element($val['erp_end_hotel_id'], $hotels));
                                                break;

                                        endswitch;
                                        ?>                                
                                    </td>
                                <? endif; ?>


                                <? if (in_array('starting', $cols)): ?>
                                    <td><span style="direction: ltr"><?= get_date($val['start_datetime']) . " " . get_time($val['start_datetime']) ?></span></td>
                                <? endif; ?>


                                <!--
                                <? if (in_array('ending', $cols)): ?>
                                            <td>
                                    <? if ($this->trips_reports_model->get_endtime($val['end_datetime']) <= '02:00:00' && $this->trips_reports_model->get_endtime($val['end_datetime']) >= '00:00:00'): ?>
                                        <? //if(((strtotime($val['end_datetime'])- time())/(60*60))<=2 && ((strtotime($val['end_datetime'])-  strtotime(date('Y-m-d H:i')))/(60*60))>=0): ?>
                                                            <span class="label"style="direction: ltr;background-color:orange;"><?= get_date($val['end_datetime']) . " " . get_time($val['end_datetime']) ?></span>
                                    <? elseif ($this->trips_reports_model->get_endtime($val['end_datetime']) < '00:00:00'): ?>
                                                            <span class="label"style="direction: ltr;background-color:red;"><?= get_date($val['end_datetime']) . " " . get_time($val['end_datetime']) ?></span>
                                    <? else: ?>
                                                            <span style="direction: ltr"><?= get_date($val['end_datetime']) . " " . get_time($val['end_datetime']) ?></span>
                                    <? endif; ?>
                                            </td>
                                <? endif; ?>
                                -->



                                <? if (in_array('seats', $cols)): ?>
                                    <td><?= $val['seats_count'] ?></td>

                                <? endif; ?>


                                <?php if (PHASE > 1): ?>
                                    <td class='TAC'>
										<!-- 
                                        <? if (count(json_decode($val['ito_driver_info']))) { ?>                                    
                                            X <?= count(json_decode($val['ito_driver_info'])); ?> 

                                            <a title='<?= lang('global_edit') ?>' 
                                               href="<?= site_url('ito/all_movementsv_ito/drivers_and_buses/' . $val['safa_internalsegment_id']) ?>"
                                               style="color:black; direction:rtl">
                                                <img src="<?= IMAGES2 ?>/car.png" align="left" >
                                            </a>
                                        <? } else { ?>
                                            <a title='<?= lang('global_edit') ?>' 
                                               href="<?= site_url('ito/all_movementsv_ito/drivers_and_buses/' . $val['safa_internalsegment_id']) ?>"
                                               >
                                                   <?= lang("global_add"); ?>
                                            </a>
                                        <? } ?>
										 -->


										<?php 
									 	$this->internalsegments_drivers_and_buses_model->safa_internalsegments_id=$val['safa_internalsegment_id'];
									 	$count_drivers_and_buses= $this->internalsegments_drivers_and_buses_model->get(true);
									 	if($count_drivers_and_buses>0) {
									 	?>	
									 	X <?php echo $count_drivers_and_buses; ?> 
									 	<a title='<?= lang('drivers_and_buses') ?>' 
                                               href="<?= site_url('ito/all_movementsv_ito/drivers_and_buses/' . $val['safa_internalsegment_id']) ?>"
                                               style="color:black; direction:rtl">
                                                <img src="<?= IMAGES2 ?>/car.png" align="left" >
                                            </a>
									 	<?php 
									 	} else {
									 	?>	
									 	<a title='<?= lang('drivers_and_buses') ?>' 
                                               href="<?= site_url('ito/all_movementsv_ito/drivers_and_buses/' . $val['safa_internalsegment_id']) ?>"
                                               >
                                                   <?= lang("global_add"); ?>
                                            </a>
									 	<?php 	
									 	}
									 	?>


                                    </td>   
                                <? endif; ?>




                            </tr>
                        <? endforeach; ?>

                    </tbody>
                </table>
            </div>
        </div>

    </div>

    <script>
        $("#from_city").change(function() {
            $.get("<?= site_url('ito/ajax/get_city_hotels') ?>/" + $(this).val(), function(data) {
                $("#from_hotel").html(data);
            });
        });
    </script>

    <script>
        $("#to_city").change(function() {
            $.get("<?= site_url('ito/ajax/get_city_hotels') ?>/" + $(this).val(), function(data) {
                $("#to_hotel").html(data);
            });
        });
    </script>

    <?= form_close() ?>   
