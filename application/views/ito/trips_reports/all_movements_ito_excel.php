<div class="row-fluid">
                <div class="widget">
        <div class="head dark">
                        <div class="icon"><i class="icos-pencil2"></i></div>
                        <h2>مجريات البحث</h2> 
                    </div>
        <div class="block-fluid">
      <div class="row-form">
<div class="span4">
<div class="span4"><b>التاريخ:</b></div>
<div class="span8">من : 18/7/2013 ألى 20/7/2013</div>
</div>
<div class="span4">
    <div class="span4"><b>الوقت:</b></div>
<div class="span8">من : 05:00 ألى: 23:00</div>
</div>
<div class="span4">
    <div class="span4"><b>العدد:</b></div>
<div class="span8">من: 5 ألى: 20</div>
</div>
</div>

<div class="row-form">
<div class="span4">
    <div class="span4"><b>شركة العمرة:</b></div>
<div class="span8">الرحمة</div>
</div>
<div class="span4">
    <div class="span4"><b>الحركة:</b></div>
<div class="span8">وصول</div>
</div>
<div class="span4">
    <div class="span4"><b>أمر التشغيل:</b></div>
<div class="span8">*****</div>
</div>
</div>

<div class="row-form">
  <div class="span4">
      <div class="span4"><b>الوكيل:</b></div>
<div class="span8">أكستريم</div>
</div>
<div class="span4">
    <div class="span4"><b>من مدينة:</b></div>
<div class="span8">مكة المكرمة</div>
</div>
<div class="span4">
    <div class="span4"><b>من الفندق:</b></div>
<div class="span8">فندق ياجد</div>
</div>
</div>


<div class="row-form">
  <div class="span4">
      <div class="span4"><b>المنفذ:</b></div>
<div class="span8">Arar</div>
</div>
<div class="span4">
    <div class="span4"><b>ألى مدينة:</b></div>
<div class="span8">المدينة المنورة</div>
</div>
<div class="span4">
    <div class="span4"><b>ألى الفندق:</b></div>
<div class="span8">أراك</div>
</div>
</div>

      </div>
    </div>
</div>  

<div class="block-fluid">
<table class="fxTable" cellpadding="0" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th><?= lang('trip') ?></th>
                
                <? if (in_array('contract', $cols)): ?>
                    <th><?= lang('uo_companies') ?></th>
                <? endif; ?>

                <? if (in_array('operator', $cols)): ?>
                    <th> <?= lang('contract') ?></th>
                <? endif; ?>                                   

                <? if (in_array('operation_order_id', $cols)): ?>
                    <th><?= lang('operation_order_id') ?></th>
                <? endif; ?>

                <? if (in_array('segment_type', $cols)): ?>
                    <th><?= lang('segment_type') ?></th>
                <? endif; ?>


                <? if (in_array('from', $cols)): ?>
                    <th><?= lang('from') ?></th>
                <? endif; ?>  


                <? if (in_array('to', $cols)): ?>
                    <th><?= lang('to') ?></th>
                <? endif; ?>


                <? if (in_array('starting', $cols)): ?>
                    <th><?= lang('starting') ?></th>
                <? endif; ?>
                
                <!--
                <? if (in_array('ending', $cols)): ?>
                    <th><?= lang('ending') ?></th>
                <? endif; ?>
                -->

                <? if (in_array('seats', $cols)): ?>
                    <th><?= lang('seats') ?></th>
                <? endif; ?>                             


            </tr>
        </thead>

        <tbody>             


            <? foreach ($trips_ds as $val): ?>
                <tr>
                    <td><?= $val['trip_name'] ?></td>
                    
                    <? if (in_array('contract', $cols)): ?>
                        <td><?= $val['uo_name'] ?></td>
                    <? endif; ?>

                    <? if (in_array('operator', $cols)): ?>
                        <td><?= $val['contract_name'] ?></td>
                    <? endif; ?>

                    <? if (in_array('operation_order_id', $cols)): ?>
                        <td><?= $val['operator_reference'] ?></td>
                    <? endif; ?>

                    <? if (in_array('segment_type', $cols)): ?>
                        <td><?= $val['segment_name'] ?></td>
                    <? endif; ?>

                                    
                            <? if (in_array('from', $cols)): ?>
                                <td><?
                                    switch ($val['safa_internalsegmenttype_id']):

                                        case 1:
                                            echo(lang('port').': '.element($val['erp_port_id'], $ports));
                                            break;

                                        case 2: echo("<font class=city_color>".$val['start_city_name']."</font><br>");
                                                echo(element($val['erp_start_hotel_id'], $hotels));
                                            break;

                                        case 3: echo("<font class=city_color>".$val['start_city_name']."</font><br>");
                                                echo(element($val['erp_start_hotel_id'], $hotels));
                                            break;

                                        case 4: 
                                                echo("<font class=city_color>".$val['start_city_name']."</font><br>");
                                                echo(element($val['erp_start_hotel_id'], $hotels));
                                            break;

                                    endswitch;
                                    ?>
                                </td>
                            <? endif; ?>                                


                            <? if (in_array('to', $cols)): ?>
                                <td><?
                                    switch ($val['safa_internalsegmenttype_id']):

                                        case 1:
                                                echo("<font class=city_color>".$val['end_city_name']."</font><br>");
                                                echo(element($val['erp_end_hotel_id'], $hotels));
                                            break;

                                        case 2: 
                                                echo(lang('port').': '.element($val['erp_port_id'], $ports));
                                            break;

                                        case 3: 
                                                echo(element($val['safa_tourism_place_id'], $places));
                                            break;

                                        case 4: 
                                                echo("<font class=city_color>".$val['end_city_name']."</font><br>");
                                                echo(element($val['erp_end_hotel_id'], $hotels));
                                            break;

                                    endswitch;
                                    ?>                                
                                </td>
                            <? endif; ?>
                               
             

                    <? if (in_array('starting', $cols)): ?>
                        <td><span style="direction: ltr"><?= get_date($val['start_datetime']) . " " . get_time($val['start_datetime']) ?></span></td>
                    <? endif; ?>

                    <!--
                    <? if (in_array('ending', $cols)): ?>
                        <td><span style="direction: ltr"><?= get_date($val['end_datetime']) . " " . get_time($val['end_datetime']) ?></span></td>
                    <? endif; ?>
                    -->
                    
                    
                    <? if (in_array('seats', $cols)): ?>
                        <td><?= $val['seats_count'] ?></td>
                    <? endif; ?>

                </tr>
<? endforeach; ?>

        </tbody>
    </table>  
    </div>
<?= ($print_statement) ? $print_statement : "" ?>