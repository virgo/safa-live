   
<? if ($this->input->get('print_arr_report')) : ?>
    <div class="block-fluid">
        <table class="fxTable" cellpadding="0" cellspacing="0" width="100%">
            <thead>
                <tr>

                    <th><?= lang('trip') ?></th>

                    <th><?= lang('trip_id') ?></th>

                    <th><?= lang('arr_date') ?></th>

                    <th><?= lang('arr_time') ?></th>

                    <th><?= lang('contract') ?></th>

                    <th><?= lang('count') ?></th>

                    <th><?= lang('hotel') ?></th>

                    <th><?= lang('trans_company') ?></th>

                    <th><?= lang('port') ?></th>

                    <th><?= lang('nationality') ?></th>

                    <th><?= lang('supervisor') ?></th>


                </tr>
            </thead>
            <tbody>

                <? foreach ($arriving as $val): ?>
                    <tr>

                        <td><?php 
			if($val['trip_name']!='') {
				echo $val['trip_name'];
			} else {
				echo $val['trip_title'];
			}
			?></td>

                        <td><?= $val['flight_number'] ?></td>

                        <td><?= get_date($val['arrival_datetime']) ?></td>

                        <td><?= get_time($val['arrival_datetime']) ?></td>

                        <td><?= $val['contract_name'] ?></td>

                        <td>
                            <?= $val['seats_count'] ?>                            
                        </td>

                        <td><?= $val['hotel_name'] ?></td>

                        <td><?= $val['operator_name'] ?></td>

                        <td><?= $val['start_port_name'] ?>
								 - 
								<?= $val['end_port_name'] ?>
								(<?= $val['end_port_hall_name'] ?>)</td>

                        <td><?= $val['nationality'] ?></td>

                        <td><?php 
			if($val['supervisor']!='') {
				echo $val['supervisor'];
			} else {
				echo $val['trip_supervisors'];
			}
			?></td>                    

                    </tr>
                <? endforeach; ?>

            </tbody>
        </table>
    </div>
<? endif; ?>



<? if ($this->input->get('print_dep_report')) : ?>
    <div class="block-fluid">
    <table class="fsTable" cellpadding="0" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th><?= lang('trip') ?></th>

                <th><?= lang('trip_id') ?></th>

                <th><?= lang('dep_date') ?></th>

                <th> <?= lang('dep_time') ?></th>

                <th><?= lang('contract') ?></th>

                <th><?= lang('count') ?></th>

                <th><?= lang('hotel') ?></th>

                <th><?= lang('trans_company') ?></th>

                <th><?= lang('port') ?></th>

                <th><?= lang('nationality') ?></th>

                <th><?= lang('supervisor') ?></th>                                       


            </tr>
        </thead>


        <tbody>                        


            <? foreach ($departing as $val): ?>
                <tr>

                    <td><?php 
			if($val['trip_name']!='') {
				echo $val['trip_name'];
			} else {
				echo $val['trip_title'];
			}
			?></td>

                    <td>
                        <?= $val['flight_number'] ?>
                    </td>

                    <td><?= get_date($val['departure_datetime']) ?></td>

                    <td><?= get_time($val['departure_datetime']) ?></td>

                    <td><?= $val['contract_name'] ?></td>

                    <td><?= $val['travellers'] ?></td>

                    <td><?= $val['hotel_name'] ?></td>

                    <td><?= $val['operator_name'] ?></td>

                    <td><?= $val['start_port_name'] ?>
			(<?= $val['start_port_hall_name'] ?>)
			 - 
			<?= $val['end_port_name'] ?></td>

                    <td><?= $val['nationality'] ?></td>

                    <td><?php 
			if($val['supervisor']!='') {
				echo $val['supervisor'];
			} else {
				echo $val['trip_supervisors'];
			}
			?></td>

                </tr>
            <? endforeach; ?>

        </tbody>
    </table>      
    </div>
<? endif; //end depatring data ?>

<?=
($print_statement) ? $print_statement : "" ?>