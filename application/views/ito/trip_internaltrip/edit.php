<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); ?>


<div class="row-fluid">
   
    <div class="widget">
    <div class="path-container Fright">
    	<div class="path-name Fright">
            <a href="<?php echo  site_url().'ito/dashboard'; 
            
            ?>"><?php echo  lang('parent_node_title') ?></a>
        </div>
        <div class="path-arrow Fright"></div>
        <div class="path-name Fright">  <a href="<?= site_url('ito/trip_internaltrip') ?>"><?= lang('internal_transportation_order') ?></a></div>
        
        <div class="path-arrow Fright"></div>
        <div class="path-name Fright">  <?= lang('edit_trip_internaltrip') ?></div>
        
        
    </div>
            
</div>
    
    
    <?= form_open_multipart(false, 'id="frm_trip_internaltrip" ') ?>
    
    <div class="widget">
        
        
         <div class="widget-header">
        <div class="widget-header-icon Fright"> <span class="icos-pencil2"></span> </div>
        <div class="widget-header-title Fright">
            <?php echo  lang('edit_trip_internaltrip') ?>
        </div>
    	</div>
        
        
        <div class="block-fluid">
        
        
        <div class="row-form" >
                <div class="span6" >
                    <div class="span4">
                        <?= lang("company_type") ?>
                        <font style='color:red'>*</font>
                    </div>
                    <div class="span8">
                    	<?php 
		                $ea_checked=false;
		                $uo_checked=false;
		                $other_checked=false;
		                
		                if($item->erp_company_type_id==3) { 
				        	$ea_checked=true;
				        } else if($item->erp_company_type_id==2) {
				        	$uo_checked=true;
				        } else {
				        	$other_checked=true;
				        }
		                ?>
                    
                        <?php echo form_radio('company_type', '2', $uo_checked, 'id="company_type"'); ?> <?php echo  lang('uo') ?><br/>
                    	<?php echo form_radio('company_type', '3', $ea_checked, 'id="company_type"'); ?><?php echo  lang('ea') ?><br/>
                    	<?php echo form_radio('company_type', '', $other_checked, 'id="company_type"'); ?><?php echo  lang('other') ?><br/>
                    </div>
                </div>
                
                <div class="span6" >
                    <div class="span4">
                        <?= lang("erp_company_id") ?>
                        <?if(PHASE == 1):?>
                            <font style='color:red'>*</font>
                        <?endif;?>
                    </div>
                    <div class="span8" >
                    	<?php 
		                //$style_erp_company_id='display: block;';
		                //echo "<script>    $(document).ready(function() { $('#erp_company_id_chosen').css('display', 'block'); }); </script>";
		                $style_erp_company_id="$('#erp_company_id_chosen').css('display', 'block');";
		                if($other_checked) {
			                //$style_erp_company_id='display: none;';
			                //echo "<script>    $(document).ready(function() { $('#erp_company_id_chosen').css('display', 'none'); }); </script>";
			                $style_erp_company_id="$('#erp_company_id_chosen').css('display', 'none');";
		                }
		                ?>
		                
                        <?php echo  form_dropdown('erp_company_id', $companies, set_value('erp_company_id', $item->erp_company_id), ' class="chosen-select chosen-rtl" style="'.$style_erp_company_id.'"  tabindex="4" id="erp_company_id" data-placeholder="'.lang('global_all').'"') ?>
                        <span class="bottom" style='color:red' >
                            <?= form_error('erp_company_id') ?> 
                        </span>
                        
                        <?php 
		                $style_erp_company_name='display: none;';
		                if($other_checked) {
		                	$style_erp_company_name='display: block;';
		                }
		                ?>
                        
                    	<?php echo form_input('erp_company_name', set_value("erp_company_name", $item->erp_company_name)," style='$style_erp_company_name' id='erp_company_name' class='input-full' ") ?>
                
                        
                    </div>
                </div>
            </div>
        
            
            <div class="row-form" >
                <div class="span6" >
                    <div class="span4">
                        <?= lang("transport_request_date") ?>
                        <font style='color:red'>*</font>
                    </div>
                    <div class="span8 "> 
                        
                            <input class="datetime input-huge validate[required]" data-format="yyyy-MM-dd" name="datetime" id="date" type="text" value="<?=set_value('datetime', $item->datetime)?>" ></input>
                            
                             <script>
	                        $('.datetime').datepicker({
	                            dateFormat: "yy-mm-dd",
	                            controlType: 'select',
	                            timeFormat: 'HH:mm'
	                        });
	                    	</script>
	                    	
                            <!-- 
                            <span class="add-on">
                                <i data-time-icon="icon-time" data-date-icon="icon-calendar">
                                </i>
                            </span>
                             -->
                        
                        <span class="bottom" style='color:red' >
                            <?= form_error('datetime') ?> 
                        </span>
                    </div>


                </div>
                
                
                 
                 <div class="span6" >
                    <div class="span4"><?= lang("transport_request_status") ?>
                        <font style='color:red'>*</font>   
                    </div>
                    <div class="span8">
                        <?= form_dropdown("safa_internaltripstatus_id", $safa_intrernaltripstatus, set_value("safa_internaltripstatus_id", $item->safa_internaltripstatus_id), "id='safa_internaltripstatus_id' class='validate[required] chosen-select'") ?>
                        <span class="bottom" style='color:red' >
                            <?= form_error('safa_internaltripstatus_id') ?> 
                        </span>
                    </div>
                </div>
                
            </div>
            
            
            
            <div class="row-form" >
               
                <div class="span6">
                    <div class="span4">
                        <?= lang("transport_request_res_code") ?>
                        <font style='color:red'>*</font>
                    </div>
                    <div class="span8" >
                        <?= form_input("operator_reference", set_value("operator_reference", $item->operator_reference)," class='validate[required]'") ?>
                        <span class="bottom" style='color:red' >
                            <?= form_error('operator_reference') ?> 
                        </span>
                    </div>
                </div>
            
            <?php if (PHASE >= 2): ?>
                    <div class="span6">
                        <div class="span4"><?= lang("transport_request_transportername") ?>
                        </div>
                        <div class="span8" >
                            <?= form_dropdown("safa_transporter_id", $transporters, set_value("safa_transporter_id", $item->safa_transporter_id), " class='chosen-select' ") ?>
                            <span class="bottom" style='color:red' >
                                <?= form_error('safa_transporter_id') ?> 
                            </span>
                        </div>
                    </div>
            <? endif; ?>
            
            </div>
            
            <div class="row-form" >
                <div class="span6" >
                    <div class="span4" ><?= lang("transport_request_res_file") ?></div>
                    <div class="span8">
                        <!-- 
                        <div class="input-append file"  >
                            <input type="file" value="" name="userfile"/>
                            <input type="text" name="text_input"   value="<?=set_value('text_input')?>" />
                            <button type="button"  class="btn">Browse</button>
                        </div>
                         -->
                         
                        <input type="file" value="" name="transport_request_res_file" id="transport_request_res_file"/>
                        <span class="bottom" style='color:red' >
                            <?= form_error('userfile') ?> 
                        </span>
                        
                        <a href="<?= base_url('static/temp/ea_files/' . session('internaltrip_file_upload_time') . $item->attachement) ?>" target="_blank"  ><?= $item->attachement ?></a>
                    </div> 
                </div>
            
                <div class="span6" >
                     <div class="span4" ><?= lang('add_ito_notes') ?></div>
                <div class="span8">
                    <textarea name="ito_notes" class="input-full" ><?= set_value('ito_notes',$item->ito_notes)?></textarea>
                </div>
                </div>
            </div>
            
            
            <!-- 
            <div class="toolbar bottom TAC">
                <input class="btn btn-primary" type="submit" name="submit" value="<?= lang('global_edit') ?>" />
                <a href='<?= site_url('ito/trip_internaltrip/index') ?>'  class="btn btn-primary"><?= lang("global_show_segments") ?></a>
            </div>
             -->
            
            
        </div>
        
        </div>
        
        <div class="widget" >
        <script>passages_counter = 0</script>
        <div class="widget-header">
        <div class="widget-header-icon Fright"> <span class="icos-pencil2"></span> </div>
        <div class="widget-header-title Fright">
            <?php echo  lang('transport_request_internal_passages') ?>
        </div>
        
    	<a class="btn Fleft" href="javascript:void(0)" onclick="add_passages('N' + passages_counter++); load_multiselect()">
            <?php echo  lang('global_add') ?>
        </a>
    	</div>
            
            <div class="" >
                <!--- the part of internal passages---> 
                <table cellpadding="0" cellspacing="0" >
                    <thead>
                        <tr>
                            <th><?= lang('internalpassage_type') ?></th>
                            <th><?= lang('internalpassage_starthotel') ?></th>
                            <th><?= lang('internalpassage_endhotel') ?></th>
                            <th><?= lang('internalpassage_startdatatime') ?></th>
                            <th><?= lang('internalpassage_enddatatime') ?></th>
                            <th><?= lang('internalpassage_seatscount') ?></th>
                            <th><?= lang('internalpassage_note') ?></th>
                            <th><?php echo  lang('actions') ?></th>
                        </tr>
                    </thead>
                    <tbody class="passages" id="tbl_body_passages">
                        <? if (isset($internalpassages)) : ?>
                            <? foreach ($internalpassages as $inernalpassage): ?>
                                <tr rel="<?php echo  $inernalpassage->safa_internalsegment_id ?>">
                                    
                                    <td>
		                            <?php 
		                            echo  form_dropdown('internalpassage_type['.$inernalpassage->safa_internalsegment_id.']'
		                                    , $safa_internalpassagetypes, set_value('internalpassage_type['.$inernalpassage->safa_internalsegment_id.']', $inernalpassage->safa_internalsegmenttype_id)
		                                    , 'class="chosen-select chosen-rtl input-full  validate[required]" id="internalpassage_type_'.$inernalpassage->safa_internalsegment_id.'" tabindex="4" ') ?>
		                                    
		                                    
		                            <script>
						            $(document).ready(function() {
						                $("#internalpassage_type_<?php echo $inernalpassage->safa_internalsegment_id; ?>").change(function() {
						                    var val_ = $(this).val();
						                    show_ports_<?php echo $inernalpassage->safa_internalsegment_id; ?>(val_);
						                });
						            });
						
						            function show_ports_<?php echo $inernalpassage->safa_internalsegment_id; ?>(pass_type) {
						             //   alert(pass_type);
									 load_multiselect();
						
									$("#start_point_con_<?php echo $inernalpassage->safa_internalsegment_id; ?>").show();
									$("#end_point_con_<?php echo $inernalpassage->safa_internalsegment_id; ?>").show();
									
						            $("#start_point_con_<?php echo $inernalpassage->safa_internalsegment_id; ?>").children().hide();
						            $("#end_point_con_<?php echo $inernalpassage->safa_internalsegment_id; ?>").children().hide();
						            if (pass_type == 1) {
						                $("#internalpassage_port_start_<?php echo $inernalpassage->safa_internalsegment_id; ?>_chosen").show();
						                $("#internalpassage_hotel_end_<?php echo $inernalpassage->safa_internalsegment_id; ?>_chosen").show();
						            }
						            if (pass_type == 2) {
						                $("#internalpassage_hotel_start_<?php echo $inernalpassage->safa_internalsegment_id; ?>_chosen").show();
						                $("#internalpassage_port_end_<?php echo $inernalpassage->safa_internalsegment_id; ?>_chosen").show();
						                $("#end_point_con_<?php echo $inernalpassage->safa_internalsegment_id; ?>").find(".error").show();
						            }
						            if (pass_type == 3) {
						                $("#internalpassage_hotel_start_<?php echo $inernalpassage->safa_internalsegment_id; ?>_chosen").show();
						                $("#internalpassage_torismplace_end_<?php echo $inernalpassage->safa_internalsegment_id; ?>_chosen").show();
						            }
						            if (pass_type == 4) {
						                $("#internalpassage_hotel_start_<?php echo $inernalpassage->safa_internalsegment_id; ?>_chosen").show();
						                $("#internalpassage_hotel_end_<?php echo $inernalpassage->safa_internalsegment_id; ?>_chosen").show();
						            }
						            
						            }
						
						            
						        	</script>
		                        	</td>
                            
                            
                                    
                                    <td>
                                    
		                            
		                            <div id='start_point_con_<?php echo $inernalpassage->safa_internalsegment_id; ?>' style='display:block' >
		                        	<?php 
		                            echo  form_dropdown('internalpassage_hotel_start['.$inernalpassage->safa_internalsegment_id.']'
		                                    , $erp_hotels, set_value('internalpassage_hotel_start['.$inernalpassage->safa_internalsegment_id.']', $inernalpassage->erp_start_hotel_id)
		                                    , 'class="chosen-select chosen-rtl input-full" id="internalpassage_hotel_start_'.$inernalpassage->safa_internalsegment_id.'" tabindex="4" style="display:none" ');
		                             ?>
		                        	<?php 
		                            echo  form_dropdown('internalpassage_torismplace_start['.$inernalpassage->safa_internalsegment_id.']'
		                                    , $safa_tourismplaces, set_value('internalpassage_torismplace_start['.$inernalpassage->safa_internalsegment_id.']', $inernalpassage->safa_tourism_place_id)
		                                    , 'class="chosen-select chosen-rtl input-full" id="internalpassage_torismplace_start_'.$inernalpassage->safa_internalsegment_id.'" tabindex="4"  style="display:none" '); 
		                            ?>
		                            <?php 
		                            echo  form_dropdown('internalpassage_port_start['.$inernalpassage->safa_internalsegment_id.']'
		                                    , $erp_ports, set_value('internalpassage_port_start['.$inernalpassage->safa_internalsegment_id.']', $inernalpassage->erp_port_id)
		                                    , 'class="chosen-select chosen-rtl input-full" id="internalpassage_port_start_'.$inernalpassage->safa_internalsegment_id.'" tabindex="4"  style="display:none" ');
		                            ?>
		                        	</div>
		                        	
		                        	</td>
		                        	
		                        	
		                        	<td>
		                            
		                            <div id='end_point_con_<?php echo $inernalpassage->safa_internalsegment_id; ?>' style='display:block' >
		                            <?php 
		                            echo  form_dropdown('internalpassage_hotel_end['.$inernalpassage->safa_internalsegment_id.']'
		                                    , $erp_hotels, set_value('internalpassage_hotel_end['.$inernalpassage->safa_internalsegment_id.']', $inernalpassage->erp_end_hotel_id)
		                                    , 'class="chosen-select chosen-rtl input-full" id="internalpassage_hotel_end_'.$inernalpassage->safa_internalsegment_id.'" tabindex="4"  style="display:none" ');
		                            ?>
		                        	
		                        	<?php 
		                            echo  form_dropdown('internalpassage_torismplace_end['.$inernalpassage->safa_internalsegment_id.']'
		                                    , $safa_tourismplaces, set_value('internalpassage_torismplace_end['.$inernalpassage->safa_internalsegment_id.']', $inernalpassage->safa_tourism_place_id)
		                                    , 'class="chosen-select chosen-rtl input-full " id="internalpassage_torismplace_end_'.$inernalpassage->safa_internalsegment_id.'" tabindex="4"  style="display:none" '); 
		                            ?>
		                            <?php 
		                            echo  form_dropdown('internalpassage_port_end['.$inernalpassage->safa_internalsegment_id.']'
		                                    , $erp_ports, set_value('internalpassage_port_end['.$inernalpassage->safa_internalsegment_id.']', $inernalpassage->erp_port_id)
		                                    , 'class="chosen-select chosen-rtl input-full " id="internalpassage_port_end_'.$inernalpassage->safa_internalsegment_id.'" tabindex="4"  style="display:none" ');
		                            ?>
		                        	</div>
		                        	
		                        	
		                        	
		                        	
		                        	
		                        	
		                        	
		                        	<script>
                                    $(document).ready(function() {
                                    	show_ports_<?php echo $inernalpassage->safa_internalsegment_id; ?>(<?php echo $inernalpassage->safa_internalsegmenttype_id;?>);
                                    });
		                           	</script>
		                            
		                        	
		                        	</td>
                                    
                                    
                                    
                                    <td>
			                            <?php echo  form_input('internalpassage_startdatatime['.$inernalpassage->safa_internalsegment_id.']'
			                                    , set_value('internalpassage_startdatatime['.$inernalpassage->safa_internalsegment_id.']', $inernalpassage->start_datetime)
			                                    , 'class="input-huge  datetimepicker" style="width:120px"') ?>
			                                    
			                        
						        	
			                        </td>
			                        <td>
			                            <?php echo  form_input('internalpassage_enddatatime['.$inernalpassage->safa_internalsegment_id.']'
			                                    , set_value('internalpassage_enddatatime['.$inernalpassage->safa_internalsegment_id.']', $inernalpassage->end_datetime)
			                                    , 'class="input-huge datetimepicker" style="width:120px"') ?>
			                        
			                        
			                        </td>
                                    <td>
			                            <?php echo  form_input('seatscount['.$inernalpassage->safa_internalsegment_id.']'
			                                    , set_value('seatscount['.$inernalpassage->safa_internalsegment_id.']', $inernalpassage->seats_count)
			                                    , 'class="input-huge" style="width:120px"') ?>
			                        </td>
			                        <td>
			                            <?php echo  form_input('internalpassage_notes['.$inernalpassage->safa_internalsegment_id.']'
			                                    , set_value('internalpassage_notes['.$inernalpassage->safa_internalsegment_id.']', $inernalpassage->notes)
			                                    , 'class="input-huge" style="width:120px"') ?>
			                        </td>
			                        
			                        
			                        
                                    <td class="TAC">
		                            	<a href="javascript:void(0)" onclick="delete_passages('<?php echo  $inernalpassage->safa_internalsegment_id ?>', true)"><span class="icon-trash"></span></a>
		                        	</td>
                                </tr>  

                            <? endforeach; ?>
                        <? endif; ?>  
                    </tbody>
                </table>  
            </div>
        </div>

		<div class="toolbar bottom TAC">
            <input type="submit"  class="btn btn-primary" name="submit" value="<?= lang('global_submit') ?>" />
            <a href='<?= site_url('ito/trip_internaltrip') ?>'  class="btn btn-primary" /><?= lang('global_back') ?></a>
        </div>
        
        <?= form_close() ?>
        
    </div>  





<script>
    $(document).ready(function() {

    	var config = {
    	        '.chosen-select': {width: "100%"},
    	        '.chosen-select-deselect': {allow_single_deselect: true},
    	        '.chosen-select-no-single': {disable_search_threshold: 10},
    	        '.chosen-select-no-results': {no_results_text: 'Oops, nothing found!'},
    	        '.chosen-select-width': {width: "95%"}
    	    }
    	    for (var selector in config) {
    	        $(selector).chosen(config[selector]);
    	    }
	    
    	// binds form submission and fields to the validation engine
        $("#frm_trip_internaltrip").validationEngine({
                prettySelect : true,
                useSuffix: "_chosen",
                promptPosition : "topRight:-150"
                //promptPosition : "bottomLeft"
        });


    	
        
        $('#company_type').live('change', function() { 

        	var erp_company_type_id=$(this).val();

            if(erp_company_type_id!='') {
        	var dataString = 'erp_company_type_id='+ erp_company_type_id;
            
            $.ajax
            ({
            	type: 'POST',
            	url: '<?php echo base_url().'ito/trip_internaltrip/getCompaniesByCompanyType'; ?>',
            	data: dataString,
            	cache: false,
            	success: function(html)
            	{
                	//alert(html);
            		$("#erp_company_id").html(html);
            		$("#erp_company_id").trigger("chosen:updated");
            	}
            });

          	//$('#erp_company_id').css("display", "block");
            $('#erp_company_id_chosen').css("display", "block");
            $('#erp_company_name').css("display", "none");
            } else {
            		$('#erp_company_name').css("display", "block");
            		//$('#erp_company_id').css("display", "none");
            		$('#erp_company_id_chosen').css("display", "none");
            }
            
        });

        
        $(".myTable").dataTable(
                {bSort: false,
                    bAutoWidth: true,
                    "iDisplayLength": false, // can be removed for basic 10 items per page
                    "sPaginationType": false,
                    "bPaginate": false,
                    "bInfo": false}
        );
    });
</script>


<script>
$(document).ready(function() {
	<?php echo $style_erp_company_id; ?>

	$('.datetimepicker').datetimepicker({
        dateFormat: "yy-mm-dd",
        controlType: 'select',
        timeFormat: 'HH:mm'
    });
});


function load_multiselect() {
	var config = {
            '.chosen-select': {width: "100%"},
            '.chosen-select-deselect': {allow_single_deselect: true},
            '.chosen-select-no-single': {disable_search_threshold: 10},
            '.chosen-select-no-results': {no_results_text: 'Oops, nothing found!'},
            '.chosen-select-width': {width: "95%"}
        }
        for (var selector in config) {
            $(selector).chosen(config[selector]);
        }
}

function add_passages(id) {
    var new_row = [
            "<tr rel=\"" + id + "\">",

            "    <td>",
            "       <select name=\"internalpassage_type[" + id + "]\" class=\"chosen-select chosen-rtl input-full validate[required] \" id=\"internalpassage_type_" + id + "\" tabindex=\"4\">",
            <? foreach($safa_internalpassagetypes as $key => $value): ?>
            "           <option value=\"<?= $key ?>\"><?= $value ?></option>",
            <? endforeach ?>
            "    </select>",

            "<script>",
            "$(document).ready(function() {",
            '    $("#internalpassage_type_' + id + '").change(function() {',
            "        var val_ = $(this).val();",
            '        show_ports_' + id + '(val_);',
            "    });",
            "});",

            'function show_ports_' + id + '(pass_type) {',
            " //   alert(pass_type);",
			" load_multiselect();",

			'$("#start_point_con_' + id + '").show();',
			'$("#end_point_con_' + id + '").show();',
			
            '$("#start_point_con_' + id + '").children().hide();',
            '$("#end_point_con_' + id + '").children().hide();',
            'if (pass_type == 1) {',
            '    $("#internalpassage_port_start_' + id + '_chosen").show();',
            '    $("#internalpassage_hotel_end_' + id + '_chosen").show();',
            '}',
            'if (pass_type == 2) {',
            '    $("#internalpassage_hotel_start_' + id + '_chosen").show();',
            '    $("#internalpassage_port_end_' + id + '_chosen").show();',
            '    $("#end_point_con_' + id + '").find(".error").show();',
            '}',
            'if (pass_type == 3) {',
            '    $("#internalpassage_hotel_start_' + id + '_chosen").show();',
            '    $("#internalpassage_torismplace_end_' + id + '_chosen").show();',
            '}',
            'if (pass_type == 4) {',
            '    $("#internalpassage_hotel_start_' + id + '_chosen").show();',
            '    $("#internalpassage_hotel_end_' + id + '_chosen").show();',
            '}',
            
            "}",

            
        	"<\/script>",
            
            "    </td>",
            
            "    <td>",
			"<div id='start_point_con_" + id + "' style='display:none' >",
            "       <select name=\"internalpassage_hotel_start[" + id + "]\" class=\"chosen-select chosen-rtl input-full hotel \"   id=\"internalpassage_hotel_start_" + id + "\" tabindex=\"4\">",
            <? foreach($erp_hotels as $key => $value): ?>
            "           <option value=\"<?= $key ?>\"><?= $value ?></option>",
            <? endforeach ?>
            "    </select>",

            "       <select name=\"internalpassage_torismplace_start[" + id + "]\" class=\"chosen-select chosen-rtl input-full place \"   id=\"internalpassage_torismplace_start_" + id + "\" tabindex=\"4\">",
            <? foreach($safa_tourismplaces as $key => $value): ?>
            "           <option value=\"<?= $key ?>\"><?= $value ?></option>",
            <? endforeach ?>
            "    </select>",

            "       <select name=\"internalpassage_port_start[" + id + "]\" class=\"chosen-select chosen-rtl input-full port \" id=\"internalpassage_port_start_" + id + "\" tabindex=\"4\">",
            <? foreach($erp_ports as $key => $value): ?>
            "           <option value=\"<?= $key ?>\"><?= $value ?></option>",
            <? endforeach ?>
            "    </select>",
            "    </div>",
            "    </td>",

            "    <td>",
			"<div id='end_point_con_" + id + "' style='display:none'>",
            "       <select name=\"internalpassage_hotel_end[" + id + "]\" class=\"chosen-select chosen-rtl input-full hotel \"   id=\"internalpassage_hotel_end_" + id + "\" tabindex=\"4\">",
            <? foreach($erp_hotels as $key => $value): ?>
            "           <option value=\"<?= $key ?>\"><?= $value ?></option>",
            <? endforeach ?>
            "    </select>",

            "       <select name=\"internalpassage_torismplace_end[" + id + "]\" class=\"chosen-select chosen-rtl input-full place \"    id=\"internalpassage_torismplace_end_" + id + "\" tabindex=\"4\">",
            <? foreach($safa_tourismplaces as $key => $value): ?>
            "           <option value=\"<?= $key ?>\"><?= $value ?></option>",
            <? endforeach ?>
            "    </select>",

            "       <select name=\"internalpassage_port_end[" + id + "]\" class=\"chosen-select chosen-rtl input-full port \"   id=\"internalpassage_port_end_" + id + "\" tabindex=\"4\">",
            <? foreach($erp_ports as $key => $value): ?>
            "           <option value=\"<?= $key ?>\"><?= $value ?></option>",
            <? endforeach ?>
            "    </select>",
            "    </div>",
            "    </td>",

            

            '     <td><?php echo  form_input("internalpassage_startdatatime[' + id + ']", FALSE, " class=\"validate[required] datetimepicker \"   ") ?>',
            
            "    </td>",



            '     <td><?php echo  form_input("internalpassage_enddatatime[' + id + ']", FALSE, " class=\"validate[required] datetimepicker \"   ") ?>',
            "<script>",
            '$(".datetimepicker").datetimepicker({',
            "    dateFormat: 'yy-mm-dd',",
            "    controlType: 'select',",
            "    timeFormat: 'HH:mm'",
            "});",
        	"<\/script>",
            "    </td>",
            

            '     <td><?php echo  form_input("seatscount[' + id + ']", FALSE, " class=\" validate[custom[integer]] seatscount' + id + '\"   ") ?>',
            "    </td>",

            '     <td><?php echo  form_input("internalpassage_notes[' + id + ']", FALSE, " class=\"internalpassage_notes' + id + '\"   ") ?>',
            "    </td>",

            
            "    <td class=\"TAC\">",
            "        <a href=\"javascript:void(0)\" onclick=\"delete_passages('" + id + "')\"><span class=\"icon-trash\"></span></a>",
            "    </td>",
            "</tr>"
            ].join("\n");
   
    
    $('#tbl_body_passages').append(new_row);
   
    
}
function delete_passages(id, database) 
{
    if(typeof database == 'undefined')
    {
        $('.passages').find('tr[rel="' + id + '"]').remove();
    }
    else
    {
        $('.passages').find('tr[rel=' + id + ']').remove();
        var hidden_input = '<input type="hidden" name="passages_remove[]" value="' + id + '" />';
        $('#tbl_body_passages').append(hidden_input);
    }
}
</script>



<script>
    function show_ports(pass_type, id) {

        $("#start_point_con"+id).children().hide();
        $("#end_point_con"+id).children().hide();
        if (pass_type == 1) {
            //show the data for arrive//
            $("#start_point_con"+id).find('.port').show();
            $("#end_point_con"+id).find('.hotel').show();

        }
        if (pass_type == 2) {
            // show data for leave//
            $("#start_point_con"+id).find('.hotel').show();
            $("#end_point_con"+id).find('.port').show();
            $("#end_point_con"+id).find('.error').show();
        }
        if (pass_type == 3) {
            // show data for place//
            $("#start_point_con"+id).find('.hotel').show();
            $("#end_point_con"+id).find('.place').show();
        }
        if (pass_type == 4) {
            // show data for transport//
            $("#start_point_con"+id).find('.hotel').show();
            $("#end_point_con"+id).find('.hotel').show();
        }
    }
</script>
<script>
    $(document).ready(function() {
        $("#passage_type").change(function() {
            var val_ = $(this).val();
            show_ports(val_);
        });
    });
</script>
<script>
    $(document).ready(function() {
        var pass_type = $("#passage_type").val();
        show_ports(pass_type);

    });
</script>
