<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); ?>
<style>
    .modal{
        height:35% !important;
    }  
</style>
<div class="row-fluid">
   
<div class="widget">
    <div class="path-container Fright">
    	<div class="path-name Fright">
            <a href="<?php echo  site_url().'ito/dashboard'; 
            
            ?>"><?php echo  lang('parent_node_title') ?></a>
        </div>
        <div class="path-arrow Fright"></div>
        <div class="path-name Fright">  <?= lang('incoming_internal_transportation') ?></div>
    </div>
            
</div>
    
    <div class="widget">
        
        
        
        <div class="widget-header">
        <div class="widget-header-icon Fright"> <span class="icos-pencil2"></span> </div>
        <div class="widget-header-title Fright">
            <?php echo  lang('global_search') ?>
        </div>
    	</div>
        
        <?= form_open("", "method='get'") ?>
        <div class="block-fluid">
            <div class="row-form">
                <div class="span6">
                    <div class="span2"><?= lang('transport_request_status') ?></div>
                    <div class="span10">
                        <?= form_dropdown("safa_tripstatus_id", $safa_intrernaltripstatus, set_value("safa_tripstatus_id"),"class='input-huge' style='width:100%'") ?>
                    </div>
                </div>
                <div class="span6">
                    <div class="span2"><?= lang('transport_request_contract') ?></div>
                    <div class="span10">
                        <?= form_dropdown("uo_contract_id", $uo_contracts, $this->input->get('uo_contract_id'), "id='contract_id',class='input-huge' style='width:100%' ") ?>
                    </div>
                </div>
            </div>
            <div class="row-form">
                <div class="span6">
                    <div class="span2"><?= lang('transport_request_trip_id') ?></div>
                    <div class="span10">
                        <?= form_dropdown("safa_trip_id", array("0" => lang('global_select_from_menu')), set_value("safa_trip_id"), "id='trip_id', class='select', style='width:100%'") ?> 
                    </div>
                </div>
                <?php if (PHASE > 1): ?>
                    <div class="span6">
                        <div class="span2"><?= lang('transport_request_transportername') ?></div>
                        <div class="span10">
                            <?= form_dropdown("safa_transporter_id", $transporters, set_value("safa_transporter_id"), "class='input-huge' style='width:100%' ") ?>
                        </div>
                    </div> 
                <? endif; ?>
            </div> 

            <div class="toolbar bottom TAC">
                <input type="submit"  class="btn btn-primary" name="search" value="<?= lang('global_search') ?>" />
            </div>
        </div>
        <?= form_close() ?>
    </div>
    <div class="widget">
        
        
        <div class="widget-header">
        <div class="widget-header-icon Fright"> <span class="icos-pencil2"></span> </div>
        <div class="widget-header-title Fright">
            <?php echo  lang('transport_requests') ?>
        </div>
    	</div>
                       
        <div class="table-responsive">
            <table class='fsTable'>
                <thead>
                    <tr>
                        <th><?= lang('confirmation_number') ?></th>
                        <th><?= lang('transport_request_trip_id') ?></th>
                        <th><?= lang('transport_request_contract') ?></th>                        
                        <th><?= lang('transport_request_transportername') ?></th>
                        <th><?= lang('transport_request_status') ?></th>
                        <th><?= lang('transport_request_res_code') ?></th>
                        <th><?= lang('transport_request_internalpassage_num') ?></th>
                        <th><?= lang('global_operations') ?></th>
                    </tr>
                </thead>
                <tbody>

                    <? if (isset($items)): ?>
                        <? foreach ($items as $item): ?>
                            <tr>
                                <td><span class='label'style='background-color:<?= item("safa_internaltripstatus", 'color', array('safa_internaltripstatus_id' => $item->safa_internaltripstatus_id)) ?>' ><?= $item->confirmation_number ?></span></td>
                                <td><?php if($item->trip_name!=''){echo $item->trip_name;}else{echo $item->trip_title; } ?></td>
                                <td><?= $item->contract_name ?></td>
                                <td><?= $item->transportername ?></td>
                                <td><?= $item->status_name ?></td>
                                <td><?= $item->operator_reference ?></td>
                                <td><?= $item->num_segments ?></td>
                                <td>
                                    <div class="TAC"  title='<?= htmlspecialchars($item->ea_notes,ENT_QUOTES) ?>' >
                                        <a href="#edit-internaltrip-notes" title="<?= lang('add_ito_notes') ?>" onclick="openNotesPanal('<?=$item->safa_trip_internaltrip_id?>','<?= htmlspecialchars($item->ito_notes,ENT_QUOTES) ?>')"  class="icosg-feed"  role="button" data-toggle="modal"></a>
                                        <a href="<?=  site_url('ito/trip_internaltrip/edit_incoming/'.$item->safa_trip_internaltrip_id)?>" title="<?= lang('global_edit') ?>"  class="icon-pencil" ></a>
                                    </div>
                                </td>
                                
                            </tr> 
                        <? endforeach; ?> 
                    <? endif; ?>
                </tbody>
            </table>
        </div> 
        <div class="table-responsive">
            <table>
                <thead>
                    <tr>
                        <? if (isset($safa_intrernaltripstatus_color)): ?>
                            <? $name = name() ?>
                            <? foreach ($safa_intrernaltripstatus_color as $status): ?>
                                <th><span class='label'style='background-color:<?= $status->color ?>'><?= $status->$name ?></span><? //$status->code ?></th>
                            <? endforeach; ?>
                        <? endif; ?>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>  
        </div>     
    </div>
    <div class='row-fluid' >
<!--        <?= $pagination ?> -->
    </div>
</div>
<!--change status panal -->
<div id="edit-internaltrip-notes" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close close_window" data-dismiss="modal" aria-hidden="true">×</button> 
        <h3 id="myModalLabel"><?= lang('add_ito_notes') ?></h3>
    </div>
    <?= form_open('ito/trip_internaltrip/edit_ito_notes') ?>
    <div class="row-fluid">
        <div class="row-form" >
            <div class="span12" >
                <textarea name="internaltrip_notes" ><?= set_value('internaltrip_notes',$item->ito_notes) ?></textarea>
                <input type="hidden" name="internal_trip_id"  />
            </div> 
        </div>

    </div>                   
    <div class="modal-footer" align="center" >
        <button class="btn btn-primary"><?= lang('global_submit') ?></button>
        <a type="button" class="close_window btn-primary btn" data-dismiss="modal" aria-hidden="true"><?= lang('global_close') ?></a>
    </div>
    <?= form_close() ?> 
</div>

<!--change status panal -->
<script>
    $(document).ready(function() {
        $("#contract_id").change(function() {
            //sending the request to get the trips//
            $.get('<?= site_url("ito/trip_internaltrip/get_ea_trips") ?>/' + $(this).val(), function(data) {
                $("#trip_id").html(data);
            });
        });
        var contract_id = $("#contract_id").val();
        $.get('<?= site_url("ito/trip_internaltrip/get_ea_trips") ?>/' + contract_id, function(data) {
            $("#trip_id").html(data);
            $("#trip_id").val('<?= $this->input->get('safa_trip_id') ?>');
        });

    });
</script>
<script>
    $(document).ready(function() {
        $(".myTable").dataTable(
                {bSort: true,
                    bAutoWidth: true,
                    "iDisplayLength": false, // can be removed for basic 10 items per page
                    "sPaginationType": false,
                    "bPaginate": false,
                    "bInfo": false}
        );
    });
</script>
<script>
    function  openNotesPanal(internal_trip_id,ito_note){
        var form = $('#edit-internaltrip-notes').find('form');
                $(form).resetForm();
                $(form).unbind('submit');
        var notes=$('#edit-internaltrip-notes').find('[name="internaltrip_notes"]');
        var trip_id=$('#edit-internaltrip-notes').find('[name="internal_trip_id"]');
        $(trip_id).val(internal_trip_id);
        $(notes).html(ito_note);
       form.submit(function(){
                $(this).ajaxSubmit({
                    beforeSubmit: function() {
                        //alert('willsubmit');
                    },
                    success: function(data) {
                        if (data==1) {
                            $('#edit-internaltrip-notes').find('.close_window').click();// to close the popup after submation//
                            window.location.reload();
                        }

                    },
                });
            return false;
        })
        return false;
    }
</script>
