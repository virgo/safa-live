<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); ?>
<!-- the script for datepicker --->
<script type="text/javascript" src='<?= JS ?>/plugins/bootstrap/bootstrap.min.js'></script>
<link href="<?= CSS_JS ?>/dateandtime/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src='<?= CSS_JS ?>/dateandtime/bootstrap-datetimepicker.min.js'></script>
<!-- the script for datepicker --->


<div class="row-fluid">
    
    
    <div class="widget">
    <div class="path-container Fright">
        <div class="icon"><i class="icos-pencil2"></i></div> 
         <div class="path-name Fright">
               <a href="<?= site_url('ito/dashboard') ?>"><?= lang('parent_node_title') ?></a>
        </div>    
        <div class="path-arrow Fright">
        </div>
            <div class="path-name Fright">
                <a href="<?= site_url('ito/trip_internaltrip/index') ?>"><?= lang('node_title') ?></a>
        	</div>
        	<div class="path-arrow Fright">
	        </div>
	        <div class="path-name Fright">
                <span><?= lang('add_transport_request') ?></span>
        	</div>
    </div>     
    </div>
    
    
    <div class="widget">
       
		<div class="widget-header">
	        <div class="widget-header-icon Fright">
	            <span class="icos-pencil2"></span>
	        </div>
	
	        <div class="widget-header-title Fright">
	           <?= lang('add_transport_request') ?>
	        </div>
		</div>
        
        
        
        <?= form_open_multipart(false, 'id="frm_trip_internaltrip" ') ?>
        <div class="block-fluid">
        
        
        	<div class="row-form" >
                <div class="span6" >
                    <div class="span4">
                        <?= lang("company_type") ?>
                        <font style='color:red'>*</font>
                    </div>
                    <div class="span8">
                        <?php echo form_radio('company_type', '2', true, 'id="company_type"'); ?> <?php echo  lang('uo') ?><br/>
                    	<?php echo form_radio('company_type', '3', false, 'id="company_type"'); ?><?php echo  lang('ea') ?><br/>
                    	<?php echo form_radio('company_type', '', false, 'id="company_type"'); ?><?php echo  lang('other') ?><br/>
                    </div>
                </div>
                
                <div class="span6" >
                    <div class="span4">
                        <?= lang("erp_company_id") ?>
                        <?if(PHASE == 1):?>
                            <font style='color:red'>*</font>
                        <?endif;?>
                    </div>
                    <div class="span8" >
                        <?php echo  form_dropdown('erp_company_id', $uo_companies, set_value('erp_company_id', ''), 'class="validate[required] chosen-select chosen-rtl"  tabindex="4" id="erp_company_id" data-placeholder="'.lang('global_all').'"') ?>
                        <span class="bottom" style='color:red' >
                            <?= form_error('erp_company_id') ?> 
                        </span>
                        
                        <?php 
		                $style_erp_company_name='display: none;';
		                ?>
                    	<?php echo form_input('erp_company_name', set_value("erp_company_name", '')," style='$style_erp_company_name' id='erp_company_name' class='input-full' ") ?>
                
                        
                    </div>
                </div>
            </div>
            
            
            <div class="row-form" >
                <div class="span6" >
                    <div class="span4">
                        <?= lang("transport_request_date") ?>
                        <font style='color:red'>*</font>
                    </div>
                    <div class="span8 "> 
                        
                            <input class="datetime input-huge validate[required]" data-format="yyyy-MM-dd" name="datetime" id="date" type="text" value="<?=set_value('datetime')?>" ></input>
                            
                             <script>
	                        $('.datetime').datepicker({
	                            dateFormat: "yy-mm-dd",
	                            controlType: 'select',
	                            timeFormat: 'HH:mm'
	                        });
	                    	</script>
	                    	
                            <!-- 
                            <span class="add-on">
                                <i data-time-icon="icon-time" data-date-icon="icon-calendar">
                                </i>
                            </span>
                             -->
                        
                        <span class="bottom" style='color:red' >
                            <?= form_error('datetime') ?> 
                        </span>
                    </div>


                </div>
                
                <!-- 
                <div class="span6" >
                    <div class="span4"><?= lang("transport_request_contract") ?>
                        <font style='color:red'>*</font>
                    </div>
                    <div class="span8">
                        <?= form_dropdown("uo_contract_id", $uo_contracts, set_value("uo_contract_id"), " id='contract_id' class='validate[required]' ") ?>
                        <span class="bottom" style='color:red' >
                            <?= form_error('uo_contract_id') ?> 
                        </span>
                    </div>
                </div>
                 -->
                 
                 <div class="span6" >
                    <div class="span4"><?= lang("transport_request_status") ?>
                        <font style='color:red'>*</font>   
                    </div>
                    <div class="span8">
                        <?= form_dropdown("safa_internaltripstatus_id", $safa_intrernaltripstatus, set_value("safa_internaltripstatus_id"), "id='safa_internaltripstatus_id' class='validate[required]' ") ?>
                        <span class="bottom" style='color:red' >
                            <?= form_error('safa_internaltripstatus_id') ?> 
                        </span>
                    </div>
                </div>
                
            </div>
            
            
            
            
            <div class="row-form" >
               
                <div class="span6">
                    <div class="span4">
                        <?= lang("transport_request_res_code") ?>
                        <font style='color:red'>*</font>
                    </div>
                    <div class="span8" >
                        <?= form_input("operator_reference", set_value("operator_reference"), " class='validate[required]' ") ?>
                        <span class="bottom" style='color:red' >
                            <?= form_error('operator_reference') ?> 
                        </span>
                    </div>
                </div>
            
            <?php if (PHASE >= 2): ?>
                    <div class="span6">
                        <div class="span4"><?= lang("transport_request_transportername") ?>
                        </div>
                        <div class="span8" >
                            <?= form_dropdown("safa_transporter_id", $transporters, set_value("safa_transporter_id")) ?>
                            <span class="bottom" style='color:red' >
                                <?= form_error('safa_transporter_id') ?> 
                            </span>
                        </div>
                    </div>
            <? endif; ?>
            
            </div>
            
            <div class="row-form" >
                <div class="span6" >
                    <div class="span4" ><?= lang("transport_request_res_file") ?></div>
                    <div class="span8">
                        <!-- 
                        <div class="input-append file"  >
                            <input type="file" value="" name="userfile"/>
                            <input type="text" name="text_input"   value="<?=set_value('text_input')?>" />
                            <button type="button"  class="btn">Browse</button>
                        </div>
                         -->
                        <input type="file" value="" name="transport_request_res_file" id="transport_request_res_file"/>
                        
                        <span class="bottom" style='color:red' >
                            <?= form_error('userfile') ?> 
                        </span>
                    </div> 
                </div>
                
                <div class="span6" >
                     <div class="span4" ><?= lang('add_ito_notes') ?></div>
                <div class="span8">
                    <textarea name="ito_notes" class="input-full" ><?= set_value('ito_notes','')?></textarea>
                </div>
                </div>
                
            </div>

        </div>
        <div class="toolbar bottom TAC">
            <input type="submit"  class="btn btn-primary" name="submit" value="<?= lang('global_submit') ?>" />
            <a href='<?= site_url('ito/trip_internaltrip/index') ?>'  class="btn btn-primary" /><?= lang('global_back') ?></a>
        </div>
        <?= form_close() ?>
    </div>
</div>  

<script>


$(document).ready(function() {

	var config = {
            '.chosen-select': {width: "100%"},
            '.chosen-select-deselect': {allow_single_deselect: true},
            '.chosen-select-no-single': {disable_search_threshold: 10},
            '.chosen-select-no-results': {no_results_text: 'Oops, nothing found!'},
            '.chosen-select-width': {width: "95%"}
        }
        for (var selector in config) {
            $(selector).chosen(config[selector]);
        }

	// binds form submission and fields to the validation engine
	$("#frm_trip_internaltrip").validationEngine({
	        prettySelect : true,
	        useSuffix: "_chosen",
	        promptPosition : "topRight:-150"
	        //promptPosition : "bottomLeft"
	});
	
    
$('#company_type').live('change', function() { 

	var erp_company_type_id=$(this).val();

    if(erp_company_type_id!='') {
	var dataString = 'erp_company_type_id='+ erp_company_type_id;
    
    $.ajax
    ({
    	type: 'POST',
    	url: '<?php echo base_url().'ito/trip_internaltrip/getCompaniesByCompanyType'; ?>',
    	data: dataString,
    	cache: false,
    	success: function(html)
    	{
        	//alert(html);
    		$("#erp_company_id").html(html);
    		$("#erp_company_id").trigger("chosen:updated");
    	}
    });

    //$('#erp_company_id').css("display", "block");
    $('#erp_company_id_chosen').css("display", "block");
    $('#erp_company_name').css("display", "none");
    } else {
    		$('#erp_company_name').css("display", "block");
    		//$('#erp_company_id').css("display", "none");
    		$('#erp_company_id_chosen').css("display", "none");
    }
    
});

});
</script>

<script>
    $(document).ready(function() {
        $("#contract_id").change(function() {
            //sending the request to get the trips//
            $.get('<?= site_url("ito/trip_internaltrip/get_ea_trips") ?>/' + $(this).val(), function(data) {
                $("#trip_id").html(data);
            });
        });
    });
</script>
<script>
    // filter with validation //
    $(document).ready(function() {
        /* initializing  the default value of the contracts by first items*/
        $("#contract_id").val($(this).find('option').eq(1).val());
        var contract_id = $("#contract_id").val();

        $.get('<?= site_url("ito/trip_internaltrip/get_ea_trips") ?>/' + contract_id, function(data) {
            $("#trip_id").html(data);
            $("#trip_id").val('<?= $this->input->post('safa_trip_id') ?>');
        });

    });
</script>
<script>
    $(document).ready(function() {
        /* input file */
        $(".file .btn, .file input:text").click(function() {
            var block = $(this).parent('.file');
            block.find('input:file').change(function() {
                var file_arr = $(this).val().split('\\');
                var file_name = file_arr[file_arr.length - 1];
                block.find('input:text').val(file_name);
            });
        });
    });
</script>
<script type="text/javascript">
    $(function() {
        $('#datetimepicker1').datetimepicker({
            language: 'pt-BR',
            pickTime: false
        });
    });
</script>
<?php if (PHASE >= 1):?>
<script>
    $(document).ready(function() {

        /*
        $("#safa_ito_id").change(function() {
          if($("#safa_ito_id").val()){
               $("#safa_internaltripstatus_id").val(2);
            }else{
               $("#safa_internaltripstatus_id").val(1);
            }
            });
        $("#safa_internaltripstatus_id").change(function() {
            if($("#safa_ito_id").val()){
              if($("#safa_internaltripstatus_id").val()!=2)
                  <? if (lang('global_lang') == 'ar'): ?> 
                     alert("قم باختيار مؤكد من شركة السياحة");
                  <?  else:?>
                     alert("choose confirmed by ea");
                  <?  endif;?>
                  $("#safa_internaltripstatus_id").val(2);
            }else{
              if($("#safa_internaltripstatus_id").val()!=1)
                  <? if (lang('global_lang') == 'ar'): ?>
                     alert("قم باختيار بانتظار شركة العمرة");
                  <?else:?>
                     alert("choose pending uo");
                  <?  endif;?>
                  $("#safa_internaltripstatus_id").val(1);
            }
        });

        */
        
    });
</script>
<?  endif;?>

