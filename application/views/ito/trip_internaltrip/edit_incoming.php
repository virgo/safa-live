<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); ?>

<!-- the script for datepicker --->
<script type="text/javascript" src='<?= JS ?>/plugins/bootstrap/bootstrap.min.js'></script>
<link href="<?= CSS_JS ?>/dateandtime/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src='<?= CSS_JS ?>/dateandtime/bootstrap-datetimepicker.min.js'></script>
<!-- the script for datepicker --->
<div class="row-fluid">
   
    <div class="widget">
    <div class="path-container Fright">
    	<div class="path-name Fright">
            <a href="<?php echo  site_url().'ito/dashboard'; 
            
            ?>"><?php echo  lang('parent_node_title') ?></a>
        </div>
        <div class="path-arrow Fright"></div>
        <div class="path-name Fright">  <a href="<?= site_url('ito/trip_internaltrip/incoming') ?>"><?= lang('incoming_internal_transportation') ?></a></div>
        
        <div class="path-arrow Fright"></div>
        <div class="path-name Fright">  <?= lang('edit_trip_internaltrip') ?></div>
        
        
    </div>
            
</div>
    
    <div class="widget">
        
        
         <div class="widget-header">
        <div class="widget-header-icon Fright"> <span class="icos-pencil2"></span> </div>
        <div class="widget-header-title Fright">
            <?php echo  lang('edit_trip_internaltrip') ?>
        </div>
    	</div>
        
        <?= form_open_multipart() ?>
        <div class="block-fluid">
            <div class="row-form" >
                <div class="span6" >
                    <div class="span4">
                        <?= lang("transport_request_date") ?>
                    </div>
                    <div class="span8">
                        <?= $item->datetime ?> 
                    </div>
                </div>
                <div class="span6">
                    <div class="span4"><?= lang("transport_request_contract") ?></div>
                    <div class="span8" >
                        <?= $item->contract_name ?>
                    </div>
                </div>
            </div>
            <div class="row-form" >
                <div class="span6" >
                    <div class="span4">
                        <?= lang("transport_request_trip_id") ?>
                    </div>
                    <div class="span8">
                        <?php if($item->trip_name!=''){echo $item->trip_name;}else{echo $item->trip_title; } ?>
                    </div>
                </div>
                <div class="span6" >
                    <div class="span4">
                        <?= lang("transport_request_status") ?>
                        <font style='color:red'>*</font>
                    </div>
                    <div class="span8">
                        <?= form_dropdown("safa_internaltripstatus_id", $safa_intrernaltripstatus, set_value("safa_internaltripstatus_id", $item->safa_internaltripstatus_id), "id='safa_internaltripstatus_id'") ?>
                        <span class="bottom" style='color:red' >
                            <?= form_error('safa_internaltripstatus_id') ?> 
                        </span>
                    </div>
                </div>
            </div>
            <div class="row-form" >
                <div class="span6">
                    <div class="span4">
                        <?= lang("transport_request_res_code") ?>
                        <font style='color:red'>*</font>
                    </div>
                    <div class="span8" >
                        <?= form_input("operator_reference", set_value("operator_reference", $item->operator_reference)) ?>
                        <span class="bottom" style='color:red' >
                            <?= form_error('operator_reference') ?> 
                        </span>
                    </div>
                </div>
                <?php if (PHASE >= 2): ?>
                    <div class="span6">
                        <div class="span4"><?= lang("transport_request_transportername") ?>
                        </div>
                        <div class="span8" >
                            <?= form_dropdown("safa_transporter_id", $transporters, set_value("safa_transporter_id", $item->safa_transporter_id), "") ?>
                            <span class="bottom" style='color:red' >
                                <?= form_error('safa_transporter_id') ?> 
                            </span>
                        </div>
                    </div>
                <? endif; ?> 
            </div>

            <div class="row-form" >
                <div class="span6" >
                    <div class="span4" ><?= lang("transport_request_res_file") ?></div>
                    <div class="span8" >
                        <a href="<?= base_url('static/ea_files/' . session('internaltrip_file_upload_time') . $item->attachement) ?>" target="_blank"  ><?= $item->attachement ?></a>
                    </div>
                </div>
                <? if (PHASE >= 2): ?>
                    <div class="span6" >
                        <div class="span4" ><?= lang('confirmation_number') ?></div>
                        <div class="span8" >
                            <?= $item->confirmation_number ?>
                        </div>
                    </div>
                <? endif; ?>
            </div>
            <div class="row-form" >
                <div class="span12" >
                     <div class="span2" ><?= lang('add_ito_notes') ?></div>
                <div class="span10">
                    <textarea name="ito_notes" class="" ><?= set_value('ito_notes',$item->ito_notes)?></textarea>
                </div>
                </div>
            </div>
            <div class="toolbar bottom TAC">
                <input class="btn btn-primary" type="submit" name="submit" value="<?= lang('global_edit') ?>" />
                <a href='<?= site_url('ito/trip_internaltrip/index') ?>'  class="btn btn-primary"><?= lang("global_show_segments") ?></a>
            </div>
            <?= form_close() ?>
        </div>
        
        </div>
        
        <div class="widget" >
           
        <div class="widget-header">
        <div class="widget-header-icon Fright"> <span class="icos-pencil2"></span> </div>
        <div class="widget-header-title Fright">
            <?php echo  lang('transport_request_internal_passages') ?>
        </div>
    	</div>
            
            <div class="table-responsive" >
                <!--- the part of internal passages---> 
                <table class="myTable" cellpadding="0" cellspacing="0">
                    <thead>
                        <tr>
                            <th><?= lang('internalpassage_type') ?></th>
                            <th><?= lang('internalpassage_starthotel') ?></th>
                            <th><?= lang('internalpassage_endhotel') ?></th>
                            <th><?= lang('internalpassage_startdatatime') ?></th>
                            <th><?= lang('internalpassage_enddatatime') ?></th>
                            <th><?= lang('internalpassage_seatscount') ?></th>
                            <th><?= lang('internalpassage_note') ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <? if (isset($internalpassages)) : ?>
                            <? foreach ($internalpassages as $inernalpassage): ?>
                                <tr>
                                    <td><?= item("safa_internalsegmenttypes", name(), array("safa_internalsegmenttype_id" => $inernalpassage->safa_internalsegmenttype_id)); ?></td>
                                    <? if ($inernalpassage->safa_internalsegmenttype_id == 1): ?>
                                        <td><?= item(" erp_ports", name(), array("erp_port_id" => $inernalpassage->erp_port_id)); ?></td>
                                        <td><?= item("erp_hotels", name(), array("erp_hotel_id" => $inernalpassage->erp_end_hotel_id)); ?></td>
                                    <? endif; ?>  
                                    <? if ($inernalpassage->safa_internalsegmenttype_id == 2): ?>
                                        <td><?= item("erp_hotels", name(), array("erp_hotel_id" => $inernalpassage->erp_start_hotel_id)); ?></td>
                                        <td><?= item(" erp_ports", name(), array("erp_port_id" => $inernalpassage->erp_port_id)); ?></td>
                                    <? endif; ?>
                                    <? if ($inernalpassage->safa_internalsegmenttype_id == 3): ?>
                                        <td><?= item("erp_hotels", name(), array("erp_hotel_id" => $inernalpassage->erp_start_hotel_id)); ?></td>
                                        <td><?= item("safa_tourismplaces", name(), array("safa_tourismplace_id" => $inernalpassage->safa_tourism_place_id)); ?></td>
                                    <? endif; ?>
                                    <? if ($inernalpassage->safa_internalsegmenttype_id == 4): ?>
                                        <td><?= item("erp_hotels", name(), array("erp_hotel_id" => $inernalpassage->erp_start_hotel_id)); ?></td>
                                        <td><?= item(" erp_hotels", name(), array("erp_hotel_id" => $inernalpassage->erp_end_hotel_id)); ?></td>
                                    <? endif; ?>
                                    <td>
                                        <? if (isset($inernalpassage->start_datetime) && $inernalpassage->start_datetime != null): ?>
                                            <? $date = date_create($inernalpassage->start_datetime); ?>
                                            <span><?= date_format($date, "m-d H:i"); ?></span>
                                        <? endif; ?>   
                                    </td>
                                    <td>
                                        <? if (isset($inernalpassage->end_datetime) && $inernalpassage->end_datetime != null): ?>
                                            <? $date_end = date_create($inernalpassage->end_datetime); ?>    
                                            <span><?= date_format($date_end, "m-d H:i"); ?></span>
                                        <? endif; ?> 
                                    </td>
                                    <td><? if (isset($inernalpassage->seats_count)): ?> <?= $inernalpassage->seats_count ?> <? endif; ?>  </td>
                                    <td><? if (isset($inernalpassage->notes)): ?> <?= $inernalpassage->notes ?> <? endif; ?>  </td>
                                </tr>  

                            <? endforeach; ?>
                        <? endif; ?>  
                    </tbody>
                </table>  
            </div>
        </div>

    </div>  
</div>
<script>
    $(document).ready(function() {
        $(".myTable").dataTable(
                {bSort: false,
                    bAutoWidth: true,
                    "iDisplayLength": false, // can be removed for basic 10 items per page
                    "sPaginationType": false,
                    "bPaginate": false,
                    "bInfo": false}
        );
    });
</script>
