
<!-- By Gouda, For popup -->
<script type="text/javascript" src='<?= CSS_JS ?>/form/form.js'></script>
<link rel="stylesheet" href="<?= CSS ?>/bootstrap/bootstrap.min.new.css"/>
        
<style>
    input {margin-bottom : 4px !important;}
</style>
<!-- By Gouda, To Close menu On this screen -->
<script>
jQuery(function() { 
	menu1.toggle();
})
</script>    

<?= form_open('',' target="_blank" ') ?>

<div class="span12">
    <div class="widget">
    
     
    <div class="path-container Fright">
        <div class="icon"><i class="icos-pencil2"></i></div> 
         <div class="path-name Fright">
               <a href="<?= site_url('uo/dashboard') ?>"><?= lang('global_system_management') ?></a>
        </div>    
        <div class="path-arrow Fright">
            </div>
            <div class="path-name Fright">
                <span><?= lang('menu_uos_movements') ?></span>
        </div>
        
        
    </div> 
    </div> 
</div>



<div class="row-fluid">              
    <div class="widget">
              
<div class="widget-header">
	
	            <div class="widget-header-icon Fright">
	                <span class="icos-pencil2"></span>
	            </div>
	
	            <div class="widget-header-title Fright">
	                <?= lang('search') ?>
	            </div>
	
	        </div>
                               
        <div class="block-fluid">
            <div class="row-form">
                <div class="span4">
                    <div class="span4"><?= lang('date') ?>:</div>
                    <div class="span8">
                        <input name="from_date"  value="<?= ($this->input->post('from_date'))?$this->input->post('from_date'):date("Y-m-d"); ?>"  class="validate[required] datepicker from_date" style=" width:48%; margin:0 1%" placeholder="<?= lang('from') ?>" type="text">
                        <input name="to_date"  value="<?= ($this->input->post('to_date'))?$this->input->post('to_date'):date("Y-m-d"); ?>"  class="validate[required] datepicker to_date" style=" width:48%; margin:0 1%" placeholder="<?= lang('to') ?>" type="text">
                        
                        <script>
                        $('.from_date').datepicker({
                            dateFormat: "yy-mm-dd",
                            controlType: 'select',
                            timeFormat: 'HH:mm'
                        });
                    </script>
                    <script>
                        $('.to_date').datepicker({
                            dateFormat: "yy-mm-dd",
                            controlType: 'select',
                            timeFormat: 'HH:mm'
                        });
                    </script>
                        
                    </div>
                </div>
                <div class="span4">
                    <div class="span4"><?= lang('time') ?>:</div>
                    <div class="span8">
                        <input name="from_time"  value="<?= $this->input->post('from_time'); ?>"  id="basic_example_1" value="" style=" width:48%; margin:0 1%" placeholder="<?= lang('from') ?>" type="text">
                        <input name="to_time"  value="<?= $this->input->post('to_time'); ?>"  id="basic_example_2" value="" style=" width:48%; margin:0 1%" placeholder="<?= lang('to') ?>" type="text">
                    </div>
                </div>
                <div class="span4">
                    <div class="span4"><?= lang('count') ?>:</div>
                    <div class="span8">

                        <input name="from_count"  value="<?= $this->input->post('from_count'); ?>"  style=" width:48%; margin:0 1%" placeholder="<?= lang('from') ?>" type="text">
                        <input name="to_count"  value="<?= $this->input->post('to_count'); ?>"  style=" width:48%; margin:0 1%" placeholder="<?= lang('to') ?>" type="text">

                    </div>
                </div>
            </div>

            <div class="row-form">
                <div class="span4">
                    <div class="span4"><?= lang('operator') ?>:</div>
                    <div class="span8">

                        <?= form_dropdown('safa_ito_id', ddgen('safa_itos', array('safa_ito_id', name())), set_value('safa_ito_id', '0'),"style='width:200px'") ?>

                    </div>
                </div>
                <div class="span4">
                    <div class="span4"><?= lang('segment_type') ?>:</div>
                    <div class="span8">
                        <?= form_dropdown('safa_internalsegmenttype_id', ddgen('safa_internalsegmenttypes', array('safa_internalsegmenttype_id', name())), set_value('safa_internalsegmenttype_id', '0'), "id='internalsegmenttypes'  ") ?>
                    </div>
                </div>
                <div class="span4">
                    <div class="span4"><?= lang('operation_order_id') ?>:</div>
                    <div class="span8">
                        <input type="text" name="operator_reference" value="<?= $this->input->post('operator_reference') ?>" class="" />
                    </div>
                </div>
            </div>

            <div class="row-form">
                <div class="span4">
                    <div class="span4"><?= lang('contract') ?>:</div>
                    <div class="span8">

                        <?= form_dropdown('safa_uo_contract_id', ddgen('safa_uo_contracts', array('safa_uo_contract_id', name()), array()), set_value('safa_uo_contract_id', '0'), "style='width:200px'") ?>

                    </div>
                </div>
                <div class="span4">
                    <div class="span4"><?= lang('from_city') ?>:</div>
                    <div class="span8">
                        <?= form_dropdown('from_city', ddgen('erp_cities', array('erp_city_id', name()), array('erp_country_id' => '966'), array(name(), 'asc')), set_value('from_city', '0'), "id=from_city") ?>
                    </div>
                </div>
                <div class="span4">
                    <div class="span4"><?= lang('from_hotel') ?>:</div>
                    <div class="span8">
                        <?= form_dropdown('from_erp_hotel_id', $this->trips_reports_model->get_related_hotels_to_contracts(session('uo_id'), $this->input->post('from_city')), set_value('from_erp_hotel_id', '0'), "id=from_hotel") ?>
                    </div>
                </div>
            </div>

            <div class="row-form">
                <div class="span4">
                    <div class="span4"><?= lang('port') ?>:</div>
                    <div class="span8">
                        <?= form_dropdown('erp_port_id', ddgen('erp_ports', array('erp_port_id', name()), array("country_code" => "SA"), array(name(), 'asc')), set_value('erp_port_id', '0')) ?>
                    </div>
                </div>
                <div class="span4">
                    <div class="span4"><?= lang('to_city') ?>:</div>
                    <div class="span8">

                        <?= form_dropdown('to_city', ddgen('erp_cities', array('erp_city_id', name()), array('erp_country_id' => '966'), array(name(), 'asc')), set_value('to_city', '0'), "id=to_city") ?>

                    </div>
                </div>
                <div class="span4">
                    <div class="span4"><?= lang('to_hotel') ?>:</div>
                    <div class     ="span8">
                        <? //ddgen('erp_hotels', array('erp_hotel_id', name()), false, array(name(), 'asc')) ?>
                        <?= form_dropdown('to_erp_hotel_id', $this->trips_reports_model->get_related_hotels_to_contracts(session('uo_id'), $this->input->post('erp_city_id')), set_value('to_erp_hotel_id', '0'), "id=to_hotel") ?>
                    </div>
                </div>
            </div>
            <? if (PHASE > 1): ?>
                <div class="row-form" >
                    <div class="span4">
                        <div class="span4"><?= lang('agent') ?>:</div>
                        <div class="span8">
                            <?= form_dropdown('safa_uo_user_id', ddgen('safa_uo_users', array('safa_uo_user_id', name()), array('safa_uo_usergroup_id' => AGENT_USERGROUP, 'safa_uo_id' => session("uo_id"))), set_value('safa_uo_user_id', '0')) ?>    
                        </div>
                    </div>

                    <div class="span4">
                        <div class="span4"><?= lang('status') ?>:</div>
                        <div class="span8">
                            <?= form_dropdown('safa_internalsegmentestatus_id', ddgen('safa_internalsegmentstatus', array('safa_internalsegmentstatus_id', name())), set_value('safa_internalsegmentestatus_id', '0')) ?>

                        </div>
                    </div>  
                </div>
            <? endif; ?>   
            <div class="row-form">
                <div class="span1"><?= lang('choose_cols') ?></div>
                <div class="span11">
                    <input name="cols[]" value="uo_name" type="checkbox" <?= (in_array('uo_name', $cols)) ? "checked=checked" : "" ?> > <?= lang('uo_name') ?> &nbsp;&nbsp; 
                    <input name="cols[]" value="code" type="checkbox" <?= (in_array('code', $cols)) ? "checked=checked" : "" ?> > <?= lang('trip_id') ?> &nbsp;&nbsp; 
                    <input name="cols[]" value="supervisor" type="checkbox" <?= (in_array('supervisor', $cols)) ? "checked=checked" : "" ?> > <?= lang('supervisor') ?> &nbsp;&nbsp; 
                    <input name="cols[]" value="contract" type="checkbox" <?= (in_array('contract', $cols)) ? "checked=checked" : "" ?> > <?= lang('contract') ?> &nbsp;&nbsp; 
                    <input name="cols[]" value="operator" type="checkbox" <?= (in_array('operator', $cols)) ? "checked=checked" : "" ?> > <?= lang('operator') ?> &nbsp;&nbsp; 
                    <input name="cols[]" value="confirmation_number" type="checkbox" <?= (in_array('confirmation_number', $cols)) ? "checked=checked" : "" ?> > <?= lang('confirmation_number') ?> &nbsp;&nbsp; 
                    <input name="cols[]" value="operation_order_id" type="checkbox" <?= (in_array('operation_order_id', $cols)) ? "checked=checked" : "" ?> > <?= lang('operation_order_id') ?> &nbsp;&nbsp; 
                    <input name="cols[]" value="buses_count" type="checkbox" <?= (in_array('buses_count', $cols)) ? "checked=checked" : "" ?> > <?= lang('buses_count') ?> &nbsp;&nbsp; 
                    <input name="cols[]" value="segment_type" type="checkbox" <?= (in_array('segment_type', $cols)) ? "checked=checked" : "" ?> > <?= lang('segment_type') ?> &nbsp;&nbsp; 
                    <input name="cols[]" value="starting" type="checkbox" <?= (in_array('starting', $cols)) ? "checked=checked" : "" ?> > <?= lang('starting') ?> &nbsp;&nbsp; 
                    <!--<input name="cols[]" value="ending" type="checkbox" <?= (in_array('ending', $cols)) ? "checked=checked" : "" ?> > <?= lang('ending') ?> &nbsp;&nbsp;--> 
                    <input name="cols[]" value="from" type="checkbox" <?= (in_array('from', $cols)) ? "checked=checked" : "" ?> > <?= lang('from') ?> &nbsp;&nbsp; 
                    <input name="cols[]" value="to" type="checkbox" <?= (in_array('to', $cols)) ? "checked=checked" : "" ?> > <?= lang('to') ?> &nbsp;&nbsp;                     
                    <input name="cols[]" value="seats" type="checkbox" <?= (in_array('seats', $cols)) ? "checked=checked" : "" ?> > <?= lang('seats') ?> &nbsp;&nbsp;
                    <input name="cols[]" value="individuals_count" type="checkbox" <?= (in_array('individuals_count', $cols)) ? "checked=checked" : "" ?> > <?= lang('individuals_count') ?> &nbsp;&nbsp;
                    <input name="cols[]" value="passports_count" type="checkbox" <?= (in_array('passports_count', $cols)) ? "checked=checked" : "" ?> > <?= lang('passports_count') ?> &nbsp;&nbsp;
                    
<!--                    <input name="cols[]" value="status" type="checkbox" <?= (in_array('status', $cols)) ? "checked=checked" : "" ?> > <?= lang('status') ?> &nbsp;&nbsp;-->
                    <input name="cols[]" value="agent" type="checkbox" <?= (in_array('agent', $cols)) ? "checked=checked" : "" ?> > <?= lang('agent') ?> &nbsp;&nbsp;
                    <input name="cols[]" value="drivers" type="checkbox" <?= (in_array('drivers', $cols)) ? "checked=checked" : "" ?> > <?= lang('drivers') ?> &nbsp;&nbsp;
                    
            		<!--
            		<? if (PHASE > 1): ?>
                        <input name="cols[]" value="status" type="checkbox" checked="checked"> <?= lang('status') ?> &nbsp;&nbsp;
                    <? endif; ?>

                    <? if (PHASE > 1): ?>
                        <input name="cols[]" value="agent" type="checkbox" checked="checked"> <?= lang('agent') ?> &nbsp;&nbsp;
                    <? endif; ?>   
                	-->
                
                </div>
            </div>

            <div class="toolbar bottom TAC">
                <button class="btn btn-primary"><?= lang('search') ?></button>
            </div>
        </div>
    </div>


    <div class="widget">
        
        <div class="widget-header">
	
	            <div class="widget-header-icon Fright">
	                <span class="icos-pencil2"></span>
	            </div>
	
	            <div class="widget-header-title Fright">

			    <div class="path-container Fright">
			        <div class="icon"><i class="icos-pencil2"></i></div> 
			         <div class="path-name Fright">
			                <?= lang('all_movements') ?>
			        </div>    
			        
			       
			    </div>
	         </div>
	         
	          <div class="btn-group" style="float:<?= lang('XALIGN') ?>; margin:1px 0 0 1px;color:#fff">
                <? if ( count($trips_ds)) :?>
                    <input type="submit" name="print_report" value="<?= lang('print') ?>" class="btn" style="height: 28px"/>
                    <input type="submit" name="excel_export" value="<?= lang('export') ?>" class="btn" style="height: 28px"/>
                <? endif;?>
            </div> 
	                

	    </div>
        
        
        
        <?= form_close() ?> 
        <div class="widget-container">
            <div class='table-responsive' >
            <table class="fsTable" cellpadding="0" cellspacing="0">
                <thead>
                
                
                <tr >
                        <th rowspan="2"><?= lang('trip_code') ?></th>
                          <? if (in_array('uo_name', $cols)): ?>
                            <th rowspan="2"><?= lang('uo_name') ?></th>
                        <? endif; ?>                    
                      	<? if (in_array('code', $cols)): ?>
                            <th rowspan="2"><?= lang('trip_id') ?></th>
                        <? endif; ?>
                        
                        <? if (in_array('supervisor', $cols)): ?>
                            <th rowspan="2"><?= lang('supervisor') ?></th>
                        <? endif; ?>  
                        
                        <? if (in_array('contract', $cols)): ?>
                            <th rowspan="2"><?= lang('contract') ?></th>
                        <? endif; ?>

                        <? if (in_array('operator', $cols)): ?>
                            <th rowspan="2"> <?= lang('operator') ?></th>
                        <? endif; ?>                                   

						<? if (in_array('confirmation_number', $cols)): ?>
                            <th rowspan="2"><?= lang('confirmation_number') ?></th>
                        <? endif; ?>
                        
                        <? if (in_array('operation_order_id', $cols)): ?>
                            <th rowspan="2"><?= lang('operation_order_id') ?></th>
                        <? endif; ?>
                            
                        <? if (in_array('buses_count', $cols)): ?>
                            <th rowspan="2"><?= lang('buses_count') ?></th>
                        <? endif; ?>

                        <? if (in_array('segment_type', $cols)): ?>
                            <th rowspan="2"><?= lang('segment_type') ?></th>
                        <? endif; ?>

                        <? if (in_array('from', $cols)): ?>
                            <th rowspan="2"><?= lang('from') ?></th>
                        <? endif; ?>  


                        <? if (in_array('to', $cols)): ?>
                            <th rowspan="2"><?= lang('to') ?></th>
                        <? endif; ?>


                        <? if (in_array('starting', $cols)): ?>
                            <th rowspan="2"><?= lang('starting') ?></th>
                        <? endif; ?>

                        <!--
                        <? if (in_array('ending', $cols)): ?>
                            <th rowspan="2"><?= lang('ending') ?></th>
                        <? endif; ?>
                        -->
                        
                        
                        
                        <? if (in_array('individuals_count', $cols)): ?>
                            <th rowspan="2"><?= lang('individuals_count') ?></th>
                        <? endif; ?>  
                        
                         <? if (in_array('passports_count', $cols)): ?>
                            <th rowspan="2"><?= lang('passports_count') ?></th>
                        <? endif; ?>      
                        
                        
                        <? if (in_array('seats', $cols)): ?>
                            <th rowspan="2"><?= lang('seats') ?></th>
                        <? endif; ?>  
                            
                            
                         <? if (in_array('agent', $cols) || in_array('status', $cols)): ?>
                            <th colspan="2"><?= lang('agent') ?></th>
                        <? endif; ?>  
                         
                        <? if (in_array('drivers', $cols)): ?>
                            <th rowspan="2"><?= lang('drivers') ?></th>
                        <? endif; ?>  
                        
                       
                            
                            
                    </tr>
                
                		
                         
                            
                         <? if (in_array('agent', $cols) || in_array('status', $cols)): ?>
                         <tr>
                            <th ><?= lang('from') ?></th>
                        
                                <th ><?= lang('to') ?></th>
                                </tr>
                        <? endif; ?>

                      
                            
                            
                            
                    
                    
                    
                </thead>
                <tbody>
                    <? foreach ($trips_ds as $val): ?>
                        <tr>
      
                            <td><?= strlen($val['trip_name'])? $val['trip_name']:$val['trip_title'] ?></td>
                            <? if (in_array('uo_name', $cols)): ?>
                                <td><?= $val['uo_name'] ?></td>
                            <? endif; ?>                      
                           <? if (in_array('code', $cols)): ?>
                           
                           		
                                <td>
                                <?php 
                           		if($val['safa_internalsegmenttype_id']==1 || $val['safa_internalsegmenttype_id']==2) {
                           		?>
                           		
                                <?php 
								$status_color = $this->flight_states->getFlightColor($val['fs_flight_id']);
								?>
						
						
                                    <span style="text-align: center ;background-color:#<?= $status_color?>" class="label">
                                        <?= $val['flight_number'] ?> <?= $val['safa_transporters_code'] ?><br /><?= $val['transporter_name'] ?>
                                    </span>
                                    
                                <?php }?>
                                </td>
                            <? endif; ?>
                            
                            
                            <? if (in_array('supervisor', $cols)): ?>
                                <td>
                                <?php 
                                if($val['supervisor']!='') {
                                        echo $val['supervisor'];
                                } else {
                                        echo $val['trip_supervisors'];
                                }
                                ?>
                                </td>
                            <? endif; ?>
                            
                            
                            <? if (in_array('contract', $cols)): ?>
                                <td><?= $val['contract_name'] ?></td>
                            <? endif; ?>

                            <? if (in_array('operator', $cols)): ?>
                                <td><?= $val['operator_name'] ?></td>
                            <? endif; ?>

							<? if (in_array('confirmation_number', $cols)): ?>
                                <td><?= $val['confirmation_number'] ?></td>
                            <? endif; ?>
                            
                            <? if (in_array('operation_order_id', $cols)): ?>
                                <td>
								<!-- <a href="<?= site_url("trip_details/details/".$val['trip_id']) ?>#tabs-3" target="_blank"><?= $val['operator_reference'] ?></a>-->
                                <?= $val['operator_reference'] ?>
                                </td>
                            <? endif; ?>
                                
                            <? if (in_array('buses_count', $cols)): ?>
                                <td>
                                <?= $val['buses_count'] ?>
                                </td>
                            <? endif; ?>    

                            <? if (in_array('segment_type', $cols)): ?>
                                <td><?= $val['segment_name'] ?></td>
                            <? endif; ?>


                            <? if (in_array('from', $cols)): ?>
                                <td><?
                                	$from_place='';
                                    switch ($val['safa_internalsegmenttype_id']):

                                        case 1:
                                        	$from_place = $val['erp_port_id'];
                                            echo(lang('port').': '.element($val['erp_port_id'], $ports));
                                            break;

                                        case 2: 
                                        	$from_place = $val['erp_start_hotel_id'];
                                        	echo("<font class=city_color>".$val['start_city_name']."</font><br>");
                                                echo(element($val['erp_start_hotel_id'], $hotels));
                                            break;

                                        case 3: 
                                        	$from_place = $val['erp_start_hotel_id'];
                                        	echo("<font class=city_color>".$val['start_city_name']."</font><br>");
                                                echo(element($val['erp_start_hotel_id'], $hotels));
                                            break;

                                        case 4: 
                                        	$from_place = $val['erp_start_hotel_id'];
                                                echo("<font class=city_color>".$val['start_city_name']."</font><br>");
                                                echo(element($val['erp_start_hotel_id'], $hotels));
                                            break;

                                    endswitch;
                                    ?>
                                </td>
                            <? endif; ?>                                


                            <? if (in_array('to', $cols)): ?>
                                <td><?
                                	$to_place='';
                                    switch ($val['safa_internalsegmenttype_id']):

                                        case 1:
                                        		$to_place = $val['erp_end_hotel_id'];
                                                echo("<font class=city_color>".$val['end_city_name']."</font><br>");
                                                echo(element($val['erp_end_hotel_id'], $hotels));
                                            break;

                                        case 2: 
                                        		$to_place = $val['erp_port_id'];
                                                echo(lang('port').': '.element($val['erp_port_id'], $ports));
                                            break;

                                        case 3: 
                                        		$to_place = $val['safa_tourism_place_id'];
                                                echo(element($val['safa_tourism_place_id'], $places));
                                            break;

                                        case 4: 
                                        		$to_place = $val['erp_end_hotel_id'];
                                                echo("<font class=city_color>".$val['end_city_name']."</font><br>");
                                                echo(element($val['erp_end_hotel_id'], $hotels));
                                            break;

                                    endswitch;
                                    ?>                                
                                </td>
                            <? endif; ?>
                                
                                
                                
                            <? if (in_array('starting', $cols)): ?>
                                <td><span style="direction: ltr">
                                <?= get_date($val['start_datetime']) . " " . get_time($val['start_datetime']) ?></span></td>
                            <? endif; ?>
                            
                            <!--
                            <? if (in_array('ending', $cols)): ?>
                                <td>
                                    <?if($this->trips_reports_model->get_endtime($val['end_datetime'])<='02:00:00' && $this->trips_reports_model->get_endtime($val['end_datetime'])>='00:00:00'):?>
                                           <span class="label"style="direction: ltr;background-color:orange;"><?= get_date($val['end_datetime'])." ".get_time($val['end_datetime']) ?></span>
                                    <?elseif($this->trips_reports_model->get_endtime($val['end_datetime'])<'00:00:00'):?>
                                           <span class="label"style="direction: ltr;background-color:red;"><?= get_date($val['end_datetime'])." ".get_time($val['end_datetime']) ?></span>
                                    <?else:?>
                                           <span style="direction: ltr"><?= get_date($val['end_datetime'])." ".get_time($val['end_datetime']) ?></span>
                                    <? endif; ?>
                                    
                            <? endif; ?>
                            -->

                            
                            <? if (in_array('individuals_count', $cols)): ?>
                                <td><?= strlen($val['individuals_count'])?$val['individuals_count']:((int)$val['adult_seats']+(int)$val['child_seats']+(int)$val['baby_seats']) ?></td>
                            <? endif; ?>
                            
                            <? if (in_array('passports_count', $cols)): ?>
                                <td><?= $val['passports_count'] ?></td>
                            <? endif; ?>
                            
                            <? if (in_array('seats', $cols)): ?>
                                <td><?= strlen($val['seats_count'])?$val['seats_count']:((int)$val['adult_seats']+(int)$val['child_seats']) ?></td>
                            <? endif; ?>
                            
                            
                            <!-- this updates for phase 2  assign a new aggents ---> 

                         	<? if (in_array('agent', $cols) || in_array('status', $cols)): ?>
                                    <td>
                                        <? //if ($usergroup_id == 1 || $usergroup_id == 2) { ?>
                                            <a style="display:none"   href="javascript:void(0)"  class="icon-remove roleback-agent"  title="<?= lang('roll_back_user') ?>" onclick="rollback_agent_view(<? if ($val['safa_uo_user_id'] == ''): ?>0<? else: ?><?= $val['safa_uo_user_id'] ?><? endif; ?>, this)"></a> 
                                        <? //}; ?> 
                                        <div class='assign-agent span12'   role='assign-agent' id='<?= $val['safa_internalsegment_id'] ?>'  style='float:none !important '  >
                                            <? if ($val['safa_uo_user_id'] == ''): ?>
                                                
                                            <? else: ?>
                                            <?php if($val['safa_uo_user_id']!=''){ echo item('safa_uo_users', name(), array('safa_uo_users.safa_uo_user_id' => $val['safa_uo_user_id']));} ?>                                    
                                            <? endif; ?>
                                            <? //if (isset($usergroup_id)) { ?>
                                                <? //if ($usergroup_id == 1 || $usergroup_id == 2) { ?>
                                                    <br>
                                                    <? if ($val['safa_uo_user_id'] == ''): ?> 
                                                        <? $title = lang('assign_uo_user') ?>
                                                    <? else: ?>
                                                        <? $title = lang('change_uo_user') ?>
                                                    <? endif; ?>
                                                    <a class="icon-pencil"   href="javascript:void(0)"  title="<?= $title ?>" onclick="get_agent_view(<? if ($val['safa_uo_user_id'] == ''): ?>0<? else: ?><?= $val['safa_uo_user_id'] ?><? endif; ?>, this, <?= $val['safa_internalsegmenttype_id'] ?>, <?= $from_place ?>, <?= $to_place ?>)"></a>
                                                </div>   
                                            <? //}; ?>
                                        <? //}; ?>
                                    </td>
                                <? endif; ?> 
                            
                            <!-- this updates for phase 2  updating the status  seggment--->     
                         <? if (in_array('agent', $cols) || in_array('status', $cols)): ?>
                                    <td>
                                        <div class="span12" <? if ($val['status_notes']): ?>title='<?= $val['status_notes'] ?>'<? endif; ?> id='chg-status<?= $val['safa_internalsegment_id'] ?>' role='change-status'   class="change_status"  >
                                            <?= $val['status'] ?> - <?php if($val['safa_uo_agent_id']!=''){ echo item('safa_uo_users', name(), array('safa_uo_users.safa_uo_user_id' => $val['safa_uo_agent_id']));} ?> 
                                            <br>
                                            <a href="#edit-segment-status" title="<?= lang('update_segment_status') ?>" onclick="openStatusChangepanal(<?= $val['safa_internalsegment_id'] ?>,<?= $val['safa_internalsegmentestatus_id'] ?>)"  class="icon-pencil"  role="button" data-toggle="modal"></a>
                                        </div>

                                        
                                    </td>
                                <? endif; ?>                              

                             <? if (in_array('drivers', $cols)): ?>
                          	<td class='TAC'>
									 	
                                    <?php 
                                    $this->internalsegments_drivers_and_buses_model->safa_internalsegments_id=$val['safa_internalsegment_id'];
                                    $drivers_and_buses_rows= $this->internalsegments_drivers_and_buses_model->get();
                                    $count_drivers_and_buses = count($drivers_and_buses_rows);
                                    if($count_drivers_and_buses>0) {
                                    ?>	
                                    X <?php echo $count_drivers_and_buses; ?> 
									    
                                        <a title='<?= lang('drivers_and_buses') ?>' href="<?= site_url('uo/all_movements/drivers_and_buses/' . $val['safa_internalsegment_id']) ?>" style="color:black; direction:rtl">
                                             <img src="<?= IMAGES2 ?>/car.png" align="left" >
                                        </a> 
                                        
                                        <br/> <br/>
									 <?php 
									 foreach($drivers_and_buses_rows as $drivers_and_buses_row) {
									 	echo $drivers_and_buses_row->safa_transporters_driver_name .' - '. $drivers_and_buses_row->safa_transporters_driver_phone.'<br/>';
									 }
									 ?>	    
                                            
                                    <?php 
                                    } else {
                                    ?>	

                                    <a title='<?= lang('drivers_and_buses') ?>'  href="<?= site_url('uo/all_movements/drivers_and_buses/' . $val['safa_internalsegment_id']) ?>" >
                                             <?= lang("global_add"); ?>
                                        </a>
									 	
                                    <?php 	
                                    }
                                    ?>
                          </td>   
                          <? endif ?>
                        </tr>
                    <? endforeach; ?>
                </tbody>
            </table>      
        </div>
        </div>
    </div>

</div>
<!--change status panal -->
<div id="edit-segment-status" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
    <div class="modal-header">
             <button type="button" class="close close_window" data-dismiss="modal" aria-hidden="true">×</button> 
        <h3 id="myModalLabel"><?= lang('update_segment_status') ?></h3>
    </div>
    <?= form_open('uo/all_movements/update_segment_status') ?>
    <div class="row-fluid">
        <div class="row-form">
            <span class="span4"><?= lang('agent').' ( '.lang('to').' )' ?></span>
            <span class="span8">
                <?= form_dropdown('safa_uo_users', ddgen('safa_uo_users', array('safa_uo_user_id', name()), array('safa_uo_usergroup_id' => AGENT_USERGROUP, "safa_uo_id" => session('uo_id'))), set_value('safa_uo_users', '0'), "class='validate[required]'") ?>
            </span>
        </div>
        <div class="row-form">
            <span class="span4"><?= lang('status') ?></span>
            <span class="span8">
                <?= form_dropdown('internal_segment_status', ddgen('safa_internalsegmentstatus', array('safa_internalsegmentstatus_id', name())), set_value('internal_segment_status', '0'), "class='validate[required]'") ?>
            </span>
        </div>
        <div class="row-form">
            <span class="span4"><?= lang('notes') ?></span>
            <span class="span8">
                <textarea name="update_segment_status_notes" ><?= set_value('update_segment_status_notes') ?></textarea>
            </span>
            <input type="hidden" name="internal_segment_id" />
        </div>
    </div>                   
    <div class="modal-footer" align="center" >
        <button class="btn btn-primary"><?= lang('global_submit') ?></button>
        <a type="button" class="close_window btn-primary btn" data-dismiss="modal" aria-hidden="true"><?=lang('global_close')?></a>
    </div>
    <?= form_close() ?> 
</div>

<!--change status panal -->

<script>
  $("#from_city").change(function() {
        $.get("<?= site_url('uo/ajax/get_city_hotels') ?>/" + $(this).val(), function(data) {
            $("#from_hotel").html(data);
        });
    });                                  
</script>
<script>
    $("#to_city").change(function() {
        $.get("<?= site_url('uo/ajax/get_city_hotels') ?>/" + $(this).val(), function(data) {
            $("#to_hotel").html(data);
        });
    });
</script>

<? if (PHASE > 1): ?>
    <script>
  // this script for updating and assign agents //
        function get_agent_view(uo_user_id, btn, safa_internalsegmenttype_id, from_place, to_place) {
            $.ajax({
                url: '<?= site_url('uo/all_movements/select_agent_ajax') ?>',
                data: {uo_user_id: uo_user_id, safa_internalsegmenttype_id: safa_internalsegmenttype_id, from_place: from_place, to_place: to_place},
                type: 'post',
                beforeSend: function() {
                    // alert('will change'); 
                },
                success: function(result) {
                    var assign_div = $("#" + btn.parentNode.id);
                    assign_div.html(result);
                    var btn_update_agent = $(assign_div).find('button');
                    var list_agent = $(assign_div).find('select');
                    $(btn_update_agent).click(function() {
                        var agent_id = list_agent.val();
                        var segment_id = assign_div.attr('id');
                        update_agent_segment(segment_id, agent_id);
                    });
                    /**close btn****/
                    var role_back_btn = $(assign_div).parent().find('.roleback-agent');
                    $(role_back_btn).show();
                    /******/
                },
            });
        }
    </script>
    <script>
        function update_agent_segment(segment_id, agent_id) {
            $.ajax({
                url: '<?= site_url('uo/all_movements/set_agent_segment_ajax') ?>',
                data: {
                    segment_id: segment_id,
                    agent_id: agent_id,
                },
                type: 'post',
                beforeSend: function() {
                    // alert('will be updated'); 
                },
                success: function(result) {
                    if (result != "0")
                    {
                        $("#" + segment_id).html(result);
                        $("#" + segment_id).parent().find('.roleback-agent').attr('style', 'display:none');
                    }
                }


            });
        }
        function rollback_agent_view(uo_id, btn) {
            var assign_div = $(btn).parent().find('.assign-agent').attr('id');
            $.ajax({
                url: '<?= site_url('uo/all_movements/rollback_agent_segment_ajax') ?>',
                data: {
                    uo_id: uo_id,
                },
                type: 'post',
                beforeSend: function() {
                    // alert('will be updated'); 
                },
                success: function(result) {
                    $("#" + assign_div).html(result);
                    $(btn).attr('style', 'display:none')
                }


            });

        }
    </script>

 <script>
 /*this script responsible for updating the internalsegment status */
        function  openStatusChangepanal(segment_id,status_id) {
            /*to refresh  the form acttions*/
                 var form = $('#edit-segment-status').find('form'); 
                 refresh_form_actions(form);
          //assign the the values for form inputs//
            //$('#edit-segment-status').find('[name="internal_segment_status"]').val(status_id);
            $('#edit-segment-status').find('[name="internal_segment_id"]').val(segment_id);
            //this request will be submited to select  the las agent of the segment//
            $.get('<?= site_url('uo/all_movements/get_segment_agent') ?>/' + segment_id, function(data) {
            		$('#edit-segment-status').find('[name="safa_uo_users"]').html(data.safa_uo_users_options);
            	
                    $('#edit-segment-status').find('[name="safa_uo_users"]').val(data.safa_uo_user_id);
                    $('#edit-segment-status').find('[name="internal_segment_status"]').val(data.safa_internalsegmentestatus_id);
                    $('#edit-segment-status').find('[name="update_segment_status_notes"]').html(data.status_notes);
                },'json');

            
            /*****************************implement the validation************************************************************/
            $(form).validationEngine('attach',
                    {
                        promptPosition: "bottomLeft",
                        scroll:false
                    }
            );
            /*************************assign the form ajax submation *************************************************************/
            form.submit(function() {
                var valid = $(form).validationEngine('validate');
              if (valid == true){
                    $(this).ajaxSubmit({
                        beforeSubmit: function() {
                            //alert('willsubmit');
                        },
                        dataType: 'json',
                        success: function(data) {

                            if (data.updated != "0") {
                                $('#edit-segment-status').find('.close_window').click();// to close the popup after submation//
                                $('#chg-status' + segment_id).html(data.content);
                                if (data.notes != undefined)
                                    $('#chg-status' + segment_id).attr('title', data.notes);

                            }

                        },
                    });
                }
                return false;
            })

        }
  //this code for refresh the form of update status//  
        function refresh_form_actions(form)
        {
                $(form).resetForm();
             // this code to prevent the form from resubmition // 
                $(form).unbind('submit');
                $(form).validationEngine('hideAll');
                $(form).validationEngine('detach');  
            
        }
    </script>    
<? endif; ?>

<script>
    $(document).ready(function() {
        $("#internalsegmenttypes").change(function() {
            if ($("#internalsegmenttypes").val() == 1) {
                $("#from_city").attr("disabled", "disabled");
                $("#from_hotel").attr("disabled", "disabled");
                $("#to_city").removeAttr("disabled");
                $("#to_hotel").removeAttr("disabled");
            }
            else if ($("#internalsegmenttypes").val() == 2) {
                $("#to_city").attr("disabled", "disabled");
                $("#to_hotel").attr("disabled", "disabled");
                $("#from_city").removeAttr("disabled");
                $("#from_hotel").removeAttr("disabled");
            }
            else if ($("#internalsegmenttypes").val() == 3) {
                $("#to_city").attr("disabled", "disabled");
                $("#to_hotel").attr("disabled", "disabled");
                $("#from_city").removeAttr("disabled");
                $("#from_hotel").removeAttr("disabled");
            }
            else {
                $("#from_city").removeAttr("disabled");
                $("#from_hotel").removeAttr("disabled");
                $("#to_city").removeAttr("disabled");
                $("#to_hotel").removeAttr("disabled");
            }

        });
    });
</script>
<script>
                                    $(document).ready(function() {
                                        $('.fancybox').fancybox({
                                            afterClose: function() {
//                                                location.reload();
                                            }
                                        });
                                    });
</script>


<script>


 if ($(".fsTable").length > 0) {

	 	//By Gouda, TO remove the warning message.
	 	$.fn.dataTableExt.sErrMode = 'throw';

        $(".fsTable").dataTable({

            bSort: false,
            bAutoWidth: true,
            "iDisplayLength":50, 
            "aLengthMenu": [5, 10, 25, 50, 100, 200, 500], // can be removed for basic 10 items per page
            "bFilter": true,
            "sPaginationType": "full_numbers",
            "aoColumnDefs": [{"bSortable": false,
            "aTargets": [-1, 0]}],
			"aDataSort": [3]
            });
    }
</script>