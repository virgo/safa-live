<?= form_open(false, 'method="get"') ?>
<div class="row-fluid">
    <div class="widget">
        <div class="widget-header">
            <div class="widget-header-icon Fright">
                <span class="icos-pencil2"></span>
            </div>
            <div class="widget-header-title Fright">
                <?= lang('global_search') ?>
            </div>
        </div>
        <div class="widget-container">
            
            <div class="row-fluid">
                <div class="span12">
                    <div class="span4">
                        <?= lang($module.'_crm_license_id') ?>
                    </div>
                    <div class="span8">
                        <?= form_dropdown('crm_license_id', ddgen('crm_licenses', array('crm_license_id', 'name')), set_value('crm_license_id')) ?>
                    </div>
                </div>
            </div>
            
            <div class="row-fluid">
                <div class="span12">
                    <div class="span4">
                        <?= lang($module.'_over_credit') ?>
                    </div>
                    <div class="span8">
                        <?= form_input('over_credit', set_value('over_credit')) ?>
                    </div>
                </div>
            </div>
            
            <div class="row-fluid">
                <div class="span12">
                    <div class="span4">
                        <?= lang($module.'_status') ?>
                    </div>
                    <div class="span8">
                        <?= form_dropdown('status', ddgen('crm_status', array('crm_status_id', name())), set_value('status')) ?>
                    </div>
                </div>
            </div>
                
            
            <div class="row-fluid">
                <div class="span12">
                    <input type="submit" name="search" value="<?= lang('global_submit') ?>" class="btn" />
                </div>
            </div>
                
                
        </div>
    </div>
</div><?= form_close() ?>