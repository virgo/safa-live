<span class="span8"></span>
<div class="row-fluid">
        
<?= form_open_multipart(false, 'id="validate"') ?>
     
    <div class="widget">
        <div class="path-container Fright">
            <div class="path-name Fright">
                <a href="<?= site_url('admin/dashboard') ?>"><?= lang('global_system_management') ?></a>
            </div>
            <div class="path-arrow Fright"></div>
            <div class="path-name Fright">
                <a href="<?= site_url('admin/safa_eas') ?>"><?= lang('menu_external_agent') ?></a>
            </div>
            <div class="path-arrow Fright"></div>
            <div class="path-name Fright">
                <?= lang('global_'.$module)?>
            </div>
        </div>
    </div>
    
    
    <div class="widget">
        <div class="widget-header">
            <div class="widget-header-icon Fright"><span class="icos-pencil2"></span></div>
            <div class="widget-header-title Fright"><?= lang('global_'.$module)?></div>
    	</div>
        <div class="block-fluid">
            <? if (validation_errors()): ?>
                <?= validation_errors(); ?>
            <? endif ?> 
            
            <div class="row-form">
                <div class="span6">
                    <div class="span4">
                        <?= lang($module.'_crm_license_id') ?>:
                    </div>
                    <div class="span8">
                        <?= form_dropdown('crm_license_id', ddgen('crm_licenses', array('crm_license_id', 'name')), set_value("crm_license_id", $item->crm_license_id), 'class="validate[required]"') ?>
                    </div>
                </div>
                
                <div class="span6">
                    <div class="span4">
                        <?= lang($module.'_over_credit') ?>:
                    </div>
                    <div class="span8">
                        <?= form_input('over_credit', set_value("over_credit", $item->over_credit), 'class="validate[required]"') ?>
                    </div>
                </div>
            </div>
            <div class="row-form">
                <div class="span6">
                    <div class="span4">
                        <?= lang($module.'_status') ?>:
                    </div>
                    <div class="span8">
                        <?= form_dropdown('status', ddgen('crm_status', array('crm_status_id', name())), set_value("status", $item->status), 'class="validate[required]"') ?>
                    </div>
                </div>
                
            </div>
            
            
        </div>
    </div>
    
    <div class="toolbar bottom TAC">
        <input type="submit" name="submit" value="<?= lang('global_submit') ?>" class="btn btn-primary"> &nbsp;
        <input type ="button" 
               value="<?= lang('global_back') ?>"
               class="btn btn-primary" 
               onclick="window.location = '<?= site_url($module.'/index/'.$ea_id) ?>'">
       
    </div>
<?= form_close() ?>       
</div> 
