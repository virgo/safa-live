<style>
    .coll_open {
        cursor:pointer;
        float:left;
    }
    .coll_close {
        cursor:pointer;
        float:left;
    }

    .fancybox-outer, .fancybox-inner {
        overflow-y: auto !important;
    }

    .coll_open {
        cursor:pointer;
        float:left;
    }
    .coll_close {
        cursor:pointer;
        float:left;
    }

    .fancybox-outer, .fancybox-inner {
        overflow-y: auto !important;
    }
    .wizerd-div {
        border-bottom: medium none !important;
        margin: -18px 0 20px;
        padding-top: 0;
    }
    .wizerd-div a {
        background: none repeat scroll 0 0 #FAFAFA;
        border: 1px solid #D5D6D6;
        border-radius: 49px;
        color: #663300;
        display: inline-block;
        margin: -20px 5px -17px -8px;
        padding: 10px 12px 8px;
    }
    .wizerd-div2 {
        border-bottom: medium none !important;
        margin: 10px 0 -10px;
        padding-top: 0;
    }
    .wizerd-div p {
        background: none repeat scroll 0 0 #FAFAFA;
        border: 1px solid #D5D6D6;
        border-radius: 49px;
        color: #663300;
        display: inline-block;
        margin: -6px 5px -17px -8px;
        padding: 10px 12px 8px;
    }

    a {
        color: #C09853;
    }
    .resalt-group {
        margin: 25px 0.5% 0.5%;
        padding: 0.5%;
        width: 98%;
    }
    th a.btn, th input[type="button"], th input[type="submit"], th button {
        margin: 0;
        padding: 4px 12px;
    }
    .coll_close, .coll_open {
        margin-top: 0;
    }
    .chosen-container {
        margin-top: 4px;
    }
    .warning{
        color: #C09853;
    }
    .lable2{
/*        border: 1px solid #D5D6D6;*/
        border-radius: 49px;
        margin-top: -2px;
        margin-right: -8px;
        display: inline-block;
        padding: 8px 9px;
        font-size: 11.844px;
        font-weight: bold;
        line-height: 14px;
        color: #4b4d51;
        /*text-shadow: 0 -1px 0 rgba(0,0,0,0.25);*/
        white-space: nowrap;
        vertical-align: baseline;
        /*background-color: #EBDDB1;*/}

    .lable3{
/*        border: 1px solid #D5D6D6;*/
        border-radius: 49px;
        margin-top: -2px;
        margin-right: 5px;
        display: inline-block;
        padding: 8px 9px;
        font-size: 11.844px;
        /*font-weight: bold;*/
        line-height: 14px;
        color: #4f3906;
        /*text-shadow: 0 -1px 0 rgba(0,0,0,0.25);*/
        white-space: nowrap;
        vertical-align: baseline;
/*        background-color: #D5D6D6;}*/

    .row_res {margin-top: 5px;
              margin-bottom: 5px;
              border-radius: 5px 5px 5px 5px;
              padding: 6px;
              background-color:  rgb(240, 240, 240);}
    </style>



    <div class="row-fluid">              
    <div class="widget">
        <div class="widget-header">
            <div class="widget-header-icon Fright"> <span class="icos-pencil2"></span> </div>
            <div class="widget-header-title Fright">
                <?= lang('trip_details_title') ?>:
                <?= $trip->name ?>
            </div>
       	</div>

        <div class="block-fluid">
            <div class="row-form">
                <!--

<div class="span4" style="text-align:center">
<strong><?= lang('trip_details_contract') ?>:</strong>
                <?= $trip->contract ?>
</div> -->

            </div>

            <? if (ensure($supervisors)): ?>
                <? foreach ($supervisors as $supervisor): ?>
                    <div class="row-form">
                        <div class="span6">
                            <div class="span3 lable2"> <?= lang('trip_details_supervisor_name') ?>:</div>
                            <div class="span6 lable3"> <?= $supervisor->{name()} ?></div>
                        </div>
                        <div class="span6">
                            <div class="span2 lable2"><?= lang('trip_details_mobile') ?>:</div>

                            <div class="span3 lable3">   <?= $supervisor->mobile_sa ?></div>
                        </div>
                    </div>

                <? endforeach ?>
            <? endif ?>
            <? if (PHASE >= 2): ?>

                <div class="row-form">
                    <div class="span6">
                        <div class="span4 lable2"> <?= lang('trip_details_ito') ?>:</div> 
                        <div class="span8 lable3"><?php
                            foreach ($ito_rows as $ito_row) {
                                echo $ito_row->ito_name . '<br/>';
                            }
                            ?></div>
                    </div>
                    <div class="span6">
                        
                        <!--
                        <div class="span12"><div class="span4 lable2"><?= lang('trip_details_fs_status') ?>:</div>
                        <?php 
						$status_color = $this->flight_states->getFlightColor($trip->fs_flight_id);
						?>
						 <?php 
							$status_name='';
							$status_row = $this->flight_states->getStatus($trip->fs_flight_id);
							if(count($status_row)>0) {
								$status_name = $status_row->{name()} ;
							}
							?>
			
                            <span style="background: #<?= $status_color ?>" class="label label-info tipb"><?= $status_name ?></span>
                        </div>
                        -->
                        
                        <div style="margin-top: 5px;" class="span12"><div class="span3 lable2">  <strong><?= lang('trip_details_status') ?>:</strong></div>
                            <div class="span3 lable3 TAC"> <?= $trip->trip_status ?></div>
                        </div>
                    </div>
                </div>
                
                <!-- 
                <div class="row-form">
                    <div class="span4">
                        <div class="span3 lable2"> <?= lang('trip_details_adult_count') ?>:</div>
                        <div class="span3 lable3 TAC"> <?= $trip->travellers_adult_count ?></div>
                    </div>
                    <div class="span4">
                        <div class="span3 lable2"><?= lang('trip_details_child_count') ?>:</div>
                        <div class="span3 lable3 TAC"> <?= $trip->travellers_child_count ?></div>
                    </div>
                    <div class="span4">
                        <div class="span3 lable2"> <?= lang('trip_details_infant_count') ?>:</div>
                        <div class="span3 lable3 TAC"><?= $trip->travellers_infant_count ?></div>
                    </div>
                </div>
                 -->
                 
            <? endif ?>
            <br/><br/>

            <div class="resalt-group" >
                <div class="wizerd-div">
                    <br/>
                    <a><?php echo lang('uo_requests') ?></a>
                </div>

                <table   id='div_safa_trips_request_group' class="">
                    <thead>
                        <tr>
                            <th ><?= lang('safa_uo_service_id') ?></th>
                            <th ><?= lang('remarks') ?></th>
                        </tr>
                    </thead>

                    <tbody>
                        <? $safa_trips_requests_conuter = 0; ?>
                        <?php
                        foreach ($safa_trips_requests_rows as $safa_trips_requests_row) {
                            $safa_trips_requests_id = $safa_trips_requests_row->safa_trips_requests_id;
                            $safa_uo_service_id = $safa_trips_requests_row->safa_uo_service_id;
                            $safa_uo_service_name = $safa_trips_requests_row->safa_uo_service_name;
                            $remarks = $safa_trips_requests_row->remarks;
                            $safa_trips_requests_conuter = $safa_trips_requests_conuter + 1;
                            ?>

                            <tr>
                                <td >
                                <?php 
                                if ($safa_uo_service_id == 2 && $trip->reservestatus != 3) {
                                	echo "<a  target='_blank' href='". site_url('safa_trip_internaltrips/manage/'.$trip->trip_internaltrip_id)."' >$safa_uo_service_name</a>";
                                } else {
                                	echo $safa_uo_service_name;
                                }
                                ?>
                                    <? if ($safa_uo_service_id == 1) {?><? if ($trip->reservestatus === NULL) { ?>

                                            <?php
                                            if ($destination == 'uo') {
                                                if (isset($safa_passport_accommodation_rooms_rows_count_without_availability)) {
                                                    if ($safa_passport_accommodation_rooms_rows_count_without_availability > 0) {
                                                        ?>
                                                        <a class="reservation-fancy btn TAC" href="<?= site_url('hotels_reservation_orders/add') ?>">إنشاء طلب اتاحة</a>
                                                        <?php
                                                    }
                                                }
                                            }
                                            ?>

                                        <?  } else { ?>
                                            <b style="font-weight: bold;">تم ارسال طلب الحجز وحالته</b>(
                                            <? if ($trip->reservestatus == 3) : ?>
                                                <a class="btn TAC" href="<?= site_url('hotels_reservation_orders/view/' . $trip->reserveid) ?>"><?= get_reserve_status($trip->reservestatus) ?></a>
                                            <? else : ?>
                                                <?= get_reserve_status($trip->reservestatus) ?>
                                            <? endif ?>
                                            )
                                        <? } ?>

                                        

                                    <? } ?>
                                    <? if ($safa_uo_service_id == 2 && $trip->reservestatus == 1) : ?>
                                        <b style="font-weight: bold;"><a class="transport-fancy" href="<?= site_url('trip_details/generate_tit/' . $trip->safa_trip_id) ?>">إنشاء  طلب نقل</a></b>
                                    <? endif ?>
                                </td>
                                <td >
                                    <?= $remarks ?>
                                </td>
                            </tr>
                            <?
                        }
                        ?>
                    </tbody>
                </table>
            </div>

            <div class="tabs">
            
            
            <div id="tabs-1" style=" clear:both; display:table; width:100%;">
                    <div class="widget-header">
                        <div class="widget-header-icon Fright">
                            <span class="icos-pencil2">

                            </span>
                        </div>
                        <div class="widget-header-title Fright">
                            <?php echo lang('accommodation_forms') ?>
                        </div>
                        
                        <div class='coll_open Fleft' id="coll_trip_details_safa_reservation_forms"></div>
                    </div>
		            <div class="block-fluid" id="container_trip_details_safa_reservation_forms">
		            <?php 
                		if(isset($item_safa_reservation_forms_rows)) {
                			if(count($item_safa_reservation_forms_rows)>0) {
                	?>
                	<table class="" cellpadding="0" cellspacing="0" width="100%">
                	<thead>
                	<tr>
                	<th rowspan="2"><?php echo lang('safa_package_name');?></th>
                	<th rowspan="2"><?php echo lang('period');?></th>
                	<th rowspan="2"><?php echo lang('trip_details_hotels');?></th>
                	<th rowspan="1" colspan="3"><?php echo lang('travellers_count');?></th>
                	<th rowspan="2"><?php echo lang('global_actions');?></th>
                	</tr>
                	<tr>
                	<th rowspan="1"><?php echo lang('trip_details_adult_count');?></th>
                	<th rowspan="1"><?php echo lang('trip_details_child_count');?></th>
                	<th rowspan="1"><?php echo lang('trip_details_infant_count');?></th>
                	</tr>
                	</thead>
                	
                	<?php		
		               
						$safa_reservation_form_adult_count=0;
						$safa_reservation_form_child_count=0;
						$safa_reservation_form_infant_count=0;
						
						$safa_reservation_form_adult_count_total=0;
						$safa_reservation_form_child_count_total=0;
						$safa_reservation_form_infant_count_total=0;
							
                		foreach($item_safa_reservation_forms_rows as $item_safa_reservation_forms_row) {
                			
                			$this->safa_passport_accommodation_rooms_model->safa_reservation_form_id = $item_safa_reservation_forms_row->safa_reservation_form_id;
							$accommodation_hotels_rows = $this->safa_passport_accommodation_rooms_model->get_hotels();
	                		
							$accommodation_hotels_names='';
							foreach($accommodation_hotels_rows as $accommodation_hotels_row) {
								$accommodation_hotels_names = $accommodation_hotels_names.$accommodation_hotels_row->erp_hotel_name." ( ".$accommodation_hotels_row->erp_city_name." )<br/>";
							}
							
							
							
							$safa_reservation_form_adult_rows = $this->safa_group_passport_accommodation_model->get_distinct_passports_by_trip_uo_form_age(false, false, $item_safa_reservation_forms_row->safa_reservation_form_id, 'adult');
							$safa_reservation_form_child_rows = $this->safa_group_passport_accommodation_model->get_distinct_passports_by_trip_uo_form_age(false, false, $item_safa_reservation_forms_row->safa_reservation_form_id, 'child');
							$safa_reservation_form_infant_rows = $this->safa_group_passport_accommodation_model->get_distinct_passports_by_trip_uo_form_age(false, false, $item_safa_reservation_forms_row->safa_reservation_form_id, 'infant');
							
							$safa_reservation_form_adult_count = count($safa_reservation_form_adult_rows); 
							$safa_reservation_form_child_count = count($safa_reservation_form_child_rows); 
							$safa_reservation_form_infant_count = count($safa_reservation_form_infant_rows); 
							
							$safa_reservation_form_adult_count_total=$safa_reservation_form_adult_count_total+$safa_reservation_form_adult_count;
							$safa_reservation_form_child_count_total=$safa_reservation_form_child_count_total+$safa_reservation_form_child_count;
							$safa_reservation_form_infant_count_total=$safa_reservation_form_infant_count_total+$safa_reservation_form_infant_count;
							
					 ?>	
                
                        
                    <tr>
                	<td ><?php echo $item_safa_reservation_forms_row->safa_package_name; ?></td>
                	<td ><?php echo $item_safa_reservation_forms_row->safa_package_period_name; ?></td>
                	<td ><?php echo $accommodation_hotels_names; ?></td>
                	
                	<td ><?php echo $safa_reservation_form_adult_count; ?></td>
                	<td ><?php echo $safa_reservation_form_child_count; ?></td>
                	<td ><?php echo $safa_reservation_form_infant_count; ?></td>
                	
                	<td >
					<a title='<?=lang('global_edit')?>' target="_blank" href="<?php echo site_url('ea/passport_accommodation_rooms/manage').'/'.$item_safa_reservation_forms_row->safa_reservation_form_id;?>"   > <span class="icon-pencil"></span> </a>
					
					<?php if(session('ea_id')) {?>
					<a title='<?=lang('global_delete')?>' target="_blank"  href="<?=  site_url('ea/passport_accommodation_rooms/delete/'.$item_safa_reservation_forms_row->safa_reservation_form_id)?> " onclick="return window.confirm('<?=lang('global_are_you_sure_you_want_to_delete')?>')"   ><span class="icon-trash"></span></a>
					<?php }?>
					</td>
                	</tr>
                	
                	<?php	
                			}
                	?>
                	
                	<tr>
                	<th colspan="3"><?php echo lang('total_travellers_count');?></th>
                	
                	<th align="right"><?php echo $safa_reservation_form_adult_count_total; ?></th>
                	<th align="right"><?php echo $safa_reservation_form_child_count_total; ?></th>
                	<th align="right"><?php echo $safa_reservation_form_infant_count_total; ?></th>
                	<th ></th>
                	</tr>
                	
                	
                	</table>
                	
                	<?php
                		}
                		}
					?>
            
            
            	
            	</div>
            	</div>
            	<br/>
            	
            
                <!-- By Gouda -->
                <!--<ul>
                        <li><a href="#tabs-1"><?= lang('trip_details_hotels') ?></a></li>
                        <li><a href="#tabs-2"><?= lang('trip_details_external_travelling') ?></a></li>
                        <li><a href="#tabs-3"><?= lang('trip_details_internal_travelling') ?></a></li>
                <? if (ensure($travellers)): ?><li><a href="#tabs-4"><?= lang('trip_details_travellers') ?></a></li><? endif ?>
                    </ul>


                -->
                
                
                
                
                <div id="tabs-1" style=" clear:both; display:table; width:100%;">
                    <div class="widget-header">
                        <div class="widget-header-icon Fright">
                            <span class="icos-pencil2">

                            </span>
                        </div>
                        <div class="widget-header-title Fright">
                            <?php echo lang('trip_details_hotels') ?>
                        </div>
                        <div class='coll_open Fleft' id="coll_trip_details_hotels"></div>
                        <div class="widget-header-title Fleft">
                            
                            			<?php
                                        //if ($destination == 'uo') {
                                            if (isset($safa_passport_accommodation_rooms_rows_count_with_availability)) {
                                                if ($safa_passport_accommodation_rooms_rows_count_with_availability > 0) {
                                                    ?>
                                                    <a class="btn TAC" href="<?= site_url('trip_details/view_available_rooms/' . $trip->safa_trip_id) ?>" target="_blank"><?php echo lang('view_available_rooms'); ?></a>
                                                    <?php
                                                }
                                            }
                                        //}
                                        ?>
                        </div>

                    </div>
                    <div class="block-fluid" id="container_trip_details_hotels">
                        <table class="" cellpadding="0" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th ><?= lang('trip_details_city') ?></th>
                                    <th ><?= lang('trip_details_hotel') ?></th>
                                    <th ><?= lang('trip_details_nights') ?></th>
                                    <th><?= lang('erp_meal_name') ?></th>
                                    <th><?= lang('trip_details_rooms') ?></th>
                                    <th><?= lang('erp_hotelroomsize_name') ?></th>
                                    <th><?= lang('trip_details_checkin_date') ?></th>
                                    <th><?= lang('trip_details_checkout_date') ?></th>
                                    <!--<th class="TAC">Actions</th>-->
                                </tr>
                            </thead>
                            <tbody>
                                <? if (ensure($hotels)) {
                                	$counter=0;
                                	$previous_erp_city_name = '';
                                	$previous_erp_hotel_name = '';
                                	$previous_days = '';
                                	
                                	$row_class = '';
                                	$previous_row_class = '';
                                	
                                    foreach ($hotels as $hotel){ ?>
                                        <tr>
                                        	<?php 
                                        	$city_style="border-top: solid 1px rgb(221, 221, 221)";
                                        	$hotel_style="border-top: solid 1px rgb(221, 221, 221)";
											
                                        	
                                        	
                                        	
											if( $previous_erp_city_name == $hotel->erp_city_name) {
												$city_style='';
												
											} else {
												$row_class_arr = array('success','info','warning'); 
												$row_class = $row_class_arr[array_rand($row_class_arr)];
												if($previous_row_class==$row_class) {
													$row_class='';
												}
												$previous_row_class = $row_class;
											}
											
                                    		if( $previous_erp_hotel_name == $hotel->erp_hotel_name) {
												$hotel_style='';
											}
                                        	?>
                                        	<td style="border-bottom:0px; <?php echo $city_style;?>" class="<?php echo $row_class;?>"><? if( $previous_erp_city_name != $hotel->erp_city_name) echo $hotel->erp_city_name ?></td>
                                            <td style="border-bottom:0px; <?php echo $hotel_style;?>" class="<?php echo $row_class;?>"><? if( $previous_erp_hotel_name != $hotel->erp_hotel_name) echo $hotel->erp_hotel_name ?></td>
                                            
                                            <td style="border-bottom:0px; <?php echo $hotel_style;?>" class="<?php echo $row_class;?>">
                                                <?php

                                                $datetime1 = strtotime($hotel->entry_date);
                                                $datetime2 = strtotime($hotel->exit_date);
                                                $secs = $datetime2 - $datetime1; // == <seconds between the two times>
                                                $days = $secs / 86400;
                                                $days = ceil($days);
                                                if($previous_erp_hotel_name != $hotel->erp_hotel_name) {
                                                	echo $days;
                                                }
                                                ?>
                                            </td>
                                        	<?php 
                                        	
                                        	$previous_erp_city_name = $hotel->erp_city_name;
                                			$previous_erp_hotel_name = $hotel->erp_hotel_name;
                                			//$previous_days = $days;
                                	
                                            ?>
                                            
                                            
                                            <td class="<?php echo $row_class;?>"><?php //echo $hotel->erp_meal_name; ?></td>
                                            <td class="<?php echo $row_class;?>">
                                                <!--<span class="label label-info tipb" title="<?= $this->trip_details_model->get_hotel_rooms($hotel->safa_trip_hotel_id) ?>">
                                                <?= lang('trip_details_details') ?>
                                                </span>
                                                -->
                                                <?= $hotel->rooms_count ?>
                                            </td>
                                            <td class="<?php echo $row_class;?>"><?= $hotel->erp_hotelroomsize_name ?></td>
                                            <td class="<?php echo $row_class;?>"><?= view_date($hotel->entry_date) ?></td>
                                            <td class="<?php echo $row_class;?>"><?= view_date($hotel->exit_date) ?></td>
                                        </tr>
                                    <? $oldhotel = $hotel;} ?>
                                <? } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <br/>

                <?php
                if ($trip->erp_transportertype_id == 2) {
                    ?>
                    <div id="tabs-2" style=" clear:both; display:table; width:100%;">

                        <div class="widget-header">
                            <div class="widget-header-icon Fright">
                                <span class="icos-pencil2"></span>
                            </div>
                            <div class="widget-header-title Fright">
                                <?php echo lang('trip_details_external_travelling') ?>
                            </div>
                            <div class='coll_open' id="coll_trip_details_going_trips"></div>
                        </div>

                        <div id="container_trip_details_going_trips">
                            <div class="block-fluid">
                                <div class="head dark">
                                    <span class="icos-paragraph-justify"></span>
    <!--                            <h3><?= lang('trip_details_going_trips') ?></h3>-->
                                    <h3><?= lang('flights') ?></h3>
                                </div>
                                <!--    
                                        <table class="" cellpadding="0" cellspacing="0" width="100%">
                                            <thead>
                                                <tr>
                                                    <th><?= lang('trip_details_trip_no') ?></th>
                                                    <th><?= lang('trip_details_departure_port') ?></th>
                                                    <th><?= lang('trip_details_arriving_datetime') ?></th>
                                                    <th><?= lang('trip_details_arriving_port') ?></th>
                                                    <th><?= lang('trip_details_leaving_datetime') ?></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                <? if (ensure($going_trips)): ?>
                                    <? foreach ($going_trips as $going_trip): ?>
                                                                        <tr>
                                                                            <td><?= $going_trip->trip_code ?></td>
                                                                            <td><?= $going_trip->start_port_hall ?></td>
                                                                            <td><?= view_date($going_trip->departure_datetime, 'datetime') ?></td>
                                                                            <td><?= $going_trip->end_port_hall ?></td>
                                                                            <td><?= view_date($going_trip->arrival_datetime, 'datetime') ?></td>
                                                                        </tr>
                                    <? endforeach ?>
                                <? endif ?>
                                            </tbody>
                                        </table>                    
                                    
                                -->
                                <table   id='div_trip_going_group'>
                                    <thead>
                                        <tr>
                                            <!--  <th ><?= lang('externalsegmenttype') ?></th> -->
                                            <!--  <th ><?= lang('trip_code') ?></th> -->
                                            <th ><?= lang('flight_from_to') ?></th>
                                            <th ><?= lang('airport_arrival_datetime') ?></th>
                                            <!-- 
                                            <th ><?= lang('airport_start') ?></th>
                                            <th ><?= lang('airport_end') ?></th>
                                            -->

                                            <th ><?= lang('trip_details_leaving_datetime') ?></th>
                                            <th ><?= lang('transporter') ?></th>
                                            <th ><?= lang('seats_count') ?></th>

        <!--<th class="TAC"> 
        <a class="btn fancybox fancybox.iframe" href="<?php echo site_url('flight_availabilities/popup_trip/1/' . $safa_trip_id) ?>"><?php echo lang('flight_availabilities') ?> </a>
        </th>
                                            --></tr>
                                    </thead>

                                    <? $trip_going_conuter = 0; ?>

                                    <tbody>
                                        <? if (isset($going_trip_flights_rows)) { ?>
                                            <?
                                            foreach ($going_trip_flights_rows as $value) {
                                                $this->flight_availabilities_model->erp_flight_availability_id = $value->erp_flight_availability_id;
                                                //$this->flight_availabilities_model->erp_path_type_id = 1;

                                                $flight_availabilities_details_rows = $this->flight_availabilities_model->get_details_for_trip();

                                                $ports = '';
                                                $start_datetime = '';
                                                $end_datetime = '';
                                                $loop_counter = 0;

                                                $erp_port_id_from = 0;
                                                $erp_port_id_to = 0;
                                                $start_ports_name = '';
                                                $end_ports_name = '';

                                                $trip_going_conuter = $trip_going_conuter + 1;

                                                foreach ($flight_availabilities_details_rows as $flight_availabilities_details_row) {
                                                    if ($loop_counter == 0) {

                                                        $erp_port_id_from = $flight_availabilities_details_row->erp_port_id_from;
                                                        $start_ports_name = $flight_availabilities_details_row->start_ports_name;

                                                        $erp_port_id_to = $flight_availabilities_details_row->erp_port_id_to;
                                                        $end_ports_name = $flight_availabilities_details_row->end_ports_name;

                                                        $ports = $flight_availabilities_details_row->start_ports_name . ' - ' . $flight_availabilities_details_row->end_ports_name;
                                                        
                                                        if($flight_availabilities_details_row->safa_externaltriptype_id==1) {
                                                        	$start_datetime = $flight_availabilities_details_row->arrival_date . ' - ' . $flight_availabilities_details_row->arrival_time;
                                                        } else if($flight_availabilities_details_row->safa_externaltriptype_id==2) {
                                                        	$end_datetime = $flight_availabilities_details_row->flight_date . ' - ' . $flight_availabilities_details_row->departure_time;
                                                        }
                                                    } else {
                                                        $erp_port_id_to = $flight_availabilities_details_row->erp_port_id_to;
                                                        $end_ports_name = $flight_availabilities_details_row->end_ports_name;

                                                        $ports = $ports . ' <br/> ' . $flight_availabilities_details_row->start_ports_name . ' - ' . $flight_availabilities_details_row->end_ports_name;
                                                    
                                                        if($flight_availabilities_details_row->safa_externaltriptype_id==1) {
                                                        	$start_datetime = $flight_availabilities_details_row->arrival_date . ' - ' . $flight_availabilities_details_row->arrival_time;
                                                        } else if($flight_availabilities_details_row->safa_externaltriptype_id==2) {
                                                        	$end_datetime = $flight_availabilities_details_row->flight_date . ' - ' . $flight_availabilities_details_row->departure_time;
                                                        }
                                                    }
                                                    $loop_counter++;
                                                }
                                                ?>
                                                <tr id="row_<?php echo $trip_going_conuter; ?>">
                                                    <td>
                                                        <input type='hidden' name='trip_going_erp_flight_availability_id<?= $trip_going_conuter ?>'  id='trip_going_erp_flight_availability_id<?= $trip_going_conuter ?>'  value='<?= $this->flight_availabilities_model->erp_flight_availability_id ?>' class='validate[required]'/>
                                                        <?php echo $ports; ?>                        
                                                    </td>
                                                    <td>
                                                        <input type='hidden' name='departure_date_<?= $trip_going_conuter ?>'  id='departure_date_<?= $trip_going_conuter ?>'  value='<?= $start_datetime ?>'/>
                                                        <?php echo $start_datetime; ?>

                                                    </td>
                                                    <td>
                                                        <input type='hidden' name='arrival_date_<?= $trip_going_conuter ?>'  id='arrival_date_<?= $trip_going_conuter ?>'  value='<?= $end_datetime ?>'/>
                                                        <?php echo $end_datetime; ?>

                                                    </td>

                                                    <td>
                                                        <input type='hidden' name='trip_safa_transporter_id_<?= $trip_going_conuter ?>'  id='trip_safa_transporter_id_<?= $trip_going_conuter ?>'  value='<?= $value->safa_transporter_id ?>'/>
                                                        <input type='hidden' name='trip_safa_transporter_name_<?= $trip_going_conuter ?>'  id='trip_safa_transporter_name_<?= $trip_going_conuter ?>'  value='<?= $value->safa_transporter_name ?>'/>
                                                        <?php echo $value->safa_transporter_name ?> 
                                                    </td>

                                                    <td>
                                                        <input type='hidden' name='available_seats_count_<?= $trip_going_conuter ?>'  id='available_seats_count_<?= $trip_going_conuter ?>'  value='<?= $value->available_seats_count ?>'/>
                                                                                    <!--  <input type='text' name='available_seats_count_text_<?= $trip_going_conuter ?>'  id='available_seats_count_text_<?= $trip_going_conuter ?>'  value='<?= $value->available_seats_count ?>' readonly="readonly" disabled="disabled"/>-->

                                                        <?= $value->available_seats_count ?> 
                                                    </td>

                        <!--                            <td>-->
                        <!--                            <a href='javascript:void(0)' class='' id='hrf_remove_trip_going' name='<?= $trip_going_conuter ?>'><span class="icon-trash"></span></a>-->
                                                    <!--                            </td>-->

                                                </tr>
                                            <? } ?>
                                        <? } ?>
                                    </tbody>
                                </table>
                            </div>
                            <!--
            <div class="block-fluid">
                <div class="head dark">
                    <span class="icos-paragraph-justify"></span>
                    <h3><?= lang('trip_details_returning_trips') ?></h3> 

                </div>



                <table class="" cellpadding="0" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th><?= lang('trip_details_trip_no') ?></th>
                            <th><?= lang('trip_details_departure_port') ?></th>
                            <th><?= lang('trip_details_departure_datetime') ?></th>
                            <th><?= lang('trip_details_arriving_port') ?></th>
                            <th><?= lang('trip_details_arriving_datetime') ?></th>
                        <th class="TAC">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                            <? if (ensure($returning_trips)): ?>
                                <? foreach ($returning_trips as $returning_trip): ?>
                                                <tr>
                                                    <td><?= $returning_trip->trip_code ?></td>
                                                    <td><?= $returning_trip->start_port_hall ?></td>
                                                    <td><?= view_date($returning_trip->departure_datetime, 'datetime') ?></td>
                                                    <td><?= $returning_trip->end_port_hall ?></td>
                                                    <td><?= view_date($returning_trip->arrival_datetime, 'datetime') ?></td>
                                                <td class="TAC">
                                                        <a href="#"><span class="icon-pencil"></span></a> 
                                                        <a href="#"><span class="icon-trash"></span></a>
                                                </td>
                                                </tr>
                                <? endforeach ?>
                            <? endif ?>
                    </tbody>
                </table>                    
            </div>
        </div>
    </div> 
                            -->
                            <?php
                        }
                        ?>
					</div>
					</div>
                        <br/> 
                        
                        
                          
                        <div id="tabs-3" style=" clear:both; display:table; width:100%;">

                            <div class="widget-header">
                                <div class="widget-header-icon Fright">
                                    <span class="icos-pencil2"></span>
                                </div>
                                <div class="widget-header-title Fright">
                                    <?php echo lang('trip_details_internal_transportation') ?>
                                </div>
                                <div class='coll_open' id="coll_trip_details_internal_transportation"></div>
                            </div>

                            <div class="block-fluid" id="container_trip_details_internal_transportation">

                                <table class="" cellpadding="0" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th><?= lang('trip_details_city') ?></th>
                                            <th><?= lang('trip_details_segment_type') ?></th>
                                            <th><?= lang('trip_details_starting_point') ?></th>
                                            <th><?= lang('trip_details_starting_datetime') ?></th>
                                            <th><?= lang('trip_details_ending_point') ?></th>
                                            <!--<th><?= lang('trip_details_ending_datetime') ?></th>-->
                                            <th><?= lang('trip_details_internalsegmentestatus') ?></th>
                                            <th><?= lang('trip_details_uo_user') ?></th>
                                            <th class="TAC"><?= lang('trip_details_drivers_vehicles') ?></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <? if (ensure($internal_trip)): ?>
                                            <? foreach ($internal_trip as $it): ?>
                                                <tr>
                                                    <td><?= $it->city ?></td>
                                                    <td><?= $it->type ?></td>
                                                    <td><?= $this->trip_details_model->get_starting_point($it) ?></td>
                                                    <td><?= $it->start_datetime ?></td>
                                                    <td><?= $this->trip_details_model->get_ending_point($it) ?></td>
                                                    <!--<td><?= $it->end_datetime ?></td>-->
                                                    <td><?= $it->internalsegmentestatus ?></td>
                                                    <td><?= $it->uo_user ?></td>
                                                    <td>
                                                        <? if (count(json_decode($it->ito_driver_info))) { ?>                                    
                                                            X <?= count(json_decode($it->ito_driver_info)); ?>
                                                            <a title='<?= lang('drivers') ?>'
                                                               href="<?= site_url('drivers/getDrivers/' . $it->safa_internalsegment_id) ?>" class="fancybox fancybox.iframe"
                                                               style="color:black; direction:rtl">
                                                                <img src="<?= IMAGES2 ?>/car.png" align="left" >
                                                            </a>
                                                        <? } else { ?>
<!--                                                            <?= lang('no_drivers') ?>-->
                                                        <? } ?>
                                                    </td>

                                                </tr>
                                            <? endforeach ?>
                                        <? endif ?>
                                    </tbody>
                                </table>                    
                            </div>
                        </div>
                        <br/>
                        <? if (ensure($travellers)): ?>
                            <div id="tabs-4" style=" clear:both; display:table; width:100%;">
                                <?= form_open() ?>
                                <? if ($destination == 'ea' || $destination == 'uo') { ?>
                <!--                            <input type="submit" name="delete" value="<?= lang('global_delete') ?>" class="btn btn-primary" style="float: left; margin-left: 5px;" onclick="if (!confirm('<?= lang('global_are_you_sure_you_want_to_delete') ?>')) return false" />-->


                                    <div class="widget-header">
                                        <div class="widget-header-icon Fright">
                                            <span class="icos-pencil2"></span>
                                        </div>
                                        <div class="widget-header-title Fright">
                                            <?php echo lang('trip_details_travellers') ?>
                                        </div>
                                        <div class='coll_open' id="coll_trip_details_travellers" ></div>
                                    </div>

                                    <div class="block-fluid" id="container_trip_details_travellers">
                                        <table class="fpTable" cellpadding="0" cellspacing="0" width="100%">
                                            <thead>
                                                <tr>
                                                    <!--<? if ($destination == 'ea'): ?>
                                                                    <th><input type="checkbox" class="checkall"/></th>
                                                    <? endif ?>-->
                                                    <th><?= lang('trip_details_traveller') ?></th>
                                                    <th><?= lang('trip_details_passport_no') ?></th>
                                                    <th><?= lang('trip_details_visa_no') ?></th>
                                                    <th><?= lang('trip_details_nationality') ?></th>
                                                    <th><?= lang('trip_details_gender') ?></th>
                                                    
                                                    <th><?= lang('group_passports_age') ?></th>
                                                    <th><?= lang('group_passports_occupation') ?></th>
                                                    <!--<th class="TAC"><?= lang('global_operations') ?></th>-->
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <? foreach ($travellers as $traveller): ?>
                                                    <tr>
                                                        <!--<? if ($destination == 'ea'): ?>
                                                                        <td><input type="checkbox" name="order[]" value="<?= $traveller->safa_trip_traveller_id ?>"/></td>
                                                        <? endif ?>-->
                                                        <td><?= $traveller->{'first_' . name()} . ' ' . $traveller->{'second_' . name()} . ' ' . $traveller->{'third_' . name()} . ' ' . $traveller->{'fourth_' . name()} ?></td>
                                                        <td><?= $traveller->passport_no ?></td>
                                                        <td><?= $traveller->visa_number ?></td>
                                                        <td><?= $traveller->erp_nationalities_name ?></td>
                                                        <td><?= $traveller->erp_gender_name ?></td>
                                                        
                                                        <td>
                                                        
                                                        <?php
					                                    $date_of_birth = strtotime($traveller->date_of_birth);
					                                    $date_now = strtotime(date('Y-m-d', time()));
					                                    $secs = $date_now - $date_of_birth; // == return sec in difference
					                                    $days = $secs / 86400;
					                                    $age = $days / 365;
														//$age=round($age);
														//$age=ceil($age);
					                                    $age = floor($age);
					                                    echo $age;
					                                    ?>
                                                        
                                                        </td>
                                                        <td><?= $traveller->occupation ?></td>
                                                        
                                                        
                										<!-- <td class="TAC">
                                                            <a class="getTraveller" href="javascript:void(0)" rel="<?= $traveller->safa_trip_traveller_id ?>"><span class="icon-search"></span></a>
                                                            <a data-toggle="modal" role="button" href="#fModal" class="getTraveller" rel="<?= $traveller->safa_trip_traveller_id ?>"><span class="icon-search"></span></a>
                                                            <a class="fancybox fancybox.iframe" href="<?= site_url('trip_details/traveller_details/' . $traveller->safa_trip_traveller_id) ?>"><span class="icon-search"></span></a>
                                                       </td>-->
                                                    </tr>
                                                <? endforeach ?>
                                            </tbody>
                                        </table>
                                    </div>
                                <? } ?>
                            </div>
                            <br/>
                        <? endif ?>

                        <? if ($destination == 'uo') { ?>
                            <?= form_open() ?>
                            <div class="widget-header">
                                <div class="widget-header-icon Fright">
                                    <span class="icos-pencil2"></span>
                                </div>
                                <div class="widget-header-title Fright">
                                    <?php echo lang('trip_status') ?>
                                </div>
                            </div>

                            <div class="block-fluid">
                                <div class="row-form">
                                    <div class="span2">
                                        <?php echo lang('trip_current_status'); ?>
                                    </div>
                                    <div class="span4">
                                        <?php
                                        echo form_dropdown('safa_tripstatus_id', $safa_tripstatus, set_value('safa_tripstatus_id', $trip->safa_tripstatus_id), "id='safa_tripstatus_id' class='validate[required]'");
                                        ?>
                                    </div>

                                    <?php
                                    $display = "display:none;";
                                    if ($trip->safa_tripstatus_id == 5) {
                                        $display = "";
                                    }
                                    ?>
                                    <div id="dv_uo_refusing_reason" style="<?php echo $display; ?>">
                                        <div class="span2" >
                                            <?php echo lang('uo_refusing_reason'); ?>
                                        </div>
                                        <div class="span4">
                                            <?php echo form_textarea('uo_refusing_reason', set_value("uo_refusing_reason", $trip->uo_refusing_reason), " id='uo_refusing_reason' class='input-full'  "); ?>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="block-fluid">
                                <div class="row-form">
                                    <input type="submit" id="smt_save" name="smt_save" class="btn btn-primary" value="<?= lang("global_submit") ?>" />
                                </div>
                            </div> 
                            <?= form_close() ?>
                        <?php } ?>

                    </div>
                </div>
            </div>
        </div>

        <div class="append"></div>
        <script type="text/javascript">
            function openDialog(url) {
                $('.opened-dialogs').dialog("close");
                $('<div class="opened-dialogs">').html('loading...').dialog({
                    position: ['center', 20],
                    open: function() {
                        $(this).load(url);
                    },
                    close: function(event, ui) {
                        $(this).remove();
                    },
                    title: '<?= lang('trip_details_traveller_details') ?>',
                    minWidth: 600
                });

                return false;
            }

            $(document).ready(function() {
                $('.reservation-fancy').click(function() {
                    var fancyurl = $(this).attr('href');
                    $.post(fancyurl, {'tripid': '<?= $trip->safa_trip_id ?>'}, function(data) {
                        $.fancybox({content: data, autoResize: true, afterShow: $.fancybox.toggle()});
                    });

                    return false;
                });

                $('.transport-fancy').click(function() {
                    var fancyurl = $(this).attr('href');
                    $.get(fancyurl, function(data) {
                        $.fancybox({content: data, autoResize: true, afterShow: $.fancybox.toggle()});
                    });

                    return false;
                });

                $('.fancybox').fancybox();

                $('.getTraveller').click(function() {
                    //    $(this).click();
                    openDialog('<?= site_url($destination . '/trip_details/traveller_details/') ?>/' + $(this).attr('rel'));
                });

                $('.btn-primary').click(function() {
                    if ($(this).attr('aria-hidden') == 'true') {
                    }
                });

                $(".closeModal").click(function() {
                    $(".modal").modal("hide");
                    $('.modal.in').modal('hide')
                });
            });

            $("a#inline").fancybox({
                'hideOnContentClick': true
            });
        </script>

        <script type="text/javascript">
            $("#coll_trip_details_hotels").click(function() {
                $header = $(this);
                //getting the next element
                $content = $header.next();
                $content = $('#container_trip_details_hotels');

                //open up the content needed - toggle the slide- if visible, slide up, if not slidedown.
                $content.slideToggle(500, function() {
                    $header.text(function() {
                        //change text based on condition
                        //return $content.is(":visible") ? "Collapse" : "Expand";
                    });
                });

                if ($("#coll_trip_details_hotels").prop("class") == 'coll_open') {
                    $('#coll_trip_details_hotels').removeClass('coll_open').addClass('coll_close');
                } else {
                    $('#coll_trip_details_hotels').removeClass('coll_close').addClass('coll_open');
                }

            });

            $("#coll_trip_details_safa_reservation_forms").click(function() {
                $header = $(this);
                //getting the next element
                $content = $header.next();
                $content = $('#container_trip_details_safa_reservation_forms');

                //open up the content needed - toggle the slide- if visible, slide up, if not slidedown.
                $content.slideToggle(500, function() {
                    $header.text(function() {
                        //change text based on condition
                        //return $content.is(":visible") ? "Collapse" : "Expand";
                    });
                });

                if ($("#coll_trip_details_safa_reservation_forms").prop("class") == 'coll_open') {
                    $('#coll_trip_details_safa_reservation_forms').removeClass('coll_open').addClass('coll_close');
                } else {
                    $('#coll_trip_details_safa_reservation_forms').removeClass('coll_close').addClass('coll_open');
                }

            });
            
            
            $("#coll_trip_details_going_trips").click(function() {
                $header = $(this);
                //getting the next element
                $content = $header.next();
                $content = $('#container_trip_details_going_trips');

                //open up the content needed - toggle the slide- if visible, slide up, if not slidedown.
                $content.slideToggle(500, function() {
                    $header.text(function() {
                        //change text based on condition
                        //return $content.is(":visible") ? "Collapse" : "Expand";
                    });
                });

                if ($("#coll_trip_details_going_trips").prop("class") == 'coll_open') {
                    $('#coll_trip_details_going_trips').removeClass('coll_open').addClass('coll_close');
                } else {
                    $('#coll_trip_details_going_trips').removeClass('coll_close').addClass('coll_open');
                }

            });

            $("#coll_trip_details_internal_transportation").click(function() {
                $header = $(this);
                //getting the next element
                $content = $header.next();
                $content = $('#container_trip_details_internal_transportation');

                //open up the content needed - toggle the slide- if visible, slide up, if not slidedown.
                $content.slideToggle(500, function() {
                    //execute this after slideToggle is done
                    //change text of header based on visibility of content div
                    $header.text(function() {
                        //change text based on condition
                        //return $content.is(":visible") ? "Collapse" : "Expand";
                    });
                });

                if ($("#coll_trip_details_internal_transportation").prop("class") == 'coll_open') {
                    $('#coll_trip_details_internal_transportation').removeClass('coll_open').addClass('coll_close');
                } else {
                    $('#coll_trip_details_internal_transportation').removeClass('coll_close').addClass('coll_open');
                }

            });

            $("#coll_trip_details_travellers").click(function() {
                $header = $(this);
                //getting the next element
                $content = $header.next();
                $content = $('#container_trip_details_travellers');

                //open up the content needed - toggle the slide- if visible, slide up, if not slidedown.
                $content.slideToggle(500, function() {
                    $header.text(function() {
                        //change text based on condition
                        //return $content.is(":visible") ? "Collapse" : "Expand";
                    });
                });

                if ($("#coll_trip_details_travellers").prop("class") == 'coll_open') {
                    $('#coll_trip_details_travellers').removeClass('coll_open').addClass('coll_close');
                } else {
                    $('#coll_trip_details_travellers').removeClass('coll_close').addClass('coll_open');
                }

            });

            $("#safa_tripstatus_id").change(function() {
                if ($(this).val() == 5) {
                    $('#dv_uo_refusing_reason').css("display", "block");
                } else {
                    $('#dv_uo_refusing_reason').css("display", "none");
                }
            });

        </script>
