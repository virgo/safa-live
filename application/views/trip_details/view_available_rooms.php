<style>
    .coll_open {
        cursor:pointer;
        float:left;
    }
    .coll_close {
        cursor:pointer;
        float:left;
    }

    .fancybox-outer, .fancybox-inner {
        overflow-y: auto !important;
    }
       .wizerd-div {
        border-bottom: medium none !important;
        margin: -18px 0 20px;
        padding-top: 0;
    }
    .wizerd-div a {
        background: none repeat scroll 0 0 #FAFAFA;
        border: 1px solid #D5D6D6;
        border-radius: 49px;
        color: #663300;
        display: inline-block;
        margin: -6px 5px -17px -8px;
        padding: 10px 12px 8px;
    }
     .wizerd-div2 {
        border-bottom: medium none !important;
     margin: 10px 0 -10px;
        padding-top: 0;
    }
    .wizerd-div p {
        background: none repeat scroll 0 0 #FAFAFA;
        border: 1px solid #D5D6D6;
        border-radius: 49px;
        color: #663300;
        display: inline-block;
        margin: -6px 5px -17px -8px;
        padding: 10px 12px 8px;
    }
    
    a {
        color: #C09853;
    }
    .resalt-group {
      margin: 25px 0.5% 0.5%;
padding: 0.5%;
width: 98%;
    }
    th a.btn, th input[type="button"], th input[type="submit"], th button {
        margin: 0;
        padding: 4px 12px;
    }
    .coll_close, .coll_open {
        margin-top: 0;
    }
    .chosen-container {
        margin-top: 4px;
    }
    .warning{
        color: #C09853;
    }
    .lable2{
        border: 1px solid #D5D6D6;
border-radius: 49px;
margin-top: -2px;
margin-right: -50px;
display: inline-block;
padding: 8px 10px;
font-size: 15.844px;
/*font-weight: bold;*/
line-height: 14px;
color: #000;
/*text-shadow: 0 -1px 0 rgba(0,0,0,0.25);*/
white-space: nowrap;
vertical-align: baseline;
background-color: #DBC16E;}
    .row_res {margin-top: 5px;
margin-bottom: 5px;
border-radius: 5px 5px 5px 5px;
padding: 6px;
background-color:  rgb(240, 240, 240);}
</style>

<div class="row-fluid">              
    <div class="widget">
        <div class="widget-header">
           
            <div class="widget-header-icon Fright"> <span class="icos-pencil2"></span> </div>
            <div class="widget-header-title Fright">
                <?= lang('view_available_rooms') ?>
            </div>
       	</div>
    
<?php 
foreach($safa_passport_accommodation_rooms_hotels_rows as $safa_passport_accommodation_rooms_hotels_row) {
	
	$this->safa_passport_accommodation_rooms_model->erp_hotel_id = $safa_passport_accommodation_rooms_hotels_row->erp_hotel_id ;
	$hotels_availability_master_rows = $this->safa_passport_accommodation_rooms_model->get_hotels_availability_master();
	
?>    
    
      <div class="resalt-group" id="dv_hotels">
          <div class="wizerd-div"><a><?php echo $safa_passport_accommodation_rooms_hotels_row->erp_hotel_name .' - '.$safa_passport_accommodation_rooms_hotels_row->erp_city_name   ?></a>  </div>
            
             <?php 
                    foreach($hotels_availability_master_rows as $hotels_availability_master_row) {
                    	$this->safa_passport_accommodation_rooms_model->erp_hotels_availability_master_id = $hotels_availability_master_row->erp_hotels_availability_master_id ;
                    	$safa_passport_accommodation_rooms_hotelroomsizes_rows = $this->safa_passport_accommodation_rooms_model->get_hotelroomsizes();
	
                    
                   	?>
            <div class="span12 row_res">
                <div class="wizerd-div2 span4"><span class="span7 label"><?php echo lang('invoice_number'); ?> <i class="icon-file-text icon-2x pull-right"></i></span><p class="lable2"><?php echo $hotels_availability_master_row->invoice_number; ?></p></div>
          
                <div class="span4"> <div class="table-warp">
                <table>
                    <thead>
                        <tr>
                           <th><?php echo lang('erp_hotelroomsize_name'); ?></th>
                           <th><?php echo lang('rooms_count'); ?></th>
                          
                        </tr>
                    </thead>
                    <tbody class="hotels" id="hotels">
                    <?php 
							foreach ($safa_passport_accommodation_rooms_hotelroomsizes_rows as $safa_passport_accommodation_rooms_hotelroomsizes_row) {
								
								$this->safa_passport_accommodation_rooms_model->safa_trip_id = $safa_trip_id;
								$this->safa_passport_accommodation_rooms_model->erp_hotel_id = $safa_passport_accommodation_rooms_hotels_row->erp_hotel_id ;
								$this->safa_passport_accommodation_rooms_model->erp_hotelroomsize_id = $safa_passport_accommodation_rooms_hotelroomsizes_row->erp_hotelroomsize_id;
								$safa_passport_accommodation_rooms_rows_count = $this->safa_passport_accommodation_rooms_model->get_linked_with_availability(true);
								
								$this->safa_passport_accommodation_rooms_model->erp_hotelroomsize_id = false;
								
							?>
							<tr>
                                 <td><?php echo $safa_passport_accommodation_rooms_hotelroomsizes_row->erp_hotelroomsize_name; ?></td>
                                 <td><?php echo $safa_passport_accommodation_rooms_rows_count; ?></td>
                            </tr>
                                
							<?php }?>
                </tbody>
                </table>
                </div></div>
               <div class="span4">
                 <span class="span11 thumbnail border">
                     <img width="300px" height="200px" src="<?php echo site_url('/static/uploads/availability');?>/<?php echo $hotels_availability_master_row->invoice_image; ?>"></img>
                 </span>
             </div>
        </div> 
      
         <?php 
                    }
         ?>
      
      </div>
<?php 
}
?>      
    
    </div></div>


