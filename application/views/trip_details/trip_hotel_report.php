<!doctype html>
<html>
    <head>
        <meta charset="UTF-8">
        <title><?= lang('reservation_report') ?></title>
        <style>
            body {
                margin: 0;
                padding: 0;
                /* [disabled]background-color: #FAFAFA; */
                font: 12pt "Tahoma";
            }

            .logo{ margin: 0 auto;
                   background-position: center;
                   display: block;
                   min-width: 366px;
                   height: 91px;
                   background-repeat: no-repeat;
                   background-size: auto;
            }
            * {
                box-sizing: border-box;
                -moz-box-sizing: border-box;
            }
            .page {
                width: 21cm;
                min-height: 29.7cm;
                padding: 10px;
                margin: 1cm auto;
                /* [disabled]border: 1px #D3D3D3 solid; */
                border-radius: 5px;
                background: white;
                box-shadow: 0 0 5px rgba(0, 0, 0, 0.1);
            }
            .subpage {
                padding: 1cm;
                /* [disabled]border: 5px solid #666; */
                height: 256mm;
                outline: 2cm #FFf solid;
            }

            .voicher{
                border: 1px solid #000;
                direction: rtl;
            }

            .voicher td {
                border: 1px solid #000;

            }

            .voicher th {
                border: 1px solid #000;

            }

            @page {
                size: A4;
                margin: 0;
            }
            @media print {
                .page {
                    margin: 0;
                    border: initial;
                    border-radius: initial;
                    width: initial;
                    min-height: initial;
                    box-shadow: initial;
                    background: initial;
                    page-break-after: always;
                }
            }

        </style>
    </head>
    <body>
        <div class="book">
            <div class="page">
                <div class="subpage">
                    <div class="logo"><img src="<?php echo site_url(); ?>static/img/uo_companies_logos/<?php echo session('uo_id'); ?>/logo.png"></div>
                    <div class="content">
                        <table width="90%" height="837" border="0" align="center">
                            <tr>
                                <td height="32" align="right" valign="top">&nbsp;</td>
                            </tr>
                            <tr>
                                <td height="689" align="right" valign="top">
                                    <div class="block-fluid" id="container_trip_details_hotels">
                                        <table class="voicher"  dir="rtl" cellpadding="0" cellspacing="0" width="100%">
                                            <thead>
                                                <tr>
                                                    <th rowspan="2"><?= lang('trip_details_hotel') ?></th>
                                                    <th rowspan="2"><?= lang('trip_details_city') ?></th>
                                                    <th rowspan="2"><?= lang('trip_details_checkin_date') ?></th>
                                                    <th rowspan="2"><?= lang('trip_details_checkout_date') ?></th>
                                                    <th rowspan="2"><?= lang('trip_details_nights') ?></th>
                                                    <th rowspan="2">رقم الفاتورة</th>
                                                    <th colspan="<?= count($roomsizes) ?>">توزيع الغرف</th>
                                                </tr>
                                                <tr>
                                                    <? foreach ($roomsizes as $roomsize => $sizename) : ?>
                                                        <th><?= $sizename ?></th>
                                                    <? endforeach ?>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?
                                                if (ensure($hotels)) {
                                                    $counter = 0;
                                                    $previous_erp_city_name = '';
                                                    $previous_erp_hotel_name = '';
                                                    $previous_days = '';

                                                    $row_class = '';
                                                    $previous_row_class = '';

                                                    foreach ($hotels as $hotel) {
                                                        ?>
                                                        <tr>

                                                            <td ><? echo $hotel['hotel']->erp_hotel_name ?></td>
                                                            <td ><? echo $hotel['hotel']->erp_city_name ?></td>

                                                            <td ><?= view_date($hotel['hotel']->entry_date) ?></td>

                                                            <td ><?= view_date($hotel['hotel']->exit_date) ?></td>
                                                            <td ><?php
                                                                $datetime1 = strtotime($hotel['hotel']->entry_date);
                                                                $datetime2 = strtotime($hotel['hotel']->exit_date);
                                                                $secs = $datetime2 - $datetime1; // == <seconds between the two times>
                                                                $days = $secs / 86400;
																$days = ceil($days);
																
                                                                if ($previous_erp_hotel_name != $hotel['hotel']->erp_hotel_name) {
                                                                    echo $days;
                                                                }
                                                                ?>
                                                            </td>
                                                            <td ></td>
                                                            <? foreach ($roomsizes as $roomsize => $sizename) : ?>
                                                                <td><?= isset($hotel['rooms'][$roomsize]) ? $hotel['rooms'][$roomsize] : '' ?></td>
                                                            <? endforeach ?>
                                                        </tr>
                                                    <? } ?>
                                                <? } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                    
                                    <br />
                                    <br />
                                    الجولات والزيارات
                                    <div class="block-fluid" id="container_trip_details_hotels">
                                        
                                        <table class="voicher"  dir="rtl" cellpadding="0" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th rowspan="2"><?= lang('serial') ?></th>
                                                <th rowspan="2"><?= lang('internaltrip_date') ?></th>
                                                <th rowspan="2"><?= lang('time') ?></th>
                                                <th rowspan="2"><?= lang('flight_no') ?></th>
                                                <th colspan="2" ><?= lang('from_to') ?></th>
                                                <!--<th colspan="2"><?= lang('agent') ?></th>-->
                                            </tr>
                                            <tr>
                                                <th><?= lang('from') ?></th>
                                                <th><?= lang('to') ?></th>
                                                <th><?= lang('from') ?></th>
                                                <th><?= lang('to') ?></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <? if (ensure($internalsegments)): ?>
                                                <?
                                                $serial = 0;
                                                $previous_safa_uo_contract_name = '';
                                                foreach ($internalsegments as $internalsegment) {
                                                    $serial++;
                                                    ?>
                                                    <tr>
                                                        <td><?= $serial ?></td>
                                                        <td><?php
                                                            $start_datetime_arr = explode(' ', $internalsegment->start_datetime);
                                                            if (isset($start_datetime_arr[0])) {
                                                                echo $start_datetime_arr[0];
                                                            }
                                                            ?></td>
                                                        <td><?php
                                                            if (isset($start_datetime_arr[1])) {
                                                                echo $start_datetime_arr[1];
                                                            }
                                                            ?></td>
                                                        <td><?= $internalsegment->flight_number ?>  <?= $internalsegment->transporter ?></td>

                                                        <td><?= $this->trip_details_model->get_starting_point($internalsegment) ?></td>


                                                        <td><?= $this->trip_details_model->get_ending_point($internalsegment) ?></td>

                                                        <td><?php
                                                            if ($internalsegment->safa_uo_user_id != '') {
                                                                echo item('safa_uo_users', name(), array('safa_uo_users.safa_uo_user_id' => $internalsegment->safa_uo_user_id));
                                                            }
                                                            ?></td>
<!--                                                        <td><?php
                                                            if ($internalsegment->safa_uo_agent_id != '') {
                                                                echo item('safa_uo_users', name(), array('safa_uo_users.safa_uo_user_id' => $internalsegment->safa_uo_agent_id));
                                                            }
                                                            ?></td>-->
                        <!--                                                    <td ><?php if ($previous_safa_uo_contract_name != $internalsegment->safa_uo_contract_name) echo $internalsegment->safa_uo_contract_name; ?></td>-->

                                                    </tr>
                                                    <?
                                                    $previous_safa_uo_contract_name = $internalsegment->safa_uo_contract_name;
                                                }
                                                ?>
                                            <? endif ?>
                                        </tbody>
                                    </table> 
                                        <br />
                                    </div>
                        <br />
                        <br />
                        <table width="100%" dir="rtl">
                            <tr>
                                <td>
                                    المسؤل عن التنفيذ
                                </td>
                                <td>
                                    المبيعات
                                </td>
                            </tr>
                        </table>
                                    
                                </td>
                            </tr>
                            <tr>
                                <td align="center"><img src="<?php echo site_url(); ?>static/img/uo_companies_logos/<?php echo session('uo_id'); ?>/footer.png" width="600" height="106"></td>
                            </tr>
                        </table>
                        
                        
                    </div>    
                </div>
            </div>
        </div>
    </body>
</html>









