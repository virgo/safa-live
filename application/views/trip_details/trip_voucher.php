<!doctype html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>فاتورة رحله</title>
        <style>
            body {
                margin: 0;
                padding: 0;
                /* [disabled]background-color: #FAFAFA; */
                font: 12pt "Tahoma";
            }

            .logo{ margin: 0 auto;
                   background-position: center;
                   display: block;
                   min-width: 366px;
                   height: 91px;
                   background-repeat: no-repeat;
                   background-size: auto;
            }
            * {
                box-sizing: border-box;
                -moz-box-sizing: border-box;
            }
            .page {
                width: 21cm;
                min-height: 29.7cm;
                padding: 10px;
                margin: 1cm auto;
                /* [disabled]border: 1px #D3D3D3 solid; */
                border-radius: 5px;
                background: white;
                box-shadow: 0 0 5px rgba(0, 0, 0, 0.1);
            }
            .subpage {
                padding: 1cm;
                /* [disabled]border: 5px solid #666; */
                height: 256mm;
                outline: 2cm #FFf solid;
            }

            .voicher{
                border: 1px solid #000;
                direction: rtl;
            }

            .voicher td {
                border: 1px solid #000;
                vertical-align: central;
                text-align: center;

            }

            .voicher th {
                border: 1px solid #000;

            }

            @page {
                size: A4;
                margin: 0;
            }
            @media print {
                .page {
                    margin: 0;
                    border: initial;
                    border-radius: initial;
                    width: initial;
                    min-height: initial;
                    box-shadow: initial;
                    background: initial;
                    page-break-after: always;
                }
            }
	
.note2{ text-align:right;
padding:5px;
font-weight:bold;
direction:rtl;}
.note1{ text-align:right;
padding:5px;
font-weight:bold;
direction:rtl;}
.rtl{ direction:rtl;}
.table2{
	margin-bottom: 2px;
	border: thin inset #666;
}	
        </style>
    </head>
    <body>
        <div class="book">
            <div class="page">
                <div class="subpage">
                    <div class="logo"><img src="<?php echo site_url(); ?>static/img/uo_companies_logos/<?php echo session('uo_id'); ?>/logo.png"></div>
                    <div class="content" style="direction: rtl;">
                        <div style="text-align: right;">
                            التاريخ :- <?= $dayhijri ?>
                        </div>
                        <div style="text-align: right;">
                            الموافق :- <?= date('Y-m-d') ?>
                        </div>
                        <div style="text-align: center;">
                            شركة <?= $ea->ea_name ?>
                        </div>
                        <div>
                            اسم المجموعه:-
                            <? foreach($groups as $group) :?>
                            <span style="margin: 0 5px;"><?= $group->name ?> </span>
                            <? endforeach ?>
                        </div>
                        <div style="width:45%;float: right;">
                            عدد الافراد:-
                            <span style="margin: 0 5px;"><?= $peoplecount ?> </span>
                        </div>
                        <div style="width:45%;float: right;">
                            عدد الاطفال:-
                            <span style="margin: 0 5px;"><?= ($childs)?$childs:'' ?> </span>
                        </div>
                        <div style="clear:both;"></div>

                        <table width="90%" height="837" border="0" align="center">
                            <tr>
                                <td height="32" align="center" valign="top">   فاتورة  رقم (<span style="margin: 0 40px;">   </span> )</td>
                            </tr>
                            <tr>
                                <td height="689" align="right" valign="top">
                                    <div class="block-fluid" id="container_trip_details_hotels">
                                        <table class="voicher"  dir="rtl" cellpadding="0" cellspacing="0" width="100%">
                                            <thead>
                                                <tr>
                                                    <th>المستوي</th>
                                                    <th><?= lang('trip_details_hotel') ?></th>
                                                    <th><?= lang('trip_details_checkin_date') ?></th>
                                                    <th><?= lang('trip_details_checkout_date') ?></th>
                                                    <th ><?= lang('trip_details_nights') ?></th>
                                                    <th colspan="2">عدد ونوع الغرفه</th>
                                                    <th> عدد الأفراد</th>
                                                    <th> سعر الفرد</th>
                                                    <th> الاجمالي</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?
                                                if (ensure($hotels)) {
                                                    $counter = 0;
                                                    $previous_erp_city_name = '';
                                                    $previous_erp_hotel_name = '';
                                                    $previous_days = '';

                                                    $row_class = '';
                                                    $previous_row_class = '';
                                                    foreach ($hotels as $hoteldata) {
                                                        foreach ($hoteldata as $hotel) {
                                                            ?>
                                                            <tr>

                                                                <td rowspan="2"><? echo $hotel['safa_packages_name'] ?></td>
                                                                <td ><? echo $hotel['hotel'][0]->erp_hotel_name ?></td>

                                                                <td ><?= view_date($hotel['hotel'][0]->entry_date,'date') ?></td>

                                                                <td ><?= view_date($hotel['hotel'][0]->exit_date,'date') ?></td>
                                                                <td ><?php
                                                                    $datetime1 = strtotime($hotel['hotel'][0]->entry_date);
                                                                    $datetime2 = strtotime($hotel['hotel'][0]->exit_date);
                                                                    $secs = $datetime2 - $datetime1; // == <seconds between the two times>
                                                                    $days = $secs / 86400;
                                                                    $days = ceil($days);
                                                                    echo $days;
                                                                    ?>
                                                                </td>
                                                                <td  rowspan="2"><? echo $hotel['roomcount'] ?></td>
                                                                <td  rowspan="2"><? echo $hotel['roomtype'] ?></td>
                                                                <td  rowspan="2"><? echo $hotel['people'] ?></td>
                                                                <td  rowspan="2"><? echo $hotel['people_price'] ?> $</td>
                                                                <td  rowspan="2"><? echo $hotel['total'] ?> $</td>

                                                            </tr>
                                                            <tr>
                                                                <td ><? echo $hotel['hotel'][1]->erp_hotel_name ?></td>

                                                                <td ><?= view_date($hotel['hotel'][1]->entry_date,'date') ?></td>

                                                                <td ><?= view_date($hotel['hotel'][1]->exit_date,'date') ?></td>
                                                                <td ><?php
                                                                    $datetime1 = strtotime($hotel['hotel'][1]->entry_date);
                                                                    $datetime2 = strtotime($hotel['hotel'][1]->exit_date);
                                                                    $secs = $datetime2 - $datetime1; // == <seconds between the two times>
                                                                    $days = $secs / 86400;
                                                                    echo $days;
                                                                    ?>
                                                                </td>
                                                            </tr>
                                                        <? }
                                                    } ?>
                                                <? } ?>
                                            </tbody>
                                            <tfoot>
                                            <th colspan="9">الاجمالي</th>
                                            <th><?= $totalamount ?> $</th>
                                            </tfoot>
                                        </table>
                                        اجمالي قيمة الفاتورة <?= $totalamount ?> $
                                    </div>

                                    <br />
                                    <br />
                                    <table width="100%" dir="rtl">
                                        <tr>
                                            <td width="70%">
                                                
                                            </td>
                                            <td>
                                                المدير المالى
                                            </td>
                                        </tr>
                                    </table>

                                </td>
                            </tr>
                            <tr>
                                <td align="center"><img src="<?php echo site_url(); ?>static/img/uo_companies_logos/<?php echo session('uo_id'); ?>/footer.png" width="600" height="106"></td>
                            </tr>
                        </table>


                    </div>    
                </div>
            </div>
        </div>
    </body>
</html>









