<!doctype html>
<html>
    <head>
        <meta charset="UTF-8">
        <title><?= lang('trip_programs_report') ?></title>
        <style>
            body {
                margin: 0;
                padding: 0;
                /* [disabled]background-color: #FAFAFA; */
                font: 12pt "Tahoma";
            }

            .logo{ margin: 0 auto;
                   background-position: center;
                   display: block;
                   min-width: 366px;
                   height: 91px;
                   background-repeat: no-repeat;
                   background-size: auto;
            }
            * {
                box-sizing: border-box;
                -moz-box-sizing: border-box;
            }
            .page {
                width: 21cm;
                min-height: 29.7cm;
                padding: 10px;
                margin: 1cm auto;
                /* [disabled]border: 1px #D3D3D3 solid; */
                border-radius: 5px;
                background: white;
                box-shadow: 0 0 5px rgba(0, 0, 0, 0.1);
            }
            .subpage {
                padding: 1cm;
                /* [disabled]border: 5px solid #666; */
                height: 256mm;
                outline: 2cm #FFf solid;
            }

            .voicher{
                border: 1px solid #000;
                direction: rtl;
            }

            .voicher td {
                border: 1px solid #000;
                vertical-align: central;
                text-align: center;

            }

            .voicher th {
                border: 1px solid #000;

            }

            @page {
                size: A4;
                margin: 0;
            }
            @media print {
                .page {
                    margin: 0;
                    border: initial;
                    border-radius: initial;
                    width: initial;
                    min-height: initial;
                    box-shadow: initial;
                    background: initial;
                    page-break-after: always;
                }
            }

            .note2{ text-align:right;
                    padding:5px;
                    font-weight:bold;
                    direction:rtl;}
            .note1{ text-align:right;
                    padding:5px;
                    font-weight:bold;
                    direction:rtl;}
            .rtl{ direction:rtl;}
            .table2{
                margin-bottom: 2px;
                border: thin inset #666;
            }	
        </style>
    </head>
    <body>
        <div class="book">
            <div class="page">
                <div class="subpage">
                    <div class="logo"><img src="<?php echo site_url(); ?>static/img/uo_companies_logos/<?php echo session('uo_id'); ?>/logo.png"></div>
                    <div class="content" style="direction: rtl;">
                        <div style="text-align: center;">
                            شركة <?= $ea->ea_name ?>
                        </div>

                        <div style="clear:both;"></div>

                        <table width="90%" height="837" border="0" align="center">
                            <tr>
                                <td height="32" align="center" valign="top"><?= $trip->name ?><br /><?= $trip->arrival_date ?></td>
                            </tr>
                            <tr>
                                <td height="689" align="right" valign="top">
                                    <div class="block-fluid" id="container_trip_details_hotels">
                                        <table class="voicher"  dir="rtl" cellpadding="0" cellspacing="0" width="100%">
                                            <thead>
                                                <tr>
                                                    <th rowspan="2">البرامج</th>
                                                    <th rowspan="2"><?= lang('trip_details_hotel') ?></th>
                                                    <th colspan="2">التاريخ</th>
                                                    <th colspan="<?= count($roomsizes) ?>">الغرف</th>
                                                    <th rowspan="2">اجمالي الغرف</th>


                                                </tr>
                                                <tr>
                                                    <th><?= lang('trip_details_checkin_date') ?></th>
                                                    <th><?= lang('trip_details_checkout_date') ?></th>
                                                    <? foreach ($roomsizes as $sizename) : ?>
                                                        <th><?= $sizename ?></th>
                                                    <? endforeach ?>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?
                                                if (ensure($hotels)) {
                                                    $counter = 0;
                                                    $previous_erp_city_name = '';
                                                    $previous_erp_hotel_name = '';
                                                    $previous_days = '';

                                                    $row_class = '';
                                                    $previous_row_class = '';
                                                    foreach ($hotels as $hotel) {
                                                        $fhotel = array_shift($hotel['hotel']);
                                                        $rowspan = count($hotel['hotel']);
                                                        $totalrooms = 0;
                                                        ?>
                                                        <tr>

                                                            <td rowspan="<?= $rowspan+1 ?>"><? echo $hotel['safa_packages_name'] ?></td>
                                                            <td ><? echo $fhotel->erp_hotel_name ?></td>
                                                            <td ><?= view_date($fhotel->entry_date, 'date') ?></td>
                                                            <td ><?= view_date($fhotel->exit_date, 'date') ?></td>
                                                            <? foreach ($roomsizes as $size => $sizename) : ?>
                                                                <td rowspan="<?= $rowspan+1 ?>"><?= isset($hotel['rooms'][$size]) ? $hotel['rooms'][$size] : '' ?></td>
                                                                <? if(isset($hotel['rooms'][$size])) $totalrooms += $hotel['rooms'][$size]; ?>
                                                            <? endforeach ?>
                                                                <td rowspan="<?= $rowspan+1 ?>"><?= $totalrooms ?></td>        

                                                        </tr>
                                                        <? foreach ($hotel['hotel'] as $endhotel) :?>
                                                        <tr>
                                                            <td ><? echo $endhotel->erp_hotel_name ?></td>
                                                            <td ><?= view_date($endhotel->entry_date, 'date') ?></td>
                                                            <td ><?= view_date($endhotel->exit_date, 'date') ?></td>
                                                        </tr>
                                                        <? endforeach ?>
                                                    <? } ?>
                                                <? } ?>
                                            </tbody>

                                        </table>
                                    </div>

                                    <br />
                                    <br />
                                    <table width="100%" dir="rtl">
                                        <tr>
                                            <td width="70%">

                                            </td>
                                            <td>
                                                 
                                            </td>
                                        </tr>
                                    </table>

                                </td>
                            </tr>
                            <tr>
                                <td align="center"><img src="<?php echo site_url(); ?>static/img/uo_companies_logos/<?php echo session('uo_id'); ?>/footer.png" width="600" height="106"></td>
                            </tr>
                        </table>


                    </div>    
                </div>
            </div>
        </div>
    </body>
</html>









