<style>
    .content {
        margin-right: 0px !important;
        margin-left: 0px !important;
    }
</style>
<?= form_open_multipart() ?>
<div class="row-fluid">              
    <div class="widget">
        <div class="head dark">
            <div class="icon"><i class="icos-pencil2"></i></div>
            <h2><?= lang('travellers_passport_info') ?></h2>
        </div>                        
        <div class="block-fluid">
            <div class="row-form">
                <div class="span4">
                    <div class="span4" style="text-align:center"><?= lang('travellers_passport_number') ?>:</div>
                    <div class="span8"><input type="text" name="passport_no" value="<?= set_value('passport_no', $item->passport_no) ?>" disabled="disabled" /></div>
                </div>
                <div class="span4">
                    <div class="span4" style="text-align:center"><?= lang('travellers_nationality') ?>:</div>
                    <div class="span8">   
                        <?= form_dropdown('nationality_id', ddgen('erp_countries', array('erp_country_id', name())), set_value('nationality_id', $item->nationality_id), ' disabled="disabled"'," name='s_example' class='select' style='width:100%;' ") ?>
                    </div>
                </div>
                <div class="span4">
                    <div class="span4" style="text-align:center"><?= lang('travellers_gender_id') ?>:</div>
                    <div class="span8">
                        <?= form_dropdown('gender_id', ddgen('erp_gender', array('erp_gender_id', name())), set_value('gender_id', $item->gender_id), ' disabled="disabled"') ?>
                    </div>
                </div>
            </div>  
            <div class="row-form">
                <div class="span8">
                    <div style="float:right" >
                        <div class="span8" style="text-align:right"><?= lang('travellers_personality') ?>:</div>
                        <div class="span10">
                            <?= form_dropdown('erp_title_id', ddgen('erp_titles', array('erp_title_id', name())), set_value('erp_title_id', $item->title_id), ' disabled="disabled"') ?>
                        </div>
                    </div>
                    <div style="float:left">
                        <div class="span2" style="text-align:center"><?= lang('travellers_english_name') ?>:</div>
                        <div class="span10">
                            <input type="text" name="first_name_la" class="input-small" value="<?= $item->first_name_la ?>" disabled="disabled" /> 
                            <input type="text" name="second_name_la " class="input-mini" value="<?= $item->second_name_la ?>" disabled="disabled" />
                            <input type="text" name="third_name_la" class="input-mini" value="<?= $item->third_name_la ?>" disabled="disabled" />
                            <input type="text" name="fourth_name_la" class="input-mini" value="<?= $item->fourth_name_la ?>" disabled="disabled" />
                        </div>
                    </div>
                    <div style=" float:left; ">
                        <div class="span2" style="text-align:center"><?= lang('travellers_arabic_name') ?>:</div>
                        <div class="span10">
                            <input type="text" name="first_name_ar" class="input-small"  value="<?= $item->first_name_ar ?>" disabled="disabled" /> 
                            <input type="text" name="second_name_ar " class="input-mini" value="<?= $item->second_name_ar ?>" disabled="disabled" />
                            <input type="text" name="third_name_ar" class="input-mini" value="<?= $item->third_name_ar ?>" disabled="disabled" />
                            <input type="text" name="fourth_name_ar" class="input-mini" value="<?= $item->fourth_name_ar ?>" disabled="disabled" />
                        </div>
                    </div>
                </div>
                <div class="span4">
                    <div class="fileupload fileupload-new" data-provides="fileupload">
                        <div class="fileupload-preview thumbnail" style="width: 100px; height: 75px; float:left">
                            <? if($item->picture): ?><img src="<?= base_url() ?>/static/uploads/<?= $item->picture ?>" /><? else: ?><img height="75" src="<?= IMAGES ?>/noimg.gif" /><? endif ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row-form">
                <div class="span4">
                    <div class="span4" style="text-align:center"><?= lang('travellers_nationality_id') ?>:</div>
                    <div class="span8"><input type="text" name="nationality_id" value="<?= $item->id_number ?>" disabled="disabled" /></div>
                </div>
            </div>
            <div class="row-form">
                <div class="span4">
                    <div class="span4" style="text-align:center"><?= lang('travellers_passport_type') ?>:</div>
                    <div class="span8">
                        <?= form_dropdown('passport_type_id', ddgen('erp_passporttypes', array('erp_passporttype_id', name())), set_value('passport_type_id', $item->passport_type_id), ' disabled="disabled"') ?>
                    </div>
                </div>
                <div class="span4">
                    <div class="span4" style="text-align:center"><?= lang('travellers_education_levels') ?>:</div>
                    <div class="span8">
                        <?= form_dropdown('educational_level_id', ddgen('erp_educationlevels', array('erp_educationlevel_id', name())), set_value('educational_level_id', $item->educational_level_id), ' disabled="disabled"') ?>
                    </div>
                </div>
                <div class="span4">
                    <div class="span4" style="text-align:center"><?= lang('travellers_family_status') ?>:</div>
                    <div class="span8">
                        <?= form_dropdown('marital_status_id', ddgen('erp_maritalstatus', array('erp_maritalstatus_id', name())), set_value('marital_status_id', $item->marital_status_id), ' disabled="disabled"') ?>
                    </div>
                </div>
            </div>
            <div class="row-form">
                <div class="span4">
                    <div class="span4" style="text-align:center"><?= lang('travellers_country') ?>:</div>
                    <div class="span8">
                        <?= form_dropdown('erp_country_id', ddgen('erp_countries', array('erp_country_id', name())), set_value('erp_country_id', $item->erp_country_id), ' disabled="disabled"'," name='s_example' class='select' style='width:100%;' ") ?>
                    </div>
                </div>
                <div class="span4">
                    <div class="span4" style="text-align:center"><?= lang('travellers_birth_country') ?>:</div>
                    <div class="span8">
                        <?= form_dropdown('birth_country_id', ddgen('erp_countries', array('erp_country_id', name())), set_value('birth_country_id', $item->birth_country_id), ' disabled="disabled"'," name='s_example' class='select' style='width:100%;' ") ?>
                    </div>
                </div>
                <div class="span4">
                    <div class="span4" style="text-align:center"><?= lang('travellers_export_country') ?>:</div>
                    <div class="span8">
                        <?= form_dropdown('passport_issuing_country_id', ddgen('erp_countries', array('erp_country_id', name())), set_value('passport_issuing_country_id', $item->passport_issuing_country_id), ' disabled="disabled"' ," name='s_example' class='select' style='width:100%;' ") ?>
                    </div>
                </div>
            </div>
            <div class="row-form">
                <div class="span4">
                    <div class="span4" style="text-align:center"><?= lang('travellers_birth_place') ?>:</div>
                    <div class="span8"><input type="text" name="birth_city" value="<?= $item->birth_city ?>" disabled="disabled" /></div>
                </div>
                <div class="span4">
                    <div class="span4" style="text-align:center"><?= lang('travellers_birth_date') ?>:</div>
                    <div class="span8">
                        <input type="text" name="date_of_birth" class="datepicker" value="<?= $item->date_of_birth ?>" disabled="disabled" />
                    </div>
                </div>
            </div>
            <div class="row-form">
                <div class="span4">
                    <div class="span4" style="text-align:center"><?= lang('travellers_start_place') ?>:</div>
                    <div class="span8"><input type="text" name="passport_issuing_city" value="<?= $item->passport_issuing_city ?>" disabled="disabled" /></div>
                </div>
                <div class="span4">
                    <div class="span4" style="text-align:center"><?= lang('travellers_start_date') ?>:</div>
                    <div class="span8"><input type="text" name="passport_issue_date" class="datepicker" value="<?= $item->passport_issue_date ?>" disabled="disabled" /></div>
                </div>
                <div class="span4">
                    <div class="span4" style="text-align:center"><?= lang('travellers_end_place') ?>:</div>
                    <div class="span8"><input type="text" name="passport_expiry_date" class="datepicker" value="<?= $item->passport_expiry_date ?>" disabled="disabled" /></div>
                </div>
            </div>
            <div class="row-form">
                <div class="span8">
                    <div class="span2" style="text-align:center"><?= lang('travellers_address') ?>:</div>
                    <div class="span10"><input type="text" name="address" value="<?= $item->address ?>" disabled="disabled" /></div>
                </div>
                <div class="span4">
                    <div class="span4" style="text-align:center"><?= lang('travellers_mobile') ?>:</div>
                    <div class="span8"><input type="text" name="mobile" value="<?= $item->mobile ?>" disabled="disabled" /></div>
                </div>
            </div>
            <div class="row-form">
                <div class="span8">
                    <div class="span2" style="text-align:center"><?= lang('travellers_me7rm') ?>:</div>
                    <div class="span10">
                        <?= form_dropdown('relative_gender_id', $reative_persons, set_value('gender_id', $item->relative_no), ' disabled="disabled"') ?>             
                    </div>
                </div>
                <div class="span4">
                    <div class="span4" style="text-align:center"><?= lang('travellers_relations') ?>:</div>
                    <div class="span8">
                        <?= form_dropdown('relative_relation_id', ddgen('erp_relations', array('erp_relation_id', name())), set_value('relative_relation_id', $item->relative_relation_id), ' disabled="disabled"') ?>                                
                    </div>
                </div>
            </div>
            <div class="row-form">
                <div class="span12">
                    <div class="span2" style="text-align:center"><?= lang('travellers_notes') ?>:</div>
                    <div class="span10"><textarea disabled="disabled"><?= $item->notes ?></textarea></div>
                </div>
            </div>
            <div class="toolbar bottom TAC">
                <button class="btn btn-primary" onclick="parent.$.fancybox.close();"><?= lang('travellers_close') ?></button>
            </div>
        </div>
    </div>
    <?= form_close() ?>