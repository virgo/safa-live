<!--<div id="fModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">-->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h3 id="myModalLabel">إرسال للوكيل</h3>
            </div>        
            <div class="row-fluid">

            <div class="row-form">
                <div class="span4">
                    <div class="span4" style="text-align:center"><?= lang('trip_details_passport_no')?>:</div>
                    <div class="span8"><input name="passport_no" value="A03136183" disabled="disabled" type="text"></div>
                </div>
                <div class="span4">
                    <div class="span4" style="text-align:center"><?= lang('trip_details_nationality')?>:</div>
                    <div class="span8">   
                        <? form_dropdown('nationality_id', ddgen('nationalities', name()), set_value('nationality_id'), 'disabled="disabled"') ?>
                    </div>
                </div>
                <div class="span4">
                    <div class="span4" style="text-align:center"><?= lang('trip_details_sex')?>:</div>
                    <div class="span8">
                        <select name="gender_id" disabled="disabled">
<option value=""><?= lang('trip_details_select_from_menu')?></option>
<option value="1" selected="selected"><?= lang('trip_details_male')?></option>
<option value="2"><?= lang('trip_details_female')?></option>
</select>                    </div>
                </div>
            </div>  
            <div class="row-form">
                <div class="span8">
                    <div style="float:right">
                        <div class="span8" style="text-align:right"><?= lang('trip_details_personality')?>:</div>
                        <div class="span10">
                            <select name="erp_title_id" disabled="disabled">
<option value=""><?= lang('trip_details_select_from_menu')?></option>
<option value="1" selected="selected"><?= lang('trip_details_mr')?></option>
<option value="2"><?= lang('trip_details_women')?></option>
<option value="3"><?= lang('trip_details_lady')?></option>
<option value="4"><?= lang('trip_details_dr')?></option>
<option value="5"><?= lang('trip_details_s3adt')?></option>
<option value="6"><?= lang('trip_details_smo')?></option>
<option value="99"><?= lang('trip_details_other')?></option>
</select>                        </div>
                    </div>
                    <div style="float:left">
                        <div class="span2" style="text-align:center"><?= lang('trip_details_english_name')?>:</div>
                        <div class="span10">
                            <input name="first_name_la" class="input-small" value="" disabled="disabled" type="text"> 
                            <input name="second_name_la " class="input-mini" value="" disabled="disabled" type="text">
                            <input name="third_name_la" class="input-mini" value="" disabled="disabled" type="text">
                            <input name="fourth_name_la" class="input-mini" value="" disabled="disabled" type="text">
                        </div>
                    </div>
                    <div style=" float:left; ">
                        <div class="span2" style="text-align:center"><?= lang('trip_details_arabic_name')?>:</div>
                        <div class="span10">
                            <input name="first_name_ar" class="input-small" value="محمد" disabled="disabled" type="text"> 
                            <input name="second_name_ar " class="input-mini" value="السعيد" disabled="disabled" type="text">
                            <input name="third_name_ar" class="input-mini" value="سلامه" disabled="disabled" type="text">
                            <input name="fourth_name_ar" class="input-mini" value="الصماد" disabled="disabled" type="text">
                        </div>
                    </div>
                </div>
                <div class="span4">
                    <div class="fileupload fileupload-new" data-provides="fileupload">
                        <div class="fileupload-preview thumbnail" style="width: 100px; height: 75px; float:left">
                            <img src="http://localhost/safa_live_phase1/static/img/noimg.gif">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row-form">
                <div class="span4">
                    <div class="span4" style="text-align:center"><?= lang('trip_details_national_id')?>:</div>
                    <div class="span8"><input name="nationality_id" value="24405161800776" disabled="disabled" type="text"></div>
                </div>
            </div>
            <div class="row-form">
                <div class="span4">
                    <div class="span4" style="text-align:center"><?= lang('trip_details_passport_type')?>:</div>
                    <div class="span8">
                        <select name="passport_type_id" disabled="disabled">
<option value=""><?= lang('trip_details_select_from_menu')?></option>
<option value="1" selected="selected"><?= lang('trip_details_normal')?></option>
<option value="2"><?= lang('trip_details_deplmacy')?></option>
<option value="3"><?= lang('trip_details_travel_docs')?></option>
<option value="4"><?= lang('trip_details_usa_passport')?></option>
<option value="99"><?= lang('trip_details_other')?></option>
</select>                    </div>
                </div>
                <div class="span4">
                    <div class="span4" style="text-align:center"><?= lang('trip_details_education_level')?>:</div>
                    <div class="span8">
                        <select name="educational_level_id" disabled="disabled">
<option value=""><?= lang('trip_details_select_from_menu')?></option>
<option value="1"><?= lang('trip_details_without_education')?></option>
<option value="2"><?= lang('trip_details_primary_education')?></option>
<option value="3"><?= lang('trip_details_secondry_education')?></option>
<option value="4" selected="selected"><?= lang('trip_details_universal_education')?></option>
<option value="99"><?= lang('trip_details_other')?></option>
</select>                    </div>
                </div>
                <div class="span4">
                    <div class="span4" style="text-align:center"><?= lang('rip_details_family_status')?>:</div>
                    <div class="span8">
                        <select name="marital_status_id" disabled="disabled">
<option value=""><?=lang('trip_details_select_from_menu')?></option>
<option value="1"><?= lang('trip_details_single')?></option>
<option value="2" selected="selected"><?= lang('trip_details_marriad')?></option>
<option value="3"><?= lang('trip_details_free')?></option>
<option value="4"><?= lang('trip_details_widower')?></option>
<option value="99"><?= lang('trip_details_other')?></option>
</select>                    </div>
                </div>
            </div>
            <div class="row-form">
                <div class="span4">
                    <div class="span4" style="text-align:center"><?= lang('trip_details_country')?>:</div>
                    <div class="span8">
                        <select name="erp_country_id" disabled="disabled">
<option value=""><?= lang('trip_details_select_from_menu')?></option>
<option value="1">الولايات المتحدة الأمريكية</option>
<option value="20" selected="selected">مصر</option>
<option value="27">جنوب افريقيا</option>
<option value="30">اليونان</option>
<option value="31">هولندا</option>
<option value="32">بلجيكا</option>
<option value="33">فرنسا</option>
<option value="34">أسبانيا</option>
<option value="36">هنغاريا</option>
<option value="39">إيطاليا</option>
<option value="40">رومانيا</option>
<option value="41">سويسرا</option>
<option value="43">النمسا</option>
<option value="44">المملكة المتحدة</option>
<option value="45">الدنمارك</option>
<option value="46">السويد</option>
<option value="47">النرويج</option>
<option value="48">بولندا</option>
<option value="49">ألمانيا</option>
<option value="51">البيرو</option>
<option value="52">المكسيك</option>
<option value="53">كوبا</option>
<option value="54">الأرجنتين</option>
<option value="55">البرازيل</option>
<option value="56">تشيلي</option>
<option value="57">كولومبيا</option>
<option value="58">فينزويلا</option>
<option value="60">ماليزيا</option>
<option value="61">أستراليا</option>
<option value="62">إندونيسيا</option>
<option value="63">الفليبين</option>
<option value="64">نيوزيلاند</option>
<option value="65">سنغافورة</option>
<option value="66">تايلاند</option>
<option value="81">اليابان</option>
<option value="82">كوريا الجنوبية</option>
<option value="84">فيتنام</option>
<option value="86">صيني</option>
<option value="90">تركيا</option>
<option value="91">الهند</option>
<option value="92">الباكستان</option>
<option value="93">أفغانستان</option>
<option value="94">سريلانكا</option>
<option value="95">مانيمار</option>
<option value="98">إيران</option>
<option value="212">المغرب</option>
<option value="213">الجزائر</option>
<option value="216">تونس</option>
<option value="218">ليبيا</option>
<option value="220">غامبيا</option>
<option value="221">السنغال</option>
<option value="222">موريتانيا</option>
<option value="223">جمهورية مالي</option>
<option value="224">غينيا</option>
<option value="225">ساحل العاج</option>
<option value="226">بوركينا فاسو</option>
<option value="227">النيجر</option>
<option value="228">توغو</option>
<option value="229">بنين</option>
<option value="230">موريشس</option>
<option value="231">ليبيريا</option>
<option value="232">سيراليون</option>
<option value="233">غانا</option>
<option value="234">نيجيريا</option>
<option value="235">تشاد</option>
<option value="236">افريقيا الوسطى</option>
<option value="237">الكاميرون</option>
<option value="238">جزر رأس أخضر</option>
<option value="239">برينسيب</option>
<option value="240">غينيا الإستوائية</option>
<option value="241">جابون</option>
<option value="242">الكونغو</option>
<option value="243">كونجو - زائير</option>
<option value="244">أنغولا</option>
<option value="245">غينيا بيساو</option>
<option value="246">ديغوغارسيا</option>
<option value="247">اسنسيون جزر</option>
<option value="248">جزر سيشيل</option>
<option value="249">السودان</option>
<option value="250">رواندا</option>
<option value="251">إثيوبيا</option>
<option value="252">الصومال</option>
<option value="253">جيبوتي</option>
<option value="254">كينيا</option>
<option value="256">أوغندا</option>
<option value="257">بوروندي</option>
<option value="258">موزمبيق</option>
<option value="260">زامبيا</option>
<option value="261">ملاجاسي/مدغشقر</option>
<option value="262">رينيون</option>
<option value="263">زمبابوي</option>
<option value="265">مالاوي</option>
<option value="266">ليسوتو</option>
<option value="267">بوتسوانا</option>
<option value="284">جزيرة فيرجين البريطانية</option>
<option value="290">سانت هيلانة</option>
<option value="291">إرتريا</option>
<option value="297">اروبا</option>
<option value="298">جزر فيرو</option>
<option value="299">جرين لاند</option>
<option value="340">جزر فيرجين الامريكية</option>
<option value="345">جزر كايمن</option>
<option value="350">جبل طارق</option>
<option value="351">البرتغال</option>
<option value="352">لوكسمبورج</option>
<option value="353">ايرلندا</option>
<option value="354">ايسلندا</option>
<option value="355">البانيا</option>
<option value="356">مالطا</option>
<option value="357">قبرص</option>
<option value="358">فنلندا</option>
<option value="359">بلغاريا</option>
<option value="370">ليتوانيا</option>
<option value="371">لاتفيا</option>
<option value="372">ايستونيا</option>
<option value="373">مولدوفا</option>
<option value="374">ارمينيا</option>
<option value="375">بيلاروسيا</option>
<option value="376">اندورا</option>
<option value="377">موناكو</option>
<option value="378">سان مارينو</option>
<option value="380">اوكرانيا</option>
<option value="385">كرواتيا</option>
<option value="386">سلوفينيا</option>
<option value="387">البوسنة والهرسك</option>
<option value="389">مقدونيا</option>
<option value="420">تشيكيا</option>
<option value="421">سلوفاكيا</option>
<option value="423">لختنشتاين</option>
<option value="441">بيرمودا</option>
<option value="473">غرينادا</option>
<option value="500">جزر فولكلاند</option>
<option value="501">بيليز</option>
<option value="502">غواتيمالا</option>
<option value="503">السلفادور</option>
<option value="504">هندوراس</option>
<option value="505">نيكاراغوا</option>
<option value="506">كوستاريكا</option>
<option value="507">بنما</option>
<option value="508">سانت بيير وميكلون</option>
<option value="509">هاييي</option>
<option value="590">جيودي لوب</option>
<option value="591">بوليفيا</option>
<option value="592">جواني</option>
<option value="593">الاكوادور</option>
<option value="594">جيانا الفرنسية</option>
<option value="595">الباراغواي</option>
<option value="597">سورينام</option>
<option value="598">الاوروغواي</option>
<option value="649">جزر تركس وكيكوس</option>
<option value="664">مونت سيرات</option>
<option value="671">جوام</option>
<option value="673">بروناي</option>
<option value="674">نورو</option>
<option value="675">بابوا نيوغينيا</option>
<option value="676">تونجا</option>
<option value="677">جزيرة سليمان</option>
<option value="678">فانويتو</option>
<option value="679">جزر فيجي</option>
<option value="680">بيلو</option>
<option value="681">جزيرة والس وفوتونا</option>
<option value="682">جزر كوك</option>
<option value="683">نيو</option>
<option value="684">ساموا الامريكية</option>
<option value="685">ساموا الغربية</option>
<option value="686">كيريباتي</option>
<option value="687">كاليدونيا الجديدة</option>
<option value="688">توفالو</option>
<option value="689">بولينيسيا الفرنسية</option>
<option value="690">توكيلاو</option>
<option value="691">ميكرونيسيا</option>
<option value="692">جزر مارشال</option>
<option value="758">سانت لوشيا</option>
<option value="767">دومينيكان</option>
<option value="784">سنت فينسنت و جريندين</option>
<option value="809">جمهورية دومينيكان</option>
<option value="850">كوريا الشمالية</option>
<option value="852">هونغ كونغ</option>
<option value="853">مكاو</option>
<option value="855">كمبوديا</option>
<option value="856">لاوس</option>
<option value="868">ترنداد وتوباجو</option>
<option value="876">جامايكا</option>
<option value="880">بنجلادش</option>
<option value="886">تايوان</option>
<option value="960">المالديف</option>
<option value="961">لبنان</option>
<option value="962">الأردن</option>
<option value="963">سوريا</option>
<option value="964">العراق</option>
<option value="965">الكويت</option>
<option value="966">المملكة العربية السعودية</option>
<option value="967">اليمن</option>
<option value="968">عمان</option>
<option value="970">فلسطين</option>
<option value="971">الامارات العربية المتحدة</option>
<option value="972">فلسطين عرب</option>
<option value="973">البحرين</option>
<option value="974">قطر</option>
<option value="975">بهوتان</option>
<option value="976">منغوليا</option>
<option value="977">نيبال</option>
<option value="992">طاجكستان</option>
<option value="993">تركمانستان</option>
<option value="994">أذربيجان</option>
<option value="995">جورجيا</option>
<option value="996">جمهوية قرغيزيا</option>
<option value="998">اوزبكستان</option>
<option value="1039">فاتيكان</option>
<option value="1061">جزيرة كريسماس</option>
<option value="1112">كندا</option>
<option value="1242">الباهاماز</option>
<option value="1246">باربادوس</option>
<option value="1787">بورتوريكو</option>
<option value="2061">جزرة كوكو- كيلنج</option>
<option value="2551">تنزانيا</option>
<option value="2552">زنجبار</option>
<option value="2641">انجويليا</option>
<option value="2642">ناميبيا</option>
<option value="2681">انتيكو</option>
<option value="2682">باربودا</option>
<option value="2683">سويزلاند</option>
<option value="2691">جزر القمر</option>
<option value="2692">جزيرة مايوت</option>
<option value="3811">سيبيريا</option>
<option value="3812">يوغسلافيا</option>
<option value="5961">فرنسية انتيل</option>
<option value="5962">مارتينيكو</option>
<option value="5991">كوراكاو</option>
<option value="5992">هولندية انتيل</option>
<option value="6701">تيمور الشرقية</option>
<option value="6702">جزر ماريانا الشمالية</option>
<option value="6721">انتاركتيكا</option>
<option value="6722">جزر نورفولك</option>
<option value="7001">كازخستان</option>
<option value="7002">روسيا</option>
<option value="8081">جزيرة ميدواي</option>
<option value="8082">جزيرة ويك</option>
<option value="8691">نفيس</option>
<option value="8692">سانت كتس ونيفيس</option>
</select>                    </div>
                </div>
                <div class="span4">
                    <div class="span4" style="text-align:center"><?= lang('trip_details_birth_country')?>:</div>
                    <div class="span8">
                        <select name="birth_country_id" disabled="disabled">
<option value=""><?= lang('trip_details_select_from_menu')?></option>
<option value="1">الولايات المتحدة الأمريكية</option>
<option value="20" selected="selected">مصر</option>
<option value="27">جنوب افريقيا</option>
<option value="30">اليونان</option>
<option value="31">هولندا</option>
<option value="32">بلجيكا</option>
<option value="33">فرنسا</option>
<option value="34">أسبانيا</option>
<option value="36">هنغاريا</option>
<option value="39">إيطاليا</option>
<option value="40">رومانيا</option>
<option value="41">سويسرا</option>
<option value="43">النمسا</option>
<option value="44">المملكة المتحدة</option>
<option value="45">الدنمارك</option>
<option value="46">السويد</option>
<option value="47">النرويج</option>
<option value="48">بولندا</option>
<option value="49">ألمانيا</option>
<option value="51">البيرو</option>
<option value="52">المكسيك</option>
<option value="53">كوبا</option>
<option value="54">الأرجنتين</option>
<option value="55">البرازيل</option>
<option value="56">تشيلي</option>
<option value="57">كولومبيا</option>
<option value="58">فينزويلا</option>
<option value="60">ماليزيا</option>
<option value="61">أستراليا</option>
<option value="62">إندونيسيا</option>
<option value="63">الفليبين</option>
<option value="64">نيوزيلاند</option>
<option value="65">سنغافورة</option>
<option value="66">تايلاند</option>
<option value="81">اليابان</option>
<option value="82">كوريا الجنوبية</option>
<option value="84">فيتنام</option>
<option value="86">صيني</option>
<option value="90">تركيا</option>
<option value="91">الهند</option>
<option value="92">الباكستان</option>
<option value="93">أفغانستان</option>
<option value="94">سريلانكا</option>
<option value="95">مانيمار</option>
<option value="98">إيران</option>
<option value="212">المغرب</option>
<option value="213">الجزائر</option>
<option value="216">تونس</option>
<option value="218">ليبيا</option>
<option value="220">غامبيا</option>
<option value="221">السنغال</option>
<option value="222">موريتانيا</option>
<option value="223">جمهورية مالي</option>
<option value="224">غينيا</option>
<option value="225">ساحل العاج</option>
<option value="226">بوركينا فاسو</option>
<option value="227">النيجر</option>
<option value="228">توغو</option>
<option value="229">بنين</option>
<option value="230">موريشس</option>
<option value="231">ليبيريا</option>
<option value="232">سيراليون</option>
<option value="233">غانا</option>
<option value="234">نيجيريا</option>
<option value="235">تشاد</option>
<option value="236">افريقيا الوسطى</option>
<option value="237">الكاميرون</option>
<option value="238">جزر رأس أخضر</option>
<option value="239">برينسيب</option>
<option value="240">غينيا الإستوائية</option>
<option value="241">جابون</option>
<option value="242">الكونغو</option>
<option value="243">كونجو - زائير</option>
<option value="244">أنغولا</option>
<option value="245">غينيا بيساو</option>
<option value="246">ديغوغارسيا</option>
<option value="247">اسنسيون جزر</option>
<option value="248">جزر سيشيل</option>
<option value="249">السودان</option>
<option value="250">رواندا</option>
<option value="251">إثيوبيا</option>
<option value="252">الصومال</option>
<option value="253">جيبوتي</option>
<option value="254">كينيا</option>
<option value="256">أوغندا</option>
<option value="257">بوروندي</option>
<option value="258">موزمبيق</option>
<option value="260">زامبيا</option>
<option value="261">ملاجاسي/مدغشقر</option>
<option value="262">رينيون</option>
<option value="263">زمبابوي</option>
<option value="265">مالاوي</option>
<option value="266">ليسوتو</option>
<option value="267">بوتسوانا</option>
<option value="284">جزيرة فيرجين البريطانية</option>
<option value="290">سانت هيلانة</option>
<option value="291">إرتريا</option>
<option value="297">اروبا</option>
<option value="298">جزر فيرو</option>
<option value="299">جرين لاند</option>
<option value="340">جزر فيرجين الامريكية</option>
<option value="345">جزر كايمن</option>
<option value="350">جبل طارق</option>
<option value="351">البرتغال</option>
<option value="352">لوكسمبورج</option>
<option value="353">ايرلندا</option>
<option value="354">ايسلندا</option>
<option value="355">البانيا</option>
<option value="356">مالطا</option>
<option value="357">قبرص</option>
<option value="358">فنلندا</option>
<option value="359">بلغاريا</option>
<option value="370">ليتوانيا</option>
<option value="371">لاتفيا</option>
<option value="372">ايستونيا</option>
<option value="373">مولدوفا</option>
<option value="374">ارمينيا</option>
<option value="375">بيلاروسيا</option>
<option value="376">اندورا</option>
<option value="377">موناكو</option>
<option value="378">سان مارينو</option>
<option value="380">اوكرانيا</option>
<option value="385">كرواتيا</option>
<option value="386">سلوفينيا</option>
<option value="387">البوسنة والهرسك</option>
<option value="389">مقدونيا</option>
<option value="420">تشيكيا</option>
<option value="421">سلوفاكيا</option>
<option value="423">لختنشتاين</option>
<option value="441">بيرمودا</option>
<option value="473">غرينادا</option>
<option value="500">جزر فولكلاند</option>
<option value="501">بيليز</option>
<option value="502">غواتيمالا</option>
<option value="503">السلفادور</option>
<option value="504">هندوراس</option>
<option value="505">نيكاراغوا</option>
<option value="506">كوستاريكا</option>
<option value="507">بنما</option>
<option value="508">سانت بيير وميكلون</option>
<option value="509">هاييي</option>
<option value="590">جيودي لوب</option>
<option value="591">بوليفيا</option>
<option value="592">جواني</option>
<option value="593">الاكوادور</option>
<option value="594">جيانا الفرنسية</option>
<option value="595">الباراغواي</option>
<option value="597">سورينام</option>
<option value="598">الاوروغواي</option>
<option value="649">جزر تركس وكيكوس</option>
<option value="664">مونت سيرات</option>
<option value="671">جوام</option>
<option value="673">بروناي</option>
<option value="674">نورو</option>
<option value="675">بابوا نيوغينيا</option>
<option value="676">تونجا</option>
<option value="677">جزيرة سليمان</option>
<option value="678">فانويتو</option>
<option value="679">جزر فيجي</option>
<option value="680">بيلو</option>
<option value="681">جزيرة والس وفوتونا</option>
<option value="682">جزر كوك</option>
<option value="683">نيو</option>
<option value="684">ساموا الامريكية</option>
<option value="685">ساموا الغربية</option>
<option value="686">كيريباتي</option>
<option value="687">كاليدونيا الجديدة</option>
<option value="688">توفالو</option>
<option value="689">بولينيسيا الفرنسية</option>
<option value="690">توكيلاو</option>
<option value="691">ميكرونيسيا</option>
<option value="692">جزر مارشال</option>
<option value="758">سانت لوشيا</option>
<option value="767">دومينيكان</option>
<option value="784">سنت فينسنت و جريندين</option>
<option value="809">جمهورية دومينيكان</option>
<option value="850">كوريا الشمالية</option>
<option value="852">هونغ كونغ</option>
<option value="853">مكاو</option>
<option value="855">كمبوديا</option>
<option value="856">لاوس</option>
<option value="868">ترنداد وتوباجو</option>
<option value="876">جامايكا</option>
<option value="880">بنجلادش</option>
<option value="886">تايوان</option>
<option value="960">المالديف</option>
<option value="961">لبنان</option>
<option value="962">الأردن</option>
<option value="963">سوريا</option>
<option value="964">العراق</option>
<option value="965">الكويت</option>
<option value="966">المملكة العربية السعودية</option>
<option value="967">اليمن</option>
<option value="968">عمان</option>
<option value="970">فلسطين</option>
<option value="971">الامارات العربية المتحدة</option>
<option value="972">فلسطين عرب</option>
<option value="973">البحرين</option>
<option value="974">قطر</option>
<option value="975">بهوتان</option>
<option value="976">منغوليا</option>
<option value="977">نيبال</option>
<option value="992">طاجكستان</option>
<option value="993">تركمانستان</option>
<option value="994">أذربيجان</option>
<option value="995">جورجيا</option>
<option value="996">جمهوية قرغيزيا</option>
<option value="998">اوزبكستان</option>
<option value="1039">فاتيكان</option>
<option value="1061">جزيرة كريسماس</option>
<option value="1112">كندا</option>
<option value="1242">الباهاماز</option>
<option value="1246">باربادوس</option>
<option value="1787">بورتوريكو</option>
<option value="2061">جزرة كوكو- كيلنج</option>
<option value="2551">تنزانيا</option>
<option value="2552">زنجبار</option>
<option value="2641">انجويليا</option>
<option value="2642">ناميبيا</option>
<option value="2681">انتيكو</option>
<option value="2682">باربودا</option>
<option value="2683">سويزلاند</option>
<option value="2691">جزر القمر</option>
<option value="2692">جزيرة مايوت</option>
<option value="3811">سيبيريا</option>
<option value="3812">يوغسلافيا</option>
<option value="5961">فرنسية انتيل</option>
<option value="5962">مارتينيكو</option>
<option value="5991">كوراكاو</option>
<option value="5992">هولندية انتيل</option>
<option value="6701">تيمور الشرقية</option>
<option value="6702">جزر ماريانا الشمالية</option>
<option value="6721">انتاركتيكا</option>
<option value="6722">جزر نورفولك</option>
<option value="7001">كازخستان</option>
<option value="7002">روسيا</option>
<option value="8081">جزيرة ميدواي</option>
<option value="8082">جزيرة ويك</option>
<option value="8691">نفيس</option>
<option value="8692">سانت كتس ونيفيس</option>
</select>                    </div>
                </div>
                <div class="span4">
                    <div class="span4" style="text-align:center"><?= lang('trip_details_export_country')?>:</div>
                    <div class="span8">
                        <select name="passport_issuing_country_id" disabled="disabled">
<option value=""><?= lang('trip_details_select_from_menu')?></option>
<option value="1">الولايات المتحدة الأمريكية</option>
<option value="20" selected="selected">مصر</option>
<option value="27">جنوب افريقيا</option>
<option value="30">اليونان</option>
<option value="31">هولندا</option>
<option value="32">بلجيكا</option>
<option value="33">فرنسا</option>
<option value="34">أسبانيا</option>
<option value="36">هنغاريا</option>
<option value="39">إيطاليا</option>
<option value="40">رومانيا</option>
<option value="41">سويسرا</option>
<option value="43">النمسا</option>
<option value="44">المملكة المتحدة</option>
<option value="45">الدنمارك</option>
<option value="46">السويد</option>
<option value="47">النرويج</option>
<option value="48">بولندا</option>
<option value="49">ألمانيا</option>
<option value="51">البيرو</option>
<option value="52">المكسيك</option>
<option value="53">كوبا</option>
<option value="54">الأرجنتين</option>
<option value="55">البرازيل</option>
<option value="56">تشيلي</option>
<option value="57">كولومبيا</option>
<option value="58">فينزويلا</option>
<option value="60">ماليزيا</option>
<option value="61">أستراليا</option>
<option value="62">إندونيسيا</option>
<option value="63">الفليبين</option>
<option value="64">نيوزيلاند</option>
<option value="65">سنغافورة</option>
<option value="66">تايلاند</option>
<option value="81">اليابان</option>
<option value="82">كوريا الجنوبية</option>
<option value="84">فيتنام</option>
<option value="86">صيني</option>
<option value="90">تركيا</option>
<option value="91">الهند</option>
<option value="92">الباكستان</option>
<option value="93">أفغانستان</option>
<option value="94">سريلانكا</option>
<option value="95">مانيمار</option>
<option value="98">إيران</option>
<option value="212">المغرب</option>
<option value="213">الجزائر</option>
<option value="216">تونس</option>
<option value="218">ليبيا</option>
<option value="220">غامبيا</option>
<option value="221">السنغال</option>
<option value="222">موريتانيا</option>
<option value="223">جمهورية مالي</option>
<option value="224">غينيا</option>
<option value="225">ساحل العاج</option>
<option value="226">بوركينا فاسو</option>
<option value="227">النيجر</option>
<option value="228">توغو</option>
<option value="229">بنين</option>
<option value="230">موريشس</option>
<option value="231">ليبيريا</option>
<option value="232">سيراليون</option>
<option value="233">غانا</option>
<option value="234">نيجيريا</option>
<option value="235">تشاد</option>
<option value="236">افريقيا الوسطى</option>
<option value="237">الكاميرون</option>
<option value="238">جزر رأس أخضر</option>
<option value="239">برينسيب</option>
<option value="240">غينيا الإستوائية</option>
<option value="241">جابون</option>
<option value="242">الكونغو</option>
<option value="243">كونجو - زائير</option>
<option value="244">أنغولا</option>
<option value="245">غينيا بيساو</option>
<option value="246">ديغوغارسيا</option>
<option value="247">اسنسيون جزر</option>
<option value="248">جزر سيشيل</option>
<option value="249">السودان</option>
<option value="250">رواندا</option>
<option value="251">إثيوبيا</option>
<option value="252">الصومال</option>
<option value="253">جيبوتي</option>
<option value="254">كينيا</option>
<option value="256">أوغندا</option>
<option value="257">بوروندي</option>
<option value="258">موزمبيق</option>
<option value="260">زامبيا</option>
<option value="261">ملاجاسي/مدغشقر</option>
<option value="262">رينيون</option>
<option value="263">زمبابوي</option>
<option value="265">مالاوي</option>
<option value="266">ليسوتو</option>
<option value="267">بوتسوانا</option>
<option value="284">جزيرة فيرجين البريطانية</option>
<option value="290">سانت هيلانة</option>
<option value="291">إرتريا</option>
<option value="297">اروبا</option>
<option value="298">جزر فيرو</option>
<option value="299">جرين لاند</option>
<option value="340">جزر فيرجين الامريكية</option>
<option value="345">جزر كايمن</option>
<option value="350">جبل طارق</option>
<option value="351">البرتغال</option>
<option value="352">لوكسمبورج</option>
<option value="353">ايرلندا</option>
<option value="354">ايسلندا</option>
<option value="355">البانيا</option>
<option value="356">مالطا</option>
<option value="357">قبرص</option>
<option value="358">فنلندا</option>
<option value="359">بلغاريا</option>
<option value="370">ليتوانيا</option>
<option value="371">لاتفيا</option>
<option value="372">ايستونيا</option>
<option value="373">مولدوفا</option>
<option value="374">ارمينيا</option>
<option value="375">بيلاروسيا</option>
<option value="376">اندورا</option>
<option value="377">موناكو</option>
<option value="378">سان مارينو</option>
<option value="380">اوكرانيا</option>
<option value="385">كرواتيا</option>
<option value="386">سلوفينيا</option>
<option value="387">البوسنة والهرسك</option>
<option value="389">مقدونيا</option>
<option value="420">تشيكيا</option>
<option value="421">سلوفاكيا</option>
<option value="423">لختنشتاين</option>
<option value="441">بيرمودا</option>
<option value="473">غرينادا</option>
<option value="500">جزر فولكلاند</option>
<option value="501">بيليز</option>
<option value="502">غواتيمالا</option>
<option value="503">السلفادور</option>
<option value="504">هندوراس</option>
<option value="505">نيكاراغوا</option>
<option value="506">كوستاريكا</option>
<option value="507">بنما</option>
<option value="508">سانت بيير وميكلون</option>
<option value="509">هاييي</option>
<option value="590">جيودي لوب</option>
<option value="591">بوليفيا</option>
<option value="592">جواني</option>
<option value="593">الاكوادور</option>
<option value="594">جيانا الفرنسية</option>
<option value="595">الباراغواي</option>
<option value="597">سورينام</option>
<option value="598">الاوروغواي</option>
<option value="649">جزر تركس وكيكوس</option>
<option value="664">مونت سيرات</option>
<option value="671">جوام</option>
<option value="673">بروناي</option>
<option value="674">نورو</option>
<option value="675">بابوا نيوغينيا</option>
<option value="676">تونجا</option>
<option value="677">جزيرة سليمان</option>
<option value="678">فانويتو</option>
<option value="679">جزر فيجي</option>
<option value="680">بيلو</option>
<option value="681">جزيرة والس وفوتونا</option>
<option value="682">جزر كوك</option>
<option value="683">نيو</option>
<option value="684">ساموا الامريكية</option>
<option value="685">ساموا الغربية</option>
<option value="686">كيريباتي</option>
<option value="687">كاليدونيا الجديدة</option>
<option value="688">توفالو</option>
<option value="689">بولينيسيا الفرنسية</option>
<option value="690">توكيلاو</option>
<option value="691">ميكرونيسيا</option>
<option value="692">جزر مارشال</option>
<option value="758">سانت لوشيا</option>
<option value="767">دومينيكان</option>
<option value="784">سنت فينسنت و جريندين</option>
<option value="809">جمهورية دومينيكان</option>
<option value="850">كوريا الشمالية</option>
<option value="852">هونغ كونغ</option>
<option value="853">مكاو</option>
<option value="855">كمبوديا</option>
<option value="856">لاوس</option>
<option value="868">ترنداد وتوباجو</option>
<option value="876">جامايكا</option>
<option value="880">بنجلادش</option>
<option value="886">تايوان</option>
<option value="960">المالديف</option>
<option value="961">لبنان</option>
<option value="962">الأردن</option>
<option value="963">سوريا</option>
<option value="964">العراق</option>
<option value="965">الكويت</option>
<option value="966">المملكة العربية السعودية</option>
<option value="967">اليمن</option>
<option value="968">عمان</option>
<option value="970">فلسطين</option>
<option value="971">الامارات العربية المتحدة</option>
<option value="972">فلسطين عرب</option>
<option value="973">البحرين</option>
<option value="974">قطر</option>
<option value="975">بهوتان</option>
<option value="976">منغوليا</option>
<option value="977">نيبال</option>
<option value="992">طاجكستان</option>
<option value="993">تركمانستان</option>
<option value="994">أذربيجان</option>
<option value="995">جورجيا</option>
<option value="996">جمهوية قرغيزيا</option>
<option value="998">اوزبكستان</option>
<option value="1039">فاتيكان</option>
<option value="1061">جزيرة كريسماس</option>
<option value="1112">كندا</option>
<option value="1242">الباهاماز</option>
<option value="1246">باربادوس</option>
<option value="1787">بورتوريكو</option>
<option value="2061">جزرة كوكو- كيلنج</option>
<option value="2551">تنزانيا</option>
<option value="2552">زنجبار</option>
<option value="2641">انجويليا</option>
<option value="2642">ناميبيا</option>
<option value="2681">انتيكو</option>
<option value="2682">باربودا</option>
<option value="2683">سويزلاند</option>
<option value="2691">جزر القمر</option>
<option value="2692">جزيرة مايوت</option>
<option value="3811">سيبيريا</option>
<option value="3812">يوغسلافيا</option>
<option value="5961">فرنسية انتيل</option>
<option value="5962">مارتينيكو</option>
<option value="5991">كوراكاو</option>
<option value="5992">هولندية انتيل</option>
<option value="6701">تيمور الشرقية</option>
<option value="6702">جزر ماريانا الشمالية</option>
<option value="6721">انتاركتيكا</option>
<option value="6722">جزر نورفولك</option>
<option value="7001">كازخستان</option>
<option value="7002">روسيا</option>
<option value="8081">جزيرة ميدواي</option>
<option value="8082">جزيرة ويك</option>
<option value="8691">نفيس</option>
<option value="8692">سانت كتس ونيفيس</option>
</select>                    </div>
                </div>
            </div>
            <div class="row-form">
                <div class="span4">
                    <div class="span4" style="text-align:center"><?= lang('trip_details_birth_place')?>:</div>
                    <div class="span8"><input name="birth_city" value="البحيرة" disabled="disabled" type="text"></div>
                </div>
                <div class="span4">
                    <div class="span4" style="text-align:center"><?= lang('trip_details_birth_date')?>:</div>
                    <div class="span8">
                        <input id="dp1370868288375" name="date_of_birth" class="datepicker hasDatepicker" value="1944-05-16" disabled="disabled" type="text">
                    </div>
                </div>
            </div>
            <div class="row-form">
                <div class="span4">
                    <div class="span4" style="text-align:center"><?= lang('trip_details_released_place')?>:</div>
                    <div class="span8"><input name="passport_issuing_city" value="36" disabled="disabled" type="text"></div>
                </div>
                <div class="span4">
                    <div class="span4" style="text-align:center"><?= lang('trip_details_released_date')?>:</div>
                    <div class="span8"><input id="dp1370868288376" name="passport_issue_date" class="datepicker hasDatepicker" value="2011-03-29" disabled="disabled" type="text"></div>
                </div>
                <div class="span4">
                    <div class="span4" style="text-align:center"><?= lang('trip_details_expired_date')?>:</div>
                    <div class="span8"><input id="dp1370868288377" name="passport_expiry_date" class="datepicker hasDatepicker" value="2018-03-28" disabled="disabled" type="text"></div>
                </div>
            </div>
            <div class="row-form">
                <div class="span8">
                    <div class="span2" style="text-align:center"><?= lang('trip_details_address')?>:</div>
                    <div class="span10"><input name="address" value="دمنهور" disabled="disabled" type="text"></div>
                </div>
                <div class="span4">
                    <div class="span4" style="text-align:center"><?= lang('trip_details_mobile')?>:</div>
                    <div class="span8"><input name="mobile" value="" disabled="disabled" type="text"></div>
                </div>
            </div>
            <div class="row-form">
                <div class="span8">
                    <div class="span2" style="text-align:center"><?= lang('trip_details_me7rm')?>:</div>
                    <div class="span10">
                        <select name="relative_gender_id" disabled="disabled">
<option value="0" selected="selected"></option>
<option value="151">محمد السعيد سلامه الصماد</option>
</select>             
                    </div>
                </div>
                <div class="span4">
                    <div class="span4" style="text-align:center"><?= lang('trip_details_person_relations')?>:</div>
                    <div class="span8">
                        <select name="relative_relation_id" disabled="disabled">
<option value=""><?= lang('trip_details_select_from_menu')?></option>
<option value="0" selected="selected"></option>
<option value="1"><?= lang('trip_details_son')?></option>
<option value="2"><?= lang('trip_details_daughter')?></option>
<option value="3"><?= lang('trip_details_father')?></option>
<option value="4"><?= lang('trip_details_mother')?></option>
<option value="5"><?= lang('trip_details_brother')?></option>
<option value="6"><?= lang('trip_details_sister')?></option>
<option value="7"><?= lang('trip_details_husband')?></option>
<option value="8"><?= lang('trip_details_wife')?></option>
<option value="9"><?= lang('trip_details_grandfather')?></option>
<option value="10"><?= lang('trip_details_grandmother')?></option>
<option value="11"><?= lang('trip_details_nephew')?></option>
<option value="12"><?= lang('trip_details_niece')?></option>
<option value="13"><?= lang('trip_details_smelting_wife')?></option>
<option value="14"><?= lang('trip_details_smelting_pair')?></option>
<option value="15"><?= lang('trip_details_league_of_Women')?></option>
<option value="16"><?= lang('trip_details_grandson')?></option>
<option value="17"><?= lang('trip_details_hama')?></option>
<option value="18"><?= lang('trip_details_uncle_m')?></option>
<option value="19"><?= lang('trip_details_uncle_f')?></option>
<option value="20"><?= lang('trip_details_uncle')?></option>
<option value="21"><?= lang('trip_details_Aunt')?></option>
<option value="22"><?= lang('trip_details_son_wife')?></option>
<option value="23"><?= lang('trip_details_daughter_husband')?></option>
<option value="24"><?= lang('trip_details_father_wife')?></option>
<option value="25"><?= lang('trip_details_mother_husband')?></option>
<option value="27"><?= lang('trip_details_hamo')?></option>
<option value="99"><?= lang('trip_details_other')?></option>
</select>                                
                    </div>
                </div>
            </div>
            <div class="row-form">
                <div class="span12">
                    <div class="span2" style="text-align:center"><?= lang('trip_details_notes')?>:</div>
                    <div class="span10"><textarea disabled="disabled"></textarea></div>
                </div>
            </div>

        
            </div>                   
            <div class="modal-footer">
                <button class="btn btn-primary" data-dismiss="modal" aria-hidden="true"><?= lang('trip_details_agree')?></button>            
            </div>
        <!--</div>-->