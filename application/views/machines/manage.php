<style>
    .widget {width:98%}
    input {margin-bottom:4px !important;}
</style>
<span class="span8"></span>
<div class="row-fluid">

    <?= form_open_multipart(false, 'id="frm_eas" ') ?>

    <? if($destination == 'ea'): ?>
    <div class="span12">
        <div class="widget">
            <div class="path-container Fright">
                <div class="path-name Fright">
                    <a href="<?= site_url('ea/dashboard') ?>"><?= lang('global_system_management') ?></a>
                </div>
                <div class="path-arrow Fright"></div>
                <div class="path-name Fright">
                    <?= lang('machines_title') ?>
                </div>
            </div>
        </div>
    </div>
    <? elseif($destination == 'admin'): ?>
    <div class="span12">
        <div class="widget">
            <div class="path-container Fright">
                <div class="path-name Fright">
                    <a href="<?= site_url('admin/dashboard') ?>"><?= lang('global_system_management') ?></a>
                </div>
                <div class="path-arrow Fright"></div>
                <div class="path-name Fright">
                    <a href="<?= site_url('admin/safa_eas') ?>"><?= lang('menu_external_agent') ?></a>
                </div>
                <div class="path-arrow Fright"></div>
                <div class="path-name Fright">
                    <?= lang('machines_title') ?>
                </div>
            </div>
        </div>
    </div>
    <? endif ?>

    <div class="widget">
        <div class="widget-header">
            <div class="widget-header-icon Fright"><span class="icos-pencil2"></span></div>
            <div class="widget-header-title Fright"><?= lang('machines_title') ?></div>
        </div>
        <div class="block-fluid">
            <? if (validation_errors()): ?>
            <?= validation_errors(); ?>
            <? endif ?> 

            <div class="row-form">
                <div class="span6">
                    <div class="span4">
                        <?= lang('machines_name') ?>:
                    </div>
                    <div class="span8">
                        <?= form_input('name', set_value("name", $item->name), 'class="validate[required]"') ?>
                    </div>
                </div>
                <div class="span6">
                    <div class="span4">
                        <?= lang('machines_code') ?>:
                    </div>
                    <div class="span8">
                        <?= form_input('code', set_value("code", $item->code), 'class="validate[required]" style="direction: ltr;"' . $update_code) ?>
                    </div>
                </div>
            </div>

        </div>
        <div class="toolbar bottom TAC">
            <input type="submit" name="submit" value="<?= lang('global_submit') ?>" class="btn btn-primary"> &nbsp;
            <? if($destination == 'admin'): ?>
            <input type ="button" value="<?= lang('global_back') ?>"class="btn btn-primary" onclick="window.location = '<?= site_url('machines/index/' . $ea_id) ?>'">
            <? elseif($destination == 'ea'): ?>
            <input type ="button" value="<?= lang('global_back') ?>"class="btn btn-primary" onclick="window.location = '<?= site_url('machines/index/') ?>'">
            <? endif ?>
        </div>
    </div>


    <?= form_close() ?>       
</div> 
<script>
    $("#frm_eas").validationEngine({
        prettySelect: true,
        useSuffix: "_chosen",
        promptPosition: "topRight:-150"
//promptPosition : "bottomLeft"
    });
</script>