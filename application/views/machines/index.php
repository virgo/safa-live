<? if($destination == 'ea'): ?>
<style>
    .widget {width:98%}
    
</style>
<div class="span12">
    <div class="widget">
        <div class="path-container Fright">
            <div class="path-name Fright">
                <a href="<?= site_url('ea/dashboard') ?>"><?= lang('global_system_management') ?></a>
            </div>
            <div class="path-arrow Fright"></div>
            <div class="path-name Fright">
                <?= lang('machines_title') ?>
            </div>
        </div>
    </div>
</div>
<? elseif($destination == 'admin'): ?>
<div class="span12">
    <div class="widget">
        <a class="btn Fleft" href="<?= site_url('machines/manage/'.$this->uri->segment(3)) ?>"><?= lang('global_add_new_record') ?></a>
        <div class="path-container Fright">
            <div class="path-name Fright">
                <a href="<?= site_url('admin/dashboard') ?>"><?= lang('global_system_management') ?></a>
            </div>
            <div class="path-arrow Fright"></div>
            <div class="path-name Fright">
                <a href="<?= site_url('admin/safa_eas') ?>"><?= lang('menu_external_agent') ?></a>
            </div>
            <div class="path-arrow Fright"></div>
            <div class="path-name Fright">
                <?= lang('machines_title') ?>
            </div>
        </div>
    </div>
</div>
<? endif ?>



<div class="row-fluid">
    <div class="widget">
        <div class="widget-header">
            <div class="widget-header-icon Fright">
                <span class="icos-pencil2"></span>
            </div>
            <div class="widget-header-title Fright">
                <?= lang('machines_title') ?>
            </div>
            <a class="btn Fleft" href="<?= site_url('machines/manage') ?>"><?= lang('global_add_new_record') ?></a>
        </div>
        <div class="widget-container">
            <div class='table-responsive' >
                <table cellpadding="0" class="fpTable" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th><?= lang('machines_name') ?></th>
                            <th><?= lang('machines_code') ?></th>
                            <th><?= lang('machines_ip_address') ?></th>
                            <th><?= lang('machines_latest_activity') ?></th>
                            <th><?= lang('global_actions') ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <? if (ensure($items)): ?>
                            <? foreach ($items as $item): ?>
                                <tr>
                                    <td><?= $item->name ?></td>
                                    <td><?= $item->code ?></td>
                                    <td><?= $item->ip ?></td>
                                    <td><?= $item->last_access ?></td>
                                    <td class="TAC">
                                        <? if($destination == 'admin'): ?>
                                        <a href="<?= site_url("machines/manage/" . $item->safa_ea_id . '/'. $item->crm_ea_machine_id) ?>"><span class="icon-pencil"></span></a>
                                        <? else: ?>
                                        <a href="<?= site_url("machines/manage/" . $item->crm_ea_machine_id) ?>"><span class="icon-pencil"></span></a>
                                        <? endif ?>
                                        <a onclick=" return confirm('<?= lang('global_are_you_sure_you_want_to_delete') ?>')" href="<?= site_url("machines/delete/" . $item->crm_ea_machine_id) ?>"><span class="icon-trash"></span></a>
                                    </td>
                                </tr>
                            <? endforeach ?>
                        <? endif ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>