<?php
$name_ar = '';
$name_la = '';
$makkah_days = '';
$madinah_days = '';

$screen_title = lang('add_title');

if (isset($item)) {
    $name_ar = $item->name_ar;
    $name_la = $item->name_la;
    $makkah_days = $item->makkah_days;
    $madinah_days = $item->madinah_days;
    $screen_title = lang('edit_title');
}
?>
<script>.widget{width:98%;}</script>
<style>
    input {margin-bottom: 4px !important; }
</style>
<div class="widget">

    <div class="path-container Fright">
        <div class="path-name Fright">
            <a href="<?php echo site_url() ?>"><?php echo lang('global_system_management') ?></a>
        </div>
        <div class="path-arrow Fright"></div>
        <div class="path-name Fright"> <a href="<?php echo site_url('package_classifications') ?>"><?php echo lang('title') ?></a></div>

        <div class="path-arrow Fright"></div>
        <div class="path-name Fright"><?php echo $screen_title; ?></div>

    </div>
</div>


<?php echo form_open_multipart(false, 'id="frm_package_classifications" ') ?>


<div class="widget">
    <div class="widget-header">

        <div class="widget-header-icon Fright">
            <span class="icos-pencil2"></span>
        </div>

        <div class="widget-header-title Fright">
<?php echo $screen_title; ?>

        </div>
    </div>

    <div class="widget-container">  
<? if (validation_errors()) { ?>
    <?php echo validation_errors(); ?>        
        <? } ?>

        <div class="row-form">
            <div class="span6">
                <div class="span4 TAL Pleft10">
<?php echo lang('name_ar') ?> <font style='color:red' >*</font>
                </div>
                <div class="span8">
                    <?php echo form_input('name_ar', set_value("name_ar", $name_ar), " style='' id='name_ar' class='validate[required] input-full' ") ?>
                </div>
            </div>

            <div class="span6">
                <div class="span4 TAL Pleft10">
<?php echo lang('name_la') ?> <font style='color:red' >*</font>
                </div>
                <div class="span8">
                    <?php echo form_input('name_la', set_value("name_la", $name_la), " style='' id='name_la' class='validate[required] input-full' ") ?>
                </div>
            </div>

        </div>

        <div class="row-form">
            <div class="span6">
                <div class="span4 TAL Pleft10">
<?php echo lang('makkah_days') ?> <font style='color:red' >*</font>
                </div>
                <div class="span8">
                    <?php echo form_input('makkah_days', set_value("makkah_days", $makkah_days), " style='' id='makkah_days' class='validate[required] input-full' onkeypress='integer_only(event)' ") ?>
                </div>
            </div>

            <div class="span6">
                <div class="span4 TAL Pleft10">
<?php echo lang('madinah_days') ?> <font style='color:red' >*</font>
                </div>
                <div class="span8">
                    <?php echo form_input('madinah_days', set_value("madinah_days", $madinah_days), " style='' id='madinah_days' class='validate[required] input-full' onkeypress='integer_only(event)' ") ?>
                </div>
            </div>

        </div>
<div class=" TAC">
    <input type="submit" class="btn" name="smt_send" value="<?php echo lang('global_submit') ?>" style="margin:10px;padding: 5px;height: auto">
</div>
    </div>
</div>





<?php echo form_close() ?>


<div class="footer"></div>


<script type="text/javascript">
    $(document).ready(function() {
        var config = {
            '.chosen-select': {width: "100%"},
            '.chosen-select-deselect': {allow_single_deselect: true},
            '.chosen-select-no-single': {disable_search_threshold: 10},
            '.chosen-select-no-results': {no_results_text: 'Oops, nothing found!'},
            '.chosen-select-width': {width: "95%"}
        }
        for (var selector in config) {
            $(selector).chosen(config[selector]);
        }

        // binds form submission and fields to the validation engine
        $("#frm_package_classifications").validationEngine({
            prettySelect: true,
            useSuffix: "_chosen",
            promptPosition: "topRight:-150"
                    //promptPosition : "bottomLeft"
        });
    });

</script>

<script>
//By Gouda
    function integer_only(evt)
    {
        var theEvent = evt || window.event;
        var key = theEvent.keyCode || theEvent.which;
        key = String.fromCharCode(key);
        var regex = /[0-9]|\./;
        if (!regex.test(key)) {
            theEvent.returnValue = false;
            if (theEvent.preventDefault)
                theEvent.preventDefault();
        }
    }
</script>