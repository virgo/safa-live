<script>
    jQuery(function() {
        menu1.toggle();
    })
</script>  

<div class="widget">
    <div class="path-container Fright">
        <div class="path-name Fright">
            <a href="<?php
            if (session('uo_id')) {
                echo site_url() . 'uo/dashboard';
            } else if (session('ea_id')) {
                echo site_url() . 'ea/dashboard';
            } else {
                echo site_url();
            }
            ?>"><?php echo lang('global_system_management') ?></a>
        </div>
        <div class="path-arrow Fright"></div>
        <div class="path-name Fright">  <?= lang('trips_report') ?></div>
    </div>
</div>

<div class="row-fluid">
    <div class="row-fluid">       
        <div class="widget">
            <div class="widget-header">
                <div class="widget-header-icon Fright"> <span class="icos-pencil2"></span> </div>
                <div class="widget-header-title Fright">
                    <?php echo lang('trips_report') ?>
                </div>
            </div>
            <div class="block-fluid">
                <form action="<?php echo base_url() . "uo/trips_report/index"; ?>" method="GET">
                    <table class="fsTable" cellpadding="0" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th rowspan="2"><?= lang('trip') ?></th>
                                <th rowspan="2"><?= lang('safa_uo_contract_name') ?></th>
                                <th rowspan="2"><?= lang('packages') ?></th>
                                <th rowspan="2"><?= lang('internal_transportation') ?></th>
                                <th colspan="3"><?= lang('external_transportation') ?></th>
                                <th rowspan="2"><?= lang('mofa') ?></th>
                                <th rowspan="2"><?= lang('visa_order_issued') ?></th>
                                <th rowspan="2"><?= lang('visa_number') ?></th>
                                <th rowspan="2"><?= lang('global_actions')?></th>
                            </tr>
                            <tr>
                                <th><?= lang('flights') ?></th>
                                <th><?= lang('buses') ?></th>
                                <th style="border-left: 1px solid #DDDDDD;"><?= lang('ships') ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?
                            foreach ($items as $item) {
                                $safa_trip_id = $item->safa_trip_id;
                                $safa_trip_name = $item->name;
                                $flights_icon = '';
                                $buses_icon = '';
                                $ships_icon = '';
                                $erp_transportertype_id = $item->erp_transportertype_id;
                                if ($erp_transportertype_id == 1) {
                                    $buses_icon = "<span class='icon-ok'>";
                                } else if ($erp_transportertype_id == 2) {
                                    $flights_icon = "<span class='icon-ok'>";
                                } else if ($erp_transportertype_id == 3) {
                                    $ships_icon = "<span class='icon-ok'>";
                                }

                   	$safa_trip_name=$item->name;
                    $safa_uo_contract_name = $item->safa_uo_contract_name;
                   	
                    
                    $flights_icon='';
                    $buses_icon='';
                    $ships_icon='';
                    $erp_transportertype_id = $item->erp_transportertype_id;
                    if($erp_transportertype_id==1) {
                    	$buses_icon = "<span class='icon-ok'>";
                    } else if($erp_transportertype_id==2) {
                    	$flights_icon = "<span class='icon-ok'>";
                    } else if($erp_transportertype_id==3) {
                    	$ships_icon = "<span class='icon-ok'>";
                    }
                    
                    $individuals_count=$item->individuals_count;
                    $mofa_count=$item->mofa_count;
                    $visa_order_count=$item->visa_order_count;
                    $visa_number_count=$item->visa_number_count;
                    
                    $trip_safa_packages='';
                    $this->safa_trips_model->safa_trip_id = $safa_trip_id;
                    $trip_safa_packages_rows = $this->safa_trips_model->get_safa_packages();
                     foreach($trip_safa_packages_rows as $trip_safa_packages_row) {
                     	$trip_safa_packages = $trip_safa_packages."<a href='".site_url("safa_packages/view/$trip_safa_packages_row->safa_package_id")."' target='_blank' >".$trip_safa_packages_row->safa_package_name.'</a> - ('.$trip_safa_packages_row->erp_package_period_name.')<br/>';
                     }
                    
                    $trip_internaltrip_id=$item->trip_internaltrip_id;
                    
                    
                    $mofa_ico='';
                    $visa_order_ico='';
                    $visa_number_ico='';
                    
                    $mofa_text='';
                    $visa_order_text='';
                    $visa_number_text='';
                    
                    if($individuals_count>0) {
                    	$mofa_text="($individuals_count/$mofa_count)";
	                    if($individuals_count==$mofa_count) {
	                    	$mofa_ico = "<span class='icon-ok'>";
	                    } else {
	                    	$mofa_ico = "<span class='icon-remove'>";
	                    }
                    }
                    
                    if($individuals_count>0) {
                    	$visa_order_text="($individuals_count/$visa_order_count)";
	                	if($individuals_count==$visa_order_count) {
	                    	$visa_order_ico = "<span class='icon-ok'>";
	                    } else {
	                    	$visa_order_ico = "<span class='icon-remove'>";
	                    }
                    }
                    
                    if($individuals_count>0) {
                    	$visa_number_text="($individuals_count/$visa_number_count)";
	                	if($individuals_count==$visa_number_count) {
	                    	$visa_number_ico = "<span class='icon-ok'>";
	                    } else {
	                    	$visa_number_ico = "<span class='icon-remove'>";
	                    }
                    }
                    ?>
                    <tr>
                        <td class=" warning"><?php echo $safa_trip_name; ?></td>
                        <td class=" success"><?php echo $safa_uo_contract_name; ?></td>
			
                        <td class="TAC info">
                        <?php echo $trip_safa_packages; ?>
                        </td>
                        
                        <!-- <td></td> -->
                        <td class="TAC">
<!--                        <?php echo "<a href='".site_url("uo/trip_internaltrip/edit/$trip_internaltrip_id")."' target='_blank' >".$trip_internaltrip_id.'</a>'; ?>-->
                        	<?php echo $trip_internaltrip_id; ?>
                        </td>
                        
                        <td class="TAC success"><?php echo $flights_icon; ?></td>
                        <td class="TAC info"><?php echo $buses_icon; ?></td>
                        <td class="TAC error"><?php echo $ships_icon; ?></td>
                        
                        <td class="TAC success"><?php echo "$mofa_text"; ?>  </td>
                        <td class="TAC info"><?php echo "$visa_order_text"; ?></td>
                        <td class="TAC warning"><?php echo "$visa_number_text"; ?></td>
                        <td>
                        <a href='<?php echo site_url("trip_details/details/$safa_trip_id"); ?>' target="_blank" ><span class='icon-search'></span></a>
                        <a href='<?php echo site_url("uo/trips/passports/$safa_trip_id"); ?>' target="_blank" ><span class='icon-user'></span></a>
                        </td>
                    </tr>
                    <? } ?>
                </tbody>
            </table> 
            </form>      
        </div>
    </div>
    
<!--    <?php echo $pagination; ?>-->
    
</div>
<script>
    $(document).ready(function() {
        $('.fancybox').fancybox({
            afterClose: function() {
                //By Gouda.
                //location.reload();
            }
        });
    });
</script>


