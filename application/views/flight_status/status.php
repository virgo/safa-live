<div class="widget" >  
    <div class="head dark" >
        <div class="icon"><i class="icos-pencil2"></i></div>
        <h2><?= lang('operationalTimes') ?></h2>
    </div>            
    <? foreach ($flight_status as $state): ?>
        <div class="block-fluid" >
            <div class="row-form">
                <table width="100%">
                    <tr>
                        <th><?= lang('departureAirportFsCode') ?></th>
                        <th><?= lang('arrivalAirportFsCode') ?></th>
                        <th><?= lang('departureDate') ?></th>
                        <th><?= lang('arrivalDate') ?></th>
                        <th><?= lang('status') ?></th>
                        <th><?= lang('flight_type') ?></th>
                    </tr>

                    <tr>
                        <td><?= $state->departureAirportFsCode ?></td>
                        <td><?= $state->arrivalAirportFsCode ?></td>
                        <td><?= $state->departureDate ?></td>
                        <td><?= $state->arrivalDate ?></td>
                        <td><?= $status[$state->status] ?></td>
                        <td><?= $flightTypes[$state->flightType] ?></td>
                    </tr>

                </table> 
            </div>
            <br>
            <div class="row-form">
                <table width="100%">
                    <? if (isset($state->publishedDeparture)): ?>
                        <tr>
                            <th>
                                <?= lang('publishedDeparture') ?>
                            </th>
                            <td>
                                <?= $state->publishedDeparture ?>
                            </td>
                        </tr>
                    <? endif ?>
                    <? if (isset($state->scheduledGateDeparture)): ?>
                        <tr>
                            <th>
                                <?= lang('scheduledGateDeparture') ?>
                            </th>
                            <td>
                                <?= $state->scheduledGateDeparture ?>
                            </td>
                        </tr>
                    <? endif ?>
                    <? if (isset($state->estimatedGateDeparture)): ?>
                        <tr>
                            <th>
                                <?= lang('estimatedGateDeparture') ?>
                            </th>
                            <td>
                                <?= $state->estimatedGateDeparture ?>
                            </td>
                        </tr>
                    <? endif ?>
                    <? if (isset($state->actualGateDeparture)): ?>
                        <tr>
                            <th>
                                <?= lang('actualGateDeparture') ?>
                            </th>
                            <td>
                                <?= $state->actualGateDeparture ?>
                            </td>
                        </tr>
                    <? endif ?>
                    <? if (isset($state->scheduledGateArrival)): ?>
                        <tr>
                            <th>
                                <?= lang('scheduledGateArrival') ?>
                            </th>
                            <td>
                                <?= $state->scheduledGateArrival ?>
                            </td>
                        </tr>
                    <? endif ?>
                    <? if (isset($state->estimatedGateArrival)): ?>
                        <tr>
                            <th>
                                <?= lang('estimatedGateArrival') ?>
                            </th>
                            <td>
                                <?= $state->estimatedGateArrival ?>
                            </td>
                        </tr>
                    <? endif ?>
                    <? if (isset($state->actualRunwayArrival)): ?>
                        <tr>
                            <th>
                                <?= lang('actualRunwayArrival') ?>
                            </th>
                            <td>
                                <?= $state->actualRunwayArrival ?>
                            </td>
                        </tr>
                    <? endif ?>
                    <? if (isset($state->flightDurations)): ?>
                        <tr>
                            <th>
                                <?= lang('flightDurations') ?>
                            </th>
                            <td>
                                <?= $state->flightDurations ?> <?= lang('Minutes') ?>
                            </td>
                        </tr>
                    <? endif ?>



                </table>
            <? endforeach ?> 
        </div> 

    </div>
</div>


</div>
</div>
<?= $this->load->view('footer') ?>