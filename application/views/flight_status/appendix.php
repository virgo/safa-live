<div class="widget" >
    <div class="head dark">
        <div class="icon"><i class="icos-pencil2"></i></div>
        <h2><?= lang('airlines') ?></h2>
    </div>  
    <div class="block-fluid" >
        <div class="row-form">
            <table width="100%">
                <tr>
                    <td>
                        <? foreach ($appendix->fs_airlines as $airline): ?>
                            <?= $airline->airline ?><br />
                        <? endforeach ?>
                    </td>
                </tr>
            </table> 
        </div> 

    </div>
</div>
<div class="widget" >
    <div class="head dark">
        <div class="icon"><i class="icos-pencil2"></i></div>
        <h2><?= lang('airports') ?></h2>
    </div>  
    <div class="block-fluid" >
        <div class="row-form">
            <table width="100%">
                <tr>
                    <th>
                        <?= lang('iata') ?>
                    </th>
                    <th>
                        icao
                    </th>
                    <th>
                        <?= lang('name') ?>
                    </th>
                    <th>
                        <?= lang('city') ?>
                    </th>
                    <th>
                        <?= lang('city_code') ?>
                    </th>
                    <th>
                        <?= lang('country_code') ?>
                    </th>
                    <th>
                        <?= lang('country_name') ?>
                    </th>
                    <th>
                        <?= lang('region_name') ?>
                    </th>
                    <th>
                        timeZoneRegionName
                    </th>
                    <th>
                        <?= lang('latitude') ?>
                    </th>
                    <th>
                        <?= lang('longitude') ?>
                    </th>
                    <th>
                        <?= lang('elevationFeet') ?>
                    </th>
                    <th>
                        <?= lang('classification') ?>
                    </th>
                </tr>
                <? foreach ($appendix->fs_airports as $airport): ?>
                    <tr>
                        <td><?= $airport->iata ?></td>
                        <td><?= $airport->icao ?></td>
                        <td><?= $airport->name ?></td>
                        <td><?= $airport->city ?></td>
                        <td><?= $airport->cityCode ?></td>
                        <td><?= $airport->countryCode ?></td>
                        <td><?= $airport->countryName ?></td>
                        <td><?= $airport->regionName ?></td>
                        <td><?= $airport->timeZoneRegionName ?></td>
                        <td><?= $airport->latitude ?></td>
                        <td><?= $airport->longitude ?></td>
                        <td><?= $airport->elevationFeet ?></td>
                        <td><?= $airport->classification ?></td>
                    </tr>
                <? endforeach ?>
            </table>
        </div> 

    </div>
</div>
<div class="widget" >
    <div class="head dark">
        <div class="icon"><i class="icos-pencil2"></i></div>
        <h2><?= lang('Equipments') ?></h2>
    </div>  
    <div class="block-fluid" >
        <div class="row-form">
            <table width="100%">
                <tr>
                    <th>
                        <?= lang('iata') ?>
                    </th>
                    <th>
                        <?= lang('name') ?>
                    </th>
                    <th>
                        turboProp
                    </th>
                    <th>
                        <?= lang('jet') ?>
                    </th>
                    <th>
                        widebody
                    </th>
                    <th>
                        regional
                    </th>
                </tr>
                <? foreach ($appendix->fs_equipments as $equipment): ?>
                    <tr>
                        <td><?= $equipment->iata ?></td>
                        <td><?= $equipment->name ?></td>
                        <td><?= $equipment->turboProp ?></td>
                        <td><?= $equipment->jet ?></td>
                        <td><?= $equipment->widebody ?></td>
                        <td><?= $equipment->regional ?></td>
                    </tr>
                <? endforeach ?>

            </table>  
        </div> 

    </div>
</div>




