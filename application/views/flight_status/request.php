<?= $this->load->view('ksa_header') ?>
<div class="row-fluid">
    <div class="span11"  >
        <div class="widget">
            <div class="head dark">
              <div class="icon"><i class="icos-pencil2"></i></div>
                <h2><?= lang('global_trip_details') ?></h2>  
            </div>
            <div class="block-fluid" >
                    <div class="row-form" >
                        <table width="100%">
                            <tr>
                                <th>
                                    <?= lang('airline') ?> 
                                </th>
                                <th>
                                    <?= lang('flight') ?>
                                </th>
                                <th>
                                    <?= lang('date') ?>
                                </th>
                            </tr>
                            <tr>
                                <td>
                                    <?= $request->airline ?>
                                </td>
                                <td>
                                    <?= $request->flight ?>
                                </td>
                                <td>
                                    <?= $request->date ?>
                                </td>
                            </tr>
                        </table>   
                    </div> 
            </div>
        </div>   


