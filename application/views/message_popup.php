<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); ?>
<link
	href="<?= CSS ?>/stylesheets.css" rel="stylesheet" type="text/css" />
<? if (lang('global_lang') == 'ar'): ?>
<link
	href="<?= CSS ?>/stylesheet-ar.css" rel="stylesheet" type="text/css" />
<? else: ?><? endif ?>
<script
	type="text/javascript"
	src='<?= JS ?>/plugins/jquery/jquery-1.9.1.min.js'></script>
<script
	type="text/javascript"
	src='<?= JS ?>/plugins/jquery/jquery-ui-1.10.1.custom.min.js'></script>
<script
	type="text/javascript"
	src='<?= JS ?>/plugins/bootstrap/bootstrap.min.js'></script>
<link
	href="<?= CSS_JS ?>/dateandtime/bootstrap-datetimepicker.min.css"
	rel="stylesheet" type="text/css" />
<script
	type="text/javascript"
	src='<?= CSS_JS ?>/dateandtime/bootstrap-datetimepicker.min.js'></script>
<style>
body {
	height: auto !important;
	overflow: auto !important;
	background: none transparent !important;
	overflow: hidden;
	min-height: 1%;
}

.row-fluid {
	padding-top: 5px !important;
	padding-bottom: 5px !important;
}
</style>

<style>
.msg {
	padding: 8px 35px 8px 14px;
	margin-bottom: 20px;
	color: #c09853;
	text-shadow: 0 1px 0 rgba(255, 255, 255, 0.5);
	background-color: #fcf8e3;
	border: 1px solid #fbeed5;
	-webkit-border-radius: 4px;
	-moz-border-radius: 4px;
	border-radius: 4px;
	color: #468847;
	background-color: #dff0d8;
	border-color: #d6e9c6;
	border: 3px solid rgb(255, 255, 255);
	text-align: center;
}
</style>
<?php echo  form_open_multipart(false, 'id="frm_contract_approving_phases" ') ?>

<script>
//var myVar = setInterval(function(){close_popup()},5000);
function close_popup() 
{
	window.parent.parent.location.reload();
}
</script>

<div class="">


<div class="modal-body">

<div class="msg">
<p>
<p><?= $msg ?></p>
<input type="button" value="<?= lang('global_submit') ?>"
	class="btn btn-primary"
	onclick="window.parent.$.fancybox.close()"> 
</p>
</div>


</div>
</div>

<?php echo  form_close() ?>


<div class="footer"></div>

