<!-- multi selection choosen -->
<link rel="stylesheet" href="<?= NEW_CSS_JS ?>/wizard/formToWizard.css"/>
<script language="javascript" type="text/javascript" src="<?= NEW_CSS_JS ?>/wizard/formToWizard.js"></script>
<script type='text/javascript' src="<?= NEW_CSS_JS ?>/fancybox/jquery.fancybox.pack.js"></script>
<link rel="stylesheet" href="<?= NEW_CSS_JS ?>/fancybox/jquery.fancybox.css" type="text/css" media="screen" />
<link rel="stylesheet" href="<?= NEW_CSS_JS ?>/multi-select-chosen/chosen.css">
<script src="<?= NEW_CSS_JS ?>/multi-select-chosen/chosen.jquery.js" type="text/javascript"></script>
<script src="https://maps.googleapis.com/maps/api/js?AIzaSyC794uPScLtRUa8Uwk9nX8RCjrepoxszxg&sensor=true"></script>

<!-- By Gouda, To use CKEditor -->        
<script type='text/javascript' src='<?= NEW_TEMPLATE ?>/plugins/ckeditor/ckeditor.js'></script>
<!-- 
<script type='text/javascript' src='<?= NEW_TEMPLATE ?>/plugins/youtube-ckeditor/plugin.js'></script>
 -->
<script>.widget{width:98%;}</script>

<div class="widget">
    <div class="path-container Fright">
        <div class="path-name Fright">
            <a href=""><?= lang('global_system_management') ?></a>
        </div>
        <div class="path-arrow Fright">
        </div>
        
        <div class="path-name Fright"> <a href="<?php echo  site_url('hotels') ?>"><?php echo  lang('menu_main_hotels') ?></a></div>
        
        <div class="path-arrow Fright">
        </div>
        <div class="path-name Fright">
            <?= lang('hotels_add_title') ?>
        </div>
    </div>
</div>
<div class="widget">
    <div class="widget-header">
        <div class="widget-header-icon Fright">
            <span class="icos-pencil2"></span>
        </div>
        <div class="widget-header-title Fright">
            <?= lang('hotels_add_title') ?>
        </div>
    </div>
    <div class="widget-container slidingDiv">
        <div>
            <?= form_open_multipart("", "id='wizard_validate'") ?>
            <fieldset>
                <legend><?= lang('step1_title') ?></legend>
                <div class="inner-widget">
                    <div class="widget-header">
                        <div class="widget-header-icon Fright">
                            <span class="icos-pencil2"></span>
                        </div>
                        <div class="widget-header-title Fright">
                            <?= lang('form1_title') ?>
                        </div>
                    </div>
                    <div class="widget-container" >
                        <div class="row-form">
                            <div class="span6">
                                <div class="span4 TAL Pleft10" >
                                    <?= lang('hotels_erp_country_id') ?>
                                    <font style="color:red;font-size:20px;position:relative;top:-9px">*</font>
                                </div>
                                <div class="span8">
                                    <?= form_dropdown('erp_country_id', ddgen('erp_countries',array('erp_country_id', name())), set_value('erp_country_id', $country->erp_country_id), "class='validate[required]' style='width:100%;' id='erp_country_id' ") ?>
                                    <?= form_error('erp_country_id', '<div class="bottom" style="color:red" >', '</div>'); ?>
                                </div>
                            </div>
                            <div class="span6">
                                <div class="span4 TAL Pleft10">
                                    <?= lang('hotels_erp_city_id') ?>
                                    <font style="color:red;font-size:20px;position:relative;top:-9px">*</font>
                                </div>
                                <div class="span8">
                                    <?= form_dropdown("erp_city_id",array("0" => lang('global_select_from_menu')),set_value("erp_city_id"), "id='erp_city_id'    style='width:100%;'  class='validate[required]' ") ?>
                                    <?= form_error('erp_city_id', '<div class="bottom" style="color:red" >', '</div>'); ?>
                                </div>
                            </div>
                        </div>
                        <div class="row-form">
                          <div class="span6">
                                <div class="span4 TAL Pleft10" >
                                    <?= lang('geoarea') ?>
                                </div>
                                <div class="span8">
                                    <?= form_dropdown('geoarea_id',array('0'=>lang('global_select_from_menu')),set_value('geoaerea_id'), "class='' style='width:100%;' id='geoid'") ?>
                                </div>
                            </div>
                            <div class="span6">
                                <div class="span4 TAL Pleft10">
                                    <?= lang('street') ?>
                                </div>
                                <div class="span8">
                                    <?= form_dropdown("street_id",array("0" =>lang('global_select_from_menu')),set_value("street_id"), "id='street_id'    style='width:100%;' ") ?>
                                </div>
                            </div>
                        </div>
                        <div class="row-form">
                            <div class="span6">
                                <div class="span4 TAL Pleft10"> <?= lang('hotels_name_ar') ?><?if(name()=='name_ar'):?><font style="color:red;font-size:20px;position:relative;top:-9px">*</font><?endif;?></div>
                                <div class="span8">
                                    <input type="text" name="name_ar" value="<?=set_value('name_ar')?>" class="input-full <?if(name()=='name_ar'):?>validate[required]<?endif;?>" >
                                    <?= form_error('name_ar', '<div class="bottom" style="color:red" >', '</div>'); ?>
                                </div>
                            </div>
                            <div class="span6">
                                <div class="span4 TAL Pleft10"> <?= lang('hotels_name_la') ?><?if(name()=='name_la'):?><font style="color:red;font-size:20px;position:relative;top:-9px">*</font><?endif;?></div>
                                <div class="span8">
                                    <input type="text" name="name_la" value="<?=set_value('name_la')?>" class="input-full <?if(name()=='name_la'):?>validate[required]<?endif;?>" >
                                    <?= form_error('name_la', '<div class="bottom" style="color:red" >', '</div>'); ?>
                                </div>
                            </div>
                        </div>    
                        <div class="row-form">
                            <div class="span6">
                                <div class="span4 TAL Pleft10"><?= lang('hotels_address_ar') ?>
                                    <?if(name()=='name_ar'):?><font style="color:red;font-size:20px;position:relative;top:-9px">*</font><?endif;?>
                                </div>
                                <div class="span8">
                                    <textarea name="address_ar" class="input-full <?if(name()=='name_ar'):?>validate[required]<?endif;?> " ><?= set_value('address_ar') ?></textarea>
                                    <?= form_error('address_ar', '<div class="bottom" style="color:red" >', '</div>'); ?>
                                </div>
                            </div>
                            <div class="span6">
                                <?if(name()=='name_la'):?><font style="color:red;font-size:20px;position:relative;top:-9px">*</font><?endif;?>
                                <div class="span4 TAL Pleft10"> <?= lang('hotels_address_la') ?>:</div>
                                <div class="span8">
                                    <textarea name="address_la" class="input-full <?if(name()=='name_la'):?>validate[required]<?endif;?>" ><?= set_value('address_la') ?></textarea>
                                    <?= form_error('address_la', '<div class="bottom" style="color:red" >', '</div>'); ?>
                                </div>
                            </div>
                        </div>
                        <div class="row-form"  ><!---->
                            <div class="span6" >
                                <div class="span4 TAL Pleft10">
                                    <?= lang('floors_count') ?>
                                </div>
                                <div class="span2" >
                                    <?= form_input('floors_count', set_value("floors_count"), 'class="input-full  validate[custom[integer]] " data-prompt-position="topLeft:-90,0" ') ?>
                                </div>
                                <div class="span4 TAL Pleft10">
                                    <?= lang('ROOM_COUNT') ?>
                                </div>
                                <div class="span2" >
                                    <?= form_input('room_count', set_value("room_count"), 'class="input-full  validate[custom[integer]] "  data-prompt-position="topLeft:-90,0"  ') ?>
                                </div>
                                <div  style="clear:both"></div>
                                <div class="span4 TAL Pleft10">
                                    <?= lang('rooms_count_for_disabled') ?>
                                </div>
                                <div class="span2" >
                                    <?= form_input('rooms_count_for_disabled', set_value("rooms_count_for_disabled"), 'class="input-full  validate[custom[integer]] "  data-prompt-position="topLeft:-90,0"  ') ?>
                                </div>
                                <div class="span4 TAL Pleft10"  >
                                    <?= lang('BED_COUNT') ?>
                                </div>
                                <div class="span2" >
                                    <?= form_input('bed_count', set_value("bed_count"), 'class="input-full validate[custom[integer]]"  data-prompt-position="topLeft:-90,0"   ') ?>
                                </div>
                            </div>
                            <div class="span6" >
                                <div class="span4 TAL Pleft10">
                                    <?= lang('flats_count') ?>
                                </div>
                                <div class="span2" >
                                    <?= form_input('flats_count', set_value("flats_count"), 'class="input-full  validate[custom[integer]] "  data-prompt-position="topLeft:-90,0"  ') ?>
                                </div>
                                <div class="span4 TAL Pleft10">
                                    <?= lang('suites_count') ?>
                                </div>
                                <div class="span2" >
                                    <?= form_input('suites_count', set_value("suites_count"), 'class="input-full  validate[custom[integer]] "  data-prompt-position="topLeft:-90,0"  ') ?>
                                </div>
                               <div class="span4 TAL Pleft10">
                                    <?= lang('lifts_count') ?>
                                </div>
                                <div class="span2" >
                                    <?= form_input('lifts_count', set_value("lifts_count"), 'class="input-full  validate[custom[integer]] "  data-prompt-position="topLeft:-90,0"  ') ?>
                                </div>
                               <div class="span4 TAL Pleft10">
                                    <?= lang('hotel_capacity') ?>
                                </div>
                                <div class="span2" >
                                    <?= form_input('hotel_capacity', set_value("hotel_capacity"), 'class="input-full  validate[custom[integer]] "  data-prompt-position="topLeft:-90,0"  ') ?>
                                </div>
                            </div>
                        </div>
                        <div class='row-form'><!---->
                            <div class="span6">
                                <div class="span4 TAL Pleft10" >
                                    <?= lang('hotel_creation_date') ?>
                                </div>
                                <div class="span8">
                                     <?= form_input('hotel_creation_date', set_value("hotel_creation_date"), 'class="input-full date "   ')?>
                                </div>
                            </div>
                            <div class="span6">
                                <div class="span4 TAL Pleft10">
                                    <?= lang('website') ?>
                                </div>
                                <div class="span8">
                                      <?= form_input('website', set_value("website"), 'class="input-full  validate[custom[url]] "  data-prompt-position="topLeft:-90,0"  ')?>
                                </div> 
                        </div>
                    </div>     
                        <div class="row-form" >
                            <div class="span12">
                                <div class="span2 TAL Pleft10" >
                                    <?= lang('remarks')?>
                                </div>
                                <div class="span10" >
                                    <textarea class="ckeditor" name="remark" ><?=set_value('remark')?></textarea>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </fieldset>
            <fieldset>
                <legend><?= lang('step2_title') ?></legend>
                <div class="inner-widget">
                    <div class="widget-header">
                        <div class="widget-header-icon Fright">
                            <span class="icos-pencil2"></span>
                        </div>
                        <div class="widget-header-title Fright">
                            <?= lang('form2_title') ?>
                        </div>
                    </div>
                    <div class="widget-container">
                        <div class="row-form">
                            <div class="span6" >
                                <div class="span4 TAL Pleft10">
                                    <?= lang('phone') ?> 
                                </div> 
                                <div class="span8">
                                    <?= form_input('phone', set_value("phone"), "class='input-full validate[custom[phone]]'") ?>
                                </div>
                            </div>
                            <div class="span6" >
                                <div class="span4 TAL Pleft10" >
                                    <?= lang('fax') ?> 
                                </div>
                                <div class="span8" >
                                    <?= form_input('fax', set_value("fax"), "class='input-full validate[custom[phone]]'") ?>
                                </div>
                            </div>
                        </div>
                        <div class="row-form">
                            <div class="span6">
                                <div class="span4 TAL Pleft10"><?= lang('email') ?> </div>
                                <div class="span8">
                                    <?= form_input('email', set_value("email"), "class='input-full validate[custom[email]]'  ") ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="inner-widget" id="official_work_info" >
                    <div class="widget-header">
                        <div class="widget-header-icon Fright">
                            <span class="icos-pencil2"></span>
                        </div>
                        <div class="widget-header-title Fright">
                            <?= lang('contact_official_info') ?>
                        </div>
                        <a class="btn Fleft" id="add_official" href="javascript:void(0)"><?= lang('global_add') ?></a>
                    </div>
                    <div class="widget-container" >
                        <div class="table-container">
                            <table cellpadding="0" cellspacing="0">
                                <thead>
                                    <tr>
                                        <th><?= lang('official_work') ?></th>
                                        <th><?= lang('official_name') ?></th>
                                        <th><?= lang('official_email') ?></th>
                                        <th><?= lang('official_phonetype') ?></th>
                                        <th><?= lang('official_phone') ?></th>
                                        <th><?= lang('official_skype') ?></th>
                                        <th><?= lang('official_massenger') ?></th>
                                        <th><?= lang('global_operations') ?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td><?=form_input('official_work[]',set_value('official_work'),' class="input-huge" ')?></td>
                                        <td><?=form_input('official_name[]',set_value('official_name'),' class="input-huge" ')?></td>
                                        <td><?=form_input('official_email[]',set_value('official_email'),' class="input-huge  validate[custom[email]] " data-prompt-position="topLeft:-40,10" ')?></td>
                                        <td>
                                           <?= form_dropdown('official_phonetype[]',ddgen('erp_phone_type'),set_value('official_phonetype'),' class="input-huge" ')?>
                                        </td>
                                        <td><?=form_input('official_phone[]',set_value('official_phone'),' class="input-huge validate[custom[phone]]"  data-prompt-position="topLeft:-40,10" ')?></td>
                                        <td><?=form_input('official_skype[]',set_value('official_skype'),' class="input-huge" ')?></td>
                                        <td><?=form_input('official_messenger[]',set_value('official_messenger'),' class="input-huge" ')?></td>
                                        <td class="TAC"><a href="javascript:void(0)" class="remove_officialwork" ><span class="icon-trash"></span></a></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </fieldset>
            <fieldset> 
                <legend><?= lang('step3_title') ?></legend>
                <div class="inner-widget">
                    <div class="widget-header">
                        <div class="widget-header-icon Fright">
                            <span class="icos-pencil2"></span>
                        </div>
                        <div class="widget-header-title Fright">
                            <?= lang('form3_title') ?>
                        </div>
                    </div>
                    <div class="widget-container">
                        <div class="row-form">
                            <div class="span3">
                                <div class="span6 TAL Pleft10">
                                     <?= lang('hotels_th_hotellevel') ?>
                                    <font style="color:red;font-size:20px;position:relative;top:-9px">*</font>
                                </div>
                                <div class="span6">
                                    <?= form_dropdown('hotellevel', ddgen('erp_hotellevels'), set_value("hotellevel"), "class='input-full  validate[required]' data-prompt-position='topLeft:-70,0'  ") ?>
                                </div>
                            </div>
                            <div class="span3">
                                <div class="span6 TAL Pleft10">
                                   <?= lang('hotel_sublevel') ?>
                                   <font style="color:red;font-size:20px;position:relative;top:-9px">*</font> 
                                </div>
                                <div class="span6">
                                    <?= form_dropdown('hotel_sub_level', ddgen('erp_hotel_sub_levels'), set_value("hotel_sub_level"), "class='input-full validate[required]' data-prompt-position='topLeft:-70,0'   ") ?>
                                </div>
                            </div>
                            <div class="span6">
                                <div class="span4 TAL Pleft10"><?= lang('advantage') ?></div>
                                <div class="span8">
                                    <?= form_dropdown('hotels_advantages[]',ddgen('erp_hotel_advantages'),set_value('hotels_advantages[]'),"class='chosen-rtl ' multiple='true'  data-placeholder='".lang('global_select_from_menu')."'   style='width:100%' id='advantages' "  ) ?>
                                </div>
                                <div class="clear:both"></div>
                                <div class="span4 TAL Pleft10"><?=lang('hotel_languages')?></div>
                                <div class="span8">
                                    <?= form_dropdown('hotels_languages[]',ddgen('erp_hotel_speakinglanguage'),set_value('hotels_languages[]'),"class='chosen-rtl ' multiple='true'  data-placeholder='".lang('global_select_from_menu')."'   style='width:100%' id='languages' "  ) ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="inner-widget" id="hotel_policy" >
                    <div class="widget-header">
                        <div class="widget-header-icon Fright">
                            <span class="icos-pencil2"></span>
                        </div>
                        <div class="widget-header-title Fright">
                            <?= lang('hotel_policy') ?>
                        </div>
                        <a class="btn Fleft" id="add_hotelpolicy" href="javascript:void(0)"><?= lang('global_add') ?></a>
                    </div>
                    <div class="widget-container">
                        <div class="row-form">
                            <table cellpadding="0" cellspacing="0" class="table-container">
                                <thead>
                                    <tr>
                                        <th width="10%"><?= lang('Policy') ?></th>
                                        <th width="80%"><?= lang('policy_description') ?></th>
                                        <th width="10%"><?= lang('global_operations') ?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>
                                            <?= form_dropdown('hotels_policy[]', ddgen('erp_hotels_policies'), set_value('hotels_policy'), "class='input-full'") ?>
                                        </td>
                                        <td>
                                            <div class="span12">
                                                <textarea name="policy_description[]"  class="input-full"  ></textarea>
                                            </div>
                                        </td>
                                        <td class="TAC"><a class="remove_hotelpolicy" href="javascript:void(0)"><span class="icon-trash"></span></a></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div> 
                    </div>
                </div>      
            </fieldset>
            <fieldset id="step_image" >
                <legend><?= lang('step4_title') ?></legend>
                <div class="inner-widget">
                    <div class="widget-header">
                        <div class="widget-header-icon Fright">
                            <span class="icos-pencil2"></span>
                        </div>
                        <div class="widget-header-title Fright">
                            <?= lang('form4_title') ?>
                        </div>
                        <div class="widget-container">
                            <div class="row-form">
                                <div class="span12">
                                    <div class="span2 TAL Pleft10"><?= lang('add_pictures') ?></div>
                                    <div class="span10" id='file_upload_images'>
                                        <input id="file_images" name="images_upload"  type ="file"  multiple style='display:none' />
                                           <a href='JavaScript:void(0)' id='press_images' class='btn'>Choose Images</a>
                                    </div>
                                </div>
                            </div>
                            <div class="row-form">
                                <div  id="img_galary" class="span12" >
                                   
                                </div> 
                            </div>
                        </div>
                    </div> 
                </div>   
            </fieldset>
            <fieldset id="step_map">
                <legend><?= lang('step5_title') ?></legend>
                <div class="inner-widget" >
                        <div class="widget-header">
                            <div class="widget-header-icon Fright">
                                <span class="icos-pencil2"></span>
                            </div>
                            <div class="widget-header-title Fright">
                               <?=lang('step5_title')?>
                            </div>

                        </div>
                        <div class="widget-container">
                            <div class="row-form">
                                <div class="span6">
                                    <div class="span4 TAL Pleft10"><?=lang('longtitude')?>:</div>
                                    <div class="span8"><?=  form_input('langtitude',set_value('langtitude'),' id="langtitude" class="input-huge"')?></div>
                                </div>
                                <div class="span6">
                                    <div class="span4 TAL Pleft10"><?= lang('latitude')?>:</div>
                                     <div class="span8"><?=  form_input('latitude',set_value('latitude'),' id="latitude" class="input-huge"')?></div>
                                </div>
                            </div>
                            <div class="row-form">
                                <div class="span4">
                                    <div class="span12">
                                        <div class="span4"><?=lang('map_distance')?></div>
                                         <div class="span8">
                                            <span class="map_distance"></span>
                                             <?= form_input('map_distance',set_value('map_distance'),'class="input-huge"')?>
                                         </div>
                                    </div>
                                </div>
                                <div class="span8">
                                    <div id="map" style="width:100%;height:500px;"></div>
                                </div>
                            </div>
                            <div style="clear:both"></div>
                            <div align="center"  style="padding:10px">
                                <input  type="submit"    class="btn"  value="<?=lang('global_add')?>" style="height:50px; width:60px !important"  />
                            </div>   
                        </div>
                </div>
            </fieldset>
            <?= form_close() ?>
        </div>
    </div>
</div>
<script type="text/javascript">
$(document).ready(function(){
   $('#advantages').chosen();
   $('#languages').chosen();
   $('#erp_country_id').chosen();
   $('.date').datepicker({    
            dateFormat: "yy-mm-dd",
            changeMonth: true,
            changeYear: true,
            showButtonPanel: true,
            yearRange:'1950:2013',
        });     
});
</script>
<script>
 $("#geoid").change(function(){
      var geo_id=$(this).val();
      $.get('get_street/'+geo_id,function(data){
          if(data!='')
              $('#street_id').html(data);
    });       
 });
</script>
<script>
    $("#erp_city_id").change(function(){
       var city_id=$(this).val();
              $.get('get_geoarea/'+city_id,function(data){
                  if(data !=''){
                      $('#geoid').html(data);
                       $("#geoid").change();
                  }
             }) 
  });
</script>

<script>
    $('#erp_country_id').change(function() {
          var country_id = $(this).val();
            $.get('get_erp_city/' + country_id, function(data){
                 $('#erp_city_id').html(data);
                 $('#erp_city_id').change();
            });
    });   
</script>   
<script>
    $('#erp_country_id').change();
</script>
<script>

var markerArray = [];

    function initialize(lat,lang){

        var myLatlng = new google.maps.LatLng(lat,lang);
        var mapOptions ={
            zoom:17,
            center:myLatlng,
            streetViewControl:false,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        }


        var map = new google.maps.Map(document.getElementById('map'),mapOptions);
        var marker=new google.maps.Marker({
                    position:myLatlng,
               });
               marker.setMap(map);

               markerArray.push(marker);
               
               google.maps.event.addListener(map, "click", function(event){

                   document.getElementById("latitude").value=event.latLng.lat();
                   document.getElementById("langtitude").value=event.latLng.lng();

                   clearMarkers();
                   
                   var marker = new google.maps.Marker({
                       position: event.latLng,
                       map: map
                    });
                   
                   markerArray.push(marker);
                   		
               });
        
    }
    
    function reIntMap(lat,lang){
        if(lat!=''&&lang!=''){
           initialize(lat,lang);
        }
    }

    function clearMarkers() {
        if (markerArray) {
            for (var i = 0; i < markerArray.length; i++)
                markerArray[i].setMap(null);
        }
    } 
</script>

<script>
    function get_alharam(){
        var city_id=$("#erp_city_id").val();
        var alharam=new Array();
        switch(city_id)
        {
            case "1": //makah
            alharam['lat']=21.422969;
            alharam['long']=39.825651;
            return alharam ; 
         break;
        case "2": // madinah
            alharam['lat']=24.468635;
            alharam['long']=39.611825;
            return alharam;
        break;
        default:
            alharam['lat']=24.246965;
            alharam['long']=44.993134;
            return alharam;
      }
      
        
  }
</script> 
<script type="text/javascript">
 $(document).ready(function(){
        $("#wizard_validate").formToWizard();
        $('.next').click(function(){
          var parent_=$("#step_map").parent(); /* init the map */
          if($(parent_).attr("style")=="display: block;"){
            var country_id=$('#erp_country_id').val(); 
            if(country_id=='966'){ /* if the country is sudi arebia*/
                 var alharam= get_alharam(); 
                 reIntMap(alharam['lat'],alharam['long']);
            }
                 
          }
          /*removing the fils from the inputs*/
          var parent_=$("#step_image").parent();
          if($(parent_).attr("style")=="display: none;"){
             $("#file_upload_images #file_images").val('');
          }
          
        });
        
    });
</script>
<script> 
  /*initializing the map*/              
  $(document).ready(function(){
      var lang_input=$("[name='langtitude']");
      var lat_input=$("[name='latitude']");
      $(lang_input).change(function(){
           reIntMap($(lat_input).val(),$(lang_input).val());
      });
     $(lat_input).change(function(){
         reIntMap($(lat_input).val(),$(lang_input).val());
    }); 
  });  
</script>
<script> /*adding official worker fun*/           
   $(document).ready(function(){
        var num_rows=$("#official_work_info .table-container tbody tr").length;
      $("#official_work_info #add_official").click(function(){
        var num_rows=$("#official_work_info .table-container tbody tr").length;
        if(num_rows<10){
          $("#official_work_info .table-container tbody").find("tr").each(function(index,object){
              var info_row_controls =$(object).html();
              $("#official_work_info .table-container tbody").append("<tr>"+info_row_controls+"</tr>");
              $("#official_work_info .table-container tbody :last-child .input-huge").val("");
              $("#official_work_info .table-container tbody").find('.remove_officialwork').click(function(){
                  var num_rows=$("#official_work_info .table-container tbody tr").length;
                  if(num_rows>1){
                    $(this).parent().parent().remove();
                  }
              })
               return false;
          });
        }  
      });
   });             
</script>
<script>
    /*adding hotels policy*/
    $(document).ready(function() {
        var num_rows = $("#hotel_policy .table-container tbody tr").length;
        $("#hotel_policy #add_hotelpolicy").click(function() {
            var num_rows = $("#hotel_policy .table-container tbody tr").length;
            if (num_rows < 10) {
                var last_row = $("#hotel_policy .table-container tbody  tr").last();
                var info_row_controls = $(last_row).html();
                $("#hotel_policy .table-container tbody").append("<tr>" + info_row_controls + "</tr>");
                var new_row = $("#hotel_policy .table-container tbody  tr").last();
                $(new_row).find('.input-full').each(function(index, input){
                    $(input).val(''); /*removing the values*/
                });
            }
           $(".remove_hotelpolicy").click(function() {
     
            var num_rows = $("#hotel_policy .table-container tbody tr").length;
            if (num_rows > 1){
                $(this).parent().parent().remove();
            }
        });
        });
        $(".remove_hotelpolicy").click(function() {
     
            var num_rows = $("#hotel_policy .table-container tbody tr").length;
            if (num_rows > 1){
                $(this).parent().parent().remove();
            }
        });

    });
</script>
<script>       
/*the script for uploading images*/
$(document).ready(function(){
   $("#file_upload_images #file_images").change(function(){
       show_images();
       
   })
   $("#file_upload_images #press_images").click(function(){
        $("#file_upload_images #file_images").click();
        
   }) 
});
function show_images(){
  var rFilter = /^image\/(?:bmp|cis\-cod|gif|ief|jpeg|pipeg|png|svg\+xml|tiff|x\-cmu\-raster|x\-cmx|x\-icon|x\-portable\-anymap|x\-portable\-bitmap|x\-portable\-graymap|x\-portable\-pixmap|x\-rgb|x\-xbitmap|x\-xpixmap|x\-xwindowdump)$/i;
  var files_arr= document.getElementById("file_images").files;
    
if(files_arr.length==0){return;} 
var index=0;
for(var i=0;i<files_arr.length;i++){
      var oFile =files_arr[i];
      var oFReader = new FileReader();
      if (!rFilter.test(oFile.type)){alert("You must select a valid image file!"); return;}
      oFReader.readAsDataURL(oFile);
 oFReader.onload=function(oFREvent){
        var file=files_arr[index];
       /*creat the image container*/
        var img_continer=document.createElement('div');
        img_continer.setAttribute('style','float:right; padding:5px; margin:5px');
        img_continer.setAttribute('class','hotel_images');
        document.getElementById('img_galary').appendChild(img_continer);
 /*create image*/ 
   var img=document.createElement("img");
            img.height='150';
            img.width='150';
            img.src = oFREvent.target.result;
            img_continer.appendChild(img);
            /*creat input_binary_info*/
        var input=document.createElement("input");
            input.setAttribute('type','hidden');
            input.setAttribute('name','image_info[]');
            input.setAttribute('value',oFREvent.target.result);
            img_continer.appendChild(input);
            
            var input=document.createElement("input");
            input.setAttribute('type','hidden');
            input.setAttribute('name','image_name[]');
            input.setAttribute('value',files_arr[index].name) /*to catch the file name we updte the counter */;
            img_continer.appendChild(input);
   
        /*create a remove btn*/
        var remove_btn= document.createElement("a");
            remove_btn.setAttribute('href','JavaScript:void(0)');
            remove_btn.setAttribute('class','btn');
            remove_btn.setAttribute('onclick','var img_con= this.parentNode.parentNode;img_con.parentNode.removeChild(img_con)');
            remove_btn.innerHTML='Remove'; 
       var warper =document.createElement("div");
            warper.setAttribute('style','padding:5px 0 5px 0; text-align:center');
            img_continer.appendChild(warper);
            warper.appendChild(remove_btn);
            index++;/*update counter*/
    }
    
/* create input that holds image_names */ 
}
}
</script>
<script>
/*the javascript validation  for a form*/
   $(document).ready(function(){
      $("#wizard_validate").validationEngine('attach',{
          promptPosition :"topLeft", 
          scroll: false,
          binded:true,
           prettySelect :true,
           useSuffix: "_chosen"
      });  
    })  
</script>                                                                                                                                                                 