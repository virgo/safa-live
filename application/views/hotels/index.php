<link href="<?= NEW_CSS_JS . '_' . lang('DIR') ?>/new_search/css/style.css" rel="stylesheet" type="text/css" />
<link href="<?= NEW_CSS_JS . '_' . lang('DIR') ?>/new_search/css/extensions.css" rel="stylesheet" type="text/css" />
<script src="<?= NEW_CSS_JS ?>/jquery.imgpreview.js" type="text/javascript"></script>
<style >
    div.grid_8 .box:first-child {
        border-radius: 8px 8px 0 0;
    }
    .widget{ margin-right: 1%;
    width: 98%;}
    .serach_btn:hover{
        box-shadow: 0px 0px 4px rgba(0,0,0,.13);
    }
.widget{width:98%;}

</style>

    <div class="widget">
        <? if ($this->destination == 'admin' || $this->destination == 'uo'): ?>
            <a class="btn btn-custom radius fr" href="<?= base_url('hotels/add') ?>"><?= lang('global_add_new_record') ?></a>
        <? endif; ?>    
        <div class="path-container Fright">
            <div class="path-name Fright">
                <a href="<?php
                if (session('uo_id')) {
                    echo site_url() . 'uo/dashboard';
                } else if (session('ea_id')) {
                    echo site_url() . 'ea/dashboard';
                } else if (session('admin_login')) {
                    echo site_url() . 'admin/dashboard';
                } else {
                    echo site_url();
                }
                ?>"><?php echo lang('global_system_management') ?></a>
            </div>
            <div class="path-arrow Fright">
            </div>
            <div class="path-name Fright">
                <?= lang('hotels_title') ?>
            </div>


        </div>
        <? if ($this->destination == 'hm' || $this->destination == 'ea' || $this->destination == 'uo') { ?>
            <a href="<?= site_url('messages/popup_new/hotel') ?>" title="<?= lang('global_add') ?>" class="fancybox fancybox.iframe btn Fleft"><?= lang('global_add') ?></a>
            <?php
        }
        ?>
    </div>
<div class="span12">
    <div class="span4 " id="leftcolumn">
        <div class="widget">
            <!-- Search form -->
            <div class="padded">
                <h2 class="headline radius"><?= lang('search_hotels') ?></h2>
                <?= form_open( site_url('hotels/index'), 'method="get" style="overflow:hidden;"') ?>
                <!-- calendar image container -->
                <span style="display:none;"><img class="trigger" id="trigger" alt="" src="img/blank.gif" /></span>
                <fieldset class="radius">
                    <p>
                        <label class="" for="destination"><?= lang('hotels_erp_city_id') ?></label><br/>
                        <?= form_dropdown("erp_city_id",
                                ddgen('erp_cities', array('erp_city_id', name()), array('erp_country_id' => 966)),
                                set_value("erp_city_id"), "id='erp_city_id' class='select'   style='width:100%;'") ?>
                    </p>
                    <hr class="thin" />
                    <p>
                        <label><?= lang('hotels_erp_hotellevel_id') ?></label>
                        <br/>
<?= form_dropdown('erp_hotellevel_id',
        ddgen('erp_hotellevels', array('erp_hotellevel_id', name())), set_value('erp_hotellevel_id'),
        "name='s_example' class='select input-full' style='width:100%;'placeholder=''") ?>
                    </p>
                    <hr class="thin" />
                    <p>
                        <label><?= lang('hotels_name_ar') ?></label>
                        <br/>
                        <?= form_input('name_ar', set_value("name_ar")," placeholder='" . lang('hotels_name_ar') . "' class='full'   ") ?>
                    </p>
                    <hr class="thin" />
                    <p>
                        <label><?= lang('hotels_name_la') ?></label>
                        <br/>
                        <?= form_input('name_la', set_value("name_la"),"placeholder='" . lang('hotels_name_la') . "' class='full'  ") ?>  
                    </p>
                    <input type="submit" name="search" class="btn btn-custom radius fr" value="<?= lang('global_search') ?>&raquo;" />
                </fieldset>
    <?= form_close() ?>
            </div>

        </div>
    </div>
    <!-- End of Search form -->
    <div class="span8">
            <? if (isset($items) && count($items) > 0): ?>
                <? foreach ($items as $item): ?>

            <div class='  widget '>
                <div class="infopanel  padded">

                    <?
                    $this->erp_hotels_photos_model->erp_hotel_id = $item->erp_hotel_id;
                    $photos = $this->erp_hotels_photos_model->get();
                    $thumb_photo = (count($photos) > 0 ? $photos[0]->hotel_photospath : ''); // returns true 
                    $image = false;

                    if ($thumb_photo)
                        if (file_exists("./static/hotel_album/" . $item->erp_hotel_id . "/" . $thumb_photo))
                            $image = base_url() . "static/hotel_album/" . $item->erp_hotel_id . "/" . $thumb_photo;
                        
                    if( ! $image)
                    {
                        $path = "./static/hotel_album/" . $item->erp_hotel_id . "/";
                        $files = glob($path.'*');
                        if(count($files)) {
                            natcasesort($files);
                            foreach($files as $file) {
                                
                                $image = base_url() . "static/hotel_album/" . $item->erp_hotel_id . "/" . str_replace($path, '', $file);
                            }
                        }
                    }
                    if (!$image)
                        $image = base_url() . "static/hotel_album/20/buidling17.jpg";
                    ?>
                    <a class="previewimg nyroModal fl fancybox" href="<?= $image ?>"><img src="<?= $image ?>" width="150" height="150" /></a>
                    <h3>
                        <a href="<?= base_url('hotels/hotel_details/' . $item->erp_hotel_id) ?>"> <?= $item->hotel_name ?></a>
                        <?
                        $x = 1;
                        while ($x <= $item->erp_hotellevel_id) {
                            ?>
                            <img src="<?= IMAGES ?>/imgs/star.png" alt="*" />
                            <?
                            $x++;
                        }
                        ?>
                    </h3>
                    <div class="address"><?= $item->country_name ?>,<?= $item->city_name ?> <a class="showmap fancybox.iframe"  href="<?= site_url('hotels/map/'.$item->erp_hotel_id) ?>">[<?= lang('map') ?>]</a></div>
                    <p>
        <?= $item->remark ?>
                        <a href="<?= base_url('hotels/hotel_details/' . $item->erp_hotel_id) ?>"><?= lang('readmore') ?></a> 
                    </p>
                    <div class="clear"></div>
                </div>

                <!-- Shortcut buttons -->
                <div class="fr">
                    <? if ($this->destination == 'admin' || $this->destination == 'uo'): ?>    

                        <a class="radius serach_btn padded" href="<?= base_url('hotels/edit/' . $item->erp_hotel_id) ?>" title="<?= lang('global_edit') ?>"><i class="icon-edit icon-2x "></i></a>
                        
                    <!-----the other btns exist------->

                    <? else: ?>
                        <? if ($this->destination == 'hm' || $this->destination == 'ea' || $this->destination == 'uo'): ?>
                            <?
                            $this->erp_companies_hotels_model->company_id = $company_id;
                            $this->erp_companies_hotels_model->erp_hotel_id = $item->erp_hotel_id;
                            $exist = $this->erp_companies_hotels_model->check_build();
                            ?>
                <? if (isset($exist)): ?>
                                <a class="radius serach_btn padded" href="<?= base_url('build_hotels/edit/' . $exist) ?>" title="<?= lang('bulid_hotel_edit') ?>"><i class="icon-building icon-2x "></i></a>
                <? else: ?>
                                <a class="radius serach_btn padded" href="<?= base_url('build_hotels/add/' . $item->erp_hotel_id) ?>" title="<?= lang('bulid_hotel_title') ?>"><i class="icon-building icon-2x "></i></a>
                            <? endif; ?> 
				<a class="fancybox fancybox.iframe radius serach_btn padded"   href="<?= site_url('messages/popup_new/hotel/' . $item->erp_hotel_id) ?>" title="<?= lang('global_edit') ?>" class="fancybox fancybox.iframe"><span class="icon-edit icon-2x"></span></a>
                <? endif; ?>
        		<? endif; ?>                                   
                </div>
                <div class="clear"></div>
            </div>
    <? endforeach; ?>
<? endif; ?>
    </div>
</div>
<div class="clear"></div>
<? if (isset($pagination) && $pagination != ''): ?>
    <?= $pagination ?><div class="clear"></div>
<? endif; ?>
    
<script type="text/javascript">
    $(document).ready(function() {
        $(".showmap").fancybox();
    });
</script>
<script>
    // Image preview on mouseover
    $('.infopanel a.previewimg').imgPreview({
        containerID: 'imgPreviewWithStyles',
        imgCSS: {height: 200},
        // When container is shown:
        onShow: function(link) {
            $(link).stop().animate({opacity: 0.8});
            $('img', this).stop().css({opacity: 0});
        },
        onLoad: function() {
            $(this).animate({opacity: 1}, 300);
        },
        onHide: function(link) {
            $(link).stop().animate({opacity: 1});
        }
    });
</script>   

<script>
    $(document).ready(function() {
        $('.fancybox').fancybox({
            afterClose: function() {
                //location.reload();
            }
        });
    });
</script>
