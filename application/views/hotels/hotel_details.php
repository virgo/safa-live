
<link href="<?= NEW_CSS_JS .'_'. lang('DIR') ?>/new_search/css/style.css" rel="stylesheet" type="text/css" />
<link href="<?= NEW_CSS_JS .'_'. lang('DIR') ?>/new_search/css/extensions.css" rel="stylesheet" type="text/css" />

    
	<script src="<?= NEW_CSS_JS .'_'. lang('DIR') ?>/new_search/js/jquery.ui.tabs.min.js" type="text/javascript"></script>
    


    <script type='text/javascript' src='<?= NEW_TEMPLATE ?>/plugins/nivo-slider/jquery.nivo.slider.js'></script>
    <link href="<?= NEW_TEMPLATE ?>/plugins/nivo-slider/style.css" rel="stylesheet" type="text/css" />
    <link href="<?= NEW_TEMPLATE ?>/plugins/nivo-slider/default/default.css" rel="stylesheet" type="text/css" />
    


   <script type="text/javascript">
	//<![CDATA[
	
	// we define tabs as a global variable!
	var tabs;
	
	// Sample hotel list -  LAT/LNG/Name
	
	

	
	$(document).ready(function(){
		
		

		/* tabs */
		tabs = $("#tabs").tabs();
		
		// Any component that requires some dimensional computation for its initialization won't work in a hidden tab, 
		// because the tab panel itself is hidden via display: none
		// For Google maps we can show the map once the tab is displayed like this: 
		$('#tabs').bind('tabsshow', function(event, ui) {
			if (ui.panel.id == "maptab") {
				// Init google map
				// center to the first hotel in the list
				// do not use clustering
				// Street view in div#pano
				Site.gmapInit(hotels[0][0], hotels[0][1], 16, false, "pano");
			}
		}); 
		
		/* template setup */
		Site.setup();
		
		/* notice box demonstration */
		
	
	//]]>
	</script>
    
    <script>
    $(document).ready(function() {
        jQuery('.adly').remove();
    });
    
    </script>

<div class="container container_12">
<div class="grid_4" id="leftcolumn">
<div class="grid_4" style="width: 295px;" id="leftcolumn">
        <div class="box radius">
            <!-- Search form -->
            <div class="padded">
                <h2 class="headline radius"><?= lang('search_hotels') ?></h2>
                <?= form_open(site_url('hotels/index'), 'method="get" style="overflow:hidden;"') ?>
                <!-- calendar image container -->
                <span style="display:none;"><img class="trigger" id="trigger" alt="" src="img/blank.gif" /></span>
                <fieldset class="radius">
                    <p>
                        <label class="" for="destination"><?= lang('hotels_erp_city_id') ?></label><br/>
                        <?= form_dropdown("erp_city_id",
                                ddgen('erp_cities', array('erp_city_id', name()), array('erp_country_id' => 966)),
                                set_value("erp_city_id"), "id='erp_city_id'    style='width:100%;'") ?>
                    </p>
                    <hr class="thin" />
                    <p>
                        <label><?= lang('hotels_erp_hotellevel_id') ?></label>
                        <br/>
<?= form_dropdown('erp_hotellevel_id',
        ddgen('erp_hotellevels', array('erp_hotellevel_id', name())), set_value('erp_hotellevel_id'),
        "name='s_example' class='select input-full' style='width:100%;'placeholder='sdfsd'") ?>
                    </p>
                    <hr class="thin" />
                    <p>
                        <label><?= lang('hotels_name_ar') ?></label>
                        <br/>
<?= form_input('name_ar', set_value("name_ar"),
        " placeholder='" . lang('hotels_name_ar') . "' class='full'   ") ?>
                    </p>
                    <hr class="thin" />
                    <p>
                        <label><?= lang('hotels_name_la') ?></label>
                        <br/>
<?= form_input('name_la', set_value("name_la"),
        "placeholder='" . lang('hotels_name_la') . "' class='full'  ") ?>  
                    </p>
                    <input type="submit" name="search" class="btn btn-custom radius fr" value="<?= lang('global_search') ?>&raquo;" />
                </fieldset>
    <?= form_close() ?>
            </div>

        </div>
    </div>
    <!-- End of Search form -->
  </div>
    
    

 <div class='grid_8 searchpanel'>	
 <div class="box radius" style="padding:10px;">	
		<h1 class="">
		
		<?= $item->hotel_name ?> 
                        <? if ($item->erp_hotellevel_id == '1'): ?>
                            <img src="<?= IMAGES ?>/imgs/star.png" style="width:15px;height: 11px"/>
                        <? endif ?>
                        <? if ($item->erp_hotellevel_id == '2'): ?>
                            <img src="<?= IMAGES ?>/imgs/star.png" style="width:15px;height: 11px"/>
                            <img src="<?= IMAGES ?>/imgs/star.png" style="width:15px;height: 11px"/>
                        <? endif ?>

                        <? if ($item->erp_hotellevel_id == '3'): ?>
                            <img src="<?= IMAGES ?>/imgs/star.png" style="width:15px;height: 11px"/>
                            <img src="<?= IMAGES ?>/imgs/star.png" style="width:15px;height: 11px"/>
                            <img src="<?= IMAGES ?>/imgs/star.png" style="width:15px;height: 11px"/>
                        <? endif ?> 

                        <? if ($item->erp_hotellevel_id == '4'): ?>
                            <img src="<?= IMAGES ?>/imgs/star.png" style="width:15px;height: 11px"/>
                            <img src="<?= IMAGES ?>/imgs/star.png" style="width:15px;height: 11px"/>
                            <img src="<?= IMAGES ?>/imgs/star.png" style="width:15px;height: 11px"/>
                            <img src="<?= IMAGES ?>/imgs/star.png" style="width:15px;height: 11px"/>
                        <? endif ?>   

                        <? if ($item->erp_hotellevel_id == '5'): ?>
                            <img src="<?= IMAGES ?>/imgs/star.png" style="width:15px;height: 11px"/>
                            <img src="<?= IMAGES ?>/imgs/star.png" style="width:15px;height: 11px"/>
                            <img src="<?= IMAGES ?>/imgs/star.png" style="width:15px;height: 11px"/>
                            <img src="<?= IMAGES ?>/imgs/star.png" style="width:15px;height: 11px"/>
                            <img src="<?= IMAGES ?>/imgs/star.png" style="width:15px;height: 11px"/>
                        <? endif ?>   

                        <? if ($item->erp_hotellevel_id == '6'): ?>
                            <img src="<?= IMAGES ?>/imgs/star.png" style="width:15px;height: 11px"/>
                            <img src="<?= IMAGES ?>/imgs/star.png" style="width:15px;height: 11px"/>
                            <img src="<?= IMAGES ?>/imgs/star.png" style="width:15px;height: 11px"/>
                            <img src="<?= IMAGES ?>/imgs/star.png" style="width:15px;height: 11px"/>
                            <img src="<?= IMAGES ?>/imgs/star.png" style="width:15px;height: 11px"/>
                            <img src="<?= IMAGES ?>/imgs/star.png" style="width:15px;height: 11px"/>
                        <? endif ?> 

                        <? if ($item->erp_hotellevel_id == '7'): ?>
                            <img src="<?= IMAGES ?>/imgs/star.png" style="width:15px;height: 11px"/>
                            <img src="<?= IMAGES ?>/imgs/star.png" style="width:15px;height: 11px"/>
                            <img src="<?= IMAGES ?>/imgs/star.png" style="width:15px;height: 11px"/>
                            <img src="<?= IMAGES ?>/imgs/star.png" style="width:15px;height: 11px"/>
                            <img src="<?= IMAGES ?>/imgs/star.png" style="width:15px;height: 11px"/>
                            <img src="<?= IMAGES ?>/imgs/star.png" style="width:15px;height: 11px"/>
                            <img src="<?= IMAGES ?>/imgs/star.png" style="width:15px;height: 11px"/>
                        <? endif ?>

		</h1>
		
		<p><em>

 		<?= $item->country_name ?>,<?= $item->city_name ?>
 		                   
		</em></p>
		
		<!-- Tabs -->
		<div id="tabs">
			<ul>
				<li><a href="#tabs-1"><?php echo lang('hotel_details');?></a></li>
				<li><a href="#maptab"><?php echo lang('GOOGLE_MAP');?></a></li>
			</ul>
			<!-- Tab 1: Hotel Overview -->
			<div id="tabs-1">
				<div class="infopanel padded">
					
					<!-- 
					<span class="padded fl tc score"><b>8.40</b><br/><a href="#" onclick="tabs.tabs('select', 1); return false;"><small>32 reviews</small></a></span>
					 -->
					 
					<!-- 
					<p class="ttc">
					-->
		
					<?
                    $this->erp_hotels_photos_model->erp_hotel_id = $item->erp_hotel_id;
                    $photos = $this->erp_hotels_photos_model->get();
                    $thumb_photo = (count($photos) > 0 ? $photos[0]->hotel_photospath : ''); // returns true 
                    $image = false;

                    if (file_exists("./static/hotel_album/" . $item->erp_hotel_id . "/" . $thumb_photo))
                     $image = base_url() . "static/hotel_album/" . $item->erp_hotel_id . "/" . $thumb_photo;
                                
                    if ($thumb_photo)
                        if (file_exists("./static/hotel_album/" . $item->erp_hotel_id . "/thumbs/" . $thumb_photo) && !$image) 
                            $image = base_url() . "static/hotel_album/" . $item->erp_hotel_id . "/thumbs/" . $thumb_photo;
                    /*
                        if ($thumb_photo)
                        if (!$image)
                            if (file_exists("./static/hotel_album/" . $item->erp_hotel_id . "/" . $thumb_photo))
                                $image = base_url() . "static/hotel_album/" . $item->erp_hotel_id . "/" . $thumb_photo;
					*/
                            
                    if (!$image)
                        $image = base_url() . "static/hotel_album/20/thumbs/buidling17.jpg";
                    ?>
                    
                    <!-- 
                    <img src="<?php echo $image;?>" alt="" class="w12 big" />
                     -->
                    
                    <div class="slider-wrapper theme-default">
                    <div id="slider" class="nivoSlider">
					 <? if (isset($hotel_photos) && count($hotel_photos) > 0): ?> 
                            <? $folder_path = base_url() . 'static/hotel_album/' . $this->uri->segment('3'); ?>
                            
                                    <? foreach ($hotel_photos as $index => $photo): ?>
                                                                        <img src="<?= $folder_path . '/' . $photo->hotel_photospath ?>" data-thumb="<?= $folder_path . '/' . $photo->hotel_photospath ?>" alt="" />
                                        
                                    <? endforeach; ?>
               

                     <? endif;?> 
                    </div>
                    </div>
                    		
 					<?php echo $item->remark;?>
					
					<!-- 
					</p>
					-->
					
					
					
					
					<!-- Hotel Facilities -->
					<h6><?php echo lang('hotel_details');?></h6>
					<table class="full">
						<tbody>
							<tr>
								<td class="w14 padded"><b><?= lang('hotel_rooms') ?></b></td>
								<td class="w34 padded"><?= $item->rooms_number ?></td>
							</tr>
							<tr class="dashed">
								<td class="w14 padded"><b><?= lang('connect_info') ?></b></td>
								<td class="w34 padded">
								
								<? if (lang('global_lang') == 'ar'): ?> <img src="<?= IMAGES ?>/filetree/css.png"> &nbsp; <?= $item->address_ar ?><? else: ?><img src="<?= IMAGES ?>/icons/alerts/sign_tick.png"><?= $item->address_la ?><? endif ?>
	                            <br>
	                            <img src="<?= IMAGES ?>/filetree/css.png"/>&nbsp; <?= lang('hotel_phone') ?> :<?= $item->phone ?>
	                            <br>
	                            <img src="<?= IMAGES ?>/filetree/css.png"/>&nbsp; <?= lang('hotel_email') ?> : <?= $item->email ?>
	                            <br>
	                            <img src="<?= IMAGES ?>/filetree/css.png"/>&nbsp; <?= lang('hotel_fax') ?> : <?= $item->fax ?>
                            
								</td>
							</tr>
							<tr class="dashed">
								<td class="w14 padded"><b><?= lang('hotel_advantages') ?></b></td>
								<td class="w34 padded">
									<br>
		                            <? if (isset($hotel_advantages) && count($hotel_advantages) > 0): ?>
		                                <? foreach ($hotel_advantages as $hotel_advantages): ?>
		                                    <br>
		                                    <img src="<?= IMAGES ?>/icons/alerts/sign_tick.png"/><?= item('erp_hotel_advantages', name(), array('erp_hotel_advantages_id' => $hotel_advantages->erp_hotel_advantages_id)) ?> 
		                                    <?= $hotel_advantages->erp_hotel_advantages_details ?> 
		                                <? endforeach; ?>
		                            <? endif ?>
								</td>
							</tr>
							
							<tr class="dashed">
								<td class="w14 padded"><b><?= lang('hotel_languages') ?></b></td>
								<td class="w34 padded">
									<br>
		                            <? if (isset($hotel_speaklanguage) && count($hotel_speaklanguage) > 0): ?>
                                	<? foreach ($hotel_speaklanguage as $hotel_speaklanguage): ?>
                                    <br>
                                    <img src="<?= IMAGES ?>/icons/alerts/sign_tick.png"/><?= item('erp_hotel_speakinglanguage', name(), array('erp_hotel_speakinglanguage_id' => $hotel_speaklanguage->erp_hotelspeakinglanguage_id)) ?> 
                                	<? endforeach; ?>
                            		<? endif ?>
								</td>
							</tr>
							
						</tbody>
					</table>
					<!-- End of Hotel Facilities -->
										
					<!-- Hotel Policies -->
					<h6><?= lang('hotel_policy') ?></h6>
					
					<table class="full">
						<tbody>
						
						<? if (isset($hotel_polices) && count($hotel_polices) > 0): ?>
                                <? foreach ($hotel_polices as $hotel_polices): ?>
                                 <tr>
								<td class="w14 padded"><b>
                                    <img src="<?= IMAGES ?>/icons/alerts/sign_tick.png"/> &nbsp;<?= item('erp_hotels_policies', name(), array('erp_hotels_policies_id' => $hotel_polices->erp_hotels_policies_id)) ?> 
                                    <?= $hotel_polices->policy_description ?> 
                                    </b></td>
                                    </tr>
                                <? endforeach; ?>
                            <? endif ?>
                            
							<!-- 
							<tr>
								<td class="w14 padded"><b>Check-in</b></td>
								<td class="w34 padded">From 15:00 hours </td>
							</tr>
							 -->
							 
						</tbody>
					</table>
					<!-- End of Hotel Policies -->
					
					
				</div>
			</div>
			<!-- End of Tab 1: Hotel Overview -->
			
			
			<!-- Tab 3: Google map -->
			<div id="maptab" style="width:600px;height:500px" >
				

			<script src="https://maps.googleapis.com/maps/api/js?AIzaSyC794uPScLtRUa8Uwk9nX8RCjrepoxszxg&sensor=true"></script>
			<div id="map" style="width:100%;height:100%" >
			</div>
			 <script>
			    function initialize(lat,long){
			        var myLatlng = new google.maps.LatLng(lat,long);
			        var mapOptions = {
			            zoom:15,
			            center:myLatlng,
			            mapTypeId: google.maps.MapTypeId.ROADMAP
			        }
			        var map = new google.maps.Map(document.getElementById('map'),mapOptions);
			         var marker = new google.maps.Marker({
			            position:myLatlng,
			            map: map,
			        });
			    }
			</script>
			<script type="text/javascript">
			            initialize(<?php echo $item->googel_map_latitude;  ?>,<?php echo $item->googel_map_langtitude;  ?>);
			             
			
			</script>



			</div>
			<!-- End of Tab 3: Google map -->

		</div> 
		<!-- End of Tabs -->
		
		<!-- Shortcut buttons -->
	
		<!-- End of Shortcut buttons -->
		
		<div class="clear">&nbsp;</div>
		<p>&nbsp;</p>
		
	</div>  
    
    
    
    
		
		<!-- Shortcut buttons -->
		<div class="fr">
			<button class="box radius" title="Print this page" onclick="window.print();"><img src="<?= IMAGES ?>/imgs/print.gif" alt="Print" /></button>
		</div>
		<!-- End of Shortcut buttons -->
		
		<div class="clear">&nbsp;</div>
	</div>
	</div>	

	
	<!--  Load the javascript files  -->
    <script type="text/javascript" src="jquery.nivo.slider.js"></script>
    <script type="text/javascript">
    
    //<!--  Load the slider  --> 
    $(window).load(function() {
        $('#slider').nivoSlider();
    });
    
    </script>