<div class="widget-container">
    <div class="widget"  style="margin-right:0px;" >
        <div class="path-container Fright">
            <div class="path-name Fright">
                <a href=""><?= lang('global_system_management') ?></a>
            </div>
            <div class="path-arrow Fright">
            </div>
            <div class="path-name Fright">
                <div class="path-name Fright"> <a href="<?php echo  site_url('hotels') ?>"><?php echo  lang('menu_main_hotels') ?></a></div>
            </div>
            <div class="path-arrow Fright">
            </div>
            <div class="path-name Fright">
                <?= lang('bulid_hotel_edit') ?>
            </div>
        </div>
    </div>
    <div>
        <?= form_open_multipart("", "id='SignupForm'") ?>
        <div class="inner-widget">
            <div class="widget-header">
                <div class="widget-header-icon Fright">
                    <span class="icos-pencil2"></span>
                </div>
                <div class="widget-header-title Fright">
                    <?= lang('general_info') ?>
                </div>
            </div>
            <div class="widget-container">
                <div class="row-form">
                    <div class="span6" >
                        <div class="span4 TAL Pleft10">
                            <?= lang('hotel_label') ?>
                            :
                        </div> 
                        <div class="span8" style="color:blue">
                            <?= $hotel_name ?>
                        </div>
                    </div>
                    <div class="span6" >
                        <div class="span4 TAL Pleft10" >
                            <?= lang('city_label') ?>
                            :
                        </div>
                        <div class="span8" style="color:blue" >
                            <?= $hotel_city ?>
                        </div>
                    </div>
                </div>
                <div class="row-form">
                    <div class="span6">
                        <div class="span4 TAL Pleft10"><?= lang('Company_label') ?> :</div>
                        <div class="span8" style="color:blue">
                            <?= $company_name ?>
                        </div>
                    </div>
                    <div class="span6">
                        <div class="span4 TAL Pleft10"><?= lang('Company_type_label') ?> :</div>
                        <div class="span8" style="color:blue" >
                            <?= $company_type ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="inner-widget" id="official_work_info" >
            <div class="widget-header">
                <div class="widget-header-icon Fright">
                    <span class="icos-pencil2"></span>
                </div>
                <div class="widget-header-title Fright">
                    <?= lang('contact_official_info') ?>
                </div>
                <a class="btn Fleft" id="add_official" href="javascript:void(0)"><?= lang('global_add') ?></a>
            </div>
            <div class="widget-container" >
                <div class="table-container">
                    <table cellpadding="0" cellspacing="0">
                        <thead>
                            <tr>
                                <th><?= lang('official_work') ?></th>
                                <th><?= lang('official_name') ?></th>
                                <th><?= lang('official_email') ?></th>
                                <th><?= lang('official_phonetype') ?></th>
                                <th><?= lang('official_phone') ?></th>
                                <th><?= lang('official_skype') ?></th>
                                <th><?= lang('official_massenger') ?></th>
                                <th><?= lang('global_operations') ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <? if ($this->input->post('official_name') && count($this->input->post('official_name')) > 0): ?>
                                <? foreach ($this->input->post('official_name') as $index => $value): ?>
                                    <tr>
                                        <td>
                                            <?= form_input('official_work[' . $index . ']', set_value('official_work[' . $index . ']'), ' class="input-huge" ') ?>
                                            <?= form_error('official_work[' . $index . ']', '<div class="bottom" style="color:red" >', '</div>'); ?>
                                        </td>
                                        <td>
                                            <?= form_input('official_name[' . $index . ']', set_value('official_name[' . $index . ']'), ' class="input-huge" ') ?>
                                            <?= form_error('official_name[' . $index . ']', '<div class="bottom" style="color:red" >', '</div>'); ?>
                                        </td>
                                        <td>
                                            <?= form_input('official_email[' . $index . ']', set_value('official_email[' . $index . ']'), ' class="input-huge" ') ?>
                                            <?= form_error('official_email[' . $index . ']', '<div class="bottom" style="color:red" >', '</div>'); ?>
                                        </td>
                                        <td>
                                            <?= form_dropdown('official_phonetype[' . $index . ']', ddgen('erp_phone_type'), set_value('official_phonetype[' . $index . ']'), ' class="input-huge" ') ?>
                                            <?= form_error('official_phonetype[' . $index . ']', '<div class="bottom" style="color:red" >', '</div>'); ?>
                                        </td>
                                        <td>
                                            <?= form_input('official_phone[' . $index . ']', set_value('official_phone[' . $index . ']'), ' class="input-huge"  data-prompt-position="topLeft:-40,10" ') ?>
                                            <?= form_error('official_phone[' . $index . ']', '<div class="bottom" style="color:red" >', '</div>'); ?> 
                                        </td>
                                        <td>
                                            <?= form_input('official_skype[' . $index . ']', set_value('official_skype[' . $index . ']'), ' class="input-huge" ') ?>
                                            <?= form_error('official_skype[' . $index . ']', '<div class="bottom" style="color:red" >', '</div>'); ?> 
                                        </td>
                                        <td>
                                            <?= form_input('official_messenger[' . $index . ']', set_value('official_messenger[' . $index . ']'), ' class="input-huge" ') ?>
                                            <?= form_error('official_messenger[' . $index . ']', '<div class="bottom" style="color:red" >', '</div>'); ?> 
                                        </td>
                                        <td class="TAC"><a href="javascript:void(0)" class="remove_officialwork" ><span class="icon-trash"></span></a></td>
                                    </tr>
                                <? endforeach; ?>
                            <? else: ?>
                                <? if (isset($contact_official) && count($contact_official) > 0): ?>
                                    <? foreach ($contact_official as $index => $item): ?>
                                        <tr>
                                            <td>
                                                <?= form_input('official_work[' . $index . ']', set_value('official_work[' . $index . ']', $item->position), ' class="input-huge" ') ?>
                                                <?= form_error('official_work[' . $index . ']', '<div class="bottom" style="color:red" >', '</div>'); ?>
                                            </td>
                                            <td>
                                                <?= form_input('official_name[' . $index . ']', set_value('official_name[' . $index . ']', $item->name), ' class="input-huge" ') ?>
                                                <?= form_error('official_name[' . $index . ']', '<div class="bottom" style="color:red" >', '</div>'); ?>
                                            </td>
                                            <td>
                                                <?= form_input('official_email[' . $index . ']', set_value('official_email[' . $index . ']', $item->email), ' class="input-huge" ') ?>
                                                <?= form_error('official_email[' . $index . ']', '<div class="bottom" style="color:red" >', '</div>'); ?>
                                            </td>
                                            <td>
                                                <?= form_dropdown('official_phonetype[' . $index . ']', ddgen('erp_phone_type'), set_value('official_phonetype[' . $index . ']', $item->phone_type_id), ' class="input-huge" ') ?>
                                                <?= form_error('official_phonetype[' . $index . ']', '<div class="bottom" style="color:red" >', '</div>'); ?>
                                            </td>
                                            <td>
                                                <?= form_input('official_phone[' . $index . ']', set_value('official_phone[' . $index . ']', $item->phone), ' class="input-huge"  data-prompt-position="topLeft:-40,10" ') ?>
                                                <?= form_error('official_phone[' . $index . ']', '<div class="bottom" style="color:red" >', '</div>'); ?> 
                                            </td>
                                            <td>
                                                <?= form_input('official_skype[' . $index . ']', set_value('official_skype[' . $index . ']', $item->skype), ' class="input-huge" ') ?>
                                                <?= form_error('official_skype[' . $index . ']', '<div class="bottom" style="color:red" >', '</div>'); ?> 
                                            </td>
                                            <td>
                                                <?= form_input('official_messenger[' . $index . ']', set_value('official_messenger[' . $index . ']', $item->messenger), ' class="input-huge" ') ?>
                                                <?= form_error('official_messenger[' . $index . ']', '<div class="bottom" style="color:red" >', '</div>'); ?> 
                                            </td>
                                            <td class="TAC"><a href="javascript:void(0)" class="remove_officialwork in_db" ><span class="icon-trash"></span></a></td>
                                        </tr>
                                    <? endforeach; ?>
                                <? else: ?>
                                    <tr>
                                        <td><?= form_input('official_work[0]', set_value('official_work[0]'), ' class="input-huge" ') ?></td>
                                        <td><?= form_input('official_name[0]', set_value('official_name[0]'), ' class="input-huge" ') ?></td>
                                        <td><?= form_input('official_email[0]', set_value('official_email[0]'), ' class="input-huge" ') ?></td>
                                        <td>
                                            <?= form_dropdown('official_phonetype[0]', ddgen('erp_phone_type'), set_value('official_phonetype[0]'), ' class="input-huge" ') ?>
                                        </td>
                                        <td><?= form_input('official_phone[0]', set_value('official_phone[0]'), ' class="input-huge "  data-prompt-position="topLeft:-40,10" ') ?></td>
                                        <td><?= form_input('official_skype[0]', set_value('official_skype[0]'), ' class="input-huge" ') ?></td>
                                        <td><?= form_input('official_messenger[0]', set_value('official_messenger[0]'), ' class="input-huge" ') ?></td>
                                        <td class="TAC"><a href="javascript:void(0)" class="remove_officialwork" ><span class="icon-trash"></span></a></td>
                                    </tr>
                                <? endif; ?>  
                            <? endif; ?>
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
        <div class="inner-widget" id="company_rooms_info" >
            <div class="widget-header">
                <div class="widget-header-icon Fright">
                    <span class="icos-pencil2"></span>
                </div>
                <div class="widget-header-title Fright">
                    <?= lang('Company_rooms_info') ?>
                </div>
                <a class="btn Fleft" id="add_official" href="javascript:void(0)"><?= lang('global_add') ?></a>
            </div>
            <div class="widget-container" >
                <div class="table-container">
                    <table cellpadding="0" cellspacing="0">
                        <thead>
                            <tr>
                                <th><?= lang('rooms_floor') ?></th>
                                <th><?= lang('rooms_section') ?></th>
                                <th><?= lang('rooms_count') ?></th>
                                <th><?= lang('beds_count') ?></th>
                                <th><?= lang('date_from') ?></th>
                                <th><?= lang('date_to') ?></th>
                                <th><?= lang('global_operations') ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <? if ($this->input->post('rooms_floor') && count($this->input->post('rooms_floor')) > 0): ?>

                                <? foreach ($this->input->post('rooms_floor') as $index => $item): ?>
                                    <tr>
                                        <td>
                                            <?= form_dropdown('rooms_floor[' . $index . ']', ddgen('erp_floors'), set_value('rooms_floor[' . $index . ']'), ' class="input-huge" ') ?>
                                            <?= form_error('rooms_floor[' . $index . ']', '<div class="bottom" style="color:red" >', '</div>'); ?>
                                        </td>
                                        <td>
                                            <?= form_input('rooms_section[' . $index . ']', set_value('rooms_section[' . $index . ']'), ' class="input-huge" ') ?>
                                            <?= form_error('rooms_section[' . $index . ']', '<div class="bottom" style="color:red" >', '</div>'); ?>
                                        </td>
                                        <td>
                                            <?= form_input('rooms_count[' . $index . ']', set_value('rooms_count[' . $index . ']'), ' class="input-huge"') ?>
                                            <?= form_error('rooms_count[' . $index . ']', '<div class="bottom" style="color:red" >', '</div>'); ?>
                                        </td>
                                        <td>
                                            <?= form_input('beds_count[' . $index . ']', set_value('beds_count[' . $index . ']'), ' class="input-huge"') ?>
                                            <?= form_error('beds_count[' . $index . ']', '<div class="bottom" style="color:red" >', '</div>'); ?>
                                        </td> 
                                        <td>
                                            <?= form_input('date_from[' . $index . ']', set_value('date_from[' . $index . ']'), ' class="input-huge  date"') ?>
                                            <?= form_error('date_from[' . $index . ']', '<div class="bottom" style="color:red" >', '</div>'); ?>
                                        </td> 
                                        <td>
                                            <?= form_input('date_to[' . $index . ']', set_value('date_to[' . $index . ']'), ' class="input-huge date"') ?>
                                            <?= form_error('date_to[' . $index . ']', '<div class="bottom" style="color:red" >', '</div>'); ?>
                                        </td> 
                                        <td class="TAC"><a href="javascript:void(0)" class="remove_hotelroom" ><span class="icon-trash"></span></a></td>
                                    </tr>
                                <? endforeach; ?>
                            <? else: ?>
                                <? if ($rooms_info && count($rooms_info) > 0): ?>
                                    <? foreach ($rooms_info as $index => $item): ?>
                                        <tr>
                                            <td>
                                                <?= form_dropdown('rooms_floor[' . $index . ']', ddgen('erp_floors'), set_value('rooms_floor[' . $index . ']', $item->erp_hotel_floor_id), ' class="input-huge" ') ?>
                                                <?= form_error('rooms_floor[' . $index . ']', '<div class="bottom" style="color:red" >', '</div>'); ?>
                                            </td>
                                            <td>
                                                <?= form_input('rooms_section[' . $index . ']', set_value('rooms_section[' . $index . ']', $item->section), ' class="input-huge" ') ?>
                                                <?= form_error('rooms_section[' . $index . ']', '<div class="bottom" style="color:red" >', '</div>'); ?>
                                            </td>
                                            <td>
                                                <?= form_input('rooms_count[' . $index . ']', set_value('rooms_count[' . $index . ']', $item->rooms_count), ' class="input-huge"') ?>
                                                <?= form_error('rooms_count[' . $index . ']', '<div class="bottom" style="color:red" >', '</div>'); ?>
                                            </td>
                                            <td>
                                                <?= form_input('beds_count[' . $index . ']', set_value('beds_count[' . $index . ']', $item->beds_count), ' class="input-huge"') ?>
                                                <?= form_error('beds_count[' . $index . ']', '<div class="bottom" style="color:red" >', '</div>'); ?>
                                            </td> 
                                            <td>
                                                <?= form_input('date_from[' . $index . ']', set_value('date_from[' . $index . ']', $item->available_from), ' class="input-huge  date"') ?>
                                                <?= form_error('date_from[' . $index . ']', '<div class="bottom" style="color:red" >', '</div>'); ?>
                                            </td> 
                                            <td>
                                                <?= form_input('date_to[' . $index . ']', set_value('date_to[' . $index . ']', $item->available_to), ' class="input-huge date"') ?>
                                                <?= form_error('date_to[' . $index . ']', '<div class="bottom" style="color:red" >', '</div>'); ?>
                                            </td> 
                                            <td class="TAC"><a href="javascript:void(0)" class="remove_hotelroom in_db" ><span class="icon-trash"></span></a></td>
                                        </tr>
                                    <? endforeach; ?>
                                <? else: ?>
                                    <tr>
                                        <td>
                                            <?= form_dropdown('rooms_floor[0]', ddgen('erp_floors'), set_value('rooms_floor[0]'), ' class="input-huge" ') ?>
                                            <?= form_error('rooms_floor[0]', '<div class="bottom" style="color:red" >', '</div>'); ?>
                                        </td>
                                        <td>
                                            <?= form_input('rooms_section[0]', set_value('rooms_section[0]'), ' class="input-huge" ') ?>
                                            <?= form_error('rooms_section[0]', '<div class="bottom" style="color:red" >', '</div>'); ?>
                                        </td>
                                        <td>
                                            <?= form_input('rooms_count[0]', set_value('rooms_count[0]'), ' class="input-huge"') ?>
                                            <?= form_error('rooms_count[0]', '<div class="bottom" style="color:red" >', '</div>'); ?>
                                        </td>
                                        <td>
                                            <?= form_input('beds_count[0]', set_value('beds_count[0]'), ' class="input-huge"') ?>
                                            <?= form_error('beds_count[0]', '<div class="bottom" style="color:red" >', '</div>'); ?>
                                        </td> 
                                        <td>
                                            <?= form_input('date_from[0]', set_value('date_from[0]'), ' class="input-huge  date"') ?>
                                            <?= form_error('date_from[0]', '<div class="bottom" style="color:red" >', '</div>'); ?>
                                        </td> 
                                        <td>
                                            <?= form_input('date_to[0]', set_value('date_to[0]'), ' class="input-huge date"') ?>
                                            <?= form_error('date_to[0]', '<div class="bottom" style="color:red" >', '</div>'); ?>
                                        </td> 
                                        <td class="TAC"><a href="javascript:void(0)" class="remove_hotelroom" ><span class="icon-trash"></span></a></td>
                                    </tr>
                                <? endif; ?>
                            <? endif; ?>        
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="inner-widget" id="hotel_policy_info" >
            <div class="widget-header">
                <div class="widget-header-icon Fright">
                    <span class="icos-pencil2"></span>
                </div>
                <div class="widget-header-title Fright">
                    <?= lang('Company_hotel_policy') ?>
                </div>
                <a class="btn Fleft" id="add_hotelpolicy" href="javascript:void(0)"><?= lang('global_add') ?></a>
            </div>
            <div class="widget-container">
                <div class="row-form">
                    <table cellpadding="0" cellspacing="0" class="table-container">
                        <thead>
                            <tr>
                                <th width="10%"><?= lang('Policy') ?></th>
                                <th width="30%"><?= lang('policy_fieldtype') ?></th>
                                <th width="50%"><?= lang('policy_description') ?></th>
                                <th width="10%"><?= lang('global_operations') ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <? if ($this->input->post('hotels_policy')): ?>
                                <? foreach ($this->input->post('hotels_policy') as $index => $value): ?>
                                    <tr>
                                        <td>
                                            <?= form_dropdown('hotels_policy[' . $index . ']', ddgen('erp_hotels_policies'), set_value('hotels_policy[' . $index . ']'), "class='input-full'") ?>
                                        </td>
                                        <td>
                                            <textarea name="policy_fieldtype[<?= $index ?>]"  class="input-full"  ><?= set_value('policy_fieldtype[' . $index . ']') ?></textarea>
                                        </td>
                                        <td>
                                            <div class="span12">
                                                <textarea name="policy_description[<?= $index ?>]"  class="input-full"  ><?= set_value('policy_description[' . $index . ']') ?></textarea>
                                            </div>
                                        </td>
                                        <td class="TAC"><a class="remove_hotelpolicy" href="javascript:void(0)"><span class="icon-trash"></span></a></td>
                                    </tr>  
                                <? endforeach; ?>
                            <? else: ?>
                                <? if (isset($policies_info) && count($policies_info) > 0): ?>
                                    <? foreach ($policies_info as $index => $item): ?>
                                        <tr>
                                            <td>
                                                <?= form_dropdown('hotels_policy[' . $index . ']', ddgen('erp_hotels_policies'), set_value('hotels_policy[' . $index . ']', $item->erp_hotels_policies_id), "class='input-full'") ?>
                                            </td>
                                            <td>
                                                <textarea name="policy_fieldtype[<?= $index ?>]"  class="input-full"  ><?= set_value('policy_fieldtype[' . $index . ']', $item->policy_field_type) ?></textarea>
                                            </td>
                                            <td>
                                                <div class="span12">
                                                    <textarea name="policy_description[<?= $index ?>]"  class="input-full"  ><?= set_value('policy_description[' . $index . ']', $item->policy_description) ?></textarea>
                                                </div>
                                            </td>
                                            <td class="TAC"><a class="remove_hotelpolicy in_db" href="javascript:void(0)"><span class="icon-trash"></span></a></td>
                                        </tr>
                                    <? endforeach; ?>
                                <? else: ?>
                                    <tr>
                                        <td>
                                            <?= form_dropdown('hotels_policy[0]', ddgen('erp_hotels_policies'), set_value('hotels_policy[0]'), "class='input-full'") ?>
                                        </td>
                                        <td>
                                            <textarea name="policy_fieldtype[0]"  class="input-full"  ><?= set_value('policy_fieldtype[0]') ?></textarea>
                                        </td>
                                        <td>
                                            <div class="span12">
                                                <textarea name="policy_description[0]"  class="input-full"  ><?= set_value('policy_description[0]') ?></textarea>
                                            </div>
                                        </td>
                                        <td class="TAC"><a class="remove_hotelpolicy" href="javascript:void(0)"><span class="icon-trash"></span></a></td>
                                    </tr>
                                <? endif; ?>
                            <? endif; ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <div align="center" >
                <input type='submit'  value="<?= lang('global_submit') ?>" class="btn" style="width:50px;height:50px"   />
            </div>
        </div>

        <?= form_close() ?>
    </div>
</div>
<script>
    /*adding official worker*/
    $(document).ready(function() {
        var num_rows = $("#official_work_info .table-container tbody tr").length;
        $("#official_work_info #add_official").click(function() {
            var num_rows = $("#official_work_info .table-container tbody tr").length;
            if (num_rows < 10) {
                var last_row = $("#official_work_info .table-container tbody  tr").last();
                var info_row_controls = $(last_row).html();
                $("#official_work_info .table-container tbody").append("<tr>" + info_row_controls + "</tr>");
                var new_row = $("#official_work_info .table-container tbody  tr").last();
                $(new_row).find('.input-huge').each(function(index, input) {
                    var nam_regx = new RegExp(/[0-9]/);
                    var number = nam_regx.exec($(input).attr('name'));
                    $(input).attr('name', $(input).attr('name').replace(nam_regx, parseInt(number) + 1));
                    $(input).val(''); /*removing the values*/
                });
                $(new_row).find('.bottom').remove();
                $(new_row).find('.remove_officialwork').click(function() {
                    var num_rows = $("#official_work_info .table-container tbody tr").length;
                    if (num_rows > 1) {
                        $(this).parent().parent().remove();
                    }
                });


            }
        });
        $(".remove_officialwork").click(function() {
            var num_rows = $("#official_work_info .table-container tbody tr").length;
            if (num_rows > 1) {
                if ($(this).hasClass('in_db')) {
                    var del = confirm('<?= lang("global_are_you_sure_you_want_to_delete") ?>');
                    if (del)
                        $(this).parent().parent().remove();
                }
            }
        });

    });
</script>
<script>
    /*adding hotels policy*/
    $(document).ready(function() {
        var num_rows = $("#hotel_policy_info .table-container tbody tr").length;
        $("#hotel_policy_info #add_hotelpolicy").click(function() {
            var num_rows = $("#hotel_policy_info .table-container tbody tr").length;
            if (num_rows < 10) {
                var last_row = $("#hotel_policy_info .table-container tbody  tr").last();
                var info_row_controls = $(last_row).html();
                $("#hotel_policy_info .table-container tbody").append("<tr>" + info_row_controls + "</tr>");
                var new_row = $("#hotel_policy_info .table-container tbody  tr").last();
                $(new_row).find('.input-full').each(function(index, input) {
                    var nam_regx = new RegExp(/[0-9]/);
                    var number = nam_regx.exec($(input).attr('name'));
                    $(input).attr('name', $(input).attr('name').replace(nam_regx, parseInt(number) + 1));
                    $(input).val(''); /*removing the values*/
                });
                $(new_row).find('.bottom').remove();
                $(new_row).find('.remove_hotelpolicy').click(function() {
                    var num_rows = $("#hotel_policy_info .table-container tbody tr").length;
                    if (num_rows > 1) {
                        $(this).parent().parent().remove();
                    }
                });


            }
        });
        $(".remove_hotelpolicy").click(function() {
            var num_rows = $("#hotel_policy_info .table-container tbody tr").length;
            if (num_rows > 1) {
                if ($(this).hasClass('in_db')) {
                    var del = confirm('<?= lang("global_are_you_sure_you_want_to_delete") ?>');
                    if (del)
                        $(this).parent().parent().remove();
                }
            }
        });

    });
</script>
<script>
    /*the javascript validation  for a form*/
    $(document).ready(function() {
        $("#SignupForm").validationEngine('attach', {
            promptPosition: "topLeft",
            scroll: false,
            binded: true,
            prettySelect: true,
            useSuffix: "_chosen"
        });
    })
</script> 
<script>
    /*adding room hotel*/
    $(document).ready(function() {
        var num_rows = $("#company_rooms_info .table-container tbody tr").length;
        $("#company_rooms_info #add_official").click(function() {
            var num_rows = $("#company_rooms_info .table-container tbody tr").length;
            if (num_rows < 10) {
                var last_row = $("#company_rooms_info .table-container tbody  tr").last();
                var info_row_controls = $(last_row).html();
                $("#company_rooms_info .table-container tbody").append("<tr>" + info_row_controls + "</tr>");
                var new_row = $("#company_rooms_info .table-container tbody  tr").last();
                $(new_row).find('.input-huge').each(function(index, input) {
                    var nam_regx = new RegExp(/[0-9]/);
                    var number = nam_regx.exec($(input).attr('name'));
                    $(input).attr('name', $(input).attr('name').replace(nam_regx, parseInt(number) + 1));
                    $(input).val(''); /*removing the values*/
                    /* reint the datepicker*/
                    $(input).removeAttr('id');
                    $(input).removeClass('hasDatepicker');
                });
                $(new_row).find('.date').datepicker({dateFormat: "yy-mm-dd"});
                $(new_row).find('.bottom').remove();
                $(new_row).find('.remove_hotelroom').click(function() {
                    var num_rows = $("#company_rooms_info .table-container tbody tr").length;
                    if (num_rows > 1) {
                        $(this).parent().parent().remove();
                    }
                });


            }
        });
        $(".remove_hotelroom").click(function() {
            var num_rows = $("#company_rooms_info .table-container tbody tr").length;
            if (num_rows > 1) {
                if ($(this).hasClass('in_db')) {
                    var del = confirm('<?= lang("global_are_you_sure_you_want_to_delete") ?>');
                    if (del)
                        $(this).parent().parent().remove();
                }
            }
        });

    });
</script>
<script>
    $(document).ready(function() {
        $('.date').datepicker({
            dateFormat: "yy-mm-dd"
        });
    });
</script>



                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         