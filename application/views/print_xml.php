<? if (isset($data)): ?>
    <? // print_r($data);exit();  ?>
    <div class="row-fluid">
        <div style="margin-bottom:10px; text-align:left; border:3px solid #FFF; background:url(<?= IMAGES ?>/backgrounds/bg_box_head.jpg); padding:5px 10px; height:30px;">

            <div style="float:right; text-align:right; font-size:14px; line-height:30px; color:#333;"> <?= $file_name; ?>  </div>
        </div>
        <div class="widget">
            <div class="head dark">
                <div class="icon"><i class="icos-pencil2"></i></div>
                <h2><?= $file_name ?></h2>
            </div>                
            <div class="block-fluid">
                <table cellpadding="0"  cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <? foreach ($header as $value): ?>
                                <th><?= $value; ?></th>
                            <? endforeach; ?>
                        </tr>
                    </thead>
                    <tbody>
                        <? if (isset($data)): ?>
                            <? foreach ($data as $key => $value): ?>
                                <tr>
                                    <? foreach ($value as $a => $s) { ?>
                                        <td>
                                            <? if ($s): ?>
                                                <?= $s ?>
                                            <? endif; ?>
                                        </td>
                                    <? } ?>
                                </tr>
                            <? endforeach; ?>

                        <? endif; ?>
                    </tbody>
                </table>
                <div class="toolbar bottom TAC">
                    <input  type ="button" value="<?= lang('global_back') ?>"class="btn btn-primary" onclick="window.location = '<?= site_url('read_xml/index') ?>'">
                </div>
            </div>
        </div>

    </div>
<? endif; ?>
<? // print_r($file);exit();?>
<? if (isset($file)): ?>
    <div class="row-fluid">
        <div style="margin-bottom:10px; text-align:left; border:3px solid #FFF; background:url(<?= IMAGES ?>/backgrounds/bg_box_head.jpg); padding:5px 10px; height:30px;">

            <div style="float:right; text-align:right; font-size:14px; line-height:30px; color:#333;"> <?= $file_name; ?>  </div>
        </div>
        <div class="widget">
            <div class="head dark">
                <div class="icon"><i class="icos-pencil2"></i></div>
                <h2><?= $file_name ?></h2>
            </div>                
            <div class="block-fluid">
                <table cellpadding="0"   cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <? foreach ($header as $value): ?>
                                <th><?= $value; ?></th>
                            <? endforeach; ?>
                        </tr>
                    </thead>
                    <tbody>
                        <? if (isset($file)): ?>
                            <? foreach ($file as $num => $line): ?>

                                <tr> 
                                    <? foreach ($line as $key => $value) { ?>

                                        <td><?= $value ?></td>
                                        <?
                                    }

                                endforeach;
                                ?>
                            </tr>
                        <? endif; ?>
                    </tbody>
                </table>
                <div class="toolbar bottom TAC">
                    <input  type ="button" value="<?= lang('global_back') ?>"class="btn btn-primary" onclick="window.location = '<?= site_url('read_xml/index') ?>'">
                </div>
            </div>
        </div>
    </div>
<? endif; ?>




