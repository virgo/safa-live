<?= form_open(false, 'method="get"') ?>
<script>.widget{width:98%;}</script>

<div class="row-fluid">
    <div class="widget">
        <div class="widget-header">
            <div class="widget-header-icon Fright">
                <span class="icos-pencil2"></span>
            </div>
            <div class="widget-header-title Fright">
                <?= lang('global_search') ?>
            </div>
        </div>
        <div class="widget-container">
            
            <div class="row-fluid">
                <div class="span12">
                    <div class="span4">
                        <?= lang($module.'_erp_country_id') ?>
                    </div>
                    <div class="span8">
                        <?= form_dropdown('erp_country_id', ddgen('erp_countries', array('erp_country_id', name())), set_value('erp_country_id')) ?>
                    </div>
                </div>
            </div>
            
            <div class="row-fluid">
                <div class="span12">
                    <div class="span4">
                        <?= lang($module.'_name') ?>
                    </div>
                    <div class="span8">
                        <?= form_input('name', set_value('name')) ?>
                    </div>
                </div>
            </div>
            
            <div class="row-fluid">
                <div class="span12">
                    <div class="span4">
                        <?= lang($module.'_amount') ?>
                    </div>
                    <div class="span8">
                        <?= form_input('amount', set_value('amount')) ?>
                    </div>
                </div>
            </div>
                
            <div class="row-fluid">
                <div class="span12">
                    <div class="span4">
                        <?= lang($module.'_erp_currency_id') ?>
                    </div>
                    <div class="span8">
                        <?= form_dropdown('erp_currency_id', ddgen('erp_currencies', array('erp_currency_id', name())), set_value('erp_currency_id')) ?>
                    </div>
                </div>
            </div>
                
            
            <div class="row-fluid">
                <div class="span12">
                    <div class="span4">
                        <?= lang($module.'_credit') ?>
                    </div>
                    <div class="span8">
                        <?= form_input('credit', set_value('credit')) ?>
                    </div>
                </div>
            </div>
                
            
            <div class="row-fluid">
                <div class="span12">
                    <div class="span4">
                        <?= lang($module.'_additional_credit') ?>
                    </div>
                    <div class="span8">
                        <?= form_input('additional_credit', set_value('additional_credit')) ?>
                    </div>
                </div>
            </div>
            <div class="row-fluid">
                <div class="span12">
                    <input type="submit" name="search" value="<?= lang('global_submit') ?>" class="btn" />
                </div>
            </div>   
        </div>
    </div>
</div><?= form_close() ?>