<script>.widget{width:98%;}</script>

<span class="span8"></span>
<div class="row-fluid">
        
<?= form_open_multipart(false, 'id="validate"') ?>
     
    <div class="widget">
        <div class="path-container Fright">
            <div class="path-name Fright">
                <a href="<?= site_url('admin/dashboard')?>"><?= lang('global_system_management') ?></a>
            </div>
            <div class="path-arrow Fright"></div>
            <div class="path-name Fright">
                <?= lang('global_'.$module)?>
            </div>
        </div>
    </div>
    
    <div class="widget">
        <div class="widget-header">
            <div class="widget-header-icon Fright"><span class="icos-pencil2"></span></div>
            <div class="widget-header-title Fright"><?= lang('global_'.$module)?></div>
    	</div>
        <div class="block-fluid">
            <? if (validation_errors()): ?>
                <?= validation_errors(); ?>
            <? endif ?> 
            
            <div class="row-form">
                <div class="span6">
                    <div class="span4">
                        <?= lang($module.'_name') ?>:
                    </div>
                    <div class="span8">
                        <?= form_input('name', set_value("name", $item->name), 'class="validate[required]"') ?>
                    </div>
                </div>
                
                <div class="span6">
                    <div class="span4">
                        <?= lang($module.'_erp_country_id') ?>:
                    </div>
                    <div class="span8">
                        <?= form_dropdown('erp_country_id', ddgen('erp_countries', array('erp_country_id',name())), set_value("erp_country_id", $item->erp_country_id), 'class="validate[required]"') ?>
                    </div>
                </div>
            </div>
            
            <div class="row-form">
                <div class="span6">
                    <div class="span4">
                        <?= lang($module.'_amount') ?>:
                    </div>
                    <div class="span8">
                        <?= form_input('amount', set_value("amount", $item->amount), 'class="validate[required]"') ?>
                    </div>
                </div>
                
                <div class="span6">
                    <div class="span4">
                        <?= lang($module.'_erp_currency_id') ?>:
                    </div>
                    <div class="span8">
                        <?= form_dropdown('erp_currency_id', ddgen('erp_currencies', array('erp_currency_id',name())), set_value("erp_currency_id", $item->erp_currency_id), 'class="validate[required]"') ?>
                    </div>
                </div>
            </div>
            
            <div class="row-form">
                <div class="span6">
                    <div class="span4">
                        <?= lang($module.'_credit') ?>:
                    </div>
                    <div class="span8">
                        <?= form_input('credit', set_value("credit", $item->credit), 'class="validate[required]"') ?>
                    </div>
                </div>
                <div class="span6">
                    <div class="span4">
                        <?= lang($module.'_additional_credit') ?>:
                    </div>
                    <div class="span8">
                        <?= form_input('additional_credit', set_value("additional_credit", $item->additional_credit), 'class="validate[required]"') ?>
                    </div>
                </div>
                
            </div>
        </div>
    </div>
    
    <div class="toolbar bottom TAC">
        <input type="submit" name="submit" value="<?= lang('global_submit') ?>" class="btn btn-primary"> &nbsp;
        <input type ="button" 
               value="<?= lang('global_back') ?>"
               class="btn btn-primary" 
               onclick="window.location = '<?= site_url($module) ?>'">
       
    </div>
<?= form_close() ?>       
</div> 
