<script>.widget{width:98%;}</script>

<div class="span12">
    <div class="widget">
        <a class="btn Fleft" href="<?= site_url($module.'/manage') ?>"><?= lang('global_add_new_record') ?></a>
        <div class="path-container Fright">
            <div class="path-name Fright">
                <a href=""><?= lang('global_system_management') ?></a>
            </div>
            <div class="path-arrow Fright"></div>
            <div class="path-name Fright">
                <?= lang('global_'.$module)?>
            </div>
        </div>
    </div>
</div>

<? $this->load->view($module.'/search') ?>

<div class="row-fluid">
    <div class="widget">
        <div class="widget-header">
            <div class="widget-header-icon Fright">
                <span class="icos-pencil2"></span>
            </div>
            <div class="widget-header-title Fright">
                <?= lang('global_'.$module)?>
            </div>
        </div>
        <div class="widget-container">
            <div class='table-responsive' >
                <table cellpadding="0" class="" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th><?= lang($module.'_name') ?></th>
                            <th><?= lang($module.'_amount') ?></th>
                            <th><?= lang($module.'_erp_currency_id') ?></th>
                            <th><?= lang($module.'_credit') ?></th>
                            <th><?= lang($module.'_additional_credit') ?></th>
                            <th><?= lang($module.'_erp_country_id') ?></th>
                            <th><?= lang('global_actions') ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <? if (ensure($items)): ?>
                            <? foreach ($items as $item): ?>
                                <tr>
                                    <td><?= $item->name ?></td>
                                    <td><?= $item->amount ?></td>
                                    <td><?= $item->currency ?></td>
                                    <td><?= $item->credit ?></td>
                                    <td><?= $item->additional_credit ?></td>
                                    <td><?= $item->country ?></td>
                                    <td class="TAC">
                                        <a href="<?= site_url($module."/manage/" . $item->{$table_pk}) ?>" ><span class="icon-pencil"></span></a>      
                                        <? if( ! $item->delete): ?>
                                        <a onclick="return  confirm('<?= lang('global_are_you_sure_you_want_to_delete') ?>')" href="<?= site_url($module."/delete/" . $item->{$table_pk}) ?>"><span class="icon-trash"></span></a>
                                        <? endif ?>
                                    </td>
                                </tr>
                            <? endforeach ?>
                        <? endif ?>
                    </tbody>
                </table>
                <?= $pagination ?>
            </div>
        </div>
    </div>
</div>



