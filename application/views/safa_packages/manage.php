<?php
$safa_package_id = '';
$package_code = '';
$date_type = '';
$start_date = '';
$end_date = '';
$name_ar = '';
$name_la = '';
$sale_currency_id = '1';
$local_currency_id = '3';
$service_cost = 0;
$round_num = 1;
$convert = 1;
$transport_type_id = '';
$remarks = '';
$active = '';

$screen_title = lang('add_title');

if (isset($item)) {
    $package_code = $item->package_code;
    $date_type = $item->date_type;
    $start_date = $item->start_date;
    $end_date = $item->end_date;
    $name_ar = $item->name_ar;
    $name_la = $item->name_la;
    $sale_currency_id = $item->sale_currency_id;
    $local_currency_id = $item->local_currency_id;
    $service_cost = $item->service_cost;
    $round_num = $item->round_num;
    $convert = $item->convert;
    $transport_type_id = $item->safa_packages_transportation_type_id;
    $remarks = $item->remarks;

    if ($item->active) {
        $active = "checked='checked'";
    }

    $screen_title = lang('edit_title');
}
?>


<style>
    .wizerd_div {
        border-bottom: medium none !important;
        margin: -18px 0 20px;
        padding-top: 0;
    }

    .wizerd_div a {
        background: none repeat scroll 0 0 #FAFAFA;
        border: 1px solid #D5D6D6;
        border-radius: 49px;
        color: #663300;
        display: inline-block;
        margin: -6px 5px -17px -8px;
        padding: 10px 12px 8px;
    }

    a {
        color: #C09853;
    }

    .resalt-group {
        margin: 18px 0.5% 0.5%;
        padding: 0.5%;
        width: 99%;
    }

    th a.btn,th input[type="button"],th input[type="submit"],th button {
        margin: 0;
        padding: 4px 12px;
    }

    .coll_close,.coll_open {
        margin-top: 0;
    }

    .chosen-container {
        margin-top: 4px;
    }

    /*By Gouda, TO solve problem of dropdown in table*/
    .table-warp {
        overflow-x: visible;
        overflow-y: visible;
    }
</style>


<!-- For Dates -->
<link
    href="<?= base_url("static/js/jquery.calendars/humanity.calendars.picker.css") ?>"
    type="text/css" rel="stylesheet">
<script
src="<?= base_url("static/js/jquery.calendars/jquery.calendars.js") ?>"></script>
<script
src="<?= base_url("static/js/jquery.calendars/jquery.calendars-ar.js") ?>"></script>
<script
src="<?= base_url("static/js/jquery.calendars/jquery.calendars.plus.js") ?>"></script>
<script
src="<?= base_url("static/js/jquery.calendars/jquery.calendars.picker.js") ?>"></script>
<script
src="<?= base_url("static/js/jquery.calendars/jquery.calendars.picker-ar.js") ?>"></script>
<script
src="<?= base_url("static/js/jquery.calendars/jquery.calendars.islamic.js") ?>"></script>
<script
src="<?= base_url("static/js/jquery.calendars/jquery.calendars.islamic-ar.js") ?>"></script>
<script
src="<?= NEW_JS ?>/plugins/jquery.fixedheadertable.js"></script>
<!-- By Gouda, To use CKEditor -->
<script
    type='text/javascript'
src='<?= NEW_TEMPLATE ?>/plugins/ckeditor/ckeditor.js'></script>
<!-- 
<script type='text/javascript' src='<?= NEW_TEMPLATE ?>/plugins/youtube-ckeditor/plugin.js'></script>
-->
<div class="widget">
    <div class="path-container Fright">
        <div class="path-name Fright"><a href=""><?= lang('global_system_management') ?></a>
        </div>
        <div class="path-arrow Fright"></div>
        <div class="path-name Fright"><a
                href="<?php echo site_url('safa_packages') ?>"><?php echo lang('title') ?></a></div>

        <div class="path-arrow Fright"></div>
        <div class="path-name Fright"><?php echo $screen_title; ?></div>
    </div>
</div>


<?php echo form_open(false, 'id="frm_packages_manage" ') ?>

<div class="widget">
    <div class="widget-header">
        <div class="widget-header-icon Fright"></div>
        <div class="widget-header-title"><?php echo $screen_title; ?></div>
    </div>
    <div class="widget-container packages">
        <div class="resalt-group">
            <div class="wizerd_div"><a><?php echo lang('main_data'); ?></a></div>
            <div class="table-warp">
                <table class="">
                    <thead>
                        <tr>
                            <th><?php echo lang('package_code'); ?></th>
                            <th><?php echo lang('package_name'); ?></th>
                            <th><?php echo lang('currency_transport'); ?></th>
                            <th colspan="2"><?php echo lang('period'); ?></th>


                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="span2"><font style='color: red'>*</font> <?php echo form_input('package_code', set_value("package_code", $package_code), " style='' id='package_code' class='validate[required] input-full' ") ?>
                            </td>
                            <td>
                                <div class="span12">
                                    <div class="span12"><label><?php echo lang('name_ar'); ?></label> <font
                                            style='color: red'>*</font></div>
                                    <span class="span12"> <?php echo form_input('name_ar', set_value("name_ar", $name_ar), " style='' id='name_ar' class='validate[required] span10' ") ?>
                                    </span></div>
                                <div class="span12">
                                    <div class="span12"><label><?php echo lang('name_la'); ?></label> <font
                                            style='color: red'>*</font></div>
                                    <div class="span12"><?php echo form_input('name_la', set_value("name_la", $name_la), " style='' id='name_la' class='validate[required] span10' ") ?>
                                    </div>
                                </div>
                            </td>
                            <td>

                                <div class="span12">
                                    <div class="span12"><label><?php echo lang('currency'); ?></label></div>
                                    <span class="span12"> <?php echo form_dropdown('sale_currency_id', $erp_currencies, set_value('sale_currency_id', $sale_currency_id), 'class="chosen-select chosen-rtl input-full"  tabindex="4" id="sale_currency_id" ') ?>
                                    </span></div>
                                <div class="span12">
                                    <div class="span12"><label><?php echo lang('transport_type_id'); ?></label></div>
                                    <div class="span12"><?php echo form_dropdown('safa_packages_transportation_type_id', $erp_transportertypes, set_value('safa_packages_transportation_type_id', $transport_type_id), 'class="chosen-select chosen-rtl input-full"  tabindex="4" id="transport_type_id" ') ?>

                                    </div>
                                </div>


                            </td>
                            <td>
                                <div class="span12"><label class="span3"><?php echo lang('date_type'); ?></label>

                                    <?php
                                    $islamic_checked = "";
                                    $gregorian_checked = "";
                                    if ($date_type == 'islamic') {
                                        $islamic_checked = "checked='checked'";
                                        echo <<<JVDATE
<script type='text/javascript'>
$(function(){
     $('.date').calendarsPicker({calendar: $.calendars.instance('islamic', 'ar'), onSelect: customRange, dateFormat: 'yyyy-mm-dd'}); 
   });
</script>
JVDATE;
                                        $start_date = $this->hijrigregorianconvert->GregorianToHijri($start_date, 'YYYY-MM-DD');
                                        $end_date = $this->hijrigregorianconvert->GregorianToHijri($end_date, 'YYYY-MM-DD');
                                    } else {
                                        echo <<<JVDATE
<script type='text/javascript'>
$(function(){
     $('.date').calendarsPicker({calendar: $.calendars.instance('gregorian', 'en'), onSelect: customRange, dateFormat: 'yyyy-mm-dd'}); 
   });
</script>
JVDATE;
                                        $gregorian_checked = "checked='checked'";
                                    }
                                    ?> <label class="span3"><?php echo lang('islamic'); ?><input
                                            onclick="$('.date').calendarsPicker('destroy').val('').calendarsPicker({calendar: $.calendars.instance('islamic', 'ar'), dateFormat: 'yyyy-mm-dd'});"
                                            class="pull-right" style="height: 25px; margin-top: 0px;"
                                            name="date_type" type="radio" value="islamic"
                                            <?php echo $islamic_checked; ?> /></label> <label class="span3"><?php echo lang('gregorian'); ?><input
                                            onclick="$('.date').calendarsPicker('destroy').val('').calendarsPicker({calendar: $.calendars.instance('gregorian', 'en'), dateFormat: 'yyyy-mm-dd'});"
                                            class="pull-right" name="date_type" type="radio" value="gregorian"
                                            <?php echo $gregorian_checked; ?> /></label></div>

                                <div class="span3"><label><?php echo lang('start_date'); ?></label></div>
                                <div class="span8"><?php echo form_input('start_date', set_value("start_date", $start_date), " style='' id='start_date' class='input-full date' ") ?>

                                </div>
                                <div class="span3"><label><?php echo lang('end_date'); ?></label></div>
                                <div class="span8"><?php echo form_input('end_date', set_value("end_date", $end_date), " style='' id='end_date' class='input-full date' ") ?>

                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>

            </div>
            <div class="span5" style="margin-top: 10px;"><label class="span2"
                                                                style="margin-top: 10px;"><input type="checkbox" name="active"
                                                 id="active" <?php echo $active; ?>></input><?php echo lang('active'); ?></label>
                                         <!--                <div class="span3" ><a class="btn"><?php echo lang('available_for'); ?></a> </div>-->
            </div>

        </div>



        <?php $this->load->view('safa_packages/hotels'); ?> 
        <?php $this->load->view('safa_packages/hotels_prices'); ?>
        <?php $this->load->view('safa_packages/execlusive_nights_prices'); ?>
        <?php $this->load->view('safa_packages/tourism_places'); ?>
        <?php $this->load->view('safa_packages/execlusive_meals_prices'); ?>

        <div class="span11 widget-container">
            <div class="span2" style="margin-top: 55px;"><?php echo lang('notes_and_conditions'); ?></div>
            <div class="span10" style="margin-top: 10px;"><?php echo form_textarea('remarks', set_value("remarks", $remarks), " id='remarks' class='input-huge ckeditor' style='width: 100%;' ") ?>
            </div>
        </div>
        <div class="span12 TAC"><input type="submit" class="btn" name="smt_save"
                                       value="<?php echo lang('global_submit') ?>"></div>
    </div>
</div>
<?php echo form_close() ?>

<script type="text/javascript">
    $(document).ready(function() {
        var config = {
            '.chosen-select': {width: "100%"},
            '.chosen-select-deselect': {allow_single_deselect: true},
            '.chosen-select-no-single': {disable_search_threshold: 10},
            '.chosen-select-no-results': {no_results_text: 'Oops, nothing found!'},
            '.chosen-select-width': {width: "95%"}
        }
        for (var selector in config) {
            $(selector).chosen(config[selector]);
        }

        // binds form submission and fields to the validation engine
        $("#frm_packages_manage").validationEngine({
            prettySelect: true,
            useSuffix: "_chosen",
            promptPosition: "topRight:-150"
                    //promptPosition : "bottomLeft"
        });
    });
</script>

<script type="text/javascript">
    isObject = function(a) {
        return (!!a) && (a.constructor === Object);
    };
    $(function() {
        function customRange(dates) {
            if (this.id == 'datefrom') {
                $('input[name=end_date]').calendarsPicker('option', 'minDate', dates[0] || null);
            } else {
                $('input[name=start_date]').calendarsPicker('option', 'maxDate', dates[0] || null);
            }
        }

        $('.packages').on('change', '.hotels_id', function() {
            $('.chosen_hotels option').remove();
            $('.chosen_hotels').append('<option value=""></option>');
            $('.hotels_id').each(function() {
                $('.chosen_hotels').append('<option value="' + $(this).val() + '">' + $(this).find('option[value=' + $(this).val() + ']').text() + '</option>');
            });
            $('.chosen_hotels').trigger("chosen:updated");
        });
    });
</script>



<script type="text/javascript">
    function get_total_price_for_package(erp_package_period_id, counter)
    {

        var dataString = 'erp_package_period_id=' + erp_package_period_id;
        $.ajax
                ({
                    type: 'POST',
                    url: '<?php echo base_url() . 'safa_packages/get_days_by_package_period_ajax'; ?>',
                    data: dataString,
                    async: false,
                    cache: false,
                    success: function(data)
                    {
                        var data_arr = JSON.parse(data);
                        data_arr = $.map(data_arr, function(value, key) {
                            return key + '-' + value;
                        });
                        $('tr[rel=' + counter + '] input[type=hidden]').val('0');
                        var hotels_inputs = $('#hotels').find('select');
                        var hotels_inputs_length = $('#hotels').find('select').length;

                        for (var i = 0; i < data_arr.length; i++) {
                            var city_days_str = data_arr[i];
                            var city_days_arr = city_days_str.split('-');

                            var city_id = city_days_arr[0];
                            var city_days = city_days_arr[1];

                            for (var h = 0; h < hotels_inputs_length; h++) {
                                if (hotels_inputs[h].id.startsWith('hotels_erp_city_id')) {
                                    if (hotels_inputs[h].value == city_id) {
                                        var suffix = hotels_inputs[h].id.substr(19);

<?php
if (isset($erp_hotelroomsizes)) {
    if (check_array($erp_hotelroomsizes)) {

        foreach ($erp_hotelroomsizes as $erp_hotelroomsize) {

            $erp_hotelroomsize_id = $erp_hotelroomsize->erp_hotelroomsize_id;
            ?>

                                                    total_price = parseInt($('#hotels_day_price_' + suffix + '_<?php echo $erp_hotelroomsize_id; ?>').val()) * parseInt(city_days);
                                                    var newvalue = 0;
                                                    //hotels_erp_city_id_
                                                    var previous_value = parseInt($('#hotels_prices_price_' + counter + '_<?php echo $erp_hotelroomsize_id; ?>').val());
                                                    var newvalue = previous_value + total_price;
                                                    $('input[rpnrel=<?php echo $erp_hotelroomsize_id; ?>]').each(function() {
                                                        if (parseInt($(this).val()) === 0 || isNaN(parseInt($(this).val())))
                                                            newvalue = 0;
                                                    });
                                                    $('#hotels_prices_price_' + counter + '_<?php echo $erp_hotelroomsize_id; ?>').val(newvalue);
            <?php
        }
    }
}
?>


                                    }

                                }
                            }
                        }





                    }
                });
    }

    function add_fees() {
        var service = parseInt($('input[name=service_cost]').val());
        var round_num = parseInt($('input[name=round_num]').val());
        var convert = parseFloat($('input[name=convert]').val());

        $('input[rprel]').each(function() {
            var inputval = parseInt($(this).val());
            var newval = service + inputval;
            if (convert)
                newval = newval / convert;
            if (round_num === 0 || isNaN(round_num))
                round_num = 1;

            var roundval = Math.round(newval / round_num);
            newval = roundval * round_num;


            if (parseInt($(this).val()) === 0 || isNaN(parseInt($(this).val())))
                newval = 0;

            if (isNaN(newval))
                newval = 0;
            $(this).val(newval);
        });
    }
    
        $("#frm_packages_manage").validationEngine({
        prettySelect: true,
        useSuffix: "_chosen",
        promptPosition: "topRight:-150"
//promptPosition : "bottomLeft"
    });
</script>
