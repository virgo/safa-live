<?php

$local_currency_id = '3';
$service_cost = 0;
$round_num = 1;
$convert = 1;


if (isset($item)) {

    $local_currency_id = $item->local_currency_id;
    $service_cost = $item->service_cost;
    $round_num = $item->round_num;
    $convert = $item->convert;

}
?>
<script>hotels_prices_counter = 0</script>
<div class="resalt-group">
    <div class="wizerd-div"><a><?php echo lang('hotels_prices'); ?></a></div>
    <div class="table-warp">
        <table>
            <thead>
                <tr>
                    <th  class="span2"><?php echo lang('package_period'); ?></th>
                    <?php
                    if (isset($erp_hotelroomsizes)) {
                        if (check_array($erp_hotelroomsizes)) {
                            foreach ($erp_hotelroomsizes as $erp_hotelroomsize) {
                                echo '<th>' . $erp_hotelroomsize->{name()} . '</th>';
                            }
                        }
                    }
                    ?>
                    <th>
                        <a class="btn Fleft" title="<?php echo lang('global_add'); ?>" href="javascript:void(0)" onclick="add_hotels_prices('N' + hotels_prices_counter++);
                                        load_multiselect()">
                               <?= lang('global_add') ?>    
                        </a>
                    </th>
                </tr>
            </thead>
            <tbody class='hotels_prices' id='hotels_prices'>
                <? if (isset($item_hotels_prices)) { ?>
                    <? if (check_array($item_hotels_prices)) { ?>
                        <? foreach ($item_hotels_prices as $item_hotels_price) { ?>
                            <tr rel="<?php echo $item_hotels_price->safa_package_periods_id ?>">
                                <td>
                                    <?php
                                    echo form_dropdown('hotels_prices_safa_package_id[' . $item_hotels_price->safa_package_periods_id . ']'
                                            , $package_periods, set_value('hotels_prices_safa_package_id[' . $item_hotels_price->safa_package_periods_id . ']', $item_hotels_price->erp_package_period_id)
                                            , 'class="chosen-select  peroid_prices chosen-rtl input-full" rel="' . $item_hotels_price->safa_package_periods_id . '" id="hotels_prices_safa_package_id[' . $item_hotels_price->safa_package_periods_id . ']"  tabindex="4" onchange="get_total_price_for_package(this.value, ' . $item_hotels_price->safa_package_periods_id . ' )" ');
                                    ?>
                                </td>
                                <?php
                                if (isset($erp_hotelroomsizes)) {
                                    if (check_array($erp_hotelroomsizes)) {

                                        foreach ($erp_hotelroomsizes as $erp_hotelroomsize) {

                                            $erp_hotelroomsize_id = $erp_hotelroomsize->erp_hotelroomsize_id;
                                            ?>
                                            <td>
                                                <input type="number" name='hotels_prices_price[<?= $item_hotels_price->safa_package_periods_id ?>][<?= $erp_hotelroomsize_id ?>]'
                                                       value="<?= set_value('hotels_prices_price[' . $item_hotels_price->safa_package_periods_id . '][' . $erp_hotelroomsize_id . ']', get_roomsize_price($item_hotels_price->safa_package_periods_id, $erp_hotelroomsize_id)) ?>"
                                                       rprel="<?= $erp_hotelroomsize_id ?>" class="input-huge" id="hotels_prices_price_<?= $item_hotels_price->safa_package_periods_id . '_' . $erp_hotelroomsize_id ?>" 
                                                       />
                                            </td>
                                            <?php
                                        }
                                    }
                                }
                                ?>
                                <td class="TAC">
                                    <a href="javascript:void(0)" onclick="delete_hotels_prices(<?php echo $item_hotels_price->safa_package_periods_id ?>, true)"><span class="icon-trash"></span></a>
                                </td>
                            </tr>
                        <? } ?>
                    <? } ?>
                <? } ?>
            </tbody>
        </table>
        <table style="background: #ccc;">
            <tr><th>عملة التكلفه</th>
                <th>قيمة الخدمات </th>
                <th> قيمة التقريب (بعملة المبيع) </th>
                <th>معدل الصرف </th>
                <th rowspan="2"><a  class=" btn calc" href="javascript:void(0)"  >احسب</a></th>
            </tr>
            <tr>
                <td><?php echo form_dropdown('local_currency_id', $erp_currencies, set_value('local_currency_id', isset($local_currency_id)?$local_currency_id:3), 'class="chosen-select chosen-rtl input-full"  tabindex="4" id="sale_currency_id" ') ?></td>
                <td><input name="service_cost" value="<?= isset($service_cost) ? $service_cost:0 ?>" /></td>
                <td><input name="round_num" value="<?= isset($round_num) ? $round_num:'1' ?>" /></td>
                <td><input name="convert"  value="<?= isset($convert) ? $convert:'1' ?>" /></td>
            </tr>
        </table>
    </div>
</div>
<script>
    function add_hotels_prices(id) {
        var text = [];
        $('#dv_hotels :input').each(function() {
            var input_id = $(this).attr("id");
            //alert(input_name);
            if (input_id !== undefined) {
                if (input_id.startsWith('hotels_erp_hotel_id')) {

                    var input_value = $("#" + input_id + " option:selected").val();
                    var input_text = $("#" + input_id + " option:selected").text();


                    text.push(new Array(input_value, input_text));
                }
            }
        })

        var slct_options_erp_hotels = "<option value=''></option>";
        for (var i = 0; i < text.length; i++) {
            slct_options_erp_hotels = slct_options_erp_hotels + "<option value='" + text[i][0] + "'>" + text[i][1] + "</option>";
        }

        var new_row = [
                "<tr rel=\"" + id + "\">",
                "    <td>",
                "       <select name=\"hotels_prices_safa_package_id[" + id + "]\" class=\"chosen-select chosen-rtl peroid_prices input-full validate[required]\" rel=\"" + id + "\"  id=\"hotels_prices_safa_uo_package_id_" + id + "\" tabindex=\"4\"  >",
<? foreach ($package_periods as $key => $value): ?>
            "           <option value=\"<?= $key ?>\"><?= $value ?></option>",
<? endforeach ?>
        "    </td>",
<?php
if (isset($erp_hotelroomsizes)) {
    if (check_array($erp_hotelroomsizes)) {

        foreach ($erp_hotelroomsizes as $erp_hotelroomsize) {

            $erp_hotelroomsize_id = $erp_hotelroomsize->erp_hotelroomsize_id;
            ?>
                    '     <td>',
                            '<input type="number"  class="input-huge" id="hotels_prices_price_' + id + '_<?= $erp_hotelroomsize_id ?>" rprel="<?= $erp_hotelroomsize_id ?>" value="0" name="hotels_prices_price[' + id + '][<?= $erp_hotelroomsize_id ?>]">',
                            "    </td>",
            <?php
        }
    }
}
?>
        "    <td class=\"TAC\">",
                "        <a href=\"javascript:void(0)\" onclick=\"delete_hotels_prices('" + id + "')\"><span class=\"icon-trash\"></span></a>",
                "    </td>",
                "</tr>"
        ].join("\n");
                $('#hotels_prices').append(new_row);
//        get_total_price_for_package($('select[rel='+id+']').val(),id);
    }
    function delete_hotels_prices(id, database) {
        if (typeof database == 'undefined')
        {
            $('.hotels_prices').find('tr[rel="' + id + '"]').remove();
        }
        else
        {
            $('.hotels_prices').find('tr[rel=' + id + ']').remove();
            var hidden_input = '<input type="hidden" name="hotels_prices_remove[]" value="' + id + '" />';
            $('#hotels_prices').append(hidden_input);
        }
    }


    $('.calc').click(function() {
        $('input[rprel]').val('0');
        $('.peroid_prices').each(function() {
            var sendperoid = $(this).attr('rel');
            get_total_price_for_package($(this).val(), sendperoid);
        });
        add_fees();
        return false;
    });

</script>
