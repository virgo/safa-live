<script>hotels_counter = 0</script>

<div class="resalt-group" id="dv_hotels">
    <div class="wizerd-div"><a><?php echo lang('hotels'); ?></a></div>
    <div class="table-warp">
        <table>
            <thead>
                <tr>
                    <th class="span5"><?php echo lang('city'); ?></th>
                    <th class="span5"><?php echo lang('hotel'); ?></th>
                    <th class="span5"><?php echo lang('meal'); ?></th>

                    <th class="span1">
                        <a class="btn Fleft" title="<?php echo lang('global_add'); ?>" href="javascript:void(0)" onclick="add_hotels('N' + hotels_counter++);
                                        load_multiselect()">
                               <?php echo lang('global_add') ?>    
                        </a>
                    </th>
                </tr>
            </thead>
            <tbody class='hotels' id='hotels'>
                <? if (isset($item_hotels)) { ?>
                    <? if (check_array($item_hotels)) { ?>
                        <? foreach ($item_hotels as $item_hotel) { ?>
                            <tr rel="<?php echo $item_hotel->safa_package_hotel_id ?>">

                                <td>
                                    <?php
                                    echo form_dropdown('hotels_erp_city_id[' . $item_hotel->safa_package_hotel_id . ']'
                                            , $erp_cities, set_value('hotels_erp_city_id[' . $item_hotel->safa_package_hotel_id . ']', $item_hotel->erp_city_id)
                                            , 'class="chosen-select chosen-rtl input-full validate[required]" id="hotels_erp_city_id_' . $item_hotel->safa_package_hotel_id . '" onchange="getHotelForHotelsPart(this.value, \'' . $item_hotel->safa_package_hotel_id . '\')" tabindex="4" id="hotels_erp_city_id_' . $item_hotel->safa_package_hotel_id . '" ');
                                    ?>
                                </td>

                                <td>
                                    <?php
                                    echo form_dropdown('hotels_erp_hotel_id[' . $item_hotel->safa_package_hotel_id . ']'
                                            , $erp_hotels, set_value('hotels_erp_hotel_id[' . $item_hotel->safa_package_hotel_id . ']', $item_hotel->erp_hotel_id)
                                            , 'class="chosen-select chosen-rtl input-full validate[required] hotels_id" id="hotels_erp_hotel_id_' . $item_hotel->safa_package_hotel_id . '" tabindex="4" ');
                                    ?>
                                </td>

                                <td>
                                    <?php
                                    echo form_dropdown('hotels_erp_meal_id[' . $item_hotel->safa_package_hotel_id . ']'
                                            , $erp_meals, set_value('hotels_erp_meal_id[' . $item_hotel->safa_package_hotel_id . ']')
                                            , 'class="chosen-select chosen-rtl input-full" id="hotels_erp_meal_id_' . $item_hotel->safa_package_hotel_id . '" tabindex="4" ');
                                    ?>
                                </td>


                                <td class="TAC">
                                    <a href="javascript:void(0)" onclick="delete_hotels(<?php echo $item_hotel->safa_package_hotel_id ?>, true)"><span class="icon-trash"></span></a>
                                </td>
                            </tr>
                            <tr rel="<?php echo $item_hotel->safa_package_hotel_id ?>">
                                <td colspan="4">
                                    <table >
                                        <tr>
                                            <td rowspan="2">
                                                <span class="FRight"><?= lang('package_room_prices') ?></span>
                                            </td>
                                            <? if (check_array($erp_hotelroomsizes)) : ?>
                                                <? foreach ($erp_hotelroomsizes as $erp_hotelroomsize) : ?>
                                                    <td><?= $erp_hotelroomsize->{name()} ?></td>
                                                <? endforeach ?>
                                            <? endif ?>
                                        </tr>
                                        <tr>
                                            <? if (check_array($erp_hotelroomsizes)) : ?>
                                                <? foreach ($erp_hotelroomsizes as $erp_hotelroomsize) : ?>
                                                    <td>
                                                        <?=
                                                        form_input('hotels_day_price[' . $item_hotel->safa_package_hotel_id . '][' . $erp_hotelroomsize->erp_hotelroomsize_id . ']'
                                                                , set_value('hotels_day_price[' . $item_hotel->safa_package_hotel_id . '][' . $erp_hotelroomsize->erp_hotelroomsize_id . ']', get_package_hotel_nights_price($item_hotel->safa_package_hotel_id, $erp_hotelroomsize->erp_hotelroomsize_id))
                                                                , 'class="input-small FRight" style="width: 100px !important;" pkgid="' . $item_hotel->safa_package_hotel_id . '" rpnrel="' . $erp_hotelroomsize->erp_hotelroomsize_id . '" placeholder="' . $erp_hotelroomsize->{name()} . '" id="hotels_day_price_' . $item_hotel->safa_package_hotel_id . '_' . $erp_hotelroomsize->erp_hotelroomsize_id . '" ')
                                                        ?>
                                                    </td>
                                                <? endforeach ?>
                                            <? endif ?>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        <? } ?>
                    <? } ?>
                <? } ?>
            </tbody>
        </table>

    </div>
</div>


<script>
    function load_multiselect() {
        var config = {
            '.chosen-select': {width: "100%"},
            '.chosen-select-deselect': {allow_single_deselect: true},
            '.chosen-select-no-single': {disable_search_threshold: 10},
            '.chosen-select-no-results': {no_results_text: 'Oops, nothing found!'},
            '.chosen-select-width': {width: "95%"}
        }
        for (var selector in config) {
            $(selector).chosen(config[selector]);
        }
    }
    function add_hotels(id) {



        var new_row = [
                "<tr rel=\"" + id + "\">",
                "    <td>",
                "       <select name=\"hotels_erp_city_id[" + id + "]\" class=\"chosen-select chosen-rtl input-full validate[required]\" id=\"hotels_erp_city_id_" + id + "\" onchange=\"getHotelForHotelsPart(this.value, '" + id + "')\" tabindex=\"4\">",
<? foreach ($erp_cities as $key => $value): ?>
            "           <option value=\"<?= $key ?>\"><?= $value ?></option>",
<? endforeach ?>
        "    </td>",
                "    <td>",
                "       <select name=\"hotels_erp_hotel_id[" + id + "]\" class=\"chosen-select chosen-rtl input-full hotels_id validate[required]\" id=\"hotels_erp_hotel_id_" + id + "\" tabindex=\"4\">",
<? foreach ($erp_hotels as $key => $value): ?>
            "           <option value=\"<?= $key ?>\"><?= $value ?></option>",
<? endforeach ?>
        "    </td>",
                "    <td>",
                "       <select name=\"hotels_erp_meal_id[" + id + "]\" class=\"chosen-select chosen-rtl input-full\" id=\"hotels_erp_meal_id_" + id + "\" tabindex=\"4\">",
<? foreach ($erp_meals as $key => $value): ?>
            "           <option value=\"<?= $key ?>\"><?= $value ?></option>",
<? endforeach ?>
        "    </td>",
                "    <td class=\"TAC\">",
                "        <a href=\"javascript:void(0)\" onclick=\"delete_hotels('" + id + "')\"><span class=\"icon-trash\"></span></a>",
                "    </td>",
                "</tr>",
                "<tr rel=\"" + id + "\">",
                '   <td colspan="4"><table >',
                '                       <tr>',
                '                            <td rowspan="2">',
                '                                <span class="FRight"><?= lang('package_room_prices') ?></span>',
                '                           </td>',
<? if (check_array($erp_hotelroomsizes)) : ?>
    <? foreach ($erp_hotelroomsizes as $erp_hotelroomsize) : ?>
                '                            <td><?= $erp_hotelroomsize->{name()} ?></td>',
    <? endforeach ?>
<? endif ?>
        '                        </tr>',
                '                        <tr>',
<? if (check_array($erp_hotelroomsizes)) : ?>
    <? foreach ($erp_hotelroomsizes as $erp_hotelroomsize) : ?>
                '      <td><?=
        form_input('hotels_day_price[\' + id + \'][' . $erp_hotelroomsize->erp_hotelroomsize_id . ']'
                , set_value('hotels_prices_price[\' + id + \'][' . $erp_hotelroomsize->erp_hotelroomsize_id . ']')
                , 'class="input-small FRight" style="width: 100px !important;" pkgid=\' + id + \' rpnrel="' . $erp_hotelroomsize->erp_hotelroomsize_id . '"  id=\"hotels_day_price_\' + id + \'_' . $erp_hotelroomsize->erp_hotelroomsize_id . '\" placeholder="' . $erp_hotelroomsize->{name()} . '"')
        ?></td>',
    <? endforeach ?>
<? endif ?>
        "   </tr></table></td>",
                "</tr>",
        ].join("\n");
                $('#hotels').append(new_row);
        $('.chosen_hotels option').remove();
        $('.chosen_hotels').append('<option value=""></option>');
        $('.hotels_id').each(function() {
            $('.chosen_hotels').append('<option value="' + $(this).val() + '">' + $(this).find('option[value=' + $(this).val() + ']').text() + '</option>');
        });
        $('.chosen_hotels').trigger("chosen:updated");

    }
    function delete_hotels(id, database)
    {
        if (typeof database == 'undefined')
        {
            $('.hotels').find('tr[rel="' + id + '"]').remove();
        }
        else
        {
            $('.hotels').find('tr[rel=' + id + ']').remove();
            var hidden_input = '<input type="hidden" name="hotels_remove[]" value="' + id + '" />';
            $('#hotels').append(hidden_input);
        }

        $('.chosen_hotels option').remove();
        $('.chosen_hotels').append('<option value=""></option>');
        $('.hotels_id').each(function() {
            $('.chosen_hotels').append('<option value="' + $(this).val() + '">' + $(this).find('option[value=' + $(this).val() + ']').text() + '</option>');
        });
        $('.chosen_hotels').trigger("chosen:updated");
    }

</script>

<script type="text/javascript">
    function getHotelForHotelsPart(cityId, counter)
    {
        var strURL = "<?php echo base_url(); ?>/safa_packages/get_hotels_by_city/" + cityId;
        $.get(strURL, function(data) {
            $('#hotels_erp_hotel_id_' + counter).html(data);
            $('#hotels_erp_hotel_id_' + counter).trigger("chosen:updated");
        });

    }
</script>        