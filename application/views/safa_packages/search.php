<style>
    .wizerd-div {
    border-bottom: medium none !important;
    margin: -18px 0 20px;
    padding-top: 0;
}
.wizerd-div a {
    background: none repeat scroll 0 0 #FAFAFA;
    border: 1px solid #D5D6D6;
    border-radius: 49px;
    color: #663300;
    display: inline-block;
    margin: -6px 5px -17px -8px;
    padding: 10px 12px 8px;
}
a {
    color: #C09853;
}
.resalt-group {
    margin: 18px 0.5% 0.5%;
    padding: 0.5%;
    width: 99%;
}
th a.btn, th input[type="button"], th input[type="submit"], th button {
    margin: 0;
    padding: 4px 12px;
}
.coll_close, .coll_open {
    margin-top: 0;
}
.chosen-container {
    margin-top: 4px;
}
.warning {
    color: #C09853;
}
</style>


<div class="widget">
    <div class="path-container Fright">
        <div class="path-name Fright">
            <a href="<?= site_url('uo/dashboard') ?>"><?= lang('global_system_management') ?></a>
        </div>
        <div class="path-arrow Fright"> </div>
        <div class="path-name Fright"><?= lang('menu_packages') ?></div>
    </div>
</div>


<div class="widget">
    <div class="widget-header">
        <div class="widget-header-icon Fright"></div>
        <div class="widget-header-title Fright"><?= lang('menu_packages') ?></div>
        <a class="btn Fleft" href="<?= site_url('safa_packages/manage') ?>"><?= lang('global_add') ?></a>
    </div>
    
    <div class="widget-container">
        <div class="resalt-group">
            <div class="wizerd-div TAC"><a>عناصر البحث</a></div>
            <div class="row-form">
                <div class="span4" style="padding-bottom: 61px;">
                    <div class="span2"><label>العقد</label></div>
                    <div class="span8"><input type="text" class="input-huge"></div>
                </div>
                <div class="span4">
                    <div class="span12">
                        <div class="span6">فندق مكة</div>
                        <div class="span6"><input type="text" class="input-huge"></div>
                    </div>
                    <div class="span12">
                        <div class="span6">مستوى الفندق</div>
                        <div class="span6"><input type="text" class="input-huge"></div>
                    </div>
                    <div class="span12">
                        <div class="span6">المسافة من الحرم</div>
                        <div class="span6"><input type="text" class="input-huge"></div>
                    </div>
                </div>
                <div class="span4">
                    <div class="span12">
                        <div class="span6">فندق المدينة</div>
                        <div class="span6"><input type="text" class="input-huge"></div>
                    </div>
                    <div class="span12">
                        <div class="span6">مستوى الفندق</div>
                        <div class="span6"><input type="text" class="input-huge"></div>
                    </div>
                    <div class="span12">
                        <div class="span6">المسافة من الحرم</div>
                        <div class="span6"><input type="text" class="input-huge"></div>
                    </div>
                </div>
            </div>
         
            <div class="row-form">
                <div class="span2">
                    <div class="span7">نوع دورة النقل</div>
                    <div class="span5"><input type="text" class="input-huge"></div>
                </div>
                <div class="span4">
                    <div class="span2">الفترة</div>
                    <div class="span10">
                        <div class="span6">
                            <div class="span2">من</div>
                            <div class="span9"><input type="text" class="input-huge"></div>
                                
                        </div>
                        <div class="span6">
                            <div class="span2">إلى</div>
                            <div class="span9"><input type="text" class="input-huge"></div>
                                
                        </div>
                    </div>
                </div>
                <div class="span2">
                    <div class="span6">مدة البرنامج</div>
                    <div class="span6"><input type="text" class="input-huge"></div>
                </div>
                <div class="span4">
                    <div class="span4">الجنسيات</div>
                    <div class="span8"><input type="text" class="input-huge"></div>
                </div>
            </div>
            <div class="row-fluid TAC"><a class="btn">بحث</a></div>
        </div>
        <div class="resalt-group">
            <div class="wizerd-div TAC"><a>نتيجة البحث</a></div>
            <div class="table-warp">
                <table>
                    <thead>
                        <tr>
                            <th>كود الباكج</th>
                            <th>إسم الباكج</th>
                            <th class="span4" colspan="2">التاريخ</th>
                            <th>فندق مكة</th>
                            <th>فندق المدينة</th>
                            
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td><label></label></td>
                            <td><label></label></td>
                            <td>
                                <div class="span3">هـ من</div>
                                <div class="span9">11/11/1111</div>
                                <div class="span3">مـ من</div>
                                <div class="span9">11/11/1111</div>
                            </td>
                            <td>
                                <div class="span3">إلى</div>
                                <div class="span9">11/11/1111</div>
                                <div class="span3">إلى</div>
                                <div class="span9">11/11/1111</div>
                            </td>
                            <td><label></label></td>
                            <td><label></label></td>
                           
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>