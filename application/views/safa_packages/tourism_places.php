   <script>tourism_places_counter = 0</script>
   <div class="resalt-group">
            <div class="wizerd-div"><a><?php echo  lang('tourism_places'); ?></a></div>
            <div class="table-warp">
                <table>
                    <thead>
                        <tr>
                            <th class="span5"><?php echo  lang('city'); ?></th>
                            <th class="span5"><?php echo  lang('tourism_place'); ?></th>
                            <th class="span1">
							<a class="btn Fleft" title="<?php echo  lang('global_add'); ?>" href="javascript:void(0)" onclick="add_tourism_places('N' + tourism_places_counter++); load_multiselect()">
					        <?php echo  lang('global_add') ?>    
					        </a>
							</th>

                    </thead>
                    <tbody class='tourism_places' id='tourism_places'>
                    <? if(isset($item_tourism_places)) { ?>
                    <? if(check_array($item_tourism_places)) { ?>
                    <? foreach($item_tourism_places as $item_tourism_place) { ?>
                    <tr rel="<?php echo  $item_tourism_place->safa_package_tourismplace_id ?>">
                        
                        
                       <td>
                        <?php 
                        $item_tourism_place_hotel_id='';
                        $item_tourism_place_hotel_city_id = $item_tourism_place->erp_city_id;
                        if(isset($item_hotels)) { 
	                     	if(check_array($item_hotels)) { 
		                    	foreach($item_hotels as $item_hotel) { 
		                    		if($item_tourism_place_hotel_city_id==$item_hotel->erp_city_id) {
		                    		$item_tourism_place_hotel_id = $item_hotel->erp_hotel_id;
		                    		}
		                    	}
	                     	}
                        }
                        
                         echo  form_dropdown('tourism_places_erp_city_id['.$item_tourism_place->safa_package_tourismplace_id.']'
                               , $erp_cities, set_value('tourism_places_erp_city_id['.$item_tourism_place->safa_package_tourismplace_id.']', $item_tourism_place_hotel_id)
                               , 'class="chosen-select chosen-rtl input-full chosen_hotels" id="tourism_places_erp_city_id['.$item_tourism_place->safa_package_tourismplace_id.']" onchange="getTorismPlacesByCityId(this.value, \''.$item_tourism_place->safa_package_tourismplace_id.'\')" tabindex="4" '); 
                        ?>
                        </td>
                        
                        <td>
                        <?php 
                         echo  form_dropdown('tourism_places_safa_tourismplace_id['.$item_tourism_place->safa_package_tourismplace_id.']'
                               , $safa_tourismplaces, set_value('tourism_places_safa_tourismplace_id['.$item_tourism_place->safa_package_tourismplace_id.']', $item_tourism_place->safa_tourismplace_id)
                               , 'class="chosen-select chosen-rtl input-full" id="tourism_places_safa_tourismplace_id['.$item_tourism_place->safa_package_tourismplace_id.']" tabindex="4" '); 
                        ?>
                        </td>
                        
                        
                        <td class="TAC">
                            <a href="javascript:void(0)" onclick="delete_tourism_places(<?php echo  $item_tourism_place->safa_package_tourismplace_id ?>, true)"><span class="icon-trash"></span></a>
                        </td>
                    </tr>
                    <? } ?>
                    <? } ?>
                    <? } ?>
                    </tbody>
                </table>

            </div></div>
            
<script>
function add_tourism_places(id) {

	
	var text = [];
	$('#dv_hotels :input').each(function(){
		var input_id = $(this).attr("id");
		//alert(input_name);
		if(input_id !== undefined) {
		    if(input_id.startsWith('hotels_erp_city_id')){

			    var input_value = $("#"+input_id+" option:selected").val();
			    var input_text = $("#"+input_id+" option:selected").text();

			    
		        text.push(new Array(input_value,input_text));
		    }
		}
	})
	//text = text.join(' ');
	//alert(text);
	
	var slct_options_erp_city = "<option value=''></option>";
	for(var i=0; i<text.length ; i++) {
		slct_options_erp_city = slct_options_erp_city + "<option value='"+text[i][0]+"'>"+text[i][1]+"</option>";
	}
	//alert(slct_options_erp_hotels);
	
    var new_row = [
            "<tr rel=\"" + id + "\">",
           
            "   <td>",
            "   <select name=\"tourism_places_erp_city_id[" + id + "]\" class=\"chosen-select chosen-rtl input-full validate[required] \" id=\"tourism_places_erp_city_id_" + id + "\" onchange=\"getTorismPlacesByCityId(this.value, '" + id + "')\" tabindex=\"4\">",
            slct_options_erp_city,
            "   </select>",
            "   </td>",

            "    <td>",
            "       <select name=\"tourism_places_safa_tourismplace_id[" + id + "]\" class=\"chosen-select chosen-rtl input-full validate[required] \" id=\"tourism_places_safa_tourismplace_id_" + id + "\" tabindex=\"4\">",
            <? foreach($safa_tourismplaces as $key => $value): ?>
            "           <option value=\"<?= $key ?>\"><?= $value ?></option>",
            <? endforeach ?>
            "   </select>",
            
            "    </td>",

            
            
            "    <td class=\"TAC\">",
            "        <a href=\"javascript:void(0)\" onclick=\"delete_tourism_places('" + id + "')\"><span class=\"icon-trash\"></span></a>",
            "    </td>",
            "</tr>"
            ].join("\n");

    
        
    $('#tourism_places').append(new_row);
    
}
function delete_tourism_places(id, database) 
{
    if(typeof database == 'undefined')
    {
        $('.tourism_places').find('tr[rel="' + id + '"]').remove();
    }
    else
    {
        $('.tourism_places').find('tr[rel=' + id + ']').remove();
        var hidden_input = '<input type="hidden" name="tourism_places_remove[]" value="' + id + '" />';
        $('#tourism_places').append(hidden_input);
    }
}

</script>

<script type="text/javascript">

    function getTorismPlacesByCityId(CityId, counter) 
    {
        var strURL = "<?php echo base_url(); ?>/safa_packages/get_torism_places_by_city_id/" + CityId;
        var req = getXMLHTTP();
        if (req) {

            req.onreadystatechange = function() {
                if (req.readyState == 4) {
                    // only if "OK"
                    if (req.status == 200) {//dv_hotel
                        document.getElementById('tourism_places_safa_tourismplace_id_' + counter).innerHTML = req.responseText;
                        $('#tourism_places_safa_tourismplace_id_' + counter).trigger("chosen:updated");
                        
                    } else {
                        alert("There was a problem while using XMLHTTP:\n" + req.statusText);
                    }
                }
            }
            req.open("GET", strURL, true);
            req.send(null);
        }

    }
</script>            