<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); ?>

<style>

body{
height: auto!important;
overflow: auto !important;
background:none transparent !important;
overflow: hidden;
min-height: 1%;
}
.modal-body{
    width: 96%;
    padding:0 2% ;
}
.row-fluid{
padding-top: 5px !important;
padding-bottom: 5px !important;
}
</style>


<!-- multi selection choosen -->
<link rel="stylesheet" href="<?= NEW_CSS_JS ?>/multi-select-chosen/chosen.css">
<script src="<?= NEW_CSS_JS ?>/multi-select-chosen/chosen.jquery.js" type="text/javascript"></script>

            

    <div class="modal-body">
        <style>
            .updated_msg{
                display:none;
                background-color:#ccee97;
                font-weight: bold;
                text-align: center;
                border:3px solid #cccdc9; 
                padding:20px;  
                margin-bottom:10px;
                border-radius:15px;
                margin:10px; 
            }
            .wizerd-div,.row-form{width: 96%;margin:6% 2%;}
            label{padding: 0;}
           
        </style>
        
        

        <div class="wizerd-div">
            <a><?php echo  lang('send_to_uo') ?></a>

    	</div>
    	
		<?= form_open('', ' name="frm_send_email_to_uo_popup" id="frm_send_email_to_uo_popup" method="post"') ?>
                       
        <div class="row-fluid">
            
			<table   id='div_safa_trips_request_group'>
                                    <thead>
                                        <tr>
                                        	<th><input type="checkbox" class="checkall" id="checkall" onchange='checkUncheck()'/> </th>
                    
                                            <th ><?= lang('name_ar') ?></th>
                                            <th ><?= lang('name_la') ?></th>
                                            <th ><?= lang('email') ?></th>
                                        </tr>


                                        <?php
                                        foreach ($safa_uos_departments_rows as $safa_uos_departments_row) {
                                            
                                        	$safa_uos_department_id = $safa_uos_departments_row->safa_uos_department_id;
                                            $name_ar = $safa_uos_departments_row->name_ar;
                                            $name_la = $safa_uos_departments_row->name_la;
                                            $email = $safa_uos_departments_row->email;                                            
                                            
                                            ?>
											
											  
                                            <tr>
                                            <td>
                                            <input type="checkbox" name="emails[]" id="emails[]"  value="<?= $email ?>" title=""/>
                                            </td>
                                             
                                            <td><?php echo $name_ar; ?></td>
											<td><?php echo $name_la; ?></td>
											<td><?php echo $email; ?></td>


                                            </tr>

							    <?
								}
								?>
                                </table>
                                
                            


        </div>
        
      
      <div class="toolbar bottom TAC">
          <input  type ="submit" name="smt_send" value="<?= lang('global_submit') ?>" onclick="return are_checked();" class="btn btn-primary">
      </div>
        
    <?= form_close() ?>
    
 </div>    

<script type="text/javascript">

	function are_checked()
	{
		var flag=0;
		for (var i = 0; i < document.frm_send_email_to_uo_popup.elements.length; i++ ) 
        {
            if (document.frm_send_email_to_uo_popup.elements[i].checked ) {
            	flag = 1;
            } 
        }
        if(flag==0) {
            alert('<?= lang('global_select_one_record_at_least') ?>');
            return false;
        }
	}
	
    function checkUncheck()
    {

        for (var i = 0; i < document.frm_send_email_to_uo_popup.elements.length; i++)
        {
            if (document.frm_send_email_to_uo_popup.elements[i].type == 'checkbox' && document.frm_send_email_to_uo_popup.elements[i].id != 'checkall')
            {
                document.frm_send_email_to_uo_popup.elements[i].checked = document.frm_send_email_to_uo_popup.elements['checkall'].checked;
            }
        }
    }
</script>
    