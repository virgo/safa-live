<?php 

$safa_trip_internaltrip_id=0;  
$safa_trip_id=0;  
$safa_ito_id='';  
$safa_uo_id='';  
//$safa_ea_id='';  
$safa_uo_contract_id='';  
$erp_flight_availability_id='';  
$erp_cruise_availability_id='';  

$safa_transporter_id='';  
$safa_internaltripstatus_id=1;  
$safa_tripstatus_id='';  
$erp_company_type_id='';  
$erp_company_id='';  
$erp_company_name='';  
$operator_reference='';  
$attachement='';  
$datetime=date('Y-m-d', time()); 

$confirmation_number=0;
if (!isset($item)) {
	//$confirmation_number= gen_itconf();
}

$ito_notes=''; 
$ea_notes='';
$trip_title='';  
$trip_supervisors='';  
$trip_supervisors_phone='';  
$adult_seats='';  
$child_seats='';  
$baby_seats=''; 
if (session('uo_id')) { 
	$serial=$safa_uo_serial ;
} 
$buses_count=''; 

$erp_transportertype_id=2;

$notes=''; 

$land_going_erp_port_id='';
$land_return_erp_port_id='';
$land_going_datetime='';
$land_return_datetime='';

//$sea_going_erp_port_id='';
//$sea_return_erp_port_id='';
//$sea_going_datetime='';
//$sea_return_datetime='';

$safa_internaltrip_type_id=0;

$manage_title = lang('add_safa_trip_internaltrips');
if (isset($item)) {
	if(count($item)>0) {
		$manage_title = lang('edit_safa_trip_internaltrips');
		
		
		$safa_trip_internaltrip_id=$item->safa_trip_internaltrip_id ;  
		$safa_trip_id=$item->safa_trip_id ;  
		$safa_ito_id=$item->safa_ito_id ;  
		$safa_uo_id=$item->safa_uo_id ;   
		$safa_uo_contract_id=$item->safa_uo_contract_id ;  
		$erp_flight_availability_id=$item->erp_flight_availability_id ; 
		$erp_cruise_availability_id=$item->erp_cruise_availability_id ;  
		$safa_transporter_id=$item->safa_transporter_id ;  
		$safa_internaltripstatus_id=$item->safa_internaltripstatus_id ;  
		$safa_tripstatus_id=$item->safa_tripstatus_id ;  
		$erp_company_type_id=$item->erp_company_type_id ;  
		$erp_company_id=$item->erp_company_id ;  
		$erp_company_name=$item->erp_company_name ;  
		$operator_reference=$item->operator_reference ;  
		$attachement=$item->attachement ;  
		$datetime=$item->datetime ;  
		$confirmation_number= $item->confirmation_number ;
		$ito_notes=$item->ito_notes ; 
		$ea_notes=$item->ea_notes ;
		$trip_title=$item->trip_title ;  
		$trip_supervisors=$item->trip_supervisors ;  
		$trip_supervisors_phone=$item->trip_supervisors_phone ;  
		$adult_seats=$item->adult_seats ;  
		$child_seats=$item->child_seats ;  
		$baby_seats=$item->baby_seats ;  
		$serial=$item->serial ;  
		$buses_count=$item->buses_count ; 
		
		$erp_transportertype_id = $item->erp_transportertype_id ;
		$safa_internaltrip_type_id = $item->safa_internaltrip_type_id ;
		
		$notes=$item->notes ;  
		
		if($erp_transportertype_id==1) {
			if (isset($internalpassages)) {
	            foreach ($internalpassages as $inernalpassage) {
		            if($inernalpassage->safa_internalsegmenttype_id == 1) {
		            	$land_going_erp_port_id=$inernalpassage->erp_port_id;
		            	$land_going_datetime=date('Y-m-d H:i', strtotime($inernalpassage->start_datetime));
		            } else if($inernalpassage->safa_internalsegmenttype_id == 2) {
						$land_return_erp_port_id=$inernalpassage->erp_port_id;
						$land_return_datetime=date('Y-m-d H:i', strtotime($inernalpassage->start_datetime));
		            }
	            }
			}
		} 
//else if($erp_transportertype_id==3) {
//			if (isset($internalpassages)) {
//	            foreach ($internalpassages as $inernalpassage) {
//		            if($inernalpassage->safa_internalsegmenttype_id == 1) {
//		            	$sea_going_erp_port_id=$inernalpassage->erp_port_id;
//		            	$sea_going_datetime=$inernalpassage->start_datetime;
//		            } else if($inernalpassage->safa_internalsegmenttype_id == 2) {
//						$sea_return_erp_port_id=$inernalpassage->erp_port_id;
//						$sea_return_datetime=$inernalpassage->start_datetime;
//		            }
//	            }
//			}
//		}
		
	}
}

?>

<div id="dv_internaltrip_manage_storage">

<div class="row-fluid" >
    <div class="widget">
        <div class="path-container Fright">
            <div class="icon"><i class="icos-pencil2"></i></div> 
            <div class="path-name Fright">
               <a href="<?php 
            if(session('uo_id')) {
            	echo  site_url().'uo/dashboard'; 
            } else if(session('ea_id')) {
            	echo  site_url().'ea/dashboard';
            } else if(session('ito_id')) {
            	echo  site_url().'ito/dashboard';
            } else {
            	echo  site_url();
            }
            ?>"><?php echo  lang('global_system_management') ?></a>
            </div>    
            <div class="path-arrow Fright">
            </div>
            <div class="path-name Fright">
                <a href="<?= site_url('safa_trip_internaltrips') ?>"><?= lang('safa_trip_internaltrips') ?></a>
            </div>
            <div class="path-arrow Fright">
            </div>
            <div class="path-name Fright">
                <span><?= $manage_title ?></span>
            </div>
        </div>     
    </div>

    <div class="widget">
        <div class="widget-header">
            <div class="widget-header-icon Fright">
                <span class="icos-pencil2"></span>
            </div>
            <div class="widget-header-title Fright">
                <?= $manage_title ?>
            </div>
        </div>

        <?= form_open_multipart(false, 'id="frm_trip_internaltrip" ') ?>
        <input type='hidden' name='hdn_safa_trip_internaltrip_id'  id='hdn_safa_trip_internaltrip_id'  value='<?php echo $safa_trip_internaltrip_id;?>'/>
        <input type='hidden' name='hdn_safa_trip_id'  id='hdn_safa_trip_id'  value='<?php echo $safa_trip_id;?>'/>
                                            
        <div class="block-fluid">
        
        
        	<?php if (session('ito_id') ) {?>
        	
        	<div class="row-form" >
                <div class="span4" >
                    <div class="span4">
                        <?= lang("transport_request_date") ?>
                        <font style='color:red'>*</font>
                    </div>
                    <div class="span8"> 
                        <div >
                            <input class="datetime validate[required]" data-format="yyyy-MM-dd" name="datetime" id="date" type="text" value="<?= set_value('datetime', $datetime) ?>" ></input>
                            <script>
                                $('.datetime').datepicker({
                                    dateFormat: "yy-mm-dd",
                                    controlType: 'select',
                                    timeFormat: 'HH:mm'
                                });
                            </script>
                        </div>
                        <span class="bottom" style='color:red' >
                            <?= form_error('datetime') ?> 
                        </span>
                    </div>


                </div>
                <div class="span4" >
                    <div class="span4"><?= lang("safa_uo_id") ?>
                        <font style='color:red'>*</font>
                    </div>
                    <div class="span8">
                        <?= form_dropdown("safa_uo_id", $safa_uos, set_value("safa_uo_id", $safa_uo_id), " id='safa_uo_id' class=' chosen-select chosen-rtl validate[required] input-huge '") ?>
                        <span class="bottom" style='color:red' >
                            <?= form_error('safa_uo_id') ?> 
                        </span>
                    </div>
                </div>
                
                <div class="span4" >
                    <div class="span4"><?= lang("transport_request_contract") ?>
                    </div>
                    <div class="span8">
                        <?= form_dropdown("safa_uo_contract_id", $safa_uo_contracts, set_value("safa_uo_contract_id", $safa_uo_contract_id), " id='safa_uo_contract_id' class=' chosen-select chosen-rtl input-huge '") ?>
                        <span class="bottom" style='color:red' >
                            <?= form_error('safa_uo_contract_id') ?> 
                        </span>
                    </div>
                </div>
                
            </div>
            
        	<?php } else {?>
            <div class="row-form" >
                <div class="span6" >
                    <div class="span4">
                        <?= lang("transport_request_date") ?>
                        <font style='color:red'>*</font>
                    </div>
                    <div class="span8"> 
                        <div >
                            <input class="datetime validate[required]" data-format="yyyy-MM-dd" name="datetime" id="date" type="text" value="<?= set_value('datetime', $datetime) ?>" ></input>

                            <script>
                                $('.datetime').datepicker({
                                    dateFormat: "yy-mm-dd",
                                    controlType: 'select',
                                    timeFormat: 'HH:mm'
                                });
                            </script>


                            <!-- 
                            <span class="add-on">
                                <i data-time-icon="icon-time" data-date-icon="icon-calendar">
                                </i>
                            </span>
                            -->
                        </div>
                        <span class="bottom" style='color:red' >
                            <?= form_error('datetime') ?> 
                        </span>
                    </div>


                </div>
                <div class="span6" >
                    <div class="span4"><?= lang("transport_request_contract") ?>
                        <font style='color:red'>*</font>
                    </div>
                    <div class="span8">
                        <?= form_dropdown("safa_uo_contract_id", $safa_uo_contracts, set_value("safa_uo_contract_id", $safa_uo_contract_id), " id='safa_uo_contract_id' class=' chosen-select chosen-rtl validate[required] input-huge '") ?>
                        <span class="bottom" style='color:red' >
                            <?= form_error('safa_uo_contract_id') ?> 
                        </span>
                    </div>
                </div>
            </div>
            <?php } ?>
            
            
            
            <div class="row-form" >
                
                <?php if (session('uo_id') ) {
                if($safa_trip_internaltrip_id!=0) {
                ?>
                
                 
                <div class="span12" >
                    <div class="span2"><?= lang("trip_code") ?>
                    <font style='color:red'>*</font>
                    </div>
                    <div class="span10">
                        
                        <?= form_input("safa_uo_code", set_value("safa_uo_code", $safa_uo_code), "class='validate[required]' readonly='readonly' ") ?>
                        <?= form_input("serial", set_value("serial", $serial), " class='validate[required]' id='serial' ") ?>
                        <span class="bottom" style='color:red' id='spn_serial_validate'>
                            <?= form_error('serial') ?> 
                        </span>
                        
                        
                    </div>
                </div>
                 
                 
                <?php 
                }
                }
                ?>
                
                <!--
                <div class="span6" >
                    <div class="span4">
                        <?= lang("transport_request_opertator") ?>
					<font style='color:red'>*</font>
                    </div>
                    <div class="span8" >
                        <?= form_dropdown("safa_ito_id", $safa_ito, set_value("safa_ito_id", $safa_ito_id), " id='safa_ito_id' class='validate[required] chosen-select chosen-rtl input-huge '") ?>
                        <span class="bottom" style='color:red' >
                            <?= form_error('safa_ito_id') ?> 
                        </span>
                    </div>
                </div>
            -->
            </div>
            <div class="row-form" >
                <div class="span6" >
                    <div class="span4"><?= lang("transport_trip_supervisors") ?>
                    </div>
                    <div class="span8">
                        <?= form_input("trip_supervisors", set_value("trip_supervisors", $trip_supervisors), " id='trip_supervisors' class=' input-huge '") ?>
                        <span class="bottom" style='color:red' >
                            <?= form_error('trip_supervisors') ?> 
                        </span>
                    </div>
                </div>
                <div class="span6" >
                    <div class="span4">
                        <?= lang("transport_trip_supervisors_phone") ?>
                    </div>
                    <div class="span8" >
                        <?= form_input("trip_supervisors_phone", set_value("trip_supervisors_phone", $trip_supervisors_phone), " id='trip_supervisors_phone' class='input-huge '") ?>
                        <span class="bottom" style='color:red' >
                            <?= form_error('trip_supervisors_phone') ?> 
                        </span>
                    </div>
                </div>
            </div>
            <div class="row-form" >
                <div class="span6" >
                    <div class="span4"><?= lang("transport_request_status") ?>
                        <font style='color:red'>*</font>   
                    </div>
                    <div class="span8">
                        <?= form_dropdown("safa_internaltripstatus_id", $safa_intrernaltripstatus, set_value("safa_internaltripstatus_id", $safa_internaltripstatus_id), "id='safa_internaltripstatus_id' class='chosen-select chosen-rtl validate[required]'") ?>
                        <span class="bottom" style='color:red' >
                            <?= form_error('safa_internaltripstatus_id') ?> 
                        </span>
                    </div>
                </div>
                <!--<div class="span6">
                    <div class="span4">
                        <?= lang("transport_request_res_code") ?>
                    </div>
                    <div class="span8" >
                        <?= form_input("operator_reference", set_value("operator_reference", $operator_reference)) ?>
                        <span class="bottom" style='color:red' >
                            <?= form_error('operator_reference') ?> 
                        </span>
                    </div>
                </div>
            --></div>
                <!--<div class="row-form" >
                    <div class="span6">
                        <div class="span4"><?= lang("transport_request_transportername") ?>
                        </div>
                        <div class="span8" >
                            <?= form_dropdown("safa_transporter_id", $bus_transporters, set_value("safa_transporter_id", $safa_transporter_id), "class='chosen-select chosen-rtl'") ?>
                            <span class="bottom" style='color:red' >
                                <?= form_error('safa_transporter_id') ?> 
                            </span>
                        </div>
                    </div>
                </div>
            --><!--<div class="row-form" >
                <div class="span6" >
                    <div class="span4" ><?= lang("transport_request_res_file") ?></div>
                    <div class="span8">
                        <input type="file" value="" name="userfile"/>
                        <span class="bottom" style='color:red' >
                            <?= form_error('userfile') ?> 
                        </span>
                    </div> 
                </div>
                
                <div class="span6" >
                    <div class="span4" ><?= lang('confirmation_number') ?></div>
                    <div class="span8" >

                        <?= form_input("confirmation_number", set_value("confirmation_number", $confirmation_number), "") ?>
                        <span class="bottom" style='color:red' >
                            <?= form_error('confirmation_number') ?> 
                        </span>
                    </div>
                </div>
            
            </div>

            --><div class="row-form" >
                <div class="span12"><?= lang("travellers_seats") ?></div>
                <div class="clear clearfix"></div>
                <div class="span4" >
                    <div class="span4" ><?= lang("adult_seats") ?>
                    <font style='color:red'>*</font> 
                    </div>
                    <div class="span8">
                        <?= form_input("adult_seats", set_value("adult_seats", $adult_seats), " onchange='//fix_seats_count();' class='validate[required]'") ?>
                        <span class="bottom" style='color:red' >
                            <?= form_error('adult_seats') ?> 
                        </span>
                    </div> 
                </div>
                <div class="span4" >
                    <div class="span4" ><?= lang('child_seats') ?></div>
                    <div class="span8" >
                        <?= form_input("child_seats", set_value("child_seats", $child_seats), " onchange='//fix_seats_count();' ") ?>
                        <span class="bottom" style='color:red' >
                            <?= form_error('child_seats') ?> 
                        </span>
                    </div>
                </div>
                <div class="span3" >
                    <div class="span4" ><?= lang('baby_seats') ?></div>
                    <div class="span8" >

                        <?= form_input("baby_seats", set_value("baby_seats", $baby_seats), " onchange='//fix_seats_count();' ") ?>
                        <span class="bottom" style='color:red' >
                            <?= form_error('baby_seats') ?> 
                        </span>
                    </div>
                </div>
            </div>
            
            <div class="row-form" >
                <div class="span12" >
                    <div class="span2"><?= lang("notes") ?>
                    </div>
                    <div class="span10">
                        <?= form_textarea("notes", set_value("notes", $notes), " style='width:100%; height:50px' class='input-huge' ") ?>
                        <span class="bottom" style='color:red' >
                        <?= form_error('notes') ?> 
                        </span>
                    </div>
                </div>
                
            </div>
            
            <div class="row-form">
             <div class="span12"></div>
                <div class="clear clearfix"></div>
                
                        <div class="span6">
                            <div class="span4"><?= lang('erp_transportertype_id') ?></div>
                            <div class="span8">
                                <?=
                                form_dropdown('erp_transportertype_id', $erp_transportertypes,
                                        set_value('erp_transportertype_id', $erp_transportertype_id),
                                        'id="erp_transportertype_id" class="validate[required]"')
                                ?>
                                <script type="text/javascript">
                                    document.getElementById('erp_transportertype_id').onchange = function() {
                                        if (this.value == '2') {
                                            document.getElementById('div_tbl_filghts').style.display = "inline-block";
                                            document.getElementById('div_land_trip').style.display = "none";
                                            document.getElementById('div_sea_trip').style.display = "none";
                                        } else if (this.value == '1') {
                                            document.getElementById('div_land_trip').style.display = "inline-block";
                                            document.getElementById('div_tbl_filghts').style.display = "none";
                                            document.getElementById('div_sea_trip').style.display = "none";
                                        } else if (this.value == '3') {
                                            document.getElementById('div_sea_trip').style.display = "inline-block";
                                            document.getElementById('div_tbl_filghts').style.display = "none";
                                            document.getElementById('div_land_trip').style.display = "none";
                                        } 
                                    };
                                </script>
                            </div>
                   	</div>
            </div>
            
        </div>

		<div class="row-fluid" id="div_land_trip" <?php if ($erp_transportertype_id != 1 && $erp_transportertype_id !='' ) { ?> style="display: none;" <?php } else { ?> style="display: inline-block;" <?php } ?>>
            <div class="resalt-group">
                <div class="wizerd-div"><a><?= lang('land_trip') ?></a></div>
                
                <div class="row-form" >
                <div class="span6" >
                    <div class="span4"><?= lang("land_going_erp_port_id") ?>
                        <font style='color:red'>*</font>   
                    </div>
                    <div class="span8">
                        <?= form_dropdown("land_going_erp_port_id", $land_erp_ports_sa, set_value("land_going_erp_port_id", $land_going_erp_port_id), "id='land_going_erp_port_id' class='chosen-select chosen-rtl validate[required]'") ?>
                        <span class="bottom" style='color:red' >
                            <?= form_error('land_going_erp_port_id') ?> 
                        </span>
                    </div>
                </div>
                <div class="span6">
                    <div class="span4">
                        <?= lang("land_going_datetime") ?>
                    </div>
                    <div class="span8" >
                        <?= form_input("land_going_datetime", set_value("land_going_datetime", $land_going_datetime)," class='datetime' id='land_going_datetime'") ?>
                        <span class="bottom" style='color:red' >
                            <?= form_error('land_going_datetime') ?> 
                        </span>
                    </div>
                </div>
            </div>
            
            
            <div class="row-form" >
                <div class="span6" >
                    <div class="span4"><?= lang("land_return_erp_port_id") ?>
                        <font style='color:red'>*</font>   
                    </div>
                    <div class="span8">
                        <?= form_dropdown("land_return_erp_port_id", $land_erp_ports_sa, set_value("land_return_erp_port_id", $land_return_erp_port_id), "id='land_return_erp_port_id' class='chosen-select chosen-rtl validate[required]'") ?>
                        <span class="bottom" style='color:red' >
                            <?= form_error('land_return_erp_port_id') ?> 
                        </span>
                    </div>
                </div>
                <div class="span6">
                    <div class="span4">
                        <?= lang("land_return_datetime") ?>
                    </div>
                    <div class="span8" >
                        <?= form_input("land_return_datetime", set_value("land_return_datetime", $land_return_datetime)," class='datetime' id='land_return_datetime' ") ?>
                        <span class="bottom" style='color:red' >
                            <?= form_error('land_return_datetime') ?> 
                        </span>
                    </div>
                </div>
            </div>
                
            </div>
        </div>
        
        
        <div class="row-form" >
                <div class="span6" >
                    <?= form_radio('rdo_trip_return_type', 1,  TRUE , " id='rdo_trip_return_type'  class='validate[required]' ") ?> <?= lang('one_return_trip') ?>
                </div>
                <div class="span6" >
                <?php 
                $multi_return_trip_value = FALSE;
                if($safa_trip_internaltrip_id!=0) {
                	if($cruise_availabilities_details_rows_count>2) {
                		$multi_return_trip_value = TRUE;
                	}
                	if($flight_availabilities_details_rows_count>2) {
                		$multi_return_trip_value = TRUE;
                	}
                }
                ?>
                    <?= form_radio('rdo_trip_return_type', 2, $multi_return_trip_value , " id='rdo_trip_return_type'  class='validate[required]' ") ?> <?= lang('multi_return_trip') ?>
                </div>
        </div>
        
        <div class="row-fluid" id="div_sea_trip" <?php if ($erp_transportertype_id != 3) { ?> style="display: none;" <?php } else { ?> style="display: inline-block;" <?php } ?>>
            
            <!-- 
            <div class="resalt-group">
                <div class="wizerd-div"><a><?= lang('sea_trip') ?></a></div>
                
                <div class="row-form" >
                <div class="span6" >
                    <div class="span4"><?= lang("sea_going_erp_port_id") ?>
                        <font style='color:red'>*</font>   
                    </div>
                    <div class="span8">
                        <?= form_dropdown("sea_going_erp_port_id", $sea_erp_ports_sa, set_value("sea_going_erp_port_id", $sea_going_erp_port_id), "id='sea_going_erp_port_id' class='chosen-select chosen-rtl validate[required]'") ?>
                        <span class="bottom" style='color:red' >
                            <?= form_error('sea_going_erp_port_id') ?> 
                        </span>
                    </div>
                </div>
                <div class="span6">
                    <div class="span4">
                        <?= lang("sea_going_datetime") ?>
                    </div>
                    <div class="span8" >
                        <?= form_input("sea_going_datetime", set_value("sea_going_datetime", $sea_going_datetime)," class='datetime' id='sea_going_datetime' ") ?>
                        <span class="bottom" style='color:red' >
                            <?= form_error('sea_going_datetime') ?> 
                        </span>
                    </div>
                </div>
            </div>
            
            
            <div class="row-form" >
                <div class="span6" >
                    <div class="span4"><?= lang("sea_return_erp_port_id") ?>
                        <font style='color:red'>*</font>   
                    </div>
                    <div class="span8">
                        <?= form_dropdown("sea_return_erp_port_id", $sea_erp_ports_sa, set_value("sea_return_erp_port_id", $sea_return_erp_port_id), "id='sea_return_erp_port_id' class='chosen-select chosen-rtl validate[required]'") ?>
                        <span class="bottom" style='color:red' >
                            <?= form_error('sea_return_erp_port_id') ?> 
                        </span>
                    </div>
                </div>
                <div class="span6">
                    <div class="span4">
                        <?= lang("sea_return_datetime") ?>
                    </div>
                    <div class="span8" >
                        <?= form_input("sea_return_datetime", set_value("sea_return_datetime", $sea_return_datetime)," class='datetime' id='sea_return_datetime' ") ?>
                        <span class="bottom" style='color:red' >
                            <?= form_error('sea_return_datetime') ?> 
                        </span>
                    </div>
                </div>
            </div>
                
            </div>
             -->
            
            
            
            
            
            
            
            <div class="resalt-group">
                <div class="wizerd-div"><a><?= lang('sea_trip') ?></a></div>
                <div class="label"><?= lang('going') ?></div>
                <table id='div_trip_going_group'>
                    <thead>
                        <tr>
                            <th><?= lang('erp_path_type_id') ?></th>
                            <th><?= lang('cruise_availabilities_lines') ?></th>
                            <th><?= lang('cruise_availabilities_trip_number') ?></th>
                            <th><?= lang('cruise_availabilities_date') ?></th>
                            <th><?= lang('cruise_availabilities_arrival_date') ?></th>
                            <th><?= lang('cruise_availabilities_ports') ?></th>
                            <th><?= lang('cruise_availabilities_seat_count') ?></th>
                            <th>
                            <?php 
                            $display_style_value='block';
                            if($safa_trip_internaltrip_id!=0) {
							 	if($cruise_availabilities_details_rows_count>1) {
							 		$display_style_value='none';
                             	}
							 }
							 ?>
							
							<a href="javascript:new_cruise(1);" class="btn" id="btn_cruise_trip_going" style="display: <?php echo $display_style_value;?>"> <?php echo lang('global_add') ?> </a>
							
							</th>
                        </tr>
                    </thead>
                    <tbody id="going_cruise_tbody" class="cls_tbl_cruises">



                        <? if (isset($cruise_availabilities_details_rows)) { 
                        	
                            
                                foreach ($cruise_availabilities_details_rows as $cruise_availabilities_details_row) {

                                        $erp_port_id_from = $cruise_availabilities_details_row->erp_port_id_from;

                                        $erp_port_id_to = $cruise_availabilities_details_row->erp_port_id_to;

                                        $start_datetime = $cruise_availabilities_details_row->cruise_date . ' - ' . $cruise_availabilities_details_row->departure_time;
                                        $end_datetime = $cruise_availabilities_details_row->cruise_date . ' - ' . $cruise_availabilities_details_row->arrival_time;
                                    
                                    

                                    if ($cruise_availabilities_details_row->safa_externaltriptype_id == 1) {
                                        ?>



                                        <tr rel='<?= $cruise_availabilities_details_row->erp_cruise_availabilities_detail_id ?>'>

                                            <td>
                                            
                                            <input type='hidden' name='erp_cruise_availabilities_detail_id[<?= $cruise_availabilities_details_row->erp_cruise_availabilities_detail_id ?>]'  id='erp_cruise_availabilities_detail_id[<?= $cruise_availabilities_details_row->erp_cruise_availabilities_detail_id ?>]'  value='<?= $cruise_availabilities_details_row->erp_cruise_availabilities_detail_id ?>'/>
                                            
                                            <input type='hidden' name='safa_externaltriptype_name_<?= $cruise_availabilities_details_row->erp_cruise_availabilities_detail_id ?>'  id='safa_externaltriptype_name_<?= $cruise_availabilities_details_row->erp_cruise_availabilities_detail_id ?>'  value='<?= $cruise_availabilities_details_row->erp_path_type_name ?>'/>
                                            <input type='hidden' name='safa_externaltriptype_id[<?= $cruise_availabilities_details_row->erp_cruise_availabilities_detail_id ?>]'  id='safa_externaltriptype_id[<?= $cruise_availabilities_details_row->erp_cruise_availabilities_detail_id ?>]'  value='<?= $cruise_availabilities_details_row->safa_externaltriptype_id ?>'/>
                                            
                                            <input type="hidden" pathrel='<?php echo $cruise_availabilities_details_row->erp_cruise_availabilities_detail_id; ?>' class="erp_path_type" name="erp_path_type_id[<?= $cruise_availabilities_details_row->erp_cruise_availabilities_detail_id ?>]" id="erp_path_type_id[<?= $cruise_availabilities_details_row->erp_cruise_availabilities_detail_id ?>]"  value="<?= $cruise_availabilities_details_row->erp_path_type_id ?>" />
                                            <?= form_dropdown('erp_path_type_id['.$cruise_availabilities_details_row->erp_cruise_availabilities_detail_id.']', $erp_path_types, set_value("erp_path_type_id[".$cruise_availabilities_details_row->erp_cruise_availabilities_detail_id."]", $cruise_availabilities_details_row->erp_path_type_id), 'class="validate[required] erp_path_type" style="width:70px" pathrel="'.$cruise_availabilities_details_row->erp_cruise_availabilities_detail_id.'" id="erp_path_type_id['.$cruise_availabilities_details_row->erp_cruise_availabilities_detail_id.']"') ?>
											
                                            </td>

                                            
											
											 
											<td>
											<table style='border:0px;' class="cls_tbl_safa_transporter_code">
											<tr inner_rel='<?= $cruise_availabilities_details_row->erp_cruise_availabilities_detail_id ?>'><td style='border:0px;'>
											<?= form_dropdown('safa_transporter_code['.$cruise_availabilities_details_row->erp_cruise_availabilities_detail_id.']', $cruise_transporters, set_value("safa_transporter_code['.$cruise_availabilities_details_row->erp_cruise_availabilities_detail_id.']", $cruise_availabilities_details_row->safa_transporter_code), ' cruise_states_rel="'.$cruise_availabilities_details_row->erp_cruise_availabilities_detail_id.'"  class=" chosen-select chosen-rtl validate[required]" style="width:60px" id="safa_transporter_code['.$cruise_availabilities_details_row->erp_cruise_availabilities_detail_id.']"') ?>
											<input type='hidden' name='last_going_safa_transporter_code'  id='last_going_safa_transporter_code'  value='<?= $cruise_availabilities_details_row->safa_transporter_code ?>'/>
                                            
											
											</td>
											<td style='border:0px;'>
											<a href="javascript:void(0);" onclick="add_transporter(3);"  class="btn Fleft " > <span class="icon-plus"></span></a>
											</td>
											</tr>
											</table>
											</td>
											
								             <td><?= form_input('cruise_number['.$cruise_availabilities_details_row->erp_cruise_availabilities_detail_id.']', set_value("cruise_number['.$cruise_availabilities_details_row->erp_cruise_availabilities_detail_id.']", $cruise_availabilities_details_row->cruise_number ), ' cruise_states_rel="'.$cruise_availabilities_details_row->erp_cruise_availabilities_detail_id.'"  class="validate[required]" style="width:40px" id="cruiseno['.$cruise_availabilities_details_row->erp_cruise_availabilities_detail_id.']"') ?></td>
								             

                                            <td>

                                                <input type="text" cruise_states_rel="<?php echo $cruise_availabilities_details_row->erp_cruise_availabilities_detail_id; ?>"  class="start_date_<?php echo $cruise_availabilities_details_row->erp_cruise_availabilities_detail_id; ?>" name='cruise_date[<?= $cruise_availabilities_details_row->erp_cruise_availabilities_detail_id ?>]'  id='cruise_date[<?= $cruise_availabilities_details_row->erp_cruise_availabilities_detail_id ?>]'  value='<?= $cruise_availabilities_details_row->cruise_date . ' ' . get_time($cruise_availabilities_details_row->departure_time) ?>' style="width:100px"/>
                                            
                                            <script>
                                            $(".start_date_<?php echo $cruise_availabilities_details_row->erp_cruise_availabilities_detail_id; ?>").datetimepicker({
                                                dateFormat: 'yy-mm-dd',
                                                timeFormat: 'HH:mm',
                                                onClose: function(selectedDate) {
                                                    $('.end_date_<?php echo $cruise_availabilities_details_row->erp_cruise_availabilities_detail_id; ?>').datetimepicker('option', 'minDate', selectedDate);
                                                }
                                            });
                                        	</script>
                                        	
                                            </td>


											<td>

                                			
                                                <input type='text' class="end_date_<?php echo $cruise_availabilities_details_row->erp_cruise_availabilities_detail_id; ?>" arriverel='<?php echo $cruise_availabilities_details_row->erp_cruise_availabilities_detail_id; ?>' name='cruise_arrival_date[<?= $cruise_availabilities_details_row->erp_cruise_availabilities_detail_id ?>]'  id='cruise_arrival_date[<?= $cruise_availabilities_details_row->erp_cruise_availabilities_detail_id ?>]'  value='<?= $cruise_availabilities_details_row->arrival_date . ' ' . get_time($cruise_availabilities_details_row->arrival_time) ?>' style="width:100px"/>
                                            
                                            <script>
                                            $(".end_date_<?php echo $cruise_availabilities_details_row->erp_cruise_availabilities_detail_id; ?>").datetimepicker({
                                                dateFormat: 'yy-mm-dd',
                                                timeFormat: 'HH:mm',
                                                onClose: function(selectedDate) {
                                                    $('.start_date_<?php echo $cruise_availabilities_details_row->erp_cruise_availabilities_detail_id; ?>').datetimepicker('option', 'maxDate', selectedDate);
                                                }
                                            });
                                        	</script>
                                        	
                                            </td>
                                            
                                            <td>				
												<?= form_dropdown('erp_port_id_from['.$cruise_availabilities_details_row->erp_cruise_availabilities_detail_id.']', $sea_erp_ports, set_value("erp_port_id_from[".$cruise_availabilities_details_row->erp_cruise_availabilities_detail_id."]", $erp_port_id_from), ' erp_port_id_from_rel="'.$cruise_availabilities_details_row->erp_cruise_availabilities_detail_id.'" class="validate[required]" style="width:80px !important" id="erp_port_id_from['.$cruise_availabilities_details_row->erp_cruise_availabilities_detail_id.']"') ?>
												-
												<?= form_dropdown('erp_port_id_to['.$cruise_availabilities_details_row->erp_cruise_availabilities_detail_id.']', $sea_erp_ports_sa, set_value("erp_port_id_to[".$cruise_availabilities_details_row->erp_cruise_availabilities_detail_id."]", $erp_port_id_to), ' erp_port_id_to_rel="'.$cruise_availabilities_details_row->erp_cruise_availabilities_detail_id.'" class="validate[required]" style="width:80px !important" id="erp_port_id_to['.$cruise_availabilities_details_row->erp_cruise_availabilities_detail_id.']"') ?>
												
												
                                                <input type='hidden' name='start_ports_name_<?= $cruise_availabilities_details_row->erp_cruise_availabilities_detail_id ?>'  id='start_ports_name_<?= $cruise_availabilities_details_row->erp_cruise_availabilities_detail_id ?>'  value='<?= $cruise_availabilities_details_row->start_ports_name; ?>'/>
                                                <input type='hidden' name='end_ports_name_<?= $cruise_availabilities_details_row->erp_cruise_availabilities_detail_id ?>'  id='end_ports_name_<?= $cruise_availabilities_details_row->erp_cruise_availabilities_detail_id ?>'  value='<?= $cruise_availabilities_details_row->end_ports_name; ?>'/>


                                                <input type='hidden' name='ports_name_<?= $cruise_availabilities_details_row->erp_cruise_availabilities_detail_id ?>'  id='ports_name_<?= $cruise_availabilities_details_row->erp_cruise_availabilities_detail_id ?>'  value='<?= $cruise_availabilities_details_row->start_ports_name ?> - <?= $cruise_availabilities_details_row->end_ports_name ?>'/>



                                            </td>

                                            <td>
                                                <input type='hidden' name='seats_count[<?= $cruise_availabilities_details_row->erp_cruise_availabilities_detail_id ?>]'  id='seats_count[<?= $cruise_availabilities_details_row->erp_cruise_availabilities_detail_id ?>]'  value='<?= $cruise_availabilities_details_row->seats_count ?>'/>
                                                <input type='text' style="width:50px" name='available_seats_count_text_<?= $cruise_availabilities_details_row->erp_cruise_availabilities_detail_id ?>'  id='available_seats_count_text_<?= $cruise_availabilities_details_row->erp_cruise_availabilities_detail_id ?>'  value='<?= $cruise_availabilities_details_row->seats_count ?>' />


                                            </td>

											<td><a href="javascript:void(0);" onclick="delete_cruises('<?= $cruise_availabilities_details_row->erp_cruise_availabilities_detail_id ?>')"><span class="icon-trash"></span> </a></td>
         
											</tr>


                        <?php
                                    }
                                }
                                
                        } 
                        ?>

                    </tbody>

                </table>
                <div class="clear clearfix" style="height: 5px;"> </div>
                <div class="label clear"><?= lang('return') ?></div>
                <table id='div_trip_return_group'>
                    <thead>
                        <tr>
                            <th><?= lang('erp_path_type_id') ?></th>
                            <th><?= lang('cruise_availabilities_lines') ?></th>
                            <th><?= lang('cruise_availabilities_trip_number') ?></th>
                            <th><?= lang('cruise_availabilities_date') ?></th>
                            <th><?= lang('cruise_availabilities_arrival_date') ?></th>
                            <th><?= lang('cruise_availabilities_ports') ?></th>
                            <th><?= lang('cruise_availabilities_seat_count') ?></th>
                            <th>
                            
                            <?php 
                            $display_style_value='block';
                            if($safa_trip_internaltrip_id!=0) {
							 	if($cruise_availabilities_details_rows_count<3) {
							 		$display_style_value='none';
                             	}
							 }
							 ?>
						
                            <a href="javascript:new_cruise(2);" class="btn" id="btn_cruise_trip_return" style="display: <?php echo $display_style_value;?>"> <?php echo lang('global_add') ?> </a>
							
							</th>
                        </tr>
                    </thead>
                    <tbody id="return_cruise_tbody" class="cls_tbl_cruises">

                        <? if (isset($cruise_availabilities_details_rows)) { 
                        	
                                foreach ($cruise_availabilities_details_rows as $cruise_availabilities_details_row) {
                                        
                                	$erp_port_id_from = $cruise_availabilities_details_row->erp_port_id_from;
                                    $erp_port_id_to = $cruise_availabilities_details_row->erp_port_id_to;
                                    $start_datetime = $cruise_availabilities_details_row->cruise_date . ' - ' . $cruise_availabilities_details_row->departure_time;
                                    $end_datetime = $cruise_availabilities_details_row->cruise_date . ' - ' . $cruise_availabilities_details_row->arrival_time;
                                    
                                    

                                    if ($cruise_availabilities_details_row->safa_externaltriptype_id == 2) {
                                        ?>



                                        <tr  rel='<?= $cruise_availabilities_details_row->erp_cruise_availabilities_detail_id ?>'>

                                				<td>
                                            
                                            	<input type='hidden' name='erp_cruise_availabilities_detail_id[<?= $cruise_availabilities_details_row->erp_cruise_availabilities_detail_id ?>]'  id='erp_cruise_availabilities_detail_id[<?= $cruise_availabilities_details_row->erp_cruise_availabilities_detail_id ?>]'  value='<?= $cruise_availabilities_details_row->erp_cruise_availabilities_detail_id ?>'/>
                                            
                                                <input type="hidden" name="trip_going_erp_cruise_availability_id<?= $cruise_availabilities_details_row->erp_cruise_availabilities_detail_id ?>" id="trip_going_erp_cruise_availability_id<?= $cruise_availabilities_details_row->erp_cruise_availabilities_detail_id ?>"  value="<?= $cruise_availabilities_details_row->erp_cruise_availabilities_detail_id ?>" />
												
												
                                                <input type='hidden' name='safa_externaltriptype_name_<?= $cruise_availabilities_details_row->erp_cruise_availabilities_detail_id ?>'  id='safa_externaltriptype_name_<?= $cruise_availabilities_details_row->erp_cruise_availabilities_detail_id ?>'  value='<?= $cruise_availabilities_details_row->erp_path_type_name ?>'/>
                                                <input type='hidden' name='safa_externaltriptype_id[<?= $cruise_availabilities_details_row->erp_cruise_availabilities_detail_id ?>]'  id='safa_externaltriptype_id[<?= $cruise_availabilities_details_row->erp_cruise_availabilities_detail_id ?>]'  value='<?= $cruise_availabilities_details_row->safa_externaltriptype_id ?>'/>
                                                
                                            
                                            	<?= form_dropdown('erp_path_type_id['.$cruise_availabilities_details_row->erp_cruise_availabilities_detail_id.']', $erp_path_types, set_value("erp_path_type_id[".$cruise_availabilities_details_row->erp_cruise_availabilities_detail_id."]", $cruise_availabilities_details_row->erp_path_type_id), 'class="validate[required] erp_path_type" style="width:70px" pathrel="'.$cruise_availabilities_details_row->erp_cruise_availabilities_detail_id.'" id="erp_path_type_id['.$cruise_availabilities_details_row->erp_cruise_availabilities_detail_id.']"') ?>
											
                                            </td>

                                            
											
											 
											<td>
											<table style='border:0px;' class="cls_tbl_safa_transporter_code">
											<tr inner_rel='<?= $cruise_availabilities_details_row->erp_cruise_availabilities_detail_id ?>'><td style='border:0px;'>
											<?= form_dropdown('safa_transporter_code['.$cruise_availabilities_details_row->erp_cruise_availabilities_detail_id.']', $cruise_transporters, set_value("safa_transporter_code['.$cruise_availabilities_details_row->erp_cruise_availabilities_detail_id.']", $cruise_availabilities_details_row->safa_transporter_code), ' cruise_states_rel="'.$cruise_availabilities_details_row->erp_cruise_availabilities_detail_id.'"  class=" chosen-select chosen-rtl validate[required]" style="width:60px" id="safa_transporter_code['.$cruise_availabilities_details_row->erp_cruise_availabilities_detail_id.']"') ?>
											
											<input type='hidden' name='last_return_safa_transporter_code'  id='last_return_safa_transporter_code'  value='<?= $cruise_availabilities_details_row->safa_transporter_code ?>'/>
                                            
											</td>
											<td style='border:0px;'>
											<a href="javascript:void(0);" onclick="add_transporter(3);"  class="btn Fleft " > <span class="icon-plus"></span></a>
											</td>
											</tr>
											</table>
											</td>
											
								             <td><?= form_input('cruise_number['.$cruise_availabilities_details_row->erp_cruise_availabilities_detail_id.']', set_value("cruise_number['.$cruise_availabilities_details_row->erp_cruise_availabilities_detail_id.']", $cruise_availabilities_details_row->cruise_number ), ' cruise_states_rel="'.$cruise_availabilities_details_row->erp_cruise_availabilities_detail_id.'" class="validate[required]" style="width:40px" id="cruiseno['.$cruise_availabilities_details_row->erp_cruise_availabilities_detail_id.']"') ?></td>
								             
                                            
                                            
                                            <td>

                                                <input type="text" cruise_states_rel="<?php echo $cruise_availabilities_details_row->erp_cruise_availabilities_detail_id; ?>"  class="start_date_<?php echo $cruise_availabilities_details_row->erp_cruise_availabilities_detail_id; ?>" name='cruise_date[<?= $cruise_availabilities_details_row->erp_cruise_availabilities_detail_id ?>]'  id='cruise_date[<?= $cruise_availabilities_details_row->erp_cruise_availabilities_detail_id ?>]'  value='<?= $cruise_availabilities_details_row->cruise_date . ' ' . get_time($cruise_availabilities_details_row->departure_time) ?>' style="width:100px"/>
                                            
                                            <script>
                                            $(".start_date_<?php echo $cruise_availabilities_details_row->erp_cruise_availabilities_detail_id; ?>").datetimepicker({
                                                dateFormat: 'yy-mm-dd',
                                                timeFormat: 'HH:mm',
                                                onClose: function(selectedDate) {
                                                    $('.end_date_<?php echo $cruise_availabilities_details_row->erp_cruise_availabilities_detail_id; ?>').datetimepicker('option', 'minDate', selectedDate);
                                                }
                                            });
                                        	</script>
                                        	
                                            </td>


											<td>

                                			
                                                <input type='text' class="end_date_<?php echo $cruise_availabilities_details_row->erp_cruise_availabilities_detail_id; ?>" arriverel='<?php echo $cruise_availabilities_details_row->erp_cruise_availabilities_detail_id; ?>' name='cruise_arrival_date[<?= $cruise_availabilities_details_row->erp_cruise_availabilities_detail_id ?>]'  id='cruise_arrival_date[<?= $cruise_availabilities_details_row->erp_cruise_availabilities_detail_id ?>]'  value='<?= $cruise_availabilities_details_row->arrival_date . ' ' . get_time($cruise_availabilities_details_row->arrival_time) ?>' style="width:100px"/>
                                            
                                            <script>
                                            $(".end_date_<?php echo $cruise_availabilities_details_row->erp_cruise_availabilities_detail_id; ?>").datetimepicker({
                                                dateFormat: 'yy-mm-dd',
                                                timeFormat: 'HH:mm',
                                                onClose: function(selectedDate) {
                                                    $('.start_date_<?php echo $cruise_availabilities_details_row->erp_cruise_availabilities_detail_id; ?>').datetimepicker('option', 'maxDate', selectedDate);
                                                }
                                            });
                                        	</script>
                                        	
                                            </td>


                                            <td>

												<?= form_dropdown('erp_port_id_from['.$cruise_availabilities_details_row->erp_cruise_availabilities_detail_id.']', $sea_erp_ports_sa, set_value("erp_port_id_from[".$cruise_availabilities_details_row->erp_cruise_availabilities_detail_id."]", $erp_port_id_from), ' erp_port_id_from_rel="'.$cruise_availabilities_details_row->erp_cruise_availabilities_detail_id.'" class="validate[required]" style="width:80px !important" id="erp_port_id_from['.$cruise_availabilities_details_row->erp_cruise_availabilities_detail_id.']"') ?>
												-
												<?= form_dropdown('erp_port_id_to['.$cruise_availabilities_details_row->erp_cruise_availabilities_detail_id.']', $sea_erp_ports, set_value("erp_port_id_to[".$cruise_availabilities_details_row->erp_cruise_availabilities_detail_id."]", $erp_port_id_to), '   erp_port_id_to_rel="'.$cruise_availabilities_details_row->erp_cruise_availabilities_detail_id.'" class="validate[required]" style="width:80px !important" id="erp_port_id_to['.$cruise_availabilities_details_row->erp_cruise_availabilities_detail_id.']"') ?>
												
												
                                                <input type='hidden' name='start_ports_name_<?= $cruise_availabilities_details_row->erp_cruise_availabilities_detail_id ?>'  id='start_ports_name_<?= $cruise_availabilities_details_row->erp_cruise_availabilities_detail_id ?>'  value='<?= $cruise_availabilities_details_row->start_ports_name; ?>'/>
                                                <input type='hidden' name='end_ports_name_<?= $cruise_availabilities_details_row->erp_cruise_availabilities_detail_id ?>'  id='end_ports_name_<?= $cruise_availabilities_details_row->erp_cruise_availabilities_detail_id ?>'  value='<?= $cruise_availabilities_details_row->end_ports_name; ?>'/>


                                                <input type='hidden' name='ports_name_<?= $cruise_availabilities_details_row->erp_cruise_availabilities_detail_id ?>'  id='ports_name_<?= $cruise_availabilities_details_row->erp_cruise_availabilities_detail_id ?>'  value='<?= $cruise_availabilities_details_row->start_ports_name ?> - <?= $cruise_availabilities_details_row->end_ports_name ?>'/>


                                                
                                            </td>


                                            <td>
                                                <input type='hidden' name='seats_count[<?= $cruise_availabilities_details_row->erp_cruise_availabilities_detail_id ?>]'  id='seats_count[<?= $cruise_availabilities_details_row->erp_cruise_availabilities_detail_id ?>]'  value='<?= $cruise_availabilities_details_row->seats_count ?>'/>
                                                <input type='text' style="width:50px" name='available_seats_count_text_<?= $cruise_availabilities_details_row->erp_cruise_availabilities_detail_id ?>'  id='available_seats_count_text_<?= $cruise_availabilities_details_row->erp_cruise_availabilities_detail_id ?>'  value='<?= $cruise_availabilities_details_row->seats_count ?>' />


                                            </td>


											<td><a href="javascript:void(0);" onclick="delete_cruises('<?= $cruise_availabilities_details_row->erp_cruise_availabilities_detail_id ?>')"><span class="icon-trash"></span> </a></td>
         
											</tr>


                                        <?php
                                    }
                                }
                          } 
                          ?>

                    </tbody>

                </table>
            </div> 
            
            
            
            
            
            
            
            
            
        </div>
        
        <div class="row-fluid" id="div_tbl_filghts" <?php if ($erp_transportertype_id != 2) { ?> style="display: none;" <?php } else { ?> style="display: inline-block;" <?php } ?>>
            <div class="resalt-group">
                <div class="wizerd-div"><a><?= lang('flight_trip') ?></a></div>
                <div class="label"><?= lang('going') ?></div>
                <table id='div_trip_going_group'>
                    <thead>
                        <tr>
                            <th><?= lang('erp_path_type_id') ?></th>
                            <th><?= lang('flight_availabilities_airlines') ?></th>
                            <th><?= lang('flight_availabilities_flight_number') ?></th>
                            <th><?= lang('flight_availabilities_date') ?></th>
                            <th><?= lang('arrival_date') ?></th>
                            <th><?= lang('flight_availabilities_airports') ?></th>
                            <th><?= lang('flight_availabilities_seat_count') ?></th>
                            <th>
                             <?php 
                            $display_style_value='block';
                            if($safa_trip_internaltrip_id!=0) {
							 	if($flight_availabilities_details_rows_count>1) {
							 		$display_style_value='none';
                             	}
							 }
							 ?>
                            <a href="javascript:new_row(1);" class="btn" id="btn_flight_trip_going" style="display: <?php echo $display_style_value;?>"> <?php echo lang('global_add') ?> </a>
                            </th>

                        </tr>
                    </thead>
                    <tbody id="going_flight_tbody" class="cls_tbl_filghts">



                        <? if (isset($going_trip_flights_rows)) { ?>
                            <?
                            foreach ($going_trip_flights_rows as $value) {
                                $this->flight_availabilities_model->erp_flight_availability_id = $value->erp_flight_availability_id;

                                //$this->flight_availabilities_model->erp_path_type_id = $flight_trip_type;

                                $flight_availabilities_details_rows = $this->flight_availabilities_model->get_details_for_trip();

                                $ports = '';

                                $safa_externaltriptype_name = '';

                                $start_datetime = '';
                                $end_datetime = '';
                                $loop_counter = 0;

                                $erp_port_id_from = 0;
                                $erp_port_id_to = 0;
                                $start_ports_name = '';
                                $end_ports_name = '';
                                ?>


                                <?php
                                foreach ($flight_availabilities_details_rows as $flight_availabilities_details_row) {
                                    if ($loop_counter == 0) {
                                        $safa_externaltriptype_name = $flight_availabilities_details_row->safa_externaltriptype_name;
                                        $erp_path_type_name = $flight_availabilities_details_row->erp_path_type_name;

                                        $erp_port_id_from = $flight_availabilities_details_row->erp_port_id_from;
                                        $start_ports_name = $flight_availabilities_details_row->start_ports_name;

                                        $erp_port_id_to = $flight_availabilities_details_row->erp_port_id_to;
                                        $end_ports_name = $flight_availabilities_details_row->end_ports_name;

                                        $ports = $flight_availabilities_details_row->start_ports_name . ' - ' . $flight_availabilities_details_row->end_ports_name;
                                        $start_datetime = $flight_availabilities_details_row->flight_date . ' - ' . $flight_availabilities_details_row->departure_time;
                                        $end_datetime = $flight_availabilities_details_row->flight_date . ' - ' . $flight_availabilities_details_row->arrival_time;
                                    } else {
                                        $safa_externaltriptype_name = $safa_externaltriptype_name . ' <br/> ' . $flight_availabilities_details_row->safa_externaltriptype_name;

                                        $erp_port_id_to = $flight_availabilities_details_row->erp_port_id_to;
                                        $end_ports_name = $flight_availabilities_details_row->end_ports_name;

                                        $erp_port_id_from = $flight_availabilities_details_row->erp_port_id_from;
                                        $start_ports_name = $flight_availabilities_details_row->start_ports_name;
                                        
                                        $ports = $ports . ' <br/> ' . $flight_availabilities_details_row->start_ports_name . ' - ' . $flight_availabilities_details_row->end_ports_name;
                                        $end_datetime = $flight_availabilities_details_row->flight_date . ' - ' . $flight_availabilities_details_row->arrival_time;
                                    }
                                    
                                    $erp_port_hall_id_from = $flight_availabilities_details_row->erp_port_hall_id_from;
                                    $erp_port_hall_id_to = $flight_availabilities_details_row->erp_port_hall_id_to;
                                    
                                    $loop_counter++;

                                    if ($flight_availabilities_details_row->safa_externaltriptype_id == 1) {
                                        ?>



                                        <tr rel='<?= $flight_availabilities_details_row->erp_flight_availabilities_detail_id ?>'>

                                                                                <!--<td><?php echo $flight_availabilities_details_row->safa_externaltriptype_name; ?>
                                                                                 <input type='hidden' name='safa_externaltriptype_name_<?= $flight_availabilities_details_row->erp_flight_availabilities_detail_id ?>'  id='safa_externaltriptype_name_<?= $flight_availabilities_details_row->erp_flight_availabilities_detail_id ?>'  value='<?= $flight_availabilities_details_row->safa_externaltriptype_name ?>'/>
                                                                                </td>

                                            --><td>
                                            
                                            <input type='hidden' name='erp_flight_availabilities_detail_id[<?= $flight_availabilities_details_row->erp_flight_availabilities_detail_id ?>]'  id='erp_flight_availabilities_detail_id[<?= $flight_availabilities_details_row->erp_flight_availabilities_detail_id ?>]'  value='<?= $flight_availabilities_details_row->erp_flight_availabilities_detail_id ?>'/>
                                            
<!--                                            <?php echo $flight_availabilities_details_row->erp_path_type_name; ?>-->
                                                <input type='hidden' name='safa_externaltriptype_name_<?= $flight_availabilities_details_row->erp_flight_availabilities_detail_id ?>'  id='safa_externaltriptype_name_<?= $flight_availabilities_details_row->erp_flight_availabilities_detail_id ?>'  value='<?= $flight_availabilities_details_row->erp_path_type_name ?>'/>
                                               <input type='hidden' name='safa_externaltriptype_id[<?= $flight_availabilities_details_row->erp_flight_availabilities_detail_id ?>]'  id='safa_externaltriptype_id[<?= $flight_availabilities_details_row->erp_flight_availabilities_detail_id ?>]'  value='<?= $flight_availabilities_details_row->safa_externaltriptype_id ?>'/>
                                            
                                            <input type="hidden" pathrel='<?php echo $flight_availabilities_details_row->erp_flight_availabilities_detail_id; ?>' class="erp_path_type" name="erp_path_type_id[<?= $flight_availabilities_details_row->erp_flight_availabilities_detail_id ?>]" id="erp_path_type_id[<?= $flight_availabilities_details_row->erp_flight_availabilities_detail_id ?>]"  value="<?= $flight_availabilities_details_row->erp_path_type_id ?>" />
                                            <?= form_dropdown('erp_path_type_id['.$flight_availabilities_details_row->erp_flight_availabilities_detail_id.']', $erp_path_types, set_value("erp_path_type_id[".$flight_availabilities_details_row->erp_flight_availabilities_detail_id."]", $flight_availabilities_details_row->erp_path_type_id), 'class="validate[required] erp_path_type" style="width:70px" pathrel="'.$flight_availabilities_details_row->erp_flight_availabilities_detail_id.'" id="erp_path_type_id['.$flight_availabilities_details_row->erp_flight_availabilities_detail_id.']"') ?>
											
                                            </td>

                                            
											
											 
											<td>
											<table style='border:0px;' class="cls_tbl_safa_transporter_code">
											<tr inner_rel='<?= $flight_availabilities_details_row->erp_flight_availabilities_detail_id ?>'><td style='border:0px;'>
											<?= form_dropdown('safa_transporter_code['.$flight_availabilities_details_row->erp_flight_availabilities_detail_id.']', $flight_transporters, set_value("safa_transporter_code['.$flight_availabilities_details_row->erp_flight_availabilities_detail_id.']", $flight_availabilities_details_row->safa_transporter_code), ' flight_states_rel="'.$flight_availabilities_details_row->erp_flight_availabilities_detail_id.'"  class=" chosen-select chosen-rtl validate[required]" style="width:60px" id="safa_transporter_code['.$flight_availabilities_details_row->erp_flight_availabilities_detail_id.']"') ?>
											<input type='hidden' name='last_going_safa_transporter_code'  id='last_going_safa_transporter_code'  value='<?= $flight_availabilities_details_row->safa_transporter_code ?>'/>
                                            
											
											</td>
											<td style='border:0px;'>
											<a href="javascript:void(0);" onclick="add_transporter(2);"  class="btn Fleft " > <span class="icon-plus"></span></a>
											</td>
											</tr>
											</table>
											</td>
											
								             <td><?= form_input('flight_number['.$flight_availabilities_details_row->erp_flight_availabilities_detail_id.']', set_value("flight_number['.$flight_availabilities_details_row->erp_flight_availabilities_detail_id.']", $flight_availabilities_details_row->flight_number ), ' flight_states_rel="'.$flight_availabilities_details_row->erp_flight_availabilities_detail_id.'"  class="validate[required]" style="width:40px" id="flightno['.$flight_availabilities_details_row->erp_flight_availabilities_detail_id.']"') ?></td>
								             

                                            <td>

                                                                                <!--<input type='hidden' name='departure_date_<?= $flight_availabilities_details_row->erp_flight_availabilities_detail_id ?>'  id='departure_date_<?= $flight_availabilities_details_row->erp_flight_availabilities_detail_id ?>'  value='<?= $start_datetime ?>'/>
                                                <?php echo $start_datetime; ?>
                                                -->
                                                <input type="text" flight_states_rel="<?php echo $flight_availabilities_details_row->erp_flight_availabilities_detail_id; ?>"  class="start_date_<?php echo $flight_availabilities_details_row->erp_flight_availabilities_detail_id; ?>" name='flight_date[<?= $flight_availabilities_details_row->erp_flight_availabilities_detail_id ?>]'  id='flight_date[<?= $flight_availabilities_details_row->erp_flight_availabilities_detail_id ?>]'  value='<?= $flight_availabilities_details_row->flight_date . ' ' . get_time($flight_availabilities_details_row->departure_time) ?>' style="width:100px"/>
<!--                                                <?php echo $flight_availabilities_details_row->flight_date . ' ' . $flight_availabilities_details_row->departure_time; ?>-->
                                            
                                            <script>
                                            $(".start_date_<?php echo $flight_availabilities_details_row->erp_flight_availabilities_detail_id; ?>").datetimepicker({
                                                dateFormat: 'yy-mm-dd',
                                                timeFormat: 'HH:mm',
                                                onClose: function(selectedDate) {
                                                    $('.end_date_<?php echo $flight_availabilities_details_row->erp_flight_availabilities_detail_id; ?>').datetimepicker('option', 'minDate', selectedDate);
                                                }
                                            });
                                        	</script>
                                        	
                                            </td>


											<td>

                                			<!--<input type='hidden' name='departure_date_<?= $flight_availabilities_details_row->erp_flight_availabilities_detail_id ?>'  id='departure_date_<?= $flight_availabilities_details_row->erp_flight_availabilities_detail_id ?>'  value='<?= $start_datetime ?>'/>
                                                <?php echo $start_datetime; ?>
                                                -->
                                                <input type='text' class="end_date_<?php echo $flight_availabilities_details_row->erp_flight_availabilities_detail_id; ?>" arriverel='<?php echo $flight_availabilities_details_row->erp_flight_availabilities_detail_id; ?>' name='flight_arrival_date[<?= $flight_availabilities_details_row->erp_flight_availabilities_detail_id ?>]'  id='flight_arrival_date[<?= $flight_availabilities_details_row->erp_flight_availabilities_detail_id ?>]'  value='<?= $flight_availabilities_details_row->arrival_date . ' ' . get_time($flight_availabilities_details_row->arrival_time) ?>' style="width:100px"/>
<!--                                                <?php echo $flight_availabilities_details_row->arrival_date . ' ' . $flight_availabilities_details_row->arrival_time; ?>-->
                                            
                                            <script>
                                            $(".end_date_<?php echo $flight_availabilities_details_row->erp_flight_availabilities_detail_id; ?>").datetimepicker({
                                                dateFormat: 'yy-mm-dd',
                                                timeFormat: 'HH:mm',
                                                onClose: function(selectedDate) {
                                                    $('.start_date_<?php echo $flight_availabilities_details_row->erp_flight_availabilities_detail_id; ?>').datetimepicker('option', 'maxDate', selectedDate);
                                                }
                                            });
                                        	</script>
                                        	
                                            </td>
                                            
                                            <td>
<!--                                                <input type='hidden' name='erp_port_id_from[<?= $flight_availabilities_details_row->erp_flight_availabilities_detail_id ?>]'  id='erp_port_id_from[<?= $flight_availabilities_details_row->erp_flight_availabilities_detail_id ?>]'  value='<?= $flight_availabilities_details_row->erp_port_id_from ?>'/>-->
<!--                                                <input type='hidden' name='erp_port_id_to[<?= $flight_availabilities_details_row->erp_flight_availabilities_detail_id ?>]'  id='erp_port_id_to[<?= $flight_availabilities_details_row->erp_flight_availabilities_detail_id ?>]'  value='<?= $flight_availabilities_details_row->erp_port_id_to ?>'/>-->

												
												<?= form_dropdown('erp_port_id_from['.$flight_availabilities_details_row->erp_flight_availabilities_detail_id.']', $erp_ports, set_value("erp_port_id_from[".$flight_availabilities_details_row->erp_flight_availabilities_detail_id."]", $erp_port_id_from), ' erp_port_id_from_rel="'.$flight_availabilities_details_row->erp_flight_availabilities_detail_id.'" class="validate[required]" style="width:60px !important" id="erp_port_id_from['.$flight_availabilities_details_row->erp_flight_availabilities_detail_id.']"') ?>
<!--												<?= form_dropdown('erp_port_hall_id_from['.$flight_availabilities_details_row->erp_flight_availabilities_detail_id.']', $erp_ports_halls, set_value("erp_port_hall_id_from[".$flight_availabilities_details_row->erp_flight_availabilities_detail_id."]", $erp_port_hall_id_from), ' erp_port_hall_id_from_rel="'.$flight_availabilities_details_row->erp_flight_availabilities_detail_id.'" class="validate[required]" style="width:120px !important" id="erp_port_hall_id_from['.$flight_availabilities_details_row->erp_flight_availabilities_detail_id.']"') ?>-->
												<br/>
												<?= form_dropdown('erp_port_id_to['.$flight_availabilities_details_row->erp_flight_availabilities_detail_id.']', $erp_sa_ports, set_value("erp_port_id_to[".$flight_availabilities_details_row->erp_flight_availabilities_detail_id."]", $erp_port_id_to), ' erp_port_id_to_rel="'.$flight_availabilities_details_row->erp_flight_availabilities_detail_id.'" class="validate[required]" style="width:60px !important" id="erp_port_id_to['.$flight_availabilities_details_row->erp_flight_availabilities_detail_id.']"') ?>
												<?= form_dropdown('erp_port_hall_id_to['.$flight_availabilities_details_row->erp_flight_availabilities_detail_id.']', $erp_sa_ports_halls, set_value("erp_port_hall_id_to[".$flight_availabilities_details_row->erp_flight_availabilities_detail_id."]", $erp_port_hall_id_to), '  erp_port_hall_id_to_rel="'.$flight_availabilities_details_row->erp_flight_availabilities_detail_id.'" class="validate[required]" style="width:120px !important" id="erp_port_hall_id_to['.$flight_availabilities_details_row->erp_flight_availabilities_detail_id.']"') ?>
												
												
                                                <input type='hidden' name='start_ports_name_<?= $flight_availabilities_details_row->erp_flight_availabilities_detail_id ?>'  id='start_ports_name_<?= $flight_availabilities_details_row->erp_flight_availabilities_detail_id ?>'  value='<?= $flight_availabilities_details_row->start_ports_name; ?>'/>
                                                <input type='hidden' name='end_ports_name_<?= $flight_availabilities_details_row->erp_flight_availabilities_detail_id ?>'  id='end_ports_name_<?= $flight_availabilities_details_row->erp_flight_availabilities_detail_id ?>'  value='<?= $flight_availabilities_details_row->end_ports_name; ?>'/>


<!--                                                <?= $flight_availabilities_details_row->start_ports_name ?> - <?= $flight_availabilities_details_row->end_ports_name ?>-->
                                                <input type='hidden' name='ports_name_<?= $flight_availabilities_details_row->erp_flight_availabilities_detail_id ?>'  id='ports_name_<?= $flight_availabilities_details_row->erp_flight_availabilities_detail_id ?>'  value='<?= $flight_availabilities_details_row->start_ports_name ?> - <?= $flight_availabilities_details_row->end_ports_name ?>'/>



                                            </td>

                                            <td>
                                                <input type='hidden' name='seats_count[<?= $flight_availabilities_details_row->erp_flight_availabilities_detail_id ?>]'  id='seats_count[<?= $flight_availabilities_details_row->erp_flight_availabilities_detail_id ?>]'  value='<?= $flight_availabilities_details_row->available_seats_count ?>'/>
                                                <input type='text' style="width:50px" name='available_seats_count_text_<?= $flight_availabilities_details_row->erp_flight_availabilities_detail_id ?>'  id='available_seats_count_text_<?= $flight_availabilities_details_row->erp_flight_availabilities_detail_id ?>'  value='<?= $flight_availabilities_details_row->available_seats_count ?>' />

                                                <!-- <?= $value->available_seats_count ?> -->

                                            </td>

											<td><a href="javascript:void(0);" onclick="delete_flights('<?= $flight_availabilities_details_row->erp_flight_availabilities_detail_id ?>')"><span class="icon-trash"></span> </a></td>
         
											</tr>


                                        <?php
                                    }
                                }
                                ?>



                            <? } ?>
                        <? } ?>

                    </tbody>

                </table>
                <div class="clear clearfix" style="height: 5px;"> </div>
                <div class="label clear"><?= lang('return') ?></div>
                <table id='div_trip_return_group'>
                    <thead>
                        <tr>
                            <th><?= lang('erp_path_type_id') ?></th>
                            <th><?= lang('flight_availabilities_airlines') ?></th>
                            <th><?= lang('flight_availabilities_flight_number') ?></th>
                            <th><?= lang('flight_availabilities_date') ?></th>
                            <th><?= lang('arrival_date') ?></th>
                            <th><?= lang('flight_availabilities_airports') ?></th>
                            <th><?= lang('flight_availabilities_seat_count') ?></th>

							<?php 
                            $display_style_value='block';
                            if($safa_trip_internaltrip_id!=0) {
							 	if($flight_availabilities_details_rows_count<3) {
							 		$display_style_value='none';
                             	}
							 }
							 ?>
                            <th><a href="javascript:new_row(2);" class="btn" id="btn_flight_trip_return" style="display: <?php echo $display_style_value;?>"> <?php echo lang('global_add') ?> </a></th>

                        </tr>
                    </thead>
                    <tbody id="return_flight_tbody" class="cls_tbl_filghts">

                        <? if (isset($going_trip_flights_rows)) { ?>
                            <?
                            foreach ($going_trip_flights_rows as $value) {
                                $this->flight_availabilities_model->erp_flight_availability_id = $value->erp_flight_availability_id;

                                //$this->flight_availabilities_model->erp_path_type_id = $flight_trip_type;

                                $flight_availabilities_details_rows = $this->flight_availabilities_model->get_details_for_trip();

                                $ports = '';

                                $safa_externaltriptype_name = '';

                                $start_datetime = '';
                                $end_datetime = '';
                                $loop_counter = 0;

                                $erp_port_id_from = 0;
                                $erp_port_id_to = 0;
                                $start_ports_name = '';
                                $end_ports_name = '';
                                ?>


                                <?php
                                foreach ($flight_availabilities_details_rows as $flight_availabilities_details_row) {
                                    if ($loop_counter == 0) {
                                        $safa_externaltriptype_name = $flight_availabilities_details_row->safa_externaltriptype_name;
                                        $erp_path_type_name = $flight_availabilities_details_row->erp_path_type_name;

                                        $erp_port_id_from = $flight_availabilities_details_row->erp_port_id_from;
                                        $start_ports_name = $flight_availabilities_details_row->start_ports_name;

                                        $erp_port_id_to = $flight_availabilities_details_row->erp_port_id_to;
                                        $end_ports_name = $flight_availabilities_details_row->end_ports_name;

                                        $ports = $flight_availabilities_details_row->start_ports_name . ' - ' . $flight_availabilities_details_row->end_ports_name;
                                        $start_datetime = $flight_availabilities_details_row->flight_date . ' - ' . $flight_availabilities_details_row->departure_time;
                                        $end_datetime = $flight_availabilities_details_row->flight_date . ' - ' . $flight_availabilities_details_row->arrival_time;
                                    } else {
                                        $safa_externaltriptype_name = $safa_externaltriptype_name . ' <br/> ' . $flight_availabilities_details_row->safa_externaltriptype_name;

                                        $erp_port_id_from = $flight_availabilities_details_row->erp_port_id_from;
                                        $start_ports_name = $flight_availabilities_details_row->start_ports_name;
                                        
                                        $erp_port_id_to = $flight_availabilities_details_row->erp_port_id_to;
                                        $end_ports_name = $flight_availabilities_details_row->end_ports_name;

                                        $ports = $ports . ' <br/> ' . $flight_availabilities_details_row->start_ports_name . ' - ' . $flight_availabilities_details_row->end_ports_name;
                                        $end_datetime = $flight_availabilities_details_row->flight_date . ' - ' . $flight_availabilities_details_row->arrival_time;
                                    }
                                    
                                    $erp_port_hall_id_from = $flight_availabilities_details_row->erp_port_hall_id_from;
                                    $erp_port_hall_id_to = $flight_availabilities_details_row->erp_port_hall_id_to;
                                    
                                    $loop_counter++;

                                    if ($flight_availabilities_details_row->safa_externaltriptype_id == 2) {
                                        ?>



                                        <tr  rel='<?= $flight_availabilities_details_row->erp_flight_availabilities_detail_id ?>'>

                                <!--<td><?php echo $flight_availabilities_details_row->safa_externaltriptype_name; ?>
                                 <input type='hidden' name='safa_externaltriptype_name_<?= $flight_availabilities_details_row->erp_flight_availabilities_detail_id ?>'  id='safa_externaltriptype_name_<?= $flight_availabilities_details_row->erp_flight_availabilities_detail_id ?>'  value='<?= $flight_availabilities_details_row->safa_externaltriptype_name ?>'/>
                                </td>

                                            --><td>
                                            
                                            	<input type='hidden' name='erp_flight_availabilities_detail_id[<?= $flight_availabilities_details_row->erp_flight_availabilities_detail_id ?>]'  id='erp_flight_availabilities_detail_id[<?= $flight_availabilities_details_row->erp_flight_availabilities_detail_id ?>]'  value='<?= $flight_availabilities_details_row->erp_flight_availabilities_detail_id ?>'/>
                                            
                                                <input type="hidden" name="trip_going_erp_flight_availability_id<?= $flight_availabilities_details_row->erp_flight_availabilities_detail_id ?>" id="trip_going_erp_flight_availability_id<?= $flight_availabilities_details_row->erp_flight_availabilities_detail_id ?>"  value="<?= $flight_availabilities_details_row->erp_flight_availabilities_detail_id ?>" />
												
												
<!--                                                <?php echo $flight_availabilities_details_row->erp_path_type_name; ?>-->
                                                <input type='hidden' name='safa_externaltriptype_name_<?= $flight_availabilities_details_row->erp_flight_availabilities_detail_id ?>'  id='safa_externaltriptype_name_<?= $flight_availabilities_details_row->erp_flight_availabilities_detail_id ?>'  value='<?= $flight_availabilities_details_row->erp_path_type_name ?>'/>
                                                <input type='hidden' name='safa_externaltriptype_id[<?= $flight_availabilities_details_row->erp_flight_availabilities_detail_id ?>]'  id='safa_externaltriptype_id[<?= $flight_availabilities_details_row->erp_flight_availabilities_detail_id ?>]'  value='<?= $flight_availabilities_details_row->safa_externaltriptype_id ?>'/>
                                                
                                            
                                            	<?= form_dropdown('erp_path_type_id['.$flight_availabilities_details_row->erp_flight_availabilities_detail_id.']', $erp_path_types, set_value("erp_path_type_id[".$flight_availabilities_details_row->erp_flight_availabilities_detail_id."]", $flight_availabilities_details_row->erp_path_type_id), 'class="validate[required] erp_path_type" style="width:70px" pathrel="'.$flight_availabilities_details_row->erp_flight_availabilities_detail_id.'" id="erp_path_type_id['.$flight_availabilities_details_row->erp_flight_availabilities_detail_id.']"') ?>
											
                                            </td>

                                            
											
											 
											<td>
											<table style='border:0px;' class="cls_tbl_safa_transporter_code">
											<tr inner_rel='<?= $flight_availabilities_details_row->erp_flight_availabilities_detail_id ?>'><td style='border:0px;'>
											<?= form_dropdown('safa_transporter_code['.$flight_availabilities_details_row->erp_flight_availabilities_detail_id.']', $flight_transporters, set_value("safa_transporter_code['.$flight_availabilities_details_row->erp_flight_availabilities_detail_id.']", $flight_availabilities_details_row->safa_transporter_code), ' flight_states_rel="'.$flight_availabilities_details_row->erp_flight_availabilities_detail_id.'"  class=" chosen-select chosen-rtl validate[required]" style="width:60px" id="safa_transporter_code['.$flight_availabilities_details_row->erp_flight_availabilities_detail_id.']"') ?>
											
											<input type='hidden' name='last_return_safa_transporter_code'  id='last_return_safa_transporter_code'  value='<?= $flight_availabilities_details_row->safa_transporter_code ?>'/>
                                            
											</td>
											<td style='border:0px;'>
											<a href="javascript:void(0);" onclick="add_transporter(2);"  class="btn Fleft " > <span class="icon-plus"></span></a>
											</td>
											</tr>
											</table>
											</td>
											
								             <td><?= form_input('flight_number['.$flight_availabilities_details_row->erp_flight_availabilities_detail_id.']', set_value("flight_number['.$flight_availabilities_details_row->erp_flight_availabilities_detail_id.']", $flight_availabilities_details_row->flight_number ), ' flight_states_rel="'.$flight_availabilities_details_row->erp_flight_availabilities_detail_id.'" class="validate[required]" style="width:40px" id="flightno['.$flight_availabilities_details_row->erp_flight_availabilities_detail_id.']"') ?></td>
								             
                                            
                                            
                                            <td>

                                                                                <!--<input type='hidden' name='departure_date_<?= $flight_availabilities_details_row->erp_flight_availabilities_detail_id ?>'  id='departure_date_<?= $flight_availabilities_details_row->erp_flight_availabilities_detail_id ?>'  value='<?= $start_datetime ?>'/>
                                                <?php echo $start_datetime; ?>
                                                -->
                                                <input type="text" flight_states_rel="<?php echo $flight_availabilities_details_row->erp_flight_availabilities_detail_id; ?>"  class="start_date_<?php echo $flight_availabilities_details_row->erp_flight_availabilities_detail_id; ?>" name='flight_date[<?= $flight_availabilities_details_row->erp_flight_availabilities_detail_id ?>]'  id='flight_date[<?= $flight_availabilities_details_row->erp_flight_availabilities_detail_id ?>]'  value='<?= $flight_availabilities_details_row->flight_date . ' ' . get_time($flight_availabilities_details_row->departure_time) ?>' style="width:100px"/>
<!--                                                <?php echo $flight_availabilities_details_row->flight_date . ' ' . $flight_availabilities_details_row->departure_time; ?>-->
                                            
                                            <script>
                                            $(".start_date_<?php echo $flight_availabilities_details_row->erp_flight_availabilities_detail_id; ?>").datetimepicker({
                                                dateFormat: 'yy-mm-dd',
                                                timeFormat: 'HH:mm',
                                                onClose: function(selectedDate) {
                                                    $('.end_date_<?php echo $flight_availabilities_details_row->erp_flight_availabilities_detail_id; ?>').datetimepicker('option', 'minDate', selectedDate);
                                                }
                                            });
                                        	</script>
                                        	
                                            </td>


											<td>

                                			<!--<input type='hidden' name='departure_date_<?= $flight_availabilities_details_row->erp_flight_availabilities_detail_id ?>'  id='departure_date_<?= $flight_availabilities_details_row->erp_flight_availabilities_detail_id ?>'  value='<?= $start_datetime ?>'/>
                                                <?php echo $start_datetime; ?>
                                                -->
                                                <input type='text' class="end_date_<?php echo $flight_availabilities_details_row->erp_flight_availabilities_detail_id; ?>" arriverel='<?php echo $flight_availabilities_details_row->erp_flight_availabilities_detail_id; ?>' name='flight_arrival_date[<?= $flight_availabilities_details_row->erp_flight_availabilities_detail_id ?>]'  id='flight_arrival_date[<?= $flight_availabilities_details_row->erp_flight_availabilities_detail_id ?>]'  value='<?= $flight_availabilities_details_row->arrival_date . ' ' . get_time($flight_availabilities_details_row->arrival_time) ?>' style="width:100px"/>
<!--                                                <?php echo $flight_availabilities_details_row->arrival_date . ' ' . $flight_availabilities_details_row->arrival_time; ?>-->
                                            
                                            <script>
                                            $(".end_date_<?php echo $flight_availabilities_details_row->erp_flight_availabilities_detail_id; ?>").datetimepicker({
                                                dateFormat: 'yy-mm-dd',
                                                timeFormat: 'HH:mm',
                                                onClose: function(selectedDate) {
                                                    $('.start_date_<?php echo $flight_availabilities_details_row->erp_flight_availabilities_detail_id; ?>').datetimepicker('option', 'maxDate', selectedDate);
                                                }
                                            });
                                        	</script>
                                        	
                                            </td>


                                            <td>
<!--                                                <input type='hidden' name='erp_port_id_from[<?= $flight_availabilities_details_row->erp_flight_availabilities_detail_id ?>]'  id='erp_port_id_from[<?= $flight_availabilities_details_row->erp_flight_availabilities_detail_id ?>]'  value='<?= $flight_availabilities_details_row->erp_port_id_from ?>'/>-->
<!--                                                <input type='hidden' name='erp_port_id_to[<?= $flight_availabilities_details_row->erp_flight_availabilities_detail_id ?>]'  id='erp_port_id_to[<?= $flight_availabilities_details_row->erp_flight_availabilities_detail_id ?>]'  value='<?= $flight_availabilities_details_row->erp_port_id_to ?>'/>-->

												<?= form_dropdown('erp_port_id_from['.$flight_availabilities_details_row->erp_flight_availabilities_detail_id.']', $erp_sa_ports, set_value("erp_port_id_from[".$flight_availabilities_details_row->erp_flight_availabilities_detail_id."]", $erp_port_id_from), ' erp_port_id_from_rel="'.$flight_availabilities_details_row->erp_flight_availabilities_detail_id.'" class="validate[required]" style="width:60px !important" id="erp_port_id_from['.$flight_availabilities_details_row->erp_flight_availabilities_detail_id.']"') ?>
												<?= form_dropdown('erp_port_hall_id_from['.$flight_availabilities_details_row->erp_flight_availabilities_detail_id.']', $erp_sa_ports_halls, set_value("erp_port_hall_id_from[".$flight_availabilities_details_row->erp_flight_availabilities_detail_id."]", $erp_port_hall_id_from), ' erp_port_hall_id_from_rel="'.$flight_availabilities_details_row->erp_flight_availabilities_detail_id.'" class="validate[required]" style="width:120px !important" id="erp_port_hall_id_from['.$flight_availabilities_details_row->erp_flight_availabilities_detail_id.']"') ?>
												<br/>
												<?= form_dropdown('erp_port_id_to['.$flight_availabilities_details_row->erp_flight_availabilities_detail_id.']', $erp_ports, set_value("erp_port_id_to[".$flight_availabilities_details_row->erp_flight_availabilities_detail_id."]", $erp_port_id_to), '   erp_port_id_to_rel="'.$flight_availabilities_details_row->erp_flight_availabilities_detail_id.'" class="validate[required]" style="width:60px !important" id="erp_port_id_to['.$flight_availabilities_details_row->erp_flight_availabilities_detail_id.']"') ?>
<!--												<?= form_dropdown('erp_port_hall_id_to['.$flight_availabilities_details_row->erp_flight_availabilities_detail_id.']', $erp_ports_halls, set_value("erp_port_hall_id_to[".$flight_availabilities_details_row->erp_flight_availabilities_detail_id."]", $erp_port_hall_id_to), ' erp_port_hall_id_to_rel="'.$flight_availabilities_details_row->erp_flight_availabilities_detail_id.'" class="validate[required]" style="width:120px !important" id="erp_port_hall_id_to['.$flight_availabilities_details_row->erp_flight_availabilities_detail_id.']"') ?>-->
												
												
                                                <input type='hidden' name='start_ports_name_<?= $flight_availabilities_details_row->erp_flight_availabilities_detail_id ?>'  id='start_ports_name_<?= $flight_availabilities_details_row->erp_flight_availabilities_detail_id ?>'  value='<?= $flight_availabilities_details_row->start_ports_name; ?>'/>
                                                <input type='hidden' name='end_ports_name_<?= $flight_availabilities_details_row->erp_flight_availabilities_detail_id ?>'  id='end_ports_name_<?= $flight_availabilities_details_row->erp_flight_availabilities_detail_id ?>'  value='<?= $flight_availabilities_details_row->end_ports_name; ?>'/>


<!--                                                <?= $flight_availabilities_details_row->start_ports_name ?> - <?= $flight_availabilities_details_row->end_ports_name ?>-->
                                                <input type='hidden' name='ports_name_<?= $flight_availabilities_details_row->erp_flight_availabilities_detail_id ?>'  id='ports_name_<?= $flight_availabilities_details_row->erp_flight_availabilities_detail_id ?>'  value='<?= $flight_availabilities_details_row->start_ports_name ?> - <?= $flight_availabilities_details_row->end_ports_name ?>'/>


                                                <!--<?php echo $ports; ?>
                                                <input type='hidden' name='ports_name_<?= $flight_availabilities_details_row->erp_flight_availabilities_detail_id ?>'  id='ports_name_<?= $flight_availabilities_details_row->erp_flight_availabilities_detail_id ?>'  value='<?= $ports ?>'/>-->

                                            </td>


                                            <td>
                                                <input type='hidden' name='seats_count[<?= $flight_availabilities_details_row->erp_flight_availabilities_detail_id ?>]'  id='seats_count[<?= $flight_availabilities_details_row->erp_flight_availabilities_detail_id ?>]'  value='<?= $flight_availabilities_details_row->available_seats_count ?>'/>
                                                <input type='text' style="width:50px" name='available_seats_count_text_<?= $flight_availabilities_details_row->erp_flight_availabilities_detail_id ?>'  id='available_seats_count_text_<?= $flight_availabilities_details_row->erp_flight_availabilities_detail_id ?>'  value='<?= $flight_availabilities_details_row->available_seats_count ?>' />

                                                <!-- <?= $value->available_seats_count ?> -->

                                            </td>


											<td><a href="javascript:void(0);" onclick="delete_flights('<?= $flight_availabilities_details_row->erp_flight_availabilities_detail_id ?>')"><span class="icon-trash"></span> </a></td>
         
											</tr>


                                        <?php
                                    }
                                }
                                ?>



                            <? } ?>
                        <? } ?>

                    </tbody>

                </table>
            </div>      
        </div>

		

        <div class="row-fluid" id="div_tbl_hotels">
            <fieldset class="resalt-group">
                <div class="wizerd-div"><a> <?= lang('hotels') ?></a></div>
                
                <?php 
                $safa_reservation_form_ids='';
                
                //By Gouda.
                //if(session('ea_id')&&1==0) {
                if(session('ea_id') || session('uo_id')) {
                	//if($safa_trip_internaltrip_id==0) {	
                ?>
                <?php //if(session('ea_id')) {?>
                <?php if(session('ea_id')&&1==0) {?>
<!--                	<a href="javascript:void(0);" onclick="add_safa_reservation_forms(0);" id="lnk_add_safa_reservation_forms" class="btn Fleft " > <span class="icon-plus"></span></a>-->
						<a href="javascript:void(0);" onclick="add_safa_reservation_forms(0);" id="lnk_add_safa_reservation_forms" class="btn Fleft " > <span class="icon-plus"></span></a>
				<?php }?>
                
                <?php 
                	//} else {
                		if(isset($item_safa_reservation_forms_rows)) {
                			if(count($item_safa_reservation_forms_rows)>0) {
                			echo " <table> <tr> <td colspan='5' align='center'> <span class='FRight'>".lang('accommodation_forms')."</span> </td></tr>";
                	?>
                	
                	<tr>
                	<td><?php echo lang('trip_code');?></td>
                	<td><?php echo lang('period');?></td>
                	<td><?php echo lang('the_hotels');?></td>
                	<td><?php echo lang('passports_count');?></td>
                	<td><?php echo lang('global_actions');?></td>
                	</tr>
                	
                <?php		
		                $row_class = '';
						$previous_row_class = '';
						
						$item_safa_reservation_forms_colors_arr=array();
						
                		foreach($item_safa_reservation_forms_rows as $item_safa_reservation_forms_row) {
                			$safa_reservation_form_ids = $safa_reservation_form_ids.','.$item_safa_reservation_forms_row->safa_reservation_form_id;
                			
                			$this->safa_passport_accommodation_rooms_model->safa_reservation_form_id = $item_safa_reservation_forms_row->safa_reservation_form_id;
							$accommodation_hotels_rows = $this->safa_passport_accommodation_rooms_model->get_hotels();
	                		
							$accommodation_hotels_names='';
							foreach($accommodation_hotels_rows as $accommodation_hotels_row) {
								$accommodation_hotels_names = $accommodation_hotels_names.$accommodation_hotels_row->erp_hotel_name." ( ".$accommodation_hotels_row->erp_city_name." )<br/>";
							}
							
							$safa_group_passport_accommodation_rows = $this->safa_group_passport_accommodation_model->get_distinct_passports_by_trip_uo_form(false, false, $item_safa_reservation_forms_row->safa_reservation_form_id);
							
							$safa_group_passport_accommodation_count = count($safa_group_passport_accommodation_rows);
							
							
							//---------------- Colors ----------------------------------
							$row_class_arr = array('success','info','warning');
							$row_class = $row_class_arr[array_rand($row_class_arr)];
							if($previous_row_class==$row_class) {
								$row_class='';
							}
							$previous_row_class = $row_class;
							
							$item_safa_reservation_forms_colors_arr[$item_safa_reservation_forms_row->safa_reservation_form_id]= $row_class;
                ?>	
                
                        
                    <tr>
                	<td class="<?php echo $row_class;?>"><?php echo $item_safa_reservation_forms_row->safa_trip_name.' / '. $item_safa_reservation_forms_row->safa_package_name; ?></td>
                	<td class="<?php echo $row_class;?>"><?php echo $item_safa_reservation_forms_row->safa_package_period_name; ?></td>
                	<td class="<?php echo $row_class;?>"><?php echo $accommodation_hotels_names; ?></td>
                	<td class="<?php echo $row_class;?>"><?php echo $safa_group_passport_accommodation_count; ?></td>
                	<td class="<?php echo $row_class;?>">
					<!-- <a target="_blank" href="<?php echo site_url('ea/passport_accommodation_rooms/manage').'/'.$item_safa_reservation_forms_row->safa_reservation_form_id;?>"   class="btn Fleft " > <?php echo $item_safa_reservation_forms_row->safa_trip_name;?> </a>-->
					<a href="javascript:void(0);" onclick="add_safa_reservation_forms(<?php echo $item_safa_reservation_forms_row->safa_reservation_form_id;?>);" id="lnk_add_safa_reservation_forms"  ><span class="icon-pencil"></span></a>
					</td>
                	</tr>
                	
                	<?php	
                			}
                			echo " </table>";
                		}
                		}
                		
                	//}
	                } 
				?>
				<input type='hidden' name='safa_reservation_form_ids'  id='safa_reservation_form_ids'  value='<?php echo $safa_reservation_form_ids;?>'/>
        		
        		
                <table id='div_hotels'>
                    <thead>
                    <tr><th colspan="6"><?= lang('the_hotels')   ?></th></tr>
                        <tr>
                            <th><?= lang('city')   ?></th>
                            <th><?= lang('hotel') ?></th>
                            <th><?= lang('entry_date')  ?></th>
                            <th><?= lang('nights_count') ?></th>
                            <th><?= lang('exit_date')  ?>
	                        <input type='hidden' name='last_exit_date'  id='last_exit_date'  value=''/>
	                        </th>
						
						<?php //if(session('uo_id') || session('ito_id')) {?>
						<?php if(session('uo_id') || session('ito_id') || session('ea_id')) {?>
						
							<?php //if($safa_trip_internaltrip_id==0) { ?>
                            <th><a href="javascript:void(0)" onclick="new_hotel();" class="btn" > <?php echo lang('global_add') ?> </a></th>
							<?php //} ?>
						<?php } ?>	
                        </tr>
                    </thead>
                    <tbody class="internalhotels">
                    
                                       
					<?php 
					$internalpassages_hotels_count = 0;
					
					$internalpassages_hotels_counter=1;
					if((session('uo_id')&& count($item_safa_reservation_forms_rows)==0) || session('ito_id') || (session('ea_id') && count($item_safa_reservation_forms_rows)==0)) {
					if(isset($internalpassages_hotels)){
						$internalpassages_hotels_count = count($internalpassages_hotels);
					
						//if($internalpassages_hotels_count>0) {
						
								
						foreach($internalpassages_hotels as $internalpassages_hotel) {
							
							//$this->internalpassages_model->safa_internalsegment_id = $internalpassages_hotel->safa_internalsegment_id+1;
                            //$next_internalpassages_row = $this->internalpassages_model->get();
                            //$this->internalpassages_model->safa_internalsegment_id = false;
                            
							$next_internalpassages_row = array();
							if(isset($internalpassages_hotels[$internalpassages_hotels_counter])) {
								$next_internalpassages_row = $internalpassages_hotels[$internalpassages_hotels_counter];
							}
							
                            //if(count($next_internalpassages_row)>0) {
                            
							//By Gouda.
							//if(session('uo_id') || session('ito_id')) {
							//if(session('uo_id') || session('ito_id') || session('ea_id')) {
					?>
					
					<tr hrel="<?php echo $internalpassages_hotel->safa_internalsegment_id ?>">
                                    <td>
                                    <input type='hidden' name='hotel_safa_internalsegment_id[<?= $internalpassages_hotel->safa_internalsegment_id ?>]'  id='hotel_safa_internalsegment_id[<?= $internalpassages_hotel->safa_internalsegment_id ?>]'  value='<?php echo $internalpassages_hotel->safa_internalsegment_id;?>'/>
                                    
                                        <?php echo form_dropdown('erp_city_id[' . $internalpassages_hotel->safa_internalsegment_id . ']', $cities, set_value('erp_city_id', $internalpassages_hotel->erp_city_id), ' cityrel="'.$internalpassages_hotel->safa_internalsegment_id.'" class="validate[required] chosen-select chosen-rtl input-full"  tabindex="4" id="erp_city_id[' . $internalpassages_hotel->safa_internalsegment_id . ']" data-placeholder="' . lang('global_all') . '"') ?>
            						
<!--            						<?php echo $internalpassages_hotel->erp_city_name; ?>-->
            						</td>
                                    <td>
                                        
                                         
                                      <table style='border:0px;'><tr inner_hrel="<?php echo $internalpassages_hotel->safa_internalsegment_id ?>"><td style='border:0px;'>
										<?php 
										$row_hotels = ddgen('erp_hotels', array('erp_hotel_id', name()), array('erp_city_id' => $internalpassages_hotel->erp_city_id), array(name(), 'ASC'), TRUE);
							                                        
										echo form_dropdown('erp_hotel_id[' . $internalpassages_hotel->safa_internalsegment_id . ']', $row_hotels, set_value('erp_hotel_id', $internalpassages_hotel->erp_end_hotel_id), ' hotelrel="' . $internalpassages_hotel->safa_internalsegment_id . '" class=" chosen-select chosen-rtl input-full erp_hotel_id"  tabindex="4" id="erp_hotel_id[' . $internalpassages_hotel->safa_internalsegment_id . ']" ') 
										?>
									</td><td style='border:0px;'>
									<a href="javascript:void(0);" onclick="add_hotel(<?php echo $internalpassages_hotel->safa_internalsegment_id ?>);"  class="btn Fleft " > <span class="icon-plus"></span></a>
									</td></tr>
									</table>  
                                         
                                         <!--<input type='hidden' name='erp_hotel_id[<?php echo $internalpassages_hotel->safa_internalsegment_id;?>]'  id='erp_hotel_id[<?php echo $internalpassages_hotel->safa_internalsegment_id;?>]'  value='<?php echo $internalpassages_hotel->erp_end_hotel_id;?>' class='erp_hotel_id' hotelrel="<?php echo $internalpassages_hotel->safa_internalsegment_id;?>" />
                    
                                        <?php echo $internalpassages_hotel->erp_hotel_name; ?>
            						
                                    -->
                                    </td>
                                 
                                    <td>
                                    
                                     
                                        <?php
                                        echo form_input('arrival_date[' . $internalpassages_hotel->safa_internalsegment_id . ']'
                                                , set_value('arrival_date[' . $internalpassages_hotel->safa_internalsegment_id . ']', date('Y-m-d',strtotime($internalpassages_hotel->end_datetime)))
                                                , 'class="input-huge ' . $internalpassages_hotel->safa_internalsegment_id . '" id="arrival_date[' . $internalpassages_hotel->safa_internalsegment_id . ']" startrel="' . $internalpassages_hotel->safa_internalsegment_id . '" style="width:120px"  readonly="readonly"  placeholder="' . lang('entry_date') . '"')
                                        ?>

                                        <script>
                                            $(".arrival_date<?php echo $internalpassages_hotel->safa_internalsegment_id; ?>").datepicker({
                                                dateFormat: 'yy-mm-dd',
                                                controlType: 'select',
                                                timeFormat: 'HH:mm',
                                                onClose: function(selectedDate) {
                                                    $('.arrival_date<?php echo $internalpassages_hotel->safa_internalsegment_id; ?>').datepicker('option', 'minDate', selectedDate);
                                                }
                                            });
                                        </script>
                                         
<!--                                        <?php echo date('Y-m-d',strtotime($internalpassages_hotel->end_datetime)); ?>-->
            						
										</td>
										
										<td>
                                        <?php 
                                        
                                        
                                        $nights_count=0;
                                        if(count($next_internalpassages_row)>0) {
	                                        if (strlen($internalpassages_hotel->end_datetime) && strlen($next_internalpassages_row->end_datetime)) {
	                                        			$enddate = explode(' ',$next_internalpassages_row->end_datetime);
                                                        $startdate = explode(' ',$internalpassages_hotel->end_datetime);
                                                        
	                                             $nights_count = ceil((strtotime($enddate[0]) - strtotime($startdate[0])) / (60 * 60 * 24)); 
	                                        } 
                                        } else {
                                        	$this->internalpassages_model->safa_internalsegmenttype_id = 2;
                            				$leaving_internalpassages_rows = $this->internalpassages_model->get();
                                                        $enddate = explode(' ',$leaving_internalpassages_rows[0]->end_datetime);
                                                        $startdate = explode(' ',$internalpassages_hotel->end_datetime);
                            				if(count($leaving_internalpassages_rows)>0) {
                            					$nights_count = ceil((strtotime($enddate[0]) - strtotime($startdate[0])) / (60 * 60 * 24));
                            				} 
                                        }
                                         ?>
                                         
                                          
                                        <?php
                                        echo form_input('nights[' . $internalpassages_hotel->safa_internalsegment_id . ']'
                                                , set_value('nights[' . $internalpassages_hotel->safa_internalsegment_id . ']', $nights_count)
                                                , 'class="input-huge" style="width:100px" nightsrel="' . $internalpassages_hotel->safa_internalsegment_id . '" onchange="fix_dates();" ')
                                        ?>
                                         
                                         
<!--                                        <?php echo $nights_count; ?>-->
            						
                                    	</td>
										
										<td>
										
										<?php 
										$current_row_exit_date='';
                                        if(count($next_internalpassages_row)>0) {
                                        	$current_row_exit_date = date('Y-m-d',strtotime($next_internalpassages_row->end_datetime));
										} else if(isset($leaving_internalpassages_rows)) {
											if(count($leaving_internalpassages_rows)>0) {
												$current_row_exit_date = date('Y-m-d',strtotime($leaving_internalpassages_rows[0]->end_datetime));
											}
										}
										
										
										//By Gouda.
										
										$current_row_exit_date = date('Y-m-d', strtotime($internalpassages_hotel->end_datetime . ' + ' . $nights_count . ' days')); 
										
										if($internalpassages_hotels_counter==$internalpassages_hotels_count) {
										?>
										<input type='hidden' name='last_exit_date'  id='last_exit_date'  value='<?= $current_row_exit_date ?>'/>
                                        <?php } ?>
                                        
                                        <?php
                                        
                                        
                                        echo form_input('exit_date[' . $internalpassages_hotel->safa_internalsegment_id . ']'
                                                , set_value('exit_date[' . $internalpassages_hotel->safa_internalsegment_id . ']', $current_row_exit_date)
                                                , 'class="input-huge ' . $internalpassages_hotel->safa_internalsegment_id . '" id="exit_date[' . $internalpassages_hotel->safa_internalsegment_id . ']" endrel="' . $internalpassages_hotel->safa_internalsegment_id . '" style="width:120px" readonly="readonly" placeholder="' . lang('exit_date') . '"')
                                        ?>

                                        <script>
                                            $(".exit_date<?php echo $internalpassages_hotel->safa_internalsegment_id; ?>").datepicker({
                                                dateFormat: 'yy-mm-dd',
                                                controlType: 'select',
                                                timeFormat: 'HH:mm',
                                                onClose: function(selectedDate) {
                                                    $('.exit_date<?php echo $internalpassages_hotel->safa_internalsegment_id; ?>').datepicker('option', 'maxDate', selectedDate);
                                                }
                                            });
                                        </script>
                                         
                                         
<!--                                        <?php echo $current_row_exit_date; ?>-->
            						
                                    </td>

									<?php //if(session('uo_id') || session('ito_id') ) {?>
									<?php if(session('uo_id') || session('ito_id') || session('ea_id')) {?>
									
                                    <?php //if($safa_trip_internaltrip_id==0) { ?>
                                    <td class="TAC">
                                        <a href="javascript:void(0)" onclick="remove_hrow(<?php echo $internalpassages_hotel->safa_internalsegment_id ?>)"><span class="icon-trash"></span></a>
                                    </td>
                                    <?php //} ?>
                                    <?php } ?>
                                    
                                </tr>
					
					
					
					
					
							<tr rel="<?php echo $internalpassages_hotel->safa_internalsegment_id ?>">
                                <td colspan="6">
                                    <table >
                                        <tr>
                                            <td rowspan="2">
                                                <span class="FRight"><?= lang('rooms_count') ?></span>
                                            </td>
                                            
                                            <? if (check_array($erp_hotelroomsizes)) { ?>
                                                <?php foreach ($erp_hotelroomsizes as $erp_hotelroomsize) { ?>
                                                    <td>
                                                    	<?php 
                                                    	$current_hotels_rooms_count=0;
                                                    	foreach($safa_trip_internaltrips_hotels_rows as $safa_trip_internaltrips_hotels_row) {
                                                    		//echo $safa_trip_internaltrips_hotels_row->safa_internalsegment_id.' - '. $internalpassages_hotel->safa_internalsegment_id .' - '. $safa_trip_internaltrips_hotels_row->erp_hotelroomsize_id.' - '. $erp_hotelroomsize->erp_hotelroomsize_id.'<br/>';
                                                    		if($safa_trip_internaltrips_hotels_row->erp_hotel_id== $internalpassages_hotel->erp_end_hotel_id && $safa_trip_internaltrips_hotels_row->erp_hotelroomsize_id== $erp_hotelroomsize->erp_hotelroomsize_id) {
                                                    			$current_hotels_rooms_count = $safa_trip_internaltrips_hotels_row->rooms_count ;
                                                    		} 
                                                    	}
                                                    	?>
                                                    	<?= $erp_hotelroomsize->{name()} ?> <?=
                                                        form_input('hotels_rooms_count[' . $internalpassages_hotel->safa_internalsegment_id . '][' . $erp_hotelroomsize->erp_hotelroomsize_id . ']'
                                                                , set_value('hotels_rooms_count[' . $internalpassages_hotel->safa_internalsegment_id . '][' . $erp_hotelroomsize->erp_hotelroomsize_id . ']', $current_hotels_rooms_count )
                                                                , 'class="input-small FRight" style="width: 50px !important;"  rpnrel="' . $erp_hotelroomsize->erp_hotelroomsize_id . '" placeholder="' . $erp_hotelroomsize->{name()} . '" id="hotels_rooms_count_' . $internalpassages_hotel->safa_internalsegment_id . '_' . $erp_hotelroomsize->erp_hotelroomsize_id . '" ')
                                                        ?>
                                                    </td>
                                                <? } ?>
                                            <? } ?>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            
                            <?php 
						$internalpassages_hotels_counter++;
						} 
						}
						} else if(session('ea_id') || session('uo_id')) {
							
							$row_class = '';
	
							if(isset($item_safa_reservation_forms_rows)) {
	                		if(count($item_safa_reservation_forms_rows)>0) {
                			
                			$accommodation_hotels_rows_count=0;
                			$accommodation_hotels_rows_counter=0;
                			
                			foreach($item_safa_reservation_forms_rows as $item_safa_reservation_forms_row) {
                				
                			if(isset($item_safa_reservation_forms_colors_arr[$item_safa_reservation_forms_row->safa_reservation_form_id])) {
                				$row_class = $item_safa_reservation_forms_colors_arr[$item_safa_reservation_forms_row->safa_reservation_form_id];
                			}	
                				
							$this->safa_passport_accommodation_rooms_model->safa_reservation_form_id = $item_safa_reservation_forms_row->safa_reservation_form_id;
							$accommodation_hotels_rows = $this->safa_passport_accommodation_rooms_model->get_hotels();
							
							
							if(count($accommodation_hotels_rows)>0) {
								$accommodation_hotels_rows_count = $accommodation_hotels_rows_count+count($accommodation_hotels_rows);
								foreach($accommodation_hotels_rows as $accommodation_hotels_row) {
								
					?>
					
					<tr class="table-row" hrel="<?php echo $accommodation_hotels_rows_counter; ?>">
	                   <td class="<?php echo $row_class;?>">
	                   <input type='hidden' name='hotel_safa_internalsegment_id[<?php echo $accommodation_hotels_rows_counter; ?>]'  id='hotel_safa_internalsegment_id[<?php echo $accommodation_hotels_rows_counter; ?>]'  value='0'/>
	                   <input type='hidden' name='erp_city_id[<?php echo $accommodation_hotels_rows_counter; ?>]'  id='erp_city_id[<?php echo $accommodation_hotels_rows_counter; ?>]'  value='<?php echo $accommodation_hotels_row->erp_city_id; ?>' cityrel="<?php echo $accommodation_hotels_rows_counter; ?>"/>
	                   <?php echo $accommodation_hotels_row->erp_city_name; ?>
	          			</td>
	          			
	                   <td class="<?php echo $row_class;?>">
	                   <table style='border:0px;'><tr inner_hrel="<?php echo $accommodation_hotels_rows_counter; ?>"><td style='border:0px;'  class="<?php echo $row_class;?>">
	          			<input type='hidden' name='erp_hotel_id[<?php echo $accommodation_hotels_rows_counter; ?>]'  id='erp_hotel_id[<?php echo $accommodation_hotels_rows_counter; ?>]'  value='<?php echo $accommodation_hotels_row->erp_hotel_id; ?>' class='erp_hotel_id' hotelrel="<?php echo $accommodation_hotels_rows_counter; ?>" />
						</td></tr></table>
	                    <?php echo $accommodation_hotels_row->erp_hotel_name; ?>
	          			</td>
	
	          			<td class="<?php echo $row_class;?>">
	          			<input type='hidden' name='arrival_date[<?php echo $accommodation_hotels_rows_counter; ?>]'  id='arrival_date[<?php echo $accommodation_hotels_rows_counter; ?>]'  value='<?php echo date('Y-m-d',strtotime($accommodation_hotels_row->from_date)); ?>' startrel="<?php echo $accommodation_hotels_rows_counter; ?>"/>
	                    <?php echo date('Y-m-d',strtotime($accommodation_hotels_row->from_date)); ?>
	          			</td>
	
	          			<td class="<?php echo $row_class;?>">
	          			
	          			<?php 
                       $nights_count = ceil((strtotime(date('Y-m-d',strtotime($accommodation_hotels_row->to_date))) - strtotime(date('Y-m-d',strtotime($accommodation_hotels_row->from_date)))) / (60 * 60 * 24));
                        ?>
	          			
	          			<input type='hidden' name='nights[<?php echo $accommodation_hotels_rows_counter; ?>]'  id='nights[<?php echo $accommodation_hotels_rows_counter; ?>]'  value='<?php echo $nights_count; ?>' nightsrel="<?php echo $accommodation_hotels_rows_counter; ?>"/>
	                    <?php echo $nights_count; ?>
	          			</td>
	          					
	          			<td class="<?php echo $row_class;?>">
	          			<input type='hidden' name='exit_date[<?php echo $accommodation_hotels_rows_counter; ?>]'  id='exit_date[<?php echo $accommodation_hotels_rows_counter; ?>]'  value='<?php echo date('Y-m-d',strtotime($accommodation_hotels_row->to_date)); ?>' endrel="<?php echo $accommodation_hotels_rows_counter; ?>"/>
	                    
	                    <?php 
						if(($accommodation_hotels_rows_counter+1)==$accommodation_hotels_rows_count) {
						?>
						<input type='hidden' name='last_exit_date'  id='last_exit_date'  value='<?= date('Y-m-d',strtotime($accommodation_hotels_row->to_date)) ?>'/>
                        <?php }?>
	                    
	                    <?php echo date('Y-m-d',strtotime($accommodation_hotels_row->to_date)); ?>
	          			</td>
	
						<td class="<?php echo $row_class;?>"></td>
	                   
	                   </tr>
	
	                   <tr rel="<?php echo $accommodation_hotels_rows_counter; ?>">
	          		   <td colspan="6"  class="<?php echo $row_class;?>">
	                   <table >
	                   <tr inner_hrel='<?php echo $accommodation_hotels_rows_counter; ?>'>
	                   <td  class="<?php echo $row_class;?>">
	                   <span class="FRight"><?= lang('rooms_count') ?></span>
	                   </td>
	          		
	          		<? 
	          		if (check_array($erp_hotelroomsizes)) {
	          		foreach ($erp_hotelroomsizes as $erp_hotelroomsize) {
	          			?>
	          		<?php 
                    $current_hotels_rooms_count=0;
                    foreach($safa_trip_internaltrips_hotels_rows as $safa_trip_internaltrips_hotels_row) {
                        if($safa_trip_internaltrips_hotels_row->erp_hotel_id== $accommodation_hotels_row->erp_hotel_id && $safa_trip_internaltrips_hotels_row->erp_hotelroomsize_id== $erp_hotelroomsize->erp_hotelroomsize_id) {
                           	$current_hotels_rooms_count = $safa_trip_internaltrips_hotels_row->rooms_count ;
                         } 
                    }
                   	?>	
	          		<td  class="<?php echo $row_class;?>">
	          		<?= $erp_hotelroomsize->{name()} ?>
	          		<input type='hidden' name='hotels_rooms_count[<?php echo $accommodation_hotels_rows_counter; ?>][<?php echo $erp_hotelroomsize->erp_hotelroomsize_id; ?>]'    value='<?php echo $current_hotels_rooms_count;?>' class='input-small FRight' pkgid='<?php echo $accommodation_hotels_rows_counter; ?>' id="hotels_rooms_count_<?php echo $accommodation_hotels_rows_counter; ?>_<?php echo $erp_hotelroomsize->erp_hotelroomsize_id; ?>" rpnrel="<?php echo $erp_hotelroomsize->erp_hotelroomsize_id; ?>" style="width: 50px !important;"/>
	          		</td>
	          		<? } 
	          		 } 
	          		 ?>
	          		</tr>
	          		</table>
	           		</td>
	                </tr>
	                
					<?php 
					
					$accommodation_hotels_rows_counter++;
						}
						
							}
                			}
	                		}
                			}
					}
					?>
                    </tbody>
                </table>
            </fieldset>
        </div>

        <div class="row-fluid" id="div_tbl_tourismplaces">
            <fieldset class="resalt-group">
                <div class="wizerd-div"><a> <?= lang('tourismplaces') ?></a></div>
                <table id='tourismplaces'>
                    <thead>
                        <tr>
                            <th><?= lang('tourismplace')   ?></th>
                            <th><?= 
                                lang('the_date')
                        ?></th>
                            
                            <?php //if($safa_trip_internaltrip_id==0) {?>
                            <th><a href="javascript:void(0)" onclick="new_tourismplace();" class="btn" > <?php echo lang('global_add') ?> </a></th>
							<?php //} ?>
                        </tr>
                    </thead>
                    <tbody class="tourismplaces">

				<?php 
					$internalpassages_tourismplaces_count = 0;
					
					$internalpassages_tourismplaces_counter=1;
					if(isset($internalpassages_tourismplaces)){
						$internalpassages_tourismplaces_count = count($internalpassages_tourismplaces);
					
						foreach($internalpassages_tourismplaces as $internalpassages_tourismplace) {
							
							$this->internalpassages_model->safa_internalsegment_id = $internalpassages_tourismplace->safa_internalsegment_id+1;
                            $next_internalpassages_row = $this->internalpassages_model->get();
                            $this->internalpassages_model->safa_internalsegment_id = false;
                            
                            //if(count($next_internalpassages_row)>0) {
					?>
					
					<tr trel="<?php echo $internalpassages_tourismplace->safa_internalsegment_id ?>">
                                    
                                    <td>
                                    <input type='hidden' name='tourism_safa_internalsegment_id[<?= $internalpassages_tourismplace->safa_internalsegment_id ?>]'  id='tourism_safa_internalsegment_id[<?= $internalpassages_tourismplace->safa_internalsegment_id ?>]'  value='<?php echo $internalpassages_tourismplace->safa_internalsegment_id;?>'/>
                                      
                                     <?php 
							            echo '       <select name="safa_tourismplace_id['.$internalpassages_tourismplace->safa_internalsegment_id.']" class="validate[required] chosen-select chosen-rtl safa_tourismplace_id"  tourismrel="'.$internalpassages_tourismplace->safa_internalsegment_id.'" id="safa_tourismplace_id['.$internalpassages_tourismplace->safa_internalsegment_id.']" >';
							            foreach($safa_tourismplaces as $key => $value){
							            	$key_arr = explode('---',$key);
							            	$safa_tourismplace_id = $key_arr[0];
							            	$erp_city_id = $key_arr[1];
							            
							            	$selected='';
							            	if($safa_tourismplace_id== $internalpassages_tourismplace->safa_tourism_place_id) {
							            		$selected ='selected';
							            	}
							            	
							            echo "           <option value=\"$safa_tourismplace_id\" city_id=\"$erp_city_id\" $selected>$value</option>";
							            } 
							            echo "    </select>";
							         ?> 
									 
<!--									 <?php echo $internalpassages_tourismplace->safa_tourismplace_name; ?>-->
                                    </td>
                                 
                                    <td>
                                         
									<?php
                                        echo form_input('tourism_date[' . $internalpassages_tourismplace->safa_internalsegment_id . ']'
                                                , set_value('tourism_date[' . $internalpassages_tourismplace->safa_internalsegment_id . ']', date('Y-m-d H:i',strtotime($internalpassages_tourismplace->start_datetime)))
                                                , 'class="validate[required] datetime " id="tourism_date[' . $internalpassages_tourismplace->safa_internalsegment_id . ']" tourdaterel="' . $internalpassages_tourismplace->safa_internalsegment_id . '" style="width:150px" ')
                                        ?>

                                        <script>

                                        var selectrel = $('select[cityrel] option[value=<?php echo $internalpassages_tourismplace->erp_city_id;?>]:selected').parent().attr('cityrel');
                                        //alert($('#tourism_date\\[<?php echo $internalpassages_tourismplace->safa_internalsegment_id;?>\\]').val());
                                        $('#tourism_date\\[<?php echo $internalpassages_tourismplace->safa_internalsegment_id;?>\\]').datetimepicker({
                                            dateFormat: 'yy-mm-dd',
                                            //minDate: new Date($('input[startrel=' + selectrel + ']').val()),
                                            //maxDate: new Date($('input[endrel=' + selectrel + ']').val())
                                        });
                                        
                                        </script>
                                         
<!--                                         <?php echo date('Y-m-d H:i',strtotime($internalpassages_tourismplace->start_datetime));?>-->
										</td>
										
										
										

                                    <?php //if($safa_trip_internaltrip_id==0) {?>
                                    <td class="TAC">
                                        <a href="javascript:void(0)" onclick="remove_trow(<?php echo $internalpassages_tourismplace->safa_internalsegment_id ?>)"><span class="icon-trash"></span></a>
                                    </td>
                                    <?php //} ?>
                                    
                                </tr>
					
					<?php 
					$internalpassages_tourismplaces_counter++;
						}
					}
					?>
					
                    </tbody>
                </table>
            </fieldset>
        </div>



<div class="row-fluid">

<fieldset class="resalt-group">
  <div class="wizerd-div"><a> <?= lang('internal_transportation') ?></a></div>

        
        <div class="row-form" >
        <?php 
        foreach($safa_internaltrip_types as $key=>$value) {
        	if(session('ea_id')) {
        		if($key!=3) {
        ?>
        
       	<div class="span6">
       	<?php if($safa_internaltrip_type_id==0 && $key==1) {?>
       	<?= form_radio('safa_internaltrip_type_id', $key,  TRUE , " id='safa_internaltrip_type_id'  class='validate[required]' ") ?> <?= $value ?>
       	<?php } else {?>
         <?= form_radio('safa_internaltrip_type_id', $key, ($safa_internaltrip_type_id == $key) ? TRUE : FALSE, " id='safa_internaltrip_type_id'  class='validate[required]' ") ?> <?= $value ?>
         <?php }?>
        </div>
        
        <?php 
        		}
        	} else if(session('uo_id')) {
        		if($key!=2) {
        ?>
        	
        <div class="span6">
       	<?php if($safa_internaltrip_type_id==0 && $key==1) {?>
       	<?= form_radio('safa_internaltrip_type_id', $key,  TRUE , " id='safa_internaltrip_type_id'  class='validate[required]' ") ?> <?= $value ?>
       	<?php } else {?>
         <?= form_radio('safa_internaltrip_type_id', $key, ($safa_internaltrip_type_id == $key) ? TRUE : FALSE, " id='safa_internaltrip_type_id'  class='validate[required]' ") ?> <?= $value ?>
         <?php }?>
        </div>
        <?php		
        		}
        	}
        }
        ?>
        
        </div>
        
       <div class="row-form" >
       <?php 
                        $ito_validate_calss='';
                        $ito_display_style='none';
                        if($safa_trip_internaltrip_id==0) {
                        	$ito_validate_calss =" validate[required]";
                        	$ito_display_style='inline';
                        } else if($safa_internaltrip_type_id==1) {
                        	$ito_validate_calss =" validate[required]";
                        	$ito_display_style='inline';
                        }
                        ?>
                        
       
       <?php if  (session('uo_id') || session('ea_id')) { ?>
       <div class="span6" >
                <div class="span4">
                        <?= lang("transport_request_opertator") ?>
				<font style='color:red; display: <?php echo $ito_display_style;?>' id="fnt_transport_request_opertator" >*</font>
                    </div>
                    
                    <div class="span8" >
                        <?= form_dropdown("safa_ito_id", $safa_ito, set_value("safa_ito_id", $safa_ito_id), " id='safa_ito_id' class=' $ito_validate_calss chosen-select chosen-rtl input-huge '") ?>
                        <span class="bottom" style='color:red' >
                            <?= form_error('safa_ito_id') ?> 
                        </span>
                </div>
        </div>
        <?php } ?>
        
        
        <div class="span6">
                        <div class="span4"><?= lang("transport_request_transportername") ?>
                        
                        </div>
                        <div class="span8" >
                        
                            <?= form_dropdown("safa_transporter_id", $bus_transporters, set_value("safa_transporter_id", $safa_transporter_id), "  id='safa_transporter_id' class=' chosen-select chosen-rtl'") ?>
                            <span class="bottom" style='color:red' >
                                <?= form_error('safa_transporter_id') ?> 
                            </span>
                        </div>
                    </div>
        
        
        </div>
        
        <div class="row-form" >
                <div class="span6">
                    <div class="span4">
                        <?= lang("transport_request_res_code") ?>
                        <font style='color:red; display: <?php echo $ito_display_style;?>' id="fnt_transport_request_res_code">*</font>
                    </div>
                    <div class="span8" >
                        <?= form_input("operator_reference", set_value("operator_reference", $operator_reference), " id='operator_reference'  class=' $ito_validate_calss' ") ?>
                        <span class="bottom" style='color:red' >
                            <?= form_error('operator_reference') ?> 
                        </span>
                    </div>
            	</div> 
                
                <div class="span6" >
                    <div class="span4" ><?= lang('confirmation_number') ?>
                    <font style='color:red;  display: <?php echo $ito_display_style;?>' id="fnt_confirmation_number">*</font>
                    </div>
                    <div class="span8" >

                        <?= form_input("confirmation_number", set_value("confirmation_number", $confirmation_number) , " id='confirmation_number' class=' $ito_validate_calss' ") ?>
                        <span class="bottom" style='color:red' id="spn_confirmation_number_validate">
                            <?= form_error('confirmation_number') ?> 
                        </span>
                    </div>
                </div>
            
            </div>    
            
        <div class="row-form" >    
        
            
            <div class="span12" >
                    <div class="span2" ><?= lang("transport_request_res_file") ?></div>
                    <div class="span4">
                        <input type="file" value="" name="transport_request_res_file"/>
                        <span class="bottom" style='color:red' >
                            <?= form_error('transport_request_res_file') ?> 
                        </span>
                    </div>
                    <?php if(isset($item)) { ?>
                   <div class="span6">
                    	<a href="<?= base_url('static/temp/uo_files/' . session('internaltrip_file_upload_time') . $item->attachement) ?>" target="_blank"  ><?= $item->attachement ?></a>
                    </div> 
                    <?php }?>
             </div>  
            
         </div>
            
        
        <div class="row-form" >  
        <script>drivers_and_buses_counter = 0</script>
        <table cellpadding="0" cellspacing="0" id="tbl_drivers_and_buses">
                <thead>
                    <tr>
                    	<!-- 
                        <th><?php echo  lang('driver') ?></th>
                        <th><?php echo  lang('driver_data') ?></th>
                        <th><?php echo  lang('bus') ?></th>
                        <th><?php echo  lang('bus_data') ?></th>
                         -->
                        
                        <th><?php echo  lang('bus_type') ?></th>
                        <th><?php echo  lang('bus_model') ?></th>
                        <th><?php echo  lang('bus_count') ?></th>
                         
                        <?php //if($safa_trip_internaltrip_id==0) {?>
                        <th><?php echo  lang('passengers_count') ?></th>
                        <th><a class="btn Fleft" title="<?php echo  lang('global_add') ?>" href="javascript:void(0)" onclick="add_drivers_and_buses('N' + drivers_and_buses_counter++); load_multiselect()" id="lnk_add_drivers_and_buses" >
				        <?php echo  lang('global_add') ?>    
			</a></th>
                        <?php 
                       //}
                        ?>
                    </tr>
                </thead>
                <tbody class="drivers_and_buses" id='drivers_and_buses'>
                <? if(isset($item_drivers_and_buses)) { ?>
                    <? if(check_array($item_drivers_and_buses)) { 
                     $item_drivers_and_buses_counter=0;
                     foreach($item_drivers_and_buses as $item_driver_and_bus) { 
                    $item_drivers_and_buses_counter++;
                    ?>
                    <tr rel="<?php echo  $item_drivers_and_buses_counter ?>">
                        
                         
                        <td>
			                  <table style='border:0px;'><tr rel="<?php echo $item_drivers_and_buses_counter; ?>"><td style='border:0px;'> 
			                  <select name="safa_buses_brands[<?php echo $item_drivers_and_buses_counter; ?>]" class="chosen-select chosen-rtl input-full  validate[required] safa_buses_brands" id="safa_buses_brands_<?php echo $item_drivers_and_buses_counter; ?>" tabindex="4">
			            <? foreach($safa_buses_brands as $key => $value){ 
			            	$selected='';
				            if($key==$item_driver_and_bus->safa_buses_brands_id) {
				            	$selected='selected';
				            }
			            ?>
			                       <option value="<?= $key ?>" <?php echo $selected;?>><?= $value ?></option>
			            <? } ?>
			               </select>
			            </td><td style='border:0px;'><a href='javascript:void(0);' onclick='add_bus_brand("<?php echo $item_drivers_and_buses_counter; ?>");'  class='btn Fl"<?php echo $item_drivers_and_buses_counter; ?>"isplay:inline-block;'> <span class='icon-plus'></span></a></td></tr></table>
			
			            <script type="text/javascript">
			            $("#safa_buses_brands_<?php echo $item_drivers_and_buses_counter; ?>").change(function(){
			            var safa_buses_brands_id=$(this).val();
			            var dataString = "safa_buses_brands_id="+ safa_buses_brands_id;
			            $.ajax
			            ({
			            	type: "POST",
			            	url: "<?php echo base_url().'safa_buses_brands/getBrandModels'; ?>",
			            	data: dataString,
			            	cache: false,
			            	success: function(html)
			            	{
			                	//alert(html);
			
			            		var data_arr = JSON.parse(html);
			            		
			            		$("#safa_buses_models_<?php echo $item_drivers_and_buses_counter; ?>").html(data_arr[0]["safa_buses_models_options"]);
			            		$("#safa_buses_models_<?php echo $item_drivers_and_buses_counter; ?>").trigger("chosen:updated");
			            		$("#safa_buses_models_<?php echo $item_drivers_and_buses_counter; ?>").val(data_arr[0]["safa_buses_models_id_selected"]);
			            		$("#safa_buses_models_<?php echo $item_drivers_and_buses_counter; ?>").trigger("chosen:updated");
			            	}
			            });
			            });
			        	</script>
			        	
			            </td>
			
			            <td>
			                  <table style='border:0px;'><tr rel="<?php echo $item_drivers_and_buses_counter; ?>"><td style='border:0px;'> <select name="safa_buses_models[<?php echo $item_drivers_and_buses_counter; ?>]" class="chosen-select chosen-rtl input-full  validate[required] safa_buses_models" id="safa_buses_models_<?php echo $item_drivers_and_buses_counter; ?>" tabindex="4">
			            <? 
			            
			            $current_safa_buses_models = ddgen('safa_buses_models', array('safa_buses_models_id', name()), array('safa_buses_brands_id' => $item_driver_and_bus->safa_buses_brands_id), array(name(), 'ASC'), TRUE);
			            foreach($current_safa_buses_models as $key => $value) {

			            	$selected='';
				            if($key==$item_driver_and_bus->safa_buses_models_id) {
				            	$selected='selected';
				            }
			            ?>
			                       <option value="<?= $key ?>" <?php echo $selected;?> ><?= $value ?></option>
			            <? } ?>
			            </select>
			            </td>
			            
			            <td style='border:0px;'><a href='javascript:void(0);' onclick='add_bus_model("<?php echo $item_drivers_and_buses_counter; ?>");'  class='btn Fleft ' style='display:inline-block;'> <span class='icon-plus'></span></a></td></tr></table>
			            
			            </td>
            
            			<td><?php echo  form_input("bus_count[".$item_drivers_and_buses_counter."]", set_value("bus_count[".$item_drivers_and_buses_counter."]", $item_driver_and_bus->bus_count), " class=\" validate[custom[integer]] bus_count \"  style=\"width:150px\"  ") ?>
			            </td>
			
			             <td><?php echo  form_input("seats_count[".$item_drivers_and_buses_counter."]", set_value("seats_count[".$item_drivers_and_buses_counter."]", $item_driver_and_bus->seats_count), " class=\" validate[custom[integer]] seats_count \"  style=\"width:150px\"  ") ?>
			             </td>
			            
                       
                         
                         
                        <td class="TAC">
                            <a href="javascript:void(0)" onclick="delete_drivers_and_buses(<?php echo  $item_drivers_and_buses_counter ?>, true)"><span class="icon-trash"></span></a>
                        </td>
                         
                    </tr>
                    <? } ?>
                    <? } ?>
                    <? } ?>
                </tbody>
            </table> 
        </div>

</fieldset>
</div>






<?php //if($safa_trip_internaltrip_id==0) {?>
<div class="toolbar bottom TAC">
  <a href="javascript:void(0)" onclick="generate_internalsegments();" class="btn" > <?php echo lang('generate_trip_internaltrip') ?> </a>
</div>
<?php //}?>

<?php 
//By Gouda, this condition doesn't occur anyway.
//if($safa_trip_internaltrip_id!=0 && $safa_trip_internaltrip_id==0) {
if($safa_trip_internaltrip_id!=0 && $safa_trip_internaltrip_id==0) {
?>
<div class="row-fluid">

<fieldset class="resalt-group">
  <div class="wizerd-div"><a> <?= lang('transport_request_internal_passages') ?></a></div>
                
        



        <div class="table-responsive" >
            <!--- the part of internal passages---> 
            <table class="myTable" cellpadding="0" cellspacing="0">
                <thead>
                    <tr>
                        <th><?= lang('internalpassage_type') ?></th>
                        <th><?= lang('internalpassage_starthotel') ?></th>
                        <th><?= lang('internalpassage_endhotel') ?></th>
                        <th><?= lang('internalpassage_startdatatime') ?></th>
                        <th><?= lang('internalpassage_enddatatime') ?></th>
<!--                        <th><?= lang('internalpassage_seatscount') ?></th>-->
                        <th><?= lang('internalpassage_note') ?></th>
                        <th>
<!--                        <?= lang('global_actions') ?>-->
                        <a href="<?= site_url("uo/trip_internalpassages/add/" . $safa_trip_internaltrip_id) ?>" class="btn  fancybox fancybox.iframe" style="<? if (lang('global_lang') == 'ar'): ?>float:left<? else: ?>float:right<? endif; ?>" ><?= lang('add_passges') ?></a>
      					
                        </th>
                    </tr>
                </thead>
                <tbody>
                        <? if (isset($internalpassages)) : ?>
                            <? foreach ($internalpassages as $inernalpassage): ?>
                            <tr>
                                <td><?= item("safa_internalsegmenttypes", name(),
                                        array("safa_internalsegmenttype_id" => $inernalpassage->safa_internalsegmenttype_id)); ?></td>
                                <? if ($inernalpassage->safa_internalsegmenttype_id == 1): 
                                
                                	$erp_port='';
                                	if($inernalpassage->erp_port_id) {
                                		$erp_port = item("erp_ports", name(), array("erp_port_id" => $inernalpassage->erp_port_id));
                                	}
                                	
                                	$erp_end_hotel='';
                                	if($inernalpassage->erp_end_hotel_id) {
                                		$erp_end_hotel = item("erp_hotels", name(), array("erp_hotel_id" => $inernalpassage->erp_end_hotel_id));
                                	}
                                	
                                
                                ?>
                                    <td><?= $erp_port; ?></td>
                                    <td><?= $erp_end_hotel; ?></td>
        <? endif; ?>  
                                <? 
                                
                                	if ($inernalpassage->safa_internalsegmenttype_id == 2){ 
                                	$erp_start_hotel='';
                                	if($inernalpassage->erp_start_hotel_id) {
                                		$erp_start_hotel = item("erp_hotels", name(), array("erp_hotel_id" => $inernalpassage->erp_start_hotel_id));
                                	}
                                	
                                	$erp_port='';
                                	if($inernalpassage->erp_port_id) {
                                		$erp_port = item("erp_ports", name(), array("erp_port_id" => $inernalpassage->erp_port_id));
                                	}
                                ?>
                                    <td><?= $erp_start_hotel; ?></td>
                                    <td><?= $erp_port; ?></td>
                                    <? }; ?>
                                    <? if ($inernalpassage->safa_internalsegmenttype_id == 3): 
                                    
                                    $erp_start_hotel='';
                                	if($inernalpassage->erp_start_hotel_id) {
                                		$erp_start_hotel = item("erp_hotels", name(), array("erp_hotel_id" => $inernalpassage->erp_start_hotel_id));
                                	}
                                	
                                	$safa_tourism_place='';
                                	if($inernalpassage->safa_tourism_place_id) {
                                		$safa_tourism_place = item("safa_tourismplaces", name(), array("safa_tourismplace_id" => $inernalpassage->safa_tourism_place_id));
                                	}
                                	
                                    ?>
                                    <td><?= $erp_start_hotel; ?></td>
                                    <td><?= $safa_tourism_place; ?></td>
                                    <? endif; ?>
                                    
                                    <? if ($inernalpassage->safa_internalsegmenttype_id == 4): 
                                    
                                    $erp_start_hotel='';
                                	if($inernalpassage->erp_start_hotel_id) {
                                		$erp_start_hotel = item("erp_hotels", name(), array("erp_hotel_id" => $inernalpassage->erp_start_hotel_id));
                                	}
                                	
                                	$erp_end_hotel='';
                                	if($inernalpassage->erp_end_hotel_id) {
                                		$erp_end_hotel = item("erp_hotels", name(), array("erp_hotel_id" => $inernalpassage->erp_end_hotel_id));
                                	}
                                    
                                    ?>
                                    <td><?= $erp_start_hotel; ?></td>
                                    <td><?= $erp_end_hotel; ?></td>
                                    <? endif; ?>
                                <td>
        <? if (isset($inernalpassage->start_datetime) && $inernalpassage->start_datetime != null && $inernalpassage->start_datetime != '0000-00-00 00:00:00'): ?>
            <? $date = date_create($inernalpassage->start_datetime); ?>
                                        <span><?= date_format($date, "m-d H:i"); ?></span>
                                    <? endif; ?>   
                                </td>
                                <td>
                                        <? if (isset($inernalpassage->end_datetime) && $inernalpassage->end_datetime != null && $inernalpassage->end_datetime != '0000-00-00 00:00:00'): ?>
            <? $date_end = date_create($inernalpassage->end_datetime); ?>    
                                        <span><?= date_format($date_end, "m-d H:i"); ?></span>
                            <? endif; ?> 
                                </td>
<!--                                <td><? if (isset($inernalpassage->seats_count)): ?> <?= $inernalpassage->seats_count ?> <? endif; ?>  </td>-->
                                <td><? if (isset($inernalpassage->notes)): ?> <?= $inernalpassage->notes ?> <? endif; ?>  </td>
                                <td class="TAC">
                                    <a  class="fancybox fancybox.iframe " href="<?= site_url("ea/trip_internalpassages/edit/" . $inernalpassage->safa_internalsegment_id) ?>"><span class="icon-pencil"></span></a> 
        <? if (!check_id('safa_internalsegment_log', 'safa_internalsegment_id',
                        $inernalpassage->safa_internalsegment_id)): ?>
                                        <a href="<?= site_url("ea/trip_internalpassages/delete/" . $inernalpassage->safa_internalsegment_id . "/" . $safa_trip_internaltrip_id) ?>" onclick="return window.confirm('<?= lang('global_are_you_sure_you_want_to_delete') ?>')" ><span class="icon-trash"></span></a>
        <? endif; ?>
                                </td>
                            </tr> 

    <? endforeach; ?>
<? endif; ?>  
                </tbody>
            </table>  
            
        </div>
        </fieldset>
    </div>
<?php 
}
?>








<!--<div class="row-fluid" id="div_internalsegments" <?php if($safa_trip_internaltrip_id==0) {?>style="display:none;" <?php }?>>-->
<div class="row-fluid" id="div_internalsegments" >

<fieldset class="resalt-group">
  <div class="wizerd-div"><a> <?= lang('trip_internaltrip') ?></a></div>
                
        


		<script>passages_counter = 0</script>
		
		<div class="row-form" >
		
		<input type="hidden" id="hdn_are_new_segments_generated" name="hdn_are_new_segments_generated" value="0"></input>
		                            
        <div class="table-responsive" id="div_tbl_internalsegments">
            <!--- the part of internal passages---> 
                <table cellpadding="0" cellspacing="0" id="tbl_internalsegments">
                    <thead>
                        <tr>
                            <th><?= lang('internalpassage_type') ?></th>
                            <th><?= lang('internalpassage_starthotel') ?></th>
                            <th><?= lang('internalpassage_endhotel') ?></th>
                            <th><?= lang('internalpassage_startdatatime') ?></th>
                            <th><?= lang('internalpassage_enddatatime') ?></th>
<!--                            <th><?= lang('internalpassage_seatscount') ?></th>-->
                            <th><?= lang('internalpassage_note') ?></th>
                            <th>
                            <a class="btn Fleft" href="javascript:void(0)" onclick="add_internalsegment('N' + passages_counter++); load_multiselect()">
					            <?php echo  lang('global_add') ?>
					        </a>
                            </th>
                        </tr>
                    </thead>
                    <tbody class="internalsegments" id="tbl_body_internalsegments">
                        <? if (isset($internalpassages)) : ?>
                            <? foreach ($internalpassages as $inernalpassage): ?>
                                <tr rel="<?php echo  $inernalpassage->safa_internalsegment_id ?>">
                                    <td>
		                            <?php 
		                            echo  form_dropdown('internalpassage_type['.$inernalpassage->safa_internalsegment_id.']'
		                                    , $safa_internalpassagetypes, set_value('internalpassage_type['.$inernalpassage->safa_internalsegment_id.']', $inernalpassage->safa_internalsegmenttype_id)
		                                    , 'class="chosen-select chosen-rtl input-full  validate[required]" id="internalpassage_type_'.$inernalpassage->safa_internalsegment_id.'" tabindex="4" ') ?>
		                                    
		                                    
		                            <script>
						            $(document).ready(function() {
						                $("#internalpassage_type_<?php echo $inernalpassage->safa_internalsegment_id; ?>").change(function() {
						                    var val_ = $(this).val();
						                    show_ports_<?php echo $inernalpassage->safa_internalsegment_id; ?>(val_);
						                });
						            });
						
						            function show_ports_<?php echo $inernalpassage->safa_internalsegment_id; ?>(pass_type) {
						             //   alert(pass_type);
									 load_multiselect();
						
									$("#start_point_con_<?php echo $inernalpassage->safa_internalsegment_id; ?>").show();
									$("#end_point_con_<?php echo $inernalpassage->safa_internalsegment_id; ?>").show();
									
						            $("#start_point_con_<?php echo $inernalpassage->safa_internalsegment_id; ?>").children().hide();
						            $("#end_point_con_<?php echo $inernalpassage->safa_internalsegment_id; ?>").children().hide();
						            if (pass_type == 1) {
						                $("#internalpassage_port_start_<?php echo $inernalpassage->safa_internalsegment_id; ?>_chosen").show();
						                $("#internalpassage_hotel_end_<?php echo $inernalpassage->safa_internalsegment_id; ?>_chosen").show();
						            }
						            if (pass_type == 2) {
						                $("#internalpassage_hotel_start_<?php echo $inernalpassage->safa_internalsegment_id; ?>_chosen").show();
						                $("#internalpassage_port_end_<?php echo $inernalpassage->safa_internalsegment_id; ?>_chosen").show();
						                $("#end_point_con_<?php echo $inernalpassage->safa_internalsegment_id; ?>").find(".error").show();
						            }
						            if (pass_type == 3) {
						                $("#internalpassage_hotel_start_<?php echo $inernalpassage->safa_internalsegment_id; ?>_chosen").show();
						                $("#internalpassage_torismplace_end_<?php echo $inernalpassage->safa_internalsegment_id; ?>_chosen").show();
						            }
						            if (pass_type == 4) {
						                $("#internalpassage_hotel_start_<?php echo $inernalpassage->safa_internalsegment_id; ?>_chosen").show();
						                $("#internalpassage_hotel_end_<?php echo $inernalpassage->safa_internalsegment_id; ?>_chosen").show();
						            }
						            
						            }
						
						            
						        	</script>
		                        	</td>
                            
                            
                                    
                                    <td>
                                    
		                            
		                            <div id='start_point_con_<?php echo $inernalpassage->safa_internalsegment_id; ?>' style='display:block' >
		                        	<?php
		                        	$hdn_start_point_value=''; 
		                        	if($inernalpassage->erp_start_hotel_id!='') {
		                        		$hdn_start_point_value = $inernalpassage->erp_start_hotel_id;
		                        	} else if($inernalpassage->safa_tourism_place_id!='') {
		                        		$hdn_start_point_value = $inernalpassage->safa_tourism_place_id;
		                        	} else if($inernalpassage->erp_port_id!='') {
		                        		$hdn_start_point_value = $inernalpassage->erp_port_id;
		                        	} 
		                        	?>
		                        	<input type="hidden" id="hdn_start_point_<?php echo $inernalpassage->safa_internalsegment_id;?>" value="<?php echo $hdn_start_point_value;?>"></input>
		                            <?php 
		                            echo  form_dropdown('internalpassage_hotel_start['.$inernalpassage->safa_internalsegment_id.']'
		                                    , $erp_hotels, set_value('internalpassage_hotel_start['.$inernalpassage->safa_internalsegment_id.']', $inernalpassage->erp_start_hotel_id)
		                                    , 'class="chosen-select chosen-rtl input-full start_point   " id="internalpassage_hotel_start_'.$inernalpassage->safa_internalsegment_id.'" tabindex="4" style="display:none" ');
		                             ?>
		                        	<?php 
		                            echo  form_dropdown('internalpassage_torismplace_start['.$inernalpassage->safa_internalsegment_id.']'
		                                    , $safa_tourismplaces, set_value('internalpassage_torismplace_start['.$inernalpassage->safa_internalsegment_id.']', $inernalpassage->safa_tourism_place_id)
		                                    , 'class="chosen-select chosen-rtl input-full start_point  " id="internalpassage_torismplace_start_'.$inernalpassage->safa_internalsegment_id.'" tabindex="4"  style="display:none" '); 
		                            ?>
		                            <?php 
		                            if ($erp_transportertype_id == 1) {
		                            	echo  form_dropdown('internalpassage_port_start['.$inernalpassage->safa_internalsegment_id.']'
		                                    , $land_erp_ports_sa, set_value('internalpassage_port_start['.$inernalpassage->safa_internalsegment_id.']', $inernalpassage->erp_port_id)
		                                    , 'class="chosen-select chosen-rtl input-full start_point  " id="internalpassage_port_start_'.$inernalpassage->safa_internalsegment_id.'" tabindex="4"  style="display:none" ');
		                            } else if ($erp_transportertype_id == 2) {
		                            	echo  form_dropdown('internalpassage_port_start['.$inernalpassage->safa_internalsegment_id.']'
		                                    , $erp_sa_ports, set_value('internalpassage_port_start['.$inernalpassage->safa_internalsegment_id.']', $inernalpassage->erp_port_id)
		                                    , 'class="chosen-select chosen-rtl input-full start_point  " id="internalpassage_port_start_'.$inernalpassage->safa_internalsegment_id.'" tabindex="4"  style="display:none" ');
		                            } if ($erp_transportertype_id == 3) {
		                            	echo  form_dropdown('internalpassage_port_start['.$inernalpassage->safa_internalsegment_id.']'
		                                    , $sea_erp_ports_sa, set_value('internalpassage_port_start['.$inernalpassage->safa_internalsegment_id.']', $inernalpassage->erp_port_id)
		                                    , 'class="chosen-select chosen-rtl input-full start_point " id="internalpassage_port_start_'.$inernalpassage->safa_internalsegment_id.'" tabindex="4"  style="display:none" ');
		                            } 
		                            ?>
		                        	</div>
		                        	
		                        	</td>
		                        	
		                        	
		                        	<td>
		                            
		                            <div id='end_point_con_<?php echo $inernalpassage->safa_internalsegment_id; ?>' style='display:block' >
		                            
		                            <?php
		                        	$hdn_end_point_value=''; 
		                        	if($inernalpassage->erp_end_hotel_id!='') {
		                        		$hdn_end_point_value = $inernalpassage->erp_end_hotel_id;
		                        	} else if($inernalpassage->safa_tourism_place_id!='') {
		                        		$hdn_end_point_value = $inernalpassage->safa_tourism_place_id;
		                        	} else if($inernalpassage->erp_port_id!='') {
		                        		$hdn_end_point_value = $inernalpassage->erp_port_id;
		                        	} 
		                        	?>
		                        	<input type="hidden" id="hdn_end_point_<?php echo $inernalpassage->safa_internalsegment_id;?>" value="<?php echo $hdn_end_point_value;?>"></input>
		                            
		                            
		                            <?php 
		                            echo  form_dropdown('internalpassage_hotel_end['.$inernalpassage->safa_internalsegment_id.']'
		                                    , $erp_hotels, set_value('internalpassage_hotel_end['.$inernalpassage->safa_internalsegment_id.']', $inernalpassage->erp_end_hotel_id)
		                                    , 'class="chosen-select chosen-rtl input-full end_point   " id="internalpassage_hotel_end_'.$inernalpassage->safa_internalsegment_id.'" tabindex="4"  style="display:none" ');
		                            ?>
		                        	
		                        	<?php 
//		                            echo  form_dropdown('internalpassage_torismplace_end['.$inernalpassage->safa_internalsegment_id.']'
//		                                    , $safa_tourismplaces, set_value('internalpassage_torismplace_end['.$inernalpassage->safa_internalsegment_id.']', $inernalpassage->safa_tourism_place_id)
//		                                    , 'class="chosen-select chosen-rtl input-full end_point" id="internalpassage_torismplace_end_'.$inernalpassage->safa_internalsegment_id.'" tabindex="4"  style="display:none" '); 
//		                                    
		                                    
							            echo '<select name="internalpassage_torismplace_end['.$inernalpassage->safa_internalsegment_id.']" class="chosen-select chosen-rtl input-full end_point   " tabindex="4"  style="display:none"  id="internalpassage_torismplace_end_'.$inernalpassage->safa_internalsegment_id.'" >';
							            foreach($safa_tourismplaces as $key => $value){
							            	$key_arr = explode('---',$key);
							            	$safa_tourismplace_id = $key_arr[0];
							            	$erp_city_id = $key_arr[1];
							            	
							            	$selected='';
							            	if($safa_tourismplace_id== $inernalpassage->safa_tourism_place_id) {
							            		$selected ='selected';
							            	}
							            
							            echo "           <option value=\"$safa_tourismplace_id\" city_id=\"$erp_city_id\" $selected>$value</option>";
							            } 
							            echo "    </select>";
		                            ?>
		                            <?php 
		                            
		                            if ($erp_transportertype_id == 1) {
		                            	echo  form_dropdown('internalpassage_port_end['.$inernalpassage->safa_internalsegment_id.']'
		                                    , $land_erp_ports_sa, set_value('internalpassage_port_end['.$inernalpassage->safa_internalsegment_id.']', $inernalpassage->erp_port_id)
		                                    , 'class="chosen-select chosen-rtl input-full end_point   " id="internalpassage_port_end_'.$inernalpassage->safa_internalsegment_id.'" tabindex="4"  style="display:none" ');
		                            } else if ($erp_transportertype_id == 2) {
		                            	echo  form_dropdown('internalpassage_port_end['.$inernalpassage->safa_internalsegment_id.']'
		                                    , $erp_sa_ports, set_value('internalpassage_port_end['.$inernalpassage->safa_internalsegment_id.']', $inernalpassage->erp_port_id)
		                                    , 'class="chosen-select chosen-rtl input-full end_point   " id="internalpassage_port_end_'.$inernalpassage->safa_internalsegment_id.'" tabindex="4"  style="display:none" ');
		                            } else if ($erp_transportertype_id == 3) {
		                            	echo  form_dropdown('internalpassage_port_end['.$inernalpassage->safa_internalsegment_id.']'
		                                    , $sea_erp_ports_sa, set_value('internalpassage_port_end['.$inernalpassage->safa_internalsegment_id.']', $inernalpassage->erp_port_id)
		                                    , 'class="chosen-select chosen-rtl input-full end_point   " id="internalpassage_port_end_'.$inernalpassage->safa_internalsegment_id.'" tabindex="4"  style="display:none" ');
		                            } 
		                            ?>
		                        	</div>
		                        	
		                        	<script>
                                    $(document).ready(function() {
                                    	show_ports_<?php echo $inernalpassage->safa_internalsegment_id; ?>(<?php echo $inernalpassage->safa_internalsegmenttype_id;?>);
                                    });
		                           	</script>
		                            
		                        	
		                        	</td>
                                    
                                    
                                    
                                    <td>
			                            <?php echo  form_input('internalpassage_startdatatime['.$inernalpassage->safa_internalsegment_id.']'
			                                    , set_value('internalpassage_startdatatime['.$inernalpassage->safa_internalsegment_id.']', date('Y-m-d H:i', strtotime($inernalpassage->start_datetime)))
			                                    , 'class="input-huge  datetimepicker" style="width:120px"') ?>
			                                    
			                        </td>
			                        <td>
			                            <?php echo  form_input('internalpassage_enddatatime['.$inernalpassage->safa_internalsegment_id.']'
			                                    , set_value('internalpassage_enddatatime['.$inernalpassage->safa_internalsegment_id.']', date('Y-m-d H:i', strtotime($inernalpassage->end_datetime)))
			                                    , 'class="input-huge datetimepicker" style="width:120px"') ?>
			                        
			                        
			                        </td>
                                    <!--<td>
			                            <?php echo  form_input('seatscount['.$inernalpassage->safa_internalsegment_id.']'
			                                    , set_value('seatscount['.$inernalpassage->safa_internalsegment_id.']', $inernalpassage->seats_count)
			                                    , 'class="" style="width:50px"') ?>
			                        </td>
			                        --><td>
			                            <?php echo  form_input('internalpassage_notes['.$inernalpassage->safa_internalsegment_id.']'
			                                    , set_value('internalpassage_notes['.$inernalpassage->safa_internalsegment_id.']', $inernalpassage->notes)
			                                    , 'class="input-huge" style="width:120px"') ?>
			                        </td>
			                        
			                        
			                        
                                    <td class="TAC">
                                   		<a href="javascript:void(0)" onclick="repeat_passages('<?php echo  $inernalpassage->safa_internalsegment_id ?>', true)"><span class="icon-copy"></span></a>
		                            	
		                            	<a href="javascript:void(0)" onclick="delete_passages('<?php echo  $inernalpassage->safa_internalsegment_id ?>', true)"><span class="icon-trash"></span></a>
		                            	
		                            	&nbsp; &nbsp;
		                            	
			                            <?php 
	                                    $this->internalsegments_drivers_and_buses_model->safa_internalsegments_id=$inernalpassage->safa_internalsegment_id;
	                                    $count_drivers_and_buses= $this->internalsegments_drivers_and_buses_model->get(true);
	                                    ?>	
	                                    X <?php echo $count_drivers_and_buses; ?> 
										 	   
	                                    <a title='<?= lang('drivers_and_buses') ?>' href="<?= site_url('uo/all_movements/drivers_and_buses/' . $inernalpassage->safa_internalsegment_id) ?>" style="color:black; direction:rtl">
	                                             <img src="<?= IMAGES2 ?>/car.png" align="left" >
	                                    </a>    
                                            
                                    
		                            	
		                        	</td>
                                </tr>  

                            <? endforeach; ?>
                        <? endif; ?>  
                    </tbody>
                </table>    
            
        </div>
        
        </div>
        
         
        
        
        </fieldset>
        
        
        
        
		  <fieldset class="resalt-group">
		  <div class="wizerd-div"><a> <?= lang('trip_details_travellers') ?></a></div>
  
                                				    		
                                    <div class="block-fluid" id="dv_tbl_trip_traveller">
                                        <table class="fpTable" id="tbl_trip_traveller" cellpadding="0" cellspacing="0" width="100%">
                                            <thead>
                                                <tr>
                                                    <th><?= lang('trip_details_traveller') ?></th>
                                                    <th><?= lang('trip_details_passport_no') ?></th>
                                                    <th><?= lang('trip_details_visa_no') ?></th>
                                                    <th><?= lang('trip_details_nationality') ?></th>
                                                    <th><?= lang('trip_details_gender') ?></th>
                                                    
                                                    <th><?= lang('group_passports_age') ?></th>
                                                    <th><?= lang('group_passports_occupation') ?></th>
                                                    <th><?= lang('group_passports_picture') ?></th>
                                                    <th class="TAC"><?= lang('global_operations') ?></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <? 
                                              
                                                $trip_travellers_arr = array();
												$this->safa_group_passports_model->safa_trip_internaltrip_id = false;
                                                $this->safa_group_passports_model->safa_trip_id = false;

                                                $trip_group_passports_rows = $this->safa_group_passports_model->get_for_table();
												foreach ($trip_group_passports_rows as $trip_group_passports_row) {
									                $trip_travellers_arr[$trip_group_passports_row->safa_group_passport_id] = $trip_group_passports_row->{'first_' . name()} . ' ' . $trip_group_passports_row->{'second_' . name()} . ' ' . $trip_group_passports_row->{'third_' . name()} . ' ' . $trip_group_passports_row->{'fourth_' . name()};
									            }
												
												
                                                $item_travellers_arr = array();

                                                //----------------------- Group Passports Linked With Accomodation Forms ----------------
                                                $distinct_passports_by_trip_rows = $this->safa_group_passport_accommodation_model->get_distinct_passports_by_trip_uo_form($safa_trip_id);
                                                $distinct_passports_by_trip_arr = array();
                                                foreach ($distinct_passports_by_trip_rows as $distinct_passports_by_trip_row){ 
                                                	$distinct_passports_by_trip_arr[]=$distinct_passports_by_trip_row->safa_group_passport_id;
                                                }
                                                
                                                $passports_forms_by_trip_arr = array();
                                                foreach ($distinct_passports_by_trip_rows as $distinct_passports_by_trip_row){ 
                                                	$passports_forms_by_trip_arr[]=$distinct_passports_by_trip_row->safa_reservation_form_id;
                                                }
                                                //---------------------------------------------------------------------------------------
                                                
                                                
                                                
                                                if (isset($safa_group_passports)) {
                                                 	
                                                $row_class='';
                                                	
                                                foreach ($safa_group_passports as $safa_group_passport){ 
                                                $item_travellers_arr[] = $safa_group_passport->safa_group_passport_id;
                                                	
                                                $passports_forms_by_trip_arr_key = array_search($safa_group_passport->safa_group_passport_id, $distinct_passports_by_trip_arr);
												if ($passports_forms_by_trip_arr_key !== false) {
                                                	$passports_safa_reservation_form_id=$passports_forms_by_trip_arr[$passports_forms_by_trip_arr_key];
                                                	
                                                	if(isset($item_safa_reservation_forms_colors_arr[$passports_safa_reservation_form_id])) {
						                				$row_class = $item_safa_reservation_forms_colors_arr[$passports_safa_reservation_form_id];
						                			}
						                				
                                                }
                                                	
                                                ?>
                                                    <tr>
                                                       
                                                        <td class="<?php echo $row_class;?>" ><?= $safa_group_passport->{'first_' . name()} . ' ' . $safa_group_passport->{'second_' . name()} . ' ' . $safa_group_passport->{'third_' . name()} . ' ' . $safa_group_passport->{'fourth_' . name()} ?></td>
                                                        <td class="<?php echo $row_class;?>" ><?= $safa_group_passport->passport_no ?></td>
                                                        <td class="<?php echo $row_class;?>" ><?= $safa_group_passport->visa_number ?></td>
                                                        <td class="<?php echo $row_class;?>" ><?= $safa_group_passport->erp_nationalities_name ?></td>
                                                        <td class="<?php echo $row_class;?>" ><?= $safa_group_passport->erp_gender_name ?></td>
                                                        
                                                        <td class="<?php echo $row_class;?>" >
                                                        
                                                        <?php
			                                    $date_of_birth = strtotime($safa_group_passport->date_of_birth);
			                                    $date_now = strtotime(date('Y-m-d', time()));
			                                    $secs = $date_now - $date_of_birth; // == return sec in difference
			                                    $days = $secs / 86400;
			                                    $age = $days / 365;
												//$age=round($age);
												//$age=ceil($age);
			                                    $age = floor($age);
			                                    echo $age;
			                                    ?>
                                                        
                                                 </td>
                                                 <td class="<?php echo $row_class;?>" ><?= $safa_group_passport->occupation ?></td>
                                                 
                                                 <td class="<?php echo $row_class;?>" >
                                                 <?php 
                                                	if ($safa_group_passport->picture != '') {
					                             ?>
					                                <img src="<?= base_url('static/group_passports') . '/' . $safa_group_passport->picture; ?>" alt="" width="50px" height="60px">
					                            <?php
					                            	}
                                                 ?>
                                                 </td>
                                                 
                                                 <td class="<?php echo $row_class;?>"  >
                                                 <?php 
                                                if (!in_array($safa_group_passport->safa_group_passport_id, $distinct_passports_by_trip_arr)) {
                                                 ?>
									             <a href="javascript:void(0)" class="" id="hrf_remove_traveller" name="<?php echo $safa_group_passport->safa_group_passport_id;?>"><span class="icon-trash"></span> </a>
									             <?php 
                                                }
									             ?>
									             </td>
                                                 
                                                    </tr>
                                                <? }
                                              }
                                                ?>
                                            </tbody>
                                        </table>
                                        
                                        
                                        <?php  
							    		echo form_multiselect('safa_trip_traveller_ids[]', $trip_travellers_arr, set_value('safa_trip_traveller_ids', $item_travellers_arr),
			                                    " id='safa_trip_traveller_ids'  class='' readonly='readonly' style='display: none;' ");
			        					?>
                                        
                                        <script>
				                         $("#hrf_remove_traveller").live('click', function() {
				                             if (!confirm('<?= lang('global_are_you_sure_you_want_to_delete') ?>'))
				                                 return false
				
				                            var traveller_id = $(this).attr('name');
				                            //alert(traveller_id);
				                            $("#safa_trip_traveller_ids").find('option[value="'+traveller_id+'"]').remove();
				                             
				                             $(this).parent().parent().remove();

				                             var hidden_input = '<input type="hidden" name="safa_group_passports_remove[]" value="' + traveller_id + '" />';
				                     	    $('#dv_tbl_trip_traveller').append(hidden_input);
				                     	    
				                         });
				                         </script>
                                        
                                        <?php if(session('ea_id')){ ?>
                                        <div class="span12">
										<div class="span10"><a class="btn btn-primary finish fancybox fancybox.iframe"
											href="<?php echo  site_url('ea/group_passports/add_to_internaltrip_by_trip_popup/'.$safa_trip_id) ?>"><?php echo  lang('add_safa_group_passports_to_internaltrip') ?></a>
										</div>
										</div>
                                        <?php }?>
                                        
                                    </div>
        					</fieldset>
                            <br/>
        
        
        
        
        <div class="toolbar bottom TAC">
            <input type="submit"  class="btn btn-primary" name="submit" value="<?= lang('global_submit') ?>" onclick="return validate_saving(); localStorage.removeItem('dv_internaltrip_manage_storage_<?php echo $safa_trip_internaltrip_id;?>'); "/>
            <a href='<?= site_url('safa_trip_internaltrips') ?>'  class="btn btn-primary" /><?= lang('global_back') ?></a>
        </div>
        
        
    </div>


        
<?= form_close() ?>
    </div>
</div>  


<script>
    $(document).ready(function() {
        $('.fancybox').fancybox({
            afterClose: function() {
                //parent.location.reload(true);
            }
        });
    });
</script>

<script type="text/javascript">
$(document).ready(function() {
    var config = {
        '.chosen-select': {width: "100%"},
        '.chosen-select-deselect': {allow_single_deselect: true},
        '.chosen-select-no-single': {disable_search_threshold: 10},
        '.chosen-select-no-results': {no_results_text: 'Oops, nothing found!'},
        '.chosen-select-width': {width: "95%"}
    }
    for (var selector in config) {
        $(selector).chosen(config[selector]);
    }


    // binds form submission and fields to the validation engine
    $("#frm_trip_internaltrip").validationEngine({
            prettySelect : true,
            useSuffix: "_chosen",
            promptPosition : "topRight:-150"
            //promptPosition : "bottomLeft"
    });

   
    
 
});


$("#lnk_add_safa_reservation_forms").click(function() {

	$("#frm_trip_internaltrip").validationEngine({
        prettySelect : true,
        useSuffix: "_chosen",
        promptPosition : "topRight:-150"
        //promptPosition : "bottomLeft"
	});
    
});

</script> 

<script>
    $(document).ready(function() {
        $("#uo_ea_id").change(function() {
            //sending the request to get the trips//
            $.get('<?= site_url("uo/trip_internaltrip/get_uo_contract_trips") ?>/' + $(this).val(), function(data) {
                $("#trip_id").html(data);
            });
        });
    });

    $('#uo_ea_id').live('change', function() {
        var uo_ea_id = $("#uo_ea_id").val();
        $.get('<?= site_url("uo/trip_internaltrip/get_uo_contract_trips") ?>/' + uo_ea_id, function(data) {
            $("#trip_id").html(data);
            $("#trip_id").val('<?= $this->input->post('safa_trip_id') ?>');
        });
    });
</script>
<script>
    // filter with validation //
    $(document).ready(function() {
        /* initializing  the default value of the contracts by first items*/
        $("#uo_ea_id").val($(this).find('option').eq(1).val());
        var uo_ea_id = $("#uo_ea_id").val();

        $.get('<?= site_url("uo/trip_internaltrip/get_uo_contract_trips") ?>/' + uo_ea_id, function(data) {
            $("#trip_id").html(data);
            $("#trip_id").val('<?= $this->input->post('safa_trip_id') ?>');
        });




    });
</script>
<script>
    $(document).ready(function() {
        /* input file */
        $(".file .btn, .file input:text").click(function() {
            var block = $(this).parent('.file');
            block.find('input:file').change(function() {
                var file_arr = $(this).val().split('\\');
                var file_name = file_arr[file_arr.length - 1];
                block.find('input:text').val(file_name);
            });
        });
    });
</script>

<!-- 
<script>
        $(document).ready(function() {
            $("#safa_ito_id").change(function() {
                if ($("#safa_ito_id").val()) {
                    $("#safa_internaltripstatus_id").val(4);
                } else {
                    $("#safa_internaltripstatus_id").val(3);
                }
            });
            $("#safa_internaltripstatus_id").change(function() {
                if ($("#safa_ito_id").val()) {
                    if ($("#safa_internaltripstatus_id").val() != 4)
    <? if (lang('global_lang') == 'ar'): ?>
                        alert("قم باختيار مؤكد من شركة العمرة");
    <? else: ?>
                        alert("choose confirmed by uo");
    <? endif; ?>
                    $("#safa_internaltripstatus_id").val(4);
                } else {
                    if ($("#safa_internaltripstatus_id").val() != 3)
    <? if (lang('global_lang') == 'ar'): ?>
                        alert("قم باختيار بانتظار مشغل النقل");
    <? else: ?>
                        alert("choose pending ito");
    <? endif; ?>
                    $("#safa_internaltripstatus_id").val(3);
                    $("#safa_internaltripstatus_id").trigger("chosen:updated");
                }
            });
        });
</script> 
 -->
 
<script>
    var c1 = 0;
    var c2 = 100;
    
    
    function new_row(safa_externaltriptype_id) {
    	
    	
        if (safa_externaltriptype_id == 1) {
            
            var id = c1;
            var new_row = function() {/*
             <tr rel="{$id}">
             <td>
             <input type='hidden' name='erp_flight_availabilities_detail_id[{$id}]'  id='erp_flight_availabilities_detail_id[{$id}]'  value='0'/>
             
             <input type="hidden" name="safa_externaltriptype_id[{$id}]" id="safa_externaltriptype_id[{$id}]"  value="1" />
             
<?= form_dropdown('erp_path_type_id[{$id}]', $erp_path_types, set_value("erp_path_type_id[{\$id}]", 2), 'class="validate[required] erp_path_type" style="width:60px" pathrel="{$id}" id="erp_path_type_id[{$id}]"') ?></td>
             <td>

<table style='border:0px;' calss='cls_tbl_safa_transporter_code' ><tr inner_rel="{$id}"><td style='border:0px;'>
<?= form_dropdown('safa_transporter_code[{$id}]', $flight_transporters, set_value("safa_transporter_code[{\$id}]"), 'flight_states_rel= "{$id}" class=" chosen-select chosen-rtl validate[required]" style="width:60px" id="safa_transporter_code[{$id}]"') ?></td>

</td><td style='border:0px;'>
<a href="javascript:void(0);" onclick="add_transporter(2);"  class="btn Fleft " > <span class="icon-plus"></span></a>
</td></tr>
</table>

</td>
             <td><?= form_input('flight_number[{$id}]', set_value("flight_number[{\$id}]"), ' flight_states_rel= "{$id}"  class="validate[required]" style="width:40px" id="flightno[{$id}]"') ?></td>
             <td>
             <?= form_input('flight_date[{$id}]', set_value("flight_date[{\$id}]"), ' flight_states_rel= "{$id}" class="validate[required] start_date_{$id}" style="width:100px" id="flight_date[{$id}]"') ?>
             {start_date_script}
             </td>
             <td>
             <?= form_input('flight_arrival_date[{$id}]', set_value("flight_arrival_date[{\$id}]"), 'class="validate[required] end_date_{$id}" arriverel="{$id}" style="width:100px" id="flight_arrival_date[{$id}]"') ?>
             {end_date_script}
             </td>
             <td style="width:220px">
             <?= form_dropdown('erp_port_id_from[{$id}]', $erp_ports, set_value("erp_port_id_from[{\$id}]"), '  erp_port_id_from_rel= "{$id}" class="validate[required]" style="width:60px" id="erp_port_id_from[{$id}]"') ?>
<!--             <?= form_dropdown('erp_port_hall_id_from[{$id}]', $erp_ports_halls, set_value("erp_port_hall_id_from[{\$id}]"), '  erp_port_hall_id_from_rel= "{$id}" class="validate[required]" style="width:120px" id="erp_port_hall_id_from[{$id}]"') ?>-->
			<br/>
			 <?= form_dropdown('erp_port_id_to[{$id}]', $erp_sa_ports, set_value("erp_port_id_to[{\$id}]"), '  erp_port_id_to_rel= "{$id}" class="validate[required]" style="width:60px" id="erp_port_id_to[{$id}]"') ?>
             <?= form_dropdown('erp_port_hall_id_to[{$id}]', $erp_sa_ports_halls, set_value("erp_port_hall_id_to[{\$id}]"), '  erp_port_hall_id_to_rel= "{$id}" class="validate[required]" style="width:120px" id="erp_port_hall_id_to[{$id}]"') ?>
 			
			

            
             <td><?= form_input('seats_count[{$id}]', set_value("seats_count[{\$id}]"), 'class="validate[required] seatcount" style="width:50px" id="seatcont[{$id}]"') ?></td>
             <td><a href="javascript:void(0);" onclick="delete_flights('{$id}')"><span class="icon-trash"></span> </a></td>
             </tr>
             */
            }.toString().replace(/{\$id}/g, id).slice(14, -3);

			new_row = new_row.replace(/{start_date_script}/, "<script> $('.start_date_"+id+"').datetimepicker({"+
                 "dateFormat: 'yy-mm-dd',"+
                 "timeFormat: 'HH:mm',"+
                 "onClose: function(selectedDate) {"+
                 "    $('.end_date_"+id+"').datetimepicker('option', 'minDate', selectedDate);"+
                 "}"+
             "});<\/script>");
             
			new_row = new_row.replace(/{end_date_script}/, "<script> $('.end_date_"+id+"').datetimepicker({"+
	                 "dateFormat: 'yy-mm-dd',"+
	                 "timeFormat: 'HH:mm',"+
	                 "onClose: function(selectedDate) {"+
	                 "    $('.start_date_"+id+"').datetimepicker('option', 'maxDate', selectedDate);"+
	                 "}"+
	             "});<\/script>");
             

            $('tbody#going_flight_tbody').append(new_row);
            $('.datetimepicker').datetimepicker({dateFormat: 'yy-mm-dd', timeFormat: 'HH:mm'});
            c1++;

            $('#btn_flight_trip_going').hide();
            
            
        } else if (safa_externaltriptype_id == 2) {
            var id = c2;
            var new_row = function() {/*
             <tr rel="{$id}">
             <td>
             <input type='hidden' name='erp_flight_availabilities_detail_id[{$id}]'  id='erp_flight_availabilities_detail_id[{$id}]'  value='0'/>
             
             <input type="hidden" name="safa_externaltriptype_id[{$id}]" id="safa_externaltriptype_id[{$id}]"  value="2" />
             
<?= form_dropdown('erp_path_type_id[{$id}]', $erp_path_types, set_value("erp_path_type_id[{\$id}]"), 'class="validate[required] erp_path_type" style="width:60px" id="erp_path_type_id[{$id}]"') ?></td>
             <td>

             <table style='border:0px;' calss='cls_tbl_safa_transporter_code'><tr inner_rel="{$id}"><td style='border:0px;'>
     		<?= form_dropdown('safa_transporter_code[{$id}]', $flight_transporters, set_value("safa_transporter_code[{\$id}]"), ' flight_states_rel= "{$id}" class="  chosen-select chosen-rtl  validate[required]" style="width:60px" id="safa_transporter_code[{$id}]"') ?></td>

     		</td><td style='border:0px;'>
     		<a href="javascript:void(0);" onclick="add_transporter(2);"  class="btn Fleft " > <span class="icon-plus"></span></a>
     		</td></tr>
     		</table>
             
             </td>
             <td><?= form_input('flight_number[{$id}]', set_value("flight_number[{\$id}]"), ' flight_states_rel= "{$id}" class="validate[required]" style="width:40px" id="flightno[{$id}]"') ?></td>
             <td>
             <?= form_input('flight_date[{$id}]', set_value("flight_date[{\$id}]"), ' flight_states_rel= "{$id}" class="validate[required] start_date_{$id}" style="width:100px" id="flight_date[{$id}]"') ?>
             {start_date_script}
             </td>
             <td>
             <?= form_input('flight_arrival_date[{$id}]', set_value("flight_arrival_date[{\$id}]"), 'class="validate[required] end_date_{$id}" style="width:100px" id="flight_arrival_date[{$id}]"') ?>
             {end_date_script}
             </td>
             
             <td style="width:220px">
             <?= form_dropdown('erp_port_id_from[{$id}]', $erp_sa_ports, set_value("erp_port_id_from[{\$id}]"), '  erp_port_id_from_rel= "{$id}" class="validate[required]" style="width:60px" id="erp_port_id_from[{$id}]"') ?>
             <?= form_dropdown('erp_port_hall_id_from[{$id}]', $erp_sa_ports_halls, set_value("erp_port_hall_id_from[{\$id}]"), '  erp_port_hall_id_from_rel= "{$id}" class="validate[required]" style="width:120px" id="erp_port_hall_id_from[{$id}]"') ?>
 			 <br/>
			 <?= form_dropdown('erp_port_id_to[{$id}]', $erp_ports, set_value("erp_port_id_to[{\$id}]"), '  erp_port_id_to_rel= "{$id}" class="validate[required]" style="width:60px" id="erp_port_id_to[{$id}]"') ?>
<!--             <?= form_dropdown('erp_port_hall_id_to[{$id}]', $erp_ports_halls, set_value("erp_port_hall_id_to[{\$id}]"), '  erp_port_hall_id_to_rel= "{$id}" class="validate[required]" style="width:120px" id="erp_port_hall_id_to[{$id}]"') ?>-->
 			
             <td><?= form_input('seats_count[{$id}]', set_value("seats_count[{\$id}]"), 'class="validate[required] seatcount" style="width:50px" id="seatcont[{$id}]"') ?></td>
             <td><a href="javascript:void(0);" onclick="delete_flights('{$id}')"><span class="icon-trash"></span> </a></td>
             
             </tr>
             */
            }.toString().replace(/{\$id}/g, id).slice(14, -3);

            new_row = new_row.replace(/{start_date_script}/, "<script> $('.start_date_"+id+"').datetimepicker({"+
                    "dateFormat: 'yy-mm-dd',"+
                    "timeFormat: 'HH:mm',"+
                    "onClose: function(selectedDate) {"+
                    "    $('.end_date_"+id+"').datetimepicker('option', 'minDate', selectedDate);"+
                    "}"+
                "});<\/script>");
                
   			new_row = new_row.replace(/{end_date_script}/, "<script> $('.end_date_"+id+"').datetimepicker({"+
   	                 "dateFormat: 'yy-mm-dd',"+
   	                 "timeFormat: 'HH:mm',"+
   	                 "onClose: function(selectedDate) {"+
   	                 "    $('.start_date_"+id+"').datetimepicker('option', 'maxDate', selectedDate);"+
   	                 "}"+
   	             "});<\/script>");
	             
            
            $('tbody#return_flight_tbody').append(new_row);
            $('.datetimepicker').datetimepicker({dateFormat: 'yy-mm-dd', timeFormat: 'HH:mm'});
            c2++;

            if($('input[name="rdo_trip_return_type"]:checked').val()==1) {
            	$('#btn_flight_trip_return').hide();
            }
        }
        load_multiselect();
        
        //fix_seats_count();

        
		if(id==0) {
			$('#safa_transporter_code\\[' + id + '\\]').val($('#last_going_safa_transporter_code').val());
        	$('#safa_transporter_code\\[' + id + '\\]').trigger("chosen:updated");
		} else if(id==100) {
			$('#safa_transporter_code\\[' + id + '\\]').val($('#last_return_safa_transporter_code').val());
        	$('#safa_transporter_code\\[' + id + '\\]').trigger("chosen:updated");
		} else {
        	$('#safa_transporter_code\\[' + id + '\\]').val($('#safa_transporter_code\\[' + (id-1) + '\\]').val());
        	$('#safa_transporter_code\\[' + id + '\\]').trigger("chosen:updated");
		}
        
    }


	function new_cruise(safa_externaltriptype_id) {
    	
    	
        if (safa_externaltriptype_id == 1) {
            var id = c1;
            var new_row = function() {/*
             <tr rel="{$id}">
             <td>
             <input type='hidden' name='erp_cruise_availabilities_detail_id[{$id}]'  id='erp_cruise_availabilities_detail_id[{$id}]'  value='0'/>
             
             <input type="hidden" name="safa_externaltriptype_id[{$id}]" id="safa_externaltriptype_id[{$id}]"  value="1" />
             
<?= form_dropdown('erp_path_type_id[{$id}]', $erp_path_types, set_value("erp_path_type_id[{\$id}]", 2), 'class="validate[required] erp_path_type" style="width:60px" pathrel="{$id}" id="erp_path_type_id[{$id}]"') ?></td>
             <td>

<table style='border:0px;' calss='cls_tbl_safa_transporter_code' ><tr inner_rel="{$id}"><td style='border:0px;'>
<?= form_dropdown('safa_transporter_code[{$id}]', $cruise_transporters, set_value("safa_transporter_code[{\$id}]"), 'cruise_states_rel= "{$id}" class=" chosen-select chosen-rtl validate[required]" style="width:60px" id="safa_transporter_code[{$id}]"') ?></td>

</td><td style='border:0px;'>
<a href="javascript:void(0);" onclick="add_transporter(3);"  class="btn Fleft " > <span class="icon-plus"></span></a>
</td></tr>
</table>

</td>
             <td><?= form_input('cruise_number[{$id}]', set_value("cruise_number[{\$id}]"), ' cruise_states_rel= "{$id}"  class="validate[required]" style="width:40px" id="cruiseno[{$id}]"') ?></td>
             <td>
             <?= form_input('cruise_date[{$id}]', set_value("cruise_date[{\$id}]"), ' cruise_states_rel= "{$id}" class="validate[required] start_date_{$id}" style="width:100px" id="cruise_date[{$id}]"') ?>
             {start_date_script}
             </td>
             <td>
             <?= form_input('cruise_arrival_date[{$id}]', set_value("cruise_arrival_date[{\$id}]"), 'class="validate[required] end_date_{$id}" arriverel="{$id}" style="width:100px" id="cruise_arrival_date[{$id}]"') ?>
             {end_date_script}
             </td>
             <td style="width:220px">
             <?= form_dropdown('erp_port_id_from[{$id}]', $sea_erp_ports, set_value("erp_port_id_from[{\$id}]"), '  erp_port_id_from_rel= "{$id}" class="validate[required]" style="width:80px" id="erp_port_id_from[{$id}]"') ?>
			-
			 <?= form_dropdown('erp_port_id_to[{$id}]', $sea_erp_ports_sa, set_value("erp_port_id_to[{\$id}]"), '  erp_port_id_to_rel= "{$id}" class="validate[required]" style="width:80px" id="erp_port_id_to[{$id}]"') ?>
 			
			

            
             <td><?= form_input('seats_count[{$id}]', set_value("seats_count[{\$id}]"), 'class="validate[required] seatcount" style="width:50px" id="seatcont[{$id}]"') ?></td>
             <td><a href="javascript:void(0);" onclick="delete_cruises('{$id}')"><span class="icon-trash"></span> </a></td>
             </tr>
             */
            }.toString().replace(/{\$id}/g, id).slice(14, -3);

			new_row = new_row.replace(/{start_date_script}/, "<script> $('.start_date_"+id+"').datetimepicker({"+
                 "dateFormat: 'yy-mm-dd',"+
                 "timeFormat: 'HH:mm',"+
                 "onClose: function(selectedDate) {"+
                 "    $('.end_date_"+id+"').datetimepicker('option', 'minDate', selectedDate);"+
                 "}"+
             "});<\/script>");
             
			new_row = new_row.replace(/{end_date_script}/, "<script> $('.end_date_"+id+"').datetimepicker({"+
	                 "dateFormat: 'yy-mm-dd',"+
	                 "timeFormat: 'HH:mm',"+
	                 "onClose: function(selectedDate) {"+
	                 "    $('.start_date_"+id+"').datetimepicker('option', 'maxDate', selectedDate);"+
	                 "}"+
	             "});<\/script>");
             

            $('tbody#going_cruise_tbody').append(new_row);
            $('.datetimepicker').datetimepicker({dateFormat: 'yy-mm-dd', timeFormat: 'HH:mm'});
            c1++;

            $('#btn_cruise_trip_going').hide();
            
        } else if (safa_externaltriptype_id == 2) {
            var id = c2;
            var new_row = function() {/*
             <tr rel="{$id}">
             <td>
             <input type='hidden' name='erp_cruise_availabilities_detail_id[{$id}]'  id='erp_cruise_availabilities_detail_id[{$id}]'  value='0'/>
             
             <input type="hidden" name="safa_externaltriptype_id[{$id}]" id="safa_externaltriptype_id[{$id}]"  value="2" />
             
<?= form_dropdown('erp_path_type_id[{$id}]', $erp_path_types, set_value("erp_path_type_id[{\$id}]"), 'class="validate[required] erp_path_type" style="width:60px" id="erp_path_type_id[{$id}]"') ?></td>
             <td>

             <table style='border:0px;' calss='cls_tbl_safa_transporter_code'><tr inner_rel="{$id}"><td style='border:0px;'>
     		<?= form_dropdown('safa_transporter_code[{$id}]', $cruise_transporters, set_value("safa_transporter_code[{\$id}]"), ' cruise_states_rel= "{$id}" class="  chosen-select chosen-rtl  validate[required]" style="width:60px" id="safa_transporter_code[{$id}]"') ?></td>

     		</td><td style='border:0px;'>
     		<a href="javascript:void(0);" onclick="add_transporter(3);"  class="btn Fleft " > <span class="icon-plus"></span></a>
     		</td></tr>
     		</table>
             
             </td>
             <td><?= form_input('cruise_number[{$id}]', set_value("cruise_number[{\$id}]"), ' cruise_states_rel= "{$id}" class="validate[required]" style="width:40px" id="cruiseno[{$id}]"') ?></td>
             <td>
             <?= form_input('cruise_date[{$id}]', set_value("cruise_date[{\$id}]"), ' cruise_states_rel= "{$id}" class="validate[required] start_date_{$id}" style="width:100px" id="cruise_date[{$id}]"') ?>
             {start_date_script}
             </td>
             <td>
             <?= form_input('cruise_arrival_date[{$id}]', set_value("cruise_arrival_date[{\$id}]"), 'class="validate[required] end_date_{$id}" style="width:100px" id="cruise_arrival_date[{$id}]"') ?>
             {end_date_script}
             </td>
             
             <td style="width:220px">
             <?= form_dropdown('erp_port_id_from[{$id}]', $sea_erp_ports_sa, set_value("erp_port_id_from[{\$id}]"), '  erp_port_id_from_rel= "{$id}" class="validate[required]" style="width:80px" id="erp_port_id_from[{$id}]"') ?>
 			 -
			 <?= form_dropdown('erp_port_id_to[{$id}]', $sea_erp_ports, set_value("erp_port_id_to[{\$id}]"), '  erp_port_id_to_rel= "{$id}" class="validate[required]" style="width:80px" id="erp_port_id_to[{$id}]"') ?>
 			
             <td><?= form_input('seats_count[{$id}]', set_value("seats_count[{\$id}]"), 'class="validate[required] seatcount" style="width:50px" id="seatcont[{$id}]"') ?></td>
             <td><a href="javascript:void(0);" onclick="delete_cruises('{$id}')"><span class="icon-trash"></span> </a></td>
             
             </tr>
             */
            }.toString().replace(/{\$id}/g, id).slice(14, -3);

            new_row = new_row.replace(/{start_date_script}/, "<script> $('.start_date_"+id+"').datetimepicker({"+
                    "dateFormat: 'yy-mm-dd',"+
                    "timeFormat: 'HH:mm',"+
                    "onClose: function(selectedDate) {"+
                    "    $('.end_date_"+id+"').datetimepicker('option', 'minDate', selectedDate);"+
                    "}"+
                "});<\/script>");
                
   			new_row = new_row.replace(/{end_date_script}/, "<script> $('.end_date_"+id+"').datetimepicker({"+
   	                 "dateFormat: 'yy-mm-dd',"+
   	                 "timeFormat: 'HH:mm',"+
   	                 "onClose: function(selectedDate) {"+
   	                 "    $('.start_date_"+id+"').datetimepicker('option', 'maxDate', selectedDate);"+
   	                 "}"+
   	             "});<\/script>");
	             
            
            $('tbody#return_cruise_tbody').append(new_row);
            $('.datetimepicker').datetimepicker({dateFormat: 'yy-mm-dd', timeFormat: 'HH:mm'});
            c2++;

            if($('input[name="rdo_trip_return_type"]:checked').val()==1) {
            	$('#btn_cruise_trip_return').hide();
            }
        }
        load_multiselect();
        
        //fix_seats_count();

        
		if(id==0) {
			$('#safa_transporter_code\\[' + id + '\\]').val($('#last_going_safa_transporter_code').val());
        	$('#safa_transporter_code\\[' + id + '\\]').trigger("chosen:updated");
		} else if(id==100) {
			$('#safa_transporter_code\\[' + id + '\\]').val($('#last_return_safa_transporter_code').val());
        	$('#safa_transporter_code\\[' + id + '\\]').trigger("chosen:updated");
		} else {
        	$('#safa_transporter_code\\[' + id + '\\]').val($('#safa_transporter_code\\[' + (id-1) + '\\]').val());
        	$('#safa_transporter_code\\[' + id + '\\]').trigger("chosen:updated");
		}
        
    }

    function load_multiselect() {
    	var config = {
                '.chosen-select': {width: "100%"},
                '.chosen-select-deselect': {allow_single_deselect: true},
                '.chosen-select-no-single': {disable_search_threshold: 10},
                '.chosen-select-no-results': {no_results_text: 'Oops, nothing found!'},
                '.chosen-select-width': {width: "95%"}
            }
            for (var selector in config) {
                $(selector).chosen(config[selector]);
            }
    }
    
    function fix_seats_count() {
        var adultseat = parseInt($('input[name=adult_seats]').val());
        var childseat = parseInt($('input[name=child_seats]').val());
        var babyseat = parseInt($('input[name=baby_seats]').val());
        if (isNaN(adultseat))
            adultseat = 0;
        if (isNaN(childseat))
            childseat = 0;
        if (isNaN(babyseat))
            babyseat = 0;
        var seats = adultseat + childseat + babyseat;
        $('.seatcount').val(seats);
    }

    function remove_row(id)
    {
        $('tr[rel=' + id + ']').remove();

    }

</script>

<script>
$('#rdo_trip_return_type').live('change', function() {
	if($(this).val()==2) {
		if($('#erp_transportertype_id').val()==2) {
				$('#btn_flight_trip_return').show();
		} else if($('#erp_transportertype_id').val()==3) {
			$('#btn_cruise_trip_return').show();
		} 
	} else {
		if($('#erp_transportertype_id').val()==2) {
			if($('#div_trip_return_group tr').length<4) {
				//alert($('#div_trip_return_group tr').length);
				$('#btn_flight_trip_return').show();
			} else {
				$('#btn_flight_trip_return').hide();
			}
		} else if($('#erp_transportertype_id').val()==3) {
			if($('#div_trip_return_group tr').length<4) {
				//alert($('#div_trip_return_group tr').length);
				$('#btn_cruise_trip_return').show();
			} else {
				$('#btn_cruise_trip_return').hide();
			}
		}
	} 
});
</script>

<script>
    var h = 0;
    var t = 0;

    
    function new_hotel() {

    	
        var id = h;

        if($('#erp_transportertype_id').val()==2){
            
        if (!$('tbody#going_flight_tbody tr').length || !$('tbody#going_flight_tbody tr select.erp_path_type option[value=2]:selected').length) {
			if(!$('tbody#going_flight_tbody tr input.erp_path_type[value=2]').length) {
            	alert('<?php echo lang('you_should_add_arrival_trip_first');?>');
            	return false;
			}
        }  
        if (!$('tbody#return_flight_tbody tr').length || !$('tbody#return_flight_tbody tr select.erp_path_type option[value=1]:selected').length) {
        	if(!$('tbody#return_flight_tbody tr input.erp_path_type[value=1]').length) {
	            alert('<?php echo lang('you_should_add_return_trip_first');?>');
	            return false;
        	}
        } 

        //else {
        	var arrivedate='';
            if (id === 0) {
                var arriverel = $('tbody#going_flight_tbody tr select.erp_path_type option[value=2]:selected').parent().attr('pathrel');
                if($('input[arriverel=' + arriverel + ']').val()!=undefined){
                 	arrivedate = ($('input[arriverel=' + arriverel + ']')).val();
                } else {
                	arrivedate = $('#last_exit_date').val();
                }

                if(arrivedate=='') {
            		alert('<?php echo lang('you_should_add_going_datetime');?>');
                    return false;
            	}
            	
            } else {
            	
                var newrel = id - 1;
                if($('input[endrel=' + newrel + ']').val()!=undefined){
                	arrivedate = $('input[endrel=' + newrel + ']').val();
                }
            }
        //}
        } else if($('#erp_transportertype_id').val()==1){
        	if($('#land_going_datetime').val()=='') {
        		alert('<?php echo lang('you_should_add_going_datetime');?>');
                return false;
        	}
			if($('#land_return_datetime').val()=='') {
				alert('<?php echo lang('you_should_add_return_datetime');?>');
	            return false;
        	}

			var arrivedate='';
			//if ($('#div_hotels tr').length === 1) {
			if ($('#div_hotels tr').length === 2) {
                 	arrivedate = $('#land_going_datetime').val()
					var arrivedatetime_arr = arrivedate.split(' ');
                 	arrivedate = arrivedatetime_arr[0];
            } else {
                var newrel = id - 1;
                if($('input[endrel=' + newrel + ']').val()!=undefined){
                	arrivedate = $('input[endrel=' + newrel + ']').val();
                }
            }

        } else if($('#erp_transportertype_id').val()==3){
//        	if($('#sea_going_datetime').val()=='') {
//        		alert('<?php echo lang('you_should_add_going_datetime');?>');
//                return false;
//        	}
//			if($('#sea_return_datetime').val()=='') {
//				alert('<?php echo lang('you_should_add_return_datetime');?>');
//	            return false;
//        	}
//
//			if ($('#div_hotels tr').length === 1) {
//	              	arrivedate = $('#sea_going_datetime').val()
//	         } else {
//	             var newrel = id - 1;
//	             if($('input[endrel=' + newrel + ']').val()!=undefined){
//	             	arrivedate = $('input[endrel=' + newrel + ']').val();
//	             }
//	         }

    		if (!$('tbody#going_cruise_tbody tr').length || !$('tbody#going_cruise_tbody tr select.erp_path_type option[value=2]:selected').length) {
			if(!$('tbody#going_cruise_tbody tr input.erp_path_type[value=2]').length) {
            	alert('<?php echo lang('you_should_add_arrival_trip_first');?>');
            	return false;
			}
        }  
        if (!$('tbody#return_cruise_tbody tr').length || !$('tbody#return_cruise_tbody tr select.erp_path_type option[value=1]:selected').length) {
        	if(!$('tbody#return_cruise_tbody tr input.erp_path_type[value=1]').length) {
            alert('<?php echo lang('you_should_add_return_trip_first');?>');
            return false;
        	}
        } 

        	var arrivedate='';
            if (id === 0) {
                var arriverel = $('tbody#going_cruise_tbody tr select.erp_path_type option[value=2]:selected').parent().attr('pathrel');
                if($('input[arriverel=' + arriverel + ']').val()!=undefined){
                 	arrivedate = ($('input[arriverel=' + arriverel + ']')).val();
                } else {
                	arrivedate = $('#last_exit_date').val();
                }

                if(arrivedate=='') {
            		alert('<?php echo lang('you_should_add_going_datetime');?>');
                    return false;
            	}
            	
            } else {
            	
                var newrel = id - 1;
                if($('input[endrel=' + newrel + ']').val()!=undefined){
                	arrivedate = $('input[endrel=' + newrel + ']').val();
                }
            }
        }

		if($('#erp_transportertype_id').val()==1) {
            
        	var new_row = function() {/*
                <tr class="table-row" hrel="{$id}">
                <td>
                <input type='hidden' name='hotel_safa_internalsegment_id[{$id}]'  id='hotel_safa_internalsegment_id[{$id}]'  value='0'/>
                           
       		<?= form_dropdown('erp_city_id[{$id}]', $cities, set_value("erp_city_id[{\$id}]"), 'class="validate[required] " cityrel="{$id}" id="erp_city_id[{$id}]"') ?></td>
                <td>

                	<table style='border:0px;'><tr inner_hrel='{$id}'><td style='border:0px;'>
       			<?= form_dropdown('erp_hotel_id[{$id}]', $hotels, set_value("erp_hotel_id[{\$id}]"), 'class="validate[required] chosen-select erp_hotel_id" hotelrel="{$id}" id="erp_hotel_id[{$id}]"') ?>
       			</td><td style='border:0px;'>
       			<a href="javascript:void(0);" onclick="add_hotel({$id});"  class="btn Fleft " > <span class="icon-plus"></span></a>
       			</td></tr>
       			</table>

       		</td>

       		
            
       						
               <td><?= form_input('arrival_date[{$id}]', set_value("arrival_date[{\$id}]", "{arrivedate}"), 'class="validate[required] start_hotel_datepicker" startrel="{$id}" readonly="readonly" style="width:100px" id="arrival_date[{$id}]"') ?></td>
                <td><?= form_input('nights[{$id}]', set_value("nights[{\$id}]"), 'class="validate[required]" nightsrel="{$id}" onchange="fix_dates_for_bus_trip();" style="width:50px" id="class[{$id}]"') ?></td>
                <td><?= form_input('exit_date[{$id}]', set_value("exit_date[{\$id}]"), 'class="validate[required] end_hotel_datepicker" endrel="{$id}" style="width:100px" readonly="readonly" id="exit_date[{$id}]"') ?></td>
                <td>
                {start_hotel_datepicker_script}
                
                <a href="javascript:void(0);" onclick="remove_hrow('{$id}')"><span class="icon-trash"></span> </a></td>
                </tr>

				
				
                <tr rel="{$id}">
       		 <td colspan="6">
                <table >
                <tr inner_hrel='{$id}'>
                <td>
                <span class="FRight"><?= lang('rooms_count') ?></span>
                </td>
       		
       		<? if (check_array($erp_hotelroomsizes)) : ?>
       		<? foreach ($erp_hotelroomsizes as $erp_hotelroomsize) : ?>
       		<td>
       		<?= $erp_hotelroomsize->{name()} ?>
       		<?=
       		 form_input('hotels_rooms_count[{$id}][' . $erp_hotelroomsize->erp_hotelroomsize_id . ']'
       		         , set_value('hotels_rooms_count[{$id}][' . $erp_hotelroomsize->erp_hotelroomsize_id . ']')
       		         , 'class="input-small FRight" style="width: 50px !important;" pkgid=\' + {$id} + \' rpnrel="' . $erp_hotelroomsize->erp_hotelroomsize_id . '"  id=\"hotels_rooms_count_\' + {$id} + \'_' . $erp_hotelroomsize->erp_hotelroomsize_id . '\" placeholder="0"')
       		 ?></td>
       		<? endforeach ?>
       		<? endif ?>
       		</tr>
       		</table>
        		</td>
                </tr>
                */
               }.toString().replace(/{\$id}/g, id).replace(/{arrivedate}/g, arrivedate).slice(14, -3);


           if(h==0) {
   			new_row = new_row.replace(/{start_hotel_datepicker_script}/, 
   		   	"<script>"+
   		   	" var enddatatime = $('#land_going_datetime').val();"+
	   		"var enddatatime_arr = enddatatime.split(' ');"+
	     	"var endtime_arr = (enddatatime_arr[1]).split(':');"+
	     	"var enddate_arr = (enddatatime_arr[0]).split('-');"+
	     	"var enddatatime_obj = new Date(enddate_arr[0], enddate_arr[1]-1, enddate_arr[2], endtime_arr[0], endtime_arr[1]);"+
	     	"enddatatime_obj.setDate(enddatatime_obj.getDate() + 1);"+
	     	"enddatatime = $.datepicker.formatDate('yy-mm-dd', enddatatime_obj);"+
     	
   		   	"$('.start_hotel_datepicker').datepicker({"+
   		   	" dateFormat: 'yy-mm-dd',"+
   		 	" minDate: new Date(enddatatime_arr[0]),"+
         	" maxDate: new Date(enddatatime)"+
   		   	"});"+
   		   	"<\/script>"
   		   	);
           } else {
        	   new_row = new_row.replace(/{start_hotel_datepicker_script}/, "");
           }
       
            
        
            
        } else if($('input[name="rdo_trip_return_type"]:checked').val()==1) {
            
        var new_row = function() {/*
         <tr class="table-row" hrel="{$id}">
         <td>
         <input type='hidden' name='hotel_safa_internalsegment_id[{$id}]'  id='hotel_safa_internalsegment_id[{$id}]'  value='0'/>
                    
		<?= form_dropdown('erp_city_id[{$id}]', $cities, set_value("erp_city_id[{\$id}]"), 'class="validate[required] " cityrel="{$id}" id="erp_city_id[{$id}]"') ?></td>
         <td>

         	<table style='border:0px;'><tr inner_hrel='{$id}'><td style='border:0px;'>
			<?= form_dropdown('erp_hotel_id[{$id}]', $hotels, set_value("erp_hotel_id[{\$id}]"), 'class="validate[required] chosen-select erp_hotel_id" hotelrel="{$id}" id="erp_hotel_id[{$id}]"') ?>
			</td><td style='border:0px;'>
			<a href="javascript:void(0);" onclick="add_hotel({$id});"  class="btn Fleft " > <span class="icon-plus"></span></a>
			</td></tr>
			</table>

		</td>

						
        <td><?= form_input('arrival_date[{$id}]', set_value("arrival_date[{\$id}]", "{arrivedate}"), 'class="validate[required]" startrel="{$id}" readonly="readonly" style="width:100px" id="arrival_date[{$id}]"') ?></td>
         <td><?= form_input('nights[{$id}]', set_value("nights[{\$id}]"), 'class="validate[required]" nightsrel="{$id}" onchange="fix_dates();" style="width:50px" id="class[{$id}]"') ?></td>
         <td><?= form_input('exit_date[{$id}]', set_value("exit_date[{\$id}]"), 'class="validate[required]" endrel="{$id}" style="width:100px" readonly="readonly" id="exit_date[{$id}]"') ?></td>
         <td><a href="javascript:void(0);" onclick="remove_hrow('{$id}')"><span class="icon-trash"></span> </a></td>
         </tr>

         <tr rel="{$id}">
		 <td colspan="6">
         <table >
         <tr inner_hrel='{$id}'>
         <td>
         <span class="FRight"><?= lang('rooms_count') ?></span>
         </td>
		
		<? if (check_array($erp_hotelroomsizes)) : ?>
		<? foreach ($erp_hotelroomsizes as $erp_hotelroomsize) : ?>
		<td>
		<?= $erp_hotelroomsize->{name()} ?>
		<?=
		 form_input('hotels_rooms_count[{$id}][' . $erp_hotelroomsize->erp_hotelroomsize_id . ']'
		         , set_value('hotels_rooms_count[{$id}][' . $erp_hotelroomsize->erp_hotelroomsize_id . ']')
		         , 'class="input-small FRight" style="width: 50px !important;" pkgid=\' + {$id} + \' rpnrel="' . $erp_hotelroomsize->erp_hotelroomsize_id . '"  id=\"hotels_rooms_count_\' + {$id} + \'_' . $erp_hotelroomsize->erp_hotelroomsize_id . '\" placeholder="0"')
		 ?></td>
		<? endforeach ?>
		<? endif ?>
		</tr>
		</table>
 		</td>
         </tr>
         */
        }.toString().replace(/{\$id}/g, id).replace(/{arrivedate}/g, arrivedate).slice(14, -3);


        
        } else  {
        	
        	var new_row = function() {/*
                <tr class="table-row" hrel="{$id}">
                <td>
                <input type='hidden' name='hotel_safa_internalsegment_id[{$id}]'  id='hotel_safa_internalsegment_id[{$id}]'  value='0'/>
                           
       		<?= form_dropdown('erp_city_id[{$id}]', $cities, set_value("erp_city_id[{\$id}]"), 'class="validate[required] " cityrel="{$id}" id="erp_city_id[{$id}]"') ?></td>
                <td>

                	<table style='border:0px;'><tr inner_hrel='{$id}'><td style='border:0px;'>
       			<?= form_dropdown('erp_hotel_id[{$id}]', $hotels, set_value("erp_hotel_id[{\$id}]"), 'class="validate[required] chosen-select erp_hotel_id" hotelrel="{$id}" id="erp_hotel_id[{$id}]"') ?>
       			</td><td style='border:0px;'>
       			<a href="javascript:void(0);" onclick="add_hotel({$id});"  class="btn Fleft " > <span class="icon-plus"></span></a>
       			</td></tr>
       			</table>

       		</td>

       		
            
       						
               <td><?= form_input('arrival_date[{$id}]', set_value("arrival_date[{\$id}]", "{arrivedate}"), 'class="validate[required] start_hotel_datepicker" startrel="{$id}" readonly="readonly" style="width:100px" id="arrival_date[{$id}]"') ?></td>
                <td><?= form_input('nights[{$id}]', set_value("nights[{\$id}]"), 'class="validate[required]" nightsrel="{$id}" onchange="fix_dates_multi_return_trip();" style="width:50px" id="class[{$id}]"') ?></td>
                <td><?= form_input('exit_date[{$id}]', set_value("exit_date[{\$id}]"), 'class="validate[required] end_hotel_datepicker" endrel="{$id}" style="width:100px" readonly="readonly" id="exit_date[{$id}]"') ?></td>
                <td>
                {start_hotel_datepicker_script}
                {end_hotel_datepicker_script}
                <a href="javascript:void(0);" onclick="remove_hrow('{$id}')"><span class="icon-trash"></span> </a></td>
                </tr>

				
				
                <tr rel="{$id}">
       		 <td colspan="6">
                <table >
                <tr inner_hrel='{$id}'>
                <td>
                <span class="FRight"><?= lang('rooms_count') ?></span>
                </td>
       		
       		<? if (check_array($erp_hotelroomsizes)) : ?>
       		<? foreach ($erp_hotelroomsizes as $erp_hotelroomsize) : ?>
       		<td>
       		<?= $erp_hotelroomsize->{name()} ?>
       		<?=
       		 form_input('hotels_rooms_count[{$id}][' . $erp_hotelroomsize->erp_hotelroomsize_id . ']'
       		         , set_value('hotels_rooms_count[{$id}][' . $erp_hotelroomsize->erp_hotelroomsize_id . ']')
       		         , 'class="input-small FRight" style="width: 50px !important;" pkgid=\' + {$id} + \' rpnrel="' . $erp_hotelroomsize->erp_hotelroomsize_id . '"  id=\"hotels_rooms_count_\' + {$id} + \'_' . $erp_hotelroomsize->erp_hotelroomsize_id . '\" placeholder="0"')
       		 ?></td>
       		<? endforeach ?>
       		<? endif ?>
       		</tr>
       		</table>
        		</td>
                </tr>
                */
               }.toString().replace(/{\$id}/g, id).replace(/{arrivedate}/g, arrivedate).slice(14, -3);


               
   			new_row = new_row.replace(/{start_hotel_datepicker_script}/, "<script>$('.start_hotel_datepicker').datepicker({dateFormat: 'yy-mm-dd'});<\/script>").replace(/{end_hotel_datepicker_script}/, "<script>$('.end_hotel_datepicker').datepicker({dateFormat: 'yy-mm-dd'});<\/script>");
        }

        
        $('tbody.internalhotels').append(new_row);
        $('.datetimepicker').datetimepicker({dateFormat: 'yy-mm-dd', timeFormat: 'HH:mm'});
        $('.chosen-select').chosen({width: "100%"});
        h++;



      //----------------- Hotels --------------------------------
	    var cityid = 0;
	    var dataString = "cityid="+ cityid;
	    $.ajax
	    ({
	    type: "POST",
	    url: "<?php echo base_url().'safa_trip_internaltrips/get_hotel_by_city'; ?>",
	    data: dataString,
	    cache: false,
	    success: function(html)
	    {
	        //alert(html);
	        var data_arr = JSON.parse(html);
	            
	        $('#erp_hotel_id\\['+id+'\\]').html(data_arr[0]['hotels_options']);
	        $('#erp_hotel_id\\['+id+'\\]').trigger("chosen:updated");
	
	    }
	    });
	//---------------------------------------------------------
        
        
    }

    function remove_hrow(id)
    {
        $('tr[hrel=' + id + ']').remove();
        $('.internalhotels').find('tr[rel=' + id + ']').remove();
        
        var hidden_input = '<input type="hidden" name="hotels_remove[]" value="' + id + '" />';
        $('#div_tbl_hotels').append(hidden_input);

        h--;
    }

    $('tbody.internalhotels').on('change', 'select[cityrel]', function() {
        var rowrel = $(this).attr('cityrel');
        var cityid = $(this).val();
        
        var dataString = "cityid="+ cityid;
        $.ajax
        ({
        	type: "POST",
        	url: "<?php echo base_url().'safa_trip_internaltrips/get_hotel_by_city'; ?>",
        	data: dataString,
        	cache: false,
        	success: function(html)
        	{
            	//alert(html);
            	var data_arr = JSON.parse(html);
                	
	         	$('select[hotelrel=' + rowrel + ']').html(data_arr[0]['hotels_options']);
	         	$('select[hotelrel=' + rowrel + ']').trigger("chosen:updated");
        	}
        });
        
    });


    $('tbody.cls_tbl_filghts').on('change', 'select[erp_port_id_from_rel]', function() {
        var rowrel = $(this).attr('erp_port_id_from_rel');
        var erp_port_id_from = $(this).val();
        $.get('<?= site_url('safa_trip_internaltrips/get_ports_halls_by_port/') ?>/' + erp_port_id_from, function(data) {
            $('select[erp_port_hall_id_from_rel=' + rowrel + ']').html(data);
            $('select[erp_port_hall_id_from_rel=' + rowrel + ']').trigger("chosen:updated");
        });
    });
    
    $('tbody.cls_tbl_filghts').on('change', 'select[erp_port_id_to_rel]', function() {
        var rowrel = $(this).attr('erp_port_id_to_rel');
        var erp_port_id_to = $(this).val();
        $.get('<?= site_url('safa_trip_internaltrips/get_ports_halls_by_port/') ?>/' + erp_port_id_to, function(data) {
            $('select[erp_port_hall_id_to_rel=' + rowrel + ']').html(data);
            $('select[erp_port_hall_id_to_rel=' + rowrel + ']').trigger("chosen:updated");
        });
    });

    $('tbody.cls_tbl_filghts').on('change', 'select[flight_states_rel],input[flight_states_rel]', function() {
        var rowrel = $(this).attr('flight_states_rel');
        
        var safa_transporter_code = $('select[flight_states_rel=' + rowrel + ']').val();
        var flight_number = $('#flightno\\[' + rowrel + '\\]').val();
        var flight_date = $('#flight_date\\[' + rowrel + '\\]').val();

        //alert(safa_transporter_code+'-'+flight_number+'-'+flight_date);

        var dataString = 'safa_transporter_code='+ safa_transporter_code+'&flight_number='+ flight_number+'&flight_date='+ flight_date;
        $.ajax
        ({
        	type: 'POST',
        	url: '<?php echo base_url().'safa_trip_internaltrips/get_ports_halls_by_states'; ?>',
        	data: dataString,
        	cache: false,
        	success: function(html)
        	{
        		var data_arr = JSON.parse(html);

        		var erp_port_id_from_value = data_arr[0]['erp_port_id_from'];
        		var erp_port_hall_id_from_value = data_arr[0]['erp_port_hall_id_from'];
        		var erp_port_id_to_value = data_arr[0]['erp_port_id_to'];
        		var erp_port_hall_id_to_value = data_arr[0]['erp_port_hall_id_to'];

				if(erp_port_id_from_value!='') {
        			$('#erp_port_id_from\\[' + rowrel + '\\]').val(erp_port_id_from_value);
				}
				if(erp_port_hall_id_from_value!='') {
	        		if($('#erp_port_hall_id_from\\[' + rowrel + '\\]').val()==1 && erp_port_hall_id_from_value==3 ) {
	
	            	} else if($('#erp_port_hall_id_from\\[' + rowrel + '\\]').val()==3 && erp_port_hall_id_from_value==1 ) {
	
	            	} else {
	        			$('#erp_port_hall_id_from\\[' + rowrel + '\\]').val(erp_port_hall_id_from_value);
	        		}
				}

				if(erp_port_id_to_value!='') {
        			$('#erp_port_id_to\\[' + rowrel + '\\]').val(erp_port_id_to_value);
				}
				if(erp_port_hall_id_to_value!='') {
	        		if($('#erp_port_hall_id_to\\[' + rowrel + '\\]').val()==1 && erp_port_hall_id_to_value==3 ) {
	
	            	} else if($('#erp_port_hall_id_to\\[' + rowrel + '\\]').val()==3 && erp_port_hall_id_to_value==1 ) {
	
	            	} else {
	        			$('#erp_port_hall_id_to\\[' + rowrel + '\\]').val(erp_port_hall_id_to_value);
	        		}
				}
        		
        		
        	}
        });
    });
    
    function new_tourismplace() {
        var id = t;
        var new_row = function() {/*
         <tr class="table-row" trel="{$id}">
         <td>
         <input type='hidden' name='tourism_safa_internalsegment_id[{$id}]'  id='tourism_safa_internalsegment_id[{$id}]'  value='0'/>


        	<?php 
            echo '       <select name="safa_tourismplace_id[{$id}]" class="validate[required] chosen-select chosen-rtl safa_tourismplace_id"  tourismrel="{$id}" id="safa_tourismplace_id[{$id}]" >';
            foreach($safa_tourismplaces as $key => $value){
            	$key_arr = explode('---',$key);
            	$safa_tourismplace_id = $key_arr[0];
            	$erp_city_id = $key_arr[1];
            
            echo "           <option value=\"$safa_tourismplace_id\" city_id=\"$erp_city_id\" >$value</option>";
            } 
            echo "    </select>";
         ?> 
		
         <!--
         <?= form_dropdown('safa_tourismplace_id[{$id}]', $safa_tourismplaces, set_value("safa_tourismplace_id[{\$id}]"), 'class="validate[required] chosen-select chosen-rtl safa_tourismplace_id" tourismrel="{$id}" id="safa_tourismplace_id[{$id}]"') ?>
		 -->
		 
		</td>
         <td><?= form_input('tourism_date[{$id}]', set_value("tourism_date[{\$id}]"), 'class="validate[required] datetimepicker" tourdaterel="{$id}" readonly="readonly" style="width:150px" id="tourism_date[{$id}]"') ?></td>
         <td><a href="javascript:void(0);" onclick="remove_trow('{$id}')"><span class="icon-trash"></span> </a></td>
         </tr>
         */
        }.toString().replace(/{\$id}/g, id).slice(14, -3);

        $('tbody.tourismplaces').append(new_row);
        var selectrel = $('select[cityrel] option[value=1]:selected').parent().attr('cityrel');
		//alert(selectrel);
        if(selectrel==undefined) {
            var selectrel = $('input[cityrel][value=1]').attr('cityrel');
        }
        
        $('input[tourdaterel=' + id + ']').datetimepicker({
            dateFormat: 'yy-mm-dd',
            minDate: new Date($('input[startrel=' + selectrel + ']').val()),
            maxDate: new Date($('input[endrel=' + selectrel + ']').val())
        }).val(addDays(new Date($('input[startrel=' + selectrel + ']').val()), parseInt($('input[nightsrel=' + selectrel + ']').val()) / 2) + " 07:00");
        t++;
    }

    function remove_trow(id)
    {
        $('tr[trel=' + id + ']').remove();
        
        var hidden_input = '<input type="hidden" name="tourismplaces_remove[]" value="' + id + '" />';
        $('#div_tbl_tourismplaces').append(hidden_input);
        t--;
    }

    $('tbody.tourismplaces').on('change', 'select[tourismrel]', function() {
       
        var trow = $(this).attr('tourismrel');
        //alert(trow);
        if ($(this).val() == 1) {
            var selectrel = $('select[cityrel] option[value=1]:selected').parent().attr('cityrel');
        } else {
            var selectrel = $('select[cityrel] option[value=2]:selected').parent().attr('cityrel');
        }
        //alert(selectrel);
        if(selectrel==null) {
        	if ($(this).val() == 1) {
                var selectrel = $('input[cityrel][value=1]').attr('cityrel');
            } else {
                var selectrel = $('input[cityrel][value=2]').attr('cityrel');
            }
        } 
        
        
        $('input[tourdaterel=' + trow + ']').datetimepicker("destroy").datetimepicker({
            dateFormat: 'yy-mm-dd',
            minDate: new Date($('input[startrel=' + selectrel + ']').val()),
            maxDate: new Date($('input[endrel=' + selectrel + ']').val())
        }).val(addDays(new Date($('input[startrel=' + selectrel + ']').val()), parseInt($('input[nightsrel=' + selectrel + ']').val()) / 2) + " 07:00");

        //By Gouda.
		if($('input[startrel=' + selectrel + ']').val()==undefined) {
			$('#tourism_date\\[' + trow + '\\]').val('');
		}
        
    });

</script>
<script type="text/javascript">

    function fix_dates() {
    	if($('#erp_transportertype_id').val()==2){
        	
        var arriverel = $('tbody#going_flight_tbody tr select.erp_path_type option[value=2]:selected').parent().attr('pathrel');
		if(arriverel==undefined) {
			arriverel = $('tbody#going_flight_tbody tr input.erp_path_type[value=2]').parent().attr('pathrel');
		}
        //alert(arriverel);
        var arrivedate = ($('input[arriverel=' + arriverel + ']')).val();
        //alert(arrivedate);
        
        if(arrivedate==undefined) {
	        if($('#last_exit_date').val()!=undefined) {
				arrivedate = $('#last_exit_date').val();
				//alert(arrivedate+'  ffff');
			}
        } else {
        	arrivedatearray = arrivedate.split(' ');
        	arrivedate = arrivedatearray[0];
		}
        //alert(arrivedate);
    	} else if($('#erp_transportertype_id').val()==1){
    		arrivedate = $('#land_going_datetime').val();
    		arrivedatearray = arrivedate.split(' ');
        	arrivedate = arrivedatearray[0];
    		
    	} else if($('#erp_transportertype_id').val()==3){
//    		arrivedate = $('#sea_going_datetime').val();
//    		arrivedatearray = arrivedate.split(' ');
//        	arrivedate = arrivedatearray[0];

			var arriverel = $('tbody#going_cruise_tbody tr select.erp_path_type option[value=2]:selected').parent().attr('pathrel');
			if(arriverel==undefined) {
				arriverel = $('tbody#going_cruise_tbody tr input.erp_path_type[value=2]').parent().attr('pathrel');
			}
	        //alert(arriverel);
	        var arrivedate = ($('input[arriverel=' + arriverel + ']')).val();
	        //alert(arrivedate);
	        
	        if(arrivedate==undefined) {
		        if($('#last_exit_date').val()!=undefined) {
					arrivedate = $('#last_exit_date').val();
					//alert(arrivedate+'  ffff');
				}
	        } else {
	        	arrivedatearray = arrivedate.split(' ');
	        	arrivedate = arrivedatearray[0];
			}
    	} 
        
        var enddate = '';
        var first = true;
        $('tbody.internalhotels tr[hrel]').each(function() {
            var rel = $(this).attr('hrel');

            if (!first) {
                arrivedate = enddate;
            }
			
            //alert(rel);
            
            first = false;

            $('input[startrel=' + rel + ']').val(arrivedate);
            var arrdate = new Date(arrivedate);
            enddate = addDays(arrdate, parseInt($('input[nightsrel=' + rel + ']').val()));
            $('input[endrel=' + rel + ']').val(enddate);

            $('#last_exit_date').val(enddate);
        });

    }

    function fix_dates_multi_return_trip() 
    {
    	if($('#erp_transportertype_id').val()==2){
        	
        var arriverel = $('tbody#going_flight_tbody tr select.erp_path_type option[value=2]:selected').parent().attr('pathrel');
		if(arriverel==undefined) {
			arriverel = $('tbody#going_flight_tbody tr input.erp_path_type[value=2]').parent().attr('pathrel');
		}
        //alert(arriverel);
        var arrivedate = ($('input[arriverel=' + arriverel + ']')).val();
        //alert(arrivedate);
        
        if(arrivedate==undefined) {
	        if($('#last_exit_date').val()!=undefined) {
				arrivedate = $('#last_exit_date').val();
				//alert(arrivedate+'  ffff');
			}
        } else {
        	arrivedatearray = arrivedate.split(' ');
        	arrivedate = arrivedatearray[0];
		}
        //alert(arrivedate);
    	} else if($('#erp_transportertype_id').val()==1){
    		arrivedate = $('#land_going_datetime').val();
    		arrivedatearray = arrivedate.split(' ');
        	arrivedate = arrivedatearray[0];
    		
    	} else if($('#erp_transportertype_id').val()==3){
//    		arrivedate = $('#sea_going_datetime').val();
//    		arrivedatearray = arrivedate.split(' ');
//        	arrivedate = arrivedatearray[0];

			var arriverel = $('tbody#going_cruise_tbody tr select.erp_path_type option[value=2]:selected').parent().attr('pathrel');
			if(arriverel==undefined) {
				arriverel = $('tbody#going_cruise_tbody tr input.erp_path_type[value=2]').parent().attr('pathrel');
			}
	        //alert(arriverel);
	        var arrivedate = ($('input[arriverel=' + arriverel + ']')).val();
	        //alert(arrivedate);
	        
	        if(arrivedate==undefined) {
		        if($('#last_exit_date').val()!=undefined) {
					arrivedate = $('#last_exit_date').val();
					//alert(arrivedate+'  ffff');
				}
	        } else {
	        	arrivedatearray = arrivedate.split(' ');
	        	arrivedate = arrivedatearray[0];
			}
    	} 
        
        var enddate = '';
        var first = true;
        $('tbody.internalhotels tr[hrel]').each(function() {
            var rel = $(this).attr('hrel');

            if (!first) {
                arrivedate = enddate;
            } else {
            	$('input[startrel=' + rel + ']').val(arrivedate);
            }
			
            //alert(rel);
            
            first = false;

            //$('input[startrel=' + rel + ']').val(arrivedate);
            //var arrdate = new Date(arrivedate);
            
            var arrdate =new Date($('input[startrel=' + rel + ']').val());
                
            enddate = addDays(arrdate, parseInt($('input[nightsrel=' + rel + ']').val()));
            $('input[endrel=' + rel + ']').val(enddate);

            $('#last_exit_date').val(enddate);
        });

    }

    function fix_dates_for_bus_trip() 
    {
        
    	if($('#erp_transportertype_id').val()==2){
        	
        var arriverel = $('tbody#going_flight_tbody tr select.erp_path_type option[value=2]:selected').parent().attr('pathrel');
		if(arriverel==undefined) {
			arriverel = $('tbody#going_flight_tbody tr input.erp_path_type[value=2]').parent().attr('pathrel');
		}
        //alert(arriverel);
        var arrivedate = ($('input[arriverel=' + arriverel + ']')).val();
        //alert(arrivedate);
        
        if(arrivedate==undefined) {
	        if($('#last_exit_date').val()!=undefined) {
				arrivedate = $('#last_exit_date').val();
				//alert(arrivedate+'  ffff');
			}
        } else {
        	arrivedatearray = arrivedate.split(' ');
        	arrivedate = arrivedatearray[0];
		}
        //alert(arrivedate);
    	} else if($('#erp_transportertype_id').val()==1){
    		arrivedate = $('#land_going_datetime').val();
    		arrivedatearray = arrivedate.split(' ');
        	arrivedate = arrivedatearray[0];
    		
    	} else if($('#erp_transportertype_id').val()==3){
//    		arrivedate = $('#sea_going_datetime').val();
//    		arrivedatearray = arrivedate.split(' ');
//        	arrivedate = arrivedatearray[0];

			var arriverel = $('tbody#going_cruise_tbody tr select.erp_path_type option[value=2]:selected').parent().attr('pathrel');
			if(arriverel==undefined) {
				arriverel = $('tbody#going_cruise_tbody tr input.erp_path_type[value=2]').parent().attr('pathrel');
			}
	        //alert(arriverel);
	        var arrivedate = ($('input[arriverel=' + arriverel + ']')).val();
	        //alert(arrivedate);
	        
	        if(arrivedate==undefined) {
		        if($('#last_exit_date').val()!=undefined) {
					arrivedate = $('#last_exit_date').val();
					//alert(arrivedate+'  ffff');
				}
	        } else {
	        	arrivedatearray = arrivedate.split(' ');
	        	arrivedate = arrivedatearray[0];
			}
    	} 
        
        var enddate = '';
        var first = true;
        $('tbody.internalhotels tr[hrel]').each(function() {
            var rel = $(this).attr('hrel');

            if (!first) {
                arrivedate = enddate;
            } else {
            	//$('input[startrel=' + rel + ']').val(arrivedate);
            }
			
            //alert(rel);
            if (!first) {
            	$('input[startrel=' + rel + ']').val(arrivedate);
            }
            
            first = false;

            
            var arrdate = new Date(arrivedate);
            
            var arrdate =new Date($('input[startrel=' + rel + ']').val());
                
            enddate = addDays(arrdate, parseInt($('input[nightsrel=' + rel + ']').val()));
            $('input[endrel=' + rel + ']').val(enddate);

            $('#last_exit_date').val(enddate);
        });

    }
    

    if($('input[name="rdo_trip_return_type"]:checked').val()==1) {
	    $('tbody#going_flight_tbody').on('change', 'input[arriverel]', function() {
	        fix_dates();
	    });

	    $('tbody#going_cruise_tbody').on('change', 'input[arriverel]', function() {
	        fix_dates();
	    });
    }

    function addDays(myDate, days)
    {
        var date_standard = new Date(myDate.getTime() + days * 24 * 60 * 60 * 1000);
        var month = (date_standard.getMonth() + 1);
        var return_date = date_standard.getFullYear() + "-" + ('0' + month).slice(-2) + "-" + ('0' + date_standard.getDate()).slice(-2);
        return return_date;
    }

    function substractDays(myDate, days)
    {
        var date_standard = new Date(myDate.getTime() - days * 24 * 60 * 60 * 1000);
        var month = (date_standard.getMonth() + 1);
        var return_date = date_standard.getFullYear() + "-" + ('0' + month).slice(-2) + "-" + ('0' + date_standard.getDate()).slice(-2);
        return return_date;
    }


</script>



<script>
function add_hotel(hotel_counter)
{
	$.fancybox({
	   'type': 'iframe', 
	   'width' : 500,
	   'height' : 300,
	   'autoDimensions' : false,
	   'autoScale' : false,
	   'href' : "<?php echo site_url('safa_trip_internaltrips/add_hotel_popup'); ?>/"+hotel_counter,

	   afterClose: function() {
           //location.reload();

       jQuery.ajaxSetup({async:false});
           
		$('#div_hotels tr').each(function () {
       	   var rowrel = $(this).attr('inner_hrel');
       	   //alert(rowrel);
           var cityid = $('select[cityrel=' + rowrel + ']').val();
           //alert(cityid);
           
           if(cityid!=undefined) {
	           var old_value = $('#erp_hotel_id\\[' + rowrel + '\\]').val();

	           var dataString = "cityid="+ cityid;
	           $.ajax
	           ({
	           	type: "POST",
	           	url: "<?php echo base_url().'safa_trip_internaltrips/get_hotel_by_city'; ?>",
	           	data: dataString,
	           	cache: false,
	           	success: function(html)
	           	{
	               	//alert(html);
	               	var data_arr = JSON.parse(html);
	                   	
	                $('#erp_hotel_id\\[' + rowrel + '\\]').html(data_arr[0]['hotels_options']);
		            $('#erp_hotel_id\\[' + rowrel + '\\]').trigger("chosen:updated");

		            $('#erp_hotel_id\\[' + rowrel + '\\]').val(data_arr[0]['hotel_selected'])
			        $('#erp_hotel_id\\[' + rowrel + '\\]').trigger("chosen:updated");
	           	}
	           });
	
	           
           }

           
		});
           
       }
    
	 });
			
}
</script>


<script>
function add_bus_brand(bus_brand_counter)
{
	$.fancybox({
	   'type': 'iframe', 
	   'width' : 500,
	   'height' : 300,
	   'autoDimensions' : false,
	   'autoScale' : false,
	   'href' : "<?php echo site_url('safa_trip_internaltrips/add_bus_brand_popup'); ?>/"+bus_brand_counter,

	   afterClose: function() {
           //location.reload();

       jQuery.ajaxSetup({async:false});
           
		$('#tbl_drivers_and_buses tr').each(function () {
       	   var rowrel = $(this).attr('rel');
       	   //alert(rowrel);
           
           
				var dataString='';
	           $.ajax
	           ({
	           	type: "POST",
	           	url: "<?php echo base_url().'safa_trip_internaltrips/get_bus_brands'; ?>",
	           	data: dataString,
	           	cache: false,
	           	success: function(html)
	           	{
	               	//alert(html);
	               	var data_arr = JSON.parse(html);
	                   	
	                $('#safa_buses_brands_'+rowrel).html(data_arr[0]['bus_brands_options']);
		            $('#safa_buses_brands_'+rowrel).trigger("chosen:updated");

		            $('#safa_buses_brands_'+rowrel).val(data_arr[0]['bus_brand_selected'])
			        $('#safa_buses_brands_'+rowrel).trigger("chosen:updated");
	           	}
	           });
	           
           

           
		});
           
       }
    
	 });
			
}

function add_bus_model(bus_model_counter)
{
	var safa_buses_brands_id = $('#safa_buses_brands_'+bus_model_counter).val();
	//alert(safa_buses_brands_id);
	
	$.fancybox({
	   'type': 'iframe', 
	   'width' : 500,
	   'height' : 300,
	   'autoDimensions' : false,
	   'autoScale' : false,
	   'href' : "<?php echo site_url('safa_trip_internaltrips/add_bus_model_popup'); ?>/"+bus_model_counter+"/"+safa_buses_brands_id,

	   afterClose: function() {
           //location.reload();

       jQuery.ajaxSetup({async:false});
           
		$('#tbl_drivers_and_buses tr').each(function () {
       	   var rowrel = $(this).attr('rel');
       	   //alert(rowrel);
           
           
				var dataString='safa_buses_brands_id='+safa_buses_brands_id;
	           $.ajax
	           ({
	           	type: "POST",
	           	url: "<?php echo base_url().'safa_buses_brands/getBrandModels'; ?>",
	           	data: dataString,
	           	cache: false,
	           	success: function(html)
	           	{
	               	//alert(html);
	               	var data_arr = JSON.parse(html);
	                   	
	                $('#safa_buses_models_'+rowrel).html(data_arr[0]['safa_buses_models_options']);
		            $('#safa_buses_models_'+rowrel).trigger("chosen:updated");

		            $('#safa_buses_models_'+rowrel).val(data_arr[0]['safa_buses_models_id_selected'])
			        $('#safa_buses_models_'+rowrel).trigger("chosen:updated");
	           	}
	           });
	           
           

           
		});
           
       }
    
	 });
			
}
</script>


<script>
function add_transporter(erp_transportertype_id)
{
	$.fancybox({
	   'type': 'iframe', 
	   'width' : 500,
	   'height' : 300,
	   'autoDimensions' : false,
	   'autoScale' : false,
	   'href' : "<?php echo site_url('safa_trip_internaltrips/add_transporter_popup'); ?>/"+erp_transportertype_id,

	   afterClose: function() {
           //location.reload();

        jQuery.ajaxSetup({async:false});
        
        var transporter_counter=0;
		$('#div_trip_going_group tr').each(function () {
			
			var rowrel = $(this).attr('inner_rel');

			if(rowrel!=undefined) {
			var old_value = $('#safa_transporter_code\\[' + rowrel + '\\]').val();
           $.get('<?= site_url('safa_trip_internaltrips/get_transporters') ?>/'+erp_transportertype_id, function(data) {
        	   
               $('#safa_transporter_code\\[' + rowrel + '\\]').html(data);
               $('#safa_transporter_code\\[' + rowrel + '\\]').trigger("chosen:updated");              
           });
           
           $('#safa_transporter_code\\[' + rowrel + '\\]').val(old_value)
           $('#safa_transporter_code\\[' + rowrel + '\\]').trigger("chosen:updated");
           
           transporter_counter++;
			}
           
		});


		var transporter_counter=0;
		$('#div_trip_return_group tr').each(function () {
			var rowrel = $(this).attr('inner_rel');
			if(rowrel!=undefined) {
			var old_value = $('#safa_transporter_code\\[' + rowrel + '\\]').val();
			
           $.get('<?= site_url('safa_trip_internaltrips/get_transporters') ?>/'+erp_transportertype_id, function(data) {
               $('#safa_transporter_code\\[' + rowrel + '\\]').html(data);
               $('#safa_transporter_code\\[' + rowrel + '\\]').trigger("chosen:updated");
           });

           $('#safa_transporter_code\\[' + rowrel + '\\]').val(old_value)
           $('#safa_transporter_code\\[' + rowrel + '\\]').trigger("chosen:updated");
           
           transporter_counter++;
			}
		});
           
       }
    
	 });
			
}
</script>

<script>
function add_safa_reservation_forms(safa_reservation_form_id)
{
	if(!$("#frm_trip_internaltrip").validationEngine('validate')){
        return false;
    }
	if(!validate_external_transport()) {
		return false;
	}
	
	var form_inputs = $("#frm_trip_internaltrip").serialize();
	var safa_trip_id = 0;
    $.ajax
    ({
     	type: "POST",
     	url: "<?php echo base_url().'safa_trip_internaltrips/save_for_accommodation_by_ajax'; ?>",
     	data: form_inputs,
     	cache: false,
     	async: false,
     	success: function(html)
     	{
         	//alert(html);
         	var data_arr = JSON.parse(html);
          	$('#hdn_safa_trip_internaltrip_id').val(data_arr[0]['safa_trip_internaltrip_id']);
          	$('#hdn_safa_trip_id').val(data_arr[0]['safa_trip_id']);
          	safa_trip_id = data_arr[0]['safa_trip_id']
     	}
    });

    window.open ("<?php echo site_url('ea/passport_accommodation_rooms/manage_popup'); ?>/"+safa_trip_id+"/"+safa_reservation_form_id,'_self',false);
    
     
//	$.fancybox({
//	   'type': 'iframe', 
//	   'width' : 1400,
//	   'height' : 1000,
//	   'autoDimensions' : false,
//	   'autoScale' : false,
//	   'href' : "<?php echo site_url('ea/passport_accommodation_rooms/manage_popup'); ?>/"+safa_trip_id+"/"+safa_reservation_form_id,
//
//	   afterClose: function() {
//           //location.reload();
//
//       jQuery.ajaxSetup({async:false});
//
//       var h='';
//       if($('#div_hotels tr').length==1) {
//       	   h=$('#div_hotels tr').length-1;
//       } else {
//    	   h=parseInt($('table#div_hotels tr:last').attr('inner_hrel'))+1;
//    	   if(isNaN(h)) {
//    		   h=parseInt($('table#div_hotels tr:last').attr('rel'))+1;
//    		   if(isNaN(h)) {
//        		   h=parseInt($('table#div_hotels tr:last').attr('hrel'))+1;
//        	   }
//    	   }
//    	   //console.log($('table#div_hotels tr:last'));
//       }
//       //alert(h);
//
//       
//       var passport_counter=$('#tbl_trip_traveller tr').length-1;
//       
//       $.ajax
//       ({
//           type: "POST",
//           url: "<?php echo base_url().'safa_trip_internaltrips/get_accommodation_hotels_passports_by_ajax'; ?>",
//           cache: false,
//           success: function(html)
//           {
//               //alert(html);
//               var data_arr = JSON.parse(html);
//
//               var current_safa_reservation_form_id = data_arr[0]['safa_reservation_form_id'];
//               var safa_reservation_form_ids_string = $("#safa_reservation_form_ids").val(); 
//               //console.log(safa_reservation_form_ids_string);
//			   var safa_reservation_form_ids_arr=safa_reservation_form_ids_string.split(',');
//			   //console.log(safa_reservation_form_ids_arr);
//			   if(safa_reservation_form_ids_arr.indexOf(current_safa_reservation_form_id) != -1) {  
//
//
//				 //------------------------------------- Group Passports Only, when update accomodation form screen--------------------------------------------------
//	               	var safa_trip_traveller_ids='';
//					var safa_trip_traveller_ids_arr=new Array();
//					//By Gouda
//					if($("#safa_trip_traveller_ids").val()!=null) {
//						safa_trip_traveller_ids_arr=$("#safa_trip_traveller_ids").val();
//					}
//					
//					var safa_trip_traveller_names='';
//	               
//	               passports_arr =  data_arr[0]['safa_group_passport_accommodation_arr'];
//	               passports_arr = $.map(passports_arr, function (value, key) { return value; });
//	               console.log(passports_arr);
//		    		for (var a=0; a<passports_arr.length ; a++ ) {
//	
//		    		var	passport_data_arr = passports_arr[a];
//		    			
//		    		safa_trip_traveller_ids = safa_trip_traveller_ids + passport_data_arr['safa_group_passport_id'] + ","
//				    safa_trip_traveller_names = safa_trip_traveller_names + passport_data_arr['full_name_ar'] + " \n "
//	
//				    safa_trip_traveller_ids_arr[a]=passport_data_arr['safa_group_passport_id'];
//			    			
//		    			
//		    		
//		    		//console.log(passport_data_arr);
//		    		//passport_data_arr = $.map(passport_data_arr, function (value, key) { return value; });
//		    		//console.log(passport_data_arr);
//		    			
//		    		var safa_group_passport_id	=passport_data_arr['safa_group_passport_id'];
//					var name_ar_value=passport_data_arr['full_name_ar'];
//					var passport_no_value=passport_data_arr['passport_no'];
//					var visa_number_value=passport_data_arr['visa_number'];
//					if(visa_number_value==null) {
//						visa_number_value='';
//					}
//					var erp_nationalities_name_value=passport_data_arr['erp_relations_name'];
//					var erp_gender_name_value=passport_data_arr['erp_gender_name'];
//					var age_value=passport_data_arr['current_age'];
//					if(age_value==undefined) {
//						age_value='';
//					}
//					var occupation_value=passport_data_arr['occupation'];
//					var picture_value="<img src='<?= base_url('static/group_passports'); ?>/"+passport_data_arr['picture']+ "' alt='' width='50px' height='60px'>";
//					
//		               
//	               var new_passport_row = function() {/*
//	                    <tr class="table-row" >
//	                   		
//	          			<td> {$name_ar_value} </td>
//	          			<td> {$passport_no_value} </td>
//	          			<td> {$visa_number_value} </td>
//	          			<td> {$erp_nationalities_name_value} </td>
//	          			<td> {$erp_gender_name_value} </td>
//	          			<td> {$age_value} </td>
//	          			<td> {$occupation_value} </td>
//	          			<td> {$picture_value} </td>
//	          			<td class="TAC" ><a href="javascript:void(0)" class="" id="hrf_remove_traveller" name="{$safa_group_passport_id}"><span class="icon-trash"></span> </a></td>
//	                   </tr>
//	
//	                   */
//	                  }.toString().replace(/{\$id}/g, passport_counter)
//	                  .replace(/{\$safa_group_passport_id}/g, safa_group_passport_id)
//	                  .replace(/{\$name_ar_value}/g, name_ar_value)
//	                  .replace(/{\$passport_no_value}/g, passport_no_value)
//	                  .replace(/{\$visa_number_value}/g, visa_number_value)
//	                  .replace(/{\$erp_nationalities_name_value}/g, erp_nationalities_name_value)
//	                  .replace(/{\$erp_gender_name_value}/g, erp_gender_name_value)
//	                  .replace(/{\$age_value}/g, age_value)
//	                  .replace(/{\$occupation_value}/g, occupation_value)
//	                  .replace(/{\$picture_value}/g, picture_value)
//	                  .slice(14, -3);
//	
//	
//		              $("#tbl_trip_traveller td.dataTables_empty").first().parent().remove();
//
//		              var safa_trip_traveller_ids_selected_values = parent.$('#safa_trip_traveller_ids').val();
//				      var are_safa_trip_traveller_id_selected=false;
//
//				      if(safa_trip_traveller_ids_selected_values!=null) {
//					        for (var j = 0; j < safa_trip_traveller_ids_selected_values.length; j++ ) { 
//				                if(safa_trip_traveller_ids_selected_values[j]==safa_group_passport_id) {
//				                	are_safa_trip_traveller_id_selected=true;
//				                } 
//					        }
//					        if(are_safa_trip_traveller_id_selected==false) {
//					        	$('#tbl_trip_traveller').append(new_passport_row);
//					        }
//				      } else {
//				        	$('#tbl_trip_traveller').append(new_passport_row);
//					  }
//
//		  		        
//	                  
//	                  $('.datetimepicker').datetimepicker({dateFormat: 'yy-mm-dd', timeFormat: 'HH:mm'});
//	                  $('.chosen-select').chosen({width: "100%"});
//	                  passport_counter++;
//	                //----------------------------------------------------------------------------------------------
//		    	}
//	
//		    	$("#safa_trip_traveller_ids").val(safa_trip_traveller_ids_arr);
//		    	$("#safa_trip_traveller_ids").trigger("chosen:updated");
//	
//
//				      
//			   } else {
//	               //------------------------------------- Hotels --------------------------------------------------
//	               hotels_arr =  data_arr[0]['accommodation_hotels_arr'];
//	               hotels_arr = $.map(hotels_arr, function (value, key) { return value; });
//	               //console.log(hotels_arr);
//		    		for (var a=0; a<hotels_arr.length ; a++ ) {
//	
//		    		var	hotel_data_arr = hotels_arr[a];
//		    		//console.log(hotel_data_arr);
//		    		//hotel_data_arr = $.map(hotel_data_arr, function (value, key) { return value; });
//		    		//console.log(hotel_data_arr);
//		    			
//					var erp_city_id=hotel_data_arr['erp_city_id'];
//					var erp_city_name=hotel_data_arr['erp_city_name'];
//					var erp_hotel_id=hotel_data_arr['erp_hotel_id'];
//					var erp_hotel_name=hotel_data_arr['erp_hotel_name'];
//	
//					var arrival_date=hotel_data_arr['from_date'];
//					var exit_date=hotel_data_arr['to_date'];
//	
//					//---------- Get Nights Count ----------------
//					var date1 = new Date(arrival_date);
//					var date2 = new Date(exit_date);
//					var timeDiff = Math.abs(date2.getTime() - date1.getTime());
//					var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
//					var nights=diffDays;
//					//--------------------------------------------
//					
//		               
//	               var new_row = function() {/*
//	                   <tr class="table-row" hrel="{$id}">
//	                   <td>
//	                   <input type='hidden' name='hotel_safa_internalsegment_id[{$id}]'  id='hotel_safa_internalsegment_id[{$id}]'  value='0'/>
//	                   <input type='hidden' name='erp_city_id[{$id}]'  id='erp_city_id[{$id}]'  value='{$erp_city_id}' cityrel="{$id}"/>
//	                   {$erp_city_name}
//	          			</td>
//	          			
//	                   <td>
//	                   <table style='border:0px;'><tr inner_hrel="{$id}"><td style='border:0px;'>
//	          			<input type='hidden' name='erp_hotel_id[{$id}]'  id='erp_hotel_id[{$id}]'  value='{$erp_hotel_id}' class='erp_hotel_id' hotelrel="{$id}" />
//						</td></tr></table>
//	                    {$erp_hotel_name}
//	          			</td>
//	
//	          			<td>
//	          			<input type='hidden' name='arrival_date[{$id}]'  id='arrival_date[{$id}]'  value='{$arrival_date}' startrel="{$id}"/>
//	                    {$arrival_date}
//	          			</td>
//	
//	          			<td>
//	          			<input type='hidden' name='nights[{$id}]'  id='nights[{$id}]'  value='{$nights}' nightsrel="{$id}"/>
//	                    {$nights}
//	          			</td>
//	          					
//	          			<td>
//	          			<input type='hidden' name='exit_date[{$id}]'  id='exit_date[{$id}]'  value='{$exit_date}' endrel="{$id}"/>
//	                    {$exit_date}
//	          			</td>
//	
//	                   
//	                   </tr>
//	
//	                   <tr rel="{$id}">
//	          		   <td colspan="6">
//	                   <table >
//	                   <tr inner_hrel='{$id}'>
//	                   <td>
//	                   <span class="FRight"><?= lang('rooms_count') ?></span>
//	                   </td>
//	          		
//	          		<? if (check_array($erp_hotelroomsizes)) : ?>
//	          		<? foreach ($erp_hotelroomsizes as $erp_hotelroomsize) : ?>
//	          		<td>
//	          		<?= $erp_hotelroomsize->{name()} ?>
//	          		<?=
	          		 form_input('hotels_rooms_count[{$id}][' . $erp_hotelroomsize->erp_hotelroomsize_id . ']'
	          		         , set_value('hotels_rooms_count[{$id}][' . $erp_hotelroomsize->erp_hotelroomsize_id . ']')
	          		         , 'class="input-small FRight" style="width: 50px !important;" pkgid=\' + {$id} + \' rpnrel="' . $erp_hotelroomsize->erp_hotelroomsize_id . '"  id=\"hotels_rooms_count_\' + {$id} + \'_' . $erp_hotelroomsize->erp_hotelroomsize_id . '\" placeholder="0"')
//	          		 ?></td>
//	          		<? endforeach ?>
//	          		<? endif ?>
//	          		</tr>
//	          		</table>
//	           		</td>
//	                   </tr>
//	                   */
//	                  }.toString().replace(/{\$id}/g, h)
//	                  .replace(/{\$erp_city_id}/g, erp_city_id)
//	                  .replace(/{\$erp_city_name}/g, erp_city_name)
//	                  .replace(/{\$erp_hotel_id}/g, erp_hotel_id)
//	                  .replace(/{\$erp_hotel_name}/g, erp_hotel_name)
//	                  .replace(/{\$arrival_date}/g, arrival_date)
//	                  .replace(/{\$nights}/g, nights)
//	                  .replace(/{\$exit_date}/g, exit_date)
//	                  .slice(14, -3);
//	
//	                  $('tbody.internalhotels').append(new_row);
//	                  $('.datetimepicker').datetimepicker({dateFormat: 'yy-mm-dd', timeFormat: 'HH:mm'});
//	                  $('.chosen-select').chosen({width: "100%"});
//	                  h++;
//	                //----------------------------------------------------------------------------------------------
//		    		}
//	
//	
//	                //------------------------------------- Group Passports --------------------------------------------------
//	               	var safa_trip_traveller_ids='';
//					var safa_trip_traveller_ids_arr=new Array();
//					//By Gouda
//					if($("#safa_trip_traveller_ids").val()!=null) {
//						safa_trip_traveller_ids_arr=$("#safa_trip_traveller_ids").val();
//					}
//					
//					var safa_trip_traveller_names='';
//	               
//	               passports_arr =  data_arr[0]['safa_group_passport_accommodation_arr'];
//	               passports_arr = $.map(passports_arr, function (value, key) { return value; });
//	               console.log(passports_arr);
//		    		for (var a=0; a<passports_arr.length ; a++ ) {
//	
//		    		var	passport_data_arr = passports_arr[a];
//		    			
//		    		safa_trip_traveller_ids = safa_trip_traveller_ids + passport_data_arr['safa_group_passport_id'] + ","
//				    safa_trip_traveller_names = safa_trip_traveller_names + passport_data_arr['full_name_ar'] + " \n "
//	
//				    safa_trip_traveller_ids_arr[a]=passport_data_arr['safa_group_passport_id'];
//			    			
//		    			
//		    		
//		    		//console.log(passport_data_arr);
//		    		//passport_data_arr = $.map(passport_data_arr, function (value, key) { return value; });
//		    		//console.log(passport_data_arr);
//		    			
//		    		var safa_group_passport_id	=passport_data_arr['safa_group_passport_id'];
//					var name_ar_value=passport_data_arr['full_name_ar'];
//					var passport_no_value=passport_data_arr['passport_no'];
//					var visa_number_value=passport_data_arr['visa_number'];
//					if(visa_number_value==null) {
//						visa_number_value='';
//					}
//					var erp_nationalities_name_value=passport_data_arr['erp_relations_name'];
//					var erp_gender_name_value=passport_data_arr['erp_gender_name'];
//					var age_value=passport_data_arr['current_age'];
//					if(age_value==undefined) {
//						age_value='';
//					}
//					var occupation_value=passport_data_arr['occupation'];
//					var picture_value="<img src='<?= base_url('static/group_passports'); ?>/"+passport_data_arr['picture']+ "' alt='' width='50px' height='60px'>";
//					
//		               
//	               var new_passport_row = function() {/*
//	                    <tr class="table-row" >
//	                   		
//	          			<td> {$name_ar_value} </td>
//	          			<td> {$passport_no_value} </td>
//	          			<td> {$visa_number_value} </td>
//	          			<td> {$erp_nationalities_name_value} </td>
//	          			<td> {$erp_gender_name_value} </td>
//	          			<td> {$age_value} </td>
//	          			<td> {$occupation_value} </td>
//	          			<td> {$picture_value} </td>
//	          			<td class="TAC" ><a href="javascript:void(0)" class="" id="hrf_remove_traveller" name="{$safa_group_passport_id}"><span class="icon-trash"></span> </a></td>
//	                   </tr>
//	
//	                   */
//	                  }.toString().replace(/{\$id}/g, passport_counter)
//	                  .replace(/{\$safa_group_passport_id}/g, safa_group_passport_id)
//	                  .replace(/{\$name_ar_value}/g, name_ar_value)
//	                  .replace(/{\$passport_no_value}/g, passport_no_value)
//	                  .replace(/{\$visa_number_value}/g, visa_number_value)
//	                  .replace(/{\$erp_nationalities_name_value}/g, erp_nationalities_name_value)
//	                  .replace(/{\$erp_gender_name_value}/g, erp_gender_name_value)
//	                  .replace(/{\$age_value}/g, age_value)
//	                  .replace(/{\$occupation_value}/g, occupation_value)
//	                  .replace(/{\$picture_value}/g, picture_value)
//	                  .slice(14, -3);
//	
//	
//		              $("#tbl_trip_traveller td.dataTables_empty").first().parent().remove();
//		  		        
//	                  $('#tbl_trip_traveller').append(new_passport_row);
//	                  $('.datetimepicker').datetimepicker({dateFormat: 'yy-mm-dd', timeFormat: 'HH:mm'});
//	                  $('.chosen-select').chosen({width: "100%"});
//	                  passport_counter++;
//	                //----------------------------------------------------------------------------------------------
//		    	}
//	
//		    	$("#safa_trip_traveller_ids").val(safa_trip_traveller_ids_arr);
//		    	$("#safa_trip_traveller_ids").trigger("chosen:updated");
//	
//	
//		    	$("#safa_reservation_form_ids").val($("#safa_reservation_form_ids").val()+','+ current_safa_reservation_form_id); 
//
//			   }
//	    	
//	    	}
//
//		
//       });
//	
//           
//       }
//    
//	 });



		
}


function validate_external_transport()
{
	var id=0;
	if($('#erp_transportertype_id').val()==2){
        
        if (!$('tbody#going_flight_tbody tr').length || !$('tbody#going_flight_tbody tr select.erp_path_type option[value=2]:selected').length) {
			if(!$('tbody#going_flight_tbody tr input.erp_path_type[value=2]').length) {
            	alert('<?php echo lang('you_should_add_arrival_trip_first');?>');
            	return false;
			}
        }  
        if (!$('tbody#return_flight_tbody tr').length || !$('tbody#return_flight_tbody tr select.erp_path_type option[value=1]:selected').length) {
        	if(!$('tbody#return_flight_tbody tr input.erp_path_type[value=1]').length) {
            alert('<?php echo lang('you_should_add_return_trip_first');?>');
            return false;
        	}
        } 

        //else {
        	var arrivedate='';
            if (id === 0) {
                var arriverel = $('tbody#going_flight_tbody tr select.erp_path_type option[value=2]:selected').parent().attr('pathrel');
                if($('input[arriverel=' + arriverel + ']').val()!=undefined){
                 	arrivedate = ($('input[arriverel=' + arriverel + ']')).val();
                } else {
                	arrivedate = $('#last_exit_date').val();
                }

                if(arrivedate=='') {
            		alert('<?php echo lang('you_should_add_going_datetime');?>');
                    return false;
            	}
            	
            } else {
            	
                var newrel = id - 1;
                if($('input[endrel=' + newrel + ']').val()!=undefined){
                	arrivedate = $('input[endrel=' + newrel + ']').val();
                }
            }
        //}
        } else if($('#erp_transportertype_id').val()==1){
        	if($('#land_going_datetime').val()=='') {
        		alert('<?php echo lang('you_should_add_going_datetime');?>');
                return false;
        	}
			if($('#land_return_datetime').val()=='') {
				alert('<?php echo lang('you_should_add_return_datetime');?>');
	            return false;
        	}

			var arrivedate='';
			if ($('#div_hotels tr').length === 1) {
                 	arrivedate = $('#land_going_datetime').val()
            } else {
                var newrel = id - 1;
                if($('input[endrel=' + newrel + ']').val()!=undefined){
                	arrivedate = $('input[endrel=' + newrel + ']').val();
                }
            }

        } else if($('#erp_transportertype_id').val()==3){
//        	if($('#sea_going_datetime').val()=='') {
//        		alert('<?php echo lang('you_should_add_going_datetime');?>');
//                return false;
//        	}
//			if($('#sea_return_datetime').val()=='') {
//				alert('<?php echo lang('you_should_add_return_datetime');?>');
//	            return false;
//        	}
//
//			if ($('#div_hotels tr').length === 1) {
//	              	arrivedate = $('#sea_going_datetime').val()
//	         } else {
//	             var newrel = id - 1;
//	             if($('input[endrel=' + newrel + ']').val()!=undefined){
//	             	arrivedate = $('input[endrel=' + newrel + ']').val();
//	             }
//	         }

    		if (!$('tbody#going_cruise_tbody tr').length || !$('tbody#going_cruise_tbody tr select.erp_path_type option[value=2]:selected').length) {
			if(!$('tbody#going_cruise_tbody tr input.erp_path_type[value=2]').length) {
            	alert('<?php echo lang('you_should_add_arrival_trip_first');?>');
            	return false;
			}
        }  
        if (!$('tbody#return_cruise_tbody tr').length || !$('tbody#return_cruise_tbody tr select.erp_path_type option[value=1]:selected').length) {
        	if(!$('tbody#return_cruise_tbody tr input.erp_path_type[value=1]').length) {
            alert('<?php echo lang('you_should_add_return_trip_first');?>');
            return false;
        	}
        } 

        	var arrivedate='';
            if (id === 0) {
                var arriverel = $('tbody#going_cruise_tbody tr select.erp_path_type option[value=2]:selected').parent().attr('pathrel');
                if($('input[arriverel=' + arriverel + ']').val()!=undefined){
                 	arrivedate = ($('input[arriverel=' + arriverel + ']')).val();
                } else {
                	arrivedate = $('#last_exit_date').val();
                }

                if(arrivedate=='') {
            		alert('<?php echo lang('you_should_add_going_datetime');?>');
                    return false;
            	}
            	
            } else {
            	
                var newrel = id - 1;
                if($('input[endrel=' + newrel + ']').val()!=undefined){
                	arrivedate = $('input[endrel=' + newrel + ']').val();
                }
            }
        }
    return true;
}


</script>

<script>

function delete_flights(id)
{
	
		
    $('.cls_tbl_filghts').find('tr[rel=' + id + ']').remove();
    var hidden_input = '<input type="hidden" name="flights_remove[]" value="' + id + '" />';
    $('#div_tbl_filghts').append(hidden_input);
    if(id<100) {
    	c1--;

    	$('#btn_flight_trip_going').show();    		 
    } else {
    	c2--;

//    	if($('#rdo_trip_return_type').val()==1) {
			$('#btn_flight_trip_return').show();
//    	} 
    }
}

function delete_cruises(id)
{
	
		
    $('.cls_tbl_cruises').find('tr[rel=' + id + ']').remove();
    var hidden_input = '<input type="hidden" name="cruises_remove[]" value="' + id + '" />';
    $('#div_sea_trip').append(hidden_input);
    if(id<100) {
    	c1--;

    	$('#btn_cruise_trip_going').show(); 
    } else {
    	$('#btn_cruise_trip_return').show();
    	c2--;
    }
}
</script>

<script>
$('.datetime').datetimepicker({dateFormat: 'yy-mm-dd', timeFormat: 'HH:mm'});
</script>

<script>
function add_internalsegment(id) {

	var select_port_start=''
		var select_port_end=''
					
		if($('#erp_transportertype_id').val()==2){
			select_port_start = "<select name=\"internalpassage_port_start[" + id + "]\" class=\"chosen-select chosen-rtl input-full port start_point  \" id=\"internalpassage_port_start_" + id + "\" tabindex=\"4\">"
	        <? foreach($erp_sa_ports as $key => $value): ?>
	        +"           <option value=\"<?= $key ?>\"><?= $value ?></option>"
	        <? endforeach ?>
	        +"    </select>";

	        select_port_end = "       <select name=\"internalpassage_port_end[" + id + "]\" class=\"chosen-select chosen-rtl input-full port end_point    \"   id=\"internalpassage_port_end_" + id + "\" tabindex=\"4\">"
	        <? foreach($erp_sa_ports as $key => $value): ?>
	        +"           <option value=\"<?= $key ?>\"><?= $value ?></option>"
	        <? endforeach ?>
	        +"    </select>";
	        
	    } else if($('#erp_transportertype_id').val()==1){

	    	select_port_start = "<select name=\"internalpassage_port_start[" + id + "]\" class=\"chosen-select chosen-rtl input-full port start_point  \" id=\"internalpassage_port_start_" + id + "\" tabindex=\"4\">"
	        <? foreach($land_erp_ports_sa as $key => $value): ?>
	        +"           <option value=\"<?= $key ?>\"><?= $value ?></option>"
	        <? endforeach ?>
	        +"    </select>";

	        select_port_end = "       <select name=\"internalpassage_port_end[" + id + "]\" class=\"chosen-select chosen-rtl input-full port end_point  \"   id=\"internalpassage_port_end_" + id + "\" tabindex=\"4\">"
	        <? foreach($land_erp_ports_sa as $key => $value): ?>
	        +"           <option value=\"<?= $key ?>\"><?= $value ?></option>"
	        <? endforeach ?>
	        +"    </select>";
	        
	    } else if($('#erp_transportertype_id').val()==3){

	    	select_port_start = "<select name=\"internalpassage_port_start[" + id + "]\" class=\"chosen-select chosen-rtl input-full port start_point  \" id=\"internalpassage_port_start_" + id + "\" tabindex=\"4\">"
	        <? foreach($sea_erp_ports_sa as $key => $value): ?>
	        +"           <option value=\"<?= $key ?>\"><?= $value ?></option>"
	        <? endforeach ?>
	        +"    </select>";

	        select_port_end = "       <select name=\"internalpassage_port_end[" + id + "]\" class=\"chosen-select chosen-rtl input-full port end_point  \"   id=\"internalpassage_port_end_" + id + "\" tabindex=\"4\">"
	        <? foreach($sea_erp_ports_sa as $key => $value): ?>
	        +"           <option value=\"<?= $key ?>\"><?= $value ?></option>"
	        <? endforeach ?>
	        +"    </select>";
	        
	    }
    
    var new_row = [
            "<tr rel=\"" + id + "\">",

            "    <td>",
            "       <select name=\"internalpassage_type[" + id + "]\" class=\"chosen-select chosen-rtl input-full validate[required] \" id=\"internalpassage_type_" + id + "\" tabindex=\"4\">",
            <? foreach($safa_internalpassagetypes as $key => $value): ?>
            "           <option value=\"<?= $key ?>\"><?= $value ?></option>",
            <? endforeach ?>
            "    </select>",

            "<script>",
            "$(document).ready(function() {",
            '    $("#internalpassage_type_' + id + '").change(function() {',
            "        var val_ = $(this).val();",
            '        show_ports_' + id + '(val_);',
            "    });",

            '    $("#internalpassage_torismplace_end_' + id + '").change(function() {',
            "        var city_id = $('option:selected', this).attr('city_id'); ",
            '        show_torism_places_dates_' + id + '(city_id);',
            "    });",
            
            "});",

            'function show_ports_' + id + '(pass_type) {',
            " //   alert(pass_type);",
			" load_multiselect();",

			'$("#start_point_con_' + id + '").show();',
			'$("#end_point_con_' + id + '").show();',
			
            '$("#start_point_con_' + id + '").children().hide();',
            '$("#end_point_con_' + id + '").children().hide();',
            'if (pass_type == 1) {',
            '    $("#internalpassage_port_start_' + id + '_chosen").show();',
            '    $("#internalpassage_hotel_end_' + id + '_chosen").show();',
            '}',
            'if (pass_type == 2) {',
            '    $("#internalpassage_hotel_start_' + id + '_chosen").show();',
            '    $("#internalpassage_port_end_' + id + '_chosen").show();',
            '    $("#end_point_con_' + id + '").find(".error").show();',
            '}',
            'if (pass_type == 3) {',
            '    $("#internalpassage_hotel_start_' + id + '_chosen").show();',
            '    $("#internalpassage_torismplace_end_' + id + '_chosen").show();',
            '}',
            'if (pass_type == 4) {',
            '    $("#internalpassage_hotel_start_' + id + '_chosen").show();',
            '    $("#internalpassage_hotel_end_' + id + '_chosen").show();',
            '}',



            "$('#internalpassage_startdatatime_" + id+"').datetimepicker('destroy');",
            "$('#internalpassage_enddatatime_" + id+"').datetimepicker('destroy');",
            
            "var selectrel = $('select[cityrel] option[value=1]:selected').parent().attr('cityrel');",
            'if (pass_type == 3 && selectrel != undefined) {',

            "//-------------------------------- By Gouda, To increse the date with 1 day - to solve the problem of 2 hours -----------",
        	"var enddate = $('input[endrel=' + selectrel + ']').val();",
        	"var enddate_arr = enddate.split('-');",
        	"var enddatatime_obj = new Date(enddate_arr[0], enddate_arr[1]-1, enddate_arr[2], '00', '00');",
        	"enddatatime_obj.setDate(enddatatime_obj.getDate() + 1);",
        	"enddatatime = $.datepicker.formatDate('yy-mm-dd', enddatatime_obj)",
        	"//-----------------------------------------------------------------------------------------------------------------------",
        	
            
            "$('#internalpassage_startdatatime_" + id+"').datetimepicker({",
            "    dateFormat: 'yy-mm-dd',",
            "    minDate: new Date($('input[startrel=' + selectrel + ']').val()),",
            "    maxDate: new Date(enddatatime)",
            "});",
            "$('#internalpassage_enddatatime_" + id+"').datetimepicker({",
            "    dateFormat: 'yy-mm-dd',",
            "    minDate: new Date($('input[startrel=' + selectrel + ']').val()),",
            "    maxDate: new Date(enddatatime)",
            "});",
            '} else {',

            "var startdatatime='';",
            "var enddatatime='';",
        	"if($('#erp_transportertype_id').val()==2){",
        	"	startdatatime = ($('input[arriverel=' + 0 + ']')).val();",
        	"	$('#return_flight_tbody tr[rel]').each(function() {",
        	"		var rel = $(this).attr('rel');",
        	"		if(enddatatime < ($('#flight_date\\\\['+rel+'\\\\]')).val()) {",
        	"			enddatatime = ($('#flight_date\\\\['+rel+'\\\\]')).val();",
        	"		}",
        	"	});",
            "} else if($('#erp_transportertype_id').val()==1){",
            "	startdatatime = $('#land_going_datetime').val();",
            "	enddatatime = $('#land_return_datetime').val();",
            "} else if($('#erp_transportertype_id').val()==3){",
            "	startdatatime = ($('input[arriverel=' + 0 + ']')).val();",
        	"	$('#return_cruise_tbody tr[rel]').each(function() {",
        	"		var rel = $(this).attr('rel');",
        	"		if(enddatatime < ($('#cruise_date\\\\['+rel+'\\\\]')).val()) {",
        	"			enddatatime = ($('#cruise_date\\\\['+rel+'\\\\]')).val();",
        	"		}",
        	"	});",
            "}",
        	"var startdatatime_arr = startdatatime.split(' ');",
        	"var enddatatime_arr = enddatatime.split(' ');",


        	"//-------------------------------- By Gouda, To increse the date with 1 day - to solve the problem of 2 hours -----------",
        	"var endtime_arr = (enddatatime_arr[1]).split(':');",
        	"var enddate_arr = (enddatatime_arr[0]).split('-');",
        	"var enddatatime_obj = new Date(enddate_arr[0], enddate_arr[1]-1, enddate_arr[2], endtime_arr[0], endtime_arr[1]);",
        	"enddatatime_obj.setDate(enddatatime_obj.getDate() + 1);",
        	"enddatatime = $.datepicker.formatDate('yy-mm-dd', enddatatime_obj)",
        	"enddatatime_arr[0] = enddatatime;",
        	"//-----------------------------------------------------------------------------------------------------------------------",

        	
            "$('#internalpassage_startdatatime_" + id+"').datetimepicker({",
            "    dateFormat: 'yy-mm-dd',",
            "   minDate: new Date(startdatatime_arr[0]),",
            "    maxDate: new Date(enddatatime_arr[0])",
            "});",
            "$('#internalpassage_enddatatime_" + id+"').datetimepicker({",
            "    dateFormat: 'yy-mm-dd',",
            "    minDate: new Date(startdatatime_arr[0]),",
            "    maxDate: new Date(enddatatime_arr[0])",
            "});",

            "}",
            
            "}",


            
            'function show_torism_places_dates_' + id + '(city_id) {',
            "var selectrel = $('select[cityrel] option[value='+city_id+']:selected').parent().attr('cityrel');",
            'if (selectrel != undefined) {',
            "$('#internalpassage_startdatatime_" + id+"').datetimepicker('destroy');",
            "$('#internalpassage_enddatatime_" + id+"').datetimepicker('destroy');",


            "//-------------------------------- By Gouda, To increse the date with 1 day - to solve the problem of 2 hours -----------",
        	"var enddate = $('input[endrel=' + selectrel + ']').val();",
        	"var enddate_arr = enddate.split('-');",
        	"var enddatatime_obj = new Date(enddate_arr[0], enddate_arr[1]-1, enddate_arr[2], '00', '00');",
        	"enddatatime_obj.setDate(enddatatime_obj.getDate() + 1);",
        	"enddatatime = $.datepicker.formatDate('yy-mm-dd', enddatatime_obj)",
        	"//-----------------------------------------------------------------------------------------------------------------------",
        	
            "$('#internalpassage_startdatatime_" + id+"').datetimepicker({",
            "    dateFormat: 'yy-mm-dd',",
            "    minDate: new Date($('input[startrel=' + selectrel + ']').val()),",
            "    maxDate: new Date(enddatatime)",
            "});",
            "$('#internalpassage_enddatatime_" + id+"').datetimepicker({",
            "    dateFormat: 'yy-mm-dd',",
            "    minDate: new Date($('input[startrel=' + selectrel + ']').val()),",
            "    maxDate: new Date(enddatatime)",
            "});",
            '} else {',

            "var startdatatime='';",
            "var enddatatime='';",
        	"if($('#erp_transportertype_id').val()==2){",
        	"	startdatatime = ($('input[arriverel=' + 0 + ']')).val();",
        	"	$('#return_flight_tbody tr[rel]').each(function() {",
        	"		var rel = $(this).attr('rel');",
        	"		if(enddatatime < ($('#flight_date\\\\['+rel+'\\\\]')).val()) {",
        	"			enddatatime = ($('#flight_date\\\\['+rel+'\\\\]')).val();",
        	"		}",
        	"	});",
            "} else if($('#erp_transportertype_id').val()==1){",
            "	startdatatime = $('#land_going_datetime').val();",
            "	enddatatime = $('#land_return_datetime').val();",
            "} else if($('#erp_transportertype_id').val()==3){",
            "	startdatatime = ($('input[arriverel=' + 0 + ']')).val();",
        	"	$('#return_cruise_tbody tr[rel]').each(function() {",
        	"		var rel = $(this).attr('rel');",
        	"		if(enddatatime < ($('#cruise_date\\\\['+rel+'\\\\]')).val()) {",
        	"			enddatatime = ($('#cruise_date\\\\['+rel+'\\\\]')).val();",
        	"		}",
        	"	});",
            "}",
        	"var startdatatime_arr = startdatatime.split(' ');",
        	"var enddatatime_arr = enddatatime.split(' ');",

        	"//-------------------------------- By Gouda, To increse the date with 1 day - to solve the problem of 2 hours -----------",
        	"var endtime_arr = (enddatatime_arr[1]).split(':');",
        	"var enddate_arr = (enddatatime_arr[0]).split('-');",
        	"var enddatatime_obj = new Date(enddate_arr[0], enddate_arr[1]-1, enddate_arr[2], endtime_arr[0], endtime_arr[1]);",
        	"enddatatime_obj.setDate(enddatatime_obj.getDate() + 1);",
        	"enddatatime = $.datepicker.formatDate('yy-mm-dd', enddatatime_obj)",
        	"enddatatime_arr[0] = enddatatime;",
        	"//-----------------------------------------------------------------------------------------------------------------------",

        	
            "$('#internalpassage_startdatatime_" + id+"').datetimepicker({",
            "    dateFormat: 'yy-mm-dd',",
            "   minDate: new Date(startdatatime_arr[0]),",
            "    maxDate: new Date(enddatatime_arr[0])",
            "});",
            "$('#internalpassage_enddatatime_" + id+"').datetimepicker({",
            "    dateFormat: 'yy-mm-dd',",
            "    minDate: new Date(startdatatime_arr[0]),",
            "    maxDate: new Date(enddatatime_arr[0])",
            "});",

            "}",
            "}",

            

            '$(".start_point").change(function() {',
            '	var id_attr = $(this).attr("id");',
            '	var id_arr = id_attr.split("_");',
            '	id = id_arr[id_arr.length - 1];',
            '	$("#hdn_start_point_"+id).val($(this).val());',
            '});',

            '$(".end_point").change(function () {',
            '	var id_attr = $(this).attr("id");',
            '	var id_arr = id_attr.split("_");',
            '	id = id_arr[id_arr.length - 1];',
            '	//alert($(this).val());',
            '	$("#hdn_end_point_"+id).val($(this).val());',
            '});',
            
        	"<\/script>",
            
            "    </td>",
            
            "    <td>",
            "<input type='hidden' id='hdn_start_point_" + id + "' value='0'></input>",
            
			"<div id='start_point_con_" + id + "' style='display:none' >",
            "       <select name=\"internalpassage_hotel_start[" + id + "]\" class=\"chosen-select chosen-rtl input-full hotel start_point \"   id=\"internalpassage_hotel_start_" + id + "\" tabindex=\"4\">",
            <? foreach($erp_hotels as $key => $value): ?>
            "           <option value=\"<?= $key ?>\"><?= $value ?></option>",
            <? endforeach ?>
            "    </select>",

            "       <select name=\"internalpassage_torismplace_start[" + id + "]\" class=\"chosen-select chosen-rtl input-full place start_point \"   id=\"internalpassage_torismplace_start_" + id + "\" tabindex=\"4\">",
            <? foreach($safa_tourismplaces as $key => $value): ?>
            "           <option value=\"<?= $key ?>\"><?= $value ?></option>",
            <? endforeach ?>
            "    </select>",

            select_port_start,
            "    </div>",
            "    </td>",

            "    <td>",
            "<input type='hidden' id='hdn_end_point_" + id + "' value='0'></input>",
            
			"<div id='end_point_con_" + id + "' style='display:none'>",
            "       <select name=\"internalpassage_hotel_end[" + id + "]\" class=\"chosen-select chosen-rtl input-full hotel end_point   \"   id=\"internalpassage_hotel_end_" + id + "\" tabindex=\"4\">",
            <? foreach($erp_hotels as $key => $value): ?>
            "           <option value=\"<?= $key ?>\"><?= $value ?></option>",
            <? endforeach ?>
            "    </select>",

            "       <select name=\"internalpassage_torismplace_end[" + id + "]\" class=\"chosen-select chosen-rtl input-full place end_point  \"    id=\"internalpassage_torismplace_end_" + id + "\" tabindex=\"4\">",
            <? foreach($safa_tourismplaces as $key => $value){
            	$key_arr = explode('---',$key);
            	$safa_tourismplace_id = $key_arr[0];
            	$erp_city_id = $key_arr[1];
            ?>
            "           <option value=\"<?= $safa_tourismplace_id ?>\" city_id=\"<?= $erp_city_id ?>\" ><?= $value ?></option>",
            <? } ?>
            "    </select>",

            select_port_end,
            "    </div>",
            "    </td>",

            

            '     <td><?php echo  form_input("internalpassage_startdatatime[' + id + ']", FALSE, " class=\"validate[required] datetimepicker \"  id=\"internalpassage_startdatatime_' + id + '\" ") ?>',
            
            "    </td>",



            '     <td><?php echo  form_input("internalpassage_enddatatime[' + id + ']", FALSE, " class=\"validate[required] datetimepicker \"   id=\"internalpassage_enddatatime_' + id + '\" ") ?>',
            
            "    </td>",
            

            '     <!--<td><?php echo  form_input("seatscount[' + id + ']", FALSE, " class=\" validate[custom[integer]] seatscount' + id + '\"  style=\"width:50px\" ") ?>',
            "    </td>-->",

            '     <td><?php echo  form_input("internalpassage_notes[' + id + ']", FALSE, " class=\"internalpassage_notes' + id + '\"   style=\"width:120px\" ") ?>',
            "    </td>",

            
            "    <td class=\"TAC\">",
            "        <a href=\"javascript:void(0)\" onclick=\"repeat_passages('" + id + "')\"><span class=\"icon-copy\"></span></a>",
            
            "        <a href=\"javascript:void(0)\" onclick=\"delete_passages('" + id + "')\"><span class=\"icon-trash\"></span></a>",
            "    </td>",
            "</tr>"
            ].join("\n");
   
    
    $('#tbl_body_internalsegments').append(new_row);


	//------------------------------ Dates Rang --------------------------------------------------------
    var startdatatime='';
    var enddatatime='';

	if($('#erp_transportertype_id').val()==2){
		startdatatime = ($('input[arriverel=' + 0 + ']')).val();
		
		$('#return_flight_tbody tr[rel]').each(function() {
			var rel = $(this).attr('rel');
			//alert(rel);
			//alert(enddatatime+' - '+($('#flight_date\\['+rel+'\\]')).val());
			if(enddatatime < ($('#flight_date\\['+rel+'\\]')).val()) {
				enddatatime = ($('#flight_date\\['+rel+'\\]')).val();
			}
		});
		
    } else if($('#erp_transportertype_id').val()==1){
    	startdatatime = $("#land_going_datetime").val();
    	enddatatime = $("#land_return_datetime").val();
    } else if($('#erp_transportertype_id').val()==3){
//    	startdatatime = $("#sea_going_datetime").val();
//    	enddatatime = $("#sea_return_datetime").val();

		startdatatime = ($('input[arriverel=' + 0 + ']')).val();
		
		$('#return_cruise_tbody tr[rel]').each(function() {
			var rel = $(this).attr('rel');
			//alert(rel);
			//alert(enddatatime+' - '+($('#cruise_date\\['+rel+'\\]')).val());
			if(enddatatime < ($('#cruise_date\\['+rel+'\\]')).val()) {
				enddatatime = ($('#cruise_date\\['+rel+'\\]')).val();
			}
		});
    }
	var startdatatime_arr = startdatatime.split(' ');
	var enddatatime_arr = enddatatime.split(' ');


	//-------------------------------- By Gouda, To increse the date with 1 day - to solve the problem of 2 hours -----------
	var endtime_arr = (enddatatime_arr[1]).split(':');
	var enddate_arr = (enddatatime_arr[0]).split('-');
	var enddatatime_obj = new Date(enddate_arr[0], enddate_arr[1]-1, enddate_arr[2], endtime_arr[0], endtime_arr[1]);
	enddatatime_obj.setDate(enddatatime_obj.getDate() + 1);
	enddatatime = $.datepicker.formatDate('yy-mm-dd', enddatatime_obj)
	enddatatime_arr[0] = enddatatime;
	//-----------------------------------------------------------------------------------------------------------------------
	
	
    //var selectrel = $('select[cityrel] option[value=1]:selected').parent().attr('cityrel');
    $('#internalpassage_startdatatime_' + id ).datetimepicker({
        dateFormat: 'yy-mm-dd',
        minDate: new Date(startdatatime_arr[0]),
        maxDate: new Date(enddatatime_arr[0])
    });
    $('#internalpassage_enddatatime_' + id ).datetimepicker({
        dateFormat: 'yy-mm-dd',
        minDate: new Date(startdatatime_arr[0]),
        maxDate: new Date(enddatatime_arr[0])
    });
  //------------------------------------------------------------------------------------------------------
    
    
	  //----------------- Hotels --------------------------------
	    var cityid = 0;
	    var dataString = "cityid="+ cityid;
	    $.ajax
	    ({
	    type: "POST",
	    url: "<?php echo base_url().'safa_trip_internaltrips/get_hotel_by_city'; ?>",
	    data: dataString,
	    cache: false,
	    success: function(html)
	    {
	        //alert(html);
	        var data_arr = JSON.parse(html);
	            
	        $('#internalpassage_hotel_start_'+id).html(data_arr[0]['hotels_options']);
	        $('#internalpassage_hotel_start_'+id).trigger("chosen:updated");
	
	        $('#internalpassage_hotel_end_'+id).html(data_arr[0]['hotels_options']);
	        $('#internalpassage_hotel_end_'+id).trigger("chosen:updated");
	    }
	    });
	//---------------------------------------------------------

}

function delete_passages(id)
{
	if(window.confirm('<?=lang('global_are_you_sure_you_want_to_delete')?>') ) {
	    $('#tbl_internalsegments').find('tr[rel=' + id + ']').remove();
	    var hidden_input = '<input type="hidden" name="passages_remove[]" value="' + id + '" />';
	    $('#div_tbl_internalsegments').append(hidden_input);
	}
}

$(".start_point").change(function() {
	var id_attr = $(this).attr('id');
	var id_arr = id_attr.split('_');
	id = id_arr[id_arr.length - 1];
	//alert($(this).val());
	$('#hdn_start_point_'+id).val($(this).val());
});

$(".end_point").change(function () {
	var id_attr = $(this).attr('id');
	var id_arr = id_attr.split('_');
	id = id_arr[id_arr.length - 1];
	//alert($(this).val());
	$('#hdn_end_point_'+id).val($(this).val());
});

function repeat_passages(id)
{
	if (typeof repeat_counter == 'undefined') {
		repeat_counter=100;
	}	
	if(window.confirm('<?=lang('are_you_sure_you_want_to_repeat_this_row')?>') ) {
	    //var new_row$('#tbl_internalsegments').find('tr[rel=' + id + ']').html();
	    //$('#tbl_body_internalsegments').append(new_row);

	    var safa_internalpassagetype = $('#internalpassage_type_'+id).val(); 
	    var start_point = $('#hdn_start_point_'+id).val(); 
	    var end_point = $('#hdn_end_point_'+id).val();
	    var startdatatime = $('input[name=internalpassage_startdatatime\\['+id+'\\]]').val(); 
	    var enddatatime = $('input[name=internalpassage_enddatatime\\['+id+'\\]]').val(); 
	    //var seatscount = $('input[name=seatscount\\['+id+'\\]]').val(); 
	    var seatscount = 0;
	    var notes = $('input[name=internalpassage_notes\\['+id+'\\]]').val(); 
	    add_internalsegment_for_generate(repeat_counter, safa_internalpassagetype, start_point, end_point, startdatatime, enddatatime, seatscount, notes);
	    repeat_counter++;
	}
}
</script>


<script>
    function show_ports(pass_type, id) {

        $("#start_point_con"+id).children().hide();
        $("#end_point_con"+id).children().hide();
        if (pass_type == 1) {
            //show the data for arrive//
            $("#start_point_con"+id).find('.port').show();
            $("#end_point_con"+id).find('.hotel').show();

        }
        if (pass_type == 2) {
            // show data for leave//
            $("#start_point_con"+id).find('.hotel').show();
            $("#end_point_con"+id).find('.port').show();
            $("#end_point_con"+id).find('.error').show();
        }
        if (pass_type == 3) {
            // show data for place//
            $("#start_point_con"+id).find('.hotel').show();
            $("#end_point_con"+id).find('.place').show();
        }
        if (pass_type == 4) {
            // show data for transport//
            $("#start_point_con"+id).find('.hotel').show();
            $("#end_point_con"+id).find('.hotel').show();
        }
    }
</script>
<script>
    $(document).ready(function() {
        $("#internalpassage_type").change(function() {
            var val_ = $(this).val();
            show_ports(val_);
        });
    });
</script>
<script>
    $(document).ready(function() {
        var pass_type = $("#internalpassage_type").val();
        show_ports(pass_type);

    });
</script>

<script>
$('.datetimepicker').datetimepicker({dateFormat: 'yy-mm-dd', timeFormat: 'HH:mm'});
</script>

<script>
function validate_saving()
{
	if ($('#div_hotels tr').length === 1) {
		alert('<?php echo lang('you_should_add_one_hotel_at_least');?>');
		return false;
	}

	if ($('#tbl_internalsegments tr').length === 1) {
		alert('<?php echo lang('you_should_generate_internaltrip_or_add_one_segment_at_least');?>');
		return false;
	}

	$( "#serial" ).trigger( "change" );
	if ($( "#serial" ).hasClass( "serial_repeated" )) {
		alert('<?php echo lang('serial_repeated_message');?>');
		return false;
	}
	
	$( "#confirmation_number" ).trigger( "change" );
	if ($( "#confirmation_number" ).hasClass( "confirmation_number_repeated" )) {
		alert('<?php echo lang('confirmation_number_repeated_message');?>');
		return false;
	}
	
	
	var seatscount =  0;
	if(!isNaN(parseInt($('input[name=adult_seats]').val()))) {
		seatscount = seatscount + parseInt($('input[name=adult_seats]').val());
	}
	if(!isNaN(parseInt($('input[name=child_seats]').val()))) {
		seatscount = seatscount + parseInt($('input[name=child_seats]').val());
	}
	if(!isNaN(parseInt($('input[name=baby_seats]').val()))) {
		seatscount = seatscount + parseInt($('input[name=baby_seats]').val());
	}
	if(isNaN(seatscount)) {
		seatscount=0;
	}

	var total_detail_seats_count = 0;
	$(".seats_count").each(function() {
		if(!isNaN(parseInt($(this).val()))) {
			total_detail_seats_count = total_detail_seats_count + parseInt($(this).val());
		}
	});
	 <?php //if($safa_trip_internaltrip_id==0) {?>
	 if ($('#drivers_and_buses tr').length > 0) {
		if(total_detail_seats_count!=seatscount) {
			//alert(total_detail_seats_count+' '+seatscount);
			alert('<?php echo lang('seats_count_should_equal_total_detail_seats_count');?>');
			return false;
		}
	 }
	<?php //}?>

	

	if($('#erp_transportertype_id').val()==2){
		
		var enddatatime='';
		$('#return_flight_tbody tr[rel]').each(function() {
			var rel = $(this).attr('rel');
			//alert(rel);
			//alert(enddatatime+' - '+($('#flight_date\\['+rel+'\\]')).val());
			if(enddatatime < ($('#flight_date\\['+rel+'\\]')).val()) {
				enddatatime = ($('#flight_date\\['+rel+'\\]')).val();
			}
		});
		
    } else if($('#erp_transportertype_id').val()==1){
    	enddatatime = $("#land_return_datetime").val();
    } else if($('#erp_transportertype_id').val()==3){
    	//enddatatime = $("#sea_return_datetime").val();

    	var enddatatime='';
		$('#return_cruise_tbody tr[rel]').each(function() {
			var rel = $(this).attr('rel');
			//alert(rel);
			//alert(enddatatime+' - '+($('#cruise_date\\['+rel+'\\]')).val());
			if(enddatatime < ($('#cruise_date\\['+rel+'\\]')).val()) {
				enddatatime = ($('#cruise_date\\['+rel+'\\]')).val();
			}
		});
		
    }
	enddatatime_arr = enddatatime.split(' ');
	//alert ($('#last_exit_date').val()+' - '+enddatatime_arr[0]) ;
	if($('#last_exit_date').val()!='NaN-aN-aN') {
		if($('#last_exit_date').val() > enddatatime_arr[0]) {
			alert('<?php echo lang('last_exit_date_should_less_than_or_equal_leaving_trip_date');?>');
			return false;
		}
	}
}

function generate_internalsegments()
{
	<?php if($safa_trip_internaltrip_id!=0) {?>
	if($('#tbl_body_internalsegments tr').length>0) {
		if(!confirm('<?php echo lang('if_you_generate_segments_you_will_miss_all_related_data');?>')) {
			return false;
		} else {
			$('#hdn_are_new_segments_generated').val(1);
		}
	}
	<?php }?>


	if(!validate_external_transport()) {
		return false;
	}
	
	if ($('#div_hotels tr').length === 1) {
		alert('<?php echo lang('you_should_add_one_hotel_at_least');?>');
		return false;
	}

	$("#tbl_body_internalsegments tr").remove(); 


	var seatscount =  0;
	if(!isNaN(parseInt($('input[name=adult_seats]').val()))) {
		seatscount = seatscount + parseInt($('input[name=adult_seats]').val());
	}
	if(!isNaN(parseInt($('input[name=child_seats]').val()))) {
		seatscount = seatscount + parseInt($('input[name=child_seats]').val());
	}
	if(!isNaN(parseInt($('input[name=baby_seats]').val()))) {
		seatscount = seatscount + parseInt($('input[name=baby_seats]').val());
	}
	if(isNaN(seatscount)) {
		seatscount=0;
	}
	
	//------------------- Arriving Path ----------------------------------
	last_makka_hotel='';
	last_madina_hotel='';
	
	var startdatatime='';
	var enddatatime='';
	var start_point='';
	var end_point='';

	var rel='';
	if($('#erp_transportertype_id').val()==2){
		rel = $('#going_flight_tbody').children('tr:first').attr('rel');
		startdatatime = ($('input[arriverel=' + rel + ']')).val();
		//alert(startdatatime);
		start_point = ($('#erp_port_id_to\\[' + rel + '\\]')).val();
    } else if($('#erp_transportertype_id').val()==1){
        
    	startdatatime = $("#land_going_datetime").val();
    	start_point = ($('#land_going_erp_port_id')).val();

    	rel = $('.internalhotels').children('tr:first').attr('hrel');
    } else if($('#erp_transportertype_id').val()==3){
    	//startdatatime = $("#sea_going_datetime").val();
    	//start_point = ($('#sea_going_erp_port_id')).val();
		rel = $('#going_cruise_tbody').children('tr:first').attr('rel');
    	startdatatime = ($('input[arriverel=' + rel + ']')).val();
		start_point = ($('#erp_port_id_to\\[' + rel + '\\]')).val();
    }

	var erp_hotel_rel = $('.internalhotels').children('tr:first').attr('hrel');
	end_point = ($('#erp_hotel_id\\[' + erp_hotel_rel + '\\]')).val();
	
	
    if(($('#erp_city_id\\[' + rel + '\\]')).val()==1) {
		last_makka_hotel = ($('#erp_hotel_id\\[' + rel + '\\]')).val();
    } else if(($('#erp_city_id\\[' + rel + '\\]')).val()==2) {
		last_madina_hotel = ($('#erp_hotel_id\\[' + rel + '\\]')).val();
    }
			

	var startdatatime_arr = startdatatime.split(' ');
	var starttime_arr = (startdatatime_arr[1]).split(':');
	var startdate_arr = (startdatatime_arr[0]).split('-');
	//alert(startdate_arr[0]+'==='+ startdate_arr[1]+'==='+ startdate_arr[2]+'==='+ starttime_arr[0]+'==='+ starttime_arr[1]);
	var enddatatime = new Date(startdate_arr[0], startdate_arr[1]-1, startdate_arr[2], starttime_arr[0], starttime_arr[1]);
	//alert(enddatatime);
	var enddatatime_hrs = enddatatime.getHours();
	enddatatime_hrs += 3;
	//alert(enddatatime);
	enddatatime.setHours(enddatatime_hrs); 
	//alert(enddatatime);
	enddatatime_hours = enddatatime.getHours();
	if(enddatatime_hours==0) {
		enddatatime_hours='00';
	}
	enddatatime_minutes = enddatatime.getMinutes();
	if(enddatatime_minutes==0) {
		enddatatime_minutes='00';
	}
	enddatatime = $.datepicker.formatDate('yy-mm-dd', enddatatime)+' '+enddatatime_hours+':'+enddatatime_minutes;
	
	add_internalsegment_for_generate(0, 1, start_point, end_point, startdatatime , enddatatime, seatscount, '');
	//------------------- Arriving Path ----------------------------------

	
	
	//------------------- Transportation Path ----------------------------------
	var transportation_counter=0;
	var start_moving = '<?php echo $this->config->item('start_moving');?>';

	var startdatatime='';
	var enddatatime='';
	var start_point='';
	var end_point='';

	var previous_startdatatime='';
	var previous_end_point='';
	
	var second_rel =0;
	
	$(".erp_hotel_id").each(function() {
		if($(this).attr('id').startsWith('erp_hotel_id')) {

			if(($('#erp_city_id\\['+($(this).closest("tr").attr('inner_hrel'))+'\\]')).val()==1) {
				last_makka_hotel = ($('#erp_hotel_id\\['+($(this).closest("tr").attr('inner_hrel'))+'\\]')).val();
		    } else if(($('#erp_city_id\\['+($(this).closest("tr").attr('inner_hrel'))+'\\]')).val()==2) {
				last_madina_hotel = ($('#erp_hotel_id\\['+($(this).closest("tr").attr('inner_hrel'))+'\\]')).val();
		    }
		    
			if(transportation_counter>0) {

				var start_rel = $(this).closest("tr").attr('inner_hrel');

				startdatatime = ($('#arrival_date\\['+(start_rel)+'\\]')).val()+' '+ start_moving;
				end_point = $(this).val();
				
				if(transportation_counter==1) {
					second_rel = $('.internalhotels tr:nth-child(2)').attr('rel');
				} else {
					if(startdatatime!== previous_startdatatime) {
						second_rel++;
					} else {
						if(previous_end_point == end_point) {
							return true;
						}
					}
				}
				previous_startdatatime = startdatatime;
				previous_end_point = end_point;
				//alert(second_rel);
				
				//console.log(($(this).closest("tr")));
				start_point = ($('#erp_hotel_id\\['+(second_rel)+'\\]')).val();
				

				var startdatatime_arr = startdatatime.split(' ');
				var starttime_arr = (startdatatime_arr[1]).split(':');
				var startdate_arr = (startdatatime_arr[0]).split('-');

				//alert(startdate_arr[0]+'==='+ startdate_arr[1]+'==='+ startdate_arr[2]+'==='+ starttime_arr[0]+'==='+ starttime_arr[1]);
				var enddatatime = new Date(startdate_arr[0], startdate_arr[1]-1, startdate_arr[2], starttime_arr[0], starttime_arr[1]);
				//alert(enddatatime);
				var enddatatime_hrs = enddatatime.getHours();
				enddatatime_hrs += 7;
				//alert(enddatatime);
				enddatatime.setHours(enddatatime_hrs); 
				//alert(enddatatime);
				enddatatime_hours = enddatatime.getHours();
				if(enddatatime_hours==0) {
					enddatatime_hours='00';
				}
				enddatatime_minutes = enddatatime.getMinutes();
				if(enddatatime_minutes==0) {
					enddatatime_minutes='00';
				}
				enddatatime = $.datepicker.formatDate('yy-mm-dd', enddatatime)+' '+enddatatime_hours+':'+enddatatime_minutes;


				add_internalsegment_for_generate(transportation_counter, 4 , start_point, end_point, startdatatime, enddatatime, seatscount, '');

				

					
			}
			transportation_counter++;
		}
	});
	//------------------- Transportation Path ----------------------------------
	
	
	
	//------------------- Tourismplace Path ----------------------------------
	var tourismplace_counter=transportation_counter;
	
	var startdatatime='';
	var enddatatime='';
	var start_point='';
	var end_point='';

	var trel_index=1;
	
	$(".safa_tourismplace_id").each(function() {
		if($(this).attr('id').startsWith('safa_tourismplace_id')) {
			if(tourismplace_counter>0) {

				
				if(tourismplace_counter==transportation_counter) {
					var start_rel = $('.tourismplaces').children('tr:first').attr('trel');
				} else {
					var start_rel = $(".tourismplaces tr:eq("+trel_index+")").attr('trel');
					trel_index++;
				}
				startdatatime = ($('#tourism_date\\['+(start_rel)+'\\]')).val();
				//start_point = ($('#erp_hotel_id\\['+(tourismplace_counter-1)+'\\]')).val();
				//Where end_ponit last hotel, or previous tourismplace.
				//start_point = end_point;
				end_point = $(this).val();

				if(end_point==1) {
					start_point = last_makka_hotel;
				} else if(end_point==4) {
					start_point = last_madina_hotel;
				}
				
				var startdatatime_arr = startdatatime.split(' ');
				var starttime_arr = (startdatatime_arr[1]).split(':');
				var startdate_arr = (startdatatime_arr[0]).split('-');

				//alert(startdate_arr[0]+'==='+ startdate_arr[1]+'==='+ startdate_arr[2]+'==='+ starttime_arr[0]+'==='+ starttime_arr[1]);
				var enddatatime = new Date(startdate_arr[0], startdate_arr[1]-1, startdate_arr[2], starttime_arr[0], starttime_arr[1]);
				//alert(enddatatime);
				var enddatatime_hrs = enddatatime.getHours();
				enddatatime_hrs += 7;
				//alert(enddatatime);
				enddatatime.setHours(enddatatime_hrs); 
				//alert(enddatatime);
				enddatatime_hours = enddatatime.getHours();
				if(enddatatime_hours==0) {
					enddatatime_hours='00';
				}
				enddatatime_minutes = enddatatime.getMinutes();
				if(enddatatime_minutes==0) {
					enddatatime_minutes='00';
				}
				enddatatime = $.datepicker.formatDate('yy-mm-dd', enddatatime)+' '+enddatatime_hours+':'+enddatatime_minutes;


				add_internalsegment_for_generate(tourismplace_counter, 3 , start_point, end_point, startdatatime, enddatatime, seatscount, '');
						
			}
			tourismplace_counter++;
		}
	});
	//------------------- Tourismplace Path ----------------------------------
	
	
	
	
	//------------------- Leaving Path ----------------------------------
	var leaving_counter = tourismplace_counter;
	if($('#erp_transportertype_id').val()==2 || $('#erp_transportertype_id').val()==3){
	$("#div_trip_return_group tr[rel]").each(function() {
	
	var startdatatime='';
	var enddatatime='';
	var start_point='';
	var end_point='';

	var last_inner_hrel = $('.internalhotels').children('tr:last').attr('rel');
	//alert(last_inner_hrel);
		
	var start_return_rel=0;	
	if($('#erp_transportertype_id').val()==2){
		start_return_rel = $(this).attr('rel');
		
		startdatatime = ($('#flight_date\\['+start_return_rel+'\\]')).val();
		start_point = ($('#erp_hotel_id\\['+last_inner_hrel+'\\]')).val();
		end_point = ($('#erp_port_id_from\\['+start_return_rel+'\\]')).val();
    } else if($('#erp_transportertype_id').val()==1){
    	startdatatime = $("#land_return_datetime").val();
    	start_point = ($('#erp_hotel_id\\['+last_inner_hrel+'\\]')).val();
    	end_point = ($('#land_return_erp_port_id')).val();
    } else if($('#erp_transportertype_id').val()==3){
//    	startdatatime = $("#sea_return_datetime").val();
//    	start_point = ($('#erp_hotel_id\\['+last_inner_hrel+'\\]')).val();
//    	end_point = ($('#sea_return_erp_port_id')).val();

		start_return_rel = $(this).attr('rel');
		
    	startdatatime = ($('#cruise_date\\['+start_return_rel+'\\]')).val();
		start_point = ($('#erp_hotel_id\\['+last_inner_hrel+'\\]')).val();
		end_point = ($('#erp_port_id_from\\['+start_return_rel+'\\]')).val();
    }

	var startdatatime_arr = startdatatime.split(' ');
	var starttime_arr = (startdatatime_arr[1]).split(':');
	var startdate_arr = (startdatatime_arr[0]).split('-');


	//Added By Gouda, For Multi Return trips
	//----------------------------------------------------
	$(".end_hotel_datepicker").each(function() {
		if($(this).val()==startdatatime_arr[0]) {
		exit_date_rel = $(this).attr('endrel');
		start_point = ($('#erp_hotel_id\\['+exit_date_rel+'\\]')).val();
		}
		
	});
	//----------------------------------------------------
	
	//alert(startdate_arr[0]+'==='+ startdate_arr[1]+'==='+ startdate_arr[2]+'==='+ starttime_arr[0]+'==='+ starttime_arr[1]);
	var enddatatime = new Date(startdate_arr[0], startdate_arr[1]-1, startdate_arr[2], starttime_arr[0], starttime_arr[1]);
	//alert(enddatatime);
	var enddatatime_hrs = enddatatime.getHours();

	start_city = ($('#erp_city_id\\['+last_inner_hrel+'\\]')).val();
	enddatatime_hrs_value=0;
	if(end_point=='6053') {
		if(start_city==1) {
			enddatatime_hrs_value = 6;
		} else if(start_city==2) {
			enddatatime_hrs_value = 12;
		}
	} else if(end_point=='6056') {
		if(start_city==1) {
			enddatatime_hrs_value = 7;
		} else if(start_city==2) {
			enddatatime_hrs_value = 4;
		}
	} else if(end_point=='6070') {
		if(start_city==1) {
			enddatatime_hrs_value = 6;
		} else if(start_city==2) {
			enddatatime_hrs_value = 6;
		}
	} else {
		enddatatime_hrs_value = 6;
	}

	enddatatime_hrs -=enddatatime_hrs_value;
	
	//alert(enddatatime);
	enddatatime.setHours(enddatatime_hrs); 
	//alert(enddatatime);
	enddatatime_hours = enddatatime.getHours();
	if(enddatatime_hours==0) {
		enddatatime_hours='00';
	}
	enddatatime_minutes = enddatatime.getMinutes();
	if(enddatatime_minutes==0) {
		enddatatime_minutes='00';
	}
	enddatatime = $.datepicker.formatDate('yy-mm-dd', enddatatime)+' '+enddatatime_hours+':'+enddatatime_minutes;
	//alert(enddatatime);
	var startdatatime_2 = new Date(startdate_arr[0], startdate_arr[1]-1, startdate_arr[2], starttime_arr[0], starttime_arr[1]);
	startdatatime_hrs_2 =enddatatime_hrs+(enddatatime_hrs_value-2);
	//alert(enddatatime_hrs_value);
	startdatatime_2.setHours(startdatatime_hrs_2); 
	//alert(startdatatime);
	startdatatime_hours_2 = startdatatime_2.getHours();
	if(startdatatime_hours_2==0) {
		startdatatime_hours_2='00';
	}
	startdatatime_minutes_2 = startdatatime_2.getMinutes();
	if(startdatatime_minutes_2==0) {
		startdatatime_minutes_2='00';
	}
	startdatatime_2 = $.datepicker.formatDate('yy-mm-dd', startdatatime_2)+' '+startdatatime_hours_2+':'+startdatatime_minutes_2;

	
	
	//add_internalsegment_for_generate(1, 2, startdatatime , startdatatime, seatscount);
	add_internalsegment_for_generate(leaving_counter, 2 , start_point, end_point, enddatatime, startdatatime_2, seatscount, '');
	leaving_counter++;
	});
	
	} else {
			
			var startdatatime='';
			var enddatatime='';
			var start_point='';
			var end_point='';

			var last_inner_hrel = $('.internalhotels').children('tr:last').attr('rel');
			//alert(last_inner_hrel);
				
			var start_return_rel=0;	
			startdatatime = $("#land_return_datetime").val();
			
		    start_point = ($('#erp_hotel_id\\['+last_inner_hrel+'\\]')).val();
		    end_point = ($('#land_return_erp_port_id')).val();
		    
			var startdatatime_arr = startdatatime.split(' ');
			var starttime_arr = (startdatatime_arr[1]).split(':');
			var startdate_arr = (startdatatime_arr[0]).split('-');


			//Added By Gouda, For Multi Return trips
			//----------------------------------------------------
			$(".end_hotel_datepicker").each(function() {
				if($(this).val()==startdatatime_arr[0]) {
				exit_date_rel = $(this).attr('endrel');
				start_point = ($('#erp_hotel_id\\['+exit_date_rel+'\\]')).val();
				}
				
			});
			//----------------------------------------------------
			
			//alert(startdate_arr[0]+'==='+ startdate_arr[1]+'==='+ startdate_arr[2]+'==='+ starttime_arr[0]+'==='+ starttime_arr[1]);
			var enddatatime = new Date(startdate_arr[0], startdate_arr[1]-1, startdate_arr[2], starttime_arr[0], starttime_arr[1]);
			//alert(enddatatime);
			var enddatatime_hrs = enddatatime.getHours();

			start_city = ($('#erp_city_id\\['+last_inner_hrel+'\\]')).val();
			enddatatime_hrs_value=0;
			if(end_point=='6053') {
				if(start_city==1) {
					enddatatime_hrs_value = 6;
				} else if(start_city==2) {
					enddatatime_hrs_value = 12;
				}
			} else if(end_point=='6056') {
				if(start_city==1) {
					enddatatime_hrs_value = 7;
				} else if(start_city==2) {
					enddatatime_hrs_value = 4;
				}
			} else if(end_point=='6070') {
				if(start_city==1) {
					enddatatime_hrs_value = 6;
				} else if(start_city==2) {
					enddatatime_hrs_value = 6;
				}
			} else {
				enddatatime_hrs_value = 6;
			}

			enddatatime_hrs -=enddatatime_hrs_value;
			
			//alert(enddatatime);
			enddatatime.setHours(enddatatime_hrs); 
			//alert(enddatatime);
			enddatatime_hours = enddatatime.getHours();
			if(enddatatime_hours==0) {
				enddatatime_hours='00';
			}
			enddatatime_minutes = enddatatime.getMinutes();
			if(enddatatime_minutes==0) {
				enddatatime_minutes='00';
			}
			enddatatime = $.datepicker.formatDate('yy-mm-dd', enddatatime)+' '+enddatatime_hours+':'+enddatatime_minutes;
			//alert(enddatatime);
			var startdatatime_2 = new Date(startdate_arr[0], startdate_arr[1]-1, startdate_arr[2], starttime_arr[0], starttime_arr[1]);
			startdatatime_hrs_2 =enddatatime_hrs+(enddatatime_hrs_value-2);
			//alert(enddatatime_hrs_value);
			startdatatime_2.setHours(startdatatime_hrs_2); 
			//alert(startdatatime);
			startdatatime_hours_2 = startdatatime_2.getHours();
			if(startdatatime_hours_2==0) {
				startdatatime_hours_2='00';
			}
			startdatatime_minutes_2 = startdatatime_2.getMinutes();
			if(startdatatime_minutes_2==0) {
				startdatatime_minutes_2='00';
			}
			startdatatime_2 = $.datepicker.formatDate('yy-mm-dd', startdatatime_2)+' '+startdatatime_hours_2+':'+startdatatime_minutes_2;

			//By Gouda.
			//----------------------------------------------
			enddatatime = ($('#exit_date\\['+(last_inner_hrel)+'\\]')).val()+' '+ start_moving;
			startdatatime_2 = $("#land_return_datetime").val();
			//----------------------------------------------
			
			//add_internalsegment_for_generate(1, 2, startdatatime , startdatatime, seatscount);
			add_internalsegment_for_generate(leaving_counter, 2 , start_point, end_point, enddatatime, startdatatime_2, seatscount, '');
			leaving_counter++;
	}
	
	//------------------- Leaving Path ----------------------------------
	
	

	load_multiselect();
	
	$("#div_internalsegments").css('display','block');

	//-------------------- To Sort Table By Date -------------------------
//	$.fn.dataTableExt.sErrMode = 'throw';
//	$("#tbl_internalsegments").dataTable({
//        bSort: false,
//        bAutoWidth: true,
//        "bProcessing": true,
//		"bServerSide": true,
//		"iDisplayLength": 150,
//		"bLengthChange": false,
//		"aaSorting": [[ 3, "desc" ]]
//     });
	sortInternalSegmentsTable();
    //--------------------------------------------------------------------

}



function sortInternalSegmentsTable()
{
  var rows = $('#tbl_internalsegments tbody  tr').get();

  rows.sort(function(a, b) {

  var A = $(a).children('td').eq(3).find('input.datetimepicker').val().toUpperCase();
  var B = $(b).children('td').eq(3).find('input.datetimepicker').val().toUpperCase();

  //alert(A+'   -   '+B);

  if(A < B) {
    return -1;
  }

  if(A > B) {
    return 1;
  }

  return 0;

  });

  $.each(rows, function(index, row) {
    $('#tbl_internalsegments').children('tbody').append(row);
  });
}


//function sortInternalSegmentsTable()
//{
//  var $sort = this;
//  var $table = $('#tbl_internalsegments');
//  var $rows = $('tbody > tr',$table);
//  $rows.sort(function(a, b){
//      var keyA = $('td',a).text();
//      var keyB = $('td',b).text();
//      if($($sort).hasClass('asc')){
//          return (keyA > keyB) ? 1 : 0;
//      } else {
//          return (keyA > keyB) ? 1 : 0;
//      }
//  });
//  $.each($rows, function(index, row){
//    $table.append(row);
//  });
//  e.preventDefault();
//}





function add_internalsegment_for_generate(id, safa_internalpassagetype, start_point, end_point, startdatatime, enddatatime, seatscount, notes) {

	var select_port_start=''
	var select_port_end=''
				
	if($('#erp_transportertype_id').val()==2){
		select_port_start = "<select name=\"internalpassage_port_start[" + id + "]\" class=\"chosen-select chosen-rtl input-full port start_point  \" id=\"internalpassage_port_start_" + id + "\" tabindex=\"4\">"
        <? foreach($erp_sa_ports as $key => $value): ?>
        +"           <option value=\"<?= $key ?>\"><?= $value ?></option>"
        <? endforeach ?>
        +"    </select>";

        select_port_end = "       <select name=\"internalpassage_port_end[" + id + "]\" class=\"chosen-select chosen-rtl input-full port end_point   \"   id=\"internalpassage_port_end_" + id + "\" tabindex=\"4\">"
        <? foreach($erp_sa_ports as $key => $value): ?>
        +"           <option value=\"<?= $key ?>\"><?= $value ?></option>"
        <? endforeach ?>
        +"    </select>";
        
    } else if($('#erp_transportertype_id').val()==1){

    	select_port_start = "<select name=\"internalpassage_port_start[" + id + "]\" class=\"chosen-select chosen-rtl input-full port start_point   \" id=\"internalpassage_port_start_" + id + "\" tabindex=\"4\">"
        <? foreach($land_erp_ports_sa as $key => $value): ?>
        +"           <option value=\"<?= $key ?>\"><?= $value ?></option>"
        <? endforeach ?>
        +"    </select>";

        select_port_end = "       <select name=\"internalpassage_port_end[" + id + "]\" class=\"chosen-select chosen-rtl input-full port end_point  \"   id=\"internalpassage_port_end_" + id + "\" tabindex=\"4\">"
        <? foreach($land_erp_ports_sa as $key => $value): ?>
        +"           <option value=\"<?= $key ?>\"><?= $value ?></option>"
        <? endforeach ?>
        +"    </select>";
        
    } else if($('#erp_transportertype_id').val()==3){

    	select_port_start = "<select name=\"internalpassage_port_start[" + id + "]\" class=\"chosen-select chosen-rtl input-full port start_point  \" id=\"internalpassage_port_start_" + id + "\" tabindex=\"4\">"
        <? foreach($sea_erp_ports_sa as $key => $value): ?>
        +"           <option value=\"<?= $key ?>\"><?= $value ?></option>"
        <? endforeach ?>
        +"    </select>";

        select_port_end = "       <select name=\"internalpassage_port_end[" + id + "]\" class=\"chosen-select chosen-rtl input-full port end_point   \"   id=\"internalpassage_port_end_" + id + "\" tabindex=\"4\">"
        <? foreach($sea_erp_ports_sa as $key => $value): ?>
        +"           <option value=\"<?= $key ?>\"><?= $value ?></option>"
        <? endforeach ?>
        +"    </select>";
        
    }
	
    var new_row = [
            "<tr rel=\"" + id + "\">",
			
            "    <td>",
            "       <select name=\"internalpassage_type[" + id + "]\" class=\"chosen-select chosen-rtl input-full validate[required] \" id=\"internalpassage_type_" + id + "\" tabindex=\"4\">",
            <? foreach($safa_internalpassagetypes as $key => $value): ?>
            "           <option value=\"<?= $key ?>\"><?= $value ?></option>",
            <? endforeach ?>
            "    </select>",

            "<script>",
            "$(document).ready(function() {",
            '    $("#internalpassage_type_' + id + '").change(function() {',
            "        var val_ = $(this).val();",
            '        show_ports_' + id + '(val_);',
            "    });",

            '    $("#internalpassage_torismplace_end_' + id + '").change(function() {',
            "        var city_id = $('option:selected', this).attr('city_id'); ",
            '        show_torism_places_dates_' + id + '(city_id);',
            "    });",
            
            "});",

            'function show_ports_' + id + '(pass_type) {',
            " //   alert(pass_type);",
			" load_multiselect();",

			'$("#start_point_con_' + id + '").show();',
			'$("#end_point_con_' + id + '").show();',
			
            '$("#start_point_con_' + id + '").children().hide();',
            '$("#end_point_con_' + id + '").children().hide();',
            'if (pass_type == 1) {',
            '    $("#internalpassage_port_start_' + id + '_chosen").show();',
            '    $("#internalpassage_hotel_end_' + id + '_chosen").show();',
            '}',
            'if (pass_type == 2) {',
            '    $("#internalpassage_hotel_start_' + id + '_chosen").show();',
            '    $("#internalpassage_port_end_' + id + '_chosen").show();',
            '    $("#end_point_con_' + id + '").find(".error").show();',
            '}',
            'if (pass_type == 3) {',
            '    $("#internalpassage_hotel_start_' + id + '_chosen").show();',
            '    $("#internalpassage_torismplace_end_' + id + '_chosen").show();',
            '}',
            'if (pass_type == 4) {',
            '    $("#internalpassage_hotel_start_' + id + '_chosen").show();',
            '    $("#internalpassage_hotel_end_' + id + '_chosen").show();',
            '}',


            "$('#internalpassage_startdatatime_" + id+"').datetimepicker('destroy');",
            "$('#internalpassage_enddatatime_" + id+"').datetimepicker('destroy');",

            " var city_id = $('option:selected', $('#internalpassage_torismplace_end_" + id+"')).attr('city_id');",
            "var selectrel = $('select[cityrel] option[value='+city_id+']:selected').parent().attr('cityrel');",
            'if (pass_type == 3 && selectrel != undefined) {',

            "//-------------------------------- By Gouda, To increse the date with 1 day - to solve the problem of 2 hours -----------",
        	"var enddate = $('input[endrel=' + selectrel + ']').val();",
        	"var enddate_arr = enddate.split('-');",
        	"var enddatatime_obj = new Date(enddate_arr[0], enddate_arr[1]-1, enddate_arr[2], '00', '00');",
        	"enddatatime_obj.setDate(enddatatime_obj.getDate() + 1);",
        	"enddatatime = $.datepicker.formatDate('yy-mm-dd', enddatatime_obj)",
        	"//-----------------------------------------------------------------------------------------------------------------------",
        	
            
            "$('#internalpassage_startdatatime_" + id+"').datetimepicker({",
            "    dateFormat: 'yy-mm-dd',",
            "    minDate: new Date($('input[startrel=' + selectrel + ']').val()),",
            "    maxDate: new Date(enddatatime)",
            "});",
            "$('#internalpassage_enddatatime_" + id+"').datetimepicker({",
            "    dateFormat: 'yy-mm-dd',",
            "    minDate: new Date($('input[startrel=' + selectrel + ']').val()),",
            "    maxDate: new Date(enddatatime)",
            "});",
            '} else {',

            "var startdatatime='';",
            "var enddatatime='';",
        	"if($('#erp_transportertype_id').val()==2){",
        	"var start_rel = $('#going_flight_tbody').children('tr:first').attr('rel');",
        	"	startdatatime = ($('input[arriverel=' + start_rel + ']')).val();",
        	"	$('#return_flight_tbody tr[rel]').each(function() {",
        	"		var rel = $(this).attr('rel');",
        	"		if(enddatatime < ($('#flight_date\\\\['+rel+'\\\\]')).val()) {",
        	"			enddatatime = ($('#flight_date\\\\['+rel+'\\\\]')).val();",
        	"		}",
        	"	});",
            "} else if($('#erp_transportertype_id').val()==1){",
            "	startdatatime = $('#land_going_datetime').val();",
            "	enddatatime = $('#land_return_datetime').val();",
            "} else if($('#erp_transportertype_id').val()==3){",
            "var start_rel = $('#going_cruise_tbody').children('tr:first').attr('rel');",
            "	startdatatime = ($('input[arriverel=' + start_rel + ']')).val();",
        	"	$('#return_cruise_tbody tr[rel]').each(function() {",
        	"		var rel = $(this).attr('rel');",
        	"		if(enddatatime < ($('#cruise_date\\\\['+rel+'\\\\]')).val()) {",
        	"			enddatatime = ($('#cruise_date\\\\['+rel+'\\\\]')).val();",
        	"		}",
        	"	});",
            "}",
        	"var startdatatime_arr = startdatatime.split(' ');",
        	"var enddatatime_arr = enddatatime.split(' ');",
        	"//-------------------------------- By Gouda, To increse the date with 1 day - to solve the problem of 2 hours -----------",
        	"var endtime_arr = (enddatatime_arr[1]).split(':');",
        	"var enddate_arr = (enddatatime_arr[0]).split('-');",
        	"var enddatatime_obj = new Date(enddate_arr[0], enddate_arr[1]-1, enddate_arr[2], endtime_arr[0], endtime_arr[1]);",
        	"enddatatime_obj.setDate(enddatatime_obj.getDate() + 1);",
        	"enddatatime = $.datepicker.formatDate('yy-mm-dd', enddatatime_obj)",
        	"enddatatime_arr[0] = enddatatime;",
        	"//-----------------------------------------------------------------------------------------------------------------------",
        	
        	
            "$('#internalpassage_startdatatime_" + id+"').datetimepicker({",
            "    dateFormat: 'yy-mm-dd',",
            "   minDateTime: new Date(startdatatime_arr[0]),",
            "    maxDateTime: new Date(enddatatime_arr[0])",
            "});",
            "$('#internalpassage_enddatatime_" + id+"').datetimepicker({",
            "    dateFormat: 'yy-mm-dd',",
            "    minDateTime: new Date(startdatatime_arr[0]),",
            "    maxDateTime: new Date(enddatatime_arr[0])",
            "});",

            "}",


            
            "}",




            'function show_torism_places_dates_' + id + '(city_id) {',
            "var selectrel = $('select[cityrel] option[value='+city_id+']:selected').parent().attr('cityrel');",
            'if (selectrel != undefined) {',
            "$('#internalpassage_startdatatime_" + id+"').datetimepicker('destroy');",
            "$('#internalpassage_enddatatime_" + id+"').datetimepicker('destroy');",

            "//-------------------------------- By Gouda, To increse the date with 1 day - to solve the problem of 2 hours -----------",
        	"var enddate = $('input[endrel=' + selectrel + ']').val();",
        	"var enddate_arr = enddate.split('-');",
        	"var enddatatime_obj = new Date(enddate_arr[0], enddate_arr[1]-1, enddate_arr[2], '00', '00');",
        	"enddatatime_obj.setDate(enddatatime_obj.getDate() + 1);",
        	"enddatatime = $.datepicker.formatDate('yy-mm-dd', enddatatime_obj)",
        	"//-----------------------------------------------------------------------------------------------------------------------",
        	
            
            "$('#internalpassage_startdatatime_" + id+"').datetimepicker({",
            "    dateFormat: 'yy-mm-dd',",
            "    minDate: new Date($('input[startrel=' + selectrel + ']').val()),",
            "    maxDate: new Date(enddatatime)",
            "});",
            "$('#internalpassage_enddatatime_" + id+"').datetimepicker({",
            "    dateFormat: 'yy-mm-dd',",
            "    minDate: new Date($('input[startrel=' + selectrel + ']').val()),",
            "    maxDate: new Date(enddatatime)",
            "});",
            
            '} else {',

            "var startdatatime='';",
            "var enddatatime='';",
        	"if($('#erp_transportertype_id').val()==2){",
        	"	startdatatime = ($('input[arriverel=' + 0 + ']')).val();",
        	"	$('#return_flight_tbody tr[rel]').each(function() {",
        	"		var rel = $(this).attr('rel');",
        	"		if(enddatatime < ($('#flight_date\\\\['+rel+'\\\\]')).val()) {",
        	"			enddatatime = ($('#flight_date\\\\['+rel+'\\\\]')).val();",
        	"		}",
        	"	});",
            "} else if($('#erp_transportertype_id').val()==1){",
            "	startdatatime = $('#land_going_datetime').val();",
            "	enddatatime = $('#land_return_datetime').val();",
            "} else if($('#erp_transportertype_id').val()==3){",
            "	startdatatime = ($('input[arriverel=' + 0 + ']')).val();",
        	"	$('#return_cruise_tbody tr[rel]').each(function() {",
        	"		var rel = $(this).attr('rel');",
        	"		if(enddatatime < ($('#cruise_date\\\\['+rel+'\\\\]')).val()) {",
        	"			enddatatime = ($('#cruise_date\\\\['+rel+'\\\\]')).val();",
        	"		}",
        	"	});",
            "}",
        	"var startdatatime_arr = startdatatime.split(' ');",
        	"var enddatatime_arr = enddatatime.split(' ');",

        	"//-------------------------------- By Gouda, To increse the date with 1 day - to solve the problem of 2 hours -----------",
        	"var endtime_arr = (enddatatime_arr[1]).split(':');",
        	"var enddate_arr = (enddatatime_arr[0]).split('-');",
        	"var enddatatime_obj = new Date(enddate_arr[0], enddate_arr[1]-1, enddate_arr[2], endtime_arr[0], endtime_arr[1]);",
        	"enddatatime_obj.setDate(enddatatime_obj.getDate() + 1);",
        	"enddatatime = $.datepicker.formatDate('yy-mm-dd', enddatatime_obj)",
        	"enddatatime_arr[0] = enddatatime;",
        	"alert(enddatatime);",
        	"//-----------------------------------------------------------------------------------------------------------------------",

        	
            "$('#internalpassage_startdatatime_" + id+"').datetimepicker({",
            "    dateFormat: 'yy-mm-dd',",
            "   minDate: new Date(startdatatime_arr[0]),",
            "    maxDate: new Date(enddatatime_arr[0])",
            "});",
            "$('#internalpassage_enddatatime_" + id+"').datetimepicker({",
            "    dateFormat: 'yy-mm-dd',",
            "    minDate: new Date(startdatatime_arr[0]),",
            "    maxDate: new Date(enddatatime_arr[0])",
            "});",

            "}",
            "}",
			
            

            '$(".start_point").change(function() {',
            '	var id_attr = $(this).attr("id");',
            '	var id_arr = id_attr.split("_");',
            '	id = id_arr[id_arr.length - 1];',
            '	$("#hdn_start_point_"+id).val($(this).val());',
            '});',

            '$(".end_point").change(function () {',
            '	var id_attr = $(this).attr("id");',
            '	var id_arr = id_attr.split("_");',
            '	id = id_arr[id_arr.length - 1];',
            '	//alert($(this).val());',
            '	$("#hdn_end_point_"+id).val($(this).val());',
            '});',
            
        	"<\/script>",
            
            "    </td>",
            
            "    <td>",
            "<input type='hidden' id='hdn_start_point_" + id + "' value='" + start_point + "'></input>",
            
			"<div id='start_point_con_" + id + "' style='display:none' >",
            "       <select name=\"internalpassage_hotel_start[" + id + "]\" class=\"chosen-select chosen-rtl input-full hotel start_point  \"   id=\"internalpassage_hotel_start_" + id + "\" tabindex=\"4\">",
            <? foreach($erp_hotels as $key => $value): ?>
            "           <option value=\"<?= $key ?>\"><?= $value ?></option>",
            <? endforeach ?>
            "    </select>",

            "       <select name=\"internalpassage_torismplace_start[" + id + "]\" class=\"chosen-select chosen-rtl input-full place start_point  \"   id=\"internalpassage_torismplace_start_" + id + "\" tabindex=\"4\">",
            <? foreach($safa_tourismplaces as $key => $value): ?>
            "           <option value=\"<?= $key ?>\"><?= $value ?></option>",
            <? endforeach ?>
            "    </select>",

            
            select_port_start,
            "    </div>",
            "    </td>",

            "    <td>",
            "<input type='hidden' id='hdn_end_point_" + id + "' value='" + end_point + "'></input>",
            
			"<div id='end_point_con_" + id + "' style='display:none'>",
            "       <select name=\"internalpassage_hotel_end[" + id + "]\" class=\"chosen-select chosen-rtl input-full hotel end_point  \"   id=\"internalpassage_hotel_end_" + id + "\" tabindex=\"4\">",
            <? foreach($erp_hotels as $key => $value): ?>
            "           <option value=\"<?= $key ?>\"><?= $value ?></option>",
            <? endforeach ?>
            "    </select>",

            "       <select name=\"internalpassage_torismplace_end[" + id + "]\" class=\"chosen-select chosen-rtl input-full place end_point \"    id=\"internalpassage_torismplace_end_" + id + "\" tabindex=\"4\">",
            <? foreach($safa_tourismplaces as $key => $value){
            	$key_arr = explode('---',$key);
            	$safa_tourismplace_id = $key_arr[0];
            	$erp_city_id = $key_arr[1];
            ?>
            "           <option value=\"<?= $safa_tourismplace_id ?>\" city_id=\"<?= $erp_city_id ?>\" ><?= $value ?></option>",
            <? } ?>
            "    </select>",
           
            select_port_end,
            "    </div>",
            "    </td>",

            

            '     <td><?php echo  form_input("internalpassage_startdatatime[' + id + ']", set_value("internalpassage_startdatatime[' + id + ']", "'+ startdatatime +'"), " class=\"validate[required] datetimepicker \"  id=\"internalpassage_startdatatime_' + id + '\" ") ?>',
            "    </td>",



            '     <td><?php echo  form_input("internalpassage_enddatatime[' + id + ']", set_value("internalpassage_enddatatime[' + id + ']", "'+ enddatatime +'"), " class=\"validate[required] datetimepicker \"   id=\"internalpassage_enddatatime_' + id + '\" ") ?>',
            
            "    </td>",
            

            '    <!-- <td><?php echo  form_input("seatscount[' + id + ']", set_value("seatscount[' + id + ']", "'+ seatscount +'"), " class=\" validate[custom[integer]] seatscount' + id + '\"  style=\"width:50px\"  ") ?>',
            "    </td> -->",

            '     <td><?php echo  form_input("internalpassage_notes[' + id + ']", set_value("internalpassage_notes[' + id + ']", "'+ notes +'"), " class=\"internalpassage_notes' + id + '\"   style=\"width:120px\" ") ?>',
            "    </td>",

            
            "    <td class=\"TAC\">",
            "        <a href=\"javascript:void(0)\" onclick=\"repeat_passages('" + id + "')\"><span class=\"icon-copy\"></span></a>",
            
            "        <a href=\"javascript:void(0)\" onclick=\"delete_passages('" + id + "')\"><span class=\"icon-trash\"></span></a>",
            "    </td>",
            "</tr>"
            ].join("\n");
   
    
    $('#tbl_body_internalsegments').append(new_row);

    $('#internalpassage_type_' + id).val(safa_internalpassagetype)
    $('#internalpassage_type_' + id).trigger("chosen:updated");

    window['show_ports_'+id](safa_internalpassagetype);

	//----------------- Hotels --------------------------------
        var cityid = 0;
        var dataString = "cityid="+ cityid;
        $.ajax
        ({
        type: "POST",
        url: "<?php echo base_url().'safa_trip_internaltrips/get_hotel_by_city'; ?>",
        data: dataString,
        cache: false,
        async: false,
        success: function(html)
        {
            //alert(html);
            var data_arr = JSON.parse(html);
                
            $('#internalpassage_hotel_start_'+id).html(data_arr[0]['hotels_options']);
            $('#internalpassage_hotel_start_'+id).trigger("chosen:updated");

            $('#internalpassage_hotel_end_'+id).html(data_arr[0]['hotels_options']);
            $('#internalpassage_hotel_end_'+id).trigger("chosen:updated");
        }
        });
	//---------------------------------------------------------
    
    
  	//----------- Start Point --------------------------------
	$('#internalpassage_hotel_start_'+id).val(start_point)
    $('#internalpassage_hotel_start_'+id).trigger("chosen:updated");

	$('#internalpassage_torismplace_start_'+id).val(start_point)
    $('#internalpassage_torismplace_start_'+id).trigger("chosen:updated");

	$('#internalpassage_port_start_'+id).val(start_point)
    $('#internalpassage_port_start_'+id).trigger("chosen:updated");
	//--------------------------------------------------------
    
    //----------- End Point --------------------------------
	$('#internalpassage_hotel_end_'+id).val(end_point)
    $('#internalpassage_hotel_end_'+id).trigger("chosen:updated");

	$('#internalpassage_torismplace_end_'+id).val(end_point)
    $('#internalpassage_torismplace_end_'+id).trigger("chosen:updated");

	$('#internalpassage_port_end_'+id).val(end_point)
    $('#internalpassage_port_end_'+id).trigger("chosen:updated");
	//--------------------------------------------------------

	//------------------------------ Dates Rang --------------------------------------------------------
    var startdatatime='';
    var enddatatime='';

    $('#internalpassage_startdatatime_' + id).datetimepicker('destroy');
    $('#internalpassage_enddatatime_' + id).datetimepicker('destroy');

    var city_id = $('option:selected', $('#internalpassage_torismplace_end_' + id)).attr('city_id');
    var selectrel = $('select[cityrel] option[value='+city_id+']:selected').parent().attr('cityrel');
    if (safa_internalpassagetype==3 && selectrel != undefined) {
    
	    $('#internalpassage_startdatatime_' + id).datetimepicker({
	        dateFormat: 'yy-mm-dd',
	        minDate: new Date($('input[startrel=' + selectrel + ']').val()),
	        maxDate: new Date($('input[endrel=' + selectrel + ']').val())
	    });
	    $('#internalpassage_enddatatime_' + id).datetimepicker({
	        dateFormat: 'yy-mm-dd',
	        minDate: new Date($('input[startrel=' + selectrel + ']').val()),
	        maxDate: new Date($('input[endrel=' + selectrel + ']').val())
	    });
    } else {
    
	if($('#erp_transportertype_id').val()==2){
		var start_rel = $('#going_flight_tbody').children('tr:first').attr('rel');
		startdatatime = ($('input[arriverel=' + start_rel + ']')).val();
		
		$('#return_flight_tbody tr[rel]').each(function() {
			var rel = $(this).attr('rel');
			//alert(rel);
			//alert(enddatatime+' - '+($('#flight_date\\['+rel+'\\]')).val());
			if(enddatatime < ($('#flight_date\\['+rel+'\\]')).val()) {
				enddatatime = ($('#flight_date\\['+rel+'\\]')).val();
			}
		});
		
    } else if($('#erp_transportertype_id').val()==1){
    	startdatatime = $("#land_going_datetime").val();
    	enddatatime = $("#land_return_datetime").val();
    } else if($('#erp_transportertype_id').val()==3){
//    	startdatatime = $("#sea_going_datetime").val();
//    	enddatatime = $("#sea_return_datetime").val();

		var start_rel = $('#going_cruise_tbody').children('tr:first').attr('rel');

		startdatatime = ($('input[arriverel=' + 0 + ']')).val();
		
		$('#return_cruise_tbody tr[rel]').each(function() {
			var rel = $(this).attr('rel');
			//alert(rel);
			//alert(enddatatime+' - '+($('#cruise_date\\['+rel+'\\]')).val());
			if(enddatatime < ($('#cruise_date\\['+rel+'\\]')).val()) {
				enddatatime = ($('#cruise_date\\['+rel+'\\]')).val();
			}
		});
    }


	
	
	var startdatatime_arr = startdatatime.split(' ');
	var enddatatime_arr = enddatatime.split(' ');

	//-------------------------------- By Gouda, To increse the date with 1 day - to solve the problem of 2 hours -----------
	var endtime_arr = (enddatatime_arr[1]).split(':');
	var enddate_arr = (enddatatime_arr[0]).split('-');
	var enddatatime_obj = new Date(enddate_arr[0], enddate_arr[1]-1, enddate_arr[2], endtime_arr[0], endtime_arr[1]);
	enddatatime_obj.setDate(enddatatime_obj.getDate() + 1);
	enddatatime = $.datepicker.formatDate('yy-mm-dd', enddatatime_obj)
	enddatatime_arr[0] = enddatatime;
	//-----------------------------------------------------------------------------------------------------------------------
	
    //var selectrel = $('select[cityrel] option[value=1]:selected').parent().attr('cityrel');
    //alert(enddatatime_arr[0]);
    $('#internalpassage_startdatatime_' + id ).datetimepicker({
        dateFormat: 'yy-mm-dd',
        minDate: new Date(startdatatime_arr[0]),
        maxDate: new Date(enddatatime_arr[0]),
    });
    $('#internalpassage_enddatatime_' + id ).datetimepicker({
        dateFormat: 'yy-mm-dd',
        minDate: new Date(startdatatime_arr[0]),
        maxDate: new Date(enddatatime_arr[0]),
    });
    }
  //------------------------------------------------------------------------------------------------------
    
}
</script>

<script>
$('#safa_internaltrip_type_id').live('change', function() {
	if($(this).val()==1) {
		$("#safa_ito_id" ).addClass( "validate[required]" );
		//$("#safa_transporter_id" ).addClass( "validate[required]" );
		$("#operator_reference" ).addClass( "validate[required]" );
		$("#confirmation_number" ).addClass( "validate[required]" );
		//document.getElementById( 'lnk_add_drivers_and_buses').style.display = 'block';

		document.getElementById('fnt_transport_request_opertator').style.display = 'inline';
		document.getElementById('fnt_confirmation_number').style.display = 'inline';
		document.getElementById('fnt_transport_request_res_code').style.display = 'inline';
		
	} else {
		$("#safa_ito_id" ).removeClass( "validate[required]" );
		//$("#safa_transporter_id" ).removeClass( "validate[required]" );
		$("#operator_reference" ).removeClass( "validate[required]" );
		$("#confirmation_number" ).removeClass( "validate[required]" );
		//document.getElementById('lnk_add_drivers_and_buses').style.display = 'none';

		document.getElementById('fnt_transport_request_opertator').style.display = 'none';
		document.getElementById('fnt_confirmation_number').style.display = 'none';
		document.getElementById('fnt_transport_request_res_code').style.display = 'none';
		
		
		
		$( "#drivers_and_buses tr" ).each( function(){
			  this.parentNode.removeChild( this ); 
		});
	}
});
</script>



<script>
load_multiselect();
function load_multiselect() {
	var config = {
            '.chosen-select': {width: "100%"},
            '.chosen-select-deselect': {allow_single_deselect: true},
            '.chosen-select-no-single': {disable_search_threshold: 10},
            '.chosen-select-no-results': {no_results_text: 'Oops, nothing found!'},
            '.chosen-select-width': {width: "95%"}
        }
        for (var selector in config) {
            $(selector).chosen(config[selector]);
        }
}
function add_drivers_and_buses(id) {

	
    var new_row = [
            "<tr rel=\"" + id + "\">",
           
            " <!--   <td>",
            "       <select name=\"drivers_and_buses_safa_transporters_drivers[" + id + "]\" class=\"chosen-select chosen-rtl input-full  validate[required] drivers_and_buses_safa_transporters_drivers\" id=\"drivers_and_buses_safa_transporters_drivers_" + id + "\" tabindex=\"4\">",
            <? foreach($safa_transporters_drivers as $key => $value): ?>
            "           <option value=\"<?= $key ?>\"><?= $value ?></option>",
            <? endforeach ?>
            "   </select>",
            "</td>",
            "    <td>",

            "<script>",
            '$("#drivers_and_buses_safa_transporters_drivers_' + id + '").change(function(){',
			'check_for_redundency($(this).val(),$(this).attr("id"));',
            'var safa_transporters_drivers_id=$(this).val();',
            'var dataString = "safa_transporters_drivers_id="+ safa_transporters_drivers_id;',
            '$.ajax',
            '({',
            '	type: "POST",',
            '	url: "<?php echo base_url().'uo/all_movements/getDriverData'; ?>",',
            '	data: dataString,',
            '	cache: false,',
            '	success: function(html)',
            '	{',
            '    	//alert(html);',
            '		$("#driver_data_' + id + '").html(html);',
            '	}',
            '});',
            "});",
        	"<\/script>",
            
            "    <div id=\"driver_data_" + id + "\"> </div> ",
            "    </td>",
            "    <td>",
            "       <select name=\"drivers_and_buses_safa_transporters_buses[" + id + "]\" class=\"chosen-select chosen-rtl input-full validate[required] drivers_and_buses_safa_transporters_buses \" id=\"drivers_and_buses_safa_transporters_buses_" + id + "\" tabindex=\"4\">",
            <? foreach($safa_transporters_buses as $key => $value): ?>
            "           <option value=\"<?= $key ?>\"><?= $value ?></option>",
            <? endforeach ?>
            "   </select>",
            "    </td>",

            "    <td>",

            "<script>",
            '$("#drivers_and_buses_safa_transporters_buses_' + id + '").change(function(){',
            'var safa_transporters_buses_id=$(this).val();',
            'var dataString = "safa_transporters_buses_id="+ safa_transporters_buses_id;',
            '$.ajax',
            '({',
            '	type: "POST",',
            '	url: "<?php echo base_url().'uo/all_movements/getBusData'; ?>",',
            '	data: dataString,',
            '	cache: false,',
            '	success: function(html)',
            '	{',
            '    	//alert(html);',
            '		$("#bus_data_' + id + '").html(html);',
            '	}',
            '});',
            "});",
        	"<\/script>",
        	
            "    <div id=\"bus_data_" + id + "\"> </div> ",
            "    </td>-->",

            "<td>",
            "      <table style='border:0px;'><tr rel=\"" + id + "\"><td style='border:0px;'> <select name=\"safa_buses_brands[" + id + "]\" class=\"chosen-select chosen-rtl input-full  validate[required] safa_buses_brands\" id=\"safa_buses_brands_" + id + "\" tabindex=\"4\">",
            <? foreach($safa_buses_brands as $key => $value): ?>
            "           <option value=\"<?= $key ?>\"><?= $value ?></option>",
            <? endforeach ?>
            "   </select>",
            "</td><td style='border:0px;'><a href='javascript:void(0);' onclick='add_bus_brand(\"" + id + "\");'  class='btn Fleft ' style='display:inline-block;'> <span class='icon-plus'></span></a></td></tr></table>",

            "<script>",
            '$("#safa_buses_brands_' + id + '").change(function(){',
            'var safa_buses_brands_id=$(this).val();',
            'var dataString = "safa_buses_brands_id="+ safa_buses_brands_id;',
            '$.ajax',
            '({',
            '	type: "POST",',
            '	url: "<?php echo base_url().'safa_buses_brands/getBrandModels'; ?>",',
            '	data: dataString,',
            '	cache: false,',
            '	success: function(html)',
            '	{',
            '    	//alert(html);',

            '		var data_arr = JSON.parse(html);',
            '		$("#safa_buses_models_' + id + '").html(data_arr[0]["safa_buses_models_options"]);',
            '		$("#safa_buses_models_' + id + '").trigger("chosen:updated");',
            '		$("#safa_buses_models_' + id + '").val(data_arr[0]["safa_buses_models_id_selected"]);',
            '		$("#safa_buses_models_' + id + '").trigger("chosen:updated");',
            '	}',
            '});',
            "});",
        	"<\/script>",
        	
            "</td>",

            "<td>",
            "      <table style='border:0px;'><tr rel=\"" + id + "\"><td style='border:0px;'> <select name=\"safa_buses_models[" + id + "]\" class=\"chosen-select chosen-rtl input-full  validate[required] safa_buses_models\" id=\"safa_buses_models_" + id + "\" tabindex=\"4\">",
            <? foreach($safa_buses_models as $key => $value): ?>
            "           <option value=\"<?= $key ?>\"><?= $value ?></option>",
            <? endforeach ?>
            "   </select>",
            "</td><td style='border:0px;'><a href='javascript:void(0);' onclick='add_bus_model(\"" + id + "\");'  class='btn Fleft ' style='display:inline-block;'> <span class='icon-plus'></span></a></td></tr></table>",
            
            "</td>",
            
            '     <td><?php echo  form_input("bus_count[' + id + ']", set_value("bus_count[' + id + ']", ""), " class=\" validate[custom[integer]] bus_count \"  style=\"width:150px\"  ") ?>',
            "    </td>",

            '     <td><?php echo  form_input("seats_count[' + id + ']", set_value("seats_count[' + id + ']", ""), " class=\" validate[custom[integer]] seats_count \"  style=\"width:150px\"  ") ?>',
            "    </td>",
            
            "    <td class=\"TAC\">",
            "        <a href=\"javascript:void(0)\" onclick=\"delete_drivers_and_buses('" + id + "')\"><span class=\"icon-trash\"></span></a>",
            "    </td>",
            "</tr>"
            ].join("\n");

    
        
    $('#drivers_and_buses').append(new_row);


	//------------------- update drivers and buses for selected transporter -----------------
    var safa_transporter_id=$('#safa_transporter_id').val();
    var dataString = "safa_transporter_id="+ safa_transporter_id;
    $.ajax
    ({
    	type: "POST",
    	url: "<?php echo base_url().'uo/all_movements/getBusesDriversByTransporterIdAjax'; ?>",
    	data: dataString,
    	cache: false,
    	success: function(html)
    	{
        	//alert(html);
        	var data_arr = JSON.parse(html);
            	
        	$(".drivers_and_buses_safa_transporters_drivers").html(data_arr[0]['drivers']);
    		$(".drivers_and_buses_safa_transporters_drivers").trigger("chosen:updated");

    		$(".drivers_and_buses_safa_transporters_buses").html(data_arr[0]['buses']);
    		$(".drivers_and_buses_safa_transporters_buses").trigger("chosen:updated");
    	}
    });
  //-----------------------------------------------------------------------------------------
    
    
}
function delete_drivers_and_buses(id, database) 
{
    if(typeof database == 'undefined')
    {
        $('.drivers_and_buses').find('tr[rel="' + id + '"]').remove();
    }
    else
    {
        $('.drivers_and_buses').find('tr[rel=' + id + ']').remove();
        var hidden_input = '<input type="hidden" name="drivers_and_buses_remove[]" value="' + id + '" />';
        $('#drivers_and_buses').append(hidden_input);
    }
}

function check_for_redundency(this_value, this_id) 
{

	var current_drivers_and_buses_safa_transporters_drivers = this_value;
	var current_select_input_id = this_id;

	$(".drivers_and_buses_safa_transporters_drivers").each(function() {
		if($(this).attr('id')!= current_select_input_id) {
			if(current_drivers_and_buses_safa_transporters_drivers ==$(this).val()) {	
		     	alert('<?= lang('drivers_and_buses_safa_transporters_drivers_was_inserted_before') ?>');
				
		     	$('#'+this_id).closest('tr').remove();
			}
		}
	});
	
}

$("#safa_uo_contract_id, #safa_ito_id ").change(function(){
var safa_uo_contract_id=$('#safa_uo_contract_id').val();
var safa_ito_id=$('#safa_ito_id').val();

var dataString = "safa_uo_contract_id="+ safa_uo_contract_id+"&safa_ito_id="+ safa_ito_id;
$.ajax
({
	type: "POST",
	url: "<?php echo base_url().'safa_trip_internaltrips/get_confirmation_number_by_safa_uo_contract_and_safa_ito_ajax'; ?>",
	data: dataString,
	cache: false,
	success: function(html)
	{
    	//alert(html);
    	var data_arr = JSON.parse(html);
        	
    	$("#confirmation_number").val(data_arr[0]['confirmation_number']);
	}
});
});


$("#confirmation_number").change(function(){
var safa_uo_contract_id=$('#safa_uo_contract_id').val();
var safa_ito_id=$('#safa_ito_id').val();
var confirmation_number=$('#confirmation_number').val();

var safa_trip_internaltrip_id = <?php echo $safa_trip_internaltrip_id; ?>;
if(safa_trip_internaltrip_id==0) {
	safa_trip_internaltrip_id = $('#hdn_safa_trip_internaltrip_id').val();
}

var dataString = "safa_uo_contract_id="+ safa_uo_contract_id+"&safa_ito_id="+ safa_ito_id+"&confirmation_number="+ confirmation_number;
$.ajax
({
	type: "POST",
	url: "<?php echo base_url().'safa_trip_internaltrips/check_confirmation_number_by_safa_uo_contract_and_safa_ito_ajax'; ?>/"+safa_trip_internaltrip_id,
	data: dataString,
	cache: false,
	async: false,
	success: function(html)
	{
    	//alert(html);
    	var data_arr = JSON.parse(html);
    	if(data_arr[0]['are_repeated']==1) {
    		$("#spn_confirmation_number_validate").html("<?php echo lang('confirmation_number_repeated_message');?>");
    		$( "#confirmation_number" ).addClass( "confirmation_number_repeated" );
    	} else {
    		$("#spn_confirmation_number_validate").html('');
    		$( "#confirmation_number" ).removeClass( "confirmation_number_repeated" );
    	}
	}
});
});


$("#serial").change(function(){
	var serial=$('#serial').val();

	var dataString = "serial="+ serial;
	$.ajax
	({
		type: "POST",
		url: "<?php echo base_url().'safa_trip_internaltrips/check_serial_by_safa_uo_contract_ajax/'.$safa_trip_internaltrip_id; ?>",
		data: dataString,
		cache: false,
		async: false,
		success: function(html)
		{
	    	//alert(html);
	    	var data_arr = JSON.parse(html);
	    	if(data_arr[0]['are_repeated']==1) {
	    		$("#spn_serial_validate").html("<?php echo lang('serial_repeated_message');?>");
	    		$( "#serial" ).addClass( "serial_repeated" );
	    	} else {
	    		$("#spn_serial_validate").html('');
	    		$( "#serial" ).removeClass( "serial_repeated" );
	    	}
		}
	});
});


$("#safa_transporter_id").change(function(){
var safa_transporter_id=$(this).val();
var dataString = "safa_transporter_id="+ safa_transporter_id;
$.ajax
({
	type: "POST",
	url: "<?php echo base_url().'uo/all_movements/getBusesDriversByTransporterIdAjax'; ?>",
	data: dataString,
	cache: false,
	success: function(html)
	{
    	//alert(html);
    	var data_arr = JSON.parse(html);
        	
    	$(".drivers_and_buses_safa_transporters_drivers").html(data_arr[0]['drivers']);
		$(".drivers_and_buses_safa_transporters_drivers").trigger("chosen:updated");

		$(".drivers_and_buses_safa_transporters_buses").html(data_arr[0]['buses']);
		$(".drivers_and_buses_safa_transporters_buses").trigger("chosen:updated");
	}
});

if(safa_transporter_id!='') {
	document.getElementById( 'lnk_add_drivers_and_buses' ).style.display = 'block';
} else {
	document.getElementById( 'lnk_add_drivers_and_buses' ).style.display = 'none';
}

});
</script>


<script>
$( "input:not(:button) , input:not(:submit) " ).live("keypress", function(e) {
    /* ENTER PRESSED*/
    if (e.keyCode == 13) {
        /* FOCUS ELEMENT */
        var inps = $(this).parents("form").eq(0).find(":input");
        for (var x = 0; x < inps.length; x++) {
            if (inps[x] == this) {
                while ((inps[x]).name == (inps[x + 1]).name) {
                x++;
                }
                if ((x + 1) < inps.length) { $(inps[x + 1]).focus()};
            }
        }   
        e.preventDefault();
    }
});
</script>


<?php if(session('ito_id')) {?>

<script>
    $(document).ready(function() {
      
    	$('#safa_uo_id').live('change', function() {
            var safa_uo_id = $("#safa_uo_id").val();
            $.get('<?= site_url("safa_trip_internaltrips/get_safa_uo_contracts_by_uo_ajax") ?>/' + safa_uo_id, function(data) {

            	var data_arr = JSON.parse(data);
                
                $('#safa_uo_contract_id').html(data_arr[0]['contracts_options']);
                $('#safa_uo_contract_id').trigger("chosen:updated");
                
            });
        });

    });
</script>
<?php }?>

</div>





<script type="text/javascript">
setInterval(function(){save_internaltrip()},30000);
function save_internaltrip()
{
	var dv_internaltrip_manage_storage = $('#dv_internaltrip_manage_storage').html();
	
    if (typeof (Storage) !== "undefined") {
        localStorage.dv_internaltrip_manage_storage_<?php echo $safa_trip_internaltrip_id;?> = dv_internaltrip_manage_storage;

    } else {
        //alert('<?php echo lang('browser_not_support_storage');?>');
    }
}

	$(document).ready(function() {
	if (typeof (Storage) !== "undefined") {
		
		if(typeof (localStorage.dv_internaltrip_manage_storage_<?php echo $safa_trip_internaltrip_id;?>)  !=='undefined') {

				//if(confirm('<?php echo lang('load_storage');?>')) {
				//	$('#dv_internaltrip_manage_storage').html(localStorage.dv_internaltrip_manage_storage_<?php echo $safa_trip_internaltrip_id;?>);
				//}
			
		}
	}
	
//	$('#load_storage').click(function() {
//	    if (typeof (Storage) !== "undefined") {
//	        $('.darg_drop').html(localStorage.demod);
//	        load_drag_drop();
//	    } else {
//	        alert('<?php echo lang('browser_not_support_storage');?>');
//	    }
//	});

	
});
</script>

