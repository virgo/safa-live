<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    </head>
    <body>
        <font face="Traditional Arabic">
        <table cellpadding="0" cellspacing="5" border="0" align="right" width="630" dir="rtl">
            <tr>
                <td>
                    <table width="100%">
                        <tr>
                            <td width="120"><img src="static/images/alrahma.jpg" /></td>
                            <td width="280" align="center" style="padding-right:80px;">
                                <b>    (( بيان وصول أفواج المعتمرين للديار المقدسة  )) <br />  رقم ( 46 )    </b>
                            </td>
                            <td align="left" width="209px">
                                <table  align="center" border="1" cellpadding="0" cellspacing="3" width="100%"><tr><td>
                                    <table width="100%" height="100%" cellpadding="0"  cellspacing="3">
                                        <tr>
                                            <td width="50px"><font color="#c00000"><b>مدة البرنامج</b></font></td>
                                            <td><font color="#c00000"><b> )) </b></font></td>
                                            <td align="center"><font color="#c00000"><b> 20 ليالى </b></font></td>
                                            <td><font color="#c00000"><b> (( </b></font></td>
                                        </tr>
                                        <tr>
                                            <td><font color="#c00000"><b>رقم البرنامج</b></font></td>
                                            <td><font color="#c00000"><b> )) </b></font></td>
                                            <td align="center"><font color="#c00000"><b> </b></font></td>
                                            <td><font color="#c00000"><b> (( </b></font></td>
                                        </tr>
                                    </table>
                                </td></tr></table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table align="center" border="1" cellpadding="0" cellspacing="3" width="100%">
                        <tr>
                            <td align="center" width="120">
                                <font color="#0000ff"><b>اسم الوكيل الخارجي</b></font>
                            </td>
                            <td align="center" width="125" colspan="2">
                                <font color="#0000ff"><b>ايجيتراف  للـسياحة</b></font>
                            </td>
                            <td align="center" width="150" colspan="2">
                                <font color="#0000ff"><b>تاريخ القدوم للمملكة</b></font>
                            </td>
                            <td align="center" width="195" colspan="2">
                                <font color="#0000ff"><b>30/ 3 / 2014    الموافق   /     /1435هـ</b></font>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <font color="#0000ff"><b>اسم مشرف المجموعة </b></font>
                            </td>
                            <td align="right" colspan="2">
                                <font color="#0000ff"><b> &nbsp; احمد عبد القادر</b></font>
                            </td>
                            <td align="center" >
                                <font color="#0000ff"><b> رقم الجوال </b></font>
                            </td>
                            <td align="center" valign="top">
                                <font color="#0000ff"><b>0566009110</b></font>
                            </td>
                            <td align="center" colspan="2">
                                <font color="#0000ff"><b>&nbsp;</b></font>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <font color="#0000ff"><b>عدد المعتمرين</b></font>
                            </td>
                            <td align="right">
                                <font color="#0000ff"><b> &nbsp; كبير : 1</b></font>
                            </td>
                            <td align="right" colspan="2">
                                <font color="#0000ff"><b> &nbsp; المرافقين:</b></font>
                            </td>
                            <td align="center" colspan="2">
                                <font color="#0000ff"><b>اسم السائق ورقم جواله</b></font>
                            </td>
                            <td align="center">
                                <font color="#0000ff"><b>&nbsp;</b></font>
                            </td>
                        </tr>
                        <tr>
                            <td align="center" colspan="2">
                                <font color="#0000ff"><b>&nbsp;</b></font>
                            </td>
                            <td align="center" colspan="4">
                                <font color="#0000ff"><b>&nbsp;</b></font>
                            </td>
                            <td align="center">
                                <font color="#0000ff"><b>&nbsp;</b></font>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center"><b>(( بيانات الوصول ))</b></td>
            </tr>
            <tr>
                <td>
                    <table width="100%" border="1" cellpadding="0" cellspacing="3">
                        <tr>
                            <td align="center"><b>منفذ القدوم</b></td>
                            <td align="center"><b>الشركة الناقلة</b></td>
                            <td align="center"><b>تاريخ الوصول</b></td>
                            <td align="center"><b>رقم الرحلة</b></td>
                            <td align="center"><b>وقت الوصول</b></td>
                            <td align="center"><b>وسيلة المواصلات الداخلية</b></td>
                        </tr>
                        <tr>
                            <td align="center"><b>جدة</b></td>
                            <td align="center"><b>Ms</b></td>
                            <td align="center"><b>30/3/2014</b></td>
                            <td align="center"><b>655</b></td>
                            <td align="center"><b>1330</b></td>
                            <td align="center"><b>ليموزين</b></td>
                        </tr>
                        <tr>
                            <td align="center"><b>&nbsp;</b></td>
                            <td align="center"><b>&nbsp;</b></td>
                            <td align="center"><b>&nbsp;</b></td>
                            <td align="center"><b>&nbsp;</b></td>
                            <td align="center"><b>&nbsp;</b></td>
                            <td align="center"><b>&nbsp;</b></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center"><b>(( بيانات التسكين ))</b></td>
            </tr>
            <tr>
                <td>
                    <table width="100%" border="1" cellpadding="0" cellspacing="3">
                        <tr height="43px">
                            <td align="center" width="60px"><b>مناطق السكن</b></td>
                            <td align="center" width="90px"><b>اسم الفندق</b></td>
                            <td align="center" width="80px"><b>تاريخ الدخول</b></td>
                            <td align="center" width="80px"><b>تاريخ الخروج</b></td>
                            <td align="center" width="50px"><b>عدد الليالي</b></td>
                            <td align="center" width="35px"><b>ثنائي</b></td>
                            <td align="center" width="35px"><b>ثلاثي</b></td>
                            <td align="center" width="35px"><b>رباعي</b></td>
                            <td align="center" width="35px"><b>خماسي</b></td>
                            <td align="center" width="80px"><b>مجموع  عدد الغرف</b></td>
                        </tr>
                        <tr height="43px">
                            <td align="center" rowspan="2"><b>مكة المكرمة</b></td>
                            <td align="center"><b>هيلتون مكة</b></td>
                            <td align="center"><b>30/03/2014</b></td>
                            <td align="center"><b>10/04/2014</b></td>
                            <td align="center"><b>&nbsp;</b></td>
                            <td align="center"><b>&nbsp;</b></td>
                            <td align="center"><b>&nbsp;</b></td>
                            <td align="center"><b>&nbsp;</b></td>
                            <td align="center"><b>&nbsp;</b></td>
                            <td align="center"><b>&nbsp;</b></td>
                        </tr>
                        <tr>
                            <td align="center"><b>&nbsp;</b></td>
                            <td align="center"><b>&nbsp;</b></td>
                            <td align="center"><b>&nbsp;</b></td>
                            <td align="center"><b>&nbsp;</b></td>
                            <td align="center"><b>&nbsp;</b></td>
                            <td align="center"><b>&nbsp;</b></td>
                            <td align="center"><b>&nbsp;</b></td>
                            <td align="center"><b>&nbsp;</b></td>
                            <td align="center"><b>&nbsp;</b></td>
                        </tr>
                        <tr>
                            <td align="center" rowspan="2"><b>المدينة المنورة</b></td>
                            <td align="center"><b>موفنبيك</b></td>
                            <td align="center"><b>10/04/2014</b></td>
                            <td align="center"><b>20/04/2014</b></td>
                            <td align="center"><b>&nbsp;</b></td>
                            <td align="center"><b>&nbsp;</b></td>
                            <td align="center"><b>&nbsp;</b></td>
                            <td align="center"><b>&nbsp;</b></td>
                            <td align="center"><b>&nbsp;</b></td>
                            <td align="center"><b>&nbsp;</b></td>
                        </tr>
                        <tr>
                            <td align="center"><b>&nbsp;</b></td>
                            <td align="center"><b>&nbsp;</b></td>
                            <td align="center"><b>&nbsp;</b></td>
                            <td align="center"><b>&nbsp;</b></td>
                            <td align="center"><b>&nbsp;</b></td>
                            <td align="center"><b>&nbsp;</b></td>
                            <td align="center"><b>&nbsp;</b></td>
                            <td align="center"><b>&nbsp;</b></td>
                            <td align="center"><b>&nbsp;</b></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center"><b>إيضاحات أخرى</b></td>
            </tr>
            <tr>
                <td border="1" valigin="top"  style="padding:10px;">
                        <font size=4>
                        موعد مزارات مكة المكرمة :  الوقت .....................   التاريخ<br/>
                        موعد مزارات المدينة المنورة :  الوقت .....................   التاريخ
                        </font>
                </td>
            </tr>
            <tr>
                <td align="center"><b>(( بيانات المغادرة النهائية ))</b></td>
            </tr>
            <tr>
                <td>
                    <table border="1" cellpadding="0" cellspacing="3" width="100%">
                        <tr>
                            <td align="center"><b>منفذ المغادرة</b></td>
                            <td align="center"><b>الشركة الناقلة</b></td>
                            <td align="center"><b>تاريخ المغادرة</b></td>
                            <td align="center"><b>رقم الرحلة</b></td>
                            <td align="center"><b>وقت المغادرة</b></td>
                            <td align="center"><b>صالة المغادرة</b></td>
                        </tr>
                        <tr>
                            <td align="center"><b>جدة</b></td>
                            <td align="center"><b>Ms  </b></td>
                            <td align="center"><b>20/4/2014</b></td>
                            <td align="center"><b>656</b></td>
                            <td align="center"><b>1430</b></td>
                            <td align="center"><b>جدة</b></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td width="100%">
                    <table>
                        <tr>
                            <td width="350px"></td>
                            <td align="center"><b>معد البيان </b></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td align="right"><b>
                                    الاسم   :   ممدوح ايمن احمد <br />
                                    التاريخ  : &nbsp;&nbsp;&nbsp;&nbsp; / &nbsp;&nbsp;&nbsp;&nbsp; / 2014م    <br />
                                </b>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="padding-right: 10px;">
                    <table width="100%" cellpadding="0" cellspacing="0" border="1">
                        <tr bgcolor="#f2f2f2">
                            <td align="center" width="205px">
                                <b>مكـة المكرمـة</b>
                            </td>
                            <td align="center" width="205px">
                                <b>المدينة المنـورة</b>
                            </td>
                            <td align="center" width="200px">
                                <b>جـــــدة</b>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <b>0504504141   /   0503505436 025500485    /   025500489  </b>
                            </td>
                            <td align="center">
                                <b>0504359736  /  0564681288  048157020 </b>
                            </td>
                            <td align="center">
                                <b>0569712101 <br /> 026737474 </b>
                            </td>
                        </tr>
                        <tr>
                            <td align="center" colspan="3"><b>فاكـس   5500486  2  966  +   بريـــد الكتــروني:    RMUVS@hotmail.com</b></td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        </font>
    </body>
</html>