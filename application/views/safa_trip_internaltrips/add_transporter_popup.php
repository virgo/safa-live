<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); ?>
<!--<link href="<?= CSS ?>/stylesheets.css" rel="stylesheet" type="text/css" />
<? if (lang('global_lang') == 'ar'): ?>
    <link href="<?= CSS ?>/stylesheet-ar.css" rel="stylesheet" type="text/css" />
<? else: ?><? endif ?>
<script type="text/javascript" src='<?= JS ?>/plugins/jquery/jquery-1.9.1.min.js'></script>
<script type="text/javascript" src='<?= JS ?>/plugins/jquery/jquery-ui-1.10.1.custom.min.js'></script>
<script type="text/javascript" src='<?= JS ?>/plugins/bootstrap/bootstrap.min.js'></script>
<link href="<?= CSS_JS ?>/dateandtime/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src='<?= CSS_JS ?>/dateandtime/bootstrap-datetimepicker.min.js'></script>-->
<style>

body{
height: auto!important;
overflow: auto !important;
background:none transparent !important;
overflow: hidden;
min-height: 3%;
}
.modal-body{
    width: 96%;
    padding:0 2% ;
}
.row-fluid{
padding-top: 5px !important;
padding-bottom: 5px !important;
}
</style>

<style>
.wizerd-div,.row-form{width: 96%;margin:6% 2%;}
label{padding: 0;}
</style>
        
        
<!-- multi selection choosen -->
<link rel="stylesheet" href="<?= NEW_CSS_JS ?>/multi-select-chosen/chosen.css">
<script src="<?= NEW_CSS_JS ?>/multi-select-chosen/chosen.jquery.js" type="text/javascript"></script>

            

<div class="modal-body" id="">
        

<?= form_open('safa_trip_internaltrips/add_transporter_popup/'.$erp_transportertype_id, ' name="frm_add_transporter_popup" id="frm_add_transporter_popup" method="post" ') ?>

      <input type='hidden' name='erp_city_id'  id='erp_city_id'  value=''/>
         
         <div class="row-fluid">
            <div class="span6">
                <div class="span4 TAL Pleft10">
                    <?php echo  lang('safa_transporter_code') ?>
                </div>
                <div class="span8">
                    <?php echo form_input('code', set_value("code", '')," style='' id='code' class='validate[required] input-full' ") ?>
                </div>
			</div>
        </div>
                      
         <div class="row-fluid">
            <div class="span6">
                <div class="span4 TAL Pleft10">
                    <?php echo  lang('name_ar') ?>
                </div>
                <div class="span8">
                    <?php echo form_input('name_ar', set_value("name_ar", '')," style='' id='name_ar' class='validate[required] input-full' ") ?>
                </div>
			</div>
        </div>
        <div class="row-fluid">
            <div class="span6">
                <div class="span4 TAL Pleft10">
                    <?php echo  lang('name_la') ?>
                </div>
                <div class="span8">
                    <?php echo form_input('name_la', set_value("name_la", '')," style='' id='name_la' class='validate[required] input-full' ") ?>
                </div>
			</div>
        </div>
        
        <div class="row-fluid">
            <div class="span6">
                <div class="span4 TAL Pleft10">
                    <?php echo  lang('safa_transporter_erp_country_id') ?>
                </div>
                <div class="span8">
                    <?= form_dropdown('erp_country_id', $erp_countries, " name='s_example' class='select' style='width:100%;' ") ?>
                </div>
			</div>
        </div>
             
             
      <div class="toolbar bottom TAC">
          <input type ="submit" name="smt_save" id="smt_save" value="<?= lang('global_submit') ?>" class="btn btn-primary">
      </div>
        
    <?= form_close() ?>
    
</div>    
