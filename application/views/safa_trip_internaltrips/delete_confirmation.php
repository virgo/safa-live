
    

<div class="widget">
    <div class="path-container Fright">
        <div class="path-name Fright">
        <a <?if(session('admin_login')):?>href='<?=  site_url('admin/dashboard')?>' 
                <?elseif(session('ea_login')):?> href='<?=  site_url('ea/dashboard')?>'
                <?elseif(session('uo_login')):?> href='<?=  site_url('uo/dashboard')?>'
                <?elseif(session('ito_login')):?> href='<?=  site_url('ito/dashboard')?>'
             <?  endif;?> ><?= lang('global_system_management') ?></a>
        </div>
        <div class="path-arrow Fright">
        </div>        
        <div class="path-name Fright">
        <a href="<?= $back_url?>"> <?= $model_title ?></a>   
        </div>
        <div class="path-arrow Fright">
        </div>        
        <div class="path-name Fright">
            <?= $action ?>
        </div>
    </div>
        
	</div>

<?= form_open_multipart($confirmation_url, 'id="delete_confirmation" ') ?>
<div class="widget">

    <div class="">
        <p> 
        
        	<div class="row-form" >
                <div class="span12" >
                    <div class="span4"><?= lang("delete_reason") ?>
                        <font style='color:red'>*</font>   
                    </div>
                    <div class="span8">
                        <?= form_textarea("delete_reason", set_value("delete_reason", '')," class='' id='delete_reason' ") ?>
                        <span class="bottom" style='color:red' >
                            <?= form_error('delete_reason') ?> 
                        </span>
                    </div>
                </div>
                
            </div>
            
            <p><?= $msg ?></p>
            <input  type ="submit" value="<?= lang('global_submit') ?>"class="btn btn-primary" >
            <a href="<?= $back_url?>" class="btn btn-primary" ><?= lang('global_back') ?></a>
            
        </p>
    </div>
    </div>
<?= form_close() ?>    
    
        <style>
            .msg{
                padding:8px 35px 8px 14px;margin-bottom:20px;color:#c09853;text-shadow:0 1px 0 rgba(255,255,255,0.5);background-color:#fcf8e3;border:1px solid #fbeed5;-webkit-border-radius:4px;-moz-border-radius:4px;border-radius:4px;
                color:#468847;background-color:#dff0d8;border-color:#d6e9c6; border: 3px solid rgb(255, 255, 255); text-align: center;
            }  
        </style>
