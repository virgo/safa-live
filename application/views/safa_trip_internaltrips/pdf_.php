<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html dir="rtl">
    <head>
        <title><?php echo lang('safa_trip_internaltrips') ?></title>
        <meta  http-equiv="Content-Type" content="text/html; charset=utf-8" >
        <meta name="viewport" content="width=device-width">

        <style>
            h1 {
                color: #444;
                font-family: arial;
                font-size: 18pt;
                font-weight: bold;

            }
            p.first {
                color: #003300;
                font-family: arial;
                font-size: 12pt;
            }
            p.first span {
                color: #006600;
                font-style: italic;
            }
            p#second {
                color: rgb(00,63,127);
                font-family: times;
                font-size: 12pt;
                text-align: justify;
            }
            p#second > span {
                background-color: #FFFFAA;
            }
            table.first {
                /*
                color: #003300;
                */
                font-family: arial;
                font-weight:normal;
                font-size: 11pt;

                border-right: 1px solid gray;
                /*
                border-bottom: 1px solid green;
                */
                border-top: 1px solid gray;
                background-color: #EEE;

                border-elft: 1px solid gray;
            }


            td {
                /*
                border: 2px solid blue;
                background-color: #ffffee;
                
                */

                height:30px;
            }
            td.second {
                border: 2px dashed green;
            }
            div.test {
                color: #CC0000;
                background-color: #FFFF66;
                font-family: dejavusans;
                font-size: 10pt;
                border-style: solid solid solid solid;
                border-width: 2px 2px 2px 2px;
                border-color: green #FF00FF blue red;
                text-align: center;
            }
        </style>
    </head>

    <div style="width: 100%; text-align: center"  dir="rtl">
        <h1 ><?php echo lang('safa_trip_internaltrips') ?></h1>

        <table align="center" border="0" cellspacing="0" class="first" width="100%">
            <tr><td></td></tr>
            <tr ><td width="100px">
                    <?php echo lang('transport_request_date'); ?>  </td><td width="80px" style="font-size: 13px"> <?php echo $item->datetime; ?> 

                </td><td width="100px"></td><td width="100px">
                    <?php echo lang('transport_request_contract'); ?> </td><td width="80px" style="font-size: 13px"> <?php echo $item->safa_uo_contract_id; ?> 

                </td>
                </tr>
            
            	<tr>
            	<td width="100px">
                    <?php echo lang('trip_code'); ?>  </td><td width="80px" style="font-size: 13px">  <?php echo $item->trip_title; ?> 

                </td><td width="100px"></td><td width="100px">
                     </td><td width="80px" style="font-size: 13px"> 
                </td>
                </tr>
                
            	<tr>
                <td width="100px">
				<?php echo lang('transport_trip_supervisors'); ?> </td><td width="80px"> <?php echo $item->trip_supervisors; ?> 
                    <br/>
                </td>

                <td width="100px"></td>

                <td width="100px">
                    <?php echo lang('transport_trip_supervisors_phone'); ?> </td><td width="80px" style="font-size: 10px"> <?php echo $item->trip_supervisors_phone; ?> 
                </td>
                </tr>
            
            
            
            	<tr>
                <td width="100px">
				<?php echo lang('transport_request_status'); ?> </td><td width="80px"> <?php echo $item->status_name; ?> 
                <br/>
                </td>

                <td width="100px"></td>

                <td width="100px">
                    </td><td width="80px" style="font-size: 10px"> 
                </td>
                </tr>
        
        
        		<tr>
                <td width="100px">
				<?php echo lang('adult_seats'); ?> </td><td width="80px"> <?php echo $item->adult_seats; ?> 
                    <br/>
                </td>

                <td width="100px">
                    <?php echo lang('child_seats'); ?> </td><td width="80px" > <?php echo $item->child_seats; ?> 
                </td>

                <td width="100px">
                    <?php echo lang('baby_seats'); ?> </td><td width="80px" > <?php echo $item->baby_seats; ?> 
                </td>
                </tr>
        
        		<tr>
                <td width="100px">
				<?php echo lang('notes'); ?> </td><td width="80px"> <?php echo $item->notes; ?> 
                <br/>
                </td>

                <td width="100px"></td>

                <td width="100px">
                    </td><td width="80px" style="font-size: 10px"> 
                </td>
                </tr>
        
        		<tr>
                <td width="100px">
				<?php echo lang('erp_transportertype_id'); ?> </td><td width="80px"> <?php echo $item->erp_transportertype_name; ?> 
                <br/>
                </td>

                <td width="100px"></td>

                <td width="100px">
                    </td><td width="80px" style="font-size: 10px"> 
                </td>
                </tr>
                
        </table>
        <br/><br/>

		<?php if ( $item->erp_transportertype_id == 3) { ?>
        
        <div style="overflow: hidden; border: 1px solid #DDD;  border-radius: 5px; padding: 10px; margin: 20px 0;">
            <h3 > <?php echo lang('going') ?> </h3>

            <table cellspacing="0" cellpadding="0" style="width:100%; float:right; direction: rtl; border: 0px; position: relative; padding: 0px; margin: 0px; border: 1px solid #DDD;">
                <thead>

                    <tr>
                        <th  style="background-color: #eee; color: #333; font-weight: bold; border-left: 1px solid #DDDDDD; box-shadow: 1px 1px 0 #FFFFFF inset; font-weight: 300 !important; padding: 6px; font-size: 11px; text-shadow: 1px 1px 0 #FFFFFF; text-align: center;"><?php echo lang('erp_path_type_id') ?></th>
                        <th  style="background-color: #eee; color: #333; font-weight: bold; border-left: 1px solid #DDDDDD; box-shadow: 1px 1px 0 #FFFFFF inset; font-weight: 300 !important; padding: 6px; font-size: 11px; text-shadow: 1px 1px 0 #FFFFFF; text-align: center;"><?php echo lang('cruise_availabilities_lines') ?></th>
                        <th  style="background-color: #eee; color: #333; font-weight: bold; border-left: 1px solid #DDDDDD; box-shadow: 1px 1px 0 #FFFFFF inset; font-weight: 300 !important; padding: 6px; font-size: 11px; text-shadow: 1px 1px 0 #FFFFFF; text-align: center;"><?php echo lang('cruise_availabilities_trip_number') ?></th>
                        <th  style="background-color: #eee; color: #333; font-weight: bold; border-left: 1px solid #DDDDDD; box-shadow: 1px 1px 0 #FFFFFF inset; font-weight: 300 !important; padding: 6px; font-size: 11px; text-shadow: 1px 1px 0 #FFFFFF; text-align: center;"><?php echo lang('cruise_availabilities_date') ?></th>
                        <th  style="background-color: #eee; color: #333; font-weight: bold; border-left: 1px solid #DDDDDD; box-shadow: 1px 1px 0 #FFFFFF inset; font-weight: 300 !important; padding: 6px; font-size: 11px; text-shadow: 1px 1px 0 #FFFFFF; text-align: center;"><?php echo lang('cruise_availabilities_arrival_date') ?></th>
                        <th  style="background-color: #eee; color: #333; font-weight: bold; border-left: 1px solid #DDDDDD; box-shadow: 1px 1px 0 #FFFFFF inset; font-weight: 300 !important; padding: 6px; font-size: 11px; text-shadow: 1px 1px 0 #FFFFFF; text-align: center;"><?php echo lang('cruise_availabilities_ports') ?></th>
                        <th  style="background-color: #eee; color: #333; font-weight: bold; border-left: 1px solid #DDDDDD; box-shadow: 1px 1px 0 #FFFFFF inset; font-weight: 300 !important; padding: 6px; font-size: 11px; text-shadow: 1px 1px 0 #FFFFFF; text-align: center;"><?php echo lang('cruise_availabilities_seat_count') ?></th>
                    </tr>


                </thead>
                <tbody>
				<? if (isset($going_cruise_availabilities_details_rows)) { ?>
			    <? if (check_array($going_cruise_availabilities_details_rows)) { ?>
                 <? foreach ($going_cruise_availabilities_details_rows as $going_cruise_availabilities_details_row) { ?>
                     <tr>

                            <td style="border-left: 1px solid #DDDDDD; border-top: 1px solid #DDDDDD; text-align: center;" > <?php echo $going_cruise_availabilities_details_row->erp_path_type_name; ?>  </td>
                            <td style="border-left: 1px solid #DDDDDD; border-top: 1px solid #DDDDDD; text-align: center;" > <?php echo $going_cruise_availabilities_details_row->safa_transporter_code; ?>  </td>
                            <td style="border-left: 1px solid #DDDDDD; border-top: 1px solid #DDDDDD; text-align: center;" > <?php echo $going_cruise_availabilities_details_row->cruise_number; ?>  </td>
                            <td style="border-left: 1px solid #DDDDDD; border-top: 1px solid #DDDDDD; text-align: center;" > <?php echo $going_cruise_availabilities_details_row->cruise_date . ' - ' . $going_cruise_availabilities_details_row->departure_time; ?>  </td>
                            <td style="border-left: 1px solid #DDDDDD; border-top: 1px solid #DDDDDD; text-align: center;" > <?php echo $going_cruise_availabilities_details_row->arrival_date . ' - ' . $going_cruise_availabilities_details_row->arrival_time; ?>  </td>
                            <td style="border-left: 1px solid #DDDDDD; border-top: 1px solid #DDDDDD; text-align: center;" > <?php echo $going_cruise_availabilities_details_row->start_ports_name .'-'. $going_cruise_availabilities_details_row->end_ports_name; ?>  </td>
                            <td style="border-left: 1px solid #DDDDDD; border-top: 1px solid #DDDDDD; text-align: center;" > <?php echo $going_cruise_availabilities_details_row->seats_count; ?>  </td>
                            
					</tr>
			        <? } ?>
			    <? } ?>
			<? } ?>
                </tbody>
            </table>

        </div>
        
        <div style="overflow: hidden; border: 1px solid #DDD;  border-radius: 5px; padding: 10px; margin: 20px 0;">
            <h3 > <?php echo lang('return') ?> </h3>

            <table cellspacing="0" cellpadding="0" style="width:100%; float:right; direction: rtl; border: 0px; position: relative; padding: 0px; margin: 0px; border: 1px solid #DDD;">
                <thead>

                    <tr>
                        <th  style="background-color: #eee; color: #333; font-weight: bold; border-left: 1px solid #DDDDDD; box-shadow: 1px 1px 0 #FFFFFF inset; font-weight: 300 !important; padding: 6px; font-size: 11px; text-shadow: 1px 1px 0 #FFFFFF; text-align: center;"><?php echo lang('erp_path_type_id') ?></th>
                        <th  style="background-color: #eee; color: #333; font-weight: bold; border-left: 1px solid #DDDDDD; box-shadow: 1px 1px 0 #FFFFFF inset; font-weight: 300 !important; padding: 6px; font-size: 11px; text-shadow: 1px 1px 0 #FFFFFF; text-align: center;"><?php echo lang('cruise_availabilities_lines') ?></th>
                        <th  style="background-color: #eee; color: #333; font-weight: bold; border-left: 1px solid #DDDDDD; box-shadow: 1px 1px 0 #FFFFFF inset; font-weight: 300 !important; padding: 6px; font-size: 11px; text-shadow: 1px 1px 0 #FFFFFF; text-align: center;"><?php echo lang('cruise_availabilities_trip_number') ?></th>
                        <th  style="background-color: #eee; color: #333; font-weight: bold; border-left: 1px solid #DDDDDD; box-shadow: 1px 1px 0 #FFFFFF inset; font-weight: 300 !important; padding: 6px; font-size: 11px; text-shadow: 1px 1px 0 #FFFFFF; text-align: center;"><?php echo lang('cruise_availabilities_date') ?></th>
                        <th  style="background-color: #eee; color: #333; font-weight: bold; border-left: 1px solid #DDDDDD; box-shadow: 1px 1px 0 #FFFFFF inset; font-weight: 300 !important; padding: 6px; font-size: 11px; text-shadow: 1px 1px 0 #FFFFFF; text-align: center;"><?php echo lang('cruise_availabilities_arrival_date') ?></th>
                        <th  style="background-color: #eee; color: #333; font-weight: bold; border-left: 1px solid #DDDDDD; box-shadow: 1px 1px 0 #FFFFFF inset; font-weight: 300 !important; padding: 6px; font-size: 11px; text-shadow: 1px 1px 0 #FFFFFF; text-align: center;"><?php echo lang('cruise_availabilities_ports') ?></th>
                        <th  style="background-color: #eee; color: #333; font-weight: bold; border-left: 1px solid #DDDDDD; box-shadow: 1px 1px 0 #FFFFFF inset; font-weight: 300 !important; padding: 6px; font-size: 11px; text-shadow: 1px 1px 0 #FFFFFF; text-align: center;"><?php echo lang('cruise_availabilities_seat_count') ?></th>
                    </tr>


                </thead>
                <tbody>
				<? if (isset($return_cruise_availabilities_details_rows)) { ?>
			    <? if (check_array($return_cruise_availabilities_details_rows)) { ?>
                 <? foreach ($return_cruise_availabilities_details_rows as $return_cruise_availabilities_details_row) { ?>
                     <tr>

                            <td style="border-left: 1px solid #DDDDDD; border-top: 1px solid #DDDDDD; text-align: center;" > <?php echo $return_cruise_availabilities_details_row->erp_path_type_name; ?>  </td>
                            <td style="border-left: 1px solid #DDDDDD; border-top: 1px solid #DDDDDD; text-align: center;" > <?php echo $return_cruise_availabilities_details_row->safa_transporter_code; ?>  </td>
                            <td style="border-left: 1px solid #DDDDDD; border-top: 1px solid #DDDDDD; text-align: center;" > <?php echo $return_cruise_availabilities_details_row->cruise_number; ?>  </td>
                            <td style="border-left: 1px solid #DDDDDD; border-top: 1px solid #DDDDDD; text-align: center;" > <?php echo $return_cruise_availabilities_details_row->cruise_date . ' - ' . $return_cruise_availabilities_details_row->departure_time; ?>  </td>
                            <td style="border-left: 1px solid #DDDDDD; border-top: 1px solid #DDDDDD; text-align: center;" > <?php echo $return_cruise_availabilities_details_row->arrival_date . ' - ' . $return_cruise_availabilities_details_row->arrival_time; ?>  </td>
                            <td style="border-left: 1px solid #DDDDDD; border-top: 1px solid #DDDDDD; text-align: center;" > <?php echo $return_cruise_availabilities_details_row->start_ports_name .'-'. $return_cruise_availabilities_details_row->end_ports_name; ?>  </td>
                            <td style="border-left: 1px solid #DDDDDD; border-top: 1px solid #DDDDDD; text-align: center;" > <?php echo $return_cruise_availabilities_details_row->seats_count; ?>  </td>
                            
					</tr>
			        <? } ?>
			    <? } ?>
			<? } ?>
                </tbody>
            </table>

        </div>
    
    
    <?php } else if ( $item->erp_transportertype_id == 2) { ?>
        
        <div style="overflow: hidden; border: 1px solid #DDD;  border-radius: 5px; padding: 10px; margin: 20px 0;">
            <h3 > <?php echo lang('going') ?> </h3>

            <table cellspacing="0" cellpadding="0" style="width:100%; float:right; direction: rtl; border: 0px; position: relative; padding: 0px; margin: 0px; border: 1px solid #DDD;">
                <thead>

                    <tr>
                        <th  style="background-color: #eee; color: #333; font-weight: bold; border-left: 1px solid #DDDDDD; box-shadow: 1px 1px 0 #FFFFFF inset; font-weight: 300 !important; padding: 6px; font-size: 11px; text-shadow: 1px 1px 0 #FFFFFF; text-align: center;"><?php echo lang('erp_path_type_id') ?></th>
                        <th  style="background-color: #eee; color: #333; font-weight: bold; border-left: 1px solid #DDDDDD; box-shadow: 1px 1px 0 #FFFFFF inset; font-weight: 300 !important; padding: 6px; font-size: 11px; text-shadow: 1px 1px 0 #FFFFFF; text-align: center;"><?php echo lang('flight_availabilities_airlines') ?></th>
                        <th  style="background-color: #eee; color: #333; font-weight: bold; border-left: 1px solid #DDDDDD; box-shadow: 1px 1px 0 #FFFFFF inset; font-weight: 300 !important; padding: 6px; font-size: 11px; text-shadow: 1px 1px 0 #FFFFFF; text-align: center;"><?php echo lang('flight_availabilities_flight_number') ?></th>
                        <th  style="background-color: #eee; color: #333; font-weight: bold; border-left: 1px solid #DDDDDD; box-shadow: 1px 1px 0 #FFFFFF inset; font-weight: 300 !important; padding: 6px; font-size: 11px; text-shadow: 1px 1px 0 #FFFFFF; text-align: center;"><?php echo lang('flight_availabilities_date') ?></th>
                        <th  style="background-color: #eee; color: #333; font-weight: bold; border-left: 1px solid #DDDDDD; box-shadow: 1px 1px 0 #FFFFFF inset; font-weight: 300 !important; padding: 6px; font-size: 11px; text-shadow: 1px 1px 0 #FFFFFF; text-align: center;"><?php echo lang('arrival_date') ?></th>
                        <th  style="background-color: #eee; color: #333; font-weight: bold; border-left: 1px solid #DDDDDD; box-shadow: 1px 1px 0 #FFFFFF inset; font-weight: 300 !important; padding: 6px; font-size: 11px; text-shadow: 1px 1px 0 #FFFFFF; text-align: center;"><?php echo lang('flight_availabilities_airports') ?></th>
                        <th  style="background-color: #eee; color: #333; font-weight: bold; border-left: 1px solid #DDDDDD; box-shadow: 1px 1px 0 #FFFFFF inset; font-weight: 300 !important; padding: 6px; font-size: 11px; text-shadow: 1px 1px 0 #FFFFFF; text-align: center;"><?php echo lang('flight_availabilities_seat_count') ?></th>
                    </tr>


                </thead>
                <tbody>
				<? if (isset($going_trip_flights_rows)) { ?>
			    <? if (check_array($going_trip_flights_rows)) { ?>
                 <? foreach ($going_trip_flights_rows as $going_trip_flights_row) { ?>
                     <tr>

                            <td style="border-left: 1px solid #DDDDDD; border-top: 1px solid #DDDDDD; text-align: center;" > <?php echo $going_trip_flights_row->erp_path_type_name; ?>  </td>
                            <td style="border-left: 1px solid #DDDDDD; border-top: 1px solid #DDDDDD; text-align: center;" > <?php echo $going_trip_flights_row->safa_transporter_code; ?>  </td>
                            <td style="border-left: 1px solid #DDDDDD; border-top: 1px solid #DDDDDD; text-align: center;" > <?php echo $going_trip_flights_row->flight_number; ?>  </td>
                            <td style="border-left: 1px solid #DDDDDD; border-top: 1px solid #DDDDDD; text-align: center;" > <?php echo $going_trip_flights_row->flight_date . ' - ' . $going_trip_flights_row->departure_time; ?>  </td>
                            <td style="border-left: 1px solid #DDDDDD; border-top: 1px solid #DDDDDD; text-align: center;" > <?php echo $going_trip_flights_row->arrival_date . ' - ' . $going_trip_flights_row->arrival_time; ?>  </td>
                            <td style="border-left: 1px solid #DDDDDD; border-top: 1px solid #DDDDDD; text-align: center;" > <?php echo $going_trip_flights_row->start_ports_name .'-'. $going_trip_flights_row->end_ports_name; ?>  </td>
                            <td style="border-left: 1px solid #DDDDDD; border-top: 1px solid #DDDDDD; text-align: center;" > <?php echo $going_trip_flights_row->available_seats_count; ?>  </td>
                            
					</tr>
			        <? } ?>
			    <? } ?>
			<? } ?>
                </tbody>
            </table>

        </div>
        
        <div style="overflow: hidden; border: 1px solid #DDD;  border-radius: 5px; padding: 10px; margin: 20px 0;">
            <h3 > <?php echo lang('return') ?> </h3>

            <table cellspacing="0" cellpadding="0" style="width:100%; float:right; direction: rtl; border: 0px; position: relative; padding: 0px; margin: 0px; border: 1px solid #DDD;">
                <thead>

                    <tr>
                        <th  style="background-color: #eee; color: #333; font-weight: bold; border-left: 1px solid #DDDDDD; box-shadow: 1px 1px 0 #FFFFFF inset; font-weight: 300 !important; padding: 6px; font-size: 11px; text-shadow: 1px 1px 0 #FFFFFF; text-align: center;"><?php echo lang('erp_path_type_id') ?></th>
                        <th  style="background-color: #eee; color: #333; font-weight: bold; border-left: 1px solid #DDDDDD; box-shadow: 1px 1px 0 #FFFFFF inset; font-weight: 300 !important; padding: 6px; font-size: 11px; text-shadow: 1px 1px 0 #FFFFFF; text-align: center;"><?php echo lang('flight_availabilities_airlines') ?></th>
                        <th  style="background-color: #eee; color: #333; font-weight: bold; border-left: 1px solid #DDDDDD; box-shadow: 1px 1px 0 #FFFFFF inset; font-weight: 300 !important; padding: 6px; font-size: 11px; text-shadow: 1px 1px 0 #FFFFFF; text-align: center;"><?php echo lang('flight_availabilities_flight_number') ?></th>
                        <th  style="background-color: #eee; color: #333; font-weight: bold; border-left: 1px solid #DDDDDD; box-shadow: 1px 1px 0 #FFFFFF inset; font-weight: 300 !important; padding: 6px; font-size: 11px; text-shadow: 1px 1px 0 #FFFFFF; text-align: center;"><?php echo lang('flight_availabilities_date') ?></th>
                        <th  style="background-color: #eee; color: #333; font-weight: bold; border-left: 1px solid #DDDDDD; box-shadow: 1px 1px 0 #FFFFFF inset; font-weight: 300 !important; padding: 6px; font-size: 11px; text-shadow: 1px 1px 0 #FFFFFF; text-align: center;"><?php echo lang('arrival_date') ?></th>
                        <th  style="background-color: #eee; color: #333; font-weight: bold; border-left: 1px solid #DDDDDD; box-shadow: 1px 1px 0 #FFFFFF inset; font-weight: 300 !important; padding: 6px; font-size: 11px; text-shadow: 1px 1px 0 #FFFFFF; text-align: center;"><?php echo lang('flight_availabilities_airports') ?></th>
                        <th  style="background-color: #eee; color: #333; font-weight: bold; border-left: 1px solid #DDDDDD; box-shadow: 1px 1px 0 #FFFFFF inset; font-weight: 300 !important; padding: 6px; font-size: 11px; text-shadow: 1px 1px 0 #FFFFFF; text-align: center;"><?php echo lang('flight_availabilities_seat_count') ?></th>
                    </tr>


                </thead>
                <tbody>
				<? if (isset($return_trip_flights_rows)) { ?>
			    <? if (check_array($return_trip_flights_rows)) { ?>
                 <? foreach ($return_trip_flights_rows as $return_trip_flights_row) { ?>
                     <tr>

                            <td style="border-left: 1px solid #DDDDDD; border-top: 1px solid #DDDDDD; text-align: center;" > <?php echo $return_trip_flights_row->erp_path_type_name; ?>  </td>
                            <td style="border-left: 1px solid #DDDDDD; border-top: 1px solid #DDDDDD; text-align: center;" > <?php echo $return_trip_flights_row->safa_transporter_code; ?>  </td>
                            <td style="border-left: 1px solid #DDDDDD; border-top: 1px solid #DDDDDD; text-align: center;" > <?php echo $return_trip_flights_row->flight_number; ?>  </td>
                            <td style="border-left: 1px solid #DDDDDD; border-top: 1px solid #DDDDDD; text-align: center;" > <?php echo $return_trip_flights_row->flight_date . ' - ' . $return_trip_flights_row->departure_time; ?>  </td>
                            <td style="border-left: 1px solid #DDDDDD; border-top: 1px solid #DDDDDD; text-align: center;" > <?php echo $return_trip_flights_row->arrival_date . ' - ' . $return_trip_flights_row->arrival_time; ?>  </td>
                            <td style="border-left: 1px solid #DDDDDD; border-top: 1px solid #DDDDDD; text-align: center;" > <?php echo $return_trip_flights_row->start_ports_name  .'-'. $return_trip_flights_row->end_ports_name; ?>  </td>
                            <td style="border-left: 1px solid #DDDDDD; border-top: 1px solid #DDDDDD; text-align: center;" > <?php echo $return_trip_flights_row->available_seats_count; ?>  </td>
                            
					</tr>
			        <? } ?>
			    <? } ?>
			<? } ?>
                </tbody>
            </table>

        </div>
    
    
    <?php } else if($item->erp_transportertype_id==1) {
    
    		$land_going_erp_port_id='';
			$land_return_erp_port_id='';
			$land_going_datetime='';
			$land_return_datetime='';
    	
			if (isset($internalpassages)) {
	            foreach ($internalpassages as $inernalpassage) {
		            if($inernalpassage->safa_internalsegmenttype_id == 1) {
		            	$land_going_erp_port_id=$inernalpassage->erp_port_id;
		            	
		            	$land_going_datetime=$inernalpassage->start_datetime;
		            } else if($inernalpassage->safa_internalsegmenttype_id == 2) {
						$land_return_erp_port_id=$inernalpassage->erp_port_id;
						$land_return_datetime=$inernalpassage->start_datetime;
		            }
	            }
			}
		 
			$this->ports_model->erp_port_id = $land_going_erp_port_id;
			$erp_ports_row = $this->ports_model->get();
			$land_going_erp_port_name = $erp_ports_row->{name()};
			
			$this->ports_model->erp_port_id = $land_return_erp_port_id;
			$erp_ports_row = $this->ports_model->get();
			$land_return_erp_port_name = $erp_ports_row->{name()};
			
			
    	
    	?>
    
    		<div style="overflow: hidden; border: 1px solid #DDD;  border-radius: 5px; padding: 10px; margin: 20px 0;">
			<h3 > <?php echo lang('land_trip') ?> </h3>
            <table cellspacing="0" cellpadding="0" style="width:100%; float:right; direction: rtl; border: 0px; position: relative; padding: 0px; margin: 0px; border: 1px solid #DDD;">
                
                <tbody>
                     <tr>
						<td style="border-left: 1px solid #DDDDDD; border-top: 1px solid #DDDDDD; text-align: center;" > <?php echo lang("land_going_erp_port_id"); ?>  </td>
                    	<td style="border-left: 1px solid #DDDDDD; border-top: 1px solid #DDDDDD; text-align: center;" > <?php echo $land_going_erp_port_name; ?>  </td>
                    	
                    	<td style="border-left: 1px solid #DDDDDD; border-top: 1px solid #DDDDDD; text-align: center;" > <?php echo lang("land_going_datetime"); ?>  </td>
                    	<td style="border-left: 1px solid #DDDDDD; border-top: 1px solid #DDDDDD; text-align: center;" > <?php echo $land_going_datetime; ?>  </td>                           
					                           
					</tr>
					
					<tr>
						<td style="border-left: 1px solid #DDDDDD; border-top: 1px solid #DDDDDD; text-align: center;" > <?php echo lang("land_going_erp_port_id"); ?>  </td>
                    	<td style="border-left: 1px solid #DDDDDD; border-top: 1px solid #DDDDDD; text-align: center;" > <?php echo $land_return_erp_port_name; ?>  </td>
                    	
                    	<td style="border-left: 1px solid #DDDDDD; border-top: 1px solid #DDDDDD; text-align: center;" > <?php echo lang("land_going_datetime"); ?>  </td>
                    	<td style="border-left: 1px solid #DDDDDD; border-top: 1px solid #DDDDDD; text-align: center;" > <?php echo $land_return_datetime; ?>  </td>                           
					                           
					</tr>
					
                </tbody>
            </table>

        </div>
    
    <?php }?>
    
    
    
    
    
    
    <!-- ----------------------------------------------  Hotels  --------------------------------------------------- -->
    
    <div style="overflow: hidden; border: 1px solid #DDD;  border-radius: 5px; padding: 10px; margin: 20px 0;">
            <h3 > <?php echo lang('hotels') ?> </h3>

            <table cellspacing="0" cellpadding="0" style="width:100%; float:right; direction: rtl; border: 0px; position: relative; padding: 0px; margin: 0px; border: 1px solid #DDD;">
                <thead>

                    <tr>
                        <th  style="background-color: #eee; color: #333; font-weight: bold; border-left: 1px solid #DDDDDD; box-shadow: 1px 1px 0 #FFFFFF inset; font-weight: 300 !important; padding: 6px; font-size: 11px; text-shadow: 1px 1px 0 #FFFFFF; text-align: center;"><?php echo lang('city') ?></th>
                        <th  style="background-color: #eee; color: #333; font-weight: bold; border-left: 1px solid #DDDDDD; box-shadow: 1px 1px 0 #FFFFFF inset; font-weight: 300 !important; padding: 6px; font-size: 11px; text-shadow: 1px 1px 0 #FFFFFF; text-align: center;"><?php echo lang('hotel') ?></th>
                        <th  style="background-color: #eee; color: #333; font-weight: bold; border-left: 1px solid #DDDDDD; box-shadow: 1px 1px 0 #FFFFFF inset; font-weight: 300 !important; padding: 6px; font-size: 11px; text-shadow: 1px 1px 0 #FFFFFF; text-align: center;"><?php echo lang('entry_date') ?></th>
                        <th  style="background-color: #eee; color: #333; font-weight: bold; border-left: 1px solid #DDDDDD; box-shadow: 1px 1px 0 #FFFFFF inset; font-weight: 300 !important; padding: 6px; font-size: 11px; text-shadow: 1px 1px 0 #FFFFFF; text-align: center;"><?php echo lang('nights_count') ?></th>
                        <th  style="background-color: #eee; color: #333; font-weight: bold; border-left: 1px solid #DDDDDD; box-shadow: 1px 1px 0 #FFFFFF inset; font-weight: 300 !important; padding: 6px; font-size: 11px; text-shadow: 1px 1px 0 #FFFFFF; text-align: center;"><?php echo lang('exit_date') ?></th>
                    </tr>


                </thead>
                <tbody>
				<? 
				$internalpassages_hotels_count = 0;
				$internalpassages_hotels_counter=1;
					
				if (isset($internalpassages_hotels)) { 
			     if (check_array($internalpassages_hotels)) { 
                  foreach ($internalpassages_hotels as $internalpassages_hotel) { 
                 
                 	$next_internalpassages_row = array();
							if(isset($internalpassages_hotels[$internalpassages_hotels_counter])) {
								$next_internalpassages_row = $internalpassages_hotels[$internalpassages_hotels_counter];
							}
							
                 	?>
                     <tr>

                            <td style="border-left: 1px solid #DDDDDD; border-top: 1px solid #DDDDDD; text-align: center;" > <?php echo $internalpassages_hotel->erp_city_name; ?>  </td>
                            <td style="border-left: 1px solid #DDDDDD; border-top: 1px solid #DDDDDD; text-align: center;" > <?php echo $internalpassages_hotel->erp_hotel_name; ?>  </td>
                            <td style="border-left: 1px solid #DDDDDD; border-top: 1px solid #DDDDDD; text-align: center;" > <?php echo date('Y-m-d',strtotime($internalpassages_hotel->end_datetime)); ?></td>
                            <td style="border-left: 1px solid #DDDDDD; border-top: 1px solid #DDDDDD; text-align: center;" > 
                            
                            <? 
                                $nights_count=0;
                                 if(count($next_internalpassages_row)>0) {
	                                	if (strlen($internalpassages_hotel->end_datetime) && strlen($next_internalpassages_row->end_datetime)) {
	                                             $nights_count = ceil((strtotime($next_internalpassages_row->end_datetime) - strtotime($internalpassages_hotel->end_datetime)) / (60 * 60 * 24)); 
	                                        } 
                                        } else {
                                        	$this->internalpassages_model->safa_internalsegmenttype_id = 2;
                            				$leaving_internalpassages_rows = $this->internalpassages_model->get();
                            				if(count($leaving_internalpassages_rows)>0) {
                            					$nights_count = ceil((strtotime($leaving_internalpassages_rows[0]->end_datetime) - strtotime($internalpassages_hotel->end_datetime)) / (60 * 60 * 24));
                            				} 
                                        }
                              ?>
                              <?php echo $nights_count; ?>         
                            </td>
                            <td style="border-left: 1px solid #DDDDDD; border-top: 1px solid #DDDDDD; text-align: center;" > 
							
							<?php 
										$current_row_exit_date='';
                                        if(count($next_internalpassages_row)>0) {
                                        	$current_row_exit_date = date('Y-m-d',strtotime($next_internalpassages_row->end_datetime));
										} else if(isset($leaving_internalpassages_rows)) {
											if(count($leaving_internalpassages_rows)>0) {
												$current_row_exit_date = date('Y-m-d',strtotime($leaving_internalpassages_rows[0]->end_datetime));
											}
										}
										
							?>
							<?php echo $current_row_exit_date; ?>
							</td>
                            
					</tr>
					
					<tr>

                            <td colspan="6">
                                    <table >
                                        <tr>
                                            <td rowspan="2">
                                                <span class="FRight"><?= lang('rooms_count') ?></span>
                                            </td>
                                            
                                            <? if (check_array($erp_hotelroomsizes)) { ?>
                                                <? foreach ($erp_hotelroomsizes as $erp_hotelroomsize) { ?>
                                                    <td>
                                                    	<?php 
                                                    	$current_hotels_rooms_count=0;
                                                    	foreach($safa_trip_internaltrips_hotels_rows as $safa_trip_internaltrips_hotels_row) {
                                                    		//echo $safa_trip_internaltrips_hotels_row->safa_internalsegment_id.' - '. $internalpassages_hotel->safa_internalsegment_id .' - '. $safa_trip_internaltrips_hotels_row->erp_hotelroomsize_id.' - '. $erp_hotelroomsize->erp_hotelroomsize_id.'<br/>';
                                                    		if($safa_trip_internaltrips_hotels_row->erp_hotel_id== $internalpassages_hotel->erp_end_hotel_id && $safa_trip_internaltrips_hotels_row->erp_hotelroomsize_id== $erp_hotelroomsize->erp_hotelroomsize_id) {
                                                    			$current_hotels_rooms_count = $safa_trip_internaltrips_hotels_row->rooms_count ;
                                                    		} 
                                                    	}
                                                    	?>
                                                    	<?= $erp_hotelroomsize->{name()} ?> <?= $current_hotels_rooms_count ?>
                                                        
                                                    </td>
                                                <? } ?>
                                            <? } ?>
                                            
                                            </tr>
                                            </table>
					
					</td>
                    </tr>
                    
			        <? } ?>
			    <? } ?>
			<? } ?>
                </tbody>
            </table>

        </div>
    <!-- ------------------------------------------------------------------------------------------------------------- -->
    
    
    
    
    
    
    
    
    <!-- ------------------------------------------------------ Torism places ---------------------------------------- -->
    <div style="overflow: hidden; border: 1px solid #DDD;  border-radius: 5px; padding: 10px; margin: 20px 0;">
            <h3 > <?php echo lang('tourismplaces') ?> </h3>

            <table cellspacing="0" cellpadding="0" style="width:100%; float:right; direction: rtl; border: 0px; position: relative; padding: 0px; margin: 0px; border: 1px solid #DDD;">
                <thead>

                    <tr>
                        <th  style="background-color: #eee; color: #333; font-weight: bold; border-left: 1px solid #DDDDDD; box-shadow: 1px 1px 0 #FFFFFF inset; font-weight: 300 !important; padding: 6px; font-size: 11px; text-shadow: 1px 1px 0 #FFFFFF; text-align: center;"><?php echo lang('tourismplace') ?></th>
                        <th  style="background-color: #eee; color: #333; font-weight: bold; border-left: 1px solid #DDDDDD; box-shadow: 1px 1px 0 #FFFFFF inset; font-weight: 300 !important; padding: 6px; font-size: 11px; text-shadow: 1px 1px 0 #FFFFFF; text-align: center;"><?php echo lang('the_date') ?></th>
                    </tr>


                </thead>
                <tbody>
				<? if (isset($internalpassages_tourismplaces)) { ?>
			    <? if (check_array($internalpassages_tourismplaces)) { ?>
                 <? foreach ($internalpassages_tourismplaces as $internalpassages_tourismplace) { ?>
                     <tr>

                            <td style="border-left: 1px solid #DDDDDD; border-top: 1px solid #DDDDDD; text-align: center;" > <?php echo $internalpassages_tourismplace->safa_tourismplace_name; ?>  </td>
                            <td style="border-left: 1px solid #DDDDDD; border-top: 1px solid #DDDDDD; text-align: center;" > <?php echo date('Y-m-d H:i',strtotime($internalpassages_tourismplace->start_datetime));?> </td>
                           
					</tr>
			        <? } ?>
			    <? } ?>
			<? } ?>
                </tbody>
            </table>

        </div>
    <!-- -------------------------------------------------------------------------------------------------------------- -->
    
    
    
    
    
    <!-- ------------------------------------------------------ Internal Passages ---------------------------------------- -->
    <div style="overflow: hidden; border: 1px solid #DDD;  border-radius: 5px; padding: 10px; margin: 20px 0;">
            <h3 > <?php echo lang('trip_internaltrip') ?> </h3>

            <table cellspacing="0" cellpadding="0" style="width:100%; float:right; direction: rtl; border: 0px; position: relative; padding: 0px; margin: 0px; border: 1px solid #DDD;">
                <thead>

                    <tr>
                        <th  style="background-color: #eee; color: #333; font-weight: bold; border-left: 1px solid #DDDDDD; box-shadow: 1px 1px 0 #FFFFFF inset; font-weight: 300 !important; padding: 6px; font-size: 11px; text-shadow: 1px 1px 0 #FFFFFF; text-align: center;"><?php echo lang('internalpassage_type') ?></th>
                        <th  style="background-color: #eee; color: #333; font-weight: bold; border-left: 1px solid #DDDDDD; box-shadow: 1px 1px 0 #FFFFFF inset; font-weight: 300 !important; padding: 6px; font-size: 11px; text-shadow: 1px 1px 0 #FFFFFF; text-align: center;"><?php echo lang('internalpassage_starthotel') ?></th>
                        <th  style="background-color: #eee; color: #333; font-weight: bold; border-left: 1px solid #DDDDDD; box-shadow: 1px 1px 0 #FFFFFF inset; font-weight: 300 !important; padding: 6px; font-size: 11px; text-shadow: 1px 1px 0 #FFFFFF; text-align: center;"><?php echo lang('internalpassage_endhotel') ?></th>
                        <th  style="background-color: #eee; color: #333; font-weight: bold; border-left: 1px solid #DDDDDD; box-shadow: 1px 1px 0 #FFFFFF inset; font-weight: 300 !important; padding: 6px; font-size: 11px; text-shadow: 1px 1px 0 #FFFFFF; text-align: center;"><?php echo lang('internalpassage_startdatatime') ?></th>
                       	<th  style="background-color: #eee; color: #333; font-weight: bold; border-left: 1px solid #DDDDDD; box-shadow: 1px 1px 0 #FFFFFF inset; font-weight: 300 !important; padding: 6px; font-size: 11px; text-shadow: 1px 1px 0 #FFFFFF; text-align: center;"><?php echo lang('internalpassage_enddatatime') ?></th>
                        <th  style="background-color: #eee; color: #333; font-weight: bold; border-left: 1px solid #DDDDDD; box-shadow: 1px 1px 0 #FFFFFF inset; font-weight: 300 !important; padding: 6px; font-size: 11px; text-shadow: 1px 1px 0 #FFFFFF; text-align: center;"><?php echo lang('internalpassage_note') ?></th>
                    
                    </tr>


                </thead>
                <tbody>
				<? if (isset($internalpassages)) { ?>
			    <? if (check_array($internalpassages)) { ?>
                 <? foreach ($internalpassages as $internalpassage) { ?>
                     <tr>

                            <td style="border-left: 1px solid #DDDDDD; border-top: 1px solid #DDDDDD; text-align: center;" > 
                            <?= item("safa_internalsegmenttypes", name(), array("safa_internalsegmenttype_id" => $internalpassage->safa_internalsegmenttype_id)); ?>
                            </td>
                            
                            
                            
                            
                            <? if ($internalpassage->safa_internalsegmenttype_id == 1): 
                                
                                	$erp_port='';
                                	if($internalpassage->erp_port_id) {
                                		$erp_port = item("erp_ports", name(), array("erp_port_id" => $internalpassage->erp_port_id));
                                	}
                                	
                                	$erp_end_hotel='';
                                	if($internalpassage->erp_end_hotel_id) {
                                		$erp_end_hotel = item("erp_hotels", name(), array("erp_hotel_id" => $internalpassage->erp_end_hotel_id));
                                	}
                                	
                                
                                ?>
                                    <td style="border-left: 1px solid #DDDDDD; border-top: 1px solid #DDDDDD; text-align: center;" ><?= $erp_port; ?></td>
                                    <td style="border-left: 1px solid #DDDDDD; border-top: 1px solid #DDDDDD; text-align: center;" ><?= $erp_end_hotel; ?></td>
        						<? endif; ?>  
                                <? 
                                
                                	if ($internalpassage->safa_internalsegmenttype_id == 2){ 
                                	$erp_start_hotel='';
                                	if($internalpassage->erp_start_hotel_id) {
                                		$erp_start_hotel = item("erp_hotels", name(), array("erp_hotel_id" => $internalpassage->erp_start_hotel_id));
                                	}
                                	
                                	$erp_port='';
                                	if($internalpassage->erp_port_id) {
                                		$erp_port = item("erp_ports", name(), array("erp_port_id" => $internalpassage->erp_port_id));
                                	}
                                ?>
                                    <td style="border-left: 1px solid #DDDDDD; border-top: 1px solid #DDDDDD; text-align: center;" ><?= $erp_start_hotel; ?></td>
                                    <td style="border-left: 1px solid #DDDDDD; border-top: 1px solid #DDDDDD; text-align: center;" ><?= $erp_port; ?></td>
                                    <? }; ?>
                                    <? if ($internalpassage->safa_internalsegmenttype_id == 3): 
                                    
                                    $erp_start_hotel='';
                                	if($internalpassage->erp_start_hotel_id) {
                                		$erp_start_hotel = item("erp_hotels", name(), array("erp_hotel_id" => $internalpassage->erp_start_hotel_id));
                                	}
                                	
                                	$safa_tourism_place='';
                                	if($internalpassage->safa_tourism_place_id) {
                                		$safa_tourism_place = item("safa_tourismplaces", name(), array("safa_tourismplace_id" => $internalpassage->safa_tourism_place_id));
                                	}
                                	
                                    ?>
                                    <td style="border-left: 1px solid #DDDDDD; border-top: 1px solid #DDDDDD; text-align: center;" ><?= $erp_start_hotel; ?></td>
                                    <td style="border-left: 1px solid #DDDDDD; border-top: 1px solid #DDDDDD; text-align: center;" ><?= $safa_tourism_place; ?></td>
                                    <? endif; ?>
                                    
                                    <? if ($internalpassage->safa_internalsegmenttype_id == 4): 
                                    
                                    $erp_start_hotel='';
                                	if($internalpassage->erp_start_hotel_id) {
                                		$erp_start_hotel = item("erp_hotels", name(), array("erp_hotel_id" => $internalpassage->erp_start_hotel_id));
                                	}
                                	
                                	$erp_end_hotel='';
                                	if($internalpassage->erp_end_hotel_id) {
                                		$erp_end_hotel = item("erp_hotels", name(), array("erp_hotel_id" => $internalpassage->erp_end_hotel_id));
                                	}
                                    
                                    ?>
                                    <td style="border-left: 1px solid #DDDDDD; border-top: 1px solid #DDDDDD; text-align: center;" ><?= $erp_start_hotel; ?></td>
                                    <td style="border-left: 1px solid #DDDDDD; border-top: 1px solid #DDDDDD; text-align: center;" ><?= $erp_end_hotel; ?></td>
                                    <? endif; ?>
                            
                            
                            
                            
                            <td style="border-left: 1px solid #DDDDDD; border-top: 1px solid #DDDDDD; text-align: center;" > <?php echo date('Y-m-d H:i',strtotime($internalpassage->start_datetime));?> </td>
                           	<td style="border-left: 1px solid #DDDDDD; border-top: 1px solid #DDDDDD; text-align: center;" > <?php echo date('Y-m-d H:i',strtotime($internalpassage->end_datetime));?> </td>
                           	
                           	<td style="border-left: 1px solid #DDDDDD; border-top: 1px solid #DDDDDD; text-align: center;" > <?php echo $internalpassage->notes; ?>  </td>
                           
					</tr>
			        <? } ?>
			    <? } ?>
			<? } ?>
			
                </tbody>
            </table>


			<table cellspacing="0" cellpadding="0" style="width:100%; float:right; direction: rtl; border: 0px; position: relative; padding: 0px; margin: 0px; border: 1px solid #DDD;">
                
                <tbody>
                
                	<tr>
						<td colspan="4"></td>                       
					</tr>
					
					<tr>
						<td colspan="4" style="border-left: 1px solid #DDDDDD; border-top: 1px solid #DDDDDD; text-align: center;" > 
						
						<?php echo item("safa_internaltrip_types", name(), array("safa_internaltrip_type_id" => $item->safa_internaltrip_type_id)); ?>
						
						</td>
					</tr>
					
                     <tr>
						<td style="border-left: 1px solid #DDDDDD; border-top: 1px solid #DDDDDD; text-align: center;" > <?php echo lang("transport_request_opertator"); ?>  </td>
                    	<td style="border-left: 1px solid #DDDDDD; border-top: 1px solid #DDDDDD; text-align: center;" > <?php if($item->safa_ito_id!='') {echo item("safa_itos", name(), array("safa_ito_id" => $item->safa_ito_id));} ?>   </td>
                    	
                    	<td style="border-left: 1px solid #DDDDDD; border-top: 1px solid #DDDDDD; text-align: center;" > <?php echo lang("transport_request_transportername"); ?>  </td>
                    	<td style="border-left: 1px solid #DDDDDD; border-top: 1px solid #DDDDDD; text-align: center;" > <?php if($item->safa_transporter_id!='') {echo item("safa_transporters", name(), array("safa_transporter_id" => $item->safa_transporter_id));} ?>  </td>                           
					                           
					</tr>
					
					<tr>
						<td style="border-left: 1px solid #DDDDDD; border-top: 1px solid #DDDDDD; text-align: center;" > <?php echo lang("transport_request_res_code"); ?>  </td>
                    	<td style="border-left: 1px solid #DDDDDD; border-top: 1px solid #DDDDDD; text-align: center;" > <?php echo $item->operator_reference; ?>  </td>
                    	
                    	<td style="border-left: 1px solid #DDDDDD; border-top: 1px solid #DDDDDD; text-align: center;" > <?php echo lang("confirmation_number"); ?>  </td>
                    	<td style="border-left: 1px solid #DDDDDD; border-top: 1px solid #DDDDDD; text-align: center;" > <?php echo $item->confirmation_number; ?>  </td>                           
					                           
					</tr>
					
                </tbody>
            </table>


        </div>
    <!-- -------------------------------------------------------------------------------------------------------------- -->
    
    
      
    
    
    
    
    
    
    <!-- ------------------------------------------------------ Group Passports ---------------------------------------- -->
    <div style="overflow: hidden; border: 1px solid #DDD;  border-radius: 5px; padding: 10px; margin: 20px 0;">
            <h3 > <?php echo lang('safa_group_passports') ?> </h3>

            <table cellspacing="0" cellpadding="0" style="width:100%; float:right; direction: rtl; border: 0px; position: relative; padding: 0px; margin: 0px; border: 1px solid #DDD;">
                <thead>

                    <tr>
                        <th  style="background-color: #eee; color: #333; font-weight: bold; border-left: 1px solid #DDDDDD; box-shadow: 1px 1px 0 #FFFFFF inset; font-weight: 300 !important; padding: 6px; font-size: 11px; text-shadow: 1px 1px 0 #FFFFFF; text-align: center;"><?php echo lang('trip_details_traveller') ?></th>
                        <th  style="background-color: #eee; color: #333; font-weight: bold; border-left: 1px solid #DDDDDD; box-shadow: 1px 1px 0 #FFFFFF inset; font-weight: 300 !important; padding: 6px; font-size: 11px; text-shadow: 1px 1px 0 #FFFFFF; text-align: center;"><?php echo lang('trip_details_passport_no') ?></th>
                    	<th  style="background-color: #eee; color: #333; font-weight: bold; border-left: 1px solid #DDDDDD; box-shadow: 1px 1px 0 #FFFFFF inset; font-weight: 300 !important; padding: 6px; font-size: 11px; text-shadow: 1px 1px 0 #FFFFFF; text-align: center;"><?php echo lang('trip_details_visa_no') ?></th>
                        <th  style="background-color: #eee; color: #333; font-weight: bold; border-left: 1px solid #DDDDDD; box-shadow: 1px 1px 0 #FFFFFF inset; font-weight: 300 !important; padding: 6px; font-size: 11px; text-shadow: 1px 1px 0 #FFFFFF; text-align: center;"><?php echo lang('trip_details_nationality') ?></th>
                    	<th  style="background-color: #eee; color: #333; font-weight: bold; border-left: 1px solid #DDDDDD; box-shadow: 1px 1px 0 #FFFFFF inset; font-weight: 300 !important; padding: 6px; font-size: 11px; text-shadow: 1px 1px 0 #FFFFFF; text-align: center;"><?php echo lang('trip_details_gender') ?></th>
                        <th  style="background-color: #eee; color: #333; font-weight: bold; border-left: 1px solid #DDDDDD; box-shadow: 1px 1px 0 #FFFFFF inset; font-weight: 300 !important; padding: 6px; font-size: 11px; text-shadow: 1px 1px 0 #FFFFFF; text-align: center;"><?php echo lang('group_passports_age') ?></th>
                    	<th  style="background-color: #eee; color: #333; font-weight: bold; border-left: 1px solid #DDDDDD; box-shadow: 1px 1px 0 #FFFFFF inset; font-weight: 300 !important; padding: 6px; font-size: 11px; text-shadow: 1px 1px 0 #FFFFFF; text-align: center;"><?php echo lang('group_passports_occupation') ?></th>
                        <th  style="background-color: #eee; color: #333; font-weight: bold; border-left: 1px solid #DDDDDD; box-shadow: 1px 1px 0 #FFFFFF inset; font-weight: 300 !important; padding: 6px; font-size: 11px; text-shadow: 1px 1px 0 #FFFFFF; text-align: center;"><?php echo lang('group_passports_picture') ?></th>
                    
                    </tr>


                </thead>
                <tbody>
				<? if (isset($safa_group_passports)) { ?>
			    <? if (check_array($safa_group_passports)) { ?>
                 <? foreach ($safa_group_passports as $safa_group_passport) { ?>
                     <tr>

                            <td style="border-left: 1px solid #DDDDDD; border-top: 1px solid #DDDDDD; text-align: center;" > <?= $safa_group_passport->{'first_' . name()} . ' ' . $safa_group_passport->{'second_' . name()} . ' ' . $safa_group_passport->{'third_' . name()} . ' ' . $safa_group_passport->{'fourth_' . name()} ?>  </td>
                            <td style="border-left: 1px solid #DDDDDD; border-top: 1px solid #DDDDDD; text-align: center;" > <?= $safa_group_passport->passport_no ?> </td>
                           	<td style="border-left: 1px solid #DDDDDD; border-top: 1px solid #DDDDDD; text-align: center;" > <?= $safa_group_passport->visa_number ?> </td>
                           	<td style="border-left: 1px solid #DDDDDD; border-top: 1px solid #DDDDDD; text-align: center;" > <?= $safa_group_passport->erp_nationalities_name ?> </td>
                           	<td style="border-left: 1px solid #DDDDDD; border-top: 1px solid #DDDDDD; text-align: center;" > <?= $safa_group_passport->erp_gender_name ?> </td>
                           	<td style="border-left: 1px solid #DDDDDD; border-top: 1px solid #DDDDDD; text-align: center;" > 
                           	
                           	<?php
			                                    $date_of_birth = strtotime($safa_group_passport->date_of_birth);
			                                    $date_now = strtotime(date('Y-m-d', time()));
			                                    $secs = $date_now - $date_of_birth; // == return sec in difference
			                                    $days = $secs / 86400;
			                                    $age = $days / 365;
												//$age=round($age);
												//$age=ceil($age);
			                                    $age = floor($age);
			                                    echo $age;
			                                    ?>
			                                    
                           	</td>
                           	<td style="border-left: 1px solid #DDDDDD; border-top: 1px solid #DDDDDD; text-align: center;" > <?= $safa_group_passport->occupation ?> </td>
                           	<td style="border-left: 1px solid #DDDDDD; border-top: 1px solid #DDDDDD; text-align: center;" > 
                           	<?php 
                                                	if ($safa_group_passport->picture != '') {
					                             ?>
					                                <img src="<?= base_url('static/group_passports') . '/' . $safa_group_passport->picture; ?>" alt="" width="50px" height="60px">
					                            <?php
					                            	}
                                                 ?>
                           	 </td>
                           
					</tr>
			        <? } ?>
			    <? } ?>
			<? } ?>
                </tbody>
            </table>

        </div>
    <!-- -------------------------------------------------------------------------------------------------------------- -->
    
    
    
    </div>