
    

<div class="widget">
    <div class="path-container Fright">
        <div class="path-name Fright">
        <a <?if(session('admin_login')):?>href='<?=  site_url('admin/dashboard')?>' 
                <?elseif(session('ea_login')):?> href='<?=  site_url('ea/dashboard')?>'
                <?elseif(session('uo_login')):?> href='<?=  site_url('uo/dashboard')?>'
                <?elseif(session('ito_login')):?> href='<?=  site_url('ito/dashboard')?>'
             <?  endif;?> ><?= lang('global_system_management') ?></a>
        </div>
        <div class="path-arrow Fright">
        </div>        
        <div class="path-name Fright">
        <a href="<?= $back_url?>"> <?= $model_title ?></a>   
        </div>
        <div class="path-arrow Fright">
        </div>        
        <div class="path-name Fright">
            <?= $action ?>
        </div>
    </div>
        
	</div>

<div class="widget">
    <div class="msg">
        <p> 
            <p><?= $msg ?></p>
            <input  type ="button" value="<?= lang('print') ?>"class="btn btn-primary" onclick="window.location = '<?= $confirmation_url ?>'">
            <a href="<?= $back_url?>" class="btn btn-primary" ><?= lang('global_back') ?></a>
            <?php if(session('ea_id')) {?>
            <a class='fancybox fancybox.iframe btn btn-primary' title='<?=lang('send_trip_internaltrip_email_to_uo')?>' href="<?= site_url('safa_trip_internaltrips/send_email_to_uo_popup/' . $id) ?>"  target="_blank" ><?php echo lang('send_to_uo');?></a>
		    <?php } ?>                                
        </p>
    </div>
    </div>
        <style>
            .msg{
                padding:8px 35px 8px 14px;margin-bottom:20px;color:#c09853;text-shadow:0 1px 0 rgba(255,255,255,0.5);background-color:#fcf8e3;border:1px solid #fbeed5;-webkit-border-radius:4px;-moz-border-radius:4px;border-radius:4px;
                color:#468847;background-color:#dff0d8;border-color:#d6e9c6; border: 3px solid rgb(255, 255, 255); text-align: center;
            }  
        </style>

<script>
    $(document).ready(function() {
        $('.fancybox').fancybox({
            afterClose: function() {
                //parent.location.reload(true);
            }
        });
    });
</script>