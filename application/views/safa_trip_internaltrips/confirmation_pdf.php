<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <style>
            table.tbl{
                border-width: 1px;
                border-top-color: black;
            border-left-color: black;
            border-bottom-color: black;
            border-right-color: black;
            }
            .tbl td,th,caption {
                border-width: 1px;
            border-top-color: black;
            border-left-color: black;
            border-bottom-color: black;
            border-right-color: black;
            font-weight: bold;
            }
            
            .ftr td,th,caption {
                border-width: 0px;
                border-color: transparent;
            }
            .ftr tr {
                border-width: 1px;
            }
        </style>
    </head>
    <body><div style="margin: 0 auto;display: table;">
        <table cellpadding="0" cellspacing="5" border="0" align="right" width="630" dir="rtl">
            <tr>
                <td>
                    <table width="100%">
                    	<tr>
                            <td align="center" width="609px"><?php if( is_file('static/img/ito_companies_logos/'.$ito_dir.'/transporters/'.$transporter_dir.'/logo.png') ) :?>
                            <img src="<?php echo site_url(); ?>static/img/ito_companies_logos/<?php echo $ito_dir; ?>/transporters/<?php echo $transporter_dir; ?>/logo.png" />
                            <?php endif ?>
                            </td>
                        </tr>
                        
                        <tr>
                            <td width="120"> تأكيد الحجز </td>
                            <td width="380" align="center" style="padding-right:80px;padding-top:10px;">
                                <b>   (( اشعار حجز  ))   <br />  رقم ( <?php echo $item->operator_reference; ?> )    </b>
                            </td>
                            <td align="left" width="109px">
                           <table><tr><td> طلب اشعار </td> <td><?php echo $item->operator_reference; ?></td></tr></table> 
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table class="" align="center" border="0" cellpadding="0" cellspacing="0" width="100%" >
                        <tr>
                            <td align="center" width="100">
                                <font><b><?php echo $item->contract_name; ?></b></font>
                            </td>
                            <td align="right" width="125" colspan="2">
                                <font ><b> <?= $safa_uo_code ?>  -  <?= $item->serial ?> </b></font>
                            </td>
                            <td align="center" width="150" colspan="2">
                                <font color="#0000ff"><b></b></font>
                            </td>
                            <td align="center" width="195" colspan="2">
                            
                            </td>
                        </tr>
                        <tr>
                            <td align="center" width="570">
                                <font ><b> السادة: شركة <?php echo $safa_uo_name; ?> المحترمين</b></font>
                            </td>
                            
                        </tr>
                        
                        <tr>
                            <td align="center" width="100">
                                <font color="#0000ff"><b></b></font>
                            </td>
                            <td align="right" width="125" colspan="2">
                                <font color="#0000ff"><b> </b></font>
                            </td>
                            <td align="center" width="150" colspan="2">
                                <font color="#0000ff"><b></b></font>
                            </td>
                            <td align="center" width="195" colspan="2">
                            مجموعة / <?php echo $item->contract_name; ?>
                            </td>
                        </tr>
                        
                        <tr>
                            <td align="center" colspan="2">
                                <font color="#0000ff"><b>&nbsp;</b></font>
                            </td>
                            <td align="center" colspan="4">
                                <font color="#0000ff"><b>&nbsp;</b></font>
                            </td>
                            <td align="center">
                                <font color="#0000ff"><b>&nbsp;</b></font>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center">&nbsp;<br /><b>نعلمكم بحجز الحافلة/الحافلات المطلوبة وفق خط السير التالى 
                		<?php 
                            $safa_buses_brand_names='';
                            if (isset($arriving_segment_bus_brands)) { 
					     		if (check_array($arriving_segment_bus_brands)) {
					     			 
					     			$arriving_segment_bus_brands_counter=0;
			                  		foreach ($arriving_segment_bus_brands as $arriving_segment_bus_brand) {
			                  			$safa_buses_brand_names = $safa_buses_brand_names.'('.$arriving_segment_bus_brand->bus_count.')  '.$arriving_segment_bus_brand->safa_buses_brand_name.' - '.$arriving_segment_bus_brand->safa_buses_model_name.' <br/> ';
			                  			
			                  			$arriving_segment_bus_brands_arr = array(
										'safa_transporters_buses_id'=>$arriving_segment_bus_brands[$arriving_segment_bus_brands_counter]->safa_buses_models_id, 
										'seats_count'=>$arriving_segment_bus_brands[$arriving_segment_bus_brands_counter]->seats_count
										);
			                  		}
					     		}
                            } 
                            echo $safa_buses_brand_names;
                 			?>
                </b></td>
            </tr>
           
        
            <tr>
                <td height="75px" border="0" cellpadding="0" cellspacing="0" valigin="top">
                    <table class="tbl" width="100%" border="0" cellpadding="0" cellspacing="0" id="tbl_internal_segments">
                        <tr>
                        	<td align="center" width="20"><b><?php echo lang('internalpassage_serial') ?></b></td>
                            <td align="center"  width="45"><b><?php echo lang('internalpassage_type') ?></b></td>
	                        <td align="center" width="90"><b><?php echo lang('internalpassage_starthotel') ?></b></td>
	                        <td align="center" width="90"><b><?php echo lang('internalpassage_endhotel') ?></b></td>
	                        <td align="center" width="75"><b><?php echo lang('internalpassage_startdatatime') ?></b></td>
	                       	<td align="center" width="75"><b><?php echo lang('internalpassage_enddatatime') ?></b></td>
	                       	<td align="center" width="120"><b><?php echo lang('internalpassage_buses') ?></b></td>
	                        <td align="center" width="70"><b>القيمة</b></td>
                        </tr>
                        
                        
                   	<?php if (isset($internalpassages)) {
			    	if (check_array($internalpassages)) { 
			    	
			    		$internalpassage_serial=1;
			    		
			    		$segment_bus_brands_different=false;
			    		
                 	foreach ($internalpassages as $internalpassage) {
                 		 
                 	?>
                     <tr>
							<td align="center"><b> <?php echo $internalpassage_serial; ?>  </b></td>
                            <td align="center"><b> <?php echo item("safa_internalsegmenttypes", name(), array("safa_internalsegmenttype_id" => $internalpassage->safa_internalsegmenttype_id)); ?>  </b></td>
                            
                            <?php if ($internalpassage->safa_internalsegmenttype_id == 1) { 
                                	$erp_port='';
                                	if($internalpassage->erp_port_id) {
                                		$erp_port = item("erp_ports", name(), array("erp_port_id" => $internalpassage->erp_port_id));
                                	}
                                	$erp_end_hotel='';
                                	if($internalpassage->erp_end_hotel_id) {
                                		$erp_end_hotel = item("erp_hotels", name(), array("erp_hotel_id" => $internalpassage->erp_end_hotel_id));
                                	}
                            ?>
                            <td align="center"><b> <?php echo $erp_port; ?>  </b></td>
                            <td align="center"><b> <?php echo $erp_end_hotel; ?>  </b></td>
                            <?php } else if ($internalpassage->safa_internalsegmenttype_id == 2){  
                            $erp_start_hotel='';
                                	if($internalpassage->erp_start_hotel_id) {
                                		$erp_start_hotel = item("erp_hotels", name(), array("erp_hotel_id" => $internalpassage->erp_start_hotel_id));
                                	}
                                	
                                	$erp_port='';
                                	if($internalpassage->erp_port_id) {
                                		$erp_port = item("erp_ports", name(), array("erp_port_id" => $internalpassage->erp_port_id));
                                	}
                            ?>
                            <td align="center"><b> <?php echo $erp_start_hotel; ?>  </b></td>
                            <td align="center"><b> <?php echo $erp_port; ?>  </b></td>
                            
                            <?php } else if ($internalpassage->safa_internalsegmenttype_id == 3){  
                            $erp_start_hotel='';
                                	if($internalpassage->erp_start_hotel_id) {
                                		$erp_start_hotel = item("erp_hotels", name(), array("erp_hotel_id" => $internalpassage->erp_start_hotel_id));
                                	}
                                	
                                	$safa_tourism_place='';
                                	if($internalpassage->safa_tourism_place_id) {
                                		$safa_tourism_place = item("safa_tourismplaces", name(), array("safa_tourismplace_id" => $internalpassage->safa_tourism_place_id));
                                	}
                            ?>
                            <td align="center"><b> <?php echo $erp_start_hotel; ?>  </b></td>
                            <td align="center"><b> <?php echo $safa_tourism_place; ?>  </b></td>
                            
                            <?php } else if ($internalpassage->safa_internalsegmenttype_id == 4){  
                            $erp_start_hotel='';
                                	if($internalpassage->erp_start_hotel_id) {
                                		$erp_start_hotel = item("erp_hotels", name(), array("erp_hotel_id" => $internalpassage->erp_start_hotel_id));
                                	}
                                	
                                	$erp_end_hotel='';
                                	if($internalpassage->erp_end_hotel_id) {
                                		$erp_end_hotel = item("erp_hotels", name(), array("erp_hotel_id" => $internalpassage->erp_end_hotel_id));
                                	}
                            ?>
                            <td align="center"><b> <?php echo $erp_start_hotel; ?>  </b></td>
                            <td align="center"><b> <?php echo $erp_end_hotel; ?>  </b></td>
                            
                            <?php }?>
                            
                            
                            <td align="center"><b> <?php echo date('m/d H:i',strtotime($internalpassage->start_datetime));?> </b></td>
                            <td align="center"><b> <?php echo date('m/d H:i',strtotime($internalpassage->end_datetime));?> </b></td>
                            <td align="center"><b>
                            <?php 
                            
                            
                            $segment_bus_brands = $this->internalsegments_drivers_and_buses_model->get_distinct_bus_brands_by_safa_internalsegment($internalpassage->safa_internalsegment_id);
                            $segment_bus_brands_arr = array();
                            $segment_bus_brands_counter=0;
                            
                            
                            
		                            $safa_buses_brand_names='';
		                            if (isset($segment_bus_brands)) { 
							     		if (check_array($segment_bus_brands)) {
					                  		foreach ($segment_bus_brands as $segment_bus_brand) {
					                  			$safa_buses_brand_names = $safa_buses_brand_names.'('.$segment_bus_brand->bus_count.')  '.$segment_bus_brand->safa_buses_brand_name.' - '.$segment_bus_brand->safa_buses_model_name.' <br/> ';
					                  			
					                  			$segment_bus_brands_arr = array(
												'safa_transporters_buses_id'=>$segment_bus_brands[$segment_bus_brands_counter]->safa_buses_models_id, 
												'seats_count'=>$segment_bus_brands[$segment_bus_brands_counter]->seats_count
												);
					                  		}
							     		}
		                            } 

					                    if(isset($arriving_segment_bus_brands_arr)) {
				                          	if($arriving_segment_bus_brands_arr!=$segment_bus_brands_arr) {
					                            echo $safa_buses_brand_names;
					                            $segment_bus_brands_different=true;
				                            }
			                            }
		                            
                            ?>
                            </b></td>
                            
                            <td align="center"><b> <?php echo $internalpassage->notes; ?> </b></td>
                            
					</tr>
			        <?php 
                 	$internalpassage_serial++;
                 	} 
			     } 
			 } ?>
                        
                        
                    </table>
                    
                    
                    
                </td>
            </tr>
            
            
            <tr><td style="padding-right: 10px;" dir="rtl"></td></tr>
            
            
            <!-- 
            <tr>
                <td style="padding-right: 10px;" dir="rtl">
                    <?php echo file_get_contents('static/img/ito_companies_logos/'.$ito_dir.'/footer.html'); ?>
                </td>
            </tr>
            
            <tr><td style="padding-right: 10px;" dir="rtl"></td></tr>
            
            <tr>
                <td style="padding-right: 10px;" dir="rtl">
                    <?php echo file_get_contents('static/img/ito_companies_logos/'.$ito_dir.'/transporters/'.$transporter_dir.'/footer.html'); ?>
                </td>
            </tr>
             -->
            
            <tr>
            <td style="padding-right: 10px;" dir="rtl" border="0">
            <table class="" width="100%" cellpadding="0" cellspacing="0" border="0">
             <tr>
             <td colspan="3" align="center">
			<?php echo file_get_contents('static/img/ito_companies_logos/'.$ito_dir.'/footer.html'); ?>
			
			<?php echo file_get_contents('static/img/ito_companies_logos/'.$ito_dir.'/transporters/'.$transporter_dir.'/footer.html'); ?>			  
						  
			</td>
              </tr>
              </table>
                        
                </td>
            </tr>         
                        
            
        </table>
        </div>
    </body>
</html>