<style>
    .widget {width:98%}
    .wizerd-div {
        border-bottom: medium none !important;
        margin: -18px 0 20px;
        padding-top: 0;
    }
    .wizerd-div a {
        background: none repeat scroll 0 0 #FAFAFA;
        border: 1px solid #D5D6D6;
        border-radius: 49px;
        color: #663300;
        display: inline-block;
        margin: -6px 5px -17px -8px;
        padding: 10px 12px 8px;
    }
    a {
        color: #C09853;
    }
    .resalt-group {
        margin: 18px 0.5% 0.5%;
        padding: 0.5%;
        width: 99%;
    }
    th a.btn, th input[type="button"], th input[type="submit"], th button {
        margin: 0;
        padding: 4px 12px;
    }
    .coll_close, .coll_open {
        margin-top: 0;
    }
    .chosen-container {
        margin-top: 4px;
    }
    .warning {
        color: #C09853;
    }
</style>

<div class="widget">
    <div class="path-container Fright">
        <div class="path-name Fright">
            <a href="<?php 
            if(session('uo_id')) {
            	echo  site_url().'uo/dashboard'; 
            } else if(session('ea_id')) {
            	echo  site_url().'ea/dashboard';
            } else if(session('ito_id')) {
            	echo  site_url().'ito/dashboard';
            } else {
            	echo  site_url();
            }
            ?>"><?php echo  lang('global_system_management') ?></a>
        </div>
        <div class="path-arrow Fright"> </div>
        <div class="path-name Fright"><?= lang('safa_trip_internaltrips') ?></div>
        
        </div>
         <a title="<?= lang('global_add') ?>" href="<?= site_url("safa_trip_internaltrips/manage") ?>" class="btn Fleft"><?= lang('global_add')?></a>
</div>


<div class="widget">
      
        <div class="widget-header">
	
	            <div class="widget-header-icon Fright">
	                <span class="icos-pencil2"></span>
	            </div>
	
	            <div class="widget-header-title Fright">
	                <?= lang('global_search') ?>
	            </div>
            
		</div>
        
        <?= form_open("","method='get' id='safa_trip_internaltrips_search' ") ?>
        <div class="block-fluid">
            <div class="row-form">
                <div class="span6">
                    <div class="span2"><?= lang('transport_request_status') ?></div>
                    <div class="span10">
                        <?= form_dropdown("safa_tripstatus_id", $safa_intrernaltripstatus, set_value("safa_tripstatus_id"),"  class=' chosen-select' ") ?>
                    </div>
                </div>
                <?php if  (session('uo_id') || session('ea_id')) { ?>
                <div class="span6">
                    <div class="span2"><?= lang('transport_request_opertator') ?></div>
                    <div class="span10">
                    
                        <?= form_dropdown("safa_ito_id", $safa_ito, set_value("safa_ito_id"), "  class=' chosen-select' "); ?>
                    
                    </div>
                </div>
                <?php } ?>
                
            </div>
            <div class="row-form">
                <div class="span6">
                    <div class="span2"><?= lang('transport_request_contract') ?></div>
                    <div class="span10">
                        <?= form_dropdown("safa_uo_contract_id", $safa_uo_contracts, set_value("safa_uo_contract_id"), " id='safa_uo_contract_id'  class=' chosen-select' ") ?>
                    </div>
                </div>
                <div class="span6">
                    <div class="span2"><?= lang('transport_request_trip_id') ?></div>
                    <div class="span10">
                        <?php if(session('ea_id')){?>
                        <?= form_dropdown("safa_trip_id", array("0" => lang('global_select_from_menu')), set_value("safa_trip_id"), "id='trip_id'  class=' chosen-select' ") ?>
                        <?php } else { ?>
                        <?= form_input("serial", set_value("serial"), "class='input-huge' ") ?>
                        <?php }?> 
                    </div>
                </div>
            </div>
            <div class="row-form">
                <div class="span6">
                    <div class="span2"><?= lang('transport_request_transportername') ?></div>
                    <div class="span10">
                        <?= form_dropdown("safa_transporter_id", $bus_transporters, set_value("safa_transporter_id"), "  class=' chosen-select'  ") ?>
                    </div>
                </div> 
                
                <?php if(session('uo_id')){?>
                <div class="span6">
                    <div class="span2"><?= lang('confirmation_number') ?></div>
                    <div class="span10">
                         <?= form_input("confirmation_number", set_value("confirmation_number"), " class='input-huge' ") ?>
                       
                    </div>
                </div> 
                <?php } ?>
                
            </div>
            
            
            <div class="row-form">
                <div class="span6">
                    <div class="span2"><?= lang('arrival_date') ?></div>
                   <div class="span1"><?= lang('from') ?>:</div> <div class="span4"><?php echo form_input('arrival_date_from', set_value("arrival_date_from")," style='' id='arrival_date_from' class=' input-huge date' ") ?>
					</div>
					<div class="span1"><?= lang('to') ?>:</div> <div class="span4"><?php echo form_input('arrival_date_to', set_value("arrival_date_to")," style='' id='arrival_date_to' class=' input-huge date' ") ?>
					</div>
                </div> 
                
                <div class="span6">
                    <div class="span2"><?= lang('leaving_date') ?></div>
                   <div class="span1"><?= lang('from') ?>:</div> <div class="span4"><?php echo form_input('leaving_date_from', set_value("leaving_date_from")," style='' id='leaving_date_from' class=' input-huge date' ") ?>
					</div>
					<div class="span1"><?= lang('to') ?>:</div> <div class="span4"><?php echo form_input('leaving_date_to', set_value("leaving_date_to")," style='' id='leaving_date_to' class=' input-huge date' ") ?>
					</div>
                </div>  
                
            </div>
            
            
            
            <div class="toolbar bottom TAC">
                <input type="submit"  class="btn btn-primary" name="search" value="<?= lang('global_search') ?>" />
            </div>
        </div>
        <?= form_close() ?>
    </div>
    

<div class="widget">
    <div class="widget-header">
        <div class="widget-header-icon Fright"></div>
        <div class="widget-header-title Fright"><?= lang('safa_trip_internaltrips') ?></div>
        
    </div>
    <div class="widget-container">
        <div class='table-responsive' >
            <table cellpadding="0" class="fsTable" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th><?= lang('confirmation_number') ?></th>
                        <th><?= lang('transport_request_trip_id') ?></th>
                        <th><?= lang('transport_request_contract') ?></th> 
                        <?php if  (session('uo_id') || session('ea_id')) { ?>                       
                        <th><?= lang('transport_request_opertator') ?></th>
                        <?php } ?>
                        <th><?= lang('transport_request_transportername') ?></th>
                        <th><?= lang('transport_request_status') ?></th>
                        <th><?= lang('transport_request_res_code') ?></th>
                        <th><?= lang('transport_request_internalpassage_num') ?></th>
                        <th width="150px"><?= lang('global_actions') ?></th>
                    </tr>
                	</thead>
                
                <tbody>
                    <? if (isset($items)) { ?>
                    
                        <? foreach ($items as $item) { ?>
                            <tr>
                                <td><span class='label'style='background-color:<?=item("safa_internaltripstatus",'color',array('safa_internaltripstatus_id'=>$item->safa_internaltripstatus_id))?>' >
                                     <?php
                                     //echo $safa_uo_code.'-'.$item->confirmation_number;
                                     echo $item->confirmation_number;
                                     ?>
                                </span></td>
                                <td><?=$item->trip_title?></td>
                                <td>
                                
                                <?php
                                if($item->contract_name!='') {
                                	echo $item->contract_name;
                                } else if(session('ito_id')) {
                                	echo $item->umoperatorname;
                                }
                                ?>
                                </td>
                                
                                <?php if  (session('uo_id') || session('ea_id')) { ?>
                                <td><?=$item->ito_name?></td>
                                <?php } ?>
                                
                                <td><?=$item->transportername?></td>
                                <td><?=$item->status_name?></td>
                                <td><?= $item->operator_reference?></td>
                                <td><?=$item->num_segments?></td>
                                <td class='TAC' title="<?= htmlspecialchars($item->ito_notes,ENT_QUOTES)?>"> 
                                    <?php if (session('uo_id') || session('ea_id')) { ?>
                                    <a title='<?=lang('global_edit')?>' href="<?=  site_url('safa_trip_internaltrips/manage/'.$item->safa_trip_internaltrip_id)?>"><span class="icon-pencil"></span></a>
                                    <!-- <a title='<?=lang('global_delete')?>' href="<?=  site_url('safa_trip_internaltrips/delete/'.$item->safa_trip_internaltrip_id)?> " onclick="return window.confirm('<?=lang('global_are_you_sure_you_want_to_delete')?>')"   ><span class="icon-trash"></span></a>-->
									<a title='<?=lang('global_delete')?>' href="<?=  site_url('safa_trip_internaltrips/delete_confirmation/'.$item->safa_trip_internaltrip_id)?> " ><span class="icon-trash"></span></a>
                                    
                                    <?php } else if (session('ito_id')) {
                                    		if($item->owner_erp_company_type_id==5) {
                                    	?>
                                    <a title='<?=lang('global_edit')?>' href="<?=  site_url('safa_trip_internaltrips/manage/'.$item->safa_trip_internaltrip_id)?>"><span class="icon-pencil"></span></a>
                                    <!-- <a title='<?=lang('global_delete')?>' href="<?=  site_url('safa_trip_internaltrips/delete/'.$item->safa_trip_internaltrip_id)?> " onclick="return window.confirm('<?=lang('global_are_you_sure_you_want_to_delete')?>')"   ><span class="icon-trash"></span></a>-->
									<a title='<?=lang('global_delete')?>' href="<?=  site_url('safa_trip_internaltrips/delete_confirmation/'.$item->safa_trip_internaltrip_id)?> " ><span class="icon-trash"></span></a>
                                    
                                    <?php }
                                    }
                                    ?>
                                    
									
                                	<?php if(session('uo_id')) { ?>
	                                	
	                                	<a class='fancybox fancybox.iframe' title='<?=lang('send_trip_internaltrip_email_to_ito')?>' href="<?= site_url('safa_trip_internaltrips/send_email_to_ito_popup/' . $item->safa_trip_internaltrip_id) ?>" target="_blank" ><span class="icon-envelope"></span></a>
		                                    
	                                <?php } else if(session('ea_id')) { ?>
									
									
										<div style="position: relative; display:inline-block; width: 10%" >
                        				<a class="dropdown-toggle" data-toggle="dropdown" href="#"><span class="icon-envelope"  <?php if($item->uo_email_sending_datetime!='') {?> style="color:#AAAAAA" <?php } ?> ></span></a>
		                                <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
		                                    <li>
		                                    <a class='fancybox fancybox.iframe' title='<?=lang('send_trip_internaltrip_email_to_uo')?>' href="<?= site_url('safa_trip_internaltrips/send_email_to_uo_popup/' . $item->safa_trip_internaltrip_id) ?>" target="_blank" ><?php echo lang('send_to_uo');?></a>
		                                    </li>
											
											<?php if($item->safa_ito_id!=0 && $item->safa_ito_id!='' && $item->safa_internaltrip_type_id==1) { ?>
											<li>
		                                    <a class='fancybox fancybox.iframe' title='<?=lang('send_trip_internaltrip_email_to_ito')?>' href="<?= site_url('safa_trip_internaltrips/send_email_to_ito_popup/' . $item->safa_trip_internaltrip_id) ?>" target="_blank" ><?php echo lang('send_to_ito');?></a>
		                                    </li>
		                                    <?php }?>
		                                    
		                                </ul>
                        				</div>
                        				
										<?php
                                	}
                                	?>
                                	
                                	<?php 
                                	if(session('uo_id') || session('ito_id')) { 
                                		if($item->operator_reference=='') {
                                	?>
                                		<a title='<?=lang('confirmation')?>'  href="javascript:void();" onclick="alert('<?=lang('you_must_enter_operator_reference_to_confirm_trip_internaltrip')?>');"><span class="icon-ok"></span></a>
                                	<?php 
                                		//} else if((session('uo_id')&& $item->safa_internaltripstatus_id== 4) || (session('ito_id')&& $item->safa_internaltripstatus_id== 5)) {
                                		} else if((session('uo_id')&& $item->safa_internaltripstatus_id== 4) || ($item->safa_internaltripstatus_id== 5)) {
                                		
                                		} else {
                                		?>
	                                	<a title='<?=lang('confirmation')?>'  href="<?=  site_url('safa_trip_internaltrips/confirm/'.$item->safa_trip_internaltrip_id)?>"><span class="icon-ok"></span></a>
	                                <?php }
                                	}
	                                ?>	
	                                	
	                               <div style="position: relative; display:inline-block; width: 10%" >
                        			<a class="dropdown-toggle" data-toggle="dropdown" href="#"><span class="icon-print" ></span></a>
	                                <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
	                                    
	                                    <?php if(session('uo_id')) { ?>
	                                	<li>
	                                    <a title='<?=lang('print_internalsegments_report')?>' target="_blank" href="<?=  site_url('safa_trip_internaltrips/internalsegments_report/'.$item->safa_trip_internaltrip_id.'/pdf/D')?>"><?php echo lang('print_internalsegments_report');?></a>
	                                    </li>   
	                                    <li>
	                                    <a title='<?=lang('print_confirmation_pdf')?>' target="_blank" href="<?=  site_url('safa_trip_internaltrips/pdf_export/'.$item->safa_trip_internaltrip_id).'/D'?>"><?php echo lang('print_pdf_export');?></a>
	                                    </li>
	                                	<?php } else if(session('ea_id')) { ?>
	                                
	                                	<li>
	                                    <a title='<?=lang('print_confirmation_pdf')?>' target="_blank" href="<?=  site_url('safa_trip_internaltrips/pdf_export/'.$item->safa_trip_internaltrip_id).'/D'?>"><?php echo lang('print_pdf_export');?></a>
	                                    </li>
	                                    
	                                    <?php
	                                	if($item->safa_uo_active==0) {
	                                	?>
	                                	
	                                	<li>
	                                    <a title='<?=lang('global_print')?>' target="_blank" href="<?=  site_url('safa_trip_internaltrips/internalsegments_report/'.$item->safa_trip_internaltrip_id.'/pdf/D')?>"><?php echo lang('print_internalsegments_report');?></a>
	                                    </li>
	                                    
	                                	<?php 
	                                	}
	                                	} else if(session('ito_id')) {
	                                	?>
	                                	
	                                	<!-- 
	                                	<li>
	                                    <a title='<?=lang('global_print')?>' target="_blank" href="<?=  site_url('safa_trip_internaltrips/pdf_export/'.$item->safa_trip_internaltrip_id.'/D')?>"><?php echo lang('print_pdf_export');?></a>
	                                    </li>
	                                     -->
	                                     
	                                    <li>
	                                    <a title='<?=lang('global_print')?>' target="_blank" href="<?=  site_url('safa_trip_internaltrips/confirmation_pdf/'.$item->safa_trip_internaltrip_id).'/D'?>"><?php echo lang('print_confirmation_pdf');?></a>
	                                    </li>
	                                	<?php } ?>
	                                </ul>
                        			</div> 	
	                                	
	                                	
	                                <?php 
	                                if(session('ito_id')) { 
	                                	if ($item->safa_internaltripstatus_id== 5) {
	                                	?>	
	                                	<a class='fancybox fancybox.iframe' title='<?=lang('send_trip_internaltrip_email_by_ito')?>' href="<?= site_url('safa_trip_internaltrips/send_email_by_ito_to_ea_uo_popup/' . $item->safa_trip_internaltrip_id) ?>" target="_blank" ><span class="icon-envelope"  ></span></a>
	                                <?php 
	                                	}
	                                }
	                                ?>
	                                
	                                <a href='<?php echo site_url("safa_trip_internaltrips/view/".$item->safa_trip_internaltrip_id); ?>' target="_blank"><span class='icon-search'></span></a> 
	                                	
                                </td>
                            </tr> 
                        <? } ?> 
                    <? } ?>
                </tbody>
            </table>
<!--            <? if (isset($pagination)): ?><?= $pagination ?><? endif ?>-->
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        $('.fancybox').fancybox({
            afterClose: function() {
                //parent.location.reload(true);
            }
        });
    });
</script>
<script type="text/javascript">
$(document).ready(function() {
    var config = {
        '.chosen-select': {width: "100%"},
        '.chosen-select-deselect': {allow_single_deselect: true},
        '.chosen-select-no-single': {disable_search_threshold: 10},
        '.chosen-select-no-results': {no_results_text: 'Oops, nothing found!'},
        '.chosen-select-width': {width: "95%"}
    }
    for (var selector in config) {
        $(selector).chosen(config[selector]);
    }


    // binds form submission and fields to the validation engine
    $("#safa_trip_internaltrips_search").validationEngine({
            prettySelect : true,
            useSuffix: "_chosen",
            promptPosition : "topRight:-150"
            //promptPosition : "bottomLeft"
    });


    $('.date').datepicker({dateFormat: "yy-mm-dd"});
    
});

</script>

<script>
    $(document).ready(function(){
       $("#safa_uo_contract_id").change(function() {
            $.get('<?= site_url("safa_trip_internaltrips/get_uo_contract_trips") ?>/' + $(this).val(), function(data) {
                $("#trip_id").html(data);
            });
            
        });
        var safa_uo_contract_id=$("#safa_uo_contract_id").val();
        $.get('<?= site_url("safa_trip_internaltrips/get_uo_contract_trips") ?>/' + safa_uo_contract_id, function(data) {
              $("#trip_id").html(data);
             $("#trip_id").val('<?=$this->input->get('safa_trip_id')?>')
        }); 
    });
</script>
<script>


 if ($(".fsTable").length > 0) {

	 	//By Gouda, TO remove the warning message.
	 	$.fn.dataTableExt.sErrMode = 'throw';

        $(".fsTable").dataTable({

            bSort: false,
            bAutoWidth: true,
            "iDisplayLength":50, 
            "aLengthMenu": [5, 10, 25, 50, 100, 200, 500], // can be removed for basic 10 items per page
            "bFilter": true,
            "sPaginationType": "full_numbers",
            "aoColumnDefs": [{"bSortable": false,
            "aTargets": [-1, 0]}],
			"aDataSort": [3]
            });
    }
</script>

<script>
$('.dropdown-toggle').dropdown();
</script>