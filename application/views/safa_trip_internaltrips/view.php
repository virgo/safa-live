<?php 

$safa_trip_internaltrip_id=0;  
$safa_trip_id=0;  
$safa_ito_id='';  
//$safa_uo_id='';  
//$safa_ea_id='';  
$safa_uo_contract_id='';  
$erp_flight_availability_id='';  
$erp_cruise_availability_id='';  

$safa_transporter_id='';  
$safa_internaltripstatus_id=1;  
$safa_tripstatus_id='';  
$erp_company_type_id='';  
$erp_company_id='';  
$erp_company_name='';  
$operator_reference='';  
$attachement='';  
$datetime=date('Y-m-d', time()); 

$confirmation_number=0;
if (!isset($item)) {
	//$confirmation_number= gen_itconf();
}

$ito_notes=''; 
$ea_notes='';
$trip_title='';  
$trip_supervisors='';  
$trip_supervisors_phone='';  
$adult_seats='';  
$child_seats='';  
$baby_seats=''; 
$buses_count=''; 

$erp_transportertype_id=2;

$notes=''; 

$land_going_erp_port_id='';
$land_return_erp_port_id='';
$land_going_datetime='';
$land_return_datetime='';

//$sea_going_erp_port_id='';
//$sea_return_erp_port_id='';
//$sea_going_datetime='';
//$sea_return_datetime='';

$safa_internaltrip_type_id=0;

if (isset($item)) {
	if(count($item)>0) {
		
		
		$safa_trip_internaltrip_id=$item->safa_trip_internaltrip_id ;  
		$safa_trip_id=$item->safa_trip_id ;  
		$safa_ito_id=$item->safa_ito_id ;   
		$safa_uo_contract_id=$item->safa_uo_contract_id ;  
		$erp_flight_availability_id= isset($item->erp_flight_availability_id)?$item->erp_flight_availability_id:'' ; 
		$erp_cruise_availability_id= isset($item->erp_cruise_availability_id)?$item->erp_cruise_availability_id:'';  
		$safa_transporter_id=$item->safa_transporter_id ;  
		$safa_internaltripstatus_id=$item->safa_internaltripstatus_id ;  
		$safa_tripstatus_id=$item->safa_tripstatus_id ;  
		$erp_company_type_id=$item->erp_company_type_id ;  
		$erp_company_id=$item->erp_company_id ;  
		$erp_company_name=$item->erp_company_name ;  
		$operator_reference=$item->operator_reference ;  
		$attachement=$item->attachement ;  
		$datetime=$item->datetime ;  
		$confirmation_number= $item->confirmation_number ;
		$ito_notes=$item->ito_notes ; 
		$ea_notes=$item->ea_notes ;
		$trip_title=$item->trip_title ;  
		$trip_supervisors=$item->trip_supervisors ;  
		$trip_supervisors_phone=$item->trip_supervisors_phone ;  
		$adult_seats=$item->adult_seats ;  
		$child_seats=$item->child_seats ;  
		$baby_seats=$item->baby_seats ;  
		$serial=$item->serial ;  
		$buses_count=$item->buses_count ; 
		
		$erp_transportertype_id = $item->erp_transportertype_id ;
		$safa_internaltrip_type_id = $item->safa_internaltrip_type_id ;
		
		$notes=$item->notes ;  
		
		if($erp_transportertype_id==1) {
			if (isset($internalpassages)) {
	            foreach ($internalpassages as $inernalpassage) {
		            if($inernalpassage->safa_internalsegmenttype_id == 1) {
		            	$land_going_erp_port_id=$inernalpassage->erp_port_id;
		            	$land_going_datetime=$inernalpassage->start_datetime;
		            } else if($inernalpassage->safa_internalsegmenttype_id == 2) {
						$land_return_erp_port_id=$inernalpassage->erp_port_id;
						$land_return_datetime=$inernalpassage->start_datetime;
		            }
	            }
			}
		} 
		
	}
}

?>

<div id="dv_internaltrip_manage_storage">

<div class="row-fluid" >
    <div class="widget">
        <div class="path-container Fright">
            <div class="icon"><i class="icos-pencil2"></i></div> 
            <div class="path-name Fright">
               <a href="<?php 
            if(session('uo_id')) {
            	echo  site_url().'uo/dashboard'; 
            } else if(session('ea_id')) {
            	echo  site_url().'ea/dashboard';
            } else if(session('ito_id')) {
            	echo  site_url().'ito/dashboard';
            } else {
            	echo  site_url();
            }
            ?>"><?php echo  lang('global_system_management') ?></a>
            </div>    
            <div class="path-arrow Fright">
            </div>
            <div class="path-name Fright">
                <a href="<?= site_url('safa_trip_internaltrips') ?>"><?= lang('safa_trip_internaltrips') ?></a>
            </div>
            <div class="path-arrow Fright">
            </div>
            <div class="path-name Fright">
                <span><?= lang('view_safa_trip_internaltrips') ?></span>
            </div>
        </div>     
    </div>

    <div class="widget">
        <div class="widget-header">
            <div class="widget-header-icon Fright">
                <span class="icos-pencil2"></span>
            </div>
            <div class="widget-header-title Fright">
                <?= lang('view_safa_trip_internaltrips') ?>
            </div>
        </div>

        <?= form_open_multipart(false, 'id="frm_view_trip_internaltrip" ') ?>
        <input type='hidden' name='hdn_safa_trip_internaltrip_id'  id='hdn_safa_trip_internaltrip_id'  value='<?php echo $safa_trip_internaltrip_id;?>'/>
        <input type='hidden' name='hdn_safa_trip_id'  id='hdn_safa_trip_id'  value='<?php echo $safa_trip_id;?>'/>
                                            
        <div class="block-fluid">
            <div class="row-form" >
                <div class="span6" >
                    <div class="span4">
                        <?= lang("transport_request_date") ?>
                    </div>
                    <div class="span8"> 
                            <?= $datetime ?>
                    </div>


                </div>
                <div class="span6" >
                    <div class="span4"><?= lang("transport_request_contract") ?>
                    </div>
                    <div class="span8">
                        <?php if($safa_uo_contract_id!='') {?>
		                <?php echo item("safa_uo_contracts", name(), array("safa_uo_contract_id" => $safa_uo_contract_id)); ?>
		        		<?php }?>
                        
                    </div>
                </div>
            </div>
            <div class="row-form" >
                
                <div class="span12" >
                    <div class="span2"><?= lang("trip_code") ?>
                    </div>
                    <div class="span10">
                        
                        <?= $safa_uo_code ?>
                        - 
                        <?= $serial ?>
                    </div>
                </div>
                
                
            </div>
            <div class="row-form" >
                <div class="span6" >
                    <div class="span4"><?= lang("transport_trip_supervisors") ?>
                    </div>
                    <div class="span8">
                        <?=  $trip_supervisors ?>
                    </div>
                </div>
                <div class="span6" >
                    <div class="span4">
                        <?= lang("transport_trip_supervisors_phone") ?>
                    </div>
                    <div class="span8" >
                        <?= $trip_supervisors_phone ?>
                    </div>
                </div>
            </div>
            <div class="row-form" >
                <div class="span6" >
                    <div class="span4"><?= lang("transport_request_status") ?>
                    </div>
                    <div class="span8">
                        <?php if($safa_internaltripstatus_id!='') {?>
		                <?php echo item("safa_internaltripstatus", name(), array("safa_internaltripstatus_id" => $safa_internaltripstatus_id)); ?>
		        		<?php }?>
                    </div>
                </div>
                
            
            </div>
                
			<div class="row-form" >
                <div class="span12"><?= lang("travellers_seats") ?></div>
                <div class="clear clearfix"></div>
                <div class="span4" >
                    <div class="span4" ><?= lang("adult_seats") ?>
                    </div>
                    <div class="span8">
                        <?= $adult_seats ?>
                      
                    </div> 
                </div>
                <div class="span4" >
                    <div class="span4" ><?= lang('child_seats') ?></div>
                    <div class="span8" >
                        <?=  $child_seats ?>
                       
                    </div>
                </div>
                <div class="span3" >
                    <div class="span4" ><?= lang('baby_seats') ?></div>
                    <div class="span8" >

                        <?=  $baby_seats ?>
                        
                    </div>
                </div>
            </div>
            
            <div class="row-form" >
                <div class="span12" >
                    <div class="span2"><?= lang("notes") ?>
                    </div>
                    <div class="span10">
                        <?=  $notes ?>
                    </div>
                </div>
                
            </div>
            
            <div class="row-form">
             <div class="span12"></div>
                <div class="clear clearfix"></div>
                
                        <div class="span6">
                            <div class="span4"><?= lang('erp_transportertype_id') ?></div>
                            <div class="span8">
                                
                                <?php if($erp_transportertype_id!='') {?>
		                        <?php echo item("erp_transportertypes", name(), array("erp_transportertype_id" => $erp_transportertype_id)); ?>
		        				<?php }?>
                            </div>
                   	</div>
            </div>
            
        </div>

		<div class="row-fluid" id="div_land_trip" <?php if ($erp_transportertype_id != 1 && $erp_transportertype_id !='' ) { ?> style="display: none;" <?php } else { ?> style="display: inline-block;" <?php } ?>>
            <div class="resalt-group">
                <div class="wizerd-div"><a><?= lang('land_trip') ?></a></div>
                
                <div class="row-form" >
                <div class="span6" >
                    <div class="span4"><?= lang("land_going_erp_port_id") ?>
                    </div>
                    <div class="span8">
                       <?php if($land_going_erp_port_id!='') {?>
                        <?php echo item("erp_ports", name(), array("erp_port_id" => $land_going_erp_port_id)); ?>
        				<?php }?>
                    </div>
                </div>
                <div class="span6">
                    <div class="span4">
                        <?= lang("land_going_datetime") ?>
                    </div>
                    <div class="span8" >
                        <?=  $land_going_datetime ?>
                        
                    </div>
                </div>
            </div>
            
            
            <div class="row-form" >
                <div class="span6" >
                    <div class="span4"><?= lang("land_return_erp_port_id") ?>
                    </div>
                    <div class="span8">                        
                        <?php if($land_return_erp_port_id!='') {?>
                        <?php echo item("erp_ports", name(), array("erp_port_id" => $land_return_erp_port_id)); ?>
        				<?php }?>
                    </div>
                </div>
                <div class="span6">
                    <div class="span4">
                        <?= lang("land_return_datetime") ?>
                    </div>
                    <div class="span8" >
                        <?= $land_return_datetime ?>
                    </div>
                </div>
            </div>
                
            </div>
        </div>
        
        <div class="row-fluid" id="div_sea_trip" <?php if ($erp_transportertype_id != 3) { ?> style="display: none;" <?php } else { ?> style="display: inline-block;" <?php } ?>>

            <div class="resalt-group">
                <div class="wizerd-div"><a><?= lang('sea_trip') ?></a></div>
                <div class="label"><?= lang('going') ?></div>
                <table id='div_trip_going_group'>
                    <thead>
                        <tr>
                            <th><?= lang('erp_path_type_id') ?></th>
                            <th><?= lang('cruise_availabilities_lines') ?></th>
                            <th><?= lang('cruise_availabilities_trip_number') ?></th>
                            <th><?= lang('cruise_availabilities_date') ?></th>
                            <th><?= lang('cruise_availabilities_arrival_date') ?></th>
                            <th><?= lang('cruise_availabilities_ports') ?></th>
                            <th><?= lang('cruise_availabilities_seat_count') ?></th>
                        </tr>
                    </thead>
                    <tbody id="going_cruise_tbody" class="cls_tbl_cruises">



                        <? if (isset($cruise_availabilities_details_rows)) { 
                        	
                            
                                foreach ($cruise_availabilities_details_rows as $cruise_availabilities_details_row) {

                                        $erp_port_id_from = $cruise_availabilities_details_row->erp_port_id_from;

                                        $erp_port_id_to = $cruise_availabilities_details_row->erp_port_id_to;

                                        $start_datetime = $cruise_availabilities_details_row->cruise_date . ' - ' . $cruise_availabilities_details_row->departure_time;
                                        $end_datetime = $cruise_availabilities_details_row->cruise_date . ' - ' . $cruise_availabilities_details_row->arrival_time;
                                    
                                    

                                    if ($cruise_availabilities_details_row->safa_externaltriptype_id == 1) {
                                        ?>



                                        <tr rel='<?= $cruise_availabilities_details_row->erp_cruise_availabilities_detail_id ?>'>

                                            <td>
                                            
                                            <input type='hidden' name='erp_cruise_availabilities_detail_id[<?= $cruise_availabilities_details_row->erp_cruise_availabilities_detail_id ?>]'  id='erp_cruise_availabilities_detail_id[<?= $cruise_availabilities_details_row->erp_cruise_availabilities_detail_id ?>]'  value='<?= $cruise_availabilities_details_row->erp_cruise_availabilities_detail_id ?>'/>
                                            
                                            <input type='hidden' name='safa_externaltriptype_name_<?= $cruise_availabilities_details_row->erp_cruise_availabilities_detail_id ?>'  id='safa_externaltriptype_name_<?= $cruise_availabilities_details_row->erp_cruise_availabilities_detail_id ?>'  value='<?= $cruise_availabilities_details_row->erp_path_type_name ?>'/>
                                            <input type='hidden' name='safa_externaltriptype_id[<?= $cruise_availabilities_details_row->erp_cruise_availabilities_detail_id ?>]'  id='safa_externaltriptype_id[<?= $cruise_availabilities_details_row->erp_cruise_availabilities_detail_id ?>]'  value='<?= $cruise_availabilities_details_row->safa_externaltriptype_id ?>'/>
                                            
                                            <input type="hidden" pathrel='<?php echo $cruise_availabilities_details_row->erp_cruise_availabilities_detail_id; ?>' class="erp_path_type" name="erp_path_type_id[<?= $cruise_availabilities_details_row->erp_cruise_availabilities_detail_id ?>]" id="erp_path_type_id[<?= $cruise_availabilities_details_row->erp_cruise_availabilities_detail_id ?>]"  value="<?= $cruise_availabilities_details_row->erp_path_type_id ?>" />
											<?php echo  $cruise_availabilities_details_row->erp_path_type_name;?>											
                                            </td>

                                            
											
											 
											<td>
											<?php echo  $cruise_availabilities_details_row->safa_transporter_code;?>											
                                            
											</td>
											<td>
											<?php echo  $cruise_availabilities_details_row->cruise_number;?>											
                                            
								             </td>

                                            <td>

                                               <?= $cruise_availabilities_details_row->cruise_date . ' ' . get_time($cruise_availabilities_details_row->departure_time) ?>
                                        	
                                            </td>


											<td>

                                			<?= $cruise_availabilities_details_row->arrival_date . ' ' . get_time($cruise_availabilities_details_row->arrival_time) ?>
                                			
                                        	
                                            </td>
                                            
                                            <td>				
												
                                                <?= $cruise_availabilities_details_row->start_ports_name ?> - <?= $cruise_availabilities_details_row->end_ports_name ?>

                                            </td>

                                            <td>
                                                <input type='hidden' name='seats_count[<?= $cruise_availabilities_details_row->erp_cruise_availabilities_detail_id ?>]'  id='seats_count[<?= $cruise_availabilities_details_row->erp_cruise_availabilities_detail_id ?>]'  value='<?= $cruise_availabilities_details_row->seats_count ?>'/>
                                                <?= $cruise_availabilities_details_row->seats_count ?>


                                            </td>

         
											</tr>


                        <?php
                                    }
                                }
                                
                        } 
                        ?>

                    </tbody>

                </table>
                <div class="clear clearfix" style="height: 5px;"> </div>
                <div class="label clear"><?= lang('return') ?></div>
                <table id='div_trip_return_group'>
                    <thead>
                        <tr>
                            <th><?= lang('erp_path_type_id') ?></th>
                            <th><?= lang('cruise_availabilities_lines') ?></th>
                            <th><?= lang('cruise_availabilities_trip_number') ?></th>
                            <th><?= lang('cruise_availabilities_date') ?></th>
                            <th><?= lang('cruise_availabilities_arrival_date') ?></th>
                            <th><?= lang('cruise_availabilities_ports') ?></th>
                            <th><?= lang('cruise_availabilities_seat_count') ?></th>


                        </tr>
                    </thead>
                    <tbody id="return_cruise_tbody" class="cls_tbl_cruises">

                        <? if (isset($cruise_availabilities_details_rows)) { 
                        	
                                foreach ($cruise_availabilities_details_rows as $cruise_availabilities_details_row) {
                                        
                                	$erp_port_id_from = $cruise_availabilities_details_row->erp_port_id_from;
                                    $erp_port_id_to = $cruise_availabilities_details_row->erp_port_id_to;
                                    $start_datetime = $cruise_availabilities_details_row->cruise_date . ' - ' . $cruise_availabilities_details_row->departure_time;
                                    $end_datetime = $cruise_availabilities_details_row->cruise_date . ' - ' . $cruise_availabilities_details_row->arrival_time;
                                    
                                    

                                    if ($cruise_availabilities_details_row->safa_externaltriptype_id == 2) {
                                        ?>



                                        <tr rel='<?= $cruise_availabilities_details_row->erp_cruise_availabilities_detail_id ?>'>

                                            <td>
                                            
                                            <input type='hidden' name='erp_cruise_availabilities_detail_id[<?= $cruise_availabilities_details_row->erp_cruise_availabilities_detail_id ?>]'  id='erp_cruise_availabilities_detail_id[<?= $cruise_availabilities_details_row->erp_cruise_availabilities_detail_id ?>]'  value='<?= $cruise_availabilities_details_row->erp_cruise_availabilities_detail_id ?>'/>
                                            
                                            <input type='hidden' name='safa_externaltriptype_name_<?= $cruise_availabilities_details_row->erp_cruise_availabilities_detail_id ?>'  id='safa_externaltriptype_name_<?= $cruise_availabilities_details_row->erp_cruise_availabilities_detail_id ?>'  value='<?= $cruise_availabilities_details_row->erp_path_type_name ?>'/>
                                            <input type='hidden' name='safa_externaltriptype_id[<?= $cruise_availabilities_details_row->erp_cruise_availabilities_detail_id ?>]'  id='safa_externaltriptype_id[<?= $cruise_availabilities_details_row->erp_cruise_availabilities_detail_id ?>]'  value='<?= $cruise_availabilities_details_row->safa_externaltriptype_id ?>'/>
                                            
                                            <input type="hidden" pathrel='<?php echo $cruise_availabilities_details_row->erp_cruise_availabilities_detail_id; ?>' class="erp_path_type" name="erp_path_type_id[<?= $cruise_availabilities_details_row->erp_cruise_availabilities_detail_id ?>]" id="erp_path_type_id[<?= $cruise_availabilities_details_row->erp_cruise_availabilities_detail_id ?>]"  value="<?= $cruise_availabilities_details_row->erp_path_type_id ?>" />
											<?php echo  $cruise_availabilities_details_row->erp_path_type_name;?>		
											
													
                                            </td>

                                            
											
											 
											<td>
											<?php echo  $cruise_availabilities_details_row->safa_transporter_code;?>											
                                            
											</td>
											<td>
											<?php echo  $cruise_availabilities_details_row->cruise_number;?>											
                                            </td>
                                            
                                            <td>

                                               <?= $cruise_availabilities_details_row->cruise_date . ' ' . get_time($cruise_availabilities_details_row->departure_time) ?>
                                        	
                                            </td>


											<td>

                                			<?= $cruise_availabilities_details_row->arrival_date . ' ' . get_time($cruise_availabilities_details_row->arrival_time) ?>
                                			
                                        	
                                            </td>
                                            
                                            <td>				
												
                                                <?= $cruise_availabilities_details_row->start_ports_name ?> - <?= $cruise_availabilities_details_row->end_ports_name ?>

                                            </td>

                                            <td>
                                                <input type='hidden' name='seats_count[<?= $cruise_availabilities_details_row->erp_cruise_availabilities_detail_id ?>]'  id='seats_count[<?= $cruise_availabilities_details_row->erp_cruise_availabilities_detail_id ?>]'  value='<?= $cruise_availabilities_details_row->seats_count ?>'/>
                                                <?= $cruise_availabilities_details_row->seats_count ?>


                                            </td>

         
											</tr>


                                        <?php
                                    }
                                }
                          } 
                          ?>

                    </tbody>

                </table>
            </div> 

            
            
        </div>
        
        <div class="row-fluid" id="div_tbl_filghts" <?php if ($erp_transportertype_id != 2) { ?> style="display: none;" <?php } else { ?> style="display: inline-block;" <?php } ?>>
            <div class="resalt-group">
                <div class="wizerd-div"><a><?= lang('flight_trip') ?></a></div>
                <div class="label"><?= lang('going') ?></div>
                <table id='div_trip_going_group'>
                    <thead>
                        <tr>
                            <th><?= lang('erp_path_type_id') ?></th>
                            <th><?= lang('flight_availabilities_airlines') ?></th>
                            <th><?= lang('flight_availabilities_flight_number') ?></th>
                            <th><?= lang('flight_availabilities_date') ?></th>
                            <th><?= lang('arrival_date') ?></th>
                            <th><?= lang('flight_availabilities_airports') ?></th>
                            <th><?= lang('flight_availabilities_seat_count') ?></th>

                        </tr>
                    </thead>
                    <tbody id="going_flight_tbody" class="cls_tbl_filghts">



                        <? if (isset($going_trip_flights_rows)) { ?>
                            <?
                            foreach ($going_trip_flights_rows as $value) {
                                $this->flight_availabilities_model->erp_flight_availability_id = $value->erp_flight_availability_id;

                                //$this->flight_availabilities_model->erp_path_type_id = $flight_trip_type;

                                $flight_availabilities_details_rows = $this->flight_availabilities_model->get_details_for_trip();

                                $ports = '';

                                $safa_externaltriptype_name = '';

                                $start_datetime = '';
                                $end_datetime = '';
                                $loop_counter = 0;

                                $erp_port_id_from = 0;
                                $erp_port_id_to = 0;
                                $start_ports_name = '';
                                $end_ports_name = '';
                                ?>


                                <?php
                                foreach ($flight_availabilities_details_rows as $flight_availabilities_details_row) {
                                    if ($loop_counter == 0) {
                                        $safa_externaltriptype_name = $flight_availabilities_details_row->safa_externaltriptype_name;
                                        $erp_path_type_name = $flight_availabilities_details_row->erp_path_type_name;

                                        $erp_port_id_from = $flight_availabilities_details_row->erp_port_id_from;
                                        $start_ports_name = $flight_availabilities_details_row->start_ports_name;

                                        $erp_port_id_to = $flight_availabilities_details_row->erp_port_id_to;
                                        $end_ports_name = $flight_availabilities_details_row->end_ports_name;

                                        $ports = $flight_availabilities_details_row->start_ports_name . ' - ' . $flight_availabilities_details_row->end_ports_name;
                                        $start_datetime = $flight_availabilities_details_row->flight_date . ' - ' . $flight_availabilities_details_row->departure_time;
                                        $end_datetime = $flight_availabilities_details_row->flight_date . ' - ' . $flight_availabilities_details_row->arrival_time;
                                    } else {
                                        $safa_externaltriptype_name = $safa_externaltriptype_name . ' <br/> ' . $flight_availabilities_details_row->safa_externaltriptype_name;

                                        $erp_port_id_to = $flight_availabilities_details_row->erp_port_id_to;
                                        $end_ports_name = $flight_availabilities_details_row->end_ports_name;

                                        $erp_port_id_from = $flight_availabilities_details_row->erp_port_id_from;
                                        $start_ports_name = $flight_availabilities_details_row->start_ports_name;
                                        
                                        $ports = $ports . ' <br/> ' . $flight_availabilities_details_row->start_ports_name . ' - ' . $flight_availabilities_details_row->end_ports_name;
                                        $end_datetime = $flight_availabilities_details_row->flight_date . ' - ' . $flight_availabilities_details_row->arrival_time;
                                    }
                                    
                                    $erp_port_hall_id_from = $flight_availabilities_details_row->erp_port_hall_id_from;
                                    $erp_port_hall_id_to = $flight_availabilities_details_row->erp_port_hall_id_to;
                                    
                                    $loop_counter++;

                                    if ($flight_availabilities_details_row->safa_externaltriptype_id == 1) {
                                        ?>



                                        <tr rel='<?= $flight_availabilities_details_row->erp_flight_availabilities_detail_id ?>'>

                                                                                <!--<td><?php echo $flight_availabilities_details_row->safa_externaltriptype_name; ?>
                                                                                 <input type='hidden' name='safa_externaltriptype_name_<?= $flight_availabilities_details_row->erp_flight_availabilities_detail_id ?>'  id='safa_externaltriptype_name_<?= $flight_availabilities_details_row->erp_flight_availabilities_detail_id ?>'  value='<?= $flight_availabilities_details_row->safa_externaltriptype_name ?>'/>
                                                                                </td>

                                            --><td>
                                            
                                            <input type='hidden' name='erp_flight_availabilities_detail_id[<?= $flight_availabilities_details_row->erp_flight_availabilities_detail_id ?>]'  id='erp_flight_availabilities_detail_id[<?= $flight_availabilities_details_row->erp_flight_availabilities_detail_id ?>]'  value='<?= $flight_availabilities_details_row->erp_flight_availabilities_detail_id ?>'/>
                                            
                                            <?php echo $flight_availabilities_details_row->erp_path_type_name; ?>
                                                <input type='hidden' name='safa_externaltriptype_name_<?= $flight_availabilities_details_row->erp_flight_availabilities_detail_id ?>'  id='safa_externaltriptype_name_<?= $flight_availabilities_details_row->erp_flight_availabilities_detail_id ?>'  value='<?= $flight_availabilities_details_row->erp_path_type_name ?>'/>
                                               <input type='hidden' name='safa_externaltriptype_id[<?= $flight_availabilities_details_row->erp_flight_availabilities_detail_id ?>]'  id='safa_externaltriptype_id[<?= $flight_availabilities_details_row->erp_flight_availabilities_detail_id ?>]'  value='<?= $flight_availabilities_details_row->safa_externaltriptype_id ?>'/>
                                            
                                            <input type="hidden" pathrel='<?php echo $flight_availabilities_details_row->erp_flight_availabilities_detail_id; ?>' class="erp_path_type" name="erp_path_type_id[<?= $flight_availabilities_details_row->erp_flight_availabilities_detail_id ?>]" id="erp_path_type_id[<?= $flight_availabilities_details_row->erp_flight_availabilities_detail_id ?>]"  value="<?= $flight_availabilities_details_row->erp_path_type_id ?>" />
											
                                            </td>

                                            
											
											 
											<td>
											
											<?= $flight_availabilities_details_row->safa_transporter_code ?>
											
											
											</td>
											
								             <td>
								             <?=  $flight_availabilities_details_row->flight_number ?>
								             </td>
								             

                                            <td>

                                                <?= $flight_availabilities_details_row->flight_date . ' ' . get_time($flight_availabilities_details_row->departure_time) ?>
                                        	
                                            </td>


											<td>

                                			<?= $flight_availabilities_details_row->arrival_date . ' ' . get_time($flight_availabilities_details_row->arrival_time) ?>
                                        	
                                            </td>
                                            
                                            <td>

                                                <?= $flight_availabilities_details_row->start_ports_name ?> - <?= $flight_availabilities_details_row->end_ports_name ?> ( <?php if($flight_availabilities_details_row->erp_port_hall_id_to!='') {echo item("erp_port_halls", name(), array("erp_port_hall_id" => $flight_availabilities_details_row->erp_port_hall_id_to));} ?> )

                                            </td>

                                            <td>
                                                
                                                 <?= $value->available_seats_count ?> 

                                            </td>

         
											</tr>


                                        <?php
                                    }
                                }
                                ?>



                            <? } ?>
                        <? } ?>

                    </tbody>

                </table>
                <div class="clear clearfix" style="height: 5px;"> </div>
                <div class="label clear"><?= lang('return') ?></div>
                <table id='div_trip_return_group'>
                    <thead>
                        <tr>
                            <th><?= lang('erp_path_type_id') ?></th>
                            <th><?= lang('flight_availabilities_airlines') ?></th>
                            <th><?= lang('flight_availabilities_flight_number') ?></th>
                            <th><?= lang('flight_availabilities_date') ?></th>
                            <th><?= lang('arrival_date') ?></th>
                            <th><?= lang('flight_availabilities_airports') ?></th>
                            <th><?= lang('flight_availabilities_seat_count') ?></th>


                        </tr>
                    </thead>
                    <tbody id="return_flight_tbody" class="cls_tbl_filghts">

                        <? if (isset($going_trip_flights_rows)) { ?>
                            <?
                            foreach ($going_trip_flights_rows as $value) {
                                $this->flight_availabilities_model->erp_flight_availability_id = $value->erp_flight_availability_id;

                                //$this->flight_availabilities_model->erp_path_type_id = $flight_trip_type;

                                $flight_availabilities_details_rows = $this->flight_availabilities_model->get_details_for_trip();

                                $ports = '';

                                $safa_externaltriptype_name = '';

                                $start_datetime = '';
                                $end_datetime = '';
                                $loop_counter = 0;

                                $erp_port_id_from = 0;
                                $erp_port_id_to = 0;
                                $start_ports_name = '';
                                $end_ports_name = '';
                                ?>


                                <?php
                                foreach ($flight_availabilities_details_rows as $flight_availabilities_details_row) {
                                    if ($loop_counter == 0) {
                                        $safa_externaltriptype_name = $flight_availabilities_details_row->safa_externaltriptype_name;
                                        $erp_path_type_name = $flight_availabilities_details_row->erp_path_type_name;

                                        $erp_port_id_from = $flight_availabilities_details_row->erp_port_id_from;
                                        $start_ports_name = $flight_availabilities_details_row->start_ports_name;

                                        $erp_port_id_to = $flight_availabilities_details_row->erp_port_id_to;
                                        $end_ports_name = $flight_availabilities_details_row->end_ports_name;

                                        $ports = $flight_availabilities_details_row->start_ports_name . ' - ' . $flight_availabilities_details_row->end_ports_name;
                                        $start_datetime = $flight_availabilities_details_row->flight_date . ' - ' . $flight_availabilities_details_row->departure_time;
                                        $end_datetime = $flight_availabilities_details_row->flight_date . ' - ' . $flight_availabilities_details_row->arrival_time;
                                    } else {
                                        $safa_externaltriptype_name = $safa_externaltriptype_name . ' <br/> ' . $flight_availabilities_details_row->safa_externaltriptype_name;

                                        $erp_port_id_from = $flight_availabilities_details_row->erp_port_id_from;
                                        $start_ports_name = $flight_availabilities_details_row->start_ports_name;
                                        
                                        $erp_port_id_to = $flight_availabilities_details_row->erp_port_id_to;
                                        $end_ports_name = $flight_availabilities_details_row->end_ports_name;

                                        $ports = $ports . ' <br/> ' . $flight_availabilities_details_row->start_ports_name . ' - ' . $flight_availabilities_details_row->end_ports_name;
                                        $end_datetime = $flight_availabilities_details_row->flight_date . ' - ' . $flight_availabilities_details_row->arrival_time;
                                    }
                                    
                                    $erp_port_hall_id_from = $flight_availabilities_details_row->erp_port_hall_id_from;
                                    $erp_port_hall_id_to = $flight_availabilities_details_row->erp_port_hall_id_to;
                                    
                                    $loop_counter++;

                                    if ($flight_availabilities_details_row->safa_externaltriptype_id == 2) {
                                        ?>



                                        <tr  rel='<?= $flight_availabilities_details_row->erp_flight_availabilities_detail_id ?>'>

                                <!--<td><?php echo $flight_availabilities_details_row->safa_externaltriptype_name; ?>
                                 <input type='hidden' name='safa_externaltriptype_name_<?= $flight_availabilities_details_row->erp_flight_availabilities_detail_id ?>'  id='safa_externaltriptype_name_<?= $flight_availabilities_details_row->erp_flight_availabilities_detail_id ?>'  value='<?= $flight_availabilities_details_row->safa_externaltriptype_name ?>'/>
                                </td>

                                            --><td>
                                            
                                            	<input type='hidden' name='erp_flight_availabilities_detail_id[<?= $flight_availabilities_details_row->erp_flight_availabilities_detail_id ?>]'  id='erp_flight_availabilities_detail_id[<?= $flight_availabilities_details_row->erp_flight_availabilities_detail_id ?>]'  value='<?= $flight_availabilities_details_row->erp_flight_availabilities_detail_id ?>'/>
                                            
                                                <input type="hidden" name="trip_going_erp_flight_availability_id<?= $flight_availabilities_details_row->erp_flight_availabilities_detail_id ?>" id="trip_going_erp_flight_availability_id<?= $flight_availabilities_details_row->erp_flight_availabilities_detail_id ?>"  value="<?= $flight_availabilities_details_row->erp_flight_availabilities_detail_id ?>" />
												
												
                                                <?php echo $flight_availabilities_details_row->erp_path_type_name; ?>
                                                <input type='hidden' name='safa_externaltriptype_name_<?= $flight_availabilities_details_row->erp_flight_availabilities_detail_id ?>'  id='safa_externaltriptype_name_<?= $flight_availabilities_details_row->erp_flight_availabilities_detail_id ?>'  value='<?= $flight_availabilities_details_row->erp_path_type_name ?>'/>
                                                <input type='hidden' name='safa_externaltriptype_id[<?= $flight_availabilities_details_row->erp_flight_availabilities_detail_id ?>]'  id='safa_externaltriptype_id[<?= $flight_availabilities_details_row->erp_flight_availabilities_detail_id ?>]'  value='<?= $flight_availabilities_details_row->safa_externaltriptype_id ?>'/>
                                                
                                            
											
                                            </td>

                                            
											
											 
											<td>
											
											<?= $flight_availabilities_details_row->safa_transporter_code ?>
											
											
											</td>
											
								             <td>
								             <?=  $flight_availabilities_details_row->flight_number ?>
								             </td>
								             

                                            <td>

                                                <?= $flight_availabilities_details_row->flight_date . ' ' . get_time($flight_availabilities_details_row->departure_time) ?>
                                        	
                                            </td>


											<td>

                                			<?= $flight_availabilities_details_row->arrival_date . ' ' . get_time($flight_availabilities_details_row->arrival_time) ?>
                                        	
                                            </td>


                                            <td>
                                                <?= $flight_availabilities_details_row->start_ports_name ?> ( <?php if($flight_availabilities_details_row->erp_port_hall_id_from!='') {echo item("erp_port_halls", name(), array("erp_port_hall_id" => $flight_availabilities_details_row->erp_port_hall_id_from));} ?> ) - <?= $flight_availabilities_details_row->end_ports_name ?>
                                                
                                            </td>


                                            <td>
                                    
                                                 <?= $value->available_seats_count ?> 

                                            </td>


         
											</tr>


                                        <?php
                                    }
                                }
                                ?>



                            <? } ?>
                        <? } ?>

                    </tbody>

                </table>
            </div>      
        </div>

		

        <div class="row-fluid" id="div_tbl_hotels">
            <fieldset class="resalt-group">
                <div class="wizerd-div"><a> <?= lang('hotels') ?></a></div>
                
                <?php 
                $safa_reservation_form_ids='';
                
                //By Gouda.
                //if(session('ea_id')&&1==0) {
                if(session('ea_id') || session('uo_id')) {
                ?>
                
                <?php 
                		if(isset($item_safa_reservation_forms_rows)) {
                			if(count($item_safa_reservation_forms_rows)>0) {
                			echo " <table> <tr> <td colspan='5' align='center'> <span class='FRight'>".lang('accommodation_forms')."</span> </td></tr>";
                	?>
                	
                	<tr>
                	<td><?php echo lang('trip_code');?></td>
                	<td><?php echo lang('period');?></td>
                	<td><?php echo lang('the_hotels');?></td>
                	<td><?php echo lang('passports_count');?></td>
                	</tr>
                	
                <?php		
		                $row_class = '';
						$previous_row_class = '';
						
						$item_safa_reservation_forms_colors_arr=array();
						
                		foreach($item_safa_reservation_forms_rows as $item_safa_reservation_forms_row) {
                			$safa_reservation_form_ids = $safa_reservation_form_ids.','.$item_safa_reservation_forms_row->safa_reservation_form_id;
                			
                			$this->safa_passport_accommodation_rooms_model->safa_reservation_form_id = $item_safa_reservation_forms_row->safa_reservation_form_id;
							$accommodation_hotels_rows = $this->safa_passport_accommodation_rooms_model->get_hotels();
	                		
							$accommodation_hotels_names='';
							foreach($accommodation_hotels_rows as $accommodation_hotels_row) {
								$accommodation_hotels_names = $accommodation_hotels_names.$accommodation_hotels_row->erp_hotel_name." ( ".$accommodation_hotels_row->erp_city_name." )<br/>";
							}
							
							$safa_group_passport_accommodation_rows = $this->safa_group_passport_accommodation_model->get_distinct_passports_by_trip_uo_form(false, false, $item_safa_reservation_forms_row->safa_reservation_form_id);
							
							$safa_group_passport_accommodation_count = count($safa_group_passport_accommodation_rows);
							
							
							//---------------- Colors ----------------------------------
							$row_class_arr = array('success','info','warning');
							$row_class = $row_class_arr[array_rand($row_class_arr)];
							if($previous_row_class==$row_class) {
								$row_class='';
							}
							$previous_row_class = $row_class;
							
							$item_safa_reservation_forms_colors_arr[$item_safa_reservation_forms_row->safa_reservation_form_id]= $row_class;
                ?>	
                
                        
                    <tr>
                	<td class="<?php echo $row_class;?>"><?php echo $item_safa_reservation_forms_row->safa_trip_name.' / '. $item_safa_reservation_forms_row->safa_package_name; ?></td>
                	<td class="<?php echo $row_class;?>"><?php echo $item_safa_reservation_forms_row->safa_package_period_name; ?></td>
                	<td class="<?php echo $row_class;?>"><?php echo $accommodation_hotels_names; ?></td>
                	<td class="<?php echo $row_class;?>"><?php echo $safa_group_passport_accommodation_count; ?></td>
                	
                	</tr>
                	
                	<?php	
                			}
                			echo " </table>";
                		}
                		}
                		
                	//}
	                } 
				?>
				<input type='hidden' name='safa_reservation_form_ids'  id='safa_reservation_form_ids'  value='<?php echo $safa_reservation_form_ids;?>'/>
        		
                <table id='div_hotels'>
                    <thead>
                    <tr><th colspan="6"><?= lang('the_hotels')   ?></th></tr>
                        <tr>
                            <th><?= lang('city')   ?></th>
                            <th><?= lang('hotel') ?></th>
                            <th><?= lang('entry_date')  ?></th>
                            <th><?= lang('nights_count') ?></th>
                            <th><?= lang('exit_date')  ?>
	                        </th>
                        </tr>
                    </thead>
                    <tbody class="internalhotels">
                    
                                       
					<?php 
					$internalpassages_hotels_count = 0;
					
					$internalpassages_hotels_counter=1;
					if((session('uo_id')&& count($item_safa_reservation_forms_rows)==0) || session('ito_id') || session('gov_id') || (session('ea_id') && count($item_safa_reservation_forms_rows)==0)) {
					if(isset($internalpassages_hotels)){
						$internalpassages_hotels_count = count($internalpassages_hotels);
											
								
						foreach($internalpassages_hotels as $internalpassages_hotel) {
							
							   
							$next_internalpassages_row = array();
							if(isset($internalpassages_hotels[$internalpassages_hotels_counter])) {
								$next_internalpassages_row = $internalpassages_hotels[$internalpassages_hotels_counter];
							}
							
					?>
					
					<tr hrel="<?php echo $internalpassages_hotel->safa_internalsegment_id ?>">
                                    <td>
                                    <input type='hidden' name='hotel_safa_internalsegment_id[<?= $internalpassages_hotel->safa_internalsegment_id ?>]'  id='hotel_safa_internalsegment_id[<?= $internalpassages_hotel->safa_internalsegment_id ?>]'  value='<?php echo $internalpassages_hotel->safa_internalsegment_id;?>'/>
                                    
            						
            						<?php echo $internalpassages_hotel->erp_city_name; ?>
            						</td>
                                    <td>
                                        <?php echo $internalpassages_hotel->erp_hotel_name; ?>
                                    </td>
                                 
                                    <td>
                                    
                                         
                                        <?php echo date('Y-m-d',strtotime($internalpassages_hotel->end_datetime)); ?>
            						
										</td>
										
										<td>
                                        <?php 
                                        
                                        
                                        $nights_count=0;
                                        if(count($next_internalpassages_row)>0) {
	                                        if (strlen($internalpassages_hotel->end_datetime) && strlen($next_internalpassages_row->end_datetime)) {
	                                        			$enddate = explode(' ',$next_internalpassages_row->end_datetime);
                                                        $startdate = explode(' ',$internalpassages_hotel->end_datetime);
                                                        
	                                             $nights_count = ceil((strtotime($enddate[0]) - strtotime($startdate[0])) / (60 * 60 * 24)); 
	                                        } 
                                        } else {
                                        	$this->internalpassages_model->safa_internalsegmenttype_id = 2;
                            				$leaving_internalpassages_rows = $this->internalpassages_model->get();
                                                        $enddate = explode(' ',$leaving_internalpassages_rows[0]->end_datetime);
                                                        $startdate = explode(' ',$internalpassages_hotel->end_datetime);
                            				if(count($leaving_internalpassages_rows)>0) {
                            					$nights_count = ceil((strtotime($enddate[0]) - strtotime($startdate[0])) / (60 * 60 * 24));
                            				} 
                                        }
                                         ?>
                                         
                                         <?php echo $nights_count; ?>
            						
                                    	</td>
										
										<td>
										
										<?php 
										$current_row_exit_date='';
                                        if(count($next_internalpassages_row)>0) {
                                        	$current_row_exit_date = date('Y-m-d',strtotime($next_internalpassages_row->end_datetime));
										} else if(isset($leaving_internalpassages_rows)) {
											if(count($leaving_internalpassages_rows)>0) {
												$current_row_exit_date = date('Y-m-d',strtotime($leaving_internalpassages_rows[0]->end_datetime));
											}
										}
										
										
										//By Gouda.
										
										$current_row_exit_date = date('Y-m-d', strtotime($internalpassages_hotel->end_datetime . ' + ' . $nights_count . ' days')); 
										
										if($internalpassages_hotels_counter==$internalpassages_hotels_count) {
										?>
										<input type='hidden' name='last_exit_date'  id='last_exit_date'  value='<?= $current_row_exit_date ?>'/>
                                        <?php } ?>
                                        
                                        <?php echo $current_row_exit_date; ?>
            						
                                    </td>

                                </tr>
					
					
					
					
					
							<tr rel="<?php echo $internalpassages_hotel->safa_internalsegment_id ?>">
                                <td colspan="6">
                                    <table >
                                        <tr>
                                            <td rowspan="2">
                                                <span class="FRight"><?= lang('rooms_count') ?></span>
                                            </td>
                                            
                                            <? if (check_array($erp_hotelroomsizes)) { ?>
                                                <?php foreach ($erp_hotelroomsizes as $erp_hotelroomsize) { ?>
                                                    <td>
                                                    	<?php 
                                                    	$current_hotels_rooms_count=0;
                                                    	foreach($safa_trip_internaltrips_hotels_rows as $safa_trip_internaltrips_hotels_row) {
                                                    		//echo $safa_trip_internaltrips_hotels_row->safa_internalsegment_id.' - '. $internalpassages_hotel->safa_internalsegment_id .' - '. $safa_trip_internaltrips_hotels_row->erp_hotelroomsize_id.' - '. $erp_hotelroomsize->erp_hotelroomsize_id.'<br/>';
                                                    		if($safa_trip_internaltrips_hotels_row->erp_hotel_id== $internalpassages_hotel->erp_end_hotel_id && $safa_trip_internaltrips_hotels_row->erp_hotelroomsize_id== $erp_hotelroomsize->erp_hotelroomsize_id) {
                                                    			$current_hotels_rooms_count = $safa_trip_internaltrips_hotels_row->rooms_count ;
                                                    		} 
                                                    	}
                                                    	?>
                                                    	<?= $erp_hotelroomsize->{name()} ?> <?= $current_hotels_rooms_count ?>
                                                    </td>
                                                <? } ?>
                                            <? } ?>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            
                            <?php 
						$internalpassages_hotels_counter++;
						} 
						}
						} else if(session('ea_id') || session('uo_id')) {
							
							$row_class = '';
	
							if(isset($item_safa_reservation_forms_rows)) {
	                		if(count($item_safa_reservation_forms_rows)>0) {
                			
                			$accommodation_hotels_rows_count=0;
                			$accommodation_hotels_rows_counter=0;
                			
                			foreach($item_safa_reservation_forms_rows as $item_safa_reservation_forms_row) {
                				
                			if(isset($item_safa_reservation_forms_colors_arr[$item_safa_reservation_forms_row->safa_reservation_form_id])) {
                				$row_class = $item_safa_reservation_forms_colors_arr[$item_safa_reservation_forms_row->safa_reservation_form_id];
                			}	
                				
							$this->safa_passport_accommodation_rooms_model->safa_reservation_form_id = $item_safa_reservation_forms_row->safa_reservation_form_id;
							$accommodation_hotels_rows = $this->safa_passport_accommodation_rooms_model->get_hotels();
							
							
							if(count($accommodation_hotels_rows)>0) {
								$accommodation_hotels_rows_count = $accommodation_hotels_rows_count+count($accommodation_hotels_rows);
								foreach($accommodation_hotels_rows as $accommodation_hotels_row) {
								
					?>
					
					<tr class="table-row" hrel="<?php echo $accommodation_hotels_rows_counter; ?>">
	                   <td class="<?php echo $row_class;?>">
	                   <input type='hidden' name='hotel_safa_internalsegment_id[<?php echo $accommodation_hotels_rows_counter; ?>]'  id='hotel_safa_internalsegment_id[<?php echo $accommodation_hotels_rows_counter; ?>]'  value='0'/>
	                   <input type='hidden' name='erp_city_id[<?php echo $accommodation_hotels_rows_counter; ?>]'  id='erp_city_id[<?php echo $accommodation_hotels_rows_counter; ?>]'  value='<?php echo $accommodation_hotels_row->erp_city_id; ?>' cityrel="<?php echo $accommodation_hotels_rows_counter; ?>"/>
	                   <?php echo $accommodation_hotels_row->erp_city_name; ?>
	          			</td>
	          			
	                   <td class="<?php echo $row_class;?>">
	                   <table style='border:0px;'><tr inner_hrel="<?php echo $accommodation_hotels_rows_counter; ?>"><td style='border:0px;'  class="<?php echo $row_class;?>">
	          			<input type='hidden' name='erp_hotel_id[<?php echo $accommodation_hotels_rows_counter; ?>]'  id='erp_hotel_id[<?php echo $accommodation_hotels_rows_counter; ?>]'  value='<?php echo $accommodation_hotels_row->erp_hotel_id; ?>' class='erp_hotel_id' hotelrel="<?php echo $accommodation_hotels_rows_counter; ?>" />
						</td></tr></table>
	                    <?php echo $accommodation_hotels_row->erp_hotel_name; ?>
	          			</td>
	
	          			<td class="<?php echo $row_class;?>">
	          			<input type='hidden' name='arrival_date[<?php echo $accommodation_hotels_rows_counter; ?>]'  id='arrival_date[<?php echo $accommodation_hotels_rows_counter; ?>]'  value='<?php echo date('Y-m-d',strtotime($accommodation_hotels_row->from_date)); ?>' startrel="<?php echo $accommodation_hotels_rows_counter; ?>"/>
	                    <?php echo date('Y-m-d',strtotime($accommodation_hotels_row->from_date)); ?>
	          			</td>
	
	          			<td class="<?php echo $row_class;?>">
	          			
	          			<?php 
                       $nights_count = ceil((strtotime(date('Y-m-d',strtotime($accommodation_hotels_row->to_date))) - strtotime(date('Y-m-d',strtotime($accommodation_hotels_row->from_date)))) / (60 * 60 * 24));
                        ?>
	          			
	          			<input type='hidden' name='nights[<?php echo $accommodation_hotels_rows_counter; ?>]'  id='nights[<?php echo $accommodation_hotels_rows_counter; ?>]'  value='<?php echo $nights_count; ?>' nightsrel="<?php echo $accommodation_hotels_rows_counter; ?>"/>
	                    <?php echo $nights_count; ?>
	          			</td>
	          					
	          			<td class="<?php echo $row_class;?>">
	          			<input type='hidden' name='exit_date[<?php echo $accommodation_hotels_rows_counter; ?>]'  id='exit_date[<?php echo $accommodation_hotels_rows_counter; ?>]'  value='<?php echo date('Y-m-d',strtotime($accommodation_hotels_row->to_date)); ?>' endrel="<?php echo $accommodation_hotels_rows_counter; ?>"/>
	                    
	                    <?php 
						if(($accommodation_hotels_rows_counter+1)==$accommodation_hotels_rows_count) {
						?>
						<input type='hidden' name='last_exit_date'  id='last_exit_date'  value='<?= date('Y-m-d',strtotime($accommodation_hotels_row->to_date)) ?>'/>
                        <?php }?>
	                    
	                    <?php echo date('Y-m-d',strtotime($accommodation_hotels_row->to_date)); ?>
	          			</td>
	
	                   
	                   </tr>
	
	                   <tr rel="<?php echo $accommodation_hotels_rows_counter; ?>">
	          		   <td colspan="6"  class="<?php echo $row_class;?>">
	                   <table >
	                   <tr inner_hrel='<?php echo $accommodation_hotels_rows_counter; ?>'>
	                   <td  class="<?php echo $row_class;?>">
	                   <span class="FRight"><?= lang('rooms_count') ?></span>
	                   </td>
	          		
	          		<? 
	          		if (check_array($erp_hotelroomsizes)) {
	          		foreach ($erp_hotelroomsizes as $erp_hotelroomsize) {
	          			?>
	          		<?php 
                    $current_hotels_rooms_count=0;
                    foreach($safa_trip_internaltrips_hotels_rows as $safa_trip_internaltrips_hotels_row) {
                        if($safa_trip_internaltrips_hotels_row->erp_hotel_id== $accommodation_hotels_row->erp_hotel_id && $safa_trip_internaltrips_hotels_row->erp_hotelroomsize_id== $erp_hotelroomsize->erp_hotelroomsize_id) {
                           	$current_hotels_rooms_count = $safa_trip_internaltrips_hotels_row->rooms_count ;
                         } 
                    }
                   	?>	
	          		<td  class="<?php echo $row_class;?>"> 
	          		<?= $erp_hotelroomsize->{name()} ?>  <?php echo $current_hotels_rooms_count;?>
	          		</td>
	          		<? } 
	          		 } 
	          		 ?>
	          		</tr>
	          		</table>
	           		</td>
	                </tr>
	                
					<?php 
					
					$accommodation_hotels_rows_counter++;
						}
						
							}
                			}
	                		}
                			}
					}
					?>
                    </tbody>
                </table>
            </fieldset>
        </div>

        <div class="row-fluid" id="div_tbl_tourismplaces">
            <fieldset class="resalt-group">
                <div class="wizerd-div"><a> <?= lang('tourismplaces') ?></a></div>
                <table id='tourismplaces'>
                    <thead>
                        <tr>
                            <th><?= lang('tourismplace')   ?></th>
                            <th><?= 
                                lang('the_date')
                        ?></th>
                            
                           
                        </tr>
                    </thead>
                    <tbody class="tourismplaces">

				<?php 
					$internalpassages_tourismplaces_count = 0;
					
					$internalpassages_tourismplaces_counter=1;
					if(isset($internalpassages_tourismplaces)){
						$internalpassages_tourismplaces_count = count($internalpassages_tourismplaces);
					
						foreach($internalpassages_tourismplaces as $internalpassages_tourismplace) {
							
							$this->internalpassages_model->safa_internalsegment_id = $internalpassages_tourismplace->safa_internalsegment_id+1;
                            $next_internalpassages_row = $this->internalpassages_model->get();
                            $this->internalpassages_model->safa_internalsegment_id = false;
                            
                            //if(count($next_internalpassages_row)>0) {
					?>
					
					<tr trel="<?php echo $internalpassages_tourismplace->safa_internalsegment_id ?>">
                                    
                                    <td>
                                    <input type='hidden' name='tourism_safa_internalsegment_id[<?= $internalpassages_tourismplace->safa_internalsegment_id ?>]'  id='tourism_safa_internalsegment_id[<?= $internalpassages_tourismplace->safa_internalsegment_id ?>]'  value='<?php echo $internalpassages_tourismplace->safa_internalsegment_id;?>'/>
                                      
                                   
									 
									 <?php echo $internalpassages_tourismplace->safa_tourismplace_name; ?>
                                    </td>
                                 
                                    <td>
                                         <?php echo date('Y-m-d H:i',strtotime($internalpassages_tourismplace->start_datetime));?>
									</td>
									
                                </tr>
					
					<?php 
					$internalpassages_tourismplaces_counter++;
						}
					}
					?>
					
                    </tbody>
                </table>
            </fieldset>
        </div>



<div class="row-fluid">

<fieldset class="resalt-group">
  <div class="wizerd-div"><a> <?= lang('internal_transportation') ?></a></div>

        <div class="row-form" >
        
         <div class="span6" > 
        <?php if($safa_internaltrip_type_id!='') {?>
        <?php echo item("safa_internaltrip_types", name(), array("safa_internaltrip_type_id" => $safa_internaltrip_type_id)); ?>
        <?php }?>
        </div>
        <div class="span6" > 
        <?php 
             if(session('uo_id') || session('ito_id')) { 
             if($item->operator_reference=='') {
             ?>
             <a title='<?=lang('confirmation')?>'  href="javascript:void();" onclick="alert('<?=lang('you_must_enter_operator_reference_to_confirm_trip_internaltrip')?>');"><span class="icon-ok"></span></a>
             <?php 
             //} else if((session('uo_id')&& $item->safa_internaltripstatus_id== 4) || (session('ito_id')&& $item->safa_internaltripstatus_id== 5)) {
             } else if((session('uo_id')&& $item->safa_internaltripstatus_id== 4) || ($item->safa_internaltripstatus_id== 5)) {
             	//echo lang('');
             } else {
             ?>
             <a title='<?=lang('confirmation')?>'  href="<?=  site_url('safa_trip_internaltrips/confirm/'.$item->safa_trip_internaltrip_id)?>"><span class="icon-ok"></span></a>
             <?php }
             }
             ?>	
        </div>
        
        </div>
        
       <div class="row-form" >
       <?php 
                        $ito_validate_calss='';
                        $ito_display_style='none';
                        if($safa_trip_internaltrip_id==0) {
                        	$ito_validate_calss =" validate[required]";
                        	$ito_display_style='inline';
                        } else if($safa_internaltrip_type_id==1) {
                        	$ito_validate_calss =" validate[required]";
                        	$ito_display_style='inline';
                        }
                        ?>
                        
       
       <?php if  (session('uo_id') || session('ea_id')) { ?>
       <div class="span6" >
                <div class="span4">
                        <?= lang("transport_request_opertator") ?>
                    </div>
                    
                    <div class="span8" >
                    	<?php if($safa_ito_id!='') {?>
                        <?php echo item("safa_itos", name(), array("safa_ito_id" => $safa_ito_id)); ?>
        				<?php }?>
                </div>
        </div>
        <?php } ?>
        
        
        <div class="span6">
                        <div class="span4"><?= lang("transport_request_transportername") ?>
                        
                        </div>
                        <div class="span8" >
                        	<?php if($safa_transporter_id!='') {?>
                            <?php echo item("safa_transporters", name(), array("safa_transporter_id" => $safa_transporter_id)); ?>
        					<?php }?>
                        </div>
                    </div>
        
        
        </div>
        
        <div class="row-form" >
                <div class="span6">
                    <div class="span4">
                        <?= lang("transport_request_res_code") ?>
                    </div>
                    <div class="span8" >
                        <?= $operator_reference ?>
                        
                    </div>
            	</div> 
                
                <div class="span6" >
                    <div class="span4" ><?= lang('confirmation_number') ?>
                    </div>
                    <div class="span8" >

                        <?= $confirmation_number ?>
                        
                    </div>
                </div>
            
            </div>    
            
        <div class="row-form" >    
        
            
            <div class="span12" >
                    <div class="span4" ><?= lang("transport_request_res_file") ?></div>
                   
                    <?php if(isset($item)) { ?>
                   <div class="span8">
                    	<a href="<?= base_url('static/temp/uo_files/' . session('internaltrip_file_upload_time') . $item->attachement) ?>" target="_blank"  ><?= $item->attachement ?></a>
                    </div> 
                    <?php }?>
             </div>  
            
         </div>
            
        
        <div class="row-form" >  
        <script>drivers_and_buses_counter = 0</script>
        <table cellpadding="0" cellspacing="0" id="tbl_drivers_and_buses">
                <thead>
                    <tr>
                    	
                        
                        <th><?php echo  lang('bus_type') ?></th>
                        <th><?php echo  lang('bus_model') ?></th>
                        <th><?php echo  lang('bus_count') ?></th>
                         
                        <th><?php echo  lang('passengers_count') ?></th>
                       
                    </tr>
                </thead>
                <tbody class="drivers_and_buses" id='drivers_and_buses'>
                <? if(isset($item_drivers_and_buses)) { ?>
                    <? if(check_array($item_drivers_and_buses)) { 
                     $item_drivers_and_buses_counter=0;
                     foreach($item_drivers_and_buses as $item_driver_and_bus) { 
                    $item_drivers_and_buses_counter++;
                    ?>
                    <tr rel="<?php echo  $item_drivers_and_buses_counter ?>">
                         
                        <td>
                        	<?php if( $item_driver_and_bus->safa_buses_brands_id!='') {?>
			                  <?php echo item("safa_buses_brands", name(), array("safa_buses_brands_id" => $item_driver_and_bus->safa_buses_brands_id)); ?>
        					<?php }?>
			            </td>
			            
			            <td>
			            <?php if( $item_driver_and_bus->safa_buses_models_id!='') {?>
			                  <?php echo item("safa_buses_models", name(), array("safa_buses_models_id" => $item_driver_and_bus->safa_buses_models_id)); ?>
        				<?php }?>
			            </td>
			            
            			<td><?php echo  $item_driver_and_bus->bus_count ?>
			            </td>
			
			             <td><?php echo $item_driver_and_bus->seats_count ?>
			             </td>
			            
                       
                    </tr>
                    <? } ?>
                    <? } ?>
                    <? } ?>
                </tbody>
            </table> 
        </div>

</fieldset>
</div>



<div class="row-fluid" id="div_internalsegments" >

<fieldset class="resalt-group">
  <div class="wizerd-div"><a> <?= lang('trip_internaltrip') ?></a></div>
                
        


		<script>passages_counter = 0</script>
		
		<div class="row-form" >
		
		<input type="hidden" id="hdn_are_new_segments_generated" name="hdn_are_new_segments_generated" value="0"></input>
		                            
        <div class="table-responsive" id="div_tbl_internalsegments">
            <!--- the part of internal passages---> 
                <table cellpadding="0" cellspacing="0" id="tbl_internalsegments">
                    <thead>
                        <tr>
                            <th><?= lang('internalpassage_type') ?></th>
                            <th><?= lang('internalpassage_starthotel') ?></th>
                            <th><?= lang('internalpassage_endhotel') ?></th>
                            <th><?= lang('internalpassage_startdatatime') ?></th>
                            <th><?= lang('internalpassage_enddatatime') ?></th>
<!--                            <th><?= lang('internalpassage_seatscount') ?></th>-->
                            <th><?= lang('internalpassage_note') ?></th>
                            <th class="TAC"><?= lang('global_operations') ?></th>
                            
                        </tr>
                    </thead>
                    <tbody class="internalsegments" id="tbl_body_internalsegments">
                        <? if (isset($internalpassages)) : ?>
                            <? foreach ($internalpassages as $inernalpassage): ?>
                                <tr>
                                <td><?= item("safa_internalsegmenttypes", name(),
                                        array("safa_internalsegmenttype_id" => $inernalpassage->safa_internalsegmenttype_id)); ?></td>
                                <? if ($inernalpassage->safa_internalsegmenttype_id == 1): 
                                
                                	$erp_port='';
                                	if($inernalpassage->erp_port_id) {
                                		$erp_port = item("erp_ports", name(), array("erp_port_id" => $inernalpassage->erp_port_id));
                                	}
                                	
                                	$erp_end_hotel='';
                                	if($inernalpassage->erp_end_hotel_id) {
                                		$erp_end_hotel = item("erp_hotels", name(), array("erp_hotel_id" => $inernalpassage->erp_end_hotel_id));
                                	}
                                	
                                
                                ?>
                                    <td><?= $erp_port; ?></td>
                                    <td><?= $erp_end_hotel; ?></td>
        						<? endif; ?>  
                                <? 
                                
                                	if ($inernalpassage->safa_internalsegmenttype_id == 2){ 
                                	$erp_start_hotel='';
                                	if($inernalpassage->erp_start_hotel_id) {
                                		$erp_start_hotel = item("erp_hotels", name(), array("erp_hotel_id" => $inernalpassage->erp_start_hotel_id));
                                	}
                                	
                                	$erp_port='';
                                	if($inernalpassage->erp_port_id) {
                                		$erp_port = item("erp_ports", name(), array("erp_port_id" => $inernalpassage->erp_port_id));
                                	}
                                ?>
                                    <td><?= $erp_start_hotel; ?></td>
                                    <td><?= $erp_port; ?></td>
                                    <? }; ?>
                                    <? if ($inernalpassage->safa_internalsegmenttype_id == 3): 
                                    
                                    $erp_start_hotel='';
                                	if($inernalpassage->erp_start_hotel_id) {
                                		$erp_start_hotel = item("erp_hotels", name(), array("erp_hotel_id" => $inernalpassage->erp_start_hotel_id));
                                	}
                                	
                                	$safa_tourism_place='';
                                	if($inernalpassage->safa_tourism_place_id) {
                                		$safa_tourism_place = item("safa_tourismplaces", name(), array("safa_tourismplace_id" => $inernalpassage->safa_tourism_place_id));
                                	}
                                	
                                    ?>
                                    <td><?= $erp_start_hotel; ?></td>
                                    <td><?= $safa_tourism_place; ?></td>
                                    <? endif; ?>
                                    
                                    <? if ($inernalpassage->safa_internalsegmenttype_id == 4): 
                                    
                                    $erp_start_hotel='';
                                	if($inernalpassage->erp_start_hotel_id) {
                                		$erp_start_hotel = item("erp_hotels", name(), array("erp_hotel_id" => $inernalpassage->erp_start_hotel_id));
                                	}
                                	
                                	$erp_end_hotel='';
                                	if($inernalpassage->erp_end_hotel_id) {
                                		$erp_end_hotel = item("erp_hotels", name(), array("erp_hotel_id" => $inernalpassage->erp_end_hotel_id));
                                	}
                                    
                                    ?>
                                    <td><?= $erp_start_hotel; ?></td>
                                    <td><?= $erp_end_hotel; ?></td>
                                    <? endif; ?>
                                <td>
					        <? if (isset($inernalpassage->start_datetime) && $inernalpassage->start_datetime != null && $inernalpassage->start_datetime != '0000-00-00 00:00:00'): ?>
					            <? $date = date_create($inernalpassage->start_datetime); ?>
					                                        <span><?= date_format($date, "m-d H:i"); ?></span>
					                                    <? endif; ?>   
					                                </td>
					                                <td>
					                                        <? if (isset($inernalpassage->end_datetime) && $inernalpassage->end_datetime != null && $inernalpassage->end_datetime != '0000-00-00 00:00:00'): ?>
					            <? $date_end = date_create($inernalpassage->end_datetime); ?>    
                                        <span><?= date_format($date_end, "m-d H:i"); ?></span>
                            <? endif; ?> 
                                </td>
<!--                                <td><? if (isset($inernalpassage->seats_count)): ?> <?= $inernalpassage->seats_count ?> <? endif; ?>  </td>-->
                                <td><? if (isset($inernalpassage->notes)): ?> <?= $inernalpassage->notes ?> <? endif; ?>  </td>
                                
                                
                                <td class="TAC">
                                   		
			                            <?php 
	                                    $this->internalsegments_drivers_and_buses_model->safa_internalsegments_id=$inernalpassage->safa_internalsegment_id;
	                                    $count_drivers_and_buses= $this->internalsegments_drivers_and_buses_model->get(true);
	                                    ?>	
	                                    X <?php echo $count_drivers_and_buses; ?> 
										 	   
	                                    <a title='<?= lang('drivers_and_buses') ?>' href="<?= site_url('uo/all_movements/drivers_and_buses/' . $inernalpassage->safa_internalsegment_id) ?>" style="color:black; direction:rtl">
	                                             <img src="<?= IMAGES2 ?>/car.png" align="left" >
	                                    </a>    
		                            	
		                        	</td>
                            </tr> 
			                        
			                        
			                        
                                    
                                </tr>  

                            <? endforeach; ?>
                        <? endif; ?>  
                    </tbody>
                </table>    
            
        </div>
        
        </div>
        
         
        
        
        </fieldset>
        
        
        
        
		  <fieldset class="resalt-group">
		  <div class="wizerd-div"><a> <?= lang('trip_details_travellers') ?></a></div>
  
                                				    		
                                    <div class="block-fluid" id="dv_tbl_trip_traveller">
                                        <table class="fpTable" id="tbl_trip_traveller" cellpadding="0" cellspacing="0" width="100%">
                                            <thead>
                                                <tr>
                                                    <th><?= lang('trip_details_traveller') ?></th>
                                                    <th><?= lang('trip_details_passport_no') ?></th>
                                                    <th><?= lang('trip_details_visa_no') ?></th>
                                                    <th><?= lang('trip_details_nationality') ?></th>
                                                    <th><?= lang('trip_details_gender') ?></th>
                                                    
                                                    <th><?= lang('group_passports_age') ?></th>
                                                    <th><?= lang('group_passports_occupation') ?></th>
                                                    <th><?= lang('group_passports_picture') ?></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <? 
                                              
                                                $trip_travellers_arr = array();
												$this->safa_group_passports_model->safa_trip_internaltrip_id = false;
                                                $this->safa_group_passports_model->safa_trip_id = false;

                                                $trip_group_passports_rows = $this->safa_group_passports_model->get_for_table();
												foreach ($trip_group_passports_rows as $trip_group_passports_row) {
									                $trip_travellers_arr[$trip_group_passports_row->safa_group_passport_id] = $trip_group_passports_row->{'first_' . name()} . ' ' . $trip_group_passports_row->{'second_' . name()} . ' ' . $trip_group_passports_row->{'third_' . name()} . ' ' . $trip_group_passports_row->{'fourth_' . name()};
									            }
												
												
                                                $item_travellers_arr = array();

                                                //----------------------- Group Passports Linked With Accomodation Forms ----------------
                                                $distinct_passports_by_trip_rows = $this->safa_group_passport_accommodation_model->get_distinct_passports_by_trip_uo_form($safa_trip_id);
                                                $distinct_passports_by_trip_arr = array();
                                                foreach ($distinct_passports_by_trip_rows as $distinct_passports_by_trip_row){ 
                                                	$distinct_passports_by_trip_arr[]=$distinct_passports_by_trip_row->safa_group_passport_id;
                                                }
                                                
                                                $passports_forms_by_trip_arr = array();
                                                foreach ($distinct_passports_by_trip_rows as $distinct_passports_by_trip_row){ 
                                                	$passports_forms_by_trip_arr[]=$distinct_passports_by_trip_row->safa_reservation_form_id;
                                                }
                                                //---------------------------------------------------------------------------------------
                                                
                                                
                                                
                                                if (isset($safa_group_passports)) {
                                                 	
                                                $row_class='';
                                                	
                                                foreach ($safa_group_passports as $safa_group_passport){ 
                                                $item_travellers_arr[] = $safa_group_passport->safa_group_passport_id;
                                                	
                                                $passports_forms_by_trip_arr_key = array_search($safa_group_passport->safa_group_passport_id, $distinct_passports_by_trip_arr);
												if ($passports_forms_by_trip_arr_key !== false) {
                                                	$passports_safa_reservation_form_id=$passports_forms_by_trip_arr[$passports_forms_by_trip_arr_key];
                                                	
                                                	if(isset($item_safa_reservation_forms_colors_arr[$passports_safa_reservation_form_id])) {
						                				$row_class = $item_safa_reservation_forms_colors_arr[$passports_safa_reservation_form_id];
						                			}
						                				
                                                }
                                                	
                                                ?>
                                                    <tr>
                                                       
                                                        <td class="<?php echo $row_class;?>" ><?= $safa_group_passport->{'first_' . name()} . ' ' . $safa_group_passport->{'second_' . name()} . ' ' . $safa_group_passport->{'third_' . name()} . ' ' . $safa_group_passport->{'fourth_' . name()} ?></td>
                                                        <td class="<?php echo $row_class;?>" ><?= $safa_group_passport->passport_no ?></td>
                                                        <td class="<?php echo $row_class;?>" ><?= $safa_group_passport->visa_number ?></td>
                                                        <td class="<?php echo $row_class;?>" ><?= $safa_group_passport->erp_nationalities_name ?></td>
                                                        <td class="<?php echo $row_class;?>" ><?= $safa_group_passport->erp_gender_name ?></td>
                                                        
                                                        <td class="<?php echo $row_class;?>" >
                                                        
                                                        <?php
			                                    $date_of_birth = strtotime($safa_group_passport->date_of_birth);
			                                    $date_now = strtotime(date('Y-m-d', time()));
			                                    $secs = $date_now - $date_of_birth; // == return sec in difference
			                                    $days = $secs / 86400;
			                                    $age = $days / 365;
												//$age=round($age);
												//$age=ceil($age);
			                                    $age = floor($age);
			                                    echo $age;
			                                    ?>
                                                        
                                                 </td>
                                                 <td class="<?php echo $row_class;?>" ><?= $safa_group_passport->occupation ?></td>
                                                 
                                                 <td class="<?php echo $row_class;?>" >
                                                 <?php 
                                                	if ($safa_group_passport->picture != '') {
					                             ?>
					                                <img src="<?= base_url('static/group_passports') . '/' . $safa_group_passport->picture; ?>" alt="" width="50px" height="60px">
					                            <?php
					                            	}
                                                 ?>
                                                 </td>
                                                 
                                                
                                                 
                                                    </tr>
                                                <? }
                                              }
                                                ?>
                                            </tbody>
                                        </table>
                                        
                                        
                                        <?php  
							    		echo form_multiselect('safa_trip_traveller_ids[]', $trip_travellers_arr, set_value('safa_trip_traveller_ids', $item_travellers_arr),
			                                    " id='safa_trip_traveller_ids'  class='' readonly='readonly' style='display: none;' ");
			        					?>
                                        
                                        <script>
				                         $("#hrf_remove_traveller").live('click', function() {
				                             if (!confirm('<?= lang('global_are_you_sure_you_want_to_delete') ?>'))
				                                 return false
				
				                            var traveller_id = $(this).attr('name');
				                            //alert(traveller_id);
				                            $("#safa_trip_traveller_ids").find('option[value="'+traveller_id+'"]').remove();
				                             
				                             $(this).parent().parent().remove();

				                             var hidden_input = '<input type="hidden" name="safa_group_passports_remove[]" value="' + traveller_id + '" />';
				                     	    $('#dv_tbl_trip_traveller').append(hidden_input);
				                     	    
				                         });
				                         </script>
                                        
                                        <?php if(session('ea_id')){ ?>
                                        <div class="span12">
										<div class="span10"><a class="btn btn-primary finish fancybox fancybox.iframe"
											href="<?php echo  site_url('ea/group_passports/add_to_internaltrip_by_trip_popup/'.$safa_trip_id) ?>"><?php echo  lang('add_safa_group_passports_to_internaltrip') ?></a>
										</div>
										</div>
                                        <?php }?>
                                        
                                    </div>
        					</fieldset>
                            <br/>
        
        
        
        
        <div class="toolbar bottom TAC">
        </div>
        
        
    </div>


        
<?= form_close() ?>
    </div>
</div>  


<script>
    $(document).ready(function() {
        $('.fancybox').fancybox({
            afterClose: function() {
                //parent.location.reload(true);
            }
        });
    });
</script>

<script type="text/javascript">
$(document).ready(function() {
    var config = {
        '.chosen-select': {width: "100%"},
        '.chosen-select-deselect': {allow_single_deselect: true},
        '.chosen-select-no-single': {disable_search_threshold: 10},
        '.chosen-select-no-results': {no_results_text: 'Oops, nothing found!'},
        '.chosen-select-width': {width: "95%"}
    }
    for (var selector in config) {
        $(selector).chosen(config[selector]);
    }

    // binds form submission and fields to the validation engine
    $("#frm_view_trip_internaltrip").validationEngine({
            prettySelect : true,
            useSuffix: "_chosen",
            promptPosition : "topRight:-150"
            //promptPosition : "bottomLeft"
    });
});

</script> 
