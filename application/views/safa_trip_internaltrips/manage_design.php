<!--toggle menu--> 
<script>
    jQuery(function() {
    menu1.toggle();
    })
</script> 
<!--toggle menu end-->

<!--css-->
<style>
    table td {
        padding:3px !important;
    }
    table th {
        padding:0px !important;

    }
    .tbofsakn table tr td {
        background: #d7e8f3 !important;
        text-align: center;

    }
    .tbofsakn table tr td input {
        margin:0px 10px;
    }
    .fpTable thead tr th{
        padding: 3px !important;
    }
    .greycolor {
        background : rgba(240,240,240,0.2) !important;
    }
</style>
<?php
$safa_trip_internaltrip_id = 0;
$safa_trip_id = '';
$safa_ito_id = '';
//$safa_uo_id='';  
//$safa_ea_id='';  
$safa_uo_contract_id = '';
$erp_flight_availability_id = '';
$safa_transporter_id = '';
$safa_internaltripstatus_id = 2;
$safa_tripstatus_id = '';
$erp_company_type_id = '';
$erp_company_id = '';
$erp_company_name = '';
$operator_reference = '';
$attachement = '';
$datetime = date('Y-m-d', time());

$confirmation_number = 0;
if (!isset($item)) {
    $confirmation_number = gen_itconf();
}

$ito_notes = '';
$ea_notes = '';
$trip_title = '';
$trip_supervisors = '';
$trip_supervisors_phone = '';
$adult_seats = '';
$child_seats = '';
$baby_seats = '';
if (session('uo_id')) {
    $serial = $safa_uo_serial;
}
$buses_count = '';

$erp_transportertype_id = '';

$land_going_erp_port_id = '';
$land_return_erp_port_id = '';
$land_going_datetime = '';
$land_return_datetime = '';

$sea_going_erp_port_id = '';
$sea_return_erp_port_id = '';
$sea_going_datetime = '';
$sea_return_datetime = '';

$safa_internaltrip_type_id = 0;

$manage_title = lang('add_safa_trip_internaltrips');
if (isset($item)) {
    if (count($item) > 0) {
        $manage_title = lang('edit_safa_trip_internaltrips');


        $safa_trip_internaltrip_id = $item->safa_trip_internaltrip_id;
        $safa_trip_id = $item->safa_trip_id;
        $safa_ito_id = $item->safa_ito_id;
        $safa_uo_contract_id = $item->safa_uo_contract_id;
        $erp_flight_availability_id = $item->erp_flight_availability_id;
        $safa_transporter_id = $item->safa_transporter_id;
        $safa_internaltripstatus_id = $item->safa_internaltripstatus_id;
        $safa_tripstatus_id = $item->safa_tripstatus_id;
        $erp_company_type_id = $item->erp_company_type_id;
        $erp_company_id = $item->erp_company_id;
        $erp_company_name = $item->erp_company_name;
        $operator_reference = $item->operator_reference;
        $attachement = $item->attachement;
        $datetime = $item->datetime;
        $confirmation_number = $item->confirmation_number;
        $ito_notes = $item->ito_notes;
        $ea_notes = $item->ea_notes;
        $trip_title = $item->trip_title;
        $trip_supervisors = $item->trip_supervisors;
        $trip_supervisors_phone = $item->trip_supervisors_phone;
        $adult_seats = $item->adult_seats;
        $child_seats = $item->child_seats;
        $baby_seats = $item->baby_seats;
        $serial = $item->serial;
        $buses_count = $item->buses_count;

        $erp_transportertype_id = $item->erp_transportertype_id;
        $safa_internaltrip_type_id = $item->safa_internaltrip_type_id;

        if ($erp_transportertype_id == 1) {
            if (isset($internalpassages)) {
                foreach ($internalpassages as $inernalpassage) {
                    if ($inernalpassage->safa_internalsegmenttype_id == 1) {
                        $land_going_erp_port_id = $inernalpassage->erp_port_id;
                        $land_going_datetime = $inernalpassage->start_datetime;
                    } else if ($inernalpassage->safa_internalsegmenttype_id == 2) {
                        $land_return_erp_port_id = $inernalpassage->erp_port_id;
                        $land_return_datetime = $inernalpassage->start_datetime;
                    }
                }
            }
        } else if ($erp_transportertype_id == 3) {
            if (isset($internalpassages)) {
                foreach ($internalpassages as $inernalpassage) {
                    if ($inernalpassage->safa_internalsegmenttype_id == 1) {
                        $sea_going_erp_port_id = $inernalpassage->erp_port_id;
                        $sea_going_datetime = $inernalpassage->start_datetime;
                    } else if ($inernalpassage->safa_internalsegmenttype_id == 2) {
                        $sea_return_erp_port_id = $inernalpassage->erp_port_id;
                        $sea_return_datetime = $inernalpassage->start_datetime;
                    }
                }
            }
        }
    }
}
?>
<style>
    .pxs{
        width:70px;
    }
    /*    .row-form {
            border:none !important;
            border-right: none !important;
        }
        .row-form > [class^="span"]{
            border-right: none !important;
        }*/
/*    input, textarea, .uneditable-input {
        padding: 0px !important;
        margin-bottom:0px !important;
    }*/
    .wizerd-div {
        margin:-12px 0px 20px;
    }
</style>
<div id="dv_internaltrip_manage_storage">

    <div class="row-fluid" >
        <div class="widget">
            <div class="path-container Fright">
                <div class="icon"><i class="icos-pencil2"></i></div> 
                <div class="path-name Fright">
                    <a href="<?php
                    if (session('uo_id')) {
                        echo site_url() . 'uo/dashboard';
                    } else if (session('ea_id')) {
                        echo site_url() . 'ea/dashboard';
                    } else if (session('ito_id')) {
                        echo site_url() . 'ito/dashboard';
                    } else {
                        echo site_url();
                    }
                    ?>"><?php echo lang('global_system_management') ?></a>
                </div>    
                <div class="path-arrow Fright">
                </div>
                <div class="path-name Fright">
                    <a href="<?= site_url('safa_trip_internaltrips') ?>"><?= lang('safa_trip_internaltrips') ?></a>
                </div>
                <div class="path-arrow Fright">
                </div>
                <div class="path-name Fright">
                    <span><?= $manage_title ?></span>
                </div>
            </div>     
        </div>

        <div class="widget">
            <div class="widget-header">
                <div class="widget-header-icon Fright">
                    <span class="icos-pencil2"></span>
                </div>
                <div class="widget-header-title Fright">
                    <?= lang('add_safa_trip_internaltrips') ?>
                </div>
            </div>

            <?= form_open_multipart(false, 'id="frm_trip_internaltrip" ') ?>
            <div class="block-fluid">
                <div class="row-form" >
                    <!--Date-->
                    <div class="span3" >
                        <div class="span6">
                            <?= lang("transport_request_date") ?>
                            <font style='color:red'>*</font>
                        </div>
                        <div class="span6"> 
                            <div >
                                <input class="datetime validate[required] span12" data-format="yyyy-MM-dd" name="datetime" id="date" type="text" value="<?= set_value('datetime', $datetime) ?>" ></input>

                                <script>
                                            $('.datetime').datepicker({
                                    dateFormat: "yy-mm-dd",
                                            controlType: 'select',
                                            timeFormat: 'HH:mm'
                                    });</script>


                                <!-- 
                                <span class="add-on">
                                    <i data-time-icon="icon-time" data-date-icon="icon-calendar">
                                    </i>
                                </span>
                                -->
                            </div>
                            <span class="bottom" style='color:red' >
                                <?= form_error('datetime') ?> 
                            </span>
                        </div>


                    </div>
                    <!--statue-->
                    <div class="span4" >
                        <div class="span4"><?= lang("transport_request_status") ?>
                            <font style='color:red'>*</font>   
                        </div>
                        <div class="span8">
                            <?= form_dropdown("safa_internaltripstatus_id", $safa_intrernaltripstatus, set_value("safa_internaltripstatus_id", $safa_internaltripstatus_id), "id='safa_internaltripstatus_id' class='chosen-select chosen-rtl validate[required]'") ?>
                            <span class="bottom" style='color:red' >
                                <?= form_error('safa_internaltripstatus_id') ?> 
                            </span>
                        </div>
                    </div>
                    <!--wakel-->
                    <div class="span5" >
                        <div class="span2"><?= lang("transport_request_contract") ?>
                            <font style='color:red'>*</font>
                        </div>
                        <div class="span10">
                            <?= form_dropdown("safa_uo_contract_id", $safa_uo_contracts, set_value("safa_uo_contract_id", $safa_uo_contract_id), " id='safa_uo_contract_id' class=' chosen-select chosen-rtl validate[required] input-huge '") ?>
                            <span class="bottom" style='color:red' >
                                <?= form_error('safa_uo_contract_id') ?> 
                            </span>
                        </div>
                    </div>

                </div>
                <!--end form-->

                <div class="row-form" >


                    <!--<div class="span6">
                        <div class="span4">
                    <?= lang("transport_request_res_code") ?>
                        </div>
                        <div class="span8" >
                    <?= form_input("operator_reference", set_value("operator_reference", $operator_reference)) ?>
                            <span class="bottom" style='color:red' >
                    <?= form_error('operator_reference') ?> 
                            </span>
                        </div>
                    </div>
                    --></div>
                <!--<div class="row-form" >
                    <div class="span6">
                        <div class="span4"><?= lang("transport_request_transportername") ?>
                        </div>
                        <div class="span8" >
                <?= form_dropdown("safa_transporter_id", $bus_transporters, set_value("safa_transporter_id", $safa_transporter_id), "class='chosen-select chosen-rtl'") ?>
                            <span class="bottom" style='color:red' >
                <?= form_error('safa_transporter_id') ?> 
                            </span>
                        </div>
                    </div>
                </div>
                --><!--<div class="row-form" >
                    <div class="span6" >
                        <div class="span4" ><?= lang("transport_request_res_file") ?></div>
                        <div class="span8">
                            <input type="file" value="" name="userfile"/>
                            <span class="bottom" style='color:red' >
                <?= form_error('userfile') ?> 
                            </span>
                        </div> 
                    </div>
                    
                    <div class="span6" >
                        <div class="span4" ><?= lang('confirmation_number') ?></div>
                        <div class="span8" >
    
                <?= form_input("confirmation_number", set_value("confirmation_number", $confirmation_number), "") ?>
                            <span class="bottom" style='color:red' >
                <?= form_error('confirmation_number') ?> 
                            </span>
                        </div>
                    </div>
                
                </div>
    
                --><div class="row-form" >
                    <div class="span6">
                        <div class="span3"><?= lang("travellers_seats") ?></div>
                        <div class="span3" >
                            <div class="span5" ><?= lang("adult_seats") ?>
                                <font style='color:red'>*</font> 
                            </div>
                            <div class="span7">
                                <?= form_input("adult_seats", set_value("adult_seats", $adult_seats), " onchange='fix_seats_count();' class='validate[required] span8'") ?>
                                <span class="bottom" style='color:red' >
                                    <?= form_error('adult_seats') ?> 
                                </span>
                            </div> 
                        </div>
                        <div class="span3" >
                            <div class="span4" ><?= lang('child_seats') ?></div>
                            <div class="span8" >
                                <?= form_input("child_seats", set_value("child_seats", $child_seats), " onchange='fix_seats_count();' class='span8' ") ?>
                                <span class="bottom" style='color:red' >
                                    <?= form_error('child_seats') ?> 
                                </span>
                            </div>
                        </div>
                        <div class="span3" >
                            <div class="span4" ><?= lang('baby_seats') ?></div>
                            <div class="span8" >

                                <?= form_input("baby_seats", set_value("baby_seats", $baby_seats), " onchange='fix_seats_count();' class='span8' ") ?>
                                <span class="bottom" style='color:red' >
                                    <?= form_error('baby_seats') ?> 
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="span6">
                        <!--phone-->
                        <div class="span6" >
                            <div class="span4">
                                <?= lang("transport_trip_supervisors_phone") ?>
                            </div>
                            <div class="span8" >
                                <?= form_input("trip_supervisors_phone", set_value("trip_supervisors_phone", $trip_supervisors_phone), " id='trip_supervisors_phone' class='input-huge '") ?>
                                <span class="bottom" style='color:red' >
                                    <?= form_error('trip_supervisors_phone') ?> 
                                </span>
                            </div>
                        </div>
                        <div class="span6" >
                            <div class="span4"><?= lang("transport_trip_supervisors") ?>
                            </div>
                            <div class="span8">
                                <?= form_input("trip_supervisors", set_value("trip_supervisors", $trip_supervisors), " id='trip_supervisors' class=' input-huge '") ?>
                                <span class="bottom" style='color:red' >
                                    <?= form_error('trip_supervisors') ?> 
                                </span>
                            </div>
                        </div>

                    </div>
                </div>


                <!--نوع النقل-->
                <div class="row-form">
                    <div class="span6">
                        <div class="span3" style='margin-top:15px'><?= lang('erp_transportertype_id') ?></div>
                        <div class='span9'>
                            <div class='span4'>
                                <a href='#' class='ship'>بحري</a>
                            </div>
                            <div class='span4'>
                                <a href='#' class='trans'>بري</a>
                            </div>
                            <div class='span4'>
                                <a href='#' class='flighting'>جوي</a>
                            </div>
                        </div>
                    </div>
                    <div class="span6">
                        <div class="span5">
                            <div class="span12" >

                                <?php if (session('uo_id')) { ?>
                                    <div class="span12" >
                                        <div class="span4"><?= lang("trip_code") ?>
                                            <font style='color:red'>*</font>
                                        </div>
                                        <div class="span8">
                                            <!--<?= form_input("trip_title", set_value("trip_title"), " id='trip_title' class=' input-huge '") ?>
                                            <span class="bottom" style='color:red' >
                                            <?= form_error('trip_title') ?> 
                                            </span>
                                            -->
                                            <?= form_input("safa_uo_code", set_value("safa_uo_code", $safa_uo_code), "class='validate[required] pxs ' readonly='readonly' ") ?>
                                            <?= form_input("serial", set_value("serial", $serial), "class='validate[required] pxs'") ?>
                                            <span class="bottom" style='color:red' >
                                                <?= form_error('serial') ?> 
                                            </span>


                                        </div>
                                    </div>

                                    <?php
                                }
                                ?>

                                <!--
                                <div class="span6" >
                                    <div class="span4">
                                <?= lang("transport_request_opertator") ?>
                                                        <font style='color:red'>*</font>
                                    </div>
                                    <div class="span8" >
                                <?= form_dropdown("safa_ito_id", $safa_ito, set_value("safa_ito_id", $safa_ito_id), " id='safa_ito_id' class='validate[required] chosen-select chosen-rtl input-huge '") ?>
                                        <span class="bottom" style='color:red' >
                                <?= form_error('safa_ito_id') ?> 
                                        </span>
                                    </div>
                                </div>
                                -->


                            </div>
                        </div>

                        <div class="span7" >
                            <div class="span3"><?= lang("notes") ?>
                            </div>
                            <div class="span9">
                                <textarea style='width:110% !important; height:50px' ></textarea>
                                <span class="bottom" style='color:red' >

                                </span>
                            </div>
                        </div>
                    </div> 
                </div>

            </div>
            <!--sec form start-->
            <div class="row-fluid" id="div_land_trip" <?php if ($erp_transportertype_id != 1 && $erp_transportertype_id != '') { ?> style="display: none;" <?php } else { ?> style="display: inline-block;" <?php } ?>>
                <div class="resalt-group">
                    <div class="wizerd-div"><a><?= lang('land_trip') ?></a></div>

                    <div class="row-form span12" style="border-top:1px solid #DDD;" >
                        <div class="span6" >
                            <div class="span4"><?= lang("land_going_erp_port_id") ?>
                                <font style='color:red'>*</font>   
                            </div>
                            <div class="span8">
                                <?= form_dropdown("land_going_erp_port_id", $land_erp_ports_sa, set_value("land_going_erp_port_id", $land_going_erp_port_id), "id='land_going_erp_port_id' class='chosen-select chosen-rtl validate[required]'") ?>
                                <span class="bottom" style='color:red' >
                                    <?= form_error('land_going_erp_port_id') ?> 
                                </span>
                            </div>
                        </div>
                        <div class="span6">
                            <div class="span4">
                                <?= lang("land_going_datetime") ?>
                            </div>
                            <div class="span8" >
                                <?= form_input("land_going_datetime", set_value("land_going_datetime", $land_going_datetime), " class='datetime' id='land_going_datetime'") ?>
                                <span class="bottom" style='color:red' >
                                    <?= form_error('land_going_datetime') ?> 
                                </span>
                            </div>
                        </div>
                    </div>


                    <div class="row-form" >
                        <div class="span6" >
                            <div class="span4"><?= lang("land_return_erp_port_id") ?>
                                <font style='color:red'>*</font>   
                            </div>
                            <div class="span8">
                                <?= form_dropdown("land_return_erp_port_id", $land_erp_ports_sa, set_value("land_return_erp_port_id", $land_return_erp_port_id), "id='land_return_erp_port_id' class='chosen-select chosen-rtl validate[required]'") ?>
                                <span class="bottom" style='color:red' >
                                    <?= form_error('land_return_erp_port_id') ?> 
                                </span>
                            </div>
                        </div>
                        <div class="span6">
                            <div class="span4">
                                <?= lang("land_return_datetime") ?>
                            </div>
                            <div class="span8" >
                                <?= form_input("land_return_datetime", set_value("land_return_datetime", $land_return_datetime), " class='datetime' id='land_return_datetime' ") ?>
                                <span class="bottom" style='color:red' >
                                    <?= form_error('land_return_datetime') ?> 
                                </span>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <!--sec form end-->
            <div class="row-fluid" id="div_sea_trip" <?php if ($erp_transportertype_id != 3) { ?> style="display: none;" <?php } else { ?> style="display: inline-block;" <?php } ?>>
                <div class="resalt-group">
                    <div class="wizerd-div"><a><?= lang('sea_trip') ?></a></div>

                    <div class="row-form" >
                        <div class="span6" >
                            <div class="span4"><?= lang("sea_going_erp_port_id") ?>
                                <font style='color:red'>*</font>   
                            </div>
                            <div class="span8">
                                <?= form_dropdown("sea_going_erp_port_id", $sea_erp_ports_sa, set_value("sea_going_erp_port_id", $sea_going_erp_port_id), "id='sea_going_erp_port_id' class='chosen-select chosen-rtl validate[required]'") ?>
                                <span class="bottom" style='color:red' >
                                    <?= form_error('sea_going_erp_port_id') ?> 
                                </span>
                            </div>
                        </div>
                        <div class="span6">
                            <div class="span4">
                                <?= lang("sea_going_datetime") ?>
                            </div>
                            <div class="span8" >
                                <?= form_input("sea_going_datetime", set_value("sea_going_datetime", $sea_going_datetime), " class='datetime' id='sea_going_datetime' ") ?>
                                <span class="bottom" style='color:red' >
                                    <?= form_error('sea_going_datetime') ?> 
                                </span>
                            </div>
                        </div>
                    </div>


                    <div class="row-form" >
                        <div class="span6" >
                            <div class="span4"><?= lang("sea_return_erp_port_id") ?>
                                <font style='color:red'>*</font>   
                            </div>
                            <div class="span8">
                                <?= form_dropdown("sea_return_erp_port_id", $sea_erp_ports_sa, set_value("sea_return_erp_port_id", $sea_return_erp_port_id), "id='sea_return_erp_port_id' class='chosen-select chosen-rtl validate[required]'") ?>
                                <span class="bottom" style='color:red' >
                                    <?= form_error('sea_return_erp_port_id') ?> 
                                </span>
                            </div>
                        </div>
                        <div class="span6">
                            <div class="span4">
                                <?= lang("sea_return_datetime") ?>
                            </div>
                            <div class="span8" >
                                <?= form_input("sea_return_datetime", set_value("sea_return_datetime", $sea_return_datetime), " class='datetime' id='sea_return_datetime' ") ?>
                                <span class="bottom" style='color:red' >
                                    <?= form_error('sea_return_datetime') ?> 
                                </span>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

            <div class="row-fluid" id="div_tbl_filghts" <?php if ($erp_transportertype_id != 2) { ?> style="display: none;" <?php } else { ?> style="display: inline-block;" <?php } ?>>
                <div class="resalt-group">
                    <div class="wizerd-div"><a><?= lang('flight_trip') ?></a></div>
                    <div class="label"><?= lang('going') ?></div>
                    <table id='div_trip_going_group'>
                        <thead>
                            <tr>
                                <th><?= lang('erp_path_type_id') ?></th>
                                <th><?= lang('flight_availabilities_airlines') ?></th>
                                <th><?= lang('flight_availabilities_flight_number') ?></th>
                                <th><?= lang('flight_availabilities_date') ?></th>
                                <th><?= lang('arrival_date') ?></th>
                                <th><?= lang('flight_availabilities_airports') ?></th>
                                <th><?= lang('flight_availabilities_seat_count') ?></th>
                                <th><a href="javascript:new_row(1);" class="btn" > <?php echo lang('global_add') ?> </a></th>

                            </tr>
                        </thead>
                        <tbody id="going_flight_tbody" class="cls_tbl_filghts">



                            <? if (isset($going_trip_flights_rows)) { ?>
                            <?
                            foreach ($going_trip_flights_rows as $value) {
                            $this->flight_availabilities_model->erp_flight_availability_id = $value->erp_flight_availability_id;

                            //$this->flight_availabilities_model->erp_path_type_id = $flight_trip_type;

                            $flight_availabilities_details_rows = $this->flight_availabilities_model->get_details_for_trip();

                            $ports = '';

                            $safa_externaltriptype_name = '';

                            $start_datetime = '';
                            $end_datetime = '';
                            $loop_counter = 0;

                            $erp_port_id_from = 0;
                            $erp_port_id_to = 0;
                            $start_ports_name = '';
                            $end_ports_name = '';
                            ?>


                            <?php
                            foreach ($flight_availabilities_details_rows as $flight_availabilities_details_row) {
                                if ($loop_counter == 0) {
                                    $safa_externaltriptype_name = $flight_availabilities_details_row->safa_externaltriptype_name;
                                    $erp_path_type_name = $flight_availabilities_details_row->erp_path_type_name;

                                    $erp_port_id_from = $flight_availabilities_details_row->erp_port_id_from;
                                    $start_ports_name = $flight_availabilities_details_row->start_ports_name;

                                    $erp_port_id_to = $flight_availabilities_details_row->erp_port_id_to;
                                    $end_ports_name = $flight_availabilities_details_row->end_ports_name;

                                    $ports = $flight_availabilities_details_row->start_ports_name . ' - ' . $flight_availabilities_details_row->end_ports_name;
                                    $start_datetime = $flight_availabilities_details_row->flight_date . ' - ' . $flight_availabilities_details_row->departure_time;
                                    $end_datetime = $flight_availabilities_details_row->flight_date . ' - ' . $flight_availabilities_details_row->arrival_time;
                                } else {
                                    $safa_externaltriptype_name = $safa_externaltriptype_name . ' <br/> ' . $flight_availabilities_details_row->safa_externaltriptype_name;

                                    $erp_port_id_to = $flight_availabilities_details_row->erp_port_id_to;
                                    $end_ports_name = $flight_availabilities_details_row->end_ports_name;

                                    $erp_port_id_from = $flight_availabilities_details_row->erp_port_id_from;
                                    $start_ports_name = $flight_availabilities_details_row->start_ports_name;

                                    $ports = $ports . ' <br/> ' . $flight_availabilities_details_row->start_ports_name . ' - ' . $flight_availabilities_details_row->end_ports_name;
                                    $end_datetime = $flight_availabilities_details_row->flight_date . ' - ' . $flight_availabilities_details_row->arrival_time;
                                }

                                $erp_port_hall_id_from = $flight_availabilities_details_row->erp_port_hall_id_from;
                                $erp_port_hall_id_to = $flight_availabilities_details_row->erp_port_hall_id_to;

                                $loop_counter++;

                                if ($flight_availabilities_details_row->safa_externaltriptype_id == 1) {
                                    ?>



                                    <tr rel='<?= $flight_availabilities_details_row->erp_flight_availabilities_detail_id ?>'>

                                                                                                                                                                                <!--<td><?php echo $flight_availabilities_details_row->safa_externaltriptype_name; ?>
                                                                                                                                                                                 <input type='hidden' name='safa_externaltriptype_name_<?= $flight_availabilities_details_row->erp_flight_availabilities_detail_id ?>'  id='safa_externaltriptype_name_<?= $flight_availabilities_details_row->erp_flight_availabilities_detail_id ?>'  value='<?= $flight_availabilities_details_row->safa_externaltriptype_name ?>'/>
                                                                                                                                                                                </td>

                                        --><td>

                                            <input type='hidden' name='erp_flight_availabilities_detail_id[<?= $flight_availabilities_details_row->erp_flight_availabilities_detail_id ?>]'  id='erp_flight_availabilities_detail_id[<?= $flight_availabilities_details_row->erp_flight_availabilities_detail_id ?>]'  value='<?= $flight_availabilities_details_row->erp_flight_availabilities_detail_id ?>'/>

                                            <!--                                            <?php echo $flight_availabilities_details_row->erp_path_type_name; ?>-->
                                            <input type='hidden' name='safa_externaltriptype_name_<?= $flight_availabilities_details_row->erp_flight_availabilities_detail_id ?>'  id='safa_externaltriptype_name_<?= $flight_availabilities_details_row->erp_flight_availabilities_detail_id ?>'  value='<?= $flight_availabilities_details_row->erp_path_type_name ?>'/>
                                            <input type='hidden' name='safa_externaltriptype_id[<?= $flight_availabilities_details_row->erp_flight_availabilities_detail_id ?>]'  id='safa_externaltriptype_id[<?= $flight_availabilities_details_row->erp_flight_availabilities_detail_id ?>]'  value='<?= $flight_availabilities_details_row->safa_externaltriptype_id ?>'/>

                                            <input type="hidden" pathrel='<?php echo $flight_availabilities_details_row->erp_flight_availabilities_detail_id; ?>' class="erp_path_type" name="erp_path_type_id[<?= $flight_availabilities_details_row->erp_flight_availabilities_detail_id ?>]" id="erp_path_type_id[<?= $flight_availabilities_details_row->erp_flight_availabilities_detail_id ?>]"  value="<?= $flight_availabilities_details_row->erp_path_type_id ?>" />
                                            <?= form_dropdown('erp_path_type_id[' . $flight_availabilities_details_row->erp_flight_availabilities_detail_id . ']', $erp_path_types, set_value("erp_path_type_id[" . $flight_availabilities_details_row->erp_flight_availabilities_detail_id . "]", $flight_availabilities_details_row->erp_path_type_id), 'class="validate[required] erp_path_type" style="width:70px" pathrel="' . $flight_availabilities_details_row->erp_flight_availabilities_detail_id . '" id="erp_path_type_id[' . $flight_availabilities_details_row->erp_flight_availabilities_detail_id . ']"') ?>

                                        </td>




                                        <td>
                                            <table style='border:0px;'>
                                                <tr inner_rel='<?= $flight_availabilities_details_row->erp_flight_availabilities_detail_id ?>'><td style='border:0px;'>
                                                        <?= form_dropdown('safa_transporter_code[' . $flight_availabilities_details_row->erp_flight_availabilities_detail_id . ']', $flight_transporters, set_value("safa_transporter_code['.$flight_availabilities_details_row->erp_flight_availabilities_detail_id.']", $flight_availabilities_details_row->safa_transporter_code), ' flight_states_rel="' . $flight_availabilities_details_row->erp_flight_availabilities_detail_id . '"  class=" chosen-select chosen-rtl validate[required]" style="width:60px" id="safa_transporter_code[' . $flight_availabilities_details_row->erp_flight_availabilities_detail_id . ']"') ?>
                                                        <input type='hidden' name='last_going_safa_transporter_code'  id='last_going_safa_transporter_code'  value='<?= $flight_availabilities_details_row->safa_transporter_code ?>'/>


                                                    </td>
                                                    <td style='border:0px;'>
                                                        <a href="javascript:void(0);" onclick="add_transporter();"  class="btn Fleft " > <span class="icon-plus"></span></a>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>

                                        <td><?= form_input('flight_number[' . $flight_availabilities_details_row->erp_flight_availabilities_detail_id . ']', set_value("flight_number['.$flight_availabilities_details_row->erp_flight_availabilities_detail_id.']", $flight_availabilities_details_row->flight_number), ' flight_states_rel="' . $flight_availabilities_details_row->erp_flight_availabilities_detail_id . '"  class="validate[required]" style="width:40px" id="flightno[' . $flight_availabilities_details_row->erp_flight_availabilities_detail_id . ']"') ?></td>


                                        <td>

                                                                                                                                                                                <!--<input type='hidden' name='departure_date_<?= $flight_availabilities_details_row->erp_flight_availabilities_detail_id ?>'  id='departure_date_<?= $flight_availabilities_details_row->erp_flight_availabilities_detail_id ?>'  value='<?= $start_datetime ?>'/>
                                            <?php echo $start_datetime; ?>
                                            -->
                                            <input type="text" flight_states_rel="<?php echo $flight_availabilities_details_row->erp_flight_availabilities_detail_id; ?>"  class="datetime" name='flight_date[<?= $flight_availabilities_details_row->erp_flight_availabilities_detail_id ?>]'  id='flight_date[<?= $flight_availabilities_details_row->erp_flight_availabilities_detail_id ?>]'  value='<?= $flight_availabilities_details_row->flight_date . ' ' . get_time($flight_availabilities_details_row->departure_time) ?>' style="width:100px"/>
                                            <!--                                                <?php echo $flight_availabilities_details_row->flight_date . ' ' . $flight_availabilities_details_row->departure_time; ?>-->



                                        </td>


                                        <td>

                                                                                                                                                        <!--<input type='hidden' name='departure_date_<?= $flight_availabilities_details_row->erp_flight_availabilities_detail_id ?>'  id='departure_date_<?= $flight_availabilities_details_row->erp_flight_availabilities_detail_id ?>'  value='<?= $start_datetime ?>'/>
                                            <?php echo $start_datetime; ?>
                                            -->
                                            <input type='text' class="datetime" arriverel='<?php echo $flight_availabilities_details_row->erp_flight_availabilities_detail_id; ?>' name='flight_arrival_date[<?= $flight_availabilities_details_row->erp_flight_availabilities_detail_id ?>]'  id='flight_arrival_date[<?= $flight_availabilities_details_row->erp_flight_availabilities_detail_id ?>]'  value='<?= $flight_availabilities_details_row->arrival_date . ' ' . get_time($flight_availabilities_details_row->arrival_time) ?>' style="width:100px"/>
                                            <!--                                                <?php echo $flight_availabilities_details_row->arrival_date . ' ' . $flight_availabilities_details_row->arrival_time; ?>-->


                                        </td>

                                        <td>
        <!--                                                <input type='hidden' name='erp_port_id_from[<?= $flight_availabilities_details_row->erp_flight_availabilities_detail_id ?>]'  id='erp_port_id_from[<?= $flight_availabilities_details_row->erp_flight_availabilities_detail_id ?>]'  value='<?= $flight_availabilities_details_row->erp_port_id_from ?>'/>-->
        <!--                                                <input type='hidden' name='erp_port_id_to[<?= $flight_availabilities_details_row->erp_flight_availabilities_detail_id ?>]'  id='erp_port_id_to[<?= $flight_availabilities_details_row->erp_flight_availabilities_detail_id ?>]'  value='<?= $flight_availabilities_details_row->erp_port_id_to ?>'/>-->


                                            <?= form_dropdown('erp_port_id_from[' . $flight_availabilities_details_row->erp_flight_availabilities_detail_id . ']', $erp_ports, set_value("erp_port_id_from[" . $flight_availabilities_details_row->erp_flight_availabilities_detail_id . "]", $erp_port_id_from), ' erp_port_id_from_rel="' . $flight_availabilities_details_row->erp_flight_availabilities_detail_id . '" class="validate[required]" style="width:60px !important" id="erp_port_id_from[' . $flight_availabilities_details_row->erp_flight_availabilities_detail_id . ']"') ?>
                                            <!--												<?= form_dropdown('erp_port_hall_id_from[' . $flight_availabilities_details_row->erp_flight_availabilities_detail_id . ']', $erp_ports_halls, set_value("erp_port_hall_id_from[" . $flight_availabilities_details_row->erp_flight_availabilities_detail_id . "]", $erp_port_hall_id_from), ' erp_port_hall_id_from_rel="' . $flight_availabilities_details_row->erp_flight_availabilities_detail_id . '" class="validate[required]" style="width:120px !important" id="erp_port_hall_id_from[' . $flight_availabilities_details_row->erp_flight_availabilities_detail_id . ']"') ?>-->
                                            <br/>
                                            <?= form_dropdown('erp_port_id_to[' . $flight_availabilities_details_row->erp_flight_availabilities_detail_id . ']', $erp_sa_ports, set_value("erp_port_id_to[" . $flight_availabilities_details_row->erp_flight_availabilities_detail_id . "]", $erp_port_id_to), ' erp_port_id_to_rel="' . $flight_availabilities_details_row->erp_flight_availabilities_detail_id . '" class="validate[required]" style="width:60px !important" id="erp_port_id_to[' . $flight_availabilities_details_row->erp_flight_availabilities_detail_id . ']"') ?>
                                            <?= form_dropdown('erp_port_hall_id_to[' . $flight_availabilities_details_row->erp_flight_availabilities_detail_id . ']', $erp_sa_ports_halls, set_value("erp_port_hall_id_to[" . $flight_availabilities_details_row->erp_flight_availabilities_detail_id . "]", $erp_port_hall_id_to), '  erp_port_hall_id_to_rel="' . $flight_availabilities_details_row->erp_flight_availabilities_detail_id . '" class="validate[required]" style="width:120px !important" id="erp_port_hall_id_to[' . $flight_availabilities_details_row->erp_flight_availabilities_detail_id . ']"') ?>


                                            <input type='hidden' name='start_ports_name_<?= $flight_availabilities_details_row->erp_flight_availabilities_detail_id ?>'  id='start_ports_name_<?= $flight_availabilities_details_row->erp_flight_availabilities_detail_id ?>'  value='<?= $flight_availabilities_details_row->start_ports_name; ?>'/>
                                            <input type='hidden' name='end_ports_name_<?= $flight_availabilities_details_row->erp_flight_availabilities_detail_id ?>'  id='end_ports_name_<?= $flight_availabilities_details_row->erp_flight_availabilities_detail_id ?>'  value='<?= $flight_availabilities_details_row->end_ports_name; ?>'/>


                                                                                                <!--                                                <?= $flight_availabilities_details_row->start_ports_name ?> - <?= $flight_availabilities_details_row->end_ports_name ?>-->
                                            <input type='hidden' name='ports_name_<?= $flight_availabilities_details_row->erp_flight_availabilities_detail_id ?>'  id='ports_name_<?= $flight_availabilities_details_row->erp_flight_availabilities_detail_id ?>'  value='<?= $flight_availabilities_details_row->start_ports_name ?> - <?= $flight_availabilities_details_row->end_ports_name ?>'/>



                                        </td>

                                        <td>
                                            <input type='hidden' name='seats_count[<?= $flight_availabilities_details_row->erp_flight_availabilities_detail_id ?>]'  id='seats_count[<?= $flight_availabilities_details_row->erp_flight_availabilities_detail_id ?>]'  value='<?= $flight_availabilities_details_row->available_seats_count ?>'/>
                                            <input type='text' style="width:50px" name='available_seats_count_text_<?= $flight_availabilities_details_row->erp_flight_availabilities_detail_id ?>'  id='available_seats_count_text_<?= $flight_availabilities_details_row->erp_flight_availabilities_detail_id ?>'  value='<?= $flight_availabilities_details_row->available_seats_count ?>' />

                                            <!-- <?= $value->available_seats_count ?> -->

                                        </td>

                                        <td><a href="javascript:void(0);" onclick="delete_flights('<?= $flight_availabilities_details_row->erp_flight_availabilities_detail_id ?>')"><span class="icon-trash"></span> </a></td>

                                    </tr>


                                    <?php
                                }
                            }
                            ?>



                            <? } ?>
                            <? } ?>

                        </tbody>

                    </table>
                    <div class="clear clearfix" style="height: 5px;"> </div>
                    <div class="label clear"><?= lang('return') ?></div>
                    <table id='div_trip_return_group'>
                        <thead>
                            <tr>
                                <th><?= lang('erp_path_type_id') ?></th>
                                <th><?= lang('flight_availabilities_airlines') ?></th>
                                <th><?= lang('flight_availabilities_flight_number') ?></th>
                                <th><?= lang('flight_availabilities_date') ?></th>
                                <th><?= lang('arrival_date') ?></th>
                                <th><?= lang('flight_availabilities_airports') ?></th>
                                <th><?= lang('flight_availabilities_seat_count') ?></th>

                                <th><a href="javascript:new_row(2);" class="btn" > <?php echo lang('global_add') ?> </a></th>

                            </tr>
                        </thead>
                        <tbody id="return_flight_tbody" class="cls_tbl_filghts">

                            <? if (isset($going_trip_flights_rows)) { ?>
                            <?
                            foreach ($going_trip_flights_rows as $value) {
                            $this->flight_availabilities_model->erp_flight_availability_id = $value->erp_flight_availability_id;

                            //$this->flight_availabilities_model->erp_path_type_id = $flight_trip_type;

                            $flight_availabilities_details_rows = $this->flight_availabilities_model->get_details_for_trip();

                            $ports = '';

                            $safa_externaltriptype_name = '';

                            $start_datetime = '';
                            $end_datetime = '';
                            $loop_counter = 0;

                            $erp_port_id_from = 0;
                            $erp_port_id_to = 0;
                            $start_ports_name = '';
                            $end_ports_name = '';
                            ?>


                            <?php
                            foreach ($flight_availabilities_details_rows as $flight_availabilities_details_row) {
                                if ($loop_counter == 0) {
                                    $safa_externaltriptype_name = $flight_availabilities_details_row->safa_externaltriptype_name;
                                    $erp_path_type_name = $flight_availabilities_details_row->erp_path_type_name;

                                    $erp_port_id_from = $flight_availabilities_details_row->erp_port_id_from;
                                    $start_ports_name = $flight_availabilities_details_row->start_ports_name;

                                    $erp_port_id_to = $flight_availabilities_details_row->erp_port_id_to;
                                    $end_ports_name = $flight_availabilities_details_row->end_ports_name;

                                    $ports = $flight_availabilities_details_row->start_ports_name . ' - ' . $flight_availabilities_details_row->end_ports_name;
                                    $start_datetime = $flight_availabilities_details_row->flight_date . ' - ' . $flight_availabilities_details_row->departure_time;
                                    $end_datetime = $flight_availabilities_details_row->flight_date . ' - ' . $flight_availabilities_details_row->arrival_time;
                                } else {
                                    $safa_externaltriptype_name = $safa_externaltriptype_name . ' <br/> ' . $flight_availabilities_details_row->safa_externaltriptype_name;

                                    $erp_port_id_from = $flight_availabilities_details_row->erp_port_id_from;
                                    $start_ports_name = $flight_availabilities_details_row->start_ports_name;

                                    $erp_port_id_to = $flight_availabilities_details_row->erp_port_id_to;
                                    $end_ports_name = $flight_availabilities_details_row->end_ports_name;

                                    $ports = $ports . ' <br/> ' . $flight_availabilities_details_row->start_ports_name . ' - ' . $flight_availabilities_details_row->end_ports_name;
                                    $end_datetime = $flight_availabilities_details_row->flight_date . ' - ' . $flight_availabilities_details_row->arrival_time;
                                }

                                $erp_port_hall_id_from = $flight_availabilities_details_row->erp_port_hall_id_from;
                                $erp_port_hall_id_to = $flight_availabilities_details_row->erp_port_hall_id_to;

                                $loop_counter++;

                                if ($flight_availabilities_details_row->safa_externaltriptype_id == 2) {
                                    ?>



                                    <tr  rel='<?= $flight_availabilities_details_row->erp_flight_availabilities_detail_id ?>'>

                                                                                                                                <!--<td><?php echo $flight_availabilities_details_row->safa_externaltriptype_name; ?>
                                                                                                                                 <input type='hidden' name='safa_externaltriptype_name_<?= $flight_availabilities_details_row->erp_flight_availabilities_detail_id ?>'  id='safa_externaltriptype_name_<?= $flight_availabilities_details_row->erp_flight_availabilities_detail_id ?>'  value='<?= $flight_availabilities_details_row->safa_externaltriptype_name ?>'/>
                                                                                                                                </td>

                                        --><td>

                                            <input type='hidden' name='erp_flight_availabilities_detail_id[<?= $flight_availabilities_details_row->erp_flight_availabilities_detail_id ?>]'  id='erp_flight_availabilities_detail_id[<?= $flight_availabilities_details_row->erp_flight_availabilities_detail_id ?>]'  value='<?= $flight_availabilities_details_row->erp_flight_availabilities_detail_id ?>'/>

                                            <input type="hidden" name="trip_going_erp_flight_availability_id<?= $flight_availabilities_details_row->erp_flight_availabilities_detail_id ?>" id="trip_going_erp_flight_availability_id<?= $flight_availabilities_details_row->erp_flight_availabilities_detail_id ?>"  value="<?= $flight_availabilities_details_row->erp_flight_availabilities_detail_id ?>" />


                                            <!--                                                <?php echo $flight_availabilities_details_row->erp_path_type_name; ?>-->
                                            <input type='hidden' name='safa_externaltriptype_name_<?= $flight_availabilities_details_row->erp_flight_availabilities_detail_id ?>'  id='safa_externaltriptype_name_<?= $flight_availabilities_details_row->erp_flight_availabilities_detail_id ?>'  value='<?= $flight_availabilities_details_row->erp_path_type_name ?>'/>
                                            <input type='hidden' name='safa_externaltriptype_id[<?= $flight_availabilities_details_row->erp_flight_availabilities_detail_id ?>]'  id='safa_externaltriptype_id[<?= $flight_availabilities_details_row->erp_flight_availabilities_detail_id ?>]'  value='<?= $flight_availabilities_details_row->safa_externaltriptype_id ?>'/>


                                            <?= form_dropdown('erp_path_type_id[' . $flight_availabilities_details_row->erp_flight_availabilities_detail_id . ']', $erp_path_types, set_value("erp_path_type_id[" . $flight_availabilities_details_row->erp_flight_availabilities_detail_id . "]", $flight_availabilities_details_row->erp_path_type_id), 'class="validate[required] erp_path_type" style="width:70px" pathrel="' . $flight_availabilities_details_row->erp_flight_availabilities_detail_id . '" id="erp_path_type_id[' . $flight_availabilities_details_row->erp_flight_availabilities_detail_id . ']"') ?>

                                        </td>




                                        <td>
                                            <table style='border:0px;'>
                                                <tr inner_rel='<?= $flight_availabilities_details_row->erp_flight_availabilities_detail_id ?>'><td style='border:0px;'>
                                                        <?= form_dropdown('safa_transporter_code[' . $flight_availabilities_details_row->erp_flight_availabilities_detail_id . ']', $flight_transporters, set_value("safa_transporter_code['.$flight_availabilities_details_row->erp_flight_availabilities_detail_id.']", $flight_availabilities_details_row->safa_transporter_code), ' flight_states_rel="' . $flight_availabilities_details_row->erp_flight_availabilities_detail_id . '"  class=" chosen-select chosen-rtl validate[required]" style="width:60px" id="safa_transporter_code[' . $flight_availabilities_details_row->erp_flight_availabilities_detail_id . ']"') ?>

                                                        <input type='hidden' name='last_return_safa_transporter_code'  id='last_return_safa_transporter_code'  value='<?= $flight_availabilities_details_row->safa_transporter_code ?>'/>

                                                    </td>
                                                    <td style='border:0px;'>
                                                        <a href="javascript:void(0);" onclick="add_transporter();"  class="btn Fleft " > <span class="icon-plus"></span></a>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>

                                        <td><?= form_input('flight_number[' . $flight_availabilities_details_row->erp_flight_availabilities_detail_id . ']', set_value("flight_number['.$flight_availabilities_details_row->erp_flight_availabilities_detail_id.']", $flight_availabilities_details_row->flight_number), ' flight_states_rel="' . $flight_availabilities_details_row->erp_flight_availabilities_detail_id . '" class="validate[required]" style="width:40px" id="flightno[' . $flight_availabilities_details_row->erp_flight_availabilities_detail_id . ']"') ?></td>



                                        <td>

                                                                                                                                                                <!--<input type='hidden' name='departure_date_<?= $flight_availabilities_details_row->erp_flight_availabilities_detail_id ?>'  id='departure_date_<?= $flight_availabilities_details_row->erp_flight_availabilities_detail_id ?>'  value='<?= $start_datetime ?>'/>
                                            <?php echo $start_datetime; ?>
                                            -->
                                            <input type='text' class="datetime" flight_states_rel="<?php echo $flight_availabilities_details_row->erp_flight_availabilities_detail_id; ?>"  name='flight_date[<?= $flight_availabilities_details_row->erp_flight_availabilities_detail_id ?>]'  id='flight_date[<?= $flight_availabilities_details_row->erp_flight_availabilities_detail_id ?>]'  value='<?= $flight_availabilities_details_row->flight_date . ' ' . get_time($flight_availabilities_details_row->departure_time) ?>' style="width:100px"/>
                                            <!--                                                <?php echo $flight_availabilities_details_row->flight_date . ' ' . $flight_availabilities_details_row->departure_time; ?>-->


                                        </td>

                                        <td>

                                                                                                                                                                <!--<input type='hidden' name='departure_date_<?= $flight_availabilities_details_row->erp_flight_availabilities_detail_id ?>'  id='departure_date_<?= $flight_availabilities_details_row->erp_flight_availabilities_detail_id ?>'  value='<?= $start_datetime ?>'/>
                                            <?php echo $start_datetime; ?>
                                            -->
                                            <input type='text' class="datetime" name='flight_arrival_date[<?= $flight_availabilities_details_row->erp_flight_availabilities_detail_id ?>]'  id='flight_arrival_date[<?= $flight_availabilities_details_row->erp_flight_availabilities_detail_id ?>]'  value='<?= $flight_availabilities_details_row->arrival_date . ' ' . get_time($flight_availabilities_details_row->arrival_time) ?>' style="width:100px"/>
                                            <!--                                                <?php echo $flight_availabilities_details_row->arrival_date . ' ' . $flight_availabilities_details_row->arrival_time; ?>-->



                                        </td>


                                        <td>
        <!--                                                <input type='hidden' name='erp_port_id_from[<?= $flight_availabilities_details_row->erp_flight_availabilities_detail_id ?>]'  id='erp_port_id_from[<?= $flight_availabilities_details_row->erp_flight_availabilities_detail_id ?>]'  value='<?= $flight_availabilities_details_row->erp_port_id_from ?>'/>-->
        <!--                                                <input type='hidden' name='erp_port_id_to[<?= $flight_availabilities_details_row->erp_flight_availabilities_detail_id ?>]'  id='erp_port_id_to[<?= $flight_availabilities_details_row->erp_flight_availabilities_detail_id ?>]'  value='<?= $flight_availabilities_details_row->erp_port_id_to ?>'/>-->

                                            <?= form_dropdown('erp_port_id_from[' . $flight_availabilities_details_row->erp_flight_availabilities_detail_id . ']', $erp_sa_ports, set_value("erp_port_id_from[" . $flight_availabilities_details_row->erp_flight_availabilities_detail_id . "]", $erp_port_id_from), ' erp_port_id_from_rel="' . $flight_availabilities_details_row->erp_flight_availabilities_detail_id . '" class="validate[required]" style="width:60px !important" id="erp_port_id_from[' . $flight_availabilities_details_row->erp_flight_availabilities_detail_id . ']"') ?>
                                            <?= form_dropdown('erp_port_hall_id_from[' . $flight_availabilities_details_row->erp_flight_availabilities_detail_id . ']', $erp_sa_ports_halls, set_value("erp_port_hall_id_from[" . $flight_availabilities_details_row->erp_flight_availabilities_detail_id . "]", $erp_port_hall_id_from), ' erp_port_hall_id_from_rel="' . $flight_availabilities_details_row->erp_flight_availabilities_detail_id . '" class="validate[required]" style="width:120px !important" id="erp_port_hall_id_from[' . $flight_availabilities_details_row->erp_flight_availabilities_detail_id . ']"') ?>
                                            <br/>
                                            <?= form_dropdown('erp_port_id_to[' . $flight_availabilities_details_row->erp_flight_availabilities_detail_id . ']', $erp_ports, set_value("erp_port_id_to[" . $flight_availabilities_details_row->erp_flight_availabilities_detail_id . "]", $erp_port_id_to), '   erp_port_id_to_rel="' . $flight_availabilities_details_row->erp_flight_availabilities_detail_id . '" class="validate[required]" style="width:60px !important" id="erp_port_id_to[' . $flight_availabilities_details_row->erp_flight_availabilities_detail_id . ']"') ?>
                                            <!--												<?= form_dropdown('erp_port_hall_id_to[' . $flight_availabilities_details_row->erp_flight_availabilities_detail_id . ']', $erp_ports_halls, set_value("erp_port_hall_id_to[" . $flight_availabilities_details_row->erp_flight_availabilities_detail_id . "]", $erp_port_hall_id_to), ' erp_port_hall_id_to_rel="' . $flight_availabilities_details_row->erp_flight_availabilities_detail_id . '" class="validate[required]" style="width:120px !important" id="erp_port_hall_id_to[' . $flight_availabilities_details_row->erp_flight_availabilities_detail_id . ']"') ?>-->


                                            <input type='hidden' name='start_ports_name_<?= $flight_availabilities_details_row->erp_flight_availabilities_detail_id ?>'  id='start_ports_name_<?= $flight_availabilities_details_row->erp_flight_availabilities_detail_id ?>'  value='<?= $flight_availabilities_details_row->start_ports_name; ?>'/>
                                            <input type='hidden' name='end_ports_name_<?= $flight_availabilities_details_row->erp_flight_availabilities_detail_id ?>'  id='end_ports_name_<?= $flight_availabilities_details_row->erp_flight_availabilities_detail_id ?>'  value='<?= $flight_availabilities_details_row->end_ports_name; ?>'/>


                                                                                                <!--                                                <?= $flight_availabilities_details_row->start_ports_name ?> - <?= $flight_availabilities_details_row->end_ports_name ?>-->
                                            <input type='hidden' name='ports_name_<?= $flight_availabilities_details_row->erp_flight_availabilities_detail_id ?>'  id='ports_name_<?= $flight_availabilities_details_row->erp_flight_availabilities_detail_id ?>'  value='<?= $flight_availabilities_details_row->start_ports_name ?> - <?= $flight_availabilities_details_row->end_ports_name ?>'/>


                                            <!--<?php echo $ports; ?>
                                            <input type='hidden' name='ports_name_<?= $flight_availabilities_details_row->erp_flight_availabilities_detail_id ?>'  id='ports_name_<?= $flight_availabilities_details_row->erp_flight_availabilities_detail_id ?>'  value='<?= $ports ?>'/>-->

                                        </td>


                                        <td>
                                            <input type='hidden' name='seats_count[<?= $flight_availabilities_details_row->erp_flight_availabilities_detail_id ?>]'  id='seats_count[<?= $flight_availabilities_details_row->erp_flight_availabilities_detail_id ?>]'  value='<?= $flight_availabilities_details_row->available_seats_count ?>'/>
                                            <input type='text' style="width:50px" name='available_seats_count_text_<?= $flight_availabilities_details_row->erp_flight_availabilities_detail_id ?>'  id='available_seats_count_text_<?= $flight_availabilities_details_row->erp_flight_availabilities_detail_id ?>'  value='<?= $flight_availabilities_details_row->available_seats_count ?>' />

                                            <!-- <?= $value->available_seats_count ?> -->

                                        </td>


                                        <td><a href="javascript:void(0);" onclick="delete_flights('<?= $flight_availabilities_details_row->erp_flight_availabilities_detail_id ?>')"><span class="icon-trash"></span> </a></td>

                                    </tr>


                                    <?php
                                }
                            }
                            ?>



                            <? } ?>
                            <? } ?>

                        </tbody>

                    </table>
                </div>      
            </div>



            <div class="row-fluid" id="div_tbl_hotels">
                <fieldset class="resalt-group">
                    <div class="wizerd-div"><a> <?= lang('hotels') ?></a></div>
                    <table id='div_hotels'>
                        <thead>
                            <tr>
                                <th><?= lang('city') ?></th>
                                <th><?=
                                    lang('hotel')
                                    ?></th>
                                <th><?=
                                    lang('entry_date')
                                    ?></th>
                                <th><?=
                                    lang('nights_count')
                                    ?></th>
                                <th><?=
                                    lang('exit_date')
                                    ?></th>

                                <th><a href="javascript:void(0)" onclick="new_hotel();" class="btn" > <?php echo lang('global_add') ?> </a></th>

                            </tr>
                        </thead>
                        <tbody class="internalhotels  ">
                            <?php
                            $internalpassages_hotels_count = 0;

                            $internalpassages_hotels_counter = 1;
                            if (isset($internalpassages_hotels)) {
                                $internalpassages_hotels_count = count($internalpassages_hotels);

                                foreach ($internalpassages_hotels as $internalpassages_hotel) {

                                    //$this->internalpassages_model->safa_internalsegment_id = $internalpassages_hotel->safa_internalsegment_id+1;
                                    //$next_internalpassages_row = $this->internalpassages_model->get();
                                    //$this->internalpassages_model->safa_internalsegment_id = false;

                                    $next_internalpassages_row = array();
                                    if (isset($internalpassages_hotels[$internalpassages_hotels_counter])) {
                                        $next_internalpassages_row = $internalpassages_hotels[$internalpassages_hotels_counter];
                                    }

                                    //if(count($next_internalpassages_row)>0) {
                                    ?>

                                    <tr hrel="<?php echo $internalpassages_hotel->safa_internalsegment_id ?>">
                                        <td>
                                            <input type='hidden' name='hotel_safa_internalsegment_id[<?= $internalpassages_hotel->safa_internalsegment_id ?>]'  id='hotel_safa_internalsegment_id[<?= $internalpassages_hotel->safa_internalsegment_id ?>]'  value='<?php echo $internalpassages_hotel->safa_internalsegment_id; ?>'/>

                                            <?php echo form_dropdown('erp_city_id[' . $internalpassages_hotel->safa_internalsegment_id . ']', $cities, set_value('erp_city_id', $internalpassages_hotel->erp_city_id), ' cityrel="' . $internalpassages_hotel->safa_internalsegment_id . '" class="validate[required] chosen-select chosen-rtl input-full"  tabindex="4" id="erp_city_id" data-placeholder="' . lang('global_all') . '"') ?>
                                        </td>
                                        <td>


                                            <table style='border:0px;' ><tr inner_hrel="<?php echo $internalpassages_hotel->safa_internalsegment_id ?>"><td style='border:0px;'>
                                                        <?php
                                                        $row_hotels = ddgen('erp_hotels', array('erp_hotel_id', name()), array('erp_city_id' => $internalpassages_hotel->erp_city_id), array(name(), 'ASC'), TRUE);

                                                        echo form_dropdown('erp_hotel_id[' . $internalpassages_hotel->safa_internalsegment_id . ']', $row_hotels, set_value('erp_hotel_id', $internalpassages_hotel->erp_end_hotel_id), ' hotelrel="' . $internalpassages_hotel->safa_internalsegment_id . '" class=" chosen-select chosen-rtl input-full erp_hotel_id"  tabindex="4" id="erp_hotel_id[' . $internalpassages_hotel->safa_internalsegment_id . ']" ')
                                                        ?>
                                                    </td><td style='border:0px;'>
                                                        <a href="javascript:void(0);" onclick="add_hotel(<?php echo $internalpassages_hotel->safa_internalsegment_id ?>);"  class="btn Fleft " > <span class="icon-plus"></span></a>
                                                    </td></tr>
                                            </table>  

                                        </td>

                                        <td>
                                            <?php
                                            echo form_input('arrival_date[' . $internalpassages_hotel->safa_internalsegment_id . ']'
                                                    , set_value('arrival_date[' . $internalpassages_hotel->safa_internalsegment_id . ']', date('Y-m-d', strtotime($internalpassages_hotel->end_datetime)))
                                                    , 'class="input-huge arrival_date' . $internalpassages_hotel->safa_internalsegment_id . '" id="arrival_date[' . $internalpassages_hotel->safa_internalsegment_id . ']" startrel="' . $internalpassages_hotel->safa_internalsegment_id . '" style="width:120px" placeholder="' . lang('entry_date') . '"')
                                            ?>

                                            <script>
                                                        $(".arrival_date<?php echo $internalpassages_hotel->safa_internalsegment_id; ?>").datepicker({
                                                dateFormat: 'yy-mm-dd',
                                                        controlType: 'select',
                                                        timeFormat: 'HH:mm',
                                                        onClose: function(selectedDate) {
                                                        $('.arrival_date<?php echo $internalpassages_hotel->safa_internalsegment_id; ?>').datepicker('option', 'minDate', selectedDate);
                                                        }
                                                });</script>
                                        </td>

                                        <td>
                                            <? 


                                            $nights_count=0;
                                            if(count($next_internalpassages_row)>0) {
                                            if (strlen($internalpassages_hotel->end_datetime) && strlen($next_internalpassages_row->end_datetime)) {
                                            $nights_count = ceil((strtotime($next_internalpassages_row->end_datetime) - strtotime($internalpassages_hotel->end_datetime)) / (60 * 60 * 24)); 
                                            } 
                                            } else {
                                            $this->internalpassages_model->safa_internalsegmenttype_id = 2;
                                            $leaving_internalpassages_rows = $this->internalpassages_model->get();
                                            if(count($leaving_internalpassages_rows)>0) {
                                            $nights_count = ceil((strtotime($leaving_internalpassages_rows[0]->end_datetime) - strtotime($internalpassages_hotel->end_datetime)) / (60 * 60 * 24));
                                            } 
                                            }
                                            ?>
                                            <?php
                                            echo form_input('nights[' . $internalpassages_hotel->safa_internalsegment_id . ']'
                                                    , set_value('nights[' . $internalpassages_hotel->safa_internalsegment_id . ']', $nights_count)
                                                    , 'class="input-huge" style="width:100px" nightsrel="' . $internalpassages_hotel->safa_internalsegment_id . '" onchange="fix_dates();" ')
                                            ?>
                                        </td>

                                        <td>

                                            <?php
                                            $current_row_exit_date = '';
                                            if (count($next_internalpassages_row) > 0) {
                                                $current_row_exit_date = date('Y-m-d', strtotime($next_internalpassages_row->end_datetime));
                                            } else if (isset($leaving_internalpassages_rows)) {
                                                if (count($leaving_internalpassages_rows) > 0) {
                                                    $current_row_exit_date = date('Y-m-d', strtotime($leaving_internalpassages_rows[0]->end_datetime));
                                                }
                                            }

                                            if ($internalpassages_hotels_counter == $internalpassages_hotels_count) {
                                                ?>
                                                <input type='hidden' name='last_exit_date'  id='last_exit_date'  value='<?= $current_row_exit_date ?>'/>
                                            <?php } ?>

                                            <?php
                                            echo form_input('exit_date[' . $internalpassages_hotel->safa_internalsegment_id . ']'
                                                    , set_value('exit_date[' . $internalpassages_hotel->safa_internalsegment_id . ']', $current_row_exit_date)
                                                    , 'class="input-huge exit_date' . $internalpassages_hotel->safa_internalsegment_id . '" id="exit_date[' . $internalpassages_hotel->safa_internalsegment_id . ']" endrel="' . $internalpassages_hotel->safa_internalsegment_id . '" style="width:120px" placeholder="' . lang('exit_date') . '"')
                                            ?>

                                            <script>
                                                        $(".exit_date<?php echo $internalpassages_hotel->safa_internalsegment_id; ?>").datepicker({
                                                dateFormat: 'yy-mm-dd',
                                                        controlType: 'select',
                                                        timeFormat: 'HH:mm',
                                                        onClose: function(selectedDate) {
                                                        $('.exit_date<?php echo $internalpassages_hotel->safa_internalsegment_id; ?>').datepicker('option', 'maxDate', selectedDate);
                                                        }
                                                });</script>
                                        </td>


                                        <td class="TAC">
                                            <a href="javascript:void(0)" onclick="remove_hrow(<?php echo $internalpassages_hotel->safa_internalsegment_id ?>)"><span class="icon-trash"></span></a>
                                        </td>
                                    </tr>





                                    <tr rel="<?php echo $internalpassages_hotel->safa_internalsegment_id ?>" class="something">
                                        <td colspan="6">
                                            <table >
                                                <tr >
                                                    <td rowspan="2">
                                                        <span class="FRight"><?= lang('rooms_count') ?></span>
                                                    </td>

                                                    <? if (check_array($erp_hotelroomsizes)) : ?>
                                                    <? foreach ($erp_hotelroomsizes as $erp_hotelroomsize) : ?>
                                                    <td>
                                                        <?php
                                                        $current_hotels_rooms_count = 0;
                                                        foreach ($safa_trip_internaltrips_hotels_rows as $safa_trip_internaltrips_hotels_row) {
                                                            //echo $safa_trip_internaltrips_hotels_row->safa_internalsegment_id.' - '. $internalpassages_hotel->safa_internalsegment_id .' - '. $safa_trip_internaltrips_hotels_row->erp_hotelroomsize_id.' - '. $erp_hotelroomsize->erp_hotelroomsize_id.'<br/>';
                                                            if ($safa_trip_internaltrips_hotels_row->erp_hotel_id == $internalpassages_hotel->erp_end_hotel_id && $safa_trip_internaltrips_hotels_row->erp_hotelroomsize_id == $erp_hotelroomsize->erp_hotelroomsize_id) {
                                                                $current_hotels_rooms_count = $safa_trip_internaltrips_hotels_row->rooms_count;
                                                            }
                                                        }
                                                        ?>
                                                        <?= $erp_hotelroomsize->{name()} ?> <?=
                                                        form_input('hotels_rooms_count[' . $internalpassages_hotel->safa_internalsegment_id . '][' . $erp_hotelroomsize->erp_hotelroomsize_id . ']'
                                                                , set_value('hotels_rooms_count[' . $internalpassages_hotel->safa_internalsegment_id . '][' . $erp_hotelroomsize->erp_hotelroomsize_id . ']', $current_hotels_rooms_count)
                                                                , 'class="input-small FRight" style="width: 50px !important;"  rpnrel="' . $erp_hotelroomsize->erp_hotelroomsize_id . '" placeholder="' . $erp_hotelroomsize->{name()} . '" id="hotels_rooms_count_' . $internalpassages_hotel->safa_internalsegment_id . '_' . $erp_hotelroomsize->erp_hotelroomsize_id . '" ')
                                                        ?>
                                                    </td>
                                                    <? endforeach ?>
                                                    <? endif ?>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>




                                    <?php
                                    $internalpassages_hotels_counter++;
                                    //}
                                }
                            }
                            ?>
                        </tbody>
                    </table>
                </fieldset>
            </div>

            <div class="row-fluid" id="div_tbl_tourismplaces">
                <fieldset class="resalt-group">
                    <div class="wizerd-div"><a> <?= lang('tourismplaces') ?></a></div>
                    <table id='tourismplaces'>
                        <thead>
                            <tr>
                                <th><?= lang('tourismplace') ?></th>
                                <th><?=
                                    lang('the_date')
                                    ?></th>
                                <th><a href="javascript:void(0)" onclick="new_tourismplace();" class="btn" > <?php echo lang('global_add') ?> </a></th>

                            </tr>
                        </thead>
                        <tbody class="tourismplaces greycolor" >

                            <?php
                            $internalpassages_tourismplaces_count = 0;

                            $internalpassages_tourismplaces_counter = 1;
                            if (isset($internalpassages_tourismplaces)) {
                                $internalpassages_tourismplaces_count = count($internalpassages_tourismplaces);

                                foreach ($internalpassages_tourismplaces as $internalpassages_tourismplace) {

                                    $this->internalpassages_model->safa_internalsegment_id = $internalpassages_tourismplace->safa_internalsegment_id + 1;
                                    $next_internalpassages_row = $this->internalpassages_model->get();
                                    $this->internalpassages_model->safa_internalsegment_id = false;

                                    //if(count($next_internalpassages_row)>0) {
                                    ?>

                                    <tr trel="<?php echo $internalpassages_tourismplace->safa_internalsegment_id ?>" >

                                        <td>
                                            <input type='hidden' name='tourism_safa_internalsegment_id[<?= $internalpassages_tourismplace->safa_internalsegment_id ?>]'  id='tourism_safa_internalsegment_id[<?= $internalpassages_tourismplace->safa_internalsegment_id ?>]'  value='<?php echo $internalpassages_tourismplace->safa_internalsegment_id; ?>'/>

                                            <?php
                                            echo form_dropdown('safa_tourismplace_id[' . $internalpassages_tourismplace->safa_internalsegment_id . ']', $safa_tourismplaces, set_value('safa_tourismplace_id', $internalpassages_tourismplace->safa_tourism_place_id), ' tourismrel="' . $internalpassages_tourismplace->safa_internalsegment_id . '" class=" chosen-select chosen-rtl input-full safa_tourismplace_id"  tabindex="4" id="safa_tourismplace_id[' . $internalpassages_tourismplace->safa_internalsegment_id . ']" ')
                                            ?>

                                        </td>

                                        <td>
                                            <?php
                                            echo form_input('tourism_date[' . $internalpassages_tourismplace->safa_internalsegment_id . ']'
                                                    , set_value('tourism_date[' . $internalpassages_tourismplace->safa_internalsegment_id . ']', date('Y-m-d H:i', strtotime($internalpassages_tourismplace->start_datetime)))
                                                    , 'class="validate[required] datetime " id="tourism_date[' . $internalpassages_tourismplace->safa_internalsegment_id . ']" tourdaterel="' . $internalpassages_tourismplace->safa_internalsegment_id . '" style="width:150px" ')
                                            ?>

                                            <script>

                                                        var selectrel = $('select[cityrel] option[value=<?php echo $internalpassages_tourismplace->erp_city_id; ?>]:selected').parent().attr('cityrel');
                                                        //alert($('#tourism_date\\[<?php echo $internalpassages_tourismplace->safa_internalsegment_id; ?>\\]').val());
                                                        $('#tourism_date\\[<?php echo $internalpassages_tourismplace->safa_internalsegment_id; ?>\\]').datetimepicker({
                                                dateFormat: 'yy-mm-dd',
                                                        //minDate: new Date($('input[startrel=' + selectrel + ']').val()),
                                                        //maxDate: new Date($('input[endrel=' + selectrel + ']').val())
                                                });</script>
                                        </td>





                                        <td class="TAC">
                                            <a href="javascript:void(0)" onclick="remove_trow(<?php echo $internalpassages_tourismplace->safa_internalsegment_id ?>)"><span class="icon-trash"></span></a>
                                        </td>
                                    </tr>

                                    <?php
                                    $internalpassages_tourismplaces_counter++;
                                }
                            }
                            ?>

                        </tbody>
                    </table>
                </fieldset>
            </div>

            <?php if ($safa_trip_internaltrip_id == 0) { ?>
                <div class="toolbar bottom TAC">
                    <a href="javascript:void(0)" onclick="generate_internalsegments();" class="btn" > <?php echo lang('generate_trip_internaltrip') ?> </a>
                </div>
            <?php } ?>

            <?php
//By Gouda, this condition doesn't occur anyway.
//if($safa_trip_internaltrip_id!=0 && $safa_trip_internaltrip_id==0) {
            if ($safa_trip_internaltrip_id != 0 && $safa_trip_internaltrip_id == 0) {
                ?>
                <div class="row-fluid">

                    <fieldset class="resalt-group">
                        <div class="wizerd-div"><a> <?= lang('transport_request_internal_passages') ?></a></div>





                        <div class="table-responsive" >
                            <!--- the part of internal passages---> 
                            <table class="myTable" cellpadding="0" cellspacing="0">
                                <thead>
                                    <tr>
                                        <th><?= lang('internalpassage_type') ?></th>
                                        <th><?= lang('internalpassage_starthotel') ?></th>
                                        <th><?= lang('internalpassage_endhotel') ?></th>
                                        <th><?= lang('internalpassage_startdatatime') ?></th>
                                        <th><?= lang('internalpassage_enddatatime') ?></th>
                <!--                        <th><?= lang('internalpassage_seatscount') ?></th>-->
                                        <th><?= lang('internalpassage_note') ?></th>
                                        <th>
                                            <!--                        <?= lang('global_actions') ?>-->
                                            <a href="<?= site_url("uo/trip_internalpassages/add/" . $safa_trip_internaltrip_id) ?>" class="btn  fancybox fancybox.iframe" style="<? if (lang('global_lang') == 'ar'): ?>float:left<? else: ?>float:right<? endif; ?>" ><?= lang('add_passges') ?></a>

                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <? if (isset($internalpassages)) : ?>
                                    <? foreach ($internalpassages as $inernalpassage): ?>
                                    <tr>
                                        <td><?= item("safa_internalsegmenttypes", name(), array("safa_internalsegmenttype_id" => $inernalpassage->safa_internalsegmenttype_id));
                ?></td>
                                        <? if ($inernalpassage->safa_internalsegmenttype_id == 1): 

                                        $erp_port='';
                                        if($inernalpassage->erp_port_id) {
                                        $erp_port = item("erp_ports", name(), array("erp_port_id" => $inernalpassage->erp_port_id));
                                        }

                                        $erp_end_hotel='';
                                        if($inernalpassage->erp_end_hotel_id) {
                                        $erp_end_hotel = item("erp_hotels", name(), array("erp_hotel_id" => $inernalpassage->erp_end_hotel_id));
                                        }


                                        ?>
                                        <td><?= $erp_port; ?></td>
                                        <td><?= $erp_end_hotel; ?></td>
                                        <? endif; ?>  
                                        <? 

                                        if ($inernalpassage->safa_internalsegmenttype_id == 2){ 
                                        $erp_start_hotel='';
                                        if($inernalpassage->erp_start_hotel_id) {
                                        $erp_start_hotel = item("erp_hotels", name(), array("erp_hotel_id" => $inernalpassage->erp_start_hotel_id));
                                        }

                                        $erp_port='';
                                        if($inernalpassage->erp_port_id) {
                                        $erp_port = item("erp_ports", name(), array("erp_port_id" => $inernalpassage->erp_port_id));
                                        }
                                        ?>
                                        <td><?= $erp_start_hotel; ?></td>
                                        <td><?= $erp_port; ?></td>
                                        <? }; ?>
                                        <? if ($inernalpassage->safa_internalsegmenttype_id == 3): 

                                        $erp_start_hotel='';
                                        if($inernalpassage->erp_start_hotel_id) {
                                        $erp_start_hotel = item("erp_hotels", name(), array("erp_hotel_id" => $inernalpassage->erp_start_hotel_id));
                                        }

                                        $safa_tourism_place='';
                                        if($inernalpassage->safa_tourism_place_id) {
                                        $safa_tourism_place = item("safa_tourismplaces", name(), array("safa_tourismplace_id" => $inernalpassage->safa_tourism_place_id));
                                        }

                                        ?>
                                        <td><?= $erp_start_hotel; ?></td>
                                        <td><?= $safa_tourism_place; ?></td>
                                        <? endif; ?>

                                        <? if ($inernalpassage->safa_internalsegmenttype_id == 4): 

                                        $erp_start_hotel='';
                                        if($inernalpassage->erp_start_hotel_id) {
                                        $erp_start_hotel = item("erp_hotels", name(), array("erp_hotel_id" => $inernalpassage->erp_start_hotel_id));
                                        }

                                        $erp_end_hotel='';
                                        if($inernalpassage->erp_end_hotel_id) {
                                        $erp_end_hotel = item("erp_hotels", name(), array("erp_hotel_id" => $inernalpassage->erp_end_hotel_id));
                                        }

                                        ?>
                                        <td><?= $erp_start_hotel; ?></td>
                                        <td><?= $erp_end_hotel; ?></td>
                                        <? endif; ?>
                                        <td>
                                            <? if (isset($inernalpassage->start_datetime) && $inernalpassage->start_datetime != null && $inernalpassage->start_datetime != '0000-00-00 00:00:00'): ?>
                                            <? $date = date_create($inernalpassage->start_datetime); ?>
                                            <span><?= date_format($date, "m-d H:i"); ?></span>
                                            <? endif; ?>   
                                        </td>
                                        <td>
                                            <? if (isset($inernalpassage->end_datetime) && $inernalpassage->end_datetime != null && $inernalpassage->end_datetime != '0000-00-00 00:00:00'): ?>
                                            <? $date_end = date_create($inernalpassage->end_datetime); ?>    
                                            <span><?= date_format($date_end, "m-d H:i"); ?></span>
                                            <? endif; ?> 
                                        </td>
        <!--                                <td><? if (isset($inernalpassage->seats_count)): ?> <?= $inernalpassage->seats_count ?> <? endif; ?>  </td>-->
                                        <td><? if (isset($inernalpassage->notes)): ?> <?= $inernalpassage->notes ?> <? endif; ?>  </td>
                                        <td class="TAC">
                                            <a  class="fancybox fancybox.iframe " href="<?= site_url("ea/trip_internalpassages/edit/" . $inernalpassage->safa_internalsegment_id) ?>"><span class="icon-pencil"></span></a> 
                                            <? if (!check_id('safa_internalsegment_log', 'safa_internalsegment_id',
                                            $inernalpassage->safa_internalsegment_id)): ?>
                                            <a href="<?= site_url("ea/trip_internalpassages/delete/" . $inernalpassage->safa_internalsegment_id . "/" . $safa_trip_internaltrip_id) ?>" onclick="return window.confirm('<?= lang('global_are_you_sure_you_want_to_delete') ?>')" ><span class="icon-trash"></span></a>
                                            <? endif; ?>
                                        </td>
                                    </tr> 

                                    <? endforeach; ?>
                                    <? endif; ?>  
                                </tbody>
                            </table>  

                        </div>
                    </fieldset>
                </div>
                <?php
            }
            ?>








<!--<div class="row-fluid" id="div_internalsegments" <?php if ($safa_trip_internaltrip_id == 0) { ?>style="display:none;" <?php } ?>>-->
            <div class="row-fluid" id="div_internalsegments" >

                <fieldset class="resalt-group">
                    <div class="wizerd-div"><a> <?= lang('trip_internaltrip') ?></a></div>




                    <script>passages_counter = 0</script>

                    <div class="row-form" >
                        <div class="table-responsive" id="div_tbl_internalsegments">
                            <!--- the part of internal passages---> 
                            <table cellpadding="0" cellspacing="0" id="tbl_internalsegments" class="greycolor">
                                <thead>
                                    <tr>
                                        <th><?= lang('internalpassage_type') ?></th>
                                        <th><?= lang('internalpassage_starthotel') ?></th>
                                        <th><?= lang('internalpassage_endhotel') ?></th>
                                        <th><?= lang('internalpassage_startdatatime') ?></th>
                                        <th><?= lang('internalpassage_enddatatime') ?></th>
            <!--                            <th><?= lang('internalpassage_seatscount') ?></th>-->
                                        <th><?= lang('internalpassage_note') ?></th>
                                        <th>
                                            <a class="btn Fleft" href="javascript:void(0)" onclick="add_internalsegment('N' + passages_counter++); load_multiselect()">
                                                <?php echo lang('global_add') ?>
                                            </a>
                                        </th>
                                    </tr>
                                </thead>
                                <tbody class="internalsegments greycolor" id="tbl_body_internalsegments">
                                    <? if (isset($internalpassages)) : ?>
                                    <? foreach ($internalpassages as $inernalpassage): ?>
                                    <tr rel="<?php echo $inernalpassage->safa_internalsegment_id ?>">

                                        <td>
                                            <?php
                                            echo form_dropdown('internalpassage_type[' . $inernalpassage->safa_internalsegment_id . ']'
                                                    , $safa_internalpassagetypes, set_value('internalpassage_type[' . $inernalpassage->safa_internalsegment_id . ']', $inernalpassage->safa_internalsegmenttype_id)
                                                    , 'class="chosen-select chosen-rtl input-full  validate[required]" id="internalpassage_type_' . $inernalpassage->safa_internalsegment_id . '" tabindex="4" ')
                                            ?>


                                            <script>
                                                        $(document).ready(function() {
                                                $("#internalpassage_type_<?php echo $inernalpassage->safa_internalsegment_id; ?>").change(function() {
                                                var val_ = $(this).val();
                                                        show_ports_<?php echo $inernalpassage->safa_internalsegment_id; ?>(val_);
                                                });
                                                });
                                                        function show_ports_<?php echo $inernalpassage->safa_internalsegment_id; ?>(pass_type) {
                                                        //   alert(pass_type);
                                                        load_multiselect();
                                                                $("#start_point_con_<?php echo $inernalpassage->safa_internalsegment_id; ?>").show();
                                                                $("#end_point_con_<?php echo $inernalpassage->safa_internalsegment_id; ?>").show();
                                                                $("#start_point_con_<?php echo $inernalpassage->safa_internalsegment_id; ?>").children().hide();
                                                                $("#end_point_con_<?php echo $inernalpassage->safa_internalsegment_id; ?>").children().hide();
                                                                if (pass_type == 1) {
                                                        $("#internalpassage_port_start_<?php echo $inernalpassage->safa_internalsegment_id; ?>_chosen").show();
                                                                $("#internalpassage_hotel_end_<?php echo $inernalpassage->safa_internalsegment_id; ?>_chosen").show();
                                                        }
                                                        if (pass_type == 2) {
                                                        $("#internalpassage_hotel_start_<?php echo $inernalpassage->safa_internalsegment_id; ?>_chosen").show();
                                                                $("#internalpassage_port_end_<?php echo $inernalpassage->safa_internalsegment_id; ?>_chosen").show();
                                                                $("#end_point_con_<?php echo $inernalpassage->safa_internalsegment_id; ?>").find(".error").show();
                                                        }
                                                        if (pass_type == 3) {
                                                        $("#internalpassage_hotel_start_<?php echo $inernalpassage->safa_internalsegment_id; ?>_chosen").show();
                                                                $("#internalpassage_torismplace_end_<?php echo $inernalpassage->safa_internalsegment_id; ?>_chosen").show();
                                                        }
                                                        if (pass_type == 4) {
                                                        $("#internalpassage_hotel_start_<?php echo $inernalpassage->safa_internalsegment_id; ?>_chosen").show();
                                                                $("#internalpassage_hotel_end_<?php echo $inernalpassage->safa_internalsegment_id; ?>_chosen").show();
                                                        }

                                                        }


                                            </script>
                                        </td>



                                        <td>


                                            <div id='start_point_con_<?php echo $inernalpassage->safa_internalsegment_id; ?>' style='display:block' >
                                                <?php
                                                $hdn_start_point_value = '';
                                                if ($inernalpassage->erp_start_hotel_id != '') {
                                                    $hdn_start_point_value = $inernalpassage->erp_start_hotel_id;
                                                } else if ($inernalpassage->safa_tourism_place_id != '') {
                                                    $hdn_start_point_value = $inernalpassage->safa_tourism_place_id;
                                                } else if ($inernalpassage->erp_port_id != '') {
                                                    $hdn_start_point_value = $inernalpassage->erp_port_id;
                                                }
                                                ?>
                                                <input type="hidden" id="hdn_start_point_<?php echo $inernalpassage->safa_internalsegment_id; ?>" value="<?php echo $hdn_start_point_value; ?>"></input>
                                                <?php
                                                echo form_dropdown('internalpassage_hotel_start[' . $inernalpassage->safa_internalsegment_id . ']'
                                                        , $erp_hotels, set_value('internalpassage_hotel_start[' . $inernalpassage->safa_internalsegment_id . ']', $inernalpassage->erp_start_hotel_id)
                                                        , 'class="chosen-select chosen-rtl input-full start_point" id="internalpassage_hotel_start_' . $inernalpassage->safa_internalsegment_id . '" tabindex="4" style="display:none" ');
                                                ?>
                                                <?php
                                                echo form_dropdown('internalpassage_torismplace_start[' . $inernalpassage->safa_internalsegment_id . ']'
                                                        , $safa_tourismplaces, set_value('internalpassage_torismplace_start[' . $inernalpassage->safa_internalsegment_id . ']', $inernalpassage->safa_tourism_place_id)
                                                        , 'class="chosen-select chosen-rtl input-full start_point" id="internalpassage_torismplace_start_' . $inernalpassage->safa_internalsegment_id . '" tabindex="4"  style="display:none" ');
                                                ?>
                                                <?php
                                                if ($erp_transportertype_id == 1) {
                                                    echo form_dropdown('internalpassage_port_start[' . $inernalpassage->safa_internalsegment_id . ']'
                                                            , $land_erp_ports_sa, set_value('internalpassage_port_start[' . $inernalpassage->safa_internalsegment_id . ']', $inernalpassage->erp_port_id)
                                                            , 'class="chosen-select chosen-rtl input-full start_point" id="internalpassage_port_start_' . $inernalpassage->safa_internalsegment_id . '" tabindex="4"  style="display:none" ');
                                                } else if ($erp_transportertype_id == 2) {
                                                    echo form_dropdown('internalpassage_port_start[' . $inernalpassage->safa_internalsegment_id . ']'
                                                            , $erp_sa_ports, set_value('internalpassage_port_start[' . $inernalpassage->safa_internalsegment_id . ']', $inernalpassage->erp_port_id)
                                                            , 'class="chosen-select chosen-rtl input-full start_point" id="internalpassage_port_start_' . $inernalpassage->safa_internalsegment_id . '" tabindex="4"  style="display:none" ');
                                                } if ($erp_transportertype_id == 3) {
                                                    echo form_dropdown('internalpassage_port_start[' . $inernalpassage->safa_internalsegment_id . ']'
                                                            , $sea_erp_ports_sa, set_value('internalpassage_port_start[' . $inernalpassage->safa_internalsegment_id . ']', $inernalpassage->erp_port_id)
                                                            , 'class="chosen-select chosen-rtl input-full start_point" id="internalpassage_port_start_' . $inernalpassage->safa_internalsegment_id . '" tabindex="4"  style="display:none" ');
                                                }
                                                ?>
                                            </div>

                                        </td>


                                        <td>

                                            <div id='end_point_con_<?php echo $inernalpassage->safa_internalsegment_id; ?>' style='display:block' >

                                                <?php
                                                $hdn_end_point_value = '';
                                                if ($inernalpassage->erp_end_hotel_id != '') {
                                                    $hdn_end_point_value = $inernalpassage->erp_end_hotel_id;
                                                } else if ($inernalpassage->safa_tourism_place_id != '') {
                                                    $hdn_end_point_value = $inernalpassage->safa_tourism_place_id;
                                                } else if ($inernalpassage->erp_port_id != '') {
                                                    $hdn_end_point_value = $inernalpassage->erp_port_id;
                                                }
                                                ?>
                                                <input type="hidden" id="hdn_end_point_<?php echo $inernalpassage->safa_internalsegment_id; ?>" value="<?php echo $hdn_end_point_value; ?>"></input>


                                                <?php
                                                echo form_dropdown('internalpassage_hotel_end[' . $inernalpassage->safa_internalsegment_id . ']'
                                                        , $erp_hotels, set_value('internalpassage_hotel_end[' . $inernalpassage->safa_internalsegment_id . ']', $inernalpassage->erp_end_hotel_id)
                                                        , 'class="chosen-select chosen-rtl input-full end_point" id="internalpassage_hotel_end_' . $inernalpassage->safa_internalsegment_id . '" tabindex="4"  style="display:none" ');
                                                ?>

                                                <?php
                                                echo form_dropdown('internalpassage_torismplace_end[' . $inernalpassage->safa_internalsegment_id . ']'
                                                        , $safa_tourismplaces, set_value('internalpassage_torismplace_end[' . $inernalpassage->safa_internalsegment_id . ']', $inernalpassage->safa_tourism_place_id)
                                                        , 'class="chosen-select chosen-rtl input-full end_point" id="internalpassage_torismplace_end_' . $inernalpassage->safa_internalsegment_id . '" tabindex="4"  style="display:none" ');
                                                ?>
                                                <?php
                                                if ($erp_transportertype_id == 1) {
                                                    echo form_dropdown('internalpassage_port_end[' . $inernalpassage->safa_internalsegment_id . ']'
                                                            , $land_erp_ports_sa, set_value('internalpassage_port_end[' . $inernalpassage->safa_internalsegment_id . ']', $inernalpassage->erp_port_id)
                                                            , 'class="chosen-select chosen-rtl input-full end_point" id="internalpassage_port_end_' . $inernalpassage->safa_internalsegment_id . '" tabindex="4"  style="display:none" ');
                                                } else if ($erp_transportertype_id == 2) {
                                                    echo form_dropdown('internalpassage_port_end[' . $inernalpassage->safa_internalsegment_id . ']'
                                                            , $erp_sa_ports, set_value('internalpassage_port_end[' . $inernalpassage->safa_internalsegment_id . ']', $inernalpassage->erp_port_id)
                                                            , 'class="chosen-select chosen-rtl input-full end_point" id="internalpassage_port_end_' . $inernalpassage->safa_internalsegment_id . '" tabindex="4"  style="display:none" ');
                                                } else if ($erp_transportertype_id == 3) {
                                                    echo form_dropdown('internalpassage_port_end[' . $inernalpassage->safa_internalsegment_id . ']'
                                                            , $sea_erp_ports_sa, set_value('internalpassage_port_end[' . $inernalpassage->safa_internalsegment_id . ']', $inernalpassage->erp_port_id)
                                                            , 'class="chosen-select chosen-rtl input-full end_point" id="internalpassage_port_end_' . $inernalpassage->safa_internalsegment_id . '" tabindex="4"  style="display:none" ');
                                                }
                                                ?>
                                            </div>

                                            <script>
                                                $(document).ready(function() {
                                                show_ports_<?php echo $inernalpassage->safa_internalsegment_id; ?>(<?php echo $inernalpassage->safa_internalsegmenttype_id; ?>);
                                                });</script>


                                        </td>



                                        <td>
                                            <?php
                                            echo form_input('internalpassage_startdatatime[' . $inernalpassage->safa_internalsegment_id . ']'
                                                    , set_value('internalpassage_startdatatime[' . $inernalpassage->safa_internalsegment_id . ']', date('Y-m-d H:i', strtotime($inernalpassage->start_datetime)))
                                                    , 'class="input-huge  datetimepicker" style="width:120px"')
                                            ?>

                                        </td>
                                        <td>
                                            <?php
                                            echo form_input('internalpassage_enddatatime[' . $inernalpassage->safa_internalsegment_id . ']'
                                                    , set_value('internalpassage_enddatatime[' . $inernalpassage->safa_internalsegment_id . ']', date('Y-m-d H:i', strtotime($inernalpassage->end_datetime)))
                                                    , 'class="input-huge datetimepicker" style="width:120px"')
                                            ?>


                                        </td>
                            <!--<td>
                                        <?php
                                        echo form_input('seatscount[' . $inernalpassage->safa_internalsegment_id . ']'
                                                , set_value('seatscount[' . $inernalpassage->safa_internalsegment_id . ']', $inernalpassage->seats_count)
                                                , 'class="" style="width:50px"')
                                        ?>
                                        </td>
                                        --><td>
                                            <?php
                                            echo form_input('internalpassage_notes[' . $inernalpassage->safa_internalsegment_id . ']'
                                                    , set_value('internalpassage_notes[' . $inernalpassage->safa_internalsegment_id . ']', $inernalpassage->notes)
                                                    , 'class="input-huge" style="width:120px"')
                                            ?>
                                        </td>



                                        <td class="TAC">
                                            <a href="javascript:void(0)" onclick="repeat_passages('<?php echo $inernalpassage->safa_internalsegment_id ?>', true)"><span class="icon-copy"></span></a>

                                            <a href="javascript:void(0)" onclick="delete_passages('<?php echo $inernalpassage->safa_internalsegment_id ?>', true)"><span class="icon-trash"></span></a>

                                            &nbsp; &nbsp;

                                            <?php
                                            $this->internalsegments_drivers_and_buses_model->safa_internalsegments_id = $inernalpassage->safa_internalsegment_id;
                                            $count_drivers_and_buses = $this->internalsegments_drivers_and_buses_model->get(true);
                                            ?>	
                                            X <?php echo $count_drivers_and_buses; ?> 

                                            <a title='<?= lang('drivers_and_buses') ?>' href="<?= site_url('uo/all_movements/drivers_and_buses/' . $inernalpassage->safa_internalsegment_id) ?>" style="color:black; direction:rtl">
                                                <img src="<?= IMAGES2 ?>/car.png" align="left" >
                                            </a>    



                                        </td>
                                    </tr>  

                                    <? endforeach; ?>
                                    <? endif; ?>  
                                </tbody>
                            </table>    

                        </div>

                    </div>

                    <div class="row-form" >
                        <br/> <br/>
                    </div>

                    <div class="row-form" >
                        <?php
                        foreach ($safa_internaltrip_types as $key => $value) {
                            if (session('ea_id')) {
                                if ($key != 3) {
                                    ?>

                                    <div class="span6">
                                        <?php if ($safa_internaltrip_type_id == 0 && $key == 1) { ?>
                                            <?= form_radio('safa_internaltrip_type_id', $key, TRUE, " id='safa_internaltrip_type_id'  class='validate[required]' ") ?> <?= $value ?>
                                        <?php } else { ?>
                                            <?= form_radio('safa_internaltrip_type_id', $key, ($safa_internaltrip_type_id == $key) ? TRUE : FALSE, " id='safa_internaltrip_type_id'  class='validate[required]' ") ?> <?= $value ?>
                                        <?php } ?>
                                    </div>

                                    <?php
                                }
                            } else if (session('uo_id')) {
                                if ($key != 2) {
                                    ?>

                                    <div class="span6">
                                        <?php if ($safa_internaltrip_type_id == 0 && $key == 1) { ?>
                                            <?= form_radio('safa_internaltrip_type_id', $key, TRUE, " id='safa_internaltrip_type_id'  class='validate[required]' ") ?> <?= $value ?>
                                        <?php } else { ?>
                                            <?= form_radio('safa_internaltrip_type_id', $key, ($safa_internaltrip_type_id == $key) ? TRUE : FALSE, " id='safa_internaltrip_type_id'  class='validate[required]' ") ?> <?= $value ?>
                                        <?php } ?>
                                    </div>
                                    <?php
                                }
                            }
                        }
                        ?>

                    </div>

                    <div class="row-form" >
                        <div class="span6" >
                            <div class="span4">
                                <?= lang("transport_request_opertator") ?>
                                <font style='color:red'>*</font>
                            </div>

                            <div class="span8" >
                                <?= form_dropdown("safa_ito_id", $safa_ito, set_value("safa_ito_id", $safa_ito_id), " id='safa_ito_id' class=' validate[required] chosen-select chosen-rtl input-huge '") ?>
                                <span class="bottom" style='color:red' >
                                    <?= form_error('safa_ito_id') ?> 
                                </span>
                            </div>
                        </div>

                        <div class="span6">
                            <div class="span4"><?= lang("transport_request_transportername") ?>
                                <font style='color:red'>*</font>
                            </div>
                            <div class="span8" >
                                <?= form_dropdown("safa_transporter_id", $bus_transporters, set_value("safa_transporter_id", $safa_transporter_id), " id='safa_transporter_id' class=' validate[required] chosen-select chosen-rtl'") ?>
                                <span class="bottom" style='color:red' >
                                    <?= form_error('safa_transporter_id') ?> 
                                </span>
                            </div>
                        </div>


                    </div>

                    <div class="row-form" >
                        <div class="span6">
                            <div class="span4">
                                <?= lang("transport_request_res_code") ?>
                            </div>
                            <div class="span8" >
                                <?= form_input("operator_reference", set_value("operator_reference", $operator_reference)) ?>
                                <span class="bottom" style='color:red' >
                                    <?= form_error('operator_reference') ?> 
                                </span>
                            </div>
                        </div> 

                        <div class="span6" >
                            <div class="span4" ><?= lang('confirmation_number') ?></div>
                            <div class="span8" >

                                <?= form_input("confirmation_number", set_value("confirmation_number", $confirmation_number), "") ?>
                                <span class="bottom" style='color:red' >
                                    <?= form_error('confirmation_number') ?> 
                                </span>
                            </div>
                        </div>

                    </div>    

                    <div class="row-form" >    


                        <div class="span6" >
                            <div class="span4" ><?= lang("transport_request_res_file") ?></div>
                            <div class="span8">
                                <input type="file" value="" name="userfile"/>
                                <span class="bottom" style='color:red' >
                                    <?= form_error('userfile') ?> 
                                </span>
                            </div> 
                        </div>  

                    </div>


                    <div class="row-form" >  
                        <script>drivers_and_buses_counter = 0</script>
                        <table cellpadding="0" cellspacing="0">
                            <thead>
                                <tr>
                                    <!-- 
                                    <th><?php echo lang('driver') ?></th>
                                    <th><?php echo lang('driver_data') ?></th>
                                    <th><?php echo lang('bus') ?></th>
                                    <th><?php echo lang('bus_data') ?></th>
                                    -->

                                    <th><?php echo lang('bus_type') ?></th>
                                    <th><?php echo lang('bus_count') ?></th>

                                    <?php if ($safa_trip_internaltrip_id == 0) { ?>
                                        <th><?php echo lang('passengers_count') ?></th>
                                        <th>
                                            <a class="btn Fleft" title="<?php echo lang('global_add') ?>" href="javascript:void(0)" onclick="add_drivers_and_buses('N' + drivers_and_buses_counter++); load_multiselect()" id="lnk_add_drivers_and_buses">
                                                <?php echo lang('global_add') ?>    
                                            </a>
                                        </th>
                                        <?php
                                    }
                                    ?>
                                </tr>
                            </thead>
                            <tbody class="drivers_and_buses" id='drivers_and_buses'>
                                <? if(isset($item_drivers_and_buses)) { ?>
                                <? if(check_array($item_drivers_and_buses)) { 
                                $item_drivers_and_buses_counter=0;
                                foreach($item_drivers_and_buses as $item_driver_and_bus) { 
                                $item_drivers_and_buses_counter++;
                                ?>
                                <tr rel="<?php echo $item_drivers_and_buses_counter ?>">
                                    <!-- 
                                    <td>
                                    
                                             
                                    <?php
                                    echo form_dropdown('drivers_and_buses_safa_transporters_drivers[' . $item_drivers_and_buses_counter . ']'
                                            , $safa_transporters_drivers, set_value('drivers_and_buses_safa_transporters_drivers[' . $item_drivers_and_buses_counter . ']', $item_driver_and_bus->safa_transporters_drivers_id)
                                            , 'class="chosen-select chosen-rtl input-full drivers_and_buses_safa_transporters_drivers" id="drivers_and_buses_safa_transporters_drivers_' . $item_drivers_and_buses_counter . '" tabindex="4" ')
                                    ?>
                                           
                                    <?php echo $item_driver_and_bus->safa_transporters_driver_name; ?>
                                    </td>
                                    
                                    <td>
                                    <script>
                                                 $("#drivers_and_buses_safa_transporters_drivers_<?php echo $item_drivers_and_buses_counter; ?>").change(function(){
                                                 var safa_transporters_drivers_id=$(this).val();
                                                 var dataString = "safa_transporters_drivers_id="+ safa_transporters_drivers_id;
                                                 $.ajax
                                                 ({
                                                    type: "POST",
                                                    url: "<?php echo base_url() . 'uo/all_movements/getDriverData'; ?>",
                                                    data: dataString,
                                                    cache: false,
                                                    success: function(html)
                                                    {
                                                            //alert(html);
                                                            $("#driver_data_<?php echo $item_drivers_and_buses_counter; ?>").html(html);
                                                    }
                                                 });
                                                });
                                                    </script>
                                        <div id="driver_data_<?php echo $item_drivers_and_buses_counter; ?>">
                                    <?php
                                    $this->transporters_drivers_model->safa_transporters_drivers_id = $item_driver_and_bus->safa_transporters_drivers_id;
                                    $driver_row = $this->transporters_drivers_model->get();

//echo lang(name()).':'. $driver_row->{name()};
                                    echo lang('phone') . ':' . $driver_row->phone;
                                    ?>
                                        </div>
                                    </td>
                                   <td>
                                   
                                     
                                    <?php
                                    echo form_dropdown('drivers_and_buses_safa_transporters_buses[' . $item_drivers_and_buses_counter . ']'
                                            , $safa_transporters_buses, set_value('drivers_and_buses_safa_transporters_buses[' . $item_drivers_and_buses_counter . ']', $item_driver_and_bus->safa_transporters_buses_id)
                                            , 'class="chosen-select chosen-rtl input-full drivers_and_buses_safa_transporters_buses" id="drivers_and_buses_safa_transporters_buses_' . $item_drivers_and_buses_counter . '" tabindex="4" ')
                                    ?>
                                                
                                    <?php echo $item_driver_and_bus->bus_no; ?>
                                    </td>
                                    <td>
                                    <script>
                                                 $("#drivers_and_buses_safa_transporters_buses_<?php echo $item_drivers_and_buses_counter; ?>").change(function(){
                                                 var safa_transporters_buses_id=$(this).val();
                                                 var dataString = "safa_transporters_buses_id="+ safa_transporters_buses_id;
                                                 $.ajax
                                                 ({
                                                    type: "POST",
                                                    url: "<?php echo base_url() . 'uo/all_movements/getBusData'; ?>",
                                                    data: dataString,
                                                    cache: false,
                                                    success: function(html)
                                                    {
                                                            //alert(html);
                                                            $("#bus_data_<?php echo $item_drivers_and_buses_counter; ?>").html(html);
                                                    }
                                                 });
                                                });
                                                    </script>
                                        <div id="bus_data_<?php echo $item_drivers_and_buses_counter; ?>">
                                    <?php
                                    $this->transporters_buses_model->safa_transporters_buses_id = $item_driver_and_bus->safa_transporters_buses_id;
                                    $this->transporters_buses_model->join = true;
                                    $bus_row = $this->transporters_buses_model->get();

//echo lang('bus_no').':'. $bus_row->bus_no;
                                    echo lang('safa_buses_brands_id') . ':' . $bus_row->buses_brands_name . '<br/>';
                                    echo lang('safa_buses_models_id') . ':' . $bus_row->buses_models_name . '<br/>';
                                    echo lang('industry_year') . ':' . $bus_row->industry_year . '<br/>';
                                    echo lang('passengers_count') . ':' . $bus_row->passengers_count . '<br/>';
                                    ?>
                                        </div>
                                    </td>
                                     
                                    <td>
                                    <?php
                                    echo form_input('seats_count[' . $item_drivers_and_buses_counter . ']'
                                            , set_value('seats_count[' . $item_drivers_and_buses_counter . ']', $item_driver_and_bus->seats_count)
                                            , 'class=" seats_count" style="width:50px"')
                                    ?>
                                            
                                    <?php echo $item_driver_and_bus->seats_count; ?>
                                    </td>
                                    -->

                                    <td>
                                        <?php echo $item_driver_and_bus->safa_buses_brand_name; ?>
                                    </td>

                                    <td>
                                        <?php echo $item_driver_and_bus->bus_count; ?>
                                    </td>


                                    <!-- 
                                    <td class="TAC">
                                        <a href="javascript:void(0)" onclick="delete_drivers_and_buses(<?php echo $item_drivers_and_buses_counter ?>, true)"><span class="icon-trash"></span></a>
                                    </td>
                                    -->
                                </tr>
                                <? } ?>
                                <? } ?>
                                <? } ?>
                            </tbody>
                        </table> 
                    </div> 


                </fieldset>




                <fieldset class="resalt-group">
                    <div class="wizerd-div"><a> <?= lang('trip_details_travellers') ?></a></div>


                    <div class="block-fluid" id="dv_tbl_trip_traveller">
                        <table class="fpTable" id="tbl_trip_traveller" cellpadding="0" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th><?= lang('trip_details_traveller') ?></th>
                                    <th><?= lang('trip_details_passport_no') ?></th>
                                    <th><?= lang('trip_details_visa_no') ?></th>
                                    <th><?= lang('trip_details_nationality') ?></th>
                                    <th><?= lang('trip_details_gender') ?></th>

                                    <th><?= lang('group_passports_age') ?></th>
                                    <th><?= lang('group_passports_occupation') ?></th>
                                    <th class="TAC"><?= lang('global_operations') ?></th>
                                </tr>
                            </thead>
                            <tbody>
                                <? 

                                $trip_travellers_arr = array();
                                $this->safa_group_passports_model->safa_trip_internaltrip_id = false;
                                $this->safa_group_passports_model->safa_trip_id = false;

                                $trip_group_passports_rows = $this->safa_group_passports_model->get_for_table();
                                foreach ($trip_group_passports_rows as $trip_group_passports_row) {
                                $trip_travellers_arr[$trip_group_passports_row->safa_group_passport_id] = $trip_group_passports_row->{'first_' . name()} . ' ' . $trip_group_passports_row->{'second_' . name()} . ' ' . $trip_group_passports_row->{'third_' . name()} . ' ' . $trip_group_passports_row->{'fourth_' . name()};
                                }


                                $item_travellers_arr = array();

                                if (isset($safa_group_passports)) {

                                foreach ($safa_group_passports as $safa_group_passport){ 
                                $item_travellers_arr[] = $safa_group_passport->safa_group_passport_id;


                                ?>
                                <tr>

                                    <td><?= $safa_group_passport->{'first_' . name()} . ' ' . $safa_group_passport->{'second_' . name()} . ' ' . $safa_group_passport->{'third_' . name()} . ' ' . $safa_group_passport->{'fourth_' . name()} ?></td>
                                    <td><?= $safa_group_passport->passport_no ?></td>
                                    <td><?= $safa_group_passport->visa_number ?></td>
                                    <td><?= $safa_group_passport->erp_nationalities_name ?></td>
                                    <td><?= $safa_group_passport->erp_gender_name ?></td>

                                    <td>

                                        <?php
                                        $date_of_birth = strtotime($safa_group_passport->date_of_birth);
                                        $date_now = strtotime(date('Y-m-d', time()));
                                        $secs = $date_now - $date_of_birth; // == return sec in difference
                                        $days = $secs / 86400;
                                        $age = $days / 365;
//$age=round($age);
//$age=ceil($age);
                                        $age = floor($age);
                                        echo $age;
                                        ?>

                                    </td>
                                    <td><?= $safa_group_passport->occupation ?></td>
                                    <td class="TAC" >
                                        <a href="javascript:void(0)" class="" id="hrf_remove_traveller" name="<?php echo $safa_group_passport->safa_group_passport_id; ?>"><span class="icon-trash"></span> </a>
                                    </td>

                                </tr>
                                <? }
                                }
                                ?>
                            </tbody>
                        </table>


                        <?php
                        echo form_multiselect('safa_trip_traveller_ids[]', $trip_travellers_arr, set_value('safa_trip_traveller_ids', $item_travellers_arr), " id='safa_trip_traveller_ids'  class='' readonly='readonly' style='display: none;' ");
                        ?>

                        <script>
                                    $("#hrf_remove_traveller").live('click', function() {
                            if (!confirm('<?= lang('global_are_you_sure_you_want_to_delete') ?>'))
                                    return false

                                    var traveller_id = $(this).attr('name');
                                    //alert(traveller_id);
                                    $("#safa_trip_traveller_ids").find('option[value="' + traveller_id + '"]').remove();
                                    $(this).parent().parent().remove();
                                    var hidden_input = '<input type="hidden" name="safa_group_passports_remove[]" value="' + traveller_id + '" />';
                                    $('#dv_tbl_trip_traveller').append(hidden_input);
                            });</script>

                        <?php if (session('ea_id')) { ?>
                            <div class="span12">
                                <div class="span10"><a class="btn btn-primary finish fancybox fancybox.iframe"
                                                       href="<?php echo site_url('ea/group_passports/add_to_internaltrip_by_trip_popup/' . $safa_trip_id) ?>"><?php echo lang('add_safa_group_passports_to_internaltrip') ?></a>
                                </div>
                            </div>
                        <?php } ?>

                    </div>
                </fieldset>
                <br/>




                <div class="toolbar bottom TAC">
                    <input type="submit" style="padding:5px 5px !important;"  class="btn btn-primary" name="submit" value="<?= lang('global_submit') ?>" onclick="return validate_saving(); localStorage.removeItem('dv_internaltrip_manage_storage_<?php echo $safa_trip_internaltrip_id; ?>'); "/>
                    <a style="margin-bottom:0px;" href='<?= site_url('uo/trip_internaltrip/index') ?>'  class="btn btn-primary" /><?= lang('global_back') ?></a>
                </div>


            </div>



            <?= form_close() ?>
        </div>
    </div>  


    <script>
                $(document).ready(function() {
        $('.fancybox').fancybox({
        afterClose: function() {
        //parent.location.reload(true);
        }
        });
        });</script>

    <script type="text/javascript">
                $(document).ready(function() {
        var config = {
        '.chosen-select': {width: "100%"},
                '.chosen-select-deselect': {allow_single_deselect: true},
                '.chosen-select-no-single': {disable_search_threshold: 10},
                '.chosen-select-no-results': {no_results_text: 'Oops, nothing found!'},
                '.chosen-select-width': {width: "95%"}
        }
        for (var selector in config) {
        $(selector).chosen(config[selector]);
        }


        // binds form submission and fields to the validation engine
        $("#frm_trip_internaltrip").validationEngine({
        prettySelect : true,
                useSuffix: "_chosen",
                promptPosition : "topRight:-150"
                //promptPosition : "bottomLeft"
        });
        });</script> 

    <script>
                $(document).ready(function() {
        $("#uo_ea_id").change(function() {
        //sending the request to get the trips//
        $.get('<?= site_url("uo/trip_internaltrip/get_uo_contract_trips") ?>/' + $(this).val(), function(data) {
        $("#trip_id").html(data);
        });
        });
        });
                $('#uo_ea_id').live('change', function() {
        var uo_ea_id = $("#uo_ea_id").val();
                $.get('<?= site_url("uo/trip_internaltrip/get_uo_contract_trips") ?>/' + uo_ea_id, function(data) {
                $("#trip_id").html(data);
                        $("#trip_id").val('<?= $this->input->post('safa_trip_id') ?>');
                });
        });</script>
    <script>
                // filter with validation //
                $(document).ready(function() {
        /* initializing  the default value of the contracts by first items*/
        $("#uo_ea_id").val($(this).find('option').eq(1).val());
                var uo_ea_id = $("#uo_ea_id").val();
                $.get('<?= site_url("uo/trip_internaltrip/get_uo_contract_trips") ?>/' + uo_ea_id, function(data) {
                $("#trip_id").html(data);
                        $("#trip_id").val('<?= $this->input->post('safa_trip_id') ?>');
                });
        });</script>
    <script>
                $(document).ready(function() {
        /* input file */
        $(".file .btn, .file input:text").click(function() {
        var block = $(this).parent('.file');
                block.find('input:file').change(function() {
        var file_arr = $(this).val().split('\\');
                var file_name = file_arr[file_arr.length - 1];
                block.find('input:text').val(file_name);
        });
        });
        });</script>

    <!-- 
    <script>
            $(document).ready(function() {
                $("#safa_ito_id").change(function() {
                    if ($("#safa_ito_id").val()) {
                        $("#safa_internaltripstatus_id").val(4);
                    } else {
                        $("#safa_internaltripstatus_id").val(3);
                    }
                });
                $("#safa_internaltripstatus_id").change(function() {
                    if ($("#safa_ito_id").val()) {
                        if ($("#safa_internaltripstatus_id").val() != 4)
        <? if (lang('global_lang') == 'ar'): ?>
                            alert("قم باختيار مؤكد من شركة العمرة");
        <? else: ?>
                            alert("choose confirmed by uo");
        <? endif; ?>
                        $("#safa_internaltripstatus_id").val(4);
                    } else {
                        if ($("#safa_internaltripstatus_id").val() != 3)
        <? if (lang('global_lang') == 'ar'): ?>
                            alert("قم باختيار بانتظار مشغل النقل");
        <? else: ?>
                            alert("choose pending ito");
        <? endif; ?>
                        $("#safa_internaltripstatus_id").val(3);
                        $("#safa_internaltripstatus_id").trigger("chosen:updated");
                         }
                         });
                         });
                         </script> 
    -->

    <script>
                var c1 = 0;
                var c2 = 100;
                function new_row(safa_externaltriptype_id) {


                if (safa_externaltriptype_id == 1) {
                var id = c1;
                        var new_row = function() {/*
                         <tr class="table-row" rel="{$id}">
                         <td>
                         <input type='hidden' name='erp_flight_availabilities_detail_id[{$id}]'  id='erp_flight_availabilities_detail_id[{$id}]'  value='0'/>
                                
                                        <input type="hidden" name="safa_externaltriptype_id[{$id}]" id="safa_externaltriptype_id[{$id}]"  value="1" />
                          
<?= form_dropdown('erp_path_type_id[{$id}]', $erp_path_types, set_value("erp_path_type_id[{\$id}]", 2), 'class="validate[required] erp_path_type" style="width:60px" pathrel="{$id}" id="erp_path_type_id[{$id}]"') ?></td>
                         <td>
                         
                          <table style='border:0px;'><tr><td style='border:0px;'>
<?= form_dropdown('safa_transporter_code[{$id}]', $flight_transporters, set_value("safa_transporter_code[{\$id}]"), 'flight_states_rel= "{$id}" class=" chosen-select chosen-rtl validate[required]" style="width:60px" id="safa_transporter_code[{$id}]"') ?></td>
                         
                         </td><td style='border:0px;'>
                         <a href="javascript:void(0);" onclick="add_transporter();"  class="btn Fleft " > <span class="icon-plus"></span></a>
                         </td></tr>
                          </table>
                         
                          </td>
                          <td><?= form_input('flight_number[{$id}]', set_value("flight_number[{\$id}]"), ' flight_states_rel= "{$id}"  class="validate[required]" style="width:40px" id="flightno[{$id}]"') ?></td>
                          <td><?= form_input('flight_date[{$id}]', set_value("flight_date[{\$id}]"), ' flight_states_rel= "{$id}" class="validate[required] datetimepicker" style="width:100px" id="flight_date[{$id}]"') ?></td>
                          <td><?= form_input('flight_arrival_date[{$id}]', set_value("flight_arrival_date[{\$id}]"), 'class="validate[required] datetimepicker" arriverel="{$id}" style="width:100px" id="flight_arrival_date[{$id}]"') ?></td>
                         <td style="width:220px">
<?= form_dropdown('erp_port_id_from[{$id}]', $erp_ports, set_value("erp_port_id_from[{\$id}]"), '  erp_port_id_from_rel= "{$id}" class="validate[required]" style="width:60px" id="erp_port_id_from[{$id}]"') ?>
                          <!--             <?= form_dropdown('erp_port_hall_id_from[{$id}]', $erp_ports_halls, set_value("erp_port_hall_id_from[{\$id}]"), '  erp_port_hall_id_from_rel= "{$id}" class="validate[required]" style="width:120px" id="erp_port_hall_id_from[{$id}]"') ?>-->
                         <br/>
<?= form_dropdown('erp_port_id_to[{$id}]', $erp_sa_ports, set_value("erp_port_id_to[{\$id}]"), '  erp_port_id_to_rel= "{$id}" class="validate[required]" style="width:60px" id="erp_port_id_to[{$id}]"') ?>
<?= form_dropdown('erp_port_hall_id_to[{$id}]', $erp_sa_ports_halls, set_value("erp_port_hall_id_to[{\$id}]"), '  erp_port_hall_id_to_rel= "{$id}" class="validate[required]" style="width:120px" id="erp_port_hall_id_to[{$id}]"') ?>
                         
                         
                         
                          
                          <td><?= form_input('seats_count[{$id}]', set_value("seats_count[{\$id}]"), 'class="validate[required] seatcount" style="width:50px" id="seatcont[{$id}]"') ?></td>
                          <td><a href="javascript:void(0);" onclick="delete_flights('{$id}')"><span class="icon-trash"></span> </a></td>
                         </tr>
                         */
                                        }.toString().replace(/{\$id}/g, id).slice(14, - 3);
                                        $('tbody#going_flight_tbody').append(new_row); $('.datetimepicker').datetimepicker({dateFormat: 'yy-mm-dd', timeFormat: 'HH:mm'});
                                        c1++;
                                } else if (safa_externaltriptype_id == 2) {
                                var id = c2; var new_row = function() {/*
                                 <tr class="table-row" rel="{$id}">
                                 <td>
                                 <input type='hidden' name='erp_flight_availabilities_detail_id[{$id}]'  id='erp_flight_availabilities_detail_id[{$id}]'  value='0'/>
                                 
<input type="hidden" name="safa_externaltriptype_id[{$id}]" id="safa_externaltriptype_id[{$id}]"  value="2" />
                         
<?= form_dropdown('erp_path_type_id[{$id}]', $erp_path_types, set_value("erp_path_type_id[{\$id}]"), 'class="validate[required] erp_path_type" style="width:60px" id="erp_path_type_id[{$id}]"') ?></td>
<td>
                         
<table style='border:0px;'><tr><td style='border:0px;'>
<?= form_dropdown('safa_transporter_code[{$id}]', $flight_transporters, set_value("safa_transporter_code[{\$id}]"), ' flight_states_rel= "{$id}" class="  chosen-select chosen-rtl  validate[required]" style="width:60px" id="safa_transporter_code[{$id}]"') ?></td>
                                 
</td><td style='border:0px;'>
<a href="javascript:void(0);" onclick="add_transporter();"  class="btn Fleft " > <span class="icon-plus"></span></a>
</td></tr>
</table>
                                 
</td>
<td><?= form_input('flight_number[{$id}]', set_value("flight_number[{\$id}]"), ' flight_states_rel= "{$id}" class="validate[required]" style="width:40px" id="flightno[{$id}]"') ?></td>
<td><?= form_input('flight_date[{$id}]', set_value("flight_date[{\$id}]"), ' flight_states_rel= "{$id}" class="validate[required] datetimepicker" style="width:100px" id="flight_date[{$id}]"') ?></td>
<td><?= form_input('flight_arrival_date[{$id}]', set_value("flight_arrival_date[{\$id}]"), 'class="validate[required] datetimepicker" style="width:100px" id="flight_arrival_date[{$id}]"') ?></td>
             
<td style="width:220px">
<?= form_dropdown('erp_port_id_from[{$id}]', $erp_sa_ports, set_value("erp_port_id_from[{\$id}]"), '  erp_port_id_from_rel= "{$id}" class="validate[required]" style="width:60px" id="erp_port_id_from[{$id}]"') ?>
<?= form_dropdown('erp_port_hall_id_from[{$id}]', $erp_sa_ports_halls, set_value("erp_port_hall_id_to[{\$id}]"), '  erp_port_hall_id_from_rel= "{$id}" class="validate[required]" style="width:120px" id="erp_port_hall_id_from[{$id}]"') ?>
<br/>
<?= form_dropdown('erp_port_id_to[{$id}]', $erp_ports, set_value("erp_port_id_to[{\$id}]"), '  erp_port_id_to_rel= "{$id}" class="validate[required]" style="width:60px" id="erp_port_id_to[{$id}]"') ?>
<!--             <?= form_dropdown('erp_port_hall_id_to[{$id}]', $erp_ports_halls, set_value("erp_port_hall_id_to[{\$id}]"), '  erp_port_hall_id_to_rel= "{$id}" class="validate[required]" style="width:120px" id="erp_port_hall_id_to[{$id}]"') ?>-->
 			
<td><?= form_input('seats_count[{$id}]', set_value("seats_count[{\$id}]"), 'class="validate[required] seatcount" style="width:50px" id="seatcont[{$id}]"') ?></td>
<td><a href="javascript:void(0);" onclick="delete_flights('{$id}')"><span class="icon-trash"></span> </a></td>
             
</tr>
*/
}.toString().replace(/{\$id}/g, id).slice(14, -3);

$('tbody#return_flight_tbody').append(new_row);
$('.datetimepicker').datetimepicker({dateFormat: 'yy-mm-dd', timeFormat: 'HH:mm'});
c2++;
}
load_multiselect();
        
fix_seats_count();

        
if(id==0) {
$('#safa_transporter_code\\[' + id + '\\]').val($('#last_going_safa_transporter_code').val());
$('#safa_transporter_code\\[' + id + '\\]').trigger("chosen:updated");
} else if(id==100) {
$('#safa_transporter_code\\[' + id + '\\]').val($('#last_return_safa_transporter_code').val());
$('#safa_transporter_code\\[' + id + '\\]').trigger("chosen:updated");
} else {
$('#safa_transporter_code\\[' + id + '\\]').val($('#safa_transporter_code\\[' + (id-1) + '\\]').val());
$('#safa_transporter_code\\[' + id + '\\]').trigger("chosen:updated");
}
        
}

function load_multiselect() {
var config = {
'.chosen-select': {width: "100%"},
'.chosen-select-deselect': {allow_single_deselect: true},
'.chosen-select-no-single': {disable_search_threshold: 10},
'.chosen-select-no-results': {no_results_text: 'Oops, nothing found!'},
'.chosen-select-width': {width: "95%"}
}
for (var selector in config) {
$(selector).chosen(config[selector]);
}
}
    
function fix_seats_count() {
var adultseat = parseInt($('input[name=adult_seats]').val());
var childseat = parseInt($('input[name=child_seats]').val());
var babyseat = parseInt($('input[name=baby_seats]').val());
if (isNaN(adultseat))
adultseat = 0;
if (isNaN(childseat))
childseat = 0;
if (isNaN(babyseat))
babyseat = 0;
var seats = adultseat + childseat + babyseat;
$('.seatcount').val(seats);
}

function remove_row(id)
{
$('tr[rel=' + id + ']').remove();

}

    </script>
    <script>
    var h = 0;
    var t = 0;
    function new_hotel() {
                                 
    var id = h;
                                 
    if($('#erp_transportertype_id').val()==2){
                                 
    if (!$('tbody#going_flight_tbody tr').length || !$('tbody#going_flight_tbody tr select.erp_path_type option[value=2]:selected').length) {
    if(!$('tbody#going_flight_tbody tr input.erp_path_type[value=2]').length) {
    alert('<?php echo lang('you_should_add_arrival_trip_first'); ?>');
    return false;
    }
    }  
    if (!$('tbody#return_flight_tbody tr').length || !$('tbody#return_flight_tbody tr select.erp_path_type option[value=1]:selected').length) {
    if(!$('tbody#return_flight_tbody tr input.erp_path_type[value=1]').length) {
    alert('<?php echo lang('you_should_add_return_trip_first'); ?>');
    return false;
    }
    } 
                                 
    //else {
    var arrivedate='';
    if (id === 0) {
    var arriverel = $('tbody#going_flight_tbody tr select.erp_path_type option[value=2]:selected').parent().attr('pathrel');
    if($('input[arriverel=' + arriverel + ']').val()!=undefined){
    arrivedate = ($('input[arriverel=' + arriverel + ']')).val();
    } else {
    arrivedate = $('#last_exit_date').val();
    }
    } else {
                                 
    var newrel = id - 1;
    if($('input[endrel=' + newrel + ']').val()!=undefined){
    arrivedate = $('input[endrel=' + newrel + ']').val();
    }
    }
    //}
    } else if($('#erp_transportertype_id').val()==1){
    if($('#land_going_datetime').val()=='') {
    alert('<?php echo lang('you_should_add_going_datetime'); ?>');
    return false;
    }
    if($('#land_return_datetime').val()=='') {
    alert('<?php echo lang('you_should_add_return_datetime'); ?>');
    return false;
    }
                                 
    var arrivedate='';
    if ($('#div_hotels tr').length === 1) {
    arrivedate = $('#land_going_datetime').val()
    } else {
    var newrel = id - 1;
    if($('input[endrel=' + newrel + ']').val()!=undefined){
    arrivedate = $('input[endrel=' + newrel + ']').val();
    }
    }
                                 
    } else if($('#erp_transportertype_id').val()==3){
    if($('#sea_going_datetime').val()=='') {
    alert('<?php echo lang('you_should_add_going_datetime'); ?>');
    return false;
    }
    if($('#sea_return_datetime').val()=='') {
    alert('<?php echo lang('you_should_add_return_datetime'); ?>');
    return false;
    }
                                 
    if ($('#div_hotels tr').length === 1) {
    arrivedate = $('#sea_going_datetime').val()
    } else {
    var newrel = id - 1;
    if($('input[endrel=' + newrel + ']').val()!=undefined){
    arrivedate = $('input[endrel=' + newrel + ']').val();
    }
    }
    }
                                 
                                 
    var new_row = function() {/*
    <tr class="table-row" hrel="{$id}">
    <td>
    <input type='hidden' name='hotel_safa_internalsegment_id[{$id}]'  id='hotel_safa_internalsegment_id[{$id}]'  value='0'/>
                                 
<?= form_dropdown('erp_city_id[{$id}]', $cities, set_value("erp_city_id[{\$id}]"), 'class="validate[required] " cityrel="{$id}" id="erp_city_id[{$id}]"') ?></td>
    <td>
                                 
    <table style='border:0px;'><tr><td style='border:0px;'>
<?= form_dropdown('erp_hotel_id[{$id}]', $hotels, set_value("erp_hotel_id[{\$id}]"), 'class="validate[required] chosen-select erp_hotel_id" hotelrel="{$id}" id="erp_hotel_id[{$id}]"') ?>
    </td><td style='border:0px;'>
    <a href="javascript:void(0);" onclick="add_hotel({$id});"  class="btn Fleft " > <span class="icon-plus"></span></a>
    </td></tr>
    </table>
                                 
    </td>
                                 
                                 
    <td><?= form_input('arrival_date[{$id}]', set_value("arrival_date[{\$id}]", "{arrivedate}"), 'class="validate[required] " startrel="{$id}" readonly="readonly" style="width:100px" id="arrival_date[{$id}]"') ?></td>
    <td><?= form_input('nights[{$id}]', set_value("nights[{\$id}]"), 'class="validate[required]" nightsrel="{$id}" onchange="fix_dates();" style="width:50px" id="class[{$id}]"') ?></td>
    <td><?= form_input('exit_date[{$id}]', set_value("exit_date[{\$id}]"), 'class="validate[required] " endrel="{$id}" style="width:100px" readonly="readonly" id="exit_date[{$id}]"') ?></td>
    <td><a href="javascript:void(0);" onclick="remove_hrow('{$id}')"><span class="icon-trash"></span> </a></td>
    </tr>
    // here     
    <tr rel="{$id}">
    <td colspan="6" class="tbofsakn">
    <table >
    <tr inner_hrel='{$id}'>
    <td>
    <span class="FRight"><?= lang('rooms_count') ?></span>
    </td>
                                 
    <? if (check_array($erp_hotelroomsizes)) : ?>
    <? foreach ($erp_hotelroomsizes as $erp_hotelroomsize) : ?>
    <td>
<?= $erp_hotelroomsize->{name()} ?>
<?=
form_input('hotels_rooms_count[{$id}][' . $erp_hotelroomsize->erp_hotelroomsize_id . ']'
        , set_value('hotels_rooms_count[{$id}][' . $erp_hotelroomsize->erp_hotelroomsize_id . ']')
        , 'class="input-small FRight" style="width: 50px !important;" pkgid=\' + {$id} + \' rpnrel="' . $erp_hotelroomsize->erp_hotelroomsize_id . '"  id=\"hotels_rooms_count_\' + {$id} + \'_' . $erp_hotelroomsize->erp_hotelroomsize_id . '\" placeholder="0"')
?></td>
    <? endforeach ?>
    <? endif ?>
    </tr>
    </table>
    </td>
    </tr>
    */
                                }.toString().replace(/{\$id}/g, id).replace(/{arrivedate}/g, arrivedate).slice(14, - 3);
                                        $('tbody.internalhotels').append(new_row);
                                        $('.datetimepicker').datetimepicker({dateFormat: 'yy-mm-dd', timeFormat: 'HH:mm'});
                                        $('.chosen-select').chosen({width: "100%"});
                                        h++;
                                }

                                function remove_hrow(id)
                                {
                                $('tr[hrel=' + id + ']').remove();
                                        $('.internalhotels').find('tr[rel=' + id + ']').remove();
                                        var hidden_input = '<input type="hidden" name="hotels_remove[]" value="' + id + '" />';
                                        $('#div_tbl_hotels').append(hidden_input);
                                        h--;
                                }

                                $('tbody.internalhotels').on('change', 'select[cityrel]', function() {
                                var rowrel = $(this).attr('cityrel');
                                        var cityid = $(this).val();
                                        $.get('<?= site_url('safa_trip_internaltrips/get_hotel_by_city/') ?>/' + cityid, function(data) {
                                        $('select[hotelrel=' + rowrel + ']').html(data);
                                                $('select[hotelrel=' + rowrel + ']').trigger("chosen:updated");
                                        });
                                });
                                        $('tbody.cls_tbl_filghts').on('change', 'select[erp_port_id_from_rel]', function() {
                                var rowrel = $(this).attr('erp_port_id_from_rel');
                                        var erp_port_id_from = $(this).val();
                                        $.get('<?= site_url('safa_trip_internaltrips/get_ports_halls_by_port/') ?>/' + erp_port_id_from, function(data) {
                                        $('select[erp_port_hall_id_from_rel=' + rowrel + ']').html(data);
                                                $('select[erp_port_hall_id_from_rel=' + rowrel + ']').trigger("chosen:updated");
                                        });
                                });
                                        $('tbody.cls_tbl_filghts').on('change', 'select[erp_port_id_to_rel]', function() {
                                var rowrel = $(this).attr('erp_port_id_to_rel');
                                        var erp_port_id_to = $(this).val();
                                        $.get('<?= site_url('safa_trip_internaltrips/get_ports_halls_by_port/') ?>/' + erp_port_id_to, function(data) {
                                        $('select[erp_port_hall_id_to_rel=' + rowrel + ']').html(data);
                                                $('select[erp_port_hall_id_to_rel=' + rowrel + ']').trigger("chosen:updated");
                                        });
                                });
                                        $('tbody.cls_tbl_filghts').on('change', 'select,input[flight_states_rel]', function() {
                                var rowrel = $(this).attr('flight_states_rel');
                                        var safa_transporter_code = $('select[flight_states_rel=' + rowrel + ']').val();
                                        var flight_number = $('#flightno\\[' + rowrel + '\\]').val();
                                        var flight_date = $('#flight_date\\[' + rowrel + '\\]').val();
                                        //alert(safa_transporter_code+'-'+flight_number+'-'+flight_date);

                                        var dataString = 'safa_transporter_code=' + safa_transporter_code + '&flight_number=' + flight_number + '&flight_date=' + flight_date;
                                        $.ajax
                                        ({
                                        type: 'POST',
                                                url: '<?php echo base_url() . 'safa_trip_internaltrips/get_ports_halls_by_states'; ?>',
                                                data: dataString,
                                                cache: false,
                                                success: function(html)
                                                {
                                                var data_arr = JSON.parse(html);
                                                        var erp_port_id_from_value = data_arr[0]['erp_port_id_from'];
                                                        var erp_port_hall_id_from_value = data_arr[0]['erp_port_hall_id_from'];
                                                        var erp_port_id_to_value = data_arr[0]['erp_port_id_to'];
                                                        var erp_port_hall_id_to_value = data_arr[0]['erp_port_hall_id_to'];
                                                        $('#erp_port_id_from\\[' + rowrel + '\\]').val(erp_port_id_from_value);
                                                        $('#erp_port_hall_id_from\\[' + rowrel + '\\]').val(erp_port_hall_id_from_value);
                                                        $('#erp_port_id_to\\[' + rowrel + '\\]').val(erp_port_id_to_value);
                                                        $('#erp_port_hall_id_to\\[' + rowrel + '\\]').val(erp_port_hall_id_to_value);
                                                }
                                        });
                                });
                                        function new_tourismplace() {
                                        var id = t;
                                                var new_row = function() {/*
                                                 <tr class="table-row" trel="{$id}">
                                                 <td>
                                                 <input type='hidden' name='tourism_safa_internalsegment_id[{$id}]'  id='tourism_safa_internalsegment_id[{$id}]'  value='0'/>
                                                                              
<?= form_dropdown('safa_tourismplace_id[{$id}]', $safa_tourismplaces, set_value("safa_tourismplace_id[{\$id}]"), 'class="validate[required] chosen-select chosen-rtl safa_tourismplace_id" tourismrel="{$id}" id="safa_tourismplace_id[{$id}]"') ?></td>
                                                 <td><?= form_input('tourism_date[{$id}]', set_value("tourism_date[{\$id}]"), 'class="validate[required] datetimepicker" tourdaterel="{$id}" readonly="readonly" style="width:150px" id="tourism_date[{$id}]"') ?></td>
                                                 <td><a href="javascript:void(0);" onclick="remove_trow('{$id}')"><span class="icon-trash"></span> </a></td>
                                                 </tr>
                                                 */
                                                }.toString().replace(/{\$id}/g, id).slice(14, - 3);
                                                $('tbody.tourismplaces').append(new_row);
                                                var selectrel = $('select[cityrel] option[value=1]:selected').parent().attr('cityrel');
                                                $('input[tourdaterel=' + id + ']').datetimepicker({
                                        dateFormat: 'yy-mm-dd',
                                                minDate: new Date($('input[startrel=' + selectrel + ']').val()),
                                                maxDate: new Date($('input[endrel=' + selectrel + ']').val())
                                        }).val(addDays(new Date($('input[startrel=' + selectrel + ']').val()), parseInt($('input[nightsrel=' + selectrel + ']').val()) / 2) + " 07:00");
                                                t++;
                                        }

                                function remove_trow(id)
                                {
                                $('tr[trel=' + id + ']').remove();
                                        var hidden_input = '<input type="hidden" name="tourismplaces_remove[]" value="' + id + '" />';
                                        $('#div_tbl_tourismplaces').append(hidden_input);
                                        t--;
                                }

                                $('tbody.tourismplaces').on('change', 'select[tourismrel]', function() {

                                var trow = $(this).attr('tourismrel');
                                        //alert(trow);
                                        if ($(this).val() == 1) {
                                var selectrel = $('select[cityrel] option[value=1]:selected').parent().attr('cityrel');
                                } else {
                                var selectrel = $('select[cityrel] option[value=2]:selected').parent().attr('cityrel');
                                }

//alert($('input[startrel=' + selectrel + ']').val());

                                $('input[tourdaterel=' + trow + ']').datetimepicker("destroy").datetimepicker({
                                dateFormat: 'yy-mm-dd',
                                        minDate: new Date($('input[startrel=' + selectrel + ']').val()),
                                        maxDate: new Date($('input[endrel=' + selectrel + ']').val())
                                }).val(addDays(new Date($('input[startrel=' + selectrel + ']').val()), parseInt($('input[nightsrel=' + selectrel + ']').val()) / 2) + " 07:00");
                                        //By Gouda.
                                        if ($('input[startrel=' + selectrel + ']').val() == undefined) {
                                $('#tourism_date\\[' + trow + '\\]').val('');
                                }

                                });</script>
    <script type="text/javascript">

                                        function fix_dates() {
                                        if ($('#erp_transportertype_id').val() == 2){

                                        var arriverel = $('tbody#going_flight_tbody tr select.erp_path_type option[value=2]:selected').parent().attr('pathrel');
                                                if (arriverel == undefined) {
                                        arriverel = $('tbody#going_flight_tbody tr input.erp_path_type[value=2]').parent().attr('pathrel');
                                        }
                                        //alert(arriverel);
                                        var arrivedate = ($('input[arriverel=' + arriverel + ']')).val();
                                                //alert(arrivedate);

                                                if (arrivedate == undefined) {
                                        if ($('#last_exit_date').val() != undefined) {
                                        arrivedate = $('#last_exit_date').val();
                                                //alert(arrivedate+'  ffff');
                                        }
                                        } else {
                                        arrivedatearray = arrivedate.split(' ');
                                                arrivedate = arrivedatearray[0];
                                        }
                                        //alert(arrivedate);
                                        } else if ($('#erp_transportertype_id').val() == 1){
                                        arrivedate = $('#land_going_datetime').val();
                                                arrivedatearray = arrivedate.split(' ');
                                                arrivedate = arrivedatearray[0];
                                        } else if ($('#erp_transportertype_id').val() == 3){
                                        arrivedate = $('#sea_going_datetime').val();
                                                arrivedatearray = arrivedate.split(' ');
                                                arrivedate = arrivedatearray[0];
                                        }

                                        var enddate = '';
                                                var first = true;
                                                $('tbody.internalhotels tr[hrel]').each(function() {
                                        var rel = $(this).attr('hrel');
                                                if (!first) {
                                        arrivedate = enddate;
                                        }

                                        //alert(rel);

                                        first = false;
                                                $('input[startrel=' + rel + ']').val(arrivedate);
                                                var arrdate = new Date(arrivedate);
                                                enddate = addDays(arrdate, parseInt($('input[nightsrel=' + rel + ']').val()));
                                                $('input[endrel=' + rel + ']').val(enddate);
                                        });
                                        }

                                $('tbody#going_flight_tbody').on('change', 'input[arriverel]', function() {
                                fix_dates();
                                });
                                        function addDays(myDate, days)
                                        {
                                        var date_standard = new Date(myDate.getTime() + days * 24 * 60 * 60 * 1000);
                                                var month = (date_standard.getMonth() + 1);
                                                var return_date = date_standard.getFullYear() + "-" + ('0' + month).slice( - 2) + "-" + ('0' + date_standard.getDate()).slice( - 2);
                                                return return_date;
                                        }

                                function substractDays(myDate, days)
                                {
                                var date_standard = new Date(myDate.getTime() - days * 24 * 60 * 60 * 1000);
                                        var month = (date_standard.getMonth() + 1);
                                        var return_date = date_standard.getFullYear() + "-" + ('0' + month).slice( - 2) + "-" + ('0' + date_standard.getDate()).slice( - 2);
                                        return return_date;
                                }


    </script>



    <script>
                                function add_hotel(hotel_counter)
                                {
                                $.fancybox({
                                'type': 'iframe',
                                        'width' : 500,
                                        'height' : 300,
                                        'autoDimensions' : false,
                                        'autoScale' : false,
                                        'href' : "<?php echo site_url('safa_trip_internaltrips/add_hotel_popup'); ?>/" + hotel_counter,
                                        afterClose: function() {
                                        //location.reload();

                                        jQuery.ajaxSetup({async:false});
                                                $('#div_hotels tr').each(function () {
                                        var rowrel = $(this).attr('inner_hrel');
                                                //alert(rowrel);
                                                var cityid = $('select[cityrel=' + rowrel + ']').val();
                                                //alert(cityid);

                                                var old_value = $('#erp_hotel_id\\[' + rowrel + '\\]').val();
                                                $.get('<?= site_url('safa_trip_internaltrips/get_hotel_by_city/') ?>/' + cityid, function(data) {
                                                $('#erp_hotel_id\\[' + rowrel + '\\]').html(data);
                                                        $('#erp_hotel_id\\[' + rowrel + '\\]').trigger("chosen:updated");
                                                });
                                                $('#erp_hotel_id\\[' + rowrel + '\\]').val(old_value)
                                                $('#erp_hotel_id\\[' + rowrel + '\\]').trigger("chosen:updated");
                                        });
                                        }

                                });
                                }
    </script>


    <script>
                                function add_transporter()
                                {
                                $.fancybox({
                                'type': 'iframe',
                                        'width' : 500,
                                        'height' : 300,
                                        'autoDimensions' : false,
                                        'autoScale' : false,
                                        'href' : "<?php echo site_url('uo/trip_internaltrip/add_transporter_popup'); ?>/",
                                        afterClose: function() {
                                        //location.reload();

                                        jQuery.ajaxSetup({async:false});
                                                var transporter_counter = 0;
                                                $('#div_trip_going_group tr').each(function () {
                                        var rowrel = $(this).attr('inner_rel');
                                                var old_value = $('#safa_transporter_code\\[' + rowrel + '\\]').val();
                                                $.get('<?= site_url('safa_trip_internaltrips/get_transporters/') ?>', function(data) {

                                                $('#safa_transporter_code\\[' + rowrel + '\\]').html(data);
                                                        $('#safa_transporter_code\\[' + rowrel + '\\]').trigger("chosen:updated");
                                                });
                                                $('#safa_transporter_code\\[' + rowrel + '\\]').val(old_value)
                                                $('#safa_transporter_code\\[' + rowrel + '\\]').trigger("chosen:updated");
                                                transporter_counter++;
                                        });
                                                var transporter_counter = 0;
                                                $('#div_trip_return_group tr').each(function () {
                                        var rowrel = $(this).attr('rel');
                                                var old_value = $('#safa_transporter_code\\[' + rowrel + '\\]').val();
                                                $.get('<?= site_url('safa_trip_internaltrips/get_transporters/') ?>', function(data) {
                                                $('#safa_transporter_code\\[' + rowrel + '\\]').html(data);
                                                        $('#safa_transporter_code\\[' + rowrel + '\\]').trigger("chosen:updated");
                                                });
                                                $('#safa_transporter_code\\[' + rowrel + '\\]').val(old_value)
                                                $('#safa_transporter_code\\[' + rowrel + '\\]').trigger("chosen:updated");
                                                transporter_counter++;
                                        });
                                        }

                                });
                                }
    </script>

    <script>

                                function delete_flights(id)
                                {


                                $('.cls_tbl_filghts').find('tr[rel=' + id + ']').remove();
                                        var hidden_input = '<input type="hidden" name="flights_remove[]" value="' + id + '" />';
                                        $('#div_tbl_filghts').append(hidden_input);
                                        if (id < 100) {
                                c1--;
                                } else {
                                c2--;
                                }
                                }
    </script>

    <script>
                                $('.datetime').datetimepicker({dateFormat: 'yy-mm-dd', timeFormat: 'HH:mm'});</script>

    <script>
                                        function add_internalsegment(id) {

                                        var select_port_start = ''
                                                var select_port_end = ''

                                                if ($('#erp_transportertype_id').val() == 2){
                                        select_port_start = "<select name=\"internalpassage_port_start[" + id + "]\" class=\"chosen-select chosen-rtl input-full port start_point \" id=\"internalpassage_port_start_" + id + "\" tabindex=\"4\">"
                                                < ? foreach($erp_sa_ports as $key = > $value): ? >
                                                + "           <option value=\"<?= $key ?>\"><?= $value ?></option>"
                                                < ? endforeach ? >
                                                + "    </select>";
                                                select_port_end = "       <select name=\"internalpassage_port_end[" + id + "]\" class=\"chosen-select chosen-rtl input-full port end_point  \"   id=\"internalpassage_port_end_" + id + "\" tabindex=\"4\">"
                                                < ? foreach($erp_sa_ports as $key = > $value): ? >
                                                + "           <option value=\"<?= $key ?>\"><?= $value ?></option>"
                                                < ? endforeach ? >
                                                + "    </select>";
                                        } else if ($('#erp_transportertype_id').val() == 1){

                                        select_port_start = "<select name=\"internalpassage_port_start[" + id + "]\" class=\"chosen-select chosen-rtl input-full port start_point \" id=\"internalpassage_port_start_" + id + "\" tabindex=\"4\">"
                                                < ? foreach($land_erp_ports_sa as $key = > $value): ? >
                                                + "           <option value=\"<?= $key ?>\"><?= $value ?></option>"
                                                < ? endforeach ? >
                                                + "    </select>";
                                                select_port_end = "       <select name=\"internalpassage_port_end[" + id + "]\" class=\"chosen-select chosen-rtl input-full port end_point \"   id=\"internalpassage_port_end_" + id + "\" tabindex=\"4\">"
                                                < ? foreach($land_erp_ports_sa as $key = > $value): ? >
                                                + "           <option value=\"<?= $key ?>\"><?= $value ?></option>"
                                                < ? endforeach ? >
                                                + "    </select>";
                                        } else if ($('#erp_transportertype_id').val() == 3){

                                        select_port_start = "<select name=\"internalpassage_port_start[" + id + "]\" class=\"chosen-select chosen-rtl input-full port start_point \" id=\"internalpassage_port_start_" + id + "\" tabindex=\"4\">"
                                                < ? foreach($sea_erp_ports_sa as $key = > $value): ? >
                                                + "           <option value=\"<?= $key ?>\"><?= $value ?></option>"
                                                < ? endforeach ? >
                                                + "    </select>";
                                                select_port_end = "       <select name=\"internalpassage_port_end[" + id + "]\" class=\"chosen-select chosen-rtl input-full port end_point \"   id=\"internalpassage_port_end_" + id + "\" tabindex=\"4\">"
                                                < ? foreach($sea_erp_ports_sa as $key = > $value): ? >
                                                + "           <option value=\"<?= $key ?>\"><?= $value ?></option>"
                                                < ? endforeach ? >
                                                + "    </select>";
                                        }

                                        var new_row = [
                                                "<tr rel=\"" + id + "\">",
                                                "    <td>",
                                                "       <select name=\"internalpassage_type[" + id + "]\" class=\"chosen-select chosen-rtl input-full validate[required] \" id=\"internalpassage_type_" + id + "\" tabindex=\"4\">",
                                                < ? foreach($safa_internalpassagetypes as $key = > $value): ? >
                                                "           <option value=\"<?= $key ?>\"><?= $value ?></option>",
                                                < ? endforeach ? >
                                                "    </select>",
                                                "<script>",
                                                "$(document).ready(function() {",
                                                '    $("#internalpassage_type_' + id + '").change(function() {',
                                                "        var val_ = $(this).val();",
                                                '        show_ports_' + id + '(val_);',
                                                "    });",
                                                "});",
                                                'function show_ports_' + id + '(pass_type) {',
                                                " //   alert(pass_type);",
                                                " load_multiselect();",
                                                '$("#start_point_con_' + id + '").show();',
                                                '$("#end_point_con_' + id + '").show();',
                                                '$("#start_point_con_' + id + '").children().hide();',
                                                '$("#end_point_con_' + id + '").children().hide();',
                                                'if (pass_type == 1) {',
                                                '    $("#internalpassage_port_start_' + id + '_chosen").show();',
                                                '    $("#internalpassage_hotel_end_' + id + '_chosen").show();',
                                                '}',
                                                'if (pass_type == 2) {',
                                                '    $("#internalpassage_hotel_start_' + id + '_chosen").show();',
                                                '    $("#internalpassage_port_end_' + id + '_chosen").show();',
                                                '    $("#end_point_con_' + id + '").find(".error").show();',
                                                '}',
                                                'if (pass_type == 3) {',
                                                '    $("#internalpassage_hotel_start_' + id + '_chosen").show();',
                                                '    $("#internalpassage_torismplace_end_' + id + '_chosen").show();',
                                                '}',
                                                'if (pass_type == 4) {',
                                                '    $("#internalpassage_hotel_start_' + id + '_chosen").show();',
                                                '    $("#internalpassage_hotel_end_' + id + '_chosen").show();',
                                                '}',
                                                "}",
                                                '$(".start_point").change(function() {',
                                                '	var id_attr = $(this).attr("id");',
                                                '	var id_arr = id_attr.split("_");',
                                                '	id = id_arr[id_arr.length - 1];',
                                                '	$("#hdn_start_point_"+id).val($(this).val());',
                                                '});',
                                                '$(".end_point").change(function () {',
                                                '	var id_attr = $(this).attr("id");',
                                                '	var id_arr = id_attr.split("_");',
                                                '	id = id_arr[id_arr.length - 1];',
                                                '	//alert($(this).val());',
                                                '	$("#hdn_end_point_"+id).val($(this).val());',
                                                '});',
                                                "<\/script>",
                                                "    </td>",
                                                "    <td>",
                                                "<input type='hidden' id='hdn_start_point_" + id + "' value='0'></input>",
                                                "<div id='start_point_con_" + id + "' style='display:none' >",
                                                "       <select name=\"internalpassage_hotel_start[" + id + "]\" class=\"chosen-select chosen-rtl input-full hotel start_point \"   id=\"internalpassage_hotel_start_" + id + "\" tabindex=\"4\">",
                                                < ? foreach($erp_hotels as $key = > $value): ? >
                                                "           <option value=\"<?= $key ?>\"><?= $value ?></option>",
                                                < ? endforeach ? >
                                                "    </select>",
                                                "       <select name=\"internalpassage_torismplace_start[" + id + "]\" class=\"chosen-select chosen-rtl input-full place start_point \"   id=\"internalpassage_torismplace_start_" + id + "\" tabindex=\"4\">",
                                                < ? foreach($safa_tourismplaces as $key = > $value): ? >
                                                "           <option value=\"<?= $key ?>\"><?= $value ?></option>",
                                                < ? endforeach ? >
                                                "    </select>",
                                                select_port_start,
                                                "    </div>",
                                                "    </td>",
                                                "    <td>",
                                                "<input type='hidden' id='hdn_end_point_" + id + "' value='0'></input>",
                                                "<div id='end_point_con_" + id + "' style='display:none'>",
                                                "       <select name=\"internalpassage_hotel_end[" + id + "]\" class=\"chosen-select chosen-rtl input-full hotel end_point \"   id=\"internalpassage_hotel_end_" + id + "\" tabindex=\"4\">",
                                                < ? foreach($erp_hotels as $key = > $value): ? >
                                                "           <option value=\"<?= $key ?>\"><?= $value ?></option>",
                                                < ? endforeach ? >
                                                "    </select>",
                                                "       <select name=\"internalpassage_torismplace_end[" + id + "]\" class=\"chosen-select chosen-rtl input-full place end_point \"    id=\"internalpassage_torismplace_end_" + id + "\" tabindex=\"4\">",
                                                < ? foreach($safa_tourismplaces as $key = > $value): ? >
                                                "           <option value=\"<?= $key ?>\"><?= $value ?></option>",
                                                < ? endforeach ? >
                                                "    </select>",
                                                select_port_end,
                                                "    </div>",
                                                "    </td>",
                                                '     <td><?php echo form_input("internalpassage_startdatatime[' + id + ']", FALSE, " class=\"validate[required] datetimepicker \"   ") ?>',
                                                "    </td>",
                                                '     <td><?php echo form_input("internalpassage_enddatatime[' + id + ']", FALSE, " class=\"validate[required] datetimepicker \"   ") ?>',
                                                "<script>",
                                                '$(".datetimepicker").datetimepicker({dateFormat: "yy-mm-dd", timeFormat: "HH:mm"});',
                                                "<\/script>",
                                                "    </td>",
                                                '     <!--<td><?php echo form_input("seatscount[' + id + ']", FALSE, " class=\" validate[custom[integer]] seatscount' + id + '\"  style=\"width:50px\" ") ?>',
                                                "    </td>-->",
                                                '     <td><?php echo form_input("internalpassage_notes[' + id + ']", FALSE, " class=\"internalpassage_notes' + id + '\"   style=\"width:120px\" ") ?>',
                                                "    </td>",
                                                "    <td class=\"TAC\">",
                                                "        <a href=\"javascript:void(0)\" onclick=\"repeat_passages('" + id + "')\"><span class=\"icon-copy\"></span></a>",
                                                "        <a href=\"javascript:void(0)\" onclick=\"delete_passages('" + id + "')\"><span class=\"icon-trash\"></span></a>",
                                                "    </td>",
                                                "</tr>"
                                        ].join("\n");
                                                $('#tbl_body_internalsegments').append(new_row);
                                        }

                                function delete_passages(id)
                                {
                                if (window.confirm('<?= lang('global_are_you_sure_you_want_to_delete') ?>')) {
                                $('#tbl_internalsegments').find('tr[rel=' + id + ']').remove();
                                        var hidden_input = '<input type="hidden" name="passages_remove[]" value="' + id + '" />';
                                        $('#div_tbl_internalsegments').append(hidden_input);
                                }
                                }

                                $(".start_point").change(function() {
                                var id_attr = $(this).attr('id');
                                        var id_arr = id_attr.split('_');
                                        id = id_arr[id_arr.length - 1];
                                        //alert($(this).val());
                                        $('#hdn_start_point_' + id).val($(this).val());
                                });
                                        $(".end_point").change(function () {
                                var id_attr = $(this).attr('id');
                                        var id_arr = id_attr.split('_');
                                        id = id_arr[id_arr.length - 1];
                                        //alert($(this).val());
                                        $('#hdn_end_point_' + id).val($(this).val());
                                });
                                        function repeat_passages(id)
                                        {
                                        if (typeof repeat_counter == 'undefined') {
                                        repeat_counter = 100;
                                        }
                                        if (window.confirm('<?= lang('are_you_sure_you_want_to_repeat_this_row') ?>')) {
                                        //var new_row$('#tbl_internalsegments').find('tr[rel=' + id + ']').html();
                                        //$('#tbl_body_internalsegments').append(new_row);

                                        var safa_internalpassagetype = $('#internalpassage_type_' + id).val();
                                                var start_point = $('#hdn_start_point_' + id).val();
                                                var end_point = $('#hdn_end_point_' + id).val();
                                                var startdatatime = $('input[name=internalpassage_startdatatime\\[' + id + '\\]]').val();
                                                var enddatatime = $('input[name=internalpassage_enddatatime\\[' + id + '\\]]').val();
                                                //var seatscount = $('input[name=seatscount\\['+id+'\\]]').val(); 
                                                var seatscount = 0;
                                                var notes = $('input[name=internalpassage_notes\\[' + id + '\\]]').val();
                                                add_internalsegment_for_generate(repeat_counter, safa_internalpassagetype, start_point, end_point, startdatatime, enddatatime, seatscount, notes);
                                                repeat_counter++;
                                        }
                                        }
    </script>


    <script>
                                function show_ports(pass_type, id) {

                                $("#start_point_con" + id).children().hide();
                                        $("#end_point_con" + id).children().hide();
                                        if (pass_type == 1) {
                                //show the data for arrive//
                                $("#start_point_con" + id).find('.port').show();
                                        $("#end_point_con" + id).find('.hotel').show();
                                }
                                if (pass_type == 2) {
                                // show data for leave//
                                $("#start_point_con" + id).find('.hotel').show();
                                        $("#end_point_con" + id).find('.port').show();
                                        $("#end_point_con" + id).find('.error').show();
                                }
                                if (pass_type == 3) {
                                // show data for place//
                                $("#start_point_con" + id).find('.hotel').show();
                                        $("#end_point_con" + id).find('.place').show();
                                }
                                if (pass_type == 4) {
                                // show data for transport//
                                $("#start_point_con" + id).find('.hotel').show();
                                        $("#end_point_con" + id).find('.hotel').show();
                                }
                                }
    </script>
    <script>
                                $(document).ready(function() {
                                $("#internalpassage_type").change(function() {
                                var val_ = $(this).val();
                                        show_ports(val_);
                                });
                                });</script>
    <script>
                                        $(document).ready(function() {
                                var pass_type = $("#internalpassage_type").val();
                                        show_ports(pass_type);
                                });</script>

    <script>
                                        $('.datetimepicker').datetimepicker({dateFormat: 'yy-mm-dd', timeFormat: 'HH:mm'});</script>

    <script>
                                        function validate_saving()
                                        {
                                        if ($('#div_hotels tr').length === 1) {
                                        alert('<?php echo lang('you_should_add_one_hotel_at_least'); ?>');
                                                return false;
                                        }

                                        if ($('#tbl_internalsegments tr').length === 1) {
                                        alert('<?php echo lang('you_should_generate_internaltrip_or_add_one_segment_at_least'); ?>');
                                                return false;
                                        }

                                        var seatscount = 0;
                                                if (!isNaN(parseInt($('input[name=adult_seats]').val()))) {
                                        seatscount = seatscount + parseInt($('input[name=adult_seats]').val());
                                        }
                                        if (!isNaN(parseInt($('input[name=child_seats]').val()))) {
                                        seatscount = seatscount + parseInt($('input[name=child_seats]').val());
                                        }
                                        if (!isNaN(parseInt($('input[name=baby_seats]').val()))) {
                                        seatscount = seatscount + parseInt($('input[name=baby_seats]').val());
                                        }
                                        if (isNaN(seatscount)) {
                                        seatscount = 0;
                                        }

                                        var total_detail_seats_count = 0;
                                                $(".seats_count").each(function() {
                                        if (!isNaN(parseInt($(this).val()))) {
                                        total_detail_seats_count = total_detail_seats_count + parseInt($(this).val());
                                        }
                                        });
<?php if ($safa_trip_internaltrip_id == 0) { ?>
                                            if ($('#drivers_and_buses tr').length > 0) {
                                            if (total_detail_seats_count != seatscount) {
                                            //alert(total_detail_seats_count+' '+seatscount);
                                            alert('<?php echo lang('seats_count_should_equal_total_detail_seats_count'); ?>');
                                                    return false;
                                            }
                                            }
<?php } ?>


                                        }

                                function generate_internalsegments()
                                {
                                if ($('#div_hotels tr').length === 1) {
                                alert('<?php echo lang('you_should_add_one_hotel_at_least'); ?>');
                                        return false;
                                }

                                $("#tbl_body_internalsegments tr").remove();
                                        var seatscount = 0;
                                        if (!isNaN(parseInt($('input[name=adult_seats]').val()))) {
                                seatscount = seatscount + parseInt($('input[name=adult_seats]').val());
                                }
                                if (!isNaN(parseInt($('input[name=child_seats]').val()))) {
                                seatscount = seatscount + parseInt($('input[name=child_seats]').val());
                                }
                                if (!isNaN(parseInt($('input[name=baby_seats]').val()))) {
                                seatscount = seatscount + parseInt($('input[name=baby_seats]').val());
                                }
                                if (isNaN(seatscount)) {
                                seatscount = 0;
                                }

                                //------------------- Arriving Path ----------------------------------
                                last_makka_hotel = '';
                                        last_madina_hotel = '';
                                        var startdatatime = '';
                                        var enddatatime = '';
                                        var start_point = '';
                                        var end_point = '';
                                        if ($('#erp_transportertype_id').val() == 2){
                                startdatatime = ($('input[arriverel=' + 0 + ']')).val();
                                        start_point = ($('#erp_port_id_to\\[0\\]')).val();
                                } else if ($('#erp_transportertype_id').val() == 1){
                                startdatatime = $("#land_going_datetime").val();
                                        start_point = ($('#land_going_erp_port_id')).val();
                                } else if ($('#erp_transportertype_id').val() == 3){
                                startdatatime = $("#sea_going_datetime").val();
                                        start_point = ($('#sea_going_erp_port_id')).val();
                                }

                                end_point = ($('#erp_hotel_id\\[0\\]')).val();
                                        if (($('#erp_city_id\\[0\\]')).val() == 1) {
                                last_makka_hotel = ($('#erp_hotel_id\\[0\\]')).val();
                                } else if (($('#erp_city_id\\[0\\]')).val() == 2) {
                                last_madina_hotel = ($('#erp_hotel_id\\[0\\]')).val();
                                }

                                var startdatatime_arr = startdatatime.split(' ');
                                        var starttime_arr = (startdatatime_arr[1]).split(':');
                                        var startdate_arr = (startdatatime_arr[0]).split('-');
                                        //alert(startdate_arr[0]+'==='+ startdate_arr[1]+'==='+ startdate_arr[2]+'==='+ starttime_arr[0]+'==='+ starttime_arr[1]);
                                        var enddatatime = new Date(startdate_arr[0], startdate_arr[1] - 1, startdate_arr[2], starttime_arr[0], starttime_arr[1]);
                                        //alert(enddatatime);
                                        var enddatatime_hrs = enddatatime.getHours();
                                        enddatatime_hrs += 3;
                                        //alert(enddatatime);
                                        enddatatime.setHours(enddatatime_hrs);
                                        //alert(enddatatime);
                                        enddatatime_hours = enddatatime.getHours();
                                        if (enddatatime_hours == 0) {
                                enddatatime_hours = '00';
                                }
                                enddatatime_minutes = enddatatime.getMinutes();
                                        if (enddatatime_minutes == 0) {
                                enddatatime_minutes = '00';
                                }
                                enddatatime = $.datepicker.formatDate('yy-mm-dd', enddatatime) + ' ' + enddatatime_hours + ':' + enddatatime_minutes;
                                        add_internalsegment_for_generate(0, 1, start_point, end_point, startdatatime, enddatatime, seatscount, '');
                                        //------------------- Arriving Path ----------------------------------



                                        //------------------- Transportation Path ----------------------------------
                                        var transportation_counter = 0;
                                        var start_moving = '<?php echo $this->config->item('start_moving'); ?>';
                                        var startdatatime = '';
                                        var enddatatime = '';
                                        var start_point = '';
                                        var end_point = '';
                                        $(".erp_hotel_id").each(function() {
                                if ($(this).attr('id').startsWith('erp_hotel_id')) {
                                if (transportation_counter > 0) {

                                startdatatime = ($('#arrival_date\\[' + (transportation_counter) + '\\]')).val() + ' ' + start_moving;
                                        start_point = ($('#erp_hotel_id\\[' + (transportation_counter - 1) + '\\]')).val();
                                        end_point = $(this).val();
                                        var startdatatime_arr = startdatatime.split(' ');
                                        var starttime_arr = (startdatatime_arr[1]).split(':');
                                        var startdate_arr = (startdatatime_arr[0]).split('-');
                                        //alert(startdate_arr[0]+'==='+ startdate_arr[1]+'==='+ startdate_arr[2]+'==='+ starttime_arr[0]+'==='+ starttime_arr[1]);
                                        var enddatatime = new Date(startdate_arr[0], startdate_arr[1] - 1, startdate_arr[2], starttime_arr[0], starttime_arr[1]);
                                        //alert(enddatatime);
                                        var enddatatime_hrs = enddatatime.getHours();
                                        enddatatime_hrs += 7;
                                        //alert(enddatatime);
                                        enddatatime.setHours(enddatatime_hrs);
                                        //alert(enddatatime);
                                        enddatatime_hours = enddatatime.getHours();
                                        if (enddatatime_hours == 0) {
                                enddatatime_hours = '00';
                                }
                                enddatatime_minutes = enddatatime.getMinutes();
                                        if (enddatatime_minutes == 0) {
                                enddatatime_minutes = '00';
                                }
                                enddatatime = $.datepicker.formatDate('yy-mm-dd', enddatatime) + ' ' + enddatatime_hours + ':' + enddatatime_minutes;
                                        add_internalsegment_for_generate(transportation_counter, 4, start_point, end_point, startdatatime, enddatatime, seatscount, '');
                                        if (($('#erp_city_id\\[' + (transportation_counter) + '\\]')).val() == 1) {
                                last_makka_hotel = ($('#erp_hotel_id\\[' + (transportation_counter) + '\\]')).val();
                                } else if (($('#erp_city_id\\[' + (transportation_counter) + '\\]')).val() == 2) {
                                last_madina_hotel = ($('#erp_hotel_id\\[' + (transportation_counter) + '\\]')).val();
                                }


                                }
                                transportation_counter++;
                                }
                                });
                                        //------------------- Transportation Path ----------------------------------



                                        //------------------- Tourismplace Path ----------------------------------
                                        var tourismplace_counter = transportation_counter;
                                        var startdatatime = '';
                                        var enddatatime = '';
                                        var start_point = '';
                                        var end_point = '';
                                        $(".safa_tourismplace_id").each(function() {
                                if ($(this).attr('id').startsWith('safa_tourismplace_id')) {
                                if (tourismplace_counter > 0) {

                                startdatatime = ($('#tourism_date\\[' + (tourismplace_counter - transportation_counter) + '\\]')).val();
                                        //start_point = ($('#erp_hotel_id\\['+(tourismplace_counter-1)+'\\]')).val();
                                        //Where end_ponit last hotel, or previous tourismplace.
                                        //start_point = end_point;
                                        end_point = $(this).val();
                                        if (end_point == 1) {
                                start_point = last_makka_hotel;
                                } else if (end_point == 4) {
                                start_point = last_madina_hotel;
                                }

                                var startdatatime_arr = startdatatime.split(' ');
                                        var starttime_arr = (startdatatime_arr[1]).split(':');
                                        var startdate_arr = (startdatatime_arr[0]).split('-');
                                        //alert(startdate_arr[0]+'==='+ startdate_arr[1]+'==='+ startdate_arr[2]+'==='+ starttime_arr[0]+'==='+ starttime_arr[1]);
                                        var enddatatime = new Date(startdate_arr[0], startdate_arr[1] - 1, startdate_arr[2], starttime_arr[0], starttime_arr[1]);
                                        //alert(enddatatime);
                                        var enddatatime_hrs = enddatatime.getHours();
                                        enddatatime_hrs += 7;
                                        //alert(enddatatime);
                                        enddatatime.setHours(enddatatime_hrs);
                                        //alert(enddatatime);
                                        enddatatime_hours = enddatatime.getHours();
                                        if (enddatatime_hours == 0) {
                                enddatatime_hours = '00';
                                }
                                enddatatime_minutes = enddatatime.getMinutes();
                                        if (enddatatime_minutes == 0) {
                                enddatatime_minutes = '00';
                                }
                                enddatatime = $.datepicker.formatDate('yy-mm-dd', enddatatime) + ' ' + enddatatime_hours + ':' + enddatatime_minutes;
                                        add_internalsegment_for_generate(tourismplace_counter, 3, start_point, end_point, startdatatime, enddatatime, seatscount, '');
                                }
                                tourismplace_counter++;
                                }
                                });
                                        //------------------- Tourismplace Path ----------------------------------




                                        //------------------- Leaving Path ----------------------------------
                                        var startdatatime = '';
                                        var enddatatime = '';
                                        var start_point = '';
                                        var end_point = '';
                                        var last_inner_hrel = $('#div_hotels tr:last').attr('inner_hrel');
                                        //alert(last_inner_hrel);

                                        if ($('#erp_transportertype_id').val() == 2){
                                startdatatime = ($('#flight_date\\[100\\]')).val();
                                        start_point = ($('#erp_hotel_id\\[' + last_inner_hrel + '\\]')).val();
                                        end_point = ($('#erp_port_id_from\\[100\\]')).val();
                                } else if ($('#erp_transportertype_id').val() == 1){
                                startdatatime = $("#land_return_datetime").val();
                                        start_point = ($('#erp_hotel_id\\[' + last_inner_hrel + '\\]')).val();
                                        end_point = ($('#land_return_erp_port_id')).val();
                                } else if ($('#erp_transportertype_id').val() == 3){
                                startdatatime = $("#sea_return_datetime").val();
                                        start_point = ($('#erp_hotel_id\\[' + last_inner_hrel + '\\]')).val();
                                        end_point = ($('#sea_return_erp_port_id')).val();
                                }

                                var startdatatime_arr = startdatatime.split(' ');
                                        var starttime_arr = (startdatatime_arr[1]).split(':');
                                        var startdate_arr = (startdatatime_arr[0]).split('-');
                                        //alert(startdate_arr[0]+'==='+ startdate_arr[1]+'==='+ startdate_arr[2]+'==='+ starttime_arr[0]+'==='+ starttime_arr[1]);
                                        var enddatatime = new Date(startdate_arr[0], startdate_arr[1] - 1, startdate_arr[2], starttime_arr[0], starttime_arr[1]);
                                        //alert(enddatatime);
                                        var enddatatime_hrs = enddatatime.getHours();
                                        start_city = ($('#erp_city_id\\[' + last_inner_hrel + '\\]')).val();
                                        enddatatime_hrs_value = 0;
                                        if (end_point == '6053') {
                                if (start_city == 1) {
                                enddatatime_hrs_value = 6;
                                } else if (start_city == 2) {
                                enddatatime_hrs_value = 12;
                                }
                                } else if (end_point == '6056') {
                                if (start_city == 1) {
                                enddatatime_hrs_value = 7;
                                } else if (start_city == 2) {
                                enddatatime_hrs_value = 4;
                                }
                                } else if (end_point == '6070') {
                                if (start_city == 1) {
                                enddatatime_hrs_value = 6;
                                } else if (start_city == 2) {
                                enddatatime_hrs_value = 6;
                                }
                                }

                                enddatatime_hrs -= enddatatime_hrs_value;
                                        //alert(enddatatime);
                                        enddatatime.setHours(enddatatime_hrs);
                                        //alert(enddatatime);
                                        enddatatime_hours = enddatatime.getHours();
                                        if (enddatatime_hours == 0) {
                                enddatatime_hours = '00';
                                }
                                enddatatime_minutes = enddatatime.getMinutes();
                                        if (enddatatime_minutes == 0) {
                                enddatatime_minutes = '00';
                                }
                                enddatatime = $.datepicker.formatDate('yy-mm-dd', enddatatime) + ' ' + enddatatime_hours + ':' + enddatatime_minutes;
                                        var enddatatime_2 = new Date(startdate_arr[0], startdate_arr[1] - 1, startdate_arr[2], starttime_arr[0], starttime_arr[1]);
                                        enddatatime_hrs_2 = enddatatime_hrs + (enddatatime_hrs_value - 2);
                                        //alert(enddatatime);
                                        enddatatime_2.setHours(enddatatime_hrs_2);
                                        //alert(enddatatime);
                                        enddatatime_hours_2 = enddatatime_2.getHours();
                                        if (enddatatime_hours_2 == 0) {
                                enddatatime_hours_2 = '00';
                                }
                                enddatatime_minutes_2 = enddatatime_2.getMinutes();
                                        if (enddatatime_minutes_2 == 0) {
                                enddatatime_minutes_2 = '00';
                                }
                                enddatatime_2 = $.datepicker.formatDate('yy-mm-dd', enddatatime_2) + ' ' + enddatatime_hours_2 + ':' + enddatatime_minutes_2;
                                        //add_internalsegment_for_generate(1, 2, startdatatime , enddatatime, seatscount);
                                        add_internalsegment_for_generate(tourismplace_counter, 2, start_point, end_point, enddatatime, enddatatime_2, seatscount, '');
                                        //------------------- Leaving Path ----------------------------------



                                        load_multiselect();
                                        $("#div_internalsegments").css('display', 'block');
                                }

                                function add_internalsegment_for_generate(id, safa_internalpassagetype, start_point, end_point, startdatatime, enddatatime, seatscount, notes) {

                                var select_port_start = ''
                                        var select_port_end = ''

                                        if ($('#erp_transportertype_id').val() == 2){
                                select_port_start = "<select name=\"internalpassage_port_start[" + id + "]\" class=\"chosen-select chosen-rtl input-full port start_point \" id=\"internalpassage_port_start_" + id + "\" tabindex=\"4\">"
                                        < ? foreach($erp_sa_ports as $key = > $value): ? >
                                        + "           <option value=\"<?= $key ?>\"><?= $value ?></option>"
                                        < ? endforeach ? >
                                        + "    </select>";
                                        select_port_end = "       <select name=\"internalpassage_port_end[" + id + "]\" class=\"chosen-select chosen-rtl input-full port end_point \"   id=\"internalpassage_port_end_" + id + "\" tabindex=\"4\">"
                                        < ? foreach($erp_sa_ports as $key = > $value): ? >
                                        + "           <option value=\"<?= $key ?>\"><?= $value ?></option>"
                                        < ? endforeach ? >
                                        + "    </select>";
                                } else if ($('#erp_transportertype_id').val() == 1){

                                select_port_start = "<select name=\"internalpassage_port_start[" + id + "]\" class=\"chosen-select chosen-rtl input-full port start_point \" id=\"internalpassage_port_start_" + id + "\" tabindex=\"4\">"
                                        < ? foreach($land_erp_ports_sa as $key = > $value): ? >
                                        + "           <option value=\"<?= $key ?>\"><?= $value ?></option>"
                                        < ? endforeach ? >
                                        + "    </select>";
                                        select_port_end = "       <select name=\"internalpassage_port_end[" + id + "]\" class=\"chosen-select chosen-rtl input-full port end_point \"   id=\"internalpassage_port_end_" + id + "\" tabindex=\"4\">"
                                        < ? foreach($land_erp_ports_sa as $key = > $value): ? >
                                        + "           <option value=\"<?= $key ?>\"><?= $value ?></option>"
                                        < ? endforeach ? >
                                        + "    </select>";
                                } else if ($('#erp_transportertype_id').val() == 3){

                                select_port_start = "<select name=\"internalpassage_port_start[" + id + "]\" class=\"chosen-select chosen-rtl input-full port start_point\" id=\"internalpassage_port_start_" + id + "\" tabindex=\"4\">"
                                        < ? foreach($sea_erp_ports_sa as $key = > $value): ? >
                                        + "           <option value=\"<?= $key ?>\"><?= $value ?></option>"
                                        < ? endforeach ? >
                                        + "    </select>";
                                        select_port_end = "       <select name=\"internalpassage_port_end[" + id + "]\" class=\"chosen-select chosen-rtl input-full port end_point \"   id=\"internalpassage_port_end_" + id + "\" tabindex=\"4\">"
                                        < ? foreach($sea_erp_ports_sa as $key = > $value): ? >
                                        + "           <option value=\"<?= $key ?>\"><?= $value ?></option>"
                                        < ? endforeach ? >
                                        + "    </select>";
                                }

                                var new_row = [
                                        "<tr rel=\"" + id + "\">",
                                        "    <td>",
                                        "       <select name=\"internalpassage_type[" + id + "]\" class=\"chosen-select chosen-rtl input-full validate[required] \" id=\"internalpassage_type_" + id + "\" tabindex=\"4\">",
                                        < ? foreach($safa_internalpassagetypes as $key = > $value): ? >
                                        "           <option value=\"<?= $key ?>\"><?= $value ?></option>",
                                        < ? endforeach ? >
                                        "    </select>",
                                        "<script>",
                                        "$(document).ready(function() {",
                                        '    $("#internalpassage_type_' + id + '").change(function() {',
                                        "        var val_ = $(this).val();",
                                        '        show_ports_' + id + '(val_);',
                                        "    });",
                                        "});",
                                        'function show_ports_' + id + '(pass_type) {',
                                        " //   alert(pass_type);",
                                        " load_multiselect();",
                                        '$("#start_point_con_' + id + '").show();',
                                        '$("#end_point_con_' + id + '").show();',
                                        '$("#start_point_con_' + id + '").children().hide();',
                                        '$("#end_point_con_' + id + '").children().hide();',
                                        'if (pass_type == 1) {',
                                        '    $("#internalpassage_port_start_' + id + '_chosen").show();',
                                        '    $("#internalpassage_hotel_end_' + id + '_chosen").show();',
                                        '}',
                                        'if (pass_type == 2) {',
                                        '    $("#internalpassage_hotel_start_' + id + '_chosen").show();',
                                        '    $("#internalpassage_port_end_' + id + '_chosen").show();',
                                        '    $("#end_point_con_' + id + '").find(".error").show();',
                                        '}',
                                        'if (pass_type == 3) {',
                                        '    $("#internalpassage_hotel_start_' + id + '_chosen").show();',
                                        '    $("#internalpassage_torismplace_end_' + id + '_chosen").show();',
                                        '}',
                                        'if (pass_type == 4) {',
                                        '    $("#internalpassage_hotel_start_' + id + '_chosen").show();',
                                        '    $("#internalpassage_hotel_end_' + id + '_chosen").show();',
                                        '}',
                                        "}",
                                        '$(".start_point").change(function() {',
                                        '	var id_attr = $(this).attr("id");',
                                        '	var id_arr = id_attr.split("_");',
                                        '	id = id_arr[id_arr.length - 1];',
                                        '	$("#hdn_start_point_"+id).val($(this).val());',
                                        '});',
                                        '$(".end_point").change(function () {',
                                        '	var id_attr = $(this).attr("id");',
                                        '	var id_arr = id_attr.split("_");',
                                        '	id = id_arr[id_arr.length - 1];',
                                        '	//alert($(this).val());',
                                        '	$("#hdn_end_point_"+id).val($(this).val());',
                                        '});',
                                        "<\/script>",
                                        "    </td>",
                                        "    <td>",
                                        "<input type='hidden' id='hdn_start_point_" + id + "' value='" + start_point + "'></input>",
                                        "<div id='start_point_con_" + id + "' style='display:none' >",
                                        "       <select name=\"internalpassage_hotel_start[" + id + "]\" class=\"chosen-select chosen-rtl input-full hotel start_point \"   id=\"internalpassage_hotel_start_" + id + "\" tabindex=\"4\">",
                                        < ? foreach($erp_hotels as $key = > $value): ? >
                                        "           <option value=\"<?= $key ?>\"><?= $value ?></option>",
                                        < ? endforeach ? >
                                        "    </select>",
                                        "       <select name=\"internalpassage_torismplace_start[" + id + "]\" class=\"chosen-select chosen-rtl input-full place start_point \"   id=\"internalpassage_torismplace_start_" + id + "\" tabindex=\"4\">",
                                        < ? foreach($safa_tourismplaces as $key = > $value): ? >
                                        "           <option value=\"<?= $key ?>\"><?= $value ?></option>",
                                        < ? endforeach ? >
                                        "    </select>",
                                        select_port_start,
                                        "    </div>",
                                        "    </td>",
                                        "    <td>",
                                        "<input type='hidden' id='hdn_end_point_" + id + "' value='" + end_point + "'></input>",
                                        "<div id='end_point_con_" + id + "' style='display:none'>",
                                        "       <select name=\"internalpassage_hotel_end[" + id + "]\" class=\"chosen-select chosen-rtl input-full hotel end_point \"   id=\"internalpassage_hotel_end_" + id + "\" tabindex=\"4\">",
                                        < ? foreach($erp_hotels as $key = > $value): ? >
                                        "           <option value=\"<?= $key ?>\"><?= $value ?></option>",
                                        < ? endforeach ? >
                                        "    </select>",
                                        "       <select name=\"internalpassage_torismplace_end[" + id + "]\" class=\"chosen-select chosen-rtl input-full place end_point \"    id=\"internalpassage_torismplace_end_" + id + "\" tabindex=\"4\">",
                                        < ? foreach($safa_tourismplaces as $key = > $value): ? >
                                        "           <option value=\"<?= $key ?>\"><?= $value ?></option>",
                                        < ? endforeach ? >
                                        "    </select>",
                                        select_port_end,
                                        "    </div>",
                                        "    </td>",
                                        '     <td><?php echo form_input("internalpassage_startdatatime[' + id + ']", set_value("internalpassage_startdatatime[' + id + ']", "'+ startdatatime +'"), " class=\"validate[required] datetimepicker \"   ") ?>',
                                        "    </td>",
                                        '     <td><?php echo form_input("internalpassage_enddatatime[' + id + ']", set_value("internalpassage_enddatatime[' + id + ']", "'+ enddatatime +'"), " class=\"validate[required] datetimepicker \"    ") ?>',
                                        "<script>",
                                        '$(".datetimepicker").datetimepicker({dateFormat: "yy-mm-dd", timeFormat: "HH:mm"});',
                                        "<\/script>",
                                        "    </td>",
                                        '    <!-- <td><?php echo form_input("seatscount[' + id + ']", set_value("seatscount[' + id + ']", "'+ seatscount +'"), " class=\" validate[custom[integer]] seatscount' + id + '\"  style=\"width:50px\"  ") ?>',
                                        "    </td> -->",
                                        '     <td><?php echo form_input("internalpassage_notes[' + id + ']", set_value("internalpassage_notes[' + id + ']", "'+ notes +'"), " class=\"internalpassage_notes' + id + '\"   style=\"width:120px\" ") ?>',
                                        "    </td>",
                                        "    <td class=\"TAC\">",
                                        "        <a href=\"javascript:void(0)\" onclick=\"repeat_passages('" + id + "')\"><span class=\"icon-copy\"></span></a>",
                                        "        <a href=\"javascript:void(0)\" onclick=\"delete_passages('" + id + "')\"><span class=\"icon-trash\"></span></a>",
                                        "    </td>",
                                        "</tr>"
                                ].join("\n");
                                        $('#tbl_body_internalsegments').append(new_row);
                                        $('#internalpassage_type_' + id).val(safa_internalpassagetype)
                                        $('#internalpassage_type_' + id).trigger("chosen:updated");
                                        window['show_ports_' + id](safa_internalpassagetype);
                                        //----------- Start Point --------------------------------
                                        $('#internalpassage_hotel_start_' + id).val(start_point)
                                        $('#internalpassage_hotel_start_' + id).trigger("chosen:updated");
                                        $('#internalpassage_torismplace_start_' + id).val(start_point)
                                        $('#internalpassage_torismplace_start_' + id).trigger("chosen:updated");
                                        $('#internalpassage_port_start_' + id).val(start_point)
                                        $('#internalpassage_port_start_' + id).trigger("chosen:updated");
                                        //--------------------------------------------------------

                                        //----------- End Point --------------------------------
                                        $('#internalpassage_hotel_end_' + id).val(end_point)
                                        $('#internalpassage_hotel_end_' + id).trigger("chosen:updated");
                                        $('#internalpassage_torismplace_end_' + id).val(end_point)
                                        $('#internalpassage_torismplace_end_' + id).trigger("chosen:updated");
                                        $('#internalpassage_port_end_' + id).val(end_point)
                                        $('#internalpassage_port_end_' + id).trigger("chosen:updated");
                                        //--------------------------------------------------------
                                }
    </script>

    <script>
                                $('#safa_internaltrip_type_id').live('change', function() {
                                if ($(this).val() == 1) {
                                $("#safa_ito_id").addClass("validate[required]");
                                        $("#safa_transporter_id").addClass("validate[required]");
                                        document.getElementById('lnk_add_drivers_and_buses').style.display = 'block';
                                } else {
                                $("#safa_ito_id").removeClass("validate[required]");
                                        $("#safa_transporter_id").removeClass("validate[required]");
                                        document.getElementById('lnk_add_drivers_and_buses').style.display = 'none';
                                        $("#drivers_and_buses tr").each(function(){
                                this.parentNode.removeChild(this);
                                });
                                }
                                });</script>



    <script>
                                        load_multiselect();
                                        function load_multiselect() {
                                        var config = {
                                        '.chosen-select': {width: "100%"},
                                                '.chosen-select-deselect': {allow_single_deselect: true},
                                                '.chosen-select-no-single': {disable_search_threshold: 10},
                                                '.chosen-select-no-results': {no_results_text: 'Oops, nothing found!'},
                                                '.chosen-select-width': {width: "95%"}
                                        }
                                        for (var selector in config) {
                                        $(selector).chosen(config[selector]);
                                        }
                                        }
                                function add_drivers_and_buses(id) {


                                var new_row = [
                                        "<tr rel=\"" + id + "\">",
                                        " <!--   <td>",
                                        "       <select name=\"drivers_and_buses_safa_transporters_drivers[" + id + "]\" class=\"chosen-select chosen-rtl input-full  validate[required] drivers_and_buses_safa_transporters_drivers\" id=\"drivers_and_buses_safa_transporters_drivers_" + id + "\" tabindex=\"4\">",
                                        < ? foreach($safa_transporters_drivers as $key = > $value): ? >
                                        "           <option value=\"<?= $key ?>\"><?= $value ?></option>",
                                        < ? endforeach ? >
                                        "   </select>",
                                        "</td>",
                                        "    <td>",
                                        "<script>",
                                        '$("#drivers_and_buses_safa_transporters_drivers_' + id + '").change(function(){',
                                        'check_for_redundency($(this).val(),$(this).attr("id"));',
                                        'var safa_transporters_drivers_id=$(this).val();',
                                        'var dataString = "safa_transporters_drivers_id="+ safa_transporters_drivers_id;',
                                        '$.ajax',
                                        '({',
                                        '	type: "POST",',
                                        '	url: "<?php echo base_url() . 'uo/all_movements/getDriverData'; ?>",',
                                        '	data: dataString,',
                                        '	cache: false,',
                                        '	success: function(html)',
                                        '	{',
                                        '    	//alert(html);',
                                        '		$("#driver_data_' + id + '").html(html);',
                                        '	}',
                                        '});',
                                        "});",
                                        "<\/script>",
                                        "    <div id=\"driver_data_" + id + "\"> </div> ",
                                        "    </td>",
                                        "    <td>",
                                        "       <select name=\"drivers_and_buses_safa_transporters_buses[" + id + "]\" class=\"chosen-select chosen-rtl input-full validate[required] drivers_and_buses_safa_transporters_buses \" id=\"drivers_and_buses_safa_transporters_buses_" + id + "\" tabindex=\"4\">",
                                        < ? foreach($safa_transporters_buses as $key = > $value): ? >
                                        "           <option value=\"<?= $key ?>\"><?= $value ?></option>",
                                        < ? endforeach ? >
                                        "   </select>",
                                        "    </td>",
                                        "    <td>",
                                        "<script>",
                                        '$("#drivers_and_buses_safa_transporters_buses_' + id + '").change(function(){',
                                        'var safa_transporters_buses_id=$(this).val();',
                                        'var dataString = "safa_transporters_buses_id="+ safa_transporters_buses_id;',
                                        '$.ajax',
                                        '({',
                                        '	type: "POST",',
                                        '	url: "<?php echo base_url() . 'uo/all_movements/getBusData'; ?>",',
                                        '	data: dataString,',
                                        '	cache: false,',
                                        '	success: function(html)',
                                        '	{',
                                        '    	//alert(html);',
                                        '		$("#bus_data_' + id + '").html(html);',
                                        '	}',
                                        '});',
                                        "});",
                                        "<\/script>",
                                        "    <div id=\"bus_data_" + id + "\"> </div> ",
                                        "    </td>-->",
                                        "<td>",
                                        "       <select name=\"safa_buses_brands[" + id + "]\" class=\"chosen-select chosen-rtl input-full  validate[required] safa_buses_brands\" id=\"safa_buses_brands_" + id + "\" tabindex=\"4\">",
                                        < ? foreach($safa_buses_brands as $key = > $value): ? >
                                        "           <option value=\"<?= $key ?>\"><?= $value ?></option>",
                                        < ? endforeach ? >
                                        "   </select>",
                                        "</td>",
                                        '     <td><?php echo form_input("bus_count[' + id + ']", set_value("bus_count[' + id + ']", ""), " class=\" validate[custom[integer]] bus_count \"  style=\"width:150px\"  ") ?>',
                                        "    </td>",
                                        '     <td><?php echo form_input("seats_count[' + id + ']", set_value("seats_count[' + id + ']", ""), " class=\" validate[custom[integer]] seats_count \"  style=\"width:150px\"  ") ?>',
                                        "    </td>",
                                        "    <td class=\"TAC\">",
                                        "        <a href=\"javascript:void(0)\" onclick=\"delete_drivers_and_buses('" + id + "')\"><span class=\"icon-trash\"></span></a>",
                                        "    </td>",
                                        "</tr>"
                                ].join("\n");
                                        $('#drivers_and_buses').append(new_row);
                                        //------------------- update drivers and buses for selected transporter -----------------
                                        var safa_transporter_id = $('#safa_transporter_id').val();
                                        var dataString = "safa_transporter_id=" + safa_transporter_id;
                                        $.ajax
                                        ({
                                        type: "POST",
                                                url: "<?php echo base_url() . 'uo/all_movements/getBusesDriversByTransporterIdAjax'; ?>",
                                                data: dataString,
                                                cache: false,
                                                success: function(html)
                                                {
                                                //alert(html);
                                                var data_arr = JSON.parse(html);
                                                        $(".drivers_and_buses_safa_transporters_drivers").html(data_arr[0]['drivers']);
                                                        $(".drivers_and_buses_safa_transporters_drivers").trigger("chosen:updated");
                                                        $(".drivers_and_buses_safa_transporters_buses").html(data_arr[0]['buses']);
                                                        $(".drivers_and_buses_safa_transporters_buses").trigger("chosen:updated");
                                                }
                                        });
                                        //-----------------------------------------------------------------------------------------


                                }
                                function delete_drivers_and_buses(id, database)
                                {
                                if (typeof database == 'undefined')
                                {
                                $('.drivers_and_buses').find('tr[rel="' + id + '"]').remove();
                                }
                                else
                                {
                                $('.drivers_and_buses').find('tr[rel=' + id + ']').remove();
                                        var hidden_input = '<input type="hidden" name="drivers_and_buses_remove[]" value="' + id + '" />';
                                        $('#drivers_and_buses').append(hidden_input);
                                }
                                }

                                function check_for_redundency(this_value, this_id)
                                {

                                var current_drivers_and_buses_safa_transporters_drivers = this_value;
                                        var current_select_input_id = this_id;
                                        $(".drivers_and_buses_safa_transporters_drivers").each(function() {
                                if ($(this).attr('id') != current_select_input_id) {
                                if (current_drivers_and_buses_safa_transporters_drivers == $(this).val()) {
                                alert('<?= lang('drivers_and_buses_safa_transporters_drivers_was_inserted_before') ?>');
                                        $('#' + this_id).closest('tr').remove();
                                }
                                }
                                });
                                }

                                $("#safa_transporter_id").change(function(){
                                var safa_transporter_id = $(this).val();
                                        var dataString = "safa_transporter_id=" + safa_transporter_id;
                                        $.ajax
                                        ({
                                        type: "POST",
                                                url: "<?php echo base_url() . 'uo/all_movements/getBusesDriversByTransporterIdAjax'; ?>",
                                                data: dataString,
                                                cache: false,
                                                success: function(html)
                                                {
                                                //alert(html);
                                                var data_arr = JSON.parse(html);
                                                        $(".drivers_and_buses_safa_transporters_drivers").html(data_arr[0]['drivers']);
                                                        $(".drivers_and_buses_safa_transporters_drivers").trigger("chosen:updated");
                                                        $(".drivers_and_buses_safa_transporters_buses").html(data_arr[0]['buses']);
                                                        $(".drivers_and_buses_safa_transporters_buses").trigger("chosen:updated");
                                                }
                                        });
                                });</script>





</div>





<script type="text/javascript">
                                    setInterval(function(){save_internaltrip()}, 30000);
                                    function save_internaltrip()
                                    {
                                    var dv_internaltrip_manage_storage = $('#dv_internaltrip_manage_storage').html();
                                            if (typeof (Storage) !== "undefined") {
                                    localStorage.dv_internaltrip_manage_storage_<?php echo $safa_trip_internaltrip_id; ?> = dv_internaltrip_manage_storage;
                                    } else {
                                    //alert('<?php echo lang('browser_not_support_storage'); ?>');
                                    }
                                    }

                            $(document).ready(function() {
                            if (typeof (Storage) !== "undefined") {

                            if (typeof (localStorage.dv_internaltrip_manage_storage_<?php echo $safa_trip_internaltrip_id; ?>) !== 'undefined') {

                            //if(confirm('<?php echo lang('load_storage'); ?>')) {
                            //	$('#dv_internaltrip_manage_storage').html(localStorage.dv_internaltrip_manage_storage_<?php echo $safa_trip_internaltrip_id; ?>);
                            //}

                            }
                            }

//	$('#load_storage').click(function() {
//	    if (typeof (Storage) !== "undefined") {
//	        $('.darg_drop').html(localStorage.demod);
//	        load_drag_drop();
//	    } else {
//	        alert('<?php echo lang('browser_not_support_storage'); ?>');
//	    }
//	});


                            });
</script>

