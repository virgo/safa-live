<!DOCTYPE html>
<html lang="<?= lang('global_lang') ?>" dir="<?= lang('global_direction') ?>">
    <head>  
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
        <link rel="icon" type="image/ico" href="favicon.ico"/>
        <title>><?= lang('global_title') ?></title>
        <link href="<?= CSS ?>/stylesheets.css" rel="stylesheet" type="text/css" />
        <link href="<?= JS ?>/css-js/lang-bar.css" rel="stylesheet" type="text/css" />
        <script type='text/javascript' src='<?= JS ?>/plugins/jquery/jquery-1.9.1.min.js'></script>
        <? if (lang('global_lang') == 'ar'): ?>
            <link href="<?= CSS ?>/stylesheet-ar.css" rel="stylesheet" type="text/css" />  
         <? endif ?>
            <script type="text/javascript">// <![CDATA[
        $(window).load(function() { $("#loading_layer").fadeOut("slow"); })
        // ]]>
        </script>
        <style>
        #loading_layer{
       position:absolute;
       width:100%;
       height:100%;
       top:0;
       left:0;
       z-index:999999;
       background-color: #FFF;
       text-align: center;
       margin: 0 auto;
       background:#e8e5db url(<?= IMAGES ?>/bg_default.jpg);
       }
        </style>
        
        <script type='text/javascript' src='<?= JS ?>/plugins/jquery/jquery-ui-1.10.1.custom.min.js'></script>
        <script type='text/javascript' src='<?= JS ?>/plugins/jquery/jquery-migrate-1.1.1.min.js'></script>

        <script type='text/javascript' src='<?= JS ?>/plugins/jquery/globalize.js'></script>
        <script type='text/javascript' src='<?= JS ?>/plugins/other/excanvas.js'></script>

        <script type='text/javascript' src='<?= JS ?>/plugins/other/jquery.mousewheel.min.js'></script>

        <script type='text/javascript' src='<?= JS ?>/plugins/bootstrap/bootstrap.min.js'></script>
        <script type='text/javascript' src='<?= JS ?>/plugins/bootstrap/bootstrap-fileupload.min.js'></script>

        <script type='text/javascript' src='<?= JS ?>/plugins/cookies/jquery.cookies.2.2.0.min.js'></script>

        <script type='text/javascript' src='<?= JS ?>/plugins/fancybox/jquery.fancybox.pack.js'></script>

        <script type='text/javascript' src='<?= JS ?>/plugins/jflot/jquery.flot.js'></script>    
        <script type='text/javascript' src='<?= JS ?>/plugins/jflot/jquery.flot.stack.js'></script>    
        <script type='text/javascript' src='<?= JS ?>/plugins/jflot/jquery.flot.pie.js'></script>
        <script type='text/javascript' src='<?= JS ?>/plugins/jflot/jquery.flot.resize.js'></script>

        <script type='text/javascript' src='<?= JS ?>/plugins/epiechart/jquery.easy-pie-chart.js'></script>
        <script type='text/javascript' src='<?= JS ?>/plugins/knob/jquery.knob.js'></script>

        <script type='text/javascript' src='<?= JS ?>/plugins/sparklines/jquery.sparkline.min.js'></script>    

        <script type='text/javascript' src='<?= JS ?>/plugins/pnotify/jquery.pnotify.min.js'></script>

        <script type='text/javascript' src='<?= JS ?>/plugins/fullcalendar/fullcalendar.min.js'></script>        

        <script type='text/javascript' src='<?= JS ?>/plugins/datatables/jquery.dataTables.min.js'></script>    

        <script type='text/javascript' src='<?= JS ?>/plugins/wookmark/jquery.wookmark.js'></script>        

        <script type='text/javascript' src='<?= JS ?>/plugins/jbreadcrumb/jquery.jBreadCrumb.1.1.js'></script>

        <script type='text/javascript' src='<?= JS ?>/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js'></script>

        <script type='text/javascript' src="<?= JS ?>/plugins/uniform/jquery.uniform.min.js"></script>
        <script type='text/javascript' src="<?= JS ?>/plugins/select/select2.min.js"></script>
        <script type='text/javascript' src='<?= JS ?>/plugins/tagsinput/jquery.tagsinput.min.js'></script>
        <script type='text/javascript' src='<?= JS ?>/plugins/maskedinput/jquery.maskedinput-1.3.min.js'></script>
        <script type='text/javascript' src='<?= JS ?>/plugins/multiselect/jquery.multi-select.min.js'></script>    

        <script type='text/javascript' src='<?= JS ?>/plugins/validationEngine/languages/jquery.validationEngine-en.js'></script>
        <script type='text/javascript' src='<?= JS ?>/plugins/validationEngine/jquery.validationEngine.js'></script>        
        <script type='text/javascript' src='<?= JS ?>/plugins/stepywizard/jquery.stepy.js'></script>

        <script type='text/javascript' src='<?= JS ?>/plugins/animatedprogressbar/animated_progressbar.js'></script>
        <script type='text/javascript' src='<?= JS ?>/plugins/hoverintent/jquery.hoverIntent.minified.js'></script>

        <script type='text/javascript' src='<?= JS ?>/plugins/media/mediaelement-and-player.min.js'></script>    

        <script type='text/javascript' src='<?= JS ?>/plugins/cleditor/jquery.cleditor.js'></script>

        <!-- only editor.html -->
        <script type='text/javascript' src='<?= JS ?>/plugins/ckeditor/ckeditor.js'></script>
        <!-- eof only editor.html -->

        <script type='text/javascript' src='<?= JS ?>/plugins/shbrush/XRegExp.js'></script>
        <script type='text/javascript' src='<?= JS ?>/plugins/shbrush/shCore.js'></script>
        <script type='text/javascript' src='<?= JS ?>/plugins/shbrush/shBrushXml.js'></script>
        <script type='text/javascript' src='<?= JS ?>/plugins/shbrush/shBrushJScript.js'></script>
        <script type='text/javascript' src='<?= JS ?>/plugins/shbrush/shBrushCss.js'></script>    

        <script type='text/javascript' src='<?= JS ?>/plugins/filetree/jqueryFileTree.js'></script>        

        <script type='text/javascript' src='<?= JS ?>/plugins/slidernav/slidernav-min.js'></script>    
        <script type='text/javascript' src='<?= JS ?>/plugins/isotope/jquery.isotope.min.js'></script>  
        <script type='text/javascript' src='<?= JS ?>/plugins/jnotes/jquery-notes_1.0.8_min.js'></script>
        <script type='text/javascript' src='<?= JS ?>/plugins/jcrop/jquery.Jcrop.min.js'></script>
        <script type='text/javascript' src='<?= JS ?>/plugins/ibutton/jquery.ibutton.min.js'></script>

        <script type='text/javascript' src='<?= JS ?>/plugins.js'></script>
        <script type='text/javascript' src='<?= JS ?>/charts.js'></script>
        <script type='text/javascript' src='<?= JS ?>/actions.js'></script>   
        <script type='text/javascript' src='<?= JS ?>/custom/create_plugins.js'></script> 
        
        	<!--	<script type="text/javascript" charset="utf-8">
			$(document).ready(function() {
				$('#example').dataTable( {
					"oLanguage": {
						"sLengthMenu": "عرض _MENU_ في كل صفحة",
						"sZeroRecords": "عذرا لم نجد شيئا",
						"sInfo": "إظهر _START_ إلى _END_ من _TOTAL_ مدخل",
						"sInfoEmpty": "إظهر 0 ألى 0 من 0 مدخل",
						"sInfoFiltered": "(filtered from _MAX_ إجمالي المدخلات)",
     				 	"sSearch": "البحث:"
    
					}
				} );
				$('#example_filter label input:text').focus();
			} );
	
   
		</script> -->
        

        <script type="text/javascript">
            function showDiv(toggle) {
                document.getElementById(toggle).style.display = 'block';
            }
        </script>
    </head>
    <body>
        <div id="loading_layer"><img src="<?= IMAGES2 ?>/loading.gif" alt="" border="0" style="margin-top:200px;"/></div>
        <script>
            $('#loading_layer').click(function() {$("#loading_layer").fadeOut("slow"); });
        </script>
<!--        <div class="header">
            <a href="#" class="logo" style="margin-right:20px;"></a>
            <div style=" margin:0 0 0 10px;">
                <div id='rbnavbar'>
                    <ul id='rbnav'>
                        <li>
                            <a href='#'>Language</a>
                            <ul>
                                <li><a href='javascript:void(0)'id="english" onclick="set_cookie('<?//=$this->uri->segment(1)."_language" ?>',this.id,365)" >English</a></li>
                                 <li><a href='javascript:void(0)' id="arabic" onclick="set_cookie('<?//=$this->uri->segment(1)."_language" ?>',this.id,365)" >عربى</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>-->
    <div class="content">
   

        






