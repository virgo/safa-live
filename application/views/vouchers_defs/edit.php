<?= $this->load->view('admin/header') ?>

<div id="tabs">
    <div id="tabs-1" class="package_ground">
        <span style="" class="title_style"><?= lang('vouchers_defs_title') ?></span>
        <div class="box box-gray">
            <div class="box-content">
                <? if (validation_errors()): ?>
                    <div class="error">
                        <?php echo validation_errors(); ?>
                    </div>
                <? endif ?>

                <?= form_open_multipart('admin/vouchers_defs/edit/' . $item->id) ?>

<? if(config('language') == 'arabic'): ?>
                <div class="form-item">
                    <label class="label_color">
                        <?= lang('vouchers_defs_name_ar') ?>
                        <span class="required">*</span>
                    </label>        

                    <?= form_input('name_ar', set_value('name_ar', $item->name_ar), 'class="text_input"') ?>
                </div> 
<? else: ?>
                <div class="form-item">
                    <label class="label_color">
                        <?= lang('vouchers_defs_name_la') ?>
                        <span class="required">*</span>
                    </label>        

                    <?= form_input('name_la', set_value('name_la', $item->name_la), 'class="text_input"') ?>
                </div> 
<? endif; ?>

                <div class="form-item">
                    <label class="label_color">
                        <?= lang('vouchers_defs_main_type') ?>
                        <span class="required">*</span>
                    </label>        

                    <?= form_dropdown('main_type', ddmenu('vouchers_types'), set_value('main_type', $item->main_type), 'class="text_input"') ?>
                </div> 

                <div class="form-item">
                    <label class="label_color">
                        <?= lang('vouchers_defs_cash_account') ?>
                        <span class="required">*</span>
                    </label>        

                    <?= form_dropdown('cash_account', ddmenu('accounts'), set_value('cash_account', $item->cash_account), 'class="text_input"') ?>
                </div> 

                <div class="form-item">
                    <label class="label_color">
                        <?= lang('vouchers_defs_currency') ?>
                        <span class="required">*</span>
                    </label>        

                    <?= form_dropdown('currency', $currencies, set_value('currency', $item->currency), 'class="text_input"') ?>
                </div> 

                <div class="form-item">
                    <label class="label_color">
                        <?= lang('vouchers_defs_notes') ?>
                    </label>        

                    <?= form_textarea('notes', set_value('notes', $item->notes), 'class="text_input"') ?>
                </div> 

                <? if(permission('manage_all_firms')): ?>
                <div class="form-item">
                    <? if($item->firm_id == '0') $checked ='TRUE'; else $checked = 'FALSE'; ?>
                    <?= form_checkbox('all_firms', '1',$checked) ?><label class="label_color"><?= lang('use_all_firms') ?></label>
                </div>
                <? endif; ?>


                <div class="submit-row" id="center_buttons">
                    <input type="submit" class="button" value="<?= lang('global_submit') ?>" />
                    <input type="button" value="<?= lang('global_back') ?>" class="button" onclick="window.location='<?= site_url('admin/vouchers_defs/index') ?>'" />
                </div>

            </div>
        </div>
    </div>
</div>
<?= form_close() ?>
