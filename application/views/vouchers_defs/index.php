<?= $this->load->view('admin/header') ?>
<h1><?= lang('vouchers_defs_title')?>
  <a class="button" href="<?= site_url('admin/vouchers_defs/add' ) ?>"><?= lang('global_add') ?></a>
</h1>
<table width="100%" class="table">
    <tr>
<? if(config('language') == 'arabic'): ?>
        <th><?= lang('vouchers_defs_name_ar') ?></th>
<? else: ?>
        <th><?= lang('vouchers_defs_name_la') ?></th>
<? endif; ?>
        <th><?= lang('vouchers_defs_main_type') ?></th>
        <th><?= lang('vouchers_defs_currency') ?></th>
        <th><?= lang('global_operations') ?></th>
    </tr>
    <? if ($items && is_array($items) && count($items)): ?>
        <? foreach ($items as $item): ?>
    <tr>
<? if(config('language') == 'arabic'): ?>
        <td><?= $item->name_ar?></td>
<? else: ?>
        <td><?= $item->name_la?></td>
<? endif; ?>
        <td><?=ddmenu('vouchers_types',$item->main_type)?></td>
        <td><?=ddmenu('currencies',$item->currency)?></td>
        
        <td width='30%'>
            <? if($item->firm_id != '0' || permission('manage_all_firms')): ?>
            <a class="button" href="<?= site_url('admin/vouchers_defs/edit/'. $item->id ) ?>"><?= lang('global_edit') ?></a>
            <a class="button" onclick="return confirm('Are you sure to delete <?=$item->name_la ?>')"   href="<?= site_url('admin/vouchers_defs/delete/' . $item->id) ?>"><?= lang('delete') ?></a> 
            <? endif; ?>
        </td>
    </tr>
    <? endforeach?>
    <? else :?>
    <tr><td colspan="7" align="center"><?=lang('branches_no_rows')?></td></tr>
    <? endif ?>

</table>

<?= $pagination ?>
<?= $this->load->view('admin/footer') ?>
