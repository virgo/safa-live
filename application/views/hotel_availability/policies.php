<link href="<?= NEW_CSS_JS . '_' . lang('DIR') ?>/common-css.css" rel="stylesheet" type="text/css" />
<style>
    .widget {
        margin: 0 !important;
        text-align: right;
        height: 100%;
    }
</style>

<div class='widget' >
    <div class="widget-header">

            <div class="widget-header-icon Fright"><i class="icos-pencil2"></i></div>
            <div class="widget-header-title Fright">
                 سياسات الفندق </div>
        </div>
    <? foreach ($hotel_details as $detail) : ?>
        <div class="widget-container">
            <div class='row-fluid' >
                <span style="margin: 0 5px"><?= $detail->{name()} ?></span> : <span><?= $detail->policy_description ?></span>
            </div>
        </div>
    <? endforeach ?>
</div>
<br style="clear: both;" />



