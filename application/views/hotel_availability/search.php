<!--
<div class="widget">
    <div class="widget-header">
        <div class="widget-header-icon Fright">
            <span class="icos-pencil2"></span>
        </div>
        <div class="widget-header-title Fright">
            <?= lang('ha_main_information') ?>
        </div>
    </div>
    <div class="widget-container">
        <? if(validation_errors()): ?>
            <?= validation_errors() ?>        
        <? endif ?>
        <div class="row-fluid">
            <div class="span3">
                <div class="span4 TAL Pleft10">
                    <label> <?= lang('ha_date_from') ?>:   </label> 
                </div>
                <div class="span8">
                    <?= form_input('date_from', set_value('date_from'), 'class="validate[required] input-full date_from"') ?>
                </div>
            </div>
            <div class="span3">
                <div class="span4 TAL Pleft10">
              <label> <?= lang('ha_date_to') ?>:   </label>   
                </div>
                <div class="span8">
                    <?= form_input('date_to', set_value('date_to'), 'class="validate[required] input-full date_to"') ?>
                </div>
            </div>
</div>
    </div>
</div>
-->
<script>.widget{width:98%;}</script>

<form id="search" action="<?= current_url() ?>" method="post">
<div class="widget">
    <div class="widget-header">
        <div class="widget-header-icon Fright">
            <span class="icos-pencil2"></span>
        </div>
        <div class="widget-header-title Fright">
            <?= lang('ha_main_search_information') ?>
        </div>
    </div>    
    <div class="widget-container">
        <div class="row-fluid">
            <div class="span3">
                <div class="span4 TAL Pleft10">
                <label><?= lang('ha_city_id') ?>:</label>
                </div>
                <div class="span8">
                    <?= form_dropdown('city_id', $cities, set_value('city_id'), 'class=" input-full chosen-rtl" style="width:100%;" id="city_id"') ?>
                </div>
            </div>
            <div style="margin-bottom:10px;" class="span6">
                <div class="span2">
                <label><?= lang('ha_hotel_id') ?>:</label>
                </div>
                <div class="span10">
                    <?= form_multiselect('hotel_id[]', $hotels, set_value('hotel_id'), 'class=" chosen-rtl" style="width:100%;" id="hotel_id" data-placeholder="'.lang('global_all').'"') ?>
                </div>
            </div>
            
            <!-- 
            <div class="span3">
                <div class="span4 TAL Pleft10">
                    <label><?= lang('ha_res_num_of_rooms') ?>:</label>
                </div>
                <div class="span8">
                    <?= form_input('rooms', set_value('rooms'), 'class=" input-full" style="width:100%;" id="rooms"') ?>
                </div>
            </div>
             -->
            
<!--            <div class="span2">
                <div class="span6 TAL Pleft10">
                <?= lang('ha_res_nights') ?>:
                </div>
                <div class="span4">
                    <?= form_input('nights', set_value('nights'), 'class=" input-full" style="width:100%;" id="nights"') ?>
                </div>
            </div>-->
        </div>
        <div class="row-fluid">
            <div class="span3">
                <div class="span4 TAL Pleft10">
                    <label><?= lang('ha_date_from') ?>:</label>
                </div>
                <div class="span8">
                    <?= form_input('date_from', set_value('date_from'), 'class="validate[required] input-full date_from datepicker"') ?>
                </div>
            </div>
            <div class="span3">
                <div class="span4 TAL Pleft10">
                <label><?= lang('ha_date_to') ?>:</label>
                </div>
                <div class="span8">
                    <?= form_input('date_to', set_value('date_to'), 'class="validate[required] input-full date_to"') ?>
                </div>
            </div>
            <div class="span3">
                <div class="span4 TAL Pleft10">
                   <label> <?= lang('ha_housing_type') ?>:  </label> 
                </div>
                <div class="span8">
                    <?= form_dropdown('housing_type_id', $housing, set_value('housing_type_id'), ' class=" input-full"') ?>
                </div>
            </div>
            <div class="span3">
                <div class="span4 TAL Pleft10">
                  <label> <?= lang('ha_room_type') ?>:  </label>  
                </div>
                <div class="span8">
                    <?= form_dropdown('room_type_id', $room_types, set_value('room_type_id'), ' class=" input-full"') ?>
                </div>
            </div>
        </div>
        <div class="row-fluid">
            <div class="span3">
                <div class="span4 TAL Pleft10">
                   <label>  <?= lang('ha_price_from') ?>: </label> 
                </div>
                <div class="span8">
                    <?= form_input('price_from', set_value('price_from'), ' class=" input-full"') ?>
                </div>
            </div>
            <div class="span3">
                <div class="span4 TAL Pleft10">
                  <label>  <?= lang('ha_price_to') ?>:  </label> 
                </div>
                <div class="span8">
                    <?= form_input('price_to', set_value('price_to'), ' class=" input-full"') ?>
                </div>
            </div>
            <div class="span3">
                <div class="span4 TAL Pleft10">
                  <label>  <?= lang('ha_currency') ?>: </label>  
                </div>
                <div class="span8">
                    <?= form_dropdown('currency', $currencies, set_value('currency'), ' class=" input-full"') ?>
                </div>
            </div>
                <div class="span3">
                    <div class="span4 TAL Pleft10">
                        <label>  <?= lang('ha_availability_type') ?>: </label>  
        </div>
                    <div class="span8">
                        <?=
                        form_dropdown('ha_type',
                                array(
                            '' => lang('global_all'),
                            '1' => lang('ha_show_my_availability'),
                            '2' => lang('ha_show_other_availability')
                                ), set_value('currency'), ' class=" input-full"')
                        ?>
                    </div>
                </div>
            </div>
        <div class="row-fluid">
            <div class="span3">
                <div class="span4 TAL Pleft10">
                 <label>  <?= lang('ha_distance_from_haram') ?>:  </label>  
                </div>
                <div class="span8">
                    <?= form_input('distance_from_haram', set_value('distance_from_haram'), ' class=" input-full"') ?>
                </div>
            </div>
            <div class="span3">
                <div class="span4 TAL Pleft10">
                  <label> <?= lang('ha_distance_to_haram') ?>:  </label>  
                </div>
                <div class="span8">
                    <?= form_input('distance_to_haram', set_value('distance_to_haram'), ' class=" input-full"') ?>
                </div>
            </div>
            <div class="span3">
                <div class="span4 TAL Pleft10">
               <label> <?= lang('ha_level') ?>:  </label> 
                </div>
                <div class="span8">
                    <?= form_multiselect('level[]', $levels, set_value('level'), 'class="chosen-rtl input-full" multiple tabindex="4" data-placeholder="'.lang('global_all').'"') ?>
                </div>
            </div>
        </div>
        <div class="row-fluid">
            <div class="span3">
                <div class="span4 TAL Pleft10">
               <label> <?= lang('ha_nationalities') ?>:  </label> 
                </div>
                <div class="span8">
                    <?= form_multiselect('nationalities[]', $nationalities, set_value('nationalities'), 'class="chosen-rtl input-full" style="width:200px" tabindex="4" data-placeholder="'.lang('global_all').'"') ?>   
                </div>
            </div>
            <div class="span3">
                <div class="span4 TAL Pleft10">
               <label> <?= lang('ha_hotel_features') ?>:  </label> 
                </div>
                <div class="span8">
                    <?= form_multiselect('hotel_features[]', $hotel_advantages, set_value('hotel_features'), 'class="chosen-rtl input-full" tabindex="4" data-placeholder="'.lang('global_all').'"') ?>
                </div>
            </div>
            <div class="span3">
                <div class="span4 TAL Pleft10">
               <label> <?= lang('ha_inclusive_service') ?>:  </label> 
                </div>
                <div class="span8">
                    <?= form_multiselect('inclusive_service[]', $additions, set_value('inclusive_service'), 'class="chosen-rtl input-full" tabindex="4" data-placeholder="'.lang('global_all').'"') ?>   
                </div>
            </div>
            <div class="span3">
                <div class="span4 TAL Pleft10">
               <label> <?= lang('ha_exclusive_service') ?>:  </label> 
                </div>
                <div class="span8">
                    <?= form_multiselect('exclusive_service[]', $additions, set_value('exclusive_service'), 'class="chosen-rtl input-full" tabindex="4" data-placeholder="'.lang('global_all').'"') ?>
                </div>
            </div>
        </div>
        <div class="row-fluid">
            <div class="span12" align="center">
                <input type="button" class="btn add_search" value="<?= lang('ha_add_search') ?>" style="margin:10px;padding: 5px;height: auto" />
            </div>
        </div>
    </div>
</div>
</form>   

<script type="text/javascript">
// Serialize Object jQuery Helper    
$.fn.serializeObject = function()
{
   var o = {};
   var a = this.serializeArray();
   $.each(a, function() {
       if (o[this.name]) {
           if (!o[this.name].push) {
               o[this.name] = [o[this.name]];
           }
           o[this.name].push(this.value || '');
       } else {
           o[this.name] = this.value || '';
       }
   });
   return o;
};

(function($){
   $.fn.serializeJSON = function(){
    var json = {}
    var form = $(this);
    form.find('input, select').each(function(){
      var val
      if (!this.name) return;
       if ('radio' === this.type) {
        if (json[this.name]) { return; }
         json[this.name] = this.checked ? this.value : '';
      } else if ('checkbox' === this.type) {
        val = json[this.name];
         if (!this.checked) {
          if (!val) { json[this.name] = ''; }
        } else {
          json[this.name] = 
            typeof val === 'string' ? [val, this.value] :
            $.isArray(val) ? $.merge(val, [this.value]) :
            this.value;
        }
      } else {
        json[this.name] = this.value;
      }
    })
    return json;
  }
 })(jQuery)
</script>
