<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html dir="rtl">
<head>
<title><?php echo lang('hotel_reservation_order') ?></title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="viewport" content="width=device-width">

<style>
h1 {
	color: #444;
	font-family: arial;
	font-size: 18pt;
	font-weight: bold;
}

p.first {
	color: #003300;
	font-family: arial;
	font-size: 12pt;
}

p.first span {
	color: #006600;
	font-style: italic;
}

p#second {
	color: rgb(00, 63, 127);
	font-family: times;
	font-size: 12pt;
	text-align: justify;
}

p#second>span {
	background-color: #FFFFAA;
}

table.first { /*
                color: #003300;
                */
	font-family: arial;
	font-weight: normal;
	font-size: 11pt;
	border-right: 1px solid gray;
	/*
                border-bottom: 1px solid green;
                */
	border-top: 1px solid gray;
	background-color: #EEE;
	border-elft: 1px solid gray;
}

td { /*
                border: 2px solid blue;
                background-color: #ffffee;
                
                */
	height: 30px;
}

td.second {
	border: 2px dashed green;
}

div.test {
	color: #CC0000;
	background-color: #FFFF66;
	font-family: dejavusans;
	font-size: 10pt;
	border-style: solid solid solid solid;
	border-width: 2px 2px 2px 2px;
	border-color: green #FF00FF blue red;
	text-align: center;
}
</style>
</head>

<body style="background: url('<?= base_url('static/images/head.jpg') ?>') no-repeat;" >
<div style="width: 100%; text-align: center" dir="rtl">
<h1><?php echo lang('hotel_reservation_order') ?></h1>

<table align="center" border="0" cellspacing="0" class="first"
	width="100%">
	<tr>
		<td></td>
	</tr>
	<tr>
		<td width="100px"><?php echo lang('ha_country_id'); ?></td>
		<td width="80px" style="font-size: 13px"><?php echo $item->country; ?>

		</td>
		<td width="100px"></td>
		<td width="100px"><?php echo lang('ha_city_id'); ?></td>
		<td width="80px" style="font-size: 13px"><?php echo $item->city; ?>

		</td>
	</tr>


	<tr>
		<td width="100px"><?php echo lang('ha_hotel_id'); ?></td>
		<td width="80px" style="font-size: 13px"><?php echo $item->hotel; ?>

		</td>
		<td width="100px"></td>
		<td width="100px"><?php echo lang('ha_season_id'); ?></td>
		<td width="80px" style="font-size: 13px"><?php echo $item->season; ?>

		</td>
	</tr>


	<tr>
		<td width="100px"><?php echo lang('ha_date_from'); ?></td>
		<td width="80px" style="font-size: 13px"><?php echo $item->date_from; ?>
		</td>
		<td width="100px"></td>
		<td width="100px"><?php echo lang('ha_date_to'); ?></td>
		<td width="80px" style="font-size: 13px"><?php echo $item->date_to; ?>

		</td>
	</tr>


	<tr>
		<td width="100px"><?php echo lang('ha_notes'); ?></td>
		<td width="80px" style="font-size: 13px;"><?php echo $item->notes; ?>

		</td>
		<td width="100px"></td>
		<td width="100px"></td>
		<td width="80px"></td>
	</tr>
</table>
<br />
<br />


<div
	style="overflow: hidden; border: 1px solid #DDD; border-radius: 5px; padding: 10px; margin: 20px 0;">
<h3><?php echo lang('ha_peroids') ?></h3>

<table cellspacing="0" cellpadding="0"
	style="width: 100%; float: right; direction: rtl; border: 0px; position: relative; padding: 0px; margin: 0px; border: 1px solid #DDD; font-family: dejavusans; font-weight: normal; font-size: 12pt;">
	<thead>

		<tr>
			<th
				style="background-color: #eee; color: #333; font-weight: bold; border-left: 1px solid #DDDDDD; box-shadow: 1px 1px 0 #FFFFFF inset; font-weight: 300 !important; padding: 6px; font-size: 11px; text-shadow: 1px 1px 0 #FFFFFF; text-align: center;">
				<?php echo lang('ha_from') ?></th>
			<th
				style="background-color: #eee; color: #333; font-weight: bold; border-left: 1px solid #DDDDDD; box-shadow: 1px 1px 0 #FFFFFF inset; font-weight: 300 !important; padding: 6px; font-size: 11px; text-shadow: 1px 1px 0 #FFFFFF; text-align: center;"><?php echo lang('ha_to') ?></th>


		</tr>
	</thead>
	<tbody>

	<? if (isset($item_peroids)) { ?>
	<? if (check_array($item_peroids)) { ?>
	<? foreach ($item_peroids as $item_peroid) { ?>
		<tr>

			<td
				style="border-left: 1px solid #DDDDDD; border-top: 1px solid #DDDDDD; text-align: center;">
				<?php echo $item_peroid->peroid_from; ?></td>
			<td
				style="border-left: 1px solid #DDDDDD; border-top: 1px solid #DDDDDD; text-align: center;">
				<?php echo $item_peroid->peroid_to; ?></td>


		</tr>
		<? } ?>
		<? } ?>
		<? } ?>

	</tbody>
</table>
</div>


<div
	style="overflow: hidden; border: 1px solid #DDD; border-radius: 5px; padding: 10px; margin: 20px 0;">
<h3><?php echo lang('rooms') ?></h3>

<table cellspacing="0" cellpadding="0"
	style="width: 100%; float: right; direction: rtl; border: 0px; position: relative; padding: 0px; margin: 0px; border: 1px solid #DDD;">
	<thead>

		<tr>
			<th
				style="background-color: #eee; color: #333; font-weight: bold; border-left: 1px solid #DDDDDD; box-shadow: 1px 1px 0 #FFFFFF inset; font-weight: 300 !important; padding: 6px; font-size: 11px; text-shadow: 1px 1px 0 #FFFFFF; text-align: center;"><?php echo lang('ha_housing_type') ?></th>
			<th
				style="background-color: #eee; color: #333; font-weight: bold; border-left: 1px solid #DDDDDD; box-shadow: 1px 1px 0 #FFFFFF inset; font-weight: 300 !important; padding: 6px; font-size: 11px; text-shadow: 1px 1px 0 #FFFFFF; text-align: center;"><?php echo lang('ha_room_type') ?></th>
			<th
				style="background-color: #eee; color: #333; font-weight: bold; border-left: 1px solid #DDDDDD; box-shadow: 1px 1px 0 #FFFFFF inset; font-weight: 300 !important; padding: 6px; font-size: 11px; text-shadow: 1px 1px 0 #FFFFFF; text-align: center;"><?php echo lang('ha_rooms_beds') ?></th>
			<!-- <th  style="background-color: #eee; color: #333; font-weight: bold; border-left: 1px solid #DDDDDD; box-shadow: 1px 1px 0 #FFFFFF inset; font-weight: 300 !important; padding: 6px; font-size: 11px; text-shadow: 1px 1px 0 #FFFFFF; text-align: center;"><?php echo lang('ha_closed_rooms') ?></th>-->
			<th
				style="background-color: #eee; color: #333; font-weight: bold; border-left: 1px solid #DDDDDD; box-shadow: 1px 1px 0 #FFFFFF inset; font-weight: 300 !important; padding: 6px; font-size: 11px; text-shadow: 1px 1px 0 #FFFFFF; text-align: center;"><?php echo lang('ha_etlala') ?></th>
			<!--<th  style="background-color: #eee; color: #333; font-weight: bold; border-left: 1px solid #DDDDDD; box-shadow: 1px 1px 0 #FFFFFF inset; font-weight: 300 !important; padding: 6px; font-size: 11px; text-shadow: 1px 1px 0 #FFFFFF; text-align: center;"><?php echo lang('had_max_beds_count') ?></th>
                        <th  style="background-color: #eee; color: #333; font-weight: bold; border-left: 1px solid #DDDDDD; box-shadow: 1px 1px 0 #FFFFFF inset; font-weight: 300 !important; padding: 6px; font-size: 11px; text-shadow: 1px 1px 0 #FFFFFF; text-align: center;"><?php echo lang('ha_can_be_split') ?></th>
                        <th  style="background-color: #eee; color: #333; font-weight: bold; border-left: 1px solid #DDDDDD; box-shadow: 1px 1px 0 #FFFFFF inset; font-weight: 300 !important; padding: 6px; font-size: 11px; text-shadow: 1px 1px 0 #FFFFFF; text-align: center;"><?php echo lang('ha_purchase_price_id') ?></th>
                        <th  style="background-color: #eee; color: #333; font-weight: bold; border-left: 1px solid #DDDDDD; box-shadow: 1px 1px 0 #FFFFFF inset; font-weight: 300 !important; padding: 6px; font-size: 11px; text-shadow: 1px 1px 0 #FFFFFF; text-align: center;"><?php echo lang('ha_available') ?></th>
                    -->
		</tr>


	</thead>
	<tbody>
	<? if (isset($item_rooms)) { ?>
	<? if (check_array($item_rooms)) { ?>
	<? foreach ($item_rooms as $item_room) { ?>
		<tr >

			<td
				style="border-left: 1px solid #DDDDDD; border-top: 1px solid #DDDDDD; text-align: center;">
				<?php echo $item_room->erp_housingtypes_name; ?></td>
			<td
				style="border-left: 1px solid #DDDDDD; border-top: 1px solid #DDDDDD; text-align: center;">
				<?php echo $item_room->hotelroomsize_name; ?></td>
			<td
				style="border-left: 1px solid #DDDDDD; border-top: 1px solid #DDDDDD; text-align: center;">
				<?php echo $item_room->rooms_beds_count; ?></td>
			<!--                                    <td style="border-left: 1px solid #DDDDDD; border-top: 1px solid #DDDDDD; text-align: center;" > <?php echo $item_room->closed_rooms_beds_count; ?>  </td>-->
			<td
				style="border-left: 1px solid #DDDDDD; border-top: 1px solid #DDDDDD; text-align: center;">
				<?php if ($item_room->erp_hotels_availability_view_id) echo lang('yes'); ?></td>



			<!--<td style="border-left: 1px solid #DDDDDD; border-top: 1px solid #DDDDDD; text-align: center;" > <?php echo $item_room->max_beds_count; ?>  </td>
                                    <td style="border-left: 1px solid #DDDDDD; border-top: 1px solid #DDDDDD; text-align: center;" > <?php if ($item_room->can_spiltting) echo lang('yes'); ?>  </td>

                                    <td style="border-left: 1px solid #DDDDDD; font-size: 9px; border-top: 1px solid #DDDDDD; text-align: center;" > <?php echo $item_room->purchase_price; ?>  </td>
                                    <td style="border-left: 1px solid #DDDDDD; font-size: 9px; border-top: 1px solid #DDDDDD; text-align: center;" > <?php if ($item_room->offer_status) echo lang('yes'); ?>  </td>
                                    

                                	-->

		</tr>
		<? } ?>
		<? } ?>
		<? } ?>
	</tbody>
</table>

</div>





</div>
</body>

</html>