<script>
    function har_sub(id, subelem) {
        if (!$('form').validationEngine('validate'))
            return;
        if ($('.har_sub_room_' + id + ' div').length)
        {
            $('.har_sub_room_' + id).center().slideDown(900);
        }
        else
        {
            var detail_title = '';
            window['sub_har_counter_' + id] = 1;
            rowtype = $(subelem).parent().parent().find('.housingtype').val();
            if (rowtype == 1)
                detail_title = '<?= lang('ha_room_details') ?>';
            else
                detail_title = '<?= lang('ha_bed_details') ?>';
            // SET WINDOW IN THE CENTER
            var embdder = function() {/*
             <div class="widget">
             <div class="widget-header">
             <div class="widget-header-icon Fright"> <span class="icos-pencil2"></span> </div>
             <div class="widget-header-title Fright"> {head_title}</div>
             <a class="btn Fleft" href="javascript:void(0)" onclick="add_sub_har('N' + sub_har_counter_{$har_counter}++, '{$har_counter}')">
<?= lang('ha_add') ?>
             </a>
             </div>
             
             <div class="widget-container">
             <div class="table-container">
             <table cellpadding="0" cellspacing="0" width="100%">
             <thead>
             <tr>
             <th><?= lang('ha_floor') ?></th>
             <th><?= lang('ha_section') ?></th>
             <th><?= lang('ha_res_num_of_rooms') ?></th>
             <th><?= lang('ha_room_nums') ?></th>
             <th><?= lang('global_operations') ?></th>
             </tr>
             </thead>
             <tbody class="sub_har_{$har_counter}" id='sub_har_{$har_counter}'>
             
             </tbody>
             <tfoot style="border-top: 1px solid #ccc">
             <tr>
             <td colspan="5">
             <a href="javascript:;" onclick="sub_rooms_close('{$har_counter}')" relok="{$har_counter}" class="btn"><?= lang('global_submit') ?></a> 
             <a href="javascript:;" onclick="sub_rooms_close_cancel('{$har_counter}')" relok="{$har_counter}" class="btn"><?= lang('global_close') ?></a> 
             </td>
             </tr>
             </tfoot>
             </table>
             </div>
             
             </div>
             </div><div class="my_shadow"></div>
             */
            }.toString().replace(/{\$har_counter}/g, id).replace(/{head_title}/g, detail_title).slice(14, -3);
            $('.har_sub_room_' + id).append(embdder);
            $('.har_sub_room_' + id).center();
            add_sub_har('N0', id);
        }
        // CALCULATE THE WINDOW CENTER
        $(window).resize(function() {
            $('.har_sub_' + id).center();
        });
        $('.har_sub_room_' + id).slideDown(900);
    }

    function add_sub_har(id, har_counter)
    {
//        if( ! $('form').validationEngine('validate'))
//            return;
        var new_row = [
                "<tr rel=\"" + id + "\">",
                "<td>",
                " <select name=\"sub_har_floor[" + har_counter + "][" + id + "]\" subalt=\"" + id + "\" class=\"validate[required] input-huge\">",
<? foreach ($har_floors as $key => $value): ?>
            "  <option value=\"<?= $key ?>\"><?= $value ?></option>",
<? endforeach ?>
        "</td>",
                '<td><?= form_input("sub_har_section[' + har_counter + '][' + id + ']", FALSE,
        'class="input-huge small"') ?></td>',
                '<td><?= form_input("sub_har_res_num_of_rooms[' + har_counter + '][' + id + ']", FALSE,
        'class="input-huge small numofrooms" subalt="\'+ id +\'"') ?></td>',
                '<td><?= form_input("sub_har_room_nums[' + har_counter + '][' + id + ']", FALSE,
        'class="input-huge small"') ?></td>',
                '<td class="TAC"><a href="javascript:void(0)" onclick="delete_sub_har(\'' + id + '\', \'' + har_counter + '\')"><span class="icon-trash"></span></a>',
//                '<div class="har_sub_' + id + '"></div>',
                '</td>',
                "</tr>"
        ].join("\n");
                $('#sub_har_' + har_counter).append(new_row);
    }
    function delete_sub_har(id, har_counter, database) {
        if (typeof database == 'undefined')
        {
            $('.sub_har_' + har_counter).find('tr[rel="' + id + '"]').remove();
        }
        else
        {
            $('.sub_har_' + har_counter).find('tr[rel=' + id + ']').remove();
            var hidden_input = '<input type="hidden" name="har_remove[]" value="' + id + '" />';
            $('#sub_har_' + har_counter).append(hidden_input);
        }
    }

    function sub_rooms_close(id) {
        if (!$('form').validationEngine('validate'))
            return;


        var housingtype = $('tr[rel=' + id + ']').find('.housingtype').val();
        //alert(housingtype);
        var hotelid = $('#hotel_id').val();
        var close_win = true;
        $.post('<?= site_url('hotel_availability/ajax_get_hotel_rooms') ?>', {'hotel_id': hotelid}, function(data) {

            if (data.status) {
                var roof = new Array();
                var roofrooms = new Array();
                var roofindex = 0;
                $('.har_sub_room_' + id).find('select').each(function() {
                    if (roof.indexOf($(this).val()) === -1) {
                        roof[roofindex++] = $(this).val();
                        roofrooms[$(this).val()] = $('input[subalt=' + $(this).attr('subalt') + ']').val();
                    } else {
                        roofrooms[$(this).val()] = parseInt(roofrooms[$(this).val()]) + parseInt($('input[subalt=' + $(this).attr('subalt') + ']').val());
                    }
                });
                //alert(data.floors.floor_1.rooms);
                $.each(roof, function(i, ro) {
                    if (housingtype == 1) {
                        if (typeof data.floors[ro] == 'undefined') {
                            alert("عفوا لا يوجد لديك غرف في الدور " + ro);
                            close_win = false;
                        }
                        else if (parseInt(data.floors[ro].rooms) < parseInt(roofrooms[ro])) {
                            alert("لا يمكن تخطي عدد الغرف عن " + data.floors[ro].rooms + " في الدور " + ro);
                            close_win = false;
                        }
                    } else {
                        if (typeof data.floors[ro] == 'undefined') {
                            alert("عفوا لا يوجد لديك اسره في الدور " + ro);
                            close_win = false;
                        }
                        else if (parseInt(data.floors[ro].beds) < parseInt(roofrooms[ro])) {
                            alert("لا يمكن تخطي عدد الاسرة عن " + data.floors[ro].beds + " في الدور " + ro);
                            close_win = false;
                        }
                    }
                });

            } else {
                var roomcount = parseInt($('tr[rel="' + id + '"]').find('td input.har_beds').val());
                //var roomsrows = $('#sub_har_'+id).find('input.numofrooms');
                var rooms = 0;
                $('#sub_har_' + id).find('input.numofrooms').each(function() {
                    rooms = rooms + parseInt($(this).val());
                });
                if (roomcount < rooms) {
                    if (housingtype == 1) {
                    alert("لا يمكن تخطي عدد الغرف عن " + roomcount);
                    close_win = false;
                } else {
                    alert("لا يمكن تخطي عدد الاسرة عن " + roomcount);
                    close_win = false;
                }
                }
            }

            if (close_win)
                $('.har_sub_room_' + id).slideUp(900);

        }, 'json');


    }
    function sub_rooms_close_cancel(id) {
        $('.har_sub_room_' + id + ' tr[rel*=N]').remove();
        $('.har_sub_room_' + id).slideUp(900);
    }
</script>
