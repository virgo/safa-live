<div class="wizerd-div"><a><?= lang('ha_main_information') ?></a></div>
<div class="row-fluid">
    <? if (validation_errors()): ?>
        <?= validation_errors() ?>        
    <? endif ?>

    <div class="span6">
        <div class="row-fluid">
            <div class="span12">
                <div class="span6">
                    <div class="span11">
                        <div class="span8 TAL Pleft10">
                            <?= lang('ha_country_id') ?>:
                        </div>
                        <div class="span11">
                            <?=
                            form_dropdown('country_id', $countries, set_value('country_id', $main->erp_country_id), 'class="validate[required] input-full chosen-rtl " style="width:100%;" id="country_id"')
                            ?>
                        </div>
                    </div>
                    <div class="span11">
                        <div class="span8 TAL Pleft10">
                            <?= lang('ha_hotel_id') ?>:
                        </div>
                        <div class="span11">
                            <?= form_dropdown('hotel_id', $hotels, set_value('hotel_id', $main->erp_hotel_id), 'class="validate[required] chosen-rtl" style="width:100%;" id="hotel_id"') ?>
                        </div>
                    </div>
                </div>

                <div class="span6">

                    <div class="span11">
                        <div class="span8 TAL Pleft10">
                            <?= lang('ha_city_id') ?>:
                        </div>
                        <div class="span11">
                            <?= form_dropdown('city_id', $cities, set_value('city_id', $main->erp_city_id), 'class="validate[required] input-full chosen-rtl" style="width:100%;" id="city_id"')
                            ?>
                        </div>
                    </div>
                    <div class="span11">
                        <div class="span8 TAL Pleft10">
                            <?= lang('ha_season_id') ?>:
                        </div>
                        <div class="span11">
                            <?= form_dropdown('season_id', $seasons, set_value('season_id', $main->season_id), 'class="chosen-rtl" style="width:100%;" id="season_id"') ?>
                        </div>
                    </div>

                </div>
            </div>
            <div class="span12">
                <div class="span6">
                    <div class="span6">
                        <div class="span11 TAL Pleft10">
                            <?= lang('ha_date_from') ?>:
                        </div>
                        <div class="span10">
                            <?= form_input('date_from', set_value('date_from', $main->date_from), 'class="validate[required] input-huge date_from datepicker"')
                            ?>
                            <script>
                                $('.date_from').datepicker({
                                    dateFormat: "yy-mm-dd",
                                    controlType: 'select',
                                    timeFormat: 'HH:mm',
                                    onClose: function(selectedDate) {
                                        $(".date_to").datepicker("option", "minDate", selectedDate);
                                    }
                                });
                            </script>
                        </div>
                    </div>
                    <div class="span6">
                        <div class="span11 TAL Pleft10">
                            <?= lang('ha_date_to') ?>:
                        </div>
                        <div class="span10">
                            <?= form_input('date_to', set_value('date_to', $main->date_to), 'class="validate[required] input-huge date_to datepicker"')
                            ?>
                            <script>
                                $('.date_to').datepicker({
                                    dateFormat: "yy-mm-dd",
                                    controlType: 'select',
                                    timeFormat: 'HH:mm',
                                    onClose: function(selectedDate) {
                                        $(".date_from").datepicker("option", "maxDate", selectedDate);
                                    }
                                });
                            </script>
                        </div>
                    </div>
                </div>
                <div class="span6">   



                    <div class="span6">
                        <div class="span12 TAL Pleft10">
                            <?= lang('ha_purchase_currency_id') ?>:
                        </div>
                        <div class="span12">
                            <?=
                            form_dropdown('purchase_currency_id', $currencies, set_value('purchase_currency_id', $main->purchase_currency_id), ' class="validate[required] input-huge"')
                            ?>
                        </div>
                    </div>
                    <div class="span5">
                        <div class="span12 TAL Pleft10">
                            <?= lang('ha_sales_currency_id') ?>:
                        </div>
                        <div class="span12">
                            <?= form_dropdown('sales_currency_id', $currencies, set_value('sales_currency_id', $main->sale_currency_id), ' class="validate[required] input-huge"')
                            ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="span12">
            <div class="span12">
                       
                <div class="span11">
                    <div class="span8 TAL Pleft10">
<?= lang('ha_peroids') ?>:
                    </div>
                    <div class="span12" id="dv_periods">
                        <table>
                            <thead>
                                <tr>
                                    <th><?= lang('ha_from') ?></th>
                                    <th colspan="2"><?= lang('ha_to') ?></th>
                                </tr>
                            </thead>
                            <tbody class="peroids">
                                <? if(isset($peroids)) :?>
                                <? foreach($peroids as $peroid) :?>
                                <tr rel="<?php echo $peroid->erp_hotels_availability_peroid_id;?>">
                                	<td><input type="hidden" value="<?= $peroid->erp_hotels_availability_peroid_id ?>" name="erp_hotels_availability_peroid_id[]" />
                                    
                                    <input type="date" value="<?= $peroid->peroid_from ?>" name="peroid_from[]" class="input-full datepicker" /></td>
                                    <td><input type="date" value="<?= $peroid->peroid_to ?>" name="peroid_to[]" class="input-full datepicker" /></td>
                                    <td><i style="cursor: pointer;" onclick="delete_periods(<?php echo $peroid->erp_hotels_availability_peroid_id;?>);" class="icon-remove-sign"></i></td>
                                </tr>
                                <? endforeach ?>
                                <? endif ?>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td colspan="2" ><i class="icon-plus-sign" style="cursor: pointer;" id="add_peroid"></i></td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div> 
            </div>
        </div>
            
                

        </div>

    </div>
    <div class="span6">
    <div class="span12" id="hotel_data">
        
    </div>
        
          <div class="span12">
                <div class="span6">
                    <div class="span11 TAL Pleft10">
                        <?= lang('ha_invoice_image') ?>:
                    </div>
                    <div class="span11">
                        <?= form_upload('invoice_image', set_value('invoice_image'), 'class="input-huge"')
                        ?>
                    </div>
                </div>
                <div class="span6">
                    <div class="span6">
                        <div class="span12 TAL Pleft10">
                            <?= lang('ha_invoice_number') ?>:
                        </div>
                        <div class="span11">
                            <?= form_input('invoice_number', set_value('invoice_number', $main->invoice_number), 'class=" input-huge"  ')
                            ?>   
                        </div>
                    </div>
                    <div class="span6">
                        <div class="span12 TAL Pleft10">
                            <?= lang('ha_status') ?>:
                        </div>
                        <div class="span10">
                            <?
                            $availability_status = array(
                                1 => lang('ha_status_1'),
                                2 => lang('ha_status_2'),
                                3 => lang('ha_status_3'),
                                4 => lang('ha_status_4')
                                    )
                            ?>
<?= form_dropdown('availability_status', $availability_status, set_value('availability_status', $main->availability_status), 'class=" input-huge"  ')
?>   
                        </div>
                    </div>
                </div>
            </div>
          <div class="span11">
                    <div class="span8 TAL Pleft10">
                        <?= lang('ha_nationalities') ?>:
                    </div>
                    <div class="span12">
                        <?=
                        form_multiselect('nationalities[]', $nationalities, set_value('nationalities', $selected_nationalities), 'class="chosen-select chosen-rtl input-full" multiple tabindex="4" data-placeholder="' . lang('global_all') . '"')
                        ?>   
                    </div>
                </div>
    </div>
    <div class="span12">
                    <div class="span12 TAL Pleft10">
                        <?= lang('ha_notes') ?>:
                    </div>
                    <div class="span12">
<?= form_textarea('note', set_value('note', $main->notes), 'class=" input-full" style="height:200%;width:100%!important" ') ?>   
                    </div>
                </div> 
    <script type="text/javascript" >
        $(function() {
            $('#add_peroid').click(function() {
                var new_tr = function() {/*
                    <tr>
                        <td><input type="hidden" name="erp_hotels_availability_peroid_id[]" value="0" />
                        <input type="date"  name="peroid_from[]" class="input-full datepicker" />  
                        </td>
                        <td><input type="date" name="peroid_to[]" class="input-full datepicker" /></td>
                        <td><i onclick="$(this).parent().parent().remove();" style="cursor: pointer;" class="icon-remove-sign"></i></td>
                    </tr>
                */
                }.toString();
                $('.peroids').append(new_tr);
                $('.datepicker').datepicker({
                    dateFormat: "yy-mm-dd",
                    controlType: 'select',
                    timeFormat: 'HH:mm',
                    showOtherMonths: true,
                    selectOtherMonths: true
                });
            });
        });
<? if ($main->erp_hotel_id) : ?>

            $('#hotel_data').load('<?= site_url('hotel_availability/hotel/' . $main->erp_hotel_id) ?>');
$(".fancybox").fancybox();

<? endif ?>
    </script>
</div>


<script>
function delete_periods(id)
{
    {
        $('.peroids').find('tr[rel=' + id + ']').remove();
        var hidden_input = '<input type="hidden" name="periods_remove[]" value="' + id + '" />';
        $('#dv_periods').append(hidden_input);
    }
}

</script>