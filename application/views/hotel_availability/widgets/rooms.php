<script>
    jQuery.fn.center = function()
    {
        this.css("position", "fixed");
        this.css("top", $(window).height() / 3 - (this.outerHeight() / 2));
        this.css("right", (($(window).width() / 3) - (this.width() / 2)));
        this.css("z-index", 4000);
        return this;
    }
</script><script>har_counter = 0;</script>


<div class="wizerd-div"><a><?= lang('ha_room_details') ?></a></div>

<div class="table-responsive">
    <table style="float: none;" cellpadding="0" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th><?= lang('ha_housing_type') ?></th>
                <th><?= lang('ha_room_type') ?></th>
                <th><?= lang('ha_rooms_beds') ?></th>
                <th><?= lang('ha_closed_rooms') ?></th>
                <th><?= lang('ha_etlala') ?></th>
                <th><?= lang('had_max_beds_count') ?></th>
                <th><?= lang('ha_can_be_split') ?></th>
                <th><?= lang('ha_purchase_price_id') ?></th>
                <th><?= lang('ha_available') ?></th>
                <th><a class="btn " href="javascript:void(0)" onclick="add_har('N' + har_counter++)">
                        <?= lang('ha_add') ?>
                    </a></th>
            </tr>
        </thead>
        <tbody class="har" id='har'>
            <? if (check_array($rooms)): ?>
                <? foreach ($rooms as $room): ?>
                    <tr rel="<?= $room->erp_hotels_availability_rooms_id ?>">
                        <td style="min-width: 85px;">
                            <?=
                            form_dropdown('ha_housing_type[' . $room->erp_hotels_availability_rooms_id . ']', $had_housing, set_value('ha_housing_type[' . $room->erp_hotels_availability_rooms_id . ']', $room->erp_housingtype_id), 'class="validate[required] input-huge housingtype"')
                            ?>
                        </td>
                        <td style="min-width: 85px;">
                            <?=
                            form_dropdown('har_room_type_id[' . $room->erp_hotels_availability_rooms_id . ']', $had_room_types, set_value('har_room_type_id[' . $room->erp_hotels_availability_rooms_id . ']', $room->erp_hotelroomsize_id), 'class="input-huge har_room_type_id" onChange="check_har_room_type_id()"')
                            ?>
                        </td>
                        <td><?=
                            form_input('har_beds[' . $room->erp_hotels_availability_rooms_id . ']'
                                    , set_value('har_beds[' . $room->erp_hotels_availability_rooms_id . ']', $room->rooms_beds_count)
                                    , 'class="validate[required] input-huge small"')
                            ?>
                        </td>
                        <td>
                            <?=
                            form_input('har_closed_rooms[' . $room->erp_hotels_availability_rooms_id . ']'
                                    , set_value('har_closed_rooms[' . $room->erp_hotels_availability_rooms_id . ']', $room->closed_rooms_beds_count)
                                    , 'class="input-huge small"')
                            ?>
                        </td>
                        <td style="min-width: 70px;">
<!--                            <input name="har_etlal[<?= $room->erp_hotels_availability_rooms_id ?>]" type="checkbox" value="1" <? if ($room->etlal): ?>checked="checked"<? endif ?>  />-->
<?=
                            form_dropdown('har_etlal[' . $room->erp_hotels_availability_rooms_id . ']', $erp_hotels_availability_views, set_value('har_etlal[' . $room->erp_hotels_availability_rooms_id . ']', $room->erp_hotels_availability_view_id), ' class="input-huge"')
                            ?>
                        </td>
                        <td>
                            <?= form_input('har_max_beds_count[' . $room->erp_hotels_availability_rooms_id . ']', set_value('had_max_beds_count[' . $room->erp_hotels_availability_rooms_id . ']', $room->max_beds_count), 'class="validate[required] input-huge small har_max_beds_count" rel="' . $room->erp_hotels_availability_rooms_id . '" ' . ($room->erp_hotelroomsize_id) ? "" : 'disabled') ?>
                        </td>
                        <td>
                            <input name="har_can_splitting[<?= $room->erp_hotels_availability_rooms_id ?>]" class="har_can_splitting" type="checkbox" value="1" <? if ($room->can_spiltting==1){ ?>checked="checked"<? } ?>  />
                        </td>
                        <td>
                            <?= form_input('har_purchase_price[' . $room->erp_hotels_availability_rooms_id . ']', set_value('har_purchase_price[' . $room->erp_hotels_availability_rooms_id . ']', $room->purchase_price), 'class="validate[required] input-huge small"') ?>
                        </td>
                        <td>
                            <input name="har_offer_status[<?= $room->erp_hotels_availability_rooms_id ?>]" type="checkbox" value="1" <? if ($room->offer_status): ?>checked="checked"<? endif ?> />
                        </td>
                        <td class="TAC">
                            <div class="dropdown">
                                <a class="dropdown-toggle" data-toggle="dropdown" href="#"><span class="icon-wrench"></span></a>
                                <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
                                    <li><a href="javascript:void(0)" onclick="har_sub('<?= $room->erp_hotels_availability_rooms_id ?>', this)" class="details"><span class="icon-plus"></span> <?= lang('rooms_floors') ?></a></li>
                                    
                                    <?php
                                    
                                    $used_rooms_count = $this->hotels_availability_room_details_model->get_reserved_by_erp_hotels_availability_rooms_id($room->erp_hotels_availability_rooms_id); 
                                    if(count($used_rooms_count)>0) {
                                    ?>
                                    <li><a href="javascript:void(0)" onclick="can_not_delete();"><span class="icon-trash"></span> <?= lang('rooms_remove') ?></a> </li>
                                    <?php } else { ?>
                                    <li><a href="javascript:void(0)" onclick="delete_har(<?= $room->erp_hotels_availability_rooms_id ?>, true)"><span class="icon-trash"></span> <?= lang('rooms_remove') ?></a> </li>                                    
                                    <?php } ?>
                                </ul>
                            </div>
                                <div class="har_sub_room_<?= $room->erp_hotels_availability_rooms_id ?> "  style="display: none;">
                                    <? if (ensure($this->hotels_availability_rooms_model->get_sub_rooms($room->erp_hotels_availability_rooms_id))): ?>
                                        <script type="text/javascript">
                                            var sub_har_counter_<?= $room->erp_hotels_availability_rooms_id ?> = <?= count($this->hotels_availability_rooms_model->get_sub_rooms($room->erp_hotels_availability_rooms_id)) ?>;
                                        </script>
                                        <div class="widget">
                                            <div class="widget-header">
                                                <div class="widget-header-icon Fright"> <span class="icos-pencil2"></span> </div>
                                                <div class="widget-header-title Fright"> <?= lang('ha_room_details') ?></div>
                                                <a class="btn Fleft" href="javascript:void(0)" onclick="add_sub_har('N' + sub_har_counter_<?= $room->erp_hotels_availability_rooms_id ?>++, '<?= $room->erp_hotels_availability_rooms_id ?>')">
                                                    <?= lang('ha_add') ?>
                                                </a>
                                            </div>

                                            <div class="widget-container">
                                                <div class="table-container">
                                                    <table cellpadding="0" cellspacing="0" width="100%">
                                                        <thead>
                                                            <tr>
                                                                <th><?= lang('ha_floor') ?></th>
                                                                <th><?= lang('ha_section') ?></th>
                                                                <th><?= lang('ha_rooms_beds') ?></th>
                                                                <th><?= lang('ha_room_nums') ?></th>
                                                                <th><?= lang('global_operations') ?></th>
                                                            </tr>
                                                        </thead>
                                                        <tbody class="sub_har_<?= $room->erp_hotels_availability_rooms_id ?>" id='sub_har_<?= $room->erp_hotels_availability_rooms_id ?>'>
                                                            <? foreach ($this->hotels_availability_rooms_model->get_sub_rooms($room->erp_hotels_availability_rooms_id) as $sub_room): ?>
                                                                <tr rel="<?= $sub_room->erp_hotels_availability_sub_room_id ?>">
                                                                    <td>
                                                                        <select name="sub_har_floor[<?= $room->erp_hotels_availability_rooms_id ?>][<?= $sub_room->erp_hotels_availability_sub_room_id ?>]" subalt="<?= $sub_room->erp_hotels_availability_sub_room_id ?>" class="validate[required] input-huge">
                                                                            <? foreach ($har_floors as $key => $value): ?>
                                                                                <option value="<?= $key ?>" <? if ($key == $sub_room->erp_floor_id) : ?>selected="selected"<? endif ?> ><?= $value ?></option>
                                                                            <? endforeach ?>
                                                                    </td>
                                                                    <td><?= form_input("sub_har_section[" . $room->erp_hotels_availability_rooms_id . "][" . $sub_room->erp_hotels_availability_sub_room_id . "]", set_value("sub_har_section[" . $room->erp_hotels_availability_rooms_id . "][" . $sub_room->erp_hotels_availability_sub_room_id . "]", $sub_room->section), 'class="input-huge small"') ?></td>
                                                                    <td><?= form_input("sub_har_res_num_of_rooms[" . $room->erp_hotels_availability_rooms_id . "][" . $sub_room->erp_hotels_availability_sub_room_id . "]", set_value("sub_har_res_num_of_rooms[" . $room->erp_hotels_availability_rooms_id . "][" . $sub_room->erp_hotels_availability_sub_room_id . "]", $sub_room->num_of_rooms), 'class="input-huge small numofrooms" subalt="' . $sub_room->erp_hotels_availability_sub_room_id . '" ') ?></td>
                                                                    <td><?= form_input("sub_har_room_nums[" . $room->erp_hotels_availability_rooms_id . "][" . $sub_room->erp_hotels_availability_sub_room_id . "]", set_value("sub_har_room_nums[" . $room->erp_hotels_availability_rooms_id . "][" . $sub_room->erp_hotels_availability_sub_room_id . "]", $sub_room->room_nums), 'class="input-huge small"') ?></td>
                                                                    <td class="TAC">
                                                                        <div class="dropdown">
                                                                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">العمليات<span class="icon-wrench"></span></a>
                                                                            <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
                                                                                <li><a href="javascript:void(0)" onclick="delete_sub_har('<?= $sub_room->erp_hotels_availability_sub_room_id ?>', '<?= $room->erp_hotels_availability_rooms_id ?>')"><span class="icon-trash"></span></a></li>
                                                                            </ul>
                                                                        </div>



                                                                    </td>
                                                                </tr> 
                                                            <? endforeach ?>
                                                        </tbody>
                                                        <tfoot style="border-top: 1px solid #ccc">
                                                            <tr>
                                                                <td colspan="5">
                                                                    <a href="javascript:;" onclick="sub_rooms_close('<?= $room->erp_hotels_availability_rooms_id ?>')" relok="<?= $room->erp_hotels_availability_rooms_id ?>" class="btn"><?= lang('global_submit') ?></a> 
                                                                    <a href="javascript:;" onclick="sub_rooms_close_cancel('<?= $room->erp_hotels_availability_rooms_id ?>')" relok="<?= $room->erp_hotels_availability_rooms_id ?>" class="btn"><?= lang('global_cancel') ?></a> 
                                                                </td>
                                                            </tr>
                                                        </tfoot>
                                                    </table>
                                                </div>

                                            </div>
                                        </div><div class="my_shadow" ></div> 
                                    <? endif ?>
                                </div>
                        </td>
                    </tr>
                <? endforeach ?>
            <? endif ?>
        </tbody>
    </table>
</div>



<script>
    function add_har(id) {
        if (!$('form').validationEngine('validate'))
            return;
        var new_row = [
                "<tr rel=\"" + id + "\">",
                "    <td>",
                "       <select name=\"ha_housing_type[" + id + "]\" class=\"validate[required] input-huge housingtype\">",
<? foreach ($had_housing as $key => $value): ?>
            "           <option value=\"<?= $key ?>\"><?= $value ?></option>",
<? endforeach ?>
        "    </td>",
                "    <td>",
                "       <select name=\"har_room_type_id[" + id + "]\" class=\"input-huge har_room_type_id\" onChange=\"check_har_room_type_id()\" rel=\"" + id + "\">",
<? foreach ($had_room_types as $key => $value): ?>
            "           <option value=\"<?= $key ?>\"><?= $value ?></option>",
<? endforeach ?>
        "    </td>",
                '     <td><?= form_input("har_beds[' + id + ']", FALSE, 'class="validate[required] input-huge small"') ?>',
                "    </td>",
                "    <td>",
                '        <?= form_input("har_closed_rooms[' + id + ']", FALSE, 'class=\"input-huge small\"') ?>',
                "    </td>",
                "   <td style='min-width: 70px;'>",
                "      <!-- <input name=\"har_etlal[" + id + "]\" type=\"checkbox\" value=\"1\"  /> -->",
                "       <select name=\"har_etlal[" + id + "]\" class=\"input-huge har_etlal\"  rel=\"" + id + "\">",
                <? foreach ($erp_hotels_availability_views as $key => $value): ?>
                            "           <option value=\"<?= $key ?>\"><?= $value ?></option>",
                <? endforeach ?>
                "   </select>",
                
                "   </td>",
                "   <td>",
                '       <?= form_input("har_max_beds_count[' + id + ']", false, 'class="validate[required] input-huge small har_max_beds_count" rel="\' + id + \'"') ?>',
                "   </td>",
                "   <td>",
                "       <input name=\"har_can_splitting[" + id + "]\"  class=\"har_can_splitting\" type=\"checkbox\" value=\"1\"  />",
                "   </td>",
                "   <td>",
                '       <?= form_input("har_purchase_price[' + id + ']", '0', 'class="input-huge small"') ?>',
                "   </td>",
                "   <td>",
                '        <input name="har_offer_status[' + id + ']" type="checkbox" value="1"  />',
                "   </td>",
                "    <td class=\"TAC\">",
                "   <div class=\"dropdown\"> ",
                "                <a class=\"dropdown-toggle\" data-toggle=\"dropdown\" href=\"#\"><span class=\"icon-wrench\"></span></a>",
                "                <ul class=\"dropdown-menu\" role=\"menu\" aria-labelledby=\"dLabel\">",
                "        <li><a href=\"javascript:void(0)\" onclick=\"har_sub('" + id + "',this)\" class=\"details\"><span class=\"icon-plus\"></span></a></li>",
                "        <li><a href=\"javascript:void(0)\" onclick=\"delete_har('" + id + "')\"><span class=\"icon-trash\"></span></a></li>",
                "                </ul>",
                "            </div><div class=\"har_sub_room_" + id + "\"></div>",
                "    </td>",
                "</tr>"
        ].join("\n");
                $('#har').append(new_row);
        check_har_room_type_id();
        $('.dropdown-toggle').dropdown();
//        har_sub(id);
    }

    function delete_har(id, database) {
        if (typeof database == 'undefined')
        {
            $('.har').find('tr[rel="' + id + '"]').remove();
        }
        else
        {
            $('.har').find('tr[rel=' + id + ']').remove();
            var hidden_input = '<input type="hidden" name="har_remove[]" value="' + id + '" />';
            $('#har').append(hidden_input);
        }

    }

    function can_not_delete()
    {
    	alert('<?php echo lang('you_cannot_delete_this_rooms_row_becouse_it_reserved');?>');    
    }
    
    function check_har_room_type_id() {
        $('.har_room_type_id').each(function(index) {
            var find = $('.har').find('.har_max_beds_count[rel=' + $(this).attr('rel') + ']');
            if ($(this).val() == '')
            {
                find.prop('disabled', false);
            }
            else
            {
                find.prop('disabled', true);
            }
        });
    }

    check_har_room_type_id();

    $(function() {
        $('.dropdown-toggle').dropdown();
        $(".har").on('change', ".housingtype", function() {
            var parent_tr = $(this).parent().parent();
            if ($(this).val() == 2) {
                parent_tr.find('.har_room_type_id').attr('disabled', 'disabled');
                parent_tr.find('.har_can_splitting').removeAttr('checked');
                parent_tr.find('.har_can_splitting').attr('disabled', 'disabled');
                parent_tr.find('.har_max_beds_count').attr('disabled', 'disabled');
                parent_tr.find('.widget-header-title').html('<?= lang('ha_bed_details') ?>');
            } else {
                parent_tr.find('.har_room_type_id').removeAttr('disabled');
                parent_tr.find('.har_can_splitting').removeAttr('disabled');
                parent_tr.find('.widget-header-title').html('<?= lang('ha_room_details') ?>');
            }
        });
    });

</script>
<? $this->load->view('hotel_availability/widgets/sub_rooms'); ?>