<script>res_counter = 0;</script>

<script type="text/javascript">
    var reschkelem;
    $(document).ready(function() {
        $("#btnResAllow").click(function() {
            reschkelem = $('input.group2:checked');
            $('.savebtn').css('display', 'none');
            $('button[name=saveresallow]').css('display', 'block');


            if (reschkelem.length) {
                var alertmessage = false;
                $.each($('input.group2:checked'), function(i) {
                    var chkey = $(this).attr('resalt');
                    if ($('input[type=hidden][resalt=' + chkey + ']').length) {
                        alertmessage = true;
                    }
                });
                if (alertmessage) {
                    if (!confirm('<?= lang('ha_alert_clean') ?>'))
                        return false;
                }


                $('input[name=uo_companies][value=none]').attr('checked', 'checked');
                $('#uo_companies_list').css('display', 'none').val([]);
                $('#uo_companies_list_chosen').css('display', 'none');

                $('input[name=hm_companies][value=none]').attr('checked', 'checked');
                $('#hm_companies_list').css('display', 'none').val([]);
                $('#hm_companies_list_chosen').css('display', 'none');

                $('input[name=ea][value=none]').attr('checked', 'checked');
                $('.countriesdiv').css('display', 'none');
                $('#ea_countries_list').css('display', 'none').val([]);
                $('#ea_countries_list_chosen').css('display', 'none');
                $('#ea_countries_companies_list option').remove();
                $('#ea_companies_list option').remove();
                $('input[name=individual]').removeAttr('checked');

                $.fancybox({
                    href: "#divForm",
                    minWidth: 800,
                    minHeight: 500
                });
                $('#tabs1').tabs();
            } else {
                alert("<?= lang('ha_choise_one') ?>");
            }
        });

        $("#btnResDisallow").click(function() {
            reschkelem = $('input.group2:checked');
            $('.savebtn').css('display', 'none');
            $('button[name=saveresdisallow]').css('display', 'block');
            if (reschkelem.length) {
                var alertmessage = false;
                $.each($('input.group2:checked'), function(i) {
                    var chkey = $(this).attr('resdisalt');
                    if ($('input[type=hidden][resdisalt=' + chkey + ']').length) {
                        alertmessage = true;
                    }
                });
                if (alertmessage) {
                    if (!confirm('<?= lang('ha_alert_clean') ?>'))
                        return false;
                }


                $('input[name=uo_companies][value=none]').attr('checked', 'checked');
                $('#uo_companies_list').css('display', 'none').val([]);
                $('#uo_companies_list_chosen').css('display', 'none');

                $('input[name=hm_companies][value=none]').attr('checked', 'checked');
                $('#hm_companies_list').css('display', 'none').val([]);
                $('#hm_companies_list_chosen').css('display', 'none');

                $('input[name=ea][value=none]').attr('checked', 'checked');
                $('.countriesdiv').css('display', 'none');
                $('#ea_countries_list').css('display', 'none').val([]);
                $('#ea_countries_list_chosen').css('display', 'none');
                $('#ea_countries_companies_list option').remove();
                $('#ea_companies_list option').remove();
                $('input[name=individual]').removeAttr('checked');

                $.fancybox({
                    href: "#divForm",
                    minWidth: 800,
                    minHeight: 500
                });
                $('#tabs1').tabs();
            } else {
                alert("<?= lang('ha_choise_one') ?>");
            }
        });

        $('#res').on('click', '.fixresallow', function() {
            $('.savebtn').css('display', 'none');
            $('button[name=saveresallow]').css('display', 'block');

            reschkelem = $(this);
            var chkey = $(this).attr('resalt');
            var uoselected = $('input[type=hidden][resalt=' + chkey + ']').val();
            var hmselected = $('input[type=hidden][reshmalt=' + chkey + ']').val();
            var easelected = $('input[type=hidden][reseaalt=' + chkey + ']').val();
            var eacoselected = $('input[type=hidden][reseacoalt=' + chkey + ']').val();
            var individual = $('input[type=hidden][resindvalt=' + chkey + ']').val();

            $.fancybox({
                href: "#divForm",
                minWidth: 800,
                minHeight: 500
            });


            if (uoselected.length) {
                $('input[name=uo_companies][value=list]').attr('checked', 'checked');
                $('#uo_companies_list').css('display', 'block').chosen();
                $('#uo_companies_list').css('display', 'none').val(uoselected.split(','));
                $('#uo_companies_list').trigger("chosen:updated");
            }
            if (hmselected.length) {
                $('input[name=hm_companies][value=list]').attr('checked', 'checked');
                $('#hm_companies_list').css('display', 'block').chosen();
                $('#hm_companies_list').css('display', 'none').val(hmselected.split(','));
                $('#hm_companies_list').trigger("chosen:updated");
            }
            if (easelected.length) {

                $('.countriesdiv').css('display', 'block');
                $('input[name=ea][value=list]').attr('checked', 'checked');
                $('#ea_countries_list').css('display', 'block').chosen();
                $('#ea_countries_list').css('display', 'none').val(easelected.split(','));
                $('#ea_countries_list').trigger("chosen:updated");

                var chosencou = easelected.split(',');
                $.each(chosencou, function(i, valu) {
                    $('#ea_countries_company_list option[value=' + valu + ']').attr('disabled', true);
                });

                $('#ea_countries_company_list').css('display', 'block').chosen().css('display', 'none');
                $('#ea_countries_company_list').trigger("chosen:updated");

                $.post('<?= site_url('hotel_availability/ajax_get_ea_companies') ?>', {'comp_id': eacoselected}, function(ajaxdata) {
                    $('#ea_companies_list option').remove();
                    $.each(ajaxdata, function(id, value) {
                        if ($('#ea_companies_list option[value=' + value.id + ']').length == 0)
                            $('#ea_companies_list').append("<option value='" + value.id + "'>" + value.name + "</option>");
                    });
                }, 'json');
            }

            if (individual == 1)
                $('input[name=individual]').attr('checked', 'checked');
            else
                $('input[name=individual]').removeAttr('checked');


            $('#tabs1').tabs();

        });

        $('#res').on('click', '.fixresdisallow', function() {
            $('.savebtn').css('display', 'none');
            $('button[name=saveresdisallow]').css('display', 'block');
            reschkelem = $(this);
            var chkey = $(this).attr('resdisalt');
            var uoselected = $('input[type=hidden][resdisalt=' + chkey + ']').val();
            var hmselected = $('input[type=hidden][resdishmalt=' + chkey + ']').val();
            var easelected = $('input[type=hidden][resdiseaalt=' + chkey + ']').val();
            var eacoselected = $('input[type=hidden][resdiseacoalt=' + chkey + ']').val();
            var individual = $('input[type=hidden][resdisindvalt=' + chkey + ']').val();

            $.fancybox({
                href: "#divForm",
                minWidth: 800,
                minHeight: 500
            });

            if (uoselected.length && uoselected !== 'all') {
                $('input[name=uo_companies][value=list]').attr('checked', 'checked');
                $('#uo_companies_list').css('display', 'block').chosen();
                $('#uo_companies_list').css('display', 'none').val(uoselected.split(','));
                $('#uo_companies_list').trigger("chosen:updated");
            }
            if (hmselected.length && hmselected !== 'all') {
                $('input[name=hm_companies][value=list]').attr('checked', 'checked');
                $('#hm_companies_list').css('display', 'block').chosen();
                $('#hm_companies_list').css('display', 'none').val(hmselected.split(','));
                $('#hm_companies_list').trigger("chosen:updated");
            }
            if (easelected.length && easelected !== 'all') {

                $('.countriesdiv').css('display', 'block');
                $('input[name=ea][value=list]').attr('checked', 'checked');
                $('#ea_countries_list').css('display', 'block').chosen();
                $('#ea_countries_list').css('display', 'none').val(easelected.split(','));
                $('#ea_countries_list').trigger("chosen:updated");

                var chosencou = easelected.split(',');
                $.each(chosencou, function(i, valu) {
                    $('#ea_countries_company_list option[value=' + valu + ']').attr('disabled', true);
                });

                $('#ea_countries_company_list').css('display', 'block').chosen().css('display', 'none');
                $('#ea_countries_company_list').trigger("chosen:updated");

                $.post('<?= site_url('hotel_availability/ajax_get_ea_companies') ?>', {'comp_id': eacoselected}, function(ajaxdata) {
                    $('#ea_companies_list option').remove();
                    $.each(ajaxdata, function(id, value) {
                        if ($('#ea_companies_list option[value=' + value.id + ']').length == 0)
                            $('#ea_companies_list').append("<option value='" + value.id + "'>" + value.name + "</option>");
                    });
                }, 'json');
            }

            if (individual == 1)
                $('input[name=individual]').attr('checked', 'checked');
            else
                $('input[name=individual]').removeAttr('checked');


            $('#tabs1').tabs();

        });

        $('#divForm').on('click', 'button[name=saveresallow]', function() {
            $.fancybox.close();
            var uo_allowed = '';
            var hm_allowed = '';
            var ea_allowed = '';
            var ea_companies_allowed = '';
            var individual = 0;

            if ($('input[name=uo_companies]:checked').val() === 'list') {
                uo_allowed = $('#uo_companies_list').val();
            } else
                uo_allowed = "all";

            if ($('input[name=hm_companies]:checked').val() === 'list') {
                hm_allowed = $('#hm_companies_list').val();
            } else
                hm_allowed = "all";

            if ($('input[name=ea]:checked').val() === 'list') {
                ea_allowed = $('#ea_countries_list').val();
                //var eacompanies = $('#ea_companies_list').val() || [];
                var selectArr = [];

                $('#ea_companies_list option').each(function() {
                    selectArr.push($(this).val());
                });
                ea_companies_allowed = selectArr.join(',');
            } else {
                ea_allowed = "all";
                ea_companies_allowed = '';
            }




            if ($('input[type=checkbox][name=individual]:checked').length) {
                individual = 1;
            }


            if (typeof reschkelem == 'undefined')
                reschkelem = $('input.group2:checked');

            $.each(reschkelem, function(i) {
                var chkey = $(this).attr('alt');
                $(this).attr('allowchosen', '1');

                if ($('input[type=hidden][resalt=' + chkey + ']').length == 0) {
                    var parent_td = $(this).parent();
                    parent_td.append('<input type="button" resalt="' + chkey + '" class="fixresallow btn group2" value="متاح ل"  />');
                    parent_td.append('<input resalt="' + chkey + '" type="hidden" value="' + uo_allowed + '" name="res_uo_allow[' + chkey + ']" />');
                    parent_td.append('<input reshmalt="' + chkey + '" type="hidden" value="' + hm_allowed + '" name="res_hm_allow[' + chkey + ']" />');
                    parent_td.append('<input reseaalt="' + chkey + '" type="hidden" value="' + ea_allowed + '" name="res_ea_allow[' + chkey + ']" />');
                    parent_td.append('<input reseacoalt="' + chkey + '" type="hidden" value="' + ea_companies_allowed + '" name="res_eaco_allow[' + chkey + ']" />');
                    parent_td.append('<input resindvalt="' + chkey + '" type="hidden" value="' + individual + '" name="res_individual[' + chkey + ']" />');
                } else {
                    $('input[type=hidden][resalt=' + chkey + ']').val(uo_allowed);
                    $('input[type=hidden][reshmalt=' + chkey + ']').val(hm_allowed);
                    $('input[type=hidden][reseaalt=' + chkey + ']').val(ea_allowed);
                    $('input[type=hidden][reseacoalt=' + chkey + ']').val(ea_companies_allowed);
                    $('input[type=hidden][resindvalt=' + chkey + ']').val(individual);
                }
            });
            $('input.group2:checked').removeAttr('checked');

            return false;
        });

        $('#divForm').on('click', 'button[name=saveresdisallow]', function() {
            $.fancybox.close();
            var uo_disallowed = '';
            var hm_disallowed = '';
            var ea_disallowed = '';
            var ea_companies_disallowed = '';
            var disindividual = 0;

            if ($('input[name=uo_companies]:checked').val() === 'list') {
                uo_disallowed = $('#uo_companies_list').val();
            } else
                uo_disallowed = "all";

            if ($('input[name=hm_companies]:checked').val() === 'list') {
                hm_disallowed = $('#hm_companies_list').val();
            } else
                hm_disallowed = "all";

            if ($('input[name=ea]:checked').val() === 'list') {
                ea_disallowed = $('#ea_countries_list').val();
                //var eacompanies = $('#ea_companies_list').val() || [];
                var selectArr = [];

                $('#ea_companies_list option').each(function() {
                    selectArr.push($(this).val());
                });
                ea_companies_disallowed = selectArr.join(',');
            } else {
                ea_disallowed = "all";
                ea_companies_disallowed = '';
            }




            if ($('input[type=checkbox][name=individual]:checked').length) {
                disindividual = 1;
            }


            if (typeof reschkelem == 'undefined')
                reschkelem = $('input.group2:checked');

            $.each(reschkelem, function(i) {
                var chkey = $(this).attr('alt');
                $(this).attr('disallowchosen', '1');

                if ($('input[type=hidden][resdisalt=' + chkey + ']').length == 0) {
                    var parent_td = $(this).parent();
                    parent_td.append('<input type="button" resdisalt="' + chkey + '" class="fixresdisallow btn group2" value="غير متاح ل"  />');
                    parent_td.append('<input resdisalt="' + chkey + '" type="hidden" value="' + uo_disallowed + '" name="res_uo_disallow[' + chkey + ']" />');
                    parent_td.append('<input resdishmalt="' + chkey + '" type="hidden" value="' + hm_disallowed + '" name="res_hm_disallow[' + chkey + ']" />');
                    parent_td.append('<input resdiseaalt="' + chkey + '" type="hidden" value="' + ea_disallowed + '" name="res_ea_disallow[' + chkey + ']" />');
                    parent_td.append('<input resdiseacoalt="' + chkey + '" type="hidden" value="' + ea_companies_disallowed + '" name="res_eaco_disallow[' + chkey + ']" />');
                    parent_td.append('<input resdisindvalt="' + chkey + '" type="hidden" value="' + disindividual + '" name="res_disindividual[' + chkey + ']" />');
                } else {
                    $('input[type=hidden][resdisalt=' + chkey + ']').val(uo_disallowed);
                    $('input[type=hidden][resdishmalt=' + chkey + ']').val(hm_disallowed);
                    $('input[type=hidden][resdiseaalt=' + chkey + ']').val(ea_disallowed);
                    $('input[type=hidden][esdiseacoalt=' + chkey + ']').val(ea_companies_disallowed);
                    $('input[type=hidden][resdisindvalt=' + chkey + ']').val(disindividual);
                }
            });
            $('input.group2:checked').removeAttr('checked');

            return false;
        });

    });


</script>

<div class="wizerd-div"><a><?= lang('ha_addditional_options') ?></a> </div>

<div class="table-responsive">
    <table style="float: none;" cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= lang('ha_check') ?></th>
                <th><?= lang('ha_service_id') ?></th>
                <th><?= lang('ha_purchase_price_id') ?></th>
                <th><?= lang('ha_sale_price_id') ?></th>
                <th><?= lang('ha_notes') ?></th>
                <th>
                    <!--<?= lang('ha_operations') ?>-->
                    <a class="btn" href="javascript:void(0)" onclick="add_res('N' + res_counter++)"><?= lang('ha_add') ?></a>

                </th>
            </tr>
        </thead>
        <tbody id="res">
            <? if (ensure($exclusive)): ?>
                <? foreach ($exclusive as $option): ?>
                    <tr rel="<?= $option->erp_room_exclusive_service_id ?>">
                        <td>
                            <input name="res_check[<?= $option->erp_room_exclusive_service_id ?>]" type="checkbox" class="checkbox group2" value="1"  />
                            <?php
                            $resallowedcomp = get_res_uo_companies($option->erp_room_exclusive_service_id);
                            $reshmallowedcomp = get_res_hm_companies($option->erp_room_exclusive_service_id);
                            $reseaallowed = get_res_ea_countries($option->erp_room_exclusive_service_id);
                            $reseacoallowed = get_res_ea_companies($option->erp_room_exclusive_service_id);
                            ?>
                            <? if ($resallowedcomp || $reshmallowedcomp || $reseaallowed) : ?>
                                <input type="button" resalt="<?= $option->erp_room_exclusive_service_id ?>" class="fixresallow btn group2" value="متاح ل"  />
                                <input type="hidden" resalt="<?= $option->erp_room_exclusive_service_id ?>" name="res_uo_allow[<?= $option->erp_room_exclusive_service_id ?>]" value="<?= (is_array($resallowedcomp)) ? implode(',', $resallowedcomp) : '' ?>" />
                                <input type="hidden" reshmalt="<?= $option->erp_room_exclusive_service_id ?>" name="res_hm_allow[<?= $option->erp_room_exclusive_service_id ?>]" value="<?= (is_array($reshmallowedcomp)) ? implode(',', $reshmallowedcomp) : '' ?>" />
                                <input type="hidden" reseaalt="<?= $option->erp_room_exclusive_service_id ?>" name="res_ea_allow[<?= $option->erp_room_exclusive_service_id ?>]" value="<?= (is_array($reseaallowed)) ? implode(',', $reseaallowed) : '' ?>" />
                                <input type="hidden" reseacoalt="<?= $option->erp_room_exclusive_service_id ?>" name="res_eaco_allow[<?= $option->erp_room_exclusive_service_id ?>]" value="<?= (is_array($reseacoallowed)) ? implode(',', $reseacoallowed) : '' ?>" />
                                <input type="hidden" resindvalt="<?= $option->erp_room_exclusive_service_id ?>" name="res_individual[<?= $option->erp_room_exclusive_service_id ?>]" value="<?= $option->individual ?>" />
                            <? endif ?>
                            <?php
                            $resdisallowedcomp = get_res_uo_companies($option->erp_room_exclusive_service_id, '0');
                            $reshmdisallowedcomp = get_res_hm_companies($option->erp_room_exclusive_service_id, '0');
                            $reseadisallowed = get_res_ea_countries($option->erp_room_exclusive_service_id, '0');
                            $reseacodisallowed = get_res_ea_companies($option->erp_room_exclusive_service_id, '0');
                            ?>
                            <? if ($resdisallowedcomp || $reshmdisallowedcomp || $reseadisallowed) : ?>
                                <input type="button" resdisalt="<?= $option->erp_room_exclusive_service_id ?>" class="fixresdisallow btn group2" value="غير متاح ل"  />
                                <input type="hidden" resdisalt="<?= $option->erp_room_exclusive_service_id ?>" name="res_uo_disallow[<?= $option->erp_room_exclusive_service_id ?>]" value="<?= (is_array($resdisallowedcomp)) ? implode(',', $resdisallowedcomp) : '' ?>" />
                                <input type="hidden" resdishmalt="<?= $option->erp_room_exclusive_service_id ?>" name="res_hm_disallow[<?= $option->erp_room_exclusive_service_id ?>]" value="<?= (is_array($reshmdisallowedcomp)) ? implode(',', $reshmdisallowedcomp) : '' ?>" />
                                <input type="hidden" resdiseaalt="<?= $option->erp_room_exclusive_service_id ?>" name="res_ea_disallow[<?= $option->erp_room_exclusive_service_id ?>]" value="<?= (is_array($reseadisallowed)) ? implode(',', $reseadisallowed) : '' ?>" />
                                <input type="hidden" resdiseacoalt="<?= $option->erp_room_exclusive_service_id ?>" name="res_eaco_disallow[<?= $option->erp_room_exclusive_service_id ?>]" value="<?= (is_array($reseacodisallowed)) ? implode(',', $reseacodisallowed) : '' ?>" />
                                <input type="hidden" resdisindvalt="<?= $option->erp_room_exclusive_service_id ?>" name="res_disindividual[<?= $option->erp_room_exclusive_service_id ?>]" value="<?= $option->individual ?>" />
                            <? endif ?>    

                        </td>
                        <td>
                            <?= form_dropdown('res_service_id[' . $option->erp_room_exclusive_service_id . ']', $additions, set_value('res_service_id[' . $option->erp_room_exclusive_service_id . ']', $option->erp_room_service_id), ' class="validate[required] input-huge"') ?>
                        </td>
                        <td>
                            <?= form_input('res_purchase_price[' . $option->erp_room_exclusive_service_id . ']', set_value('res_purchase_price[' . $option->erp_room_exclusive_service_id . ']', $option->purchase_price), 'class="input-huge"') ?>
                        </td>
                        <td>
                            <?= form_input('res_sale_price[' . $option->erp_room_exclusive_service_id . ']', set_value('res_sale_price[' . $option->erp_room_exclusive_service_id . ']', $option->sale_price), 'class="validate[required] input-huge"') ?>
                        </td>
                        <td>
                            <?= form_input('res_notes[' . $option->erp_room_exclusive_service_id . ']', set_value('res_notes[' . $option->erp_room_exclusive_service_id . ']', $option->notes), 'class=" input-huge"') ?>
                        </td>
                        <td class="TAC"><a href="javascript:void(0)" onclick="delete_res('<?= $option->erp_room_exclusive_service_id ?>', true)"><span class="icon-trash"></span></a></td>
                    </tr>
                <? endforeach ?>
            <? endif ?>
        </tbody>
        <tfoot>
            <tr>
                <td colspan="10">
                    <input type="button" class="btn group2" value="متاح ل" id="btnResAllow" />
                    <input type="button" class="btn group2" value="غير متاح ل" id="btnResDisallow" />
                </td>
            </tr>
        </tfoot>
    </table>
</div>



<div class="toolbar TAC">
    <input type="submit" class="btn" value="<?= lang('ha_save') ?>" style="margin:10px;padding: 5px;height: auto">
    <?php if(isset($main->erp_hotels_availability_master_id)) {?>
    <a href="<?= site_url('hotel_availability/pdf_export/' . $main->erp_hotels_availability_master_id) ?>" target="_blank" class="icon-book"></a>
    <?php }?>              
</div>

<script>
    function add_res(id) {
        if (!$('form').validationEngine('validate'))
            return;
        var new_row = function() {/*
         <tr rel="{$id}">
         <td>
         <input name="res_check[{$id}]" alt="{$id}"  allowchosen='0' disallowchosen='0'  type="checkbox" class="checkbox group2" value="1"  />
         </td>
         <td>
<?= form_dropdown('res_service_id[{$id}]', $additions, false, ' class="validate[required] input-huge"') ?>
         </td>
         <td>
<?= form_input('res_purchase_price[{$id}]', false, 'class="input-huge"') ?>
         </td>
         <td>
<?= form_input('res_sale_price[{$id}]', false, 'class="validate[required] input-huge"') ?>
         </td>
         <td>
<?= form_input('res_notes[{$id}]', false, 'class=" input-huge"') ?>
         </td>
         <td class="TAC"><a href="javascript:void(0)" onclick="delete_res('{$id}')"><span class="icon-trash"></span></a></td>
         </tr>
         
         */
        }.toString().replace(/{\$id}/g, id).slice(14, -3)
        $('#res').append(new_row);
    }
    function delete_res(id, database) {
        if (typeof database == 'undefined')
        {
            $('#res').find('tr[rel="' + id + '"]').remove();
        }
        else
        {
            $('#res').find('tr[rel=' + id + ']').remove();
            var hidden_input = '<input type="hidden" name="res_remove[]" value="' + id + '" />';
            $('#res').append(hidden_input);
        }

    }
</script>