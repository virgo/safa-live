<div class="embdder" style="display:none">
<div class="widget">
    <script>sub_har_counter_<?= $har_counter ?> = 0</script>
    <div class="widget-header">
        <div class="widget-header-icon Fright"> <span class="icos-pencil2"></span> </div>
        <div class="widget-header-title Fright"> <?= lang('ha_room_details') ?></div>
        <a class="btn Fleft" href="javascript:void(0)" onclick="add_sub_har('N' + sub_har_counter_<?= $har_counter ?>++, '<?= $har_counter ?>')">
            <?= lang('ha_add') ?>
        </a>
    </div>

    <div class="widget-container">
        <div class="table-container">
            <table cellpadding="0" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th><?= lang('ha_floor') ?></th>
                        <th><?= lang('ha_section') ?></th>
                        <th><?= lang('ha_res_num_of_rooms') ?></th>
                        <th><?= lang('ha_room_nums') ?></th>
                        <th><?= lang('global_operations') ?></th>
                    </tr>
                </thead>
                <tbody class="sub_har_<?= $har_counter ?>" id='sub_har_<?= $har_counter ?>'>
                    
                </tbody>
                <tfoot style="border-top: 1px solid #ccc">
                    <tr>
                        <td colspan="5">
                            <a href="javascript:;" onclick="sub_rooms_close('<?= $har_counter ?>')" class="btn"><?= lang('global_submit') ?></a> 
                        </td>
                    </tr>
                </tfoot>
            </table>
        </div>

    </div>
</div>
</div>

<script>

    function add_sub_har( id , har_counter )
    {
//    if( ! $('form').validationEngine('validate'))
//        return;
        $('#sub_har_' + har_counter).center();
        
        var new_row = [
           "<tr rel=\"" + id + "\">",
                "<td>",
                " <select name=\"sub_har_floor[" + id + "]\" class=\"validate[required] input-huge\">",
                <? foreach ($har_floors as $key => $value): ?>
                "  <option value=\"<?= $key ?>\"><?= $value ?></option>",
                <? endforeach ?>
                "</td>",
                '<td><?= form_input("sub_har_section[' + id + ']", FALSE, 'class="input-huge small"') ?></td>',
                '<td><?= form_input("sub_har_res_num_of_rooms[' + id + ']", FALSE, 'class="input-huge small"') ?></td>',
                '<td><?= form_input("sub_har_room_nums[' + id + ']", FALSE, 'class="input-huge small"') ?></td>',
                '<td class="TAC"><a href="javascript:void(0)" onclick="delete_sub_har(\'" + id + "\')"><span class="icon-trash"></span></a>',
                '<div class="har_sub_' + id + '"></div></td>',
          "</tr>"
        ].join("\n");
                $('#sub_har_' + har_counter).append(new_row);
    }
    function delete_sub_har(id, database) {
        if (typeof database == 'undefined')
        {
            $('.sub_har_<?= $har_counter ?>').find('tr[rel="' + id + '"]').remove();
        }
        else
        {
            $('.sub_har_<?= $har_counter ?>').find('tr[rel=' + id + ']').remove();
            var hidden_input = '<input type="hidden" name="har_remove[]" value="' + id + '" />';
            $('#sub_har_<?= $har_counter ?>').append(hidden_input);
        }

    }
</script>
<div class="my_shadow"></div>