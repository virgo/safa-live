<div id="modal_N0">
    <div class="widget-container">
        <div class="table-container">
            <table cellpadding="0" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th><?= lang('ha_floor') ?></th>
                        <th><?= lang('ha_section') ?></th>
                        <th><?= lang('ha_res_num_of_rooms') ?></th>
                        <th><?= lang('ha_room_nums') ?></th>
                        <th><?= lang('global_operations') ?></th>
                    </tr>
                    <tr>
                        <td><?= form_dropdown('detail_floor[]', $har_floors, set_value('detail_floor[]'), 'class="validate[required]"') ?></td>
                        <td><input type="text" /></td>
                        <td><input type="text" /></td>
                        <td><input type="text" /></td>
                        <td><a href="javascript:void(0)"><span class="icon-trash"></span></a></td>
                    </tr>
            </table>
        </div>
    </div>

    <a href="javascript:;" onclick="submit_form()">Close</a>
    <script>
        function submit_form() {
                if($('form').validationEngine('validate'))
            $.fancybox.close();
        }

    </script>
</div>