<script>had_counter = 0</script>

<div class="wizerd-div"><a><?= lang('ha_hotel_availability') ?></a></div>
<script type="text/javascript">
    var chkelem;
    $(document).ready(function() {

        $("#btnAllow").click(function() {
            chkelem = $('input.group1:checked');
            $('.savebtn').css('display', 'none');
            $('button[name=saveallow]').css('display', 'block');

            if (chkelem.length) {
                var alertmessage = false;
                $.each($('input.group1:checked'), function(i) {
                    var chkey = $(this).attr('alt');
                    if ($('input[type=hidden][alt=' + chkey + ']').length) {
                        alertmessage = true;
                    }
                });
                if (alertmessage) {
                    if (!confirm('<?= lang('ha_alert_clean') ?>'))
                        return false;
                }

                $('input[name=uo_companies][value=none]').attr('checked', 'checked');
                $('#uo_companies_list').css('display', 'none').val([]);
                $('#uo_companies_list_chosen').css('display', 'none');

                $('input[name=hm_companies][value=none]').attr('checked', 'checked');
                $('#hm_companies_list').css('display', 'none').val([]);
                $('#hm_companies_list_chosen').css('display', 'none');

                $('input[name=ea][value=none]').attr('checked', 'checked');
                $('.countriesdiv').css('display', 'none');
                $('#ea_countries_list').css('display', 'none').val([]);
                $('#ea_countries_list_chosen').css('display', 'none');
                $('#ea_countries_companies_list option').remove();
                $('#ea_companies_list option').remove();
                $('input[name=individual]').removeAttr('checked');

                $.fancybox({
                    href: "#divForm",
                    minWidth: 800,
                    minHeight: 500
                });
                $('#tabs1').tabs();
            } else {
                alert("<?= lang('ha_choise_one') ?>");
            }
        });

        $("#btnDisallow").click(function() {
            chkelem = $('input.group1:checked');
            $('.savebtn').css('display', 'none');
            $('button[name=savedisallow]').css('display', 'block');
            if (chkelem.length) {
                var alertmessage = false;
                $.each($('input.group1:checked'), function(i) {
                    var chkey = $(this).attr('disalt');
                    if ($('input[type=hidden][disalt=' + chkey + ']').length) {
                        alertmessage = true;
                    }
                });
                if (alertmessage) {
                    if (!confirm('<?= lang('ha_alert_clean') ?>'))
                        return false;
                }

                $('input[name=uo_companies][value=none]').attr('checked', 'checked');
                $('#uo_companies_list').css('display', 'none').val([]);
                $('#uo_companies_list_chosen').css('display', 'none');

                $('input[name=hm_companies][value=none]').attr('checked', 'checked');
                $('#hm_companies_list').css('display', 'none').val([]);
                $('#hm_companies_list_chosen').css('display', 'none');

                $('input[name=ea][value=none]').attr('checked', 'checked');
                $('.countriesdiv').css('display', 'none');
                $('#ea_countries_list').css('display', 'none').val([]);
                $('#ea_countries_list_chosen').css('display', 'none');
                $('#ea_countries_companies_list option').remove();
                $('#ea_companies_list option').remove();
                $('input[name=individual]').removeAttr('checked');

                $.fancybox({
                    href: "#divForm",
                    minWidth: 800,
                    minHeight: 500
                });
                $('#tabs1').tabs();
            } else {
                alert("<?= lang('ha_choise_one') ?>");
            }
        });

        $('#had').on('click', '.fixallow', function() {
            $('.savebtn').css('display', 'none');
            $('button[name=saveallow]').css('display', 'block');

            chkelem = $(this);
            var chkey = $(this).attr('alt');
            var uoselected = $('input[type=hidden][alt=' + chkey + ']').val();
            var hmselected = $('input[type=hidden][hmalt=' + chkey + ']').val();
            var easelected = $('input[type=hidden][eaalt=' + chkey + ']').val();
            var eacoselected = $('input[type=hidden][eacoalt=' + chkey + ']').val();
            var individual = $('input[type=hidden][indvalt=' + chkey + ']').val();

            $.fancybox({
                href: "#divForm",
                minWidth: 800,
                minHeight: 500
            });

            if (uoselected != "all" && uoselected != "none") {
                $('input[name=uo_companies][value=list]').attr('checked', 'checked');
                $('#uo_companies_list').css('display', 'block').chosen();
                $('#uo_companies_list').css('display', 'none').val(uoselected.split(','));
                $('#uo_companies_list').trigger("chosen:updated");
            } else {
                $('input[name=uo_companies][value='+ uoselected+']').attr('checked', 'checked');
            }
            
            if (hmselected != "all" && hmselected != "none") {
                $('input[name=hm_companies][value=list]').attr('checked', 'checked');
                $('#hm_companies_list').css('display', 'block').chosen();
                $('#hm_companies_list').css('display', 'none').val(hmselected.split(','));
                $('#hm_companies_list').trigger("chosen:updated");
            } else {
                $('input[name=hm_companies][value='+hmselected+']').attr('checked', 'checked');
            }
            if (easelected != "all" && easelected != "none") {
                $('.countriesdiv').css('display', 'block');
                $('input[name=ea][value=list]').attr('checked', 'checked');
                $('#ea_countries_list').css('display', 'block').chosen();
                $('#ea_countries_list').val(easelected.split(','));
                $('#ea_countries_list').trigger("chosen:updated");
                $('#ea_countries_list_chosen').css("width", '100%');

                var chosencou = easelected.split(',');
                $.each(chosencou, function(i, valu) {
                    $('#ea_countries_company_list option[value=' + valu + ']').attr('disabled', true);
                });

                $('#ea_countries_company_list').css('display', 'block').chosen().css('display', 'none');
                $('#ea_countries_company_list').trigger("chosen:updated");
                $('#ea_countries_company_list_chosen').css("width", '100%');

                $.post('<?= site_url('hotel_availability/ajax_get_ea_companies') ?>', {'comp_id': eacoselected}, function(ajaxdata) {
                    $('#ea_companies_list option').remove();
                    $.each(ajaxdata, function(id, value) {
                        if ($('#ea_companies_list option[value=' + value.id + ']').length == 0)
                            $('#ea_companies_list').append("<option value='" + value.id + "'>" + value.name + "</option>");
                    });
                }, 'json');
            } else {
                $('input[name=ea][value='+easelected+']').attr('checked', 'checked');
            }

            if (individual == 1)
                $('input[name=individual]').attr('checked', 'checked');
            else
                $('input[name=individual]').removeAttr('checked');


            $('#tabs1').tabs();

        });

        $('#had').on('click', '.fixdisallow', function() {
            $('.savebtn').css('display', 'none');
            $('button[name=savedisallow]').css('display', 'block');
            chkelem = $(this);
            var chkey = $(this).attr('disalt');
            var uoselected = $('input[type=hidden][disalt=' + chkey + ']').val();
            var hmselected = $('input[type=hidden][dishmalt=' + chkey + ']').val();
            var easelected = $('input[type=hidden][diseaalt=' + chkey + ']').val();
            var eacoselected = $('input[type=hidden][diseacoalt=' + chkey + ']').val();
            var individual = $('input[type=hidden][disindvalt=' + chkey + ']').val();

            $.fancybox({
                href: "#divForm",
                minWidth: 800,
                minHeight: 500
            });


            if (uoselected != "all" && uoselected != "none") {
                $('input[name=uo_companies][value=list]').attr('checked', 'checked');
                $('#uo_companies_list').css('display', 'block').chosen();
                $('#uo_companies_list').css('display', 'none').val(uoselected.split(','));
                $('#uo_companies_list').trigger("chosen:updated");
            } else {
                $('input[name=uo_companies][value='+uoselected+']').attr('checked', 'checked');
            }
            if (hmselected != "all" && hmselected != "none") {
                $('input[name=hm_companies][value=list]').attr('checked', 'checked');
                $('#hm_companies_list').css('display', 'block').chosen();
                $('#hm_companies_list').css('display', 'none').val(hmselected.split(','));
                $('#hm_companies_list').trigger("chosen:updated");
            } else {
                $('input[name=hm_companies][value='+hmselected+']').attr('checked', 'checked');
            }
            if (easelected != "all" && easelected != "none") {
                $('.countriesdiv').css('display', 'block');
                $('input[name=ea][value=list]').attr('checked', 'checked');
                $('#ea_countries_list').css('display', 'block').chosen();
                $('#ea_countries_list').css('display', 'none').val(easelected.split(','));
                $('#ea_countries_list').trigger("chosen:updated");

                var chosencou = easelected.split(',');
                $.each(chosencou, function(i, valu) {
                    $('#ea_countries_company_list option[value=' + valu + ']').attr('disabled', true);
                });

                $('#ea_countries_company_list').css('display', 'block').chosen().css('display', 'none');
                $('#ea_countries_company_list').trigger("chosen:updated");

                $.post('<?= site_url('hotel_availability/ajax_get_ea_companies') ?>', {'comp_id': eacoselected}, function(ajaxdata) {
                    $('#ea_companies_list option').remove();
                    $.each(ajaxdata, function(id, value) {
                        if ($('#ea_companies_list option[value=' + value.id + ']').length == 0)
                            $('#ea_companies_list').append("<option value='" + value.id + "' >" + value.name + "</option>");
                    });
                }, 'json');
            } else {
                $('input[name=ea][value='+easelected+']').attr('checked', 'checked');
            }

            if (individual == 1)
                $('input[name=individual]').attr('checked', 'checked');
            else
                $('input[name=individual]').removeAttr('checked');


            $('#tabs1').tabs();

        });
    });
</script>
<script>
    $(function() {
        $("#accordion").accordion({heightStyle: "content"});
    });
</script>


<div class="table-container">
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= lang('ha_check') ?></th>
                <th><?= lang('ha_period') ?></th>
                <th><?= lang('ha_housing_type') ?></th>
                <th><?= lang('ha_room_type') ?></th>
                <th><?= lang('ha_etlala') ?></th>
                <th><?= lang('ha_sale_price_id') ?></th>
                <th style="min-width: 65px;"><?= lang('ha_date_from') ?></th>
                <th><?= lang('ha_additions') ?></th>
                <th><?= lang('ha_available') ?></th>
                <th><?= lang('ha_notes') ?></th>
                <th>
                    <!--<?= lang('ha_operations') ?>-->
                    <a class="btn" href="javascript:void(0)" onclick="add_had('N' + had_counter++)"><?= lang('ha_add') ?></a>
                </th>
            </tr>
        </thead>
        <tbody id="had">
            <? if (ensure($details)): ?>
                <? foreach ($details as $detail): ?>
                    <tr rel="<?= $detail->erp_hotels_availability_detail_id ?>">
                        <td class="TAC"> <i title="متاح لـ" class=' icon-ok icon-1x green'></i>
                   
                            <input type="checkbox" class="checkbox group1" allowchosen="1" disallowchosen="1" name="had_check[<?= $detail->erp_hotels_availability_detail_id ?>]" alt="<?= $detail->erp_hotels_availability_detail_id ?>" value="1" />
                            <i title="غير متاح لـ" class=' icon-lock icon-1x '></i>
  <?php
                            $allowedcomp = get_uo_companies($detail->erp_hotels_availability_detail_id);
                            $hmallowedcomp = get_hm_companies($detail->erp_hotels_availability_detail_id);
                            $eaallowed = get_ea_countries($detail->erp_hotels_availability_detail_id);
                            $eacoallowed = get_ea_companies($detail->erp_hotels_availability_detail_id);
                            ?>
                            <? if ($allowedcomp || $hmallowedcomp || $eaallowed) : ?>
                                <input type="button" alt="<?= $detail->erp_hotels_availability_detail_id ?>" class="fixallow btn group2" value="<?= lang('ha_allow_to') ?>"  />
                                <input type="hidden" alt="<?= $detail->erp_hotels_availability_detail_id ?>" name="had_uo_allow[<?= $detail->erp_hotels_availability_detail_id ?>]" value="<?= (is_array($allowedcomp)) ? implode(',', $allowedcomp) : '' ?>" />
                                <input type="hidden" hmalt="<?= $detail->erp_hotels_availability_detail_id ?>" name="had_hm_allow[<?= $detail->erp_hotels_availability_detail_id ?>]" value="<?= (is_array($hmallowedcomp)) ? implode(',', $hmallowedcomp) : '' ?>" />
                                <input type="hidden" eaalt="<?= $detail->erp_hotels_availability_detail_id ?>" name="had_ea_allow[<?= $detail->erp_hotels_availability_detail_id ?>]" value="<?= (is_array($eaallowed)) ? implode(',', $eaallowed) : '' ?>" />
                                <input type="hidden" eacoalt="<?= $detail->erp_hotels_availability_detail_id ?>" name="had_eaco_allow[<?= $detail->erp_hotels_availability_detail_id ?>]" value="<?= (is_array($eacoallowed)) ? implode(',', $eacoallowed) : '' ?>" />
                                <input type="hidden" indvalt="<?= $detail->erp_hotels_availability_detail_id ?>" name="had_individual[<?= $detail->erp_hotels_availability_detail_id ?>]" value="<?= $detail->individual ?>" />
                            <? endif ?>
                            <?php
                            $disallowedcomp = get_uo_companies($detail->erp_hotels_availability_detail_id, '0');
                            $hmdisallowedcomp = get_hm_companies($detail->erp_hotels_availability_detail_id, '0');
                            $eadisallowed = get_ea_countries($detail->erp_hotels_availability_detail_id, '0');
                            $eacodisallowed = get_ea_companies($detail->erp_hotels_availability_detail_id, '0');
                            ?>
                            <? if ($disallowedcomp || $hmdisallowedcomp || $eadisallowed) : ?>
                                <input type="button" disalt="<?= $detail->erp_hotels_availability_detail_id ?>" class="fixdisallow btn group2" value="<?= lang('ha_disallow_to') ?>"  />
                                <input type="hidden" disalt="<?= $detail->erp_hotels_availability_detail_id ?>" name="had_uo_disallow[<?= $detail->erp_hotels_availability_detail_id ?>]" value="<?= (is_array($disallowedcomp)) ? implode(',', $disallowedcomp) : ''
                                ?>" />
                                <input type="hidden" dishmalt="<?= $detail->erp_hotels_availability_detail_id ?>" name="had_hm_disallow[<?= $detail->erp_hotels_availability_detail_id ?>]" value="<?= (is_array($hmdisallowedcomp)) ? implode(',', $hmdisallowedcomp) : ''
                                ?>" />
                                <input type="hidden" diseaalt="<?= $detail->erp_hotels_availability_detail_id ?>" name="had_ea_disallow[<?= $detail->erp_hotels_availability_detail_id ?>]" value="<?= (is_array($eadisallowed)) ? implode(',', $eadisallowed) : ''
                                ?>" />
                                <input type="hidden" diseacoalt="<?= $detail->erp_hotels_availability_detail_id ?>" name="had_eaco_disallow[<?= $detail->erp_hotels_availability_detail_id ?>]" value="<?= (is_array($eacodisallowed)) ? implode(',', $eacodisallowed) : ''
                                ?>" />
                                <input type="hidden" disindvalt="<?= $detail->erp_hotels_availability_detail_id ?>" name="had_disindividual[<?= $detail->erp_hotels_availability_detail_id ?>]" value="<?= $detail->individual ?>" />
                            <? endif ?>    

                        </td>
                        <td style="min-width: 110px;">
                            <?=
                            form_dropdown('had_period_id[' . $detail->erp_hotels_availability_detail_id . ']', $had_periods, set_value('had_period_id[' . $detail->erp_hotels_availability_detail_id . ']', $detail->erp_period_id), ' class="validate[required] input-huge"')
                            ?>
                        </td>
                        <td style="min-width: 65px;">
                            <?=
                            form_dropdown('had_housing_type_id[' . $detail->erp_hotels_availability_detail_id . ']', $had_housing, set_value('had_housing_type_id[' . $detail->erp_hotels_availability_detail_id . ']', $detail->erp_housingtype_id), ' class="validate[required] input-huge"')
                            ?>
                        </td>
                        <td style="min-width: 65px;">
                            <?=
                            form_dropdown('had_room_type_id[' . $detail->erp_hotels_availability_detail_id . ']', $had_room_types, set_value('had_room_type_id[' . $detail->erp_hotels_availability_detail_id . ']', $detail->erp_hotelroomsize_id), ' class="input-huge"')
                            ?>
                        </td>
                        <td style="min-width: 70px;">
<!--                            <input name="had_etlal[<?= $detail->erp_hotels_availability_detail_id ?>]" type="checkbox" value="1" <? if ($detail->etlal): ?>checked="checked"<? endif ?> />-->
							<?=
                            form_dropdown('had_etlal[' . $detail->erp_hotels_availability_detail_id . ']', $erp_hotels_availability_views, set_value('had_etlal[' . $detail->erp_hotels_availability_detail_id . ']', $detail->erp_hotels_availability_view_id), ' class="input-huge"')
                            ?>
                            
                        </td>
                        <td>
                            <?=
                            form_input('had_sale_price[' . $detail->erp_hotels_availability_detail_id . ']', set_value('had_sale_price[' . $detail->erp_hotels_availability_detail_id . ']', $detail->sale_price), 'class="validate[required] input-huge"')
                            ?>
                        </td>
                        <td style="min-width: 60px;">
                            <style>
                                label {
                                    display: block;
                                    font-size: 12px;
                                    margin: 1px 0;
                                }  
                            </style>

                            <div class="span3">
                                <label>من</label> 
                            </div>
                            <div class="span9">
                                <?=
                                form_input('had_start_date[' . $detail->erp_hotels_availability_detail_id . ']', set_value('had_start_date[' . $detail->erp_hotels_availability_detail_id . ']', $detail->start_date), ' class="datepicker input-huge sale_from"')
                                ?>

                            </div>
                            <div class="span3">
                                <label><?= lang('ha_date_to') ?> </label>  
                            </div>
                            <div class="span9">
                                <?=
                                form_input('had_end_date[' . $detail->erp_hotels_availability_detail_id . ']', set_value('had_end_date[' . $detail->erp_hotels_availability_detail_id . ']', $detail->end_date), ' class="datepicker input-huge sale_to"')
                                ?>

                            </div>

                        </td>
                        <td style="min-width: 85px;">
                            <?=
                            form_dropdown('had_room_service_id[' . $detail->erp_hotels_availability_detail_id . ']', $additions, set_value('had_room_service_id[' . $detail->erp_hotels_availability_detail_id . ']', $detail->erp_room_service_id), 'class="chosen-select chosen-rtl input-full" tabindex="4" data-placeholder="' . lang('global_all') . '"')
                            ?>
                        </td>
                        <td>
                            <input name="had_offer_status[<?= $detail->erp_hotels_availability_detail_id ?>]" type="checkbox" value="1" <? if ($detail->offer_status): ?>checked="checked"<? endif ?> />
                        </td>
                        <td>
                            <input name="had_notes[<?= $detail->erp_hotels_availability_detail_id ?>]" type="text" value="<?= set_value("notes[" . $detail->erp_hotels_availability_detail_id . "]", $detail->notes)
                            ?>" />
                        </td>
                        <td class="TAC"><a href="javascript:void(0)" onclick="delete_had('<?= $detail->erp_hotels_availability_detail_id ?>', true)"><span class="icon-trash"></span></a></td>
                    </tr>
                <? endforeach ?>
            <? endif ?> 
        </tbody>
        <tfoot>
            <tr class="TAC">
                <td colspan="12" class="TAC ">
                    <div class="span6 TAC">
                    
             
                        <input title="متاح لـ" type="button" class="avilable span3" value="<?= lang('ha_allow_to') ?>" id="btnAllow" />
 
                        <input title="غير متاح لـ" type="button" class="un_avilable span3" value="<?= lang('ha_disallow_to') ?>" id="btnDisallow" />
                   </div>
                </td>
            </tr>
        </tfoot>
    </table>
</div>
<?php
$companies_uo = $uo_companies;
if (session("uo_id")) {
    unset($companies_uo[session("uo_id")]);
} elseif (session("ea_id")) {
    $ea_companyid = session("ea_id");
}
?>

<div id="divForm" style="display: none;">
    <style>
        /*        #tabs .tabs-spacer { float: right; }
                .ui-tabs .ui-tabs-nav li { float: right; }
                .ui-widget-content {border: none;}
                .ui-widget {font-family:'GE-SS-med';background: url("../img/bg.jpg") repeat scroll 0 0 #FFF5D6;}
                .fancybox-outer {background: url("../img/bg.jpg") repeat scroll 0 0 #FFF5D6;}
                .ui-state-active, .ui-widget-content .ui-state-active, .ui-widget-header .ui-state-active {
                    background: url("../img/bg.jpg") repeat scroll 0 0 #FFF5D6;
                }
                .chosen-container .chosen-results {max-height: 75px;}*/

        .ui-accordion-content {
            height: auto !important;
        }
        .borderd{

            border: 1px solid #DEDEDE;
            border-radius: 4px;

            overflow: hidden;
            padding: 5% 2%;
            width: auto;
            margin-top: -22px;
        }
        .modal-body .wizerd-div {
            margin: 10px 0 21px;
            border-bottom: none!important;
        }
        .modal-body .span6{width: 48%;margin: 1%;}
        .modal-body {max-height:650px !important;
        }
        .chosen-results {max-height: 120px !important;}
        .fancybox-opened {height: 650px !important;}
        
        .ui-accordion .ui-accordion-content {
            overflow: visible;
        }
    </style>
    <div id="" class="modal-body">
        <div class="span12" id="accordion">
            <h3><?= lang('ha_uo') ?></h3>
            <div ><div id="" class="span12">
                    <div class="">
                        <div class="row-form">
                            <?= form_radio('uo_companies', 'none', set_radio('uo_companies', 'none', TRUE))
                            ?><?= lang('ha_none') ?>
                        </div>
                        <div class="row-form">
                            <?= form_radio('uo_companies', 'all', set_radio('uo_companies', 'all'))
                            ?><?= lang('ha_uo_all') ?>
                        </div>
                        <div class="row-form">
                            <?= form_radio('uo_companies', 'list', set_radio('uo_companies', 'list')) ?><?= lang('ha_uo_list') ?><br />
                            <?= form_dropdown('uo_companies_list[]', $companies_uo, set_value('uo_companies_list', ''), 'class="chosen-rtl span10" multiple tabindex="4" style="" id="uo_companies_list" data-placeholder=" "')
                            ?>
                        </div>
                    </div>
                </div>
            <div style='clear:both;'></div>
            </div>
            <h3><?= lang('ha_ea') ?></h3>
            <div ><div id="" class="span12"> 
                    <div class="span1">
                        <?= form_radio('ea', 'none', set_radio('ea', 'none', TRUE)) ?><?= lang('ha_none') ?>
                    </div>
                    <div class="span2">
                        <?= form_radio('ea', 'all', set_radio('ea', 'all')) ?><?= lang('ha_ea_all') ?>
                    </div>
                    <div class="span2">
                        <?= form_radio('ea', 'list', set_radio('ea', 'list')) ?><?= lang('ha_ea_list') ?><br />

                    </div>
                    <div class='span7 countriesdiv' style="display: none;">
                        <div class='row'>
                            <?= lang('ha_ea_allcountry') ?>
                            <?= form_dropdown('ea_countries_list[]', $ea_countries, set_value('ea_countries_list', ''), 'class="chosen-rtl span10" multiple tabindex="4" id="ea_countries_list" data-placeholder=" " ') ?><br />

                        </div>
                        <div class='row'>
                            <?= lang('ha_ea_some_companies') ?>
                            <?= form_dropdown('ea_countries_company_list[]', $countries, set_value('ea_countries_company_list', ''), 'class="chosen-rtl span10" tabindex="4" id="ea_countries_company_list" data-placeholder=" " ') ?><br /><br />
                            <?= form_dropdown('ea_countries_companies_list[]', array(), set_value('ea_countries_companies_list', ''), 'class="chosen-rtl span4" style="height:150px;width:40% !important;" tabindex="4" multiple id="ea_countries_companies_list" data-placeholder=" " ') ?>

                            <span class="span2"><button name='add' > >> </button><button name='remove' > X </button></span>
                            <?= form_dropdown('ea_companies_list[]', array(), set_value('ea_companies_list', ''), 'class="chosen-rtl span4" style="height:150px;width:40% !important;" tabindex="6" multiple id="ea_companies_list" data-placeholder=" " ')
                            ?>

                        </div>
                    </div>
                </div>
            <div style='clear:both;'></div>
            </div>
            <h3><?= lang('ha_hm') ?></h3>
            <div ><div id=""  class="span12" >
                    <div class="row-fluid">
                        <?= form_radio('hm_companies', 'none', set_radio('hm_companies', 'none', TRUE)) ?><?= lang('ha_none') ?>
                    </div>

                    <div class="row-fluid">
                        <?= form_radio('hm_companies', 'all', set_radio('hm_companies', 'all', TRUE)) ?><?= lang('ha_hm_all') ?>
                    </div>
                    <div class="row-fluid">
                        <?= form_radio('hm_companies', 'list', set_radio('hm_companies', 'list')) ?><?= lang('ha_hm_list') ?><br />
                        <?= form_dropdown('hm_companies_list[]', $hm_companies, set_value('hm_companies_list', ''), 'class="chosen-rtl span10" multiple tabindex="4" style="" id="hm_companies_list" data-placeholder=" "') ?>
                    </div>
                </div>
            <div style='clear:both;'></div>
            </div>
            <h3><?= lang('ha_vi') ?></h3>
            <div ><div id="" class="span12">
                    <div class="">
                        <?= form_checkbox('individual', '1', set_checkbox('individual', '1')) ?><?= lang('ha_individual') ?>
                    </div>
                </div>
            <div style='clear:both;'></div>
            </div></div>
        <div class="span12">
            <button name="saveallow" class="btn btn-primary savebtn" ><?= lang('ha_save') ?></button>
            <button name="savedisallow" class="btn btn-primary savebtn " ><?= lang('ha_save') ?></button>
            <button name="saveresallow" class="btn btn-primary savebtn" ><?= lang('ha_save') ?></button>
            <button name="saveresdisallow" class="btn btn-primary savebtn" ><?= lang('ha_save') ?></button>
        </div>   
    </div>

</div> 
<script type="text/javascript">
    $(document).ready(function() {
        $('#divForm').on('click', 'button[name=saveallow]', function() {
            $.fancybox.close();
            var uo_allowed = 'none';
            var hm_allowed = 'none';
            var ea_allowed = 'none';
            var ea_companies_allowed = 'none';
            var individual = 0;

            if ($('input[name=uo_companies]:checked').val() === 'list') {
                uo_allowed = $('#uo_companies_list').val();
            } else if ($('input[name=uo_companies]:checked').val() === 'all') {
                uo_allowed = "all";
            } else
                uo_allowed = "none";

            if ($('input[name=hm_companies]:checked').val() === 'list') {
                hm_allowed = $('#hm_companies_list').val();
            } else if ($('input[name=hm_companies]:checked').val() === 'all') {
                hm_allowed = 'all';
            } else
                hm_allowed = "none";

            if ($('input[name=ea]:checked').val() === 'list') {
                ea_allowed = $('#ea_countries_list').val();
                //var eacompanies = $('#ea_companies_list').val() || [];
                var selectArr = [];

                $('#ea_companies_list option').each(function() {
                    selectArr.push($(this).val());
                });
                ea_companies_allowed = selectArr.join(',');
            } else if ($('input[name=ea]:checked').val() === 'all') {
                ea_allowed = "all";
                ea_companies_allowed = '';
            } else {
                ea_allowed = "none";
                ea_companies_allowed = '';
            }

            if ($('input[type=checkbox][name=individual]:checked').length) {
                individual = 1;
            }


            if (typeof chkelem == 'undefined')
                chkelem = $('input.group1:checked');

            $.each(chkelem, function(i) {
                var chkey = $(this).attr('alt');
                $(this).attr('allowchosen', '1');

                if ($('input[type=hidden][alt=' + chkey + ']').length == 0) {
                    var parent_td = $(this).parent();
                    parent_td.append('<input type="button" alt="' + chkey + '" class="fixallow btn group2" value="متاح ل"  />');
                    parent_td.append('<input alt="' + chkey + '" type="hidden" value="' + uo_allowed + '" name="had_uo_allow[' + chkey + ']" />');
                    parent_td.append('<input hmalt="' + chkey + '" type="hidden" value="' + hm_allowed + '" name="had_hm_allow[' + chkey + ']" />');
                    parent_td.append('<input eaalt="' + chkey + '" type="hidden" value="' + ea_allowed + '" name="had_ea_allow[' + chkey + ']" />');
                    parent_td.append('<input eacoalt="' + chkey + '" type="hidden" value="' + ea_companies_allowed + '" name="had_eaco_allow[' + chkey + ']" />');
                    parent_td.append('<input indvalt="' + chkey + '" type="hidden" value="' + individual + '" name="had_individual[' + chkey + ']" />');
                } else {
                    $('input[type=hidden][alt=' + chkey + ']').val(uo_allowed);
                    $('input[type=hidden][hmalt=' + chkey + ']').val(hm_allowed);
                    $('input[type=hidden][eaalt=' + chkey + ']').val(ea_allowed);
                    $('input[type=hidden][eacoalt=' + chkey + ']').val(ea_companies_allowed);
                    $('input[type=hidden][indvalt=' + chkey + ']').val(individual);
                }
            });
            $('input.group1:checked').removeAttr('checked');

            return false;
        });

        $('#divForm').on('click', 'button[name=savedisallow]', function() {
            $.fancybox.close();
            var uo_disallowed = '';
            var hm_disallowed = '';
            var ea_disallowed = '';
            var ea_companies_disallowed = '';
            var disindividual = 0;

            if ($('input[name=uo_companies]:checked').val() === 'list') {
                uo_disallowed = $('#uo_companies_list').val();
            } else
                uo_disallowed = "all";

            if ($('input[name=hm_companies]:checked').val() === 'list') {
                hm_disallowed = $('#hm_companies_list').val();
            } else
                hm_disallowed = "all";

            if ($('input[name=ea]:checked').val() === 'list') {
                ea_disallowed = $('#ea_countries_list').val();
                //var eacompanies = $('#ea_companies_list').val() || [];
                var selectArr = [];

                $('#ea_companies_list option').each(function() {
                    selectArr.push($(this).val());
                });
                ea_companies_disallowed = selectArr.join(',');
            } else {
                ea_disallowed = "all";
                ea_companies_disallowed = '';
            }




            if ($('input[type=checkbox][name=individual]:checked').length) {
                disindividual = 1;
            }


            if (typeof chkelem == 'undefined')
                chkelem = $('input.group1:checked');

            $.each(chkelem, function(i) {
                var chkey = $(this).attr('disalt');
                $(this).attr('disallowchosen', '1');

                if ($('input[type=hidden][disalt=' + chkey + ']').length == 0) {
                    var parent_td = $(this).parent();
                    parent_td.append('<input type="button" disalt="' + chkey + '" class="fixdisallow btn group2" value="غير متاح ل"  />');
                    parent_td.append('<input disalt="' + chkey + '" type="hidden" value="' + uo_disallowed + '" name="had_uo_disallow[' + chkey + ']" />');
                    parent_td.append('<input dishmalt="' + chkey + '" type="hidden" value="' + hm_disallowed + '" name="had_hm_disallow[' + chkey + ']" />');
                    parent_td.append('<input diseaalt="' + chkey + '" type="hidden" value="' + ea_disallowed + '" name="had_ea_disallow[' + chkey + ']" />');
                    parent_td.append('<input diseacoalt="' + chkey + '" type="hidden" value="' + ea_companies_disallowed + '" name="had_eaco_disallow[' + chkey + ']" />');
                    parent_td.append('<input disindvalt="' + chkey + '" type="hidden" value="' + disindividual + '" name="had_disindividual[' + chkey + ']" />');
                } else {
                    $('input[type=hidden][disalt=' + chkey + ']').val(uo_disallowed);
                    $('input[type=hidden][dishmalt=' + chkey + ']').val(hm_disallowed);
                    $('input[type=hidden][diseaalt=' + chkey + ']').val(ea_disallowed);
                    $('input[type=hidden][diseacoalt=' + chkey + ']').val(ea_companies_disallowed);
                    $('input[type=hidden][disindvalt=' + chkey + ']').val(disindividual);
                }
            });
            $('input.group1:checked').removeAttr('checked');

            return false;
        });

        $('#divForm').on('click', 'input[name=uo_companies]', function() {
            if ($(this).val() === 'all' || $(this).val() === 'none') {
                $('#uo_companies_list').chosen().chosen('destroy').css('display', 'none');
            } else {
                $('#uo_companies_list').css('display', 'block').chosen().css('display', 'none');
            }
        });

        $('#divForm').on('click', 'input[name=hm_companies]', function() {
            if ($(this).val() === 'all' || $(this).val() === 'none') {
                $('#hm_companies_list').chosen().chosen('destroy').css('display', 'none');
            } else {
                $('#hm_companies_list').css('display', 'block').chosen().css('display', 'none');
            }
        });

        $('#divForm').on('click', 'input[name=ea]', function() {
            if ($(this).val() === 'all' || $(this).val() === 'none') {
                $('.countriesdiv').css('display', 'none');

            } else {
                $('.countriesdiv').css('display', 'block');
                $('#ea_countries_list').css('display', 'block').chosen().css('display', 'none');
                $('#ea_countries_company_list').css('display', 'block').chosen().css('display', 'none');

            }
        });


        $('#divForm').on('click', 'button[name=add]', function() {
            var selecteditems = $('#ea_countries_companies_list option:selected').clone();
            $('#ea_companies_list').append(selecteditems);
            $('#ea_countries_companies_list option:selected').remove();

        });
        $('#divForm').on('click', 'button[name=remove]', function() {
            $('#ea_companies_list option:selected').remove();
        });


        $("#ea_countries_list").change(function() {

            var chosencou = $(this).val();
            $.each(chosencou, function(i, valu) {
                $('#ea_countries_company_list option[value=' + valu + ']').attr('disabled', true);
            });

            $('#ea_countries_company_list').trigger("chosen:updated");
        });

        $('#divForm').on('change', '#ea_countries_company_list', function() {

            var cid = $(this).val();
            var ignorid = <?= (isset($ea_companyid)) ? $ea_companyid : '0' ?>;
            $.post('<?= site_url('hotel_availability/ajax_get_ea_companies') ?>', {'country_id': cid}, function(ajaxdata) {
                $('#ea_countries_companies_list option').remove();
                $.each(ajaxdata, function(id, value) {
                    if ($('#ea_companies_list option[value=' + value.id + ']').length == 0 && value.id != ignorid)
                        $('#ea_countries_companies_list').append("<option value='" + value.id + "'>" + value.name + "</option>");

                });
            }, 'json');
        });
    });
</script>
<script>
    function add_had(id) {
//    if( ! $('form').validationEngine('validate'))
//        return;
        var new_row = function() {/*
         <tr rel="{$id}">
         <td>
         <input type="checkbox" alt="{$id}" allowchosen='0' disallowchosen='0' class="checkbox group1" name="had_check[{$id}]" value="1" />
<input type="button" value="<?= lang('ha_allow_to') ?>" class="fixallow btn group2" alt="{$id}">
<input type="hidden" name="had_uo_allow[{$id}]" value="all" alt="{$id}">
<input type="hidden" name="had_hm_allow[{$id}]" value="all" hmalt="{$id}">
<input type="hidden" name="had_ea_allow[{$id}]" value="all" eaalt="{$id}">
<input type="hidden" name="had_eaco_allow[{$id}]" value="" eacoalt="{$id}">
<input type="hidden" name="had_individual[{$id}]" value="0" indvalt="{$id}">

<input type="button" value="<?= lang('ha_disallow_to') ?>" class="fixdisallow btn group2" disalt="{$id}">
<input type="hidden" name="had_uo_disallow[{$id}]" value="none" disalt="{$id}">
<input type="hidden" name="had_hm_disallow[{$id}]" value="none" dishmalt="{$id}">
<input type="hidden" name="had_ea_disallow[{$id}]" value="none" diseaalt="{$id}">
<input type="hidden" name="had_eaco_disallow[{$id}]" value="" diseacoalt="{$id}">
<input type="hidden" name="had_disindividual[{$id}]" value="0" disindvalt="{$id}">
         </td>
         <td>
<?= form_dropdown('had_period_id[{$id}]', $had_periods, false, ' class="validate[required] input-huge"') ?>
         </td>
         <td>
<?= form_dropdown('had_housing_type_id[{$id}]', $had_housing, false, ' class="validate[required] input-huge"') ?>
         </td>
         <td>
<?= form_dropdown('had_room_type_id[{$id}]', $had_room_types, false, ' class="input-huge"') ?>
         </td>
         <td style="min-width: 70px;">
<!--         <input name="had_etlal[{$id}]" type="checkbox" value="1"  /> -->
         <?= form_dropdown('had_etlal[{$id}]', $erp_hotels_availability_views, false, ' class="input-huge"') ?>
         
         </td>
         <td>
<?= form_input('had_sale_price[{$id}]', false, 'class="validate[required] input-huge"') ?>
         </td>
         <td>
         <div class="span3">
         <label>من</label> 
         </div>
         <div class="span9">
<?= form_input('had_start_date[{$id}]', set_value('had_start_date[{$id}]'), 'rel="{$id}" class="datepicker input-huge sale_from"') ?>
         </div>
         <div class="span3">
         <label><?= lang('ha_date_to') ?> </label>  
         </div>
         <div class="span9">
<?= form_input('had_end_date[{$id}]', set_value('had_end_date[{$id}]'), 'rel="{$id}" class="datepicker input-huge sale_to"') ?>
         </div>
         </td>
         <td>
<?= form_dropdown('had_room_service_id[{$id}]', $additions, set_value('had_room_service_id[{$id}]'), 'class="chosen-select chosen-rtl input-full" tabindex="4" data-placeholder="' . lang('global_all') . '"')
?>
         </td>
         <td>
         <input name="had_offer_status[{$id}]" type="checkbox" value="1"  />
         </td>
         <td>
         <input name="notes[{$id}]" type="text" value="" />
         </td>
         <td class="TAC"><a href="javascript:void(0)" onclick="delete_had('{$id}')"><span class="icon-trash"></span></a></td>
         </tr>
         
         */
        }.toString().replace(/{\$id}/g, id).slice(14, -3)
        $('#had').append(new_row);
        var config = {
            '.chosen-select': {width: "100%"},
            '.chosen-select-deselect': {allow_single_deselect: true},
            '.chosen-select-no-single': {disable_search_threshold: 10},
            '.chosen-select-no-results': {no_results_text: 'Oops, nothing found!'},
            '.chosen-select-width': {width: "95%"}
        }
        for (var selector in config) {
            $(selector).chosen(config[selector]);
        }
        $('.sale_from').datepicker({
            dateFormat: "yy-mm-dd",
            controlType: 'select',
            timeFormat: 'HH:mm',
            onClose: function(selectedDate) {
                var rid = $(this).attr('rel');
                $(".sale_to[rel=" + rid + "]").datepicker("option", "minDate", selectedDate);
            }
        });

        $('.sale_to').datepicker({
            dateFormat: "yy-mm-dd",
            controlType: 'select',
            timeFormat: 'HH:mm',
            onClose: function(selectedDate) {
                var rid = $(this).attr('rel');
                $(".sale_from[rel=" + rid + "]").datepicker("option", "maxDate", selectedDate);
            }
        });
    }
    function delete_had(id, database) {
        if (typeof database === 'undefined')
        {
            $('#had').find('tr[rel="' + id + '"]').remove();
        }
        else
        {
            $('#had').find('tr[rel=' + id + ']').remove();
            var hidden_input = '<input type="hidden" name="had_remove[]" value="' + id + '" />';
            $('#had').append(hidden_input);
        }

    }
</script>
