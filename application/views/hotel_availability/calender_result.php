<? if ($this->input->post() || isset($results)) : ?>

    <div class="nav">
        <div id="nextdate" style="cursor: pointer;" alt="<?= date('Y-m-d', strtotime($startdate) + (3600 * 24 * 15)) ?>" class="Fleft icon-arrow-left" >  </div>
        <div  class="Fleft" style="margin: 0 15px;" ><?= date('F', strtotime($enddate)) ?> </div>
        <div id="prevdate" style="cursor: pointer;" alt="<?= date('Y-m-d', strtotime($startdate) - (3600 * 24 * 15)) ?>" class="Fright icon-arrow-right" >  </div>
        <div  class="Fright" style="margin: 0 15px;" ><?= date('F', strtotime($startdate)) ?> </div>
    </div>
    <div style="clear: both"></div>
    <? foreach ($results as $hid => $result) : ?><? $passhotel = true ?>
                <? foreach ($result['rooms'] as $floor) {if (count($floor['room'])) $passhotel=FALSE;} ?>
                <? if ($passhotel) continue; ?>
        <div class="table-warp">
            <table id="availability_calender_<?= $result['hotel']->erp_hotel_id ?>" class="availability_calender" width="100%" border="0">
                <thead>
                            <tr>
                                <th colspan="2" rowspan="3" bgcolor="#FFFFFF" style="width: 56px ! important;" scope="col"><?= $result['hotel']->{name()} ?></th>
                                <th scope="col" style="width: 26px;"></th>
                                <? for ($datetime = strtotime($startdate); $datetime <= strtotime($enddate); $datetime += (3600 * 24)) : ?>
                                    <th scope="col" style="width: 26px;" <? if(in_array(date('D', $datetime),array('Fri','Sat'))):?>class="weekend"<? endif ?> ><?= date('D', $datetime) ?></th>
                                <? endfor ?>
                            </tr>
                            <tr>
                                <th>م</th>
                                <? for ($datetime = strtotime($startdate); $datetime <= strtotime($enddate); $datetime += (3600 * 24)) : ?>
                                    <th scope="col" <? if(in_array(date('D', $datetime),array('Fri','Sat'))):?>class="weekend"<? endif ?> ><?= date('d', $datetime) ?></th>
                                <? endfor ?>
                            </tr>
                            <tr>
                                <th>هـ</th>
                                <? for ($datetime = strtotime($startdate); $datetime <= strtotime($enddate); $datetime += (3600 * 24)) : ?>
                                    <th scope="col" <? if(in_array(date('D', $datetime),array('Fri','Sat'))):?>class="weekend"<? endif ?> ><?=
                                        $this->hijrigregorianconvert->ConstractDay($this->hijrigregorianconvert->GregorianToHijri(date('d-m-Y', $datetime), 'DD-MM-YYYY'), 'YYYY-MM-DD')
                                        ?></th>
                                <? endfor ?>
                            </tr>
                            </thead>
                <tbody>
                <? foreach ($result['rooms'] as $floor) : ?>
                    <? if (!count($floor['room'])) continue; ?>
                    <tr>
                        <th style="width: 20px;" rowspan="<?= count($floor['room']) ?>" class="rotate"><label><?= $floor['floor']->{name()} ?></label></th>
                        <th  style="width: 20px;" rowspan="<?= count($floor['room']) ?>"><img src="<?= base_url("static/img/roomx.png") ?>" alt="room-img" />      <label><?= $floor['floor']->erp_hotelroomsize_id ?></label></th>
                        <? foreach ($floor['room'] as $key => $value) : ?>
                            <td  style="width: 20px;"><?= $key ?></td>
                            <? for ($datetime = strtotime($startdate); $datetime <= strtotime($enddate); $datetime += (3600 * 24)) : ?>
                                <td style="width: 20px;" rel='<?= '{ "date":"' . $datetime . '","room":"' . $key . '","hotel":"' . $result['hotel']->erp_hotel_id . '" }' ?>' <? if (isset($value[date('d', $datetime)])) : ?>class="<? if ($value[date('d', $datetime)]) : ?>av-hotel<? else : ?>cl-hotel<? endif ?>"<? endif ?> ></td>
                            <? endfor ?>
                        </tr><tr>
                        <? endforeach ?>
                    <? endforeach ?>
                </tr>
                </tbody>
            </table>
<!--             <button class="btn pull-left" onclick="$(this).parent().find('table td.ui-selected').removeClass('ui-selected');"  style="margin: 5px;"><?= lang('undo_select') ?></button>
                        <button class="btn pull-left reservation_order"  style="margin: 5px;"><?= lang('reserve_rooms') ?> </button>-->
        </div>
    <div class="Div-Sep">
                        <a></a>
                    </div>
    <? endforeach ?>
<? endif ?>