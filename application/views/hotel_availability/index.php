<script>.widget{width:98%;}</script>

<div class="span12">
    <div class="widget">
       
        <div class="path-container Fright">
            <div class="path-name Fright">
                <a href=""><?= lang('global_system_management') ?></a>
            </div>
            <div class="path-arrow Fright"></div>
            <div class="path-name Fright">
                <?= lang('menu_hotel_availabilty')?>
            </div>
        </div>
    </div>
</div>

<div class="row-fluid">
    <div class="widget">
        <div class="widget-header">
            <div class="widget-header-icon Fright">
                <span class="icos-pencil2"></span>
            </div>
            <div class="widget-header-title Fright">
                <?= lang('menu_hotel_availabilty')?>
            </div>
             <a class="btn Fleft" href="<?= site_url($module.'/manage') ?>"><?= lang('global_add_new_record') ?></a>
        </div>
        <div class="widget-container">
            <div class='table-responsive' >
                <table cellpadding="0" class="fsTable" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th><?= lang('ha_city_id') ?></th>
                            <th><?= lang('ha_hotel_id') ?></th>
                            <th><?= lang('ha_date_from') ?> - <?= lang('ha_date_to') ?></th>
                            <th><?= lang('ha_status') ?></th>
                            <th><?= lang('global_actions') ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <? if (isset($items)): ?>
                            <? foreach ($items as $item): ?>
                                <tr>
                                    <td><?= $item->city ?></td>
                                    <td><?= $item->hotel ?></td>
                                    <td><?= $item->date_from ?> - <?= $item->date_to ?></td>
                                    <td><?= lang('ha_status_'.$item->availability_status) ?></td>
                                    <td class="TAC">
                                        <a href="<?= site_url($module."/manage/" . $item->erp_hotels_availability_master_id) ?>" ><span class="icon-pencil"></span></a>
                                        
                                        <a href="<?= site_url($module.'/pdf_export/' . $item->erp_hotels_availability_master_id) ?>" target="_blank" class="icon-book"></a>
                              
                              			<a href="<?= site_url($module.'/delete/' . $item->erp_hotels_availability_master_id) ?>" onclick="return confirm('<?= lang('global_are_you_sure_you_want_to_delete')?>')"><span class="icon-trash"></span></a>
                        
                                    </td>
                                </tr>
                            <? endforeach ?>
                        <? endif ?>
                    </tbody>
                </table>
<!--                <? if (isset($pagination)): ?><?= $pagination ?><? endif ?>-->
            </div>
        </div>
    </div>
</div>



