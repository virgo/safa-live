<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); ?>
<link href="<?= CSS ?>/stylesheets.css" rel="stylesheet" type="text/css" />
<? if (lang('global_lang') == 'ar'): ?>
    <link href="<?= CSS ?>/stylesheet-ar.css" rel="stylesheet" type="text/css" />
<? else: ?><? endif ?>
<script type="text/javascript" src='<?= JS ?>/plugins/jquery/jquery-1.9.1.min.js'></script>
<script type="text/javascript" src='<?= JS ?>/plugins/jquery/jquery-ui-1.10.1.custom.min.js'></script>
<script type="text/javascript" src='<?= JS ?>/plugins/bootstrap/bootstrap.min.js'></script>
<link href="<?= CSS_JS ?>/dateandtime/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src='<?= CSS_JS ?>/dateandtime/bootstrap-datetimepicker.min.js'></script>
<style>

body{
height: auto!important;
overflow: auto !important;
background:none transparent !important;
overflow: hidden;
min-height: 1%;
}

.row-fluid{
padding-top: 5px !important;
padding-bottom: 5px !important;
}
</style>


<!-- multi selection choosen -->
<link rel="stylesheet" href="<?= NEW_CSS_JS ?>/multi-select-chosen/chosen.css">
<script src="<?= NEW_CSS_JS ?>/multi-select-chosen/chosen.jquery.js" type="text/javascript"></script>

<?= form_open('hotel_availability/search_popup_for_trip', ' name="frm_search_popup_for_trip" id="frm_search_popup_for_trip" method="get"') ?>
            

    <div class="">
        <style>
            .updated_msg{
                display:none;
                background-color:#ccee97;
                font-weight: bold;
                text-align: center;
                border:3px solid #cccdc9; 
                padding:20px;  
                margin-bottom:10px;
                border-radius:15px;
                margin:10px; 
            }
            .wizerd-div,.row-form{width: 96%;margin:6% 2%;}
            label{padding: 0;}
           
        </style>
        
        
        <div class="modal-header">
            <?php echo  lang('global_search') ?>
    	</div>
                             
        <div class="modal-body" style="overflow: visible">
            
            <div class="row-fluid">
            <div class="span6">
                <div class="span4 TAL Pleft10">
                <label><?= lang('ha_city_id') ?>:</label>
                </div>
                <div class="span8">
                    <?= form_dropdown('city_id', $cities, set_value('city_id'), 'class="validate[required] input-full chosen-select" style="width:100%;" id="city_id"') ?>
                </div>
            </div>
            <div class="span6">
                <div class="span4 TAL Pleft10">
                <label><?= lang('ha_hotel_id') ?>:</label>
                </div>
                <div class="span8">
                    <?= form_multiselect('hotel_id[]', $hotels, set_value('hotel_id'), 'class="validate[required] chosen-select input-full" style="width:100%;" id="hotel_id" data-placeholder="'.lang('').'"') ?>
                </div>
            </div>
        	</div>
        <div class="row-fluid">
            <div class="span6">
                <div class="span4 TAL Pleft10">
                    <label><?= lang('ha_date_from') ?>:</label>
                </div>
                <div class="span8">
                    <?= form_input('date_from', set_value('date_from'), 'class="validate[required] input-full datepicker" id="date_from" ') ?>
                </div>
            </div>
            <div class="span6">
                <div class="span4 TAL Pleft10">
                <label><?= lang('ha_date_to') ?>:</label>
                </div>
                <div class="span8">
                    <?= form_input('date_to', set_value('date_to'), 'class="validate[required] input-full datepicker" id="date_to" ') ?>
                </div>
            </div>
        </div>
  </div>
            
  <div class="toolbar bottom TAC"><input type="submit" name="search" value="<?= lang('global_search') ?>" class="btn btn-primary" /></div>
            

       


    <div class="">
        
                    
        <div class="modal-body">
            <table cellpadding="0" class="" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th><input type="checkbox" class="checkall" id="checkall" onchange='checkUncheck()'/> </th>
                        <th><?= lang('ha_city_id') ?></th>
                        <th><?= lang('ha_hotel_id') ?></th>
                        <th><?= lang('ha_room_type') ?></th>
                        <th><?= lang('ha_date_from').' - '. lang('ha_date_to') ?></th>
                        <th><?= lang('available_rooms_count') ?></th>
                        <th><?= lang('entry_date') ?></th>
                        <th><?= lang('exit_date') ?></th>
                        <th><?= lang('required_rooms_count') ?></th>
                    </tr>
                </thead>
                <tbody>
                    <? if (isset($results)): ?>
                        <? foreach ($results as $value): ?>
                            <tr id="row_<?php echo $value->erp_hotels_availability_rooms_id; ?>">
                            <td><input type="checkbox" name="rooms[]" id="rooms[]"  value="<?= $value->erp_hotels_availability_rooms_id ?>" title=""/></td>
                            <td>
                            <input type='hidden' name='hotels_availability_sub_room_id_<?= $value->erp_hotels_availability_rooms_id ?>'  id='hotels_availability_sub_room_id_<?= $value->erp_hotels_availability_rooms_id ?>'  value="<?= $value->erp_hotels_availability_sub_room_id ?>" />
                            <input type='hidden' name='hotels_availability_room_detail_number_<?= $value->erp_hotels_availability_rooms_id ?>'  id='hotels_availability_room_detail_number_<?= $value->erp_hotels_availability_rooms_id ?>'  value='<?= $value->number ?>'/>
                            
                            <input type='hidden' name='city_id_<?= $value->erp_hotels_availability_rooms_id ?>'  id='city_id_<?= $value->erp_hotels_availability_rooms_id ?>'  value='<?= $value->erp_city_id ?>'/>
                        	<input type='hidden' name='city_name_<?= $value->erp_hotels_availability_rooms_id ?>'  id='city_name_<?= $value->erp_hotels_availability_rooms_id ?>'  value='<?= $value->city ?>'/>
                        	
                        	<?= $value->city ?></td>
                            <td>
                            <input type='hidden' name='erp_hotel_id_<?= $value->erp_hotels_availability_rooms_id ?>'  id='erp_hotel_id_<?= $value->erp_hotels_availability_rooms_id ?>'  value='<?= $value->erp_hotel_id ?>'/>
                            <input type='hidden' name='erp_hotel_name_<?= $value->erp_hotels_availability_rooms_id ?>'  id='erp_hotel_name_<?= $value->erp_hotels_availability_rooms_id ?>'  value='<?= $value->{name()} ?>'/>
                            
                        	<?= $value->{name()} ?>
                            <? for ($i = 0; $i < $value->erp_hotellevel_id; $i++) : ?>
                               <img src="<?= IMAGES ?>/imgs/star.png" alt="*" />
                            <? endfor ?>
                            </td>
                            <td>
                            <input type='hidden' name='erp_hotelroomsize_id_<?= $value->erp_hotels_availability_rooms_id ?>'  id='erp_hotelroomsize_id_<?= $value->erp_hotels_availability_rooms_id ?>'  value='<?= $value->erp_hotelroomsize_id ?>'/>
                            <?= $value->roomtype ?>
                            </td>
                            <td>
                            <input type='hidden' name='availability_from_date_<?= $value->erp_hotels_availability_rooms_id ?>'  id='availability_from_date_<?= $value->erp_hotels_availability_rooms_id ?>'  value='<?= $value->from_date ?>'/>
                            <input type='hidden' name='availability_to_date_<?= $value->erp_hotels_availability_rooms_id ?>'  id='availability_to_date_<?= $value->erp_hotels_availability_rooms_id ?>'  value='<?= $value->to_date ?>'/>
                            
                            <?= $value->from_date ?> - <?= $value->to_date ?></td>
                            <td>
                            <input type='hidden' name='available_count_<?= $value->erp_hotels_availability_rooms_id ?>'  id='available_count_<?= $value->erp_hotels_availability_rooms_id ?>'  value='<?= $value->available_count ?>'/>
                            <?= $value->available_count ?>
                            </td>
                            <td>
                            <?= form_input('entry_date_'.$value->erp_hotels_availability_rooms_id, set_value('entry_date'), 'class="validate[required] input-full datepicker" id="entry_date_'.$value->erp_hotels_availability_rooms_id.'" ') ?>
                            </td>
                            <td>
                            <?= form_input('exit_date_'.$value->erp_hotels_availability_rooms_id, set_value('exit_date'), 'class="validate[required] input-full datepicker" id="exit_date_'.$value->erp_hotels_availability_rooms_id.'" ') ?>
                            </td>
                            <td><?= form_input('required_rooms_count_'.$value->erp_hotels_availability_rooms_id, set_value('required_rooms_count'), 'class="validate[required] input-full" id="required_rooms_count_'.$value->erp_hotels_availability_rooms_id.'" ') ?></td>
                            </tr>
                        <? endforeach ?>
                    <? endif; ?>
                </tbody>
            </table>
        </div>
        
      
      <div class="toolbar bottom TAC">
          <input  type ="button" value="<?= lang('global_submit') ?>" class="btn btn-primary" onclick="selectRooms();">
      </div>
        
    </div>
 </div>    
 
<?= form_close() ?>
    
<script type="text/javascript">
//By Gouda.
function checkUncheck() 
{		
	
    for (var i = 0; i < document.frm_search_popup_for_trip.elements.length; i++ ) 
    {
        if (document.frm_search_popup_for_trip.elements[i].type == 'checkbox' && document.frm_search_popup_for_trip.elements[i].id !='checkall') 
	    {		       		    
        	document.frm_search_popup_for_trip.elements[i].checked = document.frm_search_popup_for_trip.elements['checkall'].checked; 
        }
    }
}

$(document).ready(function() {
    var config = {
        '.chosen-select': {width: "100%"},
        '.chosen-select-deselect': {allow_single_deselect: true},
        '.chosen-select-no-single': {disable_search_threshold: 10},
        '.chosen-select-no-results': {no_results_text: 'Oops, nothing found!'},
        '.chosen-select-width': {width: "95%"}
    }
    for (var selector in config) {
        $(selector).chosen(config[selector]);
    }


    // binds form submission and fields to the validation engine
    $("#frm_search_popup_for_trip").validationEngine({
            prettySelect : true,
            useSuffix: "_chosen",
            promptPosition : "topRight:-150"
            //promptPosition : "bottomLeft"
    });

 
});


//UPDATE HOTELS
function update_hotels()
{
    city_id = $('#city_id').val();
    $.post('<?= site_url('hotel_availability/ajax_get_hotels') ?>', {city_id: city_id, no_select: true}, function(data) {
        $('#hotel_id').html(data);
        $('#hotel_id').trigger('chosen:updated');
    });
}
$('#city_id').change(function() {
    update_hotels();
});


$('.datetimepicker').datetimepicker({
    dateFormat: "yy-mm-dd",
    //controlType: 'select',
    timeFormat: 'HH:mm'
});

$('.datepicker').datepicker({
    dateFormat: "yy-mm-dd",
    //controlType: 'select',
    timeFormat: 'HH:mm'
});



</script>

<script type="text/javascript">
var counter = parent.document.getElementById('hdn_trip_hotel_counter').value;

function selectRooms()
{
	//var erp_hotels_availability_rooms_id='';
	//var erp_hotels_availability_rooms_id_arr=new Array();
	
	
	//By Gouda
	var are_checked=false;
	for (var i = 0; i < document.frm_search_popup_for_trip.elements.length; i++ ) 
	{
	    if (document.frm_search_popup_for_trip.elements[i].checked ) {
	    	are_checked = true;
	     } 
	}
	if(are_checked==false) {
		alert('<?php echo lang('global_select_one_record_at_least');?>');
		return;
	}


	for (var i = 0; i < document.frm_search_popup_for_trip.elements.length; i++ ) 
    {
        if (document.frm_search_popup_for_trip.elements[i].type == 'checkbox' && document.frm_search_popup_for_trip.elements[i].id =='rooms[]') {
		    if (document.frm_search_popup_for_trip.elements[i].checked){
		    	
		    	//document.frm_search_popup_for_trip.elements[i].value;


		    	//vars
		    	var checkin_datetime_value= document.getElementById('date_from').value;
		    	var checkout_datetime_value= document.getElementById('date_to').value;

		    	var erp_hotels_availability_rooms_id= document.frm_search_popup_for_trip.elements[i].value;
		    	
		    	var entry_date_value= document.getElementById('entry_date_'+erp_hotels_availability_rooms_id).value;
		    	var exit_date_value= document.getElementById('exit_date_'+erp_hotels_availability_rooms_id).value;
		    	var date1 = new Date(entry_date_value)
		    	var date2 = new Date(exit_date_value); 
		    	var timeDiff = Math.abs(date2.getTime() - date1.getTime());
		    	var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24)); 
		    	var nights_count_value = diffDays;

		    	var availability_from_date_value= document.getElementById('availability_from_date_'+erp_hotels_availability_rooms_id).value;
		    	var availability_to_date_value= document.getElementById('availability_to_date_'+erp_hotels_availability_rooms_id).value;
		    	
		    	var available_count_value= document.getElementById('available_count_'+erp_hotels_availability_rooms_id).value;
		    	var required_rooms_count_value= document.getElementById('required_rooms_count_'+erp_hotels_availability_rooms_id).value;

		    	if (entry_date_value=='') {
		    		alert('<?php echo lang('enter_entry_date_and_exit_date');?>');
		    		document.getElementById('entry_date_'+erp_hotels_availability_rooms_id).focus();
		 	    	return false;
		 	    } 
		    	if (exit_date_value=='') {
		    		alert('<?php echo lang('enter_entry_date_and_exit_date');?>');
		    		document.getElementById('exit_date_'+erp_hotels_availability_rooms_id).focus();
		 	    	return false;
		 	    }

		    	if (entry_date_value > exit_date_value) {
			    	alert('<?php echo lang('enter_entry_must_be_less_than_exit_date');?>');
			    	document.getElementById('entry_date_'+erp_hotels_availability_rooms_id).focus();
			 	    return false;
			 	}
		 	    
		    	if (entry_date_value < availability_from_date_value || entry_date_value > availability_to_date_value) {
			    	alert('<?php echo lang('enter_exit_date_must_between_availability_date');?>');
			    	document.getElementById('entry_date_'+erp_hotels_availability_rooms_id).focus();
			 	    return false;
			 	}

		    	if (exit_date_value < availability_from_date_value || exit_date_value > availability_to_date_value) {
			    	alert('<?php echo lang('enter_exit_date_must_between_availability_date');?>');
			    	document.getElementById('exit_date_'+erp_hotels_availability_rooms_id).focus();
			 	    return false;
			 	}
		 	     
		    	if(!$.isNumeric(required_rooms_count_value)){
					alert('<?php echo lang('enter_true_value');?>');
					document.getElementById('required_rooms_count_'+erp_hotels_availability_rooms_id).focus();
					return;
				} 
		    	if(parseInt(required_rooms_count_value) > parseInt(available_count_value)) {
		    		alert('<?php echo lang('required_rooms_count_more_than_available');?>');
		    		document.getElementById('required_rooms_count_'+erp_hotels_availability_rooms_id).focus();
		    		return;
		    	}
		    }
        }
    }
	
	
	
	for (var i = 0; i < document.frm_search_popup_for_trip.elements.length; i++ ) 
    {
        if (document.frm_search_popup_for_trip.elements[i].type == 'checkbox' && document.frm_search_popup_for_trip.elements[i].id =='rooms[]') {
		    if (document.frm_search_popup_for_trip.elements[i].checked){
		    	
		    	//document.frm_search_popup_for_trip.elements[i].value;
		    	
		    	counter++;
		    	
		    	if (counter > 100) {
	                alert("Only 100 trip_hotel allow");
	                return false;
	            }


		    	//vars
		    	var checkin_datetime_value= document.getElementById('date_from').value;
		    	var checkout_datetime_value= document.getElementById('date_to').value;

		    	var erp_hotels_availability_rooms_id= document.frm_search_popup_for_trip.elements[i].value;
		    	
		    	var entry_date_value= document.getElementById('entry_date_'+erp_hotels_availability_rooms_id).value;
		    	var exit_date_value= document.getElementById('exit_date_'+erp_hotels_availability_rooms_id).value;

		    	var date1 = new Date(entry_date_value)
		    	var date2 = new Date(exit_date_value); 
		    	var timeDiff = Math.abs(date2.getTime() - date1.getTime());
		    	var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24)); 
		    	var nights_count_value = diffDays;
			    	
		    	var city_id_value= document.getElementById('city_id_'+erp_hotels_availability_rooms_id).value;
		    	var hotel_id_value= document.getElementById('erp_hotel_id_'+erp_hotels_availability_rooms_id).value;

		    	var city_name_value= document.getElementById('city_name_'+erp_hotels_availability_rooms_id).value;
		    	var hotel_name_value= document.getElementById('erp_hotel_name_'+erp_hotels_availability_rooms_id).value;
		    	
		    	var hotels_availability_sub_room_id_value= document.getElementById('hotels_availability_sub_room_id_'+erp_hotels_availability_rooms_id).value;
		    	var hotels_availability_room_detail_number_value= document.getElementById('hotels_availability_room_detail_number_'+erp_hotels_availability_rooms_id).value;

		    	var available_count_value= document.getElementById('available_count_'+erp_hotels_availability_rooms_id).value;
		    	var required_rooms_count_value= document.getElementById('required_rooms_count_'+erp_hotels_availability_rooms_id).value;

		    	if (entry_date_value=='') {
		    		alert('<?php echo lang('enter_entry_date_and_exit_date');?>');
		    		document.getElementById('entry_date_'+erp_hotels_availability_rooms_id).focus();
		 	    	return false;
		 	    } 
		    	if (exit_date_value=='') {
		    		alert('<?php echo lang('enter_entry_date_and_exit_date');?>');
		    		document.getElementById('exit_date_'+erp_hotels_availability_rooms_id).focus();
		 	    	return false;
		 	    }
		    	if (entry_date_value > exit_date_value) {
			    	alert('<?php echo lang('enter_entry_must_be_less_than_exit_date');?>');
			    	document.getElementById('entry_date_'+erp_hotels_availability_rooms_id).focus();
			 	    return false;
			 	}
		 	     
		    	if(!$.isNumeric(required_rooms_count_value)){
					alert('<?php echo lang('enter_true_value');?>');
					document.getElementById('required_rooms_count_'+erp_hotels_availability_rooms_id).focus();
					return;
				} 
		    	if(parseInt(required_rooms_count_value) > parseInt(available_count_value)) {
		    		alert('<?php echo lang('required_rooms_count_more_than_available');?>');
		    		document.getElementById('required_rooms_count_'+erp_hotels_availability_rooms_id).focus();
		    		return;
		    	}

		    	
	            var new_div_trip_hotel = $(document.createElement('tr'));
	            new_div_trip_hotel.html('<td >' +
	                    '<input type="hidden"  name="safa_trip_hotel_id' + counter + '"  id="safa_trip_hotel_id' + counter + '"  value="0"/>' +
	                    '<input type="hidden"  name="hotels_availability_sub_room_id' + counter + '"  id="hotels_availability_sub_room_id' + counter + '"  value="'+hotels_availability_sub_room_id_value+'"/>' +
	                    '<input type="hidden"  name="hotels_availability_room_detail_number' + counter + '"  id="hotels_availability_room_detail_number' + counter + '"  value="'+hotels_availability_room_detail_number_value+'"/>' +
	                    
	                    '<!--<select name="trip_hotel_erp_city_id' + counter + '" id="trip_hotel_city_id' + counter + '"onchange="getHotel(this.value, ' + counter + ',' + parent.$("#safa_ea_id").val() + ')" class="validate[required] trip_hotel_erp_city_id" rel="' + counter + '">' +
						<? foreach ($ksa_cities as $key => $value): ?>
						                '<option value="<?= $key ?>"><?= $value ?></option>' +
						<? endforeach ?>
		
			            '</select> -->' +
			            '<input type="hidden"  name="trip_hotel_erp_city_id' + counter + '"  id="trip_hotel_erp_city_id' + counter + '"  value="'+city_id_value+'"/>' +
			            city_name_value +
			            '</td>' +
	                    '<td >' +
	                    '<!-- <div id="dv_hotel' + counter + '">' +
	                    '<select name="trip_hotel_erp_hotel_id' + counter + '" id="trip_hotel_erp_hotel_id' + counter + '"  class="validate[required] menu">' +
	                    <? foreach ($hotels as $key => $value): ?>
		                '<option value="<?= $key ?>"><?= $value ?></option>' +
						<? endforeach ?>
	                    '</select>' +
	                    '</div> --> '+
	                    '<input type="hidden"  name="trip_hotel_erp_hotel_id' + counter + '"  id="trip_hotel_erp_hotel_id' + counter + '"  value="'+hotel_id_value+'"/>' +
			            
	                    hotel_name_value +
	                    '</td>' +
	                    '<td >' +
	                    '<!--<input type="text" name="trip_hotel_nights_count' + counter + '"  id="trip_hotel_nights_count' + counter + '"class="validate[required] input-huge"  value="" />-->' +
	                    '<input type="hidden" name="trip_hotel_nights_count' + counter + '"  id="trip_hotel_nights_count' + counter + '"class="validate[required] input-huge"  value="'+nights_count_value+'" />' +
	                    nights_count_value +
	                    '</td>' +
	                    '<td >' +
	                    '<select name="trip_hotel_erp_meal_id' + counter + '" id="trip_hotel_erp_meal_id' + counter + '"   class="validate[required]">' +
	                    <? foreach ($erp_meals as $key => $value): ?>
	                        '<option value="<?= $key ?>"><?= $value ?></option>' +
	                    <? endforeach ?>
	                    '</select>' +
	                    '</td>' +
	                    '<td >' +
	                    '<!--<input type="text" name="trip_hotel_rooms' + counter + '"   id="trip_hotel_rooms' + counter + '" class=" input-huge"  value="'+required_rooms_count_value+'"/> -->' +
	                    '<input type="hidden" name="trip_hotel_rooms' + counter + '"   id="trip_hotel_rooms' + counter + '" class=" input-huge"  value="'+required_rooms_count_value+'"/>' +
	                    required_rooms_count_value +
	                    '</td>' +
	                    '<td >' +
//	                    '<div id="trip_hotel_checkin_datetime_' + counter + '" class="input-append date">' +
	                    '<!--<input type="text" data-format="yyyy-MM-dd" name="trip_hotel_checkin_datetime' + counter + '"  id="trip_hotel_checkin_datetime' + counter + '"   value="'+checkin_datetime_value+'" class="validate[required, custom[hotel_checkin_datetime], custom[hotel_checkin_datetime_1]] datepicker"  /> -->' +
	                    '<input type="hidden" name="trip_hotel_checkin_datetime' + counter + '"   id="trip_hotel_checkin_datetime' + counter + '" class=" input-huge"  value="'+entry_date_value+'"/>' +
	                    entry_date_value +
//	                    '<span class="add-on">' +
//	                    '<i data-time-icon="icon-time" data-date-icon="icon-calendar">' +
//	                    '</i>' +
//	                    '</span>' +
//	                    '</div>' +
	                    '</td>' +
	                    '<td >' +
//	                    '<div id="trip_hotel_checkout_datetime_' + counter + '" class="input-append date">' +
	                    '<!-- <input type="text" data-format="yyyy-MM-dd" name="trip_hotel_checkout_datetime' + counter + '"  id="trip_hotel_checkout_datetime' + counter + '"   value="'+checkout_datetime_value+'" class="validate[required, custom[date], future[#trip_hotel_checkin_datetime' + counter + '], custom[trip_hotel_checkout_datetime], custom[trip_hotel_checkout_datetime_]] trip_hotel_checkout_datetime datepicker" rel="' + counter + '" "/> -->' +
	                    '<input type="hidden" name="trip_hotel_checkout_datetime' + counter + '"   id="trip_hotel_checkout_datetime' + counter + '" class=" input-huge"  value="'+exit_date_value+'"/>' +
	                    exit_date_value +
//						'<span class="add-on">' +
//	                    '<i data-time-icon="icon-time" data-date-icon="icon-calendar">' +
//	                    '</i>' +
//	                    '</span>' +
//	                    '</div>' +
	                    '</td>' +
	                    '<td>' +
	                    '<a href="javascript:void(0)" class="" id="hrf_remove_trip_hotel" name="' + counter + '"><span class="icon-trash"></span></a>' +
	                    '</td>'
	                    );

                		new_div_trip_hotel.appendTo(parent.$("#div_trip_hotel_group"));

                		parent.$('.datepicker').datepicker({
			                dateFormat: "yy-mm-dd",
			                //controlType: 'select',
			                timeFormat: 'HH:mm'
			            });

                		//parent.document.getElementById('trip_hotel_city_id' + counter ).value=city_id_value;
                		//parent.document.getElementById('trip_hotel_erp_hotel_id' + counter ).value = hotel_id_value;
                		

                		parent.document.getElementById('hdn_trip_hotel_counter').value=counter;
		            	
						//alert(counter);
		   }
        }
	}

	
	//parent.$("#erp_hotels_availability_rooms_id").val(erp_hotels_availability_rooms_id_arr);
	//parent.$("#erp_hotels_availability_rooms_id").trigger("chosen:updated");

	
	parent.$.fancybox.close(); 
	//parent.Shadowbox.close();
}

function getHotel(cityId, counter, contract) {
    var strURL = "<?php echo base_url(); ?>/ea/trips/get_hotels_by_city/" + cityId + "/" + contract;
    var req = getXMLHTTP();
    if (req) {

        req.onreadystatechange = function() {
            if (req.readyState == 4) {
                // only if "OK"
                if (req.status == 200) {//dv_hotel
                    document.getElementById('trip_hotel_erp_hotel_id' + counter).innerHTML = req.responseText;
                } else {
                    alert("There was a problem while using XMLHTTP:\n" + req.statusText);
                }
            }
        }
        req.open("GET", strURL, true);
        req.send(null);
    }

}
</script>