<style>
    table tr th{background-color: #f1f1f1;}
    table td, table th{padding:1px!important;}
    th label,td label {
        display: inline-block!important;
        margin: 1px 0!important;
    }
    td table{
        border: none;

    }
    td {
        padding: 0!important;
    }
    table tr td{
        padding: 4px!important;
        border-bottom: 1px solid #ddd!important;
        background-color: #fff;
        /*        border-right: 1px solid #ddd!important;*/
    }

    /*    .availability_calender thead {
            display: block;
            
        }
        
        .availability_calender tbody {
            display: block;
            height: 200px;
            
        }
        
        table.availability_calender  tr td {
            width: 27px;
            padding: 1px !important;
        }*/


    td.ui-selected {
        background-color: #D6B476;
    }

    .rotate{background-color: #fff;}
    .rotate label {-webkit-transform: rotate(-90deg); 
                   -moz-transform: rotate(-90deg);margin: 10px 0!important;
                   width: 26px;
    }

    .av-hotel,.ch-hotel,.cl-hotel,.na-hotel{min-height: 20px;min-width: 20px;box-shadow: none;/*border-left: 0;*/}
    /*    .av-hotel:hover,.ch-hotel:hover,.cl-hotel:hover{box-shadow:0px -2px 12px 0px rgba(0,0,0,.14),0px 2px 12px 0px rgba(0,0,0,.14);}*/
    .av-hotel img,.ch-hotel img,.cl-hotel img{cursor: pointer;}
    .av-hotel{background: #B8D776;}
    .ch-hotel{background-color:#DE856D;}
    /*    .av-hotel:hover,.ch-hotel:hover,.cl-hotel:hover{
                        -webkit-transition: all .4s ease;
        -moz-transition: all .4s ease;
        -o-transition: all .4s ease;
        transition: all .4s ease;}*/
    .cl-hotel{background-color:#918183;}
    .plan {float: left;}


    .hotel{}
    .group{}

    label{margin: 0;padding: 5px;}
    .row-form > [class^="span"]{border: none;}

    .table-warp .box-shadow-last  {box-shadow: -12px 0 8px -4px rgba(0,0,0, 0.3);}
    .table-warp .box-shadow-first {box-shadow: 12px 0 15px -4px rgba(0,0,0, 0.3)}

    .table-warp .box-shadow-last-bottom  {box-shadow: -12px 0 8px -4px rgba(0,0,0, 0.3);}
    .table-warp .box-shadow-first-top {box-shadow: 0px -6px 10px 0px rgba(174, 187, 148, 0.79)}

    .table-warp th label {
        font-family: tahoma;
        font-size: 10px;
        font-weight: 900;
    }

    .calendars-month-header select{
        padding: 0;
    }

    .weekend {
        background-color: #666;
        color: #F1F1F1;
        text-shadow: 1px 1px 0 #000;
    }

</style>
<link href="<?= base_url("static/js/jquery.calendars/humanity.calendars.picker.css") ?>" type="text/css" rel="stylesheet">
<script src="<?= base_url("static/js/jquery.calendars/jquery.calendars.js") ?>"></script>
<script src="<?= base_url("static/js/jquery.calendars/jquery.calendars-ar.js") ?>"></script>
<script src="<?= base_url("static/js/jquery.calendars/jquery.calendars.plus.js") ?>"></script>
<script src="<?= base_url("static/js/jquery.calendars/jquery.calendars.picker.js") ?>"></script>
<script src="<?= base_url("static/js/jquery.calendars/jquery.calendars.picker-ar.js") ?>"></script>
<script src="<?= base_url("static/js/jquery.calendars/jquery.calendars.islamic.js") ?>"></script>
<script src="<?= base_url("static/js/jquery.calendars/jquery.calendars.islamic-ar.js") ?>"></script>
<script src="<?= NEW_JS ?>/plugins/jquery.fixedheadertable.js"></script>
<div class="widget">
    <div class="path-container Fright">
        <div class="path-name Fright">
            <a href=""><?= lang('global_system_management') ?></a>
        </div>
        <div class="path-arrow Fright">
        </div>
        <div class="path-name Fright">
            <a href=""><?= lang('menu_main_hotels') ?></a>
        </div>
        <div class="path-arrow Fright">
        </div>
        <div class="path-name Fright">
            <?= lang('availabilty_detail') ?>
        </div>
    </div>
</div>

<div class="widget">
    <div class="widget-header">
        <div class="widget-header-icon icon-pencil pull-right"></div>
        <div class="widget-header-title pull-right"><?= lang('availabilty_detail') ?></div>
    </div>
    <div class="widget-container">
        <form method="post">
            <div class="row-form" style="border: none;">
                <div class="span3">
                    <div class="span3"><label><?= lang('hotel') ?></label></div>
                    <div class="span9"><?=
                        form_multiselect('hotels[]', $hotels, isset($_POST['hotels']) ? $_POST['hotels'] : array(), 'class="hotels chosen-rtl"', 'style="width:100%!important;" placeholder="'.lang('global_all').'"')
                        ?></div>
                </div>
                <div class="" style="float:right;margin-right:37px">
                    <label class="pull-right" style="width: 35px; height: 21px;">هــ<input onclick="$('.date').calendarsPicker('destroy').val('').calendarsPicker({calendar: $.calendars.instance('islamic', 'ar'), dateFormat: 'dd-mm-yyyy'});" class="pull-right" style="height: 25px; margin-top: 0px;" name="datetype" type="radio" value="H"/></label>
                    <label class="pull-right"  style="clear: both;">م<input onclick="$('.date').calendarsPicker('destroy').val('').calendarsPicker({calendar: $.calendars.instance('gregorian', 'en'), dateFormat: 'dd-mm-yyyy'});" class="pull-right"  checked="checked" name="datetype" type="radio" value="G" /></label></div>
                <div class="span5">
                    <div class="span2"><label><?= lang('datefrom') ?></label></div>
                    <div class="span4"><input class="input-huge date" name="datefrom" id="datefrom" type="text"  value="<?=
                        set_value('datefrom', (isset($startdate)) ? $startdate : '01-' . date('m') . '-' . date('Y'))
                        ?>"></div>
                    <div class="span1"><label><?= lang('dateto') ?></label></div>
                    <div class="span4"><input class="input-huge date" name="dateto" id="dateto" type="text" value="<?= set_value('dateto', (isset($enddate)) ? $enddate : '30-' . date('m') . '-' . date('Y')) ?>"></div>
                </div>
                <div class="span1"><input name="filter" type="submit" class="btn" value="<?= lang('search') ?>" /></div>
            </div>
        </form>
        <div id="results">
            <? if (isset($startdate)) : ?>
                <div class="nav">
                    <div id="nextdate" style="cursor: pointer;" alt="<?=
                date('Y-m-d', strtotime($startdate) + (3600 * 24 * 15))
                ?>" class="Fleft icon-arrow-left" >  </div>
                    <div  class="Fleft" style="margin: 0 15px;" ><?= date('F', strtotime($enddate)) ?> </div>
                    <div id="prevdate" style="cursor: pointer;" alt="<?=
                     date('Y-m-d', strtotime($startdate) - (3600 * 24 * 15))
                ?>" class="Fright icon-arrow-right" > </div>
                    <div  class="Fright" style="margin: 0 15px;" ><?= date('F', strtotime($startdate)) ?> </div>
                </div>
                <div style="clear: both;"></div>
            <? endif ?>
            <? if ($this->input->post() || isset($results)) : ?>
                <? foreach ($results as $hid => $result) : ?><? $passhotel = true ?>
                    <? foreach ($result['rooms'] as $floor) {
                        if (count($floor['room'])) $passhotel = FALSE;
                    } ?>
        <? if ($passhotel) continue; ?>
                    <div class="table-warp">
                        <table id="availability_calender_<?= $result['hotel']->erp_hotel_id ?>" class="availability_calender" width="100%" border="0">
                            <thead>
                                <tr>
                                    <th colspan="2" rowspan="3" bgcolor="#FFFFFF" style="width: 58px ! important;" scope="col"><?= $result['hotel']->{name()} ?></th>
                                    <th scope="col" style="width: 26px;"></th>
                                    <? for ($datetime = strtotime($startdate); $datetime <= strtotime($enddate); $datetime += (3600 * 24)) : ?>
                                        <th scope="col" style="width: 26px;" <? if (in_array(date('D', $datetime), array('Fri', 'Sat'))): ?>class="weekend"<? endif ?> ><?= date('D', $datetime) ?></th>
        <? endfor ?>
                                </tr>
                                <tr>
                                    <th>م</th>
                                    <? for ($datetime = strtotime($startdate); $datetime <= strtotime($enddate); $datetime += (3600 * 24)) : ?>
                                        <th scope="col" <? if (in_array(date('D', $datetime), array('Fri', 'Sat'))): ?>class="weekend"<? endif ?> ><?= date('d', $datetime) ?></th>
        <? endfor ?>
                                </tr>
                                <tr>
                                    <th>هـ</th>
                                        <? for ($datetime = strtotime($startdate); $datetime <= strtotime($enddate); $datetime += (3600 * 24)) : ?>
                                        <th scope="col" <? if (in_array(date('D', $datetime), array('Fri', 'Sat'))): ?>class="weekend"<? endif ?> ><?=
                                            $this->hijrigregorianconvert->ConstractDay($this->hijrigregorianconvert->GregorianToHijri(date('d-m-Y', $datetime), 'DD-MM-YYYY'), 'YYYY-MM-DD')
                                            ?></th>
        <? endfor ?>
                                </tr>
                            </thead>
                            <tbody>
                                <? foreach ($result['rooms'] as $floor) : ?>
            <? if (!count($floor['room'])) continue; ?>
                                    <tr>
                                        <th  style="width: 20px;" rowspan="<?= count($floor['room']) ?>" class="rotate"><label><?= $floor['floor']->{name()} ?></label></th>
                                        <th  style="width: 20px;" rowspan="<?= count($floor['room']) ?>"><img src="../static/img/roomx.png" alt="room-img" />      <label><?= $floor['floor']->erp_hotelroomsize_id ?></label></th>
                                        <? foreach ($floor['room'] as $key => $value) : ?>
                                            <td  style="width: 24px;"><?= $key ?></td>
                                            <? for ($datetime = strtotime($startdate); $datetime <= strtotime($enddate); $datetime += (3600 * 24)) : ?>
                                                <td rel='<?= '{ "date":"' . $datetime . '","room":"' . $key . '","hotel":"' . $result['hotel']->erp_hotel_id . '" }' ?>' <?
                                                    if (isset($value[date('d', $datetime)])) :
                                                        ?>class="<? if ($value[date('d', $datetime)] == 1) : ?>av-hotel<? elseif ($value[date('d', $datetime)] == 2) : ?>ch-hotel<? else : ?>cl-hotel<? endif ?>"<? endif ?> >  </td>
                                            <? endfor ?>
                                        </tr><tr>
                                        <? endforeach ?>
        <? endforeach ?>
                                </tr>
                            </tbody>
                        </table>
        <!--                     <button class="btn pull-left" onclick="$(this).parent().find('table td.ui-selected').removeClass('ui-selected');"  style="margin: 5px;"><?= lang('undo_select') ?></button>
                         <button class="btn pull-left reservation_order"  style="margin: 5px;"><?= lang('reserve_rooms') ?> </button>-->
                    </div>
                    <div class="Div-Sep">
                        <a></a>
                    </div>
    <? endforeach ?>


            </div>
            <div class="row-fluid">
                <!--<button class="btn pull-left" onclick="$(this).parent().find('table td.ui-selected').removeClass('ui-selected');"  style="margin: 5px;"><?= lang('undo_select_all') ?> </button>-->

            </div>
            <div class="row-fluid">
                <div class="span4">
                    <div class="span1 av-hotel">
                    </div>
                    <div class="span2 "><label><?= lang('available') ?></label></div>
                    <div class="span1 ch-hotel"></div>
                    <div class="span2"><label><?= lang('disavailable') ?></label></div>
                    <div class="span1 cl-hotel"></div>
                    <div class="span2"><label><?= lang('closed') ?></label></div>
                </div>
                <div class="span4">
                    <div class="span1"><img src='../static/img/plan/ea.png' alt='room-img' style='width:24px;height:24px;' /></div>
                    <div class="span6"><label><?= lang('client') ?></label></div>
                    <div class="span1"><img src='../static/img/plan/bkg.png' alt='room-img' style='width:24px;height:24px;' /></div>
                    <div class="span3"><label><?= lang('pickpakage') ?></label></div>
                </div>
                <div class="span4">

                    <div class="span1"><img src='../static/img/plan/hotel.png' alt='room-img' style='width:24px;height:24px;' /></div>
                    <div class="span3"><label><?= lang('pickroom') ?></label></div>

                    <div class="span1"><img src='../static/img/plan/group.png' alt='room-img' style='width:24px;height:24px;' /></div>
                    <div class="span7"><label><?= lang('group_detail') ?></label></div>
                </div>
            </div>
<? endif ?>

    </div>
</div>
<script type="text/javascript">
    isObject = function(a) {
        return (!!a) && (a.constructor === Object);
    };
    $(function() {
        $('.date').calendarsPicker({calendar: $.calendars.instance('gregorian', 'en'), onSelect: customRange, dateFormat: 'dd-mm-yyyy'});
        function customRange(dates) {
            if (this.id == 'datefrom') {
                $('input[name=dateto]').calendarsPicker('option', 'minDate', dates[0] || null);
            } else {
                $('input[name=datefrom]').calendarsPicker('option', 'maxDate', dates[0] || null);
            }
        }

        $('.hotels').chosen();

//        $("table .ch-hotel").append($("<div class='plan'><a href='#' title='clint details'><img src='../static/img/plan/ea.png' alt='room-img' style='width:24px;height:24px;' /></a><a href='#' title='packege details'><img src='../static/img/plan/bkg.png' alt='room-img'  style='width:24px;height:24px;'/></a></div>"));
//        $("table .hotel").append($("<a href='#' title='room reservesion'><img src='../static/img/plan/hotel.png' alt='room-img' style='width:24px;height:24px;' /></a>"));
//        $("table .group").append($("<a href='#' title='person who reserved this room'><img src='../static/img/plan/group.png' alt='room-img' style='width:24px;height:24px;' /></a>"));

        $('td.ch-hotel').filter(function() {
            return !$(this).next('td').hasClass('ch-hotel')
        }).html("<img src='../static/img/plan/bkg.png' alt='room-img' style='width:18px;height:18px;' />");
        $('td.ch-hotel').filter(function() {
            return !$(this).next('td').hasClass('ch-hotel')
        }).prev().html("<img src='../static/img/plan/ea.png' alt='room-img' style='width:18px;height:18px;' />");


        $('tr').hover(function() {
            $(this).children("td:last").toggleClass("box-shadow-last", 500, "easeOutSine");
            $(this).children("td:first").toggleClass("box-shadow-first", 500, "easeOutSine");
        });

        $('tbody td').hover(function() {
            var cell = $(this).closest('td');
            var cellIndex = cell[0].cellIndex;
            if ($(this).parent().find('th').length)
                cellIndex -= 2;
            var tds = $(this).parents('table').find('thead tr:last-child th');
            $(tds[cellIndex]).toggleClass("box-shadow-first-top", 500, "easeOutSine");
        });

        $("table").tooltip();

        $('#results').on('mousedown', '#nextdate', function() {
            $.post('<?= site_url('hotel_availability/ajax_calendar') ?>', {datetype: 'G', datefrom: $(this).attr('alt'), hotels: $('.hotels').val()}, function(data) {
                $('#results').html(data);
//                $("#results").bind("mousedown", function(e) {
//                    e.metaKey = true;
//                }).selectable({filter: "td.av-hotel"});
                $('td.ch-hotel').filter(function() {
                    return !$(this).next('td').hasClass('ch-hotel')
                }).html("<img src='../static/img/plan/bkg.png' alt='room-img' style='width:18px;height:18px;' />");
                $('td.ch-hotel').filter(function() {
                    return !$(this).next('td').hasClass('ch-hotel')
                }).prev().html("<img src='../static/img/plan/ea.png' alt='room-img' style='width:18px;height:18px;' />");
            });
        });

        $('#results').on('click', '.reservation_order', function() {
            var seleted_data = {};
            var i = 0;
            $(this).parent().find('td.ui-selected').each(function(indx, elem) {
                var tdres = $.parseJSON($(elem).attr('rel'));
                if (!isObject(seleted_data[tdres.hotel]))
                    seleted_data[tdres.hotel] = {};
                if (!isObject(seleted_data[tdres.hotel][tdres.room]))
                    seleted_data[tdres.hotel][tdres.room] = {};
                seleted_data[tdres.hotel][tdres.room][i] = tdres.date;
                i++;
            });
            $.post('<?= site_url('hotel_availability/reserve') ?>', {'data': JSON.stringify(seleted_data)}, function(data) {
            });
        });

//        $("#results").bind("mousedown", function(e) {
//            e.metaKey = true;
//        }).selectable({filter: "td.av-hotel"});

        $('#results').on('mousedown', '#prevdate', function() {
            $.post('<?= site_url('hotel_availability/ajax_calendar') ?>', {datetype: 'G', datefrom: $(this).attr('alt'), hotels: $('.hotels').val()}, function(data) {
                $('#results').html(data);
//                $("#results").bind("mousedown", function(e) {
//                    e.metaKey = true;
//                }).selectable({filter: "td.av-hotel"});
                $('td.ch-hotel').filter(function() {
                    return !$(this).next('td').hasClass('ch-hotel')
                }).html("<img src='../static/img/plan/bkg.png' alt='room-img' style='width:18px;height:18px;' />");
                $('td.ch-hotel').filter(function() {
                    return !$(this).next('td').hasClass('ch-hotel')
                }).prev().html("<img src='../static/img/plan/ea.png' alt='room-img' style='width:18px;height:18px;' />");
            });
        });

        //$('td.av-hotel').filter(function(){return !$(this).prev('td').hasClass('av-hotel')}).html("<img src='../static/img/plan/hotel.png' alt='room-img' style='width:24px;height:24px;' />");
        //$(".availability_calender tbody").customScrollbar({preventDefaultScroll: true});
    });
</script>
