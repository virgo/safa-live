<script src="//ajax.googleapis.com/ajax/libs/jquery/1.6.1/jquery.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?AIzaSyC794uPScLtRUa8Uwk9nX8RCjrepoxszxg&sensor=true"></script>
<script src="http://google-maps-utility-library-v3.googlecode.com/svn/trunk/infobubble/src/infobubble.js"></script>
<div id="map" style="width:100%;height:100%" >
</div>
<div style="display: none;" id="hotel_data">
    <table style="width: 100%" dir="rtl">
        <tr>
            <td valign="top" >
                <img style="width:190px;height: 100px;" src="<?= base_url("static/hotel_album/" . $hotel->erp_hotel_id . '/' . get_h_img($hotel->erp_hotel_id)) ?>"> 
            </td>
            <td valign="top">
                <h1 style="margin: 5px 0 0"><?= $hotel->hotel_name ?>
                <? for($i=0;$i <$hotel->erp_hotellevel_id;$i++) :?>
                    <img src="<?= IMAGES ?>/imgs/star.png" alt="*" />
                    <? endfor ?>
                </h1>
                <div class="row-fluid">
                    <span style="padding: 0 5px;"> <?= $hotel->city_name ?></span>
                </div>
                <div class="row-fluid">
                    <span style="padding: 0 5px;"> <?= $hotel->{"address_".name(TRUE)} ?></span>
                </div>
                <? if (strlen($hotel->distance)) : ?>
<div>
                            <span ><?= lang('distance_from') ?></span>
                            <span style="padding: 0 5px;"><?= $hotel->distance ?></span>
</div>
                <? endif ?>
            </td>
        </tr>
    </table>
</div>
<script>
    function initialize(lat, long) {
        var myLatlng = new google.maps.LatLng(lat, long);
        var mapOptions = {
            zoom: 17,
            center: myLatlng,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        }
        var map = new google.maps.Map(document.getElementById('map'), mapOptions);
        var marker = new google.maps.Marker({
            position: myLatlng,
            map: map,
        });

        var infowindow = new InfoBubble({
            content: $('#hotel_data').html(),
            width: 500
        });
        google.maps.event.addListener(marker, 'click', function() {
            infowindow.open(map, marker);
        });

        infowindow.open(map, marker);

    }




</script>
<script type="text/javascript">
    window.onload = function() {
        initialize('<?= $hotel->googel_map_latitude ?>', '<?= $hotel->googel_map_langtitude ?>');
    }


</script>
