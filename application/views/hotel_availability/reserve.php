<link href="<?= NEW_CSS_JS . '_' . lang('DIR') ?>/common-css.css" rel="stylesheet" type="text/css" />
<style>
    .modal-body {
        margin: 0 !important;
        text-align: right;
        height: 92%;
        width: 95%;
        overflow-y: auto;
        overflow-x: hidden; 
        min-width: 600px;
    }
    h4{
        margin-top: 0px;
    }
</style>
<div class="modal-body" id="reserve-div" >
        <form name="doreserve" method="post" action="<?= site_url("hotel_availability/reserve/do") ?>">
    <div class="modal-headar">
        <div class="wizerd-div"><a class="text-warning" style="margin-top: 18px; margin-right: 10px;"> <?= lang('ha_reservation_info') ?></a></div>
    </div>
    <? foreach ($results as $result) : ?>
        <div class=""style="border-radius: 4px;border: 1px solid #dedede; width: auto;padding: 2%;overflow: hidden;margin: 5% auto;">
            <div class="row-fluid">
                <div class="span4">
                    <div class="span12">
                        <div class="span4"><h4><?= lang('ha_city') ?></h4></div>
                        <div class="span8"><?= $result['availability']->city ?></div>   
                    </div>
                    <div class="span12">
                        <div class="span4"><h4><?= lang('ha_hotel_name') ?></h4></div>
                        <div class="span8"><?= $result['availability']->hotel ?></div>    
                    </div>
                </div>
                <? if ($co_type == $result['availability']->erp_company_type_id && $co_id == $result['availability']->safa_company_id) : ?>
                    <div class="span8">
                        <div class="row-fluid">
                            <div class="span4">
                               <?= form_dropdown('reservetype['.$result['availability']->erp_hotels_availability_master_id .']',
                    array('1' => lang('ha_status_1'), '2' => lang('ha_status_2'), '3' => lang('ha_status_3')),null," class='reservetype' ") ?>
                            </div>
<!--                            <div class="span8"><input placeholder="<?= lang('ha_reserve_ex_date') ?>" name='reserveorderv[<?= $result['availability']->erp_hotels_availability_master_id ?>]' style="display: none;" class="datepicker reserveorderv" /></div>-->
                        </div>
                        <div class="row-fluid">
                        <?= lang('ha_owner') ?>
                            <div class="span12">
                                <div class="span6">
                                    <input value="1" type="radio" name="customertype[<?= $result['availability']->erp_hotels_availability_master_id ?>]" checked="checked" onclick="$('#systemcustomer<?= $result['availability']->erp_hotels_availability_master_id ?>').hide();" /><?= lang('ha_out_customer') ?>
                                </div>
                                <div class="span6"><input value="2" type="radio" name="customertype[<?= $result['availability']->erp_hotels_availability_master_id ?>]" onclick="$('#systemcustomer<?= $result['availability']->erp_hotels_availability_master_id ?>').show();" /><?= lang('ha_sys_customer') ?></div>
                            </div>
                            <div class="span12" id="systemcustomer<?= $result['availability']->erp_hotels_availability_master_id ?>" style="display: none;">
                                <div class="span6"><?= form_dropdown('systemcustomertype['.$result['availability']->erp_hotels_availability_master_id .']',
                    array('2' => lang('ha_uo_companies'), '3' => lang('ha_ea_companies')),null," class='systemcustomertype'") ?></div>
                                <div class="span6"><?= form_dropdown('systemcustomerid['.$result['availability']->erp_hotels_availability_master_id .']',$companies,null,'class="systemcustomerid"') ?></div>
                            </div>
                        </div>
                    </div>
                        
                <? endif ?>
            </div>

            <table>
                <thead>
                    <tr>
                        <th><?= lang('ha_reserve_type') ?></th>
                        <th><?= lang('ha_room_type') ?></th>
                        <th><?= lang('ha_room_price') ?></th>
                        <th><?= lang('ha_available_count') ?></th>
                        <th><?= lang('ha_request_count') ?></th>
                    </tr>
                </thead>
                <tbody>
                <? foreach ($result['avdetails'] as $detail) : ?>
                    <tr>
                        <td><?= $detail['details']->housingtype ?></td>
                        <td><?= $detail['details']->roomsize ?></td>
                        <td><?= $detail['details']->sale_price ?></td>
                        <td><?= $detail['available'] ?></td>
                        <td><?= $detail['count'] ?></td>
                    </tr>
                <? endforeach; ?>
                </tbody>
            </table>
        </div>
    <? endforeach; ?>
        <input type="hidden" value='<?= $datas ?>' name="roomcount" />
        <input type="hidden" value='<?= $fromdate ?>' name="fromdate" />
        <input type="hidden" value='<?= $todate ?>' name="todate" />
        <input class="btn btn-primary" name="submit" value="<?= lang('ha_reserve_do') ?>" type="submit">
    </form>

    <script type="text/javascript">
        $(function() {
            $('form[name=doreserve]').submit(function() {
                $.post("<?= site_url("hotel_availability/reserve/do") ?>", $(this).serialize(), function(data) {
                    $('#reserve-div').html(data);
                });
            });
            
//            $('.reservetype').change(function(){
//                var restype = $(this).val();
//                var reservevd = $(this).parent().parent().find('.reserveorderv');
//                if(restype == 2) reservevd.css('display','inline');
//                else reservevd.css('display','none');
//            });
//            
//            $('.reserveorderv').datepicker({dateFormat: 'yy-mm-dd'});
            
            $('.systemcustomertype').change(function(){
                var ctype = $(this).val();
                var companiesdd = $(this).parent().parent().find('.systemcustomerid');
                companiesdd.html('');
                $.post('<?= site_url('hotel_availability/ajax_get_companies') ?>',{'type':ctype},function(data){
                    $.each(data,function(idx,optn){
                        companiesdd.append('<option value="'+optn.id+'">'+optn.name+'</option>')
                    });
                },'JSON');
            });
        });
    </script>
</div>