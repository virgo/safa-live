<div class="row-fluid">
    <div class="span12"><label class="text-warning"><?= $hotel->hotel_name ?></label></div>
    <div class="span12">
        <div class="span7"><span class="span11 thumbnail border"><img src="<?= base_url("static/hotel_album/" . $hotel->erp_hotel_id . '/' . get_h_img($hotel->erp_hotel_id)) ?>" /></span></div>
        <div class="span5">
            <div class="span12 text-warning"><?= lang('ha_about_hotel') ?></div>
            <div class="span12"><?= $hotel->remark ?></div>
            <div class="span12 text-warning"><?= lang('ha_level') ?></div>
            <div class="span12 rates">
                <? for($i=0;$i <$hotel->erp_hotellevel_id;$i++) :?>
                    <span class="icon-star"></span>
                    <? endfor ?>
            </div>
            <div class="span9 text-warning"><a  class="fancybox fancybox.iframe" href="<?= site_url('hotel_availability/map/'.$hotel->erp_hotel_id) ?>"><?= lang('ha_map_place') ?><span class=" icon-map-marker icon-2x pull-left"></span></a></div>
            <? if($hotel->distance) :?>
            <div class="span12 "><div class="span8 text-warning"><?= lang('ha_distance_from_haram') ?></div><div class="span3"><?= $hotel->distance ?></div></div>
            <? endif ?>
        </div>
    </div>
</div>