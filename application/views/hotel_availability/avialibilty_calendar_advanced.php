<style>
    table tr th{background-color: #f1f1f1;}
    table td, table th{padding:1px!important;}
    th label,td label {
        display: inline-block!important;
        margin: 1px 0!important;
    }
    td table{
        border: none;

    }
    td {
        padding: 0!important;
    }
    table tr td{
        padding: 4px!important;
        border-bottom: 1px solid #ddd!important;
        background-color: #fff;
        /*        border-right: 1px solid #ddd!important;*/
    }.rotate{background-color: #fff;}
    .rotate label {-webkit-transform: rotate(-90deg); 
                   -moz-transform: rotate(-90deg);margin: 10px 0!important;
    }

    .av-hotel,.ch-hotel,.cl-hotel{min-height: 20px;min-width: 20px;box-shadow: none;border-left: 0;}
    /*    .av-hotel:hover,.ch-hotel:hover,.cl-hotel:hover{box-shadow:0px -2px 12px 0px rgba(0,0,0,.14),0px 2px 12px 0px rgba(0,0,0,.14);}*/
    .av-hotel img,.ch-hotel img,.cl-hotel img{cursor: pointer;}
    .av-hotel{background: #B8D776;}
    .ch-hotel{background-color:#DE856D;}
    /*    .av-hotel:hover,.ch-hotel:hover,.cl-hotel:hover{
                        -webkit-transition: all .4s ease;
        -moz-transition: all .4s ease;
        -o-transition: all .4s ease;
        transition: all .4s ease;}*/
    .cl-hotel{background-color:#918183;}
    .plan {float: left;}


    .hotel{}
    .group{}

    label{margin: 0;padding: 5px;}
    .row-form > [class^="span"]{border: none;}

    .table-warp .box-shadow-last  {box-shadow: -12px 0 8px -4px rgba(0,0,0, 0.3);}
    .table-warp .box-shadow-first {box-shadow: 12px 0 15px -4px rgba(0,0,0, 0.3)}

    .table-warp th label {
        font-family: tahoma;
        font-size: 10px;
        font-weight: 900;
    }

    .calendars-month-header select{
        padding: 0;
    }

</style>
<link href="<?= base_url("static/js/jquery.calendars/jquery.calendars.picker.css") ?>" type="text/css" rel="stylesheet">
<link href="<?= base_url("static/js/jquery.calendars/humanity.calendars.picker") ?>" type="text/css" rel="stylesheet">
<script src="<?= base_url("static/js/jquery.calendars/jquery.calendars.js") ?>"></script>
<script src="<?= base_url("static/js/jquery.calendars/jquery.calendars-ar.js") ?>"></script>
<script src="<?= base_url("static/js/jquery.calendars/jquery.calendars.plus.js") ?>"></script>
<script src="<?= base_url("static/js/jquery.calendars/jquery.calendars.picker.js") ?>"></script>
<script src="<?= base_url("static/js/jquery.calendars/jquery.calendars.picker-ar.js") ?>"></script>
<script src="<?= base_url("static/js/jquery.calendars/jquery.calendars.islamic.js") ?>"></script>
<script src="<?= base_url("static/js/jquery.calendars/jquery.calendars.islamic-ar.js") ?>"></script>

<div class="widget">
    <div class="path-container Fright">
        <div class="path-name Fright">
            <a href=""><?= lang('global_system_management') ?></a>
        </div>
        <div class="path-arrow Fright">
        </div>
        <div class="path-name Fright">
            <a href=""><?= lang('menu_main_hotels') ?></a>
        </div>
        <div class="path-arrow Fright">
        </div>
        <div class="path-name Fright">
            <?= lang('availabilty_detail') ?>
        </div>
    </div>
</div>

<div class="widget">
    <div class="widget-header">
        <div class="widget-header-icon icon-pencil pull-right"></div>
        <div class="widget-header-title pull-right"><?= lang('availabilty_detail') ?></div>
    </div>
    <div class="widget-container">
        <form method="post">
            <div class="row-form" style="border: none;">
                <div class="span3">
                    <div class="span3"><label><?= lang('ha_city_id') ?></label></div>
                    <div class="span9"><?= form_dropdown('city_id', $cities, set_value('city_id'), 'class="validate[required] input-full chosen-rtl" style="width:100%;" id="city_id"') ?></div>
                </div>
                <div class="span3">
                    <div class="span3"><label><?= lang('hotel') ?></label></div>
                    <div class="span9"><?= form_multiselect('hotels[]', $hotels, set_value('hotels', ' '),
                    'class="hotels chosen-rtl"', 'style="width:100%!important;" placeholder=" "') ?></div>
                </div>
                <div class="" style="float:right;margin-right:35px">
                    <label class="pull-right" style="width: 34px; height: 21px;">هــ<input onclick="$('.date').calendarsPicker('destroy').val('').calendarsPicker({calendar: $.calendars.instance('islamic', 'ar'), dateFormat: 'dd-mm-yyyy'});" checked="checked" class="pull-right" style="height: 25px; margin-top: 0px;" name="datetype" type="radio" value="H"/></label>
                    <label class="pull-right"  style="clear: both;">م<input onclick="$('.date').calendarsPicker('destroy').val('').calendarsPicker({calendar: $.calendars.instance('gregorian', 'en'), dateFormat: 'dd-mm-yyyy'});" class="pull-right" name="datetype" type="radio" value="G" /></label></div>
                <div class="span5">
                    <div class="span2"><label><?= lang('datefrom') ?></label></div>
                    <div class="span4"><input class="input-huge date" name="datefrom" type="text"  value="<?= set_value('datefrom') ?>"></div>
                    <div class="span1"><label><?= lang('dateto') ?></label></div>
                    <div class="span4"><input class="input-huge date" name="dateto" type="text" value="<?= set_value('dateto') ?>"></div>
                </div>

            </div>
            <div class="span1"><input name="filter" type="submit" class="btn" value="<?= lang('search') ?>" /></div>
        </form>
        <div id="results">
<? if (isset($startdate)) : ?>
                <div class="nav">
                    <div id="nextdate" style="cursor: pointer;" alt="<?= date('Y-m-d', strtotime($startdate) + (3600 * 24 * 15)) ?>" class="Fleft icon-arrow-left" >  </div>
                    <div id="prevdate" style="cursor: pointer;" alt="<?= date('Y-m-d', strtotime($startdate) - (3600 * 24 * 15)) ?>" class="Fright icon-arrow-right" > </div>
                </div>
                <div style="clear: both;"></div>
            <? endif ?>
            <? if ($this->input->post() || isset($results)) : ?>
    <? foreach ($results as $hid => $result) : ?>
                    <div class="table-warp">
                        <table width="100%" border="0">
                            <tr>
                                <th colspan="2" rowspan="2" bgcolor="#FFFFFF" scope="col"><?= $result['hotel']->{name()} ?></th>
                                <th scope="col">م</th>
                                <? for ($datetime = strtotime($startdate); $datetime <= strtotime($enddate); $datetime += (3600 * 24)) : ?>
                                    <th scope="col"><?= date('d', $datetime) ?></th>
        <? endfor ?>
                            </tr>
                            <tr>
                                <th>هـ</th>
                                    <? for ($datetime = strtotime($startdate); $datetime <= strtotime($enddate); $datetime += (3600 * 24)) : ?>
                                    <th scope="col"><?= $this->hijrigregorianconvert->ConstractDay($this->hijrigregorianconvert->GregorianToHijri(date('d-m-Y',
                                                                $datetime), 'DD-MM-YYYY'), 'YYYY-MM-DD')
                                        ?></th>
                            <? endfor ?>
                            </tr>
        <? foreach ($result['rooms'] as $floor) : ?>
            <? if (!count($floor['room'])) continue; ?>
                                <tr>
                                    <th width="1%" rowspan="5" class="rotate"><label><?= $floor['floor']->{name()} ?></label></th>
                                    <th rowspan="<?= count($floor['room']) ?>"><img src="../static/img/roomx.png" alt="room-img" />      <label><?= $floor['floor']->erp_hotelroomsize_id ?></label></th>
                                    <? foreach ($floor['room'] as $key => $value) : ?>
                                        <td><?= $key ?></td>
                                        <? for ($datetime = strtotime($startdate); $datetime <= strtotime($enddate); $datetime += (3600 * 24)) : ?>
                                            <td <? if (isset($value[date('d', $datetime)])) : ?>class="<? if ($value[date('d', $datetime)]) : ?>av-hotel<? else : ?>cl-hotel<? endif ?>"<? endif ?> ></td>
                                        <? endfor ?>
                                    </tr><tr>
            <? endforeach ?>
        <? endforeach ?>
                            </tr>
                        </table>
                    </div>
            <? endforeach ?>
        <? endif ?>
        </div>
<? if ($this->input->post() && isset($results)) : ?>
            <div class="row-fluid ">
                <div class="span4">
                    <div class="span1 av-hotel">
                    </div>
                    <div class="span2 "><label><?= lang('available') ?></label></div>
                    <div class="span1 ch-hotel"></div>
                    <div class="span2"><label><?= lang('disavailable') ?></label></div>
                    <div class="span1 cl-hotel"></div>
                    <div class="span2"><label><?= lang('closed') ?></label></div>
                </div>
                <div class="span4">
                    <div class="span1"><img src='../static/img/plan/ea.png' alt='room-img' style='width:24px;height:24px;' /></div>
                    <div class="span6"><label><?= lang('client') ?></label></div>
                    <div class="span1"><img src='../static/img/plan/bkg.png' alt='room-img' style='width:24px;height:24px;' /></div>
                    <div class="span3"><label><?= lang('pickpakage') ?></label></div>
                </div>
                <div class="span4">

                    <div class="span1"><img src='../static/img/plan/hotel.png' alt='room-img' style='width:24px;height:24px;' /></div>
                    <div class="span3"><label><?= lang('pickroom') ?></label></div>

                    <div class="span1"><img src='../static/img/plan/group.png' alt='room-img' style='width:24px;height:24px;' /></div>
                    <div class="span7"><label><?= lang('group_detail') ?></label></div>
                </div>
            </div>
<? endif ?>
    </div>
</div>
<script type="text/javascript">
    $(function() {
        $('.date').calendarsPicker({calendar: $.calendars.instance('islamic', 'ar'), dateFormat: 'dd-mm-yyyy'});
        $('.hotels').chosen();

        $("table .ch-hotel").append($("<div class='plan'><a href='#' title='clint details'><img src='../static/img/plan/ea.png' alt='room-img' style='width:24px;height:24px;' /></a><a href='#' title='packege details'><img src='../static/img/plan/bkg.png' alt='room-img'  style='width:24px;height:24px;'/></a></div>"));
        $("table .hotel").append($("<a href='#' title='room reservesion'><img src='../static/img/plan/hotel.png' alt='room-img' style='width:24px;height:24px;' /></a>"));
        $("table .group").append($("<a href='#' title='person who reserved this room'><img src='../static/img/plan/group.png' alt='room-img' style='width:24px;height:24px;' /></a>"));
        $('tr').hover(function() {
            $(this).children("td:last").toggleClass("box-shadow-last", 500, "easeOutSine");
            $(this).children("td:first").toggleClass("box-shadow-first", 500, "easeOutSine");
        });
        $("table").tooltip();

        $('#results').on('click', '#nextdate', function() {
            $.post('<?= site_url('hotel_availability/ajax_calendar') ?>', {datetype: 'G', datefrom: $(this).attr('alt'), hotels: $('.hotels').val()}, function(data) {
                $('#results').html(data);
            });
        });

        $('#results').on('click', '#prevdate', function() {
            $.post('<?= site_url('hotel_availability/ajax_calendar') ?>', {datetype: 'G', datefrom: $(this).attr('alt'), hotels: $('.hotels').val()}, function(data) {
                $('#results').html(data);
            });
        });
    });
</script>