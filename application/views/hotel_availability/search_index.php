<style>
    .formError {
        position: inherit !important;
    }
</style>
<div class="widget">
    <a class="btn Fleft" href="<?= site_url('hotel_availability/manage') ?>">
        <?= lang('global_add') ?>
    </a>
    <div class="path-container Fright">
        <div class="path-name Fright">
            <a href="<?= site_url() ?>"><?= lang('global_system_management') ?></a>
        </div>
        <div class="path-arrow Fright"></div>
        <div class="path-name Fright"><?= lang('menu_hotel_availabilty_search') ?></div>
    </div>
</div>

<? $this->load->view('hotel_availability/search') ?>

<form id="send_search" action="<?= current_url() ?>" method="post">
    <input type="hidden" name="date_from" value="<?= set_value('date_from') ?>" id="date_from" />
    <input type="hidden" name="date_to" value="<?= set_value('date_to') ?>" id="date_to" />
    <div class="widget">
        <div class="widget-header">
            <div class="widget-header-icon Fright">
                <span class="icos-pencil2"></span>
            </div>
            <div class="widget-header-title Fright">
                <?= lang('ha_hotel_availability_search_options') ?>
            </div>
        </div>

        <div class="widget-container">
            <div class="">
                <table class="" cellpadding="0" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th width="90%"><?= lang('ha_search') ?></th>
                            <th width="10%"><?= lang('ha_operations') ?></th>
                        </tr>
                    </thead>
                    <tbody id="search_append">
                    </tbody>
                </table>
            </div>
            <div class="row-form" align="center">
                <input type="submit" class="btn" value="<?= lang('ha_search') ?>" style="margin:10px;padding: 5px;height: auto" />
            </div>
        </div>
    </div>

</form>
<script>
    $('.date_from').datepicker({
        dateFormat: "yy-mm-dd",
        controlType: 'select',
        timeFormat: 'HH:mm',
        onClose: function(selectedDate) {
            $(".date_to").datepicker("option", "minDate", selectedDate);
        }
    });
    $('.date_to').datepicker({
        dateFormat: "yy-mm-dd",
        controlType: 'select',
        timeFormat: 'HH:mm',
        onClose: function(selectedDate) {
            $(".date_from").datepicker("option", "maxDate", selectedDate);
        }
    });
    $('.chosen-rtl').chosen();
    var counter = 0;

    $('.add_search').click(function() {
        if (!$("#search").validationEngine('validate'))
            return false;

        counter++;
        $('#send_search').find('tr[class="odd"]').remove();
        var cityname = $('#city_id option:selected').text();

        var hotelsnames = [];

        $('#hotel_id option:selected').each(function(inx, ele) {
            hotelsnames[inx] = $(ele).text();
        });
        var datefrom = $('#search input[name=date_from]').val();
        var dateto = $('#search input[name=date_to]').val();

        var searchname = '';

        if (cityname.length)
            searchname += '<?= lang('ha_city_id') ?>: ' + cityname + ' / ';
        if (hotelsnames.length)
            searchname += '<?= lang('ha_hotel_id') ?>: ' + hotelsnames.join(',') + ' / ';
        if (datefrom.length)
            searchname += '<?= lang('ha_date_from') ?>: ' + datefrom + ' <?= lang('ha_date_to') ?>: ' + dateto;

        form_data = $('#search').serialize();
        console.log(form_data);
        var new_row = function() {/* <tr rel="{$id}">  
                            <td>{$searchname} <input type="hidden" name="search[{$id}]" value="{$form_data}" /></td>
                            <td><a href="javascript:void(0)" onclick="remove_search({$id})"><span class="icon-trash"></span></a></td>
                        </tr>
        */
        }.toString().replace(/{\$id}/g, counter).replace(/{\$searchname}/g, searchname).replace(/{\$form_data}/g, form_data).slice(14, -3);
        console.log(new_row);
        $('#search_append').append(new_row);
    });

    function remove_search(id) {
        $('#search_append').find('tr[rel="' + id + '"]').remove();
    }

    // UPDATE HOTELS
    function update_hotels()
    {
        city_id = $('#city_id').val();
        $.post('<?= site_url('hotel_availability/ajax_get_hotels') ?>', {city_id: city_id, no_select: true}, function(data) {
            $('#hotel_id').html(data);
            $('#hotel_id').trigger('chosen:updated');
        });
    }
    $('#city_id').change(function() {
        update_hotels();
    });
    $('.date_from').change(function() {
        $('#date_from').val($('.date_from').val());
    });
    $('.date_to').change(function() {
        $('#date_to').val($('.date_to').val());
    });
</script>
