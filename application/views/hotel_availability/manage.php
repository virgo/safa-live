<style>
    .small{
        width: 60%;
    }
    .har_sub{
        width: 100%;
        position: fixed;
        display: none;
        left: 50%;
        top: 50%;
/*        margin-left: 150px;
        margin-top: 150px;*/
    }
    .har_sub .widget{
        /*z-index: 30000;*/
        /*opacity: 1;*/
    }
    
    .table-container {
        padding-bottom: 35px;
    }
    
    .table-container table td,.table-container table th {
        padding: 5px;
    }
    
    .dropdown{
        position: relative;
    }
    .my_shadow{
        position: fixed; 
        z-index: -100;
        opacity: 0.3; 
        background: #000; 
        width: 100%; 
        height: 100%; 
        top:0; 
        left:0; 
        right:0; 
        bottom: 0;
    } 
    .chosen-container{
        margin-bottom: 11px;
    }
    .row-fluid [class*="span"]{line-height: 30px;}
   .widget{width:98%;}

</style>
<div class="widget">
    <div class="path-container Fright">
        <div class="path-name Fright">
            <a href="">
                <?= lang('global_system_management') ?> 
            </a>
        </div>
        <div class="path-arrow Fright">
        </div>
        <div class="path-name Fright">
           
        </div>
    </div>
</div>
<div class="widget">
    <div class="widget-header">
        <div class="widget-header-icon pull-right"><span class="icon icon-pencil"></span></div>
        <div class="widget-header-title pull-right"> <?= lang('ha_register_title') ?></div>
        
        
    </div>
    <div class="widget-container">
        
        <style>    
.wizerd-div {
    border-bottom: 1px solid #FFFFFF;
    margin: -33px 0 50px;
    padding-top: 0;
}
</style>
<?= form_open_multipart(false, 'id="wizard_validate"') ?>
<!-- ROOMS -->
<fieldset title="<?= lang('ha_step1') ?>">
    <legend><?= lang('ha_step1') ?></legend>
<? $this->load->view('hotel_availability/widgets/main') ?>
</fieldset>
<fieldset title="<?= lang('ha_step2') ?>">
    <legend><?= lang('ha_step2') ?></legend>
<!-- ROOMS -->
<? $this->load->view('hotel_availability/widgets/rooms') ?>
</fieldset>
<fieldset title="<?= lang('ha_step3') ?>">
    <legend><?= lang('ha_step3') ?></legend>
<!-- DETAILS -->
<? $this->load->view('hotel_availability/widgets/details') ?>
</fieldset>
<fieldset title="<?= lang('ha_step4') ?>">
    <legend><?= lang('ha_step4') ?></legend>
<!-- ADDITIONAL OPTIONS -->
<? $this->load->view('hotel_availability/widgets/additional_options') ?>
</fieldset>
<?= form_close() ?>
<div class="footer"></div>
    </div>
    
</div>

<?// $this->load->view('hotel_availability/widgets/popup') ?>
<script type="text/javascript">
$(document).ready(function(){
    $("#wizard_validate").validationEngine('attach',{prettySelect : true, useSuffix: "_chosen", promptPosition : "topRight:-150"});
    $("#wizard_validate").formToWizard({
//            submitButton: '<?= lang('ha_save') ?>',
              callback: function() {
                if( ! $("#wizard_validate").validationEngine('validate')) 
                    return false;
                return true;
              }
        });
        
        $('.validate').blur(function(){
            alert($(this).val());
            if($(this).val().length) {
                $($(this).attr('id')+'formError').fadeOut();
            }
        });

    var config = {
        '.chosen-select': {width: "100%"},
        '.chosen-select-deselect': {allow_single_deselect: true},
        '.chosen-select-no-single': {disable_search_threshold: 10},
        '.chosen-select-no-results': {no_results_text: 'Oops, nothing found!'},
        '.chosen-select-width': {width: "95%"}
    }
    for (var selector in config) {
        $(selector).chosen(config[selector]);
    }
    $("#country_id").chosen();
    $("#city_id").chosen();
    $("#hotel_id").chosen();
    $("#season_id").chosen();

    // binds form submission and fields to the validation engine
//    $("#form").validationEngine();//{
//            prettySelect : true,
//            useSuffix: "_chosen",
//            promptPosition : "topRight:-150"
//            //promptPosition : "bottomLeft"
//    });

    // UPDATE CITIES
    function update_cities( id )
    {
        $.post('<?= site_url('hotel_availability/ajax_get_cities') ?>', {country_id: $('#country_id').val(), id: id}, function(data){
            $('#city_id').html(data);
            $('#city_id').trigger('chosen:updated');                
        });
    }
    update_cities(<?= set_value('city_id', $main->erp_city_id) ?>);
    $('#country_id').change(function(){
        update_cities();            
    });
    
    // UPDATE HOTELS
    function update_hotels( city_id, id )
    {
 
        $.post('<?= site_url('hotel_availability/ajax_get_hotels') ?>', {city_id: city_id, id: id}, function(data){
            $('#hotel_id').html(data);
            $('#hotel_id').trigger('chosen:updated'); 
        });
    }
    
    update_hotels(<?= $main->erp_city_id ?>, <?= ($main->erp_hotel_id)?$main->erp_hotel_id:"''" ?> );    
    
    $('#city_id').change(function(){
        
        update_hotels($('#city_id').val(),'');            
    });
    
    $('body').on('change','#hotel_id',function(){
        var hotelid = $(this).val();
        $.get('<?= site_url('hotel_availability/hotel/') ?>/'+hotelid,function(data){
            $('#hotel_data').html(data);
            $(".fancybox").fancybox();
        });
        
        
    });
    
    
    
});

// VALIDATION
//    function valid_datetime(field, rules, i, options){
//            if (field.val() != "HELLO") {
//                    // this allows to use i18 for the error msgs
//                    return options.allrules.validate2fields.alertText;
//            }
//    }
</script>
