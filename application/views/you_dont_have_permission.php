<!DOCTYPE html>
<html>
    <head>
        <title>login</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width">
        <link href="<?= base_url() ?>/static/new_template/css-js_rtl/common-css.css" rel="stylesheet" type="text/css" />
        <style>
            a{
                margin: 5px;
            }
            .reg-btn {
                padding: 2px 8px !important;
                color: #000333;
                background: #ddc889;
            }
            .logo2 {
                display: block;
                text-align: center;
                margin: 0 auto;
            }
            .banner2 {
                background-image: url("<?= base_url() ?>/static/front/banner2.png");
                background-repeat: no-repeat;
                height: 260px;
                margin: 0 auto;
                position: relative;
                width: 640px;
                margin-top: 10px;
            }
            .forg {
                margin: 0 auto !important;
                float: none !important;
                overflow: hidden;
            }
            .reg2 {
                margin-top: 70px;
                margin-bottom: 70px;
            }
            .reg-btn2 {
                padding: 4px 10px !important;
                color: #000333;
                background: #ddc889;
                text-shadow: none;
                margin-top: 2px !important;

            }
            .log {
                margin-right: 10px;
                margin-top: -110px;
            }

            input, textarea, .uneditable-input{margin-bottom: 3px;}
            .wizerd-div {
                border-bottom: medium none !important;
                margin: -18px 0 20px;
                padding-top: 0;
            }
            .wizerd-div a {
                background: none repeat scroll 0 0 #FAFAFA;
                border: 1px solid #D5D6D6;
                border-radius: 49px;
                color: #663300;
                display: inline-block;
                margin: -23px 5px -17px -8px;
                padding: 39px 12px 13px;
                width: 90%;
                text-align: center;
            }
            a {
                color: #C09853;
            }
            .resalt-group2 {
                -moz-box-sizing: border-box;
                border: 2px solid #E1E1E1;
                border-radius: 20px;
                float: right;
                margin: 1% 2%;
                padding: 2%;
                width: 96%;

            }
            .pad {padding: 10px;}
            .t2 {font-size: 16px;
                 font-weight: 500;
                 margin-top: 35px;
                 color: #444343;

            }
            .f1 {margin-bottom: 15px;
                 margin-top: 10px;
                 font-size: 13px;
                 font-weight: 500;
                 color: #88774A;
            }
            .false {
                color: #800000;
                margin-bottom: -15px;
                margin-top: 15px;
            }
            .wizerd-div a h3 {
                padding: -10px !important;
                margin: 0px;
                font-weight: 600;
                font-size: 17px;
                color: #8B742B;
            }
        </style>
    </head>
    <body style="padding: 10px;">
        <div class="row-fluid">
            <div class="span12 pad reg2" style="background-color: rgba(240, 240, 240, 0.63);">
                <div class="span12" style="border-radius: 30px 0px 35px 0px;background-color: rgba(219, 206, 179, 0.63);">
                    <div class="span12 TAC" style="border-top-left-radius: 30px;background-color: rgba(189, 185, 178, 0.55);"><h3 style="text-shadow: 1px 0px 6px black;font-size: 20px;color: white;padding-top: 3px;padding-right: 5px;"><?= lang('global_system_message') ?></h3></div>
                    <div class="span12  ">
                        <div class="span12  TAC">
                            <p class="TAC" style="color: chocolate;font-size: 15px;padding: 20px;"><?= lang('global_you_dont_have_permission') ?></p>
                        </div>
                    </div>
                </div>
                <div class="span12 TAC" style="padding-top: 10px;">
                    <a href="<?= site_url() ?>" 
                       class="btn reg-btn2" pmbx_context="8C529BC9-926F-425E-9040-E3D26022E694" 
                       style="font-size: 15px !important;width: 170px;"><?= lang('global_back') ?></a>
                </div>
            </div>
        </div>
    </body>
</html>
