<!doctype html>
<html>
    <head>
        <meta content="text/html; charset=utf-8" />
        <title><?= lang('global_title') ?></title>
        <style>
            .t-right { text-align:right;}
            .pull-right{float:right;}
            .content {margin:0 auto; width:auto; display:block;}
        </style>
    </head>
    <body>
        <div style="margin:0 auto; width:auto; display:block;">
            <table border="0" align="center" width="588">
                <tbody>
                    <tr align="center">
                        <td height="76" colspan="2"><img src="<?= base_url() ?>static/new_template/contract_email_form/img/Safa-ContractMail-Final_03.png"></td>
                    </tr>
                    <tr align="center">
                        <td height="81" colspan="2"><img src="<?= base_url() ?>static/new_template/contract_email_form/img/Safa-ContractMail-Final_07.png"></td>
                    </tr>
                    <tr align="center">
                        <td height="190" colspan="2">
                            {content}
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <p style="color: rgb(255, 255, 255); padding:10px; margin-top: 0px; background-color: rgb(33, 31, 31); font-weight: bolder; text-align:right;">
                                للدخول الى نظام صفا يرجى الضغط
                                <a href="<?= site_url() ?>">هنا</a>
                                او نسخ الرابط التالي ولصقه في المستعرض 
                                <br />
                                <?= site_url() ?>
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <p style="padding: 10px; margin: -16px 10px 0px; color: rgb(129, 106, 61); font-weight: bolder; text-align:right;">
                                في حال وجود اي استفسار حول استخدام نظام صفا يرجى الدخول الى مركز الدعم الفني 
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2"> </td>
                    </tr>
                    <tr>
                        <td width="298">
                            <h4 style="margin: 5px; padding: 5px; background-color: rgb(174, 161, 138); color: rgb(255, 255, 255);">
                                Need help? We've got you!
                            </h4>
                        </td>
                        <td width="302">
                            <h4 style="background-color: rgb(174, 161, 138); color: rgb(255, 255, 255); padding: 5px; margin: 5px;">
                                Connect with us
                            </h4>
                        </td>
                    </tr>
                    <tr>
                        <td style="padding: 5px;">
                            <p style="margin-top: 0px; padding-left: 5px; padding-right: 5px; color: rgb(34, 31, 31);">
                                isit Safa's community space to see our FAQs,
                                get advice from other small business owners,
                                or ask an accountant a question.
                            </p>
                            <button style="margin-bottom: 10px; border-radius: 8px 17px 2px; padding: 6px; color: rgb(128, 105, 59);" class=" btn btn-round btn-default">GO</button>
                        </td>
                        <td style="padding: 5px;">
                            <p style="padding-left: 5px; padding-right: 5px; margin-top: -15px;">
                                <img src='<?= base_url() ?>static/new_template/contract_email_form/img/Safa-ContractMail-Final_11.png'  style="float:right;" />
                                Get Safa updates, small business
                                tips and stay in touch with us.
                            </p>
                            <a href="#"><img src="<?= base_url() ?>static/new_template/contract_email_form/img/icon_02_facebook.png"></a>
                            <a href="#"><img src="<?= base_url() ?>static/new_template/contract_email_form/img/icon_02_twitter.png"></a>
                            <a href="#"><img src="<?= base_url() ?>static/new_template/contract_email_form/img/icon_02_linkedin.png"></a>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <p style="background-color:#221f1f; color:#fff; padding:10px;">
                                We are located at 31 El Nasr St. New Maadi, Egypt. You received this message because this email was used to
                                create an account at Safa. Ensure delivery of future emails by adding <?php if(isset($template_footer_email)) {echo $template_footer_email;} else {?>contracts@umrah-booking.com<?php }?> to your
                                address book, or unsubscribe.
                            </p>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </body>
</html>