<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); ?>
<!--<link href="<?= CSS ?>/stylesheets.css" rel="stylesheet" type="text/css" />
<? if (lang('global_lang') == 'ar'): ?>
    <link href="<?= CSS ?>/stylesheet-ar.css" rel="stylesheet" type="text/css" />
<? else: ?><? endif ?>
<script type="text/javascript" src='<?= JS ?>/plugins/jquery/jquery-1.9.1.min.js'></script>
<script type="text/javascript" src='<?= JS ?>/plugins/jquery/jquery-ui-1.10.1.custom.min.js'></script>
<script type="text/javascript" src='<?= JS ?>/plugins/bootstrap/bootstrap.min.js'></script>
<link href="<?= CSS_JS ?>/dateandtime/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src='<?= CSS_JS ?>/dateandtime/bootstrap-datetimepicker.min.js'></script>-->
<style>

body{
height: auto!important;
overflow: auto !important;
background:none transparent !important;
overflow: hidden;
min-height: 3%;
}
.modal-body{
    width: 96%;
    padding:0 2% ;
}
.row-fluid{
padding-top: 5px !important;
padding-bottom: 5px !important;
}
</style>

<style>
.wizerd-div,.row-form{width: 96%;margin:6% 2%;}
label{padding: 0;}
</style>
        
        
<!-- multi selection choosen -->
<link rel="stylesheet" href="<?= NEW_CSS_JS ?>/multi-select-chosen/chosen.css">
<script src="<?= NEW_CSS_JS ?>/multi-select-chosen/chosen.jquery.js" type="text/javascript"></script>

            

<div class="modal-body" id="">
        

<?= form_open('transporters/add_bus_popup/'.$safa_transporters_id, ' name="frm_add_bus_popup" id="frm_add_bus_popup" method="post" ') ?>

         <div class="row-fluid">
            <div class="span6">
                <div class="span4 TAL Pleft10">
                    <?php echo  lang('bus_no') ?>
                </div>
                <div class="span8">
                    <?php echo form_input('bus_no', set_value("bus_no", '')," style='' id='bus_no' class='validate[required] input-full' ") ?>
                </div>
			</div>
        </div>
        
        <div class="row-fluid">
            <div class="span6">
                <div class="span4 TAL Pleft10">
                    <?php echo  lang('safa_buses_brands_id') ?>
                </div>
                <div class="span8">
                     <?php echo  form_dropdown('safa_buses_brands_id', $safa_buses_brands, set_value('safa_buses_brands_id', ''), 'class="validate[required] chosen-select chosen-rtl input-full"  tabindex="4" id="safa_buses_brands_id" data-placeholder="'.lang('global_all').'"') ?>   
                </div>
			</div>
        </div>
        
        <div class="row-fluid">
            <div class="span6">
                <div class="span4 TAL Pleft10">
                    <?php echo  lang('safa_buses_models_id') ?>
                </div>
                <div class="span8">
                    <?php echo  form_dropdown('safa_buses_models_id', $safa_buses_models, set_value('safa_buses_models_id', ''), 'class="validate[required] chosen-select chosen-rtl input-full"  tabindex="4" id="safa_buses_models_id" data-placeholder="'.lang('global_all').'"') ?>   
                </div>
			</div>
        </div>
        
        <div class="row-fluid">
            <div class="span6">
                <div class="span4 TAL Pleft10">
                    <?php echo  lang('industry_year') ?>
                </div>
                <div class="span8">
                    <?php echo form_input('industry_year', set_value("industry_year", '')," style='' id='industry_year' class='validate[required, custom[integer]] input-full' ") ?>
                </div>
			</div>
        </div>
        
        <div class="row-fluid">
            <div class="span6">
                <div class="span4 TAL Pleft10">
                    <?php echo  lang('passengers_count') ?>
                </div>
                <div class="span8">
                    <?php echo form_input('passengers_count', set_value("passengers_count", '')," style='' id='passengers_count' class='validate[required, custom[integer]] input-full' ") ?>
                </div>
			</div>
        </div>
        
        
        <div class="row-fluid">
            <div class="span6">
                <div class="span4 TAL Pleft10">
                    <?php echo  lang('notes') ?>
                </div>
                <div class="span8">
                    <?php echo form_input('notes', set_value("notes", '')," style='' id='notes' class='validate[required] input-full' ") ?>
                </div>
			</div>
        </div>
        
        
      <div class="toolbar bottom TAC">
          <input type ="submit" name="smt_save" id="smt_save" value="<?= lang('global_submit') ?>" class="btn btn-primary">
      </div>
        
    <?= form_close() ?>
    
</div>    


