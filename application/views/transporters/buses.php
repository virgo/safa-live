<!-- By Gouda, For popup -->
<script type="text/javascript" src='<?= CSS_JS ?>/form/form.js'></script>
<link rel="stylesheet" href="<?= CSS ?>/bootstrap/bootstrap.min.new.css"/>
     


<div class="row-fluid">

    
    
       
<div class="widget">
    <div class="path-container Fright">
    	<div class="path-name Fright">
            <a href="<?php 
            if(session('uo_id')) {
            	echo  site_url().'uo/dashboard'; 
            } else if(session('ea_id')) {
            	echo  site_url().'ea/dashboard';
            } else if(session('ito_id')) {
            	echo  site_url().'ito/dashboard';
            } else {
            	echo  site_url();
            }
            ?>"><?php echo  lang('global_system_management') ?></a>
        </div>
        <div class="path-arrow Fright"></div>
        <div class="path-name Fright">
            <a href="<?php echo  site_url().'transporters'; 
            
            ?>"><?php echo  lang('menu_main_transport_companies') ?></a>
        </div>
        <div class="path-arrow Fright"></div>
        <div class="path-name Fright">  <?= lang('buses') ?></div>
    </div>
    <a href="<?= site_url("transporters/add_bus") ?>/<?= $safa_transporters_id ?>" class="btn Fleft"><?= lang('global_add') ?></a>
</div>


    <div class="widget">
        
<style>
    .updated_msg{
        display:none;
        background-color:#ccee97;
        font-weight: bold;
        text-align: center;
        border:3px solid #cccdc9; 
        padding:20px;  
        margin-bottom:10px;
        border-radius:15px;
        margin:10px; 
    }
</style>
<div class='row-fluid' align='center' >
    <div  class='updated_msg' >
        <br><input  type ="button" value="<?=lang('global_submit')?>"class="btn btn-primary" id='ok' >
    </div> 
 </div>
        
       
        <div class="widget-header">
        <div class="widget-header-icon Fright"> <span class="icos-pencil2"></span> </div>
        <div class="widget-header-title Fright">
            <?php echo  lang('buses') ?>
        </div>
    	</div>
                      
        <div class="block-fluid">
            <table cellpadding="0" class="fsTable" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th><?= lang('bus_no') ?></th>
                        <th><?= lang('safa_buses_brands_id') ?></th>
                        <th><?= lang('safa_buses_models_id') ?></th>
                        <th><?= lang('industry_year') ?></th>
                        <th><?= lang('passengers_count') ?></th>
                        <th><?= lang('global_actions')?></th>
                    </tr>
                </thead>
                <tbody>
                    <? if (isset($items)) { ?>

                     <? foreach($items as $item) {?>
                    <tr id="row_<?php echo $item->safa_transporters_buses_id;?>">
                        <td><?= $item->bus_no ?></td>
                        <td><?= $item->buses_brands_name ?></td>
                        <td><?= $item->buses_models_name ?></td>
                        <td><?= $item->industry_year ?></td>
                        <td><?= $item->passengers_count ?></td>
                        
                        <td class="TAC">
                          <a href="<?= site_url("transporters/edit_bus") ?>/<?= $item->safa_transporters_id ?>/<?= $item->safa_transporters_buses_id ?>"><span class="icon-pencil"></span></a>
                          <a href="<?= site_url("transporters/delete_bus") ?>/<?= $item->safa_transporters_id ?>/<?= $item->safa_transporters_buses_id ?>" onclick="return confirm('<?= lang('global_are_you_sure_you_want_to_delete')?>')"><span class="icon-trash"></span></a>
                       
                        </td>
                        
                    </tr>
                   <?php } ?>
                   <?php } ?>

                </tbody>
            </table>
           
            <?= form_close() ?> 
        </div>
        
        <?= $pagination ?>
        
    </div>
</div>
  

