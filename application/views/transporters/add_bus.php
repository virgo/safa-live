<div class="widget">
    <div class="path-container Fright">
    	<div class="path-name Fright">
            <a href="<?php 
            if(session('uo_id')) {
            	echo  site_url().'uo/dashboard'; 
            } else if(session('ea_id')) {
            	echo  site_url().'ea/dashboard';
            } else if(session('ito_id')) {
            	echo  site_url().'ito/dashboard';
            } else {
            	echo  site_url();
            }
            ?>"><?php echo  lang('global_system_management') ?></a>
        </div>
        <div class="path-arrow Fright"></div>
        <div class="path-name Fright">
            <a href="<?php echo  site_url().'transporters'; 
            
            ?>"><?php echo  lang('menu_main_transport_companies') ?></a>
        </div>
        <div class="path-arrow Fright"></div>
        <div class="path-name Fright">
            <a href="<?php echo  site_url().'transporters/buses/'.$safa_transporters_id; 
            
            ?>"><?php echo  lang('buses') ?></a>
        </div>
        <div class="path-arrow Fright"></div>
        <div class="path-name Fright">  <?= lang('add_bus') ?></div>
    </div>
</div>


<?php echo  form_open_multipart(false, 'id="frm_add_bus" ') ?>


<div class="widget">
    <div class="widget-header">

        <div class="widget-header-icon Fright">
            <span class="icos-pencil2"></span>
        </div>

        <div class="widget-header-title Fright">
            <?php echo  lang('main_data') ?>
            
        </div>
    </div>

    <div class="widget-container">  
        <? if(validation_errors()){ ?>
            <?php echo validation_errors(); ?>        
        <? } ?>
        
        <div class="row-form">
            <div class="span6">
                <div class="span4 TAL Pleft10">
                    <?php echo  lang('bus_no') ?>
                </div>
                <div class="span8">
                    <?php echo form_input('bus_no', set_value("bus_no")," style='' id='bus_no' class='validate[required] input-full' ") ?>
                </div>
			</div>
        
            <div class="span6">
                <div class="span4 TAL Pleft10">
                    <?php echo  lang('safa_buses_brands_id') ?>
                </div>
                <div class="span8">
                    <?php echo  form_dropdown('safa_buses_brands_id', $safa_buses_brands, set_value('safa_buses_brands_id', ''), 'class="validate[required] chosen-select chosen-rtl input-full"  tabindex="4" id="safa_buses_brands_id" data-placeholder="'.lang('global_select_from_menu').'"') ?>   
                </div>
			</div>
        
        </div>
        
        <div class="row-form">
            <div class="span6">
                <div class="span4 TAL Pleft10">
                    <?php echo  lang('safa_buses_models_id') ?>
                </div>
                <div class="span8">
                    <?php echo  form_dropdown('safa_buses_models_id', $safa_buses_models, set_value('safa_buses_models_id', ''), 'class="validate[required] chosen-select chosen-rtl input-full"  tabindex="4" id="safa_buses_models_id" data-placeholder="'.lang('global_select_from_menu').'"') ?>   
                
                </div>
			</div>
        
            <div class="span6">
                <div class="span4 TAL Pleft10">
                    <?php echo  lang('industry_year') ?>
                </div>
                <div class="span8">
                    <?php echo form_input('industry_year', set_value("industry_year")," style='' id='industry_year' class='validate[required, custom[integer]] input-full' ") ?>
                </div>
			</div>
        
        </div>
        
        <div class="row-form">
            <div class="span6">
                <div class="span4 TAL Pleft10">
                    <?php echo  lang('passengers_count') ?>
                </div>
                <div class="span8">
                    <?php echo form_input('passengers_count', set_value("passengers_count")," style='' id='passengers_count' class='validate[required, custom[integer]] input-full' ") ?>
                </div>
			</div>
        
           
        </div>
        
        <div class="row-form">
            <div class="span6">
                <div class="span4 TAL Pleft10">
                    <?php echo  lang('notes') ?>
                </div>
                <div class="span8">
                    <?php echo form_textarea('notes', set_value("notes")," style='' id='notes' class='input-full' ") ?>
                </div>
			</div>
            
            
           
            
            
       </div>
                
    </div>
</div>



<div class="widget TAC">
    <input type="submit" class="btn" name="smt_save" value="<?php echo lang('global_submit') ?>" style="margin:10px;padding: 5px;height: auto">
</div>


<?php echo  form_close() ?>


<div class="footer"></div>


<script type="text/javascript">
$(document).ready(function() {
    var config = {
        '.chosen-select': {width: "100%"},
        '.chosen-select-deselect': {allow_single_deselect: true},
        '.chosen-select-no-single': {disable_search_threshold: 10},
        '.chosen-select-no-results': {no_results_text: 'Oops, nothing found!'},
        '.chosen-select-width': {width: "95%"}
    }
    for (var selector in config) {
        $(selector).chosen(config[selector]);
    }


    // binds form submission and fields to the validation engine
    $("#frm_add_bus").validationEngine({
            prettySelect : true,
            useSuffix: "_chosen",
            promptPosition : "topRight:-150"
            //promptPosition : "bottomLeft"
    });

 
});


</script>

<script type="text/javascript">
$("#safa_buses_brands_id").change(function(){
var safa_buses_brands_id=$(this).val();
var dataString = "safa_buses_brands_id="+ safa_buses_brands_id;
$.ajax
({
type: "POST",
url: "<?php echo base_url().'safa_buses_brands/getBrandModels'; ?>",
data: dataString,
cache: false,
success: function(html)
{
    //alert(html);

var data_arr = JSON.parse(html);

$("#safa_buses_models_id").html(data_arr[0]["safa_buses_models_options"]);
$("#safa_buses_models_id").trigger("chosen:updated");
//$("#safa_buses_models_id").val(data_arr[0]["safa_buses_models_id_selected"]);
//$("#safa_buses_models_id").trigger("chosen:updated");
}
});
});
</script>
