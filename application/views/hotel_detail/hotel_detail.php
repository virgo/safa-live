
<link href="<?= NEW_CSS_JS .'_'. lang('DIR') ?>/new_search/css/style.css" rel="stylesheet" type="text/css" />
<link href="<?= NEW_CSS_JS .'_'. lang('DIR') ?>/new_search/css/extensions.css" rel="stylesheet" type="text/css" />


<!-- Could be loaded remotely from Google CDN : 
<script  src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js" type="text/javascript"></script> -->

	<!--<script src="<?= NEW_CSS_JS .'_'. lang('DIR') ?>/new_search/js/jquery.ui.core.min.js" type="text/javascript"></script>-->
    
	
    
	<script src="<?= NEW_CSS_JS .'_'. lang('DIR') ?>/new_search/js/jquery.ui.tabs.min.js" type="text/javascript"></script>
    
	<script src="js/jquery.datepick.pack.js" type="text/javascript"></script>
    
	<script src="js/jquery.datepick-en-GB.js" type="text/javascript"></script> 
    <!--  Datepick localisations: http://keith-wood.name/datepick.html -->
    
	<script src="<?= NEW_CSS_JS .'_'. lang('DIR') ?>/new_search/js/jquery.imgpreview.js" type="text/javascript"></script>
    <script src="<?= NEW_CSS_JS .'_'. lang('DIR') ?>/new_search/js/jquery.nyroModal.pack.js" type="text/javascript"></script>

	
    

    

	
	<!-- Google maps and marker clustering -->
	<script src="http://www.google.com/jsapi?autoload={'modules':[{name:'maps',version:3,other_params:'sensor=false'}]}" type="text/javascript"></script>

    <script type="text/javascript">
	//<![CDATA[
	
	// we define tabs as a global variable!
	var tabs;
	
	// Sample hotel list -  LAT/LNG/Name
	
	

	
	$(document).ready(function(){
		
		

		/* tabs */
		tabs = $("#tabs").tabs();
		
		// Any component that requires some dimensional computation for its initialization won't work in a hidden tab, 
		// because the tab panel itself is hidden via display: none
		// For Google maps we can show the map once the tab is displayed like this: 
		$('#tabs').bind('tabsshow', function(event, ui) {
			if (ui.panel.id == "maptab") {
				// Init google map
				// center to the first hotel in the list
				// do not use clustering
				// Street view in div#pano
				Site.gmapInit(hotels[0][0], hotels[0][1], 16, false, "pano");
			}
		}); 
		
		/* template setup */
		Site.setup();
		
		/* notice box demonstration */
		
	
	//]]>
	</script>
    
    <script>
    $(document).ready(function() {
        jQuery('.adly').remove();
    });
    
    </script>

<div class="container container_12">
<div class="grid_4" id="leftcolumn">
		<!-- Search form -->
		
		<!-- End of Search form --><!-- Around Hotel Content boxes -->
		<div class="box radius">
			<div class="padded">
				<h2 class="gray radius">حول الفندق</h2>
				<div class="content-box radius">
				  <div class="headers">المزارات</div>
					<div class="section">
						<table class="full">
							<tbody>
								<tr>
									<td>Water park</td>
									<td class="tr"><small>3 km</small></td>
								</tr>
								<tr>
									<td>Wild park</td>
									<td class="tr"><small>4.5 km</small></td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
				<div class="content-box radius">
				  <div class="headers">التسوق</div>
					<div class="section">
						<table class="full">
							<tbody>
								<tr>
									<td>Mall</td>
									<td class="tr"><small>1 km</small></td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
				<div class="content-box radius">
				  <div class="headers">المواصلات</div>
					<div class="section">
					  <table class="full">
							<tbody>
								<tr>
									<td>Train station</td>
									<td class="tr"><small>1.5 km</small></td>
								</tr>
								<tr>
									<td>Marina</td>
									<td class="tr"><small>3 km</small></td>
								</tr>
								<tr>
									<td><img src="<?= IMAGES ?>/imgs/ico/air.png"  /><span class="td_h">المطار </span></td>
									<td class="tr"><small>31 km</small></td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
				<div class="content-box radius">
				  <div class="headers">قريب من</div>
					<div class="section">
						<table class="full">
							<tbody>
								<tr>
									<td><a href="#">Lorem Ipsum</a></td>
									<td class="tr"><small>0.5 km</small></td>
								</tr>
								<tr>
									<td><a href="#">Dolor</a></td>
									<td class="tr"><small>0.7 km</small></td>
								</tr>
								<tr>
									<td><a href="#">Sit Amet</a></td>
									<td class="tr"><small>1.5 km</small></td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		<!-- End of Around Hotel Content boxes -->
		
		<!-- Share box -->
		<div class="">&nbsp;</div>
		<div class="box radius">
			<div class="padded">
				<h2 class="gray radius">Share</h2>
				<p class="tc">
					<a href="#" title="Share on Facebook"><img src="<?= IMAGES ?>/imgs/social/facebook.png" alt="Share on Facebook" /></a>
					<a href="#" title="Share on Twitter"><img src="<?= IMAGES ?>/imgs/social/twitter.png" alt="Share on Twitter" /></a>
					<a href="#" title="Share on MySpace"><img src="<?= IMAGES ?>/imgs/social/myspace.png" alt="Share on MySpace" /></a>
					<a href="#" title="Bookmark on Delicious"><img src="<?= IMAGES ?>/imgs/social/delicious.png" alt="Bookmark on Delicious" /></a>
					<a href="#" title="Digg this"><img src="<?= IMAGES ?>/imgs/social/digg.png" alt="Digg this" /></a>
					<a href="#" title="Submit to StumbleUpon"><img src="<?= IMAGES ?>/imgs/social/stumbleupon.png" alt="Submit to StumbleUpon" /></a>
					<a href="#" title="Share on Reddit"><img src="<?= IMAGES ?>/imgs/social/reddit.png" alt="Share on Reddit" /></a>
				</p>
				<hr class="thin" />
				<a href="#" onclick="$('#friend').fadeIn('fast'); return false;">Email this to a friend</a>
				<form id="friend" method="post" action="" style="display:none;">
					<fieldset class="radius">
						<p>
							<label class="" for="email1">Email (yours)</label><br/>
							<input type="text" id="email1" class="full" value="" name="email1"/>
						</p>
						<p>
							<label class="" for="email1">Email (friend's)</label><br/>
							<input type="text" id="email2" class="full" value="" name="email2"/>
						</p>
						<p>
							<label class="" for="msg">Message</label><br/>
							<textarea id="msg" class="medium full" name="msg" rows="4" cols="20"></textarea>
						</p>
						<input type="submit" class="btn radius" value="Send Email" /> or <a href="#" onclick="$('#friend').fadeOut('fast'); return false;">close</a>
					</fieldset>
				</form>
			</div>
		</div>
		<!-- End of Share box -->
	<div class="">&nbsp;</div>
		
		<!-- Guest Corner box --><!-- End of Guest Corner box -->
	<div class="">&nbsp;</div>
		
		<!-- Money exchange box -->
		<p>&nbsp;</p>
		<!-- End of Money exchange box -->
  </div>
    
    

 <div class='grid_8 searchpanel'>	
 <div class="box radius" style="padding:10px;">	
		<h1 class="">فندق الحرم <img src="<?= IMAGES ?>/imgs/star.png" alt="*" />
        <img src="<?= IMAGES ?>/imgs/star.png" alt="*" />
        <img src="<?= IMAGES ?>/imgs/star.png" alt="*" />
        <img src="<?= IMAGES ?>/imgs/star.png" alt="*" /></h1>
		<input type="submit" class="btn btn-custom radius fr" value="Book Now!" onclick="tabs.tabs('select', 0); Site.scroll_to_hash('#availability'); return false;" />
		
		<p><em>الحرم, جده, المملكه السعوديه</em></p>
		
		<!-- Tabs -->
		<div id="tabs">
			<ul>
				<li><a href="#tabs-1">معلومات عامة</a></li>
				<li><a href="#tabs-2">اراء وتقييم</a></li>
				<li><a href="#maptab">الموقع</a></li>
			</ul>
			<!-- Tab 1: Hotel Overview -->
			<div id="tabs-1">
				<div class="infopanel padded">
					<span class="padded fl tc score"><b>8.40</b><br/><a href="#" onclick="tabs.tabs('select', 1); return false;"><small>32 reviews</small></a></span>
					<p class="ttc"><img src="<?= IMAGES ?>/imgs/hotels/room-1.jpg" alt="" class="w12 big" /></p>
					
					<div class=" box padded">
						<a href="<?= IMAGES ?>/imgs/hotels/room-1.jpg" class="nyroModal" title="Hotel Name" rel="gal"><img class="small" src="<?= IMAGES ?>/imgs/hotels/room-1t.jpg" alt="" /></a>
						<a href="<?= IMAGES ?>/imgs/hotels/room-2.jpg" class="nyroModal" title="Hotel Name" rel="gal"><img class="small" src="<?= IMAGES ?>/imgs/hotels/room-2t.jpg" alt="" /></a>
						<a href="<?= IMAGES ?>/imgs/hotels/room-3.jpg" class="nyroModal" title="Hotel Name" rel="gal"><img class="small" src="<?= IMAGES ?>/imgs/hotels/room-3t.jpg" alt="" /></a>
						<a href="<?= IMAGES ?>/imgs/hotels/room-4.jpg" class="nyroModal" title="Hotel Name" rel="gal"><img class="small" src="<?= IMAGES ?>/imgs/hotels/room-4t.jpg" alt="" /></a>
							<a href="<?= IMAGES ?>/imgs/hotels/room-1.jpg" class="nyroModal" title="Hotel Name" rel="gal"><img class="small" src="<?= IMAGES ?>/imgs/hotels/room-1t.jpg" alt="" /></a>
						<a href="<?= IMAGES ?>/imgs/hotels/room-2.jpg" class="nyroModal" title="Hotel Name" rel="gal"><img class="small" src="<?= IMAGES ?>/imgs/hotels/room-2t.jpg" alt="" /></a>
						<a href="<?= IMAGES ?>/imgs/hotels/room-3.jpg" class="nyroModal" title="Hotel Name" rel="gal"><img class="small" src="<?= IMAGES ?>/imgs/hotels/room-3t.jpg" alt="" /></a>
						<a href="<?= IMAGES ?>/imgs/hotels/room-4.jpg" class="nyroModal" title="Hotel Name" rel="gal"><img class="small" src="<?= IMAGES ?>/imgs/hotels/room-4t.jpg" alt="" /></a>
						<a href="<?= IMAGES ?>/imgs/hotels/room-5.jpg" class="nyroModal" title="Hotel Name" rel="gal"><img class="small" src="<?= IMAGES ?>/imgs/hotels/room-5t.jpg" alt="" /></a>
							<a href="<?= IMAGES ?>/imgs/hotels/room-1.jpg" class="nyroModal" title="Hotel Name" rel="gal"><img class="small" src="<?= IMAGES ?>/imgs/hotels/room-1t.jpg" alt="" /></a>
						<a href="<?= IMAGES ?>/imgs/hotels/room-2.jpg" class="nyroModal" title="Hotel Name" rel="gal"><img class="small" src="<?= IMAGES ?>/imgs/hotels/room-2t.jpg" alt="" /></a>
						<a href="<?= IMAGES ?>/imgs/hotels/room-3.jpg" class="nyroModal" title="Hotel Name" rel="gal"><img class="small" src="<?= IMAGES ?>/imgs/hotels/room-3t.jpg" alt="" /></a>
						<a href="<?= IMAGES ?>/imgs/hotels/room-4.jpg" class="nyroModal" title="Hotel Name" rel="gal"><img class="small" src="<?= IMAGES ?>/imgs/hotels/room-4t.jpg" alt="" /></a>
						<a href="<?= IMAGES ?>/imgs/hotels/room-5.jpg" class="nyroModal" title="Hotel Name" rel="gal"><img class="small" src="<?= IMAGES ?>/imgs/hotels/room-5t.jpg" alt="" /></a>
						<a href="<?= IMAGES ?>/imgs/hotels/room-5.jpg" class="nyroModal" title="Hotel Name" rel="gal"><img class="small" src="<?= IMAGES ?>/imgs/hotels/room-5t.jpg" alt="" /></a>
					</div>
					<p class="tj">Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?</p>
					
					<h6 id="availability">الاتاحه</h6>
					<div class="padded">
						<p>Available rooms from Sat 24 Jul 2010, to Sun 25 Jul 2010 [ <a href="#" onclick="$('#changedates').show(); return false;">Change dates</a> ]</p>
						<p>Most recent booking for this hotel was yesterday at 19:12 from Czech Republic</p>
					</div>
					<form name="changedates" id="changedates" method="get" action="" style="display:none;">
						<fieldset class="box">
							<p class="fl">
								<label class="" for="startdate_change">Check-in</label><br/>
								<input type="text" id="startdate_change" class="" value="" name="startdate_change"/>
								<input type="hidden" name="sd_timestamp_change" id="sd_timestamp_change" />
							</p>
							<p class="fl">
								<label class="" for="enddate_change">Check-out</label><br/>
								<input type="text" id="enddate_change" class="" value="" name="enddate_change"/>
								<input type="hidden" name="ed_timestamp_change" id="ed_timestamp_change" />
							</p>
							<p class="fl">
								<label class="" for="number_change">Persons</label><br/>
								<select id="number_change" name="number_change">
									<option value="1" selected="selected">1</option>
									<option value="2">2</option>
									<option value="3">3</option>
									<option value="4">4</option>
									<option value="5">5</option>
									<option value="6">6</option>
									<option value="7">7</option>
									<option value="8">8</option>
									<option value="9">9</option>
									<option value="10">10</option>
								</select>
							</p>
							<div class="clear"></div>
							<input type="submit" class="btn radius small" value="Check availability" /> or <a href="#" onclick="$('#changedates').hide(); return false;">close</a>
						</fieldset>
					</form>
					<table class="full box">
						<tbody>
							<tr>
								<td class="w12 padded"><b class="big">Double bed room</b><br /><small><a href="#" onclick="$('#roomid_1').toggle(); return false;">Details &raquo;</a></small></td>
								<td class="w12 padded tr">price from (person/day): <span class="price"><em>$29.99</em></span></td>
							</tr>
							<tr style="display:none;" id="roomid_1" class="oddbox">
								<td class="padded" colspan="2">
									<p class="small">
										<a class="previewimg nyroModal" title="Hotel Name: Double bed room" href="<?= IMAGES ?>/imgs/hotels/room-1.jpg"><img src="<?= IMAGES ?>/imgs/hotels/room-1t.jpg" alt="" class="fl" /></a>
										<b>Room Facilities:</b> Shower, Bath, Safety Deposit Box, TV, Telephone, Air Conditioning, Balcony, Radio, Toilet, Satellite TV, Cable TV, Tea/Coffee Maker, Hairdryer, Iron, Refrigerator, Work Desk, DVD Player, Microwave, Dishwasher, Washing Machine, Kitchen<br />
										<b>Room Size:</b> 110 square metres<br />
										<b>Bed Size(s):</b> 1 Extra large double bed
									</p>
									<hr class="thin" />
									<b>Bed and breakfast (person/day)</b>
									<table class="full roomlist">
										<thead>
											<tr>
												<th class="tc w14">Persons</th>
												<th class="tc w14">Days</th>
												<th class="tc w12" colspan="2">Rate</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td class="tc">1</td>
												<td class="tc">1</td>
												<td class="tr"><span class="price">$49.99</span></td>
												<td class="tl"><input type="submit" class="radius" value="Book" /></td>
											</tr>
											<tr>
												<td class="tc">2</td>
												<td class="tc">1</td>
												<td class="tr"><span class="price">$41.99</span></td>
												<td class="tl"><input type="submit" class="radius" value="Book" /></td>
											</tr>
										</tbody>
									</table>
									<b>Half board (person/day)</b>
									<table class="full roomlist">
										<thead>
											<tr>
												<th class="tc w14">Persons</th>
												<th class="tc w14">Days</th>
												<th class="tc w12" colspan="2">Rate</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td class="tc">1</td>
												<td class="tc">1</td>
												<td class="tr"><span class="price">$49.99</span></td>
												<td class="tl"><input type="submit" class="radius" value="Book" /></td>
											</tr>
											<tr>
												<td class="tc">2</td>
												<td class="tc">1</td>
												<td class="tr"><span class="price">$41.99</span></td>
												<td class="tl"><input type="submit" class="radius" value="Book" /></td>
											</tr>
										</tbody>
									</table>
								</td>
							</tr>
							<tr class="dashed">
								<td class="w12 padded"><b class="big">Suite</b><br /><small><a href="#" onclick="$('#roomid_2').toggle(); return false;">Details &raquo;</a></small></td>
								<td class="w12 padded tr">price from (person/day): <span class="price"><em>$39.99</em></span></td>
							</tr>
							<tr style="display:none;" id="roomid_2" class="oddbox">
								<td class="padded" colspan="2">
									<p class="small">
										<a class="previewimg nyroModal" title="Hotel Name: Suite" href="img/hotels/room-2.jpg"><img src="<?= IMAGES ?>/imgs/hotels/room-2t.jpg" alt="" class="fl" /></a>
										<b>Room Facilities:</b> Shower, Bath, Safety Deposit Box, TV, Telephone, Air Conditioning, Balcony, Radio, Toilet, Satellite TV, Cable TV, Tea/Coffee Maker, Hairdryer, Iron, Refrigerator, Work Desk, DVD Player, Microwave, Dishwasher, Washing Machine, Kitchen<br />
										<b>Room Size:</b> 110 square metres<br />
										<b>Bed Size(s):</b> 1 Extra large double bed
									</p>
									<hr class="thin" />
									<b>Bed and breakfast (person/day)</b>
									<table class="full roomlist">
										<thead>
											<tr>
												<th class="tc w14">Persons</th>
												<th class="tc w14">Days</th>
												<th class="tc w12" colspan="2">Rate</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td class="tc">1</td>
												<td class="tc">1</td>
												<td class="tr"><span class="price">$49.99</span></td>
												<td class="tl"><input type="submit" class="radius" value="Book" /></td>
											</tr>
											<tr>
												<td class="tc">2</td>
												<td class="tc">1</td>
												<td class="tr"><span class="price">$41.99</span></td>
												<td class="tl"><input type="submit" class="radius" value="Book" /></td>
											</tr>
										</tbody>
									</table>
									<b>Half board (person/day)</b>
									<table class="full roomlist">
										<thead>
											<tr>
												<th class="tc w14">Persons</th>
												<th class="tc w14">Days</th>
												<th class="tc w12" colspan="2">Rate</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td class="tc">1</td>
												<td class="tc">1</td>
												<td class="tr"><span class="price">$49.99</span></td>
												<td class="tl"><input type="submit" class="radius" value="Book" /></td>
											</tr>
											<tr>
												<td class="tc">2</td>
												<td class="tc">1</td>
												<td class="tr"><span class="price">$41.99</span></td>
												<td class="tl"><input type="submit" class="radius" value="Book" /></td>
											</tr>
										</tbody>
									</table>
								</td>
							</tr>
						</tbody>
					</table>
					<p class="padded"><b>You will pay in the hotel's preferred currency (AED) after your stay at the hotel. The exchange rate is subject to fluctuations.</b></p>
					
					<!-- Hotel Facilities -->
					<h6>تفاصيل الفندق</h6>
					<table class="full">
						<tbody>
							<tr>
								<td class="w14 padded"><b>معلومات عامة : </b></td>
								<td class="w34 padded">Restaurant, Bar, 24-Hour Front Desk, Newspapers, Terrace, Non-Smoking Rooms, Rooms/Facilities for Disabled Guests, Elevator, Express Check-In/Check-Out, Safety Deposit Box, Soundproofed Rooms, Design Hotel, Baggage Storage, Ski Storage, Airconditioning.</td>
							</tr>
							<tr class="dashed">
								<td class="w14 padded"><b>انشطه :</b></td>
								<td class="w34 padded">Tennis Court, Sauna, Fitness Center, Golf Course (within 2 miles), Skiing, Spa &amp; Health Club, Massage, Ski School, Outdoor Swimming Pool. </td>
							</tr>
							<tr class="dashed">
								<td class="w14 padded"><b>خدمات :</b></td>
								<td class="w34 padded">Room Service, Meeting/Banquet Facilities, Airport Shuttle, Business Center, Babysitting/Child Services, Laundry, Dry Cleaning, Barber/Beauty Shop, VIP Room Facilities, Breakfast in the Room, Ironing Service, Currency Exchange, Shoe Shine, Car Rental, Tour Desk, Fax/Photocopying. </td>
							</tr>
						</tbody>
					</table>
					<!-- End of Hotel Facilities -->
					
					<!-- Hotel Policies -->
					<h6>Hotel Policies</h6>
					<table class="full">
						<tbody>
							<tr>
								<td class="w14 padded"><b>Check-in</b></td>
								<td class="w34 padded">From 15:00 hours </td>
							</tr>
							<tr class="dashed">
								<td class="w14 padded"><b>Check-out</b></td>
								<td class="w34 padded">Until 12:00 hours </td>
							</tr>
							<tr class="dashed">
								<td class="w14 padded"><b>Pets</b></td>
								<td class="w34 padded">Pets are NOT allowed.</td>
							</tr>
						</tbody>
					</table>
					<!-- End of Hotel Policies -->
					
					<!-- Important Information -->
					<h6>Important Information</h6>
					<div class="padded">
						<p>A security deposit or pre-authorization on the credit card is taken upon arrival. Full payment of the stay is required upon departure or during the stay. Late check-out fees apply after 12pm.</p>
						<p>Transportation to/from the airport can be arranged at an additional cost in a luxury limousine with prior request.</p>
						<p>Some nationalities can get an entry permit stamped in their passport upon arrival at the airport. Please check your visa requirements before traveling.</p>
						<p>This hotel can arrange a visa for you after your reservation is made. Please contact them via phone or email (details provided in your confirmation).</p>
					</div>
					<!-- End of Important Information -->
					
				</div>
			</div>
			<!-- End of Tab 1: Hotel Overview -->
			
			<!-- Tab 2: Guest Review -->
			<div id="tabs-2">
				<div class="padded">
					<table class="full">
						<tbody>
							<tr>
								<td class="w14 tc">
									<div class="box radius padded tc score">Total Score<br /><b class="color-green">8.40</b></div>
								</td>
								<td class="w34">
									<!-- Progress bars -->
									<table class="full">
										<tbody>
											<tr>
												<td class="tr">Clean&nbsp;</td>
												<td><div class="progress progress-green"><span style="width:88%"><b>8.80</b></span></div></td>
											</tr>
											<tr>
												<td class="tr">Comfort&nbsp;</td>
												<td><div class="progress progress-green"><span style="width:88%"><b>8.80</b></span></div></td>
											</tr>
											<tr>
												<td class="tr">Location&nbsp;</td>
												<td><div class="progress progress-green"><span style="width:89%"><b>8.90</b></span></div></td>
											</tr>
											<tr>
												<td class="tr">Services&nbsp;</td>
												<td><div class="progress progress-green"><span style="width:86%"><b>8.60</b></span></div></td>
											</tr>
											<tr>
												<td class="tr">Staff&nbsp;</td>
												<td><div class="progress progress-yellow"><span style="width:74%"><b>7.40</b></span></div></td>
											</tr>
											<tr>
												<td class="tr">Value for money&nbsp;</td>
												<td><div class="progress progress-red"><span style="width:71%"><b>7.10</b></span></div>	</td>
											</tr>
										</tbody>
									</table>
									<!-- End of Progress bars -->
								</td>
							</tr>
						</tbody>
					</table>
					
					<!-- Guest Reviews -->
					<h6>Individual Guest Reviews (32)</h6>
					
					<hr class="thin" />
					<div class="padded">
						<table class="full">
							<tbody>
								<tr>
									<td class="w14 padded">
										<b>Guest1</b><br/><small>July 20, 2010</small>
									</td>
									<td class="w12 padded">
										<p class="comment_good">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
										<p class="comment_bad">Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
									</td>
									<td class="w14 tc padded">
										<span class="baloon-green"><b>9.00</b></span>
									</td>
								</tr>
								<tr class="dashed">
									<td class="w14 padded">
										<b>Guest2</b><br/><small>July 19, 2010</small>
									</td>
									<td class="w12 padded">
										<p class="comment_good">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
										<p class="comment_bad">Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
									</td>
									<td class="w14 tc padded">
										<span class="baloon-red"><b>7.50</b></span>
									</td>
								</tr>
								<tr class="dashed">
									<td class="w14 padded">
										<b>Guest3</b><br/><small>July 19, 2010</small>
									</td>
									<td class="w12 padded">
										<p class="comment_good">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
										<p class="comment_bad">Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
									</td>
									<td class="w14 tc padded">
										<span class="baloon-green"><b>9.00</b></span>
									</td>
								</tr>
								<tr class="dashed">
									<td class="w14 padded">
										<b>Guest4</b><br/><small>July 10, 2010</small>
									</td>
									<td class="w12 padded">
										<p class="comment_good">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
										<p class="comment_bad">Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
									</td>
									<td class="w14 tc padded">
										<span class="baloon-red"><b>7.50</b></span>
									</td>
								</tr>
								<tr class="dashed">
									<td class="w14 padded">
										<b>Guest5</b><br/><small>July 10, 2010</small>
									</td>
									<td class="w12 padded">
										<p class="comment_good">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
										<p class="comment_bad">Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
									</td>
									<td class="w14 tc padded">
										<span class="baloon-green"><b>10.00</b></span>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
					<!-- End of Guest Reviews -->
					
					<hr class="thin" />
					<p><span class="fr"><a href="#">Next page &gt;</a></span><a href="#">&lt; Previous page</a></p>
				</div>
			</div>
			<!-- End of Tab 2: Guest Review -->
			
			<!-- Tab 3: Google map -->
			<div id="maptab">
				<div id="map_canvas" style="height:310px;"></div>
				<div id="pano" style="height: 310px;"></div>
			</div>
			<!-- End of Tab 3: Google map -->

		</div> 
		<!-- End of Tabs -->
		
		<!-- Shortcut buttons -->
	
		<!-- End of Shortcut buttons -->
		
		<div class="clear">&nbsp;</div>
		<p>&nbsp;</p>
		
	</div>  
    
    
    
    
		
		<!-- Shortcut buttons -->
		<div class="fr">
			<button class="box radius" title="Print this page" onclick="window.print();"><img src="<?= IMAGES ?>/imgs/print.gif" alt="Print" /></button>
			<button class="box radius" title="Email to a friend"><img src="<?= IMAGES ?>/imgs/email.gif" alt="Email" /></button>
			<button class="box radius" title="Bookmark"><img src="<?= IMAGES ?>/imgs/bookmark.gif" alt="Bookmark" /></button>
		</div>
		<!-- End of Shortcut buttons -->
		
		<div class="clear">&nbsp;</div>
	</div>
	</div>	
