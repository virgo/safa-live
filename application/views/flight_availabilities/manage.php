<script>.widget{width:98%;}</script>

<div class="span12">
    <div class="widget">
        <div class="path-container Fright">
            <div class="path-name Fright">
                <a href="<?= site_url('ea/dashboard') ?>"><?= lang('global_system_management') ?></a>
            </div>
            <div class="path-arrow Fright"></div>
            <div class="path-name Fright">
                <?= lang('global_' . $module) ?>
            </div>
        </div>
    </div>
</div>

<?= form_open() ?>
<div class="row-fluid">
    <div class="widget">
        <div class="widget-header">
            <div class="widget-header-icon Fright"><span class="icos-pencil2"></span></div>
            <div class="widget-header-title Fright"><?= lang('global_' . $module) ?></div>
        </div>
        <div class="block-fluid">
            <? if (validation_errors()): ?>
                <?= validation_errors(); ?>
            <? endif ?> 
            <div class="row-form">
                <div class="span12">
                    <div class="span12">
                        <?= lang($module . '_amadeus') ?>:
                    </div>
                    <div class="span12">
                        <?= form_textarea('amadeus', set_value("amadeus", $item->amadeus), 'class="span12" id="am" style="direction: ltr"') ?>
                        <div class="toolbar bottom TAC">
                            <? if( ! $id ): ?>
                            <input type ="button" 
                                   value="<?= lang($module . '_parse') ?>"
                                   class="btn btn-primary" 
                                   onclick="parsing('CHECK NAMES','DEPOSIT','get names','ISSUE TKTS','EGP'); $(this).hide()" />
                            <? endif ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row-form">
                <div class="span4">
                    <div class="span4">
                        <?= lang($module . '_pay_deposit_value') ?>:
                    </div>
                    <div class="span8">
                        <?= form_input('pay_deposit_value', set_value("pay_deposit_value", $item->pay_deposit_value), 'class="validate[required]" id="deposVal"') ?>
                    </div>
                </div>
                  <div class="span4">
                    <div class="span4">
                        <?= lang($module . '_pnr') ?>:
                    </div>
                    <div class="span8">
                        <?= form_input('pnr', set_value("pnr", $item->pnr), ' id="pnr"') ?>
                    </div>
                </div>
                <div class="span4">
                    <div class="span4">
                        <?= lang($module . '_check_names') ?>:
                    </div>
                    <div class="span8">
                        <?= form_input('check_names', set_value("check_names", $item->check_names), 'class="validate[required] datepicker" id="chname"') ?>
                    </div>
                </div>
            </div>
            <div class="row-form">
                <div class="span4">
                    <div class="span4">
                        <?= lang($module . '_pay_deposit') ?>:
                    </div>
                    <div class="span8">
                        <?= form_input('pay_deposit', set_value("pay_deposit", $item->pay_deposit), 'class="validate[required] datepicker" id="pay"') ?>
                    </div>
                </div>
                
                <div class="span4">
                    <div class="span4">
                        <?= lang($module . '_to_get_names') ?>:
                    </div>
                    <div class="span8">
                        <?= form_input('to_get_names', set_value("to_get_names", $item->to_get_names), 'class="validate[required] datepicker" id="getname"') ?>
                    </div>
                </div>
                
                <div class="span4">
                    <div class="span4">
                        <?= lang($module . '_to_issue_ticket') ?>:
                    </div>
                    <div class="span8">
                        <?= form_input('to_issue_ticket', set_value("to_issue_ticket", $item->to_issue_ticket), 'class="validate[required] datepicker" id="ticket"') ?>
                    </div>
                </div>
            </div>
            
            <? $this->load->view('flight_availabilities/details') ?>
            
            <div class="toolbar bottom TAC">
                <?// if( ! $id ): ?>
                <input type="submit" name="submit" value="<?= lang('global_submit') ?>" class="btn btn-primary"> &nbsp;
                <?// endif ?>
                <input type ="button" 
                       value="<?= lang('global_back') ?>"
                       class="btn btn-primary" 
                       onclick="window.location = '<?= site_url($module.'/index/'.$this->uri->segment(3)) ?>'" />
            </div>
        </div>
    </div>
    
<!-- By Gouda, For Private flight for specific safa_group_passports -->
    <input type='hidden' name='hdn_safa_group_passport_ids'  id='hdn_safa_group_passport_ids'  value=''/>

</div>
<?= form_close() ?>


<!-- By Gouda, For Private flight for specific safa_group_passports -->
<script type="text/javascript">
    $('.datepicker').datepicker({dateFormat: 'yy-mm-dd'});
if(opener!=null) {
	var safa_group_passport_ids_str = opener.document.getElementById('hdn_safa_group_passport_ids').value;
	//alert(safa_group_passport_ids_str);
	$("#hdn_safa_group_passport_ids").val(safa_group_passport_ids_str);
}
</script>
<script type="text/javascript" src="<?= base_url() ?>/static/new_template/js/amadeus_parser.js"></script>
