<script>.widget{width:98%;}</script>

<div class="span12">
    <div class="widget">
        <div class="path-container Fright">
            <div class="path-name Fright">
                <a href="<?= site_url('ea') ?>"><?= lang('global_system_management') ?></a>
            </div>
            <div class="path-arrow Fright"></div>
            <div class="path-name Fright">
                <a href="<?= site_url('ea/safa_eas') ?>"><?= lang('menu_external_agent') ?></a>
            </div>
            <div class="path-arrow Fright"></div>
            <div class="path-name Fright">
                <?= lang('global_' . $module) ?>
            </div>
        </div>
    </div>
</div>

<div class="row-fluid">
    <div class="widget">
        <div class="widget-header">
            <div class="widget-header-icon Fright">
                <span class="icos-pencil2"></span>
            </div>
            <div class="widget-header-title Fright">
                <?= lang('global_' . $module) ?>
            </div><a class="btn Fleft" href="<?= site_url($module . '/manage/') ?>"><?= lang('global_add_new_record') ?></a>
        </div>
        <div class="widget-container">
            <div class='table-responsive' >
                <table cellpadding="0" class="fsTable" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th><?= lang($module . '_'.'pnr') ?></th>
                            <th><?= lang('global_actions') ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <? if (ensure($items)): ?>
                            <? foreach ($items as $item): ?>
                                <tr>
                                    <td><?= $item->pnr ?></td>
                                    <td class="TAC">
                                        <a href="<?= site_url($module."/manage/" . $item->erp_flight_availability_id) ?>" ><span class="icon-adjust"></span></a>
                                        <? if( ! $item->safa_trip_id): ?>
                                        <a href="<?= site_url($module."/delete/" . $item->erp_flight_availability_id) ?>" ><span class="icon-trash"></span></a> 
                                        <? endif ?>
                                    </td>
                                </tr>
                            <? endforeach ?>
                        <? endif ?>
                    </tbody>
                </table>
<!--                <?= $pagination ?>-->
            </div>
        </div>
    </div>
</div>