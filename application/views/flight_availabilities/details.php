<script>
    var c = 0;
    var global_classes = new Array();
    <? foreach($erp_flight_classes as $key => $value): ?>
    <? if($key): ?>
    global_classes['<?= $value ?>'] = <?= $key ?>;
    <? endif ?>
    <? endforeach; ?>
    var global_status = new Array();
    <? foreach($erp_flight_status as $key => $value): ?>
    <? if($key): ?>
    global_status['<?= $value ?>'] = <?= $key ?>;
    <? endif ?>
    <? endforeach; ?>
</script>
<style>
    table input {
    width: 90%;
    margin: auto;
    }
</style>
<div class="row-form">
    <div class="span12">
        <div class="span12">
            <?= lang($module . '_details') ?>:
            <a href="javascript:new_row();" class="btn">+</a>
        </div>
    </div>
</div>
<div class="row-form">
<table class="details">
    <thead>
    <tr>
        <th><?= lang($module . '_airlines') ?></th>
        <th><?= lang($module . '_flight_number') ?></th>
        <th><?= lang($module . '_class') ?></th>
        <th><?= lang($module . '_date') ?></th>
        <th><?= lang($module . '_airports') ?></th>
        <th><?= lang($module . '_status') ?></th>
        <th><?= lang($module . '_seat_count') ?></th>
        <th><?= lang($module . '_department_time') ?></th>
        <th><?= lang($module . '_arrival_time') ?></th>
        <th><?= lang('global_operations') ?></th>
    </tr>
    </thead>
    <tbody class="embed">
    <? if(ensure($details)): ?>
    <? foreach($details as $item): $id = $item->erp_flight_availabilities_detail_id ?>
    <tr class="table-row" rel="<?= $id ?>">
        <td><?= form_input('erp_airline_id['.$id.']', set_value("erp_airline_id['.$id.']", $item->airline), 'class="validate[required]" id="airline['.$id.']"') ?></td>
        <td><?= form_input('flight_number['.$id.']', set_value("flight_number['.$id.']", $item->flight_number), 'class="validate[required]" id="flightno['.$id.']"') ?></td>
        <td><?= form_dropdown('erp_flight_class_id['.$id.']', $erp_flight_classes, set_value("erp_flight_class_id['.$id.']", $item->erp_flight_class_id), 'class="validate[required]" id=" class['.$id.']"') ?></td>
        <td><?= form_input('flight_date['.$id.']', set_value("flight_date['.$id.']", $item->flight_date), 'class="validate[required] datepicker" id="datefields['.$id.']"') ?></td>
        <td><?= form_input('airports['.$id.']', set_value("airports['.$id.']", $item->erp_port_from.$item->erp_port_to), 'class="validate[required]" id="airports['.$id.']"') ?></td>
        <td><?= form_dropdown('erp_flight_status_id['.$id.']', $erp_flight_status, set_value("erp_flight_status_id['.$id.']", $item->erp_flight_status_id), 'class="validate[required]" id="status['.$id.']"') ?></td>
        <td><?= form_input('seats_count['.$id.']', set_value("seats_count['.$id.']", $item->seats_count), 'class="validate[required]" id="seatcont['.$id.']"') ?></td>
        <td><?= form_input('departure_time['.$id.']', set_value("departure_time['.$id.']", date('H:i', strtotime($item->departure_time))), 'class="validate[required]" id="deptime['.$id.']"') ?></td>
        <td><?= form_input('arrival_time['.$id.']', set_value("arrival_time['.$id.']", date('H:i', strtotime($item->arrival_time))), 'class="validate[required]" id="arrtime['.$id.']"') ?></td>
        <td><a href="javascript:void(0);" onclick="remove_row(<?= $id ?>)"><?= lang('global_delete') ?></a></td>
    </tr>
    <? endforeach ?>
    <? endif ?>
    </tbody>
</table>
</div>

<script>
    function new_row() {
        var id = c;
        var new_row = function() {/*
        <tr class="table-row" rel="{$id}">
            <td><?= form_input('erp_airline_id[{$id}]', set_value("erp_airline_id[{\$id}]"), 'class="validate[required]" id="airline[{$id}]"') ?></td>
            <td><?= form_input('flight_number[{$id}]', set_value("flight_number[{\$id}]"), 'class="validate[required]" id="flightno[{$id}]"') ?></td>
            <td><?= form_dropdown('erp_flight_class_id[{$id}]', $erp_flight_classes, set_value("erp_flight_class_id[{\$id}]"), 'class="validate[required]" id="class[{$id}]"') ?></td>
            <td><?= form_input('flight_date[{$id}]', set_value("flight_date[{\$id}]"), 'class="validate[required] datepicker" id="datefields[{$id}]"') ?></td>
            <td><?= form_input('airports[{$id}]', set_value("airports[{\$id}]"), 'class="validate[required]" id="airports[{$id}]"') ?></td>
            <td><?= form_dropdown('erp_flight_status_id[{$id}]', $erp_flight_status, set_value("erp_flight_status_id[{\$id}]"), 'class="validate[required]" id="status[{$id}]"') ?></td>
            <td><?= form_input('seats_count[{$id}]', set_value("seats_count[{\$id}]"), 'class="validate[required]" id="seatcont[{$id}]"') ?></td>
            <td><?= form_input('departure_time[{$id}]', set_value("departure_time[{\$id}]"), 'class="validate[required]" id="deptime[{$id}]"') ?></td>
            <td><?= form_input('arrival_time[{$id}]', set_value("arrival_time[{\$id}]"), 'class="validate[required]" id="arrtime[{$id}]"') ?></td>
            <td><a href="javascript:void(0);" onclick="remove_row('{$id}')"><?= lang('global_delete') ?></a></td>
        </tr>
        */}.toString().replace(/{\$id}/g, id).slice(14, -3);
    
        $('tbody.embed').append(new_row);
        $('.datepicker').datepicker({dateFormat: 'yy-mm-dd'});
        c++;
    }
    function remove_row(id)
    {
        $('tr[rel='+id+']').remove();
    
    }
    
</script>