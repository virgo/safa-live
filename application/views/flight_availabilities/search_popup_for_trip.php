<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); ?>
<!--<link href="<?= CSS ?>/stylesheets.css" rel="stylesheet" type="text/css" />
<? if (lang('global_lang') == 'ar'): ?>
                    <link href="<?= CSS ?>/stylesheet-ar.css" rel="stylesheet" type="text/css" />
<? else: ?><? endif ?>
<script type="text/javascript" src='<?= JS ?>/plugins/jquery/jquery-1.9.1.min.js'></script>
<script type="text/javascript" src='<?= JS ?>/plugins/jquery/jquery-ui-1.10.1.custom.min.js'></script>
<script type="text/javascript" src='<?= JS ?>/plugins/bootstrap/bootstrap.min.js'></script>
<link href="<?= CSS_JS ?>/dateandtime/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src='<?= CSS_JS ?>/dateandtime/bootstrap-datetimepicker.min.js'></script>-->
<style>

    body{
        height: auto!important;
        overflow: auto !important;
        background:none transparent !important;
        overflow: hidden;
        min-height: 1%;
    }
    .modal-body{
        width: 96%;
        padding:0 2% ;
    }
    .row-fluid{
        padding-top: 5px !important;
        padding-bottom: 5px !important;
    }
</style>

<!-- multi selection choosen -->
<link rel="stylesheet" href="<?= NEW_CSS_JS ?>/multi-select-chosen/chosen.css">
<script src="<?= NEW_CSS_JS ?>/multi-select-chosen/chosen.jquery.js" type="text/javascript"></script>



<div class="modal-body">
    <style>
        .updated_msg{
            display:none;
            background-color:#ccee97;
            font-weight: bold;
            text-align: center;
            border:3px solid #cccdc9; 
            padding:20px;  
            margin-bottom:10px;
            border-radius:15px;
            margin:10px; 
        }
        .wizerd-div,.row-form{width: 96%;margin:6% 2%;}
        label{padding: 0;}

        .popup_width_25 {
            width: 25%;
            float:right;
        }
 
        .pop_up_search{
            float: right;
            margin-top: 20px;
            margin-right: 35px;
        }

    </style>



    <div class="wizerd-div">
        <a><?php echo lang('global_search') ?></a>

    </div>

    <?= form_open('flight_availabilities/popup_trip/' . $flight_trip_type . '/' . $trip_id, ' name="frm_search_popup_for_trip" id="frm_search_popup_for_trip" method="get"') ?>

    <input type='hidden' name='flight_trip_type'  id='flight_trip_type'  value='<?= $flight_trip_type ?>'/>
    <input type='hidden' name='trip_id'  id='trip_id'  value='<?= $trip_id ?>'/>

    <div class="row-fluid" style="overflow: visible">


        <div class="popup_width_25">
            <div class="span2 TAL Pleft10">
                <label class="popup_width_content_30"><?= lang('pnr') ?>:</label>
            </div>
            <div class="span8">
                <?php echo form_input('pnr', set_value('pnr', ''), ' id="pnr" class="input-full" '); ?>
            </div>
        </div>

        <div class="popup_width_25">
            <div class="span4 TAL Pleft10">
                <label><?= lang('flight_number') ?>:</label>
            </div>
            <div class="span8">
                <?php echo form_input('flight_number', set_value('flight_number', ''), ' id="flight_number" class="input-full" '); ?>
            </div>
        </div>
        <div class="popup_width_25">
            <div class="span4 TAL Pleft10">
                <label><?= lang('departure_date') ?>:</label>
            </div>
            <div class="span8">
                <input class="datepicker" name="departure_date" id="departure_date" type="text" value="">
            </div>
        </div>
        <div class="pop_up_search"><input type="submit" name="search" value="<?= lang('global_search') ?>" class="btn btn-primary" /></div>
    </div>

    

    <?= form_close() ?>


    <?= form_open('flight_availabilities/popup_trip/' . $flight_trip_type . '/' . $trip_id, ' name="frm_search_popup_for_trip_table" id="frm_search_popup_for_trip_table" method="get"') ?>



    <div class="row-fluid">
        <table cellpadding="0" class="" cellspacing="0" width="100%" id="tbl_flight_availabilities">
            <thead>
                <tr>
                    <th><input type="checkbox" class="checkall" id="checkall" onchange='checkUncheck()'/> </th>
                    <th><?= lang('pnr') ?></th>

                </tr>
            </thead>
            <tbody>
                <? if (isset($results)) { ?>
                    <?
                    foreach ($results as $value) {
                        $this->flight_availabilities_model->erp_flight_availability_id = $value->erp_flight_availability_id;
						$this->flight_availabilities_model->arrival_date = $arrival_date;
						
                        //$this->flight_availabilities_model->erp_path_type_id = $flight_trip_type;

                        $flight_availabilities_details_rows = $this->flight_availabilities_model->get_details_for_trip();

                        $ports = '';

                        $safa_externaltriptype_name = '';

                        $start_datetime = '';
                        $end_datetime = '';
                        $loop_counter = 0;

                        $erp_port_id_from = 0;
                        $erp_port_id_to = 0;
                        $start_ports_name = '';
                        $end_ports_name = '';
                        
                        if(count($flight_availabilities_details_rows)>0) {
                        ?>
                        <tr id="row_<?php echo $value->erp_flight_availability_id; ?>">
                            <td><input type="checkbox" name="seats[]" id="seats[]"  value="<?= $value->erp_flight_availability_id ?>" title=""/></td>

                            <td>
                                <input type='hidden' name='trip_safa_transporter_id_<?= $value->erp_flight_availability_id ?>'  id='trip_safa_transporter_id_<?= $value->erp_flight_availability_id ?>'  value='<?= $value->safa_transporter_id ?>'/>
                                <input type='hidden' name='trip_safa_transporter_name_<?= $value->erp_flight_availability_id ?>'  id='trip_safa_transporter_name_<?= $value->erp_flight_availability_id ?>'  value='<?= $value->safa_transporter_name ?>'/>

                                <table>
                                    <thead>
                                        <tr>
                                            <th><?= lang('flight') ?></th>
                                            <th><?= lang('erp_path_type_id') ?></th>
                                            <th><?= lang('flight_availabilities_airlines') ?></th>
                                            <th><?= lang('flight_availabilities_flight_number') ?></th>
                                            <th><?= lang('flight_availabilities_class') ?></th>
                                            <th><?= lang('flight_availabilities_date') ?></th>
                                            <th><?= lang('arrival_date') ?></th>
                                            <th><?= lang('flight_availabilities_airports') ?></th>
                                            <th><?= lang('flight_availabilities_status') ?></th>
                                            <th><?= lang('flight_availabilities_seat_count') ?></th>
                                            <th><?= lang('flight_availabilities_department_time') ?></th>
                                            <th><?= lang('flight_availabilities_arrival_time') ?></th>


                                        </tr>
                                    </thead>
                                    <tbody>

                                        <?php
                                        foreach ($flight_availabilities_details_rows as $flight_availabilities_details_row) {
                                            if ($loop_counter == 0) {
                                                $safa_externaltriptype_name = $flight_availabilities_details_row->safa_externaltriptype_name;
                                                $erp_path_type_name = $flight_availabilities_details_row->erp_path_type_name;

                                                $erp_port_id_from = $flight_availabilities_details_row->erp_port_id_from;
                                                $start_ports_name = $flight_availabilities_details_row->start_ports_name;

                                                $erp_port_id_to = $flight_availabilities_details_row->erp_port_id_to;
                                                $end_ports_name = $flight_availabilities_details_row->end_ports_name;

                                                $ports = $flight_availabilities_details_row->start_ports_name . ' - ' . $flight_availabilities_details_row->end_ports_name;
                                                $start_datetime = $flight_availabilities_details_row->flight_date . ' - ' . $flight_availabilities_details_row->departure_time;
                                                $end_datetime = $flight_availabilities_details_row->flight_date . ' - ' . $flight_availabilities_details_row->arrival_time;
                                            } else {
                                                $safa_externaltriptype_name = $safa_externaltriptype_name . ' <br/> ' . $flight_availabilities_details_row->safa_externaltriptype_name;

                                                $erp_port_id_to = $flight_availabilities_details_row->erp_port_id_to;
                                                $end_ports_name = $flight_availabilities_details_row->end_ports_name;

                                                $ports = $ports . ' <br/> ' . $flight_availabilities_details_row->start_ports_name . ' - ' . $flight_availabilities_details_row->end_ports_name;
                                                $end_datetime = $flight_availabilities_details_row->flight_date . ' - ' . $flight_availabilities_details_row->arrival_time;
                                            }
                                            $loop_counter++;
                                            ?>



                                            <tr>

                                                <td><?php echo $flight_availabilities_details_row->safa_externaltriptype_name; ?>
                                                    <input type='hidden' name='safa_externaltriptype_name_<?= $value->erp_flight_availability_id ?>'  id='safa_externaltriptype_name_<?= $value->erp_flight_availability_id ?>'  value='<?= $flight_availabilities_details_row->safa_externaltriptype_name ?>'/>
                                                </td>

                                                <td><?php echo $flight_availabilities_details_row->erp_path_type_name; ?>
                                                    <input type='hidden' name='safa_externaltriptype_name_<?= $value->erp_flight_availability_id ?>'  id='safa_externaltriptype_name_<?= $value->erp_flight_availability_id ?>'  value='<?= $flight_availabilities_details_row->erp_path_type_name ?>'/>
                                                </td>

                                                <td><?php echo $flight_availabilities_details_row->airline; ?></td>
                                                <td><?php echo $flight_availabilities_details_row->flight_number; ?></td>
                                                <td><?php echo $flight_availabilities_details_row->erp_flight_classes_name; ?></td>

                                                <td>

                                                <!--<input type='hidden' name='departure_date_<?= $value->erp_flight_availability_id ?>'  id='departure_date_<?= $value->erp_flight_availability_id ?>'  value='<?= $start_datetime ?>'/>
                                                    <?php echo $start_datetime; ?>
                                                    -->
                                                    <input type='hidden' name='departure_date_<?= $value->erp_flight_availability_id ?>'  id='departure_date_<?= $value->erp_flight_availability_id ?>'  value='<?= $flight_availabilities_details_row->flight_date . ' - ' . $flight_availabilities_details_row->departure_time ?>'/>
                                                    <?php echo $flight_availabilities_details_row->flight_date . ' - ' . $flight_availabilities_details_row->departure_time; ?>
                                                </td>

                                                <td>

                                                <!--<input type='hidden' name='arrival_date_<?= $value->erp_flight_availability_id ?>'  id='arrival_date_<?= $value->erp_flight_availability_id ?>'  value='<?= $start_datetime ?>'/>
                                                    <?php echo $start_datetime; ?>
                                                    -->
                                                    <input type='hidden' name='arrival_date_<?= $value->erp_flight_availability_id ?>'  id='arrival_date_<?= $value->erp_flight_availability_id ?>'  value='<?= $flight_availabilities_details_row->flight_date . ' - ' . $flight_availabilities_details_row->departure_time ?>'/>
                                                    <?php echo $flight_availabilities_details_row->arrival_date . ' - ' . $flight_availabilities_details_row->arrival_time; ?>
                                                </td>


                                                <td>
                                                    <input type='hidden' name='erp_port_id_from_<?= $value->erp_flight_availability_id ?>'  id='erp_port_id_from_<?= $value->erp_flight_availability_id ?>'  value='<?= $flight_availabilities_details_row->erp_port_id_from ?>'/>
                                                    <input type='hidden' name='erp_port_id_to_<?= $value->erp_flight_availability_id ?>'  id='erp_port_id_to_<?= $value->erp_flight_availability_id ?>'  value='<?= $flight_availabilities_details_row->erp_port_id_to ?>'/>

                                                    <input type='hidden' name='start_ports_name_<?= $value->erp_flight_availability_id ?>'  id='start_ports_name_<?= $value->erp_flight_availability_id ?>'  value='<?= $flight_availabilities_details_row->start_ports_name; ?>'/>
                                                    <input type='hidden' name='end_ports_name_<?= $value->erp_flight_availability_id ?>'  id='end_ports_name_<?= $value->erp_flight_availability_id ?>'  value='<?= $flight_availabilities_details_row->end_ports_name; ?>'/>


                                                    <?= $flight_availabilities_details_row->start_ports_name ?> - <?= $flight_availabilities_details_row->end_ports_name ?>
                                                    <input type='hidden' name='ports_name_<?= $value->erp_flight_availability_id ?>'  id='ports_name_<?= $value->erp_flight_availability_id ?>'  value='<?= $flight_availabilities_details_row->start_ports_name ?> - <?= $flight_availabilities_details_row->end_ports_name ?>'/>


                                                    <!--<?php echo $ports; ?>
                                                    <input type='hidden' name='ports_name_<?= $value->erp_flight_availability_id ?>'  id='ports_name_<?= $value->erp_flight_availability_id ?>'  value='<?= $ports ?>'/>-->

                                                </td>

                                                <td><?php echo $flight_availabilities_details_row->erp_flight_status_name; ?></td>


                                                <td>
                                                    <input type='hidden' name='available_seats_count_<?= $value->erp_flight_availability_id ?>'  id='available_seats_count_<?= $value->erp_flight_availability_id ?>'  value='<?= $flight_availabilities_details_row->available_seats_count ?>'/>
                                                    <input type='text' name='available_seats_count_text_<?= $value->erp_flight_availability_id ?>'  id='available_seats_count_text_<?= $value->erp_flight_availability_id ?>'  value='<?= $flight_availabilities_details_row->available_seats_count ?>' readonly="readonly" disabled="disabled"/>

                                                    <!-- <?= $value->available_seats_count ?> -->

                                                </td>


                                                <td>
                                                    <?php echo $flight_availabilities_details_row->departure_time; ?>
                                                </td>
                                                <td>
                                                    <?php echo $flight_availabilities_details_row->arrival_time; ?>
                                                </td>



                                            </tr>


                                        <?php } ?>

                                    </tbody>

                                </table>

                            </td>


                        </tr>

						<?php } ?>




                        <tr style="display: none">
                            <td><input type="checkbox" name="seats[]" id="seats[]"  value="<?= $value->erp_flight_availability_id ?>" title=""/></td>

                            <td colspan="2">
                                <input type='hidden' name='trip_safa_transporter_id_<?= $value->erp_flight_availability_id ?>'  id='trip_safa_transporter_id_<?= $value->erp_flight_availability_id ?>'  value='<?= $value->safa_transporter_id ?>'/>
                                <input type='hidden' name='trip_safa_transporter_name_<?= $value->erp_flight_availability_id ?>'  id='trip_safa_transporter_name_<?= $value->erp_flight_availability_id ?>'  value='<?= $value->safa_transporter_name ?>'/>

                                <table>

                                    <tbody id="going_<?php echo $value->erp_flight_availability_id; ?>">



                                        <?php
                                        $loop_counter = 0;
                                        foreach ($flight_availabilities_details_rows as $flight_availabilities_details_row) {
                                            if ($loop_counter == 0) {
                                                $safa_externaltriptype_name = $flight_availabilities_details_row->safa_externaltriptype_name;
                                                $erp_path_type_name = $flight_availabilities_details_row->erp_path_type_name;

                                                $erp_port_id_from = $flight_availabilities_details_row->erp_port_id_from;
                                                $start_ports_name = $flight_availabilities_details_row->start_ports_name;

                                                $erp_port_id_to = $flight_availabilities_details_row->erp_port_id_to;
                                                $end_ports_name = $flight_availabilities_details_row->end_ports_name;

                                                $ports = $flight_availabilities_details_row->start_ports_name . ' - ' . $flight_availabilities_details_row->end_ports_name;
                                                $start_datetime = $flight_availabilities_details_row->flight_date . ' - ' . $flight_availabilities_details_row->departure_time;
                                                $end_datetime = $flight_availabilities_details_row->flight_date . ' - ' . $flight_availabilities_details_row->arrival_time;
                                            } else {
                                                $safa_externaltriptype_name = $safa_externaltriptype_name . ' <br/> ' . $flight_availabilities_details_row->safa_externaltriptype_name;

                                                $erp_port_id_to = $flight_availabilities_details_row->erp_port_id_to;
                                                $end_ports_name = $flight_availabilities_details_row->end_ports_name;

                                                $ports = $ports . ' <br/> ' . $flight_availabilities_details_row->start_ports_name . ' - ' . $flight_availabilities_details_row->end_ports_name;
                                                $end_datetime = $flight_availabilities_details_row->flight_date . ' - ' . $flight_availabilities_details_row->arrival_time;
                                            }
                                            $loop_counter++;

                                            if ($flight_availabilities_details_row->safa_externaltriptype_id == 1) {
                                                ?>



                                                <tr>

                                                                <!--<td><?php echo $flight_availabilities_details_row->safa_externaltriptype_name; ?>
                                                                 <input type='hidden' name='safa_externaltriptype_name_<?= $value->erp_flight_availability_id ?>'  id='safa_externaltriptype_name_<?= $value->erp_flight_availability_id ?>'  value='<?= $flight_availabilities_details_row->safa_externaltriptype_name ?>'/>
                                                                </td>

                                                    --><td>

                                                        <?php if ($loop_counter == 1) { ?>
                                                            <input type="hidden" name="trip_going_erp_flight_availability_id<?= $value->erp_flight_availability_id ?>" id="trip_going_erp_flight_availability_id<?= $value->erp_flight_availability_id ?>"  value="<?= $value->erp_flight_availability_id ?>" />
                                                            <input type='hidden' name='trip_safa_transporter_id_<?= $value->erp_flight_availability_id ?>'  id='trip_safa_transporter_id_<?= $value->erp_flight_availability_id ?>'  value='<?= $value->safa_transporter_id ?>'/>
                                                            <input type='hidden' name='trip_safa_transporter_name_<?= $value->erp_flight_availability_id ?>'  id='trip_safa_transporter_name_<?= $value->erp_flight_availability_id ?>'  value='<?= $value->safa_transporter_name ?>'/>
                                                        <?php } ?>

                                                        <?php echo $flight_availabilities_details_row->erp_path_type_name; ?>
                                                        <input type='hidden' name='safa_externaltriptype_name_<?= $value->erp_flight_availability_id ?>'  id='safa_externaltriptype_name_<?= $value->erp_flight_availability_id ?>'  value='<?= $flight_availabilities_details_row->erp_path_type_name ?>'/>
                                                    </td>

                                                    <td><?php echo $flight_availabilities_details_row->airline; ?></td>
                                                    <td><?php echo $flight_availabilities_details_row->flight_number; ?></td>
                                                    <td><?php echo $flight_availabilities_details_row->erp_flight_classes_name; ?></td>

                                                    <td>

                                                                <!--<input type='hidden' name='departure_date_<?= $value->erp_flight_availability_id ?>'  id='departure_date_<?= $value->erp_flight_availability_id ?>'  value='<?= $start_datetime ?>'/>
                                                        <?php echo $start_datetime; ?>
                                                        -->
                                                        <input type='hidden' name='departure_date_<?= $value->erp_flight_availability_id ?>'  id='departure_date_<?= $value->erp_flight_availability_id ?>'  value='<?= $flight_availabilities_details_row->flight_date . ' - ' . $flight_availabilities_details_row->departure_time ?>'/>
                                                        <?php echo $flight_availabilities_details_row->flight_date . ' - ' . $flight_availabilities_details_row->departure_time; ?>
                                                    </td>

                                                    <td>

                                                <!--<input type='hidden' name='arrival_date_<?= $value->erp_flight_availability_id ?>'  id='arrival_date_<?= $value->erp_flight_availability_id ?>'  value='<?= $start_datetime ?>'/>
                                                    <?php echo $start_datetime; ?>
                                                    -->
                                                    <input type='hidden' name='arrival_date_<?= $value->erp_flight_availability_id ?>'  id='arrival_date_<?= $value->erp_flight_availability_id ?>'  value='<?= $flight_availabilities_details_row->flight_date . ' - ' . $flight_availabilities_details_row->departure_time ?>'/>
                                                    <?php echo $flight_availabilities_details_row->arrival_date . ' - ' . $flight_availabilities_details_row->arrival_time; ?>
                                                	</td>


                                                    <td>
                                                        <input type='hidden' name='erp_port_id_from_<?= $value->erp_flight_availability_id ?>'  id='erp_port_id_from_<?= $value->erp_flight_availability_id ?>'  value='<?= $flight_availabilities_details_row->erp_port_id_from ?>'/>
                                                        <input type='hidden' name='erp_port_id_to_<?= $value->erp_flight_availability_id ?>'  id='erp_port_id_to_<?= $value->erp_flight_availability_id ?>'  value='<?= $flight_availabilities_details_row->erp_port_id_to ?>'/>

                                                        <input type='hidden' name='start_ports_name_<?= $value->erp_flight_availability_id ?>'  id='start_ports_name_<?= $value->erp_flight_availability_id ?>'  value='<?= $flight_availabilities_details_row->start_ports_name; ?>'/>
                                                        <input type='hidden' name='end_ports_name_<?= $value->erp_flight_availability_id ?>'  id='end_ports_name_<?= $value->erp_flight_availability_id ?>'  value='<?= $flight_availabilities_details_row->end_ports_name; ?>'/>


                                                        <?= $flight_availabilities_details_row->start_ports_name ?> - <?= $flight_availabilities_details_row->end_ports_name ?>
                                                        <input type='hidden' name='ports_name_<?= $value->erp_flight_availability_id ?>'  id='ports_name_<?= $value->erp_flight_availability_id ?>'  value='<?= $flight_availabilities_details_row->start_ports_name ?> - <?= $flight_availabilities_details_row->end_ports_name ?>'/>


                                                        <!--<?php echo $ports; ?>
                                                        <input type='hidden' name='ports_name_<?= $value->erp_flight_availability_id ?>'  id='ports_name_<?= $value->erp_flight_availability_id ?>'  value='<?= $ports ?>'/>-->

                                                    </td>

                                                    <td><?php echo $flight_availabilities_details_row->erp_flight_status_name; ?></td>


                                                    <td>
                                                        <input type='hidden' name='available_seats_count_<?= $value->erp_flight_availability_id ?>'  id='available_seats_count_<?= $value->erp_flight_availability_id ?>'  value='<?= $flight_availabilities_details_row->available_seats_count ?>'/>
                                                        <input type='text' name='available_seats_count_text_<?= $value->erp_flight_availability_id ?>'  id='available_seats_count_text_<?= $value->erp_flight_availability_id ?>'  value='<?= $flight_availabilities_details_row->available_seats_count ?>' readonly="readonly" disabled="disabled"/>

                                                        <!-- <?= $value->available_seats_count ?> -->

                                                    </td>


                                                                <!--<td>
                                                    <?php echo $flight_availabilities_details_row->departure_time; ?>
                                                                </td>
                                                                <td>
                                                    <?php echo $flight_availabilities_details_row->arrival_time; ?>
                                                                </td>



                                                    --></tr>


                                                <?php
                                            }
                                        }
                                        ?>

                                    </tbody>

                                </table>

                            </td>


                        </tr>



                        <tr  style="display: none">
                            <td><input type="checkbox" name="seats[]" id="seats[]"  value="<?= $value->erp_flight_availability_id ?>" title=""/></td>

                            <td colspan="2">
                                <input type='hidden' name='trip_safa_transporter_id_<?= $value->erp_flight_availability_id ?>'  id='trip_safa_transporter_id_<?= $value->erp_flight_availability_id ?>'  value='<?= $value->safa_transporter_id ?>'/>
                                <input type='hidden' name='trip_safa_transporter_name_<?= $value->erp_flight_availability_id ?>'  id='trip_safa_transporter_name_<?= $value->erp_flight_availability_id ?>'  value='<?= $value->safa_transporter_name ?>'/>

                                <table>

                                    <tbody id="return_<?php echo $value->erp_flight_availability_id; ?>">


                                        <?php
                                        foreach ($flight_availabilities_details_rows as $flight_availabilities_details_row) {
                                            if ($loop_counter == 0) {
                                                $safa_externaltriptype_name = $flight_availabilities_details_row->safa_externaltriptype_name;
                                                $erp_path_type_name = $flight_availabilities_details_row->erp_path_type_name;

                                                $erp_port_id_from = $flight_availabilities_details_row->erp_port_id_from;
                                                $start_ports_name = $flight_availabilities_details_row->start_ports_name;

                                                $erp_port_id_to = $flight_availabilities_details_row->erp_port_id_to;
                                                $end_ports_name = $flight_availabilities_details_row->end_ports_name;

                                                $ports = $flight_availabilities_details_row->start_ports_name . ' - ' . $flight_availabilities_details_row->end_ports_name;
                                                $start_datetime = $flight_availabilities_details_row->flight_date . ' - ' . $flight_availabilities_details_row->departure_time;
                                                $end_datetime = $flight_availabilities_details_row->flight_date . ' - ' . $flight_availabilities_details_row->arrival_time;
                                            } else {
                                                $safa_externaltriptype_name = $safa_externaltriptype_name . ' <br/> ' . $flight_availabilities_details_row->safa_externaltriptype_name;

                                                $erp_port_id_to = $flight_availabilities_details_row->erp_port_id_to;
                                                $end_ports_name = $flight_availabilities_details_row->end_ports_name;

                                                $ports = $ports . ' <br/> ' . $flight_availabilities_details_row->start_ports_name . ' - ' . $flight_availabilities_details_row->end_ports_name;
                                                $end_datetime = $flight_availabilities_details_row->flight_date . ' - ' . $flight_availabilities_details_row->arrival_time;
                                            }
                                            $loop_counter++;

                                            if ($flight_availabilities_details_row->safa_externaltriptype_id == 2) {
                                                ?>



                                                <tr>

                                                                <!--<td><?php echo $flight_availabilities_details_row->safa_externaltriptype_name; ?>
                                                                 <input type='hidden' name='safa_externaltriptype_name_<?= $value->erp_flight_availability_id ?>'  id='safa_externaltriptype_name_<?= $value->erp_flight_availability_id ?>'  value='<?= $flight_availabilities_details_row->safa_externaltriptype_name ?>'/>
                                                                </td>

                                                    --><td>
                                                
                                                 <input type="hidden" name="trip_going_erp_flight_availability_id<?= $value->erp_flight_availability_id ?>" id="trip_going_erp_flight_availability_id<?= $value->erp_flight_availability_id ?>"  value="<?= $value->erp_flight_availability_id ?>" />
                                                             
                                                <?php echo $flight_availabilities_details_row->erp_path_type_name; ?>
                                                        <input type='hidden' name='safa_externaltriptype_name_<?= $value->erp_flight_availability_id ?>'  id='safa_externaltriptype_name_<?= $value->erp_flight_availability_id ?>'  value='<?= $flight_availabilities_details_row->erp_path_type_name ?>'/>
                                                    </td>

                                                    <td><?php echo $flight_availabilities_details_row->airline; ?></td>
                                                    <td><?php echo $flight_availabilities_details_row->flight_number; ?></td>
                                                    <td><?php echo $flight_availabilities_details_row->erp_flight_classes_name; ?></td>

                                                    <td>

                                                                <!--<input type='hidden' name='departure_date_<?= $value->erp_flight_availability_id ?>'  id='departure_date_<?= $value->erp_flight_availability_id ?>'  value='<?= $start_datetime ?>'/>
                                                        <?php echo $start_datetime; ?>
                                                        -->
                                                        <input type='hidden' name='departure_date_<?= $value->erp_flight_availability_id ?>'  id='departure_date_<?= $value->erp_flight_availability_id ?>'  value='<?= $flight_availabilities_details_row->flight_date . ' - ' . $flight_availabilities_details_row->departure_time ?>'/>
                                                        <?php echo $flight_availabilities_details_row->flight_date . ' - ' . $flight_availabilities_details_row->departure_time; ?>
                                                    </td>

                                                    <td>

                                                <!--<input type='hidden' name='arrival_date_<?= $value->erp_flight_availability_id ?>'  id='arrival_date_<?= $value->erp_flight_availability_id ?>'  value='<?= $start_datetime ?>'/>
                                                    <?php echo $start_datetime; ?>
                                                    -->
                                                    <input type='hidden' name='arrival_date_<?= $value->erp_flight_availability_id ?>'  id='arrival_date_<?= $value->erp_flight_availability_id ?>'  value='<?= $flight_availabilities_details_row->flight_date . ' - ' . $flight_availabilities_details_row->departure_time ?>'/>
                                                    <?php echo $flight_availabilities_details_row->arrival_date . ' - ' . $flight_availabilities_details_row->arrival_time; ?>
                                                </td>


                                                    <td>
                                                        <input type='hidden' name='erp_port_id_from_<?= $value->erp_flight_availability_id ?>'  id='erp_port_id_from_<?= $value->erp_flight_availability_id ?>'  value='<?= $flight_availabilities_details_row->erp_port_id_from ?>'/>
                                                        <input type='hidden' name='erp_port_id_to_<?= $value->erp_flight_availability_id ?>'  id='erp_port_id_to_<?= $value->erp_flight_availability_id ?>'  value='<?= $flight_availabilities_details_row->erp_port_id_to ?>'/>

                                                        <input type='hidden' name='start_ports_name_<?= $value->erp_flight_availability_id ?>'  id='start_ports_name_<?= $value->erp_flight_availability_id ?>'  value='<?= $flight_availabilities_details_row->start_ports_name; ?>'/>
                                                        <input type='hidden' name='end_ports_name_<?= $value->erp_flight_availability_id ?>'  id='end_ports_name_<?= $value->erp_flight_availability_id ?>'  value='<?= $flight_availabilities_details_row->end_ports_name; ?>'/>


                                                        <?= $flight_availabilities_details_row->start_ports_name ?> - <?= $flight_availabilities_details_row->end_ports_name ?>
                                                        <input type='hidden' name='ports_name_<?= $value->erp_flight_availability_id ?>'  id='ports_name_<?= $value->erp_flight_availability_id ?>'  value='<?= $flight_availabilities_details_row->start_ports_name ?> - <?= $flight_availabilities_details_row->end_ports_name ?>'/>


                                                        <!--<?php echo $ports; ?>
                                                        <input type='hidden' name='ports_name_<?= $value->erp_flight_availability_id ?>'  id='ports_name_<?= $value->erp_flight_availability_id ?>'  value='<?= $ports ?>'/>-->

                                                    </td>

                                                    <td><?php echo $flight_availabilities_details_row->erp_flight_status_name; ?></td>


                                                    <td>
                                                        <input type='hidden' name='available_seats_count_<?= $value->erp_flight_availability_id ?>'  id='available_seats_count_<?= $value->erp_flight_availability_id ?>'  value='<?= $flight_availabilities_details_row->available_seats_count ?>'/>
                                                        <input type='text' name='available_seats_count_text_<?= $value->erp_flight_availability_id ?>'  id='available_seats_count_text_<?= $value->erp_flight_availability_id ?>'  value='<?= $flight_availabilities_details_row->available_seats_count ?>' readonly="readonly" disabled="disabled"/>

                                                        <!-- <?= $value->available_seats_count ?> -->

                                                    </td><!--
                                                    
                                                    
                                                    <td>
                                                    <?php echo $flight_availabilities_details_row->departure_time; ?>
                                                    </td>
                                                    <td>
                                                    <?php echo $flight_availabilities_details_row->arrival_time; ?>
                                                    </td>
                                                    
                                                    
                                                    
                                                    --></tr>


                                                <?php
                                            }
                                        }
                                        ?>

                                    </tbody>

                                </table>

                            </td>


                        </tr>








                    <? } ?>
                <? } ?>
            </tbody>
        </table>
    </div>


    <div class="toolbar bottom TAC">
        <input  type ="button" value="<?= lang('global_submit') ?>" class="btn btn-primary" onclick="selectPNR();">
    </div>

    <?= form_close() ?>

</div>    



<script type="text/javascript">
//By Gouda.
    function checkUncheck()
    {

        for (var i = 0; i < document.frm_search_popup_for_trip_table.elements.length; i++)
        {
            if (document.frm_search_popup_for_trip_table.elements[i].type == 'checkbox' && document.frm_search_popup_for_trip_table.elements[i].id != 'checkall')
            {
                document.frm_search_popup_for_trip_table.elements[i].checked = document.frm_search_popup_for_trip_table.elements['checkall'].checked;
            }
        }
    }

    $(document).ready(function() {
        var config = {
            '.chosen-select': {width: "100%"},
            '.chosen-select-deselect': {allow_single_deselect: true},
            '.chosen-select-no-single': {disable_search_threshold: 10},
            '.chosen-select-no-results': {no_results_text: 'Oops, nothing found!'},
            '.chosen-select-width': {width: "95%"}
        }
        for (var selector in config) {
            $(selector).chosen(config[selector]);
        }


        // binds form submission and fields to the validation engine
        $("#frm_search_popup_for_trip_table").validationEngine({
            prettySelect: true,
            useSuffix: "_chosen",
            promptPosition: "topRight:-150"
                    //promptPosition : "bottomLeft"
        });


    });


    $('.datetimepicker').datetimepicker({
        dateFormat: "yy-mm-dd",
        //controlType: 'select',
        timeFormat: 'HH:mm'
    });

    $('.datepicker').datepicker({
        dateFormat: "yy-mm-dd",
        //controlType: 'select',
        timeFormat: 'HH:mm'
    });



</script>


<script type="text/javascript">

    function selectPNR()
    {
<?php
$flight_trip_type = 1;

if ($flight_trip_type == 1) {
    ?>
            var counter = parent.document.getElementById('hdn_trip_going_conuter').value;
<?php } else { ?>
            var counter = parent.document.getElementById('hdn_trip_return_conuter').value;
<?php } ?>


        //By Gouda
        var are_checked = false;
        for (var i = 0; i < document.frm_search_popup_for_trip_table.elements.length; i++)
        {
            if (document.frm_search_popup_for_trip_table.elements[i].checked) {
                are_checked = true;
            }
        }
        if (are_checked == false) {
            alert('<?php echo lang('global_select_one_record_at_least'); ?>');
            return;
        }

        for (var i = 0; i < document.frm_search_popup_for_trip_table.elements.length; i++)
        {
            if (document.frm_search_popup_for_trip_table.elements[i].type == 'checkbox' && document.frm_search_popup_for_trip_table.elements[i].id == 'seats[]') {
                if (document.frm_search_popup_for_trip_table.elements[i].checked) {

                    var erp_flight_availability_id = document.frm_search_popup_for_trip_table.elements[i].value;

                    var available_seats_count_value = document.getElementById('available_seats_count_' + erp_flight_availability_id).value;

                    /*
                     var required_seats_count_value= document.getElementById('required_seats_count_'+erp_flight_availability_id).value;
                     
                     if(!$.isNumeric(required_seats_count_value)){
                     alert('<?php echo lang('enter_true_value'); ?>');
                     document.getElementById('required_seats_count_'+erp_flight_availability_id).focus();
                     return;
                     } 
                     
                     if(parseInt(required_seats_count_value) > parseInt(available_seats_count_value)) {
                     alert('<?php echo lang('required_seats_count_more_than_available'); ?>');
                     document.getElementById('required_seats_count_'+erp_flight_availability_id).focus();
                     return;
                     }
                     */
                }
            }
        }


        var erp_flight_availability_ids_arr = new Array();
        if (parent.$("#safa_trip_going_ids").val() != null) {
            erp_flight_availability_ids_arr = parent.$("#safa_trip_going_ids").val();
        }

        for (var i = 0; i < document.frm_search_popup_for_trip_table.elements.length; i++)
        {
            if (document.frm_search_popup_for_trip_table.elements[i].type == 'checkbox' && document.frm_search_popup_for_trip_table.elements[i].id == 'seats[]') {
                if (document.frm_search_popup_for_trip_table.elements[i].checked) {

                    //document.frm_search_popup_for_trip_table.elements[i].value;

                    counter++;

                    if (counter > 100) {
                        alert("Only 100 trip_flight allow");
                        return false;
                    }


                    //vars
                    var erp_flight_availability_id = document.frm_search_popup_for_trip_table.elements[i].value;

                    erp_flight_availability_ids_arr[i] = erp_flight_availability_id;


                    var safa_trip_going_ids_selected_values = parent.$('#safa_trip_going_ids').val();
                    var are_safa_trip_going_selected = false;

                    if (safa_trip_going_ids_selected_values != null) {
                        for (var j = 0; j < safa_trip_going_ids_selected_values.length; j++) {
                            if (safa_trip_going_ids_selected_values[j] == erp_flight_availability_id) {
                                are_safa_trip_going_selected = true;
                            }
                        }
                    }


                    if (are_safa_trip_going_selected == false) {
                        $('#going_' + erp_flight_availability_id + ' > tr').each(function() {
                            var new_div_trip_going = $(document.createElement('tr'));
                            var going_flight_tr_html = $(this).html();
                            //alert(going_flight_tr_html);
                            new_div_trip_going.html(going_flight_tr_html);
                            new_div_trip_going.appendTo(parent.$("#div_trip_going_group"));
                        });


                        $('#return_' + erp_flight_availability_id + ' > tr').each(function() {
                            var new_div_trip_return = $(document.createElement('tr'));
                            var return_flight_tr_html = $(this).html();
                            //alert(trip_flight_tr_html);
                            new_div_trip_return.html(return_flight_tr_html);
                            new_div_trip_return.appendTo(parent.$("#div_trip_return_group"));

                        });
                    }


                    parent.$('.datepicker').datepicker({
                        dateFormat: "yy-mm-dd",
                        //controlType: 'select',
                        timeFormat: 'HH:mm'
                    });

                    parent.document.getElementById('hdn_trip_going_conuter').value = counter;

                }
            }
        }


        parent.$("#safa_trip_going_ids").val(erp_flight_availability_ids_arr);
        parent.$("#safa_trip_going_ids").trigger("chosen:updated");



        parent.$.fancybox.close();
    }

</script>



<script type="text/javascript">

    function selectSeats()
    {
<?php
$flight_trip_type = 1;

if ($flight_trip_type == 1) {
    ?>
            var counter = parent.document.getElementById('hdn_trip_going_conuter').value;
<?php } else { ?>
            var counter = parent.document.getElementById('hdn_trip_return_conuter').value;
<?php } ?>


        //By Gouda
        var are_checked = false;
        for (var i = 0; i < document.frm_search_popup_for_trip_table.elements.length; i++)
        {
            if (document.frm_search_popup_for_trip_table.elements[i].checked) {
                are_checked = true;
            }
        }
        if (are_checked == false) {
            alert('<?php echo lang('global_select_one_record_at_least'); ?>');
            return;
        }

        for (var i = 0; i < document.frm_search_popup_for_trip_table.elements.length; i++)
        {
            if (document.frm_search_popup_for_trip_table.elements[i].type == 'checkbox' && document.frm_search_popup_for_trip_table.elements[i].id == 'seats[]') {
                if (document.frm_search_popup_for_trip_table.elements[i].checked) {

                    var erp_flight_availability_id = document.frm_search_popup_for_trip_table.elements[i].value;

                    var available_seats_count_value = document.getElementById('available_seats_count_' + erp_flight_availability_id).value;

                    /*
                     var required_seats_count_value= document.getElementById('required_seats_count_'+erp_flight_availability_id).value;
                     
                     if(!$.isNumeric(required_seats_count_value)){
                     alert('<?php echo lang('enter_true_value'); ?>');
                     document.getElementById('required_seats_count_'+erp_flight_availability_id).focus();
                     return;
                     } 
                     
                     if(parseInt(required_seats_count_value) > parseInt(available_seats_count_value)) {
                     alert('<?php echo lang('required_seats_count_more_than_available'); ?>');
                     document.getElementById('required_seats_count_'+erp_flight_availability_id).focus();
                     return;
                     }
                     */
                }
            }
        }


        var erp_flight_availability_ids_arr = new Array();
        if (parent.$("#safa_trip_going_ids").val() != null) {
            erp_flight_availability_ids_arr = parent.$("#safa_trip_going_ids").val();
        }

        for (var i = 0; i < document.frm_search_popup_for_trip_table.elements.length; i++)
        {
            if (document.frm_search_popup_for_trip_table.elements[i].type == 'checkbox' && document.frm_search_popup_for_trip_table.elements[i].id == 'seats[]') {
                if (document.frm_search_popup_for_trip_table.elements[i].checked) {

                    //document.frm_search_popup_for_trip_table.elements[i].value;

                    counter++;

                    if (counter > 100) {
                        alert("Only 100 trip_flight allow");
                        return false;
                    }


                    //vars
                    var erp_flight_availability_id = document.frm_search_popup_for_trip_table.elements[i].value;

                    erp_flight_availability_ids_arr[i] = erp_flight_availability_id;

                    var trip_safa_transporter_id = document.getElementById('trip_safa_transporter_id_' + erp_flight_availability_id).value;
                    var trip_safa_transporter_name = document.getElementById('trip_safa_transporter_name_' + erp_flight_availability_id).value;


                    //var flight_number_value= document.getElementById('flight_number_'+erp_flight_availability_id).value;

                    var erp_port_id_from_value = document.getElementById('erp_port_id_from_' + erp_flight_availability_id).value;
                    var erp_port_id_to_value = document.getElementById('erp_port_id_to_' + erp_flight_availability_id).value;

                    var start_ports_name_value = document.getElementById('start_ports_name_' + erp_flight_availability_id).value;
                    var end_ports_name_value = document.getElementById('end_ports_name_' + erp_flight_availability_id).value;

                    var ports_value = document.getElementById('ports_name_' + erp_flight_availability_id).value;
                    var safa_externaltriptype_name = document.getElementById('safa_externaltriptype_name_' + erp_flight_availability_id).value;

                    var departure_date_value = document.getElementById('departure_date_' + erp_flight_availability_id).value;
                    var arrival_date_value = document.getElementById('arrival_date_' + erp_flight_availability_id).value;

                    if (erp_port_id_to_value == 6053 || erp_port_id_to_value == 6056 || erp_port_id_to_value == 6070) {
                        var trip_safa_externalsegmenttype_id = 1;
                        var trip_safa_externalsegmenttype_name = '<?php echo lang('arrival'); ?>';
                    } else if (erp_port_id_from_value == 6053 || erp_port_id_from_value == 6056 || erp_port_id_from_value == 6070) {
                        var trip_safa_externalsegmenttype_id = 2;
                        var trip_safa_externalsegmenttype_name = '<?php echo lang('departure'); ?>';
                    } else {
                        var trip_safa_externalsegmenttype_id = 4;
                        var trip_safa_externalsegmenttype_name = '<?php echo lang('transit'); ?>';
                    }



                    var available_seats_count_value = document.getElementById('available_seats_count_' + erp_flight_availability_id).value;
                    //var required_seats_count_value= document.getElementById('required_seats_count_'+erp_flight_availability_id).value;


<?php if ($flight_trip_type == 1) { ?>
                        var new_div_trip_going = $(document.createElement('tr'));
                        new_div_trip_going.html('<td rel="' + counter + '">' +
                                safa_externaltriptype_name +
                                '</td>' +
                                '<td >' +
                                '<input type="hidden" name="trip_going_safa_externalsegmenttype_id' + counter + '"  id="trip_going_safa_externalsegmenttype_id' + counter + '" value="' + trip_safa_externalsegmenttype_id + '" class="" rel="' + counter + '">' +
                                '   <input type="hidden" name="safa_trip_going_id' + counter + '"  id="safa_trip_going_id' + counter + '" class="safa_trip_going_id"/>' +
                                '   <input type="hidden" name="trip_going_erp_flight_availability_id' + counter + '"  id="trip_going_erp_flight_availability_id' + counter + '"  value="' + erp_flight_availability_id + '"/>' +
                                ports_value +
                                '</td>' +
                                '<td >' +
                                '    <input type="hidden" name="trip_going_departure_datetime' + counter + '"  id="trip_going_departure_datetime' + counter + '"   value="' + departure_date_value + '" class="" rel="' + counter + '" />' +
                                departure_date_value +
                                '</td>' +
                                '<!-- <td >' +
                                '   <input type="hidden" name="trip_going_airport_start' + counter + '" id="trip_going_airport_start' + counter + '" value="' + erp_port_id_from_value + '" class="">' +
                                start_ports_name_value +
                                '</td>' +
                                '<td >' +
                                '    <input type="hidden"  name="trip_going_airport_end' + counter + '" id="trip_going_airport_end' + counter + '" value="' + erp_port_id_to_value + '" class="">' +
                                end_ports_name_value +
                                '</td> -->' +
                                '<td >' +
                                '    <input type="hidden"  name="trip_going_arrival_datetime' + counter + '"  id="trip_going_arrival_datetime' + counter + '"  value="' + arrival_date_value + '" class="" rel="' + counter + '" />' +
                                arrival_date_value +
                                '</td>' +
                                '<td >' +
                                '    <input type="hidden" name="trip_going_safa_transporter_id' + counter + '" id="trip_going_safa_transporter_id' + counter + '" value="' + trip_safa_transporter_id + '" class="">' +
                                trip_safa_transporter_name +
                                '</td>' +
                                '<td >' +
                                available_seats_count_value +
                                '</td>' +
                                '<td>' +
                                '    <a href="javascript:void(0)" class="" id="hrf_remove_trip_going" name="' + counter + '"><span class="icon-trash"></span></a>' +
                                '</td>'
                                );



                        var safa_trip_going_ids_selected_values = parent.$('#safa_trip_going_ids').val();
                        var are_safa_trip_going_selected = false;

                        if (safa_trip_going_ids_selected_values != null) {
                            for (var j = 0; j < safa_trip_going_ids_selected_values.length; j++) {
                                if (safa_trip_going_ids_selected_values[j] == erp_flight_availability_id) {
                                    are_safa_trip_going_selected = true;
                                }
                            }
                            if (are_safa_trip_going_selected == false) {
                                new_div_trip_going.appendTo(parent.$("#div_trip_going_group"));
                            }
                        } else {
                            new_div_trip_going.appendTo(parent.$("#div_trip_going_group"));
                        }


                        parent.$('.datepicker').datepicker({
                            dateFormat: "yy-mm-dd",
                            //controlType: 'select',
                            timeFormat: 'HH:mm'
                        });

                        parent.document.getElementById('hdn_trip_going_conuter').value = counter;


                        //alert(counter);
<?php } else { ?>

                        var new_div_trip_return = $(document.createElement('tr')).attr("rel", counter);
                        new_div_trip_return.html('<td rel="' + counter + '">' +
                                '<input type="hidden" name="trip_return_safa_externalsegmenttype_id' + counter + '"  id="trip_return_safa_externalsegmenttype_id' + counter + '" value="' + trip_safa_externalsegmenttype_id + '" class="" rel="' + counter + '">' +
                                '   <input type="hidden" name="safa_trip_return_id' + counter + '"  id="safa_trip_return_id' + counter + '" class="safa_trip_return_id"/>' +
                                '   <input type="hidden" name="trip_return_erp_flight_availability_id' + counter + '"  id="trip_return_erp_flight_availability_id' + counter + '"  value="' + erp_flight_availability_id + '"/>' +
                                '    <input type="hidden" name="trip_return_departure_datetime' + counter + '"  id="trip_return_departure_datetime' + counter + '"   value="' + departure_date_value + '" class="" rel="' + counter + '" />' +
                                departure_date_value +
                                '</td>' +
                                '<td >' +
                                '   <input type="hidden" name="trip_return_airport_start' + counter + '" id="trip_return_airport_start' + counter + '" value="' + erp_port_id_from_value + '" class="">' +
                                start_ports_name_value +
                                '</td>' +
                                '<td >' +
                                '    <input type="hidden"  name="trip_return_airport_end' + counter + '" id="trip_return_airport_end' + counter + '" value="' + erp_port_id_to_value + '" class="">' +
                                end_ports_name_value +
                                '</td>' +
                                '<td >' +
                                '    <input type="hidden"  name="trip_return_arrival_datetime' + counter + '"  id="trip_return_arrival_datetime' + counter + '"  value="' + arrival_date_value + '" class="" rel="' + counter + '" />' +
                                arrival_date_value +
                                '</td>' +
                                '<td >' +
                                '    <input type="hidden" name="trip_return_safa_transporter_id' + counter + '" id="trip_return_safa_transporter_id' + counter + '" value="' + trip_safa_transporter_id + '" class="">' +
                                trip_safa_transporter_name +
                                '</td>' +
                                '<td >' +
                                available_seats_count_value +
                                '</td>' +
                                '<td>' +
                                '    <a href="javascript:void(0)" class="" id="hrf_remove_trip_return" name="' + counter + '"><span class="icon-trash"></span></a>' +
                                '</td>'
                                );


                        new_div_trip_return.appendTo(parent.$("#div_trip_return_group"));

                        parent.$('.datepicker').datepicker({
                            dateFormat: "yy-mm-dd",
                            //controlType: 'select',
                            timeFormat: 'HH:mm'
                        });

                        parent.document.getElementById('hdn_trip_return_conuter').value = counter;


<?php } ?>
                }
            }
        }


        parent.$("#safa_trip_going_ids").val(erp_flight_availability_ids_arr);
        parent.$("#safa_trip_going_ids").trigger("chosen:updated");



        parent.$.fancybox.close();
        //parent.Shadowbox.close();
    }

</script>

<script>
    parent.$('#div_trip_going_group :input').each(function() {
        var input_id = $(this).attr("id");
        //alert(input_name);
        if (input_id !== undefined) {
            if (input_id.startsWith('trip_going_required_seats_count')) {

                var input_counter = input_id.substr(31);
                //alert(input_counter);
                var erp_flight_availability_id = parent.$("#trip_going_erp_flight_availability_id" + input_counter).val();

                var input_value = parent.$("#" + input_id).val();

                //alert(input_value);
                $('#tbl_flight_availabilities :input').each(function() {

                    var popup_input_id = $(this).attr("id");
                    if (popup_input_id !== undefined) {

                        if (popup_input_id == 'available_seats_count_' + erp_flight_availability_id) {

                            var reset_available_seats_count = parseInt($('#available_seats_count_text_' + erp_flight_availability_id).val()) - parseInt(input_value);
                            $(this).val(reset_available_seats_count);
                            $('#available_seats_count_text_' + erp_flight_availability_id).val(reset_available_seats_count);

                        }
                    }
                })

            }
        }
    })
</script>