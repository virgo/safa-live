<div class="widget">
<div class="path-container Fright">
<div class="path-name Fright"><a href=""> <?php echo lang('global_system_management') ?>
</a></div>
<div class="path-arrow Fright"></div>
<div class="path-name Fright"><?php echo lang('new_message') ?></div>
</div>
</div>
<?php echo form_open_multipart(false, 'id="frm_messages" ') ?>
<div class="widget">
<div class="widget-header">

<div class="widget-header-icon Fright"><span class="icos-pencil2"></span>
</div>

<div class="widget-header-title Fright"><?php echo lang('ha_main_information') ?>

</div>
</div>

<div class="widget-container"><? if (validation_errors()) { ?> <?php echo validation_errors(); ?>
<? } ?> <!--
        <div class="row-form">
            <div class="span6">
                <div class="span4 TAL Pleft10">
                    <?php echo lang('company_type') ?>
                </div>
                <div class="span8">
                    <?php
                    echo form_dropdown('erp_company_type_id', $company_types, set_value('erp_company_type_id', ''),
                            'class="validate[required] chosen-select input-full chosen-rtl " style="width:100%;" id="erp_company_type_id"')
                    ?>
                </div>
            </div>

            <div class="span6">
                <div class="span4 TAL Pleft10">
						<?php echo lang('country') ?>
                </div>
                <div class="span8">
                    <?php echo form_multiselect('countries[]', $countries, set_value('countries', ''),
                            ' id="countries" class="chosen-select chosen-rtl input-full" disabled="disabled" multiple tabindex="4" data-placeholder="' . lang('global_all') . '"')
                    ?>   
                </div>
            </div>
        </div>
        
        
        <div class="row-form">
            <div class="span6">
                <div class="span4 TAL Pleft10">
                    <?php echo lang('company') ?>
                </div>
                <div class="span8">
					<?php echo form_multiselect('companies[]', array(), set_value('companies', ''),
					        ' id="companies" class="chosen-select chosen-rtl input-full"  multiple tabindex="4" data-placeholder="' . lang('global_all') . '"')
					?>
                </div>
            </div>
        </div>
        
         -->


<div class="row-form">
<div class="span6">
<div class="span4 TAL Pleft10"><?php echo lang('administrators') ?></div>
<div class="span8"><input type="checkbox" name="chk_administrators"
	id="chk_administrators"></input></div>
</div>


</div>

<div class="row-form">
<div class="span6">
<div class="span4 TAL Pleft10"><?php echo lang('uos') ?></div>
<div class="span8"><?php echo form_multiselect('uos[]', $uos, set_value('uos', ''),
					        ' id="uos" class="chosen-select chosen-rtl input-full"  multiple tabindex="4" data-placeholder="' . lang('global_select_from_menu') . '"')
?></div>
</div>


</div>

<div class="row-form">
<div class="span6">
<div class="span4 TAL Pleft10"><?php echo lang('eas') ?></div>

<div class="span8"><?php echo form_multiselect('eas_countries[]', $countries, set_value('eas_countries', ''),
                            ' id="eas_countries" class="chosen-select chosen-rtl input-full"  multiple tabindex="4" data-placeholder="' . lang('global_select_from_menu') . '"')
?></div>
</div>
<div class="span6">
<div class="span8"><?php echo form_multiselect('eas[]', array(), set_value('eas', ''),
					        ' id="eas" class="chosen-select chosen-rtl input-full"  multiple tabindex="4" data-placeholder="' . lang('global_select_from_menu') . '"')
?></div>
</div>
</div>


<div class="row-form">
<div class="span6">
<div class="span4 TAL Pleft10"><?php echo lang('hms') ?></div>

<div class="span8"><?php echo form_multiselect('hms[]', $hms, set_value('hms', ''),
					        ' id="hms" class="chosen-select chosen-rtl input-full"  multiple tabindex="4" data-placeholder="' . lang('global_select_from_menu') . '"')
?></div>
</div>
</div>

 
<div class="row-form">
<div class="span6">
<div class="span4 TAL Pleft10"><?php echo lang('itos') ?></div>

<div class="span8"><?php echo form_multiselect('itos_countries[]', $countries, set_value('itos_countries', ''),
                            ' id="itos_countries" class="chosen-select chosen-rtl input-full"  multiple tabindex="4" data-placeholder="' . lang('global_select_from_menu') . '"')
?></div>
</div>
<div class="span6">
<div class="span8"><?php echo form_multiselect('itos[]', array(), set_value('itos', ''),
					        ' id="itos" class="chosen-select chosen-rtl input-full"  multiple tabindex="4" data-placeholder="' . lang('global_select_from_menu') . '"')
?></div>
</div>
</div>
 






<div class="row-form">
<div class="span6">
<div class="span4 TAL Pleft10"><?php echo lang('message_subject') ?></div>
<div class="span8"><?php echo form_input('message_subject', set_value("message_subject"),
        " style='' id='message_subject' class='validate[required] input-full' ")
?></div>
</div>
</div>

<div class="row-form">
<div class="span6">
<div class="span4 TAL Pleft10"><?php echo lang('message_body') ?></div>
<div class="span8"><?php echo form_textarea('message_body', set_value("message_body"),
        "style='height:200px' class='validate[required] input-full' id='message_body'  ")
?></div>
</div>
</div>

<div class="row-form">
<div class="span6">
<div class="span4 TAL Pleft10"><?php echo lang('attachement') ?>:</div>
<div class="span8"><?php echo form_upload('attachement[]', set_value("attachement"),
        " multiple style='' id='attachement[]'  ")
?></div>

</div>

<div class="span6">
<div class="span4 TAL Pleft10"><?php echo lang('importance') ?>:</div>
<div class="span8"><?php echo form_dropdown('erp_importance_id', $importances, set_value('erp_importance_id', ''),
        'class="validate[required] chosen-select input-full chosen-rtl " style="width:100%;" id="erp_importance_id"')
?></div>
</div>
</div>

</div>
</div>
<div class="widget TAC"><input type="submit" class="btn" name="smt_send"
	value="<?php echo lang('send') ?>"
	style="margin: 10px; padding: 5px; height: auto"></div>
<?php echo form_close() ?>
<div class="footer"></div>
<? $this->load->view('hotel_availability/widgets/popup') ?>
<script type="text/javascript">
    $(document).ready(function() {
        var config = {
            '.chosen-select': {width: "100%"},
            '.chosen-select-deselect': {allow_single_deselect: true},
            '.chosen-select-no-single': {disable_search_threshold: 10},
            '.chosen-select-no-results': {no_results_text: 'Oops, nothing found!'},
            '.chosen-select-width': {width: "95%"}
        }
        for (var selector in config) {
            $(selector).chosen(config[selector]);
        }
        $("#erp_company_type_id").chosen();
        $("#frm_messages").validationEngine({
            prettySelect: true,
            useSuffix: "_chosen",
            promptPosition: "topRight:-150"
        });

		
        $('#erp_company_type_id').change(function()
        {
            var erp_company_type_id = $('#erp_company_type_id').val();
            var countries = $('#countries').val();
            if (erp_company_type_id == 1) {
                $('#countries').val({}).attr('disabled', 'disabled').chosen().trigger("chosen:updated");
                $('#companies').val({}).attr('disabled', 'disabled').chosen().trigger("chosen:updated");
                return true;
            } else if (erp_company_type_id == 2) {
                $('#countries').val({}).attr('disabled', 'disabled').chosen().trigger("chosen:updated");
                $('#companies').removeAttr('disabled').chosen().trigger("chosen:updated");
            } else {
                $('#countries').removeAttr('disabled').chosen().trigger("chosen:updated");
                $('#companies').removeAttr('disabled').chosen().trigger("chosen:updated");
            }
            var dataString = 'erp_company_type_id=' + erp_company_type_id + '&countries=' + countries;

            $.ajax
                    ({
                        type: 'POST',
                        url: '<?php echo base_url() . 'messages/getCompaniesByCompanyTypeAndCountries'; ?>',
                        data: dataString,
                        cache: false,
                        success: function(html)
                        {
                            //alert(html);
                            $("#companies").html(html);
                            $("#companies").trigger("chosen:updated");
                        }
                    });

        });

        $('#countries').change(function()
        {
            var erp_company_type_id = $('#erp_company_type_id').val();
            var countries = $(this).val();
            var dataString = 'erp_company_type_id=' + erp_company_type_id + '&countries=' + countries;
            $.ajax
                    ({
                        type: 'POST',
                        url: '<?php echo base_url() . 'messages/getCompaniesByCompanyTypeAndCountries'; ?>',
                        data: dataString,
                        cache: false,
                        success: function(html)
                        {
                            $("#companies").html(html);
                            $("#companies").trigger("chosen:updated");
                        }
                    });

        });













        $('#eas_countries').change(function()
                {
                    var erp_company_type_id = 3;
                    var countries = $(this).val();
                    var dataString = 'erp_company_type_id=' + erp_company_type_id + '&countries=' + countries;
                    $.ajax
                            ({
                                type: 'POST',
                                url: '<?php echo base_url() . 'messages/getEasByCompanyTypeAndCountries'; ?>',
                                data: dataString,
                                cache: false,
                                success: function(html)
                                {
                                    $("#eas").html(html);
                                    $("#eas").trigger("chosen:updated");
                                }
                            });

                });

		
        $('#itos_countries').change(function()
                {
                    var erp_company_type_id = 5;
                    var countries = $(this).val();
                    var dataString = 'erp_company_type_id=' + erp_company_type_id + '&countries=' + countries;
                    $.ajax
                            ({
                                type: 'POST',
                                url: '<?php echo base_url() . 'messages/getItosByCompanyTypeAndCountries'; ?>',
                                data: dataString,
                                cache: false,
                                success: function(html)
                                {
                                    $("#itos").html(html);
                                    $("#itos").trigger("chosen:updated");
                                }
                            });

                });



        $('#uos').change(function()
         {
        	if($(this).val()!=null) {
	           var uo_id = $(this).val();
	           //alert(uo_id);
	           if (uo_id.indexOf('all') >= 0) {
	        	   $("#uos").val([<?php foreach($uos_rows as $uos_row) { echo("'$uos_row->safa_uo_id',"); }?>]);
	           		$("#uos").trigger("chosen:updated");
	           }
        	}
         });

        $('#hms').change(function()
         {
        	if($(this).val()!=null) {
	           var hm_id = $(this).val();
	           //alert(hm_id);
	           if (hm_id.indexOf('all') >= 0) {
	           $("#hms").val([<?php foreach($hms_rows as $hms_row) { echo("'$hms_row->safa_hm_id',"); }?>]);
	           $("#hms").trigger("chosen:updated");
	           }
        	}
         });
		

        $('#eas_countries').change(function()
         {
        	if($(this).val()!=null) {
	           var eas_country_id = $(this).val();
	           //alert(eas_country_id);
	           if (eas_country_id.indexOf('all') >= 0) {
	           $("#eas_countries").val([<?php foreach($countries_rows as $countries_row) { echo("'$countries_row->erp_country_id',"); }?>]);
	           $("#eas_countries").trigger("chosen:updated");
	           }
        	}
         });

        $('#itos_countries').change(function()
         {
        	if($(this).val()!=null) {
	           var itos_country_id = $(this).val();
	           //alert(itos_country_id);
	           if (itos_country_id.indexOf('all') >= 0) {
	          //alert("<?php foreach($countries_rows as $countries_row) { echo("'$countries_row->erp_country_id',"); }?>");
	           $("#itos_countries").val([<?php foreach($countries_rows as $countries_row) { echo("'$countries_row->erp_country_id',"); }?>]);
	           $("#itos_countries").trigger("chosen:updated");
	           }
        	}
         });
       		
        $('#eas').change(function(e)
         {
        	if($(this).val()!=null) {
	           var ea_id = $(this).val();
	           //alert(ea_id);
	           if (ea_id.indexOf('all') >= 0) {
	
					var eas_select=document.getElementById("eas");
					var eas_select_values=new Array();
					for (var i = 0; i < eas_select.options.length; i++) {
						if(eas_select.options[i].value!='all') {
							eas_select_values[i]=eas_select.options[i].value;
						}
	        		  }
					//alert(eas_select_values);
	           		$("#eas").val(eas_select_values);
	           		$("#eas").trigger("chosen:updated");
	           		
	           }
        	}
         });

        $('#itos').change(function(e)
         {
           if($(this).val()!=null) {
	           var ito_id = $(this).val();
	           //alert(ito_id);
	           if (ito_id.indexOf('all') >= 0) {
	
	       			var itos_select=document.getElementById("itos");
	       			var itos_select_values=new Array();
	       			for (var i = 0; i < itos_select.options.length; i++) {
	       				if(itos_select.options[i].value!='all') {
	       					itos_select_values[i]=itos_select.options[i].value;
	       				}
	               		  }
	       			//alert(itos_select_values);
	           		$("#itos").val(itos_select_values);
	           		$("#itos").trigger("chosen:updated");
	           		
	           }
           }
         });
        
        
              		
               
    });
</script>
