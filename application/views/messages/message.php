<!DOCTYPE html>


<style>
    a{margin: 5px;}


    .reg-btn {padding: 2px 8px !important;
              color: #000333;
              background: #ddc889;
    }
    .logo2 {

        display: block;
        text-align: center;
        margin: 0 auto;

    }
    .banner2 {
        background-image: url("<?php echo STATIC_FOLDER_DIR; ?>/front/banner2.png");
        background-repeat: no-repeat;
        height: 260px;
        margin: 0 auto;
        position: relative;
        width: 640px;
        margin-top: 10px;
    }
    .forg {margin: 0 auto !important;

           float: none !important;
           overflow: hidden;

    }
    .reg2 {

        margin-top: 70px;
        margin-bottom: 70px;
    }
    .reg-btn2 {padding: 4px 10px !important;
               color: #000333;
               background: #ddc889;
               text-shadow: none;
               margin-top: 2px !important;

    }
    .log {
        margin-right: 10px;
        margin-top: -110px;
    }

    input, textarea, .uneditable-input{margin-bottom: 3px;}
    .wizerd-div {
        border-bottom: medium none !important;
        margin: -18px 0 20px;
        padding-top: 0;
    }
    .wizerd-div a {
        background: none repeat scroll 0 0 #FAFAFA;
        border: 1px solid #D5D6D6;
        border-radius: 49px;
        color: #663300;
        display: inline-block;
        margin: -23px 5px -17px -8px;
        padding: 39px 12px 13px;
        width: 90%;
        text-align: center;
    }
    a {
        color: #C09853;
    }
    .resalt-group2 {
        -moz-box-sizing: border-box;
        border: 2px solid #E1E1E1;
        border-radius: 20px;
        float: right;
        margin: 1% 2%;
        padding: 2%;
        width: 96%;

    }
    .pad {padding: 10px;}
    .t2 {font-size: 16px;
         font-weight: 500;
         margin-top: 35px;
         color: #444343;

    }
    .f1 {margin-bottom: 15px;
         margin-top: 10px;
         font-size: 13px;
         font-weight: 500;
         color: #88774A;
    }
    .false {color: #800000;
            margin-bottom: -15px;
            margin-top: 15px;
    }
    .wizerd-div a h3 {padding: -10px !important;
                      margin: 0px;
                      font-weight: 600;
                      font-size: 17px;
                      color: #8B742B;}
    .fancybox-inner {
        overflow: hidden !important;
    }

    
</style>

<?php
if ($mark_as_important == 1) {
    $mark_as_important_value = 0;
    $mark_as_important_link_text = lang("mark_as_un_important");
} else {
    $mark_as_important_value = 1;
    $mark_as_important_link_text = lang("mark_as_important");
}
?>




<div style="max-width: 700px;padding: 5px;"> 

    <div class="span12" style="background-color: rgba(240, 240, 240, 0.63);margin-bottom: 5px; border-radius: 25px;">
        <div class="span2">

            <div class="span12 message_label_bk"><label class="message_label"><?php echo $erp_importance_name; ?></label></div>       
            <div class="span12 TAC message_label_noti"><h3> <?php echo $from_name; ?></h3></div>

        </div>

        <div class="span10" style="border-bottom-right-radius: 15px;border-bottom-left-radius: 15px;background-color: rgba(219, 206, 179, 0.63);">

            <div class="span12 TAL" style="/* border-radius: 15px; */background-color: rgba(223, 215, 190, 0.77);">
                <h4 class="message_first_h4"><?php echo $message_subject; ?></h4>
            </div>

            <?php
            if (count($attachements) > 0) {
                ?>
                <div class="span12 TAL" style="background-color: #EDB;"><span class="att">

                        <?php
                        foreach ($attachements as $attachement) {
                            $path = $attachement->path;
                            $path = base_url() . $path;

                            $file_name_arr = explode('/', $path);
                            $file_name = end($file_name_arr);
                            echo "<a href='$path' target='_blank' >$file_name</a><br/>";
                        }
                        ?>

                    </span></div>
                <?php
            }
            ?>


            <div class="span12  ">

                <div class="span12  TAL"><p class="message_last_p">
                        <?php echo $message_body; ?>
                    </p></div>






            </div>
            <div class="span11 TAR message_last_datetime"> <?php echo $message_datetime; ?> </div>   
        </div> 



    </div>


    <div class="span12 TAR">
        <a href="#" class=""><span class="icon-star message_star" alt='<?php echo $mark_as_important_value; ?>' id='<?php echo $erp_messages_detail_id; ?>'></span></a>
        <a href="#"><span class="icon-trash message_del" id='<?php echo $erp_messages_detail_id; ?>'></span></a>
    </div>


</div>

<script>
    $(document).ready(function() {
        $('.message_del').click(function() {
            alert('<?php echo lang('global_deleted_message'); ?>');
            var message = $(this);
            $.get('<?= site_url('messages/hide') ?>/' + $(this).attr('id'), function(data) {
                //message.parent().remove();

            });
        });

        $('.message_star').on('click', function() {
            alert('<?php echo lang('global_updated_message'); ?>');
            $.get('<?= site_url('messages/markAsImportant') ?>/' + $(this).attr('id') + '/' + $(this).attr('alt'), function(data) {
            });
        });

    });

</script>   