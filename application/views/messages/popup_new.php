<script src="<?= NEW_CSS_JS ?>/jquery-1.9.1.js"></script>
<script src="<?= NEW_CSS_JS ?>/jquery-ui.js"></script>
<!-- accordion menu -->
<link rel="stylesheet" href="<?= NEW_CSS_JS ?>/accordionmenu.css" type="text/css" media="screen" />
 
<!-- multi selection choosen -->
<link rel="stylesheet" href="<?= NEW_CSS_JS ?>/multi-select-chosen/chosen.css">
<script src="<?= NEW_CSS_JS ?>/multi-select-chosen/chosen.jquery.js" type="text/javascript"></script>


        
<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); ?>
<link href="<?= CSS ?>/stylesheets.css" rel="stylesheet" type="text/css" />
<? if (lang('global_lang') == 'ar'): ?>
    <link href="<?= CSS ?>/stylesheet-ar.css" rel="stylesheet" type="text/css" />
<? else: ?><? endif ?>
<link href="<?= CSS_JS ?>/dateandtime/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css" />
<style>
body{
height: auto!important;
overflow: auto !important;
background:none transparent !important;
overflow: hidden;
min-height: 1%;
}

.row-fluid{
padding-top: 5px !important;
padding-bottom: 5px !important;
}

</style>





<div class="modal-header">
   <h3 id="myModalLabel"><?php echo  lang('new_message') ?></h3>
</div>
    
<?php echo  form_open_multipart(false, 'id="frm_messages" ') ?>


<div class="">
    

    <div class="modal-body">  
        <? if(validation_errors()){ ?>
            <?php echo validation_errors(); ?>        
        <? } ?>
        
        
        <!-- 
        <div class="row-fluid">
            <div class="span6">
                <div class="span4 TAL Pleft10">
                    <?php echo  lang('company_type') ?>
                </div>
                <div class="span8">
                    <?php echo  form_dropdown('erp_company_type_id', $company_types, set_value('erp_company_type_id', ''), 'class="validate[required] chosen-select input-full chosen-rtl " style="width:100%;" id="erp_company_type_id"') ?>
                </div>
			</div>
        
            <div class="span6">
                <div class="span4 TAL Pleft10">
                <?php echo  lang('country') ?>
                </div>
                <div class="span8">
                    <?php echo  form_multiselect('countries[]', $countries, set_value('countries', ''), ' id="countries" class="chosen-select chosen-rtl input-full" multiple tabindex="4" data-placeholder="'.lang('global_all').'"') ?>   
                </div>
            </div>
        </div>
        
        <div class="row-fluid">
            <div class="span6">
                <div class="span4 TAL Pleft10">
                <?php echo  lang('company') ?>
                </div>
                <div class="span8">
                    <?php echo  form_multiselect('companies[]', array(), set_value('companies', ''), ' id="companies" class="chosen-select chosen-rtl input-full" multiple tabindex="4" data-placeholder="'.lang('global_all').'"') ?>
                </div>
            </div>
        </div>
         -->
        
        <!-- 
        <div class="row-fluid">
            <div class="span6">
                <div class="span4 TAL Pleft10">
                
                </div>
                <div class="span10">
                    <?php echo  $success_message; ?>
                </div>
            </div>
        </div>
         -->
         
        <div class="row-fluid">
            <div class="span6">
                <div class="span4 TAL Pleft10">
                <?php echo  lang('message_subject') ?>
                </div>
              
                <div class="span4">
                    <?php echo form_input('message_subject', set_value("message_subject", $message_subject_text),"  id='message_subject' style='width:200px' class='validate[required] input-full' readonly='readonly' ") ?>
                </div>
                
                
            </div>
               <div class="span6">
            <div class="span3 TAL Pleft10">
                <?php echo  lang('importance') ?>:
                </div>
                <div class="span5">
                    <?php echo  form_dropdown('erp_importance_id', $importances, set_value('erp_importance_id', ''), 'class="validate[required] chosen-select input-full chosen-rtl " style="width:100%;" id="erp_importance_id"') ?>
                </div></div>
           
        </div>
        
        <div class="row-fluid">
            <div class="span6">
                <div class="span4 TAL Pleft10">
                <?php echo  lang('message_body') ?>
                </div>
                <div class="span8">
                    <?php echo form_textarea('message_body', set_value("message_body", $message_body_text), "style='height:200px; width:500px' class='validate[required] input-full' id='message_body'  ") ?>
                </div>
            </div>
        </div>
        
        <div class="row-fluid">
            <div class="span6">
                <div class="span4 TAL Pleft10">
                <?php echo  lang('attachement') ?>:
                </div>
                <div class="span8">
                    <?php echo form_upload('attachement[]', set_value("attachement")," multiple style='' id='attachement[]'  ") ?>
                </div>
               
            </div>
         </div>   
        
        
    </div>
</div>

<div class="widget TAC">
    <input type="submit" class="btn" name="smt_send" value="<?php echo lang('send') ?>" style="margin:10px;padding: 5px;height: auto">
</div>


<?php echo  form_close() ?>


<div class="footer"></div>

<? $this->load->view('hotel_availability/widgets/popup') ?>

<script type="text/javascript">
$(document).ready(function() {
    var config = {
        '.chosen-select': {width: "100%"},
        '.chosen-select-deselect': {allow_single_deselect: true},
        '.chosen-select-no-single': {disable_search_threshold: 10},
        '.chosen-select-no-results': {no_results_text: 'Oops, nothing found!'},
        '.chosen-select-width': {width: "95%"}
    }
    for (var selector in config) {
        $(selector).chosen(config[selector]);
    }

    $("#erp_company_type_id").chosen();
   
    // binds form submission and fields to the validation engine
    $("#frm_messages").validationEngine({
            prettySelect : true,
            useSuffix: "_chosen",
            promptPosition : "topRight:-150"
            //promptPosition : "bottomLeft"
    });


    $('#erp_company_type_id').change(function()
    {
        var erp_company_type_id=$('#erp_company_type_id').val();
        var countries=$('#countries').val();
        
    	var dataString = 'erp_company_type_id='+ erp_company_type_id+'&countries='+ countries;
        
        $.ajax
        ({
        	type: 'POST',
        	url: '<?php echo base_url().'messages/getCompaniesByCompanyTypeAndCountries'; ?>',
        	data: dataString,
        	cache: false,
        	success: function(html)
        	{
            	//alert(html);
        		$("#companies").html(html);
        		$("#companies").trigger("chosen:updated");
        	}
        });
        
    });
    
    $('#countries').change(function()
    {
    	var erp_company_type_id=$('#erp_company_type_id').val();
    	var countries=$(this).val();

    	var dataString = 'erp_company_type_id='+ erp_company_type_id+'&countries='+ countries;
    
    	$.ajax
    	({
    		type: 'POST',
    		url: '<?php echo base_url().'messages/getCompaniesByCompanyTypeAndCountries'; ?>',
    		data: dataString,
    		cache: false,
    		success: function(html)
    		{
        		//alert(html);
    			$("#companies").html(html);
    			$("#companies").trigger("chosen:updated");
    		}
    	});
    	
    });
    	    

    
});
</script>
