<!-- 
<link rel="stylesheet" href="<?= NEW_CSS_JS .'_'. lang('DIR') ?>/bootstrap/bootstrap.min.css" />
 -->

<link rel="stylesheet" href="<?= NEW_CSS_JS .'_'. lang('DIR') ?>/icons.css" />

<!--<div class="row-fluid" >
    <div class="span12" >
        <div class="row-fluid" >
            <div class="row-fluid">
                <div style="margin-bottom:10px; text-align:left; border:3px solid #FFF; background:url(<?php echo  IMAGES ?>/backgrounds/bg_box_head.jpg); padding:5px 10px; height:30px;">
                    <div style="float:right; text-align:right; font-size:14px; line-height:30px; color:#333;"> <a href="<?php echo   site_url('ea/dashboard')?>"> <?php echo  lang('global_system_management') ?></a> 
                            <span style="color:#80693d"><img src="<?php echo  IMAGES ?>/jbreadcrumb/Chevron-ar.gif" width="5" height="20"></span>  <?php echo  lang('messages') ?> </div>
                </div> 
            </div>
           
           
            -->
      <div class="widget">
    <div class="path-container Fright">
        <div class="path-name Fright">
        <a href="<?= site_url() ?>"><?= lang('global_system_management') ?></a>
        </div>
        <div class="path-arrow Fright">
        </div>
        <div class="path-name Fright">
            <?php echo  lang('messages') ?>
        </div>
        
    </div>
</div>      
            
            
           
            <div class="widget" >
                <div class="widget-header">
                     
                    
                    <div class="widget-header">
			        <div class="widget-header-icon Fright">  </div>
			        <div class="widget-header-title Fright">
			            <?php echo  lang('messages') ?>
			        </div>
			    	</div>
                    
                    <div class="widget-container">
                    
                    <div class="row-fluid"  style="padding: 10px 0;">
                    <a href="<?php echo site_url("messages/viewAll"); ?>"  class="btn btn-primary"><?php echo lang("messages") ?></a>
	                <a href="<?php echo site_url("messages/viewAllImportant"); ?>"  class="btn btn-primary"><?php echo lang("important_messages") ?></a>
	                <a href="<?php echo site_url("messages/viewAllDeleted"); ?>"  class="btn btn-primary"><?php echo lang("deleted_messages") ?></a>
	            	
	            	<a href="<?php echo site_url("messages/index"); ?>"  class="btn btn-primary Fleft"><?php echo lang("new_message") ?></a>
	            	
	            	</div>
                    
                    
                        <table cellpadding="0" class="MyTable" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th><?php echo  lang('message') ?></th>
                                </tr>
                            </thead>
                            <tbody>
      <? if (isset($items)) { ?>
           <? foreach ($items as $item) { ?>
                   <tr>
                   
           <?php 
												
            $mark_as_read= $item->mark_as_read;
        	$mark_as_important= $item->mark_as_important;
        	$is_hide= $item->is_hide;
        	
        	$erp_messages_detail_id= $item->erp_messages_detail_id;
        	

        	$message_datetime= $item->message_datetime;
        	
        	 
	        $message_subject=$item->message_subject;
	        $message_body=$item->message_body;
	        
        	
           if($mark_as_important==1) {
	        	$style_message_message='background:#fbf3d7';
	        	$mark_as_important_value=0;
	        	$mark_as_important_link_text=lang('');
        	} else {
	        	if($mark_as_read==1) {
	        		$style_message_message='background:#FFFFFF';
	        	} else {
	        		$style_message_message='background:#f5f5f5';
	        	}        	
	        	$mark_as_important_value=1;
	        	$mark_as_important_link_text=lang('');
        	}
        	
           if($is_hide==1) {
	        	
	        	$is_hide_value=0;
        	} else {
	        	
	        	$is_hide_value=1;
        	}
        	
        	$message_body_brief=substr($message_body, 0, 650).'...';
        	
            echo("
            <td style='$style_message_message'>
            <div style=''>
            <a class='fancybox' href='".site_url("messages/read/$erp_messages_detail_id")."'>
            <b>$message_subject</b>
            <br/>
            $message_body_brief
            </a>
            </div>
            
            ");
            
            echo "
            
            <script>
            $(document).ready(function()
			{
				$('#message_message_delete_dv_$erp_messages_detail_id').click(function()
				{
					$.ajax
					({
						type: 'POST',
						url: '". base_url().'messages/hide'."/$erp_messages_detail_id/$is_hide_value',
						cache: false,
						success: function(html)
						{
							location.reload();
						}
					});
					
				});
				
				$('#message_message_important_dv_$erp_messages_detail_id').click(function()
				{
					$.ajax
					({
						type: 'POST',
						url: '". base_url().'messages/markAsImportant'."/$erp_messages_detail_id/$mark_as_important_value',
						cache: false,
						success: function(html)
						{
							location.reload();
						}
					});
					
				});
		
			});
            </script>
            ";
            
            if($is_hide==1) {
            echo"
            <span id='message_message_delete_dv_$erp_messages_detail_id' class='icos-arrow-right' title='".lang('restore_message')."' style='float:left; cursor: pointer; margin: 3px 3px 3px 3px;'  >".lang('')."</span>
            ";
            } else {
            	echo"
            <span id='message_message_delete_dv_$erp_messages_detail_id' class='icos-cancel' title='".lang('delete_message')."' style='float:left; cursor: pointer; margin: 3px 3px 3px 3px;'  >".lang('')."</span>
            ";
            }
              echo"
            <span id='message_message_important_dv_$erp_messages_detail_id' class='icos-star' title='$mark_as_important_link_text' style='float:left; cursor: pointer; margin: 3px 3px 3px 3px;'  >$mark_as_important_link_text</span>
            ";         

              echo"</td>";
              
                                            ?>
                        </tr>

                                    <? } ?>
                                <? }; ?>
                            </tbody>
                        </table>
                    </div>
                </div>  
            </div>
            <div class="row-fluid">
                <?php echo  $pagination ?>
            </div>
        </div>

<script>
$(document).ready(function() {

    $('.fancybox').click(function(){
        $.get($(this).attr('href'),function(data){
            $.fancybox({content:data});
        });
        return false;
    });
});

</script>



    </div>  
</div>
