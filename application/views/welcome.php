<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title><?= lang('global_title') ?></title>
        
        <? $this->load->view("analytics") ?>
    </head>
   <style type="text/css">
        body{
            margin:0;
            padding:0;
            background:#ffffff;
        }
        .logo{
            background-image:url(<?= base_url() ?>/static/front/logo.png);
            width:320px;
            height:95px;
            margin:20px auto;
        }
        .banner{
            background-image:url(<?= base_url() ?>/static/front/banner.png);
            width:655px;
            height:377px;
            margin:20px auto;
            position: relative;
        }
        .icon-container{
            width:600px;
            position: absolute;
            bottom: -20px;
            left: 95px;
        }
        .link{
            width:655px;
            height:211px;
            margin:20px auto;
            float:left;
        }
        .icon1{
            background-image:url(<?= base_url() ?>/static/front/nakel.png);
            width:117px;
            height:120px;
            margin:20px auto;
            float:left;
        }
        .icon2{
            background-image:url(<?= base_url() ?>/static/front/umra.png);
            width:117px;
            height:120px;
            margin:20px 0px 20px 48px;
            float:left;
        }
        .icon3{
            background-image:url(<?= base_url() ?>/static/front/tours.png);
            width:117px;
            height:120px;
            margin:20px 45px;
            float:left;
        }
        .icon4 {
            background-image: url("<?= base_url() ?>/static/front/gov.png");
            float: right;
            height: 120px;
            margin: 136px 245px;
            position: absolute;
            width: 117px;
        }
        .icon5 {
            background-image: url("<?= base_url() ?>/static/front/hm.png");
            float: right;
            height: 120px;
            margin: 136px 80px;
            position: absolute;
            width: 117px;
        }
        
    </style>
    <body>
        <div class="logo"></div>
        <div class="banner">
            <div class="icon-container">
                <a href="<?= site_url('ito') ?>"><div class="icon1"></div></a>
                <a href="<?= site_url('uo') ?>"><div class="icon2"></div></a>
                <a href="<?= site_url('ea') ?>"><div class="icon3"></div></a>
                <a href="<?= site_url('gov') ?>"><div class="icon4"></div></a>
                <a href="<?= site_url('hm') ?>"><div class="icon5"></div></a>
            </div>
        </div>
        <!--<div class="link"></div>-->
    </body>
</html>
