<div class='span12' >
        <div class='widget'>
            <div class='head dark' >
                <div class="icon"><span class="icos-search"></span></div>
                <h2><?= lang('cities_search_title') ?></h2>
            </div>
            <div class='block-fluid' >
                <?= form_open('admin/cities/index', 'method="get"') ?>
                <div class="row-form">
                    <div class="span2"><?= lang('cities_erp_countery_id') ?></div>
                    <?= form_dropdown('erp_country_id', ddgen('erp_countries', array('erp_country_id', name())), set_value('erp_country_id')," class='input-small' ") ?>
                    <?= form_error('name_ar'); ?>
                </div>
                 <div class="row-form">
                    <div class="span2"><?= lang('cities_name_ar') ?></div>
                    <?//= form_input('name_ar', set_value("name_ar"), 'class="input-small"') ?>
                    <input class="input-small" name="name_ar" type="text" value="<?=  set_value('name_ar')?>" pattern="[a-zA-Z0-9_-]{3,12}" autofocus title="must be alphanumeric in 3-12 chars" >
                    <?= form_error('name_ar'); ?>
                </div>
                 <div class="row-form">
                    <div class="span2"><?= lang('cities_name_la') ?></div>
                    <?//= form_input('name_ar', set_value("name_ar"), 'class="input-small"') ?>
                    <input class="input-small" name="name_la" type="text" value="<?=  set_value('name_la')?>" pattern="[a-zA-Z0-9_-]{3,12}" autofocus title="must be alphanumeric in 3-12 chars" >
                    <?= form_error('name_la'); ?>
                </div>

                <div class="row-form">
                    <div style="text-align:center">
                        <input type="submit" name="search" value="search" class="btn btn-primary" />
                    </div>
                </div>
                <?= form_close() ?>
            </div>
        </div>
    </div>
