<div class="span12">
    <div class="widget">
        <a class="btn Fleft" href="<?= site_url('admin/cities/add') ?>"><?= lang('global_add_new_record') ?></a>

        <div class="path-container Fright">
            <div class="path-name Fright">
                <a href=""><?= lang('global_system_management') ?></a>
            </div>
            <div class="path-arrow Fright">
            </div>
            <div class="path-name Fright">
                <?= lang('menu_main_cities') ?>
            </div>
        </div>

    </div>
</div>

    <style>
        .updated_msg{
            display:none;
            background-color:#ccee97;
            font-weight: bold;
            text-align: center;
            border:3px solid #cccdc9; 
            padding:20px;  
            margin-bottom:10px;
            border-radius:15px;
            margin:10px; 
        }
    </style>
    <div class='row-fluid' align='center' >
        <div  class='updated_msg' >
            <br><input  type ="button" value="<?= lang('global_submit') ?>"class="btn btn-primary" id='ok' >
        </div> 
    </div>


    <div class="widget">
        <div class="widget-header">

            <div class="widget-header-icon Fright">
                <span class="icos-pencil2"></span>
            </div>

            <div class="widget-header-title Fright"><?= lang('global_search') ?></div>

            <a href="#" class="show_hide Fleft"><img src="<?= IMAGES ?>/collapse-arrow.png" width="19" height="19" /></a>

        </div>

        <div class="widget-container slidingDiv">
            <?= form_open('admin/cities/index', 'method="get"') ?>
            <div class="row-form">
                <div class="span6">
                    <div class="span4 TAL Pleft10"><?= lang('cities_erp_countery_id') ?>:</div>
                    <div class="span8">
                        <?= form_dropdown('erp_country_id', ddgen('erp_countries', array('erp_country_id', name())), set_value('erp_country_id'), " name='s_example' class='select' style='width:100%;' ") ?>
                        <?= form_error('erp_country_id'); ?>
                    </div>
                </div>
            </div>

            <div class="row-form BGgrey">
                <div class="span12 TAC">
                    <div class="toolbar bottom TAC"><input type="submit" name="search" value="<?= lang('global_search') ?>" class="btn btn-primary" /></div>
                </div>
            </div>

        </div>
    </div>


    <div class="widget">
        <div class="widget-header">

            <div class="widget-header-icon Fright">
                <span class="icos-pencil2"></span>
            </div>

            <div class="widget-header-title Fright">
                <?= lang('menu_main_cities') ?>
            </div>

        </div> 

        <div class="widget-container">
            <div class="block-fluid">
                <table cellpadding="0" class="fpTable" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th><?= lang('cities_th_city_name') ?></th>
                            <th><?= lang('cities_th_country_name') ?></th>
                            <th><?= lang('global_actions') ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <? if (isset($items)): ?>
                            <? foreach ($items as $item): ?>
                                <tr id="row_<?php echo $item->erp_city_id; ?>">
                                    <td><?= $item->{name()} ?></td>
                                    <td><? if (lang('global_lang') == 'ar'): ?><?= $item->country_name_ar ?><? else: ?><?= $item->country_name_la ?><? endif ?></td>
                                    <td class="TAC">
                                        <a href="<?= site_url("admin/cities/edit") ?>/<?= $item->erp_city_id ?>"><span class="icon-pencil"></span></a>
                                        <? if ($this->cities_model->check_delete_ability($item->erp_city_id) == 0): ?> 
                                            <? if (lang('global_lang') == 'ar'): ?>
                                                <a  onclick=" return  confirm('<?= lang('global_are_you_sure_you_want_to_delete') . '  ' . $item->name_ar ?>')" href="<?= site_url("admin/cities/delete/" . $item->erp_city_id) ?>"><span class="icon-trash"></span></a>
                                                    <? else: ?>
                                                    <a  onclick=" return  confirm('<?= lang('global_are_you_sure_you_want_to_delete') . '  ' . $item->name_la ?>')" href="<?= site_url("admin/cities/delete/" . $item->erp_city_id) ?>"><span class="icon-trash"></span></a>
                                                    <? endif ?>         
                                                    <!--<span class="icon-trash"></span></a>-->
                                                <? else: ?><? endif ?>
                                    </td>
                                </tr>
                            <? endforeach; ?>
                        <? endif; ?>
                    </tbody>
                </table>
                <?= form_close() ?>
            </div>
        </div>
    </div>

<script type='text/javascript' src='<?= JS ?>/custom/table_operation.js'></script>

<script type="text/javascript">
    $(document).ready(function() {
        $('.delete_item').click(function() {
            var name = $(this).attr('name');
            var erp_city_id = $(this).attr('id');
<? if (lang('global_lang') == 'ar'): ?>
                $('#msg').text("<?= lang('global_are_you_sure_you_want_to_delete') ?>" + " " + name)
<? else: ?>
                $('#msg').text("<?= lang('global_are_you_sure_you_want_to_delete') ?>" + " " + name)
<? endif; ?>
            $('#yes').click(function() {
                var answer = $(this).attr('id');
                if (answer === 'yes') {
                    $.ajax({
                        type: "POST",
                        dataType: 'json',
                        url: "<?php echo site_url('admin/cities/delete'); ?>",
                        data: {'erp_city_id': erp_city_id},
                        success: function(msg) {
                            if (msg.response == true) {
                                var del = erp_city_id;
                                $("#row_" + del).remove();
<? if (lang('global_lang') == 'ar'): ?>
                                    $('.updated_msg').text("<?= lang('global_delete_confirm') ?>" + " " + name);
                                    $('.updated_msg').append('<br>');
                                    $('.updated_msg').append('<input  type ="button" value="<?= lang('global_submit') ?>"class="btn btn-primary" id="ok" >');
<? else: ?>
                                    $('.updated_msg').text(name + " " + "<?= lang('global_delete_confirm') ?>");
                                    $('.updated_msg').append('<br>');
                                    $('.updated_msg').append('<input  type ="button" value="<?= lang('global_submit') ?>"class="btn btn-primary" id="ok" >');
<? endif; ?>
                                $('.updated_msg').show();
                                $('#ok').click(function() {
                                    $('.updated_msg').hide();
                                });
                            } else if (msg.response == false) {
                                $('.updated_msg').text(msg.msg);
                                $('.updated_msg').append('<br>');
                                $('.updated_msg').append('<input  type ="button" value="<?= lang('global_submit') ?>"class="btn btn-primary" id="ok" >');
                                $('.updated_msg').show();
                                $('#ok').click(function() {
                                    $('.updated_msg').hide();
                                });
                            }
                        }
                    });
                } else {
                    return FALSE;
                }
            });
        });
    });
</script>   


