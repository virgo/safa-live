<div class="span12">
    <div class="widget">
        <div class="path-container Fright">
            <div class="path-name Fright">
                <a href=""><?= lang('global_system_management') ?></a>
            </div>
            <div class="path-arrow Fright">
            </div>
            <div class="path-name Fright">
                <a href="<?= site_url('admin/cities/index') ?>"><?= lang('menu_main_cities') ?></a>
            </div>
            <div class='path-arrow Fright'>
            </div>
            <div class="path-name Fright">
                <?= lang('cities_add_title') ?>
            </div>
        </div>
    </div>
</div>

<div class="widget">
    <div class="widget-header">
        <div class="widget-header-icon Fright">
            <span class="icos-pencil2"></span>
        </div>

        <div class="widget-header-title Fright">
            <?= lang('cities_add_title') ?> 
        </div>
    </div>  

    <div class="widget-container">
        <div class="block-fluid">
            <?= form_open() ?>
            <div class="row-form">
                <div class="span6">
                    <div class="span4 TAL Pleft10"> <?= lang('cities_name_ar') ?><span style="color: red">*</span>:</div>
                    <div class="span8">
                        <?= form_input('name_ar', set_value("name_ar"), "class='input-huge' ") ?>
                        <?= form_error('name_ar', '<div class="bottom" style="color:red" >', '</div>'); ?>
                    </div>
                </div>
                <div class="span6">
                    <div class="span4 TAL Pleft10"> <?= lang('cities_name_la') ?><span style="color: red">*</span>:</div>
                    <div class="span8">
                        <?= form_input('name_la', set_value("name_la"), "class='input-huge' ") ?>
                        <?= form_error('name_la', '<div class="bottom" style="color:red" >', '</div>'); ?>
                    </div>
                </div>
            </div>

            <div class="row-form">
                <div class="span6">
                    <div class="span4 TAL Pleft10"><?= lang('cities_erp_countery_id') ?><span style="color: red">*</span>:</div>
                    <div class="span8">
                        <?= form_dropdown('erp_country_id', ddgen('erp_countries', array('erp_country_id', name())), set_value('erp_country_id'), " name='s_example' class='select input-huge' style='width:90%;' ") ?>
                        <?= form_error('erp_country_id', '<div class="bottom" style="color:red" >', '</div>'); ?>
                    </div>
                </div>
            </div>

      
           <div class="toolbar bottom TAC">
           <input type="submit" name="submit" value="<?= lang('global_submit') ?>" class="btn btn-primary"> &nbsp;     
                <input  type ="button" value="<?= lang('global_back') ?>"class="btn btn-primary" onclick="window.location = '<?= site_url('admin/cities/index') ?>'">
            </div>
            
            <?= form_close() ?>     
        </div>
    </div>
</div>
  


