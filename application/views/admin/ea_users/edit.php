<div class="widget">
    <div class="path-container Fright">
        <div class="path-name Fright">
        <a href="<?php echo   site_url('admin/dashboard')?>"> <?php echo  lang('global_system_management') ?></a>
        </div>
        <div class="path-arrow Fright">
        </div>
        <div class="path-name Fright">
            <a href="<?= site_url('admin/safa_eas')?>"><?= lang('menu_external_agent')?></a>
        </div>
        <div class="path-arrow Fright">
        </div>
        <div class="path-name Fright">
            <a href="<?= site_url('admin/safa_eas/users_index/'. $item->safa_ea_id)?>"><?= lang('show_ea_user') ?> :: <?= item('safa_eas',name(),array('safa_ea_id'=>$item->safa_ea_id))?></a>
        </div>
        <div class="path-arrow Fright">
        </div>
        <div class="path-name Fright">
           <?= lang('users_edit') ?><? if (lang('global_lang') == 'ar'): ?><?= $item->name_ar ?><? else: ?><?= $item->name_la ?><? endif ?>   
        </div>
        
        
    </div>
    
	</div>
	
<div class="row-fluid">
    <div class="span12" >
        <div class="widget">
            
            
        <div class="widget-header">
        <div class="widget-header-icon Fright"> <span class="icos-pencil2"></span> </div>
        <div class="widget-header-title Fright">
            <?= lang('users_edit') ?><? if (lang('global_lang') == 'ar'): ?><?= $item->name_ar ?><? else: ?><?= $item->name_la ?><? endif ?>
        </div>
    	</div>
    	
                                   
            <div class="block-fluid">
                <?= form_open() ?>

                <div class="row-form">
                    <div class="span6">
                        <div class="span4"><?= lang('ea_users_name_ar') ?><font style="color:red" >*</font></div>
                        <div class="span8" >
                            <?= form_input('name_ar', set_value('name_ar',$item->name_ar)) ?>
                            <span class="bottom" style="color:red" ><?=form_error('name_ar')?></span> 
                        </div>   
                    </div>
                    <div class="span6">
                        <div class="span4"><?= lang('ea_users_name_la') ?><font style="color:red" >*</font></div>
                        <div class="span8" >
                            <?= form_input('name_la', set_value('name_la',$item->name_la)) ?>
                            <span class="bottom" style="color:red" ><?=form_error('name_la')?></span> 
                        </div>   
                    </div>
                </div>

                <div class="row-form">
                    <div class="span6">
                        <div class="span4"><?= lang('ea_users_username') ?><font style="color:red" >*</font></div>
                        <div class="span8" >
                            <?= form_input('username', set_value('username',$item->username)) ?>
                            <span class="bottom" style="color:red" ><?=form_error('username')?></span> 
                        </div>   
                    </div>
                </div>

                <div class="row-form">
                    <div class="span6">
                        <div class="span4"><?= lang('ea_users_password') ?></div>
                        <div class="span8" >
                            <?= form_password('password', set_value('password')) ?>
                            <span class="bottom" style="color:red">
                            </span>
                        </div>
                    </div>
                    <div class="span6">
                        <div class="span4"><?= lang('ea_users_confirm_password') ?></div>
                        <div class="span8">
                            <?= form_password('passconf', set_value('passconf')) ?>
                               <span class="bottom" style="color:red" >
                               </span>
                        </div>
                    </div>
                </div>
                
                
                <div class="row-form">
                    <div class="span6">
                        <div class="span4"><?= lang('active') ?><font style="color:red" ></font></div>
                        <div class="span8" >
                        	<?php 
                        	$are_checked=false;
                        	if($item->active==1) {
                        		$are_checked=true;
                        	}
                        	?>
                            <?= form_checkbox('active', set_value('active'), $are_checked) ?>
                            
                        </div>
                    </div>
                   
                </div>
                
                
                <div class="toolbar bottom TAC">
                    <button class="btn btn-primary"><?= lang('global_submit') ?></button>
                   <a  class="btn btn-primary" href="<?= site_url('admin/safa_eas/users_index') ?>/<?= $this->uri->segment("5"); ?>" ><?= lang('global_back') ?></a>
                </div>
                
                <?= form_close() ?> 
            </div>
        </div>  
    </div>
</div>  

