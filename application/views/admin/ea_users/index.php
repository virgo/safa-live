
<div class="widget">
    <div class="path-container Fright">
        <div class="path-name Fright">
        <?= lang('show_ea_user') ?> :: <?= item('safa_eas',name(),array('safa_ea_id'=>$safa_ea_id))?>
        </div>
        
        
    </div>
    <a href="<?= site_url("admin/safa_eas/users_add/".$this->uri->segment("4"))?>" class="btn Fleft"><?=lang('safa_eas_users_add_title')?></a>
</div>
	
<div class='row-fluid'>
    <div class='span12'>
        <div class='widget'>
            
            
            
             
            
            
            <div class='block-fluid' >
                <table class="table" width="100%" >
                    <thead>
                        <tr>                     
                            <th><?= lang('ea_users_name_ar') ?></th>
                            <th><?= lang('ea_users_username') ?></th>
                            <th><?= lang('safa_uos_th_usergroup') ?></th>
                            <th><?= lang('global_actions') ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <? if (isset($items)): ?>
                            <? foreach ($items as $item): ?>
                                <tr>
                                    <td><?= $item->{name()} ?></td>
                                    <td><?= $item->username ?></td>
                                    <? if (lang('global_lang') == 'ar'): ?>
                                    <td><?= $item->usergroup_name_ar ?></td>
                                    <?else:?>
                                    <td><?= $item->usergroup_name_la ?></td>
                                    <?  endif;?>
                                    <td class="TAC">
                                        <a href="<?= site_url("admin/safa_eas/users_edit") ?>/<?= $item->safa_ea_user_id ?>/<?= $this->uri->segment("4"); ?>"><span class="icon-pencil"></span></a>
                                        <? if ($this->eas_model->check_users_delete_ability($item->safa_ea_id) > 1): ?>
                                            <a onclick="return confirm('<?= lang('global_are_you_sure_you_want_to_delete')?> <? if (lang('global_lang') == 'ar'): ?><?= $item->name_ar ?><? else: ?><?= $item->name_la ?><? endif ?>')"  href= "<?= site_url("admin/safa_eas/users_delete") ?>/<?= $item->safa_ea_user_id ?>/<?= $this->uri->segment("4"); ?>"><span class="icon-trash"></span></a>
                                     <? endif; ?>
                                    </td>
                                </tr>
                            <? endforeach; ?>
                        <? endif; ?>
                    </tbody>
                </table>
            </div>
        </div>
        <?= $pagination ?>
        
           <div class="row-fluid" >
            <div class="span12">
                <div class="bottom TAC" >
                    <a  class="btn btn-primary" href="<?= site_url('admin/safa_eas/index') ?>" ><?= lang('global_back') ?></a>
                </div>
            </div>
        </div>
    </div>
</div>

