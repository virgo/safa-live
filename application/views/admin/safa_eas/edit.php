<span class="span8">

</span>
<div class="row-fluid">
      
    <?= form_open(false, 'id="frm_eas" ') ?>
    
    <div class="widget">
    <div class="path-container Fright">
        <div class="path-name Fright">
        <a href="<?php echo   site_url('admin/dashboard')?>"> <?php echo  lang('global_system_management') ?></a>
        </div>
        <div class="path-arrow Fright">
        </div>
        <div class="path-name Fright">
            <a href="<?= site_url('admin/safa_eas/index')?>"><?= lang('menu_external_agent')?></a>
        </div>
        <div class="path-arrow Fright">
        </div>
        <div class="path-name Fright">
            <?= lang('safa_eas_edit_title')?> <? if (lang('global_lang') == 'ar'): ?><?= $items->arabic_name ?><? else: ?><?= $items->english_name ?><? endif ?>
        </div>
        
        
    </div>
    
	</div> 
    
    
    <div class="widget">
        
        
        <div class="widget-header">
        <div class="widget-header-icon Fright"> <span class="icos-pencil2"></span> </div>
        <div class="widget-header-title Fright">
            <?= lang('safa_eas_edit_title') ?> <? if (lang('global_lang') == 'ar'): ?><?= $items->arabic_name ?><? else: ?><?= $items->english_name ?><? endif ?>
        </div>
    	</div>
    	
                                
        <div class="block-fluid">

            <? if (validation_errors()): ?>
<!--                <div style="font:normal 10px tahoma">
                    <?php echo validation_errors(); ?>
                </div>   -->
            <? endif ?> 
            <div class="row-form">
                <div class="span4">
                    <div class="span4"><?= lang('safa_eas_name_ar') ?>
                        <span style="color: red">*</span>:
                    </div>
                    <div class="span8"><?= form_input('name_ar', set_value("name_ar", $items->arabic_name), 'class="validate[required]"') ?>
                    <?= form_error('name_ar', '<div class="bottom" style="color:red" >', '</div>'); ?>
                    </div>
                </div>
                <div class="span4">
                    <div class="span4"><?= lang('safa_eas_name_la') ?>
                        <span style="color: red">*</span>:
                    </div>
                    <div class="span8"><?= form_input('name_la', set_value("name_la", $items->english_name), 'class="validate[required]"') ?>
                        <?= form_error('name_la', '<div class="bottom" style="color:red" >', '</div>'); ?>
                    </div>
                </div>
                <div class="span4">
                    <div class="span4"><?= lang('safa_eas_erp_country_id') ?>
                        <span style="color: red">*</span>:
                    </div>
                    <div class="span8">
                        <?= form_dropdown('erp_country_id', ddgen('erp_countries', array('erp_country_id', name())), set_value('erp_country_id',$items->erp_country_id), " name='s_example' class='select validate[required]' style='width:100%;' ") ?>
                        <?= form_error('erp_country_id', '<div class="bottom" style="color:red" >', '</div>'); ?>
                    </div>
                </div>
            </div>

            <div class="row-form">
                <div class="span4">
                    <div class="span4"><?= lang('safa_eas_phone') ?>
                       <span style="color: red">*</span>:
                    </div>
                    <div class="span8"><?= form_input('phone', set_value("phone", $items->phone), 'class="validate[required]"') ?>
                    <?= form_error('phone', '<div class="bottom" style="color:red" >', '</div>'); ?>
                    </div>
                </div>
                <div class="span4">
                    <div class="span4"><?= lang('safa_eas_mobile') ?>
                        <span style="color: red">*</span>:
                    </div>
                    <div class="span8"><?= form_input('mobile', set_value("mobile", $items->mobile), 'class="validate[required]"') ?>
                    <?= form_error('mobile', '<div class="bottom" style="color:red" >', '</div>'); ?>
                    </div>   
                </div>
                <div class="span4">
                    <div class="span4"><?= lang('safa_eas_fax') ?>
                       <span style="color: red">*</span>:
                    </div>
                    <div class="span8"><?= form_input('fax', set_value("fax", $items->fax), 'class="validate[required]"') ?>
                    <?= form_error('fax', '<div class="bottom" style="color:red" >', '</div>'); ?>
                    </div>
                </div>
            </div>

            <div class="row-form">
                <div class="span5">
                    <div class="span4"><?= lang('safa_eas_email') ?>
                        <span style="color: red">*</span>:
                    </div>
                    <div class="span8"><?= form_input('email', set_value("email", $items->email), 'class="validate[required]"') ?>
                    <?= form_error('email', '<div class="bottom" style="color:red" >', '</div>'); ?>
                    </div>
                </div>

                
                <div class="span4">
                    <div class="span4"><?= lang('safa_eas_logo') ?>:</div>
                    <div class="span8">
                        <? if($items->logo !== NULL):?>
                        <img src="<?= base_url('/static/ea_files/' . $items->logo) ?> " style="border:2px solid #fff"/>
                        <? endif ?>
                        <input type="file" name="logo" size="20"/>
                        <?= form_error('logo', '<div class="bottom" style="color:red" >', '</div>'); ?>                 
                    </div>
                 </div>
            </div>    


            

             
        </div>
    </div>
    
    <? $this->load->view('admin/safa_eas/branches') ?>
    
    <div class="toolbar bottom TAC"><input type="submit" name="submit" value="<?= lang('global_submit') ?>" class="btn btn-primary"> &nbsp;
                <input  type ="button" value="<?= lang('global_back') ?>"class="btn btn-primary" onclick="window.location = '<?= site_url('admin/safa_eas/index') ?>'">
            </div>
    
    <?= form_close() ?>
    
</div> 
<!--<script type='text/javascript' src='<? //= MODULE_JS        ?>/poll/add.js'></script>-->


<script type="text/javascript">
$(document).ready(function() {
   
    // binds form submission and fields to the validation engine
    $("#frm_eas").validationEngine({
            prettySelect : true,
            useSuffix: "_chosen",
            promptPosition : "topRight:-150"
            //promptPosition : "bottomLeft"
    });
});   
</script>