<div class='row-fluid' style="display:<? if (isset($_GET['search'])): ?>block<? else: ?>none<? endif; ?>" id="search_panal">
    <?= $this->load->view('admin/safa_eas/search') ?>
</div>
<div  class="row-fluid" >
    <div class="span6" id="show_fileds" >
    </div>
</div>
<div class='row-fluid'>
    <div class='span12'>
        <div class='widget'>
            <div class="dialog" id="deleting_error" style="display: none;" title="Error">
                <p><?= lang('global_select_one_record_at_least') ?></p>                
            </div>
            <div class='head dark'>
                <div class="icon"><span class="icos-cube1"></span></span></div>
                <h2><?= lang('safa_eas_title') ?></h2>
                <ul class="buttons">
                    <li>
                        <a href="#"><span class="icos-arrow-down5"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="<?= site_url("admin/safa_eas/add") ?>"><?= lang('global_add_new_record') ?></a></li>
                            <li><a id="delete_all" href="javascript:void(0)"><?= lang('global_delete_selected_records') ?></a></li>
                            <li class="divider"></li>
                            <li><? if (isset($_GET["search"])): ?><a href="<?= site_url('admin/safa_eas/index') ?>"><?= lang('global_full_list') ?></a><? else: ?><a href="javascript:void(0)" id="show_hide_search"><?= lang('global_search') ?></a><? endif; ?></li>
                        </ul>
                    </li>
                </ul>
            </div>
            <div class='block-fluid' > 
                <?= form_open("admin/safa_eas/delete_all", "id='form_delete'") ?>
                <table class="table" width="100%" >
                    <thead>
                        <tr>
                            <th><?= lang('safa_eas_name_ar') ?></th>
                            <th><?= lang('safa_eas_name_la') ?></th>
                            <th><?= lang('safa_eas_erp_country_id') ?></th>
                            <th><?= lang('safa_eas_direct_contracts') ?></th>
                            <th><?= lang('safa_eas_indirect_contracts') ?></th>
                            <th><?= lang('eas_operations') ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <? if (isset($items)): ?>
                            <? foreach ($items as $item): ?>
                                <tr>
                                    <td><?= $item->arabic_name ?></td>
                                    <td><?= $item->english_name ?></td>
                                    <td><?= $item->country_name ?></td>
                                    <td><?= $item->direct_contracts ?></td>
                                    <td><?= $item->indirect_contracts ?></td>
                                    <td>
                                        <div class="btn-group" >
                                            <?= $this->eas_model->check_delete_ability($item->safa_ea_id) ?>
                                            <a href="<?= site_url("admin/safa_eas/edit") ?>/<?= $item->safa_ea_id ?>" class="btn"><?= lang('global_edit') ?></a>
                                            <? if ($this->eas_model->check_delete_ability($item->safa_ea_id) == 0): ?><a href="javascript:void(0)" id="<?= $item->safa_ea_id ?>" class=" delete_item btn"><?= lang('global_delete') ?></a><? endif ?>

                                        </div>    
                                    </td>
                                </tr>
                            <? endforeach; ?>
                        <? endif; ?>
                    </tbody>
                </table>
                <?= form_close() ?> 
            </div>
        </div>
    </div>
</div>

<div class="row-fluid">
    <?= $pagination ?>
    <script type='text/javascript' src='<?= JS ?>/custom/table_operation.js'></script>
    <script>
        check_list_delete_submit("delete_all", "table", "form_delete", '<?= lang('global_are_you_sure_you_want_to_delete') ?>', "deleting_error");
    </script>

    <script type="text/javascript">
        $(document).ready(function() {
            $('.delete_item').click(function() {
                var answer = confirm('<?= lang('global_are_you_sure_you_want_to_delete') ?>');
                if (answer === true) {
                    var safa_ea_id = $(this).attr('id');
                    $.ajax({
                        type: "POST",
                        dataType: 'json',
                        url: "<?php echo site_url('admin/safa_eas/delete'); ?>",
                        data: {'safa_ea_id': $(this).attr('id')},
                        success: function(msg) {
                            if (msg.response == true) {
                                var del = safa_ea_id;
                                $("#row_" + del).remove();
                                alert("deleted");

                            } else if (msg.response == false) {
                                alert("not deleted");

                            }
                        }
                    });
                } else {
                    return FALSE;
                }
            });
        });
    </script>    
