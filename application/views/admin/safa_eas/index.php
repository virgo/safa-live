<div class="row-fluid">
    <div class="widget">
        <div class="path-container Fright">
            <div class="path-name Fright">
            <a href="<?php echo   site_url('admin/dashboard')?>"> <?php echo  lang('global_system_management') ?></a>
            </div>
            <div class="path-arrow Fright">
            </div>
            <div class="path-name Fright">
                <?php echo  lang('menu_external_agent') ?>
            </div>
        </div>
        <a href="<?= site_url("admin/safa_eas/add") ?>" class="btn Fleft"><?= lang('global_add_new_record')?></a>
    </div> 
    <div class="widget">
        <div class="widget-header">
            <div class="widget-header-icon Fright"> <span class="icos-pencil2"></span> </div>
            <div class="widget-header-title Fright">
                <?php echo  lang('global_search') ?>
            </div>
    	</div>
        <div class="block-fluid slidingDiv">
            <?= form_open('admin/safa_eas/index', 'method="get"') ?>
            <div class="row-form">
                <div class="span6">
                    <div class="span4"><?= lang('safa_eas_erp_country_id') ?></div>
                    <div class="span8"><?= form_dropdown('erp_country_id', ddgen('erp_countries', array('erp_country_id', name())), set_value('erp_country_id'), "  name='s_example'  style='width:100%;'  ") ?>
                        <?= form_error('erp_country_id'); ?>
                    </div>
                </div>
            </div>
            <div class="row-form">
                <div class="span6">
                    <div class="span4"><?= lang('safa_eas_name') ?></div>
                    <div class="span8">
                    <?= form_input('name', set_value('name')) ?>
                    <?= form_error('name'); ?>
                    </div>
                </div>
            </div>
            <div class="toolbar bottom TAC"><input type="submit" name="search" value="<?= lang('global_search') ?>" class="btn btn-primary" /></div>
            <?= form_close() ?>
        </div>
    </div>


    <div class="widget">
          
        
        <div class="widget-header">
        <div class="widget-header-icon Fright"> <span class="icos-pencil2"></span> </div>
        <div class="widget-header-title Fright">
            <?php echo  lang('menu_external_agent') ?>
        </div>
        
    	</div>
                 
        <div class="widget-container">              
        <div class="block-fluid">
            <table cellpadding="0" class="" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th><?= lang('global_arabic_name') ?></th>
                        <th><?= lang('global_english_name') ?></th>
                        <th><?= lang('safa_eas_erp_country_id') ?></th>
                        <th><?= lang('safa_eas_direct_contracts') ?></th>
                        <th><?= lang('safa_eas_indirect_contracts') ?></th>
                        <th><?= lang('global_actions')?></th> 
                       
                    </tr>
                </thead>
                <tbody><?$name=name()?>
                    <? if (isset($items)): ?>
                        <? foreach ($items as $item): ?>
                            <tr id="row_<?php echo $item->safa_ea_id;?>">
                                <td><?= $item->arabic_name ?></td>
                                <td><?= $item->english_name ?></td>
                                <td><?= $item->country_name ?></td>
                                <td><?= $item->direct_contracts ?></td>
                                <td><?= $item->indirect_contracts ?></td>
                                <td class="TAC">
                                    <a href="<?= site_url("ea_licenses/index") ?>/<?= $item->safa_ea_id ?>"><span class="icon-barcode"></span></a>
                                    <a href="<?= site_url("machines/index") ?>/<?= $item->safa_ea_id ?>"><span class="icon-wrench"></span></a>
                                    <a href="<?= site_url("admin/safa_eas/edit") ?>/<?= $item->safa_ea_id ?>"><span class="icon-pencil"></span></a>
                                    <? if ($this->eas_model->check_delete_ability($item->safa_ea_id) == 0): ?> 
                                        <a href="javascript:void(0)"   id="<?= $item->safa_ea_id ?>" class=" delete_item" data-toggle="modal">
                                            <span class="icon-trash"></span></a>
                                   <? endif ?>
                                    <a title="<?= lang('show_uo_user') ?>"    href="<?= site_url("admin/safa_eas/users_index") ?>/<?= $item->safa_ea_id ?>"><span class="icon-user"></span></a>
                                  <?if($item->disabled== 0):?>
                                    <a title="<?= lang('global_cancel') ?>"  onclick="return confirm('<?=lang('global_are_you_sure_you_want_to_block').' '.$item->$name?>')"  href="<?= site_url("admin/safa_eas/cancel_ea")?>/<?= $item->safa_ea_id ?>"><span class="icosg-blocked"></span></a>
                                    <?else:?>
                                        <a title="<?= lang('global_activate') ?>"  onclick="return confirm('<?=lang('global_are_you_sure_you_want_to_activate').' '.$item->$name?>')"  href="<?= site_url("admin/safa_eas/active_ea")?>/<?= $item->safa_ea_id ?>"><span class="icosg-checkmark"></span></a>
                                  <?endif;?>  
                                </td>
                            </tr>
                        <? endforeach; ?>
                    <? endif; ?>
                </tbody>
            </table>
            </div></div>
            <?= $pagination ?>
        
    </div>
</div>


    <script type='text/javascript' src='<?= JS ?>/custom/table_operation.js'></script>
    <script>
        check_list_delete_submit("delete_all", "table", "form_delete", '<?= lang('global_are_you_sure_you_want_to_delete') ?>', "deleting_error");
    </script>

     <script type="text/javascript">
        $(document).ready(function() {
            $('.delete_item').click(function() {
                var answer = confirm('<?= lang('global_are_you_sure_you_want_to_delete') ?>');
                if (answer === true) {
                    var safa_ea_id = $(this).attr('id');
                    $.ajax({
                        type: "POST",
                        dataType: 'json',
                        url: "<?php echo site_url('admin/safa_eas/delete'); ?>",
                        data: {'safa_ea_id': $(this).attr('id')},
                        success: function(msg) {
    //                                            alert(msg);
                            if (msg.response == true) {
                                var del = safa_ea_id;
                                $("#row_" + del).remove();
                                alert("deleted");

                            } else if (msg.response == false) {
                                alert("not deleted");

                            }
                        }
                    });
                } else {
                    return FALSE;
                }
            });
        });
    </script>   