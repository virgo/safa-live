<div class='span12' >
    <div class='widget'>
        <div class='head dark' >
            <div class="icon"><span class="icos-search"></span></div>
            <h2><?= lang('safa_eas_search_title') ?></h2>
        </div>
        <div class='block-fluid' >
            <?= form_open('admin/safa_eas/index', 'method="get"') ?>
            <div class="row-form">
                <div class="span2"><?= lang('safa_eas_erp_country_id') ?></div>
                 <?= form_dropdown('erp_country_id', ddgen('erp_countries', array('erp_country_id', name())), set_value('erp_country_id'),"  class='span8'  ") ?>
                <?= form_error('erp_country_id'); ?>
            </div>
            <div class="row-form">
                <div class="span2"><?= lang('safa_eas_name_ar') ?></div>
                 <?= form_input('name', set_value('name'),"class='span8'") ?>
                 <?= form_error('name'); ?>
            </div>
  

            <div class="row-form">
                <div style="text-align:center">
                    <input type="submit" name="search" value="search" class="btn btn-primary" />
                </div>
            </div>
            <?= form_close() ?>
        </div>
    </div>
</div>
