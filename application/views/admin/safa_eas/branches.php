<div class="widget" >
    <script>branches_counter = 0</script>
    <div class="widget-header">
        <div class="widget-header-icon Fright"> <span class="icos-pencil2"></span> </div>
        <div class="widget-header-title Fright"> <?php echo  lang('branches') ?></div>
        <!-- <div class='coll_open' id="coll_branches"></div> -->
        <a class="icon-plus Fleft" style="margin-top:5px; margin-left:15px; margin-right:15px;" title="<?php echo  lang('add') ?>" href="javascript:void(0)" onclick="add_branches('N' + branches_counter++); load_multiselect()">
            
        </a>
        
        
    </div>

    <div class="widget-container" id="container_branches">
        <div class="table-container" style="min-height: 250px">
            <table cellpadding="0" cellspacing="0">
                <thead>
                    <tr>
                        <th><?php echo  lang('name_ar') ?></th>
                        <th><?php echo  lang('name_la') ?></th>
                        <th><?php echo  lang('city') ?></th>
                        <th><?php echo  lang('address') ?></th>
                        <th><?php echo  lang('phone') ?></th>
                        <th><?php echo  lang('mobile') ?></th>
                        <th><?php echo  lang('fax') ?></th>
                        <th><?php echo  lang('email') ?></th>
                        
                        <th><?php echo  lang('actions') ?></th>
                    </tr>
                </thead>
                <tbody class="branches" id='branches'>
                <? if(isset($item_branches)) { ?>
                    <? if(check_array($item_branches)) { ?>
                    <? foreach($item_branches as $item_branch) { ?>
                    <tr rel="<?php echo  $item_branch->safa_ea_branch_id ?>">
                        
                       
                        <td><?php echo  form_input('branches_name_ar['.$item_branch->safa_ea_branch_id.']'
                                , set_value('branches_name_ar['.$item_branch->safa_ea_branch_id.']', $item_branch->name_ar)
                                , 'class="input-huge validate[required]" style="width:100px"') ?>
                        </td>
                        <td><?php echo  form_input('branches_name_la['.$item_branch->safa_ea_branch_id.']'
                                , set_value('branches_name_la['.$item_branch->safa_ea_branch_id.']', $item_branch->name_la)
                                , 'class="input-huge validate[required]" style="width:100px"') ?>
                        </td>
                        <td>
                            <?php 
                            
                            echo  form_dropdown('branches_erp_city_id['.$item_branch->safa_ea_branch_id.']'
                                    , $erp_cities, set_value('branches_erp_city_id['.$item_branch->safa_ea_branch_id.']', $item_branch->erp_city_id)
                                    , 'class="chosen-select chosen-rtl input-full" id="branches_erp_city_id['.$item_branch->safa_ea_branch_id.']" tabindex="4" ') ?>
                        </td>
                        <td><?php echo  form_input('branches_address['.$item_branch->safa_ea_branch_id.']'
                                , set_value('branches_address['.$item_branch->safa_ea_branch_id.']', $item_branch->address)
                                , 'class="input-huge" style="width:100px"') ?>
                        </td>
                        <td><?php echo  form_input('branches_phone['.$item_branch->safa_ea_branch_id.']'
                                , set_value('branches_phone['.$item_branch->safa_ea_branch_id.']', $item_branch->phone)
                                , 'class="input-huge" style="width:100px"') ?>
                        </td>
                        <td><?php echo  form_input('branches_mobile['.$item_branch->safa_ea_branch_id.']'
                                , set_value('branches_mobile['.$item_branch->safa_ea_branch_id.']', $item_branch->mobile)
                                , 'class="input-huge" style="width:100px"') ?>
                        </td>
                        <td><?php echo  form_input('branches_fax['.$item_branch->safa_ea_branch_id.']'
                                , set_value('branches_fax['.$item_branch->safa_ea_branch_id.']', $item_branch->fax)
                                , 'class="input-huge" style="width:100px"') ?>
                        </td>
                        <td><?php echo  form_input('branches_email['.$item_branch->safa_ea_branch_id.']'
                                , set_value('branches_email['.$item_branch->safa_ea_branch_id.']', $item_branch->email)
                                , 'class="input-huge validate[email]" style="width:100px"') ?>
                        </td>
                        
                        <td class="TAC">
                            <a href="javascript:void(0)" onclick="delete_branches(<?php echo  $item_branch->safa_ea_branch_id ?>, true)"><span class="icon-trash"></span></a>
                        </td>
                    </tr>
                    <? } ?>
                    <? } ?>
                    <? } ?>
                </tbody>
            </table>
        </div>

    </div>
</div>

<script>
function load_multiselect() {
	var config = {
            '.chosen-select': {width: "100%"},
            '.chosen-select-deselect': {allow_single_deselect: true},
            '.chosen-select-no-single': {disable_search_threshold: 10},
            '.chosen-select-no-results': {no_results_text: 'Oops, nothing found!'},
            '.chosen-select-width': {width: "95%"}
        }
        for (var selector in config) {
            $(selector).chosen(config[selector]);
        }
}
function add_branches(id) {

	
	
    var new_row = [
            "<tr rel=\"" + id + "\">",

            '     <td><?php echo  form_input("branches_name_ar[' + id + ']", FALSE, 'class="validate[required] " style=\"width:100px\"') ?>',
            "    </td>",
            '     <td><?php echo  form_input("branches_name_la[' + id + ']", FALSE, 'class="validate[required] " style=\"width:100px\"') ?>',
            "    </td>",
			
            "    <td>",
            "       <select name=\"branches_erp_city_id[" + id + "]\" class=\"chosen-select chosen-rtl input-full\" id=\"branches_erp_city_id_" + id + "\" tabindex=\"4\">",
            <? foreach($erp_cities as $key => $value): ?>
            "           <option value=\"<?= $key ?>\"><?= $value ?></option>",
            <? endforeach ?>
            "    </td>",

            '     <td><?php echo  form_input("branches_address[' + id + ']", FALSE, 'class="validate[required] " style=\"width:100px\"') ?>',
            "    </td>",
            '     <td><?php echo  form_input("branches_phone[' + id + ']", FALSE, 'class="validate[required] " style=\"width:100px\"') ?>',
            "    </td>",
            '     <td><?php echo  form_input("branches_mobile[' + id + ']", FALSE, 'class="validate[required] " style=\"width:100px\"') ?>',
            "    </td>",
            '     <td><?php echo  form_input("branches_fax[' + id + ']", FALSE, 'class="validate[required] " style=\"width:100px\"') ?>',
            "    </td>",
            '     <td><?php echo  form_input("branches_email[' + id + ']", FALSE, 'class="validate[required] " style=\"width:100px\"') ?>',
            "    </td>",
            
            "    <td class=\"TAC\">",
            "        <a href=\"javascript:void(0)\" onclick=\"delete_branches('" + id + "')\"><span class=\"icon-trash\"></span></a>",
            "    </td>",
            "</tr>"
            ].join("\n");

    
        
    $('#branches').append(new_row);
    
}
function delete_branches(id, database) 
{
    if(typeof database == 'undefined')
    {
        $('.branches').find('tr[rel="' + id + '"]').remove();
    }
    else
    {
        $('.branches').find('tr[rel=' + id + ']').remove();
        var hidden_input = '<input type="hidden" name="branches_remove[]" value="' + id + '" />';
        $('#branches').append(hidden_input);
    }
}

</script>

