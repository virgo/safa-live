<div class="row-fluid" >
    <div style="margin-bottom:10px; text-align:left; border:3px solid #FFF; background:url(<?= IMAGES ?>/backgrounds/bg_box_head.jpg); padding:5px 10px; height:30px;">
        <div style="float:right; text-align:right; font-size:14px; line-height:30px; color:#333;"> 
            <a href="<?= site_url('admin/dashboard')?>"><?= lang('global_system_management') ?></a> 
            <span style="color:#80693d"><img src="<?=IMAGES?>/jbreadcrumb/Chevron-ar.gif" width="5" height="20"></span>
            <a href="<?=  site_url('admin/hotels_meals/index')?>"><?= lang('parent_child_title') ?></a>
            <span style="color:#80693d"><img src="<?=IMAGES?>/jbreadcrumb/Chevron-ar.gif" width="5" height="20"></span>
            <?= lang('edit_hotels_meals') ?>
        </div>
    </div> 
</div>
<div class="row-fluid">
    <div class="widget">
        <div class="head dark">
            <div class="icon"><i class="icos-pencil2"></i></div>
            <h2> 
                <?= lang('edit_hotels_meals') ?> <?= $items->{name()} ?>
            </h2> 
        </div>                          
        <div class="block-fluid">
           <?= form_open_multipart('', 'autocomplete="off" ') ?> 
            
            <div class="row-form">
                <div class="span6">
                    <div class="span4"><?= lang('hotel_marketing_companies_name_ar') ?><?if(name()=='name_ar'):?><font style="color:red" >*</font><?endif;?></div>
                    <div class="span6">
                        <?= form_input('name_ar', set_value("name_ar",$items->name_ar)) ?>
                        <?= form_error('name_ar', '<span class="bottom" style="color:red" >', '</span>'); ?>
                    </div>
                </div>
                <div class="span6">
                    <div class="span4"><?= lang('hotel_marketing_companies_name_la') ?><?if(name()=='name_la'):?><font style="color:red" >*</font><?endif;?></div>
                    <div class="span6" >
                        <?= form_input('name_la', set_value("name_la",$items->name_la)) ?>
                        <?= form_error('name_la', '<span class="bottom" style="color:red" >', '</span>'); ?>  
                    </div>     
                </div>
               
            </div>
            
            <div class="row-form">
                
                <div class="span6">
                    <div class="span4"><?= lang('hotel_marketing_companies_phone') ?><font style="color:red" >*</font></div>
                    <div class="span6" >
                      <?= form_input('phone', set_value("phone",$items->phone)) ?>
                      <?= form_error('phone', '<div class="bottom" style="color:red" >', '</div>'); ?>
                    </div>     
                </div>
                
               <div class="span6">
                    <div class="span4"><?= lang('hotel_marketing_companies_fax') ?><font style="color:red" >*</font></div>
                    <div class="span6" >
                      <?= form_input('fax', set_value("fax",$items->fax)) ?>
                      <?= form_error('fax', '<div class="bottom" style="color:red" >', '</div>'); ?>
                    </div>     
                </div>
     
            </div>
            
            <div class="row-form">
                <div class="span6">
                    <div class="span4"><?= lang('hotel_marketing_comapnies_email')?><font style="color:red" >*</font></div>
                    <div class="span6">
                         <?= form_input('email', set_value("email",$items->email))?>
                         <?= form_error('email', '<div class="bottom" style="color:red" >', '</div>'); ?>
                    </div>
                </div>
            </div>
         
            
            <div class="row-form">
                <div class="span6">
                    <div class="span4"><?= lang('hotel_marketing_companies_remarks')?></div>
                    <div class="span6">
                         <?= form_textarea('remarks', set_value("remarks",$items->remarks))?>
                         <?= form_error('remarks', '<div class="bottom" style="color:red" >', '</div>'); ?>
                    </div>
                </div>
            </div>
            
            <div class="toolbar bottom TAC">
                <button name="sub mit" class="btn btn-primary"><?= lang('global_submit') ?></button>
                <a href="<?= site_url('admin/hotel_marketing_companies/index') ?>" class="btn btn-primary"  ><?= lang('global_back') ?></a>
           </div>

            
            
            <?= form_close() ?> 
        </div>
    </div>
</div> 


