<div class="row-fluid" >
    <div style="margin-bottom:10px; text-align:left; border:3px solid #FFF; background:url(<?= IMAGES ?>/backgrounds/bg_box_head.jpg); padding:5px 10px; height:30px;"><a href="<?= site_url("admin/meals/add") ?>" class="btn btn-primary"><?= lang('global_add_new_record') ?></a>
        <div style="float:right; text-align:right; font-size:14px; line-height:30px; color:#333;"> 
            <a href="<?= site_url('admin/dashboard')?>"><?= lang('global_system_management') ?></a>
            <span style="color:#80693d"><img src="<?= IMAGES ?>/jbreadcrumb/Chevron-ar.gif" width="5" height="20"></span>
            <?= lang('parent_child_title') ?>
        </div>
    </div>  
</div>
<div class='row-fluid'>
    <div class='span12'>
        <div class='widget'>
            <div class='head dark'>
                <div class="icon"><span class="icos-cube1"></span></span></div>
                <h2><?= lang('meals') ?></h2>
            </div>
            <div class='block-fluid '> 
                <table cellpadding="0" class="myTable" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th><?= lang('meals_namear') ?></th>
                            <th><?= lang('meals_namela') ?></th>
                            <th><?= lang('global_actions') ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <? if (isset($items)): ?>
                            <? foreach ($items as $item): ?>
                                <tr>
                                    <td><?= $item->name_ar ?></td>
                                    <td><?= $item->name_la ?></td>
                                    <td class="TAC"  >
                                        <div >
                                        <a title="<?= lang('global_edit') ?>"   href="<?= site_url("admin/meals/edit") ?>/<?= $item->erp_meal_id ?>"><span class="icon-pencil"></span></a>
                                        <?if($this->meals_model->check_delete_ability($item->erp_meal_id)):?>
                                              <a title="<?= lang('global_delete') ?>" onclick="return confirm('<?=lang('global_are_you_sure_you_want_to_delete')?>')"  href="<?= site_url("admin/meals/delete") ?>/<?= $item->erp_meal_id ?>"><span class="icon-trash"></span></a>
                                        <?endif;?>
                                        </div>
                                    </td>
                                </tr>
                            <? endforeach; ?>
                        <? endif; ?>
                    </tbody>
                </table>

            </div>
        </div>
        <div style="width:100%;">
            <?= $pagination ?>
        </div>
    </div>
   
