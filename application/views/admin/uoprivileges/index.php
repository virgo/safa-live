<!-- By Gouda, For popup -->
<script type="text/javascript" src='<?= CSS_JS ?>/form/form.js'></script>
<link rel="stylesheet" href="<?= CSS ?>/bootstrap/bootstrap.min.new.css"/>
      


<div class="row-fluid">
  
<div class="widget">
    <div class="path-container Fright">
        <div class="path-name Fright">
        <a href="<?php echo   site_url('admin/dashboard')?>"> <?php echo  lang('global_system_management') ?></a>
        </div>
        <div class="path-arrow Fright">
        </div>
        <div class="path-name Fright">
            <?php echo  lang('uoprivileges_title') ?>
        </div>
        
        
        
    </div>
    <a href="<?= site_url("admin/uoprivileges/add") ?>" class="btn Fleft"><?= lang('global_add_new_record')?></a>
</div>


   <div class="widget">
<style>
    .updated_msg{
        display:none;
        background-color:#ccee97;
        font-weight: bold;
        text-align: center;
        border:3px solid #cccdc9; 
        padding:20px;  
        margin-bottom:10px;
        border-radius:15px;
        margin:10px; 
    }
</style>
<div class='row-fluid' align='center' >
    <div  class='updated_msg' >
        <br><input  type ="button" value="<?=lang('global_submit')?>"class="btn btn-primary" id='ok' >
    </div> 
 </div>
        
        
        <div class="widget-header">
	        <div class="widget-header-icon Fright"> <span class="icos-pencil2"></span> </div>
	        <div class="widget-header-title Fright">
	            <?php echo  lang('global_search') ?>
	        </div>
	        
	    	</div>
                               
        <div class="block-fluid slidingDiv">
            <?= form_open('admin/uoprivileges/index', 'method="get"') ?>
            <div class="row-form">
                <div class="span6">
                    <div class="span3"><?= lang('privileges_name_ar') ?>:</div>
                    <div class="span8">
                        <?= form_input('name_ar', set_value("name_ar")) ?>
                        <?= form_error('name_ar', '<div class="bottom" style="color:red" >', '</div>'); ?>
                    </div>
                </div>
                <div class="span6">
                    <div class="span3"><?= lang('privileges_name_la') ?>:</div>
                    <div class="span8">
                        <?= form_input('name_la', set_value("name_la")) ?>
                        <?= form_error('name_la', '<div class="bottom" style="color:red" >', '</div>'); ?>
                    </div>
                </div>
            </div>
            <div class="toolbar bottom TAC"><input type="submit" name="search" value="<?= lang('global_search') ?>" class="btn btn-primary" /></div>
            <?= form_close() ?>
        </div>
    </div>


    <div class="widget">
        
        
        <div class="widget-header">
	        <div class="widget-header-icon Fright"> <span class="icos-pencil2"></span> </div>
	        <div class="widget-header-title Fright">
	            <?php echo  lang('uoprivileges_title') ?>
	        </div>
	        
	    	</div>
                      
        <div class="block-fluid">
            <table cellpadding="0" class="fpTable" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th><?= lang('privileges_th_privileges_name_ar') ?></th>
                        <th><?= lang('privileges_th_privileges_name_la') ?></th>
                        <th><?= lang('privileges_th_privileges_description') ?></th>
                        <th><?= lang('privileges_th_privileges_role_name') ?></th>
                        <th><?= lang('global_actions')?></th>
                    </tr>
                </thead>
                <tbody>
                   <? if (isset($items)): ?>
                     <? foreach($items as $item): ?>
                    <tr id="row_<?php echo $item->safa_uoprivilege_id;?>">
                         <td> <?= $item->name_ar ?></td>
                         <td><?= $item->name_la ?></td>
                         <td>
                             <? if (lang('global_lang') == 'ar'): ?>
                                <span class="label label-info tipb" title="<?=$item->description_ar; ?> ">
                             <?else:?>
                                <span class="label label-info tipb" title="<?=$item->description_la; ?> ">
                             <?  endif;?>      
                                 <?= lang('privileges_description') ?>
                                </span>
                         </td>
                         <td><?= $item->role_name ?></td>
                         <td class="TAC">
                          <a href="<?= site_url("admin/uoprivileges/edit") ?>/<?= $item->safa_uoprivilege_id ?>"><span class="icon-pencil"></span></a>
                           <? if($this->uoprivileges_model->check_delete_ability($item->safa_uoprivilege_id)==0):?> 
                               <? if (lang('global_lang') == 'ar'): ?>
                                <a href="#fModal" name="<?= $item->name_ar ?>"  id="<?= $item->safa_uoprivilege_id ?>" class="delete_item" data-toggle="modal">
                             <? else: ?>
                                <a href="#fModal" name="<?= $item->name_la ?>"  id="<?= $item->safa_uoprivilege_id ?>" class="delete_item" data-toggle="modal">
                             <? endif ?>         
                               <span class="icon-trash"></span></a>
                          <? else:?><?endif?>
                        </td>
                    </tr>
                    <? endforeach; ?>
                     <? endif; ?>
                </tbody>
            </table>
             <?= form_close() ?>
        </div>
    </div>
</div>
<div class="row-fluid">
    <?= $pagination ?>
<div id="fModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="myModalLabel"><?= lang('global_system_message') ?></h3>
    </div>        
    <div class="row-fluid">
        <div id="msg" class="row-form">
            <?=  lang('global_are_you_sure_you_want_to_delete')?>
        </div>
    </div>                   
    <div class="modal-footer">
        <button class="btn btn-primary" data-dismiss="modal" aria-hidden="true"id="yes"><?= lang('global_yes')?></button>
        <button class="btn btn-primary" data-dismiss="modal" aria-hidden="true"id="no"><?= lang('global_no')?></button>       </div>
</div>
  <script type="text/javascript">
    $(document).ready(function(){
        $('.delete_item').click(function(){
                        var name= $(this).attr('name');
                        var safa_uoprivilege_id= $(this).attr('id');
                        <? if (lang('global_lang') == 'ar'): ?>
                            $('#msg').text("<?= lang('global_are_you_sure_you_want_to_delete') ?>"+" "+name)
                        <? else: ?>
                            $('#msg').text(name+" "+"<?= lang('global_are_you_sure_you_want_to_delete') ?>")
                        <?  endif;?>    
			 $('#yes').click(function(){
                         var answer =$(this).attr('id');
                           if(answer==='yes'){
				$.ajax({
					type: "POST",
					dataType: 'json',
					url: "<?php echo site_url('admin/uoprivileges/delete');?>",
					data: {'safa_uoprivilege_id':safa_uoprivilege_id},
					success: function(msg){
                                         if(msg.response==true){
                                            var del = safa_uoprivilege_id;
                                            $("#row_"+del).remove();
                                             <? if (lang('global_lang') == 'ar'): ?>
                                                $('.updated_msg').text("<?=lang('global_delete_confirm')?>"+" "+name);
                                                $('.updated_msg').append('<br>');
                                                $('.updated_msg').append('<input  type ="button" value="<?=lang('global_submit')?>"class="btn btn-primary" id="ok" >');
                                             <? else: ?>
                                                $('.updated_msg').text(name+" "+"<?=lang('global_delete_confirm')?>");
                                                $('.updated_msg').append('<br>');
                                                $('.updated_msg').append('<input  type ="button" value="<?=lang('global_submit')?>"class="btn btn-primary" id="ok" >');
                                             <?  endif;?>
                                              $('.updated_msg').show();
                                              $('#ok').click(function(){
                                                 $('.updated_msg').hide();
                                               });
				          }else if(msg.response==false){
                                                    $('.updated_msg').text(msg.msg);
                                                    $('.updated_msg').append('<br>');
                                                $('.updated_msg').append('<input  type ="button" value="<?=lang('global_submit')?>"class="btn btn-primary" id="ok" >');
                                                    $('.updated_msg').show();
                                                    $('#ok').click(function(){
                                                       $('.updated_msg').hide();
                                                    });
				          }
					}
				});
			}else{
				return FALSE;
			}
                      });
		});
	});
	</script>   

