
<div class="span12">
    <div class="widget">
        <div class="path-container Fright">
            <div class="path-name Fright">
                <a href=""><?= lang('global_system_management') ?></a>
            </div>
            <div class="path-arrow Fright">
            </div>
            <div class="path-name Fright">
                <a href="<?= site_url('admin/admins/index') ?>"><?= lang('menu_management_users') ?></a>
            </div>
            <div class='path-arrow Fright'>
            </div>
            <div class="path-name Fright">
                <?= lang('admins_add_title') ?>
            </div>
        </div>
    </div>
</div>


<div class="widget">
    <div class="widget-header">

        <div class="widget-header-icon Fright">
            <span class="icos-pencil2"></span>
        </div>

        <div class="widget-header-title Fright">
            <?= lang('admins_add_title') ?> 
        </div>

    </div>  

<div class="widget-container">
    <div class="block-fluid">
        <?= form_open_multipart('', 'autocomplete="off" id="admins"') ?>

        <div class="row-form">
            <div class="span6">
                <div class="span4 TAL Pleft10"><?= lang('admins_username') ?>
                    <font style="color:red" >*</font>:
                </div>
                <div class="span8"><?= form_input('username', set_value("username"), "class='input-huge validate[required]' ") ?>
                    <?= form_error('username', '<div class="bottom" style="color:red" >', '</div>'); ?>
                </div>
            </div>
            <div class="span6">
                <div class="span4 TAL Pleft10"><?= lang('admins_email') ?>
                    <font style="color:red" >*</font>:
                </div>
                <div class="span8"><?= form_input('email', set_value("email"),"class='input-huge validate[required]'" ,'autocomplete="off"' ) ?>
                    <?= form_error('email', '<div class="bottom" style="color:red" >', '</div>'); ?>
                </div>
            </div>
        </div>

        <div class="row-form">
            <div class="span6">
                <div class="span4 TAL Pleft10"><?= lang('admins_password') ?>
                    <font style="color:red" >*</font>:
                </div>
                <div class="span8"><?= form_password('password', set_value("password"), "class='input-huge validate[required]' ") ?>
                </div>
            </div>
            <div class="span6">
                <div class="span4 TAL Pleft10"><?= lang('admins_repeat_password') ?>
                    <font style="color:red" >*</font>:
                </div>
                <div class="span8"><?= form_password('passconf', set_value('passconf'), "class='input-huge validate[required]'") ?> 
                    <?= form_error('password', '<div class="bottom" style="color:red" >', '</div>'); ?>
                </div> 
            </div>
        </div>

        <div class="toolbar bottom TAC">
            <input type="submit" name="submit" value="<?= lang('global_submit') ?>" class="btn btn-primary"> &nbsp;
            <input  type ="button" value="<?= lang('global_back') ?>"class="btn btn-primary" onclick="window.location = '<?= site_url('admin/admins/index') ?>'">
        </div>

        <?= form_close() ?> 
    </div>
</div>
</div>
<script>
$("#admins").validationEngine({
prettySelect : true,
useSuffix: "_chosen",
promptPosition : "topRight:-150"
//promptPosition : "bottomLeft"
});
</script>



