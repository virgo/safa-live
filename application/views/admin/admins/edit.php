<div class="widget">
    <div class="path-container Fright">
        <div class="path-name Fright">
            <a href=""><?= lang('global_system_management') ?></a>
        </div>
        <div class="path-arrow Fright">
        </div>
        <div class="path-name Fright">
            <a href="<?= site_url('admin/admins/index') ?>"><?= lang('menu_management_users') ?></a>
        </div>
        <div class="path-arrow Fright">
        </div>
        <div class="path-name Fright">
            <?= lang('admins_edit_title') ?> <?= $admins->username ?>
        </div>
    </div>
</div>


<div class="widget">
    <div class="widget-header">
        <div class="widget-header-icon Fright">
            <span class="icos-pencil2"></span>
        </div>
        <div class="widget-header-title Fright">
            <?= lang('admins_edit_title') ?> <?= $admins->username ?>
        </div>
    </div>

    <div class="widget-container">
        <div class="block-fluid">
            <?= form_open_multipart() ?>
            <div class="row-form">
                <div class="span6">
                    <div class="span4 TAL Pleft10"><?= lang('admins_username') ?>
                        <font style="color:red" >*</font>:
                    </div>
                    <div class="span8"><?= form_input('username', set_value("username", $admins->username), "class='input-huge' ") ?>
                        <?= form_error('username', '<div class="bottom" style="color:red" >', '</div>'); ?>
                    </div>
                </div>
                <div class="span6">
                    <div class="span4 TAL Pleft10"><?= lang('admins_email') ?>
                        <font style="color:red" >*</font>:
                    </div>
                    <div class="span8"><?= form_input('email', set_value("email", $admins->email), " class='input-huge' ") ?>
                        <?= form_error('email', '<div class="bottom" style="color:red" >', '</div>'); ?>
                    </div>
                </div>
            </div>

            <div class="row-form">
                <div class="span6">
                    <div class="span4 TAL Pleft10"><?= lang('admins_password') ?>
                    </div>
                    <div class="span8"><?= form_password('password', set_value("password"), " class='input-huge' ") ?>
                        <? //= form_error('password', '<div class="bottom" style="color:red" >', '</div>'); ?>
                    </div>
                </div>
                <div class="span6">
                    <div class="span4 TAL Pleft10"><?= lang('admins_repeat_password') ?>
                    </div>
                    <div class="span8"><?= form_password('passconf', set_value('passconf'), "class='input-huge'") ?>  
                        <? //= form_error('passconf', '<div class="bottom" style="color:red" >', '</div>'); ?>
                        <?= form_error('password', '<div class="bottom" style="color:red" >', '</div>'); ?>
                    </div>
                </div>
            </div>

            <div class="row-form BGgrey">
                <div class="span12 TAC">
                    <button name="submit" class="btn btn-primary"><?= lang('global_submit') ?></button>
                    <input type ="button" value="<?= lang('global_back') ?>"class="btn" onclick="window.location = '<?= site_url('admin/admins/index') ?>'">
                </div>
            </div>
            <?= form_close() ?> 
        </div>
    </div>
</div>




