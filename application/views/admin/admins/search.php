<div class='span12' >
        <div class='widget'>
            <div class='head dark' >
                <div class="icon"><span class="icos-search"></span></div>
                <h2><?= lang('admins_search_title') ?></h2>
            </div>
            <div class='block-fluid' >
                <?= form_open('admin/admins/index', 'method="get"') ?>
                <div class="row-form">
                    <div class="span2"><?= lang('admins_username') ?></div>
                    <?= form_input('username', set_value("username"), 'class="input-small"') ?>
                    <!--<input class="input-small" name="username" type="text" value="<?//=  set_value('username')?>" pattern="[a-zA-Z0-9_-]{3,12}" autofocus title="must be alphanumeric in 3-12 chars" >-->
                    <?= form_error('username'); ?>
                </div>
                <div class="row-form">
                 <div class="span2"><?= lang('admins_email') ?></div>
                    <?//= form_input("email", set_value('email'), 'class="input-small"'); ?>
                 <input class="input-small" type="email" name="email" value="<?=  set_value('email')?>">
                    <?= form_error('email'); ?>
                </div>

                <div class="row-form">
                    <div style="text-align:center">
                        <input type="submit" name="search" value="search" class="btn btn-primary" />
                    </div>
                </div>
                <?= form_close() ?>
            </div>
        </div>
    </div>
