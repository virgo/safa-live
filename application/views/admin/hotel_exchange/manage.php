<style>
    .widget {width:98%}
    .wizerd-div {
        border-bottom: medium none !important;
        margin: -18px 0 20px;
        padding-top: 0;
    }
    .wizerd-div a {
        background: none repeat scroll 0 0 #FAFAFA;
        border: 1px solid #D5D6D6;
        border-radius: 49px;
        color: #663300;
        display: inline-block;
        margin: -6px 5px -17px -8px;
        padding: 10px 12px 8px;
    }
    a {
        color: #C09853;
    }
    .resalt-group {
        margin: 18px 0.5% 0.5%;
        padding: 0.5%;
        width: 99%;
    }
    th a.btn, th input[type="button"], th input[type="submit"], th button {
        margin: 0;
        padding: 4px 12px;
    }
    .coll_close, .coll_open {
        margin-top: 0;
    }
    .chosen-container {
        margin-top: 4px;
    }
    .warning {
        color: #C09853;
    }
</style>

<div class="widget">
    <div class="path-container Fright">
        <div class="path-name Fright">
            <a href="<?php echo  site_url().'admin/dashboard';?>"><?php echo  lang('global_system_management') ?></a>
        </div>
        <div class="path-arrow Fright"> </div>
        <div class="path-name Fright"><?= lang('hotel_exchange') ?></div>
        
        </div>
</div>


<div class="widget">

        <?= form_open("","method='post' id='frm_hotel_exchange' ") ?>
        <div class="block-fluid">
            <div class="row-form">
                <div class="span6">
                    <div class="span2"><?= lang('from_hotel') ?></div>
                    <div class="span10">
                        <?= form_dropdown("old_erp_city_id", $erp_cities, set_value("old_erp_city_id")," id='old_erp_city_id' class=' chosen-select' ") ?>
                    </div>
                </div>
                <div class="span6">
                    <div class="span2"><?= lang('') ?></div>
                    <div class="span10">
                        <?= form_dropdown("old_erp_hotel_id", $erp_hotels, set_value("old_erp_hotel_id"), "  id='old_erp_hotel_id' class=' chosen-select' ") ?>
                    </div>
                </div>
            </div>
            
            
            <div class="row-form">
                <div class="span6">
                    <div class="span2"><?= lang('to_hotel') ?></div>
                    <div class="span10">
                        <?= form_dropdown("new_erp_city_id", $erp_cities, set_value("new_erp_city_id")," id='new_erp_city_id' class=' chosen-select' ") ?>
                    </div>
                </div>
                <div class="span6">
                    <div class="span2"><?= lang('') ?></div>
                    <div class="span10">
                        <?= form_dropdown("new_erp_hotel_id", $erp_hotels, set_value("new_erp_hotel_id"), "  id='new_erp_hotel_id' class=' chosen-select' ") ?>
                    </div>
                </div>
            </div>

            
            <div class="toolbar bottom TAC">
                <input type="submit"  class="btn btn-primary" name="smt_save" value="<?= lang('global_submit') ?>" />
            </div>
        </div>
        <?= form_close() ?>
    </div>


<script type="text/javascript">
$(document).ready(function() {
    var config = {
        '.chosen-select': {width: "100%"},
        '.chosen-select-deselect': {allow_single_deselect: true},
        '.chosen-select-no-single': {disable_search_threshold: 10},
        '.chosen-select-no-results': {no_results_text: 'Oops, nothing found!'},
        '.chosen-select-width': {width: "95%"}
    }
    for (var selector in config) {
        $(selector).chosen(config[selector]);
    }


    // binds form submission and fields to the validation engine
    $("#frm_hotel_exchange").validationEngine({
            prettySelect : true,
            useSuffix: "_chosen",
            promptPosition : "topRight:-150"
            //promptPosition : "bottomLeft"
    });


    
});

</script>

<script>
    $(document).ready(function(){
       $("#old_erp_city_id").change(function() {
            $.get('<?= site_url("admin/hotel_exchange/get_hotels_by_city_ajax") ?>/' + $(this).val(), function(data) {
                $("#old_erp_hotel_id").html(data);
                $("#old_erp_hotel_id").trigger("chosen:updated");
            });
            
        });

       $("#new_erp_city_id").change(function() {
           $.get('<?= site_url("admin/hotel_exchange/get_hotels_by_city_ajax") ?>/' + $(this).val(), function(data) {
               $("#new_erp_hotel_id").html(data);
               $("#new_erp_hotel_id").trigger("chosen:updated");
           });
           
       });
    });
</script>
