<div class="row-fluid">
   
<div class="widget">
    <div class="path-container Fright">
        <div class="path-name Fright">
        <a href="<?php echo   site_url('admin/dashboard')?>"> <?php echo  lang('global_system_management') ?></a>
        </div>
        <div class="path-arrow Fright">
        </div>
        <div class="path-name Fright">
        <a href="<?= site_url('admin/uo_usergroups/index') ?>"> <?= lang('uo_usergroups_title') ?></a>
        </div>
        <div class="path-arrow Fright">
        </div>
        <div class="path-name Fright">
             <?= lang('uo_usergroups_edit_title') ?> <?= $items->{name()} ?> 
        </div>
        
        
        
    </div>
</div>


    <div class="widget">
         
        
        <div class="widget-header">
	        <div class="widget-header-icon Fright"> <span class="icos-pencil2"></span> </div>
	        <div class="widget-header-title Fright">
	            <?= lang('uo_usergroups_edit_title') ?> <?= $items->{name()} ?>
	        </div>
	        
	    	</div>
                               
        <div class="block-fluid">
            <?= form_open() ?>
            <div class="row-form">
                <div class="span6">
                    <div class="span4"> <?= lang('uo_usergroups_name_ar') ?><span style="color: red">*</span>:</div>
                    <div class="span8">
                        <?= form_input('name_ar', set_value("name_ar",$items->name_ar), " ") ?>
                        <?= form_error('name_ar', '<div class="bottom" style="color:red" >', '</div>'); ?>
                    </div>
                </div>
                <div class="span6">
                    <div class="span4"> <?= lang('uo_usergroups_name_la') ?><span style="color: red">*</span>:</div>
                    <div class="span8">
                        <?= form_input('name_la', set_value("name_la",$items->name_la), " ") ?>
                        <?= form_error('name_la', '<div class="bottom" style="color:red" >', '</div>'); ?>
                    </div>
                </div>
            </div>

            <div class="row-form">
                <?if(isset($privileges) ):?>
                   <div class="span4"><?=lang('uo_usergroups_privilege')?>:</div><br><br><br><br>
                  <?foreach ($privileges as $privilege){
                      
//              echo'<input type="checkbox"   name="privilege[]" value="'.$item->safa_uoprivilege_id.'">';

                                        if ($privilege->name_ar != NULL): ?>
                                        <?
                                        $dataCheck = array(
                                           'name' => 'privilege[]',
                                            'id' => 'checkbtn2',
                                            'value' => $privilege->safa_uoprivilege_id,
                                            'checked' => set_checkbox('privilege[]', $privilege->safa_uoprivilege_id, in_array($privilege->safa_uoprivilege_id, $user_privileges))
                                        );
                                        echo form_checkbox($dataCheck);
                                        ?>
                                    <? else : ?>
                                        <?
                                        $inputpreferences = array(
                                            'checked' => false,
                                            'disabled' => 'true'
                                        );
                                        echo form_checkbox($inputpreferences);
                                        ?>


                                    <? endif ?>
                                                    <?if (config('language') == 'arabic') {
                                        echo $privilege->name_ar;
                                        if (isset($privilege->description_ar) && !empty($privilege->description_ar))
                                            echo ' ( <span style="color:red">' . $privilege->description_ar . '</span> ) ';
                                    }

                                    else {
                                        echo $privilege->name_la;
                                        if (isset($privilege->description_la) && !empty($privilege->description_la))
                                            echo ' ( <span style="color:red">' . $privilege->description_la . '</span> ) ';
                                    }?>
                <? echo'<br>';?><? echo'<br>';?>
                       <? }?>
             <?  endif;?>
                
            </div>

            <div class="toolbar bottom TAC"><input type="submit" name="submit" value="<?= lang('global_submit') ?>" class="btn btn-primary"> &nbsp;
                <input  type ="button" value="<?= lang('global_back') ?>"class="btn btn-primary" onclick="window.location = '<?= site_url('admin/uo_usergroups/index') ?>'">
            </div>
            <?= form_close() ?>     
        </div>
    </div>
</div>

