<span class="span8">

</span>
<div class="row-fluid">
    
    
    <div class="widget">
    <div class="path-container Fright">
    	<div class="path-name Fright">
            <a href="<?php echo  site_url().'admin/dashboard'; 
            
            ?>"><?php echo  lang('global_system_management') ?></a>
        </div>
        <div class="path-arrow Fright"></div>
        <div class="path-name Fright">  <a href="<?= site_url('admin/ports/index')?>"><?= lang('menu_main_ports')?></a></div>
        
         <div class="path-arrow Fright"></div>
        <div class="path-name Fright">  <?= lang('ports_add_title') ?></div>
        
        
    </div>
            
	</div>
    
    
    <div class="widget">
       
        
         <div class="widget-header">
        <div class="widget-header-icon Fright"> <span class="icos-pencil2"></span> </div>
        <div class="widget-header-title Fright">
            <?php echo  lang('ports_add_title') ?>
        </div>
    	</div>
                               
        <div class="block-fluid">
            <?= form_open_multipart(false, 'id="portsadd" ') ?>
            <div class="row-form">
                <div class="span6">
                    <div class="span4"> <?= lang('ports_name_ar') ?><span style="color: red">*</span>:</div>
                    <div class="span8">
                        <?= form_input('name_ar', set_value("name_ar") , "class='validate[required]'") ?>
                        <?= form_error('name_ar', '<div class="bottom" style="color:red" >', '</div>'); ?>
                    </div>
                </div>
                <div class="span6">
                    <div class="span4"><?= lang('ports_name_la') ?><span style="color: red">*</span>:</div>
                    <div class="span8">
                        <?= form_input('name_la', set_value("name_la") , "class='validate[required]'") ?>
                        <?= form_error('name_la', '<div class="bottom" style="color:red" >', '</div>'); ?>
                    </div>
                </div>
            </div>
 
            <div class="row-form">
                <div class="span6">
                    <div class="span4"><?= lang('ports_country_name') ?><span style="color: red">*</span>:</div>
                    <div class="span8">
                        <?= form_dropdown('erp_country_id', ddgen('erp_countries', array('erp_country_id', name())), set_value('erp_country_id'), "  class='select' style='width:100%;' id='erp_country_id' ") ?>
                        <?= form_error('erp_country_id', '<div class="bottom" style="color:red" >', '</div>'); ?>
                    </div>
                </div>
                <div class="span6">
                    <div class="span4"><?= lang('ports_city_id') ?><span style="color: red">*</span>:</div>
                    <div class="span8">
                        <?= form_dropdown("erp_city_id", array("0" => lang('')), set_value("erp_city_id"), "id='erp_city_id'    style='width:100%;'") ?>
                        <?= form_error('erp_city_id', '<div class="bottom" style="color:red" >', '</div>'); ?>
                    </div>
                </div>

                
            </div>
            <div class="row-form">
                
                <div class="span6">
                    <div class="span4"> <?= lang('ports_country_code') ?><span style="color: red">*</span>:</div>
                    <div class="span8">
                        <?= form_input('country_code', set_value("country_code"),"id='country_code' class='validate[required]'" ) ?>
                        <?= form_error('country_code', '<div class="bottom" style="color:red" >', '</div>'); ?>
                    </div>
                </div>
                <div class="span6">
                    <div class="span4"> <?= lang('ports_code') ?><span style="color: red">*</span>:</div>
                    <div class="span8">
                        <?= form_input('code', set_value("code") , "class='validate[required]'") ?>
                        <?= form_error('code', '<div class="bottom" style="color:red" >', '</div>'); ?>
                    </div>
                </div>
                
            </div>
            
            <div class="row-form">
              <div class="span6">
                    <div class="span4"><?= lang('ports_world_area_code') ?><span style="color: red">*</span>:</div>
                    <div class="span8">
                        <?= form_input('world_area_code', set_value("world_area_code") , "class='validate[required]'") ?>
                        <?= form_error('world_area_code', '<div class="bottom" style="color:red" >', '</div>'); ?>
                    </div>
                </div>  
            </div>

   
            <div class="toolbar bottom TAC"><input type="submit" name="submit" value="<?= lang('global_submit') ?>" class="btn btn-primary"> &nbsp;
                <input  type ="button" value="<?= lang('global_back') ?>"class="btn btn-primary" onclick="window.location = '<?= site_url('admin/ports/index') ?>'">
            </div>

            <?= form_close() ?> 
        </div>
    </div>
</div> 

<script>
        $(document).ready(function() {
            $("#erp_country_id").change(function() {
                //sending the request to get the cities//
                $.get('<?= site_url("admin/ports/get_erp_city") ?>/' + $(this).val(), function(data) {
                    $("#erp_city_id").html(data);
                });
                $.get('<?= site_url("admin/ports/get_country_code") ?>/' + $(this).val(), function(data) {
                    $("#country_code").val(data);
                });
                
            });
            var country_id = $("#erp_country_id").val();
            $.get('<?= site_url("admin/ports/get_erp_city") ?>/' + country_id, function(data) {
                $("#erp_city_id").html(data);
                $("#erp_city_id").val('<?= $this->input->post('erp_city_id') ?>');
            });
            
            $.get('<?= site_url("admin/ports/get_country_code") ?>/' + $(this).val(), function(data) {
                    $("#country_code").val(data);
                    $("#country_code").val('<?= $this->input->post('country_code') ?>');
                });
        });
        
        $("#portsadd").validationEngine({
prettySelect : true,
useSuffix: "_chosen",
promptPosition : "topRight:-150"
//promptPosition : "bottomLeft"
});
</script>

