<div class="row-fluid" >
   
    
<div class="widget">
    <div class="path-container Fright">
        <div class="path-name Fright">
        <a href="<?php echo   site_url('admin/dashboard')?>"> <?php echo  lang('global_system_management') ?></a>
        </div>
        <div class="path-arrow Fright">
        </div>
        <div class="path-name Fright">
            <a href="<?=  site_url('admin/safa_uos/index')?>"><?= lang('parent_child_title') ?></a>
        </div>
        
        <div class="path-arrow Fright">
        </div>
        <div class="path-name Fright">
            <? if (lang('global_lang') == 'ar'): ?><?= $item->name_ar ?><?else:?><?= $item->name_la?><?  endif;?>
        </div>
        
        
    </div>
 
</div>
      
</div>
<div class="row-fluid">
    <div class="widget">
        
        
        <div class="widget-header">
	        <div class="widget-header-icon Fright"> <span class="icos-pencil2"></span> </div>
	        <div class="widget-header-title Fright">
	            <?= lang('safa_uos_edit_title') ?> <? if (lang('global_lang') == 'ar'): ?><?= $item->name_ar ?><?else:?><?= $item->name_la?><?  endif;?>
	        </div>
	        
	    	</div>
                               
        <div class="block-fluid">
           <?= form_open_multipart('','autocomplete=" off " ') ?>
            <div class="row-form">
                <div class="span6">
                    <div class="span4"><?= lang('safa_uos_name_ar') ?><?if(name()=='name_ar'):?><font style="color:red" >*</font><?endif;?></div>
                    <div class="span8">
                        <?= form_input('name_ar', set_value("name_ar",$item->name_ar)) ?>
                        <?= form_error('name_ar', '<span class="bottom" style="color:red" >', '</span>'); ?>
                    </div>
                </div>
                <div class="span6">
                    <div class="span4"><?= lang('safa_uos_name_la') ?><?if(name()=='name_la'):?><font style="color:red" >*</font><?endif;?></div>
                    <div class="span8" >
                        <?= form_input('name_la', set_value("name_la",$item->name_la)) ?>
                        <?= form_error('name_la', '<span class="bottom" style="color:red" >', '</span>'); ?>  
                    </div>     
                </div>
            </div>
            <div class="row-form">
                <div class="span6">
                    <div class="span4"><?= lang('safa_uos_phone') ?>:</div>
                    <div class="span8" >
                        <?= form_input('phone', set_value("phone",$item->phone)) ?>
                    </div>
                </div>
                <div class="span6">
                    <div class="span4"><?= lang('safa_uos_mobile') ?>:</div>
                    <div class="span8" >
                        <?= form_input('mobile', set_value("mobile",$item->mobile)) ?>
                    </div>
                </div>
            </div>
            <div class="row-form">
                <div class="span6">
                    <div class="span4"><?= lang('safa_uos_email') ?>:</div>
                    <div class="span8" >
                        <?= form_input('email', set_value("email",$item->email)) ?>
                        <?= form_error('email', '<span class="bottom" style="color:red" >', '</span>'); ?>
                    </div>
                </div>
                <div class="span6">
                    <div class="span4"><?= lang('safa_uos_fax') ?>:</div>
                    <div class="span8" >
                        <?= form_input('fax', set_value("fax",$item->fax)) ?>
                    </div>
                </div>
            </div>
            <div class="row-form">
                <div class="span6">
                    <div class="span4"><?= lang('safa_uos_code') ?><font style="color:red" >*</font></div>
                    <div class="span8" >
                        <?= form_input('code', set_value('code',$item->code)) ?>
                        <?= form_error('code', '<span class="bottom" style="color:red" >', '</span>'); ?>
                    </div>
                </div>
            </div>
            <div class="row-form">
                <div class="span6">
                    <div class="span4"><?= lang('erp_uasp_id') ?><font style="color:red" >*</font></div>
                    <div class="span8" >
                        <?= form_input('erp_uasp_id', set_value('erp_uasp_id',$item->erp_uasp_id)) ?>
                        <?= form_error('erp_uasp_id', '<span class="bottom" style="color:red" >', '</span>'); ?>
                    </div>
                </div>
            </div>
            <div class="toolbar bottom TAC">
                <button name="submit" class="btn btn-primary"><?= lang('global_submit') ?></button>
                <input   type="button" name="back" value="<?= lang('global_back') ?>" class="btn btn-primary" onclick="window.location = '<?= site_url('admin/safa_uos/index') ?>'">
            </div>

            <?= form_close() ?> 
        </div>
    </div>
</div> 
<!--<script type='text/javascript' src='<? //= MODULE_JS     ?>/poll/add.js'></script>-->

