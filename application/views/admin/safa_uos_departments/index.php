<!-- By Gouda, For popup -->
<script type="text/javascript" src='<?= CSS_JS ?>/form/form.js'></script>
<link rel="stylesheet" href="<?= CSS ?>/bootstrap/bootstrap.min.new.css"/>
     


<div class="row-fluid">

    
    
       
<div class="widget">
    <div class="path-container Fright">
    	<div class="path-name Fright">
            <a href="<?php 
            if(session('uo_id')) {
            	echo  site_url().'uo/dashboard'; 
            } else {
            	echo  site_url().'admin/dashboard';
            }
            ?>"><?php echo  lang('global_system_management') ?></a>
        </div>
        
        <div class="path-arrow Fright"></div>
        <div class="path-name Fright">  <?= lang('safa_uos_departments') ?></div>
    </div>
    <a href="<?= site_url("admin/safa_uos_departments/manage") ?>/<?= $safa_uo_id ?>" class="btn Fleft"><?= lang('global_add') ?></a>
</div>


    <div class="widget">
        
<style>
    .updated_msg{
        display:none;
        background-color:#ccee97;
        font-weight: bold;
        text-align: center;
        border:3px solid #cccdc9; 
        padding:20px;  
        margin-bottom:10px;
        border-radius:15px;
        margin:10px; 
    }
</style>
<div class='row-fluid' align='center' >
    <div  class='updated_msg' >
        <br><input  type ="button" value="<?=lang('global_submit')?>"class="btn btn-primary" id='ok' >
    </div> 
 </div>
        
       
        <div class="widget-header">
        <div class="widget-header-icon Fright"> <span class="icos-pencil2"></span> </div>
        <div class="widget-header-title Fright">
            <?php echo  lang('safa_uos_departments') ?>
        </div>
    	</div>
                      
        <div class="block-fluid">
            <table cellpadding="0" class="fsTable" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th><?= lang('name_ar') ?></th>
                        <th><?= lang('name_la') ?></th>
                        <th><?= lang('email') ?></th>
                        <th><?= lang('global_actions')?></th>
                    </tr>
                </thead>
                <tbody>
                    <? if (isset($items)) { ?>

                     <? foreach($items as $item) {?>
                    <tr id="row_<?php echo $item->safa_uos_department_id;?>">
                        <td><?= $item->name_ar ?></td>
                        <td><?= $item->name_la ?></td>
                        <td><?= $item->email ?></td>
                        
                        <td class="TAC">
                          <a href="<?= site_url("admin/safa_uos_departments/manage") ?>/<?= $item->safa_uo_id ?>/<?= $item->safa_uos_department_id ?>"><span class="icon-pencil"></span></a>
                          <a href="<?= site_url("admin/safa_uos_departments/delete") ?>/<?= $item->safa_uo_id ?>/<?= $item->safa_uos_department_id ?>" onclick="return confirm('<?= lang('global_are_you_sure_you_want_to_delete')?>')"><span class="icon-trash"></span></a>
                       
                        </td>
                        
                    </tr>
                   <?php }?>
                       <?php } ?>

                </tbody>
            </table>
           
            <?= form_close() ?> 
        </div>
        
    </div>
</div>
  

