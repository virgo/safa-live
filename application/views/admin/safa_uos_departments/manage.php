<?php
$name_ar = '';
$name_la = '';
$email = '';

$screen_title = lang('add_safa_uos_departments');

if (isset($item)) {
    $name_ar = $item->name_ar;
    $name_la = $item->name_la;
    $email = $item->email;

    $screen_title = lang('edit_safa_uos_departments');
}
?>
<script>.widget{width:98%;}</script>

<div class="widget">

    <div class="path-container Fright">
        <div class="path-name Fright">
            <a href="<?php echo site_url() ?>"><?php echo lang('global_system_management') ?></a>
        </div>
        <div class="path-arrow Fright"></div>
        <div class="path-name Fright"> <a href="<?php echo site_url('admin/safa_uos_departments/index/'.$safa_uo_id) ?>"><?php echo lang('safa_uos_departments') ?></a></div>

        <div class="path-arrow Fright"></div>
        <div class="path-name Fright"><?php echo $screen_title; ?></div>

    </div>
</div>


<?php echo form_open_multipart(false, 'id="frm_safa_uos_departments" ') ?>


<div class="widget">
    <div class="widget-header">

        <div class="widget-header-icon Fright">
            <span class="icos-pencil2"></span>
        </div>

        <div class="widget-header-title Fright">
<?php echo $screen_title; ?>

        </div>
    </div>

    <div class="widget-container">  
<? if (validation_errors()) { ?>
    <?php echo validation_errors(); ?>        
        <? } ?>

        <div class="row-form">
            <div class="span6">
                <div class="span4 TAL Pleft10">
				<?php echo lang('name_ar') ?> <font style='color:red' >*</font>
                </div>
                <div class="span8">
                    <?php echo form_input('name_ar', set_value("name_ar", $name_ar), " style='' id='name_ar' class='validate[required] input-full' ") ?>
                </div>
            </div>

            <div class="span6">
                <div class="span4 TAL Pleft10">
				<?php echo lang('name_la') ?> <font style='color:red' >*</font>
                </div>
                <div class="span8">
                    <?php echo form_input('name_la', set_value("name_la", $name_la), " style='' id='name_la' class='validate[required] input-full' ") ?>
                </div>
            </div>

        </div>

        <div class="row-form">
            <div class="span6">
                <div class="span4 TAL Pleft10">
				<?php echo lang('email') ?> <font style='color:red' >*</font>
                </div>
                <div class="span8">
                    <?php echo form_email('email', set_value("email", $email), " style='' id='email' class='validate[required] input-full'  ") ?>
                </div>
            </div>

            

        </div>

    </div>
</div>


<div class="widget TAC">
    <input type="submit" class="btn" name="smt_send" value="<?php echo lang('global_submit') ?>" style="margin:10px;padding: 5px;height: auto">
</div>


<?php echo form_close() ?>


<div class="footer"></div>


<script type="text/javascript">
    $(document).ready(function() {
        var config = {
            '.chosen-select': {width: "100%"},
            '.chosen-select-deselect': {allow_single_deselect: true},
            '.chosen-select-no-single': {disable_search_threshold: 10},
            '.chosen-select-no-results': {no_results_text: 'Oops, nothing found!'},
            '.chosen-select-width': {width: "95%"}
        }
        for (var selector in config) {
            $(selector).chosen(config[selector]);
        }

        // binds form submission and fields to the validation engine
        $("#frm_safa_uos_departments").validationEngine({
            prettySelect: true,
            useSuffix: "_chosen",
            promptPosition: "topRight:-150"
                    //promptPosition : "bottomLeft"
        });
    });

</script>
