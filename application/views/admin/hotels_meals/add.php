<div class="span12">
    <div class="widget">
        <div class="path-container Fright">
            <div class="path-name Fright">
                <a href=""><?= lang('global_system_management') ?></a>
            </div>
            <div class="path-arrow Fright">
            </div>
            <div class="path-name Fright">
                <a href="<?= site_url('admin/hotels_meals/index') ?>"><?= lang('hotels_meals') ?></a>
            </div>
            <div class='path-arrow Fright'>
            </div>
            <div class="path-name Fright">
                <?= lang('add_hotels_meals') ?>
            </div>
        </div>
    </div>
</div>

<div class="widget">
    <div class="widget-header">

        <div class="widget-header-icon Fright">
            <span class="icos-pencil2"></span>
        </div>

        <div class="widget-header-title Fright">
            <?= lang('add_hotels_meals') ?> 
        </div>

    </div>  



      <div class="widget-container">               
        <div class="block-fluid">
           <?= form_open_multipart('', 'autocomplete="off" ') ?> 
            <div class="row-form">
                <div class="span6">
                    <div class="span4 TAL Pleft10"><?= lang('hotels_meals_name_ar') ?><?if(name()=='name_ar'):?><font style="color:red" >*</font><?endif;?></div>
                    <div class="span6">
                        <?= form_input('name_ar', set_value("name_ar"),"class='input-huge' ") ?>
                        <?= form_error('name_ar', '<span class="bottom" style="color:red" >', '</span>'); ?>
                    </div>
                </div>
                <div class="span6">
                    <div class="span4 TAL Pleft10"><?= lang('hotels_meals_name_la') ?><?if(name()=='name_la'):?><font style="color:red" >*</font><?endif;?></div>
                    <div class="span6" >
                        <?= form_input('name_la', set_value("name_la"),"class='input-huge' ") ?>
                        <?= form_error('name_la', '<span class="bottom" style="color:red" >', '</span>'); ?>  
                    </div>     
                </div>
               
            </div>
            
            <div class="row-form">
                <div class="span6">
                    <div class="span4 TAL Pleft10"><?= lang('hotels_meals_description_ar') ?></div>
                    <div class="span6" >
                      <?= form_textarea('description_ar', set_value("description_ar"),"class='input-huge' ") ?>
                      <?= form_error('description_ar', '<div class="bottom" style="color:red" >', '</div>'); ?>
                    </div>     
                </div>
                
                <div class="span6">
                    <div class="span4 TAL Pleft10"><?= lang('hotels_meals_description_la') ?></div>
                    <div class="span6" >
                      <?= form_textarea('description_la', set_value("description_la"),"class='input-huge'") ?>
                      <?= form_error('description_la', '<div class="bottom" style="color:red" >', '</div>'); ?>
                    </div>     
                </div> 
            </div>
          
            
            <div class="row-form">
                <div class="span6">
                    <div class="span4 TAL Pleft10"><?= lang('hotels_meals_code') ?><font style="color:red" >*</font></div>
                    <div class="span6" >
                      <?= form_input('code', set_value("code"),"class='input-huge'") ?>
                      <?= form_error('code', '<div class="bottom" style="color:red" >', '</div>'); ?>
                    </div>     
                </div>
           </div>
            
            <div class="toolbar bottom TAC">
                <input type="submit" name="submit" value="<?= lang('global_submit') ?>" class="btn btn-primary"> &nbsp;
                <a href="<?= site_url('admin/hotels_meals/index') ?>" class="btn btn-primary"  ><?= lang('global_back') ?></a>
           </div>

            <?= form_close() ?> 
        </div>
</div>
</div>
 



