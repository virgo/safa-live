<div class="span12">
    <div class="widget">
        <div class="path-container Fright">
            <div class="path-name Fright">
                <a href="<?= site_url('admin/dashboard') ?>"><?= lang('global_system_management') ?></a>
            </div>
            <div class="path-arrow Fright"></div>
            <div class="path-name Fright">
                <?= lang('seasons') ?>
            </div>
        </div>
    </div>
</div>



<div class="row-fluid">
    <div class="widget">
        <div class="widget-header">
            <div class="widget-header-icon Fright">
                <span class="icos-pencil2"></span>
            </div>
            <div class="widget-header-title Fright">
                  <?= lang('seasons') ?>
            </div><a class="btn Fleft" href="<?= site_url('admin/seasons/add/') ?>"><?= lang('global_add_new_record') ?></a>
        </div>
        <div class="widget-container">
            <div class='table-responsive' >
                <table cellpadding="0" class="" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th><?= lang('seasons_name_ar') ?></th>
                            <th><?= lang('seasons_name_la')?></th>
                            <th><?= lang('seasons_start_date') ?></th>
                            <th><?= lang('seasons_end_date')?></th>
                            <th><?= lang('global_actions') ?></th>
                        </tr>
                    </thead>
                    <tbody>
                       <? if (isset($items)): ?>
                            <? foreach ($items as $item): ?>
                               <tr id="row_<?php echo $item->erp_season_id;?>">
                                    <td><?= $item->name_ar ?></td>
                                    <td><?= $item->name_la ?></td>
                                    <td><?= $item->start_date ?></td>
                                    <td><?= $item->end_date?></td>
                                    <td class="TAC">
                                        <a href="<?= site_url("admin/seasons/edit") ?>/<?= $item->erp_season_id ?>"><span class="icon-pencil"></span></a>
                                        <a href="<?= site_url("admin/seasons/delete") ?>/<?= $item->erp_season_id ?>" name="<? if (lang('global_lang') == 'ar'): ?><?= $item->name_ar?><? else: ?><?= $item->name_la ?><? endif ?>"  id="<?= $item->erp_season_id ?>" class=" delete_item" data-toggle="modal"><span class="icon-trash"></span></a>
                                    </td>
                                </tr>
                            <? endforeach ?>
                        <? endif ?>
                    </tbody>
                </table>
                <?= $pagination ?>
            </div>
        </div>
    </div>
</div>