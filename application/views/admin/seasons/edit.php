<div class="span12">
    <div class="widget">
        <div class="path-container Fright">
            <div class="path-name Fright">
                <a href="<?= site_url('admin/dashboard') ?>"><?= lang('global_system_management') ?></a>
            </div>
            <div class="path-arrow Fright"></div>
            <div class="path-name Fright">
                <?= lang('seasons') ?>
            </div>
        </div>
    </div>
</div>









<div class="row-fluid">
    <div class="widget">
        <div class="widget-header">
            <div class="widget-header-icon Fright"><span class="icos-pencil2"></span></div>
            <div class="widget-header-title Fright"><?= lang('seasons') ?></div>
        </div>
        <div class="block-fluid">
            <? if (validation_errors()): ?>
                <?= validation_errors(); ?>
            <? endif ?> 
            <?= form_open_multipart('', 'autocomplete="off" ') ?> 

            <div class="row-form">

                <div class="span6">
                    <div class="span4"><?= lang('seasons_name_ar') ?><?if(name()=='name_ar'):?><font style="color:red" >*</font><?endif;?></div>
                    <div class="span6">
                        <?= form_input('name_ar', set_value("name_ar", $items->name_ar)) ?>
                    </div>
                </div>

                <div class="span6">
                   <div class="span4"><?= lang('seasons_name_la') ?><?if(name()=='name_la'):?><font style="color:red" >*</font><?endif;?></div>
                    <div class="span6">
                        <?= form_input('name_la', set_value("name_la", $items->name_la)) ?>
                    </div>
                </div>
            </div>
            
            
            <div class="row-form">
                <div class="span6">
                    <div class="span4" >
                        <?= lang('seasons_start_date') ?>
                        <font style='color:red'>*</font>
                    </div>
                    <div class="span6" >
                        <input class="date" name="start_date"  type="text" value="<?= set_value('start_date', $items->start_date) ?>" style=" direction:ltr" />
                    </div>
                </div>
                <div class="span6">
                    <div class="span4">
                        <?= lang('seasons_end_date') ?>
                        <font style='color:red'>*</font>
                    </div>
                    <div class="span6" >
                        <input class="date" name="end_date" type="text" value="<?= set_value('end_date', $items->end_date) ?>" style=" direction:ltr" />
                    </div>
                </div>
            </div>
        </div>




            <div class="toolbar bottom TAC">
                <button name="submit" class="btn btn-primary"><?= lang('global_submit') ?></button>
                <a href="<?= site_url('admin/seasons/index') ?>" class="btn btn-primary"  ><?= lang('global_back') ?></a>
            </div>

            <?= form_close() ?> 

            <script type="text/javascript">
                $(function() {
                    $('.date').datepicker({ dateFormat: 'yy-mm-dd'});
                });
            </script>

        </div>
    </div>
</div> 
