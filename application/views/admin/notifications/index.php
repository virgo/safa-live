<link type="text/css" rel="stylesheet" href="<?php echo base_url().'static/js/admin/notifications/contextmenu';?>/webtoolkit.contextmenu.css" />
<script type="text/javascript" src="<?php echo base_url().'static/js/admin/notifications/contextmenu';?>/webtoolkit.contextmenu.js"></script>

<script type="text/javascript">
	SimpleContextMenu.setup({'preventDefault':false, 'preventForms':false});
	SimpleContextMenu.attach('menu_shortcut_class_subject', 'menu_subject');
	SimpleContextMenu.attach('menu_shortcut_class_body', 'menu_body');
	
	
	function insertAtCursor(myField, myValue) {
	    //IE support
	    if (document.selection) {
	        myField.focus();
	        sel = document.selection.createRange();
	        sel.text = myValue;
	    }
	    //MOZILLA and others
	    else if (myField.selectionStart || myField.selectionStart == '0') {
	        var startPos = myField.selectionStart;
	        var endPos = myField.selectionEnd;
	        myField.value = myField.value.substring(0, startPos)
	            + myValue
	            + myField.value.substring(endPos, myField.value.length);
	    } else {
	        myField.value += myValue;
	    }
	}

	$(document).ready(function()
	{
		$("#event").change(function()
		{
			
			var event_key=$(this).val();
			var dataString = 'event_key='+ event_key;

			$.ajax
			({
				type: "POST",
				url: "<?php echo base_url().'admin/notifications/getTagDataForAjax'?>",
				data: dataString,
				cache: false,
				success: function(html)
				{
					texts = html.split('#$@&*#@#$#');
					$("#subject").val(texts[0]);
					$("#body").val(texts[1]);
					
					
				}
			});
	
		});

	});
	
</script>
	
	
 
<ul id="menu_subject" class="SimpleContextMenu">
<!-- 
<li><a href="javascript:void();" onclick="insertAtCursor(document.frm_notification.subject, '#*trip#*');"><?php echo lang('trip') ?></a></li>
<li><a href="javascript:void();" onclick="insertAtCursor(document.frm_notification.subject, '#*trip_no#*');"><?php echo lang('trip_no') ?></a></li>
<li><a href="javascript:void();" onclick="insertAtCursor(document.frm_notification.subject, '#*trip_date#*');"><?php echo lang('trip_date') ?></a></li>
<li><a href="javascript:void();" onclick="insertAtCursor(document.frm_notification.subject, '#*the_date#*');"><?php echo lang('the_date') ?></a></li>
<li><a href="javascript:void();" onclick="insertAtCursor(document.frm_notification.subject, '#*trip_arrival_datetime#*');"><?php echo lang('trip_arrival_datetime') ?></a></li>
<li><a href="javascript:void();" onclick="insertAtCursor(document.frm_notification.subject, '#*trip_leaving_datetime#*');"><?php echo lang('trip_leaving_datetime') ?></a></li>
<li><a href="javascript:void();" onclick="insertAtCursor(document.frm_notification.subject, '#*madina_go_to_datetime#*');"><?php echo lang('madina_go_to_datetime') ?></a></li>
<li><a href="javascript:void();" onclick="insertAtCursor(document.frm_notification.subject, '#*makka_go_to_datetime#*');"><?php echo lang('makka_go_to_datetime') ?></a></li>
<li><a href="javascript:void();" onclick="insertAtCursor(document.frm_notification.subject, '#*hotel_arrival_datetime#*');"><?php echo lang('hotel_arrival_datetime') ?></a></li>
<li><a href="javascript:void();" onclick="insertAtCursor(document.frm_notification.subject, '#*hotel_leaving_datetime#*');"><?php echo lang('hotel_leaving_datetime') ?></a></li>
 -->

<?php 
foreach($erp_notifications_tags_rows as $erp_notifications_tags_row) {
$erp_notifications_tags	=$erp_notifications_tags_row->notifications_tags;
$erp_notifications_name	=$erp_notifications_tags_row->{name()};
?> 
<li><a href="javascript:void();" onclick="insertAtCursor(document.frm_notification.subject, '<?php echo $erp_notifications_tags;?>');"><?php echo $erp_notifications_name;?></a></li>
<?php 
}
?>  

</ul> 
	
<ul id="menu_body" class="SimpleContextMenu">
<!-- 
<li><a href="javascript:void();" onclick="insertAtCursor(document.frm_notification.body, '#*trip#*');"><?php echo lang('trip') ?></a></li>
<li><a href="javascript:void();" onclick="insertAtCursor(document.frm_notification.body, '#*trip_no#*');"><?php echo lang('trip_no') ?></a></li>
<li><a href="javascript:void();" onclick="insertAtCursor(document.frm_notification.body, '#*trip_date#*');"><?php echo lang('trip_date') ?></a></li>
<li><a href="javascript:void();" onclick="insertAtCursor(document.frm_notification.body, '#*the_date#*');"><?php echo lang('the_date') ?></a></li>
<li><a href="javascript:void();" onclick="insertAtCursor(document.frm_notification.body, '#*trip_arrival_datetime#*');"><?php echo lang('trip_arrival_datetime') ?></a></li>
<li><a href="javascript:void();" onclick="insertAtCursor(document.frm_notification.body, '#*trip_leaving_datetime#*');"><?php echo lang('trip_leaving_datetime') ?></a></li>
<li><a href="javascript:void();" onclick="insertAtCursor(document.frm_notification.body, '#*madina_go_to_datetime#*');"><?php echo lang('madina_go_to_datetime') ?></a></li>
<li><a href="javascript:void();" onclick="insertAtCursor(document.frm_notification.body, '#*makka_go_to_datetime#*');"><?php echo lang('makka_go_to_datetime') ?></a></li>
<li><a href="javascript:void();" onclick="insertAtCursor(document.frm_notification.body, '#*hotel_arrival_datetime#*');"><?php echo lang('hotel_arrival_datetime') ?></a></li>
<li><a href="javascript:void();" onclick="insertAtCursor(document.frm_notification.body, '#*hotel_leaving_datetime#*');"><?php echo lang('hotel_leaving_datetime') ?></a></li>
 -->


<?php 
foreach($erp_notifications_tags_rows as $erp_notifications_tags_row) {
$erp_notifications_tags	=$erp_notifications_tags_row->notifications_tags;
$erp_notifications_name	=$erp_notifications_tags_row->{name()};
?> 
<li><a href="javascript:void();" onclick="insertAtCursor(document.frm_notification.body, '<?php echo $erp_notifications_tags;?>');"><?php echo $erp_notifications_name;?></a></li>
<?php 
}
?>  

</ul>

 
 
<span class="span8">

</span>
<div class="row-fluid">
    
    
    <div class="widget">
    <div class="path-container Fright">
        <div class="path-name Fright">
        <a href="<?php echo   site_url('admin/dashboard')?>"> <?php echo  lang('global_system_management') ?></a>
        </div>
        <div class="path-arrow Fright">
        </div>
        <div class="path-name Fright">
            <?php echo  lang('menu_title') ?>
        </div>
        
    </div>
	</div> 
    
    <div class="widget">
        
        
        <div class="widget-header">
        <div class="widget-header-icon Fright"> <span class="icos-pencil2"></span> </div>
        <div class="widget-header-title Fright">
            <?php echo  lang('title') ?>
        </div>
    	</div>
                              
        <div class="block-fluid">
            <?php echo form_open_multipart("","name='frm_notification'","") ?>
            
            <div class="row-form">
                <div class="span6">
                    <div class="span4"> <?php echo lang('event_type') ?><span style="color: red">*</span>:</div>
                    <div class="span8">
                        <select name="event_type"  id='event_type'  class='select' style='width:100%;' >
						<option value=""><?php echo lang('global_select_from_menu') ?></option>
						<option value="notification"><?php echo lang('notification') ?></option>
						<option value="email"><?php echo lang('email') ?></option>
						</select>
                        <?php echo form_error('event_type', '<div class="bottom" style="color:red" >', '</div>'); ?>
                    </div>
                </div>
                
            </div>
            
            <div class="row-form">
                <div class="span6">
                    <div class="span4"> <?php echo lang('event') ?><span style="color: red">*</span>:</div>
                    <div class="span8">
                        <?php echo form_dropdown('event', ddgen('erp_system_events', array('erp_system_events_id', name())), set_value('event'), " id='event' name='event' class='select' style='width:100%;' ") ?>
                        <?php echo form_error('event', '<div class="bottom" style="color:red" >', '</div>'); ?>
                    </div>
                </div>
                
            </div>
 
            <div class="row-form">
                <div class="span6">
                    <div class="span4"><?php echo lang('subject') ?><span style="color: red">*</span>:</div>
                    <div class="span8">
                        <?php echo form_input('subject', set_value("subject")," id='subject' style='width:500px;' class='menu_shortcut_class_subject' ") ?>
                        <?php echo form_error('subject', '<div class="bottom" style="color:red" >', '</div>'); ?>
                    </div>
                </div>

                
            </div>
           
            <div class="row-form">
              <div class="span6">
                    <div class="span4"><?php echo lang('body') ?><span style="color: red">*</span>:</div>
                    <div class="span8" >
                        <?php echo form_textarea('body', set_value("body"), "style='width:500px; height:200px' id='body' class='menu_shortcut_class_body' ") ?>
                        <?php echo form_error('body', '<div class="bottom" style="color:red" >', '</div>'); ?>
                    </div>
                </div>  
            </div>
   
            <div class="toolbar bottom TAC">
            	<input type="submit" name="submit" value="<?php echo lang('global_submit') ?>" class="btn btn-primary"> &nbsp;
            </div>

            <?php echo form_close() ?> 
        </div>
    </div>
</div> 

<script type="text/javascript">
$(document).ready(function() {

    $('#event_type').change(function(){ 
        
        var event_type=$(this).val();
        
    	var dataString = 'event_type='+ event_type;
        
        $.ajax
        ({
        	type: 'POST',
        	url: '<?php echo base_url().'admin/notifications/getEventsByEventType'; ?>',
        	data: dataString,
        	cache: false,
        	success: function(html)
        	{
            	//alert(html);
        		$("#event").html(html);
        		$("#even").trigger("chosen:updated");
        		
        	}
        });
        
    });

});

</script>