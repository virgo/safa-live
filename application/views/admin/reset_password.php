<?= $this->load->view("login_header") ?>
<body>
<!--    <div class="header">
        <a href="<?//= site_url('admine/dashboard')?>" class="logo centralize"></a>
    </div>-->
    <div class="login"  id="login"> 
        <div class="wrap">
            <? if (validation_errors()): ?>
                 <div style="direction:<?=lang("global_direction")?>" >
                    <?php echo validation_errors(); ?>
                </div> 
         <? endif ?>
            <h1><?=lang('login_resetpass')?></h1>
            <hr/>
         <?=form_open("","id='validate'")?>
            <div class="row-fluid" > 
                <div>
                    <div class="span4"><?=lang('login_newpass')?></div>
                    <div class="span7"><input name="new_pass" type="password" value="<?=set_value("new_pass")?>"  class="validate[required]" /> </div>
                </div>
                <DIV  STYLE="clear:both;height:5px"></DIV>
             
                <div>
                  <div class="span4"><?=lang('login_confpass')?></div>
                    <div class="span7"><input name="conf_password" type="password" value="<?=set_value("conf_password")?>" class="validate[required]" /> </div>      
                </div>
                <DIV   STYLE="clear:both;height:5px"></DIV>
                <div>
                    <div class="span4"><?=lang('login_rememberme')?></div>
                    <div class="span7" style="" >
                        <input type="checkbox" name="rem" value="<?= set_value('remember', '1'); ?>" />
                    </div>
                </div>
                <DIV  STYLE="clear:both;height:5px"></DIV>
                <hr/>
                <div align="left" >
                        <input type="submit" value="<?=lang("global_submit")?>" class="btn  btn-primary"  />
                </div>
            </div>
         <?=form_close()?>       
        </div>
</body>
</html>

