<div class="row-fluid">



<div class="widget">
    <div class="path-container Fright">
    	<div class="path-name Fright">
            <a href="<?php echo  site_url().'admin/dashboard'; 
            
            ?>"><?php echo  lang('global_system_management') ?></a>
        </div>
        <div class="path-arrow Fright"></div>
        <div class="path-name Fright">  <a href="<?= site_url('admin/itoprivileges/index') ?>"> <?= lang('itoprivileges_title') ?></a></div>
        
        <div class="path-arrow Fright"></div>
        <div class="path-name Fright"> <?= lang('privileges_edit_title') ?> <?= $items->{name()} ?> </div>
    </div>
    
        
</div>


    <div class="widget">
        
        
        <div class="widget-header">
        <div class="widget-header-icon Fright"> <span class="icos-pencil2"></span> </div>
        <div class="widget-header-title Fright">
             <?= lang('privileges_edit_title') ?> <?= $items->{name()} ?>
             
        </div>
    	</div>
                             
        <div class="block-fluid">
            <?= form_open() ?>
            <div class="row-form">
                <div class="span6">
                    <div class="span4"> <?= lang('privileges_name_ar') ?><span style="color: red">*</span>:</div>
                    <div class="span8">
                        <?= form_input('name_ar', set_value("name_ar",$items->name_ar), " ") ?>
                        <?= form_error('name_ar', '<div class="bottom" style="color:red" >', '</div>'); ?>
                    </div>
                </div>
                <div class="span6">
                    <div class="span4"> <?= lang('privileges_name_la') ?><span style="color: red">*</span>:</div>
                    <div class="span8">
                        <?= form_input('name_la', set_value("name_la",$items->name_la), " ") ?>
                        <?= form_error('name_la', '<div class="bottom" style="color:red" >', '</div>'); ?>
                    </div>
                </div>
            </div><div class="row-form">
                <div class="span6">
                    <div class="span4"> <?= lang('privileges_description_ar') ?><span style="color: red">*</span>:</div>
                    <div class="span8">
                        <?= form_input('description_ar', set_value("description_ar",$items->description_ar), " ") ?>
                        <?= form_error('description_ar', '<div class="bottom" style="color:red" >', '</div>'); ?>
                    </div>
                </div>
                <div class="span6">
                    <div class="span4"> <?= lang('privileges_description_la') ?><span style="color: red">*</span>:</div>
                    <div class="span8">
                        <?= form_input('description_la', set_value("description_la",$items->description_la), " ") ?>
                        <?= form_error('description_la', '<div class="bottom" style="color:red" >', '</div>'); ?>
                    </div>
                </div>
            </div>
            <div class="row-form">
            <div class="span6">
                    <div class="span4"> <?= lang('privileges_role_name') ?><span style="color: red">*</span>:</div>
                    <div class="span8">
                        <?= form_input('role_name', set_value("role_name",$items->role_name), " ") ?>
                        <?= form_error('role_name', '<div class="bottom" style="color:red" >', '</div>'); ?>
                    </div>
                </div>
            </div>
          

            <div class="toolbar bottom TAC"><input type="submit" name="submit" value="<?= lang('global_submit') ?>" class="btn btn-primary"> &nbsp;
                <input  type ="button" value="<?= lang('global_back') ?>"class="btn btn-primary" onclick="window.location = '<?= site_url('admin/itoprivileges/index') ?>'">
            </div>
            <?= form_close() ?>     
        </div>
    </div>
</div>
