<span class="span8">

</span>
<div class="row-fluid">
    <div style="margin-bottom:10px; text-align:left; border:3px solid #FFF; background:url(<?= IMAGES ?>/backgrounds/bg_box_head.jpg); padding:5px 10px; height:30px;">

        <div style="float:right; text-align:right; font-size:14px; line-height:30px; color:#333;"> <?= lang('global_virgo') ?> <span style="color:#80693d"><img src="<?= IMAGES ?>/jbreadcrumb/Chevron-ar.gif" width="5" height="20"></span> <a href="<?= site_url('admin/transporter_users/index') ?>"><?= lang('menu_transport_companies_users') ?></a> <span style="color:#80693d"><img src="<?= IMAGES ?>/jbreadcrumb/Chevron-ar.gif" width="5" height="20"></span> <?= lang('safa_transporter_users_add') ?></div>
    </div>


    <div class="widget">
        <div class="head dark">
            <div class="icon"><i class="icos-pencil2"></i></div>
            <h2><?= lang('safa_transporter_users_add') ?></h2> 
        </div>                        
        <div class="block-fluid">
            <?= form_open_multipart() ?>

            <div class="row-form">
                <div class="span6">
                    <div class="span4"><?= lang('safa_transporter_users_name_ar') ?>
                        <span class="required">*</span>:
                    </div>
                    <div class="span8"><?= form_input('name_ar', set_value("name_ar"), "  ") ?>
                        <?= form_error('name_ar', '<div class="bottom" style="color:red" >', '</div>'); ?>
                    </div>
                </div>
                <div class="span6">
                    <div class="span4"><?= lang('safa_transporter_users_name_la') ?>
                        <span class="required">*</span>:
                    </div>
                    <div class="span8"><?= form_input('name_la', set_value("name_la"), "  ") ?>
                        <?= form_error('name_la', '<div class="bottom" style="color:red" >', '</div>'); ?>
                    </div>
                </div>
            </div>


            <div class="row-form">
                
               <div class="span6">
                    <div class="span4"><?= lang('safa_transporter_users_safa_transporter_id') ?>
                        <span class="required">*</span>:
                    </div>
                    <div class="span8">
                        <?= form_dropdown('safa_transporter_id', ddgen('safa_transporters', array('safa_transporter_id', name())), set_value('safa_transporter_id'), " name='s_example' class='select' style='width:100%;' ") ?>
                        <?= form_error('safa_transporter_id', '<div class="bottom" style="color:red" >', '</div>'); ?>                  
                    </div>
                </div>
                
                <div class="span6">
                    <div class="span4"><?= lang('safa_transporter_users_username') ?>
                        <span class="required">*</span>:
                    </div>
                    <div class="span8"><?= form_input('username', set_value("username"), "  ") ?>
                        <?= form_error('username', '<div class="bottom" style="color:red" >', '</div>'); ?>                  
                    </div>
                </div>
            </div>

            <div class="row-form">
                <div class="span6">
                    <div class="span4"><?= lang('safa_transporter_users_password') ?><font style="color:red" >*</font></div>
                    <div class="span8" >
                        <?= form_password('password', set_value('password')) ?>
                        <span class="bottom" style="color:red">
                            <?= form_error('password') ?>
                        </span>
                    </div>
                </div>
                <div class="span6">
                    <div class="span4"><?= lang('safa_transporter_users_repeat_password') ?><font style="color:red" >*</font></div>
                    <div class="span8">
                        <?= form_password('passconf', set_value('passconf')) ?>
                        <span class="bottom" style="color:red" >
                            <?= form_error('passconf') ?>
                        </span>
                    </div>
                </div>
            </div>


            <div class="toolbar bottom TAC"><input type="submit" name="submit" value="<?= lang('global_submit') ?>" class="btn btn-primary"> &nbsp;
                <input  type ="button" value="<?= lang('global_back') ?>"class="btn btn-primary" onclick="window.location = '<?= site_url('admin/transporter_users/index') ?>'">
            </div>

            <?= form_close() ?> 
        </div>
    </div>
</div> 
<!--<script type='text/javascript' src='<? //= MODULE_JS               ?>/poll/add.js'></script>-->

