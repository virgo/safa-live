<span class="span8">

</span>
<div class="row-fluid">
    

<div class="widget">
    <div class="path-container Fright">
    	<div class="path-name Fright">
            <a href="<?php echo  site_url().'admin/dashboard'; 
            
            ?>"><?php echo  lang('global_system_management') ?></a>
        </div>
        <div class="path-arrow Fright"></div>
        <div class="path-name Fright">  <a href="<?= site_url('admin/safa_itos/index')?>"><?= lang('menu_safa_itos') ?> </a></div>
        
        <div class="path-arrow Fright"></div>
        <div class="path-name Fright">  <?= lang('safa_itos_add_title')?></div>
    </div>
    
        
</div>
    
    <div class="widget">
        
        
         <div class="widget-header">
        <div class="widget-header-icon Fright"> <span class="icos-pencil2"></span> </div>
        <div class="widget-header-title Fright">
            <?= lang('safa_itos_add_title') ?>
        </div>
    	</div>
                              
        <div class="block-fluid">
            <?= form_open_multipart() ?>

 
            <div class="row-form">
                <div class="span4">
                    <div class="span4"><?= lang('safa_itos_name_ar') ?>
                       <span style="color: red">*</span>:
                    </div>
                    <div class="span8"><?= form_input('name_ar', set_value("name_ar"), " ") ?>
                        <?= form_error('name_ar', '<div class="bottom" style="color:red" >', '</div>'); ?>                    
                    </div>
                </div>
                <div class="span4">
                    <div class="span4"><?= lang('safa_itos_name_la') ?>
                        <span style="color: red">*</span>:
                    </div>
                    <div class="span8"><?= form_input('name_la', set_value("name_la"), " ") ?>
                        <?= form_error('name_la', '<div class="bottom" style="color:red" >', '</div>'); ?>                    
                    </div>
                </div>
                <div class="span4">
                    <div class="span4"><?= lang('safa_itos_erp_country_id') ?>
                        <span style="color: red">*</span>:
                    </div>
                    <div class="span8">
                        <?= form_dropdown('erp_country_id', ddgen('erp_countries', array('erp_country_id', name())), set_value('erp_country_id')," name='s_example' class='select' style='width:100%;' ") ?>
                        <?= form_error('erp_country_id', '<div class="bottom" style="color:red" >', '</div>'); ?>                    
                    </div>
                </div>
            </div>


            <div class="row-form">
                <div class="span4">
                    <div class="span4"><?= lang('safa_itos_user_username') ?>
                        <span style="color: red">*</span>:
                    </div>
                    <div class="span8"><?= form_input('user_username', set_value('user_username'), " ") ?>
                    <?= form_error('user_username', '<div class="bottom" style="color:red" >', '</div>'); ?>                    
                    </div>
                </div>
                <div class="span4">
                    <div class="span4"><?= lang('safa_itos_user_password') ?>
                       <span style="color: red">*</span>:
                    </div>
                    <div class="span8"><?= form_password('user_password', set_value('user_password'), ' ') ?>
                        <?= form_error('user_password', '<div class="bottom" style="color:red" >', '</div>'); ?>                    
                    </div>
                </div>
                <div class="span4">
                    <div class="span4"><?= lang('safa_itos_user_repeat_password') ?>
                       <span style="color: red">*</span>:
                    </div>
                    <div class="span8"><?= form_password('user_passconf', set_value('user_passconf'), ' ') ?>
                        <?= form_error('user_passconf', '<div class="bottom" style="color:red" >', '</div>'); ?>                    
                    </div>
                </div>
            </div>

            
            <div class="row-form">
                <div class="span4">
                    <div class="span4"><?= lang('safa_itos_reference_code') ?>
                       <span style="color: red">*</span>:
                    </div>
                    <div class="span8"><?= form_input('reference_code', set_value('reference_code'), " ") ?>
                        <?= form_error('reference_code', '<div class="bottom" style="color:red" >', '</div>'); ?>                    
                    </div>
                </div>
            </div>




            <div class="toolbar bottom TAC"><input type="submit" name="submit" value="<?= lang('global_submit') ?>" class="btn btn-primary"> &nbsp;
                <input  type ="button" value="<?= lang('global_back') ?>"class="btn btn-primary" onclick="window.location = '<?= site_url('admin/safa_itos/index') ?>'">
            </div>

            <?= form_close() ?> 
        </div>
    </div>
</div> 
<!--<script type='text/javascript' src='<? //= MODULE_JS        ?>/poll/add.js'></script>-->

