<span class="span8">

</span>
<div class="row-fluid">
    

<div class="widget">
    <div class="path-container Fright">
    	<div class="path-name Fright">
            <a href="<?php echo  site_url().'admin/dashboard'; 
            
            ?>"><?php echo  lang('global_system_management') ?></a>
        </div>
        <div class="path-arrow Fright"></div>
        <div class="path-name Fright">  <a href="<?= site_url('admin/safa_itos/index')?>"><?= lang('menu_safa_itos') ?> </a></div>
        
        <div class="path-arrow Fright"></div>
        <div class="path-name Fright">  <?= lang('safa_itos_edit_title')?><? if (lang('global_lang') == 'ar'): ?><?= $items->arabic_name ?><? else: ?><?= $items->english_name ?><? endif ?></div>
    </div>
    
        
</div>
    
    
    <div class="widget">
        
        <div class="widget-header">
        <div class="widget-header-icon Fright"> <span class="icos-pencil2"></span> </div>
        <div class="widget-header-title Fright">
            <?= lang('safa_itos_edit_title') ?><? if (lang('global_lang') == 'ar'): ?><?= $items->arabic_name ?><? else: ?><?= $items->english_name ?><? endif ?>
        </div>
    	</div>
                              
        <div class="block-fluid">
            <?= form_open_multipart() ?>

 
            <div class="row-form">
                <div class="span4">
                    <div class="span4"><?= lang('safa_itos_name_ar') ?>
                        <span style="color: red">*</span>:
                    </div>
                    <div class="span8"><?= form_input('name_ar', set_value("name_ar",$items->arabic_name), " ") ?></div>
                    <? if(form_error('name_ar')): ?><div class="span8 required"><?= form_error('name_ar') ?></div><?endif ?>
                </div>
                <div class="span4">
                    <div class="span4"><?= lang('safa_itos_name_la') ?>
                       <span style="color: red">*</span>:
                    </div>
                    <div class="span8"><?= form_input('name_la', set_value("name_la",$items->english_name), " ") ?></div>
                   <? if(form_error('name_la')):?> <div class="span8 required"><?= form_error('name_la') ?></div><? endif?>
                </div>
                <div class="span4">
                    <div class="span4"><?= lang('safa_itos_erp_country_id') ?>
                        <span style="color: red">*</span>:
                    </div>
                    <div class="span8">
                        <?= form_dropdown('erp_country_id', ddgen('erp_countries', array('erp_country_id', name())), set_value('erp_country_id',$items->erp_country_id), " name='s_example' class='select' style='width:100%;' ") ?>
                      <? if(form_error('erp_country_id')):?>  <div class="span8 required"><?= form_error('erp_country_id') ?></div><? endif?>
                    </div>
                </div>
            </div>

            
            <div class="row-form">
                <div class="span4">
                    <div class="span4"><?= lang('safa_itos_reference_code') ?>
                        <span style="color: red">*</span>:
                    </div>
                    <div class="span8"><?= form_input('reference_code', set_value('reference_code',$items->reference_code), " ") ?></div>
                    <div class="span8 required"><?= form_error('reference_code') ?></div>
                </div>
            </div>




            <div class="toolbar bottom TAC"><input type="submit" name="submit" value="<?= lang('global_submit') ?>" class="btn btn-primary"> &nbsp;
                <input  type ="button" value="<?= lang('global_back') ?>"class="btn btn-primary" onclick="window.location = '<?= site_url('admin/safa_itos/index') ?>'">
            </div>

            <?= form_close() ?> 
        </div>
    </div>
</div> 
<!--<script type='text/javascript' src='<? //= MODULE_JS        ?>/poll/add.js'></script>-->

