<? if(isset($model_name)): ?>

<div class="widget">
    <div class="path-container Fright">
        <div class="path-name Fright">
        <a href="<?php echo   site_url('admin/dashboard')?>"> <?php echo  lang('global_system_management') ?></a>
        </div>
        <div class="path-arrow Fright">
        </div>
        <div class="path-name Fright">
            <a href="<?= site_url('admin/'.$model_name.'/index')?>"> <?= $model_title ?></a> 
        </div>
        <div class="path-arrow Fright"></div>
        <div class="path-name Fright">
              <?= $action ?> 
        </div>
        
    </div>
	</div>


<? endif ?>
<div class="widget">
        <div class="msg">
        <p>
            <? if(isset($url)): ?>
            <p><?= $msg ?></p>
            <input  type ="button" value="<?= lang('global_submit') ?>"class="btn btn-primary" onclick="window.location = '<?= $url ?>'">
            <? if(isset($id)): ?>
            <input  type ="button" value="<?= lang('global_back') ?>" class="btn btn-primary" onclick="window.location = '<?=$url?>/edit/<?=$id?>'">
            <? endif ?>
         
            <? endif ?>
        </p>
    </div>
    </div>
        <style>
            .msg{
                background-color:#ccee97;
                font-weight: bold;
                text-align: center;
                border:1px solid #cccdc9;
                display:block; 
                padding:15px;  
                margin-bottom:10px;
                border-radius:15px;
                margin:10px;  
            }  
        </style>
