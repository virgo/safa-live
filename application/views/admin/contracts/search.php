<div class="span12">
    <div class="widget" >
        <div class="head dark" >
            <span class="icos-search"></span>
            <h2><?= lang("global_search") ?></h2>
            <a href="#" class="show_hide" style="float:left;"><img src="<?= IMAGES2 ?>/down-arrow.png" width="34" height="32"></a>
        </div>
        <?= form_open("", "method='get'") ?>
        <div class="block-fluid slidingDiv" >
            <div  class="row-form"  >
                <div class="span6" >
                    <div class="span3"><?= lang('contracts_name_ar') ?></div>
                    <div class="span8"><?= form_input('contarct_name_ar', set_value('contarct_name_ar',$this->input->get('contarct_name_ar'))) ?></div>  
                </div>
                <div class="span6" >
                    <div class="span3"><?= lang('contracts_name_la') ?></div>
                    <div class="span8"><?= form_input('contarct_name_la', set_value('contarct_name_la',$this->input->get('contarct_name_la'))) ?></div>   
                </div>
            </div>
            <div  class="row-form"  >
                <div class="span6" >
                    <div class="span3"><?= lang('contracts_country') ?></div>
                    <div class="span8" ><?= form_dropdown('contarct_country', ddgen('erp_countries', array('erp_country_id', name())),$this->input->get('contarct_country'), "style='width:100%;' ") ?></div>  
                </div>
            </div>
            <div class="row-form" >
                <div class="span6" >
                    <div class="span3"><?= lang('uo_contract') ?></div>
                    <div class="span8" ><?= form_dropdown('safa_uos',$uos,$this->input->get('safa_uos')) ?></div>  
                </div>
                <div class="span6" >
                    <div class="span3"><?= lang('contracts_exrenalagent') ?></div>
                    <div class="span8" ><?= form_dropdown('safa_uo_eas',$eas,$this->input->get('safa_uo_eas'),"style='width:100%;' ") ?></div>  
                </div>
            </div>
            <div  class="toolbar bottom TAC" style="text-align:center !important " >
                <button name="search" class="btn btn-primary" ><?= lang('global_search') ?></button>
            </div>  
        </div> 
        <?= form_close() ?> 
    </div>
</div>    
