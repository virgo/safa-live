
<div class='row-fluid' >
    <div class='span10' >
        <div style="margin-bottom:10px; text-align:left; border:3px solid #FFF; background:url(<?= IMAGES ?>/backgrounds/bg_box_head.jpg); padding:5px 10px; height:30px;">
            <div style="float:right; text-align:right; font-size:14px; line-height:30px; color:#333;">
                <a href="<?=  site_url('admin/dashboard')?>"><?= lang('global_system_management') ?></a>
                <span style="color:#80693d"><img src="<?= IMAGES ?>/jbreadcrumb/Chevron-ar.gif" width="5" height="20"></span> 
                <a href='<?=  site_url('admin/contracts')?>'><?= lang('node_title')?></a>
                <span style="color:#80693d"><img src="<?= IMAGES ?>/jbreadcrumb/Chevron-ar.gif" width="5" height="20">
                </span><span><?= lang('contracts_add') ?></span>
            </div>
        </div> 
    </div>  
</div>
<div class="row-fluid">
    <div class="span10">
        <div class="widget">
            <div class="head dark">
                <div class="icon"><i class="icos-pencil2" align="right"> </i></div>
                <h2><?= lang('contracts_add') ?></h2> 
            </div>
            <?= form_open("", "method='POST' id='wizard_validate' autocomplete='off' ") ?>
            <fieldset title="<?= lang('contract_step1') ?>">
                <legend></legend>
                <div class="block-fluid" >

                    <div class="row-form" >
                        <div class="span2"><?= lang('uo_contract') ?><font style="color:red">*</font></div>
                        <div class="span10" ><?= form_dropdown('contract_uo', ddgen('safa_uos'), set_value("contract_uo"), "class='validate[required]'") ?>
                            <span class='bottom' style='color:red'>
                                <?= form_error('contract_uo') ?>
                            </span>
                        </div>
                    </div>
                    <div class="row-form" >
                        <div class="span2"><?= lang('contracts_name_ar') ?><? if (name() == 'name_ar'): ?><font style="color:red" >*</font><? endif; ?></div>
                        <div class="span10"><input type="text" name="contarct_name_ar" value="<?= set_value('contarct_name_ar') ?>"<? if (name() == 'name_ar'): ?> class="validate[required]" <? endif; ?>     />
                            <span class='bottom' style='color:red'>
                                <?= form_error('contarct_name_ar') ?>
                            </span> 
                        </div> 
                    </div>
                    <div class="row-form" >
                        <div class="span2"><?= lang('contracts_name_la') ?><? if (name() == 'name_la'): ?><font style="color:red" >*</font><? endif; ?></div>
                        <div class="span10"><input type="text" name="contarct_name_la" value="<?= set_value('contarct_name_la') ?>"<? if (name() == 'name_la'): ?> class="validate[required]" <? endif; ?>     />
                            <span class='bottom' style='color:red'>
                                <?= form_error('contarct_name_la') ?>
                            </span>  
                        </div> 
                    </div>
                    <div class="row-form" >
                        <div class="span2"><?= lang('contracts_nationality') ?><font style="color:red" >*</font></div>
                        <div class="span10" ><?= form_dropdown('contarct_nationality', ddgen('erp_countries', array('erp_country_id', name())), set_value("contarct_nationality"), " class='validate[required]'  style='width:100%;' " ) ?>
                            <span class='bottom' style='color:red'>
                                <?= form_error('contarct_nationality') ?>
                            </span>   
                        </div>  
                    </div>
                    <div class="row-form" >
                        <div class="span2"><?= lang('contracts_country') ?><font style="color:red" >*</font></div>
                        <div class="span10" ><?= form_dropdown('contarct_country', ddgen('erp_countries', array('erp_country_id', name())), set_value('contarct_country'), "class='validate[required]'  ") ?>
                            <span class='bottom' style='color:red'>
                                <?= form_error('contarct_country') ?>
                            </span>  
                        </div>  
                    </div>
                    <div class="row-form" >
                        <div class="span2"><?= lang('contracts_phone') ?><font style="color:red" >*</font></div>
                        <div class="span10"><?= form_input('contarct_phone', set_value('contarct_phone'), 'class="validate[required,custom[phone]]"') ?>
                            <span class='bottom' style='color:red'>
                                <?= form_error('contarct_phone') ?>
                            </span>  
                        </div>
                    </div>
                    <div class="row-form" >
                        <div class="span2"><?= lang('contracts_fax') ?></div>
                        <div class="span10"><?= form_input('contarct_fax', set_value('contarct_fax')) ?></div>
                    </div>
                    <div class="row-form" >
                        <div class="span2"><?= lang('contracts_email') ?><font style="color:red" >*</font></div>
                        <div class="span10" ><?= form_input('contarct_email', set_value('contarct_email'), 'class="validate[required,custom[email]]"') ?>
                            <span class='bottom' style='color:red'>
                                <?= form_error('contarct_email') ?>
                            </span> 
                        </div> 
                    </div>

                </div>
            </fieldset>
            <fieldset title="<?= lang('contract_step2') ?>">
                <legend></legend>
                <div class="block-fluid" >
                    <div class="row-form" >
                        <div class="span2"><?= lang('contracts_ayata_num') ?></div>
                        <div class="span10"><?= form_input('contarct_ayata_num', set_value('contarct_ayata_num')) ?></div> 
                    </div>
                    <div class="row-form" >
                        <div class="span2"><?= lang('contracts_agencysymbol') ?><font style="color:red" >*</font></div>
                        <div class="span10"><?= form_input('contarct_agency_symbol', set_value('contarct_agency_symbol'), 'class="validate[required]"') ?>
                            <span class='bottom' style='color:red'>
                                <?= form_error('contarct_agency_symbol') ?>
                            </span> 
                        </div> 
                    </div>
                    <div class="row-form" >
                        <div class="span2"><?= lang('contracts_agencyname') ?><font style="color:red" >*</font></div>
                        <div class="span10" ><?= form_input('contarct_agency_name', set_value('contarct_agency_name'), ' class="validate[required]" ') ?>
                            <span class='bottom' style='color:red'>
                                <?= form_error('contarct_agency_name') ?>
                            </span> 
                        </div>  
                    </div>
                    <div class="row-form" >
                        <div class="span2"><?= lang('contracts_address') ?></div>
                        <div class="span10"><?= form_input('contarct_address', set_value('contarct_address')) ?></div> 
                    </div>
                    <div class="row-form" >
                        <div class="span2"><?= lang('contracts_ksaaddress') ?></div>
                        <div class="span10"><?= form_input('contarct_ksa_address', set_value('contarct_ksa_address')) ?></div>  
                    </div>
                    <div>

                    </div>
                </div>
            </fieldset>
            <fieldset title="<?= lang('contract_step3') ?>" >
                <legend></legend>
                <div class="block-fluid" >
                    <div class="row-form">
                        <div class="span2"><?= lang('contracts_resposible_name') ?></div>
                        <div class="span10" ><?= form_input('contarct_responsible_name', set_value('contarct_responsible_name'), "  ") ?></div>
                    </div>
                    <div class="row-form">
                        <div class="span2"><?= lang('contracts_resposible_phone') ?></div>
                        <div class="span10" ><?= form_input('contarct_responsible_phone', set_value('contarct_responsible_phone'), ' class="validate[custom[phone]]" ') ?></div>  
                    </div>
                    <div class="row-form" >
                        <div class="span2"><?= lang('contracts_username') ?><font style="color:red" >*</font></div>
                        <div class="span10"><?= form_input('contarct_username', set_value('contarct_username'), 'class="validate[required,custom[UserName]]" id="contarct_username" ') ?>
                            <span class='bottom' style='color:red'>
                                <?= form_error('contarct_username') ?>
                            </span>  
                        </div> 
                    </div>
                    <div class="row-form" >
                        <div class="span2"><?= lang('contracts_password') ?><font style="color:red" >*</font></div>
                        <div class="span10"><?= form_password('contarct_password', set_value('contarct_password'), ' class="validate[required]" id="contarct_password" ') ?>
                            <span class='bottom' style='color:red'>
                                <?= form_error('contarct_password') ?>
                            </span>
                        </div>  
                    </div>
                    <div class="row-form" >
                        <div class="span2"><?= lang('contracts_repeat_password') ?><font style="color:red" >*</font></div>
                        <div class="span10"><?= form_password('contarct_confpassword', set_value('contarct_confpassword'), 'class="validate[required,equals[contarct_password]]" ') ?>
                            <span class='bottom' style='color:red'>
                                <?= form_error('contarct_confpassword') ?>
                            </span> 
                        </div>
                    </div>
                    <div class="row-form" >
                        <div class="span2" ><?= lang('contract_notes') ?></div>
                        <div class="" ><textarea  name="contract_notes"    > <?= set_value('contract_notes') ?> </textarea></div> 
                    </div>
                </div>
                
                
                
                
                
                
                
                
                
                
                <div class="wizerd-div"><a><?= lang('contract_uasp') ?></a></div><br />
                    <div class="row-fluid"style="margin: 3%;width: 94%;border-left:1px solid #fff">
                        <div class="row-fluid">
                            <div class="span4">
                                <div class="span11">
                                    <?= lang('uasp_username') ?>
                                </div>
                                <div class="span11">
                                    <?= form_input('uasp_username', set_value('uasp_username', $item->uasp_username), "class=' input-huge'") ?>
                                </div>
                            </div>
                            <div class="span4">
                                <div class="span11">
                                    <?= lang('uasp_password') ?>
                                </div>
                                <div class="span11">
                                    <?= form_input('uasp_password', set_value('uasp_password', $item->uasp_password), "class=' input-huge'") ?>
                                </div>
                            </div>
                            <div class="span4">
                                <div class="span11">
                                    <?= lang('uasp_eacode') ?>
                                </div>
                                <div class="span11">
                                    <?= form_input('uasp_eacode', set_value('uasp_eacode', $item->uasp_eacode), "class=' input-huge'") ?>
                                </div>
                            </div>
                        </div>
                    </div>
                
                
                
                
                
                
                
                
                
            </fieldset>
            <button name="submit" class="btn btn-primary finish" ><?= lang('contract_save') ?></button>
            <?= form_close() ?> 
            <div class="toolbar bottom TAC">
                <a href="<?= site_url('admin/contracts/index') ?>" class="btn btn-primary" ><?= lang('global_back') ?></a>
            </div>

        </div>

    </div>
</div>