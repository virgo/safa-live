<div class='row-fluid' >
    <div class='span12' >
        <div style="margin-bottom:10px; text-align:left; border:3px solid #FFF; background:url(<?= IMAGES ?>/backgrounds/bg_box_head.jpg); padding:5px 10px; height:30px;">
            <a title="<?= lang('global_add_new_record') ?>" href="<?= site_url("admin/contracts/add") ?>" class="btn btn-primary"><?= lang('global_add_new_record')?></a>
            <div style="float:right; text-align:right; font-size:14px; line-height:30px; color:#333;">
                <a href="<?=  site_url('admin/dashboard')?>"><?= lang('global_system_management') ?></a>
                <span style="color:#80693d"><img src="<?= IMAGES ?>/jbreadcrumb/Chevron-ar.gif" width="5" height="20"></span> 
                <span><?= lang('node_title')?></span>
            </div>
        </div> 
    </div>  
</div>
<div class='row-fluid'>
    <?
        $data['uos']=$uos;
        $data['eas']=$eas;
    ?>
    <?= $this->load->view('admin/contracts/search',$data) ?>
</div>
<div class='row-fluid'>
    <div class="span12" >
        <div class='widget'>
            <div class="dialog" id="deleting_error" style="display: none;" title="Error">
                <p><?= lang('global_select_one_record_at_least') ?></p>                
            </div>
            <div class='head dark'>
                <div class="icon"><span class="icos-file"></span></span></div>
                <h2><?= lang('companies_contracts')?></h2>
            </div>
            <div class='block-fluid' > 
                <?= form_open("admin/contracts/delete_all", "id='form_delete'") ?>
                <table class="myTable" width="100%">
                    <thead>
                        <tr>
                            <th><?= lang('contracts_name') ?></th>
                            <th><?= lang('contracts_phone') ?></th>
                            <th><?= lang('contracts_address') ?></th>
                            <th><?= lang('global_actions') ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <? if ($items && is_array($items) && count($items)): ?>
                            <? foreach ($items as $item): ?>
                                <tr>
                                    <td>
                                       <?=$item->contract_name?>
                                    </td>
                                    <td><?= $item->phone ?></td>
                                    <td><?= $item->address ?></td>
                                    <td align="center" >
                                        <a href="<?= site_url("admin/contracts/edit") ?>/<?= $item->safa_uo_contract_id ?>" ><span class="icon-pencil"></span></a>   
                                    </td>
                                </tr>
                            <? endforeach; ?>
                        <? endif; ?>
                    </tbody>
                </table>
                <?= form_close() ?>
              <div><?= $pagination ?></div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        $(".myTable").dataTable(
                {bSort: true,
                    bAutoWidth: true,
                    "iDisplayLength": false, // can be removed for basic 10 items per page
                    "sPaginationType": false,
                    "bPaginate": false,
                    "bInfo": false}
        );
    });
</script>

