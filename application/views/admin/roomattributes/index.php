<div class="span12">
    <div class="widget">
        <a class="btn Fleft" href="<?= site_url('admin/roomattributes/add') ?>"><?= lang('global_add_new_record') ?></a>

        <div class="path-container Fright">
            <div class="path-name Fright">
                <a href=""><?= lang('global_system_management') ?></a>
            </div>
            <div class="path-arrow Fright">
            </div>
            <div class="path-name Fright">
                <?= lang('roomattributes') ?>
            </div>
        </div>

    </div>
</div>


<div class="row-fluid">
        <div class='widget'>
            
       <div class="widget-header">

            <div class="widget-header-icon Fright">
                <span class="icos-pencil2"></span>
            </div>

            <div class="widget-header-title Fright">
                <?= lang('roomattributes') ?>
            </div>

        </div>
    
            <div class='block-fluid '> 
                <table cellpadding="0" class="fpTable" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th><?= lang('roomattributes_namear') ?></th>
                            <th><?= lang('roomattributes_namela') ?></th>
                            <th><?= lang('global_actions') ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <? if (isset($items)): ?>
                            <? foreach ($items as $item): ?>
                                <tr>
                                    <td><?= $item->name_ar ?></td>
                                    <td><?= $item->name_la ?></td>
                                    <td class="TAC"  >
                                        <div >
                                        <a title="<?= lang('global_edit') ?>"   href="<?= site_url("admin/roomattributes/edit") ?>/<?= $item->safa_room_attribute_id ?>"><span class="icon-pencil"></span></a>
                                        </div>
                                    </td>
                                </tr>
                            <? endforeach; ?>
                        <? endif; ?>
                    </tbody>
                </table>

            </div>
        </div>
</div>


   
