

<div class="widget">
    <div class="path-container Fright">
    	<div class="path-name Fright">
            <a href="<?php echo  site_url().'admin/dashboard'; ?>"><?php echo  lang('global_system_management') ?></a>
        </div>
        <div class="path-arrow Fright"></div>
        <div class="path-name Fright">  <?= lang('add_hotels_polices') ?></div>
    </div>

   
</div>
<div class="row-fluid">
    <div class="widget">
        <div class="head dark">
            <div class="icon"><i class="icos-pencil2"></i></div>
            <h2><?= lang('add_hotels_polices') ?></h2> 
        </div>                        
        <div class="block-fluid">
           <?= form_open_multipart('', 'autocomplete="off" id="polices_add" ') ?> 
            <div class="row-form">
                
                <div class="span6">
                    <div class="span4"><?= lang('hotels_polices_name_ar') ?><?if(name()=='name_ar'):?><font style="color:red" >*</font><?endif;?></div>
                    <div class="span6">
                        <?= form_input('name_ar', set_value("name_ar"), "class='validate[required]'") ?>
                        <?= form_error('name_ar', '<span class="bottom" style="color:red" >', '</span>'); ?>
                    </div>
                </div>
                
                <div class="span6">
                    <div class="span4"><?= lang('hotels_polices_name_la') ?><?if(name()=='name_la'):?><font style="color:red" >*</font><?endif;?></div>
                    <div class="span6" >
                        <?= form_input('name_la', set_value("name_la"), "class='validate[required]'") ?>
                        <?= form_error('name_la', '<span class="bottom" style="color:red" >', '</span>'); ?>  
                    </div>     
                </div>
               
            </div>

            <div class="toolbar bottom TAC">
                <input type="submit" name="submit" value="<?= lang('global_submit') ?>" class="btn btn-primary"> &nbsp;
                <a href="<?= site_url('admin/hotels_polices/index') ?>" class="btn btn-primary"  ><?= lang('global_back') ?></a>
           </div>

            <?= form_close() ?> 
        </div>
    </div>
</div> 
        <script type="text/javascript">

            $("#polices_add").validationEngine({
                prettySelect: true,
                useSuffix: "_chosen",
                promptPosition: "topRight:-150"
//promptPosition : "bottomLeft"
            });
        </script>

