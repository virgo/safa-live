<div class="row-fluid">
    <div class="span12">
        <div class="widget">
            <div class="head dark">
                <div class="icon"><i class="icos-pencil2" align="right"> </i></div>
                <h2><?= lang('admins_edit_title') ?></h2> 
            </div>
            <div class="block-fluid">
             <?= form_open() ?>
                        
                <? if (validation_errors()): ?>
                    <td class="red-left" style="line-height:3px">
                        <div style="font:normal 10px tahoma">
                            <?php echo validation_errors(); ?>
                        </div>
                    </td>   
                    <td class="red-right"><a class="close-red"><img src="<?= IMAGES ?>/table/icon_close_red.gif"   alt="" /></a></td>
                <? endif ?>

                <div class="row-form">
                    <div class="span1"><?= lang('admins_username') ?></div>
                    <?= form_input('username', set_value("username",$admins->username), " class='input-small'  ") ?>
                </div>
                <div class="row-form">
                    <div class="span1"><?= lang('admins_email') ?></div>
                     <?= form_input('email', set_value('email',$admins->email), " class='input-small'  ") ?>
                </div>
                
                <div class="row-form">
                    <div class="span1"><?= lang('admins_password') ?></div>
                    <?= form_password('password', set_value('password'), 'class="input-small"') ?>  
                </div>
                <div class="row-form">
                    <div class="span1"><?= lang('admins_repeat_password') ?></div>
                    <?= form_password('passconf', set_value('passconf'), 'class="input-small"') ?>  
                </div>
                <div class="row-form" style="text-align:center">
                    <input type="button" name="back" value="<?= lang('global_back') ?>" class="btn btn-primary" onclick="window.location = '<?= site_url('admin/admins/index') ?>'">
                    <input type="submit" name="submit" value="<?= lang('global_submit') ?>" class="btn btn-primary">
                </div>
                <?= form_close() ?>  
            </div>
        </div>
    </div>
</div>
<script type='text/javascript' src='<?= MODULE_JS ?>/admin/admins/edit.js'></script>
