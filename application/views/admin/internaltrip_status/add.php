<div class="row-fluid" >
    <div class="span12" >
        <div style="margin-bottom:10px; text-align:left; border:3px solid #FFF; background:url(<?= IMAGES ?>/backgrounds/bg_box_head.jpg); padding:5px 10px; height:30px;">
            <div style="float:right; text-align:right; font-size:14px; line-height:30px; color:#333;"> 
                <a href="#"><?= lang('parent_node_title') ?></a> 
                <span style="color:#80693d"><img src="<?= IMAGES ?>/jbreadcrumb/Chevron-ar.gif" width="5" height="20"></span>
                <a href="<?= site_url("admin/internaltrip_status/index") ?>" ><?= lang('internaltripstatus_title') ?></a>
                <span style="color:#80693d"><img src="<?= IMAGES ?>/jbreadcrumb/Chevron-ar.gif" width="5" height="20"></span>
                <?= lang('internaltripstatus_add') ?>
            </div>
        </div>  
    </div>  
</div>
<div class="row-fluid" >
    <div class="span12" >
        <div class="widget">
            <div class="head dark">
                <div class="icon"><i class="icos-pencil2"></i></div>
                <h2><?= lang('internaltripstatus_add') ?> </h2> 
            </div>                        
            <div class="block-fluid">
                <?=form_open()?>
                <div class="row-form">
                    <div class="span6">
                        <div class="span4"><?= lang('internaltripstatus_name_ar') ?><font style="color:red" >*</font></div>
                        <div class="span8"><?=  form_input("internaltripstatus_name_ar",  set_value("internaltripstatus_name_ar"))?></div>
                    </div>
                    <div class="span6">
                        <div class="span4"><?= lang('internaltripstatus_name_la') ?><font style="color:red" >*</font></div>
                        <div class="span8"><?=  form_input("internaltripstatus_name_la",  set_value("internaltripstatus_name_la"))?></div>
                    </div>
                </div>
                <div class="row-form">
                    <div class="span6">
                        <div class="span4"><?= lang('internaltripstatus_code') ?><font style="color:red" >*</font> </div>
                        <div class="span8"><?=  form_input("internaltripstatus_code",  set_value("internaltripstatus_code"))?></div>
                    </div>
                    <div class="span6">
                        <div class="span4"><?= lang('internaltripstatus_color') ?><font style="color:red" >*</font></div>
                        <div class="span8"><input name="internaltripstatus_color" value="<?=set_value("internaltripstatus_color")?>" type="text" id="color1" size="7" maxlength="7" class=" input-xlarge" /> 
                            <span onclick="openPicker('color1')" class="picker_buttons btn"><?=lang('color_pick')?></span></div>
                    </div>
                </div>
                <div class="row-form">
                    <div class="span6">
                        <div class="span4"><?= lang('internaltripstatus_description_ar') ?> </div>
                        <div class="span8">
                            <textarea  name="internaltripstatus_description_ar" >
                                    <?=  set_value('internaltripstatus_description_ar')?>
                            </textarea> 
                        </div>
                    </div>
                    <div class="span6">
                        <div class="span4"><?= lang('internaltripstatus_description_la') ?></div>
                        <div class="span8">
                            <textarea name="internaltripstatus_description_la" >
                                <?=  set_value('internaltripstatus_description_la')?>
                             </textarea>
                        </div>
                    </div>
                </div>
                <div class="toolbar bottom TAC">
                    <input type="submit" name="submit" value="<?= lang('global_submit') ?>" class="btn btn-primary"> &nbsp;
                    <a class="btn btn-primary" href="<?=  site_url('internaltrip_status/index')?>" ><?=lang('global_back')?></a>
                </div>
                <div>  
                </div>
                <?=form_close()?>
            </div> 
        </div>
       