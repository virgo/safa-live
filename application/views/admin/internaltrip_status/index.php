<div class="row-fluid" >
    <div style="margin-bottom:10px; text-align:left; border:3px solid #FFF; background:url(<?= IMAGES ?>/backgrounds/bg_box_head.jpg); padding:5px 10px; height:30px;">
        <a href="<?= site_url("admin/internaltrip_status/add") ?>" class="btn btn-primary"><?= lang('global_add_new_record') ?></a>
        <div style="float:right; text-align:right; font-size:14px; line-height:30px; color:#333;"> 
            <a href="#"><?= lang('parent_node_title') ?></a> 
            <span style="color:#80693d"><img src="<?= IMAGES ?>/jbreadcrumb/Chevron-ar.gif" width="5" height="20"></span>
            <?= lang('internaltripstatus_title') ?>
        </div>
    </div>  
</div>
<div class="row-fluid">
    <div class="widget">
        <div class="head dark">
            <div class="icon"><i class="icos-pencil2"></i></div>
            <h2><?= lang('internaltripstatus_title') ?></h2>
        </div>                
        <div class="block-fluid">
            <table cellpadding="0" class="ffTable" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th><?= lang('internaltripstatus_name_ar') ?></th>
                        <th><?= lang('internaltripstatus_name_la') ?></th>
                        <th><?= lang('internaltripstatus_description_ar') ?></th>
                        <th><?= lang('internaltripstatus_description_la') ?></th>
                        <th><?= lang('internaltripstatus_code') ?></th>
                        <th><?= lang('internaltripstatus_color') ?></th>
                        <th><?= lang('global_actions') ?></th>
                    </tr>
                </thead>
                <tbody>
                    <? if (isset($items)): ?>
                        <? foreach ($items as $item): ?>
                            <tr>
                                <td><?= $item->name_ar ?></td>
                                <td><?= $item->name_la ?></td>
                                <td><?= $item->description_ar ?></td>
                                <td><?= $item->description_la ?></td>
                                <td><?= $item->code ?></td>
                                <td><span class="label label" style="background: <?= $item->color ?>"><?= $item->color ?></span></td>
                                <td class="TAC">
                                    <a href="#"><span class="icon-pencil"></span></a> 
                                    <a href="#"><span class="icon-trash"></span></a> 
                                </td>
                            </tr>
                        <? endforeach; ?>
                    <? endif; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>  

