<div class="row-fluid" >
    
    <div class="widget">
    <div class="path-container Fright">
        <div class="path-name Fright">
        <a href="<?php echo   site_url('admin/dashboard')?>"> <?php echo  lang('global_system_management') ?></a>
        </div>
        <div class="path-arrow Fright">
        </div>
        <div class="path-name Fright">
            <a href="<?=  site_url('admin/safa_uos/index')?>"><?= lang('parent_child_title') ?></a>
        </div>
        
        <div class="path-arrow Fright">
        </div>
        <div class="path-name Fright">
            <?php echo  lang('safa_uos_add_title') ?>
        </div>
        
        
    </div>
 
</div>
    
    
</div>
<div class="row-fluid">
    <div class="widget">
        
         <div class="widget-header">
	        <div class="widget-header-icon Fright"> <span class="icos-pencil2"></span> </div>
	        <div class="widget-header-title Fright">
	            <?php echo  lang('safa_uos_add_title') ?>
	        </div>
	        
	    	</div>
            
                              
        <div class="block-fluid">
           <?= form_open_multipart('', 'autocomplete="off" id="safa_uos_add" ') ?> 
            <div class="row-form">
                <div class="span6">
                    <div class="span4"><?= lang('safa_uos_name_ar') ?><?if(name()=='name_ar'):?><font style="color:red" >*</font><?endif;?></div>
                    <div class="span8">
                        <?= form_input('name_ar', set_value("name_ar"),"class=' validate[required]'") ?>
                        <?= form_error('name_ar', '<span class="bottom" style="color:red" >', '</span>'); ?>
                    </div>
                </div>
                <div class="span6">
                    <div class="span4"><?= lang('safa_uos_name_la') ?><?if(name()=='name_la'):?><font style="color:red" >*</font><?endif;?></div>
                    <div class="span8" >
                        <?= form_input('name_la', set_value("name_la")) ?>
                        <?= form_error('name_la', '<span class="bottom" style="color:red" >', '</span>'); ?>  
                    </div>     
                </div>
            </div>
            <div class="row-form">
                <div class="span6">
                    <div class="span4"><?= lang('safa_uos_phone') ?>:</div>
                    <div class="span8" >
                        <?= form_input('phone', set_value("phone")) ?>
                    </div>
                </div>
                <div class="span6">
                    <div class="span4"><?= lang('safa_uos_mobile') ?>:</div>
                    <div class="span8" >
                        <?= form_input('mobile', set_value("mobile")) ?>
                    </div>
                </div>
            </div>
            <div class="row-form">
                <div class="span6">
                    <div class="span4"><?= lang('safa_uos_email') ?>:</div>
                    <div class="span8" >
                        <?= form_input('email', set_value("email")) ?>
                        <?= form_error('email', '<span class="bottom" style="color:red" >', '</span>'); ?>
                    </div>
                </div>
                <div class="span6">
                    <div class="span4"><?= lang('safa_uos_fax') ?>:</div>
                    <div class="span8" >
                        <?= form_input('fax', set_value("fax")) ?>
                    </div>
                </div>
            </div>
            <div class="row-form">
                <div class="span6">
                    <div class="span4"><?= lang('safa_uos_code') ?><font style="color:red" >*</font></div>
                    <div class="span8" >
                        <?= form_input('code', set_value('code')) ?>
                        <?= form_error('code', '<span class="bottom validate[required]" style="color:red" >', '</span>'); ?>
                    </div>
                </div>
            </div>

            <div class="row-form">
                <div class="span6">
                    <div class="span4"><?= lang('safa_uos_user_username') ?><font style="color:red" >*</font></div>
                    <div class="span8">
                        <?= form_input('user_username', set_value('user_username'),"autocomplete='off' class='validate[required]'") ?>
                        <?= form_error('user_username', '<span class="bottom" style="color:red" >', '</span>'); ?>
                    </div>
                </div>
            </div>

            <div class="row-form">
                <div class="span6">
                    <div class="span4"><?= lang('safa_uos_user_password') ?><font style="color:red" >*</font></div>
                    <div class="span8" >
                        <?= form_password('user_password', set_value('user_password')," autocomplete='off' class='validate[required]'") ?>
                        <?= form_error('user_password', '<div class="bottom" style="color:red" >', '</div>'); ?>
                    </div>
                </div>
                <div class="span6">
                    <div class="span4"><?= lang('safa_uos_user_repeat_password') ?><font style="color:red" >*</font></div>
                    <div class="span8" >
                       <?= form_password('user_passconf', set_value('user_passconf')," autocomplete='off'class='validate[required]' ") ?> 
                       <?= form_error('user_passconf', '<div class="bottom" style="color:red" >', '</div>'); ?> 
                    </div>
                </div>
            </div>
            <div class="row-form">
                <div class="span6">
                    <div class="span4"><?= lang('erp_uasp_id') ?><font style="color:red" >*</font></div>
                    <div class="span8" >
                        <?= form_input('erp_uasp_id', set_value('erp_uasp_id'),"class=' validate[required]'") ?>
                        <?= form_error('erp_uasp_id', '<span class="bottom" style="color:red" >', '</span>'); ?>
                    </div>
                </div>
            </div>
            <div class="toolbar bottom TAC">
                <input type="submit" name="submit" value="<?= lang('global_submit') ?>" class="btn btn-primary"> &nbsp;
                <a href="<?= site_url('admin/safa_uos/index') ?>" class="btn btn-primary"  ><?= lang('global_back') ?></a>
           </div>

            <?= form_close() ?> 
        </div>
    </div>
</div> 
        <script type="text/javascript">

            $("#safa_uos_add").validationEngine({
                prettySelect: true,
                useSuffix: "_chosen",
                promptPosition: "topRight:-150"
//promptPosition : "bottomLeft"
            });
        </script>
<!--<script type='text/javascript' src='<? //= MODULE_JS     ?>/poll/add.js'></script>-->

