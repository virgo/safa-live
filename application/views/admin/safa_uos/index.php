<div class="row-fluid" >

<div class="widget">
    <div class="path-container Fright">
        <div class="path-name Fright">
        <a href="<?php echo   site_url('admin/dashboard')?>"> <?php echo  lang('global_system_management') ?></a>
        </div>
        <div class="path-arrow Fright">
        </div>
        <div class="path-name Fright">
            <?php echo  lang('parent_child_title') ?>
        </div>
        
        
        
    </div>
    <a href="<?= site_url("admin/safa_uos/add") ?>" class="btn Fleft"><?= lang('global_add_new_record')?></a>
 
</div>
      
</div>
<div class="row-fluid">
    <div class="span12" >
        <div class="widget">
             
            
            <div class="widget-header">
	        <div class="widget-header-icon Fright"> <span class="icos-pencil2"></span> </div>
	        <div class="widget-header-title Fright">
	            <?php echo  lang('global_search') ?>
	        </div>
	        
	    	</div>
            
                                  
            <div class="block-fluid slidingDiv">
                <?= form_open('admin/safa_uos/index', "method='get'") ?>
                <div class="row-form">
                    <div class="span6">
                        <div class="span4"><?= lang('uos') ?></div>
                        <div class="span8"><?=  form_dropdown('uo_deleted',array('0'=>lang('uo_active'),'1'=>lang('uo_removed'),''=>lang('uo_all')),$this->input->get('uo_deleted'))?></div>
                    </div>
                </div>
                <div class="toolbar bottom TAC"><button name="search" class="btn btn-primary"><?= lang('global_search') ?></button></div>
                <?= form_open() ?>
            </div>
        </div>
    </div>
</div>
<div class='row-fluid'>
    <div class='span12'>
        <div class='widget'>
            
            <div class="widget-header">
	        <div class="widget-header-icon Fright"> <span class="icos-pencil2"></span> </div>
	        <div class="widget-header-title Fright">
	            <?php echo  lang('menu_ksa_agent') ?>
	        </div>
	        
	    	</div>
            
            <div class='block-fluid '> 
                <table cellpadding="0" class="myTable" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th><?= lang('safa_uos_code') ?></th>
                            <th><?= lang('global_arabic_name') ?></th>
                            <th><?= lang('global_english_name') ?></th>
                            <th><?= lang('safa_uos_contract_num') ?></th>
                            <th><?= lang('safa_uos_trips_num') ?></th>
                            <th><?= lang('global_actions')?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <? if (isset($items)): ?>
                            <? foreach ($items as $item): ?>
                                <tr rel="<?= $item->safa_uo_id ?>">
                                    <td><?= $item->code ?></td>
                                    <td><?= $item->name_ar ?></td>
                                    <td><?= $item->name_la ?></td>
                                    <td><?= $item->num_contracts ?></td>
                                    <td><?= $item->num_trips ?></td>
                                    <td class="TAC">
                                        <?$name=name()?>
                                        <? if ($item->deleted == 1): ?>
                                                <a  title="<?= lang('global_activate') ?>" id="<?= $item->safa_uo_id ?>" href="javascript:void(0)" role="button" data-toggle="modal" class="delete_enable" ><span class="icon-ok"></span></a>
                                        <? else:?>
                                            <a title="<?= lang('global_edit') ?>"   href="<?= site_url("admin/safa_uos/edit") ?>/<?= $item->safa_uo_id ?>"><span class="icon-pencil"></span></a> 
                                            <a  title="<?= lang('global_delete') ?>" id="<?= $item->safa_uo_id ?>" href="javascript:void(0)" role="button" data-toggle="modal  " class="delete_dis" ><span class="icon-trash"></span></a>
                                            <a title="<?= lang('show_uo_user') ?>"   href="<?= site_url("admin/safa_uos/users_index") ?>/<?= $item->safa_uo_id ?>"><span class="icon-user"></span></a>
                                            
                                            <a title="<?= lang('safa_uos_departments') ?>" href="<?= site_url("admin/safa_uos_departments/index") ?>/<?= $item->safa_uo_id ?>">@</a>
                          
                                        <? endif; ?>
                                         <?if($item->disabled==0):?>
                                            <a title="<?= lang('global_cancel') ?>"  onclick="return confirm('<?=lang('global_are_you_sure_you_want_to_block').' '.$item->$name?>')"  href="<?= site_url("admin/safa_uos/cancel_uo")?>/<?= $item->safa_uo_id ?>"><span class="icosg-blocked"></span></a>
                                         <?else:?>
                                            <a title="<?= lang('global_activate') ?>"  onclick="return confirm('<?=lang('global_are_you_sure_you_want_to_activate').' '.$item->$name?>')"  href="<?= site_url("admin/safa_uos/active_uo")?>/<?= $item->safa_uo_id ?>"><span class="icosg-checkmark"></span></a>
                                        <?endif;?>    
                                    </td>
                                    

                                </tr>
                            <? endforeach; ?>
                        <? endif; ?>
                    </tbody>
                </table>

            </div>
        </div>
        <div style="width:100%;float:right">
            <?= $pagination ?>
        </div>
    </div>

    <script>
        $(document).ready(function() {
            $(".myTable").dataTable(
                    {bSort: true,
                        bAutoWidth: true,
                        "iDisplayLength": false, // can be removed for basic 10 items per page
                        "sPaginationType": false,
                        "bPaginate": false,
                        "bInfo": false}
            );
        });
    </script>
    <script>
        $(document).ready(function() {
            $('.delete_dis').click(function() {
                var conf = confirm("<?=lang('global_are_you_sure_you_want_to_delete')?>");
                if (conf == true) {
                    var tr_id = $(this).attr('id');
                    $.get('<?= site_url() . "admin/safa_uos/delete_disable/" ?>' + $(this).attr('id'), function(msg) {
                        if (msg == "1") {
                            $('.myTable').find("[rel='" + tr_id + "']").remove();
                        }

                    });
                }
            });
            $('.delete_enable').click(function() {
                var conf = confirm("<?=lang('global_are_you_sure_you_want_to_activate')?>");
                if (conf == true) {
                    var tr_id = $(this).attr('id');
                    $.get('<?= site_url() . "admin/safa_uos/delete_enable/" ?>' + $(this).attr('id'), function(msg) {
                        if (msg == "1") {
                            $('.myTable').find("[rel='" + tr_id + "']").remove();
                           if($('[name="uo_deleted"]').val()=='2'){
                                window.location.reload();
                           }
                        }

                    });
                }
            });
        });
    </script>
