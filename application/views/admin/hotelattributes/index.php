<div class="span12">
    <div class="widget">
        <a class="btn Fleft" href="<?= site_url('admin/hotelattributes/add') ?>"><?= lang('global_add_new_record') ?></a>
        <div class="path-container Fright">
            <div class="path-name Fright">
                <a href=""><?= lang('global_system_management') ?></a>
            </div>
            <div class="path-arrow Fright">
            </div>
            <div class="path-name Fright">
                <?= lang('hotelattributes') ?>
            </div>
        </div>

    </div>
</div>

<div class="widget">
    <div class="widget-header">
        <div class="widget-header-icon Fright">
            <span class="icos-pencil2"></span>
        </div>
        <div class="widget-header-title Fright">
            <?= lang('hotelattributes') ?>
        </div>
    </div> 


    <div class="widget-container">
        <div class='block-fluid '> 
            <table cellpadding="0" class="fpTable" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th><?= lang('hotelattributes_namear') ?></th>
                        <th><?= lang('hotelattributes_namela') ?></th>
                        <th><?= lang('global_actions') ?></th>
                    </tr>
                </thead>
                <tbody>
                    <? if (isset($items)): ?>
                        <? foreach ($items as $item): ?>
                            <tr>
                                <td><?= $item->name_ar ?></td>
                                <td><?= $item->name_la ?></td>
                                <td class="TAC"  >
                                    <div >
                                        <a title="<?= lang('global_edit') ?>"   href="<?= site_url("admin/hotelattributes/edit") ?>/<?= $item->safa_hotel_attribute_id ?>"><span class="icon-pencil"></span></a>
                                        <? if ($this->hotelattributes_model->check_delete_ability($item->safa_hotel_attribute_id)): ?>
                                            <a title="<?= lang('global_delete') ?>" onclick="return confirm('<?= lang('global_are_you_sure_you_want_to_delete') ?>')"  href="<?= site_url("admin/hotelattributes/delete") ?>/<?= $item->erp_meal_id ?>"><span class="icon-trash"></span></a>
                                            <? endif; ?>
                                    </div>
                                </td>
                            </tr>
                        <? endforeach; ?>
                    <? endif; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>




