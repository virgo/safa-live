<div class="span12">
    <div class="widget">
        <div class="path-container Fright">
            <div class="path-name Fright">
                <a href=""><?= lang('global_system_management') ?></a>
            </div>
            <div class="path-arrow Fright">
            </div>
            <div class="path-name Fright">
                <a href="<?= site_url('admin/hotelattributes/index') ?>"><?= lang('hotelattributes') ?></a>
            </div>
            <div class='path-arrow Fright'>
            </div>
            <div class="path-name Fright">
                <?= lang('add_hotelattribute') ?> 
            </div>
        </div>
    </div>
</div>

<div class="widget">
    <div class="widget-header">
        <div class="widget-header-icon Fright">
            <span class="icos-pencil2"></span>
        </div>

        <div class="widget-header-title Fright">
            <?= lang('add_hotelattribute') ?> 
        </div>
    </div>  

    <div class="widget-container">  
        <div class="block-fluid">
            <?= form_open_multipart('', 'autocomplete="off" id="hotelatt_add"') ?> 
            <div class="row-form">
                <div class="span6">
                    <div class="span4 TAL Pleft10"><?= lang('hotelattributes_namear') ?><? if (name() == 'name_ar'): ?><font style="color:red" >*</font><? endif; ?></div>
                    <div class="span8">
                        <?= form_input('name_ar', set_value("name_ar"), "class='input-huge validate[required]'") ?>
                        <?= form_error('name_ar', '<span class="bottom" style="color:red" >', '</span>'); ?>
                    </div>
                </div>
                <div class="span6">
                    <div class="span4 TAL Pleft10"><?= lang('hotelattributes_namela') ?><? if (name() == 'name_la'): ?><font style="color:red" >*</font><? endif; ?></div>
                    <div class="span8" >
                        <?= form_input('name_la', set_value("name_la"), "class='input-huge'") ?>
                        <?= form_error('name_la', '<span class="bottom" style="color:red" >', '</span>'); ?>  
                    </div>     
                </div>
            </div>
            <div class="toolbar bottom TAC">
            <input type="submit" name="submit" value="<?= lang('global_submit') ?>" class="btn btn-primary"> &nbsp;
                <a href="<?= site_url('admin/hotelattributes/index') ?>" class="btn btn-primary"  ><?= lang('global_back') ?></a>
            </div>

            <?= form_close() ?> 
        </div>
    </div>
</div>

<script>
        $("#hotelatt_add").validationEngine({
prettySelect : true,
useSuffix: "_chosen",
promptPosition : "topRight:-150"
//promptPosition : "bottomLeft"
});
</script>


