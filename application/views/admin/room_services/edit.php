<div class="row-fluid" >
    <div style="margin-bottom:10px; text-align:left; border:3px solid #FFF; background:url(<?= IMAGES ?>/backgrounds/bg_box_head.jpg); padding:5px 10px; height:30px;">
        <div style="float:right; text-align:right; font-size:14px; line-height:30px; color:#333;"> 
            <a href="<?= site_url('admin/dashboard') ?>"><?= lang('global_system_management') ?></a> 
            <span style="color:#80693d"><img src="<?= IMAGES ?>/jbreadcrumb/Chevron-ar.gif" width="5" height="20"></span>
            <a href="<?= site_url('admin/room_services/index') ?>"><?= lang('room_services') ?></a>
            <span style="color:#80693d"><img src="<?= IMAGES ?>/jbreadcrumb/Chevron-ar.gif" width="5" height="20"></span>
            <?= lang('edit_room_services') ?>
        </div>
    </div> 
</div>
<div class="row-fluid">
    <div class="widget">
        <div class="head dark">
            <div class="icon"><i class="icos-pencil2"></i></div>
            <h2> 
                <?= lang('edit_room_services') ?> <?= $items->{name()} ?>
            </h2> 
        </div>                          
        <div class="block-fluid">
            <?= form_open_multipart('', 'autocomplete="off" ') ?> 

            <div class="row-form">
                <div class="span6">
                    <div class="span4"><?= lang('room_services_name_ar') ?><? if (name() == 'name_ar'): ?><font style="color:red" >*</font><? endif; ?></div>
                    <div class="span6">
                        <?= form_input('name_ar', set_value("name_ar", $items->name_ar)) ?>
                        <?= form_error('name_ar', '<span class="bottom" style="color:red" >', '</span>'); ?>
                    </div>
                </div>
                <div class="span6">
                    <div class="span4"><?= lang('room_services_name_la') ?><? if (name() == 'name_la'): ?><font style="color:red" >*</font><? endif; ?></div>
                    <div class="span6" >
                        <?= form_input('name_la', set_value("name_la", $items->name_la)) ?>
                        <?= form_error('name_la', '<span class="bottom" style="color:red" >', '</span>'); ?>  
                    </div>     
                </div>

            </div>



            <div class="toolbar bottom TAC">
                <button name="submit" class="btn btn-primary"><?= lang('global_submit') ?></button>
                <a href="<?= site_url('admin/room_services/index') ?>" class="btn btn-primary"  ><?= lang('global_back') ?></a>
            </div>

            <?= form_close() ?> 
        </div>
    </div>
</div> 


