<div class="row-fluid" >
    <div style="margin-bottom:10px; text-align:left; border:3px solid #FFF; background:url(<?= IMAGES ?>/backgrounds/bg_box_head.jpg); padding:5px 10px; height:30px;"><a href="<?= site_url("admin/room_services/add") ?>" class="btn btn-primary"><?= lang('global_add_new_record') ?></a>
        <div style="float:right; text-align:right; font-size:14px; line-height:30px; color:#333;"> 
            <a href="<?= site_url('admin/dashboard')?>"><?= lang('global_system_management') ?></a>
            <span style="color:#80693d"><img src="<?= IMAGES ?>/jbreadcrumb/Chevron-ar.gif" width="5" height="20"></span>
            <?= lang('room_services') ?>
        </div>
    </div>  
</div>
<div class='row-fluid'>
    <div class='span12'>
        <div class='widget'>
            <div class='head dark'>
                <div class="icon"><span class="icos-cube1"></span></span></div>
                <h2><?= lang('room_services') ?></h2>
            </div>
            <div class='block-fluid '> 
                <table cellpadding="0" class="myTable" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th><?= lang('room_services_name_ar') ?></th>
                            <th><?= lang('room_services_name_la') ?></th>
                            <th><?= lang('global_actions') ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <? if (isset($items)): ?>
                            <? foreach ($items as $item): ?>
                               <tr id="row_<?php echo $item->erp_room_service_id;?>">
                                    <td><?= $item->name_ar ?></td>
                                    <td><?= $item->name_la ?></td>
                                    <td class="TAC">
                                    <a href="<?= site_url("admin/room_services/edit") ?>/<?= $item->erp_room_service_id ?>"><span class="icon-pencil"></span></a>
                                    <a href="#fModal" name="<?= $item->name_ar ?>"  id="<?= $item->erp_room_service_id ?>" class=" delete_item" data-toggle="modal">
                                    <span class="icon-trash"></span></a>
                                    </td>
                                </tr>
                            <? endforeach; ?>
                        <? endif; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
   

<?= $pagination ?>
<div id="fModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="myModalLabel"><?= lang('global_system_message') ?></h3>
    </div>        
    <div class="row-fluid">
        <div id="msg" class="row-form">
            <?=  lang('global_are_you_sure_you_want_to_delete')?>
        </div>
    </div>                   
    <div class="modal-footer">
        <button class="btn btn-primary" data-dismiss="modal" aria-hidden="true"id="yes"><?= lang('global_yes')?></button>
        <button class="btn btn-primary" data-dismiss="modal" aria-hidden="true"id="no"><?= lang('global_no')?></button>       </div>
</div>
    <script type='text/javascript' src='<?= JS ?>/custom/table_operation.js'></script>
    <script>
        check_list_delete_submit("delete_all", "table", "form_delete", '<?= lang('global_are_you_sure_you_want_to_delete') ?>', "deleting_error");
    </script>

   
  <!-- hide and show -->
  <script type="text/javascript">
    $(document).ready(function(){
        $('.delete_item').click(function(){
                        var name= $(this).attr('name');
                        var erp_room_service_id = $(this).attr('id');
                        <? if (lang('global_lang') == 'ar'): ?>
                            $('#msg').text("<?= lang('global_are_you_sure_you_want_to_delete') ?>"+" "+name)
                        <? else: ?>
                            $('#msg').text(name+" "+"<?= lang('global_are_you_sure_you_want_to_delete') ?>")
                        <?  endif;?>    
			 $('#yes').click(function(){
                         var answer =$(this).attr('id');
                           if(answer==='yes'){
				$.ajax({
                                        type: "POST",
					dataType: 'json',
					url: "<?php echo site_url('admin/room_services/delete');?>",
					data: {'erp_room_service_id':erp_room_service_id},
					success: function(msg){
                                         if(msg.response==true){
                                            var del = erp_room_service_id;
                                            $("#row_"+del).remove();
                                             <? if (lang('global_lang') == 'ar'): ?>
                                                $('.updated_msg').text("<?=lang('global_delete_confirm')?>"+" "+name);
                                                $('.updated_msg').append('<br>');
                                                $('.updated_msg').append('<input  type ="button" value="<?=lang('global_submit')?>"class="btn btn-primary" id="ok" >');
                                             <? else: ?>
                                                $('.updated_msg').text(name+" "+"<?=lang('global_delete_confirm')?>");
                                                $('.updated_msg').append('<br>');
                                                $('.updated_msg').append('<input  type ="button" value="<?=lang('global_submit')?>"class="btn btn-primary" id="ok" >');
                                             <?  endif;?>
                                              $('.updated_msg').show();
                                              $('#ok').click(function(){
                                                 $('.updated_msg').hide();
                                               });
				           }else if(msg.response==false){
                                                    $('.updated_msg').text(msg.msg);
                                                    $('.updated_msg').append('<br>');
                                                    $('.updated_msg').append('<input  type ="button" value="<?=lang('global_submit')?>"class="btn btn-primary" id="ok" >');
                                                    $('.updated_msg').show();
                                                    $('#ok').click(function(){
                                                       $('.updated_msg').hide();
                                                       
                                                    });
				          }
					}
				});
			}else{
				return FALSE;
			}
                      });
		});
	});
	</script>