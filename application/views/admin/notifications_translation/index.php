<link type="text/css" rel="stylesheet" href="<?php echo base_url().'static/js/admin/notifications/contextmenu';?>/webtoolkit.contextmenu.css" />
<script type="text/javascript" src="<?php echo base_url().'static/js/admin/notifications/contextmenu';?>/webtoolkit.contextmenu.js"></script>

<script type="text/javascript">
	SimpleContextMenu.setup({'preventDefault':false, 'preventForms':false});
	SimpleContextMenu.attach('menu_shortcut_class_subject_lang2', 'menu_subject');
	SimpleContextMenu.attach('menu_shortcut_class_body_lang2', 'menu_body');
	
	
	function insertAtCursor(myField, myValue) {
	    //IE support
	    if (document.selection) {
	        myField.focus();
	        sel = document.selection.createRange();
	        sel.text = myValue;
	    }
	    //MOZILLA and others
	    else if (myField.selectionStart || myField.selectionStart == '0') {
	        var startPos = myField.selectionStart;
	        var endPos = myField.selectionEnd;
	        myField.value = myField.value.substring(0, startPos)
	            + myValue
	            + myField.value.substring(endPos, myField.value.length);
	    } else {
	        myField.value += myValue;
	    }
	}

	$(document).ready(function()
	{
		$("#language_1").change(function()
		{
			
			var erp_language_id=$(this).val();
			var dataString = 'erp_language_id='+ erp_language_id;

			$.ajax
			({
				type: "POST",
				url: "<?php echo base_url().'admin/notifications_translation/getTagElementsForAjax'?>",
				data: dataString,
				cache: false,
				success: function(html)
				{
					$("#subject_lang1").html(html);					
				}
			});

			$.ajax
			({
				type: "POST",
				url: "<?php echo base_url().'admin/notifications_translation/fillLanguage2DropdownByAjax'?>",
				data: dataString,
				cache: false,
				success: function(html)
				{
					$("#language_2").html(html);					
				}
			});
			
	
		});



		$("#subject_lang1").change(function()
		{
			var language_1=$("#language_1").val();
			var language_2=$("#language_2").val();
			var event_key=$(this).val();
			
			var dataString = 'language_1='+ language_1+'&language_2='+ language_2+'&event_key='+ event_key;
			
			$.ajax
			({
				type: "POST",
				url: "<?php echo base_url().'admin/notifications_translation/getTagDataForAjax'?>",
				data: dataString,
				cache: false,
				success: function(html)
				{
					texts = html.split('#$@&*#@#$#');
					$("#event").val(texts[0]);
					$("#body_lang1").val(texts[1]);

					$("#subject_lang2").val(texts[2]);
					$("#body_lang2").val(texts[3]);
				}
			});
	
		});
		

	});
	
</script>
	
	
 
<ul id="menu_subject" class="SimpleContextMenu">
<!-- 
<li><a href="javascript:void();" onclick="insertAtCursor(document.frm_notification.subject_lang2, '#*trip#*');"><?php echo lang('trip') ?></a></li>
<li><a href="javascript:void();" onclick="insertAtCursor(document.frm_notification.subject_lang2, '#*trip_no#*');"><?php echo lang('trip_no') ?></a></li>
<li><a href="javascript:void();" onclick="insertAtCursor(document.frm_notification.subject_lang2, '#*trip_date#*');"><?php echo lang('trip_date') ?></a></li>
<li><a href="javascript:void();" onclick="insertAtCursor(document.frm_notification.subject_lang2, '#*the_date#*');"><?php echo lang('the_date') ?></a></li>
<li><a href="javascript:void();" onclick="insertAtCursor(document.frm_notification.subject_lang2, '#*trip_arrival_datetime#*');"><?php echo lang('trip_arrival_datetime') ?></a></li>
<li><a href="javascript:void();" onclick="insertAtCursor(document.frm_notification.subject_lang2, '#*trip_leaving_datetime#*');"><?php echo lang('trip_leaving_datetime') ?></a></li>
<li><a href="javascript:void();" onclick="insertAtCursor(document.frm_notification.subject_lang2, '#*madina_go_to_datetime#*');"><?php echo lang('madina_go_to_datetime') ?></a></li>
<li><a href="javascript:void();" onclick="insertAtCursor(document.frm_notification.subject_lang2, '#*makka_go_to_datetime#*');"><?php echo lang('makka_go_to_datetime') ?></a></li>
<li><a href="javascript:void();" onclick="insertAtCursor(document.frm_notification.subject_lang2, '#*hotel_arrival_datetime#*');"><?php echo lang('hotel_arrival_datetime') ?></a></li>
<li><a href="javascript:void();" onclick="insertAtCursor(document.frm_notification.subject_lang2, '#*hotel_leaving_datetime#*');"><?php echo lang('hotel_leaving_datetime') ?></a></li>
 -->
<?php 
foreach($erp_notifications_tags_rows as $erp_notifications_tags_row) {
$erp_notifications_tags	=$erp_notifications_tags_row->notifications_tags;
$erp_notifications_name	=$erp_notifications_tags_row->{name()};
?> 
<li><a href="javascript:void();" onclick="insertAtCursor(document.frm_notification.subject_lang2, '<?php echo $erp_notifications_tags;?>');"><?php echo $erp_notifications_name;?></a></li>
<?php 
}
?> 
 
</ul> 
	
<ul id="menu_body" class="SimpleContextMenu">
<!-- 
<li><a href="javascript:void();" onclick="insertAtCursor(document.frm_notification.body_lang2, '#*trip#*');"><?php echo lang('trip') ?></a></li>
<li><a href="javascript:void();" onclick="insertAtCursor(document.frm_notification.body_lang2, '#*trip_no#*');"><?php echo lang('trip_no') ?></a></li>
<li><a href="javascript:void();" onclick="insertAtCursor(document.frm_notification.body_lang2, '#*trip_date#*');"><?php echo lang('trip_date') ?></a></li>
<li><a href="javascript:void();" onclick="insertAtCursor(document.frm_notification.body_lang2, '#*the_date#*');"><?php echo lang('the_date') ?></a></li>
<li><a href="javascript:void();" onclick="insertAtCursor(document.frm_notification.body_lang2, '#*trip_arrival_datetime#*');"><?php echo lang('trip_arrival_datetime') ?></a></li>
<li><a href="javascript:void();" onclick="insertAtCursor(document.frm_notification.body_lang2, '#*trip_leaving_datetime#*');"><?php echo lang('trip_leaving_datetime') ?></a></li>
<li><a href="javascript:void();" onclick="insertAtCursor(document.frm_notification.body_lang2, '#*madina_go_to_datetime#*');"><?php echo lang('madina_go_to_datetime') ?></a></li>
<li><a href="javascript:void();" onclick="insertAtCursor(document.frm_notification.body_lang2, '#*makka_go_to_datetime#*');"><?php echo lang('makka_go_to_datetime') ?></a></li>
<li><a href="javascript:void();" onclick="insertAtCursor(document.frm_notification.body_lang2, '#*hotel_arrival_datetime#*');"><?php echo lang('hotel_arrival_datetime') ?></a></li>
<li><a href="javascript:void();" onclick="insertAtCursor(document.frm_notification.body_lang2, '#*hotel_leaving_datetime#*');"><?php echo lang('hotel_leaving_datetime') ?></a></li>
 -->

<?php 
foreach($erp_notifications_tags_rows as $erp_notifications_tags_row) {
$erp_notifications_tags	=$erp_notifications_tags_row->notifications_tags;
$erp_notifications_name	=$erp_notifications_tags_row->{name()};
?> 
<li><a href="javascript:void();" onclick="insertAtCursor(document.frm_notification.body_lang2, '<?php echo $erp_notifications_tags;?>');"><?php echo $erp_notifications_name;?></a></li>

<?php 
}
?> 


</ul>

 
 
<span class="span8">

</span>
<div class="row-fluid">
   
    
     <div class="widget">
    <div class="path-container Fright">
        <div class="path-name Fright">
        <a href="<?php echo   site_url('admin/dashboard')?>"> <?php echo  lang('global_system_management') ?></a>
        </div>
        <div class="path-arrow Fright">
        </div>
        <div class="path-name Fright">
            <?php echo  lang('menu_title') ?>
        </div>
        
    </div>
	</div> 
    
    
    <div class="widget">
           
        
        
        
        <div class="widget-header">
        <div class="widget-header-icon Fright"> <span class="icos-pencil2"></span> </div>
        <div class="widget-header-title Fright">
            <?php echo  lang('title') ?>
        </div>
    	</div>
                             
        <div class="block-fluid">
            <?php echo form_open_multipart("","name='frm_notification'","") ?>
            <div class="row-form">
                <div class="span6">
                    <div class="span4"> <?php echo lang('translate_from') ?><span style="color: red">*</span>:</div>
                    <div class="span8">
                        <?php echo form_dropdown('language_1', $languages, '0', " id='language_1' name='language_1' class='select'  style='width:100%;' ") ?>
                        <?php echo form_error('language_1', '<div class="bottom" style="color:red" >', '</div>'); ?>
                    </div>
                </div>
                
                <div class="span6">
                    <div class="span4"> <?php echo lang('to') ?><span style="color: red">*</span>:</div>
                    <div class="span8">
                        <?php echo form_dropdown('language_2', $languages, '0', " id='language_2' name='language_2' class='select' style='width:100%;' ") ?>
                        <?php echo form_error('language_2', '<div class="bottom" style="color:red" >', '</div>'); ?>
                    </div>
                </div>
                
            </div>
 
  			<div class="row-form">
                <div class="span6">
                    <div class="span4"><?php echo lang('subject_lang1') ?>:</div>
                    <div class="span8">
                        <?php 
						echo "<select name='subject_lang1'  id='subject_lang1' name='subject_lang1' class='select' style='width:100%;' >";
						if(isset($_POST['language_1'])) {
							$language_1_id=$_POST['language_1'];
    	
					    	$this->erp_languages_model->erp_language_id=$language_1_id;
					        $erp_languages_row= $this->erp_languages_model->get();
					
					         $lang_suffix = $erp_languages_row->suffix;
					
					    	 if(file_exists("./static/xml/notifications/notifications_$lang_suffix.xml")) {
					    	 	
					    	 	$xml = new DOMDocument('1.0', 'utf-8');
					            /**
					             * Added to format the xml code, when open with editor.
					             */
					            $xml->formatOutput = true;
								$xml->preserveWhiteSpace = false;
					            
					            $xml->Load("./static/xml/notifications/notifications_$lang_suffix.xml");
					            
					            
					            $notifications = $xml->getElementsByTagName('notification');
					
								foreach ($notifications as $notification) {
									$loop_current_key=$notification->getElementsByTagName('key');
									$loop_current_key_value = $loop_current_key->item(0)->nodeValue;
					  
									$loop_current_subject= $notification->getElementsByTagName('subject');
									$loop_current_subject_value= $loop_current_subject->item(0)->nodeValue;
											
									//$loop_current_body= $notification->getElementsByTagName('body');
									//$loop_current_body_value= $loop_current_body->item(0)->nodeValue;
									
									echo"<option value='$loop_current_key_value'>$loop_current_subject_value</option>";
								}
					            	
					    	 } 
						}
						echo"</select>";
                        ?>
                        <?php echo form_error('subject_lang1', '<div class="bottom" style="color:red" >', '</div>'); ?>
                    </div>
                </div>
			
				<div class="span6">
                    <div class="span4"> <?php echo lang('event') ?>:</div>
                    <div class="span8">
                    	<?php echo form_hidden('event_key', ''," id='event_key' ") ?>
                        <?php echo form_input('event', set_value("event")," id='event' readonly='readonly' class=' input-huge '") ?>
                        <?php echo form_error('event', '<div class="bottom" style="color:red" >', '</div>'); ?>
                    </div>
                </div>
                
            </div>
            
            
            <div class="row-form">
                <div class="span6">
                    <div class="span4"><?php echo lang('subject_lang2') ?><span style="color: red">*</span>:</div>
                    <div class="span8">
                        <?php echo form_input('subject_lang2', set_value("subject_lang2")," id='subject_lang2' class='menu_shortcut_class_subject_lang2  input-huge ' ") ?>
                        <?php echo form_error('subject_lang2', '<div class="bottom" style="color:red" >', '</div>'); ?>
                    </div>
                </div>

                
            </div>
           
            <div class="row-form">
              <div class="span6">
                    <div class="span4"><?php echo lang('body_lang1') ?>:</div>
                    <div class="span8" >
                        <?php echo form_textarea('body_lang1', set_value("body_lang1"), "style='width:500px; height:100px' id='body_lang1' class='menu_shortcut_class_body_lang1'  readonly='readonly' ") ?>
                        <?php echo form_error('body_lang1', '<div class="bottom" style="color:red" >', '</div>'); ?>
                    </div>
                </div>  
            </div>
            
            <div class="row-form">
              <div class="span6">
                    <div class="span4"><?php echo lang('body_lang2') ?><span style="color: red">*</span>:</div>
                    <div class="span8" >
                        <?php echo form_textarea('body_lang2', set_value("body_lang2"), "style='width:500px; height:100px' id='body_lang2' class='menu_shortcut_class_body_lang2' ") ?>
                        <?php echo form_error('body_lang2', '<div class="bottom" style="color:red" >', '</div>'); ?>
                    </div>
                </div>  
            </div>
   
            <div class="toolbar bottom TAC">
            	<input type="submit" name="submit" value="<?php echo lang('global_submit') ?>" class="btn btn-primary"> &nbsp;
            </div>

            <?php echo form_close() ?> 
        </div>
    </div>
</div> 

