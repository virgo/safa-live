<div class="span12">
    <div class="widget">
        <div class="path-container Fright">
            <div class="path-name Fright">
                <a href=""><?= lang('global_system_management') ?></a>
            </div>
            <div class="path-arrow Fright">
            </div>
            <div class="path-name Fright">
                <a href="<?= site_url('admin/vehicles/index') ?>"> <?= lang('safa_vehicles_title') ?></a>
            </div>
            <div class='path-arrow Fright'>
            </div>
            <div class="path-name Fright">
                <?= lang('edit_vehicles_title') ?> <?= $item->{name()} ?>
            </div>
        </div>
    </div>
</div>
    


  
<div class="widget">
    <div class="widget-header">
        <div class="widget-header-icon Fright">
            <span class="icos-pencil2"></span>
        </div>

        <div class="widget-header-title Fright">
           <?= lang('edit_vehicles_title') ?> <?= $item->{name()} ?>
        </div>
    </div>
    
        <div class="block-fluid">
            <?= form_open_multipart() ?>
            <div class="row-form">
                <div class="span6">
                    <div class="span4 TAL Pleft10"> <?= lang('vehicles_name_ar') ?><? if (name() == 'name_ar'): ?><span style="color: red">*</span><? endif; ?></div>
                    <div class="span8">
                        <?= form_input('name_ar', set_value("name_ar",$item->name_ar),"class='input-huge'") ?>
                        <?= form_error('name_ar', '<div class="bottom" style="color:red" >', '</div>'); ?>
                    </div>
                </div>
                <div class="span6">
                    <div class="span4 TAL Pleft10"><?= lang('vehicles_name_la') ?><? if (name() == 'name_la'): ?><span style="color: red">*</span><? endif; ?></div>
                    <div class="span8">
                        <?= form_input('name_la', set_value("name_la",$item->name_la),"class='input-huge'") ?>
                        <?= form_error('name_la', '<div class="bottom" style="color:red" >', '</div>'); ?>
                    </div>
                </div>
            </div>
                <div class="toolbar bottom TAC">
                    <button name="submit" class="btn btn-primary"><?= lang('global_submit') ?></button>
                    <input  type ="button" value="<?= lang('global_back') ?>"class="btn btn-primary" onclick="window.location = '<?= site_url('admin/vehicles/index') ?>'">
                </div>
            <?= form_close() ?>   
        </div>
</div>


