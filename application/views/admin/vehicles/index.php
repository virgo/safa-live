<div class="span12">
    <div class="widget">
        <a class="btn Fleft" href="<?= site_url('admin/vehicles/add') ?>"><?= lang('global_add_new_record') ?></a>
        <div class="path-container Fright">
            <div class="path-name Fright">
                <a href=""><?= lang('global_system_management') ?></a>
            </div>
            <div class="path-arrow Fright">
            </div>
            <div class="path-name Fright">
                <?= lang('safa_vehicles_title') ?>
            </div>
        </div>

    </div>
</div>


<div class="widget">
    <div class="widget-header">
        <div class="widget-header-icon Fright">
            <span class="icos-pencil2"></span>
        </div>
        <div class="widget-header-title Fright">
            <?= lang('safa_vehicles_title') ?>
        </div>
    </div> 


    <div class="widget-container">
        <div class="block-fluid">
            <table cellpadding="0" class="fpTable" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th><?= lang('vehicles_name_ar') ?></th>
                        <th><?= lang('vehicles_name_la') ?></th>
                        <th><?= lang('global_actions') ?></th>
                    </tr>
                </thead>
                <tbody>
                    <? if (isset($items)): ?>
                        <? foreach ($items as $item): ?>
                            <tr>
                                <td><?= $item->name_ar ?></td>
                                <td><?= $item->name_la ?></td>
                                <td class="TAC">
                                    <a href="<?= site_url("admin/vehicles/edit") ?>/<?= $item->safa_vehicle_id ?>"><span class="icon-pencil"></span></a>
                                    <a  onclick="return confirm('<?= lang('global_are_you_sure_you_want_to_delete') ?>')" href="<?= site_url("admin/vehicles/delete") ?>/<?= $item->safa_vehicle_id ?>"><span class="icon-trash"></span></a>
                                </td>
                            </tr>

                        <? endforeach; ?>
                    <? endif; ?>
                </tbody>
            </table>
        </div>
    </div> 
</div>

