
<? if (isset($item)): ?>

    <div class="span12">
        <div class="widget">
            <div class="path-container Fright">
                <div class="path-name Fright">
                    <a href=""><?= lang('global_system_management') ?></a>
                </div>
                <div class="path-arrow Fright">
                </div>
                <div class="path-name Fright">
                    <?= lang('hotel_data') ?>
                </div>
                <div class="path-arrow Fright">
                </div>
                <div class="path-name Fright">
                    <?= lang('hotel_info') ?> <?= $item->{name()} ?>
                </div>
            </div>


        </div>
    </div>


    <div class="span2">
        <div class="search-widget">
            <div class="widget-header">

                <div class="widget-header-icon Fright">
                    <span class="icos-pencil2"></span>
                </div>

                <div class="widget-header-title Fright">
                    <a href="<?= site_url('admin/search_hotels/index') ?>"><?= lang('search_hotels') ?></a>
                </div>

            </div>





        </div>
    </div>
    <div class="span10">
        <div class="widget">

            <div class="widget-container">

                <table cellpadding="0" class="myTable" cellspacing="0" width="100%" style="margin-bottom: 7PX">
                    <tr>
                        <td style="text-align: right">
                            <? if (lang('global_lang') == 'ar'): ?><?= $item->name_ar ?><? else: ?><?= $item->name_la ?><? endif ?>

                            <? if ($item->erp_hotellevel_id == '1'): ?>
                                <img src="<?= IMAGES ?>/star.png" style="width:15px;height: 11px"/>
                            <? endif ?>

                            <? if ($item->erp_hotellevel_id == '2'): ?>
                                <img src="<?= IMAGES ?>/star.png" style="width:15px;height: 11px"/><img src="<?= IMAGES ?>/star.png" style="width:15px;height: 11px"/>
                            <? endif ?>

                            <? if ($item->erp_hotellevel_id == '3'): ?>
                                <img src="<?= IMAGES ?>/star.png" style="width:15px;height: 11px"/><img src="<?= IMAGES ?>/star.png" style="width:15px;height: 11px"/><img src="<?= IMAGES ?>/star.png" style="width:15px;height: 11px"/>
                            <? endif ?>

                            <? if ($item->erp_hotellevel_id == '4'): ?>
                                <img src="<?= IMAGES ?>/star.png" style="width:15px;height: 11px"/><img src="<?= IMAGES ?>/star.png" style="width:15px;height: 11px"/><img src="<?= IMAGES ?>/star.png" style="width:15px;height: 11px"/><img src="<?= IMAGES ?>/star.png" style="width:15px;height: 11px"/>
                            <? endif ?>

                            <? if ($item->erp_hotellevel_id == '5'): ?>
                                <img src="<?= IMAGES ?>/star.png" style="width:15px;height: 11px"/><img src="<?= IMAGES ?>/star.png" style="width:15px;height: 11px"/><img src="<?= IMAGES ?>/star.png" style="width:15px;height: 11px"/><img src="<?= IMAGES ?>/star.png" style="width:15px;height: 11px"/><img src="<?= IMAGES ?>/star.png" style="width:15px;height: 11px"/>
                            <? endif ?>

                            <? if ($item->erp_hotellevel_id == '6'): ?>
                                <img src="<?= IMAGES ?>/star.png" style="width:15px;height: 11px"/><img src="<?= IMAGES ?>/star.png" style="width:15px;height: 11px"/><img src="<?= IMAGES ?>/star.png" style="width:15px;height: 11px"/><img src="<?= IMAGES ?>/star.png" style="width:15px;height: 11px"/><img src="<?= IMAGES ?>/star.png" style="width:15px;height: 11px"/><img src="<?= IMAGES ?>/star.png" style="width:15px;height: 11px"/>
                            <? endif ?>

                            <? if ($item->erp_hotellevel_id == '7'): ?>
                                <img src="<?= IMAGES ?>/star.png" style="width:15px;height: 11px"/><img src="<?= IMAGES ?>/star.png" style="width:15px;height: 11px"/><img src="<?= IMAGES ?>/star.png" style="width:15px;height: 11px"/><img src="<?= IMAGES ?>/star.png" style="width:15px;height: 11px"/><img src="<?= IMAGES ?>/star.png" style="width:15px;height: 11px"/><img src="<?= IMAGES ?>/star.png" style="width:15px;height: 11px"/><img src="<?= IMAGES ?>/star.png" style="width:15px;height: 11px"/>
                            <? endif ?>

                            (<? if (lang('global_lang') == 'ar'): ?><?= $item->hotel_sublevel_name_ar ?><? else: ?><?= $item->hotel_sublevel_name_la ?><? endif ?>)</td>
                    </tr>
                     </table>
                
          
                
                <div class="tabs" style="margin:0 auto;text-align: center">
                        <a href="#sub1" style="padding:5px;margin:10px;color:#000"><?= lang('hotel_description') ?></a>
                        <a href="#sub2" style="padding: 5px;margin: 10px;color:#000 "><?= lang('connect_info') ?></a>
                        <a href="#sub3" style="padding: 5px; margin: 10px;color:#000"><?= lang('hotel_advantages') ?></a>
                        <a href="#sub4" style="padding: 5px;margin: 10px;color:#000"><?= lang('hotel_policy') ?></a>
                </div>     
                  

                <div class="span11 inner-widget" style="text-align: center">

                    <link rel="stylesheet" href="<?= CSS ?>/nivo/default.css" type="text/css" media="screen" />
                    <link rel="stylesheet" href="<?= CSS ?>/nivo/light.css" type="text/css" media="screen" />
                    <link rel="stylesheet" href="<?= CSS ?>/nivo/dark.css" type="text/css" media="screen" />
                    <link rel="stylesheet" href="<?= CSS ?>/nivo/bar.css" type="text/css" media="screen" />
                    <link rel="stylesheet" href="<?= CSS ?>/nivo/nivo-slider.css" type="text/css" media="screen" />
                    <link rel="stylesheet" href="<?= CSS ?>/nivo/style.css" type="text/css" media="screen" />
                    <link rel="stylesheet" href="<?= CSS ?>/nivo/nivo.css" type="text/css" media="screen" />

                    <div id="wrapper">
                        <div class="slider-wrapper theme-default">
                            <div id="slider" class="nivoSlider" style='height:350px;'>
                                <? if (isset($images)): ?>
                                    <? foreach ($images as $image): ?>

                                        <img src="<?= HOTEL_ABS_PATH ?>/<?= $image->hotel_photospath ?>"  />

                                    <? endforeach; ?>
                                <? endif ?>

                            </div>

                        </div>

                    </div>
                    <script type="text/javascript" src="<?= JS ?>/jquery-1.9.0.min.js"></script>
                    <script type="text/javascript" src="<?= JS ?>/jquery.nivo.slider.js"></script>
                    <script type="text/javascript">
                        $(window).load(function() {
                            $('#slider').nivoSlider();
                        });
                    </script>
                </div>

                <div id="sub1">
                    <div class="span11 inner-widget">
                       <p style='line-height:25px;padding:10px'> <?= $item->remark ?></p>
                    </div>
                </div>

                <div class="span11 inner-widget" style='padding:10px'>
                    
                    <span style="color:#663300;font-weight: bold;font-size: 15px;"><?= lang('hotel_rooms') ?></span> : <span style="color:red"><?= $item->rooms_number ?></span>
                    <br>
                </div>

                <div class="span11 inner-widget" style='padding:10px'>
                    <span style="color:#663300;font-weight: bold;font-size: 15px"><?= lang('connect_info') ?></span> : 
                    <br><br>
                    <? if (lang('global_lang') == 'ar'): ?><img src="<?= IMAGES ?>/filetree/css.png"> <?= $item->address_ar ?><? else: ?><img src="<?= IMAGES ?>/icons/alerts/sign_tick.png"><?= $item->address_la ?> <? endif ?>
                    <br><br>
                     <img src="<?= IMAGES ?>/filetree/css.png"/> <?= lang('hotel_phone') ?> :<?= $item->phone ?>
                    <br><br>
                     <img src="<?= IMAGES ?>/filetree/css.png"/> <?= lang('hotel_email') ?> : <?= $item->email ?>
                    <br><br>
                     <img src="<?= IMAGES ?>/filetree/css.png"/> <?= lang('hotel_fax') ?> : <?= $item->fax ?>
                    <br><br>
                </div>

                <div class="span11 inner-widget" style='padding:10px'>
                 
                    <span style="color:#663300;font-weight: bold;font-size: 15px"><?= lang('hotel_advantages') ?></span> :
                    <br>
                     <? if (isset($hotel_advantages)): ?>
                        <? foreach ($hotel_advantages as $hotel_advantages): ?>
                              <br>
                               <? if (lang('global_lang') == 'ar'): ?><img src="<?= IMAGES ?>/icons/alerts/sign_tick.png"/> <?= $hotel_advantages->arabic_advantage ?><? else: ?><img src="<?= IMAGES ?>/icons/alerts/sign_tick.png"/><?= $hotel_advantages->english_advantage ?><? endif ?>
                              <span style='color:red'>:</span> <?= $hotel_advantages->hotel_advantages_details ?>
                               <br>
                        <? endforeach; ?>
                    <? endif ?>
                </div>

                <div class="span11 inner-widget" style='padding:10px'>
                    <span style="color:#663300;font-weight: bold;font-size: 15px"><?= lang('hotel_policy') ?></span> : 
                   
                    <br> <? if (isset($hotel_polices)): ?>
                        <? foreach ($hotel_polices as $policy): ?>
                          <br>
                               <? if (lang('global_lang') == 'ar'): ?><img src="<?= IMAGES ?>/icons/alerts/sign_tick.png"/> <?= $policy->arabic_policy ?><? else: ?><img src="<?= IMAGES ?>/icons/alerts/sign_tick.png"/><?= $policy->english_policy ?><? endif ?>
                              <span style='color:red'> : </span> <?= $policy->policy_description ?>
                               <br>
                        <? endforeach; ?>
                    <? endif ?>
                </div>

                <span class="" style="float:left;"><a href="" style="color:black;"><?= lang('build_your_hotel') ?></a></span>
            </div>



        </div>

    </div>


<? endif ?>


