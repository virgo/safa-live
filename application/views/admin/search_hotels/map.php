<script src="https://maps.googleapis.com/maps/api/js?AIzaSyC794uPScLtRUa8Uwk9nX8RCjrepoxszxg&sensor=true"></script>

<div id="map" style="width:750px;height:300px" >

</div>
 <script>
    function initialize(lat,long){
        var myLatlng = new google.maps.LatLng(lat,long);
        var mapOptions = {
            zoom:15,
            center:myLatlng,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        }
        var map = new google.maps.Map(document.getElementById('map'),mapOptions);
         var marker = new google.maps.Marker({
            position:myLatlng,
            map: map,
            title: 'Visit us here!'
        });
    }
</script>
<script type="text/javascript">
      initialize(<?=$this->input->get("lat")?>,<?=$this->input->get("long")?>);       

</script>