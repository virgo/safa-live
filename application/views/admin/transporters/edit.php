<span class="span8">

</span>
<div class="row-fluid">
    
    
    <div class="widget">
    <div class="path-container Fright">
    	<div class="path-name Fright">
            <a href="<?php echo  site_url().'admin/dashboard'; 
            
            ?>"><?php echo  lang('global_system_management') ?></a>
        </div>
        <div class="path-arrow Fright"></div>
        <div class="path-name Fright"> <a href="<?= site_url('admin/transporters/index') ?>"><?= lang('menu_main_transport_companies') ?></a>  </div>
        
        <div class="path-arrow Fright"></div>
        <div class="path-name Fright">  <?= lang('safa_transporters_edit') ?> <? if (lang('global_lang') == 'ar'): ?><?= $item->name_ar ?><? else: ?><?= $item->name_la ?><? endif ?></div>
    </div>
    
        
	</div>

    <div class="widget">
        
        
        <div class="widget-header">
        <div class="widget-header-icon Fright"> <span class="icos-pencil2"></span> </div>
        <div class="widget-header-title Fright">
            <?= lang('safa_transporters_edit') ?> <? if (lang('global_lang') == 'ar'): ?><?= $item->name_ar ?><? else: ?><?= $item->name_la ?><? endif ?>
        </div>
    	</div>
                              
        <div class="block-fluid">
            <?= form_open_multipart() ?>

            <div class="row-form">
                <div class="span6">
                    <div class="span4"><?= lang('safa_transporter_name_ar') ?>
                        <span style="color: red">*</span>:
                    </div>
                    <div class="span8"><?= form_input('name_ar', set_value("name_ar", $item->name_ar), " ") ?>
                        <?= form_error('name_ar', '<div class="bottom" style="color:red" >', '</div>'); ?>                    
                    </div>
                </div>
                <div class="span6">
                    <div class="span4"><?= lang('safa_transporter_name_la') ?>
                        <span style="color: red">*</span>:
                    </div>
                    <div class="span8"><?= form_input('name_la', set_value("name_la", $item->name_la), " ") ?>
                        <?= form_error('name_la', '<div class="bottom" style="color:red" >', '</div>'); ?>                    
                    </div>
                </div>
            </div>


            <div class="row-form">
                <div class="span6">
                    <div class="span4"><?= lang('safa_transporter_erp_country_id') ?>
                       <span style="color: red">*</span>:
                    </div>
                    <div class="span8">
                        <?= form_dropdown('erp_country_id', ddgen('erp_countries', array('erp_country_id', name())), set_value('erp_country_id', $item->erp_country_id), " name='s_example' class='select' style='width:100%;' ") ?>
                        <?= form_error('erp_country_id', '<div class="bottom" style="color:red" >', '</div>'); ?>                    
                    </div>
                </div>
                <div class="span6">
                    <div class="span4"><?= lang('safa_transporter_erp_transportertype_id') ?>
                        <span style="color: red">*</span>:
                    </div>
                    <div class="span8">
                        <?= form_dropdown('erp_transportertype_id', ddgen('erp_transportertypes', array('erp_transportertype_id', name())), set_value('erp_transportertype_id', $item->erp_transportertype_id), " name='s_example' class='select' style='width:100%;' ") ?>
                        <?= form_error('erp_transportertype_id', '<div class="bottom" style="color:red" >', '</div>'); ?>                    
                    </div>
                </div>
            </div>

            <div class="row-form">
                <div class="span6">
                    <div class="span4"><?= lang('safa_transporter_code') ?>
                       <span style="color: red">*</span>:
                    </div>
                    <div class="span8"><?= form_input('code', set_value("code", $item->code), " ") ?>
                        <?= form_error('code', '<div class="bottom" style="color:red" >', '</div>'); ?>                    
                    </div>
                </div>
            </div>




            <div class="toolbar bottom TAC"><input type="submit" name="submit" value="<?= lang('global_submit') ?>" class="btn btn-primary"> &nbsp; 
                <input  type ="button" value="<?= lang('global_back') ?>"class="btn btn-primary" onclick="window.location = '<?= site_url('admin/transporters/index') ?>'">
            </div>
            <?= form_close() ?> 
        </div>
    </div>
</div> 
<!--<script type='text/javascript' src='<? //= MODULE_JS              ?>/poll/add.js'></script>-->

