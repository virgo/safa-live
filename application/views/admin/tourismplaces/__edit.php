<div class="row-fluid">
    <div style="margin-bottom:10px; text-align:left; border:3px solid #FFF; background:url(<?= IMAGES ?>/backgrounds/bg_box_head.jpg); padding:5px 10px; height:30px;">
        <div style="float:right; text-align:right; font-size:14px; line-height:30px; color:#333;">  <?= lang('global_system_management') ?> <span style="color:#80693d"><img src="<?= IMAGES ?>/jbreadcrumb/Chevron-ar.gif" width="5" height="20"></span> <a href="<?= site_url('admin/tourismplaces/index') ?>"> <?= lang('menu_main_sights') ?></a> <span style="color:#80693d"><img src="<?= IMAGES ?>/jbreadcrumb/Chevron-ar.gif" width="5" height="20"></span> <?= lang('tourismplaces_edit_title') ?> <?= $items->{name()} ?>  </div>
    </div>


    <div class="widget">
        <div class="head dark">
            <div class="icon"><i class="icos-pencil2"></i></div>
            <h2> 
            <?= lang('tourismplaces_edit_title') ?> <?= $items->{name()} ?>
            </h2> 
        </div>                        
        <div class="block-fluid">
            <?= form_open() ?>
            <div class="row-form">
                <div class="span6">
                    <div class="span4"><?= lang('tourismplaces_erp_country_id') ?><span style="color: red">*</span>:</div>
                    <div class="span6">
                        <?= form_dropdown('erp_country_id', ddgen('erp_countries', array('erp_country_id', name())), set_value('erp_country_id', $country->erp_country_id), " id='erp_country_id' name='s_example' class='select' style='width:100%;' ") ?>
                        <?= form_error('erp_country_id', '<div class="bottom" style="color:red" >', '</div>'); ?> 
                    </div>
                </div>
                <div class="span6">
                    <div class="span4"><?= lang('tourismplaces_erp_city_id') ?><span style="color: red">*</span>:</div>
                    <div class="span6">
                        <? //= form_dropdown('erp_city_id', ddgen('erp_cities', array('erp_city_id', name())), set_value('erp_city_id')," class='input-small' id='erp_city_id' ") ?>
                        <?= form_dropdown("erp_city_id", array("0" => lang('tourismplaces_select_from_menu_cities')), set_value("erp_city_id"), "id='erp_city_id'") ?>
                        <?= form_error('erp_city_id', '<div class="bottom" style="color:red" >', '</div>'); ?>
                    </div>
                </div>
            </div>

            <div class="row-form">
                <div class="span6">
                    <div class="span4"> <?= lang('tourismplaces_name_ar') ?><span style="color: red">*</span>:</div>
                    <div class="span6">
                        <?= form_input('name_ar', set_value("name_ar", $items->name_ar)) ?>
                        <?= form_error('name_ar', '<div class="bottom" style="color:red" >', '</div>'); ?>
                    </div>
                </div>
                <div class="span6">
                    <div class="span4"> <?= lang('tourismplaces_name_la') ?><span style="color: red">*</span>:</div>
                    <div class="span6">
                        <?= form_input('name_la', set_value("name_la", $items->name_la)) ?>
                        <?= form_error('name_la', '<div class="bottom" style="color:red" >', '</div>'); ?>
                    </div>
                </div>
            </div>

            <div class="toolbar bottom TAC"><input type="submit" name="submit" value="<?= lang('global_submit') ?>" class="btn btn-primary"> &nbsp;
                <input  type ="button" value="<?= lang('global_back') ?>"class="btn btn-primary" onclick="window.location = '<?= site_url('admin/tourismplaces/index') ?>'">
            </div>
            <?= form_close() ?>       
        </div>
    </div>
</div>
<script>
                    $(document).ready(function() {
                        var country_id = $("#erp_country_id").val();

                        $("#erp_country_id").change(function() {
                            $.get('<?= site_url("admin/tourismplaces/get_erp_city") ?>/' + $(this).val(), function(data) {
                                $("#erp_city_id").html(data);
                            });
                        });
                        $.get('<?= site_url("admin/tourismplaces/get_erp_city") ?>/' + country_id, function(data) {
                            $("#erp_city_id").html(data);
<? if (isset($_POST['erp_city_id'])): ?>
                                $("#erp_city_id").val('<?= $this->input->post('erp_city_id') ?>');
<? else: ?>
                                $("#erp_city_id").val('<?= $items->erp_city_id ?>');
<? endif; ?>
                        });
                    });
</script>

