<!-- By Gouda, For popup -->
<script type="text/javascript" src='<?= CSS_JS ?>/form/form.js'></script>
<link rel="stylesheet" href="<?= CSS ?>/bootstrap/bootstrap.min.new.css"/>
     
   

<div class="row-fluid">
   


<div class="widget">
    <div class="path-container Fright">
    	<div class="path-name Fright">
            <a href="<?php echo  site_url().'admin/dashboard'; 
            
            ?>"><?php echo  lang('global_system_management') ?></a>
        </div>
        <div class="path-arrow Fright"></div>
        <div class="path-name Fright">  <?= lang('menu_main_sights') ?></div>
    </div>
    
    <a href="<?= site_url("admin/tourismplaces/add") ?>" class="btn Fleft"><?= lang('global_add') ?></a>
        
</div>

 <div class="widget">
<style>
    .updated_msg{
        display:none;
        background-color:#ccee97;
        font-weight: bold;
        text-align: center;
        border:3px solid #cccdc9; 
        padding:20px;  
        margin-bottom:10px;
        border-radius:15px;
        margin:10px; 
    }
</style>
<div class='row-fluid' align='center' >
    <div  class='updated_msg' >
        <br><input  type ="button" value="<?=lang('global_submit')?>"class="btn btn-primary" id='ok' >
    </div> 
 </div>
       
        
        <div class="widget-header">
        <div class="widget-header-icon Fright"> <span class="icos-pencil2"></span> </div>
        <div class="widget-header-title Fright">
            <?php echo  lang('global_search') ?>
        </div>
    	</div>
                              
        <div class="block-fluid slidingDiv">
            <?= form_open('admin/tourismplaces/index', 'method="get"') ?>
            <div class="row-form">
                <div class="span6">
                    <div class="span2"><?= lang('tourismplaces_erp_country_id') ?></div>
                    <div class="span6">
                        <?= form_dropdown('erp_country_id', ddgen('erp_countries', array('erp_country_id', name())), set_value('erp_country_id', $country->erp_country_id), " id='erp_country_id' name='s_example' class='select' style='width:100%;' ") ?>
                        <?= form_error('erp_country_id', '<div class="bottom" style="color:red" >', '</div>'); ?> 
                    </div>
                </div>
                <div class="span6">
                    <div class="span2"><?= lang('tourismplaces_erp_city_id') ?></div>
                    <div class="span6">
                        <? //= form_dropdown('erp_city_id', ddgen('erp_cities', array('erp_city_id', name())), set_value('erp_city_id')," class='input-small' id='erp_city_id' ") ?>
                        <?= form_dropdown("erp_city_id", array("0" => lang('tourismplaces_select_from_menu_cities')), set_value("erp_city_id"), "id='erp_city_id'") ?>
                        <?= form_error('erp_city_id', '<div class="bottom" style="color:red" >', '</div>'); ?>
                    </div>
                </div>
            </div>
            <div class="toolbar bottom TAC"><input type="submit" name="search" value="<?= lang('global_search') ?>" class="btn btn-primary" /></div>
            <?= form_close() ?>
        </div>
    </div>


    <div class="widget">
           
        
        <div class="widget-header">
        <div class="widget-header-icon Fright"> <span class="icos-pencil2"></span> </div>
        <div class="widget-header-title Fright">
            <?php echo  lang('menu_main_sights') ?>
        </div>
    	</div>
                     
        <div class="block-fluid">
            <table cellpadding="0" class="fpTable" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th><?= lang('global_arabic_name') ?></th>
                        <th><?= lang('global_english_name') ?></th>
                        <th><?= lang('tourismplaces_th_city_name') ?></th>
                        <th><?= lang('global_actions') ?></th>
                    </tr>
                </thead>
                <tbody>
                    <? if (isset($items)): ?>
                        <? foreach ($items as $item): ?>
                            <tr id="row_<?php echo $item->safa_tourismplace_id; ?>">
                                <td><?= $item->name_ar ?></td>
                                <td><?= $item->name_la ?></td>
                                <td><? if (lang('global_lang') == 'ar'): ?><?= $item->city_name_ar ?><? else: ?><?= $item->city_name_la ?><? endif ?></td>
                                <td class="TAC">
                                    <a href="<?= site_url("admin/tourismplaces/edit") ?>/<?= $item->safa_tourismplace_id ?>"><span class="icon-pencil"></span></a>
                                    <? if ($this->tourismplaces_model->check_delete_ability($item->safa_tourismplace_id) == 0): ?>
                                        <? if (lang('global_lang') == 'ar'): ?>
                                            <a href="#fModal"  name="<?= $item->name_ar ?>" id="<?= $item->safa_tourismplace_id ?>" class=" delete_item" data-toggle="modal">
                                            <? else: ?>
                                                <a href="#fModal"  name="<?= $item->name_la ?>" id="<?= $item->safa_tourismplace_id ?>" class=" delete_item" data-toggle="modal">
                                                <? endif ?>       
                                                <span class="icon-trash"></span></a>
                                        <? else: ?><? endif ?>
                                </td>
                            </tr>
                        <? endforeach; ?>
                    <? endif; ?>
                </tbody>
            </table>
            <?= form_close() ?>
        </div>
    </div>
</div>
<div class="row-fluid">
    <?= $pagination ?>
<div id="fModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="myModalLabel"><?= lang('global_system_message') ?></h3>
    </div>        
    <div class="row-fluid">
        <div id="msg" class="row-form">
            <?=  lang('global_are_you_sure_you_want_to_delete')?>
        </div>
    </div>                   
    <div class="modal-footer">
        <button class="btn btn-primary" data-dismiss="modal" aria-hidden="true"id="yes"><?= lang('global_yes')?></button>
        <button class="btn btn-primary" data-dismiss="modal" aria-hidden="true"id="no"><?= lang('global_no')?></button>       </div>
</div>
  <script type="text/javascript">
    $(document).ready(function(){
        $('.delete_item').click(function(){
                        var name= $(this).attr('name');
                        var safa_tourismplace_id= $(this).attr('id');
                        <? if (lang('global_lang') == 'ar'): ?>
                            $('#msg').text("<?= lang('global_are_you_sure_you_want_to_delete') ?>"+" "+name)
                        <? else: ?>
                            $('#msg').text(name+" "+"<?= lang('global_are_you_sure_you_want_to_delete') ?>")
                        <?  endif;?>    
			 $('#yes').click(function(){
                         var answer =$(this).attr('id');
                           if(answer==='yes'){
				$.ajax({
					type: "POST",
					dataType: 'json',
					url: "<?php echo site_url('admin/tourismplaces/delete');?>",
					data: {'safa_tourismplace_id':safa_tourismplace_id},
					success: function(msg){
                                         if(msg.response==true){
                                            var del = safa_tourismplace_id;
                                            $("#row_"+del).remove();
                                             <? if (lang('global_lang') == 'ar'): ?>
                                                $('.updated_msg').text("<?=lang('global_delete_confirm')?>"+" "+name);
                                                $('.updated_msg').append('<br>');
                                                $('.updated_msg').append('<input  type ="button" value="<?=lang('global_submit')?>"class="btn btn-primary" id="ok" >');
                                             <? else: ?>
                                                $('.updated_msg').text(name+" "+"<?=lang('global_delete_confirm')?>");
                                                $('.updated_msg').append('<br>');
                                                $('.updated_msg').append('<input  type ="button" value="<?=lang('global_submit')?>"class="btn btn-primary" id="ok" >');
                                             <?  endif;?>
                                              $('.updated_msg').show();
                                              $('#ok').click(function(){
                                                 $('.updated_msg').hide();
                                               });
				          }else if(msg.response==false){
                                                    $('.updated_msg').text(msg.msg);
                                                    $('.updated_msg').append('<br>');
                                                $('.updated_msg').append('<input  type ="button" value="<?=lang('global_submit')?>"class="btn btn-primary" id="ok" >');
                                                    $('.updated_msg').show();
                                                    $('#ok').click(function(){
                                                       $('.updated_msg').hide();
                                                    });
				          }
					}
				});
			}else{
				return FALSE;
			}
                      });
		});
	});
	</script>  
    <script>
        $(document).ready(function() {
            $("#erp_country_id").change(function() {
                //sending the request to get the cities//
                $.get('<?= site_url("admin/tourismplaces/get_erp_cities") ?>/' + $(this).val(), function(data) {
                    $("#erp_city_id").html(data);
                });
            });
            var country_id = $("#erp_country_id").val();
            $.get('<?= site_url("admin/tourismplaces/get_erp_cities") ?>/' + country_id, function(data) {
                $("#erp_city_id").html(data);
                $("#erp_city_id").val('<?= $this->input->get('erp_city_id') ?>');
            });
        });
    </script>

   
