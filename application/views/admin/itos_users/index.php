<div class='row-fluid'>
    <div class='span12'>
        <div class='widget'>
            <div class='head dark'>
                <div class="icon"><span class="icos-cube1"></div>
                <h2><?= lang('menu_safa_itos_users') ?></h2>
                <ul class="buttons">                            
                    <li>
                        <a href="#"><span class="icos-arrow-down5"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="<?= site_url("admin/safa_itos/users_add/".$this->uri->segment("4"))?>"><?=lang('safa_itos_users_add_title')?></a></li>
                        </ul>
                    </li>
            </div>
            <div class='block-fluid' >
                <table class="table" width="100%" >
                    <thead>
                        <tr>                     
                            <th><?= lang('itos_users_name_ar') ?></th>
                            <th><?= lang('itos_users_name_la') ?></th>
                            <th><?= lang('global_actions') ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <? if (isset($items)): ?>
                            <? foreach ($items as $item): ?>
                                <tr>
                                    <td><?= $item->name_ar ?></td>
                                    <td><?= $item->name_la ?></td>
                                    <td class="TAC">
                                        <a href="<?= site_url("admin/safa_itos/users_edit") ?>/<?= $item->safa_ito_user_id ?>/<?= $this->uri->segment("4"); ?>"><span class="icon-pencil"></span></a>
                                        <? if ($this->itos_model->check_users_delete_ability($item->safa_ito_id) > 1): ?>
                                            <a onclick="return confirm('<?= lang('global_are_you_sure_you_want_to_delete')?> <? if (lang('global_lang') == 'ar'): ?><?= $item->name_ar ?><? else: ?><?= $item->name_la ?><? endif ?>')"  href= "<?= site_url("admin/safa_itos/users_delete") ?>/<?= $item->safa_ito_user_id ?>/<?= $this->uri->segment("4"); ?>"><span class="icon-trash"></span></a>
                                     <? endif; ?>
                                    </td>
                                </tr>
                            <? endforeach; ?>
                        <? endif; ?>
                    </tbody>
                </table>
            </div>
        </div>
        <?= $pagination ?>
    </div>
</div>

