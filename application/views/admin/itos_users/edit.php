<div class="row-fluid">
    <div class="span12" >
        <div class="widget">
            <div class="head dark">
                <div class="icon"><i class="icos-pencil2"></i></div>
                <h2><?= lang('safa_itos_edit') ?><?= $item->name_ar?></h2> 
            </div>                        
            <div class="block-fluid">
                <?= form_open() ?>

                <div class="row-form">
                    <div class="span6">
                        <div class="span4"><?= lang('itos_users_name_ar') ?><font style="color:red" >*</font></div>
                        <div class="span8" >
                            <?= form_input('name_ar', set_value('name_ar' , $item->name_ar)) ?>
                            <span class="bottom" style="color:red" ><?=form_error('name_ar')?></span> 
                        </div>   
                    </div>
                    <div class="span6">
                        <div class="span4"><?= lang('itos_users_name_la') ?><font style="color:red" >*</font></div>
                        <div class="span8" >
                            <?= form_input('name_la', set_value('name_la' ,$item->name_la)) ?>
                            <span class="bottom" style="color:red" ><?=form_error('name_la')?></span> 
                        </div>   
                    </div>
                </div>

                <div class="row-form">
                    <div class="span6">
                        <div class="span4"><?= lang('itos_users_username') ?><font style="color:red" >*</font></div>
                        <div class="span8" >
                            <?= form_input('username', set_value('username',$item->username)) ?>
                            <span class="bottom" style="color:red" ><?=form_error('username')?></span> 
                        </div>   
                    </div>
                </div>

                <div class="row-form">
                    <div class="span6">
                        <div class="span4"><?= lang('itos_users_password') ?><font style="color:red" >*</font></div>
                        <div class="span8" >
                            <?= form_password('password', set_value('password')) ?>
                            <span class="bottom" style="color:red">
                                <?=form_error('password')?>
                            </span>
                        </div>
                    </div>
                    <div class="span6">
                        <div class="span4"><?= lang('itos_users_repeat_password') ?><font style="color:red" >*</font></div>
                        <div class="span8">
                            <?= form_password('passconf', set_value('passconf')) ?>
                               <span class="bottom" style="color:red" >
                                   <?=form_error('passconf')?>
                               </span>
                        </div>
                    </div>
                </div>
                
               <div class="row-form">
                    <div class="span6">
                        <div class="span4"><?= lang('itos_users_ver_code') ?><font style="color:red" >*</font></div>
                        <div class="span8" >
                            <?= form_input('ver_code', set_value('ver_code' , $item->ver_code)) ?>
                            <span class="bottom" style="color:red">
                                <?=form_error('ver_code')?>
                            </span>
                        </div>
                    </div>
                    <div class="span6">
                        <div class="span4"><?= lang('itos_users_email') ?><font style="color:red" >*</font></div>
                        <div class="span8">
                            <?= form_input('email', set_value('email' ,$item->email)) ?>
                               <span class="bottom" style="color:red" >
                                   <?=form_error('email')?>
                               </span>
                        </div>
                    </div>
                </div>
                <div class="toolbar bottom TAC">
                    <button class="btn btn-primary"><?= lang('global_submit') ?></button>
                   <a  class="btn btn-primary" href="<?= site_url('admin/safa_itos/users_index') ?>/<?= $this->uri->segment("5"); ?>" ><?= lang('global_back') ?></a>
                </div>
                
                <?= form_close() ?> 
            </div>
        </div>  
    </div>
</div>  

