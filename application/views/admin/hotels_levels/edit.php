<div class="span12">
    <div class="widget">
        <div class="path-container Fright">
            <div class="path-name Fright">
                <a href=""><?= lang('global_system_management') ?></a>
            </div>
            <div class="path-arrow Fright">
            </div>
            <div class="path-name Fright">
                <a href="<?= site_url('admin/hotels_levels/index') ?>"> <?= lang('hotel_levels') ?></a>
            </div>
            <div class='path-arrow Fright'>
            </div>
            <div class="path-name Fright">
                <?= lang('edit_hotels_levels') ?> <?= $items->{name()} ?>
            </div>
        </div>
    </div>
</div>

<div class="widget">
    <div class="widget-header">
        <div class="widget-header-icon Fright">
            <span class="icos-pencil2"></span>
        </div>

        <div class="widget-header-title Fright">
           <?= lang('edit_hotels_levels') ?> <?= $items->{name()} ?>
        </div>
    </div>


                         
        <div class="block-fluid">
           <?= form_open_multipart('', 'autocomplete="off" ') ?> 
            
            <div class="row-form">
                <div class="span6">
                    <div class="span4 TAL Pleft10"><?= lang('hotels_levels_name_ar') ?><?if(name()=='name_ar'):?><font style="color:red" >*</font><?endif;?></div>
                    <div class="span6">
                        <?= form_input('name_ar', set_value("name_ar",$items->name_ar),"class='input-huge'") ?>
                        <?= form_error('name_ar', '<span class="bottom" style="color:red" >', '</span>'); ?>
                    </div>
                </div>
                <div class="span6">
                    <div class="span4 TAL Pleft10"><?= lang('hotels_levels_name_la') ?><?if(name()=='name_la'):?><font style="color:red" >*</font><?endif;?></div>
                    <div class="span6" >
                        <?= form_input('name_la', set_value("name_la",$items->name_la),"class='input-huge'") ?>
                        <?= form_error('name_la', '<span class="bottom" style="color:red" >', '</span>'); ?>  
                    </div>     
                </div>
               
            </div>
            
            <div class="row-form">
                <div class="span6">
                    <div class="span4 TAL Pleft10"><?= lang('hotels_levels_code') ?></div>
                    <div class="span6" >
                      <?= form_input('code', set_value("code",$items->code),"class='input-huge'") ?>
                      <?= form_error('code', '<div class="bottom" style="color:red" >', '</div>'); ?>
                    </div>     
                </div>
            </div>
            
            <div class="toolbar bottom TAC">
                <button name="submit" class="btn btn-primary"><?= lang('global_submit') ?></button>
                <a href="<?= site_url('admin/hotels_levels/index') ?>" class="btn btn-primary"  ><?= lang('global_back') ?></a>
           </div>

            <?= form_close() ?> 
        </div>




