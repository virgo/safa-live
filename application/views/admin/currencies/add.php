

<div class="widget">
    <div class="path-container Fright">
    	<div class="path-name Fright">
            <a href="<?php echo  site_url().'admin/dashboard'; 
            
            ?>"><?php echo  lang('global_system_management') ?></a>
        </div>
        <div class="path-arrow Fright"></div>
        <div class="path-name Fright">  <a href="<?=  site_url('admin/currencies/index')?>"><?= lang('parent_child_title') ?></a></div>
        <div class="path-arrow Fright"></div>
        <div class="path-name Fright">  <?= lang('add_currency') ?></div>
    </div>
    
        
</div>

<div class="row-fluid">
    <div class="widget">
         
        
        <div class="widget-header">
        <div class="widget-header-icon Fright"> <span class="icos-pencil2"></span> </div>
        <div class="widget-header-title Fright">
            <?php echo  lang('add_currency') ?>
        </div>
    	</div>
                              
        <div class="block-fluid">
           <?= form_open_multipart('', 'autocomplete="off" id="currencies_add" ') ?> 
            <div class="row-form">
                <div class="span6">
                    <div class="span4"><?= lang('currencies_name_ar') ?><?if(name()=='name_ar'):?><font style="color:red" >*</font><?endif;?></div>
                    <div class="span8">
                        <?= form_input('name_ar', set_value("name_ar"),"class='validate[required]'") ?>
                        <?= form_error('name_ar', '<span class="bottom" style="color:red" >', '</span>'); ?>
                    </div>
                </div>
                <div class="span6">
                    <div class="span4"><?= lang('currencies_name_la') ?><?if(name()=='name_la'):?><font style="color:red" >*</font><?endif;?></div>
                    <div class="span8" >
                        <?= form_input('name_la', set_value("name_la")) ?>
                        <?= form_error('name_la', '<span class="bottom" style="color:red" >', '</span>'); ?>  
                    </div>     
                </div>
            </div>
            <div class="row-form" >
                <div class="span6" >
                  <div class="span4" ><?=lang('currencies_short_name_la')?><?if(name()=='name_la'):?><font style="color:red" >*</font><?endif;?></div>
                  <div class="span8" >
                        <?= form_input('short_name_la', set_value("short_name_la")) ?>
                        <?= form_error('short_name_la', '<span class="bottom" style="color:red" >', '</span>'); ?>
                  </div>
                </div> 
                <div class="span6" >
                   <div class="span4" ><?=lang('currencies_short_name_ar')?><?if(name()=='name_ar'):?><font style="color:red" >*</font><?endif;?></div>
                  <div class="span8" >
                       <?= form_input('short_name_ar', set_value("short_name_ar"),"class='validate[required]'") ?>
                        <?= form_error('short_name_ar', '<span class="bottom" style="color:red" >', '</span>'); ?>
                  </div>
                </div>
             </div> 
                <div class="row-form" >
                    <div class="span6" >
                      <div class="span4" >
                        <?=lang('currencies_symbol')?>
                    </div>
                    <div class="span8" >
                        <?= form_input('symbol', set_value("symbol"),"class='validate[required]'") ?>
                        <?= form_error('symbol', '<span class="bottom" style="color:red" >', '</span>'); ?>
                    </div>  
                    </div>
                    
                </div>
            <div class="toolbar bottom TAC">
            <input type="submit" name="submit" value="<?= lang('global_submit') ?>" class="btn btn-primary"> &nbsp; 
                <a href="<?= site_url('admin/currencies/index') ?>" class="btn btn-primary"  ><?= lang('global_back') ?></a>
           </div>

            <?= form_close() ?> 
        </div>
    </div>
</div> 


<script>
        $("#currencies_add").validationEngine({
prettySelect : true,
useSuffix: "_chosen",
promptPosition : "topRight:-150"
//promptPosition : "bottomLeft"
});
</script>

<!--<script type='text/javascript' src='<? //= MODULE_JS     ?>/poll/add.js'></script>-->

