<div class="widget">
    <div class="path-container Fright">
    	<div class="path-name Fright">
            <a href="<?php echo  site_url().'admin/dashboard'; 
            
            ?>"><?php echo  lang('global_system_management') ?></a>
        </div>
        <div class="path-arrow Fright"></div>
        <div class="path-name Fright">  <a href="<?=  site_url('admin/currencies/index')?>"><?= lang('parent_child_title') ?></a></div>
        <div class="path-arrow Fright"></div>
        <div class="path-name Fright">  <?= lang('edit_currency') ?></div>
    </div>
    
        
</div>

<div class="row-fluid">
    <div class="widget">
        
        
        <div class="widget-header">
        <div class="widget-header-icon Fright"> <span class="icos-pencil2"></span> </div>
        <div class="widget-header-title Fright">
            <?php echo  lang('edit_currency') ?>
        </div>
    	</div>
                                
        <div class="block-fluid">
           <?= form_open_multipart('', 'autocomplete="off" ') ?> 
            <div class="row-form">
                <div class="span6">
                    <div class="span4"><?= lang('currencies_name_ar') ?><?if(name()=='name_ar'):?><font style="color:red" >*</font><?endif;?></div>
                    <div class="span8">
                        <?= form_input('name_ar', set_value("name_ar",$item->name_ar)) ?>
                        <?= form_error('name_ar', '<span class="bottom" style="color:red" >', '</span>'); ?>
                    </div>
                </div>
                <div class="span6">
                    <div class="span4"><?= lang('currencies_name_la') ?><?if(name()=='name_la'):?><font style="color:red" >*</font><?endif;?></div>
                    <div class="span8" >
                        <?= form_input('name_la', set_value("name_la",$item->name_la)) ?>
                        <?= form_error('name_la', '<span class="bottom" style="color:red" >', '</span>'); ?>  
                    </div>     
                </div>
            </div>
            <div class="row-form" >
                <div class="span6" >
                  <div class="span4" ><?=lang('currencies_short_name_la')?><?if(name()=='name_la'):?><font style="color:red" >*</font><?endif;?></div>
                  <div class="span8" >
                        <?= form_input('short_name_la', set_value("short_name_la",$item->short_name)) ?>
                        <?= form_error('short_name_la', '<span class="bottom" style="color:red" >', '</span>'); ?>
                  </div>
                </div> 
                <div class="span6" >
                   <div class="span4" ><?=lang('currencies_short_name_ar')?><?if(name()=='name_ar'):?><font style="color:red" >*</font><?endif;?></div>
                  <div class="span8" >
                       <?= form_input('short_name_ar', set_value("short_name_ar",$item->short_name_ar)) ?>
                        <?= form_error('short_name_ar', '<span class="bottom" style="color:red" >', '</span>'); ?>
                  </div>
                </div>
             </div> 
                <div class="row-form" >
                    <div class="span6" >
                      <div class="span4" >
                        <?=lang('currencies_symbol')?>
                    </div>
                    <div class="span8" >
                        <?= form_input('symbol', set_value("symbol",$item->symbol)) ?>
                        <?= form_error('symbol', '<span class="bottom" style="color:red" >', '</span>'); ?>
                    </div>  
                    </div>
                    
                </div>
            <div class="toolbar bottom TAC">
                <button name="submit" class="btn btn-primary"><?= lang('global_submit') ?></button>
                <a href="<?= site_url('admin/currencies/index') ?>" class="btn btn-primary"  ><?= lang('global_back') ?></a>
           </div>

            <?= form_close() ?> 
        </div>
    </div>
</div> 
<!--<script type='text/javascript' src='<? //= MODULE_JS     ?>/poll/add.js'></script>-->

