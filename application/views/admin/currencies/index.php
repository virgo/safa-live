<div class="widget">
    <div class="path-container Fright">
    	<div class="path-name Fright">
            <a href="<?php echo  site_url().'admin/dashboard'; 
            
            ?>"><?php echo  lang('global_system_management') ?></a>
        </div>
        <div class="path-arrow Fright"></div>
        <div class="path-name Fright">  <?= lang('parent_child_title') ?></div>
    </div>
    
    <a href="<?= site_url("admin/currencies/add") ?>" class="btn Fleft"><?= lang('global_add') ?></a>
        
</div>

<div class='row-fluid'>
    <div class='span12'>
        <div class='widget'>
            
            
            <div class="widget-header">
        <div class="widget-header-icon Fright"> <span class="icos-pencil2"></span> </div>
        <div class="widget-header-title Fright">
            <?php echo  lang('currencies') ?>
        </div>
    	</div>
    	
            <div class='block-fluid '> 
                <table cellpadding="0" class="myTable" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th><?= lang('currencies_name_ar') ?></th>
                            <th><?= lang('currencies_name_la') ?></th>
                            <th><?= lang('currencies_symbol') ?></th>
                            <th><?= lang('global_actions') ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <? if (isset($items)): ?>
                            <? foreach ($items as $item): ?>
                                <tr>
                                    <td><?= $item->name_ar ?></td>
                                    <td><?= $item->name_la ?></td>
                                    <td><?= $item->symbol ?></td>
                                    <td class="TAC"  >
                                        <div >
                                            <a title="<?= lang('global_edit') ?>"   href="<?= site_url("admin/currencies/edit") ?>/<?= $item->erp_currency_id ?>"><span class="icon-pencil"></span></a>
                                        </div>
                                    </td>
                                </tr>
                            <? endforeach; ?>
                        <? endif; ?>
                    </tbody>
                </table>

            </div>
        </div>
        <div style="width:100%;">
            <?= $pagination ?>
        </div>
    </div>
   
