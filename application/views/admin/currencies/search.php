<div class='span12' >
    <div class='widget'>
        <div class='head dark' >
            <div class="icon"><span class="icos-search"></span></div>
            <h2><?= lang('safa_uos_search_title') ?></h2>
        </div>
        <div class='block-fluid' >
            <?= form_open('admin/safa_uos/index', 'method="get"') ?>

            <div class="row-form">
                <div class="span2"><?= lang('safa_uos_name_ar') ?></div>
                <?= form_input('name_ar', set_value("name_ar"), 'class="input-small"') ?>
                <!--<input class="input-small" name="name_ar" type="text" value="<?= set_value('name_ar') ?>" pattern="[a-zA-Z0-9_-]{2,12}" autofocus title="must be alphanumeric in 3-12 chars" >-->
                <?= form_error('name_ar'); ?>
            </div>
            <div class="row-form">
                <div class="span2"><?= lang('safa_uos_name_la') ?></div>
                <?= form_input('name_la', set_value("name_la"), 'class="input-small"') ?>
                <!--<input class="input-small" name="name_la" type="text" value="<?= set_value('name_la') ?>" pattern="[a-zA-Z0-9_-]{2,12}" autofocus title="must be alphanumeric in 3-12 chars" >-->
                <?= form_error('name_la'); ?>
            </div>
            <div class="row-form">
                <div class="span2"><?= lang('safa_uos_phone') ?></div>
                <?= form_input('phone', set_value("phone"), 'class="input-small"') ?>
                <!--<input class="input-small" name="phone" type="text" value="<?= set_value('name_la') ?>" pattern="[a-zA-Z0-9_-]{3,12}" autofocus title="must be alphanumeric in 3-12 chars" >-->
                <?= form_error('phone'); ?>
            </div>
            <div class="row-form">
                <div class="span2"><?= lang('safa_uos_fax') ?></div>
                <?= form_input('fax', set_value("fax"), 'class="input-small"') ?>
                <!--<input class="input-small" name="phone" type="text" value="<?= set_value('name_la') ?>" pattern="[a-zA-Z0-9_-]{3,12}" autofocus title="must be alphanumeric in 3-12 chars" >-->
                <?= form_error('fax'); ?>
            </div>
            <div class="row-form">
                <div class="span2"><?= lang('safa_uos_email') ?></div>
                <? //= form_input('name_ar', set_value("name_ar"), 'class="input-small"') ?>
                <input class="input-small" type="email" name="email" value="<?= set_value('email') ?>">                    <?= form_error('name_la'); ?>
            </div>

            <div class="row-form">
                <div style="text-align:center">
                    <input type="submit" name="search" value="search" class="btn btn-primary" />
                </div>
            </div>
            <?= form_close() ?>
        </div>
    </div>
</div>
