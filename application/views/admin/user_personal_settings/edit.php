
<span class="span8">

</span>
<div class="row-fluid">

    <div class="widget">
        <div class="head dark">
            <div class="icon"><i class="icos-pencil2"></i></div>
            <h2><?= lang('global_user_personal_settings') ?> <?= $item->username?></h2> 
        </div>                        
        <div class="block-fluid">
            <?= form_open_multipart() ?>

            <? if (validation_errors()): ?>
<!--                <div style="font:normal 10px tahoma">
                    <?php echo validation_errors(); ?>
                </div>   -->
            <? endif ?> 

            <div class="row-form">
                <div class="span6">
                    <div class="span4"> <?= lang('user_personal_settings_username') ?>:</div>
                    <div class="span8">
                        <?= form_input('username', set_value("username" ,$item->username),'readonly="readonly"') ?>
                        <?= form_error('username', '<div class="bottom" style="color:red" >', '</div>'); ?>
                    </div>
                </div>
                <div class="span6">
                    <div class="span4"> <?= lang('user_personal_settings_password') ?><span style="color: red">*</span>:</div>
                    <div class="span8">
                         <?= form_password('password', set_value('password'), 'class="text_input" autocomplete="off"') ?>
                         <?= form_error('check_user_info', '<div class="bottom" style="color:red" >', '</div>'); ?>   
                    </div>
                </div>
            </div>

            <div class="row-form">
                <div class="span6">
                    <div class="span4"> <?= lang('user_personal_settings_new_password') ?><span style="color: red">*</span>:</div>
                    <div class="span8">
                         <?= form_password('new_password', set_value('new_password'), 'class="text_input" autocomplete="off"') ?>
                         <?= form_error('new_password', '<div class="bottom" style="color:red" >', '</div>'); ?>
                    </div>
                </div>
                <div class="span6">
                    <div class="span4"> <?= lang('user_personal_settings_resetpassword') ?><span style="color: red">*</span>:</div>
                    <div class="span8">
                         <?= form_password('passconf', set_value('passconf'), ' ') ?>
                        <?= form_error('passconf', '<div class="bottom" style="color:red" >', '</div>'); ?>
                    </div>
                </div>
            </div>

            <div class="toolbar bottom TAC"><input type="submit" name="submit" value="<?= lang('global_submit') ?>" class="btn btn-primary"> &nbsp;
                
            </div>

            <?= form_close() ?> 
        </div>
    </div>
</div> 


