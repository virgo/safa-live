
<span class="span8">

</span>
<div class="row-fluid">
    <div style="margin-bottom:10px; text-align:left; border:3px solid #FFF; background:url(<?= IMAGES ?>/backgrounds/bg_box_head.jpg); padding:5px 10px; height:30px;">

        <div style="float:right; text-align:right; font-size:14px; line-height:30px; color:#333;"> <?= lang('global_system_management') ?> <span style="color:#80693d"><img src="<?= IMAGES ?>/jbreadcrumb/Chevron-ar.gif" width="5" height="20"></span> <a href="<?= site_url('admin/safa_tripstatus/index') ?>"><?= lang('menu_techn_flight_cases') ?></a> <span style="color:#80693d"><img src="<?= IMAGES ?>/jbreadcrumb/Chevron-ar.gif" width="5" height="20"></span> <?= lang('safa_tripstatus_title_edit') ?>  <? if (lang('global_lang') == 'ar'): ?><?= $item->name_ar ?><? else: ?><?= $item->name_la ?><? endif ?></div>
    </div>
    <div class="widget">
        <div class="head dark">
            <div class="icon"><i class="icos-pencil2"></i></div>
            <h2><?= lang('safa_tripstatus_title_edit')?> <?= $item->{name()} ?> </h2>
        </div>                        
        <div class="block-fluid">
            <?= form_open_multipart() ?>

            <div class="row-form">
                <div class="span6">
                    <div class="span4"><?= lang('safa_tripstatus_name_ar') ?>
                        <span style="color: red">*</span>:
                    </div>
                    <div class="span8"><?= form_input('name_ar', set_value("name_ar",$item->name_ar), "  ") ?>
                    <?= form_error('name_ar', '<div class="bottom" style="color:red" >', '</div>'); ?>                    
                    </div>
                </div>
                <div class="span6">
                    <div class="span4"><?= lang('safa_tripstatus_name_la') ?>
                       <span style="color: red">*</span>:
                    </div>
                    <div class="span8"><?= form_input('name_la', set_value("name_la",$item->name_la), " ") ?>
                    <?= form_error('name_la', '<div class="bottom" style="color:red" >', '</div>'); ?>                    
                    </div>
                </div>
            </div>

            <div class="row-form">
                <div class="span6">
                    <div class="span4"><?= lang('safa_tripstatus_code') ?>
                        <span style="color: red">*</span>:
                    </div>
                    <div class="span8"><?= form_input('code', set_value("code",$item->code), "  ") ?>
                        <?= form_error('code', '<div class="bottom" style="color:red" >', '</div>'); ?>                    
                    </div>
                </div>
            </div>

            <div class="row-form">
                <div class="span6">
                    <div class="span4"><?= lang('safa_tripstatus_description_ar') ?>
                    </div>
                    <div class="span8"><?= form_textarea('description_ar', set_value("description_ar",$item->description_ar), " ") ?>
                    <?= form_error('description_ar', '<div class="bottom" style="color:red" >', '</div>'); ?>                    
                    </div>
                </div>
                <div class="span6">
                    <div class="span4"><?= lang('safa_tripstatus_description_la') ?></div>
                    <div class="span8"><?= form_textarea('description_la', set_value("description_la",$item->description_la), " ") ?>
                      <?= form_error('description_la', '<div class="bottom" style="color:red" >', '</div>'); ?>                      
                    </div>
                </div>
            </div>

            <div class="toolbar bottom TAC"><input type="submit" name="submit" value="<?= lang('global_submit') ?>" class="btn btn-primary"> &nbsp;
                <input  type ="button" value="<?= lang('global_back') ?>"class="btn btn-primary" onclick="window.location = '<?= site_url('admin/safa_tripstatus/index') ?>'">
            </div>

            <?= form_close() ?> 
        </div>
    </div>
</div> 
<!--<script type='text/javascript' src='<? //= MODULE_JS            ?>/poll/add.js'></script>-->
