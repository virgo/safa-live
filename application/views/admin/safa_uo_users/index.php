<div class=" row-fluid" >
    
    
    <div class="widget">
    <div class="path-container Fright">
        <div class="path-name Fright">
        <a href="<?php echo   site_url('admin/dashboard')?>"> <?php echo  lang('global_system_management') ?></a>
        </div>
        <div class="path-arrow Fright">
        </div>
        <div class="path-name Fright">
            <a href="<?= site_url('admin/safa_uos') ?>"><?= lang('parent_child_title') ?></a>
        </div>
        <div class="path-arrow Fright">
        </div>
        <div class="path-name Fright">
            <?= lang('child_node_title_ou_users') ?>
        </div>
        
        
    </div>
    <a href="<?= site_url("admin/safa_uos/users_add/".$safa_uo_id) ?>" class="btn Fleft"><?= lang('global_add_new_record') ?></a>
 
</div>
    
    
</div>


<div class='row-fluid'>
    <div class='span12'>
        <div class='widget'>
            
            
            
             <div class="widget-header">
	        <div class="widget-header-icon Fright"> <span class="icos-pencil2"></span> </div>
	        <div class="widget-header-title Fright">
	            <?= lang('safa_uos_users_title') ?> :: <?= item('safa_uos',name(),array('safa_uo_id'=>$safa_uo_id))?>
	        </div>
	        
	    	</div>
            
            <div class='block-fluid' >
                <table class="myTable" width="100%" >
                    <thead>
                        <tr>                     
                            <th><?= lang('safa_uos_th_name') ?></th>
                            <th><?= lang('safa_uos_th_username') ?></th>
                            <th><?= lang('safa_uos_th_usergroup') ?></th>
                            <th><?= lang('global_actions') ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <? if (isset($items)): ?>
                            <? foreach ($items as $item): ?>
                                <tr>
                                    <td><? if (name() == 'name_ar'): ?><?= $item->name_ar ?><? else: ?><?= $item->name_la ?><? endif; ?></td>
                                    <td><?= $item->username ?></td>
                                    <? if (lang('global_lang') == 'ar'): ?>
                                    <td><?= $item->usergroup_name_ar ?></td>
                                    <?else:?>
                                    <td><?= $item->usergroup_name_la ?></td>
                                    <?  endif;?>
                                    <td class="TAC">
                                        <a href="<?= site_url("admin/safa_uos/users_edit") ?>/<?= $item->safa_uo_user_id ?>/<?= $this->uri->segment("4"); ?>"><span class="icon-pencil"></span></a>
                                    </td>
                                </tr>
                            <? endforeach; ?>
                        <? endif; ?>
                    </tbody>
                </table>
            </div>
        </div>
        <?= $pagination ?>
        <div class="row-fluid" >
            <div class="span12">
                <div class="bottom TAC" >
                    <a href="<?= site_url('admin/safa_uos') ?>" class="btn btn-primary" ><?= lang('global_back') ?></a>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
                                            $(document).ready(function() {
                                                $(".myTable").dataTable(
                                                        {bSort: true,
                                                            bAutoWidth: true,
                                                            "iDisplayLength": false, // can be removed for basic 10 items per page
                                                            "sPaginationType": false,
                                                            "bPaginate": false,
                                                            "bInfo": false}
                                                );
                                            });
</script>
