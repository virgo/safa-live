<div class="row-fluid" >
    
    <div class="widget">
    <div class="path-container Fright">
        <div class="path-name Fright">
        <a href="<?php echo   site_url('admin/dashboard')?>"> <?php echo  lang('global_system_management') ?></a>
        </div>
        <div class="path-arrow Fright">
        </div>
        <div class="path-name Fright">
            <a href="<?= site_url('admin/safa_uos') ?>"><?= lang('parent_child_title') ?></a>
        </div>
        <div class="path-arrow Fright">
        </div>
        <div class="path-name Fright">
            <a href="<?= site_url('admin/safa_uos/users_index/' . $safa_uo_id) ?>" ><?= lang('child_node_title_ou_users') ?></a>
        </div>
        <div class="path-arrow Fright">
        </div>
        <div class="path-name Fright">
            <?= lang('safa_uos_users_add_title') ?>
        </div>
        
        
    </div> 
</div>
    
    
</div>
<div class="row-fluid">
    <div class="span12" >
        <div class="widget">
            
            <div class="widget-header">
	        <div class="widget-header-icon Fright"> <span class="icos-pencil2"></span> </div>
	        <div class="widget-header-title Fright">
	            <?= lang('safa_uos_users_add_title') ?> :: <?= item('safa_uos', name(), array('safa_uo_id' => $safa_uo_id)) ?>
	        </div>
	    	</div>
            
            
                                   
            <div class="block-fluid">
                <?= form_open() ?>

                <div class="row-form">
                    <div class="span6">
                        <div class="span4"><?= lang('safa_uo_users_name_ar') ?><? if (name() == 'name_ar'): ?><font style="color:red" >*</font><? endif; ?></div>
                        <div class="span8" >
                            <?= form_input('name_ar', set_value("name_ar")) ?>
                            <span class="bottom" style="color:red" >
                                <?= form_error('name_ar') ?>
                            </span>
                        </div>
                    </div>
                    <div class="span6">
                        <div class="span4"><?= lang('safa_uo_users_name_la') ?><? if (name() == 'name_la'): ?><font style="color:red" >*</font><? endif; ?></div>
                        <div class="span8" >
                            <?= form_input('name_la', set_value("name_la")) ?>
                            <span class="bottom" style="color:red" ><?= form_error('name_la') ?></span> 
                        </div>    
                    </div>
                </div>

                <div class="row-form">
                    <div class="span6">
                        <div class="span4"><?= lang('safa_uos_user_username') ?><font style="color:red" >*</font></div>
                        <div class="span8" >
                            <?= form_input('username', set_value('username')) ?>
                            <span class="bottom" style="color:red" ><?= form_error('username') ?></span> 
                        </div>   
                    </div>
                    <div class="span6" >
                        <div class="span4"><?= lang('safa_uo_users_usersgroup') ?><font style="color:red" >*</font></div>
                        <div class="span8">
                            <?= form_dropdown('safa_uo_usergroup_id', ddgen('safa_uo_usergroups', array('safa_uo_usergroup_id', name())), set_value('safa_uo_usergroup_id')) ?>
                            <span class="bottom" style="color:red">
                                <?= form_error('safa_uo_usergroup_id') ?>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="row-form" >
                    <div class="span6" >
                        <div class="span4"><?= lang('safa_uo_users_email') ?></div>
                        <div class="span8" >
                            <?= form_input('uo_user_email',set_value('uo_user_email')) ?>
                            <span class="bottom" style="color:red">
                                <?= form_error('uo_user_email') ?>
                            </span>

                        </div>
                    </div>
                    <div class="span6" >
                        <div class="span4"><?= lang('safa_uo_users_mobile') ?></div>
                        <div class="span8" >
                            <?= form_input('uo_user_mobile', set_value('uo_user_mobile')) ?>
                            <span class="bottom" style="color:red">
                                <?= form_error('uo_user_mobile') ?>
                            </span>

                        </div>
                    </div>
                </div>

                <div class="row-form">
                    <div class="span6">
                        <div class="span4"><?= lang('safa_uos_user_password') ?><font style="color:red" >*</font></div>
                        <div class="span8" >
                            <?= form_password('password', set_value('password')) ?>
                            <span class="bottom" style="color:red">
                                <?= form_error('password') ?>
                            </span>
                        </div>
                    </div>
                    <div class="span6">
                        <div class="span4"><?= lang('safa_uos_user_repeat_password') ?><font style="color:red" >*</font></div>
                        <div class="span8">
                            <?= form_password('passconf', set_value('passconf')) ?>
                            <span class="bottom" style="color:red" >
                                <?= form_error('passconf') ?>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="toolbar bottom TAC">
                    <a  class="btn btn-primary" href="<?= site_url('admin/safa_uos/users_index') ?>/<?= $this->uri->segment("4"); ?>" ><?= lang('global_back') ?></a>
                    <button class="btn btn-primary"><?= lang('global_submit') ?></button>
                </div>

                <?= form_close() ?> 
            </div>
        </div>  
    </div>
</div>  

