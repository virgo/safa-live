<div class="span12">
    <div class="widget">
        <div class="path-container Fright">
            <div class="path-name Fright">
                <a href=""><?= lang('global_system_management') ?></a>
            </div>
            <div class="path-arrow Fright">
            </div>
            <div class="path-name Fright">
                <a href="<?= site_url('admin/port_halls/index') ?>"><?= lang('menu_main_hole_airports') ?></a>
            </div>
            <div class='path-arrow Fright'>
            </div>
            <div class="path-name Fright">
                <?= lang('port_halls_edit_title') ?> <?= $items->{name() } ?>
            </div>
        </div>
    </div>
</div>

<div class="widget">
    <div class="widget-header">
        <div class="widget-header-icon Fright">
            <span class="icos-pencil2"></span>
        </div>

        <div class="widget-header-title Fright">
            <?= lang('port_halls_edit_title') ?> <?= $items->{name()} ?> 
        </div>
    </div>  

    <div class="widget-container">                
        <div class="block-fluid">
            <?= form_open_multipart() ?>

            <? if (validation_errors()): ?>
                <!--                <div style="font:normal 10px tahoma">
                <?php echo validation_errors(); ?>
                                </div>   -->
            <? endif ?> 
            <div class="row-form">
                <div class="span6">
                    <div class="span4 TAL Pleft10"><?= lang('port_halls_erp_port_id') ?>
                        <span style="color: red">*</span>:
                    </div>
                    <div class="span8">
                        <?= form_dropdown('erp_port_id', $erp_ports, set_value('erp_port_id', $items->erp_port_id), " name='s_example' id='erp_port_id' class='select' style='width:93%;direction:rtl' ") ?>
                        <?= form_error('erp_port_id', '<div class="bottom" style="color:red" >', '</div>'); ?>
                    </div>
                </div>
                <div id="chose"class="span6">
                    <div>
                        <span>    <?= lang('port_halls_port_id_message') ?>  </span>
                    </div>
                </div>
            </div>


            <div class="row-form">
                <div class="span6">
                    <div class="span4 TAL Pleft10"> <?= lang('port_halls_name_ar') ?><span style="color: red">*</span>:</div>
                    <div class="span8">
                        <?= form_input('name_ar', set_value("name_ar", $items->name_ar),"class='input-huge'") ?>
                        <?= form_error('name_ar', '<div class="bottom" style="color:red" >', '</div>'); ?>
                    </div>
                </div>
                <div class="span6">
                    <div class="span4 TAL Pleft10"><?= lang('port_halls_name_la') ?><span style="color: red">*</span>:</div>
                    <div class="span8">
                        <?= form_input('name_la', set_value("name_la", $items->name_la),"class='input-huge'") ?>
                        <?= form_error('name_la', '<div class="bottom" style="color:red" >', '</div>'); ?>
                    </div>
                </div>
            </div>

            <div class="row-form">
                <div class="span6">
                    <div class="span4 TAL Pleft10"> <?= lang('port_halls_code') ?><span style="color: red">*</span>:</div>
                    <div class="span8">
                        <?= form_input('code', set_value("code", $items->code),"class='input-huge' ") ?>
                        <?= form_error('code', '<div class="bottom" style="color:red" >', '</div>'); ?>
                    </div>
                </div>
            </div>

            <div class="toolbar bottom TAC">
                <button name="submit" class="btn btn-primary"><?= lang('global_submit') ?></button>
                <input type ="button" value="<?= lang('global_back') ?>"class="btn btn-primary" onclick="window.location = '<?= site_url('admin/port_halls/index') ?>'">
            </div>

            <?= form_close() ?> 
        </div>
    </div>
</div>



