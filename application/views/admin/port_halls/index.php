
<!-- By Gouda, For popup -->
<script type="text/javascript" src='<?= CSS_JS ?>/form/form.js'></script>
<link rel="stylesheet" href="<?= CSS ?>/bootstrap/bootstrap.min.new.css"/>
     
<div class="row-fluid nomargin">

   
<div class="widget">
    <div class="path-container Fright">
    	<div class="path-name Fright">
            <a href="<?php echo  site_url().'admin/dashboard'; 
            
            ?>"><?php echo  lang('global_system_management') ?></a>
        </div>
        <div class="path-arrow Fright"></div>
        <div class="path-name Fright">  <?= lang('menu_main_hole_airports') ?></div>
    </div>
    
    <a href="<?= site_url("admin/port_halls/add") ?>" class="btn Fleft"><?= lang('global_add') ?></a>    
</div>


<div class="widget">
<style>
    .updated_msg{
        display:none;
        background-color:#ccee97;
        font-weight: bold;
        text-align: center;
        border:3px solid #cccdc9; 
        padding:20px;  
        margin-bottom:10px;
        border-radius:15px;
        margin:10px; 
    }
</style>
<div class='row-fluid' align='center' >
    <div  class='updated_msg' >
        <br><input  type ="button" value="<?=lang('global_submit')?>"class="btn btn-primary" id='ok' >
    </div> 
 </div>
        
        
         <div class="widget-header">
        <div class="widget-header-icon Fright"> <span class="icos-pencil2"></span> </div>
        <div class="widget-header-title Fright">
            <?php echo  lang('menu_main_hole_airports') ?>
        </div>
    	</div>
        
                       
        <div class="block-fluid">
            <table cellpadding="0" class="fpTable" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th><?= lang('port_halls_name_ar') ?></th>
                        <th><?= lang('port_halls_name_la') ?></th>
                        <th><?= lang('port_halls_code')?></th>
                        <th><?= lang('port_halls_erp_port_id')?></th>
                        <th><?= lang('global_actions')?></th>
                    </tr>
                </thead>
                <tbody>
                    <? if (isset($items)): ?>
                        <? foreach ($items as $item): ?>
                          <tr id="row_<?php echo $item->erp_port_hall_id;?>">
                                <td><?= $item->arabic_name ?></td>
                                <td><?= $item->english_name ?></td>
                                <td><?= $item->code?></td>
                                <td><? if (lang('global_lang') == 'ar'): ?><?= $item->port_name ?><? else: ?><?= $item->port_english_name ?><? endif ?></td>
                                
                        <td class="TAC">
                          <a href="<?= site_url("admin/port_halls/edit") ?>/<?= $item->erp_port_hall_id ?>"><span class="icon-pencil"></span></a>
                           <? if($this->port_halls_model->check_delete_ability($item->erp_port_hall_id)==0):?> 
                               <? if (lang('global_lang') == 'ar'): ?>
                                <a href="#fModal" name="<?= $item->arabic_name?>"  id="<?= $item->erp_port_hall_id?>" class=" delete_item" data-toggle="modal">
                             <? else: ?>
                                <a href="#fModal" name="<?= $item->english_name ?>"  id="<?= $item->erp_port_hall_id ?>" class=" delete_item" data-toggle="modal">
                             <? endif ?>         
                               <span class="icon-trash"></span></a>
                          <? else:?><?endif?>
                        </td>
                            </tr>

                        <? endforeach; ?>
                    <? endif; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

<?= $pagination ?>
<div id="fModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="myModalLabel"><?= lang('global_system_message') ?></h3>
    </div>        
    <div class="row-fluid">
        <div id="msg" class="row-form">
            <?=  lang('global_are_you_sure_you_want_to_delete')?>
        </div>
    </div>                   
    <div class="modal-footer">
        <button class="btn btn-primary" data-dismiss="modal" aria-hidden="true"id="yes"><?= lang('global_yes')?></button>
        <button class="btn btn-primary" data-dismiss="modal" aria-hidden="true"id="no"><?= lang('global_no')?></button>       </div>
</div>
    <script type='text/javascript' src='<?= JS ?>/custom/table_operation.js'></script>
    <script>
        check_list_delete_submit("delete_all", "table", "form_delete", '<?= lang('global_are_you_sure_you_want_to_delete') ?>', "deleting_error");
    </script>

        <!-- hide and show -->
  <script type="text/javascript">
    $(document).ready(function(){
        $('.delete_item').click(function(){
                        var name= $(this).attr('name');
                        var erp_port_hall_id= $(this).attr('id');
                        <? if (lang('global_lang') == 'ar'): ?>
                            $('#msg').text("<?= lang('global_are_you_sure_you_want_to_delete') ?>"+" "+name)
                        <? else: ?>
                            $('#msg').text(name+" "+"<?= lang('global_are_you_sure_you_want_to_delete') ?>")
                        <?  endif;?>    
			 $('#yes').click(function(){
                         var answer =$(this).attr('id');
                           if(answer==='yes'){
				$.ajax({
					type: "POST",
					dataType: 'json',
					url: "<?php echo site_url('admin/port_halls/delete');?>",
					data: {'erp_port_hall_id':erp_port_hall_id},
					success: function(msg){
                                         if(msg.response==true){
                                            var del = erp_port_hall_id;
                                            $("#row_"+del).remove();
                                             <? if (lang('global_lang') == 'ar'): ?>
                                                $('.updated_msg').text("<?=lang('global_delete_confirm')?>"+" "+name);
                                                $('.updated_msg').append('<br>');
                                                $('.updated_msg').append('<input  type ="button" value="<?=lang('global_submit')?>"class="btn btn-primary" id="ok" >');
                                             <? else: ?>
                                                $('.updated_msg').text(name+" "+"<?=lang('global_delete_confirm')?>");
                                                $('.updated_msg').append('<br>');
                                                $('.updated_msg').append('<input  type ="button" value="<?=lang('global_submit')?>"class="btn btn-primary" id="ok" >');
                                             <?  endif;?>
                                              $('.updated_msg').show();
                                              $('#ok').click(function(){
                                                 $('.updated_msg').hide();
                                               });
				          }else if(msg.response==false){
                                                    $('.updated_msg').text(msg.msg);
                                                    $('.updated_msg').append('<br>');
                                                    $('.updated_msg').append('<input  type ="button" value="<?=lang('global_submit')?>"class="btn btn-primary" id="ok" >');
                                                    $('.updated_msg').show();
                                                    $('#ok').click(function(){
                                                       $('.updated_msg').hide();
                                                       
                                                    });
				          }
					}
				});
			}else{
				return FALSE;
			}
                      });
		});
	});
	</script>