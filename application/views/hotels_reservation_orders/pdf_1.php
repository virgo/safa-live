<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html dir="rtl">
    <head>
        <title><?php echo lang('hotel_reservation_order') ?></title>
        <meta  http-equiv="Content-Type" content="text/html; charset=utf-8" >
        <meta name="viewport" content="width=device-width">

        <style>
            h1 {
                color: #444;
                font-family: arial;
                font-size: 18pt;
                font-weight: bold;

            }
            p.first {
                color: #003300;
                font-family: arial;
                font-size: 12pt;
            }
            p.first span {
                color: #006600;
                font-style: italic;
            }
            p#second {
                color: rgb(00,63,127);
                font-family: times;
                font-size: 12pt;
                text-align: justify;
            }
            p#second > span {
                background-color: #FFFFAA;
            }
            table.first {
                /*
                color: #003300;
                */
                font-family: arial;
                font-weight:normal;
                font-size: 11pt;

                border-right: 1px solid gray;
                /*
                border-bottom: 1px solid green;
                */
                border-top: 1px solid gray;
                background-color: #EEE;

                border-elft: 1px solid gray;
            }


            td {
                /*
                border: 2px solid blue;
                background-color: #ffffee;
                
                */

                height:30px;
            }
            td.second {
                border: 2px dashed green;
            }
            div.test {
                color: #CC0000;
                background-color: #FFFF66;
                font-family: dejavusans;
                font-size: 10pt;
                border-style: solid solid solid solid;
                border-width: 2px 2px 2px 2px;
                border-color: green #FF00FF blue red;
                text-align: center;
            }
        </style>
    </head>

    <div style="width: 100%; text-align: center"  dir="rtl">
        <h1 ><?php echo lang('title') ?></h1>

        <table align="center" border="0" cellspacing="0" class="first" width="100%">
            <tr><td></td></tr>
            <tr ><td width="100px">
                    <?php echo lang('order_no'); ?>  </td><td width="80px" style="font-size: 13px"> <?php echo $current_order_no; ?> 

                </td><td width="100px"></td><td width="100px">
                    <?php echo lang('order_date'); ?> </td><td width="80px" style="font-size: 13px"> <?php echo $item->order_date; ?> 

                </td></tr>
            <tr><td width="100px">
                    <?php echo lang('company_type'); ?>  </td><td width="80px" style="font-size: 13px"> <?php
                    if ($item->erp_company_types_id == 3) {
                        echo lang('ea');
                    } else if ($item->erp_company_types_id == 5) {
                        echo lang('hm');
                    } else {
                        echo lang('uo');
                    }
                    ?> 

                </td><td width="100px"></td><td width="100px">
                    <?php echo lang('company'); ?>  </td><td width="80px" style="font-size: 13px">  <?php
                    if ($item->erp_company_types_id == 3) {
                        if (name() == 'name_la') {
                            echo $item->ea_name_la;
                        } else {
                            echo $item->ea_name_ar;
                        }
                    } else if ($item->erp_company_types_id == 5) {
                        if (name() == 'name_la') {
                            echo $item->hm_name_la;
                        } else {
                            echo $item->hm_name_ar;
                        }
                    } else {
                        if (name() == 'name_la') {
                            echo $item->uo_name_la;
                        } else {
                            echo $item->uo_name_ar;
                        }
                    }
                    ?> 

                </td></tr>
            <tr><td width="100px">
                    <?php echo lang('city'); ?>  </td><td width="80px" style="font-size: 13px">  <?php echo $item->city_name; ?> 

                </td><td width="100px"></td><td width="100px">
                    <?php echo lang('hotel'); ?>  </td><td width="80px" style="font-size: 13px">  <?php
                    if (name() == 'name_la') {
                        echo $item->hotel_name_la;
                    } else {
                        echo $item->hotel_name_ar;
                    }
                    ?> 

                </td></tr>
            <tr>

                <td width="100px">

<?php echo lang('alternative_hotel'); ?> </td><td width="80px"> <?php echo $item->alternative_hotels_name; ?> 
                    <br/>

                </td>

                <td width="100px"></td>

                <td width="100px">
                    <?php echo lang('arrival_date'); ?> </td><td width="80px" style="font-size: 10px"> <?php echo $item->arrival_date; ?> 

                </td></tr>
            <tr><td width="100px" >
                    <?php echo lang('departure_date'); ?> </td><td width="80px" style="font-size: 10px"> <?php echo $item->departure_date; ?> 

                </td><td width="100px"></td><td width="100px">
<?php if (session('ea_id')) { ?>
                        <?php echo lang('safa_uo_contract_id'); ?> </td><td width="80px" style="font-size: 13px"> <?php echo $item->safa_uo_contract_name; ?> 

<?php } ?>
                </td></tr>
            <tr><td width="100px">
<?php echo lang('contact_person'); ?> </td><td width="80px" style="font-size: 13px;"> <?php echo $item->contact_person; ?> 

                </td><td width="100px"></td><td width="80px">
                </td></tr>
        </table>
        <br/><br/>

        <table align="center" border="0" cellspacing="0" class="first" width="100%">
            <tr><td></td></tr>
            <tr ><td width="100px">
                    <?php echo lang('distance_from'); ?>  </td><td width="120px" style="font-size: 13px"> <?php echo $item->distance_from; ?> <?php echo lang('distance_to') ?> <?php echo $item->distance_to; ?> 

                </td><td width="100px"></td><td width="100px">
                    <?php echo lang('look_to_haram'); ?> </td><td width="80px" style="font-size: 13px"> 

                    <?php
                    if ($item->look_to_haram == 1) {
                        echo lang('yes');
                    } else {
                        echo lang('no');
                    }
                    ?>


                </td></tr>

            <tr ><td width="100px">
                    <?php echo lang('nationalities'); ?>  </td><td colspan="4" style="font-size: 13px">

                    <?php
                    foreach ($item_nationalities as $item_nationality) {
                        echo $item_nationality->erp_nationality_name . ' - ';
                    }
                    ?> 

                </td></tr>

            <tr ><td width="100px">

                    <?php echo lang('inclusive_services'); ?> </td><td colspan="4" style="font-size: 13px"> 

                    <?php
                    foreach ($item_room_services as $item_room_service) {
                        echo $item_room_service->erp_room_service_name . ' - ';
                    }
                    ?> 
                </td></tr>


            <tr ><td width="100px">
                    <?php echo lang('advantages'); ?>  </td><td colspan="4" style="font-size: 13px"> 

                    <?php
                    foreach ($item_advantages as $item_advantage) {
                        echo $item_advantage->erp_hotel_advantage_name . ' - ';
                    }
                    ?> 

                </td></tr>



        </table>
        <br/><br/>

        <div style="overflow: hidden; border: 1px solid #DDD;  border-radius: 5px; padding: 10px; margin: 20px 0;">
            <h3 > <?php echo lang('rooms') ?> </h3>

            <table cellspacing="0" cellpadding="0" style="width:100%; float:right; direction: rtl; border: 0px; position: relative; padding: 0px; margin: 0px; border: 1px solid #DDD;">
                <thead>

                    <tr>
                        <th  style="background-color: #eee; color: #333; font-weight: bold; border-left: 1px solid #DDDDDD; box-shadow: 1px 1px 0 #FFFFFF inset; font-weight: 300 !important; padding: 6px; font-size: 11px; text-shadow: 1px 1px 0 #FFFFFF; text-align: center;"><?php echo lang('housing_type') ?></th>
                        <th  style="background-color: #eee; color: #333; font-weight: bold; border-left: 1px solid #DDDDDD; box-shadow: 1px 1px 0 #FFFFFF inset; font-weight: 300 !important; padding: 6px; font-size: 11px; text-shadow: 1px 1px 0 #FFFFFF; text-align: center;"><?php echo lang('room_type') ?></th>
                        <th  style="background-color: #eee; color: #333; font-weight: bold; border-left: 1px solid #DDDDDD; box-shadow: 1px 1px 0 #FFFFFF inset; font-weight: 300 !important; padding: 6px; font-size: 11px; text-shadow: 1px 1px 0 #FFFFFF; text-align: center;"><?php echo lang('count') ?></th>
                        <th  style="background-color: #eee; color: #333; font-weight: bold; border-left: 1px solid #DDDDDD; box-shadow: 1px 1px 0 #FFFFFF inset; font-weight: 300 !important; padding: 6px; font-size: 11px; text-shadow: 1px 1px 0 #FFFFFF; text-align: center;"><?php echo lang('price_from') ?></th>
                        <th  style="background-color: #eee; color: #333; font-weight: bold; border-left: 1px solid #DDDDDD; box-shadow: 1px 1px 0 #FFFFFF inset; font-weight: 300 !important; padding: 6px; font-size: 11px; text-shadow: 1px 1px 0 #FFFFFF; text-align: center;"><?php echo lang('price_to') ?></th>
                        <th  style="background-color: #eee; color: #333; font-weight: bold; border-left: 1px solid #DDDDDD; box-shadow: 1px 1px 0 #FFFFFF inset; font-weight: 300 !important; padding: 6px; font-size: 11px; text-shadow: 1px 1px 0 #FFFFFF; text-align: center;"><?php echo lang('currency') ?></th>
                        <th  style="background-color: #eee; color: #333; font-weight: bold; border-left: 1px solid #DDDDDD; box-shadow: 1px 1px 0 #FFFFFF inset; font-weight: 300 !important; padding: 6px; font-size: 11px; text-shadow: 1px 1px 0 #FFFFFF; text-align: center;"><?php echo lang('nights_count') ?></th>
                        <th  style="background-color: #eee; color: #333; font-weight: bold; border-left: 1px solid #DDDDDD; box-shadow: 1px 1px 0 #FFFFFF inset; font-weight: 300 !important; padding: 6px; font-size: 11px; text-shadow: 1px 1px 0 #FFFFFF; text-align: center;"><?php echo lang('entry_date') ?></th>
                        <th  style="background-color: #eee; color: #333; font-weight: bold; border-left: 1px solid #DDDDDD; box-shadow: 1px 1px 0 #FFFFFF inset; font-weight: 300 !important; padding: 6px; font-size: 11px; text-shadow: 1px 1px 0 #FFFFFF; text-align: center;"><?php echo lang('exit_date') ?></th>
                        <th  style="background-color: #eee; color: #333; font-weight: bold; border-left: 1px solid #DDDDDD; box-shadow: 1px 1px 0 #FFFFFF inset; font-weight: 300 !important; padding: 6px; font-size: 11px; text-shadow: 1px 1px 0 #FFFFFF; text-align: center;"><?php echo lang('meal') ?></th>
                    </tr>


                </thead>
                <tbody>
<? if (isset($item_rooms)) { ?>
    <? if (check_array($item_rooms)) { ?>
                                    <? foreach ($item_rooms as $item_room) { ?>
                                <tr rel="<?php echo $item_room->erp_hotels_reservation_orders_rooms_id ?>">

                                    <td style="border-left: 1px solid #DDDDDD; border-top: 1px solid #DDDDDD; text-align: center;" > <?php echo $item_room->erp_housingtypes_name; ?>  </td>
                                    <td style="border-left: 1px solid #DDDDDD; border-top: 1px solid #DDDDDD; text-align: center;" >  <?php
                            if (name() == 'name_la') {
                                echo $item_room->hotelroomsize_name_la;
                            } else {
                                echo $item_room->hotelroomsize_name_ar;
                            }
                            ?>   </td>
                                    <td style="border-left: 1px solid #DDDDDD; border-top: 1px solid #DDDDDD; text-align: center;" > <?php echo $item_room->rooms_count; ?>  </td>
                                    <td style="border-left: 1px solid #DDDDDD; border-top: 1px solid #DDDDDD; text-align: center;" > <?php echo $item_room->price_from; ?>  </td>
                                    <td style="border-left: 1px solid #DDDDDD; border-top: 1px solid #DDDDDD; text-align: center;" > <?php echo $item_room->price_to; ?>  </td>



                                    <td style="border-left: 1px solid #DDDDDD; border-top: 1px solid #DDDDDD; text-align: center;" > <?php echo ''; ?>  </td>
                                    <td style="border-left: 1px solid #DDDDDD; border-top: 1px solid #DDDDDD; text-align: center;" > <?php echo $item_room->nights_count; ?>  </td>

                                    <td style="border-left: 1px solid #DDDDDD; font-size: 9px; border-top: 1px solid #DDDDDD; text-align: center;" > <?php echo $item_room->entry_date; ?>  </td>
                                    <td style="border-left: 1px solid #DDDDDD; font-size: 9px; border-top: 1px solid #DDDDDD; text-align: center;" > <?php echo $item_room->exit_date; ?>  </td>
                                    <td style="border-left: 1px solid #DDDDDD; border-top: 1px solid #DDDDDD; text-align: center;" > <?php echo $item_room->erp_meal_name; ?>  </td>


                                </tr>
        <? } ?>
    <? } ?>
<? } ?>
                </tbody>
            </table>

        </div>


        <div style="overflow: hidden; border: 1px solid #DDD;  border-radius: 5px; padding: 10px; margin: 20px 0;">
            <h3 ><?php echo lang('meals') ?></h3>

            <table  cellspacing="0" cellpadding="0" style="width:100%; float:right; direction: rtl; border: 0px; position: relative; padding: 0px; margin: 0px; border: 1px solid #DDD; font-family: dejavusans; font-weight:normal; font-size: 12pt;">
                <thead>

                    <tr>
                        <th style="background-color: #eee; color: #333; font-weight: bold; border-left: 1px solid #DDDDDD; box-shadow: 1px 1px 0 #FFFFFF inset; font-weight: 300 !important; padding: 6px; font-size: 11px; text-shadow: 1px 1px 0 #FFFFFF; text-align: center;"> <?php echo lang('meal') ?></th>
                        <th style="background-color: #eee; color: #333; font-weight: bold; border-left: 1px solid #DDDDDD; box-shadow: 1px 1px 0 #FFFFFF inset; font-weight: 300 !important; padding: 6px; font-size: 11px; text-shadow: 1px 1px 0 #FFFFFF; text-align: center;"><?php echo lang('count') ?></th>


                    </tr>
                </thead>
                <tbody>

<? if (isset($item_meals)) { ?>
    <? if (check_array($item_meals)) { ?>
        <? foreach ($item_meals as $item_meal) { ?>
                                <tr >

                                    <td style="border-left: 1px solid #DDDDDD; border-top: 1px solid #DDDDDD; text-align: center;"> <?php echo $item_meal->erp_meal_name; ?>  </td>
                                    <td style="border-left: 1px solid #DDDDDD; border-top: 1px solid #DDDDDD; text-align: center;"> <?php echo $item_meal->meals_count; ?>  </td>


                                </tr>
        <? } ?>
    <? } ?>
<? } ?>

                </tbody>
            </table>
        </div>


    </div>