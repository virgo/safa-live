<style>
    .lable_w {
        width: auto;
        color: firebrick;
        font-size: 16px;
        background: none repeat scroll 0 0 #F7F4B5;
        border: 1px solid #D5D6D6;
        border-radius: 49px;
        display: block;
        margin: 10px 10px -5px;
        padding: 10px 12px 8px;
    }
    .coll_open {
        cursor:pointer;
    }
    .coll_close {
        cursor:pointer;
    }
    .green, .green:focus{
        padding: auto;
        background: yellowgreen;
        /*background: -webkit-linear-gradient(top, green 0%, yellowgreen 100%);*/
        background: -moz-linear-gradient(top, green 0%, yellowgreen 100%);

    }
    .red, .red:focus{
        padding: auto;
        /*        background: red;
                background: -webkit-linear-gradient(top, pink 0%, red 100%);
                background: -moz-linear-gradient(top, pink 0%, red 100%);*/
    }
    .icon-ok {
        color: green;
    }
    .icon-remove {
        color: red;
    }
    .coll_open {
        cursor: pointer;
    }
    .coll_close {
        cursor: pointer;
    }
    .wizerd-div {
        border-bottom: medium none !important;
        margin: -18px 0 20px!important;
        padding-top: 0;
    }
    .wizerd-div a {
        background: none repeat scroll 0 0 #FAFAFA;
        border: 1px solid #D5D6D6;
        border-radius: 49px;
        color: #663300;
        display: inline-block;
        margin: -6px 5px -17px -8px;
        padding: 10px 12px 8px;
    }
    a{ color: #C09853;}
    .resalt-group {
        margin: 18px 0.5% 0.5%;
        padding: 0.5%;
        width: 98%;
    }
    th a.btn, th input[type="button"], th input[type="submit"], th button {
        margin: 0;
        padding: 4px 12px;
    }
    .coll_close, .coll_open {
        margin-top: 0;
    }
    .chosen-container{
        margin-top: 4px;   
    }
</style>
<div class="widget">
    <div class="path-container Fright">
        <div class="path-name Fright">
            <a href="<?php echo site_url() ?>"><?php echo lang('global_system_management') ?></a>
        </div>
        <div class="path-arrow Fright"></div>
        <div class="path-name Fright"> <a href="<?php echo site_url('hotels_reservation_orders') ?>"><?php echo lang('title') ?></a></div>

    </div>
</div>


<?php echo form_open_multipart(false, 'id="frm_hotels_reservation_orders" ') ?>
<div class="widget">
    <div class="widget-header">
        <div class="widget-header-icon Fright">
            <span class="icos-pencil2"></span>
        </div>
        <div class="widget-header-title Fright">
        </div>
    </div>
    <? if (strlen($item->refused_reason) && $item->order_status == 2) : ?>
        <div class="lable_w  TAC"><?= $item->refused_reason ?></div>
    <? endif ?>
    <div class="resalt-group" style="margin-top: 30px;">
        <div class="wizerd-div"><a> <?php echo lang('main_data') ?><div class='coll_open' id="coll_main_data"></div></a></div>
        <div class="row-fluid" id="container_main_data">  
            <? if (validation_errors()) { ?>
                <?php echo validation_errors(); ?>        
            <? } ?>
            <div class="row-form">
                <div class="span3">
                    <div class="span6">
                        <div class="span10 TAL Pleft10">
                            <?php echo lang('order_no') ?>
                        </div>
                        <div class="span10">
                            <?= form_input('order_no', set_value('order_no', $current_order_no), 'class="validate[required] input-huge" disabled="disabled" ') ?>
                        </div>
                    </div>

                    <div class="span6">
                        <div class="span10 TAL Pleft10">
                            <?php echo lang('order_date') ?>
                        </div>
                        <div class="span10">
                            <?= form_input('order_date', set_value('order_date', $item->order_date), 'class="validate[required] input-huge order_date" readonly') ?>
                        </div>
                    </div>
                </div>
                <div class="span3">
                    <div class="span6">
                        <div class="span10 TAL Pleft10">
                            <?php echo lang('arrival_date') ?>
                        </div>
                        <div class="span10">
                            <?= form_input('arrival_date', set_value('arrival_date', $item->arrival_date), 'class="validate[required] input-huge arrival_date" readonly') ?>
                        </div>
                    </div>

                    <div class="span6">
                        <div class="span10 TAL Pleft10">
                            <?php echo lang('departure_date') ?>
                        </div>
                        <div class="span10">
                            <?= form_input('departure_date', set_value('departure_date', $item->departure_date), 'class="validate[required] input-huge departure_date" readonly') ?>
                        </div>
                    </div>
                </div>
                <div class="span6">
                    <?php
                    if (session('ea_id')) {
                        ?>
                        <div class="span6">
                            <div class="span10 TAL Pleft10">
                                <?php echo lang('safa_uo_contract_id') ?>
                            </div>
                            <div class="span10">
                                <?php echo form_dropdown('safa_uo_contract_id', $uo_contracts, set_value('safa_uo_contract_id', $item->safa_uo_contract_id), 'class=" chosen-select chosen-rtl input-full"  tabindex="4" id="safa_uo_contract_id" disabled="disabled"') ?>   
                                <?= form_hidden('safa_uo_contract_id', set_value('safa_uo_contract_id', $item->safa_uo_contract_id)) ?>
                            </div>
                        </div>
                        <?php
                    }
                    ?>
                    <div class="span6">
                        <div class="span10 TAL Pleft10">
                            <?php echo lang('contact_person') ?>
                        </div>
                        <div class="span10">
                            <?php echo form_input('contact_person', set_value("contact_person", $item->contact_person), " style='' id='contact_person' readonly") ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row-form">
                <?php
                if (isset($contract_row)) {
                    if (count($contract_row) > 0) {

                        if ($contract_row[0]->services_buy_other_hotel == 1) {
                            ?>
                            <div class="span4">
                                <div class="span4 TAL Pleft10">
                                    <?php echo lang('company_type') ?>
                                </div>
                                <div class="span8">
                                    <?php
                                    $ea_checked = false;
                                    $uo_checked = false;
                                    $hm_checked = false;
                                    if ($item->erp_company_types_id == 3) {
                                        $ea_checked = true;
                                    } else if ($item->erp_company_types_id == 5) {
                                        $hm_checked = true;
                                    } else {
                                        $uo_checked = true;
                                    }
                                    ?>
                                    <?php echo form_radio('company_type', '2', $uo_checked, 'id="company_type"'); ?> <?php echo lang('uo') ?><br/>
                                    <?php echo form_radio('company_type', '3', $ea_checked, 'id="company_type"'); ?><?php echo lang('ea') ?><br/>
                                    <?php echo form_radio('company_type', '5', $hm_checked, 'id="company_type"'); ?><?php echo lang('hm') ?><br/>
                                </div>
                            </div>
                            <?php
                        } else if ($contract_row[0]->services_buy_uo_hotels == 1) {
                            ?>	

                            <div class="span3">
                                <div class="span10 TAL Pleft10">
                                    <?php echo lang('company_type') ?>
                                </div>
                                <div class="span10">
                                    <?php
                                    $ea_checked = false;
                                    $uo_checked = false;
                                    $hm_checked = false;
                                    if ($item->erp_company_types_id == 3) {
                                        $ea_checked = true;
                                    } else if ($item->erp_company_types_id == 5) {
                                        $hm_checked = true;
                                    } else {
                                        $uo_checked = true;
                                    }
                                    ?>
                                    <?php echo form_radio('company_type', '2', $uo_checked, 'id="company_type"'); ?> <?php echo lang('uo') ?><br/>

                                    <!-- 
                                    <?php echo form_radio('company_type', '3', $ea_checked, 'id="company_type"'); ?><?php echo lang('ea') ?><br/>
                                    <?php echo form_radio('company_type', '5', $hm_checked, 'id="company_type"'); ?><?php echo lang('hm') ?><br/>
                                    -->
                                </div>
                            </div>

                            <?php
                        } else {

                            session('error_message', lang('don_not_have_privilege_to_add_reservation_order'));
                            session('error_url', site_url('hotels_reservation_orders'));
                            session('error_title', lang('title'));
                            session('error_action', lang('edit_title'));

//                            redirect("error_message/show");
                        }
                    } else {
                        ?>

                        <div class="span3">
                            <div class="span10 TAL Pleft10">
                                <?php echo lang('company_type') ?>
                            </div>
                            <div class="span10">
                                <?php
                                $ea_checked = false;
                                $uo_checked = false;
                                $hm_checked = false;
                                if ($item->erp_company_types_id == 3) {
                                    $ea_checked = true;
                                } else if ($item->erp_company_types_id == 5) {
                                    $hm_checked = true;
                                } else {
                                    $uo_checked = true;
                                }
                                ?>
                                <?php echo form_radio('company_type', '2', $uo_checked, 'id="company_type"'); ?> <?php echo lang('uo') ?><br/>
                                <?php echo form_radio('company_type', '3', $ea_checked, 'id="company_type"'); ?><?php echo lang('ea') ?><br/>
                                <?php echo form_radio('company_type', '5', $hm_checked, 'id="company_type"'); ?><?php echo lang('hm') ?><br/>
                            </div>
                        </div>
                        <?php
                    }
                }
                ?>

                <div class="span3" style="padding-bottom: 32px;">
                    <div class="span10 TAL Pleft10">
                        <?php echo lang('company') ?>
                    </div>
                    <div class="span10">
                        <?php echo form_dropdown('erp_company_id2', $companies, set_value('erp_company_id', $item->owner_erp_company_id), 'class="validate[required] chosen-select chosen-rtl input-full"  tabindex="4" id="erp_company_id" data-placeholder="' . lang('global_all') . '" disabled="disabled"') ?>   
                        <?php echo form_hidden('erp_company_id',  $item->erp_company_id) ?>   
                    </div>
                </div>

            </div>
        </div>
    </div>

    <? $this->load->view('hotels_reservation_orders/rooms_view_owner') ?>
    <? $this->load->view('hotels_reservation_orders/meals_view_owner') ?>


    <!-- Gouda -->

    <!--<div class="row-fluid TAC">
        <? if ($item->order_status == 3): ?>
                <a class="btn green" href="<?= site_url('hotels_reservation_orders/accept/') ?>"><?= lang('accept') ?></a>
            <input type="button" class="btn green"  name="smt_send" onclick="form_submit()" value="<?= lang('accept') ?>" style="background: none repeat scroll 0 0 #72BA28;
                   ">
            <input type="button" class="btn red" value="<?= lang('deny') ?>" onclick="location.href = '#reasons'">
        <? else: ?>
            <input type="button" class="btn" onclick="document.location = '<?= site_url('hotels_reservation_orders/incoming') ?>'" name="smt_send" value="<?php echo lang('global_back') ?>" style="margin:10px;padding: 5px;height: auto">
        <? endif ?>
        <a href="<?= site_url('hotels_reservation_orders/pdf_export/' . $item->erp_hotels_reservation_orders_id) ?>" target="_blank" class="icon-book"></a>

    </div>
    --><?= form_close() ?>

</div>
<div id="reasons" style="display: none">

    <?= form_open('hotels_reservation_orders/cancel/' . $this->uri->segment(3)) ?>
    <div class="modal-body">
        <style>
            .wizerd-div{margin: 12px 0 33px;}
        </style>
        <div class="wizerd-div"><a><?= lang('purpose_of_deny') ?></a></div>
        <div class="border" id="container_main_data">
            <div class="row-fluid">
                <div class="span12" style="text-align: center">
                    <?= form_textarea('reason', set_value('reason'), 'class="validate[required] input-huge"') ?>
                </div>
            </div>
            <div class="row-fluid" align="center"> 
                <input type="submit" class="btn" name="smt_send" value="<?php echo lang('send') ?>" style="margin: auto">
            </div>
        </div>
    </div>
    <?= form_close() ?>
</div>
<script>
    $('.red').fancybox({content: $('#reasons')});
    function form_submit() {
        $.post('<?= site_url('hotels_reservation_orders/check_availability') ?>', $('#frm_hotels_reservation_orders').serialize(), function(data) {
            json = JSON.parse(data);
            error = false;
            $.each(json, function(i, result) {
                if (result < 0)
                {
                    error = true;
                }
            });
            if (error == true) {
                alert("<?php echo lang('unavailabile_rooms') ?>");
                return false;
            } else {
                $('#frm_hotels_reservation_orders').submit();
            }
        });
    }

</script>
<!-- MUHAMMAD EL-SAEED -->

<div class="footer"></div>
<script type="text/javascript">
    $(document).ready(function() {
        var config = {
            '.chosen-select': {width: "100%"},
            '.chosen-select-deselect': {allow_single_deselect: true},
            '.chosen-select-no-single': {disable_search_threshold: 10},
            '.chosen-select-no-results': {no_results_text: 'Oops, nothing found!'},
            '.chosen-select-width': {width: "95%"}
        }
        for (var selector in config) {
            $(selector).chosen(config[selector]);
        }


        // binds form submission and fields to the validation engine
        $("#frm_hotels_reservation_orders").validationEngine({
            prettySelect: true,
            useSuffix: "_chosen",
            promptPosition: "topRight:-150"
                    //promptPosition : "bottomLeft"
        });


        $('#company_type').live('change', function() {

            var erp_company_type_id = $(this).val();

            var dataString = 'erp_company_type_id=' + erp_company_type_id;

            $.ajax
                    ({
                        type: 'POST',
                        url: '<?php echo base_url() . 'hotels_reservation_orders/getCompaniesByCompanyType'; ?>',
                        data: dataString,
                        cache: false,
                        success: function(html)
                        {
                            //alert(html);
                            $("#erp_company_id").html(html);
                            $("#erp_company_id").trigger("chosen:updated");
                        }
                    });

        });


        $('#erp_cities_id').change(function() {

            var erp_cities_id = $(this).val();

            var dataString = 'erp_cities_id=' + erp_cities_id;

            $.ajax
                    ({
                        type: 'POST',
                        url: '<?php echo base_url() . 'hotels_reservation_orders/getHotelsByCities'; ?>',
                        data: dataString,
                        cache: false,
                        success: function(html)
                        {
                            //alert(html);
                            $("#erp_hotels_id").html(html);
                            $("#erp_hotels_id").trigger("chosen:updated");

                            $("#erp_hotels_id_alternative").html(html);
                            $("#erp_hotels_id_alternative").trigger("chosen:updated");
                        }
                    });

        });

        $('#chk_alternative_hotel').change(function() {
            if ($('#chk_alternative_hotel').is(':checked')) {
                $("#erp_hotels_id_alternative").val('').trigger("chosen:updated");
                $("#erp_hotels_id_alternative").attr('disabled', false).trigger("chosen:updated");
            } else {
                $("#erp_hotels_id_alternative").attr('disabled', true).trigger("chosen:updated");
            }
        });


    });


</script>

<script type="text/javascript">
    $(document).ready(function() {

        $("#all_nationalities_lnk").click(function() {
            $("#nationalities").val([<?php
    foreach ($nationalities_rows as $nationalities_row) {
        echo("'$nationalities_row->erp_nationality_id',");
    }
    ?>]);
            $("#nationalities").trigger("chosen:updated");
        });
        $("#arabic_nationalities_lnk").click(function() {
            $("#nationalities").val([<?php
    foreach ($nationalities_rows as $nationalities_row) {
        if ($nationalities_row->arabian == 1) {
            echo("'$nationalities_row->erp_nationality_id',");
        }
    }
    ?>]);
            $("#nationalities").trigger("chosen:updated");
        });
        $("#foreign_nationalities_lnk").click(function() {
            $("#nationalities").val([<?php
    foreach ($nationalities_rows as $nationalities_row) {
        if ($nationalities_row->arabian == 0) {
            echo("'$nationalities_row->erp_nationality_id',");
        }
    }
    ?>]);
            $("#nationalities").trigger("chosen:updated");
        });
        $("#clear_nationalities_lnk").click(function() {
            $("#nationalities").val('');
            $("#nationalities").trigger("chosen:updated");
        });

    });


</script>


<script type="text/javascript">
    $('div.widget-header a').click(function(evt) {
        evt.stopPropagation();
    });

    $("#coll_main_data").click(function() {
        $header = $(this);
        //getting the next element
        $content = $header.next();
        $content = $('#container_main_data');

        //open up the content needed - toggle the slide- if visible, slide up, if not slidedown.
        $content.slideToggle(500, function() {
            //execute this after slideToggle is done
            //change text of header based on visibility of content div
            $header.text(function() {
                //change text based on condition
                //return $content.is(":visible") ? "Collapse" : "Expand";
            });
        });

        if ($("#coll_main_data").prop("class") == 'coll_open') {
            $('#coll_main_data').removeClass('coll_open').addClass('coll_close');
        } else {
            $('#coll_main_data').removeClass('coll_close').addClass('coll_open');
        }

    });

    $("#coll_general_feature_for_hotel").click(function() {
        $header = $(this);
        //getting the next element
        $content = $header.next();
        $content = $('#container_general_feature_for_hotel');

        //open up the content needed - toggle the slide- if visible, slide up, if not slidedown.
        $content.slideToggle(500, function() {
            //execute this after slideToggle is done
            //change text of header based on visibility of content div
            $header.text(function() {
                //change text based on condition
                //return $content.is(":visible") ? "Collapse" : "Expand";
            });
        });

        if ($("#coll_general_feature_for_hotel").prop("class") == 'coll_open') {
            $('#coll_general_feature_for_hotel').removeClass('coll_open').addClass('coll_close');
        } else {
            $('#coll_general_feature_for_hotel').removeClass('coll_close').addClass('coll_open');
        }

    });
    $("#coll_general_feature_for_hotel").click();
    $("#coll_rooms").click(function() {
        $header = $(this);
        //getting the next element
        $content = $header.next();
        $content = $('#container_rooms');

        //open up the content needed - toggle the slide- if visible, slide up, if not slidedown.
        $content.slideToggle(500, function() {
            //execute this after slideToggle is done
            //change text of header based on visibility of content div
            $header.text(function() {
                //change text based on condition
                //return $content.is(":visible") ? "Collapse" : "Expand";
            });
        });

        if ($("#coll_rooms").prop("class") == 'coll_open') {
            $('#coll_rooms').removeClass('coll_open').addClass('coll_close');
        } else {
            $('#coll_rooms').removeClass('coll_close').addClass('coll_open');
        }

    });

    $("#coll_meals").click(function() {
        $header = $(this);
        //getting the next element
        $content = $header.next();
        $content = $('#container_meals');

        //open up the content needed - toggle the slide- if visible, slide up, if not slidedown.
        $content.slideToggle(500, function() {
            //execute this after slideToggle is done
            //change text of header based on visibility of content div
            $header.text(function() {
                //change text based on condition
                //return $content.is(":visible") ? "Collapse" : "Expand";
            });
        });

        if ($("#coll_meals").prop("class") == 'coll_open') {
            $('#coll_meals').removeClass('coll_open').addClass('coll_close');
        } else {
            $('#coll_meals').removeClass('coll_close').addClass('coll_open');
        }

    });


    $(".search").click(function() {
        $.fancybox.showLoading();
        $.ajax({
            type: "POST",
            cache: false,
            url: "<?= site_url('hotels_reservation_orders/hotel_search') ?>",
            data: $('#frm_hotels_reservation_orders').serialize(),
            success: function(data) {
                $.fancybox(data);
            }
        });

    });


</script>

