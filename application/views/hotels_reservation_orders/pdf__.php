<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <title></title>
        <meta  http-equiv="Content-Type" content="text/html; charset=utf-8" >
        <meta name="viewport" content="width=device-width">
    
    <!-- 
    <style>
        body{
            margin: 0;
            padding: 0;
        }
        .content{
            width: 950px;
            height: auto;
            margin: 0 auto;
            padding: 5px;
            text-align: right;
            overflow: hidden;
              font-family: tahoma;
              font-size: 11px;
              color: #333!important;
              
        }
        .content .table {
            overflow: hidden;
            border: 1px solid #DDD;
            border-radius: 5px;
            padding: 10px;
            margin: 20px 0;
        }
        
        .table-border{
            border: 1px solid #DDD;
            border-radius: 5px;
            padding: 5px;
            width: auto;
            margin-bottom:  -14px;
            padding-bottom:  10px;
            overflow: hidden;
        }
        
  
   /* table */  
  table{
	  width:100%;
	  float:right;
          direction: rtl;
  }
  
.highlight{
  background:#f0ebdc !important;
}
  
    table thead th.sorting, table thead td.sorting{background: url('../img/backgrounds/dropdown_arrow.png') right center no-repeat;}
    table thead th.sorting_asc, table thead td.sorting_asc{background: url('../img/backgrounds/dropdown_arrow_up.png') right center no-repeat;}
    table thead th.sorting_desc, table thead td.sorting_desc{background: url('../img/backgrounds/dropdown_arrow_down.png') right center no-repeat;}
    
    table{border: 0px; position: relative; padding: 0px; margin: 0px; border: 1px solid #DDD;}
        table td, table th,.content h3{   border-bottom: 1px solid #DDDDDD;
    border-left: 1px solid #DDDDDD;
    box-shadow: 1px 1px 0 #FFFFFF inset;
    font-family: tahoma !important;
    font-weight: 300 !important;
    padding: 6px;
    font-size: 13px;
    text-shadow: 1px 1px 0 #FFFFFF;
                  text-shadow: 1px 1px 0px #FFF;}
            table td:last-child{border-left: 0px;}
			table th:last-child{border-left: 0px;}
			table tr:last-child td{border-bottom: 1px solid rgba(58, 56, 56, 0.2);}
        table thead td, table thead th{background: #F1F1F1; color: #333; font-weight: bold;}
        table tbody tr:nth-child(2n+1){background: #FCFCFC;}
        
        .table-hover tbody tr:hover td, table tbody tr.active td{background: #ffffcb;}       
        .table{margin-bottom: 0px;}
        
        
    /* eof table */
       
    .output-div{width: 23%;padding: 1%;float: right;direction: rtl;}
    .output{width: 48%;padding: 1%;float: right}
    .output-div .lable{font-weight: 700!important;}
    .output-div .text {font-size: 12px;}
    .table h3{
            border: 1px solid #DDD;
            border-radius: 5px;
            padding: 5px;
            width: auto;
            margin-top: -14px;
            padding-top: 10px;
            font-weight: bold!important;
            font-size: 12px;
    }
    </style>
     -->
    
    
    </head>
    
    
    
    <body>
        
    	<div style="width: 950px; height: auto; margin: 0 auto; padding: 5px; text-align: right; overflow: hidden; font-family: tahoma; font-size: 11px; color: #333!important;">
        <div style="overflow: hidden; border: 1px solid #DDD;  border-radius: 5px; padding: 10px; margin: 20px 0;">
        <h3 style="border: 1px solid #DDD;border-radius: 5px; padding: 5px;width: auto;margin-top: -14px;padding-top: 10px;font-weight: bold!important;font-size: 12px;"><?php echo  lang('main_data') ?></h3>
        <div style="border: 1px solid #DDD; border-radius: 5px; padding: 5px; width: auto; margin-bottom:  -14px; padding-bottom:  10px; overflow: hidden;">
            
		<table cellspacing="0" cellpadding="0" >
		
		<tbody>
		<tr>
           <td style=""><?php echo lang('order_no'); ?></td>
           <td style=""><?php echo $current_order_no; ?></td>
           
           <td style=""><?php echo lang('order_date'); ?></td>
           <td style=""><?php echo $item->order_date; ?></td>
		</tr>
		<tr>
          <td style=""><?php echo lang('company_type'); ?></td>
           <td style=""><?php if($item->erp_company_types_id==3) { 
		        	echo  lang('ea');
		        } else if($item->erp_company_types_id==5) {
		        	echo  lang('hm');
		        } else {
		        	echo  lang('uo');
		        }?></td>
           
           <td style=""><?php echo lang('company'); ?></td>
           <td style=""><?php if($item->erp_company_types_id==3) { 
		        	if(name()=='name_la') {
                            	echo   $item->ea_name_la ;
                            } else {
                            	echo   $item->ea_name_ar ;
                            } 
		        } else if($item->erp_company_types_id==5) {
		        	if(name()=='name_la') {
                            	echo   $item->hm_name_la ;
                            } else {
                            	echo   $item->hm_name_ar ;
                            } 
		        } else {
		        	if(name()=='name_la') {
                            	echo   $item->uo_name_la ;
                            } else {
                            	echo   $item->uo_name_ar ;
                            } 
		        }?></td>
		</tr>
		<tr>
           <td style="width: 20%"><?php echo lang('city'); ?></td>
           <td style="width: 30%"><?php echo   $item->city_name ; ?></td>
           
           <td style="width: 20%"><?php echo lang('hotel'); ?></td>
           <td style="width: 30%"><?php 
                            if(name()=='name_la') {
                            	echo   $item->hotel_name_la ;
                            } else {
                            	echo   $item->hotel_name_ar ;
                            } 
                            ?></td>
		</tr>
		<!-- 
		<tr>
           <td style=""><?php echo lang('alternative_hotel'); ?></td>
           <td style=""><?php echo $item->erp_hotels_id_alternative; ?></td>
           
           <td style=""><?php echo lang('hotel'); ?></td>
           <td style=""><?php echo $item->hotel; ?></td>
		</tr>
		 -->
		<tr>
           <td style=""><?php lang('arrival_date'); ?></td>
           <td style=""><?php echo $item->arrival_date; ?></td>
           
           <td style=""><?php lang('departure_date'); ?></td>
           <td style=""><?php echo $item->departure_date; ?></td>
		</tr>
		<tr>
		<?php if(session('ea_id')) {?>
           <td style=""><?php lang('safa_uo_contract_id'); ?></td>
           <td style=""><?php echo $item->safa_uo_contract_name; ?></td>
        <?php } ?> 
           <td style=""><?php lang('contact_person'); ?></td>
           <td style=""><?php echo $item->contact_person; ?></td>
		</tr>
		</tbody>
		</table>
            
        
        </div>
        </div>
        <div style="overflow: hidden; border: 1px solid #DDD;  border-radius: 5px; padding: 10px; margin: 20px 0;">
        <h3> <?php echo  lang('general_feature_for_hotel') ?> </h3>
       
        <div style="border: 1px solid #DDD; border-radius: 5px; padding: 5px; width: auto; margin-bottom:  -14px; padding-bottom:  10px; overflow: hidden;">
           
<table cellspacing="0" cellpadding="0" >
		
		<tbody>
		<tr>
           <td style=""><?php echo lang('distance_from'); ?></td>
           <td style=""><?php echo  lang('distance_from') ?> <?php echo $item->distance_from; ?> - <?php echo  lang('distance_to') ?> <?php echo $item->distance_to; ?></td>
           
           <td style=""><?php echo lang('look_to_haram'); ?></td>
           <td style=""><?php echo $look_to_haram_checked; ?></td>
		</tr>
		<tr>
          <td style=""><?php echo lang('nationalities'); ?></td>
           <td style=""><?php echo $item_nationalities; ?></td>
           
           <td style=""><?php echo lang('inclusive_services'); ?></td>
           <td style=""><?php echo $item_room_services; ?></td>
		</tr>
		<tr>
           <td style=""><?php echo lang('advantages'); ?></td>
           <td style=""><?php echo $item_advantages; ?></td>
           
           <td style=""></td>
           <td style=""></td>
		</tr>
		
		</tbody>
		</table>
            
            </div>
        </div>
        
        <div style="overflow: hidden; border: 1px solid #DDD;  border-radius: 5px; padding: 10px; margin: 20px 0;">
        <h3 style="border: 1px solid #DDD;
            border-radius: 5px;
            padding: 5px;
            width: auto;
            margin-top: -14px;
            padding-top: 10px;
            font-weight: bold!important;
            font-size: 12px;"> <?php echo  lang('rooms') ?> </h3>
        <div style="border: 1px solid #DDD; border-radius: 5px; padding: 5px; width: auto; margin-bottom:  -14px; padding-bottom:  10px; overflow: hidden;">
        <table cellspacing="0" cellpadding="0" style="width:100%; float:right; direction: rtl; border: 0px; position: relative; padding: 0px; margin: 0px; border: 1px solid #DDD;">
<thead>

<tr>
                        <th  style="background: #F1F1F1; color: #333; font-weight: bold; border-left: 1px solid #DDDDDD; box-shadow: 1px 1px 0 #FFFFFF inset; font-family: tahoma !important; font-weight: 300 !important; padding: 6px; font-size: 13px; text-shadow: 1px 1px 0 #FFFFFF; text-shadow: 1px 1px 0px #FFF;"><?php echo  lang('housing_type') ?></th>
                        <th  style="background: #F1F1F1; color: #333; font-weight: bold; border-left: 1px solid #DDDDDD; box-shadow: 1px 1px 0 #FFFFFF inset; font-family: tahoma !important; font-weight: 300 !important; padding: 6px; font-size: 13px; text-shadow: 1px 1px 0 #FFFFFF; text-shadow: 1px 1px 0px #FFF;"><?php echo  lang('room_type') ?></th>
                        <th  style="background: #F1F1F1; color: #333; font-weight: bold; border-left: 1px solid #DDDDDD; box-shadow: 1px 1px 0 #FFFFFF inset; font-family: tahoma !important; font-weight: 300 !important; padding: 6px; font-size: 13px; text-shadow: 1px 1px 0 #FFFFFF; text-shadow: 1px 1px 0px #FFF;"><?php echo  lang('count') ?></th>
                        <th  style="background: #F1F1F1; color: #333; font-weight: bold; border-left: 1px solid #DDDDDD; box-shadow: 1px 1px 0 #FFFFFF inset; font-family: tahoma !important; font-weight: 300 !important; padding: 6px; font-size: 13px; text-shadow: 1px 1px 0 #FFFFFF; text-shadow: 1px 1px 0px #FFF;"><?php echo  lang('price_from') ?></th>
                        <th  style="background: #F1F1F1; color: #333; font-weight: bold; border-left: 1px solid #DDDDDD; box-shadow: 1px 1px 0 #FFFFFF inset; font-family: tahoma !important; font-weight: 300 !important; padding: 6px; font-size: 13px; text-shadow: 1px 1px 0 #FFFFFF; text-shadow: 1px 1px 0px #FFF;"><?php echo  lang('price_to') ?></th>
                        <th  style="background: #F1F1F1; color: #333; font-weight: bold; border-left: 1px solid #DDDDDD; box-shadow: 1px 1px 0 #FFFFFF inset; font-family: tahoma !important; font-weight: 300 !important; padding: 6px; font-size: 13px; text-shadow: 1px 1px 0 #FFFFFF; text-shadow: 1px 1px 0px #FFF;"><?php echo  lang('currency') ?></th>
                        <th  style="background: #F1F1F1; color: #333; font-weight: bold; border-left: 1px solid #DDDDDD; box-shadow: 1px 1px 0 #FFFFFF inset; font-family: tahoma !important; font-weight: 300 !important; padding: 6px; font-size: 13px; text-shadow: 1px 1px 0 #FFFFFF; text-shadow: 1px 1px 0px #FFF;"><?php echo  lang('nights_count') ?></th>
                        <th  style="background: #F1F1F1; color: #333; font-weight: bold; border-left: 1px solid #DDDDDD; box-shadow: 1px 1px 0 #FFFFFF inset; font-family: tahoma !important; font-weight: 300 !important; padding: 6px; font-size: 13px; text-shadow: 1px 1px 0 #FFFFFF; text-shadow: 1px 1px 0px #FFF;"><?php echo  lang('entry_date') ?></th>
                        <th  style="background: #F1F1F1; color: #333; font-weight: bold; border-left: 1px solid #DDDDDD; box-shadow: 1px 1px 0 #FFFFFF inset; font-family: tahoma !important; font-weight: 300 !important; padding: 6px; font-size: 13px; text-shadow: 1px 1px 0 #FFFFFF; text-shadow: 1px 1px 0px #FFF;"><?php echo  lang('exit_date') ?></th>
                        <th  style="background: #F1F1F1; color: #333; font-weight: bold; border-left: 1px solid #DDDDDD; box-shadow: 1px 1px 0 #FFFFFF inset; font-family: tahoma !important; font-weight: 300 !important; padding: 6px; font-size: 13px; text-shadow: 1px 1px 0 #FFFFFF; text-shadow: 1px 1px 0px #FFF;"><?php echo  lang('meal') ?></th>
                    </tr>


</thead>
<tbody>
<? if(isset($item_rooms)) { ?>
                    <? if(check_array($item_rooms)) { ?>
                    <? foreach($item_rooms as $item_room) { ?>
                    <tr rel="<?php echo  $item_room->erp_hotels_reservation_orders_rooms_id ?>">
                        
                        <td> <?php echo '';?>  </td>
                        <td> <?php echo '';?>  </td>
                        <td> <?php echo $item_room->rooms_count;?>  </td>
                        <td> <?php echo $item_room->price_from;?>  </td>
                        <td> <?php echo $item_room->price_to;?>  </td>
                        
                        
                        
                        <td> <?php echo '';?>  </td>
                        <td> <?php echo $item_room->nights_count;?>  </td>
                        
                       <td> <?php echo $item_room->entry_date;?>  </td>
                       <td> <?php echo $item_room->exit_date;?>  </td>
                       <td> <?php echo '';?>  </td>
                      
                        
                    </tr>
                    <? } ?>
                    <? } ?>
                    <? } ?>
</tbody>
</table></div>
        </div>
        <div style="overflow: hidden; border: 1px solid #DDD;  border-radius: 5px; padding: 10px; margin: 20px 0;">
        <h3 style="border: 1px solid #DDD;
            border-radius: 5px;
            padding: 5px;
            width: auto;
            margin-top: -14px;
            padding-top: 10px;
            font-weight: bold!important;
            font-size: 12px;"><?php echo  lang('meals') ?></h3>
       
        <div style="border: 1px solid #DDD; border-radius: 5px; padding: 5px; width: auto; margin-bottom:  -14px; padding-bottom:  10px; overflow: hidden;">
            <table  cellspacing="0" cellpadding="0" style="width:100%; float:right; direction: rtl; border: 0px; position: relative; padding: 0px; margin: 0px; border: 1px solid #DDD;">
            <thead>
                
                <tr>
                    <th style="background: #F1F1F1; color: #333; font-weight: bold; border-left: 1px solid #DDDDDD; box-shadow: 1px 1px 0 #FFFFFF inset; font-family: tahoma !important; font-weight: 300 !important; padding: 6px; font-size: 13px; text-shadow: 1px 1px 0 #FFFFFF; text-shadow: 1px 1px 0px #FFF;"> <?php echo  lang('meal') ?></th>
                    <th style="background: #F1F1F1; color: #333; font-weight: bold; border-left: 1px solid #DDDDDD; box-shadow: 1px 1px 0 #FFFFFF inset; font-family: tahoma !important; font-weight: 300 !important; padding: 6px; font-size: 13px; text-shadow: 1px 1px 0 #FFFFFF; text-shadow: 1px 1px 0px #FFF;"><?php echo  lang('count') ?></th>
                   
                    
                </tr>
            </thead>
            <tbody>
               
				 <? if(isset($item_meals)) { ?>
                    <? if(check_array($item_meals)) { ?>
                    <? foreach($item_meals as $item_meal) { ?>
                    <tr >
                        
                        <td> <?php echo '';?>  </td>
                        <td> <?php echo '';?>  </td>
                        
                      
                    </tr>
                    <? } ?>
                    <? } ?>
                    <? } ?>
				
            </tbody>
        </table>
            </div>
        </div>
    </div>
    </body>
</html>