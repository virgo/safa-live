<style>
    input[readonly],select[readonly] {
        background-color: #EEE;
    }

    .wizerd-div {
        margin-bottom: 25px;
    }
</style>
<form action="" id="package_reservation" method="post" accept-charset="utf-8" autocomplete="off" > 
    <div class="widget resalt-group">
        <div class="wizerd-div TAC"><a>طلب حجز برنامج - <?= $packagename ?></a></div>
        <div class="row-form"><input type="hidden" name="dosearch" value="all" >
            <div class="span4">
                <div class="span12">
                    <div class="span4">رقم الحجز</div>
                    <div class="span8"><input value="<?php echo ((int)$orderid->erp_hotels_reservation_orders_id +1) ?>" name="orderid" readonly="readonly" type="text" class="span6 "></div>
                    <div class="span4">تاريخ الطلب</div>
                    <div class="span8"><input name="oreder_date" value="<?= date('Y-m-d') ?>" readonly="readonly" type="text" class="span6" ></div>
                </div> 
            </div>
            <div class="span4">
                <div class="span12">
                    <div class="">
                        <div class="span2">الدخول</div>
                        <div class="span10"><input type="text" name="fromdate" onchange="updatedates();" class="validate[required] span6 datetime " id="" ></div>
                    </div>
                </div>
            </div>
            <div class="span4">
                <div class="span12">
                    <div class="row-fluid TAC"><input onclick="addhotel();
                            return false;" type="button" class="btn" value="اضافة" ></div>
                </div>
            </div>
        </div>      
    </div>
    <div class="package_hotels">
        <div class="widget resalt-group">
            <div class="N0">
                <table class="pkgtable">
                    <thead>
                        <tr>
                            <th  class="span2"></th> 
                            <th class="span2">الفندق</th>
                            <th class="span1">الفترة</th>
                            <th class="span1">عدد الليالي</th>
                            <th class="span1">نوع الغرفة</th>
                            <th>العدد</th>
                            <th>الخروج</th>
                            <th colspan = "2">تكلفة البرنامج</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody class="execlusive_nights_prices" id="execlusive_nights_prices">
                        <tr>
                            <td>
                                <i style="display: none;" rel="h0" class="icon-ok-circle icon-2x pull-right "></i> 
                                <i style="display: none;" rel="h0" class="icon-remove-circle icon-2x pull-right "></i>
                                <input name="city_0"  hrel="0" readonly="readonly" value="<?= $rows[0]->city_name ?>" type="text" class="span8 city_0">
                                <input name="rooms[N0][cityid_0]"  hrel="0" type="hidden" value="<?= $rows[0]->erp_city_id ?>" type="text" class="span8 cityid_0">
                                <span class="span12 TAC" style="position: relative;" > <i class="icon-refresh icon-2x TAC "  style="cursor: pointer; position: absolute; bottom: -19px;" ></i></span>
                            </td> 
                            <td>
                                <input name="hotel_0" id="h0" hrel="<?= $rows[0]->erp_hotel_id ?>" readonly="readonly" value="<?= $rows[0]->hotel_name ?>" type="text" class="span10 hotel_0" >
                                <input name="rooms[N0][hotelid_0]" class="hotelid_0" type="hidden" htrel="0" value="<?= $rows[0]->erp_hotel_id ?>" />
                            </td>
                            <td class="span1" rowspan="2">
                                <select name="rooms[N0][erp_package_peroid]" onchange="personprice(this);" class="chosen-select chosen-rtl span6 package_peroid erp_package_peroid" id="erp_package_peroid" tabindex="-1" >
                                    <?php foreach ($package_periods as $periodid => $peroidname) : ?>
                                        <option value="<?= $periodid ?>"><?= $peroidname ?></option>
                                        <?php
                                    endforeach;
                                    reset($package_periods);
                                    ?>
                                </select>
                            </td>
                            <td class="TAC">
                                <input name="rooms[N0][nights_0]" readonly="readonly" value="<?= get_peroid_night_count(key($package_periods), $rows[0]->erp_city_id) ?>" type="text" class="span6 nights_0">
                            </td>
                            <td class="TAC" rowspan="2">   
                                <select name="rooms[N0][erp_hotelroomsize]" onchange="fix_persons(this);" class="chosen-select chosen-rtl span10 erp_hotelroomsize" id="" tabindex="-1" style="">
                                    <?php foreach ($erp_hotelroomsizes as $size => $sizename) : ?>
                                        <option value="<?= $size ?>"><?= $sizename ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </td>

                            <td class="TAC" rowspan="2">
                                <input type="text"  onchange="fix_persons(this);"  name="rooms[N0][roomscount]" value="0" class="validate[required] span8 roomscount">
                            </td>
                            <td class="TAC">
                                <input class="span10 hotel0exit" style="width: 65px;" name="rooms[N0][hotel0exit]" readonly="readonly" id="" type="text" value="" style=" " >
                            </td>

                            <td class="TAC">
                                <div style="margin-bottom: 18px;" class="span10">الافراد</div>

                            </td>

                            <td class="TAC">
                                <div class=""><input type="text"  readonly="readonly" name="rooms[N0][personscount]" value="0" class="span10 personscount"></div>

                            </td>
                            <td class="TAC" rowspan="2">
                                <a class="btn Fleft check_availability" rel="N0" title="فحص الاتاحة" href="javascript:void(0)">
                                    فحص الاتاحة
                                </a>
                                <a href="javascript:void(0)" onclick="$(this).parents('.resalt-group').remove();"><span class="icon-trash"></span></a>
                            </td>
                        </tr>
                        <tr style="background: none repeat scroll 0 0 #FCFCFC;">
                            <td class="TAC">
                                <i style="display: none;" rel="h1" class="icon-ok-circle icon-2x pull-right "></i> 
                                <i style="display: none;" rel="h1" class="icon-remove-circle icon-2x pull-right "></i>
                                <input name="city_1"  hrel="1" readonly="readonly" value="<?= $rows[1]->city_name ?>" type="text" class="span8 city_1">
                                <input name="rooms[N0][cityid_1]"  hrel="1" class="cityid_1" type="hidden" value="<?= $rows[1]->erp_city_id ?>" >

                            </td>
                            <td class="TAC">
                                <input name="hotel_1" id="h0" hrel="<?= $rows[1]->erp_hotel_id ?>" readonly="readonly" value="<?= $rows[1]->hotel_name ?>" type="text" class="span10 hotel_1" >
                                <input name="rooms[N0][hotelid_1]" class="hotelid_1" hrel="1" type="hidden" value="<?= $rows[1]->erp_hotel_id ?>" >
                            </td>
                            <td class="TAC">
                                <input name="rooms[N0][nights_1]" readonly="readonly" value="<?= get_peroid_night_count(key($package_periods), $rows[1]->erp_city_id) ?>" type="text" class="span6 nights_1">
                            </td>
                            <td class="TAC">
                                <input class="span10 hotel1exit" style="width: 65px;" name="rooms[N0][hotel1exit]" readonly="readonly" id="" type="text" value="" style=" " >
                            </td>
                            <td class="TAC">
                                <div class="span10">الفرد</div>
                                <input type="text" readonly="readonly" name="personcost[N0]" value="0" class="validate[required] span10 personcost">
                            </td>
                            <td class="TAC">
                                <div class="span10">الجميع</div>
                                <input type="text"  readonly="readonly" name="allcost[N0]" value="0" class="validate[required] span10 allcost">
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>

    </div>

    <div class="widget resalt-group">
        <div class="row-form">
        <?php if (isset($item_execlusive_meals_prices) && count($item_execlusive_meals_prices)) : ?>
                <div class="span8">
                    <table style="width:600px">
                        <tr>
                            <th>الفندق</th>
                            <th>الوجبة</th>
                            <th>السعر</th>
                            <th>العدد</th>
                            <th>الاجمالي</th>
                        </tr>
                        <?php foreach ($item_execlusive_meals_prices as $mealprice) :?>
                        <tr>
                            <td class="TAC">
                                <input name="hotel_name" readonly="readonly" value="<?php echo $mealprice->hotel_name ?>" type="text" class="span8 " >
                                <input type="hidden" name="meal_hotel_id[<?php echo $mealprice->safa_package_execlusive_meal_id ?>]" value="<?php echo $mealprice->hotel_id ?>" />
                            </td>
                            
                            <td class="TAC">
                                <input name="mealname" readonly="readonly" value="<?php echo $mealprice->meal_name ?>" type="text" class="span8 " >
                                <input type="hidden" name="meals_erp_meals[<?php echo $mealprice->safa_package_execlusive_meal_id ?>]" value="<?php echo $mealprice->erp_meal_id ?>" />
                            </td>
                            </td>
                            <td class="TAC"><input name="mealsprice[<?php echo $mealprice->safa_package_execlusive_meal_id ?>]"  readonly="readonly" value="<?php echo $mealprice->price ?>" type="text" class="span8 "></td>
                            <td class="TAC"><input name="mealscount[<?php echo $mealprice->safa_package_execlusive_meal_id ?>]"  value="0" type="text" class="span8 " onchange="mealtotal(<?php echo $mealprice->price ?>,$(this).val(),<?php echo $mealprice->safa_package_execlusive_meal_id ?>);"></td>
                            <td class="TAC"><input name="total_mail_price[<?php echo $mealprice->safa_package_execlusive_meal_id ?>]" mealrel="<?php echo $mealprice->safa_package_execlusive_meal_id ?>" readonly="readonly" value="" type="text" class="span8 " ></td>
                        </tr>
                        <?php endforeach ?>
                    </table> 
                </div>
            <?php endif ?>
            <div class="span4 TAC warning">
                <div class="wizerd-div"><a>تكلفة البرنامج</a></div> 
                <div class="row-fluid" style="margin: 3%;width: 94%;">
                    <div class="span12">
                        <div class="span12">
                            <div class="span6">  اجمالي الافراد:</div>
                            <div class="span6">
                                <input type="text" readonly="readonly" name="total_persons" value="" class="span6" pmbx_context="">                                            </div>
                        </div>
                    </div>
                    <div class="span12">
                        <div class="span12">
                            <div class="span6">  اجمالي الغرف:</div>
                            <div class="span6">
                                <input type="text" readonly="readonly" name="total_rooms" value="" class="span6"pmbx_context="">                                            </div>
                        </div>
                    </div>
                    <div class="span12">
                        <div class="span12 ">
                            <div class="span6">  اجمالي البرنامج غرف:</div>
                            <div class="span6">
                                <input type="text" readonly="readonly" name="total_package_price" value="" class="span6" pmbx_context="">                                            </div>
                        </div>
                    </div>
                    <?php if (isset($item_execlusive_meals_prices) && count($item_execlusive_meals_prices)) : ?>
                    <div class="span12">
                        <div class="span12">
                            <div class="span6">  اجمالي الوجبات:</div>
                            <div class="span6">
                                <input type="text" readonly="readonly" name="total_meals" value="" class="span6 total_meals" pmbx_context="">                                            </div>
                        </div>
                    </div>
                    <?php endif ?>
                </div>
                <div class="row-fluid " style="">
                    <div class="span12 info2 TAC">
                        <div class="span6">  اجمالي البرنامج:</div>
                        <div class="span6 ">
                            <input type="text" readonly="readonly" name="total_all" value="" class="span6 TAC total_all" pmbx_context="">                                            </div>
                    </div>
                </div>  
            </div>
            <div class="span12 TAC">
                <div class="span5" style="display: none;"> <span class="span10">يوجد غرف غير متاحة في طلبك هل تود المتابعة ؟</span>  
                    <input class="span1" type="checkbox" >
                </div>
                <div class="span2">  <input class="btn" type="submit" value="ارسل الطلب" /></div>
            </div>
        </div>
    </div>
</form>
<script type="text/javascript">
    var newordersnum = 1;
    $(document).ready(function() {
        var newordersnum = 1;
        var config = {
            '.chosen-select': {width: "100%"},
            '.chosen-select-deselect': {allow_single_deselect: true},
            '.chosen-select-no-single': {disable_search_threshold: 10},
            '.chosen-select-no-results': {no_results_text: 'Oops, nothing found!'},
            '.chosen-select-width': {width: "95%"}
        }
        for (var selector in config) {
            $(selector).chosen(config[selector]);
        }

        $("#package_reservation").validationEngine({
            prettySelect: true,
            useSuffix: "_chosen",
            promptPosition: "topRight:-150"
        });

        $('.datetime').datepicker({
            dateFormat: "yy-mm-dd",
            controlType: 'select'
        });

        $('.package_hotels').on('click', '.icon-refresh', function() {
            var hotelbody = $(this).parents('.execlusive_nights_prices');
            var city0 = $(hotelbody).find('input.city_0').val();
            var cityid0 = $(hotelbody).find('input.cityid_0').val();
            var hotel0 = $(hotelbody).find('input.hotel_0').val();
            var hotelid0 = $(hotelbody).find('input.hotelid_0').val();
            var nights0 = $(hotelbody).find('input.nights_0').val();
            $(hotelbody).find('input.city_0').val($(hotelbody).find('input.city_1').val());
            $(hotelbody).find('input.cityid_0').val($(hotelbody).find('input.cityid_1').val());
            $(hotelbody).find('input.hotel_0').val($(hotelbody).find('input.hotel_1').val());
            $(hotelbody).find('input.hotelid_0').val($(hotelbody).find('input.hotelid_1').val());
            $(hotelbody).find('input.nights_0').val($(hotelbody).find('input.nights_1').val());
            $(hotelbody).find('input.city_1').val(city0);
            $(hotelbody).find('input.cityid_1').val(cityid0);
            $(hotelbody).find('input.hotel_1').val(hotel0);
            $(hotelbody).find('input.hotelid_1').val(hotelid0);
            $(hotelbody).find('input.nights_1').val(nights0);

            $(hotelbody).find('i[rel=h0].icon-ok-circle').hide();
            $(hotelbody).find('i[rel=h0].icon-remove-circle').hide();
            $(hotelbody).find('i[rel=h1].icon-ok-circle').hide();
            $(hotelbody).find('i[rel=h1].icon-remove-circle').hide();
            updatedates();

        });


        $('.package_hotels').on('change', '.package_peroid', function() {
            var peroidid = $(this).val();
            var hotelbody = $(this).parents('.execlusive_nights_prices');
            var city0 = $(hotelbody).find('input.cityid_0').val();
            var city1 = $(hotelbody).find('input.cityid_1').val();

            $.get('<?= site_url('hotels_reservation_orders/get_nights') ?>/' + peroidid + '/' + city0, function(nights0) {
                $(hotelbody).find('input.nights_0').val(nights0);
                $.get('<?= site_url('hotels_reservation_orders/get_nights') ?>/' + peroidid + '/' + city1, function(nights1) {
                    $(hotelbody).find('input.nights_1').val(nights1);
                    updatedates();
                });
            });

        });

        $('.package_hotels').on('click', '.check_availability', function() {
            var tblbody = $(this).parents('.execlusive_nights_prices');
            var alldata = $(this).parents('.execlusive_nights_prices').find('input,select');
            $.post('<?= site_url('hotels_reservation_orders/pkg_check_availability/' . $packageid) ?>', alldata.serialize(), function(data) {
                if (data.h0 >= 0) {
                    $(tblbody).find('i[rel=h0].icon-ok-circle').show();
                    $(tblbody).find('i[rel=h0].icon-remove-circle').hide();
                } else {
                    $(tblbody).find('i[rel=h0].icon-ok-circle').hide();
                    $(tblbody).find('i[rel=h0].icon-remove-circle').show();
                }

                if (data.h1 >= 0) {
                    $(tblbody).find('i[rel=h1].icon-ok-circle').show();
                    $(tblbody).find('i[rel=h1].icon-remove-circle').hide();
                } else {
                    $(tblbody).find('i[rel=h1].icon-ok-circle').hide();
                    $(tblbody).find('i[rel=h1].icon-remove-circle').show();
                }
            }, 'JSON')


        });

    });

    function updatedates() {
        var fromdate = $('input[name=fromdate]').val();
        var arrdate = new Date(fromdate);
        $('.pkgtable').each(function() {
            var night0 = $(this).find('input.nights_0').val();
            var night1 = $(this).find('input.nights_1').val();
            var h0exit = addDays(arrdate, night0);
            var h1exit = addDays(arrdate, (parseInt(night0) + parseInt(night1)));
            $(this).find('input.hotel0exit').val(h0exit);
            $(this).find('input.hotel1exit').val(h1exit);
        });
    }

    function addDays(myDate, days) {
        var date_standard = new Date(myDate.getTime() + days * 24 * 60 * 60 * 1000);
        var month = (date_standard.getMonth() + 1);
        var return_date = date_standard.getFullYear() + "-" + ('0' + month).slice(-2) + "-" + ('0' + date_standard.getDate()).slice(-2);
        return return_date;
    }

    function fix_persons(ele) {
        var hotelbody = $(ele).parents('.execlusive_nights_prices');
        var roomsize = parseInt($(hotelbody).find('select.erp_hotelroomsize').val());
        var roomcount = parseInt($(hotelbody).find('input.roomscount').val());
        $(hotelbody).find('input.personscount').val(roomcount * roomsize);
        var allprice = parseInt($(hotelbody).find('input.personscount').val()) * parseInt($(hotelbody).find('input.personcost').val());
        $(hotelbody).find('input.allcost').val(allprice);
        personprice(ele);
    }

    function personprice(ele) {
        var hotelbody = $(ele).parents('.execlusive_nights_prices');
        var peroidid = $(hotelbody).find('.package_peroid').val();
        var roomsize = parseInt($(hotelbody).find('select.erp_hotelroomsize').val());
        $.get('<?= site_url('hotels_reservation_orders/get_price/' . $packageid) ?>/' + peroidid + '/' + roomsize, function(price) {
            $(hotelbody).find('input.personcost').val(parseInt(price));
            var allprice = parseInt($(hotelbody).find('input.personscount').val()) * parseInt(price);
            $(hotelbody).find('input.allcost').val(allprice);
            total_all();
        });

    }

    function mealtotal(price,count,rel){
        $('input[mealrel='+rel+']').val(price*count);
        total_all();
    }
    
    function total_all() {
        var allcost = 0;
        $('input.allcost').each(function() {
            allcost += parseInt($(this).val());
        });

        var totalpersons = 0;
        $('input.personscount').each(function() {
            totalpersons += parseInt($(this).val());
        });

        var roomscount = 0;
        $('input.roomscount').each(function() {
            roomscount += parseInt($(this).val());
        });

        var mealsprices = 0;
        $('input[mealrel]').each(function() {
            mealsprices += parseInt($(this).val());
        });

        $('input[name=total_package_price]').val(allcost);
        $('input[name=total_persons]').val(totalpersons);
        $('input[name=total_rooms]').val(roomscount);
        $('input[name=total_meals]').val(mealsprices);
        $('input.total_all').val(allcost+mealsprices);
    }

    function addhotel() {
        var hoteldata = function() {/*
         <div class="widget resalt-group">
        <div class="N0">
            <table class="pkgtable">
                <thead>
                    <tr>
                        <th  class="span2"></th> 
                        <th class="span2">الفندق</th>
                        <th class="span1">الفترة</th>
                        <th class="span1">عدد الليالي</th>
                        <th class="span1">نوع الغرفة</th>
                        <th>العدد</th>
                        <th>الخروج</th>
                        <th colspan = "2">تكلفة البرنامج</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody class="execlusive_nights_prices" id="execlusive_nights_prices">
                    <tr>
                        <td>
                            <i style="display: none;" rel="h1" class="icon-ok-circle icon-2x pull-right "></i> 
                            <i style="display: none;" rel="h1" class="icon-remove-circle icon-2x pull-right "></i>
                            <input name="city_0"  hrel="0" readonly="readonly" value="<?= $rows[0]->city_name ?>" type="text" class="span8 city_0">
                            <input name="rooms[{$id}][cityid_0]"  hrel="0" type="hidden" value="<?= $rows[0]->erp_city_id ?>" type="text" class="span8 cityid_0">
                            <span class="span12 TAC" style="position: relative;" > <i class="icon-refresh icon-2x TAC "  style="cursor: pointer; position: absolute; bottom: -19px;" ></i></span>
                        </td> 
                        <td>
                            <input name="hotel_0" id="h0" hrel="<?= $rows[0]->erp_hotel_id ?>" readonly="readonly" value="<?= $rows[0]->hotel_name ?>" type="text" class="span10 hotel_0" >
                            <input name="rooms[{$id}][hotelid_0]" class="hotelid_0" type="hidden" htrel="0" value="<?= $rows[0]->erp_hotel_id ?>" />
                        </td>
                        <td class="span1" rowspan="2">
                            <select name="rooms[{$id}][erp_package_peroid]" onchange="personprice(this);" class="chosen-select chosen-rtl span6 package_peroid erp_package_peroid" id="erp_package_peroid" tabindex="-1" >
<?php foreach ($package_periods as $periodid => $peroidname) : ?>
                                            <option value="<?= $periodid ?>"><?= $peroidname ?></option>
    <?php
endforeach;
reset($package_periods);
?>
                            </select>
                        </td>
                        <td class="TAC">
                            <input name="rooms[{$id}][nights_0]" readonly="readonly" value="<?= get_peroid_night_count(key($package_periods), $rows[0]->erp_city_id) ?>" type="text" class="span6 nights_0">
                        </td>
                        <td class="TAC" rowspan="2">   
                            <select name="rooms[{$id}][erp_hotelroomsize]" onchange="fix_persons(this);" class="chosen-select chosen-rtl span10 erp_hotelroomsize" id="" tabindex="-1" style="">
<?php foreach ($erp_hotelroomsizes as $size => $sizename) : ?>
                                            <option value="<?= $size ?>"><?= $sizename ?></option>
<?php endforeach; ?>
                            </select>
                        </td>

                        <td class="TAC" rowspan="2">
                            <input type="text"  onchange="fix_persons(this);"  name="rooms[{$id}][roomscount]" value="0" class="validate[required] span8 roomscount">
                        </td>
                        <td class="TAC">
                            <input class="span10 hotel0exit" style="width: 65px;" name="rooms[{$id}][hotel0exit]" readonly="readonly" id="" type="text" value="" style=" " >
                        </td>

                        <td class="TAC">
                            <div style="margin-bottom: 18px;" class="span10">الافراد</div>

                        </td>

                        <td class="TAC">
                            <div class=""><input type="text"  readonly="readonly" name="rooms[{$id}][personscount]" value="0" class="span10 personscount"></div>

                        </td>
                        <td class="TAC" rowspan="2">
                            <a class="btn Fleft check_availability" rel="N0" title="فحص الاتاحة" href="javascript:void(0)">
                                فحص الاتاحة
                            </a>
                            <a href="javascript:void(0)" onclick="$(this).parents('.resalt-group').remove();"><span class="icon-trash"></span></a>
                        </td>
                    </tr>
                    <tr style="background: none repeat scroll 0 0 #FCFCFC;">
                        <td class="TAC">
                            <i style="display: none;" rel="h1" class="icon-ok-circle icon-2x pull-right "></i> 
                            <i style="display: none;" rel="h1" class="icon-remove-circle icon-2x pull-right "></i>
                            <input name="city_1"  hrel="1" readonly="readonly" value="<?= $rows[1]->city_name ?>" type="text" class="span8 city_1">
                            <input name="rooms[{$id}][cityid_1]"  hrel="1" class="cityid_1" type="hidden" value="<?= $rows[1]->erp_city_id ?>" >

                        </td>
                        <td class="TAC">
                            <input name="hotel_1" id="h0" hrel="<?= $rows[1]->erp_hotel_id ?>" readonly="readonly" value="<?= $rows[1]->hotel_name ?>" type="text" class="span10 hotel_1" >
                            <input name="rooms[{$id}][hotelid_1]" class="hotelid_1" hrel="1" type="hidden" value="<?= $rows[1]->erp_hotel_id ?>" >
                        </td>
                        <td class="TAC">
                            <input name="rooms[{$id}][nights_1]" readonly="readonly" value="<?= get_peroid_night_count(key($package_periods), $rows[1]->erp_city_id) ?>" type="text" class="span6 nights_1">
                        </td>
                        <td class="TAC">
                            <input class="span10 hotel1exit" style="width: 65px;" name="rooms[{$id}][hotel1exit]" readonly="readonly" id="" type="text" value="" style=" " >
                        </td>
                        <td class="TAC">
                            <div class="span10">الفرد</div>
                            <input type="text" readonly="readonly" name="personcost[{$id}]" value="0" class="validate[required] span10 personcost">
                        </td>
                        <td class="TAC">
                            <div class="span10">الجميع</div>
                            <input type="text"  readonly="readonly" name="allcost[{$id}]" value="0" class="validate[required] span10 allcost">
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
         */
        }.toString().replace(/{\$id}/g, "N" + newordersnum).slice(15, -4);
        newordersnum++;
        $('.package_hotels').append(hoteldata);

        var config = {
            '.chosen-select': {width: "100%"},
            '.chosen-select-deselect': {allow_single_deselect: true},
            '.chosen-select-no-single': {disable_search_threshold: 10},
            '.chosen-select-no-results': {no_results_text: 'Oops, nothing found!'},
            '.chosen-select-width': {width: "95%"}
        }
        for (var selector in config) {
            $(selector).chosen(config[selector]);
        }

        updatedates();

    }
</script>