<? $meal_options = $erp_meals;
unset($meal_options[1]); ?>
<div class="resalt-group"><div class="wizerd-div"><a><?php echo lang('meals') ?><div class='coll_open' id="coll_meals"></div></a></div>

    <script>meals_counter = 0</script>

    <div class="row-fluid" id="container_meals">
        <div class="table-responsive">
            <table cellpadding="0" cellspacing="0">
                <thead>
                    <tr>
                        <th><?php echo lang('hotel') ?></th>
                        <th><?php echo lang('meal') ?></th>
                        <th><?php echo lang('count') ?></th>
                        <th><a class="btn " title="<?php echo lang('add') ?>" href="javascript:void(0)" onclick="add_meals('N' + meals_counter++);
                                        load_multiselect()">
                                <?php echo lang('add') ?>    
                            </a></th>
                    </tr>
                </thead>
                <tbody class="meals" id='meals'>
                    <? if (isset($item_meals)) { ?>
                        <? if (check_array($item_meals)) { ?>
                            <? foreach ($item_meals as $item_meal) { ?>
                                <tr rel="<?php echo $item_meal->erp_hotels_reservation_orders_meals_id ?>">
                                    <td>
                                        <?php
                                        echo form_dropdown('meals_erp_hotel_id[' . $item_meal->erp_hotels_reservation_orders_meals_id . ']'
                                                , $erp_hotels, set_value('meals_erp_hotel_id[' . $item_meal->erp_hotels_reservation_orders_meals_id . ']', $item_meal->erp_hotel_id)
                                                , 'class="chosen-select chosen-rtl input-full" id="meals_erp_hotel_id[' . $item_meal->erp_hotels_reservation_orders_meals_id . ']" tabindex="4" ')
                                        ?>
                                    </td>
                                    <td>
                                        <?php
                                        echo form_dropdown('meals_erp_meals[' . $item_meal->erp_hotels_reservation_orders_meals_id . ']'
                                                , $meal_options, set_value('meals_erp_meals[' . $item_meal->erp_hotels_reservation_orders_meals_id . ']', $item_meal->erp_meals_id)
                                                , 'class="chosen-select chosen-rtl input-full" id="meals_erp_meals[' . $item_meal->erp_hotels_reservation_orders_meals_id . ']" tabindex="4" ')
                                        ?>
                                    </td>

                                    <td><?php
                                        echo form_input('meals_meals_count[' . $item_meal->erp_hotels_reservation_orders_meals_id . ']'
                                                , set_value('meals_meals_count[' . $item_meal->erp_hotels_reservation_orders_meals_id . ']', $item_meal->meals_count)
                                                , 'class="input-huge" style="width:250px"')
                                        ?>
                                    </td>




                                    <td class="TAC">
                                        <a href="javascript:void(0)" onclick="delete_meals(<?php echo $item_meal->erp_hotels_reservation_orders_meals_id ?>, true)"><span class="icon-trash"></span></a>
                                    </td>
                                </tr>
        <? } ?>
    <? } ?>
<? } ?>
                </tbody>
            </table>
        </div>

    </div>
</div>

<script>
    function load_multiselect() {
        var config = {
            '.chosen-select': {width: "100%"},
            '.chosen-select-deselect': {allow_single_deselect: true},
            '.chosen-select-no-single': {disable_search_threshold: 10},
            '.chosen-select-no-results': {no_results_text: 'Oops, nothing found!'},
            '.chosen-select-width': {width: "95%"}
        }
        for (var selector in config) {
            $(selector).chosen(config[selector]);
        }
    }
    function add_meals(id) {

if(!$('.rooms tr').length) {
    alert('عفوا يجب اضافة حجرات اولا');
    return false;
}
        var text = [];
        $('#container_rooms :input').each(function() {
            var input_id = $(this).attr("id");
            //alert(input_name);
            if (input_id !== undefined) {
                if (input_id.startsWith('erp_hotels_id')) {

                    var input_value = $("#" + input_id + " option:selected").val();
                    var input_text = $("#" + input_id + " option:selected").text();


                    text[input_value] = input_text;
                }
            }
        })
        var slct_options_erp_hotels = "<option value=''></option>";
        $.each(text, function(key, val) {
            if (typeof val != 'undefined') {
                slct_options_erp_hotels = slct_options_erp_hotels + "<option value='" + key + "'>" + val + "</option>";
            }
        });
        /*
         for (var i = 0; i < text.length; i++) {
         slct_options_erp_hotels = slct_options_erp_hotels + "<option value='" + text[i][0] + "'>" + text[i][1] + "</option>";
         }
         */
        var new_row = [
                "<tr rel=\"" + id + "\">",
                "    <td>",
                "       <select name=\"meals_erp_hotel_id[" + id + "]\" class=\"chosen-select chosen-rtl input-full\" id=\"meals_erp_hotel_id_" + id + "\" tabindex=\"4\">",
                slct_options_erp_hotels,
                "   </select>",
                "    </td>",
                "    <td>",
                "       <select name=\"meals_erp_meals[" + id + "]\" class=\"chosen-select chosen-rtl input-full\" id=\"meals_erp_meals_" + id + "\" tabindex=\"4\">",
<? foreach ($meal_options as $key => $value): ?>
            "           <option value=\"<?= $key ?>\"><?= $value ?></option>",
<? endforeach ?>
        "    </td>",
                '     <td><?php echo form_input("meals_meals_count[' + id + ']", FALSE, 'class="validate[required] input-huge" ') ?>',
     "    </td>",
                "    <td class=\"TAC\">",
                "        <a href=\"javascript:void(0)\" onclick=\"delete_meals('" + id + "')\"><span class=\"icon-trash\"></span></a>",
                "    </td>",
                "</tr>"
        ].join("\n");
                $('#meals').append(new_row);

    }
    function delete_meals(id, database)
    {
        if (typeof database == 'undefined')
        {
            $('.meals').find('tr[rel="' + id + '"]').remove();
        }
        else
        {
            $('.meals').find('tr[rel=' + id + ']').remove();
            var hidden_input = '<input type="hidden" name="meals_remove[]" value="' + id + '" />';
            $('#meals').append(hidden_input);
        }
    }

</script>

