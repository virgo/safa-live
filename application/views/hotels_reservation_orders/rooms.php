<style>
    .table-responsive table th,.table-responsive table td {
        padding: 4px;
    }
</style>

<div class="resalt-group"><div class="wizerd-div"><a><?php echo lang('rooms') ?><div class='coll_open' id="coll_rooms"></div></a></div>
    <script>rooms_counter = 0</script>

    <div class="row-fluid" id="container_rooms">
        <div class="table-responsive">
            <table cellpadding="0" cellspacing="0">
                <thead>
                    <tr>
                        <th><?php echo lang('city') ?> / <?php echo lang('hotel') ?></th>
                        <th><?php echo lang('housing_type') ?></th>
                        <th><?php echo lang('room_type') ?></th>
                        <th><?php echo lang('count') ?></th>
                        <th><?php echo lang('nights_count') ?></th>
                        <th><?php echo lang('order_date') ?></th>
                        <th><?php echo lang('meal') ?></th>
                        <th><?php echo lang('notes') ?></th>
                        <th><a class="btn" title="<?php echo lang('add') ?>" href="javascript:void(0)" onclick="add_rooms('N' + rooms_counter++);
                                        load_multiselect()">
                                   <?php echo lang('add') ?>    
                            </a></th>
                    </tr>
                </thead>
                <tbody class="rooms" id='rooms'>
                    <?
                    if (isset($item_rooms)) {
                        $num = 0;
                        ?>
                        <? if (check_array($item_rooms)) { ?>
                            <? foreach ($item_rooms as $item_room) { ?><?
                                if (isset($tripid)) {
                                    $item_room->erp_housingtypes_id = 1;
                                    $item_room->notes = '';
                                    $item_room->nights_count = '';
                                    $item_room->erp_hotels_reservation_orders_rooms_id = "N" . $num;
                                    $num++;
                                }
                                ?>
                                <tr rel="<?php echo $item_room->erp_hotels_reservation_orders_rooms_id ?>">
                                    <td>
                                        <?php echo form_dropdown('rooms_erp_cities_id[' . $item_room->erp_hotels_reservation_orders_rooms_id . ']', $erp_cities, set_value('erp_cities_id', $item_room->erp_cities_id), 'class="validate[required] chosen-select chosen-rtl input-full"  tabindex="4" id="erp_cities_id" data-placeholder="' . lang('global_all') . '"') ?>
            <?php echo form_dropdown('rooms_erp_hotels_id[' . $item_room->erp_hotels_reservation_orders_rooms_id . ']', $erp_hotels, set_value('erp_hotels_id', $item_room->erp_hotels_id), 'class=" chosen-select chosen-rtl input-full erp_hotels"  tabindex="4" id="erp_hotels_id' . $item_room->erp_hotels_reservation_orders_rooms_id . '" ') ?>   
                                    </td>
                                    <td>
                                        <?php
                                        echo form_dropdown('rooms_erp_housingtypes_id[' . $item_room->erp_hotels_reservation_orders_rooms_id . ']'
                                                , $erp_housingtypes, set_value('rooms_erp_housingtypes_id[' . $item_room->erp_hotels_reservation_orders_rooms_id . ']', $item_room->erp_housingtypes_id)
                                                , 'class="chosen-select chosen-rtl input-full" id="rooms_erp_housingtypes_id_' . $item_room->erp_hotels_reservation_orders_rooms_id . '" tabindex="4" ')
                                        ?>
                                    </td>
                                    <td>
                                        <?php
                                        echo form_dropdown('rooms_erp_hotelroomsizes_id[' . $item_room->erp_hotels_reservation_orders_rooms_id . ']'
                                                , $erp_hotelroomsizes, set_value('rooms_erp_hotelroomsizes_id[' . $item_room->erp_hotels_reservation_orders_rooms_id . ']', $item_room->erp_hotelroomsizes_id)
                                                , 'class="chosen-select chosen-rtl input-full" id="rooms_erp_hotelroomsizes_id_' . $item_room->erp_hotels_reservation_orders_rooms_id . '" tabindex="4" ')
                                        ?>
                                    </td>
                                    <td><?php
                                        echo form_input('rooms_rooms_count[' . $item_room->erp_hotels_reservation_orders_rooms_id . ']'
                                                , set_value('rooms_rooms_count[' . $item_room->erp_hotels_reservation_orders_rooms_id . ']', $item_room->rooms_count)
                                                , 'class="input-huge" style="width:25px"')
                                        ?>
                                    </td>

                                    <td>
                                        <? if (strlen($item_room->entry_date) && strlen($item_room->exit_date)) : ?>
                                            <? $item_room->nights_count = floor((strtotime($item_room->exit_date) - strtotime($item_room->entry_date)) / (60 * 60 * 24)); ?>
                                        <? endif ?>
                                        <?php
                                        echo form_input('rooms_nights_count[' . $item_room->erp_hotels_reservation_orders_rooms_id . ']'
                                                , set_value('rooms_nights_count[' . $item_room->erp_hotels_reservation_orders_rooms_id . ']', $item_room->nights_count)
                                                , 'class="input-huge" style="width:100px"')
                                        ?>
                                    </td>

                                    <td>
                                        <?php
                                        echo form_input('rooms_entry_date[' . $item_room->erp_hotels_reservation_orders_rooms_id . ']'
                                                , set_value('rooms_entry_date[' . $item_room->erp_hotels_reservation_orders_rooms_id . ']', $item_room->entry_date)
                                                , 'class="input-huge rooms_entry_date' . $item_room->erp_hotels_reservation_orders_rooms_id . '" style="width:120px" placeholder="' . lang('entry_date') . '"')
                                        ?>

                                        <script>
                                            $(".rooms_entry_date<?php echo $item_room->erp_hotels_reservation_orders_rooms_id; ?>").datepicker({
                                                dateFormat: 'yy-mm-dd',
                                                controlType: 'select',
                                                timeFormat: 'HH:mm',
                                                onClose: function(selectedDate) {
                                                    $('.rooms_exit_date<?php echo $item_room->erp_hotels_reservation_orders_rooms_id; ?>').datepicker('option', 'minDate', selectedDate);
                                                }
                                            });
                                        </script>

                                        <?php
                                        echo form_input('rooms_exit_date[' . $item_room->erp_hotels_reservation_orders_rooms_id . ']'
                                                , set_value('rooms_exit_date[' . $item_room->erp_hotels_reservation_orders_rooms_id . ']', $item_room->exit_date)
                                                , 'class="input-huge exitdate  rooms_exit_date' . $item_room->erp_hotels_reservation_orders_rooms_id . '" style="width:120px" placeholder="' . lang('exit_date') . '"')
                                        ?>

                                        <script>
                                            $(".rooms_exit_date<?php echo $item_room->erp_hotels_reservation_orders_rooms_id; ?>").datepicker({
                                                dateFormat: 'yy-mm-dd',
                                                controlType: 'select',
                                                timeFormat: 'HH:mm',
                                                onClose: function(selectedDate) {
                                                    $('.rooms_entry_date<?php echo $item_room->erp_hotels_reservation_orders_rooms_id; ?>').datepicker('option', 'maxDate', selectedDate);
                                                }
                                            });
                                        </script>
                                    </td>

                                    <td>
                                        <?php
                                        echo form_dropdown('rooms_erp_meals_id[' . $item_room->erp_hotels_reservation_orders_rooms_id . ']'
                                                , $erp_meals, set_value('rooms_erp_meals_id[' . $item_room->erp_hotels_reservation_orders_rooms_id . ']', $item_room->erp_meals_id)
                                                , 'class="chosen-select chosen-rtl input-full" id="rooms_erp_meals_id_' . $item_room->erp_hotels_reservation_orders_rooms_id . '" tabindex="4" ')
                                        ?>
                                    </td>
                                    <td>
                                        <?php
                                        echo form_textarea('rooms_notes[' . $item_room->erp_hotels_reservation_orders_rooms_id . ']'
                                                , set_value('rooms_notes[' . $item_room->erp_hotels_reservation_orders_rooms_id . ']', $item_room->notes)
                                                , 'class="input-huge" style="width:115px;height:50px;"')
                                        ?>
                                    </td>
                                    <td class="TAC">
                                        <a href="javascript:void(0)" onclick="delete_rooms(<?php echo $item_room->erp_hotels_reservation_orders_rooms_id ?>, true)"><span class="icon-trash"></span></a>
                                    </td>
                                </tr>
                            <? } ?>
                        <? } ?>
<? } ?>
                </tbody>
            </table>
        </div>

    </div>
</div>

<script>
    function load_multiselect() {
        var config = {
            '.chosen-select': {width: "100%"},
            '.chosen-select-deselect': {allow_single_deselect: true},
            '.chosen-select-no-single': {disable_search_threshold: 10},
            '.chosen-select-no-results': {no_results_text: 'Oops, nothing found!'},
            '.chosen-select-width': {width: "95%"}
        }
        for (var selector in config) {
            $(selector).chosen(config[selector]);
        }
    }
    function add_rooms(id) {

        var departure_date = document.getElementById('departure_date').value;
        var arrival_date = document.getElementById('arrival_date').value;


        var new_row = [
                "<tr rel=\"" + id + "\">",
                "   <td>",
                "       <select name=\"rooms_erp_cities_id[" + id + "]\" class=\"validate[required] chosen-select chosen-rtl input-full\" id=\"erp_cities_id\" tabindex=\"4\">",
<? foreach ($erp_cities as $key => $value): ?>
            "           <option value=\"<?= $key ?>\"><?= $value ?></option>",
<? endforeach ?>
        "            </select>",
                "            ",
                "       <select name=\"rooms_erp_hotels_id[" + id + "]\" class=\"validate[required] chosen-select erp_hotels chosen-rtl input-full\" id=\"erp_hotels_id" + id + "\" tabindex=\"4\">",
<? foreach ($erp_hotels as $key => $value): ?>
            "           <option value=\"<?= $key ?>\"><?= $value ?></option>",
<? endforeach ?>
        "            </select></td>",
                "    <td>",
                "       <select name=\"rooms_erp_housingtypes_id[" + id + "]\" class=\"chosen-select chosen-rtl input-full\" id=\"rooms_erp_housingtypes_id_" + id + "\" tabindex=\"4\">",
<? foreach ($erp_housingtypes as $key => $value): ?>
            "           <option value=\"<?= $key ?>\"><?= $value ?></option>",
<? endforeach ?>
        "    </td>",
                "    <td>",
                "       <select name=\"rooms_erp_hotelroomsizes_id[" + id + "]\" class=\"chosen-select chosen-rtl input-full\" id=\"rooms_erp_hotelroomsizes_id_" + id + "\" tabindex=\"4\">",
<? foreach ($erp_hotelroomsizes as $key => $value): ?>
            "           <option value=\"<?= $key ?>\"><?= $value ?></option>",
<? endforeach ?>
        "    </td>",
                '     <td><?php echo form_input("rooms_rooms_count[' + id + ']", FALSE, 'class="validate[required] " style=\"width:25px\"') ?>',
                "    </td>",
                "    <td>",
                '        <?php echo form_input("rooms_nights_count[' + id + ']", FALSE, "class=\"validate[required] \" style=\"width:50px\" onchange=\"setDatesValues(\'' + id + '\', \'rooms_nights_count\') \" ") ?>',
                "    </td>",
                '     <td><?php
echo form_input("rooms_entry_date[' + id + ']", FALSE, " class=\"validate[required] rooms_entry_date' + id + '\" style=\"width:75px\" onchange=\"setDatesValues(\'' + id + '\', \'rooms_entry_date\') \" placeholder=\'" . lang('entry_date') .
        "\' ")
?>',
                "<script>",
                '$(".rooms_entry_date' + id + '").datepicker({',
                "    dateFormat: 'yy-mm-dd',",
                "    controlType: 'select',",
                "    timeFormat: 'HH:mm',",
                "    minDate: new Date('" + arrival_date + "'),",
                "   onClose: function(selectedDate) {$('.rooms_exit_date" + id + "').datepicker('option', 'minDate', selectedDate)}",
                "});",
                "<\/script>",
                '        <?php
echo form_input("rooms_exit_date[' + id + ']", FALSE, "class=\"validate[required] rooms_exit_date' + id + ' exitdate \" style=\"width:75px\"  onchange=\"setDatesValues(\'' + id + '\', \'rooms_exit_date\') \" placeholder=\'" . lang('exit_date') .
        "\' ")
?>',
                "<script>",
                '$(".rooms_exit_date' + id + '").datepicker({',
                "    dateFormat: 'yy-mm-dd',",
                "    controlType: 'select',",
                "    timeFormat: 'HH:mm',",
                "   onClose: function(selectedDate) {$('.rooms_entry_date" + id + "').datepicker('option', 'maxDate', selectedDate)}",
                "});",
                "<\/script>",
                "    </td>",
                "    <td>",
                "       <select name=\"rooms_erp_meals_id[" + id + "]\" class=\"chosen-select chosen-rtl input-full\" id=\"rooms_erp_meals_id_" + id + "\" tabindex=\"4\">",
<? foreach ($erp_meals as $key => $value): ?>
            "           <option value=\"<?= $key ?>\"><?= $value ?></option>",
<? endforeach ?>
        "    </td>",
                '     <td><?php echo form_textarea("rooms_notes[' + id + ']", FALSE, 'class=" " style=\"width:115px;height:50px;\"') ?>',
                "    </td>",
                "    <td class=\"TAC\">",
                "        <a href=\"javascript:void(0)\" onclick=\"delete_rooms('" + id + "')\"><span class=\"icon-trash\"></span></a>",
                "    </td>",
                "</tr>"
        ].join("\n");
                $('#rooms').append(new_row);

        document.getElementsByName('rooms_entry_date[' + id + ']')[0].value = arrival_date;
        document.getElementsByName('rooms_exit_date[' + id + ']')[0].value = departure_date;


        if (arrival_date != '' && departure_date != '') {
            var date1 = new Date(arrival_date)
            var date2 = new Date(departure_date);
            var timeDiff = Math.abs(date2.getTime() - date1.getTime());
            var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
            var nights_count_value = diffDays;

            document.getElementsByName('rooms_nights_count[' + id + ']')[0].value = nights_count_value;
        }

    }



    function delete_rooms(id, database)
    {
        if (typeof database == 'undefined')
        {
            $('.rooms').find('tr[rel="' + id + '"]').remove();
        }
        else
        {
            $('.rooms').find('tr[rel=' + id + ']').remove();
            var hidden_input = '<input type="hidden" name="rooms_remove[]" value="' + id + '" />';
            $('#rooms').append(hidden_input);
        }
    }

    function setDatesValues(id, field)
    {
        if (field == 'rooms_entry_date') {
            if (document.getElementsByName('rooms_exit_date[' + id + ']')[0].value != '' ) {
                var date1 = new Date(document.getElementsByName('rooms_entry_date[' + id + ']')[0].value)
                var date2 = new Date(document.getElementsByName('rooms_exit_date[' + id + ']')[0].value);
                var timeDiff = Math.abs(date2.getTime() - date1.getTime());
                var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
                document.getElementsByName('rooms_nights_count[' + id + ']')[0].value = diffDays;
            } else if (document.getElementsByName('rooms_exit_date[' + id + ']')[0].value == '' && document.getElementsByName('rooms_nights_count[' + id + ']')[0].value != '') {
                var myDate = new Date(document.getElementsByName('rooms_entry_date[' + id + ']')[0].value)
                var nights_count_value = document.getElementsByName('rooms_nights_count[' + id + ']')[0].value;
                document.getElementsByName('rooms_exit_date[' + id + ']')[0].value = addDays(myDate, nights_count_value);
            }
        }
        else if (field == 'rooms_exit_date') {
            if (document.getElementsByName('rooms_entry_date[' + id + ']')[0].value != '' ) {
                var date1 = new Date(document.getElementsByName('rooms_entry_date[' + id + ']')[0].value)
                var date2 = new Date(document.getElementsByName('rooms_exit_date[' + id + ']')[0].value);
                var timeDiff = Math.abs(date2.getTime() - date1.getTime());
                var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
                document.getElementsByName('rooms_nights_count[' + id + ']')[0].value = diffDays;
            } else if (document.getElementsByName('rooms_entry_date[' + id + ']')[0].value == '' && document.getElementsByName('rooms_nights_count[' + id + ']')[0].value != '') {
                var myDate = new Date(document.getElementsByName('rooms_exit_date[' + id + ']')[0].value)
                var nights_count_value = document.getElementsByName('rooms_nights_count[' + id + ']')[0].value;
                document.getElementsByName('rooms_entry_date[' + id + ']')[0].value = substractDays(myDate, nights_count_value);
            }
        }
        else if (field == 'rooms_nights_count') {
            if (document.getElementsByName('rooms_entry_date[' + id + ']')[0].value != '' ) {
                var myDate = new Date(document.getElementsByName('rooms_entry_date[' + id + ']')[0].value)
                //var date2 = new Date(document.getElementsByName('rooms_exit_date[' + id + ']')[0].value); 
                //var timeDiff = Math.abs(date2.getTime() - date1.getTime());
                //var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24)); 
                var nights_count_value = document.getElementsByName('rooms_nights_count[' + id + ']')[0].value;
                document.getElementsByName('rooms_exit_date[' + id + ']')[0].value = addDays(myDate, nights_count_value);
            } else if (document.getElementsByName('rooms_entry_date[' + id + ']')[0].value == '' && document.getElementsByName('rooms_exit_date[' + id + ']')[0].value != '') {
                var myDate = new Date(document.getElementsByName('rooms_exit_date[' + id + ']')[0].value)
                var nights_count_value = document.getElementsByName('rooms_nights_count[' + id + ']')[0].value;
                document.getElementsByName('rooms_entry_date[' + id + ']')[0].value = substractDays(myDate, nights_count_value);
            }
        }

        var lastdate = false;
        $('.exitdate').each(function() {
            if ($(this).val() !== '') {
                var tempdate = new Date($(this).val());
                if (!lastdate)
                    lastdate = tempdate;
                else if (tempdate > lastdate)
                    lastdate = tempdate;
            }
        });

        if (lastdate) {
            $('#departure_date').val(addDays(lastdate, 0));
        }
    }

    function addDays(myDate, days)
    {
        var date_standard = new Date(myDate.getTime() + days * 24 * 60 * 60 * 1000);
        var return_date = date_standard.getFullYear() + "-" + ('0' + (date_standard.getMonth() + 1)).slice(-2) + "-" + ('0' +date_standard.getDate()).slice(-2);
        return return_date;
    }

    function substractDays(myDate, days)
    {
        var date_standard = new Date(myDate.getTime() - days * 24 * 60 * 60 * 1000);
        var return_date = date_standard.getFullYear() + "-" + ('0' + (date_standard.getMonth() + 1)).slice(-2) + "-" + ('0' + date_standard.getDate()).slice(-2);
        return return_date;
    }

</script>

