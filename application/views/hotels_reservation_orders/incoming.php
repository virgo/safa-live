<div class="widget">
    <div class="path-container Fright">
    	<div class="path-name Fright">
            <a href="<?php 
            if(session('uo_id')) {
            	echo  site_url().'uo/dashboard'; 
            } else if(session('ea_id')) {
            	echo  site_url().'ea/dashboard';
            } else {
            	echo  site_url();
            }
            ?>"><?php echo  lang('global_system_management') ?></a>
        </div>
        <div class="path-arrow Fright"></div>
        <div class="path-name Fright"> <?php echo  lang('title_incoming') ?></div>
    </div>
</div>

<div class="widget">
    <div class="widget-header">
        <div class="widget-header-icon Fright"> <span class="icos-pencil2"></span> </div>
        <div class="widget-header-title Fright">
            <?php echo  lang('title_incoming') ?>
        </div>
    </div>
     <div class="widget-container">
          <div class="table-responsive">
            <table cellpadding="0" cellspacing="0" class="fsTable" >
                <thead>
                    <tr>
                    	<th><?php echo  lang('serial') ?></th>
                        <th><?php echo  lang('order_no') ?></th>
                        <th><?php echo  lang('order_date') ?></th>
                        <th><?php echo  lang('company_type') ?></th>
                        <th><?php echo  lang('company') ?></th>
                        <th><?php echo  lang('rooms_count') ?></th>
                        <th><?php echo  lang('available') ?></th>
                        <th><?php echo  lang('reset_to_complete_order') ?></th>
                        <th><?php echo  lang('order_status') ?></th>
                        <th><?php echo  lang('actions') ?></th>
                    </tr>
                </thead>
                <tbody id="had" class="sortable">
                <? if(ensure($items)) { ?>
                    <? foreach($items as $item) {?>
                    <tr id="row_<?php echo $item->erp_hotels_reservation_orders_id; ?>" rel="<?php echo  $item->erp_hotels_reservation_orders_id ?>" >

						<td>
                            <?php echo   $item->sort ; ?>
                        </td>
                        
                        <td>
                            <?php echo   $item->erp_hotels_reservation_orders_id ; ?>
                        </td>
                        <td>
                            <?php echo   $item->order_date ; ?>
                        </td>
                        
                        <td>
                        <?php 
                    	if($item->owner_erp_company_types_id==2) { 
				        	echo  lang('uo');
				        } else if($item->owner_erp_company_types_id==3) { 
				        	echo  lang('ea') ;
				        } else if($item->owner_erp_company_types_id==5) {
				        	echo   lang('hm') ;
				        }  
		                ?>
                        </td>
                        
                        <td>
                        <?php 
                            
                    	if ($item->owner_erp_company_types_id == 2) {
                    	if(name()=='name_la') {
                            	if(isset($item->uo_name_la)) {
                            		echo   $item->uo_name_la ;
                            	}
                            } else {
                            	if(isset($item->uo_name_ar)) {
                            		echo   $item->uo_name_ar ;
                            }
                        }
			        	} else if ($item->owner_erp_company_types_id == 3) {
			        		if(name()=='name_la') {
	                            	if(isset($item->ea_name_la)) {
	                            		echo   $item->ea_name_la ;
	                            	}
	                            } else {
	                            	if(isset($item->ea_name_ar)) {
	                            		echo   $item->ea_name_ar ;
	                            }
	                        }
			        	} else if ($item->owner_erp_company_types_id == 5) {
			        		if(name()=='name_la') {
	                            	if(isset($item->hm_name_la)) {
	                            		echo   $item->hm_name_la ;
	                            	}
	                            } else {
	                            	if(isset($item->hm_name_ar)) {
	                            		echo   $item->hm_name_ar ;
	                            }
	                        }
			        	}
                             
                            ?>
                        </td>
                        <td>
                        	<!-- 
                            <a class='fancybox fancybox.iframe' href="<?= site_url("hotels_reservation_orders/rooms_details_popup/". $item->erp_hotels_reservation_orders_id); ?>"><span title="<?php 
                            /*
                            $room_sizes_string_arr=explode(',',$item->room_sizes_string);
                            $room_count_string_arr=explode(',',$item->room_count_string);
                            foreach($room_count_string_arr as $key=>$value) {
                            	for($i=0; $i<$room_sizes_string_arr[$key]; $i++) {
                            		//echo nl2br("&lt;span class='icon-edit' &gt;  &lt;/span&gt; ");
                            	}
                            	echo $room_sizes_string_arr[$key].' - ';
                            	echo $value.'&#xA;';
                            }
                            */
                            $this->erp_hotels_reservation_orders_model->rooms_erp_hotels_reservation_orders_id = $item->erp_hotels_reservation_orders_id;
        					$item_rooms = $this->erp_hotels_reservation_orders_model->getRooms();
        					foreach($item_rooms as $item_room) {
                            	
	        					if(name()=='name_la') {
	                            	echo   $item_room->hotelroomsize_name_la ;
	                            } else {
	                            	echo   $item_room->hotelroomsize_name_ar ;
	                            } 
	        					echo ' - ';
	                            echo $item_room->rooms_count.'&#xA;';
        					}
        					
                            ?>"><?php echo   $item->total_rooms_count ; ?></span>
                            </a>
                             -->
                             
                             
                             <?php

                             $arr_required_rooms=array();
                             
                             
	                            $this->erp_hotels_reservation_orders_model->rooms_erp_hotels_reservation_orders_id = $item->erp_hotels_reservation_orders_id;
	        					$item_rooms = $this->erp_hotels_reservation_orders_model->getRooms();
	        					
	        					echo"<table>";
	        					foreach($item_rooms as $item_room) {
	                            	echo "<tr>";
	                            	
	                            	
	                            	echo "<td>".$item_room->erp_hotel_name."</td>";
	                            	
	                            	echo "<td>".$item_room->rooms_count."</td>";
	                            	
		        					if(name()=='name_la') {
		                            	echo   "<td>".$item_room->hotelroomsize_name_la."</td>" ;
		                            } else {
		                            	echo   "<td>".$item_room->hotelroomsize_name_ar."</td>" ;
		                            } 
		                            
		                            echo "<td>".$item_room->entry_date."<br/>".$item_room->exit_date."</td>";
		                            
		                            if(name()=='name_la') {
		                            	$arr_required_rooms[$item_room->hotelroomsize_name_la]=$item_room->rooms_count;
		                            } else {
		                            	$arr_required_rooms[$item_room->hotelroomsize_name_ar]=$item_room->rooms_count;
		                            }
		                            echo "</tr>";
	        					}
	        					
	        					echo "</table>";
                            ?>
                        </td>
                        <td>
                        <?php 
                        // Hotel Availability
                        $arr_available_rooms=array();
                        
                        echo"<table>";
                        
                        foreach($item_rooms as $item_room) {
                        $this->hotel_availability_search_model->main_date_from=$item->arrival_date;
        				$this->hotel_availability_search_model->main_date_to=$item->departure_date;
         				$this->hotel_availability_search_model->city_id=$item->erp_cities_id;
				        
         				$reservation_hotels_ids=array($item->erp_hotels_id);
				        if($item->erp_hotels_id_alternative!='') {
				        	$reservation_hotels_ids[]=$item->erp_hotels_id_alternative;
				        }
         				$this->hotel_availability_search_model->hotel_id=$reservation_hotels_ids;
				        
         				//$this->hotel_availability_search_model->level=$item->erp_hotel_levels_id;
         				if($item->distance_from>0) {
         				$this->hotel_availability_search_model->distance_from_haram=$item->distance_from;
         				}
         				if($item->distance_to>0) {
				        $this->hotel_availability_search_model->distance_to_haram=$item->distance_to;
         				}
         				/*
         				if($item->price_from>0) {
				        $this->hotel_availability_search_model->price_from=$item->price_from;
         				}
         				if($item->price_to>0) {
         				$this->hotel_availability_search_model->price_to=$item->price_to;
         				}
         				if($item->erp_currencies_id!=0 && $item->erp_currencies_id!='') {
         				$this->hotel_availability_search_model->currency=$item->erp_currencies_id;
         				}
         				*/
         				
         				//------------------- Nationalities, Services, Advantages ---------------------------
         				$this->erp_hotels_reservation_orders_model->nationalities_erp_hotels_reservation_orders_id=$item->erp_hotels_reservation_orders_id;
				        $this->erp_hotels_reservation_orders_model->advantages_erp_hotels_reservation_orders_id=$item->erp_hotels_reservation_orders_id;
				        $this->erp_hotels_reservation_orders_model->room_services_erp_hotels_reservation_orders_id=$item->erp_hotels_reservation_orders_id;
				        
				        $reservation_nationalities_rows = $this->erp_hotels_reservation_orders_model->getNationalities();
                        $reservation_nationalities_ids=array();
				        foreach($reservation_nationalities_rows as $reservation_nationalities_row) {
				        	$reservation_nationalities_ids[]=$reservation_nationalities_row->erp_nationalities_id;
				        }
         				$this->hotel_availability_search_model->nationalities=$reservation_nationalities_ids;
         				
         				$reservation_hotel_features_rows = $this->erp_hotels_reservation_orders_model->getHotelAdvantages();
                        $reservation_hotel_features_ids=array();
				        foreach($reservation_hotel_features_rows as $reservation_hotel_features_row) {
				        	$reservation_hotel_features_ids[]=$reservation_hotel_features_row->erp_hotel_advantages_id;
				        }
         				$this->hotel_availability_search_model->hotel_features=$reservation_hotel_features_ids;
         				
         				
         				$reservation_inclusive_service_rows = $this->erp_hotels_reservation_orders_model->getRoomServices();
                        $reservation_inclusive_service_ids=array();
				        foreach($reservation_inclusive_service_rows as $reservation_inclusive_service_row) {
				        	$reservation_inclusive_service_ids[]=$reservation_inclusive_service_row->erp_room_services_id;
				        }
         				$this->hotel_availability_search_model->inclusive_service=$reservation_inclusive_service_ids;
				        //-----------------------------------------------------------------------------------
         				
         				
         				
         				//$this->hotel_availability_search_model->rooms=$item_room->rooms_count;
				        
         				
				        $this->hotel_availability_search_model->date_from=$item_room->entry_date;
         				$this->hotel_availability_search_model->date_to=$item_room->exit_date;
				        $this->hotel_availability_search_model->housing_type_id=$item_room->erp_housingtypes_id;
         				$this->hotel_availability_search_model->room_type_id=$item_room->erp_hotelroomsizes_id;
         				
         				$this->hotel_availability_search_model->price_from=$item_room->price_from;
         				$this->hotel_availability_search_model->price_to=$item_room->price_to;
         				
         				$availability_rooms=$this->hotel_availability_search_model->get();
				        
         				//echo $this->hotel_availability_search_model->db->last_query(); exit;
         				
         				
         				echo "<tr>";
         				
         				//echo "<td>".$item_room->entry_date."<br/>".$item_room->exit_date."</td>";
         				
         				if(name()=='name_la') {
		                  echo   "<td>".$item_room->hotelroomsize_name_la."<br/><br/></td>" ;
		                  } else {
		                   echo  "<td>".$item_room->hotelroomsize_name_ar."<br/><br/></td>" ;
		                  } 
		        		
		        		if(isset($availability_rooms[0])) {
		                	echo "<td><a href='".site_url("hotels_reservation_orders/available_rooms_popup/$item->erp_hotels_reservation_orders_id/$item_room->erp_hotelroomsizes_id")."'  class='fancybox fancybox.iframe'>".$availability_rooms[0]->available_count .'</a></td>';
		        		} else {
		        			echo '<td>0</td>';
		        		}
		        		
		        		if(isset($availability_rooms[0])) {
	                        if(name()=='name_la') {
	                            $arr_available_rooms[$item_room->hotelroomsize_name_la]=$availability_rooms[0]->available_count;
	                         } else {
	                            $arr_available_rooms[$item_room->hotelroomsize_name_ar]=$availability_rooms[0]->available_count;
	                         }
			        		} else {
				        		if(name()=='name_la') {
		                            $arr_available_rooms[$item_room->hotelroomsize_name_la]=0;
		                         } else {
		                            $arr_available_rooms[$item_room->hotelroomsize_name_ar]=0;
		                         }
			        		}
		        		
		        		echo "</tr>";
		        		
                        }
                        
                        echo "</table>";
         
                        ?>
                        </td>
                        
                        <td>
                        <?php 
                        echo "<table>";
                        foreach($arr_available_rooms as $available_key=>$available_value) {
                        	echo "<tr>";
                        	foreach($arr_required_rooms as $required_key=>$required_value) {
	                        	if($available_key==$required_key) {
	                        		$reset_to_complete=$required_value-$available_value;
	                        		if($reset_to_complete<0) {
	                        			$reset_to_complete=0;
	                        		}
	                        		
	                        		echo "<td>".$required_key.'<br/><br/></td><td>'.$reset_to_complete.'</td>';
	                        	}
                        	}
                        	echo "</tr>";
                        }
                        echo "</table>";
                        ?>
                        </td>
                        
                        <td>
                        <?php echo $item->order_status_name; ?>
                        </td>
                        
                        <td class="TAC">
                        <a href="<?= site_url('hotels_reservation_orders/pdf_export/' . $item->erp_hotels_reservation_orders_id) ?>" target="_blank" class="icon-book"></a>
                        
                        <a href="<?= site_url('hotels_reservation_orders/view/' . $item->erp_hotels_reservation_orders_id) ?>"><span class="icon-search"></span></a>
                        </td>
                    </tr>
                    <? } ?>
                <? } ?> 
                </tbody>
            </table>
            
            
            
        </div>
<!--        <div><?= $pagination ?></div>-->
    </div>
</div>

<script>
    $(document).ready(function() {
        $(".myTable").dataTable(
                {bSort: true,
                    bAutoWidth: true,
                    "iDisplayLength": false, // can be removed for basic 10 items per page
                    "sPaginationType": false,
                    "bPaginate": false,
                    "bInfo": false}
        );
    });
</script>
<script>
    	//By Gouda
        //$(document).ready(function() {
            $(".myTable tbody tr").css("cursor", "move");
            // Return a helper with preserved width of cells
            var fixHelper = function(e, ui) {
                ui.children().each(function() {
                    $(this).width($(this).width());
                });
                return ui;
            };

            $('.sortable').sortable({
                update: function() {
                    var stringDiv = "";
                    
                    $(".myTable tbody").children().each(function(i) {
                        var tr = $(this);
                        stringDiv += " " + tr.attr("id") + '=' + (i+1) + '&';
                    });

                    $.ajax({
                        type: "POST",
                        url: "<?php echo site_url('hotels_reservation_orders/incoming_sort'); ?>/" ,
                        data: stringDiv
                    });
                }
            });
            $(".sortable").disableSelection();
        //});
</script>

<script>
$(document).ready(function() {
    $('.fancybox').fancybox({
        afterClose: function() {
            //location.reload();
        }
    });
});
</script>

