<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); ?>
<link href="<?= CSS ?>/stylesheets.css" rel="stylesheet" type="text/css" />
<? if (lang('global_lang') == 'ar'): ?>
    <link href="<?= CSS ?>/stylesheet-ar.css" rel="stylesheet" type="text/css" />
<? else: ?><? endif ?>
<script type="text/javascript" src='<?= JS ?>/plugins/jquery/jquery-1.9.1.min.js'></script>
<script type="text/javascript" src='<?= JS ?>/plugins/jquery/jquery-ui-1.10.1.custom.min.js'></script>
<script type="text/javascript" src='<?= JS ?>/plugins/bootstrap/bootstrap.min.js'></script>
<link href="<?= CSS_JS ?>/dateandtime/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src='<?= CSS_JS ?>/dateandtime/bootstrap-datetimepicker.min.js'></script>
<style>
body{
height: auto!important;
overflow: auto !important;
background:none transparent !important;
overflow: hidden;
min-height: 1%;
}

.row-fluid{
padding-top: 5px !important;
padding-bottom: 5px !important;
}
</style>




<?php echo  form_open_multipart(false, 'id="frm_room_details_popup" ') ?>



<div class="">
    
	<div class="modal-header"> 
	<?php echo  lang('rooms') ?> 
    </div>
        
    <div class="modal-body">  
       
        
        
        <div class="row-fluid">
            <div class="span12">
               
       <div class="span11">
		<div class="">
        <div class="" >
            <table cellpadding="0" cellspacing="0">
                <thead>
                    <tr>
                        <th><?php echo  lang('hotel') ?></th>
                        <th><?php echo  lang('address') ?></th>
                        <th><?php echo  lang('distance_from') ?></th>
                        <th><?php echo  lang('hotel_level') ?></th>
                        <th><?php echo  lang('price') ?></th>
                    </tr>
                </thead>
                <tbody id="had">
                <?php
               if(count($item)>0) { 
                	
                	// Hotel Availability
                        $arr_available_rooms=array();
                        
                        
                        $this->hotel_availability_search_model->main_date_from=$item->arrival_date;
        				$this->hotel_availability_search_model->main_date_to=$item->departure_date;
         				$this->hotel_availability_search_model->city_id=$item->erp_cities_id;
				        
         				$reservation_hotels_ids=array($item->erp_hotels_id);
				        if($item->erp_hotels_id_alternative!='') {
				        	$reservation_hotels_ids[]=$item->erp_hotels_id_alternative;
				        }
         				$this->hotel_availability_search_model->hotel_id=$reservation_hotels_ids;
				        
         				//$this->hotel_availability_search_model->level=$item->erp_hotel_levels_id;
         				if($item->distance_from>0) {
         				$this->hotel_availability_search_model->distance_from_haram=$item->distance_from;
         				}
         				if($item->distance_to>0) {
				        $this->hotel_availability_search_model->distance_to_haram=$item->distance_to;
         				}
         				
         				/*
         				if($item->price_from>0) {
				        $this->hotel_availability_search_model->price_from=$item->price_from;
         				}
         				if($item->price_to>0) {
         				$this->hotel_availability_search_model->price_to=$item->price_to;
         				}
         				
         				if($item->erp_currencies_id!=0 && $item->erp_currencies_id!='') {
         				$this->hotel_availability_search_model->currency=$item->erp_currencies_id;
         				}
         				*/
         				
         				//------------------- Nationalities, Services, Advantages ---------------------------
         				$this->erp_hotels_reservation_orders_model->nationalities_erp_hotels_reservation_orders_id=$item->erp_hotels_reservation_orders_id;
				        $this->erp_hotels_reservation_orders_model->advantages_erp_hotels_reservation_orders_id=$item->erp_hotels_reservation_orders_id;
				        $this->erp_hotels_reservation_orders_model->room_services_erp_hotels_reservation_orders_id=$item->erp_hotels_reservation_orders_id;
         				
				        
				        $nationalities_array=array();
				        $nationalities_rows=$this->erp_hotels_reservation_orders_model->getNationalities();
				        foreach($nationalities_rows as $nationalities_row) {
				        	$nationalities_array[]=$nationalities_row->erp_nationality_id;
				        }
				        $this->hotel_availability_search_model->nationalities=$nationalities_array;
				        
				        
				        $advantages_array=array();
				        $advantages_rows=$this->erp_hotels_reservation_orders_model->getHotelAdvantages();
				        foreach($advantages_rows as $advantages_row) {
				        	$advantages_array[]=$advantages_row->erp_hotel_advantages_id;
				        }
				        $this->hotel_availability_search_model->hotel_features=$advantages_array;
				        

				        
				        
				        $inclusive_services_array=array();
				        $inclusive_services_rows=$this->erp_hotels_reservation_orders_model->getRoomServices();
				        foreach($inclusive_services_rows as $inclusive_services_row) {
				        	$inclusive_services_array[]=$inclusive_services_row->erp_room_services_id;
				        }
				        $this->hotel_availability_search_model->inclusive_service=$inclusive_services_array;
				        
				        //-----------------------------------------------------------------------------------
         				
         				
         				
         				//$this->hotel_availability_search_model->rooms=$item_room->rooms_count;
				        
				        $this->hotel_availability_search_model->date_from=$item->room_entry_date;
         				$this->hotel_availability_search_model->date_to=$item->room_exit_date;

         				$this->hotel_availability_search_model->housing_type_id=$item->room_erp_housingtypes_id;
         				$this->hotel_availability_search_model->room_type_id=$item->room_erp_hotelroomsizes_id;
         				
         				$this->hotel_availability_search_model->price_from=$item->room_price_from;
         				$this->hotel_availability_search_model->price_to=$item->room_price_to;
         				
         				
         				$availability_rooms=$this->hotel_availability_search_model->get();
				        
         				//echo $this->hotel_availability_search_model->db->last_query(); exit;
         				
         				foreach($availability_rooms as $availability_room) {
         				?>
         					<tr>
         					<td>
                            <?php echo   $availability_room->name_ar ; ?>
                        	</td>
                        	<td>
                            <?php echo   $availability_room->address_ar ; ?>
                        	</td>
                        	<td>
                            <?php echo   $availability_room->distance ; ?>
                        	</td>
                        	<td>
                            <?php echo   $availability_room->hotellevel ; ?>
                        	</td>
                        	<td>
                            <?php echo   $availability_room->sale_price ; ?>
                        	</td>
                        	</tr>

         				<?php 
         				}
                        
                	
                	}
                ?> 
                </tbody>
            </table>
    </div>
</div>
    
    
       
                </div>
                
			</div>
        </div>
        
                
    </div>
</div>



<?php echo  form_close() ?>


<div class="footer"></div>



<script type="text/javascript">
$(document).ready(function() {
    var config = {
        '.chosen-select': {width: "100%"},
        '.chosen-select-deselect': {allow_single_deselect: true},
        '.chosen-select-no-single': {disable_search_threshold: 10},
        '.chosen-select-no-results': {no_results_text: 'Oops, nothing found!'},
        '.chosen-select-width': {width: "95%"}
    }
    for (var selector in config) {
        $(selector).chosen(config[selector]);
    }


    // binds form submission and fields to the validation engine
    $("#frm_room_details_popup").validationEngine({
            prettySelect : true,
            useSuffix: "_chosen",
            promptPosition : "topRight:-150"
            //promptPosition : "bottomLeft"
    });

 
});


</script>
