<html>
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8">
	<title><?php echo lang('hotel_reservation_order') ?></title>
                <style>
            table.tbl{
                border-width: 1px;
                border-top-color: #000;
            border-left-color: #000;
            border-bottom-color: #000;
            border-right-color: #000;
            text-align: center;
            direction: rtl;
            }
            .tbl td,th,caption {
                border-width: 1px;
            border-top-color: #000;
            border-left-color: #000;
            border-bottom-color: #000;
            border-right-color: #000;
            font-weight: bold;
            }
            
            .ftr td,th,caption {
                border-width: 0px;
                border-color: transparent;
            }
            .ftr tr {
                border-width: 1px;
            }
        </style>
</head>
<body lang="en-US">
    <p style="height: 250px">
        <br />&nbsp;<br />&nbsp;<br />&nbsp;<br />
    </p>
<p dir="RTL" align="center">
    طلب حجز اتاحة فندقية <br />
    (  <?php echo $item->erp_hotels_reservation_orders_id; ?>  )
</p>
<p dir="RTL" align="right">
    السادة : <?php
                    if ($item->erp_company_types_id == 3) {
                         echo $item->{'to_ea_'.name()};
                    } else if ($item->erp_company_types_id == 5) {
                        echo $item->{'to_hm_'.name()};
                    } else {
                        echo $item->{'to_uo_'.name()};
                    }
                    ?> 
</p>
<p dir="RTL" align="right">
    عناية الاستاذ/ة :.....................................
</p>
<p dir="RTL" align="right">
    التاريخ : <?php echo $item->order_date; ?>
</p>

<p dir="RTL" align="right">
    تحية طيبة وبعد ,,,
</p>
<p dir="RTL" align="right">
    برجاء من سيادتكم التكرم بحجز الغرف التالية
</p>

<div align="right">
    <table class="tbl" dir="rtl" cellpadding="0" cellspacing="0" border="0">
        <tbody>
            <tr>
                <td rowspan="2" valign="top">
                        <strong>الفندق</strong>
                </td>
                <td colspan="<?= count($erp_hotelroomsizes) ?>" width="<?= count($erp_hotelroomsizes)*35 ?>" valign="top" align="center" >
                        <strong>عدد الغرف</strong>
                </td>
                <td rowspan="2" valign="top" align="center">
                        <strong>اجمالي عدد الغرف</strong>
                </td>
                <td rowspan="2" valign="top">
                        <strong>تاريخ الدخول</strong>
                </td>
                <td rowspan="2" valign="top">
                        <strong>عدد الليالي</strong>
                </td>
                <td rowspan="2" valign="top" >
                        <strong>تاريخ الخروج</strong>
                </td>
                <td rowspan="2" valign="top" width="150" >
                        <strong>ملاحظات</strong>
                </td>
            </tr>
            <tr style="background-color: #eee;">
                <?php foreach ($erp_hotelroomsizes as $erp_hotelroomsize) : ?>
                <td valign="top" width="35">
                        <?php echo $erp_hotelroomsize->{name()} ?>
                </td>
                <?php endforeach ?>

            </tr>
            <?php foreach ($item_rooms as $hotel) :?>
            <tr><?php $roomscount = 0 ?>
                <td valign="top">
                        <strong><?php echo $hotel['data']->erp_hotel_name."  - ".$hotel['data']->erp_city_name ?></strong>
                </td>
                <?php foreach ($erp_hotelroomsizes as $erp_hotelroomsize) : ?>
                <td valign="top">
                        <?php echo ($hotel['rooms'][$erp_hotelroomsize->erp_hotelroomsize_id])?$hotel['rooms'][$erp_hotelroomsize->erp_hotelroomsize_id]:''; ?>
                    <?php $roomscount += ($hotel['rooms'][$erp_hotelroomsize->erp_hotelroomsize_id]) ? $hotel['rooms'][$erp_hotelroomsize->erp_hotelroomsize_id]:0; ?>
                </td>
                <?php endforeach ?>
                <td valign="top">
                    <?php echo $roomscount; ?>
                </td>
                <td valign="top"><?php echo $hotel['data']->entry_date ?></td>
                <td valign="top"><?php echo $hotel['data']->nights_count ?></td>
                <td valign="top"><?php echo $hotel['data']->exit_date ?></td>
                <td valign="top"><?php echo $hotel['data']->notes ?></td>
            </tr>
            <?php endforeach ?>
        </tbody>
    </table>
</div>

<p dir="RTL" align="right">
    ولكم جزيل الشكر والاحترام
</p>
<p  align="left">
    قسم المبيعات
</p>
</body>
</html>