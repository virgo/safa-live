<div class="resalt-group"><div class="wizerd-div"><a><?php echo lang('meals') ?><div class='coll_open' id="coll_meals"></div></a></div>
    <script>meals_counter = 0</script>
    <div class="row-fluid" id="container_meals">
        <div class="table-responsive">
            <table cellpadding="0" cellspacing="0">
                <thead>
                    <tr>
                        <th><?php echo  lang('hotel') ?></th>
                        <th><?php echo lang('meal') ?></th>
                        <th><?php echo lang('count') ?></th>
                        <th><?php echo lang('price') ?></th>
                        <!--<th>
                            <a class="btn " title="<?php echo lang('add') ?>" href="javascript:void(0)" onclick="add_meals('N' + meals_counter++); load_multiselect()">
                                <?php echo lang('add') ?>    
                            </a>
                        </th>
                    --></tr>
                </thead>
                <tbody class="meals" id='meals'>
                    <? if (isset($item_meals)) { ?>
                        <? if (check_array($item_meals)) { ?>
                            <? foreach ($item_meals as $item_meal) { ?>
                                <tr rel="<?php echo $item_meal->erp_hotels_reservation_orders_meals_id ?>">
                                    <td>
                                        <?php
                                        echo form_dropdown('meals_erp_hotel_id[' . $item_meal->erp_hotels_reservation_orders_meals_id . ']'
                                                , $erp_hotels, set_value('meals_erp_hotel_id[' . $item_meal->erp_hotels_reservation_orders_meals_id . ']', $item_meal->erp_hotel_id)
                                                , 'class="chosen-select chosen-rtl input-full" disabled="disabled"  id="meals_erp_hotel_id[' . $item_meal->erp_hotels_reservation_orders_meals_id . ']" tabindex="4" ') ?>
                                    </td>
                                    <td>
                                        <?php
                                        echo form_dropdown('meals_erp_meals[' . $item_meal->erp_hotels_reservation_orders_meals_id . ']'
                                                , $erp_meals, set_value('meals_erp_meals[' . $item_meal->erp_hotels_reservation_orders_meals_id . ']', $item_meal->erp_meals_id)
                                                , 'class="chosen-select chosen-rtl input-full" disabled="disabled"  id="meals_erp_meals[' . $item_meal->erp_hotels_reservation_orders_meals_id . ']" tabindex="4" ') ?>
                                    </td>
                                    <td><?php
                                        echo form_input('meals_meals_count[' . $item_meal->erp_hotels_reservation_orders_meals_id . ']'
                                                , set_value('meals_meals_count[' . $item_meal->erp_hotels_reservation_orders_meals_id . ']', $item_meal->meals_count)
                                                , 'class="input-huge" style="width:250px"  readonly="readonly"') ?>
                                    </td>
                                    <td><?php
                                        echo form_input('meals_price[' . $item_meal->erp_hotels_reservation_orders_meals_id . ']'
                                                , set_value('meals_price[' . $item_meal->erp_hotels_reservation_orders_meals_id . ']', $item_meal->price)
                                                , 'class="input-huge" style="width:250px"  readonly="readonly"') ?>
                                    </td>
<!--                                    <td class="TAC">-->
<!--                                        <a href="javascript:void(0)" onclick="delete_meals(<?php echo $item_meal->erp_hotels_reservation_orders_meals_id ?>, true)"><span class="icon-trash"></span></a>-->
<!--                                    </td>-->
                                </tr>
        <? } ?>
    <? } ?>
<? } ?>
                </tbody>
            </table>
        </div>

    </div>
</div>

<script>
    function load_multiselect() {
        var config = {
            '.chosen-select': {width: "100%"},
            '.chosen-select-deselect': {allow_single_deselect: true},
            '.chosen-select-no-single': {disable_search_threshold: 10},
            '.chosen-select-no-results': {no_results_text: 'Oops, nothing found!'},
            '.chosen-select-width': {width: "95%"}
        }
        for (var selector in config) {
            $(selector).chosen(config[selector]);
        }
    }
    function add_meals(id) {



        var new_row = [
                "<tr rel=\"" + id + "\">",
                "    <td>",
                "       <select name=\"meals_erp_hotel_id[" + id + "]\" class=\"chosen-select chosen-rtl input-full\" id=\"meals_erp_hotel_id_" + id + "\" tabindex=\"4\">",
<? foreach ($erp_hotels as $key => $value): ?>
            "           <option value=\"<?= $key ?>\"><?= $value ?></option>",
<? endforeach ?>
        "    </td>",
                "    <td>",
                "       <select name=\"meals_erp_meals[" + id + "]\" class=\"chosen-select chosen-rtl input-full\" id=\"meals_erp_meals_" + id + "\" tabindex=\"4\">",
<? foreach ($erp_meals as $key => $value): ?>
            "           <option value=\"<?= $key ?>\"><?= $value ?></option>",
<? endforeach ?>
        "    </td>",
                '     <td><?php echo form_input("meals_meals_count[' + id + ']", FALSE, 'class="validate[required] input-huge" ') ?>',
     "    </td>",
                '     <td><?php echo form_input("meals_price[' + id + ']", FALSE, 'class="validate[required] input-huge" ') ?>',
     "    </td>",
                "    <td class=\"TAC\">",
                "        <a href=\"javascript:void(0)\" onclick=\"delete_meals('" + id + "')\"><span class=\"icon-trash\"></span></a>",
                "    </td>",
                "</tr>"
        ].join("\n");
                $('#meals').append(new_row);

    }
    function delete_meals(id, database)
    {
        if (typeof database == 'undefined')
        {
            $('.meals').find('tr[rel="' + id + '"]').remove();
        }
        else
        {
            $('.meals').find('tr[rel=' + id + ']').remove();
            var hidden_input = '<input type="hidden" name="meals_remove[]" value="' + id + '" />';
            $('#meals').append(hidden_input);
        }
    }

</script>

