<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); ?>
<link href="<?= CSS ?>/stylesheets.css" rel="stylesheet" type="text/css" />
<? if (lang('global_lang') == 'ar'): ?>
    <link href="<?= CSS ?>/stylesheet-ar.css" rel="stylesheet" type="text/css" />
<? else: ?><? endif ?>
<script type="text/javascript" src='<?= JS ?>/plugins/jquery/jquery-1.9.1.min.js'></script>
<script type="text/javascript" src='<?= JS ?>/plugins/jquery/jquery-ui-1.10.1.custom.min.js'></script>
<script type="text/javascript" src='<?= JS ?>/plugins/bootstrap/bootstrap.min.js'></script>
<link href="<?= CSS_JS ?>/dateandtime/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src='<?= CSS_JS ?>/dateandtime/bootstrap-datetimepicker.min.js'></script>
<style>
body{
height: auto!important;
overflow: auto !important;
background:none transparent !important;
overflow: hidden;
min-height: 1%;
}

.row-fluid{
padding-top: 5px !important;
padding-bottom: 5px !important;
}
</style>




<?php echo  form_open_multipart(false, 'id="frm_room_details_popup" ') ?>



<div class="">
    
	<div class="modal-header"> 
	<?php echo  lang('rooms') ?> 
    </div>
        
    <div class="modal-body">  
       
        
        
        <div class="row-fluid">
            <div class="span12">
               
       <div class="span11">
		<div class="">
        <div class="" >
            <table cellpadding="0" cellspacing="0">
                <thead>
                    <tr>
                        <th><?php echo  lang('room') ?></th>
                        <th><?php echo  lang('rooms_count') ?></th>
                    </tr>
                </thead>
                <tbody id="had">
                <? if(ensure($items)) { ?>
                    <? foreach($items as $item) {
                    ?>
                    <tr rel="<?php echo  $item->erp_hotels_reservation_orders_rooms_id; ?>">

                        <td>
                            <?php 
                            if(name()=='name_la') {
                            	echo   $item->hotelroomsize_name_la ;
                            } else {
                            	echo   $item->hotelroomsize_name_ar ;
                            } 
                            ?>
                        </td>
                        
                        <td>
                             <?php echo   $item->rooms_count ; ?>
                        </td>
                        
                    </tr>
                    <? } ?>
                <? } ?> 
                </tbody>
            </table>
    </div>
</div>
    
    
       
                </div>
                
			</div>
        </div>
        
                
    </div>
</div>



<?php echo  form_close() ?>


<div class="footer"></div>



<script type="text/javascript">
$(document).ready(function() {
    var config = {
        '.chosen-select': {width: "100%"},
        '.chosen-select-deselect': {allow_single_deselect: true},
        '.chosen-select-no-single': {disable_search_threshold: 10},
        '.chosen-select-no-results': {no_results_text: 'Oops, nothing found!'},
        '.chosen-select-width': {width: "95%"}
    }
    for (var selector in config) {
        $(selector).chosen(config[selector]);
    }


    // binds form submission and fields to the validation engine
    $("#frm_room_details_popup").validationEngine({
            prettySelect : true,
            useSuffix: "_chosen",
            promptPosition : "topRight:-150"
            //promptPosition : "bottomLeft"
    });

 
});


</script>
