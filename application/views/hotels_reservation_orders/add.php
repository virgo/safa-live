<style>
    .coll_open {
        cursor: pointer;
    }
    .coll_close {
        cursor: pointer;
    }
    .wizerd-div {
        border-bottom: medium none !important;
        margin: -18px 0 20px;
        padding-top: 0;
    }
    .wizerd-div a {
        background: none repeat scroll 0 0 #FAFAFA;
        border: 1px solid #D5D6D6;
        border-radius: 49px;
        color: #663300;
        display: inline-block;
        margin: -6px 5px -17px -8px;
        padding: 10px 12px 8px;
    }
    a{ color: #C09853;}
    .resalt-group {
        margin: 18px 0.5% 0.5%;
        padding: 0.5%;
        width: 99%;
    }
    th a.btn, th input[type="button"], th input[type="submit"], th button {
        margin: 0;
        padding: 4px 12px;
    }
    .coll_close, .coll_open {
        margin-top: 0;
    }
    .chosen-container{
        margin-top: 4px;   
    }


    .fancybox-outer, .fancybox-inner {
        overflow-y: auto !important;
    }
    .widget{width:98%;}

</style>
<? if (!isset($tripid) && !isset($packageid)) : ?>
    <div class="widget">

        <div class="path-container Fright">
            <div class="path-name Fright">
                <a href="<?php echo site_url() ?>"><?php echo lang('global_system_management') ?></a>
            </div>
            <div class="path-arrow Fright"></div>
            <div class="path-name Fright"> <a href="<?php echo site_url('hotels_reservation_orders') ?>"><?php echo lang('title') ?></a></div>

            <div class="path-arrow Fright"></div>
            <div class="path-name Fright"><?php echo lang('add_saving_availability_title') ?></div>

        </div>
    </div>
<? endif ?>

<?php echo form_open_multipart(false, 'id="frm_hotels_reservation_orders" ') ?>

<? if (isset($tripid)) : ?><input type="hidden" name="tripid" value="<?= $tripid ?>" /><? endif ?>
<? if (isset($packageid)) : ?><input type="hidden" name="packageid" value="<?= $packageid ?>" /><? endif ?>
<div class="widget">
    <div class="widget-header">

        <div class="widget-header-icon Fright">
            <span class="icos-pencil2"></span>
        </div>

        <div class="widget-header-title Fright">
            <?php echo lang('add_saving_availability_title') ?>  

        </div>

    </div>
    <div class="resalt-group"style="margin-top: 30px;">
        <div class="wizerd-div"><a><?php echo lang('main_data') ?><div class='coll_open' id="coll_main_data"></div></a></div>
        <div class="row-fluid" id="container_main_data">  
            <? if (validation_errors() && isset($_POST['erp_company_id'])) { ?>
                <?php echo validation_errors(); ?>        
            <? } ?>

            <div class="row-form">
                <div class="span3">
                    <div class="span6">
                        <div class="span10 TAL Pleft10">
                            <?php echo lang('order_no') ?>
                        </div>
                        <div class="span10">
                            <?= form_input('order_no', set_value('order_no', $current_order_no), 'class="validate[required] input-huge" disabled="disabled" ') ?>
                        </div>   
                    </div>
                    <div class="span6">
                        <div class="span10 TAL Pleft10">
                            <?php echo lang('order_date') ?>
                        </div>
                        <div class="span10">
                            <?php $now_datetime = date('Y-m-d', time()); ?>
                            <?= form_input('order_date', set_value('order_date', $now_datetime), 'class="validate[required] input-huge order_date"') ?>
                            <script>
                                $('.order_date').datepicker({
                                    dateFormat: "yy-mm-dd",
                                    controlType: 'select',
                                    timeFormat: 'HH:mm'
                                });
                            </script>
                        </div>  
                    </div>



                </div>
                <div class="span3">
                    <div class="span6">
                        <div class="span10 TAL Pleft10">
                            <?php echo lang('arrival_date') ?>
                        </div>
                        <div class="span10">
                            <?= form_input('arrival_date', set_value('arrival_date', ''), 'class="input-huge arrival_date" id="arrival_date"') ?>
                            <script>
                                $('.arrival_date').datepicker({
                                    dateFormat: "yy-mm-dd",
                                    controlType: 'select',
                                    timeFormat: 'HH:mm',
                                    onClose: function(selectedDate) {
                                        //$('.departure_date').datepicker("option", "minDate", selectedDate);
                                    }
                                });
                            </script>
                        </div>
                    </div>
                    <div class="span6">
                        <div class="span10 TAL Pleft10">
                            <?php echo lang('departure_date') ?>
                        </div>
                        <div class="span10">
                            <?= form_input('departure_date', set_value('departure_date', ''), 'class="input-huge departure_date" readonly="readonly" id="departure_date" ') ?>
                        </div>
                    </div>
                </div>
                <div class="span6">
                    <?php
                    if (session('ea_id')) {
                        ?>

                        <div class="span6">
                            <div class="span10 TAL Pleft10">
                                <?php echo lang('safa_uo_contract_id') ?>
                            </div>
                            <div class="span10">
                                <?php
                                $safa_uo_contract_id = 0; /*
                                  $this->contracts_model->by_eas_id = session('ea_id');
                                  $this->contracts_model->uo_contracts_eas_is_original_owner = 1;
                                  $ea_contracts_rows = $this->contracts_model->get_contract_ea();

                                  if (count($ea_contracts_rows) > 0) {
                                  $safa_uo_contract_id = $ea_contracts_rows[0]->safa_uo_contract_id;
                                  } */
                                ?>
                                <?php echo form_dropdown('safa_uo_contract_id', $uo_contracts, set_value('safa_uo_contract_id', $safa_uo_contract_id), 'class=" chosen-select chosen-rtl input-full"  tabindex="4" id="safa_uo_contract_id" ') ?>   
                            </div>
                        </div>

                        <?php
                    }
                    ?>
                    <div class="span6">
                        <div class="span10 TAL Pleft10">
                            <?php echo lang('contact_person') ?>
                        </div>
                        <div class="span10">
                            <?php echo form_input('contact_person', set_value("contact_person"), " style='' id='contact_person'  ") ?>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row-form">
                <?php
                if (isset($contract_row)) {
                    if (count($contract_row) > 0) {
                        if ($contract_row[0]->services_buy_other_hotel == 1) {
                            ?>
                            <div class="span4">
                                <div class="span4 TAL Pleft10">
                                    <?php echo lang('company_type') ?>
                                </div>
                                <div class="span8">
                                    <div class="span12"><?php echo form_radio('company_type', '2', true, 'id="company_type"'); ?> <?php echo lang('uo') ?><br/>
                                    </div>
                                    <div class="span12"><?php echo form_radio('company_type', '3', false, 'id="company_type"'); ?><?php echo lang('ea') ?><br/>
                                    </div>
                                    <div class="span12"><?php echo form_radio('company_type', '5', false, 'id="company_type"'); ?><?php echo lang('hm') ?><br/>
                                    </div>
                                </div>
                            </div>
                            <?php
                        } else if ($contract_row[0]->services_buy_uo_hotels == 1) {
                            ?>	
                            <div class="span4">
                                <div class="span4 TAL Pleft10">
                                    <?php echo lang('company_type') ?>
                                </div>
                                <div class="span8">
                                    <?php echo form_radio('company_type', '2', true, 'id="company_type"'); ?> <?php echo lang('uo') ?><br/>
                                    <!-- 
                                    <?php echo form_radio('company_type', '3', false, 'id="company_type"'); ?><?php echo lang('ea') ?><br/>
                                    <?php echo form_radio('company_type', '5', false, 'id="company_type"'); ?><?php echo lang('hm') ?><br/>
                                    -->
                                </div>
                            </div>
                            <?php
                        } else {
                            session('error_message', lang('don_not_have_privilege_to_add_reservation_order'));
                            session('error_url', site_url('hotels_reservation_orders'));
                            session('error_title', lang('title'));
                            session('error_action', lang('edit_title'));
                            //redirect("error_message/show");
                        }
                    } else {
                        ?>
                        <div class="span4">
                            <div class="span4 TAL Pleft10">
                                <?php echo lang('company_type') ?>
                            </div>
                            <div class="span8">
                                <?php echo form_radio('company_type', '2', true, 'id="company_type"'); ?> <?php echo lang('uo') ?><br/>
                                <?php echo form_radio('company_type', '3', false, 'id="company_type"'); ?><?php echo lang('ea') ?><br/>
                                <?php echo form_radio('company_type', '5', false, 'id="company_type"'); ?><?php echo lang('hm') ?><br/>
                            </div>
                        </div>
                        <?php
                    }
                } else {
                    ?>
                    <div class="span4">
                        <div class="span4 TAL Pleft10">
                            <?php echo lang('company_type') ?>
                        </div>
                        <div class="span8">
                            <?php echo form_radio('company_type', '2', true, 'id="company_type"'); ?> <?php echo lang('uo') ?><br/>
                            <?php echo form_radio('company_type', '3', false, 'id="company_type"'); ?><?php echo lang('ea') ?><br/>
                            <?php echo form_radio('company_type', '5', false, 'id="company_type"'); ?><?php echo lang('hm') ?><br/>
                        </div>
                    </div>
                    <?php
                }
                ?>     

                <div class="span6">
                    <div class="span3 TAL Pleft10">
                        <?php echo lang('company') ?>
                    </div>
                    <div class="span8">
                        <?php echo form_dropdown('erp_company_id', (isset($companyid)) ? array($companyid => $uo_companies[$companyid]) : $uo_companies, set_value('erp_company_id', (isset($companyid)) ? $companyid : ''), 'class="validate[required] chosen-select chosen-rtl input-full"  tabindex="4" id="erp_company_id" data-placeholder="' . lang('global_all') . '"') ?>   
                    </div>
                </div>
            </div>  
        </div>
    </div>

    <? $this->load->view('hotels_reservation_orders/rooms') ?>
    <? $this->load->view('hotels_reservation_orders/meals') ?>

    <div class="row-fluid TAC">
        <input type="submit" class="btn" name="smt_send" value="<?php echo lang('send') ?>" style="margin:10px;padding: 5px;height: auto">
    </div>
</div>
<?php echo form_close() ?>
<div class="footer"></div>
<script type="text/javascript">
    $(document).ready(function() {
        var config = {
            '.chosen-select': {width: "100%"},
            '.chosen-select-deselect': {allow_single_deselect: true},
            '.chosen-select-no-single': {disable_search_threshold: 10},
            '.chosen-select-no-results': {no_results_text: 'Oops, nothing found!'},
            '.chosen-select-width': {width: "95%"}
        }
        for (var selector in config) {
            $(selector).chosen(config[selector]);
        }


        // binds form submission and fields to the validation engine
        $("#frm_hotels_reservation_orders").validationEngine({
            prettySelect: true,
            useSuffix: "_chosen",
            promptPosition: "topRight:-150"
                    //promptPosition : "bottomLeft"
        });


        $('#company_type').live('change', function() {
            var erp_company_type_id = $(this).val();
            var dataString = 'erp_company_type_id=' + erp_company_type_id;
            $.ajax({
                type: 'POST',
                url: '<?php echo base_url() . 'hotels_reservation_orders/getCompaniesByCompanyType'; ?>',
                data: dataString,
                cache: false,
                success: function(html) {
                    $("#erp_company_id").html(html);
                    $("#erp_company_id").trigger("chosen:updated");
                }
            });
        });


        $('#container_rooms').on('change', '#erp_cities_id', function() {
            var erp_cities_id = $(this).val();
            var hoteldd = $(this).parent().find(".erp_hotels");
            var dataString = 'erp_cities_id=' + erp_cities_id;
            $.ajax({
            type: 'POST',
<? if (isset($packageid)) : ?>
                url: '<?php echo base_url() . 'hotels_reservation_orders/getHotelsByCities/' . $packageid . '/'; ?>',
<? else : ?>
                url: '<?php echo base_url() . 'hotels_reservation_orders/getHotelsByCities'; ?>',
<? endif ?>
            data: dataString,
                    cache: false,
                    success: function(html) {
                    //alert(html);
                    $(hoteldd).html(html);
                    $(hoteldd).trigger("chosen:updated");
                    }
        });
    });

    $('#chk_alternative_hotel').change(function() {
        if ($('#chk_alternative_hotel').is(':checked')) {
            $("#erp_hotels_id_alternative").val('').trigger("chosen:updated");
            $("#erp_hotels_id_alternative").attr('disabled', false).trigger("chosen:updated");
        } else {
            $("#erp_hotels_id_alternative").attr('disabled', true).trigger("chosen:updated");
        }
    });
    });</script>
<script type="text/javascript">
            $(document).ready(function() {

        $("#all_nationalities_lnk").click(function() {
            $("#nationalities").val([<?php
foreach ($nationalities_rows as $nationalities_row) {
    echo("'$nationalities_row->erp_nationality_id',");
}
?>]);
            $("#nationalities").trigger("chosen:updated");
        });
        $("#arabic_nationalities_lnk").click(function() {
            $("#nationalities").val([<?php
foreach ($nationalities_rows as $nationalities_row) {
    if ($nationalities_row->arabian == 1) {
        echo("'$nationalities_row->erp_nationality_id',");
    }
}
?>]);
            $("#nationalities").trigger("chosen:updated");
        });
        $("#foreign_nationalities_lnk").click(function() {
            $("#nationalities").val([<?php
foreach ($nationalities_rows as $nationalities_row) {
    if ($nationalities_row->arabian == 0) {
        echo("'$nationalities_row->erp_nationality_id',");
    }
}
?>]);
            $("#nationalities").trigger("chosen:updated");
        });
        $("#clear_nationalities_lnk").click(function() {
            $("#nationalities").val('');
            $("#nationalities").trigger("chosen:updated");
        });

    });


</script>

<script type="text/javascript">
    $('div.widget-header a').click(function(evt) {
        evt.stopPropagation();
    });

    $("#coll_main_data").click(function() {
        $header = $(this);
        //getting the next element
        $content = $header.next();
        $content = $('#container_main_data');

        //open up the content needed - toggle the slide- if visible, slide up, if not slidedown.
        $content.slideToggle(500, function() {
            //execute this after slideToggle is done
            //change text of header based on visibility of content div
            $header.text(function() {
                //change text based on condition
                //return $content.is(":visible") ? "Collapse" : "Expand";
            });
        });

        if ($("#coll_main_data").prop("class") == 'coll_open') {
            $('#coll_main_data').removeClass('coll_open').addClass('coll_close');
        } else {
            $('#coll_main_data').removeClass('coll_close').addClass('coll_open');
        }

    });

    $("#coll_general_feature_for_hotel").click(function() {
        $header = $(this);
        //getting the next element
        $content = $header.next();
        $content = $('#container_general_feature_for_hotel');

        //open up the content needed - toggle the slide- if visible, slide up, if not slidedown.
        $content.slideToggle(500, function() {
            //execute this after slideToggle is done
            //change text of header based on visibility of content div
            $header.text(function() {
                //change text based on condition
                //return $content.is(":visible") ? "Collapse" : "Expand";
            });
        });

        if ($("#coll_general_feature_for_hotel").prop("class") == 'coll_open') {
            $('#coll_general_feature_for_hotel').removeClass('coll_open').addClass('coll_close');
        } else {
            $('#coll_general_feature_for_hotel').removeClass('coll_close').addClass('coll_open');
        }

    });

    $("#coll_rooms").click(function() {
        $header = $(this);
        //getting the next element
        $content = $header.next();
        $content = $('#container_rooms');

        //open up the content needed - toggle the slide- if visible, slide up, if not slidedown.
        $content.slideToggle(500, function() {
            //execute this after slideToggle is done
            //change text of header based on visibility of content div
            $header.text(function() {
                //change text based on condition
                //return $content.is(":visible") ? "Collapse" : "Expand";
            });
        });

        if ($("#coll_rooms").prop("class") == 'coll_open') {
            $('#coll_rooms').removeClass('coll_open').addClass('coll_close');
        } else {
            $('#coll_rooms').removeClass('coll_close').addClass('coll_open');
        }

    });

    $("#coll_meals").click(function() {
        $header = $(this);
        //getting the next element
        $content = $header.next();
        $content = $('#container_meals');

        //open up the content needed - toggle the slide- if visible, slide up, if not slidedown.
        $content.slideToggle(500, function() {
            //execute this after slideToggle is done
            //change text of header based on visibility of content div
            $header.text(function() {
                //change text based on condition
                //return $content.is(":visible") ? "Collapse" : "Expand";
            });
        });

        if ($("#coll_meals").prop("class") == 'coll_open') {
            $('#coll_meals').removeClass('coll_open').addClass('coll_close');
        } else {
            $('#coll_meals').removeClass('coll_close').addClass('coll_open');
        }
    });
</script>

<script>
    $(document).ready(function() {
        $('#safa_uo_contract_id').live('change', function() {
            var safa_uo_contract_id = $(this).val();
            var dataString = 'safa_uo_contract_id=' + safa_uo_contract_id;
            $.ajax({
                type: 'POST',
                url: '<?php echo base_url() . 'hotels_reservation_orders/get_uos_companies_by_safa_uo_contract_id'; ?>',
                data: dataString,
                cache: false,
                success: function(html) {
                    $("#erp_company_id").html(html);
                    $("#erp_company_id").trigger("chosen:updated");
                }
            });
        });

    });
</script>