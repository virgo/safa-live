<style>
    table tr th{background-color: #f1f1f1;}
    table td, table th{padding:1px!important;}
    th label,td label {
        display: inline-block!important;
        margin: 1px 0!important;
    }
    td table{
        border: none;

    }
    td {
        padding: 0!important;
    }
    table tr td{
        padding: 4px!important;
        border-bottom: 1px solid #ddd!important;
        background-color: #fff;
    }

    .fancybox-outer, .fancybox-inner {
        overflow-y: auto !important;
    }


    td.ui-selected {
        background-color: #D6B476;
    }

    .rotate{background-color: #fff;}
    .rotate label {-webkit-transform: rotate(-90deg); 
                   -moz-transform: rotate(-90deg);margin: 10px 0!important;
                   width: 26px;
    }

    .av-hotel,.ch-hotel,.cl-hotel,.na-hotel{min-height: 20px;min-width: 20px;box-shadow: none;/*border-left: 0;*/}
    .av-hotel img,.ch-hotel img,.cl-hotel img{cursor: pointer;}
    .av-hotel{background: #B8D776;}
    .ch-hotel{background-color:#DE856D;}

    .cl-hotel{background-color:#918183;}
    .plan {float: left;}


    .hotel{}
    .group{}

    label{margin: 0;padding: 5px;}
    .row-form > [class^="span"]{border: none;}

    .table-warp .box-shadow-last  {box-shadow: -12px 0 8px -4px rgba(0,0,0, 0.3);}
    .table-warp .box-shadow-first {box-shadow: 12px 0 15px -4px rgba(0,0,0, 0.3)}

    .table-warp .box-shadow-last-bottom  {box-shadow: -12px 0 8px -4px rgba(0,0,0, 0.3);}
    .table-warp .box-shadow-first-top {box-shadow: 0px -6px 10px 0px rgba(174, 187, 148, 0.79)}

    .table-warp th label {
        font-family: tahoma;
        font-size: 10px;
        font-weight: 900;
    }

    .calendars-month-header select{
        padding: 0;
    }

    .weekend {
        background-color: #666;
        color: #F1F1F1;
        text-shadow: 1px 1px 0 #000;
    }

</style>
<div class="resalt-group" >
    <div class="wizerd-div"><a><?php echo lang('rooms') ?> <div class='coll_open' id="coll_rooms"></div></a></div>
    <script>rooms_counter = 0</script>


    <div class="row-fluid" id="container_rooms">
        <div class="table-responsive">
            <table cellpadding="0" cellspacing="0">
                <thead>
                    <tr>
                        <th></th>
                        <th><?php echo lang('city') ?> / <?php echo  lang('hotel') ?></th>
                        <th><?php echo lang('housing_type') ?></th>
                        <th><?php echo lang('room_type') ?></th>
                        <th><?php echo lang('count') ?></th>
                        <th><?php echo lang('nights_count') ?></th>
                        <th><?php echo lang('arrival_date') ?></th>
                        <th><?php echo lang('departure_date') ?></th>
                        <th><?php echo lang('notes') ?></th>
                    </tr>
                </thead>
                <tbody class="rooms" id='rooms'>
                    <? if (isset($item_rooms)) { ?>
                        <? if (check_array($item_rooms)) { ?>
        <? foreach ($item_rooms as $item_room) { ?>
                                <tr rel="<?php echo $item_room->erp_hotels_reservation_orders_rooms_id ?>">
                                    <td><span class="availability availability_icon_<?php echo $item_room->erp_hotels_reservation_orders_rooms_id ?>"></span></td>
                                    <td>
                                        <?php echo form_dropdown('rooms_erp_cities_id['.$item_room->erp_hotels_reservation_orders_rooms_id.']', $erp_cities, set_value('erp_cities_id', $item_room->erp_cities_id ), 'class="validate[required] chosen-select chosen-rtl input-full" disabled="disabled" tabindex="4" id="erp_cities_id" data-placeholder="'.lang('global_all').'"') ?>
                                        <?php echo form_dropdown('rooms_erp_hotels_id['.$item_room->erp_hotels_reservation_orders_rooms_id.']', $erp_hotels, set_value('erp_hotels_id', $item_room->erp_hotels_id ), 'class=" chosen-select chosen-rtl input-full" disabled="disabled" tabindex="4" id="erp_hotels_id" ') ?>   
                                        <?php echo form_hidden('rooms_erp_cities_id['.$item_room->erp_hotels_reservation_orders_rooms_id.']',  $item_room->erp_cities_id ) ?>
                                        <?php echo form_hidden('rooms_erp_hotels_id['.$item_room->erp_hotels_reservation_orders_rooms_id.']', $item_room->erp_hotels_id ) ?>   
                                    </td>
                                    <td>
                                        <?php
                                        echo form_dropdown('rooms_erp_housingtypes_id[' . $item_room->erp_hotels_reservation_orders_rooms_id . ']'
                                                , $erp_housingtypes, set_value('rooms_erp_housingtypes_id[' . $item_room->erp_hotels_reservation_orders_rooms_id . ']', $item_room->erp_housingtypes_id)
                                                , 'class="chosen-select chosen-rtl input-full" disabled="disabled" id="rooms_erp_housingtypes_id_' . $item_room->erp_hotels_reservation_orders_rooms_id . '" tabindex="4" ');
                                        echo form_hidden('rooms_erp_housingtypes_id[' . $item_room->erp_hotels_reservation_orders_rooms_id . ']'
                                                , set_value('rooms_erp_housingtypes_id[' . $item_room->erp_hotels_reservation_orders_rooms_id . ']', $item_room->erp_housingtypes_id));
                                        ?>
                                    </td>
                                    <td>
                                        <?php
                                        echo form_dropdown('rooms_erp_hotelroomsizes_id[' . $item_room->erp_hotels_reservation_orders_rooms_id . ']'
                                                , $erp_hotelroomsizes, set_value('rooms_erp_hotelroomsizes_id[' . $item_room->erp_hotels_reservation_orders_rooms_id . ']', $item_room->erp_hotelroomsizes_id)
                                                , 'class="chosen-select chosen-rtl input-full" disabled="disabled" id="rooms_erp_hotelroomsizes_id_' . $item_room->erp_hotels_reservation_orders_rooms_id . '" tabindex="4" ');
                                        echo form_hidden('rooms_erp_hotelroomsizes_id[' . $item_room->erp_hotels_reservation_orders_rooms_id . ']'
                                                , set_value('rooms_erp_hotelroomsizes_id[' . $item_room->erp_hotels_reservation_orders_rooms_id . ']', $item_room->erp_hotelroomsizes_id));
                                        ?>
                                    </td>
                                    <td><?php
                                        echo form_input('rooms_rooms_count[' . $item_room->erp_hotels_reservation_orders_rooms_id . ']'
                                                , set_value('rooms_rooms_count[' . $item_room->erp_hotels_reservation_orders_rooms_id . ']', $item_room->rooms_count)
                                                , 'class="input-huge" readonly="readonly" style="width:100px"')
                                        ?>
                                    </td>
                                    <td>
                                        <?php
                                        echo form_input('rooms_nights_count[' . $item_room->erp_hotels_reservation_orders_rooms_id . ']'
                                                , set_value('rooms_nights_count[' . $item_room->erp_hotels_reservation_orders_rooms_id . ']', $item_room->nights_count)
                                                , 'class="input-huge" readonly="readonly" style="width:100px"')
                                        ?>
                                    </td>

                                    <td>
                                        <?php
                                        echo form_input('rooms_entry_date[' . $item_room->erp_hotels_reservation_orders_rooms_id . ']'
                                                , set_value('rooms_entry_date[' . $item_room->erp_hotels_reservation_orders_rooms_id . ']', $item_room->entry_date)
                                                , 'class="input-huge rooms_entry_date' . $item_room->erp_hotels_reservation_orders_rooms_id . '" readonly="readonly" style="width:120px" readonly="readonly"')
                                        ?>
                                    </td>
                                    <td>
                                        <?php
                                        echo form_input('rooms_exit_date[' . $item_room->erp_hotels_reservation_orders_rooms_id . ']'
                                                , set_value('rooms_exit_date[' . $item_room->erp_hotels_reservation_orders_rooms_id . ']', $item_room->exit_date)
                                                , 'class="input-huge  rooms_exit_date' . $item_room->erp_hotels_reservation_orders_rooms_id . '" style="width:120px" readonly="readonly"');
                                        
                                        ?>
                                    </td>
                                    <td>
                                        <?php
                                        echo form_dropdown('rooms_erp_meals_id[' . $item_room->erp_hotels_reservation_orders_rooms_id . ']'
                                                , $erp_meals, set_value('rooms_erp_meals_id[' . $item_room->erp_hotels_reservation_orders_rooms_id . ']', $item_room->erp_meals_id)
                                                , 'class="chosen-select chosen-rtl input-full"  disabled="disabled" id="rooms_erp_meals_id_' . $item_room->erp_hotels_reservation_orders_rooms_id . '" tabindex="4" ');
                                        echo form_hidden('rooms_erp_meals_id[' . $item_room->erp_hotels_reservation_orders_rooms_id . ']'
                                                , set_value('rooms_erp_meals_id[' . $item_room->erp_hotels_reservation_orders_rooms_id . ']', $item_room->erp_meals_id));
                                        ?>
                                    </td>

                                </tr>
                            <? } ?>
                        <? } ?>
<? } ?>
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="12" class="availability_tr">
                            <a class="btn Fleft" href="javascript:void(0)" onclick="check_availability()"><?= lang('check_availability') ?></a>
                        </td>
                    </tr>
                </tfoot>
            </table>
        </div>

    </div>
</div>

<script>
    function load_multiselect() {
        var config = {
            '.chosen-select': {width: "100%"},
            '.chosen-select-deselect': {allow_single_deselect: true},
            '.chosen-select-no-single': {disable_search_threshold: 10},
            '.chosen-select-no-results': {no_results_text: 'Oops, nothing found!'},
            '.chosen-select-width': {width: "95%"}
        }
        for (var selector in config) {
            $(selector).chosen(config[selector]);
        }
    }
    function add_rooms(id) {
        var new_row = [
                "<tr rel=\"" + id + "\">",
                "<td><span class=\"availability availability_icon_" + id + "\"></span></td>",
                "    <td>",
                "       <select name=\"rooms_erp_housingtypes_id[" + id + "]\" class=\"chosen-select chosen-rtl input-full\" id=\"rooms_erp_housingtypes_id_" + id + "\" tabindex=\"4\">",
                <? foreach ($erp_housingtypes as $key => $value): ?>
                "           <option value=\"<?= $key ?>\"><?= $value ?></option>",
                <? endforeach ?>
                "    </select></td>",
                "    <td>",
                "       <select name=\"rooms_erp_hotelroomsizes_id[" + id + "]\" class=\"chosen-select chosen-rtl input-full\" id=\"rooms_erp_hotelroomsizes_id_" + id + "\" tabindex=\"4\">",
                <? foreach ($erp_hotelroomsizes as $key => $value): ?>
                "           <option value=\"<?= $key ?>\"><?= $value ?></option>",
                <? endforeach ?>
                "    </select></td>",
                '     <td><?php echo form_input("rooms_rooms_count[' + id + ']", FALSE, 'class="validate[required] "  style="width: 35px"') ?>',
                "    </td>",
                "    <td>",
                '        <?php echo form_input("rooms_price_from[' + id + ']", FALSE, "class=\"validate[required] \" style=\"width: 55px\"") ?>',
                "    </td>",
                "    <td>",
                '        <?php echo form_input("rooms_price_to[' + id + ']", FALSE, "class=\"validate[required] \"  style=\"width: 55px\"") ?>',
                "    </td>",
                "    <td>",
                "       <select name=\"rooms_erp_currencies_id[" + id + "]\" class=\"chosen-select chosen-rtl input-full\" id=\"rooms_erp_currencies_id_" + id + "\" tabindex=\"4\">",
                <? foreach ($erp_currencies as $key => $value): ?>
                "           <option value=\"<?= $key ?>\"><?= $value ?></option>",
                <? endforeach ?>
                "    </select></td>",
                "    <td>",
                '        <?php echo form_input("rooms_nights_count[' + id + ']", FALSE, "class=\"validate[required] \" style=\"width: 35px\"") ?>',
                "    </td>",
                '     <td><?php echo form_input("rooms_entry_date[' + id + ']", FALSE, " class=\"validate[required] rooms_entry_date' + id + '\" style=\"width: 95px\" ") ?>',
                "<script>",
                '$(".rooms_entry_date' + id + '").datepicker({',
                "    dateFormat: 'yy-mm-dd',",
                "    controlType: 'select',",
                "    timeFormat: 'HH:mm',",
                "   onClose: function(selectedDate) {$('.rooms_exit_date" + id + "').datepicker('option', 'minDate', selectedDate)}",
                "});",
                "<\/script>",
                "    </td>",
                "    <td>",
                '        <?php echo form_input("rooms_exit_date[' + id + ']", FALSE, "class=\"validate[required] rooms_exit_date' + id + ' \" style=\"width: 95px\" ") ?>',
                "<script>",
                '$(".rooms_exit_date' + id + '").datepicker({',
                "    dateFormat: 'yy-mm-dd',",
                "    controlType: 'select',",
                "    timeFormat: 'HH:mm',",
                "   onClose: function(selectedDate) {$('.rooms_entry_date" + id + "').datepicker('option', 'maxDate', selectedDate)}",
                "});",
                "<\/script>",
                "    </td>",
                "    <td>",
                "       <select name=\"rooms_erp_meals_id[" + id + "]\" class=\"chosen-select chosen-rtl input-full\" id=\"rooms_erp_meals_id_" + id + "\" tabindex=\"4\">",
                <? foreach ($erp_meals as $key => $value): ?>
                "           <option value=\"<?= $key ?>\"><?= $value ?></option>",
                <? endforeach ?>
                "    </select></td>",
                "    <td class=\"TAC\">",
                "        <a href=\"javascript:void(0)\" onclick=\"delete_rooms('" + id + "')\"><span class=\"icon-trash\"></span></a>",
                "        <a class=\"btn Fleft\" href=\"javascript:void(0)\" onclick=\"check_calender('" + id + "')\"><?= lang('check_calender') ?></a>",
                "    </td>",
                "</tr>"
        ].join("\n");
                $('#rooms').append(new_row);
    }
    function delete_rooms(id, database) {
        if (typeof database == 'undefined')
        {
            $('.rooms').find('tr[rel="' + id + '"]').remove();
        }
        else
        {
            $('.rooms').find('tr[rel=' + id + ']').remove();
            var hidden_input = '<input type="hidden" name="rooms_remove[]" value="' + id + '" />';
            $('#rooms').append(hidden_input);
        }
    }
    function check_availability() {
        $.post('<?= site_url('hotels_reservation_orders/check_availability') ?>' , $('#frm_hotels_reservation_orders').serialize(), function(data) {
            json = JSON.parse(data);
            var notavailabile = '';
            var add_availabile_btn = false;
            $.each(json, function(i, result) {
                if (result < 0)
                {
                    if(notavailabile.length)
                        notavailabile = notavailabile+',';
                    notavailabile = notavailabile+i;
                    add_availabile_btn = true;
                    $('.availability_icon_' + i).removeClass('icon-ok');
                    $('.availability_icon_' + i).addClass('icon-remove');
                }
                else
                {
                    $('.availability_icon_' + i).removeClass('icon-remove');
                    $('.availability_icon_' + i).addClass('icon-ok');
                }
            });
            if(add_availabile_btn) {
                $('.add_availability').remove();
                $('.availability_tr').append('<a class="btn Fleft add_availability" rel="'+notavailabile+'" href="javascript:void(0)" ><?= lang('add_availability') ?></a>');
            }
        });
    }

    function check_calender(roomid) {

        var postdata = $('#erp_cities_id,#erp_hotels_id,tr[rel="' + roomid + '"] select,tr[rel="' + roomid + '"] input').serialize();
        $.post('<?= site_url('hotel_availability/ajax_calendar') ?>', postdata, function(data) {
                $.fancybox({
                    content: data
                });
        });
    }
    
    $(function(){
        $('#container_rooms').on('click','.add_availability',function(){
            var fileds = $(this).attr('rel').split(',');
            var newwindow = $('<table>');
            $.each(fileds,function(idx,vlu){
                newwindow.append($('tr[rel='+vlu+']').clone());
            });
            newwindow.append('<input type="submit" class="btn" value="تنفيذ" />');
            var reserveform = $('<form name="add_availability" action="<?= site_url('hotels_reservation_orders/add_availability') ?>" method="post">');
            reserveform.append(newwindow);
            $.fancybox({
                    content: reserveform
                });
        });
    });
    
    $('body').on('submit','form[name=add_availability]',function(){
        $.post($('form[name=add_availability]').attr('action'),$('form[name=add_availability]').serialize(),function(data){
            if(data == 'Done'){
                $('.add_availability').remove();
                check_availability();
                $.fancybox.close();
            } else {
                alert('عذرا لا يمكن توفير الاتاحه يرجي اضافتها بشكل يدوى اولا');
            }
        });
        return false;
    });
</script>

