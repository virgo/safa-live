<script>.widget{width:98%;}</script>

<div class="widget">
   
    <div class="path-container Fright">
    	<div class="path-name Fright">
            <a href="<?php 
            if(session('uo_id')) {
            	echo  site_url().'uo/dashboard'; 
            } else if(session('ea_id')) {
            	echo  site_url().'ea/dashboard';
            } else {
            	echo  site_url();
            }
            ?>"><?php echo  lang('global_system_management') ?></a>
        </div>
        <div class="path-arrow Fright"></div>
        <div class="path-name Fright"> <?php echo  lang('title') ?></div>
    </div>
</div>

<div class="widget">
    <div class="widget-header">
        <div class="widget-header-icon Fright"> <span class="icos-pencil2"></span> </div>
        <div class="widget-header-title Fright">
            <?php echo  lang('title') ?>
        </div>
        <a class="btn Fleft" href="<?php echo  site_url('hotels_reservation_orders/add') ?>">
      <?php echo  lang('global_add') ?>
    </a> 
    </div>
     <div class="widget-container">
          <div class="table-responsive">
            <table cellpadding="0" cellspacing="0" class="fsTable" >
                <thead>
                    <tr>
                        <th><?php echo  lang('order_no') ?></th>
                        <th><?php echo  lang('order_date') ?></th>
                        <th><?php echo  lang('company_type') ?></th>
                        <th><?php echo  lang('company') ?></th>
                        <th><?php echo  lang('rooms_count') ?></th>
                        <th><?php echo  lang('order_status') ?></th>
                        <th><?php echo  lang('actions') ?></th>
                    </tr>
                </thead>
                <tbody id="had">
                <? if(ensure($items)) { ?>
                    <? foreach($items as $item) {?>
                    <tr rel="<?php echo  $item->erp_hotels_reservation_orders_id ?>">

                        <td>
                            <?php echo   $item->erp_hotels_reservation_orders_id ; ?>
                        </td>
                        <td>
                            <?php echo   $item->order_date ; ?>
                        </td>
                        
                        <td>
                        <?php 
                    	if($item->erp_company_types_id==2) { 
				        	echo  lang('uo');
				        } else if($item->erp_company_types_id==3) { 
				        	echo  lang('ea') ;
				        } else if($item->erp_company_types_id==5) {
				        	echo   lang('hm') ;
				        }  
		                ?>
                        </td>
                        
                        <td>
                        <?php 
                            
                    	if ($item->erp_company_types_id == 2) {
                    	if(name()=='name_la') {
                            	if(isset($item->to_uo_name_la)) {
                            		echo   $item->to_uo_name_la ;
                            	}
                            } else {
                            	if(isset($item->to_uo_name_ar)) {
                            		echo   $item->to_uo_name_ar ;
                            }
                        }
			        	} else if ($item->erp_company_types_id == 3) {
			        		if(name()=='name_la') {
	                            	if(isset($item->to_ea_name_la)) {
	                            		echo   $item->to_ea_name_la ;
	                            	}
	                            } else {
	                            	if(isset($item->to_ea_name_ar)) {
	                            		echo   $item->to_ea_name_ar ;
	                            }
	                        }
			        	} else if ($item->erp_company_types_id == 5) {
			        		if(name()=='name_la') {
	                            	if(isset($item->to_hm_name_la)) {
	                            		echo   $item->to_hm_name_la ;
	                            	}
	                            } else {
	                            	if(isset($item->to_hm_name_ar)) {
	                            		echo   $item->to_hm_name_ar ;
	                            }
	                        }
			        	}
                             
                            ?>
                        </td>
                        
                        <!-- 
                        <td>
                            <a class='fancybox fancybox.iframe' href="<?= site_url("hotels_reservation_orders/rooms_details_popup/". $item->erp_hotels_reservation_orders_id); ?>"><span title="<?php 
                            /*
                            $room_sizes_string_arr=explode(',',$item->room_sizes_string);
                            $room_count_string_arr=explode(',',$item->room_count_string);
                            foreach($room_count_string_arr as $key=>$value) {
                            	for($i=0; $i<$room_sizes_string_arr[$key]; $i++) {
                            		//echo nl2br("&lt;span class='icon-edit' &gt;  &lt;/span&gt; ");
                            	}
                            	echo $room_sizes_string_arr[$key].' - ';
                            	echo $value.'&#xA;';
                            }
                            */
                            $this->erp_hotels_reservation_orders_model->rooms_erp_hotels_reservation_orders_id = $item->erp_hotels_reservation_orders_id;
        					$item_rooms = $this->erp_hotels_reservation_orders_model->getRooms();
        					foreach($item_rooms as $item_room) {
                            	
	        					if(name()=='name_la') {
	                            	echo   $item_room->hotelroomsize_name_la ;
	                            } else {
	                            	echo   $item_room->hotelroomsize_name_ar ;
	                            } 
	        					echo ' - ';
	                            echo $item_room->rooms_count.'&#xA;';
        					}
        					
                            ?>"><?php echo   $item->total_rooms_count ; ?></span>
                            </a>
                        </td>
                         -->
                        
                        
                        <td>
                        	<!-- 
                            <a class='fancybox fancybox.iframe' href="<?= site_url("hotels_reservation_orders/rooms_details_popup/". $item->erp_hotels_reservation_orders_id); ?>"><span title="<?php 
                            /*
                            $room_sizes_string_arr=explode(',',$item->room_sizes_string);
                            $room_count_string_arr=explode(',',$item->room_count_string);
                            foreach($room_count_string_arr as $key=>$value) {
                            	for($i=0; $i<$room_sizes_string_arr[$key]; $i++) {
                            		//echo nl2br("&lt;span class='icon-edit' &gt;  &lt;/span&gt; ");
                            	}
                            	echo $room_sizes_string_arr[$key].' - ';
                            	echo $value.'&#xA;';
                            }
                            */
                            $this->erp_hotels_reservation_orders_model->rooms_erp_hotels_reservation_orders_id = $item->erp_hotels_reservation_orders_id;
        					$item_rooms = $this->erp_hotels_reservation_orders_model->getRooms();
        					foreach($item_rooms as $item_room) {
                            	
	        					if(name()=='name_la') {
	                            	echo   $item_room->hotelroomsize_name_la ;
	                            } else {
	                            	echo   $item_room->hotelroomsize_name_ar ;
	                            } 
	        					echo ' - ';
	                            echo $item_room->rooms_count.'&#xA;';
        					}
        					
                            ?>"><?php echo   $item->total_rooms_count ; ?></span>
                            </a>
                             -->
                             
                             
                             
                            
                            
                            <?php

                             $arr_required_rooms=array();
                             
                             
	                            $this->erp_hotels_reservation_orders_model->rooms_erp_hotels_reservation_orders_id = $item->erp_hotels_reservation_orders_id;
	        					$item_rooms = $this->erp_hotels_reservation_orders_model->getRooms();
	        					
	        					echo"<table>";
	        					foreach($item_rooms as $item_room) {
	                            	echo "<tr>";
	                            	
	                            	
	                            	echo "<td>".$item_room->erp_hotel_name."</td>";
	                            	
	                            	echo "<td>".$item_room->rooms_count."</td>";
	                            	
		        					if(name()=='name_la') {
		                            	echo   "<td>".$item_room->hotelroomsize_name_la."</td>" ;
		                            } else {
		                            	echo   "<td>".$item_room->hotelroomsize_name_ar."</td>" ;
		                            } 
		                            
		                            echo "<td>".$item_room->entry_date."<br/>".$item_room->exit_date."</td>";
		                            
		                            if(name()=='name_la') {
		                            	$arr_required_rooms[$item_room->hotelroomsize_name_la]=$item_room->rooms_count;
		                            } else {
		                            	$arr_required_rooms[$item_room->hotelroomsize_name_ar]=$item_room->rooms_count;
		                            }
		                            echo "</tr>";
	        					}
	        					
	        					echo "</table>";
                            ?>
                            
                        </td>
                        
                        <td>
                        <?php echo $item->order_status_name; ?>
                        </td>
                        
                        <td class="TAC">
                        <a href="<?= site_url('hotels_reservation_orders/pdf_export/' . $item->erp_hotels_reservation_orders_id) ?>" target="_blank" class="icon-book"></a>
                        <?php if($item->order_status!=1) {?>
                        <a href="<?= site_url('hotels_reservation_orders/edit/' . $item->erp_hotels_reservation_orders_id) ?>"><span class="icon-pencil"></span></a>
                        <a href="<?= site_url('hotels_reservation_orders/delete/' . $item->erp_hotels_reservation_orders_id) ?>" onclick="return confirm('<?= lang('global_are_you_sure_you_want_to_delete')?>')"><span class="icon-trash"></span></a>
                        <?php } else {
                        	//echo $item->order_status_name;
                        }
                        ?>
                        <a href="<?= site_url('hotels_reservation_orders/view_owner/' . $item->erp_hotels_reservation_orders_id) ?>"><span class="icon-search"></span></a>
                        </td>
                        
                    </tr>
                    <? } ?>
                <? } ?> 
                </tbody>
            </table>
<!--            <div><?= $pagination ?></div>-->
        </div>
    </div>
</div>

