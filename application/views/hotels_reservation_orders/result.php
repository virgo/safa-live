<style>
    .Div-Sep {
        border-bottom: 1px solid #DEDEDE !important;
        height: 10px;

        padding-top: 0;
    }

    .Div-Sep a{
        border: 1px solid #DEDEDE;
    }

    .wizerd-div {
        border-bottom: none!important;
        margin: -29px 0 29px;
        padding-top: 0;
    }
    .wizerd-div a {
        background: none repeat scroll 0 0 #FAFAFA;
        border: 1px solid #D0D0D0;
        border-radius: 49px;
        display: inline-block;
        margin: -6px -7px -17px -8px;
        padding: 10px 12px 8px;
    }


</style>

<div class="row-fluid">

    <div class="widget">
        <form name="reservition" method="post" >
            <div class="widget-header">

                <div class="widget-header-icon Fright"><i class="icos-pencil2"></i></div>
                <div class="widget-header-title Fright">
                    نتيجه البحث </div>
            </div><? $searchs = $this->input->post('search') ?>
            <? foreach ($results as $reskey => $values) : ?>
                <? parse_str($searchs[$reskey], $postdata) ?>
                <? $oldhotel = 0 ?>
                <? foreach ($values as $value) : ?>
                    <? if ($value->erp_hotel_id != $oldhotel) : ?>
                        <? if ($oldhotel != 0) : ?></div><? endif ?>

                <div class="resalt-group" style="margin-top: 40px;">
                    <div class="row-fluid">
                        <div class="span9">
                            <div class="wizerd-div"><a><?= $value->{name()} ?> <? for ($i = 0; $i < $value->erp_hotellevel_id; $i++) : ?>
                                        <img src="<?= IMAGES ?>/imgs/star.png" alt="*" />
                                    <? endfor ?></a></div>
                            <div class="span12">
                                <div class="row-fluid">
                                    <div class="span2"> المدينه</div>
                                    <div class="span10"> <span class="res"> <?= $value->city ?></span></div>
                                </div>
                                <div class="row-fluid">
                                    <div class="span2"> العنوان</div>
                                    <div class="span10"> <span class="res"> <?= $value->{'address_' . name(TRUE)} ?></span></div>
                                </div>
                                <? if (strlen($value->distance)) : ?>
                                    <div class="row-fluid">
                                        <div class="span12">
                                            <div class="span3"> المسافه من الحرم</div>
                                            <div class="span5"><span class="res"><?= $value->distance ?></span> </div>
                                        </div>
                                    </div>
                                <? endif ?>
                            </div>
                            <div class="span9">
                                <a class="fancybox  fancybox.iframe" href="<?= site_url('hotel_availability/advantages/' . $value->erp_hotel_id) ?>" style="margin: 5px;">مزايا/ صفات الفندق</a> |
                                <a class="fancybox  fancybox.iframe" href="<?= site_url('hotel_availability/policies/' . $value->erp_hotel_id) ?>" style="margin: 5px;">سياسات الفندق</a> |
                                <a class="fancybox  fancybox.iframe" href="<?= site_url('hotel_availability/map/' . $value->erp_hotel_id) ?>" style="margin: 5px;">اعرض الخارطه</a>
                            </div>
                        </div>
                        <div class="span3">
                            <div style="width: 100%;min-height: 100px; border: 1px solid rgb(221, 221, 221); overflow: hidden; padding: 4px; border-radius: 6px;" class="">     
                                <img style="width:100%;height: auto;margin:0 auto;vertical-align: left;" src="<?= base_url("static/hotel_album/" . $value->erp_hotel_id . '/' . get_h_img($value->erp_hotel_id)) ?>"> </div>
                        </div>
                    </div>
                    <? $oldhotel = $value->erp_hotel_id; ?>
                <? endif ?>
                <div class="Div-Sep"><a></a></div>
                <div class="row-fluid">
                    <div class="span6">
                        <div class="row-fluid">
                            <div class="span6">
                                <div class="span6">نوع الغرفه</div>
                                <div class="span6"><span class="res"><?= $value->roomtype ?></span></div>
                            </div>
                            <div class="span6">
                                <div class="span6"> السعر</div>
                                <div class="span6"><span class="res"><?= $value->sale_price ?></span></div>
                            </div> 
                        </div>

                        <div class="row-fluid">
                            <div class="span6">
                                <div class="span4"> الفتره من :</div> 
                                <div class="span8"><span class="res"><input name="fromdate[<?= $value->erp_hotels_availability_master_id ?>][<?= $value->erp_hotels_availability_detail_id ?>]" type="hidden" value="<?= $postdata['date_from'] ?>" /><?= $postdata['date_from'] ?></span>
                                </div>
                            </div>

                            <div class="span6">
                                <div class="span2"> الي:</div>
                                <div class="span10"><span class="res"><input name="todate[<?= $value->erp_hotels_availability_master_id ?>][<?= $value->erp_hotels_availability_detail_id ?>]" type="hidden" value="<?= $postdata['date_to'] ?>" /><?= $postdata['date_to'] ?></span>
                                </div>
                            </div>
                        </div>
                        <div class="row-fluid">
                            <div class="span6">
                                <div class="span6"> العدد المتاح</div>
                                <div class="span6"><span class="res"><?= $value->available_count ?> </span> </div>
                            </div> 
                            <div class="span6">
                                <div class="span4"> المطلوب</div>
                                <div class="span2"><input name="roomcount[<?= $value->erp_hotels_availability_master_id ?>][<?= $value->erp_hotels_availability_detail_id ?>]" maxcount="<?= $value->available_count ?>"  style="margin-top: -6px;" class="input-mini roomcount" value="0" /></div>
                            </div>
                        </div>

                    </div>
                    <div class="span6">
                        <? if ($value->erp_hotels_availability_view_id) : ?>
                            <div class="row-fluid">
                                <div class="span12">
                                    تطل علي الحرم
                                </div>
                            </div>
                        <? endif ?>

                        <? $services = get_room_services($value->erp_hotels_availability_master_id) ?>
                        <? if (count($services['free'])) : ?>
                            <div class="row-fluid">
                                <div class="span7">الاضافات شاملة السعر</div>
                                <div class="span5">
                                    <? foreach ($services['free'] as $service) : ?>
                                        <span class="res"><?= $service->{name()} ?></span><br />
                                    <? endforeach ?>
                                </div>
                            </div>
                        <? endif ?>
                        <? if (count($services['paid'])) : ?>
                            <div class="row-fluid">
                                <div class="span7">الاضافات غير شاملة السعر</div>
                                <div class="span5">
                                    <? foreach ($services['paid'] as $service) : ?>
                                        <span class="res"><?= $service->{name()} ?></span><br />
                                    <? endforeach ?>
                                </div>
                            </div>
                        <? endif ?>
                        <? if($co_type == $value->erp_company_type_id && $co_id == $value->safa_company_id) :?>
                        <span class="Fleft"><?= lang("ha_registerd_to")." ".get_company_name($co_id,$co_type) ?> </span>
                        <? else :?>
                        <span class="Fleft"><a class="fancybox fancybox.iframe" href="<?= site_url('hotel_availability/availability_detail/' . $value->erp_hotels_availability_master_id) ?>" style="margin: 5px;">اظهار بيانات صاحب الاتاحه</a></span>
                        <? endif ?>
                    </div>
                </div>
            <? endforeach ?>
        <? endforeach ?>
    </div>

    <div class="span3">
        <div class="toolbar bottom TAC">
            <input class="btn btn-primary" name="submit" value="ارسال طلب الحجز" type="submit">
        </div>  
    </div>

</div>
</div>

<script type="text/javascript">
    $(function() {

        $('form[name=reservition]').submit(function(event) {
            event.preventDefault();
            var notsend = true;
            $('.roomcount').each(function() {
                if ($(this).val() != 0)
                    notsend = false;
            });
            
            if(notsend) {
                alert('يجب اختيار غرفه علي الاقل');
                return false;
            }
            
            $.post('<?= site_url("hotel_availability/reserve") ?>', $(this).serialize(), function(data) {
                $.fancybox({
                    type: 'iframe',
                    content: data
                });
            });
        });

        $(".fancybox").fancybox();

        $('.roomcount').change(function() {
            if (parseInt($(this).val()) > parseInt($(this).attr('maxcount'))) {
                alert('عدد الغرف لا يمكن ان يزيد عن' + $(this).attr('maxcount'));
                $(this).focus();
            }
        });
    });
</script>

