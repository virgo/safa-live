
<h1><?= lang('global_entries') ?> 
 <a class="button" href="<?= site_url('accounting/entries/add' ) ?>"><?= lang('global_add') ?></a></h1>
</h1>
<table width="100%" class="table">
    <tr>
        <th><?= lang('entry_number') ?></th>
        <th><?= lang('entry_date') ?></th>
<!--        <th><?= lang('num_entries') ?></th>-->
        <th><?= lang('global_operations') ?></th>
    </tr>
    <? if($items && is_array($items) && count($items)): ?>
    <? foreach($items as $item): ?>
    <tr>
        <td><?=$item->entry_number?></td>
        <td><?=$item->master_date?></td>
<!--        <td><?=$item->num_rows?></td>-->
        <td>
            <? if($item->transaction_entry == '0'): ?>
            <a class="button" href="<?= site_url('accounting/entries/add/'. $item->id ) ?>"><?= lang('global_edit') ?></a> 
            <a class="button" onclick="return confirm('Are you sure to delete?')"   href="<?= site_url('accounting/entries/delete/' . $item->id) ?>"><?= lang('delete') ?></a> 
            <? else:?>
            <a class="button" href="<?= site_url('accounting/entries/add/'. $item->id ) ?>"><?= lang('show') ?></a>
            <? endif; ?>
        </td>
    </tr>
    <? endforeach ?>
    
   <? else: ?>
            <tr>
                <td colspan="6">
        <?= lang('global_no_entries') ?>
                </td>
            </tr>
    <? endif ?>
 
</table>
<?= $pagination ?>

