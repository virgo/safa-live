<script type='text/javascript' src='<?= js ?>/addremovefield.js'></script>

<div id="tabs">


<style>
    .entries {
        /*border: #000 thin solid;*/
    }
    .entries th {
        text-align: center;
    }
    .entries td, .entries th, .summary td  {
        width: 90px;
    }
    .entries td, .entries th {
        border: #000 thin solid;
    }
    .summary td  {
        border: transparent thin solid;
    }
    .entries input, .summary input, .entries select, .summary select {
        width: 80px;
        background: transparent;
        font-size: 10px;
    }
    .first_td {
        width: 30px !important;
        border: none !important;
    }
</style> 


    <div id="tabs-1" class="package_ground">
      
        <span class="title_style"><?= lang('add_entry') ?></span>
        <div class="box box-gray">
            <div class="box-content">
                <? if (validation_errors()): ?>
                    <div class="error">
                        <?php echo validation_errors(); ?>
                    </div>
                <? endif ?>
<? if (isset($err)): ?>
    <div class="error">
        <?php echo $err; ?>
    </div>
<? endif ?>
<?= form_open("", "id='entry_form'") ?>  



            
<table width="90%" >
    <tr>
        <td>
            <?= lang('entry_number') ?>
        </td>
        <td>
            <?= form_input('entry_number', set_value('entry_number', $number), 'class="text_input"') ?>
        </td>
        <td>
            <?= lang('entry_date') ?>
        </td>
        <td>
            <?= form_input('entry_date', set_value('entry_date', date('Y-m-d')), 'class="text_input" id="entry_date"') ?>
        </td>
    </tr>
    <tr>
        <td>
            <?= lang('entry_currency') ?>
        </td>
        <td>
            <?= form_dropdown('entry_currency', $currencies,set_value('entry_currency', $this->config->item('default_currency')),"id='currency' ") ?>
        </td>
        <td>
            <?= lang('entry_rate') ?>
        </td>
        <td>
            <input type="text" name="entry_rate" id="entry_rate"  class="entry_rate" value="1.0000" />
        </td>
    </tr>
</table>

<table class="entries" id="entries">
    <tr>
        <th class="first_td"><a href="javascript:void(0)" id="add"><img src="<?= IMAGES ?>/green_button.png" /></a></th>
        <th><?= lang('debit') ?></th>
        <th><?= lang('credit') ?></th>
        <th><?= lang('account') ?></th>
        <th><?= lang('voucher') ?></th>
        <th><?= lang('description') ?></th>
        <th><?= lang('remarks') ?></th>
        <th><?= lang('currency') ?></th>
        <th><?= lang('rate') ?></th>
        <th><?= lang('second account') ?></th>
    </tr>
    <tr>
        <td class="first_td">&nbsp;</td>
        <td><input type="text" id="debit-1" name="debit[]" class="debit" value="0.00" onblur="getDebitSum()" onfocus="this.select()" /></td>
        <td><input type="text" id="credit-1" name="credit[]" class="credit" value="0.00" onblur="getCreditSum()" onfocus="this.select()" /></td>
        <td><input type="hidden" name="account_hidden[]" />
            <input type="text" name="account[]" class="account" id="first_account-1"  onblur="check_acc(this)" />
            </td>
        <td><input type="text" name="voucher[]" class="voucher" /></td>
        <td><input type="text" name="description[]" class="description" /></td>
        <td><input type="text" name="remarks[]" class="remarks" /></td>
        <td><?= form_dropdown('currency[]', $currencies, set_value('currency[]', $this->config->item('default_currency'))) ?></td>
        <td><input type="text" id="rate-1" name="rate[]" class="rate" value="1.0000" onblur="rate_changes(this)" /></td>
        <td>
            <input type="hidden" name="second_account_hidden[]"/>
            <input type="text" name="second_account[]" class="second_account" id="scound_account-1"  onblur="check_acc(this)" />
            
        </td>
    </tr>
    <tr>
        <td class="first_td">&nbsp;</td>
        <td><input id="debit-2" type="text" name="debit[]" class="debit" value="0.00" onblur="getDebitSum()" onfocus="this.select()" /></td>
        <td><input id="credit-2" type="text" name="credit[]" class="credit" value="0.00" onblur="getCreditSum()" onfocus="this.select()" /></td>
        <td><input type="hidden" name="account_hidden[]"  />
            <input type="text" name="account[]" class="account" id="first_account-2" onblur="check_acc(this)"  />
          </td>
        <td><input type="text" name="voucher[]" class="voucher" /></td>
        <td><input type="text" name="description[]" class="description" /></td>
        <td><input type="text" name="remarks[]" class="remarks" /></td>
        <td><?= form_dropdown('currency[]', $currencies, set_value('currency[]', $this->config->item('default_currency'))) ?></td>
        <td><input type="text" id="rate-2" name="rate[]" class="rate" value="1.0000" onblur="rate_changes(this)" /></td>
        <td>
            <input type="hidden" name="second_account_hidden[]"/>
            <input type="text" name="second_account[]" class="second_account" id="scound_account-2" onblur="check_acc(this)" />
            
        </td>
    </tr>
</table>

<table class="summary" border="0">
    <tr>
        <td class="first_td"><a href="javascript:void(0)" id="add2"><img src="<?= IMAGES ?>/green_button.png" /></a></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td class="first_td"></td>
        <td><input type="text" name="debit_sum" id="debit" value="0.00" onclick="getDebitSum()" /></td>
        <td><input type="text" name="credit_sum" id="credit" value="0.00" onclick="getCreditSum()" /></td>
    </tr>
    <tr>
        <td class="first_td"></td>
        <td><input id="diff_total_debit" type="text" name="diff_total_debit" value="" onclick="getCreditSum()" /></td>
        <td><input id="diff_total_credit" type="text" name="diff_total_creditit" value="" onclick="getCreditSum()" /></td>
    </tr>
</table>


                <div class="submit-row" id="center_buttons">
                    <input type="submit" value="<?= lang('global_submit') ?>" class="button" name="submit" />
                    <input type="button" value="<?= lang('global_back') ?>" class="button" onclick="window.location='<?= site_url('admin/entries/index') ?>'" />
                </div>

            </div>
        </div>
    </div>
</div>
<?= form_close() ?>


<table class="appended" style="display: none">
    <tr>
        <td class="first_td"><a href="javascript:void(0)" class="minus">
                <img src="<?= IMAGES ?>/red_button.png" />
            </a></td>
        <td><input type="text" name="debit[]" class="debit" value="0.00" onblur="getDebitSum()" onfocus="this.select()" /></td>
        <td><input type="text" name="credit[]" class="credit" value="0.00" onblur="getCreditSum()" onfocus="this.select()" /></td>
        <td><input type="hidden" name="account_hidden[]"  />
            <input type="text" name="account[]" class="account"   onblur="check_acc(this)"  />
            </td>
        <td><input type="text" name="voucher[]" class="voucher" /></td>
        <td><input type="text" name="description[]" class="description" /></td>
        <td><input type="text" name="remarks[]" class="remarks" /></td>
        <td><?= form_dropdown('currency[]', $currencies, set_value('currency[]',$this->config->item('default_currency'))) ?></td>
        <td><input type="text" name="rate[]" class="rate" value="1.0000" onblur="rate_changes(this)" /></td>
        <td>
            <input type="hidden" name="second_account_hidden[]"/>
            <input type="text" name="second_account[]" class="second_account" onblur="check_acc(this)" />
            
        </td>

    </tr>
</table>

<script>
    $("#entry_date").datepicker({ dateFormat: "yy-mm-dd" });
    $( "#entry_date" ).datepicker( "option", "appendText", " (yyyy-mm-dd)" );
    
    
    $(document).ready(function(){
        $(function(){
            $( ".account" ).autocomplete({
                source: "<?= site_url('admin/accounts/get_accounts/2') ?>"+$(this).val(),
                minLength:1,
                select: function( event, ui ) {
                    $(this).prev().val(ui.item.id);
                    //                    log( ui.item ?
                    //                        "Selected: " + ui.item.value + " aka " + ui.item.id :
                    //                        "Nothing selected, input was " + this.value );
                }
            });
            $( ".second_account" ).autocomplete({
                source: "<?= site_url('admin/accounts/get_accounts/2') ?>"+$(this).val(),
                minLength:1,
                select: function( event, ui ) {
                    $(this).prev().val(ui.item.id);
                    //                    log( ui.item ?
                    //                        "Selected: " + ui.item.value + " aka " + ui.item.id :
                    //                        "Nothing selected, input was " + this.value );
                }
            });  
        });
        //-------------------------------------------------------------------------------------//
        $("#entry_form").submit(function(){
            var ret= check_account();
             if(ret==false)
               {

                   return false;
                   
               } 
              else{
                  
              
            var return_=getdebit_credit();
            if(return_!=false)
            {
                var x= checkTotal("credit","debit");
                if(x==0)
                {
                    if(parseFloat($("#debit").val()) == 0 | parseFloat($("#credit").val()) == 0 ) {
                        alert ('<?=lang('error_no_value')?>');
                        return false;
                    }
                    else
                        return true;
                }
                else if(x<0)
                {
                    $("#diff_total_debit").val("");
                    $("#diff_total_credit").val("");
                    $("#diff_total_debit").val('['+x+']');
                    alert ('<?=lang('error_balance')?>');
                    return false;
                }
                else if(x>0)
                {
                    $("#diff_total_debit").val("");
                    $("#diff_total_credit").val("");
                    $("#diff_total_credit").val('['+x+']');
                    alert ('<?=lang('error_balance')?>');
                    return false;
                }
                
                
                
            }
            else
            {
                return false;
            }
         
         
         }  
            
            
        });
    });
    function get_native_id(strconc_id,sep_char)
    {
        var index_=strconc_id.lastIndexOf(sep_char);
        var id=strconc_id.substring(index_+1)
        return id; 
    } 
</script>
 <script type="text/javascript" src="<?= JS ?>/accounting.js"/></script>
<script type="text/javascript" src="<?= JS ?>/add_enteries.js"/></script>
<script> 
    $("#add").click(function(event){
        creat($(this));
    });
    $("#add2").click(function(event){
        creat($(this));
    });
    function remove(){
        $('.minus').click(function(){
            $(this).parent().parent().remove();    
        });
    }
    function creat(){
        var last_tr_id= $("#entries").find(".account:last").attr("id");
        var last_tr_scoundaccount_id= $("#entries").find(".second_account:last").attr("id");
        var id_=get_native_id(last_tr_id,"-");
        var first_account_id=parseInt(id_)+1; 
        var id_sc=get_native_id(last_tr_scoundaccount_id,"-");
        var scound_account_id=parseInt(id_sc)+1;
        //-------------------------------------------------------------------------------------------//  
        var last_debit_id=$("#entries").find(".debit:last").attr("id");
        var l_deb_id=get_native_id(last_debit_id,"-");
        l_deb_id=parseInt(l_deb_id)+1;
//        //---------------------------------------------------------------------------------//\
//        var last_credit_id=$("#entries").find(".credit:last").attr("id");
//        var l_crdt_id=get_native_id(last_credit_id,"-");
//        l_crdt_id=parseInt(l_crdt_id)+1;
//        //---------------------------------------------------------------------------------//\
//        var last_rate_id=$("#entries").find(".rate:last").attr("id");
//        var l_rate_id=get_native_id(last_rate_id,"-");
//        l_rate_id=parseInt(l_rate_id)+1;
//        //----------------------------------------------------------------------------------//   
        $('#entries').append($('.appended').find('tbody').html());
        $("#entries").find("[name='currency[]']").val($("#currency").val());
        $("#entries").find("[name='rate[]']").val($("#entry_rate").val());
        $("#entries").find(".debit:last").attr("id","debit-"+l_deb_id);
        $("#entries").find(".credit:last").attr("id","credit-"+l_deb_id);
        $("#entries").find(".rate:last").attr("id","rate-"+l_deb_id);
        $("#entries").find(".account:last").attr("id","first_account-"+first_account_id);
        var ent=$("#entries").find(".second_account:last").attr("id","scound_account-"+scound_account_id);
        $("#first_account-"+first_account_id).autocomplete({
            source: "<?= site_url('admin/accounts/get_accounts/2') ?>"+$("#first_account-"+first_account_id).val(),
            minLength:1,
            select: function( event, ui ) {
                $("#first_account-"+first_account_id).next().val(ui.item.id);
            }
            //----------------------------------------------------------------------------------------------------//
                
                 
        });
        //-------------------------------------------------------------------------------------------------//
        $("#scound_account-"+scound_account_id).autocomplete({
            source: "<?= site_url('admin/accounts/get_accounts/2') ?>"+$("#scound_account-"+scound_account_id).val(),
            minLength:1,
            select: function( event, ui ) {
                $("#scound_account-"+scound_account_id).next().val(ui.item.id);
            }
                 
        });
        //---------------------------------------------------------------------------------------------------------------------//
        remove();
       //----------------------------------get currency rate----------------------------------------------------// 
        get_rate_by_currency('<?=site_url('admin/vouchers/get_rate')?>');
     //----------------------------------------------------------------------------------------------------------//   
        
    }
    function getDebitSum() {
        var debt_= document.getElementsByClassName("debit");
        var rate_= document.getElementsByClassName("rate");
        var debt_len=debt_.length;
        var sum=0.00;
        for (var i=0;i<debt_len;i++)
        {
            if(parseFloat(debt_[i].value).toFixed(2) == 'NaN')
                debt_[i].value = 0.00;
            var ele= parseFloat(debt_[i].value).toFixed(2);
            var eleval= parseFloat(ele) * parseFloat(rate_[i].value);
            sum+=parseFloat(eleval);
//            alert(''+ele+'*'+rate_[i].value+'='+eleval+' '+sum);
            debt_[i].value = ele;
        }
        
        document.getElementById("debit").value = parseFloat(sum).toFixed(2);
    }
    function getCreditSum() {
        var debt_= document.getElementsByClassName("credit");
        var rate_= document.getElementsByClassName("rate");
        var debt_len=debt_.length;
        var sum=0.00;
        for (var i=0;i<debt_len;i++)
        {
            if(parseFloat(debt_[i].value).toFixed(2) == 'NaN')
                debt_[i].value = 0.00;
            var ele= parseFloat(debt_[i].value).toFixed(2);
            var eleval= parseFloat(ele) * parseFloat(rate_[i].value);
            sum+=parseFloat(eleval);
            debt_[i].value = ele;
        }
        
        document.getElementById("credit").value = parseFloat(sum).toFixed(2);
    }
    function getdebit_credit()
    {
        var debit_arr=document.getElementsByClassName("debit");
        var credit_arr=document.getElementsByClassName("credit");
        var debt_len =debit_arr.length;
        var crdt_len = credit_arr.length;
        var flag=0;
        for(var i=0; i<debt_len;i++)
        {
            var debt_ele= debit_arr[i];
            var crdet_ele=credit_arr[i];
            var return_=check_values(crdet_ele.value,debt_ele.value);
            if(return_==false)
            {
                debt_ele.style.backgroundColor="yellow";
                crdet_ele.style.backgroundColor="yellow";
                flag++;
            }
            else
            {
                debt_ele.style.backgroundColor="";
                crdet_ele.style.backgroundColor="";
                                
            }
        }
        if(flag>0)
        {
            return false;    
        }
        else
        {
            return true;
        }
            
            
    } 
    function check_values(val1,val2){
        if((parseFloat(val1)!=0) && (parseFloat(val2)!=0) )
        {
                 
            return false
                 
        }
        else
        {
            return true;
        }
            
    }
    function checkTotal(con1,con2)
    {
       
        var val1= document.getElementById(con1).value;
        var val2=document.getElementById(con2).value;
        var total=(parseFloat(val2)-parseFloat(val1));
        return total.toFixed(2);
        
    }
    function check_acc(ths) {
        if($( ths ).val() == '') {
            $(ths).prev().val('0');
        }        
    }
    function rate_changes(ths) {
        ths.value = parseFloat(ths.value).toFixed(4);
        getDebitSum();
        getCreditSum();
    }
    function check_account()
   {
      
       var x=0;
       var debit_arr=document.getElementsByClassName("debit");
       var credit_arr=document.getElementsByClassName("credit");
        $("#entries").find("[name='account_hidden[]']").each(function(index,ele){
            if((parseFloat(debit_arr[index].value) >= 0) && (parseFloat(credit_arr[index].value)>=0))
               {
                if((parseFloat(debit_arr[index].value) > 0) || (parseFloat(credit_arr[index].value)>0) )
               {
                   
                   if(ele.value=="0" || ele.value==undefined || ele.value=="")
                       {
                         
                         ele.parentNode.style.backgroundColor="yellow";
                         x=1;
                         alert('<?=lang("insert_account")?>');
                         return false;
                         
                          
                       }
                       else{
                           ele.parentNode.style.backgroundColor="";
                           x=0;
                       }
                       
               }
                x=0;
             }
               else{
                   alert(parseFloat(debit_arr[index].value));
                   alert ('<?=lang('error_negative')?>');
                   x=1;
                   return false ;
               }
               


        })
        if(x==1)
           {
               return false;
           }
           else{
               return true;
           }
        
   } 
</script>
<script>
    $(document).ready(function(){
           get_currency_rate('<?=site_url('admin/vouchers/get_rate')?>',$("#currency").val(),$("#entry_rate"));
           change_rate_all('<?=site_url('admin/vouchers/get_rate')?>',$("#currency").val());
           get_rate_by_currency('<?=site_url('admin/vouchers/get_rate')?>');
      //----------------------------------------change and blur-------------------------------------------------------------------------//
        change_input_rate();
        $("#currency").change(function(){
           change_rate_list();
           get_currency_rate('<?=site_url('admin/vouchers/get_rate')?>',$("#currency").val(),$("#entry_rate"));
           change_rate_all('<?=site_url('admin/vouchers/get_rate')?>',$("#currency").val())
        });
    });
    
</script>


