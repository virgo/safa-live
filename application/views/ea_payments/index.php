<div class="span12">
    <div class="widget">
        <a class="btn Fleft" href="<?= site_url($module.'/manage/'.$ea_license_id) ?>"><?= lang('global_add_new_record') ?></a>
        <div class="path-container Fright">
            <div class="path-name Fright">
                <a href="<?= site_url('admin/dashboard') ?>"><?= lang('global_system_management') ?></a>
            </div>
            <div class="path-arrow Fright"></div>
            <div class="path-name Fright">
                <a href="<?= site_url('admin/safa_eas') ?>"><?= lang('menu_external_agent') ?></a>
            </div>
            <div class="path-arrow Fright"></div>
            <div class="path-name Fright">
                <a href="<?= site_url('ea_licenses/index/'.$ea_license_id) ?>"><?= lang('global_ea_licenses') ?></a>
            </div>
            <div class="path-arrow Fright"></div>
            <div class="path-name Fright">
                <?= lang('global_'.$module)?>
            </div>
        </div>
    </div>
</div>

<?// $this->load->view($module.'/search') ?>

<div class="row-fluid">
    <div class="widget">
        <div class="widget-header">
            <div class="widget-header-icon Fright">
                <span class="icos-pencil2"></span>
            </div>
            <div class="widget-header-title Fright">
                <?= lang('global_'.$module)?>
            </div>
        </div>
        <div class="widget-container">
            <div class='table-responsive' >
                <table cellpadding="0" class="" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th><?= lang($module.'_date') ?></th>
                            <th><?= lang($module.'_payment_type') ?></th>
                            <th><?= lang($module.'_amount') ?></th>
                            <th><?= lang($module.'_credit') ?></th>
                            <th><?= lang($module.'_notes') ?></th>
                            <th><?= lang($module.'_erp_admin_id') ?></th>
                            <th><?= lang('global_actions') ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <? if (ensure($items)): ?>
                            <? foreach ($items as $item): ?>
                                <tr>
                                    <td><?= $item->date ?></td>
                                    <td><?= $item->payment ?></td>
                                    <td><?= $item->amount ?> <?= $item->currency ?></td>
                                    <td><?= $item->credit ?></td>
                                    <td><?= $item->notes ?></td>
                                    <td><?= $item->admin ?></td>
                                    <td class="TAC">
                                        <a class="fancybox fancybox.iframe" href="<?= site_url($module."/log/" . $item->{$table_pk}) ?>" class="popup_win" rel="fancybox"><span class="icon-camera"></span></a>      
                                        <a href="<?= site_url($module."/manage/" . $ea_license_id . '/'. $item->{$table_pk}) ?>" ><span class="icon-pencil"></span></a>      
                                        <!--<a onclick="return  confirm('<?= lang('global_are_you_sure_you_want_to_delete') ?>')" href="<?= site_url($module."/delete/" . $item->{$table_pk}) ?>"><span class="icon-trash"></span></a>-->
                                    </td>
                                </tr>
                            <? endforeach ?>
                        <? endif ?>
                    </tbody>
                </table>
                <?= $pagination ?>
            </div>
        </div>
    </div>
</div>
<!--<script type="text/javascript" src="http://code.jquery.com/jquery-latest.min.js"></script>
<script type="text/javascript" src="/fancybox/lib/jquery.mousewheel-3.0.6.pack.js"></script>
<link rel="stylesheet" href="/fancybox/source/jquery.fancybox.css?v=2.1.5" type="text/css" media="screen" />
<script type="text/javascript" src="/fancybox/source/jquery.fancybox.pack.js?v=2.1.5"></script>
<link rel="stylesheet" href="/fancybox/source/helpers/jquery.fancybox-buttons.css?v=1.0.5" type="text/css" media="screen" />
<script type="text/javascript" src="/fancybox/source/helpers/jquery.fancybox-buttons.js?v=1.0.5"></script>
<script type="text/javascript" src="/fancybox/source/helpers/jquery.fancybox-media.js?v=1.0.6"></script>
<link rel="stylesheet" href="/fancybox/source/helpers/jquery.fancybox-thumbs.css?v=1.0.7" type="text/css" media="screen" />
<script type="text/javascript" src="/fancybox/source/helpers/jquery.fancybox-thumbs.js?v=1.0.7"></script>-->
<script>
//    $(document).ready(function() {
//        $('.popup_win').fancybox();
//    });
        $(".fancybox").fancybox();
</script>



