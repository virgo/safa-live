<div class="row-fluid">
    <div class="widget">
        <div class="widget-header">
            <div class="widget-header-icon Fright">
                <span class="icos-pencil2"></span>
            </div>
            <div class="widget-header-title Fright">
                <?= lang('global_'.$module)?>
            </div>
        </div>
        <div class="widget-container">
            <div class='table-responsive' >
                <table cellpadding="0" class="" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th><?= lang($module.'_date') ?></th>
                            <th><?= lang($module.'_payment_type') ?></th>
                            <th><?= lang($module.'_amount') ?></th>
                            <th><?= lang($module.'_credit') ?></th>
                            <th><?= lang($module.'_notes') ?></th>
                            <th><?= lang($module.'_erp_admin_id') ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <? if (ensure($items)): ?>
                            <? foreach ($items as $item): ?>
                                <tr>
                                    <td><?= $item->date ?></td>
                                    <td><?= $item->payment ?></td>
                                    <td><?= $item->amount ?> <?= $item->currency ?></td>
                                    <td><?= $item->credit ?></td>
                                    <td><?= $item->notes ?></td>
                                    <td><?= $item->admin ?></td>
                                </tr>
                            <? endforeach ?>
                        <? endif ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>



