<span class="span8"></span>
<div class="row-fluid">

    <?= form_open_multipart(false, 'id="validate"') ?>

    <div class="span12">
    <div class="widget">
        <div class="path-container Fright">
            <div class="path-name Fright">
                <a href="<?= site_url('admin/dashboard') ?>"><?= lang('global_system_management') ?></a>
            </div>
            <div class="path-arrow Fright"></div>
            <div class="path-name Fright">
                <a href="<?= site_url('admin/safa_eas') ?>"><?= lang('menu_external_agent') ?></a>
            </div>
            <div class="path-arrow Fright"></div>
            <div class="path-name Fright">
                <a href="<?= site_url('ea_licenses/index/'.$this->uri->segment(3)) ?>"><?= lang('global_ea_licenses') ?></a>
            </div>
            <div class="path-arrow Fright"></div>
            <div class="path-name Fright">
                <?= lang('global_'.$module)?>
            </div>
        </div>
    </div>
</div>

    <div class="widget">
        <div class="widget-header">
            <div class="widget-header-icon Fright"><span class="icos-pencil2"></span></div>
            <div class="widget-header-title Fright"><?= lang('global_' . $module) ?></div>
        </div>
        <div class="block-fluid">
            <? if (validation_errors()): ?>
                <?= validation_errors(); ?>
            <? endif ?> 

            <div class="row-form">
                <div class="span6">
                    <div class="span4">
                        <?= lang($module . '_payment_type') ?>:
                    </div>
                    <div class="span8">
                        <?= form_dropdown('payment_type', ddgen('crm_payment_types', array('crm_payment_type_id', name())), set_value("payment_type", $item->payment_type), 'class="validate[required] payment_type"') ?>
                    </div>
                </div>
                
                <div class="span6">
                    <div class="span4">
                        <?= lang($module . '_date') ?>:
                    </div>
                    <div class="span8">
                        <?= form_input('date', set_value("date", $item->date), 'class="validate[required] datetimepicker"') ?>
                    </div>
                </div>
            </div>
            <div class="row-form">
                <div class="span6">
                    <div class="span4">
                        <?= lang($module . '_crm_package_id') ?>:
                    </div>
                    <div class="span8">
                        <?= form_dropdown('crm_package_id', $country_packages, set_value("crm_package_id", $item->crm_package_id), 'class="package"') ?>
                    </div>
                </div>
                
                <div class="span6">
                    <div class="span4">
                        <?= lang($module . '_voucher') ?>:
                    </div>
                    <div class="span8">
                        <?= form_input('voucher', set_value("voucher", $item->voucher), 'class="validate[required] voucher"') ?>
                    </div>
                </div>
                
                
            </div>
            <div class="row-form">
                <div class="span6">
                    <div class="span4">
                        <?= lang($module . '_crm_cashbox_id') ?>:
                    </div>
                    <div class="span8">
                        <?= form_dropdown('crm_cashbox_id', ddgen('crm_cashbox', array('crm_cashbox_id', 'name')), set_value("crm_cashbox_id", $item->crm_cashbox_id), 'class="validate[required] cashbox crm_cashbox_id"') ?>
                    </div>
                </div>
                <div class="span6">
                    <div class="span4">
                        <?= lang($module . '_erp_currency_id') ?>:
                    </div>
                    <div class="span8">
                        <?= form_dropdown('erp_currency_id', ddgen('erp_currencies', array('erp_currency_id', name())), set_value("erp_currency_id", $item->erp_currency_id), 'class="validate[required] erp_currency_id" disabled') ?>
                    </div>
                </div>
            </div>
            <div class="row-form">
                <div class="span6">
                    <div class="span4">
                        <?= lang($module . '_amount') ?>:
                    </div>
                    <div class="span8">
                        <?= form_input('amount', set_value("amount", $item->amount), 'class="validate[required] amount"') ?>
                    </div>
                </div>
                <div class="span6">
                    <div class="span4">
                        <?= lang($module . '_credit') ?>:
                    </div>
                    <div class="span8">
                        <?= form_input('credit', set_value("credit", $item->credit), 'class="validate[required] credit"') ?>
                    </div>
                </div>
                
            </div>
            <div class="row-form">
                <div class="span12">
                    <div class="span2">
                        <?= lang($module . '_notes') ?>:
                    </div>
                    <div class="span10">
                        <?= form_textarea('notes', set_value("notes", $item->notes), 'class="span12"') ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="toolbar bottom TAC">
        <input type="submit" name="submit" value="<?= lang('global_submit') ?>" class="btn btn-primary"> &nbsp;
        <input type ="button" 
               value="<?= lang('global_back') ?>"
               class="btn btn-primary" 
               onclick="window.location = '<?= site_url($module.'/index/'.$this->uri->segment(3)) ?>'" />

    </div>
    <?= form_close() ?>       
</div> 
<script>
   
    function change_package()
    {
        $.post('<?= site_url('ea_payments/package') ?>', {package_id: $('.package').val()}, function(data){
            json = JSON.parse(data);
            console.log(json);
            if(json.release == 1)
            {
//                $('.amount').prop('disabled', false);
//                $('.erp_currency_id').prop('disabled', false);
                $('.credit').prop('disabled', false);
                $('.crm_cashbox_id').prop('disabled', false);
            }
            else
            {
                // VALUES
                $('.amount').val(json.amount);
//                $('.erp_currency_id').val(json.erp_currency_id);
                $('.credit').val(json.credit);
//                $('.crm_cashbox_id').val(json.crm_cashbox_id);
//                $('.amount').prop('disabled', true);
//                $('.erp_currency_id').prop('disabled', true);
                $('.credit').prop('disabled', true);
//                $('.crm_cashbox_id').prop('disabled', true);
            }
        });
    }
    $('.crm_cashbox_id').change(function(){
        $.getJSON('<?= site_url('ea_payments/cashbox') ?>/' + $('.crm_cashbox_id').val(), function(data){
            $('.erp_currency_id').val(data.erp_currency_id);
        });
    });
    
    
    $('.package').change(function(){
        change_package();
    });
    change_package('<?= set_value('crm_cashbox_id', $item->crm_cashbox_id) ?>');
     $('.datetimepicker').datetimepicker({
        dateFormat: 'yy-mm-dd',
        timeFormat: 'HH:mm'
    });
    
    
</script>