
<style>
table td, table th {
    border: 1px solid #000;
    border-right: 0 none !important;
}
</style>
<div class="table-responsive">
<table class="fsTable" cellpadding="0" cellspacing="0" >
	<thead>
		<tr>                      
			<th rowspan="2"><?= lang('trip_code') ?></th>

			<? if (in_array('code', $cols)): ?>
			<th rowspan="2"><?= lang('trip_id') ?></th>
			<? endif; ?>

			<? if (in_array('supervisor', $cols)): ?>
			<th rowspan="2"><?= lang('supervisor') ?></th>
			<? endif; ?>

			<? if (in_array('contract', $cols)): ?>
			<th rowspan="2"><?= lang('uo_name') ?></th>
			<? endif; ?>

			<? if (in_array('operator', $cols)): ?>
			<th rowspan="2"><?= lang('operator') ?></th>
			<? endif; ?>

			<? if (in_array('confirmation_number', $cols)): ?>
            <th rowspan="2"><?= lang('confirmation_number') ?></th>
           	<? endif; ?>
                        
			<? if (in_array('operation_order_id', $cols)): ?>
			<th rowspan="2"><?= lang('operation_order_id') ?></th>
			<? endif; ?>

                        <? if (in_array('buses_count', $cols)): ?>
                            <th rowspan="2"><?= lang('buses_count') ?></th>
                        <? endif; ?>
                            
			<? if (in_array('segment_type', $cols)): ?>
			<th rowspan="2"><?= lang('segment_type') ?></th>
			<? endif; ?>


			<? if (in_array('from', $cols)): ?>
			<th rowspan="2"><?= lang('from') ?></th>
			<? endif; ?>


			<? if (in_array('to', $cols)): ?>
			<th rowspan="2"><?= lang('to') ?></th>
			<? endif; ?>


			<? if (in_array('starting', $cols)): ?>
			<th rowspan="2"><?= lang('starting') ?></th>
			<? endif; ?>

         

			<? if (in_array('individuals_count', $cols)): ?>
			<th rowspan="2"><?= lang('individuals_count') ?></th>
			<? endif; ?>

			<? if (in_array('passports_count', $cols)): ?>
			<th rowspan="2"><?= lang('passports_count') ?></th>
			<? endif; ?>


			<? if (in_array('seats', $cols)): ?>
			<th rowspan="2"><?= lang('seats') ?></th>
			<? endif; ?>

			<!-- 
			<? if (in_array('agent', $cols) || in_array('status', $cols)): ?>
			<th colspan="2"><?= lang('agent') ?></th>
			<? endif; ?>

			<? if (in_array('drivers', $cols)): ?>
			<th rowspan="2"><?= lang('drivers') ?></th>
			<? endif; ?>
             -->
                         
           <th rowspan="2" width="150px"><?= lang('notes') ?></th>
		</tr>

		

		<!-- 
		<? if (in_array('agent', $cols) || in_array('status', $cols)): ?>
		<tr>
			<th><?= lang('from') ?></th>
			
			<th><?= lang('to') ?></th>
			</tr>
			<? endif; ?>
 		-->
		
		
	</thead>



	<tbody>
	<? foreach ($trips_ds as $val): ?>
		<tr>
                         
			<td><?php 
			if($val['trip_name']!='') {
				echo $val['trip_name'];
			} else {
				echo $val['trip_title'];
			}
			?></td>


			<? if (in_array('code', $cols)): ?>
			<td>
			
			<?php 
                  if($val['safa_internalsegmenttype_id']==1 || $val['safa_internalsegmenttype_id']==2) {
             ?>
                    <?php 
						$status_color = $this->flight_states->getFlightColor($val['fs_flight_id']);
					?>
				
				<span style="text-align: center ;background-color:#<?= $status_color?>" class="label">
                   <?= $val['flight_number'] ?> <?= $val['safa_transporters_code'] ?><br /><?= $val['transporter_name'] ?>
                  </span>
                                    
            <?php }?>
         </td>
			<? endif; ?>


			<? if (in_array('supervisor', $cols)): ?>
			<td><?php 
			if($val['supervisor']!='') {
				echo $val['supervisor'];
			} else {
				echo $val['trip_supervisors'];
			}
			?></td>
			<? endif; ?>


			<? if (in_array('contract', $cols)): ?>
			<td><?= $val['uo_name'] ?></td>
			<? endif; ?>

			<? if (in_array('operator', $cols)): ?>
			<td><?= $val['operator_name'] ?></td>
			<? endif; ?>

			<? if (in_array('confirmation_number', $cols)): ?>
          	<td><?= $val['confirmation_number'] ?></td>
         	<? endif; ?>
                            
			<? if (in_array('operation_order_id', $cols)): ?>
			<td><?= $val['operator_reference'] ?></td>
			<? endif; ?>

                            
                            <? if (in_array('buses_count', $cols)): ?>
                                <td>
                                 <?php 
                                    $this->internalsegments_drivers_and_buses_model->safa_internalsegments_id=$val['safa_internalsegment_id'];
                                    $drivers_and_buses_rows= $this->internalsegments_drivers_and_buses_model->get();
                                    $count_drivers_and_buses = count($drivers_and_buses_rows);
                                    if($count_drivers_and_buses>0) {
                                    ?>	
                                    X <?php echo $count_drivers_and_buses; ?> 
									    
                                        <a title='<?= lang('drivers_and_buses') ?>' href="<?= site_url('uo/all_movements/drivers_and_buses/' . $val['safa_internalsegment_id']) ?>" style="color:black; direction:rtl">
                                             <img src="<?= IMAGES2 ?>/car.png" align="left" >
                                        </a> 
                                    <?php 
                                    } 
                                    ?>
                                </td>
                            <? endif; ?>
                                
			<? if (in_array('segment_type', $cols)): ?>
			<td><?= $val['segment_name'] ?></td>
			<? endif; ?>



			<? if (in_array('from', $cols)): ?>
			<td><?
			switch ($val['safa_internalsegmenttype_id']):

case 1:
	echo(lang('port').': '.element($val['erp_port_id'], $ports));
	break;

case 2: echo("<font class=city_color>".$val['start_city_name']."</font><br>");
echo(element($val['erp_start_hotel_id'], $hotels));
break;

case 3: echo("<font class=city_color>".$val['start_city_name']."</font><br>");
echo(element($val['erp_start_hotel_id'], $hotels));
break;

case 4:
	echo("<font class=city_color>".$val['start_city_name']."</font><br>");
	echo(element($val['erp_start_hotel_id'], $hotels));
	break;

	endswitch;
	?></td>
	<? endif; ?>


	<? if (in_array('to', $cols)): ?>
			<td><?
			switch ($val['safa_internalsegmenttype_id']):

case 1:
	echo("<font class=city_color>".$val['end_city_name']."</font><br>");
	echo(element($val['erp_end_hotel_id'], $hotels));
	break;

case 2:
	echo(lang('port').': '.element($val['erp_port_id'], $ports));
	break;

case 3:
	echo(element($val['safa_tourism_place_id'], $places));
	break;

case 4:
	echo("<font class=city_color>".$val['end_city_name']."</font><br>");
	echo(element($val['erp_end_hotel_id'], $hotels));
	break;

	endswitch;
	?></td>
	<? endif; ?>



	<? if (in_array('starting', $cols)): ?>
			<td><span style="direction: ltr"><?= get_date($val['start_datetime']) . " " . get_time($val['start_datetime']) ?></span></td>
			<? endif; ?>


			<!--
                    <? if (in_array('ending', $cols)): ?>
                        <td><span style="direction: ltr"><?= get_date($val['end_datetime']) . " " . get_time($val['end_datetime']) ?></span></td>
                    <? endif; ?>
                    -->


							<? if (in_array('individuals_count', $cols)): ?>
                                <td><?= strlen($val['individuals_count'])?$val['individuals_count']:((int)$val['adult_seats']+(int)$val['child_seats']+(int)$val['baby_seats']) ?></td>
                            <? endif; ?>
                            
                            <? if (in_array('passports_count', $cols)): ?>
                                <td><?= $val['passports_count'] ?></td>
                            <? endif; ?>
                            
                            <? if (in_array('seats', $cols)): ?>
                                <td><?= strlen($val['seats_count'])?$val['seats_count']:((int)$val['adult_seats']+(int)$val['child_seats']) ?></td>
                            <? endif; ?>
                            
                            
                            <!-- this updates for phase 2  assign a new aggents ---> 

                         	<? if (in_array('agent', $cols) || in_array('status', $cols)): ?>
                                    <td>
                                        <? //if ($usergroup_id == 1 || $usergroup_id == 2) { ?>
                                            <a style="display:none"   href="javascript:void(0)"  class="icon-remove roleback-agent"  title="<?= lang('roll_back_user') ?>" onclick="rollback_agent_view(<? if ($val['safa_uo_user_id'] == ''): ?>0<? else: ?><?= $val['safa_uo_user_id'] ?><? endif; ?>, this)"></a> 
                                        <? //}; ?> 
                                        <div class='assign-agent'   role='assign-agent' id='<?= $val['safa_internalsegment_id'] ?>'  style='float:none !important '  >
                                            <? if ($val['safa_uo_user_id'] == ''): ?>
                                                
                                            <? else: ?>
                                                <?php if($val['safa_uo_user_id']!=''){ echo item('safa_uo_users', name(), array('safa_uo_users.safa_uo_user_id' => $val['safa_uo_user_id']));} ?>                                    
                                               
                                            <? endif; ?>
                                            <? //if (isset($usergroup_id)) { ?>
                                                <? //if ($usergroup_id == 1 || $usergroup_id == 2) { ?>
                                                    <br>
                                                    <? if ($val['safa_uo_user_id'] == ''): ?> 
                                                        <? $title = lang('assign_uo_user') ?>
                                                    <? else: ?>
                                                        <? $title = lang('change_uo_user') ?>
                                                    <? endif; ?>
<!--                                                    <a class="icon-pencil"   href="javascript:void(0)"  title="<?= $title ?>" onclick="get_agent_view(<? if ($val['safa_uo_user_id'] == ''): ?>0<? else: ?><?= $val['safa_uo_user_id'] ?><? endif; ?>, this)"></a>-->
                                                </div>   
                                            <? //}; ?>
                                        <? //}; ?>
                                    </td>
                                <? endif; ?> 
                            
                            <!-- this updates for phase 2  updating the status  seggment--->     
                         <? if (in_array('agent', $cols) || in_array('status', $cols)): ?>
                                    <td>
                                        <div class="" <? if ($val['status_notes']): ?>title='<?= $val['status_notes'] ?>'<? endif; ?> id='chg-status<?= $val['safa_internalsegment_id'] ?>' role='change-status'   class="change_status"  >
<!--                                            <?= $val['status'] ?>-->
                                            
                                            <?php if($val['safa_uo_agent_id']!=''){ echo item('safa_uo_users', name(), array('safa_uo_users.safa_uo_user_id' => $val['safa_uo_agent_id']));} ?> 
                                            <br>
<!--                                            <a href="#edit-segment-status" title="<?= lang('update_segment_status') ?>" onclick="openStatusChangepanal(<?= $val['safa_internalsegment_id'] ?>,<?= $val['safa_internalsegmentestatus_id'] ?>)"  class="icon-pencil"  role="button" data-toggle="modal"></a>-->
                                        </div>

                                    </td>
                                <? endif; ?>                              

                             <? if (in_array('drivers', $cols)): ?>
                          <td class='TAC'>
								<?php 
                                    $this->internalsegments_drivers_and_buses_model->safa_internalsegments_id=$val['safa_internalsegment_id'];
                                    $drivers_and_buses_rows= $this->internalsegments_drivers_and_buses_model->get();
                                    $count_drivers_and_buses = count($drivers_and_buses_rows);
                                    if($count_drivers_and_buses>0) {
                                    ?>	
                                    X <?php echo $count_drivers_and_buses; ?> 
									    
                                       <img src="<?= IMAGES2 ?>/car.png" align="left" >
                                        
                                        <br/> <br/>
									 <?php 
									 foreach($drivers_and_buses_rows as $drivers_and_buses_row) {
									 	echo $drivers_and_buses_row->safa_transporters_driver_name .' - '. $drivers_and_buses_row->safa_transporters_driver_phone.'<br/>';
									 }
									 ?>	    
                                            
                                    <?php 
                                    } else {
                                    ?>
                            	
<!--                            <?=  lang('no_drivers')?>-->
                            <?php 	
                            }
                            ?>
                          </td>  
                          <? endif ?>
                          <td>
                          <?= $val['safa_uo_agent_notes'] ?>
                          </td>
                        </tr>

		<? endforeach; ?>

	</tbody>
</table>
</div>
		<?= ($print_statement)?$print_statement:"" ?>