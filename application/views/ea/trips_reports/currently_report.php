<!-- By Gouda, To Close menu On this screen -->
<script>
jQuery(function() { 
	menu1.toggle();
})
</script> 

<!-- arraving trips section -->


<?= form_open('', array('method' => 'GET')) ?>

<!-- this field to hold values (today, twm) -->
<input type="hidden" name="departure" value="<?=$this->input->get('departure')?>" class="btn " />
<input type="hidden" name="arrival" value="<?=$this->input->get('arrival')?>" class="btn " />


<div class="span12">
    <div class="widget">
    
     
    <div class="path-container Fright">
        <div class="icon"><i class="icos-pencil2"></i></div> 
         <div class="path-name Fright">
               <a href="<?= site_url('uo/dashboard') ?>"><?= lang('global_system_management') ?></a>
        </div>    
        <div class="path-arrow Fright">
            </div>
            <div class="path-name Fright">
                <span><?= lang('menu_uos_current_status') ?></span>
        </div>
        
        <div class="btn-group" style="float:<?=lang('XALIGN')?>; margin:1px 0 0 1px;">
            <? if ( count($arriving)) : ?>
                <input type="submit" name="print_arr_report" value="<?=lang('print')?>" class="btn " />
                <input type="submit" name="export_arr_report" value="<?=lang('export')?>" class="btn " />
            <? endif; ?>
             </div>
    </div> 
    </div> 
 </div>

    
    <div class='row-fluid'>
        <div class='widget'>  
        
        <div class="widget-header">
	
	            <div class="widget-header-icon Fright">
	                <span class="icos-pencil2"></span>
	            </div>
	
	            <div class="widget-header-title Fright">

			    <div class="path-container Fright">
			        <div class="icon"><i class="icos-pencil2"></i></div> 
			         <div class="path-name Fright">
			                <?= lang('arriving_form_name').$this->arriving_pane ?>
			        </div>    
			        
			        <div class="btn-group" style="float:<?=lang('XALIGN')?>; margin:1px 0 0 1px;">
			            <? if ( count($arriving)) : ?>
			                <input type="submit" name="print_arr_report" value="<?=lang('print')?>" class="btn " />
			                <input type="submit" name="export_arr_report" value="<?=lang('export')?>" class="btn " />
			            <? endif; ?>
			             </div>
			    </div>
	                
	                
	                
	            </div>
	            
	            
	            <a href="<?= site_url("uo/dashboard/?arrival=today&departure=".$this->input->get('departure')) ?>" class="btn Fleft"><?= lang('today') ?></a> 
    			<a href="<?= site_url("uo/dashboard/?arrival=twm&departure=".$this->input->get('departure')) ?>"  class="btn Fleft" ><?= lang('twm') ?></a>                                                    
      
	
	    </div>
	        
	        
        <div class="widget-container">
            <div class='table-responsive' >   
                
        <table class="fpTable" cellpadding="0" cellspacing="0">
            <thead>
                <tr>
                    
                    <th><?= lang('trip') ?></th>
                    
                    <th><?= lang('trip_id') ?></th>

                    <th><?= lang('arr_date') ?></th>

                    <th><?= lang('arr_time') ?></th>

                    <th><?= lang('contract') ?></th>

                    <th><?= lang('count') ?></th>

                    <th><?= lang('hotel') ?></th>

                    <th><?= lang('trans_company') ?></th>

                    <th><?= lang('port') ?></th>

                    <th><?= lang('nationality') ?></th>

                    <th><?= lang('supervisor') ?></th>
                    
                    <th><?= lang('status') ?></th>                    

                </tr>
            </thead>
            <tbody>

                <? foreach ($arriving as $val): ?>
                    <tr>
                        
                        <td>
                        <?php 
			if($val['trip_name']!='') {
				echo $val['trip_name'];
			} else {
				echo $val['trip_title'];
			}
			?>
                        </td>
                        
                        <td>
                            <span style="background-color:#<?= $val['status_color']?>" class="label">
                            <?= $val['flight_number'] ?>
                            </span>                            
                         </td>

                        <td><?= get_date($val['arrival_date']) ?></td>

                        <td><?= get_time($val['arrival_time']) ?></td>

                        <td><?= $val['cn'] ?></td>

                        <td>
                            <a href="<?= site_url("trip_details/details/". $val['trip_id']) ?>" target="_blank">
                                <?= ($val['travellers'])?($val['travellers']):($val['travellers_adult_count']+$val['travellers_child_count']+$val['travellers_infant_count']) ?>
                            </a>
                        </td>

                        <td><?= $val['hotel_name'] ?></td>

                        <td><?= $val['transporter'] ?></td>

                        <td>
                        
                                <?= $val['start_port_name'] ?>
								 - 
								<?= $val['end_port_name'] ?>
								(<?= $val['end_port_hall_name'] ?>)
                                
                                
                                </td>

                        <td><?= $val['nationality'] ?></td>

                        <td>
                        <?php 
			if($val['supervisor']!='') {
				echo $val['supervisor'];
			} else {
				echo $val['trip_supervisors'];
			}
			?>
                        </td>    
                        
                        <td><?= $val['status_name'] ?></td>

                    </tr>
                <? endforeach; ?>

            </tbody>
        </table> 
        
</div>
</div>
             
    </div>
</div>
<!-- end arraving trips section -->

<!-- depatrual trip section  -->

<div class="widget">
 <div class="widget-header">
            <div class="widget-header-icon Fright">
                <span class="icos-pencil2"></span>
            </div>
            <div class="widget-header-title Fright">
	    <div class="path-container Fright">
	        <div class="icon"><i class="icos-pencil2"></i></div> 
	         <div class="path-name Fright">
	                <?= lang('departing_form_name').$this->departing_pane ?>
	        </div>    
	        <div class="btn-group" style="float:<?=lang('XALIGN')?>; margin:1px 0 0 1px;">
         
	           <? if (count($departing)) :?>
                <input type="submit" name="print_dep_report" value="<?=lang('print')?>" class="btn " />
                <input type="submit" name="export_dep_report" value="<?=lang('export')?>" class="btn " />
            	<? endif;?>
	        </div>
	    </div>
	    
	    
      </div>
      
      <a href="<?= site_url("uo/dashboard/?arrival=".$this->input->get('arrival')."&departure=today") ?>" class="btn Fleft"><?= lang('today') ?></a>
      <a href="<?= site_url("uo/dashboard/?arrival=".$this->input->get('arrival')."&departure=twm") ?>"  class="btn Fleft" ><?= lang('twm') ?></a>                                                                
      
      
	 </div>
    
  
    
    
     
     <div class="widget-container">
            <div class='table-responsive' >            
        <table class="fpTable" cellpadding="0" cellspacing="0">
            <thead>
                <tr>

                    <th><?= lang('trip') ?></th>
                    
                    <th><?= lang('trip_id') ?></th>

                    <th><?= lang('dep_date') ?></th>

                    <th> <?= lang('dep_time') ?></th>

                    <th><?= lang('contract') ?></th>

                    <th><?= lang('count') ?></th>

                    <th><?= lang('hotel') ?></th>

                    <th><?= lang('trans_company') ?></th>

                    <th><?= lang('port') ?></th>

                    <th><?= lang('nationality') ?></th>

                    <th><?= lang('supervisor') ?></th>      
                    
                    <th><?= lang('status') ?></th>


                </tr>
            </thead>


            <tbody>                        


                <? foreach ($departing as $val): ?>
                    <tr>
                        
                        <td><?= $val['trip_name'] ?></td>
                        
                        <td>
                            <span style="background-color:#<?= $val['status_color']?>" class="label">
                            <?= $val['flight_number'] ?>
                            </span>
                        </td>

                        <td><?= get_date($val['flight_date']) ?></td>

                        <td><?= get_time($val['departure_time']) ?></td>

                        <td><?= $val['cn'] ?></td>

                        <td>
                            <!--<a href="<?= site_url("trip_details/details/". $val['trip_id']) ?>" target="_blank">
                                <?= ($val['travellers'])?($val['travellers']):($val['travellers_adult_count']+$val['travellers_child_count']+$val['travellers_infant_count']) ?>
                            </a>
                        -->
                        <?= $val['seats_count'] ?>
                        </td>

                        <td><?= $val['hotel_name'] ?></td>

                        <td><?= $val['transporter'] ?></td>

                        <td>
                        
                        <?= $val['start_port_name'] ?>
			(<?= $val['start_port_hall_name'] ?>)
			 - 
			<?= $val['end_port_name'] ?>
                        
                        </td>

                        <td><?= $val['nationality'] ?></td>

                        <td><?= $val['supervisor'] ?></td>

                        <td><?= $val['status_name'] ?></td>


                    </tr>
                <? endforeach; ?>

            </tbody>
        </table>      
    </div>
     </div>
</div>
<?= form_close() ?>
<!-- end departual trip section -->

