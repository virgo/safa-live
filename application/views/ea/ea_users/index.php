<div class="span12">
    <div class="widget">
       

        <div class="path-container Fright">
            <div class="path-name Fright">
                <a href=""><?= lang('global_system_management') ?></a>
            </div>
            <div class="path-arrow Fright">
            </div>
            <div class="path-name Fright">
                <?= lang('menu_external_agent_Users') ?>
            </div>
        </div>

    </div>
</div>


<div class="row-fluid">
    <div class="widget">
        <style>
            .updated_msg{
                display:none;
                background-color:#ccee97;
                font-weight: bold;
                text-align: center;
                border:3px solid #cccdc9; 
                padding:20px;  
                margin-bottom:10px;
                border-radius:15px;
                margin:10px; 
            }
    .widget {width:98%}

        </style>

        <div class='row-fluid' align='center' >
            <div  class='updated_msg' >
                <br><input  type ="button" value="<?= lang('global_submit') ?>"class="btn btn-primary" id='ok' >
            </div> 
        </div>


        <div class="widget-header">

            <div class="widget-header-icon Fright">
                <span class="icos-pencil2"></span>
            </div>

            <div class="widget-header-title Fright">
                <?= lang('menu_external_agent_Users') ?>
            </div>
             <a class="btn Fleft" href="<?= site_url('ea/ea_users/add') ?>"><?= lang('global_add_new_record') ?></a>
        </div>

<div class="widget-container">
        <div class='table-responsive' >
            <table cellpadding="0" class="fsTable" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <? if (name() == 'name_ar'): ?>
                            <th><?= lang('ea_users_name_ar') ?></th>
                        <? else: ?> 
                            <th><?= lang('ea_users_name_la') ?></th>
                        <? endif; ?>
                        <th><?= lang('ea_users_username') ?></th>
                        <th><?= lang('global_actions') ?></th>
                    </tr>
                </thead>
                <tbody>
                    <? if (isset($items)): ?>
                        <? foreach ($items as $item): ?>
                            <tr>
                                <? if (name() == 'name_ar'): ?>
                                    <td><?= $item->name_ar ?></td>
                                <? else: ?>  
                                    <td><?= $item->name_la ?></td>
                                <? endif; ?>   
                                <td><?= $item->username ?></td>
                                <td class="TAC">
                                    <a href="<?= site_url("ea/ea_users/edit/" . $item->safa_ea_user_id) ?>" ><span class="icon-pencil"></span></a>      
                                    <a  onclick=" return  confirm('<?= lang('global_are_you_sure_you_want_to_delete') . '  ' . $item->username ?>')" href="<?= site_url("ea/ea_users/delete/" . $item->safa_ea_user_id) ?>"><span class="icon-trash"></span></a>
                                </td>
                            </tr>
                        <? endforeach; ?>
                    <? endif; ?>
                </tbody>
            </table>
        </div>
    </div>
    </div>
</div>



<script type='text/javascript' src='<?= JS ?>/custom/table_operation.js'></script>
<script>
    
</script>

           <!-- hide and show -->
  <script type="text/javascript">
    $(document).ready(function(){
        $('.delete_item').click(function(){
                        var name= $(this).attr('name');
                        var safa_ea_user_id = $(this).attr('id');
                        <? if (lang('global_lang') == 'ar'): ?>
                            $('#msg').text("<?= lang('global_are_you_sure_you_want_to_delete') ?>"+" "+name)
                        <? else: ?>
                            $('#msg').text(name+" "+"<?= lang('global_are_you_sure_you_want_to_delete') ?>")
                        <?  endif;?>    
			 $('#yes').click(function(){
                         var answer =$(this).attr('id');
                           if(answer==='yes'){
				$.ajax({
					type: "POST",
					dataType: 'json',
					url: "<?php echo site_url('ea/ea_users/delete');?>",
					data: {'safa_ea_user_id':safa_ea_user_id},
					success: function(msg){
                                         if(msg.response==true){
                                            var del = safa_ea_user_id;
                                            $("#row_"+del).remove();
                                             <? if (lang('global_lang') == 'ar'): ?>
                                                $('.updated_msg').text("<?=lang('global_delete_confirm')?>"+" "+name);
                                                $('.updated_msg').append('<br>');
                                                $('.updated_msg').append('<input  type ="button" value="<?=lang('global_submit')?>"class="btn btn-primary" id="ok" >');
                                             <? else: ?>
                                                $('.updated_msg').text(name+" "+"<?=lang('global_delete_confirm')?>");
                                                $('.updated_msg').append('<br>');
                                                $('.updated_msg').append('<input  type ="button" value="<?=lang('global_submit')?>"class="btn btn-primary" id="ok" >');
                                             <?  endif;?>
                                              $('.updated_msg').show();
                                              $('#ok').click(function(){
                                                 $('.updated_msg').hide();
                                               });
				          }else if(msg.response==false){
                                                    $('.updated_msg').text(msg.msg);
                                                    $('.updated_msg').append('<br>');
                                                    $('.updated_msg').append('<input  type ="button" value="<?=lang('global_submit')?>"class="btn btn-primary" id="ok" >');
                                                    $('.updated_msg').show();
                                                    $('#ok').click(function(){
                                                       $('.updated_msg').hide();
                                                       
                                                    });
				          }
					}
				});
			}else{
				return FALSE;
			}
                      });
		});
	});
	</script>

