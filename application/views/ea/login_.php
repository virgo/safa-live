    <div class="login" id="login">
        <div class="wrap">
                 <? if (validation_errors()): ?>
                 <div>
                    <?php echo validation_errors(); ?>
                </div>
         <? endif ?>
            <h1><?=  lang('wellcom_title')?></h1>
            <?=form_open("ea/login/index","id='validate'")?>  
            <div class="row-fluid">
                <div class="input-prepend">
                    <span class="add-on"><i class="icon-user"></i></span>
                    <input type="text" name="username" value="<?=set_value("username","")?>" placeholder="<?=  lang('login_placeholder_username')?>" class="validate[required]"/>
                </div>                                                 
                <div class="input-prepend">
                    <span class="add-on"><i class="icon-exclamation-sign"></i></span>
                    <input type="password" name="password" value="<?=set_value("password","")?>" placeholder="<?=  lang('login_placeholder_password')?>" class="validate[required]"/>
                </div>          
                <div class="dr"><span></span></div>                                
            </div>
            <div class="input-prepend">
                    <?= form_dropdown('language', $languages, set_value('language', 2)) ?>
            </div>
            <div class="row-fluid">
                <div class="span8 remember">                    
                    <input type="checkbox" name="remember" value="1" <?= set_checkbox('remember', '0'); ?>/><?=  lang('login_rememberme')?>                    
                </div>
                <div class="span4    TAR">
                    <input type="submit" class="btn btn-block btn-primary" value="<?=  lang('login_label')?>"/>
                </div>
            </div>
          <?=form_close()?>
            <div class="dr"><span></span></div>
            <div class="row-fluid">
                <div class="span8">                    
                    <button class="btn btn-block" value="<?=lang("login_forget_password")?>" onClick="loginBlock('#forgot');"><?=  lang('login_forget_password_q')?></button>
                </div>
                <div class="span4">
                    <button class="btn btn-warning btn-block" onClick="loginBlock('#sign');"><?=lang("registration")?></button>
                </div>
            </div>            
        </div>
    </div>
    
    <!-- By Gouda -->
    <?=form_open("ea/login/register","id='validate'")?>
    <div class="login" id="sign" style="width: 500px; margin-top: -100px">
        <div class="wrap" style="width: 480px">
            <h1><?=lang("registration")?></h1>
            
            <div class="row-fluid">
            
            	<!-- 
                <div class="input-prepend">
                    <span class="add-on"><i class="icon-user"></i></span>
                    <input type="text" name="login" placeholder="Login"/>
                </div>
                <div class="input-prepend">
                    <span class="add-on"><i class="icon-envelope"></i></span>
                    <input type="text" name="login" placeholder="E-mail"/>
                </div>                
                <div class="dr"><span></span></div>                
                <div class="input-prepend">
                    <span class="add-on"><i class="icon-exclamation-sign"></i></span>
                    <input type="text" name="password" placeholder="Password"/>
                </div>                          
                <div class="input-prepend">
                    <span class="add-on"><i class="icon-question-sign"></i></span>
                    <input type="text" name="re-password" placeholder="Re-Password"/>
                </div>                                
                <div class="dr"><span></span></div> 
                 -->
                 
                 
                 
                 <div class="row-form" >
                        <div class="span2"><?= lang('username') ?><font style='color:red' >*</font></div>
                        <div class="span10"><?= form_input('username', set_value('username'),"class='validate[required,custom[UserName]]' id='username' ") ?>
                            <span class='bottom' style='color:red'>
								<!-- <font style="color:gray" > <?=lang('unique_username')?></font>-->
                                  <?=form_error('username')?>
                                  <div id="dv_username_availability"></div>
                           </span>
                        </div> 
                    </div>
                    <div class="row-form" >
                        <div class="span2"><?= lang('password') ?><font style='color:red' >*</font></div>
                        <div class="span10"><?= form_password('password', set_value('password'),"class='validate[required]' id='password' ") ?>
                             <span class='bottom' style='color:red'>
                                  <?=form_error('password')?>
                           </span>
                        </div>  
                    </div>
                    <div class="row-form" >
                        <div class="span2"><?= lang('repeat_password') ?><font style='color:red' >*</font></div>
                        <div class="span10"><?= form_password('confpassword', set_value('confpassword'),"class='validate[required,equals[password]]'") ?>
                            <span class='bottom' style='color:red'>
                                  <?=form_error('confpassword')?>
                           </span> 
                        </div>
                    </div>
                        
                	<div class="row-form" >
                        <div class="span2"><?= lang('name_ar') ?>
                             <?if(name()=='name_ar'):?><font style='color:red' >*</font><?endif;?>
                        </div>
                        <div class="span10">
                            <input type ='text' name='name_ar' value='<?=set_value('name_ar')?>'<?if(name()=='name_ar'):?>class="validate[required]"<?endif;?>/> 
                            <span class='bottom' style='color:red'>
                                <?=form_error('name_ar')?>
                            </span>
                        </div> 
                    </div>
                    <div class="row-form" >
                        <div class="span2">
                            <?= lang('name_la') ?>
                             <?if(name()=='name_la'):?><font style='color:red' >*</font><?endif;?>
                        </div>
                        <div class="span10"><input type ='text' name='name_la' value='<?=set_value('name_la')?>'<?if(name()=='name_la'):?>class="validate[required]"<?endif;?>/> 
                              <span class='bottom' style='color:red'>
                                   <?=form_error('name_la')?>
                            </span>
                        </div> 
                    </div>
                    
                    <div class="row-form" >
                        <div class="span2"><?= lang('country') ?><font style='color:red' >*</font></div>
                        <div class="span10" ><?= form_dropdown('country', ddgen('erp_countries', array('erp_country_id', name())), set_value('country'), " class='validate[required] val1'  style='width:100%;' "  ) ?>
                          <span class='bottom' style='color:red'>
                                   <?=form_error('country')?>
                           </span>   
                        </div>  
                    </div>
                    <div class="row-form" >
                        <div class="span2"><?= lang('phone') ?><font style='color:red' >*</font></div>
                        <div class="span10"><?= form_input('phone', set_value('phone'),'class="validate[required,custom[phone]]"') ?>
                          <span class='bottom' style='color:red'>
                                   <?=form_error('phone')?>
                           </span>  
                        </div>
                    </div>
                    <div class="row-form" >
                        <div class="span2"><?= lang('fax') ?></div>
                        <div class="span10"><?= form_input('fax', set_value('fax')) ?></div>
                    </div>
                    <div class="row-form" >
                        <div class="span2"><?= lang('email') ?><font style='color:red' >*</font></div>
                        <div class="span10" ><?= form_input('email', set_value('email'), "class='validate[required,custom[email]]'  ") ?>
                           <span class='bottom' style='color:red'>
                                   <?=form_error('email')?>
                           </span>
                        </div> 
                    </div>        
                        
                        
                        
                        
                    <div class="row-form" >
                        <div class="span2"><?= lang('contract_username') ?><font style='color:red' >*</font></div>
                        <div class="span10"><?= form_input('contract_username', set_value('contract_username'),"class='validate[required,custom[UserName]]'") ?>
                            <span class='bottom' style='color:red'>
									<!--  <font style="color:gray" > <?=lang('contract_unique_username')?></font>-->
                                  <?=form_error('contract_username')?>
                           </span>
                        </div> 
                    </div>
                    <div class="row-form" >
                        <div class="span2"><?= lang('contract_password') ?><font style='color:red' >*</font></div>
                        <div class="span10"><?= form_password('contract_password', set_value('contract_password'),"class='validate[required]' id='contract_password' ") ?>
                             <span class='bottom' style='color:red'>
                                  <?=form_error('contract_password')?>
                           </span>
                        </div>  
                    </div>     
                        
                        
                        
                                               
            </div>
            <div class="row-fluid">
            <!-- 
                <div class="span8 remember">
                    <input type="checkbox"/> I agree with terms...
                </div>
             -->    
                <div class="span4 TAR">
                    <button class="btn btn-block btn-primary" id="smt_registeration"><?=lang("registration")?></button>
                </div>
            </div>
            <div class="dr"><span></span></div>
            <div class="row-fluid">
                <div class="span4">                    
                    <button class="btn btn-block" onClick="loginBlock('#login');"><?=lang("login_back")?></button>
                </div>                
            </div>             
        </div>
    </div>    
    <?=form_close()?>
    
    
    
    <div class="login" id="forgot">
        <div class="wrap">
              <? if (validation_errors()): ?>
                 <div>
             <?php echo validation_errors(); ?>
                </div>
         <? endif ?>
            <h1><?=lang("login_forget_password")?></h1>
            <?=form_open("ea/login/forgot_password","id='validate'")?>
            <div class="row-fluid">
                <p><?=lang("recover_password")?></p>
                <div class="input-prepend">
                    <span class="add-on"><i class="icon-envelope"></i></span>
                    <input type="text" name="email" placeholder="<?=  lang('login_placeholder_email')?>" value="<?=set_value("email")?>" class="validate[required]"/>
                </div>                                                           
                <div class="dr"><span></span></div>                               
            </div>                   
            <div class="row-fluid">
                <div class="span4">                    
                    <input type="button" value="<?=lang("login_back")?>" class="btn btn-block" onClick="loginBlock('#login')"/>
                </div>                                
                <div class="span4"></div>
                <div class="span4 TAR">
                    <input type="submit" value="<?=lang("login_recover")?>" class="btn btn-block btn-primary"  />
                </div>
            </div>
            <?=form_close()?>
        </div>
    </div>
<script>
   <?if(isset($email_error)):?>
        document.getElementById("login").style.cssText="left: 70%; opacity: 0; display: none";
        document.getElementById("forgot").style.cssText="opacity: 1; display: block; left: 50%";
       <?else:?>
           document.getElementById("forgot").style.cssText="left: 70%; opacity: 0; display: none";
           document.getElementById("login").style.cssText="opacity: 1; display: block; left: 50%";
   <?endif;?> 
</script>


<script type="text/javascript">
	
    $('#username').change(function()
     {
        var username=$('#username').val();
            
        var dataString = 'username='+ username;
            
            $.ajax
            ({
            type: 'POST',
            url: '<?php echo base_url().'ea/login/check_username_availability'; ?>',
            data: dataString,
            cache: false,
            success: function(html)
            {
                //alert(html);
            $("#dv_username_availability").html(html);
            }
            });
            
     });

    $('#smt_registeration').click(function()
    {
        
        if($('#hdn_username_not_available_error').val()==1) {
        	alert('');
            return false;
        }        
                
    });
    
</script>