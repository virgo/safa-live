<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); ?>
<link href="<?= CSS ?>/stylesheets.css" rel="stylesheet" type="text/css" />
<? if (lang('global_lang') == 'ar'): ?>
    <link href="<?= CSS ?>/stylesheet-ar.css" rel="stylesheet" type="text/css" />
<? else: ?><? endif ?>
<script type="text/javascript" src='<?= JS ?>/plugins/jquery/jquery-1.9.1.min.js'></script>
<script type="text/javascript" src='<?= JS ?>/plugins/jquery/jquery-ui-1.10.1.custom.min.js'></script>
<script type="text/javascript" src='<?= JS ?>/plugins/bootstrap/bootstrap.min.js'></script>
<link href="<?= CSS_JS ?>/dateandtime/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src='<?= CSS_JS ?>/dateandtime/bootstrap-datetimepicker.min.js'></script>

<script type='text/javascript' src="<?= NEW_CSS_JS . '_' . lang('DIR') ?>/fancybox/jquery.fancybox.pack.js"></script>
<link rel="stylesheet" href="<?= NEW_CSS_JS . '_' . lang('DIR') ?>/fancybox/jquery.fancybox.css" type="text/css" media="screen" />        

<style>

body{
height: auto!important;
overflow: auto !important;
background:none transparent !important;
overflow: hidden;
min-height: 1%;
width: 96%;
}

.row-fluid{
padding-top: 5px !important;
padding-bottom: 5px !important;
}
</style>




        <style>
            .updated_msg{
                display:none;
                background-color:#ccee97;
                font-weight: bold;
                text-align: center;
                border:3px solid #cccdc9; 
                padding:20px;  
                margin-bottom:10px;
                border-radius:15px;
                margin:10px; 
            }
        </style>

         

    

  
        <style>
            .updated_msg{
                display:none;
                background-color:#ccee97;
                font-weight: bold;
                text-align: center;
                border:3px solid #cccdc9; 
                padding:20px;  
                margin-bottom:10px;
                border-radius:15px;
                margin:10px; 
            }
        </style>
        
    <div class="widget">
        
        
        <div class="modal-header">
        <div class="widget-header-icon Fright"> </div>
       
        <div class="widget-header-title Fright">
    	 <?= lang('group_passports_title_2') ?> 
        </div> 
        
        <!-- 
        <a title='<?= lang('choose_cols') ?>'href="<?= site_url('ea/group_passports/columns') ?>" class="btn Fleft fancybox fancybox.iframe">
           <?= lang('choose_cols') ?>
        </a> 
        -->
             
    	</div>
        
        
        <?= form_open("ea/group_passports/add_umrah_passports", " id= 'form_mutamers' name='form_mutamers'") ?>
        
        
        <!-- By Gouda -->
        <input type="hidden" name="copy_cut_val" value="cut" id='copy_cut_val' class="btn btn-primary">
        <input type="hidden" name="confirm_val" value="create" id='confirm_val' class="btn btn-primary">
        
        <div class="modal-body" style="padding-bottom: 75px;">
            <table cellpadding="0" class="fsTable" cellspacing="0" width="100%" id="tbl_mutamers">
                <thead>
                    <tr>
<!--                    <th>-->
<!--                    <input type="checkbox" class="checkall" id="checkall" onchange='checkUncheck()'/> -->
<!--                    </th>-->
                        
                        <? if (in_array('display_order', $cols)): ?>
                            <th><?= lang('group_passports_display_order') ?></th>
                        <? endif; ?>
                        <? if (in_array('group_id', $cols)): ?>
                            <th><?= lang('group_passports_group_id') ?></th>
                        <? endif; ?>
                        <? if (in_array('no', $cols)): ?>
                            <th><?= lang('group_passports_no') ?></th>
                        <? endif; ?> 
                        <? if (in_array('passport_no', $cols)): ?>
                            <th><?= lang('group_passports_passport_no') ?></th>
                        <? endif; ?>
                        <? if (in_array('passport_type_id', $cols)): ?>
                            <th><?= lang('group_passports_passport_type_id') ?></th>
                        <? endif; ?>
                        <? if (in_array('name_ar', $cols)): ?>
                            <th><?= lang('group_passports_name_ar') ?></th>
                        <? endif; ?>
                        <? if (in_array('name_la', $cols)): ?>
                            <th><?= lang('group_passports_name_la') ?></th>
                        <? endif; ?>
                        
                        <? if (in_array('erp_country_id', $cols)): ?>
                            <th><?= lang('group_passports_erp_country_id') ?></th>
                        <? endif; ?>
                        <? if (in_array('nationality_id', $cols)): ?>
                            <th><?= lang('group_passports_nationality_id') ?></th>
                        <? endif; ?>
                        <? if (in_array('date_of_birth', $cols)): ?>
                            <th><?= lang('group_passports_date_of_birth') ?></th>
                        <? endif; ?>
                        <? if (in_array('age', $cols)): ?>
                            <th><?= lang('group_passports_age') ?></th>
                        <? endif; ?>
                        <? if (in_array('passport_issue_date', $cols)): ?>
                            <th><?= lang('group_passports_passport_issue_date') ?></th>
                        <? endif; ?>
                        <? if (in_array('passport_expiry_date', $cols)): ?>
                            <th><?= lang('group_passports_passport_expiry_date') ?></th>
                        <? endif; ?>
                        <? if (in_array('passport_issuing_country_id', $cols)): ?>
                            <th><?= lang('group_passports_passport_issuing_country_id') ?></th>
                        <? endif; ?>
                        <? if (in_array('passport_issuing_city', $cols)): ?>
                            <th><?= lang('group_passports_passport_issuing_city') ?></th>
                        <? endif; ?>
                        <? if (in_array('passport_dpn_count', $cols)): ?>
                            <th><?= lang('group_passports_passport_dpn_count') ?></th>
                        <? endif; ?>
                        <? if (in_array('relative_relation_id', $cols)): ?>
                            <th><?= lang('group_passports_relative_relation_id') ?></th>
                        <? endif; ?>
                        <? if (in_array('occupation', $cols)): ?>
                            <th><?= lang('group_passports_occupation') ?></th>
                        <? endif; ?>
                        <? if (in_array('city', $cols)): ?>
                            <th><?= lang('group_passports_city') ?></th>
                        <? endif; ?>
                        <? if (in_array('marital_status_id', $cols)): ?>
                            <th><?= lang('group_passports_marital_status_id') ?></th>
                        <? endif; ?>
                        <? if (in_array('relative_no', $cols)): ?>
                            <th><?= lang('group_passports_relative_no') ?></th>
                        <? endif; ?>
                        <? if (in_array('dpn_serial_no', $cols)): ?>
                            <th><?= lang('group_passports_dpn_serial_no') ?></th>
                        <? endif; ?>
                        <? if (in_array('educational_level_id', $cols)): ?>
                            <th><?= lang('group_passports_educational_level_id') ?></th>
                        <? endif; ?>
                        
                        
                        <? if (in_array('birth_country_id', $cols)): ?>
                            <th><?= lang('group_passports_birth_country_id') ?></th>
                        <? endif; ?>
                        <? if (in_array('birth_city', $cols)): ?>
                            <th><?= lang('group_passports_birth_city') ?></th>
                        <? endif; ?>
                        
                        <? if (in_array('gender_id', $cols)): ?>
                            <th><?= lang('group_passports_gender_id') ?></th>
                        <? endif; ?>
                        <? if (in_array('relative_gender_id', $cols)): ?>
                            <th><?= lang('group_passports_relative_gender_id') ?></th>
                        <? endif; ?>
                        
                        
                    </tr>
                </thead>
                <tbody class="sortable">
                    <? if (isset($items)): ?>
                        <? foreach ($items as $item): ?>
                            <tr  id="row_<?php echo $item->safa_group_passport_id; ?>" group_id="<?= $item->safa_group_id; ?>">
<!--                        	<td>-->
<!--                        	<input type="checkbox" name="mutamers[]" id="mutamers[]"  value="<?= $item->safa_group_passport_id ?>" title="<?= $item->first_name_ar ?> <?= $item->second_name_ar ?> <?= $item->third_name_ar ?>"/>-->
<!--                        	-->
<!--                        	-->
<!--                        	<input type="hidden" name="passport_no_<?php echo $item->safa_group_passport_id; ?>" id="passport_no_<?php echo $item->safa_group_passport_id; ?>" value="<?= $item->passport_no ?>"></input>-->
<!--                            <input type="hidden" name="group_id_<?php echo $item->safa_group_passport_id; ?>" id="group_id_<?php echo $item->safa_group_passport_id; ?>" value="<?= $item->safa_groups_name ?>"></input> -->
<!--                            <input type="hidden" name="no_<?php echo $item->safa_group_passport_id; ?>" id="no_<?php echo $item->safa_group_passport_id; ?>" value="<?= $item->no ?>"></input> -->
<!--                            <input type="hidden" name="name_ar_<?php echo $item->safa_group_passport_id; ?>" id="name_ar_<?php echo $item->safa_group_passport_id; ?>" value="<?= $item->first_name_ar ?> <?= $item->second_name_ar ?> <?= $item->third_name_ar ?>"></input> -->
<!--                                     -->
<!--                        	</td>-->
                                   
                           
                                       
                                          
                        	<? if (in_array('display_order', $cols)): ?>
                                    <td><?= $item->display_order ?></td>
                                <? endif; ?>
                                <? if (in_array('group_id', $cols)): ?>
                                    <td><?= $item->safa_groups_name ?></td>
                                <? endif; ?>
                                 <? if (in_array('no', $cols)): ?>
                                    <td><?= $item->no ?></td>
                                <? endif; ?> 
                                <? if (in_array('passport_no', $cols)): ?>
                                    <td>
                                    
                                    <?= $item->passport_no ?>
                                    </td>
                                <? endif; ?>
                                <? if (in_array('passport_type_id', $cols)): ?>
                                    <td><?= $item->erp_passporttypes_name ?></td>
                                <? endif; ?>
                                <? if (in_array('name_ar', $cols)): ?>
                                    <td> 
                                       
                                    
                                        <?= $item->first_name_ar ?> 
                                        <?= $item->second_name_ar ?> 
                                        <?= $item->third_name_ar ?>
                                    </td>
                                <? endif; ?>  
                                <? if (in_array('name_la', $cols)): ?>
                                    <td> 
                                        <?= $item->first_name_la ?> 
                                        <?= $item->second_name_la ?> 
                                        <?= $item->third_name_la ?>
                                    </td>
                                <? endif; ?>
                                <? if (in_array('erp_country_id', $cols)): ?>
                                    <td><?= $item->erp_countries_name ?></td>
                                <? endif; ?>
                                <? if (in_array('nationality_id', $cols)): ?>
                                    <td><?= $item->erp_nationalities_name ?></td>
                                <? endif; ?>
                                <? if (in_array('date_of_birth', $cols)): ?>
                                    <td><?= $item->date_of_birth ?></td>
                                <? endif; ?>
                                 <? if (in_array('age', $cols)): ?>
                                    <td><?= $item->age ?></td>
                                <? endif; ?>
                                
                                <? if (in_array('passport_issue_date', $cols)): ?>
                                    <td><?= $item->passport_issue_date ?></td>
                                <? endif; ?>
                                <? if (in_array('passport_expiry_date', $cols)): ?>
                                    <td><?= $item->passport_expiry_date ?></td>
                                <? endif; ?>
                                
                                <? if (in_array('passport_issuing_country_id', $cols)): ?>
                                    <td><?= $item->issuing_country_name ?></td>
                                <? endif; ?>
                                <? if (in_array('passport_issuing_city', $cols)): ?>
                                    <td><?= $item->passport_issuing_city ?></td>
                                <? endif; ?>
                                
                                <? if (in_array('passport_dpn_count', $cols)): ?>
                                    <td><?= $item->passport_dpn_count ?></td>
                                <? endif; ?>
                               
                                <? if (in_array('relative_relation_id', $cols)): ?>
                                    <td><?= $item->erp_relations_name ?></td>
                                <? endif; ?>
                               
                                <? if (in_array('occupation', $cols)): ?>
                                    <td><?= $item->occupation ?></td>
                                <? endif; ?>
                                <? if (in_array('city', $cols)): ?>
                                    <td><?= $item->city ?></td>
                                <? endif; ?>
                                <? if (in_array('marital_status_id', $cols)): ?>
                                    <td><?= $item->erp_maritalstatus_name ?></td>
                                <? endif; ?>
                                <? if (in_array('relative_no', $cols)): ?>
                                    <td><?php 
                                    
                                    if($item->relative_no!=0 &&  !empty($item->relative_no)) {
                                    	$this->safa_group_passports_model->safa_group_id= $item->safa_group_id;
                                    	//$this->safa_group_passports_model->no=$item->relative_no;
                                    	$this->safa_group_passports_model->safa_group_passport_id=$item->relative_no;
                                    	
                                    	$relative_row=$this->safa_group_passports_model->get();
                                    	
                                    	if($relative_row) {
                                    		$first_name_ar=$relative_row->first_name_ar;
                                    		$second_name_ar=$relative_row->second_name_ar;
                                    		$third_name_ar=$relative_row->third_name_ar;
                                    		$fourth_name_ar=$relative_row->fourth_name_ar;
                                    		
                                    		echo $first_name_ar.' '.$second_name_ar.' '.$third_name_ar;
                                    	}
                                    }
                                    
                                    ?></td>
                                <? endif; ?>
                                 <? if (in_array('dpn_serial_no', $cols)): ?>
                                    <td><?= $item->dpn_serial_no ?></td>
                                <? endif; ?>
                               <? if (in_array('educational_level_id', $cols)): ?>
                                    <td><?= $item->erp_educationlevels_name ?></td>
                                <? endif; ?>
                                
                               
                               <? if (in_array('birth_country_id', $cols)): ?>
                                    <td><?= $item->birth_country_name ?></td>
                                <? endif; ?>
                                <? if (in_array('birth_city', $cols)): ?>
                                    <td><?= $item->birth_city ?></td>
                                <? endif; ?>
                                
                                <? if (in_array('gender_id', $cols)): ?>
                                    <td><?= $item->gender_id ?></td>
                                <? endif; ?>
                                <? if (in_array('relative_gender_id', $cols)): ?>
                                    <td><?= $item->relative_gender_id ?></td>
                                <? endif; ?>
                                
                                
                            </tr>
                        <? endforeach; ?>
                    <? endif; ?>
                </tbody>
            </table>
        </div>
       


		
        <?= form_close() ?> 
        <div class="modal-footer TAC">
        <input  type ="button" value="<?= lang('global_submit') ?>" class="btn btn-primary " onclick="selectPassports();" >
        </div>
    </div>
    
<div class="row-fluid">
    <? //= $pagination   ?>
    <div id="fModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h3 id="myModalLabel"><?= lang('global_system_message') ?></h3>
        </div>        
        <div class="row-fluid">
            <div id="msg" class="row-form">
                <?= lang('global_are_you_sure_you_want_to_delete') ?>
            </div>
        </div>                   
        <div class="modal-footer">
            <button class="btn btn-primary" data-dismiss="modal" aria-hidden="true"id="yes"><?= lang('global_yes') ?></button>
            <button class="btn btn-primary" data-dismiss="modal" aria-hidden="true"id="no"><?= lang('global_no') ?></button>       
        </div>
    </div>
    <div id="f2Modal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h3 id="myModalLabel"><?= lang('global_system_message') ?></h3>
        </div>        
        <div class="row-fluid">
            <div id='msg_body'class="row-form">

            </div>
        </div>                   
        <div id='yes_no'class="modal-footer">
            <button class="btn btn-primary" data-dismiss="modal" aria-hidden="true" id="as"><?= lang('global_yes') ?></button>
            <button class="btn btn-primary" data-dismiss="modal" aria-hidden="true" id="2"><?= lang('global_no') ?></button>       
        </div>
        <div id='sub' class="modal-footer">
            <button class='btn btn-primary' data-dismiss='modal' aria-hidden='true'><?= lang('global_submit') ?></button>
        </div>
    </div>
</div>    

<script type="text/javascript">
function selectPassports()
{
	var safa_trip_traveller_ids='';
	var safa_trip_traveller_ids_arr=new Array();
	//By Gouda
	if(parent.$("#safa_trip_traveller_ids").val()!=null) {
		safa_trip_traveller_ids_arr=parent.$("#safa_trip_traveller_ids").val();
	}
	
	var safa_trip_traveller_names='';

	for (var i = 0; i < document.form_mutamers.elements.length; i++ ) 
    {
        if (document.form_mutamers.elements[i].type == 'checkbox' && document.form_mutamers.elements[i].id =='mutamers[]') {
		    if (document.form_mutamers.elements[i].checked){
		    	safa_trip_traveller_ids = safa_trip_traveller_ids + document.form_mutamers.elements[i].value + ","
		    	safa_trip_traveller_names = safa_trip_traveller_names + document.form_mutamers.elements[i].title + " \n "

		    	safa_trip_traveller_ids_arr[i]=document.form_mutamers.elements[i].value;


		    	//For Table
		    	//---------------------------------------------------------------------------
		    	var safa_group_passport_id= document.form_mutamers.elements[i].value;
		    	var group_id_value= document.getElementById('group_id_'+safa_group_passport_id).value;
		    	var no_value= document.getElementById('no_'+safa_group_passport_id).value;
		    	var passport_no_value= document.getElementById('passport_no_'+safa_group_passport_id).value;
		    	var name_ar_value= document.getElementById('name_ar_'+safa_group_passport_id).value;

		    	
		        var new_tr_trip_passports = $(document.createElement('tr'));
		        new_tr_trip_passports.html(

		        		'<td>'+group_id_value+'</td>' +
		        		'<td>'+no_value+'</td>' +
		                '<td>'+passport_no_value+'</td>' +
		                '<td>'+name_ar_value+'</td>' +
		                '<td class="TAC" ><a href="<?= site_url("ea/group_passports/show_data") ?>/'+safa_group_passport_id+'" class="fancybox fancybox.iframe" ><span class="icon-user"></span> </a>'+
		                '<a href="javascript:void(0)" class="" id="hrf_remove_traveller" name="' + safa_group_passport_id + '"><span class="icon-trash"></span> </a></td>' 
		                );


		        var safa_trip_traveller_ids_selected_values = parent.$('#safa_trip_traveller_ids').val();
		        var are_safa_trip_traveller_id_selected=false;

		        if(safa_trip_traveller_ids_selected_values!=null) {
			        for (var j = 0; j < safa_trip_traveller_ids_selected_values.length; j++ ) { 
		                if(safa_trip_traveller_ids_selected_values[j]==safa_group_passport_id) {
		                	are_safa_trip_traveller_id_selected=true;
		                } 
			        }
			        if(are_safa_trip_traveller_id_selected==false) {
			        	new_tr_trip_passports.appendTo(parent.$("#tbl_trip_traveller"));
			        }
		        } else {
		        	new_tr_trip_passports.appendTo(parent.$("#tbl_trip_traveller"));
			    }
		    	//----------------------------------------------------------------------------
		    		    	
		   }
        }
	}
    
	//parent.safa_trip_traveller_ids.value=safa_trip_traveller_ids;
	//parent.safa_trip_traveller_names.value=safa_trip_traveller_names;
	//alert(safa_trip_traveller_ids_arr);
	parent.$("#safa_trip_traveller_ids").val(safa_trip_traveller_ids_arr);
	parent.$("#safa_trip_traveller_ids").trigger("chosen:updated");




	
	
	parent.$.fancybox.close(); 
	parent.Shadowbox.close();
}
</script>




    <script type="text/javascript">
    $('#inline').click(function(){
    	  $.fancybox.close();
    	});
    
                $(document).ready(function() {
                    delete_item();
                    $('.paginate_button').click(function() {
                        delete_item();
                    });
                    function delete_item() {
                        $('.delete_item').click(function() {
                            var name = $(this).attr('name');
                            var safa_group_passport_id = $(this).attr('id');
<? if (lang('global_lang') == 'ar'): ?>
                                $('#msg').text("<?= lang('global_are_you_sure_you_want_to_delete') ?>" + " " + name)
<? else: ?>
                                $('#msg').text(name + " " + "<?= lang('global_are_you_sure_you_want_to_delete') ?>")
<? endif; ?>
                            $('#yes').click(function() {
                                var answer = $(this).attr('id');
                                if (answer === 'yes') {
                                    $.ajax({
                                        type: "POST",
                                        dataType: 'json',
                                        url: "<?php echo site_url('ea/group_passports/delete'); ?>",
                                        data: {'safa_group_passport_id': safa_group_passport_id},
                                        success: function(msg) {
                                            if (msg.response == true) {
                                                var del = safa_group_passport_id;
                                                $("#row_" + del).remove();
<? if (lang('global_lang') == 'ar'): ?>
                                                    if (msg.passno)
                                                        $('.updated_msg').text(msg.msg + " " + name + " " + msg.passno);
                                                    else
                                                        $('.updated_msg').text(msg.msg + " " + name);
                                                    $('.updated_msg').append('<br>');
                                                    $('.updated_msg').append('<input  type ="button" value="<?= lang('global_submit') ?>"class="btn btn-primary" id="ok" >');
<? else: ?>
                                                    if (msg.passno)
                                                        $('.updated_msg').text(msg.msg + " " + name + " " + msg.passno);
                                                    else
                                                        $('.updated_msg').text(msg.msg + " " + name);
                                                    $('.updated_msg').append('<br>');
                                                    $('.updated_msg').append('<input  type ="button" value="<?= lang('global_submit') ?>"class="btn btn-primary" id="ok" >');
<? endif; ?>
                                                $('.updated_msg').show();
                                                $('#ok').click(function() {
                                                    $('.updated_msg').hide();
                                                });
                                            } else if (msg.response == false) {
                                                $('.updated_msg').text(msg.msg);
                                                //                                                alert(msg.msg);
                                                $('.updated_msg').append('<br>');
                                                $('.updated_msg').append('<input  type ="button" value="<?= lang('global_submit') ?>"class="btn btn-primary" id="ok" >');
                                                $('.updated_msg').show();
                                                $('#ok').click(function() {
                                                    $('.updated_msg').hide();
                                                });
                                            }
                                        }
                                    });
                                } else {
                                    return FALSE;
                                }
                            });
                        });
                    }
                });
    </script>
    <script type="text/javascript">
        $(function() {
            $('#datetimepicker1').datetimepicker({
                language: 'pt-BR',
                pickTime: false
            });
        });
    </script>
    <script type="text/javascript">
        $(function() {
            $('#datetimepicker2').datetimepicker({
                language: 'pt-BR',
                pickTime: false
            });
        });
    </script>
    <script>
        $(document).ready(function() {
            $("#search").keypress(function(e) {
                if (e.which == 13)
                {
                    e.preventDefault();
                }
                var searched = $('#search').val();
                $.getJSON("<?php echo site_url('ea/group_passports/get_passport_no'); ?>/" + searched, function(result) {
                    var elements = [];
                    $.each(result, function(i, val) {
                        elements.push(val.passport_no)
                    });

                    $('#search').autocomplete({
                        source: elements
                    });
                    $('.ui-helper-hidden-accessible').hide();
                });
            });

            $("#mutamer_name").keypress(function(e) {
                if (e.which == 13)
                {
                    e.preventDefault();
                }
                var searched = $('#mutamer_name').val();
                $.ajax({
                    type: "POST",
                    dataType: 'json',
                    url: "<?php echo site_url('ea/group_passports/mutamer_name'); ?>",
                    data: {'searched': "" + searched + ""},
                    success: function(data) {
                        var elements = [];
                        $.each(data, function(i, val) {
                            if (val.first_name_ar)
                                elements.push(val.first_name_ar + " " + val.second_name_ar + " " + val.third_name_ar)
                            else
                                elements.push(val.first_name_la + " " + val.second_name_la + " " + val.third_name_la)
                        });
                        $('#mutamer_name').autocomplete({
                            source: elements
                        });
                        $('.ui-helper-hidden-accessible').hide();

                    }
                });
            });
        });

    </script>
    <script>
        $(document).ready(function() {
            $('.fancybox').fancybox({
                afterClose: function() {
                    location.reload();
                }
            });
        });
    </script>
    <script type='text/javascript' src='<?= JS ?>/custom/table_operation.js'></script>
    <script>

        check_list_delete_submit("mutamers", "fsTable", "form_mutamers", "<?= lang('create_umrah_passports_msg') ?>");

        //By Gouda.
        function checkUncheck() 
        {		
            for (var i = 0; i < document.form_mutamers.elements.length; i++ ) 
            {
                if (document.form_mutamers.elements[i].type == 'checkbox' && document.form_mutamers.elements[i].id !='checkall') 
        	    {		       		    
                	document.form_mutamers.elements[i].checked = document.form_mutamers.elements['checkall'].checked; 
                }

            }
        }
    </script>

    <script type="text/javascript">
        $(document).ready(function() {
            $('#addumrah_group').click(function() {
                var flag = 0;
                $(".fsTable").find("[type='checkbox']").each(function(index, object) {
                    if ($(object).attr("class") != "checkall" && $(object).parent().attr("class") == "checked") {
                        flag = 1;
                        return false;
                    }
                });

              //By Gouda
				for (var i = 0; i < document.form_mutamers.elements.length; i++ ) 
	            {
	                if (document.form_mutamers.elements[i].checked ) {
	                	flag = 1;
	                } 
	            }
	            
                if (flag == 1) {
                    $("#form_mutamers").attr("action", "<?= site_url('ea/group_passports/add_umrah_passports') ?>");
                    $('#msg_body').text("<?= lang('group_passports_mutamers_confirm_add') ?>");
                    $("<br>").appendTo($("#msg_body"));
                    $('<?= lang('create_new_umrahgroup') ?>:<input type="radio"  name="confirm" checked value="create">').appendTo($("#msg_body"));

                    $("<span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>").appendTo($("#msg_body"));
                    $('<?= lang('move_to_umrahgroup') ?>:<input type="radio"  name="confirm" value="move">').appendTo($("#msg_body"));
                    $('#sub').hide();
                    $('#yes_no').show();
                    var confirm_val = null;
                    $("input[name='confirm']").click(function() {
                        confirm_val = $(this).filter(':checked').val();
                        $('input#confirm_val').val(confirm_val);
                    });
                    $('#as').click(function() {
                        document.getElementById('form_mutamers').submit();
                    });
                } else {
                    $('#msg_body').text("<?= lang('global_select_one_record_at_least') ?>");
                    $('#yes_no').hide();
                    $('#sub').show();
                }
                //      return false ;    
            });
        });
    </script>

    <script type="text/javascript">
        $(document).ready(function() {
            $('#delete_selected').click(function() {
                var flag = 0;
                $(".fsTable").find("[type='checkbox']").each(function(index, object) {
                    if ($(object).attr("class") != "checkall" && $(object).parent().attr("class") == "checked") {
                        flag = 1;
                        return false;
                    }
                });

				//By Gouda
				for (var i = 0; i < document.form_mutamers.elements.length; i++ ) 
	            {
	                if (document.form_mutamers.elements[i].checked ) {
	                	flag = 1;
	                } 
	            }
                
                if (flag == 1) {
                    $("#form_mutamers").attr("action", "<?= site_url('ea/group_passports/delete_all') ?>");
                    $('#msg_body').text("<?= lang('global_delete_selected_records') ?>");
                    $("<br>").appendTo($("#msg_body"));

                    $('#sub').hide();
                    $('#yes_no').show();
                    $('#as').click(function() {
                        document.getElementById('form_mutamers').submit();
                    });
                } else {
                    $('#msg_body').text("<?= lang('global_select_one_record_at_least') ?>");
                    $('#yes_no').hide();
                    $('#sub').show();
                }
                //      return false ;    
            });
        });
    </script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('#copy_cut_selected').click(function() {
                var flag = 0;
                $(".fsTable").find("[type='checkbox']").each(function(index, object) {
                    if ($(object).attr("class") != "checkall" && $(object).parent().attr("class") == "checked") {
                        flag = 1;
                        return false;
                    }
                });


              	//By Gouda
				for (var i = 0; i < document.form_mutamers.elements.length; i++ ) 
	            {
	                if (document.form_mutamers.elements[i].checked ) {
	                	flag = 1;
	                } 
	            }
                
                if (flag == 1) {
                    $("#form_mutamers").attr("action", "<?= site_url('ea/group_passports/copy_cut') ?>");
                    $('#msg_body').text("<?= lang('group_passports_mutamers_confirm_cut') ?>");
                    $("<br>").appendTo($("#msg_body"));
                    $('#sub').hide();
                    $('#yes_no').show();

                    $('#as').click(function() {
                        document.getElementById('form_mutamers').submit();
                    });
                } else {
                    $('#msg_body').text("<?= lang('global_select_one_record_at_least') ?>");
                    $('#yes_no').hide();
                    $('#sub').show();
                }
            });
        });
    </script>
   
    <script>
    	//By Gouda
        //$(document).ready(function() {
            $(".fsTable tbody tr").css("cursor", "move");
            // Return a helper with preserved width of cells
            var fixHelper = function(e, ui) {
                ui.children().each(function() {
                    $(this).width($(this).width());
                });
                return ui;
            };

            $('.sortable').sortable({
                update: function() {
                    var stringDiv = "";
                    var group_id = $(".fsTable tbody tr").attr("group_id");
                    $(".fsTable tbody").children().each(function(i) {
                        var tr = $(this);
                        stringDiv += " " + tr.attr("id") + '=' + i + '&';
                    });

                    $.ajax({
                        type: "POST",
                        url: "<?php echo site_url('ea/group_passports/sortable'); ?>/" + group_id,
                        data: stringDiv
                    });
                }
            });
            $(".sortable").disableSelection();
        //});




//By Gouda

 if ($(".fsTable").length > 0) {

	 	//By Gouda, TO remove the warning message.
	 	//$.fn.dataTableExt.sErrMode = 'throw';

        $(".fsTable").dataTable({

            bSort: true,
            bAutoWidth: true,
            "iDisplayLength":<?php 
            $ea_user_id = session('ea_user_id');
            if ($this->input->cookie('' . $ea_user_id . '_' . 'group_passports_rows_count_per_page' . '')) {
                echo $this->input->cookie('' . $ea_user_id . '_' . 'group_passports_rows_count_per_page' . '');
            }else {echo 10;}?>, 
            "aLengthMenu": [5, 10, 25, 50, 100, 200, 500], // can be removed for basic 10 items per page
            "bFilter": true,
            "sPaginationType": "full_numbers",
            "aoColumnDefs": [{"bSortable": false,
            "aTargets": [-1, 0]}]

            });
    }



</script>

<script>


$('[name="tbl_mutamers_length"]').change(function() {
	
	var rows_count_per_page=$(this).val();
    
	var dataString = 'rows_count_per_page='+ rows_count_per_page;
    
    $.ajax
    ({
    	type: 'POST',
    	url: '<?php echo base_url().'ea/group_passports/setRowsCountCookie'; ?>',
    	data: dataString,
    	cache: true,
    	success: function(html)
    	{
        	
    	}
    });
	
});

</script>
