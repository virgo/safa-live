<link rel="stylesheet" href="<?= NEW_CSS_JS . '_' . lang('DIR') ?>/jquery-ui.css" type="text/css" media="screen" />
<style>
    /*    .row-fluid{border-bottom: 1px solid #fff;padding: 8px 0 0;}*/
    label{margin: 5px!important;}
    .wizerd-div{padding-top: 0px; border-bottom: 1px solid rgb(255, 255, 255); margin: 12px 0px 50px;}
    
    .ui-tabs .ui-tabs-nav li {
        float: right;
    }
    ul.ui-tabs-nav {
        background-color: transparent;
        border: none;
    }
    
    
    /* By Gouda, To Update font family and color of this page, becouse jquery.ui.css overwrite it*/
    .ui-widget{
    font-family: "GE-SS-med";
	font-size:12px;
	}
	.ui-widget-content {
    color: #666666;	
	}
	
</style>


<!-- Added By Gouda, for autocomplete. It hide when I choose it, and these lines solve the problem -->
<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>


<div class="row-fluid">
    <div class="widget">
        <div class="path-container Fright">
            <div class="path-name Fright">
                <a href="<?php echo site_url() . 'ea/dashboard'; ?>"><?php echo lang('global_system_management') ?></a>
            </div>

            <div class="path-arrow Fright"></div>
            <div class="path-name Fright"> <a href="<?= site_url('ea/safa_groups/') ?>"> <?= lang('group_passports_title') ?></a></div>
            <div class="path-arrow Fright"></div>
            <div class="path-name Fright"> 
                <? if ($item->safa_group_id !== FALSE) { ?>
                    <?
                    $name = $this->safa_group_passports_model->get_group_name($item->safa_group_id);
                    ?>
                    <a href="<?= site_url('ea/group_passports/passports_index/' . $item->safa_group_id) ?>"> <?= lang('group_passports_group_mutamers_title') ?> <?= $name->name ?></a></div>
            <? } else { ?>
                <?= lang('group_passports_title_2') ?> 
            <? } ?>
            <div class="path-arrow Fright"></div>
            <div class="path-name Fright">  <?= lang('group_passports_edit_title') ?></div>
        </div>   
    </div>

    <div class="widget">
        <div class="widget-header">
            <div class="widget-header-icon Fright"> <span class="icos-pencil2"></span> </div>
            <div class="widget-header-title Fright">
                <?php echo lang('group_passports_edit_title') ?>
            </div>
        </div>

        <div class="widget-container slidingDiv" style="">
            <?= form_open() ?>
            <div id="tabs" class="" style="margin-top: 39px;">
                <ul  style="margin-top: -38px;">
                    <li><a href="#tabs-1"><?php echo lang('client_data'); ?></a></li>
                    <li><a href="#tabs-2"><?php echo lang('relatives'); ?></a></li>
                    <li><a href="#tabs-3"><?php echo lang('notes'); ?></a></li>
                </ul>
                <div id="tabs-1">
                    <fieldset>
                        <div class="wizerd-div">

                            <a ><?= lang('client_data') ?></a></div>
                        <div class="group1" style="float: none;overflow: hidden;">
                            <legend><?= lang('client_data') ?></legend>                      
                            <div class="row-fluid span6">
                                <div class="span12">
                                    <div class="span3"> <label><?= lang('group_passports_group_id') ?>:</label> </div>
                                    <div class="span9">
                                        <?= form_dropdown('safa_group_id', ddgen('safa_groups', array('safa_group_id', 'name'), false , array('name','')), set_value('safa_group_id', $item->safa_group_id), "  class='chosen select input-huge' ") ?>

                                        <?= form_error('safa_group_id', '<div class="bottom" style="color:red" >', '</div>'); ?>
                                    </div>
                                </div>
                            </div>                           
                            <div class="row-fluid span3">
                                <div class="span12">
                                    <div class="span6"><label> <?= lang('group_passports_no') ?>:</label> </div>
                                    <div class="span6">
                                        <?= form_input('no', set_value("no", $item->no), " ") ?>
                                        <?= form_error('no', '<div class="bottom" style="color:red" >', '</div>'); ?>
                                    </div>
                                </div>
                            </div>   
                            <div class="row-fluid span3">
                                <div class="span12">
                                    <div class="span6"><label><?= lang('group_passports_title_id') ?>:</label>  </div>
                                    <div class="span6">
                                        <?= form_dropdown('title_id', ddgen('erp_titles', array('erp_title_id', name())), set_value('title_id', $item->title_id), "  class='chosen input-huge' ") ?>

                                        <?= form_error('title_id', '<div class="bottom" style="color:red" >', '</div>'); ?>
                                    </div>
                                </div>

                            </div>
                        </div> 

                        <div class="Div-Sep">
                            <a></a>
                        </div>
                        <div class="group1" style="float: none;overflow: hidden;">

                            <div class="row-fluid span3">
                                <div class="span6"> <label><?= lang('group_passports_first_name_ar') ?>:</label> </div>
                                <div class="span6">
                                    <?= form_input('first_name_ar', set_value("first_name_ar", $item->first_name_ar), " class='autocomplete' id='first_name_ar' ") ?>
                                    <?= form_error('first_name_ar', '<div class="bottom" style="color:red" >', '</div>'); ?>
                                </div>
                            </div>
                            <div class="row-fluid span3">
                                <div class="span6"><label><?= lang('group_passports_second_name_ar') ?>:</label>  </div>
                                <div class="span6">
                                    <?= form_input('second_name_ar', set_value("second_name_ar", $item->second_name_ar), " class='autocomplete'  id='second_name_ar' ") ?>
                                    <?= form_error('second_name_ar', '<div class="bottom" style="color:red" >', '</div>'); ?>
                                </div>
                            </div>
                            <div class="row-fluid span3">
                                <div class="span6"> <label><?= lang('group_passports_third_name_ar') ?>:</label> </div>
                                <div class="span6">
                                    <?= form_input('third_name_ar', set_value("third_name_ar", $item->third_name_ar), " class='autocomplete'  id='third_name_ar' ") ?>
                                    <?= form_error('third_name_ar', '<div class="bottom" style="color:red" >', '</div>'); ?>
                                </div>
                            </div>
                            <div class="row-fluid span3">
                                <div class="span6"><label><?= lang('group_passports_fourth_name_ar') ?>:</label>  </div>
                                <div class="span6">
                                    <?= form_input('fourth_name_ar', set_value("fourth_name_ar", $item->fourth_name_ar), " class='autocomplete'  id='fourth_name_ar' ") ?>
                                    <?= form_error('fourth_name_ar', '<div class="bottom" style="color:red" >', '</div>'); ?>
                                </div>
                            </div>


                        </div> 

                        <div class="group1" style="float: none;overflow: hidden;">
                            <div class="row-fluid span3">
                                <div class="span6"> <label><?= lang('group_passports_first_name_la') ?>:</label> </div>
                                <div class="span6">
                                    <?= form_input('first_name_la', set_value("first_name_la", $item->first_name_la), " class='autocomplete'  id='first_name_la' ") ?>
                                    <?= form_error('first_name_la', '<div class="bottom" style="color:red" >', '</div>'); ?>
                                </div>
                            </div>
                            <div class="row-fluid span3">
                                <div class="span6"> <label><?= lang('group_passports_second_name_la') ?>:</label> </div>
                                <div class="span6">
                                    <?= form_input('second_name_la', set_value("second_name_la", $item->second_name_la), " class='autocomplete'  id='second_name_la' ") ?>
                                    <?= form_error('second_name_la', '<div class="bottom" style="color:red" >', '</div>'); ?>
                                </div>
                            </div>
                            <div class="row-fluid span3">
                                <div class="span6"> <label><?= lang('group_passports_third_name_la') ?>:</label> </div>
                                <div class="span6">
                                    <?= form_input('third_name_la', set_value("third_name_la", $item->third_name_la), " class='autocomplete'   id='third_name_la' ") ?>
                                    <?= form_error('third_name_la', '<div class="bottom" style="color:red" >', '</div>'); ?>
                                </div>
                            </div>
                            <div class="row-fluid span3">
                                <div class="span6"> <label><?= lang('group_passports_fourth_name_la') ?>:</label> </div>
                                <div class="span6">
                                    <?= form_input('fourth_name_la', set_value("fourth_name_la", $item->fourth_name_la), " class='autocomplete'   id='fourth_name_la' ") ?>
                                    <?= form_error('fourth_name_la', '<div class="bottom" style="color:red" >', '</div>'); ?>
                                </div>
                            </div>


                        </div>        

                    </fieldset>

                    <fieldset>  
                        <div class="wizerd-div">
                            <a ><?= lang('passports_details') ?></a></div>
                        <legend><?= lang('passports_details') ?></legend>

                        <div class="group1" style="float: none;overflow: hidden;">

                            <div class="row-fluid span6">
                                <div class="span3"> <label> <?= lang('group_passports_nationality_id') ?>:</label></div>
                                <div class="span9">

                                    <?= form_dropdown('nationality_id', ddgen('erp_nationalities', array('erp_nationality_id', name()), false , array(name(),'')), set_value('nationality_id', $item->nationality_id), "  class='chosen select input-huge' ") ?>
                                    <?= form_error('nationality_id', '<div class="bottom" style="color:red" >', '</div>'); ?>
                                </div>
                            </div>
                            
                            <!-- 
                            <div class="row-fluid span3">
                                <div class="span6"> <label> <?= lang('group_passports_previous_nationality_id') ?>:</label></div>
                                <div class="span6">
                                    <?= form_dropdown('previous_nationality_id', ddgen('erp_nationalities', array('erp_nationality_id', name())), set_value('previous_nationality_id', $item->previous_nationality_id), "  class='chosen input-huge' ") ?>

                                    <?= form_error('previous_nationality_id', '<div class="bottom" style="color:red" >', '</div>'); ?>
                                </div>
                            </div>
                             -->
                             
                            <div class="row-fluid span3">
                                <div class="span6"> <label> <?= lang('group_passports_passport_no') ?>:</label></div>
                                <div class="span6">
                                    <?= form_input('passport_no', set_value("passport_no", $item->passport_no), " ") ?>
                                    <?= form_error('passport_no', '<div class="bottom" style="color:red" >', '</div>'); ?>
                                </div>
                            </div>

                            <div class="row-fluid span3">
                                <div class="span6"> <label> <?= lang('group_passports_passport_type_id') ?>:</label></div>
                                <div class="span6">
                                    <?= form_dropdown('passport_type_id', ddgen('erp_passporttypes', array('erp_passporttype_id', name())), set_value('passport_type_id', $item->passport_type_id), "  class='chosen input-huge' ") ?>

                                    <?= form_error('passport_type_id', '<div class="bottom" style="color:red" >', '</div>'); ?>
                                </div>
                            </div>

                        </div>

                        <div class="group1" style="overflow: hidden; float: none;">        

                            <div class="span3">
                                <div class="span6">  <label><?= lang('group_passports_passport_issue_date') ?>:</label></div>
                                <div class="span6">
                                    <?= form_input('passport_issue_date', set_value("passport_issue_date", $item->passport_issue_date), " class='passport_issue_date' ") ?>
                                    <?= form_error('passport_issue_date', '<div class="bottom" style="color:red" >', '</div>'); ?>
                                    <script>
                                        $('.passport_issue_date').datepicker({
                                            dateFormat: "yy-mm-dd",
                                            controlType: 'select',
                                            timeFormat: 'HH:mm'
                                        });
                                    </script>
                                </div>
                            </div>
                            <div class="span3">
                                <div class="span6"> <label><?= lang('group_passports_passport_expiry_date') ?>:</label> </div>
                                <div class="span6">
                                    <?= form_input('passport_expiry_date', set_value("passport_expiry_date", $item->passport_expiry_date), " class='passport_expiry_date'") ?>
                                    <?= form_error('passport_expiry_date', '<div class="bottom" style="color:red" >', '</div>'); ?>
                                    <script>
                                        $('.passport_expiry_date').datepicker({
                                            dateFormat: "yy-mm-dd",
                                            controlType: 'select',
                                            timeFormat: 'HH:mm'
                                        });
                                    </script>
                                </div>
                            </div>
                            <div class="span3">
                                <div class="span6"> <label><?= lang('group_passports_passport_issuing_city') ?>:</label> </div>
                                <div class="span6">
                                    <?= form_input('passport_issuing_city', set_value('passport_issuing_city', $item->passport_issuing_city), "   class='autocomplete'  id='passport_issuing_city'  ") ?>

                                    <?= form_error('passport_issuing_city', '<div class="bottom" style="color:red" >', '</div>'); ?>
                                </div>
                            </div>
                            <div class="span3">
                                <div class="span6"> <label> <?= lang('group_passports_passport_issuing_country_id') ?>:</label></div>
                                <div class="span6">
                                    <?= form_dropdown('passport_issuing_country_id', ddgen('erp_countries', array('erp_country_id', name()), false , array(name(),'')), set_value('passport_issuing_country_id', $item->passport_issuing_country_id), "  class='chosen select input-huge' ") ?>                       
                                    <?= form_error('passport_issuing_country_id', '<div class="bottom" style="color:red" >', '</div>'); ?>
                                </div>
                            </div>

                        </div>  
                        <div class="Div-Sep"><a></a></div>
                        <div class="group1" style="overflow: hidden; float: none;">

                            <!-- 
                            <div class="span3">
                                <div class="span6"> <label><?= lang('group_passports_passport_dpn_count') ?>:</label> </div>
                                <div class="span6">
                            <?= form_input('passport_dpn_count', set_value("passport_dpn_count", $item->passport_dpn_count), " ") ?>
                            <?= form_error('passport_dpn_count', '<div class="bottom" style="color:red" >', '</div>'); ?>
                                </div>
                            </div>
                            <div class="span3">
                                <div class="span6">  <label><?= lang('group_passports_dpn_serial_no') ?>:</label></div>
                                <div class="span6">
                            <?= form_input('dpn_serial_no', set_value("dpn_serial_no", $item->dpn_serial_no), " ") ?>
                            <?= form_error('dpn_serial_no', '<div class="bottom" style="color:red" >', '</div>'); ?>
                                </div>
                            </div>
                            -->

                            <div class="span6">
                                <div class="span3">  <label><?= lang('group_passports_relative_no') ?>:</label></div>
                                <div class="span9">
                                    <?php
                                    $this->countries_model->erp_country_id= $item->passport_issuing_country_id;
                                    $country_row=$this->countries_model->get();
                                    $passport_fullname='full_name_la';
                                    if(count($country_row)>0) {
                                    	if($country_row->arab==1) {
                                    		$passport_fullname='full_name_ar';
                                    	}
                                    }
                                    
                                    $structure = array('safa_group_passport_id', $passport_fullname);
                                    $key = $structure['0'];
                                    if (isset($structure['1'])) {
                                        $value = $structure['1'];
                                    }
                                    $passports_arr = array();
                                    $passports_arr[''] = lang('global_select_from_menu');
                                    $this->safa_group_passports_model->safa_group_id = $item->safa_group_id;
                                    $this->safa_group_passports_model->safa_group_passport_id = false;
                                    //$this->safa_group_passports_model->limit=false;

                                    $this->safa_group_passports_model->order_by=array($passport_fullname, '');
                                    
                                    $passports = $this->safa_group_passports_model->get_for_table();
                                    foreach ($passports as $passport) {
                                        $passports_arr[$passport->$key] = $passport->$value;
                                    }
                                    ?>
                                    <?= form_dropdown('relative_no', $passports_arr, set_value('relative_no', $item->relative_no), "  class='chosen select input-huge' ") ?>                       

                                    <?= form_error('relative_no', '<div class="bottom" style="color:red" >', '</div>'); ?>
                                </div>
                            </div>

                            <div class="span3">
                                <div class="span6">  <label><?= lang('group_passports_relative_relation_id') ?>:</label></div>
                                <div class="span6">
                                    <?= form_dropdown('relative_relation_id', ddgen('erp_relations', array('erp_relation_id', name()), '', '', true), set_value('relative_relation_id', $item->relative_relation_id), "  class='chosen input-huge' ") ?>                       

                                    <?= form_error('relative_relation_id', '<div class="bottom" style="color:red" >', '</div>'); ?>
                                </div>
                            </div>
                            <div class="span3">
                                <div class="span6"> <label> <?= lang('group_passports_educational_level_id') ?>:</label></div>
                                <div class="span6">
                                    <?= form_dropdown('educational_level_id', ddgen('erp_educationlevels', array('erp_educationlevel_id', name())), set_value('educational_level_id', $item->educational_level_id), "  class='chosen input-huge' ") ?>                       

                                    <?= form_error('educational_level_id', '<div class="bottom" style="color:red" >', '</div>'); ?>
                                </div>
                            </div>
                            
                            
                            
                        </div>    



                        <div class="group1" style="overflow: hidden; float: none;">
                            
                            <div class="span6">
                                <div class="span3"> <label><?= lang('group_passports_occupation') ?>:</label> </div>
                                <div class="span9">
                                    <?= form_input('occupation', set_value("occupation", $item->occupation), "  class='autocomplete'  id='occupation'  ") ?>
                                    <?= form_error('occupation', '<div class="bottom" style="color:red" >', '</div>'); ?>
                                </div>
                            </div>
                            
                            <div class="span3">
                                <div class="span6">  <label><?= lang('group_passports_date_of_birth') ?>:</label></div>
                                <div class="span6">
                                    <?= form_input('date_of_birth', set_value("date_of_birth", $item->date_of_birth), " class='date_of_birth' ") ?>
                                    <?= form_error('date_of_birth', '<div class="bottom" style="color:red" >', '</div>'); ?>
                                    <script>
                                        $('.date_of_birth').datepicker({
                                            dateFormat: "yy-mm-dd",
                                            controlType: 'select',
                                            timeFormat: 'HH:mm'
                                        });
                                    </script>
                                </div>
                            </div>
                            <div class="span3">
                                <div class="span6"> <label><?= lang('group_passports_age') ?>:</label> </div>
                                <div class="span6">
                                    <?php
                                    $date_of_birth = strtotime($item->date_of_birth);
                                    $date_now = strtotime(date('Y-m-d', time()));
                                    $secs = $date_now - $date_of_birth; // == return sec in difference
                                    $days = $secs / 86400;
                                    $age = $days / 365;
//$age=round($age);
//$age=ceil($age);
                                    $age = floor($age);
                                    ?>

                                    <?= form_input('age', set_value("age", $age), " disabled='disabled' ") ?>
                                    <?= form_error('age', '<div class="bottom" style="color:red" >', '</div>'); ?>
                                </div>
                            </div>



                        </div>

                    </fieldset>
                    <fieldset>  
                        <div class="wizerd-div">
                            <a><?= lang('country_details') ?></a></div>
                        <legend><?= lang('country_details') ?></legend>

                        <div class="group1" style="overflow: hidden;float: none;">
                            
                            <div class="span6">
                                <div class="span3"> <label> <?= lang('group_passports_birth_country_id') ?>:</label></div>
                                <div class="span9">
                                    <?= form_dropdown('birth_country_id', ddgen('erp_countries', array('erp_country_id', name()), false , array(name(),'')), set_value('birth_country_id', $item->birth_country_id), "  class='chosen select input-huge' ") ?>                       

                                    <?= form_error('birth_country_id', '<div class="bottom" style="color:red" >', '</div>'); ?>
                                </div>
                            </div>
                            
                            <div class="span6">
                                <div class="span3">  <label><?= lang('group_passports_birth_city') ?>:</label></div>
                                <div class="span9">
                                    <?= form_input('birth_city', set_value("birth_city", $item->birth_city), "  class='autocomplete' id='birth_city'  ") ?>
                                    <?= form_error('birth_city', '<div class="bottom" style="color:red" >', '</div>'); ?>
                                </div>
                            </div>
                            


                        </div>
                        <div class="Div-Sep"><a></a></div>
                        <div class="group1" style="overflow: hidden;float: none;">

                            <!-- 
                            <div class="span3">
                                <div class="span6">  <label><?= lang('group_passports_relative_gender_id') ?>:</label></div>
                                <div class="span6">
                            <?= form_dropdown('relative_gender_id', ddgen('erp_gender', array('erp_gender_id', name())), set_value('relative_gender_id', $item->relative_gender_id), "  class='chosen input-huge' ") ?>                       
                                    
                            <?= form_error('relative_gender_id', '<div class="bottom" style="color:red" >', '</div>'); ?>
                                </div>
                            </div>
                            -->

							<div class="span6">
                                <div class="span3"> <label><?= lang('group_passports_erp_country_id') ?>:</label></div>
                                <div class="span9">
                                    <?= form_dropdown('erp_country_id', ddgen('erp_countries', array('erp_country_id', name()), false , array(name(),'')), set_value('erp_country_id', $item->erp_country_id), "  class='chosen select input-huge' ") ?>                       

                                    <?= form_error('erp_country_id', '<div class="bottom" style="color:red" >', '</div>'); ?>
                                </div>
                            </div>
                            <div class="span6">
                                <div class="span3">  <label><?= lang('group_passports_city') ?>:</label></div>
                                <div class="span9">
                                    <?= form_input('city', set_value("city", $item->city), "  class='autocomplete'  id='city'  ") ?>
                                    <?= form_error('city', '<div class="bottom" style="color:red" >', '</div>'); ?>
                                </div>
                            </div>
                            
                            
                             
                            
                        </div>
                        
                        
                        <div class="group1" style="overflow: hidden;float: none;">
                        
                        <div class="span6">
                                <div class="span3">  <label><?= lang('group_passports_address') ?>:</label></div>
                                <div class="span9">
                                    <?= form_input('address', set_value("address", $item->address), " ") ?>
                                    <?= form_error('address', '<div class="bottom" style="color:red" >', '</div>'); ?>
                                </div>
                            </div>
                            
                            
                        <div class="span3">
                                <div class="span6">  <label><?= lang('group_passports_gender_id') ?>:</label></div>
                                <div class="span6">
                                    <?= form_dropdown('gender_id', ddgen('erp_gender', array('erp_gender_id', name())), set_value('gender_id', $item->gender_id), "  class='chosen input-huge' ") ?>                       
                                    <?= form_error('gender_id', '<div class="bottom" style="color:red" >', '</div>'); ?>
                                </div>
                            </div>
                            <div class="span3">
                                <div class="span6"> <label> <?= lang('group_passports_marital_status_id') ?>:</label></div>
                                <div class="span6">
                                    <?= form_dropdown('marital_status_id', ddgen('erp_maritalstatus', array('erp_maritalstatus_id', name())), set_value('marital_status_id', $item->marital_status_id), "  class='chosen input-huge' ") ?>                       

                                    <?= form_error('marital_status_id', '<div class="bottom" style="color:red" >', '</div>'); ?>
                                </div>
                            </div>
                            
                           
                        </div>
                        
                        
                        
                    </fieldset>
                    
                    
                    
                </div>
                <div id="tabs-2">             
                    <fieldset>  
                        <div class="wizerd-div">
                            <a > <?php echo lang('relatives'); ?></a></div>
                        <legend></legend>
                        
                         
                        <div class="group1" style="overflow: hidden; float: none;">
                            <div class="span3">
                                <div class="span6"> <label><?= lang('group_passports_passport_dpn_count') ?>:</label> </div>
                                <div class="span6">
                                    <?= form_input('passport_dpn_count', set_value("passport_dpn_count", $item->passport_dpn_count), " ") ?>
                                    <?= form_error('passport_dpn_count', '<div class="bottom" style="color:red" >', '</div>'); ?>
                                </div>
                            </div>
                            
                            <!-- 
                            <div class="span3">
                                <div class="span6">  <label><?= lang('group_passports_dpn_serial_no') ?>:</label></div>
                                <div class="span6">
                                    <?= form_input('dpn_serial_no', set_value("dpn_serial_no", $item->dpn_serial_no), " ") ?>
                                    <?= form_error('dpn_serial_no', '<div class="bottom" style="color:red" >', '</div>'); ?>
                                </div>
                            </div>
                            <div class="span3">
                                <div class="span6">  <label><?= lang('group_passports_relative_relation_id') ?>:</label></div>
                                <div class="span6">
                                    <?= form_input('relative_relation_id', set_value("relative_relation_id", $item->relative_relation_id), " ") ?>
                                    <?= form_error('relative_relation_id', '<div class="bottom" style="color:red" >', '</div>'); ?>
                                </div>
                            </div>
                            <div class="span3">
                                <div class="span6"> <label> <?= lang('group_passports_educational_level_id') ?>:</label></div>
                                <div class="span6">
                                    <?= form_input('educational_level_id', set_value("educational_level_id", $item->educational_level_id), " ") ?>
                                    <?= form_error('educational_level_id', '<div class="bottom" style="color:red" >', '</div>'); ?>
                                </div>
                            </div>
                             -->
                            
                        </div>
                        
                        <!--
                        <div class="group1" style="overflow: hidden; float: none;">
                            <div class="span3">
                                <div class="span6"> <label><?= lang('group_passports_occupation') ?>:</label> </div>
                                <div class="span6">
                                    <?= form_input('occupation', set_value("occupation", $item->occupation), " ") ?>
                                    <?= form_error('occupation', '<div class="bottom" style="color:red" >', '</div>'); ?>
                                </div>
                            </div>
                            <div class="span3">
                                <div class="span6">  <label><?= lang('group_passports_date_of_birth') ?>:</label></div>
                                <div class="span6">
                                    <?= form_input('date_of_birth', set_value("date_of_birth", $item->date_of_birth), " ") ?>
                                    <?= form_error('date_of_birth', '<div class="bottom" style="color:red" >', '</div>'); ?>
                                </div>
                            </div>
                            <div class="span3">
                                <div class="span6"> <label><?= lang('group_passports_age') ?>:</label> </div>
                                <div class="span6">
                                    <?= form_input('age', set_value("age", $item->age), " ") ?>
                                    <?= form_error('age', '<div class="bottom" style="color:red" >', '</div>'); ?>
                                </div>
                            </div>
                        </div>
                         -->
                        
                        
                        <?php 
                        if(isset($relative_rows)) {
                        	if(count($relative_rows)>0) {
                        ?>
                        		
               <table cellpadding="0" class="fsTable" cellspacing="0" width="100%" id="tbl_mutamers">
                <thead>
                    <tr>
                        <th><?= lang('group_passports_name_ar') ?></th>
                        <th><?= lang('group_passports_name_la') ?></th>
                        <th><?= lang('group_passports_date_of_birth') ?></th>
                        <th><?= lang('group_passports_age') ?></th>
                        <th><?= lang('group_passports_relative_relation_id') ?></th>
                        <th><?= lang('group_passports_occupation') ?></th>
                        <th><?= lang('group_passports_dpn_serial_no') ?></th>
                        <th><?= lang('group_passports_gender_id') ?></th>                        
					<!--<th><?= lang('global_actions') ?></th>-->
                    </tr>
                </thead>
                <tbody class="sortable">
                        <? foreach ($relative_rows as $item) { ?>
                            <tr  id="row_<?php echo $item->safa_group_passport_id; ?>" group_id="<?= $item->safa_group_id; ?>">
                              
                                
                                    <td> 
                                        <?= $item->full_name_ar ?> 
                                    </td>
                               
                                    <td> 
                                        <?= $item->full_name_la ?> 
                                       
                                    </td>
                               
                                    <td><?= $item->date_of_birth ?></td>
                               
                                    <td><?= $item->age ?></td>
                                
                                    <td><?= $item->erp_relations_name ?></td>
                                
                                    <td><?= $item->occupation ?></td>
                               
                                    <td><?= $item->dpn_serial_no ?></td>
                                
                                    <td><?= $item->erp_gender_name ?></td>
                                
                               
                                <!-- 
                                <td class="TAC">

                                    <a title='<?= lang('mutamer_data') ?>' 
                                       href="<?= site_url("ea/group_passports/show_data") ?>/<?= $item->safa_group_passport_id ?>" class="fancybox fancybox.iframe"
                                       style="color:black; direction:rtl">
                                        <span class="icon-user"></span>
                                    </a>
                                    
									<a href="<?= site_url('ea/group_passports/delete/'.$item->safa_group_id.'/'.$item->safa_group_passport_id) ?>" onclick="return confirm('<?= lang('global_are_you_sure_you_want_to_delete')?>')"><span class="icon-trash"></span></a>
                        
                                </td>
                                 -->
                                 
                            </tr>
                        <?php } ?>
                </tbody>
            </table>	
                        <?php 		
                        	}
                        }
                        ?>
                        
                        
                    </fieldset>
                </div>
                <div id="tabs-3">
                    <fieldset >  
                        <div class="wizerd-div">
                            <a><?php echo lang('notes'); ?></a></div>
                        <legend></legend>
                        <div class="group1">
                            <div class="span6" style="float:none">
                                <textarea  rows="3" style="height: 300%" class="input-huge"></textarea>
                            </div></div>
                        <div class="Div-Sep"><a></a></div>
                        <div class="toolbar bottom TAC"><input type="submit" name="submit" value="<?= lang('global_submit') ?>" class="btn btn-primary"> &nbsp;
                            <!--<input  type ="button" value="<?= lang('global_back') ?>"class="btn btn-primary" onclick="window.location = '<?= site_url('ea/group_passports/index') ?>'">-->
                            <input type="button" value="<?= lang('global_back') ?>" class="btn btn-primary" onclick="window.history.back()"/>
                        </div>
                    </fieldset> 
                </div>
            </div> 
            <?= form_close() ?> 
        </div>
    </div>
</div>


<script>
   $(document).ready(function() {
       jQuery(".adly").attr("disabled", "disabled");
       $("input[type=text]").addClass("input-huge");

        $("#tabs").tabs();
    });
</script>



<script>
$(document).ready(function() {
$(".autocomplete").keypress(function(e){
	
      if (e.which == 13)
        {
         e.preventDefault();
        }
        var field = $(this).attr('name');
        var field_value = $(this).val();
        //alert(field+'--'+field_value);
        
        var data= {'field':field, 'field_value':field_value};

        $.getJSON("<?php echo site_url('ea/group_passports/auto_complete'); ?>/",data, function(result) {
            var elements = [];
            $.each(result, function(i, val) {
                elements.push(val.field_name)
                
            });
            //alert(elements);
            $('#'+field).autocomplete({
                source: elements
            });

            
        });
        
});

});
</script>

    <script>
        $(document).ready(function() {
            $('.fancybox').fancybox({
                afterClose: function() {
                    //location.reload();
                }
            });
        });
    </script>