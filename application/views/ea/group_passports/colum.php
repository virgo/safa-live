<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); ?>
<link href="<?php echo CSS ?>/stylesheets.css" rel="stylesheet" type="text/css" />
<? if (lang('global_lang') == 'ar'): ?>
    <link href="<?php echo CSS ?>/stylesheet-ar.css" rel="stylesheet" type="text/css" />
<? else: ?><? endif ?>
<script type="text/javascript" src='<?php echo JS ?>/plugins/jquery/jquery-1.9.1.min.js'></script>
<script type="text/javascript" src='<?php echo JS ?>/plugins/jquery/jquery-ui-1.10.1.custom.min.js'></script>
<script type="text/javascript" src='<?php echo JS ?>/plugins/bootstrap/bootstrap.min.js'></script>
<link href="<?php echo CSS_JS ?>/dateandtime/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src='<?php echo CSS_JS ?>/dateandtime/bootstrap-datetimepicker.min.js'></script>

<style>
body{
height: auto!important;
overflow: auto !important;
background:none transparent !important;
overflow: hidden;
min-height: 1%;
}

.row-fluid{
padding-top: 5px !important;
padding-bottom: 5px !important;
}
.row-form{padding: 0px!important;}
</style>

<div class="">
    <div class="modal-headar">
        <div class="ifix movement reportscon"><h2><?= lang('choose_cols') ?></h2> </div>
    </div> 
    <div class="modal-body">
        <?= form_open() ?>
        <div class="row-form">
            <div class="span12">
            <input type="checkbox" id="chk_new" onclick="checkAll('chk');" class="checkall"/><?= lang('choose_cols') ?><br>
            </div>
            
            </div>
            
            <div class="row-form">
                <div class="span3">
                <input  id="chk" name="cols[]" value="group_id" type="checkbox" <?= (in_array('group_id', $cols)) ? "checked=checked" : "" ?> > <?= lang('group_passports_group_id') ?>
            </div>
                <div class="span3"><input id="chk" name="cols[]" value="passport_no" type="checkbox" <?= (in_array('passport_no', $cols)) ? "checked=checked" : "" ?> > <?= lang('group_passports_passport_no') ?> 
                </div>
                <div class="span3"><input id="chk" name="cols[]" value="name_la" type="checkbox" <?= (in_array('name_la', $cols)) ? "checked=checked" : "" ?> > <?= lang('group_passports_name_la') ?> 
                </div>
                <div class="span3"><input id="chk" name="cols[]" value="name_ar" type="checkbox" <?= (in_array('name_ar', $cols)) ? "checked=checked" : "" ?> > <?= lang('group_passports_name_ar') ?>
                </div>
                
            </div>
            <div class="row-form">
                <div class="span3"><input id="chk" name="cols[]" value="nationality_id" type="checkbox" <?= (in_array('nationality_id', $cols)) ? "checked=checked" : "" ?> > <?= lang('group_passports_nationality_id') ?>
               </div>
                <div class="span3"><input id="chk" name="cols[]" value="passport_type_id" type="checkbox" <?= (in_array('passport_type_id', $cols)) ? "checked=checked" : "" ?> > <?= lang('group_passports_passport_type_id') ?>
                </div>
                <div class="span3"><input id="chk" name="cols[]" value="passport_issue_date" type="checkbox" <?= (in_array('passport_issue_date', $cols)) ? "checked=checked" : "" ?> > <?= lang('group_passports_passport_issue_date') ?>
                </div>
                <div class="span3"><input id="chk" name="cols[]" value="passport_expiry_date" type="checkbox" <?= (in_array('passport_expiry_date', $cols)) ? "checked=checked" : "" ?> > <?= lang('group_passports_passport_expiry_date') ?>                   
                </div>
                
            </div>
            <div class="row-form">
                <div class="span3"><input id="chk" name="cols[]" value="passport_issuing_city" type="checkbox" <?= (in_array('passport_issuing_city', $cols)) ? "checked=checked" : "" ?> > <?= lang('group_passports_passport_issuing_city') ?>
                </div>
                <div class="span3"><input id="chk" name="cols[]" value="passport_issuing_country_id" type="checkbox" <?= (in_array('passport_issuing_country_id', $cols)) ? "checked=checked" : "" ?> > <?= lang('group_passports_passport_issuing_country_id') ?>
                </div>
                <div class="span3"><input id="chk" name="cols[]" value="passport_dpn_count" type="checkbox" <?= (in_array('passport_dpn_count', $cols)) ? "checked=checked" : "" ?> > <?= lang('group_passports_passport_dpn_count') ?>
                </div>
                <div class="span3"><input id="chk" name="cols[]" value="dpn_serial_no" type="checkbox" <?= (in_array('dpn_serial_no', $cols)) ? "checked=checked" : "" ?> > <?= lang('group_passports_dpn_serial_no') ?>
                </div>
                
            </div>
            <div class="row-form">
                <div class="span3"><input id="chk" name="cols[]" value="relative_relation_id" type="checkbox" <?= (in_array('relative_relation_id', $cols)) ? "checked=checked" : "" ?> > <?= lang('group_passports_relative_relation_id') ?>
                </div>
                <div class="span3"><input id="chk" name="cols[]" value="educational_level_id" type="checkbox" <?= (in_array('educational_level_id', $cols)) ? "checked=checked" : "" ?> > <?= lang('group_passports_educational_level_id') ?>
                </div>
                <div class="span3"><input id="chk" name="cols[]" value="occupation" type="checkbox" <?= (in_array('occupation', $cols)) ? "checked=checked" : "" ?> > <?= lang('group_passports_occupation') ?>
                </div>
                <div class="span3"><input id="chk" name="cols[]" value="date_of_birth" type="checkbox" <?= (in_array('date_of_birth', $cols)) ? "checked=checked" : "" ?> > <?= lang('group_passports_date_of_birth') ?> 
                </div>
                
            </div>
            <div class="row-form">
                <div class="span3"><input id="chk" name="cols[]" value="age" type="checkbox" <?= (in_array('age', $cols)) ? "checked=checked" : "" ?> > <?= lang('group_passports_age') ?>
                </div>
                <div class="span3"><input id="chk" name="cols[]" value="birth_country_id" type="checkbox" <?= (in_array('birth_country_id', $cols)) ? "checked=checked" : "" ?> > <?= lang('group_passports_birth_country_id') ?>
                </div>
                <div class="span3"><input id="chk" name="cols[]" value="marital_status_id" type="checkbox" <?= (in_array('marital_status_id', $cols)) ? "checked=checked" : "" ?> > <?= lang('group_passports_marital_status_id') ?> 
                </div>
                <div class="span3"><input id="chk" name="cols[]" value="city" type="checkbox" <?= (in_array('city', $cols)) ? "checked=checked" : "" ?> > <?= lang('group_passports_city') ?>
                </div>
                
            </div>
            <div class="row-form">
                <div class="span3"><input id="chk" name="cols[]" value="erp_country_id" type="checkbox" <?= (in_array('erp_country_id', $cols)) ? "checked=checked" : "" ?> > <?= lang('group_passports_erp_country_id') ?>
                </div>
                <div class="span3"><input id="chk" name="cols[]" value="no" type="checkbox" <?= (in_array('no', $cols)) ? "checked=checked" : "" ?> > <?= lang('group_passports_no') ?>
               </div>
                <div class="span3"><input id="chk" name="cols[]" value="relative_no" type="checkbox" <?= (in_array('relative_no', $cols)) ? "checked=checked" : "" ?> > <?= lang('group_passports_relative_no') ?>
                </div>
                <div class="span3"><input id="chk" name="cols[]" value="display_order" type="checkbox" <?= (in_array('display_order', $cols)) ? "checked=checked" : "" ?> > <?= lang('group_passports_display_order') ?>
                </div>
                
            </div>
            
           <div class="row-form">
                <div class="span3"><input id="chk" name="cols[]" value="name" type="checkbox" <?= (in_array('name', $cols)) ? "checked=checked" : "" ?> > <?= lang('name') ?>
                </div>
                
            </div>
                
                
                
                
                <!--
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <input id="chk" name="cols[]" value="relative_gender_id" type="checkbox" <?= (in_array('relative_gender_id', $cols)) ? "checked=checked" : "" ?> > <?= lang('group_passports_relative_gender_id') ?>
                 -->
                 
                
                
            </div>
        </div>
        
            
                    
                    <!-- 
                    <div class="row-form">
                    <div class="span6">
	                <div class="span4 TAL Pleft10">
	                    <?php echo  lang('rows_per_page') ?>
	                </div>
	                <div class="span8">
	                <select name="rows_count_per_page" size="1" aria-controls="tbl_mutamers">
					
					    <option value="5" >5</option>
					    <option value="10">10</option>
					    <option value="25">25</option>
					    <option value="50">50</option>
					    <option value="100">100</option>
					    <option value="200">200</option>
					    <option value="500">500</option>
					
					</select>
	                </div>
					</div>
					</div>
					 -->
        	   
        
        
        <div class="toolbar bottom TAL">
            <button name="submit"  class="btn btn-primary close_frame"><?= lang("global_submit") ?></button>    
        </div>
        <?= form_close() ?>
    </div>
</div>
<script>
                $('.close_frame').click(function() {
                    //parent.$.fancybox.close();
                });
</script>
<script type="text/javascript">
    function checkAll(checkId) {

        var inputs = document.getElementsByTagName("input");

        for (var i = 0; i < inputs.length; i++) {
            if ($(".checkall").is(':checked')) {
                inputs[i].checked = true;
                $('span').addClass('checked');
            } else {
                inputs[i].checked = false;
                $('span').removeClass('checked');
            }
        }
    }
</script>

