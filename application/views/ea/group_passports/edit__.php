<style>
    .wizerd-div {
        margin: 10px 0px 25px !important; 
    }
    .lesspx {width: 95px;}
    a.togglebutton  { padding: 5px 8px;
                      border-radius: 0px;
                      font-weight: bolder;
                      display: inline;
                      transition-duration: 1s;
                      cursor: pointer;
                      margin-top: 1px;

    }
    div.upload {
        width: 250px;
        height: 57px;
        background: url(<?= base_url('static/images') ?>/button-ar.png) no-repeat;
        overflow: hidden;
    }
    .width100{width:96% !important;}

    div.upload input {
        display: block !important;
        width: 200px !important;
        height: 57px !important;
        opacity: 0 !important;
        overflow: hidden !important;
    }
    a.togglebutton:hover {background: #888;
                          transition-duration: 1s;}
    </style>
    <div class="row-fluid">


    <div class="widget">
        <div class="widget-header">
            <div class="widget-header-icon Fright">  </div>
            <div class="widget-header-title Fright">
                تعديل بيانات المعتمر
            </div>
        </div>

        <?= form_open(false, ' id="passportsForm" ') ?>
        <div class="widget-container ">

            <div class="wizerd-div"><a>البيانات الشخصية للمعتمر</a></div>

            <div class="group1"  style="float: none;overflow: hidden;">
                <div class="row-fluid span5">
                    <div class="span12">
                        <div class="span3"><label>رقم المعتمر:</label>  </div>
                        <div class="span4"><input type="number" value="0" readonly></div>


                    </div>
                </div>
                <div class="row-fluid span4">
                    <div class="span12">
                        <div class="span3"> <label>المجموعة:</label> </div>
                        <div class="span9">
                            <select class="validate[required]" type="drop" ></select>
                        </div>
                    </div>
                </div>  
            </div> 

            <div class="wizerd-div"><a>البيانات الشخصية للمعتمر</a></div>

            <div class="group1" style="float: none;overflow: hidden;">

                <div class="row-fluid span10 mar10fleft">
                    <div class="row-fluid span3 mar10fleft">
                        <div class="span5"> <label>الاسم الاول(ع):</label> </div>
                        <div class="span6"><input class="validate[required] lesspx" type="text" maxlength="15">
                        </div>
                    </div>
                    <div class="row-fluid span3 mar10fleft">
                        <div class="span5"> <label>اسم الاب (ع):</label> </div>
                        <div class="span6"><input type="text" maxlength="15" class="validate[required] lesspx">
                        </div>
                    </div>
                    <div class="row-fluid span3 mar10fleft">
                        <div class="span5"> <label>اسم الجد (ع):</label> </div>
                        <div class="span6"><input type="text"maxlength="15" class="validate[required] lesspx">
                        </div>
                    </div>
                    <div class="row-fluid span3 mar10fleft">
                        <div class="span5"> <label class="lesspx">اسم العائلة(ع):</label> </div>
                        <div class="span6"><input type="text"maxlength="15" class="validate[required] lesspx">
                        </div>
                    </div>
                </div>
                <div class="row-fluid span2 " style="float:left !important; ">
                    <div class="span12">
                        <div class="addpicbox span12 TAC" >
                            <img src="<?= base_url('static/images') ?>/safa-pic.png" alt="اضف صورة">
                        </div>
                        <div class="span12  upload">
                            <input name="upload " type="file" style="width:100px;"></input>
                        </div>
                    </div>
                </div>
                <div class="row-fluid span10 mar10fleft ">
                    <div class="row-fluid span3 mar10fleft">
                        <div class="span5"> <label>الاسم الاول(E):</label> </div>
                        <div class="span6"><input type="text" maxlength="15" class="validate[required] lesspx">
                        </div>
                    </div>
                    <div class="row-fluid span3 mar10fleft">
                        <div class="span5"> <label>اسم الاب (E):</label> </div>
                        <div class="span6"><input type="text" maxlength="15" class="validate[required] lesspx">
                        </div>
                    </div>
                    <div class="row-fluid span3 mar10fleft">
                        <div class="span5"> <label>اسم الجد (E):</label> </div>
                        <div class="span6"><input type="text" maxlength="15" class="validate[required] lesspx">
                        </div>
                    </div>
                    <div class="row-fluid span3 mar10fleft">
                        <div class="span5"> <label class="lesspx">اسم العائلة (E):</label> </div>
                        <div class="span6">
                            <input type="text" maxlength="15" class="validate[required] lesspx">
                        </div>
                    </div>
                </div>

                <div class="row-fluid span10 mar10fleft ">
                    <div class="row-fluid span3 mar10fleft">
                        <div class="span2"> <label>البلد:</label> </div>
                        <div class="span10">
                            <?= form_dropdown('country', ddgen('erp_nationalities', array('erp_nationality_id', name())), set_value('country'), 'class="validate[required] width100 " type="drop"') ?>
                        </div>
                    </div>
                    <div class="row-fluid span3 mar10fleft">
                        <div class="span5"> <label>تاريخ الميلاد:</label> </div>
                        <div class="span6"><input type="text" class="datepicker lesspx ">
                        </div>
                    </div>
                    <div class="row-fluid span3 mar10fleft">
                        <div class="span5"> <label>مدينة الميلاد:</label> </div>
                        <div class="span6"> <input type="text" class="lesspx validate[required]">
                        </div>
                    </div>
                    <div class="row-fluid span3 mar10fleft">
                        <div class="span5"> <label>الرقم القومي</label> </div>
                        <div class="span7 ">
                            <input class="socialid longinputs validate[required,custom[checkid]]" type="text" maxlength="14" >
                        </div>
                    </div>




                </div>

                <div class="row-fluid span10 mar10fleft ">
                    <div class="row-fluid span3 mar10fleft">
                        <div class="span5"> <label>الجنس:</label> </div>
                        <div class="span7"><?= form_dropdown('', ddgen('erp_gender', array('erp_gender_id', name())), set_value(), 'class="input-huge sexid " type="drop"') ?>



                        </div>
                    </div>

                    <div class="row-fluid span3 mar10fleft">
                        <div class="span5"> <label style="font-size:10px;">الحالة الاجتماعية:</label> </div>
                        <div class="span7"><?= form_dropdown('', ddgen('erp_maritalstatus', array('erp_maritalstatus_id', name())), set_value(''), 'class="validate[required] input-huge" type="drop"') ?>
                        </div>
                    </div>
                    <div class="row-fluid span3 mar10fleft">
                        <div class="span5"> <label style="font-size:10px;">مستوى التعليم:</label> </div>
                        <div class="span7">
                            <?= form_dropdown('', ddgen('', array('', name())), set_value(''), 'class="validate[required] input-huge" type="drop"') ?>
                        </div>
                    </div>
                    <div class="row-fluid span3 mar10fleft">
                        <div class="span5"> <label>المهنة:</label> </div>
                        <div class="span6"><input class="longinputs" style="width:170%;" type="text">
                        </div>
                    </div>
                </div>

            </div>


            <div class="wizerd-div"><a>بيانات جواز السفر</a></div>
            <div class="group1" style="float: none;overflow: hidden;">


                <div class="row-fluid span12 mar10fleft">
                    <div class="row-fluid span3 mar10fleft">
                        <div class="span5"> <label>رقم جواز السفر:</label> </div>
                        <div class="span6"><input type="text" class="validate[required] lesspx">
                        </div>
                    </div>
                    <div class="row-fluid span3 mar10fleft">
                        <div class="span5"> <label>بلد الاصدار:</label> </div>
                        <div class="span7">  <?= form_dropdown('country', ddgen('erp_nationalities', array('erp_nationality_id', name())), set_value('country'), 'class="validate[required] input-huge" type="drop"') ?>
                        </div>
                    </div>
                    <div class="row-fluid span3 mar10fleft">
                        <div class="span5"> <label>مدينة الاصدار:</label> </div>
                        <div class="span6"><input type="text" class="validate[required] lesspx">
                        </div>
                    </div>
                    <!-- HERE -->
                    <div class="row-fluid span3 mar10fleft">
                        <div class="span5"> <label>تاريخ الانتهاء:</label> </div>
                        <div class="span6"><input type="text" id="passportend" class="lesspx validate[required,custom[checkExpireDate]]">
                        </div>
                    </div>
                </div>
                <div class="row-fluid span12 mar10fleft">

                    <div class="row-fluid span3 mar10fleft">
                        <div class="span5"> <label>تاريخ الاصدار:</label> </div>
                        <div class="span6"><input type="text" class="lesspx datepicker" id=" passportstarts">
                        </div>
                    </div>
                    <div class="row-fluid span3 mar10fleft">
                        <div class="span3"> <label>الجنسية:</label> </div>
                        <div class="span9">
                            <?= form_dropdown('country', ddgen('erp_nationalities', array('erp_nationality_id', name())), set_value('country'), 'class="validate[required] width100 " type="drop"') ?>
                        </div>
                    </div>
                    <div class="row-fluid span3 mar10fleft">
                        <div class="span5"> <label>نوع جواز السفر</label> </div>
                        <div class="span7"><?= form_dropdown('', ddgen('erp_passporttypes', array('erp_passporttype_id', name())), 'class="input-huge" type="drop"') ?>
                        </div>
                    </div>
                </div>
            </div>

            <div class="wizerd-div"><a>بيانات التأشيرة</a></div>
            <div class="group1" style="float: none;overflow: hidden;">


                <div class="row-fluid span12 mar10fleft">
                    <div class="row-fluid span3 mar10fleft">
                        <div class="span5"> <label>الموافقة:</label> </div>
                        <div class="span6"><input type="text" class="validate[required] lesspx">
                        </div>
                    </div>
                    <div class="row-fluid span3 mar10fleft">
                        <div class="span5"> <label>رقم الطلب:</label> </div>
                        <div class="span7"> <input type="text" class="validate[required] lesspx">
                        </div>
                    </div>
                    <div class="row-fluid span3 mar10fleft">
                        <div class="span5"> <label>رقم التأشيرة:</label> </div>
                        <div class="span6"><input type="text" class="validate[required] lesspx">
                        </div>
                    </div>
                    <!-- HERE -->
                    <div class="row-fluid span3 mar10fleft">
                        <div class="span5"> <label>رقم الحدود:</label> </div>
                        <div class="span6"><input type="text"  class="lesspx validate[required]">
                        </div>
                    </div>
                </div>
                <div class="row-fluid span12 mar10fleft">

                    <div class="row-fluid span3 mar10fleft">
                        <div class="span5"> <label>تاريخ الموافقة:</label> </div>
                        <div class="span6"><input type="text" class="lesspx datepicker" >
                        </div>
                    </div>
                    <div class="row-fluid span3 mar10fleft">
                        <div class="span5"> <label>تاريخ الطلب:</label> </div>
                        <div class="span7"> <input type="text" class="lesspx datepicker" >
                        </div>
                    </div>
                    <div class="row-fluid span3 mar10fleft">
                        <div class="span5"> <label>تاريخ التأشيرة:</label> </div>
                        <div class="span7"><input type="text" class="lesspx datepicker" >
                        </div>
                    </div>
                </div>
            </div>

            <div class="hideable">
                <div class="wizerd-div " ><a >بيانات المحرم</a></div>


                <div class="group1"  style="float: none; overflow: hidden;">

                    <div class="row-fluid span3 mar10fleft" >
                        <div class="span5"> <label>المحرم:</label> </div>
                        <div class="span7"><?= form_dropdown('', array(), 'class="input-huge" type="drop"') ?>
                        </div>

                    </div>

                    <div class="row-fluid span3 mar10fleft">
                        <div class="span5"> <label>صلة القرابة:</label> </div>
                        <div class="span7"><?= form_dropdown('', ddgen('erp_relations', array('erp_relation_id', name())), 'class="input-huge" type="drop"') ?>
                        </div>
                    </div>

                </div>
            </div>
            <div class="wizerd-div " ><a >بيانات العنوان</a> <a class="Fleft togglebutton" id="slide_me">-</a></div>
            <div class="group1" style="float: none;overflow: hidden;" id="slide_down">
                <div class="row-fluid span12 mar10fleft">
                    <div class="row-fluid span3 mar10fleft">
                        <div class="span3"> <label>
                                بلد الميلاد:
                            </label> </div>
                        <div class="span9">
                            <?= form_dropdown('country', ddgen('erp_nationalities', array('erp_nationality_id', name())), set_value('country'), 'class="validate[required] width100 " type="drop"') ?>
                        </div>
                    </div>
                    <div class="row-fluid span2 ">
                        <div class="span5"> <label>الولاية:</label> </div>
                        <div class="span6"><input type="text" class="lesspx">
                        </div>
                    </div>
                    <div class="row-fluid span2">
                        <div class="span5"> <label>المدينة:</label> </div>
                        <div class="span6"><input type="text" class="lesspx">
                        </div>
                    </div>
                    <div class="row-fluid span2">
                        <div class="span5"> <label>الشارع:</label> </div>
                        <div class="span6"><input type="text" class="lesspx">
                        </div>
                    </div>
                    <div class="row-fluid span3">
                        <div class="span5"> <label>الرمز البريدي:</label> </div>
                        <div class="span6"><input type="text" class="lesspx">
                        </div>
                    </div>
                </div>
                <div class="row-fluid span12 mar10fleft">
                    <div class="row-fluid span3 mar10fleft">
                        <div class="span3"> <label>بريد الكتروني:</label> </div>
                        <div class="span9">
                            <input type="text" class="lesspx">
                        </div>
                    </div>
                    <div class="row-fluid span2 ">
                        <div class="span5"> <label>الهاتف:</label> </div>
                        <div class="span6"><input type="text" class="lesspx">
                        </div>
                    </div>
                    <div class="row-fluid span2">
                        <div class="span5"> <label>الموبايل:</label> </div>
                        <div class="span6"><input type="text" class="lesspx">
                        </div>
                    </div>
                    <div class="row-fluid span2">
                        <div class="span5"> <label>ملاحظات:</label> </div>
                        <div class="span6"><input type="text" class="lesspx">
                        </div>
                    </div>

                </div>
            </div>


            <div class="wizerd-div">
                <div class="wizerd-div " ><a >بيانات المرافقين في جواز السفر</a> <a class="Fleft togglebutton" id="slide_me2">-</a></div>
                <div class="group1" style="float: none;overflow: hidden;" id="slide_down2">
                    <div class="group1" style="float: none;overflow: hidden;">

                        <div class="row-fluid span12 mar10fleft">
                            <div class="row-fluid span3 mar10fleft">
                                <div class="span5"> <label>الاسم الاول(ع):</label> </div>
                                <div class="span6"><input class="validate[required] lesspx" type="text" maxlength="15">
                                </div>
                            </div>
                            <div class="row-fluid span3 mar10fleft">
                                <div class="span5"> <label>اسم الاب (ع):</label> </div>
                                <div class="span6"><input type="text" maxlength="15" class="validate[required] lesspx">
                                </div>
                            </div>
                            <div class="row-fluid span3 mar10fleft">
                                <div class="span5"> <label>اسم الجد (ع):</label> </div>
                                <div class="span6"><input type="text"maxlength="15" class="validate[required] lesspx">
                                </div>
                            </div>
                            <div class="row-fluid span3 mar10fleft">
                                <div class="span5"> <label class="lesspx">اسم العائلة(ع):</label> </div>
                                <div class="span6"><input type="text"maxlength="15" class="validate[required] lesspx">
                                </div>
                            </div>
                        </div>
                        <div class="row-fluid span12 mar10fleft">
                            <div class="row-fluid span3 mar10fleft">
                                <div class="span5"> <label>الاسم الاول(E):</label> </div>
                                <div class="span6"><input class="validate[required] lesspx" type="text" maxlength="15">
                                </div>
                            </div>
                            <div class="row-fluid span3 mar10fleft">
                                <div class="span5"> <label>اسم الاب (E):</label> </div>
                                <div class="span6"><input type="text" maxlength="15" class="validate[required] lesspx">
                                </div>
                            </div>
                            <div class="row-fluid span3 mar10fleft">
                                <div class="span5"> <label>اسم الجد (E):</label> </div>
                                <div class="span6"><input type="text"maxlength="15" class="validate[required] lesspx">
                                </div>
                            </div>
                            <div class="row-fluid span3 mar10fleft">
                                <div class="span5"> <label class="lesspx">اسم العائلة(E):</label> </div>
                                <div class="span6"><input type="text"maxlength="15" class="validate[required] lesspx">
                                </div>
                            </div>
                        </div>
                        <div class="row-fluid span3 mar10fleft">
                            <div class="span5"> <label>تاريخ الميلاد:</label> </div>
                            <div class="span6"><input type="text" class=" lesspx datepicker">
                            </div>
                        </div>
                        <div class="row-fluid span3 mar10fleft">
                            <div class="span5"> <label>مدينة الميلاد:</label> </div>
                            <div class="span6"><input type="text">
                            </div>
                        </div>


                        <div class="row-fluid span12 ">

                            <div class="row-fluid span3 mar10fleft">
                                <div class="span2"> <label>البلد:</label> </div>
                                <div class="span10">
                                    <?= form_dropdown('country', ddgen('erp_nationalities', array('erp_nationality_id', name())), set_value('country'), 'class="validate[required] width100 " type="drop"') ?>
                                </div>
                            </div>
                            <div class="row-fluid span3 mar10fleft">
                                <div class="span5"> <label>الجنس:</label> </div>
                                <div class="span7"><?= form_dropdown('', ddgen('erp_gender', array('erp_gender_id', name())), 'class="input-huge" type="drop"') ?>
                                </div>
                            </div>
                            <div class="row-fluid span3 mar10fleft">
                                <div class="span5"> <label>العلاقة:</label> </div>
                                <div class="span7"><?= form_dropdown('', ddgen('erp_relations', array('erp_relation_id', name())), 'class="input-huge" type="drop"') ?>
                                </div>

                            </div>
                            <div class="span3"> <button class="btn" style="margin-top:-3px;">إضافةمرافق</button></div>

                        </div>

                        <div class="row-fluid span12 ">
                            <table cellpadding="0" class="" cellspacing="0" style="width: 100%;">
                                <thead>
                                    <tr role="row">
                                        <td>الاسم </td>
                                        <td>تاريخ الميلاد</td>
                                        <td>مدينة الميلاد</td>
                                        <td>بلد الميلاد</td>
                                        <td>الجنس</td>
                                        <td>العلاقة</td>
                                        <td>
                                            عمليات
                                        </td>
                                    </tr>

                                </thead>
                                <tbody>
                                    <!--Single row-->
                                    <tr colspan="7" >
                                        <td rowspan="1">
                                            هدى محمد احمد محمد 
                                        </td>
                                        <td rowspan="2">
                                            تاريخ
                                        </td>
                                        <td rowspan="2">
                                            الاسكندرية
                                        </td>
                                        <td rowspan="2">
                                            مصر
                                        </td>
                                        <td rowspan="2">
                                            انثى
                                        </td>
                                        <td rowspan="2">
                                            ابنة
                                        </td>
                                        <td colspan="1">
                                            <input type="checkbox" name="edit" >حذف<br>
                                        </td>

                                    </tr>
                                    <tr colspan="7" >
                                        <td rowspan="1">
                                            Huda Muhammad Ahmad Muhammad 
                                        </td>

                                        <td colspan="1">
                                            <input type="checkbox" name="edit">تعديل<br>
                                        </td>
                                        <!--single row end-->
                                        <!--Single row-->
                                    <tr colspan="7" >
                                        <td rowspan="1">
                                            محمد احمد محمد الاسيوطي
                                        </td>
                                        <td rowspan="2">
                                            تاريخ
                                        </td>
                                        <td rowspan="2">
                                            الاسكندرية
                                        </td>
                                        <td rowspan="2">
                                            مصر
                                        </td>
                                        <td rowspan="2">
                                            ذكر
                                        </td>
                                        <td rowspan="2">
                                            زوج
                                        </td>
                                        <td colspan="1">
                                            <input type="checkbox" name="edit" >حذف<br>
                                        </td>

                                    </tr>
                                    <tr colspan="7" >
                                        <td rowspan="1">
                                            Muhammad Ahmad Muhammad El-asiouty
                                        </td>

                                        <td colspan="1">
                                            <input type="checkbox" name="edit">تعديل<br>
                                        </td>
                                        <!--single row end-->

                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>



                    <div class="span12" style="margin:auto">
                        <div class="row-fluid mar10fleft">
                            <div class="span4"></div>
                            <div class="span1"><input type="submit" class="btn submit_button" value="حفظ"></div>
                            <div class="span1"><input onclick="return confirm('هل انت متأكد من حذف كل ما قد تم  ادخاله  في هذه الشاشة ؟')" type="reset" style="height: 35px; margin-top: 5px; font-family: GE-SS-med;" class="btn submit_button" value="مسح"></div>
                            <div class="span1"><input type="submit" class="btn " value="تحديث"></div>
                            <div class="span1"><input type="submit" class="btn " value="حذف"></div>


                        </div>
                    </div>
                </div>
                <?= form_close() ?> 
            </div>

        </div>

        <script>

            $(document).ready(function() {
                $("#passportsForm").validationEngine({
                    promptPosition: "topRight:-150"
                });


                $('#slide_me').click(function() {
                    $('#slide_down').toggle('slow');
                });
                $('#slide_me2').click(function() {
                    $('#slide_down2').toggle('slow');
                });
                $(".datepicker").datepicker({dateFormat: 'yy-mm-dd', minDate: '-120y', maxDate: '0', showButtonPanel: true, showAnim: 'show'});
                $("#passportend").datepicker({dateFormat: 'yy-mm-dd', minDate: '-7y', maxDate: '+7y', showButtonPanel: true, showAnim: 'show'});
                $("#passportstarts").datepicker({dateFormat: 'yy-mm-dd', minDate: '-7y', maxDate: '0', showButtonPanel: true, showAnim: 'show'});
            });

            var idofid = function() {
            };
            $('.sexid').change(function() {

                if ($('.sexid').val() == 1) {
                    $('.hideable').hide();
                } else {
                    $('.hideable').show();
                }

            })



        </script>