<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); ?>
<!--<link href="<?= CSS ?>/stylesheets.css" rel="stylesheet" type="text/css" />
<? if (lang('global_lang') == 'ar'): ?>
    <link href="<?= CSS ?>/stylesheet-ar.css" rel="stylesheet" type="text/css" />
<? else: ?><? endif ?>
<script type="text/javascript" src='<?= JS ?>/plugins/jquery/jquery-1.9.1.min.js'></script>
<script type="text/javascript" src='<?= JS ?>/plugins/jquery/jquery-ui-1.10.1.custom.min.js'></script>
<script type="text/javascript" src='<?= JS ?>/plugins/bootstrap/bootstrap.min.js'></script>
<link href="<?= CSS_JS ?>/dateandtime/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src='<?= CSS_JS ?>/dateandtime/bootstrap-datetimepicker.min.js'></script>-->
<style>

body{
height: auto!important;
overflow: auto !important;
background:none transparent !important;
overflow: hidden;
min-height: 3%;
}
.modal-body{
    width: 96%;
    padding:0 2% ;
}
.row-fluid{
padding-top: 5px !important;
padding-bottom: 5px !important;
}
</style>

<style>
.wizerd-div,.row-form{width: 96%;margin:6% 2%;}
label{padding: 0;}
</style>
        
        
<!-- multi selection choosen -->
<link rel="stylesheet" href="<?= NEW_CSS_JS ?>/multi-select-chosen/chosen.css">
<script src="<?= NEW_CSS_JS ?>/multi-select-chosen/chosen.jquery.js" type="text/javascript"></script>

            

<div class="modal-body" id="dv_main_flight_availabilities" style="display: none;">
        

<?= form_open('ea/group_passports/add_flights_popup', ' name="frm_add_flights_popup_table" id="frm_add_flights_popup_table" method="post" ') ?>

        <div class="row-fluid" id="dv_flight_availabilities">
            <table cellpadding="0" class="" cellspacing="0" width="100%" id="tbl_flight_availabilities">
                <thead>
                    <tr>
                        <th><input type="checkbox" class="checkall" id="checkall" onchange='checkUncheck()'/> </th>
                       
                        <th><?= lang('flight_from_to') ?></th>
                        <th><?= lang('departure_date') ?></th>
                        <th><?= lang('arrival_date') ?></th>
                        <th><?= lang('available_seats_count') ?></th>

                    </tr>
                </thead>
                <tbody>
                    <? if (isset($results)) { ?>
                        <? foreach ($results as $value) { 
                        $this->flight_availabilities_model->erp_flight_availability_id = $value->erp_flight_availability_id;
                        $flight_availabilities_details_rows = $this->flight_availabilities_model->get_details_for_trip();
                        
                        $ports='';
                        $start_datetime='';
                        $end_datetime='';
                        $loop_counter=0;
                        
                        $erp_port_id_from = 0;
                        $erp_port_id_to =0;
                        $start_ports_name ='';
                        $end_ports_name ='';
                        
                        
                        foreach($flight_availabilities_details_rows as $flight_availabilities_details_row) {
                        	if($loop_counter==0) {
                        		
                        		$erp_port_id_from = $flight_availabilities_details_row->erp_port_id_from;
                        		$start_ports_name =$flight_availabilities_details_row->start_ports_name;
                        
                        		$erp_port_id_to = $flight_availabilities_details_row->erp_port_id_to;
                        		$end_ports_name =$flight_availabilities_details_row->end_ports_name;
                        
                        		$ports = $flight_availabilities_details_row->start_ports_name.' - '.$flight_availabilities_details_row->end_ports_name;
                        		$start_datetime = $flight_availabilities_details_row->flight_date .' - '.$flight_availabilities_details_row->departure_time;
                        		$end_datetime = $flight_availabilities_details_row->flight_date .' - '.$flight_availabilities_details_row->arrival_time;
                        	} else {
                        		$erp_port_id_to = $flight_availabilities_details_row->erp_port_id_to;
                        		$end_ports_name =$flight_availabilities_details_row->end_ports_name;
                        
                        		$ports = $ports.' <br/> '.$flight_availabilities_details_row->start_ports_name.' - '.$flight_availabilities_details_row->end_ports_name;
                        		$end_datetime = $flight_availabilities_details_row->flight_date .' - '.$flight_availabilities_details_row->arrival_time;
                        	}
                        	$loop_counter++;
                        }
                       	?>
                            <tr id="row_<?php echo $value->erp_flight_availability_id; ?>">
                            <td>
                            <input type="checkbox" name="erp_flight_availability_ids[]" id="erp_flight_availability_ids[]"  value="<?= $value->erp_flight_availability_id ?>" title=""/>
                           
                            <input type='hidden' name='trip_safa_transporter_id_<?= $value->erp_flight_availability_id ?>'  id='trip_safa_transporter_id_<?= $value->erp_flight_availability_id ?>'  value='<?= $value->safa_transporter_id ?>'/>
                        	<input type='hidden' name='trip_safa_transporter_name_<?= $value->erp_flight_availability_id ?>'  id='trip_safa_transporter_name_<?= $value->erp_flight_availability_id ?>'  value='<?= $value->safa_transporter_name ?>'/>
                        	
                        	
                            </td>
                            
                            
                             
                            <td>
                            
                             
                            <input type='hidden' name='erp_port_id_from_<?= $value->erp_flight_availability_id ?>'  id='erp_port_id_from_<?= $value->erp_flight_availability_id ?>'  value='<?= $erp_port_id_from ?>'/>
                            <input type='hidden' name='erp_port_id_to_<?= $value->erp_flight_availability_id ?>'  id='erp_port_id_to_<?= $value->erp_flight_availability_id ?>'  value='<?= $erp_port_id_to ?>'/>
                            
                            <input type='hidden' name='start_ports_name_<?= $value->erp_flight_availability_id ?>'  id='start_ports_name_<?= $value->erp_flight_availability_id ?>'  value='<?= $start_ports_name ?>'/>
                            <input type='hidden' name='end_ports_name_<?= $value->erp_flight_availability_id ?>'  id='end_ports_name_<?= $value->erp_flight_availability_id ?>'  value='<?= $end_ports_name ?>'/>

                             <?php echo $ports; ?>
                             <input type='hidden' name='ports_name_<?= $value->erp_flight_availability_id ?>'  id='ports_name_<?= $value->erp_flight_availability_id ?>'  value='<?= $ports ?>'/>
                            
                            </td>
                            
                            
                             <td>
                             
                            <input type='hidden' name='departure_date_<?= $value->erp_flight_availability_id ?>'  id='departure_date_<?= $value->erp_flight_availability_id ?>'  value='<?= $start_datetime ?>'/>
                             <?php echo $start_datetime; ?>
                            </td>
                            
                            <td>
                            
                            <input type='hidden' name='arrival_date_<?= $value->erp_flight_availability_id ?>'  id='arrival_date_<?= $value->erp_flight_availability_id ?>'  value='<?= $end_datetime ?>'/>
                            <?php echo $end_datetime; ?>
                             
                            </td>
                            
                            <td>
                            <input type='hidden' name='available_seats_count_<?= $value->erp_flight_availability_id ?>'  id='available_seats_count_<?= $value->erp_flight_availability_id ?>'  value='<?= $value->available_seats_count ?>'/>
                            <input type='text' name='available_seats_count_text_<?= $value->erp_flight_availability_id ?>'  id='available_seats_count_text_<?= $value->erp_flight_availability_id ?>'  value='<?= $value->available_seats_count ?>' readonly="readonly" disabled="disabled"/>
                            
							
                            </td>
                            
                            
                            </tr>
                        <? } ?>
                    <? } ?>
                </tbody>
            </table>
        </div>
        
      <input type='hidden' name='hdn_safa_group_passport_ids'  id='hdn_safa_group_passport_ids'  value=''/>
                            
      <div class="toolbar bottom TAC">
          <input type ="submit" name="smt_save" id="smt_save" value="<?= lang('global_submit') ?>" class="btn btn-primary" onclick="return select_seats();">
      </div>
        
    <?= form_close() ?>
    
</div>    

<div class="modal-body" id="dv_private_or_availability" >
<div class="toolbar bottom TAC">
   <a href="javascript:void();"  id="lnk_private_flight" class="btn btn-primary" ><?= lang('private_flight') ?></a>
   <a href="javascript:void();"  id="lnk_flight_availability" class="btn btn-primary" ><?= lang('flight_availability') ?></a>
</div>
</div>

<script type="text/javascript">
$(document).ready(function() {
	$("#lnk_private_flight").click(function() {
		page_flight_availabilities = window.open('<?= site_url('flight_availabilities/manage') ?>');		
		//window.parent.location.reload();
	});

	$("#lnk_flight_availability").click(function() {
		document.getElementById('dv_main_flight_availabilities').style.display='block';
		document.getElementById('dv_private_or_availability').style.display='none';
	});
});

</script>


<script type="text/javascript">
$(document).ready(function() {

	var safa_group_passport_ids_str= '';
	for (var i = 0; i < parent.document.form_mutamers.elements.length; i++ ) {
        if (parent.document.form_mutamers.elements[i].type == 'checkbox' && parent.document.form_mutamers.elements[i].id !='checkall'){
	        if(parent.document.form_mutamers.elements[i].checked) {	
		        if(safa_group_passport_ids_str=='') {
		        	safa_group_passport_ids_str =  parent.document.form_mutamers.elements[i].value;
		        } else {	       		    
        			safa_group_passport_ids_str = safa_group_passport_ids_str +',' +  parent.document.form_mutamers.elements[i].value;
		        }
	        } 
        }
    }

	$("#hdn_safa_group_passport_ids").val(safa_group_passport_ids_str);
	
	var dataString = 'safa_group_passport_ids_str='+ safa_group_passport_ids_str;
    
	$.ajax({
        type: "POST",
        url: "<?php echo site_url('ea/group_passports'); ?>/get_flight_availabilities_by_safa_group_passport_ids",
        data: dataString,
    	cache: true,
    	success: function(html)
    	{
        	$("#dv_flight_availabilities").html(html);
    	}
      });
});
</script>

<script>
function select_seats()
{
	var are_checked=false;
	for (var i = 0; i < document.frm_add_flights_popup_table.elements.length; i++ ) 
	{
	    if (document.frm_add_flights_popup_table.elements[i].checked ) {
	    	are_checked = true;
	     } 
	 }
	if(are_checked==false) {
		alert('<?php echo lang('global_select_one_record_at_least');?>');
		return false;
	}


	var safa_group_passport_ids_count= 0;
	for (var i = 0; i < parent.document.form_mutamers.elements.length; i++ ) {
        if (parent.document.form_mutamers.elements[i].type == 'checkbox' && parent.document.form_mutamers.elements[i].id !='checkall'){
	        if(parent.document.form_mutamers.elements[i].checked) {	
	        	safa_group_passport_ids_count = safa_group_passport_ids_count + 1;
	        } 
        }
    }

	for (var i = 0; i < document.frm_add_flights_popup_table.elements.length; i++ ) 
    {
        if (document.frm_add_flights_popup_table.elements[i].type == 'checkbox' && document.frm_add_flights_popup_table.elements[i].id =='erp_flight_availability_ids[]') {
		    if (document.frm_add_flights_popup_table.elements[i].checked){

		    	var erp_flight_availability_id= document.frm_add_flights_popup_table.elements[i].value;
		    	var available_seats_count= document.getElementById('available_seats_count_'+erp_flight_availability_id).value;
		    	
		    	if(parseInt(safa_group_passport_ids_count) > parseInt(available_seats_count)) {
		    		alert('<?php echo lang('required_seats_count_more_than_available');?>');
		    		return false;
		    	}
		    }
        }
    }
	
	
}
</script>