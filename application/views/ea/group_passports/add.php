<style>
    .wizerd-div {
        margin: 10px 0px 25px !important; 
    }
    .lesspx {width: 95px;}
    a.togglebutton  { padding: 5px 8px;
                      border-radius: 0px;
                      font-weight: bolder;
                      display: inline;
                      transition-duration: 1s;
                      cursor: pointer;
                      margin-top: 1px;

    }
    div.upload {
        width: 250px;
        height: 57px;
        background: url(<?= base_url('static/images') ?>/button-ar.png) no-repeat;
        overflow: hidden;
    }
    .width100{width:96% !important;}

    div.upload input {
        display: block !important;
        width: 200px !important;
        height: 57px !important;
        opacity: 0 !important;
        overflow: hidden !important;
    }
    a.togglebutton:hover {background: #888;
                          transition-duration: 1s;}
    </style>
    <div class="row-fluid">


 	<div class="widget">
        <div class="path-container Fright">
            <div class="path-name Fright">
                <a href="<?php echo site_url() . 'ea/dashboard'; ?>"><?php echo lang('global_system_management') ?></a>
            </div>

            <div class="path-arrow Fright"></div>
            <div class="path-name Fright"> <a href="<?= site_url('ea/safa_groups/') ?>"> <?= lang('group_passports_title') ?></a></div>
            <div class="path-arrow Fright"></div>
            <div class="path-name Fright"> 
                <? if ($safa_group_id !== FALSE) { ?>
                    <?
                    $name = $this->safa_group_passports_model->get_group_name($safa_group_id);
                    ?>
                    <a href="<?= site_url('ea/group_passports/passports_index/' . $safa_group_id) ?>"> <?= lang('group_passports_group_mutamers_title') ?> <?= $name->name ?></a></div>
            <? } else { ?>
                <?= lang('group_passports_title_2') ?> 
            <? } ?>
            <div class="path-arrow Fright"></div>
            <div class="path-name Fright">  <?= lang('add_group_passports') ?></div>
        </div>   
    </div>
    
    <div class="widget">
        <div class="widget-header">
            <div class="widget-header-icon Fright">  </div>
            <div class="widget-header-title Fright">
                <?php echo lang('add_group_passports') ?>
            </div>
        </div>

        <?= form_open_multipart(false, ' id="passportsForm" ') ?>
        <div class="widget-container ">

            <!--<div class="wizerd-div"><a><?= lang('personal_data') ?></a></div>

            <div class="group1"  style="float: none;overflow: hidden;">
                <div class="row-fluid span5">
                    <div class="span12">
                        <div class="span3"><label><?= lang('group_passports_no') ?>:</label>  </div>
                        <div class="span4">
						<?= form_input('no', set_value("no", ''), " readonly") ?>
                        <?= form_error('no', '<div class="bottom" style="color:red" >', '</div>'); ?>
                                   
						</div>


                    </div>
                </div>
                <div class="row-fluid span4">
                    <div class="span12">
                        <div class="span3"> <label><?= lang('group_passports_group_id') ?>:</label> </div>
                        <div class="span9">
                            <?= form_dropdown('safa_group_id', ddgen('safa_groups', array('safa_group_id', 'name'), false , array('name','')), set_value('safa_group_id', $safa_group_id), "  class='chosen select input-huge'  ") ?>
							<?= form_error('safa_group_id', '<div class="bottom" style="color:red" >', '</div>'); ?>
                                   
                        </div>
                    </div>
                </div>  
            </div> 

            --><div class="wizerd-div"><a><?= lang('personal_data') ?></a></div>

            <div class="group1" style="float: none;overflow: hidden;">

                <div  class="row-fluid span10 mar10fleft">

                            <div class="row-fluid span3 mar10fleft">
                                <div class="span5"> <label><?= lang('group_passports_first_name_ar') ?>:</label> </div>
                                <div class="span6">
                                    <?= form_input('first_name_ar', set_value("first_name_ar"), " class='validate[required] lesspx '  maxlength='15'  id='first_name_ar' ") ?>
                                    <?= form_error('first_name_ar', '<div class="bottom" style="color:red" >', '</div>'); ?>
                                </div>
                            </div>
                            <div class="row-fluid span3 mar10fleft">
                                <div class="span5"><label><?= lang('group_passports_second_name_ar') ?>:</label>  </div>
                                <div class="span6">
                                    <?= form_input('second_name_ar', set_value("second_name_ar"), " class='validate[required] lesspx'  maxlength='15'  id='second_name_ar' ") ?>
                                    <?= form_error('second_name_ar', '<div class="bottom" style="color:red" >', '</div>'); ?>
                                </div>
                            </div>
                            <div class="row-fluid span3 mar10fleft">
                                <div class="span5"> <label><?= lang('group_passports_third_name_ar') ?>:</label> </div>
                                <div class="span6">
                                    <?= form_input('third_name_ar', set_value("third_name_ar"), " class='validate[required] lesspx'  maxlength='15'  id='third_name_ar' ") ?>
                                    <?= form_error('third_name_ar', '<div class="bottom" style="color:red" >', '</div>'); ?>
                                </div>
                            </div>
                            <div class="row-fluid span3 mar10fleft">
                                <div class="span5"><label class="lesspx"><?= lang('group_passports_fourth_name_ar') ?>:</label>  </div>
                                <div class="span6">
                                    <?= form_input('fourth_name_ar', set_value("fourth_name_ar"), " class='validate[required] lesspx'  maxlength='15'  id='fourth_name_ar' ") ?>
                                    <?= form_error('fourth_name_ar', '<div class="bottom" style="color:red" >', '</div>'); ?>
                                </div>
                            </div>


                        </div> 
                
                
                
                
                
                <div class="row-fluid span2 " style="float:left !important; ">
                    <div class="span12">
                        <div class="addpicbox span12 TAC" >
                            <img src="<?= base_url('static/images') ?>/safa-pic.png" alt="<?php echo lang('add_image');?>">
                        </div>
                        <div class="span12  upload">
                            <input id="picture_file" name="picture_file"  type="file" style="width:100px;"></input>
                        
                        </div>
                    </div>
                </div>
                
                
                
                <div class="row-fluid span10 mar10fleft " >
                            <div class="row-fluid span3 mar10fleft">
                                <div class="span5"> <label><?= lang('group_passports_first_name_la') ?>:</label> </div>
                                <div class="span6">
                                    <?= form_input('first_name_la', set_value("first_name_la"), " class='validate[required] lesspx'  maxlength='15'  id='first_name_la' ") ?>
                                    <?= form_error('first_name_la', '<div class="bottom" style="color:red" >', '</div>'); ?>
                                </div>
                            </div>
                            <div class="row-fluid span3 mar10fleft">
                                <div class="span5"> <label><?= lang('group_passports_second_name_la') ?>:</label> </div>
                                <div class="span6">
                                    <?= form_input('second_name_la', set_value("second_name_la"), " class='validate[required] lesspx'  maxlength='15'  id='second_name_la' ") ?>
                                    <?= form_error('second_name_la', '<div class="bottom" style="color:red" >', '</div>'); ?>
                                </div>
                            </div>
                            <div class="row-fluid span3 mar10fleft">
                                <div class="span5"> <label><?= lang('group_passports_third_name_la') ?>:</label> </div>
                                <div class="span6">
                                    <?= form_input('third_name_la', set_value("third_name_la"), " class='validate[required] lesspx'  maxlength='15'   id='third_name_la' ") ?>
                                    <?= form_error('third_name_la', '<div class="bottom" style="color:red" >', '</div>'); ?>
                                </div>
                            </div>
                            <div class="row-fluid span3 mar10fleft">
                                <div class="span5" > <label lass="lesspx"><?= lang('group_passports_fourth_name_la') ?>:</label> </div>
                                <div class="span6">
                                    <?= form_input('fourth_name_la', set_value("fourth_name_la"), " class='validate[required] lesspx'  maxlength='15'  id='fourth_name_la' ") ?>
                                    <?= form_error('fourth_name_la', '<div class="bottom" style="color:red" >', '</div>'); ?>
                                </div>
                            </div>


                        </div> 
                

                <div class="row-fluid span10 mar10fleft ">
                    <div class="row-fluid span3 mar10fleft">
                        <div class="span2"> <label><?= lang('group_passports_birth_country_id') ?>:</label> </div>
                        <div class="span10">
                            <?= form_dropdown('birth_country_id', ddgen('erp_countries', array('erp_country_id', name()), false , array(name(),'')), set_value('birth_country_id'), "  class='chosen select lesspx width100' ") ?>                       
							<?= form_error('birth_country_id', '<div class="bottom" style="color:red" >', '</div>'); ?>
                                
                        </div>
                    </div>
                    <div class="row-fluid span3 mar10fleft">
                        <div class="span5"> <label><?= lang('group_passports_date_of_birth') ?>:</label> </div>
                        <div class="span6">
                        <?= form_input('date_of_birth', set_value("date_of_birth"), " class='lesspx datepicker' ") ?>
                                    <?= form_error('date_of_birth', '<div class="bottom" style="color:red" >', '</div>'); ?>
                                    <script>
                                        $('.date_of_birth').datepicker({
                                            dateFormat: "yy-mm-dd",
                                            controlType: 'select',
                                            timeFormat: 'HH:mm'
                                        });
                                    </script>
                        </div>
                    </div>
                    <div class="row-fluid span3 mar10fleft">
                        <div class="span5"> <label><?= lang('group_passports_birth_city') ?>:</label> </div>
                        <div class="span6"> 
							<?= form_input('birth_city', set_value("birth_city"), "  class='lesspx validate[required]' id='birth_city'  ") ?>
                                    <?= form_error('birth_city', '<div class="bottom" style="color:red" >', '</div>'); ?>
                        </div>
                    </div>
                     <div class="row-fluid span3 mar10fleft">
                        <div class="span5"> <label><?= lang('group_passports_id_number') ?> </label> </div>
                        <div class="span7 ">
                            
                        <?= form_input('id_number', set_value("id_number"), "  class='socialid longinputs validate[required,custom[checkid]] lesspx' maxlength='14' id='id_number'  ") ?>
                       <?= form_error('id_number', '<div class="bottom" style="color:red" >', '</div>'); ?>
                           
                        
                        </div>
                    </div>




                </div>

                <div class="row-fluid span10 mar10fleft ">
                    <div class="row-fluid span3 mar10fleft">
                        <div class="span5"> <label><?= lang('group_passports_gender_id') ?>:</label> </div>
                        <div class="span7">
                        <?= form_dropdown('gender_id', ddgen('erp_gender', array('erp_gender_id', name())), set_value('gender_id'), "  id='gender_id'  class='chosen input-huge' ") ?>                       
                        <?= form_error('gender_id', '<div class="bottom" style="color:red" >', '</div>'); ?>

                        </div>
                    </div>

                    <div class="row-fluid span3 mar10fleft">
                        <div class="span5"> <label style="font-size:10px;"><?= lang('group_passports_marital_status_id') ?>:</label> </div>
                        <div class="span7"> 
                        <?= form_dropdown('marital_status_id', ddgen('erp_maritalstatus', array('erp_maritalstatus_id', name())), set_value('marital_status_id'), "  class='chosen input-huge' ") ?>                       
						<?= form_error('marital_status_id', '<div class="bottom" style="color:red" >', '</div>'); ?>
                        </div>
                    </div>
                    <div class="row-fluid span3 mar10fleft">
                        <div class="span5"> <label style="font-size:10px;"><?= lang('group_passports_educational_level_id') ?>:</label> </div>
                        <div class="span7">
                            <?= form_dropdown('educational_level_id', ddgen('erp_educationlevels', array('erp_educationlevel_id', name())), set_value('educational_level_id'), "  class='chosen input-huge' ") ?>                       
							<?= form_error('educational_level_id', '<div class="bottom" style="color:red" >', '</div>'); ?>
                        </div>
                    </div>
                    <div class="row-fluid span3 mar10fleft">
                        <div class="span5"> <label><?= lang('group_passports_occupation') ?>:</label> </div>
                        <div class="span6">
                        
                        <?= form_input('occupation', set_value("occupation"), "  class='longinputs'  id='occupation'  ") ?>
                       <?= form_error('occupation', '<div class="bottom" style="color:red" >', '</div>'); ?>
                                
                        </div>
                    </div>
                </div>

            </div>


            <div class="wizerd-div"><a><?= lang('passports_details') ?></a></div>
            <div class="group1" style="float: none;overflow: hidden;">


                <div class="row-fluid span12 mar10fleft">
                    <div class="row-fluid span3 mar10fleft">
                        <div class="span5"> <label><?= lang('group_passports_passport_no') ?>:</label> </div>
                        <div class="span6">
                        
                        <?= form_input('passport_no', set_value("passport_no"), " class='validate[required] lesspx'") ?>
                        <?= form_error('passport_no', '<div class="bottom" style="color:red" >', '</div>'); ?>
                                    
                        </div>
                    </div>
                    <div class="row-fluid span3 mar10fleft">
                        <div class="span5"> <label><?= lang('group_passports_passport_issuing_country_id') ?>:</label> </div>
                        <div class="span7">  
                        <?= form_dropdown('passport_issuing_country_id', ddgen('erp_countries', array('erp_country_id', name()), false , array(name(),'')), set_value('passport_issuing_country_id'), "  class='validate[required] chosen-select chosen-rtl input-full' ") ?>                       
                        <?= form_error('passport_issuing_country_id', '<div class="bottom" style="color:red" >', '</div>'); ?>
                        </div>
                    </div>
                    <div class="row-fluid span3 mar10fleft">
                        <div class="span5"> <label><?= lang('group_passports_passport_issuing_city') ?>:</label> </div>
                        <div class="span6">
                        <?= form_input('passport_issuing_city', set_value('passport_issuing_city'), "   class='validate[required] lesspx'  id='passport_issuing_city'  ") ?>
                       	<?= form_error('passport_issuing_city', '<div class="bottom" style="color:red" >', '</div>'); ?>
                        </div>
                    </div>
                    <!-- HERE -->
                    <div class="row-fluid span3 mar10fleft">
                        <div class="span5"> <label><?= lang('group_passports_passport_expiry_date') ?>:</label> </div>
                        <div class="span6">
                        
                        <?= form_input('passport_expiry_date', set_value("passport_expiry_date"), " id='passportend' class='lesspx validate[required,custom[checkExpireDate]]' ") ?>
                         <?= form_error('passport_expiry_date', '<div class="bottom" style="color:red" >', '</div>'); ?>
                                    
                        </div>
                    </div>
                </div>
                <div class="row-fluid span12 mar10fleft">

                    <div class="row-fluid span3 mar10fleft">
                        <div class="span5"> <label><?= lang('group_passports_passport_issue_date') ?>:</label> </div>
                        <div class="span6">
                        <?= form_input('passport_issue_date', set_value("passport_issue_date"), " class='lesspx datepicker' id='passportstarts' ") ?>
                        <?= form_error('passport_issue_date', '<div class="bottom" style="color:red" >', '</div>'); ?>
                                    
                        </div>
                    </div>
                    <div class="row-fluid span3 mar10fleft">
                        <div class="span3"> <label><?= lang('group_passports_nationality_id') ?>:</label> </div>
                        <div class="span9">
                            <?= form_dropdown('nationality_id', ddgen('erp_nationalities', array('erp_nationality_id', name()), false , array(name(),'')), set_value('nationality_id'), "  class='chosen select width100' ") ?>
                            <?= form_error('nationality_id', '<div class="bottom" style="color:red" >', '</div>'); ?>
                        </div>
                    </div>
                    <div class="row-fluid span3 mar10fleft">
                        <div class="span5"> <label><?= lang('group_passports_passport_type_id') ?></label> </div>
                        <div class="span7">
                        <?= form_dropdown('passport_type_id', ddgen('erp_passporttypes', array('erp_passporttype_id', name())), set_value('passport_type_id'), "  class='chosen input-huge' ") ?>
						<?= form_error('passport_type_id', '<div class="bottom" style="color:red" >', '</div>'); ?>
                        </div>
                    </div>
                </div>
            </div>

            <!--<div class="wizerd-div"><a><?= lang('visa') ?></a></div>
            <div class="group1" style="float: none;overflow: hidden;">

            	<div class="row-fluid span12 mar10fleft">
                            
                            <div class="row-fluid span3 mar10fleft">
                                <div class="span5">  <label><?= lang('mofa') ?>:</label></div>
                                <div class="span7">
                                    <?= form_input('mofa', set_value("mofa", $item->mofa), " ") ?>
                                    <?= form_error('mofa', '<div class="bottom" style="color:red" >', '</div>'); ?>
                                </div>
                            </div>
                            

							<div class="row-fluid span3 mar10fleft">
                                <div class="span5">  <label><?= lang('mofa_date') ?>:</label></div>
                                <div class="span7">
                                    <?= form_input('mofa_date', set_value("mofa_date", $item->mofa_date), " class='datepicker' ") ?>
                                    <?= form_error('mofa_date', '<div class="bottom" style="color:red" >', '</div>'); ?>
                                </div>
                            </div>
                            
                            <div class="row-fluid span3 mar10fleft">
                                <div class="span5">  <label><?= lang('enumber') ?>:</label></div>
                                <div class="span7">
                                    <?= form_input('enumber', set_value("enumber", $item->enumber), " ") ?>
                                    <?= form_error('enumber', '<div class="bottom" style="color:red" >', '</div>'); ?>
                                </div>
                            </div>
                            
                            <div class="row-fluid span3 mar10fleft">
                                <div class="span5">  <label><?= lang('enumber_date') ?>:</label></div>
                                <div class="span7">
                                    <?= form_input('enumber_date', set_value("enumber_date", $item->enumber_date), " class='datepicker' ") ?>
                                    <?= form_error('enumber_date', '<div class="bottom" style="color:red" >', '</div>'); ?>
                                </div>
                            </div>
                            

                        </div>
            
            			<div class="row-fluid span12 mar10fleft">
            			
            			 <div class="row-fluid span3 mar10fleft">
                                <div class="span5">  <label><?= lang('visa_number') ?>:</label></div>
                                <div class="span7">
                                    <?= form_input('visa_number', set_value("visa_number", $item->visa_number), " ") ?>
                                    <?= form_error('visa_number', '<div class="bottom" style="color:red" >', '</div>'); ?>
                                </div>
                            </div>
                            
                            <div class="row-fluid span3 mar10fleft">
                                <div class="span5">  <label><?= lang('visa_date') ?>:</label></div>
                                <div class="span7">
                                    <?= form_input('visa_date', set_value("visa_date", $item->visa_date), " class='datepicker' ") ?>
                                    <?= form_error('visa_date', '<div class="bottom" style="color:red" >', '</div>'); ?>
                                </div>
                            </div>
                            
                            <div class="row-fluid span3 mar10fleft">
                                <div class="span5">  <label><?= lang('border_number') ?>:</label></div>
                                <div class="span7">
                                    <?= form_input('border_number', set_value("border_number", $item->border_number), " ") ?>
                                    <?= form_error('border_number', '<div class="bottom" style="color:red" >', '</div>'); ?>
                                </div>
                            </div>
                            
                            
                             
                            
                        </div>
            
            </div>
            

            --><div class="hideable">
                <div class="wizerd-div " ><a ><?= lang('group_passports_relative_data') ?></a></div>


                <div class="group1"  style="float: none; overflow: hidden;">

                    <div class="row-fluid span3 mar10fleft" >
                        <div class="span5"> <label><?= lang('group_passports_relative_no') ?>:</label> </div>
                        <div class="span7">
                        <?php
                                    $passport_fullname='full_name_ar';
                                    
                                    $structure = array('safa_group_passport_id', $passport_fullname);
                                    $key = $structure['0'];
                                    if (isset($structure['1'])) {
                                        $value = $structure['1'];
                                    }
                                    $passports_arr = array();
                                    $passports_arr[''] = lang('global_select_from_menu');
                                    $this->safa_group_passports_model->safa_group_id = $safa_group_id;
                                    $this->safa_group_passports_model->safa_group_passport_id = false;
                                    //$this->safa_group_passports_model->limit=false;

                                    $this->safa_group_passports_model->order_by=array($passport_fullname, '');
                                    
                                    $passports = $this->safa_group_passports_model->get_for_table();
                                    foreach ($passports as $passport) {
                                        $passports_arr[$passport->$key] = $passport->$value;
                                    }
                                    ?>
                                    <?= form_dropdown('relative_no', $passports_arr, set_value('relative_no'), "  class='chosen select input-huge' ") ?>                       

                                    <?= form_error('relative_no', '<div class="bottom" style="color:red" >', '</div>'); ?>
                                
                               
                        </div>

                    </div>

                    <div class="row-fluid span3 mar10fleft">
                        <div class="span5"> <label><?= lang('group_passports_relative_relation_id') ?>:</label> </div>
                        <div class="span7">
                        <?= form_dropdown('relative_relation_id', ddgen('erp_relations', array('erp_relation_id', name()), '', '', true), set_value('relative_relation_id'), "  class='chosen input-huge' ") ?>                       
						<?= form_error('relative_relation_id', '<div class="bottom" style="color:red" >', '</div>'); ?>
                        </div>
                    </div>

                </div>
            </div>
            <div class="wizerd-div " ><a ><?= lang('address_data') ?></a> <a class="Fleft togglebutton" id="slide_me">-</a></div>
            <div class="group1" style="float: none;overflow: hidden;" id="slide_down">
                <div class="row-fluid span12 mar10fleft">
                    <div class="row-fluid span3 mar10fleft">
                        <div class="span3"> <label>
                                <?= lang('group_passports_erp_country_id') ?>:
                            </label> </div>
                        <div class="span9">
                           <?= form_dropdown('erp_country_id', ddgen('erp_countries', array('erp_country_id', name()), false , array(name(),'')), set_value('erp_country_id'), "  class=' input-huge  validate[required]' ") ?>                       
							<?= form_error('erp_country_id', '<div class="bottom" style="color:red" >', '</div>'); ?>
                        </div>
                    </div>
                    <div class="row-fluid span2 ">
                        <div class="span5"> <label><?= lang('group_passports_region_name') ?>:</label> </div>
                        <div class="span6">
                         <?= form_input('region_name', set_value("region_name"), "  class='lesspx'  id='region_name'  ") ?>
                         <?= form_error('region_name', '<div class="bottom" style="color:red" >', '</div>'); ?>
                        </div>
                    </div>
                    <div class="row-fluid span2">
                        <div class="span5"> <label><?= lang('group_passports_city') ?>:</label> </div>
                        <div class="span6">
                         <?= form_input('city', set_value("city"), "  class='lesspx'  id='city'  ") ?>
                                    <?= form_error('city', '<div class="bottom" style="color:red" >', '</div>'); ?>
                        </div>
                    </div>
                    <div class="row-fluid span2">
                        <div class="span5"> <label><?= lang('group_passports_address') ?>:</label> </div>
                        <div class="span6">
						<?= form_input('address', set_value("address"), " class='lesspx'  ") ?>
                        <?= form_error('address', '<div class="bottom" style="color:red" >', '</div>'); ?>
                        </div>
                    </div>
                    <div class="row-fluid span3">
                        <div class="span5"> <label><?= lang('zip_code') ?>:</label> </div>
                        <div class="span6">
                        <?= form_input('zip_code', set_value("zip_code"), " class='lesspx'  ") ?>
                        <?= form_error('zip_code', '<div class="bottom" style="color:red" >', '</div>'); ?>
                        </div>
                    </div>
                </div>
                <div class="row-fluid span12 mar10fleft">
                    <div class="row-fluid span3 mar10fleft">
                        <div class="span3"> <label><?= lang('email_address') ?>:</label> </div>
                        <div class="span9">
                            <?= form_input('email_address', set_value("email_address"), " class='lesspx' ") ?>
                        <?= form_error('email_address', '<div class="bottom" style="color:red" >', '</div>'); ?>
                        </div>
                    </div>
                    <div class="row-fluid span2 ">
                        <div class="span5"> <label><?= lang('phone') ?>:</label> </div>
                        <div class="span6">
                        <?= form_input('phone', set_value("phone"), " class='lesspx' ") ?>
                        <?= form_error('phone', '<div class="bottom" style="color:red" >', '</div>'); ?>
                        </div>
                    </div>
                    <div class="row-fluid span2">
                        <div class="span5"> <label><?= lang('mobile') ?>:</label> </div>
                        <div class="span6">
                        <?= form_input('mobile', set_value("mobile"), " class='lesspx' ") ?>
                        <?= form_error('mobile', '<div class="bottom" style="color:red" >', '</div>'); ?>
                        </div>
                    </div>
                    <div class="row-fluid span2">
                        <div class="span5"> <label><?= lang('remarks') ?>:</label> </div>
                        <div class="span6">
                        <?= form_input('remarks', set_value("remarks"), " class='lesspx' ") ?>
                        <?= form_error('remarks', '<div class="bottom" style="color:red" >', '</div>'); ?>
                        </div>
                    </div>

                </div>
            </div>

            <div class="wizerd-div">
                <div class="wizerd-div " ><a ><?= lang('relatives_data_in_passport') ?></a> <a class="Fleft togglebutton" id="slide_me2">-</a></div>
                <div class="group1" style="float: none;overflow: hidden;" id="slide_down2">
                    <div class="group1" style="float: none;overflow: hidden; ">

                        <div class="row-fluid span12 mar10fleft">
                            <div class="row-fluid span3 mar10fleft">
                                <div class="span5"> <label><?= lang('group_passports_first_name_ar') ?>:</label> </div>
                                <div class="span6">
                               		<input type='hidden' name='relative_safa_group_passport_id'  id='relative_safa_group_passport_id'  value='0'/>
         
                                	<?= form_input('relative_first_name_ar', set_value("relative_first_name_ar", ''), " class=' lesspx'  maxlength='15'  id='relative_first_name_ar' ") ?>
                                   	<?= form_error('relative_first_name_ar', '<div class="bottom" style="color:red" >', '</div>'); ?>
                                </div>
                            </div>
                            <div class="row-fluid span3 mar10fleft">
                                <div class="span5"> <label><?= lang('group_passports_second_name_ar') ?>:</label> </div>
                                <div class="span6">
									<?= form_input('relative_second_name_ar', set_value("relative_second_name_ar", ''), " class=' lesspx'  maxlength='15'  id='relative_second_name_ar' ") ?>
                                    <?= form_error('relative_second_name_ar', '<div class="bottom" style="color:red" >', '</div>'); ?>
                                </div>
                            </div>
                            <div class="row-fluid span3 mar10fleft">
                                <div class="span5"> <label><?= lang('group_passports_third_name_ar') ?>:</label> </div>
                                <div class="span6">
                                	<?= form_input('relative_third_name_ar', set_value("relative_third_name_ar", ''), " class=' lesspx'  maxlength='15'  id='relative_third_name_ar' ") ?>
                                    <?= form_error('relative_third_name_ar', '<div class="bottom" style="color:red" >', '</div>'); ?>
                                </div>
                            </div>
                            <div class="row-fluid span3 mar10fleft">
                                <div class="span5"> <label class="lesspx"><?= lang('group_passports_fourth_name_ar') ?>:</label> </div>
                                <div class="span6">
									<?= form_input('relative_fourth_name_ar', set_value("relative_fourth_name_ar", ''), " class=' lesspx'  maxlength='15'  id='relative_fourth_name_ar' ") ?>
                                    <?= form_error('relative_fourth_name_ar', '<div class="bottom" style="color:red" >', '</div>'); ?>
                                </div>
                            </div>
                        </div>
                        <div class="row-fluid span12 mar10fleft">
                            <div class="row-fluid span3 mar10fleft">
                                <div class="span5"> <label><?= lang('group_passports_first_name_la') ?>:</label> </div>
                                <div class="span6">
                                	<?= form_input('relative_first_name_la', set_value("relative_first_name_la", ''), " class=' lesspx'  maxlength='15'  id='relative_first_name_la' ") ?>
                                   	<?= form_error('relative_first_name_la', '<div class="bottom" style="color:red" >', '</div>'); ?>
                                </div>
                            </div>
                            <div class="row-fluid span3 mar10fleft">
                                <div class="span5"> <label><?= lang('group_passports_second_name_la') ?>:</label> </div>
                                <div class="span6">
                                <?= form_input('relative_second_name_la', set_value("relative_second_name_la", ''), " class=' lesspx'  maxlength='15'  id='relative_second_name_la' ") ?>
                                <?= form_error('relative_second_name_la', '<div class="bottom" style="color:red" >', '</div>'); ?>
                                </div>
                            </div>
                            <div class="row-fluid span3 mar10fleft">
                                <div class="span5"> <label><?= lang('group_passports_third_name_la') ?>:</label> </div>
                                <div class="span6">
								<?= form_input('relative_third_name_la', set_value("relative_third_name_la", ''), " class=' lesspx'  maxlength='15'  id='relative_third_name_la' ") ?>
                                   	<?= form_error('relative_third_name_la', '<div class="bottom" style="color:red" >', '</div>'); ?>
                                </div>
                            </div>
                            <div class="row-fluid span3 mar10fleft">
                                <div class="span5"> <label class="lesspx"><?= lang('group_passports_fourth_name_la') ?>:</label> </div>
                                <div class="span6">
                                <?= form_input('relative_fourth_name_la', set_value("relative_fourth_name_la", ''), " class=' lesspx'  maxlength='15'  id='relative_fourth_name_la' ") ?>
                                   	<?= form_error('relative_fourth_name_la', '<div class="bottom" style="color:red" >', '</div>'); ?>
                                </div>
                            </div>
                        </div>
                        <div class="row-fluid span3 mar10fleft">
                            <div class="span5"> <label><?= lang('group_passports_date_of_birth') ?>:</label> </div>
                            <div class="span6">
							<?= form_input('relative_date_of_birth', set_value("relative_date_of_birth", ''), " class='lesspx datepicker' id='relative_date_of_birth' ") ?>
                            <?= form_error('relative_date_of_birth', '<div class="bottom" style="color:red" >', '</div>'); ?>
                                  
                            </div>
                        </div>
                        <div class="row-fluid span3 mar10fleft">
                            <div class="span5"> <label><?= lang('group_passports_birth_city') ?>:</label> </div>
                            <div class="span6">
							<?= form_input('relative_birth_city', set_value("relative_birth_city", ''), "  class='lesspx ' id='relative_birth_city'  ") ?>
                            <?= form_error('relative_birth_city', '<div class="bottom" style="color:red" >', '</div>'); ?>
                            </div>
                        </div>


                        <div class="row-fluid span12 ">

                            <div class="row-fluid span3 mar10fleft">
                                <div class="span2"> <label><?= lang('group_passports_birth_country_id') ?>:</label> </div>
                                <div class="span10">
                                    <?= form_dropdown('relative_birth_country_id', ddgen('erp_countries', array('erp_country_id', name()), false , array(name(),'')), set_value('relative_birth_country_id', ''), "  class='chosen select lesspx width100' id='relative_birth_country_id' ") ?>                       
									<?= form_error('relative_birth_country_id', '<div class="bottom" style="color:red" >', '</div>'); ?>
                             
                                </div>
                            </div>
                            <div class="row-fluid span3 mar10fleft">
                                <div class="span5"> <label><?= lang('group_passports_gender_id') ?>:</label> </div>
                                <div class="span7">
                                <?= form_dropdown('relative_gender_id', ddgen('erp_gender', array('erp_gender_id', name())), set_value('relative_gender_id', ''), "  class='chosen input-huge' id='relative_gender_id' ") ?>                       
                        		<?= form_error('relative_gender_id', '<div class="bottom" style="color:red" >', '</div>'); ?>
									
                                </div>
                            </div>
                            <div class="row-fluid span3 mar10fleft">
                                <div class="span5"> <label><?= lang('group_passports_relative_relation_id') ?>:</label> </div>
                                <div class="span7">
                                <?= form_dropdown('relative_relative_relation_id', ddgen('erp_relations', array('erp_relation_id', name()), '', '', true), set_value('relative_relative_relation_id', ''), "  class='chosen input-huge' id='relative_relative_relation_id' ") ?>                       
								<?= form_error('relative_relative_relation_id', '<div class="bottom" style="color:red" >', '</div>'); ?>
                        
                                </div>

                            </div>
                            <script>  
							relatives_counter = 0;
							</script>
							
                            <div class="span3"> 
                            <a href='javascript:void(0)' id="lnk_add_relative" class="btn-primary" style="margin-top:-3px;" onclick="add_relative(relatives_counter++);"><?= lang('group_passports_add_relative') ?></a>
                            <a href='javascript:void(0)' id="lnk_edit_relative" class="btn-primary" style="margin-top:-3px; display:none;" onclick="update_relative();" ><?= lang('global_edit') ?></a>
                                                       
                            </div>

                        </div>

                        <div class="row-fluid span12 " id="relatives">
                            <table cellpadding="0" class="" cellspacing="0" style="width: 100%; ">
                                <thead >
                                    <tr role="row">
                                        <td><?= lang('group_passports_full_name') ?> </td>
                                        <td><?= lang('group_passports_date_of_birth') ?></td>
                                        <td><?= lang('group_passports_birth_city') ?></td>
                                        <td><?= lang('group_passports_birth_country_id') ?></td>
                                        <td><?= lang('group_passports_gender_id') ?></td>
                                        <td><?= lang('group_passports_relative_relation_id') ?></td>
                                        <td>
                                            <?= lang('global_actions') ?>
                                        </td>
                                    </tr>

                                </thead>
                                <tbody class="relatives">
                            
                                </tbody>
                            </table>
                        </div>
                    </div>



                    <div class="span12" style="margin:auto " >
                        <div class="row-fluid mar10fleft">
                            <div class="span4"></div>
                            <div class="span1"><input type="submit" class="btn submit_button" value="<?= lang('global_submit') ?>"></div>
<!--                            <div class="span1"><input onclick="return confirm('<?= lang('are_you_want_all_you_enter_on_screen') ?>')" type="reset" style="height: 35px; margin-top: 5px; font-family: GE-SS-med;" class="btn submit_button" value="مسح"></div>-->
<!--                            <div class="span1"><input type="submit" class="btn " value="<?= lang('renew') ?>"></div>-->
                            


                        </div>
                    </div>
                </div>
                <?= form_close() ?> 
            </div>

        </div>

        <script>

            $(document).ready(function() {
                $("#passportsForm").validationEngine({
                    promptPosition: "topRight:-150"
                });


                $('#slide_me').click(function() {
                    $('#slide_down').toggle('slow');
                });
                $('#slide_me2').click(function() {
                    $('#slide_down2').toggle('slow');
                });
                $(".datepicker").datepicker({dateFormat: 'yy-mm-dd', minDate: '-120y', maxDate: '0', showButtonPanel: true, showAnim: 'show'});
                $("#passportend").datepicker({dateFormat: 'yy-mm-dd', minDate: '-7y', maxDate: '+7y', showButtonPanel: true, showAnim: 'show'});
                $("#passportstarts").datepicker({dateFormat: 'yy-mm-dd', minDate: '-7y', maxDate: '0', showButtonPanel: true, showAnim: 'show'});
            });

            var idofid = function() {
            };
            $('#gender_id').change(function() {

                if ($('#gender_id').val() == 1) {
                    $('.hideable').hide();
                } else {
                    $('.hideable').show();
                }

            })



        </script>
        
        
<script>
$('.dropdown-toggle').dropdown();
</script>
     
<script>  


function add_relative(id) {

	var relative_first_name_ar = $("#relative_first_name_ar").val();
	var relative_second_name_ar = $("#relative_second_name_ar").val();
	var relative_third_name_ar = $("#relative_third_name_ar").val();
	var relative_fourth_name_ar = $("#relative_fourth_name_ar").val();

	var relative_first_name_la = $("#relative_first_name_la").val();
	var relative_second_name_la = $("#relative_second_name_la").val();
	var relative_third_name_la = $("#relative_third_name_la").val();
	var relative_fourth_name_la = $("#relative_fourth_name_la").val();

	var relative_full_name_ar = relative_first_name_ar+' '+relative_second_name_ar+' '+relative_third_name_ar+' '+relative_fourth_name_ar;
	var relative_full_name_la = relative_first_name_la+' '+relative_second_name_la+' '+relative_third_name_la+' '+relative_fourth_name_la;

	var relative_date_of_birth = $("#relative_date_of_birth").val();
	
	var relative_birth_city = $("#relative_birth_city").val();
	
	var relative_birth_country_id = $("#relative_birth_country_id option:selected").val();
	var relative_birth_country_name_text = '';
	if(relative_birth_country_id!='') {
		relative_birth_country_name_text = $("#relative_birth_country_id option:selected").text();
	}
	
	var relative_gender_id = $("#relative_gender_id option:selected").val();
	var relative_erp_gender_name_text = '';
	if(relative_gender_id!='') {
		relative_erp_gender_name_text = $("#relative_gender_id option:selected").text();
	}
	 
	
	var relative_relative_relation_id = $("#relative_relative_relation_id option:selected").val();
	var relative_erp_relations_name_text = $("#relative_relative_relation_id option:selected").text();
	
	var new_row = function() {/*
               <!--Single row-->
               <tr  rel="{$id}" >
              
                           <td rowspan="1">
                           		<input type='hidden' name='relat_safa_group_passport_id[{$id}]'  id='relat_safa_group_passport_id[{$id}]'  value='0'/>

                           		<input type='hidden' name='relat_first_name_ar[{$id}]'  id='relat_first_name_ar[{$id}]'  value='{$relative_first_name_ar} '/>
								<input type='hidden' name='relat_second_name_ar[{$id}]'  id='relat_second_name_ar[{$id}]'  value='{$relative_second_name_ar}'/>
								<input type='hidden' name='relat_third_name_ar[{$id}]'  id='relat_third_name_ar[{$id}]'  value='{$relative_third_name_ar}'/>
								<input type='hidden' name='relat_fourth_name_ar[{$id}]'  id='relat_fourth_name_ar[{$id}]'  value='{$relative_fourth_name_ar}'/>
	
                               {$relative_full_name_ar} 
                           </td>
                           <td rowspan="2" >
                               {$relative_date_of_birth} 
                               <input type='hidden' name='relat_date_of_birth[{$id}]'  id='relat_date_of_birth[{$id}]'  value='{$relative_date_of_birth}'/>
                            	
                           </td>
                           <td rowspan="2">
                               {$relative_birth_city} 
                               <input type='hidden' name='relat_birth_city[{$id}]'  id='relat_birth_city[{$id}]'  value='{$relative_birth_city}'/>
                            	
                           </td>
                           <td rowspan="2">
                               {$relative_birth_country_name_text} 
                               <input type='hidden' name='relat_birth_country_id[{$id}]'  id='relat_birth_country_id[{$id}]'  value='{$relative_birth_country_id}'/>
                            	
                           </td>
                           <td rowspan="2">
                               {$relative_erp_gender_name_text} 
                               <input type='hidden' name='relat_gender_id[{$id}]'  id='relat_gender_id[{$id}]'  value='{$relative_gender_id}'/>
                            	
                           </td>
                           <td rowspan="2">
                               {$relative_erp_relations_name_text} 
                               <input type='hidden' name='relat_relative_relation_id[{$id}]'  id='relat_relative_relation_id[{$id}]'  value='{$relative_relative_relation_id}'/>
                            	
                           </td>
                           <td rowspan="2">
                           
                           <a href='javascript:void(0)' style="border: 0px;" onclick="edit_relative('{$id}')" ><span class="icon-edit"></span></a>
                           <a href='javascript:void(0)' style="border: 0px;" onclick="delete_relative('{$id}')"><span class="icon-trash"></span></a>
                            
                           
							</td>
							
                       </tr>
                       <tr rel="{$id}" >
                           <td rowspan="1">

                           <input type='hidden' name='relat_first_name_la[{$id}]'  id='relat_first_name_la[{$id}]'  value='{$relative_first_name_la} '/>
							<input type='hidden' name='relat_second_name_la[{$id}]'  id='relat_second_name_la[{$id}]'  value='{$relative_second_name_la}'/>
							<input type='hidden' name='relat_third_name_la[{$id}]'  id='relat_third_name_la[{$id}]'  value='{$relative_third_name_la}'/>
							<input type='hidden' name='relat_fourth_name_la[{$id}]'  id='relat_fourth_name_la[{$id}]'  value='{$relative_fourth_name_la}'/>

                               {$relative_full_name_la}  
                           </td>

                      </tr>
                      <!--single row end-->
                      <!--Single row-->
                      
               */
        }.toString().replace(/{\$id}/g, id)
        .toString().replace(/{\$relative_full_name_ar}/g, relative_full_name_ar)
        .toString().replace(/{\$relative_first_name_ar}/g, relative_first_name_ar)
        .toString().replace(/{\$relative_second_name_ar}/g, relative_second_name_ar)
        .toString().replace(/{\$relative_third_name_ar}/g, relative_third_name_ar)
        .toString().replace(/{\$relative_fourth_name_ar}/g, relative_fourth_name_ar)
        
        .toString().replace(/{\$relative_full_name_la}/g, relative_full_name_la)
        .toString().replace(/{\$relative_first_name_la}/g, relative_first_name_la)
        .toString().replace(/{\$relative_second_name_la}/g, relative_second_name_la)
        .toString().replace(/{\$relative_third_name_la}/g, relative_third_name_la)
        .toString().replace(/{\$relative_fourth_name_la}/g, relative_fourth_name_la)
        
        .toString().replace(/{\$relative_date_of_birth}/g, relative_date_of_birth)
        .toString().replace(/{\$relative_birth_city}/g, relative_birth_city)
        
        .toString().replace(/{\$relative_birth_country_name_text}/g, relative_birth_country_name_text)
        .toString().replace(/{\$relative_birth_country_id}/g, relative_birth_country_id)
        
        .toString().replace(/{\$relative_erp_gender_name_text}/g, relative_erp_gender_name_text)
        .toString().replace(/{\$relative_gender_id}/g, relative_gender_id)
        
        .toString().replace(/{\$relative_erp_relations_name_text}/g, relative_erp_relations_name_text)
        .toString().replace(/{\$relative_relative_relation_id}/g, relative_relative_relation_id)
        
        .slice(14, -3);


$('.relatives').append(new_row);



$("#relative_safa_group_passport_id").val(0);

$("#relative_first_name_ar").val('');
$("#relative_second_name_ar").val('');
$("#relative_third_name_ar").val('');
$("#relative_fourth_name_ar").val('');

$("#relative_first_name_la").val('');
$("#relative_second_name_la").val('');
$("#relative_third_name_la").val('');
$("#relative_fourth_name_la").val('');


$("#relative_date_of_birth").val('');
$("#relative_birth_city").val('');
	
$("#relative_birth_country_id").val('');
$("#relative_birth_country_id").trigger("chosen:updated");

$("#relative_gender_id").val('');
$("#relative_relative_relation_id").val('');

$("#lnk_edit_relative").css('display', 'none');
$("#lnk_add_relative").css('display', 'block');


}


function edit_relative(id) {
	
	$("#relative_safa_group_passport_id").val($("#relat_safa_group_passport_id\\["+id+"\\]").val());
	$("#relative_safa_group_passport_id").attr('rel',id);
		
	$("#relative_first_name_ar").val($("#relat_first_name_ar\\["+id+"\\]").val());
	$("#relative_second_name_ar").val($("#relat_second_name_ar\\["+id+"\\]").val());
	$("#relative_third_name_ar").val($("#relat_third_name_ar\\["+id+"\\]").val());
	$("#relative_fourth_name_ar").val($("#relat_fourth_name_ar\\["+id+"\\]").val());
	
	$("#relative_first_name_la").val($("#relat_first_name_la\\["+id+"\\]").val());
	$("#relative_second_name_la").val($("#relat_second_name_la\\["+id+"\\]").val());
	$("#relative_third_name_la").val($("#relat_third_name_la\\["+id+"\\]").val());
	$("#relative_fourth_name_la").val($("#relat_fourth_name_la\\["+id+"\\]").val());
	
	
	$("#relative_date_of_birth").val($("#relat_date_of_birth\\["+id+"\\]").val());
	$("#relative_birth_city").val($("#relat_birth_city\\["+id+"\\]").val());
		
	$("#relative_birth_country_id").val($("#relat_birth_country_id\\["+id+"\\]").val());
	$("#relative_birth_country_id").trigger("chosen:updated");
	
	$("#relative_gender_id").val($("#relat_gender_id\\["+id+"\\]").val());
	$("#relative_relative_relation_id").val($("#relat_relative_relation_id\\["+id+"\\]").val());

	$("#lnk_add_relative").css('display', 'none');

	//$("#lnk_edit_relative").attr('rel', id)
	$("#lnk_edit_relative").css('display', 'block');
	
	
}


function update_relative() {

	var id_value = $("#relative_safa_group_passport_id").val();

	var id = $("#relative_safa_group_passport_id").attr('rel');
	
	$('.relatives').find('tr[rel=' + id + ']').remove();
	
	var relative_first_name_ar = $("#relative_first_name_ar").val();
	var relative_second_name_ar = $("#relative_second_name_ar").val();
	var relative_third_name_ar = $("#relative_third_name_ar").val();
	var relative_fourth_name_ar = $("#relative_fourth_name_ar").val();

	var relative_first_name_la = $("#relative_first_name_la").val();
	var relative_second_name_la = $("#relative_second_name_la").val();
	var relative_third_name_la = $("#relative_third_name_la").val();
	var relative_fourth_name_la = $("#relative_fourth_name_la").val();

	var relative_full_name_ar = relative_first_name_ar+' '+relative_second_name_ar+' '+relative_third_name_ar+' '+relative_fourth_name_ar;
	var relative_full_name_la = relative_first_name_la+' '+relative_second_name_la+' '+relative_third_name_la+' '+relative_fourth_name_la;

	var relative_date_of_birth = $("#relative_date_of_birth").val();
	
	var relative_birth_city = $("#relative_birth_city").val();
	
	var relative_birth_country_id = $("#relative_birth_country_id option:selected").val();
	var relative_birth_country_name_text = '';
	if(relative_birth_country_id!='') {
		relative_birth_country_name_text = $("#relative_birth_country_id option:selected").text();
	}
	
	var relative_gender_id = $("#relative_gender_id option:selected").val();
	var relative_erp_gender_name_text = '';
	if(relative_gender_id!='') {
		relative_erp_gender_name_text = $("#relative_gender_id option:selected").text();
	}
	 
	
	var relative_relative_relation_id = $("#relative_relative_relation_id option:selected").val();
	var relative_erp_relations_name_text = $("#relative_relative_relation_id option:selected").text();
	
	var new_row = function() {/*
               <!--Single row-->
               <tr  rel="{$id}" >
              
                           <td rowspan="1">
                           		<input type='hidden' name='relat_safa_group_passport_id[{$id}]'  id='relat_safa_group_passport_id[{$id}]'  value='{$id_value}'/>

                           		<input type='hidden' name='relat_first_name_ar[{$id}]'  id='relat_first_name_ar[{$id}]'  value='{$relative_first_name_ar} '/>
								<input type='hidden' name='relat_second_name_ar[{$id}]'  id='relat_second_name_ar[{$id}]'  value='{$relative_second_name_ar}'/>
								<input type='hidden' name='relat_third_name_ar[{$id}]'  id='relat_third_name_ar[{$id}]'  value='{$relative_third_name_ar}'/>
								<input type='hidden' name='relat_fourth_name_ar[{$id}]'  id='relat_fourth_name_ar[{$id}]'  value='{$relative_fourth_name_ar}'/>
	
                               {$relative_full_name_ar} 
                           </td>
                           <td rowspan="2" >
                               {$relative_date_of_birth} 
                               <input type='hidden' name='relat_date_of_birth[{$id}]'  id='relat_date_of_birth[{$id}]'  value='{$relative_date_of_birth}'/>
                            	
                           </td>
                           <td rowspan="2">
                               {$relative_birth_city} 
                               <input type='hidden' name='relat_birth_city[{$id}]'  id='relat_birth_city[{$id}]'  value='{$relative_birth_city}'/>
                            	
                           </td>
                           <td rowspan="2">
                               {$relative_birth_country_name_text} 
                               <input type='hidden' name='relat_birth_country_id[{$id}]'  id='relat_birth_country_id[{$id}]'  value='{$relative_birth_country_id}'/>
                            	
                           </td>
                           <td rowspan="2">
                               {$relative_erp_gender_name_text} 
                               <input type='hidden' name='relat_gender_id[{$id}]'  id='relat_gender_id[{$id}]'  value='{$relative_gender_id}'/>
                            	
                           </td>
                           <td rowspan="2">
                               {$relative_erp_relations_name_text} 
                               <input type='hidden' name='relat_relative_relation_id[{$id}]'  id='relat_relative_relation_id[{$id}]'  value='{$relative_relative_relation_id}'/>
                            	
                           </td>
                           <td rowspan="2">
                           
                           <a href='javascript:void(0)' style="border: 0px;" onclick="edit_relative('{$id}')" ><span class="icon-edit"></span></a>
                           <a href='javascript:void(0)' style="border: 0px;" onclick="delete_relative('{$id}')"><span class="icon-trash"></span></a>
                            
                           
							</td>
							
                       </tr>
                       <tr rel="{$id}" >
                           <td rowspan="1">

                           <input type='hidden' name='relat_first_name_la[{$id}]'  id='relat_first_name_la[{$id}]'  value='{$relative_first_name_la} '/>
							<input type='hidden' name='relat_second_name_la[{$id}]'  id='relat_second_name_la[{$id}]'  value='{$relative_second_name_la}'/>
							<input type='hidden' name='relat_third_name_la[{$id}]'  id='relat_third_name_la[{$id}]'  value='{$relative_third_name_la}'/>
							<input type='hidden' name='relat_fourth_name_la[{$id}]'  id='relat_fourth_name_la[{$id}]'  value='{$relative_fourth_name_la}'/>

                               {$relative_full_name_la}  
                           </td>

                      </tr>
                      <!--single row end-->
                      <!--Single row-->
                      
               */
        }.toString().replace(/{\$id}/g, id)
        .toString().replace(/{\$id_value}/g, id_value)
        .toString().replace(/{\$relative_full_name_ar}/g, relative_full_name_ar)
        .toString().replace(/{\$relative_first_name_ar}/g, relative_first_name_ar)
        .toString().replace(/{\$relative_second_name_ar}/g, relative_second_name_ar)
        .toString().replace(/{\$relative_third_name_ar}/g, relative_third_name_ar)
        .toString().replace(/{\$relative_fourth_name_ar}/g, relative_fourth_name_ar)
        
        .toString().replace(/{\$relative_full_name_la}/g, relative_full_name_la)
        .toString().replace(/{\$relative_first_name_la}/g, relative_first_name_la)
        .toString().replace(/{\$relative_second_name_la}/g, relative_second_name_la)
        .toString().replace(/{\$relative_third_name_la}/g, relative_third_name_la)
        .toString().replace(/{\$relative_fourth_name_la}/g, relative_fourth_name_la)
        
        .toString().replace(/{\$relative_date_of_birth}/g, relative_date_of_birth)
        .toString().replace(/{\$relative_birth_city}/g, relative_birth_city)
        
        .toString().replace(/{\$relative_birth_country_name_text}/g, relative_birth_country_name_text)
        .toString().replace(/{\$relative_birth_country_id}/g, relative_birth_country_id)
        
        .toString().replace(/{\$relative_erp_gender_name_text}/g, relative_erp_gender_name_text)
        .toString().replace(/{\$relative_gender_id}/g, relative_gender_id)
        
        .toString().replace(/{\$relative_erp_relations_name_text}/g, relative_erp_relations_name_text)
        .toString().replace(/{\$relative_relative_relation_id}/g, relative_relative_relation_id)
        
        .slice(14, -3);


$('.relatives').append(new_row);



$("#relative_safa_group_passport_id").val(0);

$("#relative_first_name_ar").val('');
$("#relative_second_name_ar").val('');
$("#relative_third_name_ar").val('');
$("#relative_fourth_name_ar").val('');

$("#relative_first_name_la").val('');
$("#relative_second_name_la").val('');
$("#relative_third_name_la").val('');
$("#relative_fourth_name_la").val('');


$("#relative_date_of_birth").val('');
$("#relative_birth_city").val('');
	
$("#relative_birth_country_id").val('');
$("#relative_birth_country_id").trigger("chosen:updated");

$("#relative_gender_id").val('');
$("#relative_relative_relation_id").val('');

$("#lnk_edit_relative").css('display', 'none');
$("#lnk_add_relative").css('display', 'block');

}

function delete_relative(id)
{
    $('.relatives').find('tr[rel=' + id + ']').remove();
    var hidden_input = '<input type="hidden" name="relativess_remove[]" value="' + id + '" />';
    $('#relatives').append(hidden_input);
}

</script>
     