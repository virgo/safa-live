<? //print_r($mutamers);exit();    ?>
<div class="row-fluid">
   
    
<div class="widget">
    <div class="path-container Fright">
    	<div class="path-name Fright">
            <a href="<?php echo  site_url().'ea/dashboard'; 
            
            ?>"><?php echo  lang('global_system_management') ?></a>
        </div>
        <div class="path-arrow Fright"></div>
        <div class="path-name Fright"> <a href="<?= site_url('ea/group_passports/index') ?>"> <?= lang('group_passports_title') ?></a></div>
        
        <div class="path-arrow Fright"></div>
        <div class="path-name Fright"> <? if ($confirm == 'create'): ?> <?= lang('create_new_umrahgroup') ?><? else: ?><?= lang('move_to_umrahgroup') ?><? endif; ?></div>
    </div>
        
</div>
    
    <? if ($confirm == 'create') { ?>
        <div class="widget">
             
            
        <div class="widget-header">
        <div class="widget-header-icon Fright"> <span class="icos-pencil2"></span> </div>
        <div class="widget-header-title Fright">
            <?php echo  lang('create_new_umrahgroup') ?>
        </div>
    	</div>
                                 
            <div class="block-fluid slidingDiv">
                <?= form_open('ea/group_passports/add_umrah_passports') ?>
                <div class="row-form">
                    <div class="span6">
                        <div class="span4"><?= lang('group_passports_group_name') ?><span style="color: red">*</span>:</div>
                        <div class="span8">
                            <?= form_input('safa_umrahgroup_name', set_value("safa_umrahgroup_name"), "") ?>
                            <?= form_error('safa_umrahgroup_name', '<div class="bottom" style="color:red" >', '</div>'); ?>
                        </div>
                    </div>
                </div>
                <div class="row-form">
                    <div class="span6">
                        <div class="span4"> <?= lang('group_passports_uo_contract') ?><span style="color: red">*</span>:</div>
                        <div class="span8">
                            <? //= form_dropdown('safa_uo_contract_id', ddgen('safa_uo_contracts', array('safa_uo_contract_id', name())), set_value('safa_uo_contract_id'), " name='s_example' class='select' style='width:100%;' ") ?>
                            <?= form_dropdown("safa_uo_contract_id", $uo_contracts, set_value("safa_uo_contract_id"), " name='s_example' class='select' style='width:100%;' ") ?>
                            <?= form_error('safa_uo_contract_id', '<div class="bottom" style="color:red" >', '</div>'); ?>
                        </div>
                    </div>
                </div>
                <div class="toolbar bottom TAC">
                    <input type="submit" name="add" value="<?= lang('global_add') ?>" class="btn btn-primary" />
                    <? if ($safa_group_id !== FALSE): ?>
                        <input  type ="button" value="<?= lang('global_back') ?>"class="btn btn-primary" onclick="window.location = '<?= site_url('ea/group_passports/passports_index/' . $safa_group_id . '') ?>'">
                    <? else: ?>
                        <input  type ="button" value="<?= lang('global_back') ?>"class="btn btn-primary" onclick="window.history.back()">
                    <? endif; ?>
                    <input type='hidden' name='mutamers' value='<?= $mutamers; ?>' class='btn btn-primary'>
                    <input type="hidden" name="confirm_val" value="<?= $confirm; ?>" class="btn btn-primary">
                    <? if ($safa_group_id !== FALSE): ?>
                        <input type="hidden" name="safa_group_id" value="<?= $safa_group_id ?>" class="btn btn-primary">
                    <? endif; ?>
                </div>
                <?= form_close() ?>
            </div>
        </div>
    <? } elseif ($confirm == 'move') { ?>
        <div class="widget">
              
            
            
        <div class="widget-header">
        <div class="widget-header-icon Fright"> <span class="icos-pencil2"></span> </div>
        <div class="widget-header-title Fright">
            <?php echo  lang('move_to_umrahgroup') ?>
        </div>
    	</div>
                               
            <div class="block-fluid slidingDiv">
                <?= form_open('ea/group_passports/add_umrah_passports') ?>
                
                
                <div class="row-form">
                    <div class="span6">
                        <div class="span4"> <?= lang('group_passports_uo_contract') ?><span style="color: red">*</span>:</div>
                        <div class="span8">
                            <?= form_dropdown("safa_uo_contract_id", $uo_contracts, set_value("safa_uo_contract_id"), " name='s_example'  id='contract_id' class='select' style='width:100%;' ") ?>
                            <?= form_error('safa_uo_contract_id', '<div class="bottom" style="color:red" >', '</div>'); ?>
                        </div>
                    </div>
                </div>
                
                
                <div class="row-form">
                    <div class="span6">
                        <div class="span4"><?= lang('group_passports_group_name') ?><span style="color: red">*</span>:</div>
                        <div class="span8">
                        <!-- 
                            <?= form_dropdown('safa_umrahgroup_id', ddgen('safa_umrahgroups', array('safa_umrahgroup_id', 'name')), set_value('safa_umrahgroup_id'), ' name="s_example" class="select" id="umrahgroup_id" style="width:100%;" ') ?>
                        -->
                        <?= form_dropdown('safa_umrahgroup_id', array(), set_value('safa_umrahgroup_id'), ' name="s_example" class="select" id="umrahgroup_id" style="width:100%;" ') ?>
                            <?= form_error('safa_umrahgroup_id', '<div class="bottom" style="color:red" >', '</div>'); ?>
                        </div>
                    </div>
                </div>
                
                
                
                <div class="toolbar bottom TAC">
                    <input type="submit" name="add" value="<?= lang('global_add') ?>" class="btn btn-primary" />
                    <? if ($safa_group_id !== FALSE): ?>
                        <input  type ="button" value="<?= lang('global_back') ?>"class="btn btn-primary" onclick="window.location = '<?= site_url('ea/group_passports/passports_index/' . $safa_group_id . '') ?>'">
                    <? else: ?>
                        <input  type ="button" value="<?= lang('global_back') ?>"class="btn btn-primary" onclick="window.history.back()">
                    <? endif; ?>
                    <input type='hidden' name='mutamers' value='<?= $mutamers; ?>' class='btn btn-primary'>
                    <input type="hidden" name="confirm_val" value="<?= $confirm ?>" class="btn btn-primary">
                    <? if ($safa_group_id !== FALSE): ?>
                        <input type="hidden" name="safa_group_id" value="<?= $safa_group_id ?>" class="btn btn-primary">
                    <? endif; ?>
                </div>
                <?= form_close() ?>
            </div>
        </div>
    <? } ?>
</div>
<script>

//By Gouda
/*
                    $(document).ready(function() {
                        $("#umrahgroup_id").change(function() {
                            //sending the request to get the cities//
                            $.get('<?= site_url("ea/group_passports/get_uo_contract_id") ?>/' + $(this).val(), function(data) {
                                $("#contract_id").html(data);
                                //                    $("#contract_id").removeAttr("disabled");
                                //                    $("#contract_id").attr("disabled", "disabled");
                            });
                        });
                        var country_id = $("#umrahgroup_id").val();
                        $.get('<?= site_url("ea/group_passports/get_uo_contract_id") ?>/' + country_id, function(data) {
                            $("#contract_id").html(data);
                            $("#contract_id").val('<?= $this->input->post('safa_uo_contract_id') ?>');
                        });
                    });
*/   
</script>

<script>
$(document).ready(function() {


    $("#contract_id").change(function() {

    		var contract_id=$(this).val();
    	    
    		var dataString = 'contract_id='+ contract_id;
    	    
    	    $.ajax
    	    ({
    	    	type: 'POST',
    	    	url: '<?php echo base_url().'ea/group_passports/getUmrahgroupsByContract'; ?>',
    	    	data: dataString,
    	    	cache: false,
    	    	success: function(html)
	        	{
	            	//alert(html);
	        		$("#umrahgroup_id").html(html);
	        		$("#umrahgroup_id").trigger("chosen:updated");
	        	}
    	    });
    		
    	});


    
});
</script>
