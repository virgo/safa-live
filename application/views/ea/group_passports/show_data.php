<script type='text/javascript' src='<?= JS ?>/plugins/jquery/jquery-1.9.1.min.js'></script>

<style>
body{
height: auto!important;
overflow: auto !important;
background:none transparent !important;
overflow: hidden;
min-height: 1%;
}

.row-fluid{
padding-top: 5px !important;
padding-bottom: 5px !important;
}
</style>

<div class="row-fluid" >


    <div class="widget" id="windo">
        <div class="modal-header">
             <?= lang('mutamer_data') ?>
        </div>                        
        <div class="modal-body">
            <table id="table" cellpadding="0" cellspacing="0" width="100%">
                <thead>
                </thead>
                <tbody>
                <div class="row-form">
                    <div class="span6">
                        <div class="span4"> <?= lang('group_passports_group_id') ?>:</div>
                        <div class="span8">
                            <?= form_dropdown('safa_group_id', ddgen('safa_groups', array('safa_group_id', 'name')), set_value('safa_group_id',$item->safa_group_id), "  class='chosen input-huge' DISABLED") ?>
                        
                        </div>
                    </div>
                    <div class="span6">
                        <div class="span4"> <?= lang('group_passports_no') ?>:</div>
                        <div class="span8">
                            <?= form_input('no', set_value("no", $item->no), " DISABLED ") ?>
                        </div>
                    </div>
                </div>

                <div class="row-form">
                    <div class="span6">
                        <div class="span4"> <?= lang('group_passports_title_id') ?>:</div>
                        <div class="span8">
                            <?= form_dropdown('title_id', ddgen('erp_titles', array('erp_title_id', name())), set_value('title_id',$item->title_id), "  class='chosen input-huge' DISABLED ") ?>
                        
                        </div>
                    </div>
                    <div class="span6">
                        <div class="span4"> <?= lang('group_passports_first_name_ar') ?>:</div>
                        <div class="span8">
                            <?= form_input('first_name_ar', set_value("first_name_ar", $item->first_name_ar), " DISABLED ") ?>
                        </div>
                    </div>
                </div>  

                <div class="row-form">
                    <div class="span6">
                        <div class="span4"> <?= lang('group_passports_second_name_ar') ?>:</div>
                        <div class="span8">
                            <?= form_input('second_name_ar', set_value("second_name_ar", $item->second_name_ar), " DISABLED ") ?>
                        </div>
                    </div>
                    <div class="span6">
                        <div class="span4"> <?= lang('group_passports_third_name_ar') ?>:</div>
                        <div class="span8">
                            <?= form_input('third_name_ar', set_value("third_name_ar", $item->third_name_ar), " DISABLED ") ?>
                        </div>
                    </div>
                </div> 

                <div class="row-form">
                    <div class="span6">
                        <div class="span4"> <?= lang('group_passports_fourth_name_ar') ?>:</div>
                        <div class="span8">
                            <?= form_input('fourth_name_ar', set_value("fourth_name_ar", $item->fourth_name_ar), " DISABLED") ?>
                        </div>
                    </div>
                    <div class="span6">
                        <div class="span4"> <?= lang('group_passports_first_name_la') ?>:</div>
                        <div class="span8">
                            <?= form_input('first_name_la', set_value("first_name_la", $item->first_name_la), " DISABLED ") ?>
                        </div>
                    </div>
                </div>   

                <div class="row-form">
                    <div class="span6">
                        <div class="span4"> <?= lang('group_passports_second_name_la') ?>:</div>
                        <div class="span8">
                            <?= form_input('second_name_la', set_value("second_name_la", $item->second_name_la), " DISABLED ") ?>
                        </div>
                    </div>
                    <div class="span6">
                        <div class="span4"> <?= lang('group_passports_third_name_la') ?>:</div>
                        <div class="span8">
                            <?= form_input('third_name_la', set_value("third_name_la", $item->third_name_la), " DISABLED ") ?>
                        </div>
                    </div>
                </div>  

                <div class="row-form">
                    <div class="span6">
                        <div class="span4"> <?= lang('group_passports_fourth_name_la') ?>:</div>
                        <div class="span8">
                            <?= form_input('fourth_name_la', set_value("fourth_name_la", $item->fourth_name_la), " DISABLED ") ?>
                        </div>
                    </div>
                    <div class="span6">
                        <div class="span4"> <?= lang('group_passports_nationality_id') ?>:</div>
                        <div class="span8">
                            <?= form_dropdown('nationality_id', ddgen('erp_nationalities', array('erp_nationality_id', name())), set_value('nationality_id',$item->nationality_id), " DISABLED  class='chosen input-huge' ") ?>
                        
                        </div>
                    </div>
                </div>    

                <div class="row-form">
                    <div class="span6">
                        <div class="span4"> <?= lang('group_passports_previous_nationality_id') ?>:</div>
                        <div class="span8">
                            <?= form_dropdown('previous_nationality_id', ddgen('erp_nationalities', array('erp_nationality_id', name())), set_value('previous_nationality_id',$item->previous_nationality_id), " DISABLED  class='chosen input-huge' ") ?>
                        
                        </div>
                    </div>
                    <div class="span6">
                        <div class="span4"> <?= lang('group_passports_passport_no') ?>:</div>
                        <div class="span8">
                            <?= form_input('passport_no', set_value("passport_no", $item->passport_no), " DISABLED ") ?>
                        </div>
                    </div>
                </div>  


                <div class="row-form">
                    <div class="span6">
                        <div class="span4"> <?= lang('group_passports_passport_type_id') ?>:</div>
                        <div class="span8">
                            <?= form_dropdown('passport_type_id', ddgen('erp_passporttypes', array('erp_passporttype_id', name())), set_value('passport_type_id',$item->passport_type_id), " DISABLED class='chosen input-huge' ") ?>
                        
                        </div>
                    </div>
                    <div class="span6">
                        <div class="span4"> <?= lang('group_passports_passport_issue_date') ?>:</div>
                        <div class="span8">
                            <?= form_input('passport_issue_date', set_value("passport_issue_date", $item->passport_issue_date), " DISABLED ") ?>
                        </div>
                    </div>
                </div>

                <div class="row-form">
                    <div class="span6">
                        <div class="span4"> <?= lang('group_passports_passport_expiry_date') ?>:</div>
                        <div class="span8">
                            <?= form_input('passport_expiry_date', set_value("passport_expiry_date", $item->passport_expiry_date), " DISABLED ") ?>
                        </div>
                    </div>
                    <div class="span6">
                        <div class="span4"> <?= lang('group_passports_passport_issuing_city') ?>:</div>
                        <div class="span8">
                            <?= form_input('passport_issuing_city', set_value("passport_issuing_city", $item->passport_issuing_city), " DISABLED ") ?>
                        </div>
                    </div>
                </div>   

                <div class="row-form">
                    <div class="span6">
                        <div class="span4"> <?= lang('group_passports_passport_issuing_country_id') ?>:</div>
                        <div class="span8">
                            <?= form_dropdown('passport_issuing_country_id', ddgen('erp_countries', array('erp_country_id', name())), set_value('passport_issuing_country_id',$item->passport_issuing_country_id), " DISABLED class='chosen input-huge' ") ?>                       
                        
                        </div>
                    </div>
                    <div class="span6">
                        <div class="span4"> <?= lang('group_passports_passport_dpn_count') ?>:</div>
                        <div class="span8">
                            <?= form_input('passport_dpn_count', set_value("passport_dpn_count", $item->passport_dpn_count), " DISABLED ") ?>
                        </div>
                    </div>
                </div>

                <div class="row-form">
                   
                    <div class="span6">
                        <div class="span4"> <?= lang('group_passports_relative_no') ?>:</div>
                        <div class="span8">
                            
                    <?php 
					$this->countries_model->erp_country_id= $item->passport_issuing_country_id;
                    $country_row=$this->countries_model->get();
                    $passport_fullname='full_name_la';
                    if(count($country_row)>0) {
                        if($country_row->arab==1) {
                            $passport_fullname='full_name_ar';
                        }
                    }
                                    
                   $structure = array('safa_group_passport_id', $passport_fullname);
                                          
			       $key = $structure['0'];
			       if (isset($structure['1'])) {
			       		$value = $structure['1'];
			       }
			       $passports_arr=array();
			       $passports_arr['']=lang('global_select_from_menu');
			       $this->safa_group_passports_model->safa_group_id=$item->safa_group_id;
			       $this->safa_group_passports_model->safa_group_passport_id=false;
			       //$this->safa_group_passports_model->limit=false;
			       
			       $passports=$this->safa_group_passports_model->get_for_table();
			       foreach ($passports as $passport) {
			            $passports_arr[$passport->$key] = $passport->$value;
			        }
                    ?>
                    	<?= form_dropdown('relative_no', $passports_arr , set_value('relative_no',$item->relative_no), " DISABLED class='chosen input-huge' ") ?>                       
                        
                        </div>
                    </div>
                    
                    <div class="span6">
                        <div class="span4"> <?= lang('group_passports_relative_relation_id') ?>:</div>
                        <div class="span8">
                            <?= form_dropdown('relative_relation_id', ddgen('erp_relations', array('erp_relation_id', name()),'' ,'' , true), set_value('relative_relation_id',$item->relative_relation_id), " DISABLED class='chosen input-huge' ") ?>                       
                        
                        </div>
                    </div>
                </div>    

                <div class="row-form">
                    <div class="span6">
                        <div class="span4"> <?= lang('group_passports_educational_level_id') ?>:</div>
                        <div class="span8">
                        <?= form_dropdown('educational_level_id', ddgen('erp_educationlevels', array('erp_educationlevel_id', name())), set_value('educational_level_id',$item->educational_level_id), " DISABLED class='chosen input-huge' ") ?>                       
                        
                        </div>
                    </div>
                    <div class="span6">
                        <div class="span4"> <?= lang('group_passports_occupation') ?>:</div>
                        <div class="span8">
                            <?= form_input('occupation', set_value("occupation", $item->occupation), " DISABLED ") ?>
                        </div>
                    </div>
                </div>


                <div class="row-form">
                    <div class="span6">
                        <div class="span4"> <?= lang('group_passports_date_of_birth') ?>:</div>
                        <div class="span8">
                            <?= form_input('date_of_birth', set_value("date_of_birth", $item->date_of_birth), "DISABLED ") ?>
                        </div>
                    </div>
                    <div class="span6">
                        <div class="span4"> <?= lang('group_passports_age') ?>:</div>
                        <div class="span8">
                            <?php 
                    	$date_of_birth = strtotime($item->date_of_birth);
						$date_now = strtotime( date('Y-m-d', time()));
						$secs = $date_now - $date_of_birth;// == return sec in difference
						$days = $secs / 86400 ;
						$age=$days/365;
						//$age=round($age);
						//$age=ceil($age);
						$age=floor($age);
                    	?>
                    	
                        <?= form_input('age', set_value("age", $age), " disabled='disabled' ") ?>
                        </div>
                    </div>
                </div>


                <div class="row-form">
                    <div class="span6">
                        <div class="span4"> <?= lang('group_passports_birth_city') ?>:</div>
                        <div class="span8">
                            <?= form_input('birth_city', set_value("birth_city", $item->birth_city), " DISABLED ") ?>
                        </div>
                    </div>
                    <div class="span6">
                        <div class="span4"> <?= lang('group_passports_birth_country_id') ?>:</div>
                        <div class="span8">
                            <?= form_dropdown('birth_country_id', ddgen('erp_countries', array('erp_country_id', name())), set_value('birth_country_id',$item->birth_country_id), " DISABLED class='chosen input-huge' ") ?>                       
                        
                        </div>
                    </div>
                </div> 

                <div class="row-form">
                    
                    <div class="span6">
                        <div class="span4"> <?= lang('group_passports_dpn_serial_no') ?>:</div>
                        <div class="span8">
                            <?= form_input('dpn_serial_no', set_value("dpn_serial_no", $item->dpn_serial_no), " DISABLED ") ?>
                        </div>
                    </div>
                    
                     <div class="span6">
                        <div class="span4"> <?= lang('group_passports_address') ?>:</div>
                        <div class="span8">
                            <?= form_input('address', set_value("address", $item->address), " DISABLED ") ?>
                        </div>
                    </div>
                    
                    <!-- 
                    <div class="span6">
                        <div class="span4"> <?= lang('group_passports_relative_gender_id') ?>:</div>
                        <div class="span8">
                            <?= form_input('relative_gender_id', set_value("relative_gender_id", $item->relative_gender_id), " DISABLED ") ?>
                        </div>
                    </div>
                     -->
                    
                </div>  

                <div class="row-form">
                    <div class="span6">
                        <div class="span4"> <?= lang('group_passports_gender_id') ?>:</div>
                        <div class="span8">
                            <?= form_dropdown('gender_id', ddgen('erp_gender', array('erp_gender_id', name())), set_value('gender_id',$item->gender_id), " DISABLED class='chosen input-huge' ") ?>                       
                        
                        </div>
                    </div>
                    <div class="span6">
                        <div class="span4"> <?= lang('group_passports_marital_status_id') ?>:</div>
                        <div class="span8">
                            <?= form_dropdown('marital_status_id', ddgen('erp_maritalstatus', array('erp_maritalstatus_id', name())), set_value('marital_status_id',$item->marital_status_id), " DISABLED class='chosen input-huge' ") ?>                       
                        
                        </div>
                    </div>
                </div>   

                <div class="row-form">
                
                <div class="span6">
                        <div class="span4"> <?= lang('group_passports_erp_country_id') ?>:</div>
                        <div class="span8">
                            <?= form_dropdown('erp_country_id', ddgen('erp_countries', array('erp_country_id', name())), set_value('erp_country_id',$item->erp_country_id), " DISABLED class='chosen input-huge' ") ?>                       
                        
                        </div>
                    </div>
                    
                    <div class="span6">
                        <div class="span4"> <?= lang('group_passports_city') ?>:</div>
                        <div class="span8">
                            <?= form_input('city', set_value("city", $item->city), " DISABLED ") ?>
                        </div>
                    </div>
                    
                </div>     

                </tbody>
            </table>


        </div>
    </div>
</div>



