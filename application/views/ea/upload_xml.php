<!-- By Gouda, For File Input -->
<script type="text/javascript" src='<?= JS ?>/plugins/cookies/jquery.cookies.2.2.0.min.js'></script>
<script type="text/javascript" src='<?= JS ?>/plugins/hoverintent/jquery.hoverIntent.minified.js'></script>    
<script type="text/javascript" src='<?= JS ?>/actions.js'></script>
    

<div class="widget">
    <div class="path-container Fright">
    	<div class="path-name Fright">
            <a href="<?php echo  site_url().'ea/dashboard'; 
            
            ?>"><?php echo  lang('global_system_management') ?></a>
        </div>
        <div class="path-arrow Fright"></div>
        <div class="path-name Fright">  <?= lang('read_xml_title') ?></div>
    </div>
</div>


<div class="row-fluid">
    <div class="widget">
       
        
        <div class="widget-header">
        <div class="widget-header-icon Fright"> <span class="icos-pencil2"></span> </div>
        <div class="widget-header-title Fright">
            <?php echo  lang('read_xml_upload_file') ?>
        </div>
    	</div>
        
        <?= form_open_multipart('',"id='frm_upload_xml'") ?>
        <div class="block-fluid">
            <div class="row-form" >
                <div class="span6" >
                    <div class="span4" ><?= lang('read_xml_file') ?>
                        <font style='color:red'>*</font>
                    </div>
                    <div class="span8">
                        <div class="input-append file"  >
                        <!-- 
                            <input type="file" value="<?= set_value('userfile') ?>" name="userfile"/>
        				 -->                    
        					<?= form_upload('userfile', set_value('userfile'), "") ?>
                            <input type="text" name="text_input"   value="" class="validate[required]"/>
                            <button type="button"  class="btn"><?= lang('upload_xml_browse') ?></button>
                        </div>
                        <span class="bottom" style='color:red' >
                            <?= form_error('userfile') ?> 
                        </span>
                    </div>  
                </div>
                <div class="span6">
                    <div class="span4"><?= lang('file_type') ?>
                        <span style="color: red">*</span>:
                    </div>
                    <div class="span8">
                        <?= form_dropdown('file_type', array("" => 'اختر من القائمة', "1" => lang('gama'), "2" => lang('bab_umrah'), "3" => lang('road_umrah'), "4" => lang('safa_file')), set_value('file_type'), " name='s_example' id='file_type' class='validate[required]' style='width:100%;direction:rtl' ") ?>
                        <?= form_error('file_type', '<div class="bottom" style="color:red" >', '</div>'); ?>
                    </div>
                </div>
            </div>
            <div class="row-form">
<!--                <div class="span6">
                    <div class="span4"><?//= lang('writeFile_type') ?>
                        <span style="color: red">*</span>:
                    </div>
                    <div class="span8">
                        <?//= form_dropdown('writeFile_type', array("0" => 'حفظ فى قاعدة البيانات ', "1" => lang('gama'), "2" => lang('bab_umrah'), "3" => lang('road_umrah')), set_value('writeFile_type', 0), "  name='s_example'  style='width:100%;direction:rtl' ") ?>
                        <?//= form_error('writeFile_type', '<div class="bottom" style="color:red" >', '</div>'); ?>
                    </div>
                </div>-->
                <div class="span6">
                    <div class="span4"><?= lang('group_name') ?>
                    <span style="color: red">*</span>:
                    </div>
                    <div class="span8">
                        <?= form_dropdown('safa_group_id', ddgen('safa_groups', array('safa_group_id', 'name'), array('safa_ea_id' => session('ea_id'))), set_value('safa_group_id'), " id='safa_group_id' name='s_example' class='validate[required]' style='width:100%;' ") ?>
                        <?= form_error('safa_group_id', '<div class="bottom" style="color:red" >', '</div>'); ?>
                    </div>
                </div>
            </div>
        </div>

        <div class="toolbar bottom TAC">
            <input type="submit"  class="btn btn-primary" name="submit" value="<?= lang('global_submit') ?>" />

        </div>
        <?= form_close() ?>
    </div>
</div>

<script>
    $(document).ready(function() {
        /* input file */
        $(".file .btn, .file input:text").click(function() {
            var block = $(this).parent('.file');

            block.find('input:file').change(function() {
                var file_arr = $(this).val().split('\\');
                var file_name = file_arr[file_arr.length - 1];
                block.find('input:text').val(file_name);
                if (file_name == 'gama.XML' || file_name == 'جاما.XML') {

                    $('#file_type').val(1);
                }
                else if (file_name == 'bab_umrah.XML' || file_name == 'باب العمرة.XML') {
                    $('#file_type').val(2);
                }
                else if (file_name == 'طريق  العمرة.txt') {
                    $('#file_type').val(3);
                }
            });
        });
    });
</script>

<script type="text/javascript">

    // binds form submission and fields to the validation engine
    $("#frm_upload_xml").validationEngine({
            prettySelect : true,
            useSuffix: "_chosen",
            promptPosition : "topRight:-150"
            //promptPosition : "bottomLeft"
    });

    $("#safa_group_id").chosen();



</script>