<style>
    .widget {width:98%}
</style>
<div class="widget">
    
    <div class="path-container Fright">
    	<div class="path-name Fright">
            <a href="<?php echo  site_url() ?>"><?php echo  lang('global_system_management') ?></a>
        </div>
        <div class="path-arrow Fright"></div>
        <div class="path-name Fright"> <a href="<?php echo  site_url('ea/supervisors') ?>"><?php echo  lang('title') ?></a></div>
        
        <div class="path-arrow Fright"></div>
        <div class="path-name Fright"><?php echo  lang('add_title') ?></div>
    
    </div>
</div>


<?php echo  form_open_multipart(false, 'id="frm_supervisors" ') ?>


<div class="widget">
    <div class="widget-header">

        <div class="widget-header-icon Fright">
            <span class="icos-pencil2"></span>
        </div>

        <div class="widget-header-title Fright">
            <?php echo  lang('main_data') ?>
            
        </div>
    </div>

    <div class="widget-container">  
        <? if(validation_errors()){ ?>
            <?php echo validation_errors(); ?>        
        <? } ?>
        
        <div class="row-form">
            <div class="span6">
                <div class="span4 TAL Pleft10">
                    <?php echo  lang('name_ar') ?>
                </div>
                <div class="span8">
                    <?php echo form_input('name_ar', set_value("name_ar")," style='' id='name_ar' class='validate[required] input-full' ") ?>
                </div>
			</div>
        
            <div class="span6">
                <div class="span4 TAL Pleft10">
                    <?php echo  lang('name_la') ?>
                </div>
                <div class="span8">
                    <?php echo form_input('name_la', set_value("name_la")," style='' id='name_la' class='validate[required] input-full' ") ?>
                </div>
			</div>
        
        </div>
        
        <div class="row-form">
            <div class="span6">
                <div class="span4 TAL Pleft10">
                    <?php echo  lang('phone') ?>
                </div>
                <div class="span8">
                    <?php echo form_input('phone', set_value("phone")," style='' id='phone' class='validate[custom[phone]] input-full' ") ?>
                </div>
			</div>
        
            <div class="span6">
                <div class="span4 TAL Pleft10">
                    <?php echo  lang('mobile') ?>
                </div>
                <div class="span8">
                    <?php echo form_input('mobile', set_value("mobile")," style='' id='mobile' class='validate[required, custom[phone]] input-full' ") ?>
                </div>
			</div>
        
        </div>
        
        <div class="row-form">
            <div class="span6">
                <div class="span4 TAL Pleft10">
                    <?php echo  lang('mobile_sa') ?>
                </div>
                <div class="span8">
                    <?php echo form_input('mobile_sa', set_value("mobile_sa")," style='' id='mobile_sa' class='validate[custom[phone]] input-full' ") ?>
                </div>
			</div>
        
           
        </div>
        
        <div class="row-form">
           
            <div class="span6">
                <div class="span4 TAL Pleft10">
                    <?php echo  lang('notes') ?>
                </div>
                <div class="span8">
                    <?php echo form_textarea('notes', set_value("notes")," style='' id='notes' class='input-full' ") ?>
                </div>
			</div>
        
        </div>
        
        
       
                
    </div>
</div>



<div class="widget TAC">
    <input type="submit" class="btn" name="smt_send" value="<?php echo lang('global_submit') ?>" style="margin:10px;padding: 5px;height: auto">
</div>


<?php echo  form_close() ?>


<div class="footer"></div>


<script type="text/javascript">
$(document).ready(function() {
    var config = {
        '.chosen-select': {width: "100%"},
        '.chosen-select-deselect': {allow_single_deselect: true},
        '.chosen-select-no-single': {disable_search_threshold: 10},
        '.chosen-select-no-results': {no_results_text: 'Oops, nothing found!'},
        '.chosen-select-width': {width: "95%"}
    }
    for (var selector in config) {
        $(selector).chosen(config[selector]);
    }


    // binds form submission and fields to the validation engine
    $("#frm_supervisors").validationEngine({
            prettySelect : true,
            useSuffix: "_chosen",
            promptPosition : "topRight:-150"
            //promptPosition : "bottomLeft"
    });

 
});


</script>
