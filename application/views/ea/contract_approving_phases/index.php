<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); ?>
<link href="<?= CSS ?>/stylesheets.css" rel="stylesheet" type="text/css" />
<? if (lang('global_lang') == 'ar'): ?>
    <link href="<?= CSS ?>/stylesheet-ar.css" rel="stylesheet" type="text/css" />
<? else: ?><? endif ?>
<script type="text/javascript" src='<?= JS ?>/plugins/jquery/jquery-1.9.1.min.js'></script>
<script type="text/javascript" src='<?= JS ?>/plugins/jquery/jquery-ui-1.10.1.custom.min.js'></script>
<script type="text/javascript" src='<?= JS ?>/plugins/bootstrap/bootstrap.min.js'></script>
<link href="<?= CSS_JS ?>/dateandtime/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src='<?= CSS_JS ?>/dateandtime/bootstrap-datetimepicker.min.js'></script>
<style>
.widget{width:98%;}

    body{
height: auto!important;
overflow: auto !important;
background:none transparent !important;
overflow: hidden;
min-height: 1%;
}

.row-fluid{
padding-top: 5px !important;
padding-bottom: 5px !important;
}
</style>


<?php echo  form_open_multipart(false, 'id="frm_contract_approving_phases" ') ?>


<div class="">
    

    <div class="modal-header">  
        <? if(validation_errors()){ ?>
            <?php echo validation_errors(); ?>        
        <? } ?>
       
        </div>
        
        
         <div class="modal-body">
         
          <!-- 
        <div class="row-form">
            <div class="span6">
                <div class="span4 TAL Pleft10">
                    <?php echo  lang('contract_code') ?>
                </div>
                <div class="span8">
                    <?php echo form_input('contract_code', set_value("contract_code", '')," style='' id='contract_code' class='validate[required] input-full' ") ?>
                </div>
			</div>
        </div>
         -->
    
        
        <div class="row-fluid">
            <div class='span6'>
                <div class="span4 TAL Pleft10">
                    <?php echo  lang('contract_name') ?>
                </div>
                <div class="span8">
                    <?php echo form_input('contract_name', set_value("contract_name", $item->{name()})," style='' id='contract_name' class=' input-full' readonly='readonly' ") ?>
                </div>
            </div>	
        </div>
         
         
        <div class="row-fluid">
            <div class="span6">
                <div class="span4 TAL Pleft10">
                    <?php echo  lang('current_phase') ?>
                </div>
                <div class="span8">
                <?php 
                if(name()=='name_ar') {
                	$safa_contract_phases_name_current=$item->safa_contract_phases_name_ar_current;
                } else {
                	$safa_contract_phases_name_current=$item->safa_contract_phases_name_la_current;
                }
                ?>
                    <?php echo form_input('current_phase', set_value("current_phase", $safa_contract_phases_name_current)," style='' id='current_phase' class=' input-full'  readonly='readonly' ") ?>
                </div>
			</div>
               <div class="span6">
                <div class="span4 TAL Pleft10">
                    <?php echo  lang('contracts_status') ?>
                </div>
                <div class="span8">
                <?php 
                if(name()=='name_ar') {
                	$safa_uo_contracts_status_name=$item->safa_uo_contracts_status_name_ar;
                } else {
                	$safa_uo_contracts_status_name=$item->safa_uo_contracts_status_name_la;
                }
                ?>
                    <?php echo form_input('contracts_status', set_value("contracts_status", $safa_uo_contracts_status_name)," style='' id='contracts_status' class=' input-full'  readonly='readonly' ") ?>
                </div>
			</div>
        </div>
        
        
       
        
   		<div class="row-fluid">
            <div class="span6">
                <div class="span4 TAL Pleft10">
                    <?php echo  lang('next_phase') ?>
                </div>
                <div class="span8">
                    
                    <?php 
	                if(name()=='name_ar') {
	                	$safa_contract_phases_name_next=$item->safa_contract_phases_name_ar_next;
	                } else {
	                	$safa_contract_phases_name_next=$item->safa_contract_phases_name_la_next;
	                }
	                ?>
                    <?php echo form_input('next_phase', set_value("next_phase", $safa_contract_phases_name_next)," style='' id='next_phase' class=' input-full'  readonly='readonly' ") ?>
               
                </div>
			</div>
        </div>
        
        
        <div class="row-fluid">
            <div class="span12">
                <div class="span4 TAL Pleft10">
                    <?php echo  lang('required_documents') ?>
                </div>
                <div class="span11">


<div class="">
        <div class="" >
            <table cellpadding="0" cellspacing="0">
                <thead>
                    <tr>
                    
                    <?php
                    
                    $all_ea_documents_new=true;
                    if(isset($required_documents)) {
	                    if(ensure($required_documents)) { 
						
		                	foreach($required_documents as $required_document) {
		                    
		                    	$safa_contract_approving_phases_id = $required_document->safa_contract_approving_phases_id;
		                        $safa_contract_phases_documents_id = $required_document->safa_contract_phases_documents_id;
		                    	$refuse_reason= $required_document->refuse_reason;
		                    	$safa_contract_approving_phases_status_id= $required_document->safa_contract_approving_phases_status_id;
		                    	
			                	if($required_document->safa_contract_approving_phases_status_id==2 || $required_document->safa_contract_approving_phases_status_id==3) {
				        			$all_ea_documents_new=false;
				        		}
	                		}
                    	}
                    }
                    ?>
                    
                        <th><?php echo  lang('document_name') ?></th>
                        <th><?php echo  lang('upload_file') ?></th>
                        
                        <?php 
                        if(!$all_ea_documents_new) {
                        ?>
                        
                        <th><?php echo  lang('status') ?></th>
                        <th><?php echo  lang('refuse_reason') ?></th>
                        
                        <?php 
                        }
                        ?>
                        
                    </tr>
                </thead>
                <tbody id="had">
                <? if(ensure($required_documents)) { 
                	foreach($required_documents as $required_document) {
                    
                    	$safa_contract_approving_phases_id = $required_document->safa_contract_approving_phases_id;
                        $safa_contract_phases_documents_id = $required_document->safa_contract_phases_documents_id;
                    	$refuse_reason= $required_document->refuse_reason;
                    	$safa_contract_approving_phases_status_id= $required_document->safa_contract_approving_phases_status_id;
                    
                    ?>
                    <tr rel="<?php echo  $safa_contract_approving_phases_id; ?>">

                        <td>
                            <?php echo   $required_document->{name()} ; ?>
                        </td>
                        <td>
                          <?php 
                          $safa_contract_approving_phases_id = $required_document->safa_contract_approving_phases_id;
                          $safa_contract_phases_documents_id = $required_document->safa_contract_phases_documents_id;
                          if($required_document->document_path!='') {
                          	$document_path=$required_document->document_path;
	                          $document_file_name_arr=explode('/', $document_path);
	                          $document_file_name=end($document_file_name_arr);
	                          
	                          if($required_document->safa_contract_approving_phases_status_id==2) {
	                          	echo "<a target='_blank' href='".base_url().$document_path."'>$document_file_name</a>";
	                          } else {
	                          	echo "<a target='_blank' href='".base_url().$document_path."'>$document_file_name</a> "." "." <a href='".site_url("ea/contract_approving_phases/removeFile/$safa_contract_approving_phases_id")."'>".lang('remove_file')."</a>";
	                          }
	                          	
                          } else {
                          	 echo form_upload("document_file_$safa_contract_phases_documents_id", "","  style='' id='document_file_$safa_contract_phases_documents_id'  ") ;
                          }
                          
                          ?> 
                        </td>
                        <?php 
                        if(!$all_ea_documents_new) {
                        ?>
                        <td>
                            <?php 
                            $this->db->where('safa_contract_approving_phases_status.safa_contract_approving_phases_status_id',$safa_contract_approving_phases_status_id);
                            $safa_contract_approving_phases_status_row=$this->db->get('safa_contract_approving_phases_status')->row();
                            if(count($safa_contract_approving_phases_status_row)>0) {
                            	$safa_contract_approving_phases_status_name=$safa_contract_approving_phases_status_row->{name()};
                            	echo $safa_contract_approving_phases_status_name; 
                            }
                            ?>
                        </td>
                        <td>
                            <?php echo $refuse_reason; ?>
                        </td>
                        <?php 
                        }
                        ?>
                    </tr>
                    <? } ?>
                <? } ?> 
                </tbody>
            </table>
    </div>
</div>
    
    
       
                </div>
                
			</div>
        </div>
        
                
    </div>



<div class="toolbar bottom TAL">
<div class=" ">
    <input type="submit" class="btn" name="smt_save" value="<?php echo lang('save') ?>" style="margin:10px;padding: 5px;height: auto">
</div>
</div>
</div>
<?php echo  form_close() ?>



</div>
</div>
</div>
<script type="text/javascript">
$(document).ready(function() {
    var config = {
        '.chosen-select': {width: "100%"},
        '.chosen-select-deselect': {allow_single_deselect: true},
        '.chosen-select-no-single': {disable_search_threshold: 10},
        '.chosen-select-no-results': {no_results_text: 'Oops, nothing found!'},
        '.chosen-select-width': {width: "95%"}
    }
    for (var selector in config) {
        $(selector).chosen(config[selector]);
    }


    // binds form submission and fields to the validation engine
    $("#frm_contract_approving_phases").validationEngine({
            prettySelect : true,
            useSuffix: "_chosen",
            promptPosition : "topRight:-150"
            //promptPosition : "bottomLeft"
    });

 
});


</script>

<script type="text/javascript">
    

	
    $('#next_phase').change(function()
     {
        var next_phase_id=$('#next_phase').val();
            
        var dataString = 'next_phase_id='+ next_phase_id;
            
            $.ajax
            ({
            type: 'POST',
            url: '<?php echo base_url().'ea/contract_approving_phases/getPhaseDocuments'; ?>',
            data: dataString,
            cache: false,
            success: function(html)
            {
                //alert(html);
            $("#required_documents").html(html);
            $("#required_documents").trigger("chosen:updated");
            }
            });
            
     });


    $("#remove_all_required_documents_lnk").click(function(){
    	$("#required_documents").val([]);
    	$("#required_documents").trigger("chosen:updated");
	});
    
</script>