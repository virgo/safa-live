<script>.widget{width:98%;}</script>

<div class="widget">
    
    <div class="path-container Fright">
    	<div class="path-name Fright">
            <a href="<?php echo  site_url() ?>"><?php echo  lang('global_system_management') ?></a>
        </div>
        <div class="path-arrow Fright"></div>
        <div class="path-name Fright"> <?php echo  lang('menu_title') ?></div>
    </div>
</div>

<div class="widget">
    <div class="widget-header">
        <div class="widget-header-icon Fright"> <span class="icos-pencil2"></span> </div>
        <div class="widget-header-title Fright">
            <?php echo  lang('title') ?>
        </div>
        
        <?php 
        if($total_rows==0) {
        ?>
        <a class="btn Fleft fancybox fancybox.iframe" href="<?php echo  site_url('ea/contract_approving_phases/add') ?>">
      	<?php echo  lang('global_add') ?>
    	</a>
    
	    <?php 
	        }
	    ?>
    
    </div>
     <div class="widget-container">
          <div class="table-responsive">
            <table cellpadding="0" cellspacing="0" class="fpTable" >
                <thead>
                    <tr>
                        <th><?php echo  lang('name_ar') ?></th>
                        <th><?php echo  lang('name_la') ?></th>
                        <th><?php echo  lang('contract_name') ?></th>
                        <th><?php echo  lang('actions') ?></th>
                    </tr>
                </thead>
                <tbody id="had">
                <? if(ensure($items)) { ?>
                    <? foreach($items as $item) {?>
                    <tr rel="<?php echo  $item->safa_uo_contract_id ?>">

                        <td>
                            <?php echo   $item->safa_uo_contracts_eas_name_ar ; ?>
                        </td>
                        <td>
                            <?php echo   $item->safa_uo_contracts_eas_name_la ; ?>
                        </td>
                        <td>
                            <?php echo   $item->safa_uos_name ; ?>
                        </td>
                        <td class="TAC">
                        <a class='fancybox fancybox.iframe' id='' href="<?php echo site_url('ea/contract_approving_phases/index/' . $item->safa_uo_contract_id) ?>"><span class="icon-file"></span></a>
                        <a href="<?= site_url('ea/contract_approving_phases/delete/' . $item->safa_uo_contract_id) ?>" onclick="return confirm('<?= lang('global_are_you_sure_you_want_to_delete')?>')"><span class="icon-trash"></span></a>
                        <a href="<?= site_url("ea/contracts/edit") ?>/<?= $item->safa_uo_contract_id ?>"><span class="icon-pencil"></span></a> 
                        
                        </td>
                    </tr>
                    <? } ?>
                <? } ?> 
                </tbody>
            </table>
        </div>
    </div>
</div>


<script>
$(document).ready(function() {
    $('.fancybox').fancybox({
        afterClose: function() {
            location.reload();
        }
    });
});

</script>