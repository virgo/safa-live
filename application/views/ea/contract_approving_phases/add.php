<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); ?>
<link href="<?= CSS ?>/stylesheets.css" rel="stylesheet" type="text/css" />
<? if (lang('global_lang') == 'ar'): ?>
    <link href="<?= CSS ?>/stylesheet-ar.css" rel="stylesheet" type="text/css" />
<? else: ?><? endif ?>
<script type="text/javascript" src='<?= JS ?>/plugins/jquery/jquery-1.9.1.min.js'></script>
<script type="text/javascript" src='<?= JS ?>/plugins/jquery/jquery-ui-1.10.1.custom.min.js'></script>
<script type="text/javascript" src='<?= JS ?>/plugins/bootstrap/bootstrap.min.js'></script>
<link href="<?= CSS_JS ?>/dateandtime/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src='<?= CSS_JS ?>/dateandtime/bootstrap-datetimepicker.min.js'></script>
<style>
.widget{width:98%;}

body{
height: auto!important;
overflow: auto !important;
background:none transparent !important;
overflow: hidden;
min-height: 1%;
}

.row-fluid{
padding-top: 5px !important;
padding-bottom: 5px !important;
}
</style>


<?php echo  form_open_multipart(false, 'id="frm_contract_approving_phases" ') ?>


<div class="">
    

    <div class="modal-body">  
        <? if(validation_errors()){ ?>
            <?php echo validation_errors(); ?>        
        <? } ?>
       
        <div class="row-fluid">
            <div class="span6">
                <div class="span4 TAL Pleft10">
                    <?php echo  lang('username') ?>
                </div>
                <div class="span8">
                    <?php echo form_input('username', set_value("username", '')," style='' id='username' class='validate[required] input-full' ") ?>
                </div>
			</div>
        </div>
        
        <div class="row-fluid">
            <div class="span6">
                <div class="span4 TAL Pleft10">
                    <?php echo  lang('password') ?>
                </div>
                <div class="span8">
                    <?php echo form_password('password', set_value("password", '')," style='' id='password' class='validate[required] input-full' ") ?>
                </div>
			</div>
        </div>
        
        
        <div class="row-fluid">
            <div class="span6">
                <div class="span4 TAL Pleft10">
                    <?php echo  lang('name_ar') ?>
                </div>
                <div class="span8">
                    <?php echo form_input('name_ar', set_value("name_ar", '')," style='' id='name_ar' class='validate[required] input-full' ") ?>
                </div>
			</div>
        </div>
        <div class="row-fluid">
            <div class="span6">
                <div class="span4 TAL Pleft10">
                    <?php echo  lang('name_la') ?>
                </div>
                <div class="span8">
                    <?php echo form_input('name_la', set_value("name_la", '')," style='' id='name_la' class='validate[required] input-full' ") ?>
                </div>
			</div>
        </div>
        
                
    </div>
</div>



<div class="toolbar bottom TAL">
    <input type="submit" class="btn" name="smt_save" value="<?php echo lang('save') ?>" style="margin:10px;padding: 5px;height: auto">
</div>


<?php echo  form_close() ?>


<div class="footer"></div>



<script type="text/javascript">
$(document).ready(function() {
    var config = {
        '.chosen-select': {width: "100%"},
        '.chosen-select-deselect': {allow_single_deselect: true},
        '.chosen-select-no-single': {disable_search_threshold: 10},
        '.chosen-select-no-results': {no_results_text: 'Oops, nothing found!'},
        '.chosen-select-width': {width: "95%"}
    }
    for (var selector in config) {
        $(selector).chosen(config[selector]);
    }


    // binds form submission and fields to the validation engine
    $("#frm_contract_approving_phases").validationEngine({
            prettySelect : true,
            useSuffix: "_chosen",
            promptPosition : "topRight:-150"
            //promptPosition : "bottomLeft"
    });

 
});


</script>
