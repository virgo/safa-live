
<style>
    .widget {width:98%}
</style>
<div class="widget">
    <div class="path-container Fright">
        <div class="path-name Fright">
            <a href=""><?= lang('global_system_management') ?></a>
        </div>
        <div class="path-arrow Fright">
        </div>
        <div class="path-name Fright">
            <?= lang('menu_main_ports') ?>
        </div>
    </div>
</div>



<div class="widget">
    <div class="widget-header">
        <div class="widget-header-icon Fright">
            <span class="icos-pencil2"></span>
        </div>
        <div class="widget-header-title Fright"><?= lang('global_search') ?></div>
<!--        <a href="#" class="show_hide Fleft"><img src="<?= IMAGES ?>/collapse-arrow.png" width="19" height="19" /></a>-->

    </div>


    <div class="widget-container slidingDiv">
        <lable><?= form_open('ea/ea_ports/index', 'method="get"') ?></lable>
        
        <div class="row-form ">
            <div class="span6">
                <div class="span4 TAL Pleft10"><?= lang('ea_ports_country_name') ?>:</div>
                <div class="span5 input-huge">
                    <?= form_dropdown('erp_country_id', ddgen('erp_countries', array('erp_country_id', name())), set_value('erp_country_id'), " name='s_example' class='select input-huge' style='width:90%'") ?>
                    <?= form_error('erp_country_id'); ?>
                </div>
                <div class="span1 TAC">
                <div class="toolbar bottom TAC"><input type="submit" name="search" value="<?= lang('global_search') ?>" class="btn btn-primary" /></div>
            </div>
            
            </div>
        </div>

    </div>


</div>

<div class="widget">
    <div class="widget-header">

        <div class="widget-header-icon Fright">
            <span class="icos-pencil2"></span>
        </div>

        <div class="widget-header-title Fright">
            <?= lang('menu_main_ports') ?>
        </div>

    </div>

    <div class="widget-container">
        <div class=" table-responsive">
            <table cellpadding="0" class="Table " cellspacing="0" >
                <thead>
                    <tr>
                        <th><?= lang('ea_ports_th_name') ?></th>
                        <th><?= lang('ea_ports_th_country_name') ?></th>
                        <th><?= lang('ea_ports_code') ?></th>
                        <th><?= lang('global_actions') ?></th>
                    </tr>
                </thead>
                <tbody>
                    <? if (isset($items)): ?>
                        <? foreach ($items as $item): ?>
                            <tr id="row_<?php echo $item->erp_port_id; ?>">
                                <td><?= $item->{name()} ?></td>
                                <td><?= $item->country_name ?></td>
                                <td><?= $item->code ?></td>

                                <td align="center">
                                    <? if ($this->ports_model->check_ea_ports($item->erp_port_id, session('ea_id')) > 0): ?>
                                        <a  style="color:red;text-decoration: none"  safa_ea_id="<?= session('ea_id') ?>" name="<?= $item->name_ar ?>" id="<?= $item->erp_port_id ?>"  onclick="remove_item(this)"  href="javascript:void(0)"><?= lang('global_delete') ?></a>
                                    <? else : ?>
                                        <a  style="text-decoration: none" safa_ea_id="<?= session('ea_id') ?>" name="<?= $item->name_ar ?>" id="<?= $item->erp_port_id ?>" onclick="add_item(this)" href="javascript:void(0)"><?= lang('global_add') ?></a>
                                    <? endif; ?>

                                </td>
                            </tr>

                        <? endforeach; ?>
                    <? endif; ?>
                </tbody>
            </table>
        </div>


 <?= $pagination ?>

    </div>
</div>

<script>
                                function remove_item(btn) {
                                    var name = $(btn).attr('name');
                                    var answer = confirm("<?= lang('global_are_you_sure_you_want_to_delete') ?>" + " " + name);
                                    if (answer === true) {
                                        var erp_port_id = $(btn).attr('id');
                                        var safa_ea_id = $(btn).attr('safa_ea_id');
                                        $.ajax({
                                            type: "POST",
                                            dataType: 'json',
                                            url: "<?php echo site_url('ea/ea_ports/delete_port'); ?>",
                                            data: {'erp_port_id': $(btn).attr('id'), 'safa_ea_id': $(btn).attr('safa_ea_id')},
                                            success: function(msg) {
                                                if (msg.response === true) {
                                                    var del = erp_port_id;
<? if (lang('global_lang') == 'ar'): ?>
                                                        notify("<?= lang('global_deleted_message') ?>", name);
<? else: ?>
                                                        notify("<?= lang('global_deleted_message') ?>", name);
<? endif; ?>
                                                    $("#" + erp_port_id).html('<?= lang('global_add') ?>');
                                                    $("#" + erp_port_id).attr('onclick', 'add_item(this)');
                                                    $("#" + erp_port_id).removeAttr('style');
                                                } else if (msg.response === false) {
                                                    alert("error");
                                                }
                                            }
                                        });
                                    } else {
                                        return FALSE;
                                    }
                                }
                                function add_item(btn) {
                                    var id = $(btn).attr('id');
                                    var name = $(btn).attr('name');
                                    var answer = confirm("<?= lang('global_are_you_sure_you_want_to_add') ?>" + " " + name);
                                    if (answer === true) {
                                        var erp_port_id = $(btn).attr('id');
                                        var safa_ea_id = $(btn).attr('safa_ea_id');
                                        $.ajax({
                                            type: "POST",
                                            dataType: 'json',
                                            url: "<?php echo site_url('ea/ea_ports/add_port'); ?>",
                                            data: {'erp_port_id': $(btn).attr('id'), 'safa_ea_id': $(btn).attr('safa_ea_id')},
                                            success: function(msg) {
                                                if (msg.response === true) {
<? if (lang('global_lang') == 'ar'): ?>
                                                        notify("<?= lang('global_added_successfully') ?>", name);
<? else: ?>
                                                        notify("<?= lang('global_added_successfully') ?>", name);
<? endif; ?>
                                                    $("#" + erp_port_id).html('<?= lang('global_delete') ?>');
                                                    $("#" + erp_port_id).attr('onclick', 'remove_item(this)');
                                                    $("#" + erp_port_id).css({'color': 'red'});
                                                    $("#" + erp_port_id).attr('onclick', 'remove_item(this)');

                                                } else if (msg.response === false) {
                                                    alert("error");
                                                }
                                            }
                                        });
                                    }
                                }
</script>