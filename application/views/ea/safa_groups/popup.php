<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); ?>
<link href="<?= CSS ?>/stylesheets.css" rel="stylesheet" type="text/css" />
<? if (lang('global_lang') == 'ar'): ?>
    <link href="<?= CSS ?>/stylesheet-ar.css" rel="stylesheet" type="text/css" />
<? else: ?><? endif ?>
<script type="text/javascript" src='<?= JS ?>/plugins/jquery/jquery-1.9.1.min.js'></script>
<script type="text/javascript" src='<?= JS ?>/plugins/jquery/jquery-ui-1.10.1.custom.min.js'></script>
<script type="text/javascript" src='<?= JS ?>/plugins/bootstrap/bootstrap.min.js'></script>
<link href="<?= CSS_JS ?>/dateandtime/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src='<?= CSS_JS ?>/dateandtime/bootstrap-datetimepicker.min.js'></script>
<style>

body{
height: auto!important;
overflow: auto !important;
background:none transparent !important;
overflow: hidden;
min-height: 1%;
width: 96%;
}

.row-fluid{
padding-top: 5px !important;
padding-bottom: 5px !important;
}
.modal-body{
    max-height: 800px;
}
</style>


    <div class="">
        <style>
            .updated_msg{
                display:none;
                background-color:#ccee97;
                font-weight: bold;
                text-align: center;
                border:3px solid #cccdc9; 
                padding:20px;  
                margin-bottom:10px;
                border-radius:15px;
                margin:10px; 
            }
            
           
        </style>
        
        
        <div class="modal-header">
            <?php echo  lang('global_search') ?>
    	</div>
                             
        <div class="modal-body">
            <?= form_open('ea/safa_groups/popup', 'method="get"') ?>
            
            <div class="row-fluid">
                <div class="widget-header"><a><?php echo  lang('safa_groups_title') ?> </a></div>
                <div class="row-fluid">
                
                <div class="row-fluid" >
                    <div class="span12"><?= lang("safa_groups_creation_date") ?></div>
                    <div class="span12">
                        <div class="span6">
                        
                        <div class="span1">
                        <?= lang("safa_groups_from_date") ?>
                    </div>
                        
                    <div class="span6"> 
                    <!-- 
                        <div id="datetimepicker1" class="input-append date">
                     -->    
                        <div id="" class="input-append date" >
                            <input class="from_creation_date" data-format="yyyy-MM-dd" name="from_creation_date" id="from_creation_date" type="text" value="<?= set_value('from_creation_date') ?>" style=" width: 150px;"></input>
                            <script>
		                        $('.from_creation_date').datepicker({
		                            dateFormat: "yy-mm-dd",
		                            controlType: 'select',
		                            timeFormat: 'HH:mm'
		                        });
		                    </script>
                            <!-- 
                            <span class="add-on">
                                <i data-time-icon="icon-time" data-date-icon="icon-calendar">
                                </i>
                            </span>
                             -->
                            
                        </div>
                        <span class="" style='color:red' >
                            <?= form_error('from_creation_date') ?> 
                        </span>
                        
                    </div>
                     </div>   
                    <div class="span6">
                        <div class="span1">
                        <?= lang("safa_groups_to_date") ?>
                    	</div>
                        <!-- <div id="datetimepicker2" class="input-append date">-->
                        <div id="" class="input-append date span6">
                            <input class="to_creation_date" data-format="yyyy-MM-dd" name="to_creation_date" id="to_creation_date" type="text" value="<?= set_value('to_creation_date') ?>" ></input>
                            
                            <script>
		                        $('.to_creation_date').datepicker({
		                            dateFormat: "yy-mm-dd",
		                            controlType: 'select',
		                            timeFormat: 'HH:mm'
		                        });
		                    </script>
                            <!-- 
                            <span class="add-on">
                                <i data-time-icon="icon-time" data-date-icon="icon-calendar">
                                </i>
                            </span>
                             -->
                        </div>
                        <span  >
                            <?= form_error('to_creation_date') ?> 
                        </span>
                    </div>
                    </div>
                    
                    
                    
                        
                        
                        
                    </div>
               
                <div class="row-fluid" >
                    <div class="span12">
                        <?= lang("safa_groups_no_of_mutamers") ?>:
                    </div>
                <div class="span12"> 
                       
                        
                 <div class="span6" >
                    <div class="span1">
                        <?= lang("safa_groups_from_date") ?>
                    </div>
                    <div class="span6"> 
                        <?= form_input('safa_groups_no_of_mutamers_from', set_value("safa_groups_no_of_mutamers_from"), "class='input-huge'  ") ?>
                    </div>
                </div>
                <div class="span6" >
                    <div class="span1">
                        <?= lang("safa_groups_to_date") ?>
                    </div>
                    <div class="span6"> 
                       <?= form_input('safa_groups_no_of_mutamers_to', set_value("safa_groups_no_of_mutamers_to"), "class='input-huge'  ") ?>
                    </div>
                </div>
                        
                        
                        
                    </div>
                </div>
                <div class="row-fluid">
                        <div class="span12"><?= lang('group_passports_group_name') ?>:</div>
                        <div class="span12">
                            <?= form_dropdown('safa_group_id', ddgen('safa_groups', array('safa_group_id', 'name')), set_value('safa_group_id'), " name='s_example' class='select input-huge' style='width:100%;' ") ?>
                            <?= form_error('safa_group_id'); ?>
                        </div>
               </div>
                    </div>
            </div>
          
          
          
            
            
            
            <div class="row-fluid">
              <div class="widget-header"><a><?= lang("mutamers_data") ?></a></div> 
              <div class="row-fluid">
                 <div class="span4">
                
                
                <div class="span10">
                    <div class="span12"> <label> <?= lang('mutamer_name') ?>:</label></div>
                    <div class="span12">
                        <?= form_input('mutamer_name', set_value("mutamer_name", ''), "class='input-huge'") ?>
                        <?= form_error('mutamer_name', '<div class="bottom" style="color:red" >', '</div>'); ?>
                    </div>
                </div>
                <div class="span10">
                <div class="span12"> <label> <?= lang('group_passports_nationality_id') ?>:</label></div>
                    <div class="span12">
                        
                        <?= form_dropdown('nationality_id', ddgen('erp_nationalities', array('erp_nationality_id', name())), set_value('nationality_id',''), "  class='select input-huge' ") ?>
                        <?= form_error('nationality_id', '<div class="bottom" style="color:red" >', '</div>'); ?>
                    </div>
               </div>
                
               
             </div>
             
            <div class="span4">
                
                <div class="span10">
                    <div class="span12"> <label> <?= lang('group_passports_passport_no') ?>:</label></div>
                    <div class="span12">
                        <?= form_input('passport_no', set_value("passport_no", ''), " ") ?>
                        <?= form_error('passport_no', '<div class="bottom" style="color:red" >', '</div>'); ?>
                    </div>
                </div>
                <div class="span10">
                    <div class="span12">  <label><?= lang('group_passports_relative_no') ?>:</label></div>
                    <div class="span12">
                        <?= form_input('relative_no', set_value("relative_no", ''), " ") ?>
                        <?= form_error('relative_no', '<div class="bottom" style="color:red" >', '</div>'); ?>
                    </div>
                    </div>
                
               
                
            </div>
                
                
                <div class="span4">
                    <div class="span10">
                    <div class="span12"> <label><?= lang('group_passports_age') ?>:</label> </div>
                    <div class="span12">
                        <div class="span4"><?= lang('safa_groups_from_date') ?></div>
                        <div class="span8"> <?= form_input('age_from', set_value("age_from", ''), " ") ?>
                        <?= form_error('age', '<div class="bottom" style="color:red" >', '</div>'); ?></div>
                       
                    </div>
                    <div class="span12">
                        <div class="span4"><?= lang('safa_groups_to_date') ?></div>
                        <div class="span8"> <?= form_input('age_to', set_value("age_to", ''), " ") ?>
                        <?= form_error('age', '<div class="bottom" style="color:red" >', '</div>'); ?></div>
                       
                    </div>
                    </div>
                    
                </div>
              </div>
                
             
                
                
            </div>
            
            <div class="  TAC"><input type="submit" name="search" value="<?= lang('global_search') ?>" class="btn btn-primary" /></div>
            <?= form_close() ?>
        
    


    <div class="">
        
        
            <?php echo  lang('safa_groups_title') ?>
        </div>
    	
                    
        <div class="row-fluid" style="margin-bottom: 54px;">
            <table cellpadding="0" class="" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th><?= lang('safa_groups_th_name') ?></th>
                        <th><?= lang('safa_groups_creation_date') ?></th>
                        <th><?= lang('safa_groups_no_of_mutamers') ?></th>
                        <th><?= lang('global_actions') ?></th>
                    </tr>
                </thead>
                <tbody>
                    <? if (isset($items)): ?>
                        <? foreach ($items as $item): ?>
                            <tr id="row_<?php echo $item->safa_group_id; ?>">
                                <td> <?= $item->name ?></td>
                                <td><?= $item->creation_date ?></td>
                                <td><?= $item->no_of_mutamers ?></td>
                                <td class="TAC">
                                    <a title="<?  lang('show_mutamers')?>" href="<?= site_url("ea/group_passports/popup") ?>/<?= $item->safa_group_id ?>"><span class="icon-user"></span></a>
                                </td>
                            </tr>
                        <? endforeach; ?>
                    <? endif; ?>
                </tbody>
            </table>
            <?= form_close() ?>
        
      </div>  
      </div>
      <div class="modal-footer">
      <div class="span12 TAC">
          <input  type ="button" value="<?= lang('global_close') ?>" class="btn btn-primary" onclick="parent.$.fancybox.close();">
      </div>
      </div>          
        
    </div>
    

    <?= $pagination ?>
    <div id="fModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h3 id="myModalLabel"><?= lang('global_system_message') ?></h3>
        </div>        
        <div class="row-fluid">
            <div id="msg" class="row-form">
                <?= lang('global_are_you_sure_you_want_to_delete') ?>
            </div>
        </div>                   
        <div class="modal-footer">
            
            <button class="btn btn-primary" data-dismiss="modal" aria-hidden="true"id="yes"><?= lang('global_yes') ?></button>
            <button class="btn btn-primary" data-dismiss="modal" aria-hidden="true"id="no"><?= lang('global_no') ?></button>       
        </div>
    </div>
    <script type="text/javascript">
        $(document).ready(function() {
            $('.delete_item').click(function() {
                var name = $(this).attr('name');
                var safa_group_id = $(this).attr('id');
<? if (lang('global_lang') == 'ar'): ?>
                    $('#msg').text("<?= lang('global_are_you_sure_you_want_to_delete') ?>" + " " + name)
<? else: ?>
                    $('#msg').text(name + " " + "<?= lang('global_are_you_sure_you_want_to_delete') ?>")
<? endif; ?>
                $('#yes').click(function() {
                    var answer = $(this).attr('id');
                    if (answer === 'yes') {
                        $.ajax({
                            type: "POST",
                            dataType: 'json',
                            url: "<?php echo site_url('ea/safa_groups/delete'); ?>",
                            data: {'safa_group_id': safa_group_id},
                            success: function(msg) {
                                if (msg.response == true) {
                                    var del = safa_group_id;
                                    $("#row_" + del).remove();
<? if (lang('global_lang') == 'ar'): ?>
                                        $('.updated_msg').text("<?= lang('global_delete_confirm') ?>" + " " + name);
                                        $('.updated_msg').append('<br>');
                                        $('.updated_msg').append('<input  type ="button" value="<?= lang('global_submit') ?>"class="btn btn-primary" id="ok" >');
<? else: ?>
                                        $('.updated_msg').text(name + " " + "<?= lang('global_delete_confirm') ?>");
                                        $('.updated_msg').append('<br>');
                                        $('.updated_msg').append('<input  type ="button" value="<?= lang('global_submit') ?>"class="btn btn-primary" id="ok" >');
<? endif; ?>
                                    $('.updated_msg').show();
                                    $('#ok').click(function() {
                                        $('.updated_msg').hide();
                                    });
                                } else if (msg.response == false) {
                                    $('.updated_msg').text(msg.msg);
                                    $('.updated_msg').append('<br>');
                                    $('.updated_msg').append('<input  type ="button" value="<?= lang('global_submit') ?>"class="btn btn-primary" id="ok" >');
                                    $('.updated_msg').show();
                                    $('#ok').click(function() {
                                        $('.updated_msg').hide();
                                    });
                                }
                            }
                        });
                    } else {
                        return FALSE;
                    }
                });
            });
        });
    </script>
    <script type="text/javascript">
        $(function() {
            $('#datetimepicker1').datetimepicker({
                language: 'pt-BR',
                pickTime: false
            });
        });
    </script>
    <script type="text/javascript">
        $(function() {
            $('#datetimepicker2').datetimepicker({
                language: 'pt-BR',
                pickTime: false
            });
        });
    </script>
<script>
$(document).ready(function(){
  $("#search").keypress(function(e){
      if (e.which == 13)
        {
          e.preventDefault();
        }
        var searched = $('#search').val();
    $.getJSON("<?php echo site_url('ea/safa_groups/get_passport_no'); ?>/" + searched,function(result){
        var elements =[];
      $.each(result, function(i, val){
        elements.push(val.passport_no)
      });

      $('#search').autocomplete({
        source : elements
      });
      $('.ui-helper-hidden-accessible').hide();
  });
});

$("#mutamer_name").keypress(function(e){
      if (e.which == 13)
        {
         e.preventDefault();
        }
        var searched = $('#mutamer_name').val();
    $.getJSON("<?php echo site_url('ea/safa_groups/mutamer_name'); ?>/" + searched,function(result){
        var elements =[];
      $.each(result, function(i, val){
        elements.push(val.first_name_la +" "+ val.second_name_la +" "+ val.third_name_la)
      });

      $('#mutamer_name').autocomplete({
        source : elements
      });
      $('.ui-helper-hidden-accessible').hide();
  });
});

});

</script>


