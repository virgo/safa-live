<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); ?>
<!-- the script for datepicker --->
<script type="text/javascript" src='<?= JS ?>/plugins/bootstrap/bootstrap.min.js'></script>
<link href="<?= CSS_JS ?>/dateandtime/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src='<?= CSS_JS ?>/dateandtime/bootstrap-datetimepicker.min.js'></script>
<!-- the script for datepicker --->
<div class="row-fluid">
   <script>.widget{width:98%;}</script>

<div class="widget">
    <div class="path-container Fright">
    	<div class="path-name Fright">
            <a href="<?php echo  site_url().'ea/dashboard'; 
            
            ?>"><?php echo  lang('global_system_management') ?></a>
        </div>
        <div class="path-arrow Fright"></div>
        <div class="path-name Fright"> <a href="<?= site_url('ea/safa_groups/index') ?>"> <?= lang('safa_groups_title') ?></a></div>
        <div class="path-arrow Fright"></div>
        <div class="path-name Fright"> <?php echo  lang('safa_groups_edit_title') ?> <?=$item->name?></div>
    </div>
</div>

    <div class="widget">
       
        
         <div class="widget-header">
        <div class="widget-header-icon Fright"> <span class="icos-pencil2"></span> </div>
        <div class="widget-header-title Fright">
            <?php echo  lang('safa_groups_edit_title') ?>
        </div>
    	</div>
                                
        <div class="block-fluid">
            <?= form_open() ?>
            <div class="row-form">
                <div class="span6">
                    <div class="span4"> <?= lang('safa_groups_name') ?><span style="color: red">*</span>:</div>
                    <div class="span8">
                        <?= form_input('name', set_value("name", $item->name), " ") ?>
                        <?= form_error('name', '<div class="bottom" style="color:red" >', '</div>'); ?>
                    </div>
                </div>
                <div class="span6" >
                    <div class="span4">
                        <?= lang("safa_groups_creation_date") ?>
                        <font style='color:red'>*</font>
                    </div>
                    <div class="span8"> 
                        <!-- <div id="datetimepicker1" class="input-append date"> -->
                        <div id="" class="input-append date">
                        
                            <input class="creation_date" data-format="yyyy-MM-dd" name="creation_date" id="creation_date" type="text" value="<?= set_value('creation_date', $item->creation_date) ?>" style=" direction:ltr"></input>
                            <!-- 
                            <span class="add-on">
                                <i data-time-icon="icon-time" data-date-icon="icon-calendar">
                                </i>
                            </span>
                             -->
                            <script>
		                        $('.creation_date').datepicker({
		                            dateFormat: "yy-mm-dd",
		                            controlType: 'select',
		                            timeFormat: 'HH:mm'
		                        });
		                    </script>
                        </div>
                        <span class="bottom" style='color:red' >
                            <?= form_error('creation_date') ?> 
                        </span>
                    </div>
                </div>
            </div>

            <div class="toolbar bottom TAC"><input type="submit" name="submit" value="<?= lang('global_submit') ?>" class="btn btn-primary"> &nbsp;
                <input  type ="button" value="<?= lang('global_back') ?>"class="btn btn-primary" onclick="window.location = '<?= site_url('ea/safa_groups/index') ?>'">
            </div>
            <?= form_close() ?>     
        </div>
    </div>
</div>
<script type="text/javascript">
                    $(function() {
                        $('#datetimepicker1').datetimepicker({
                            language: 'pt-BR',
                            pickTime: false
                        });
                    });
</script>

