<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); ?>

<!-- the script for datepicker --->
<!-- Commented By Gouda -->
<!-- 
<script type="text/javascript" src='<?= JS ?>/plugins/bootstrap/bootstrap.min.js'></script>
<link href="<?= CSS_JS ?>/dateandtime/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src='<?= CSS_JS ?>/dateandtime/bootstrap-datetimepicker.min.js'></script>
-->
<!-- the script for datepicker --->

<script>.widget{width:98%;}</script>

<div class="row-fluid">



    <div class="widget">
        <div class="path-container Fright">
            <div class="icon"><i class="icos-pencil2"></i></div> 
            <div class="path-name Fright">
                <a href="<?= site_url('ea/dashboard') ?>"><?= lang('parent_node_title') ?></a>
            </div>    
            <div class="path-arrow Fright">
            </div>
            <div class="path-name Fright">
                <a href="<?= site_url('ea/trip_internaltrip/index') ?>"><?= lang('node_title') ?></a>
            </div>
            <div class="path-arrow Fright">
            </div>
            <div class="path-name Fright">
                <span><?= lang('edit_trip_internaltrip') ?></span>
            </div>
        </div>     
    </div>

    <div class="widget">


        <div class="widget-header">
            <div class="widget-header-icon Fright">
                <span class="icos-pencil2"></span>
            </div>

            <div class="widget-header-title Fright">
                <?= lang('edit_trip_internaltrip') ?>
            </div>
        </div>

        <?= form_open_multipart(false, 'id="frm_trip_internaltrip" ') ?>
        <div class="block-fluid">
            <div class="row-form" >
                <div class="span6" >
                    <div class="span4">
                        <?= lang("transport_request_date") ?>
                        <font style='color:red'>*</font>
                    </div>
                    <div class="span8"> 
                        <div >
                            <input class="datetime validate[required] input-full" data-format="yyyy-MM-dd" name="datetime" id="date" type="text" value="<?= set_value('datetime',
                                $item->datetime) ?>" ></input>

                            <script>
                                $('.datetime').datepicker({
                                    dateFormat: "yy-mm-dd",
                                    controlType: 'select',
                                    timeFormat: 'HH:mm'
                                });
                            </script>
                            <!-- 
                            <span class="add-on">
                                <i data-time-icon="icon-time" data-date-icon="icon-calendar">
                                </i>
                            </span>
                            -->

                        </div>
                        <span class="bottom" style='color:red' >
<?= form_error('datetime') ?> 
                        </span>
                    </div>
                </div>
                <div class="span6">
                    <div class="span4"><?= lang("transport_request_contract") ?>
                        <font style='color:red'>*</font>
                    </div>
                    
					<div class="span8" >
                            <?= form_dropdown("safa_uo_contract_id", $uos,
                                    set_value("safa_uo_contract_id", $item->safa_uo_contract_id), "id='safa_uo_contract_id' class='validate[required] input-full'") ?>
                        <span class="bottom" style='color:red' >
						<?= form_error('safa_uo_contract_id') ?> 
                        </span>
                    </div>
                </div>
            </div>
            <div class="row-form" >
                <div class="span6" >
                    <div class="span4">
<?= lang("transport_request_trip_id") ?>
                        
                    </div>
                    <div class="span8">
                            <?= form_dropdown("safa_trip_id",
                                    $safa_trips, set_value("safa_trip_id", $item->safa_trip_id),
                                    "id='trip_id'") ?>
                        <span class="bottom" style='color:red' >
<?= form_error('safa_trip_id') ?> 
                        </span>
                    </div>
                </div>
                <div class="span6" >
                    <div class="span4">
                            <?= lang("transport_request_opertator") ?>
                    </div>
                    <div class="span8" >
<?= form_dropdown("safa_ito_id", $safa_ito, set_value("safa_ito_id", $item->safa_ito_id),
        "id='safa_ito_id'") ?>
                        <span class="bottom" style='color:red' >
<?= form_error('safa_ito_id') ?> 
                        </span>
                    </div>
                </div>
            </div>
            <div class="row-form" >
                <div class="span6" >
                    <div class="span4">
                            <?= lang("transport_request_status") ?>
                        <font style='color:red'>*</font>
                    </div>
                    <div class="span8">
<?= form_dropdown("safa_internaltripstatus_id", $safa_intrernaltripstatus,
        set_value("safa_internaltripstatus_id", $item->safa_internaltripstatus_id), "id='safa_internaltripstatus_id' class='validate[required] input-full'") ?>
                        <span class="bottom" style='color:red' >
                        <?= form_error('safa_internaltripstatus_id') ?> 
                        </span>
                    </div>
                </div>
                <div class="span6">
                    <div class="span4">
                            <?= lang("transport_request_res_code") ?>
                        <font style='color:red'>*</font>
                    </div>
                    <div class="span8" >
            <?= form_input("operator_reference",
                    set_value("operator_reference", $item->operator_reference), " class='validate[required] input-full' ") ?>
                        <span class="bottom" style='color:red' >
<?= form_error('operator_reference') ?> 
                        </span>
                    </div>
                </div>
            </div>
                            <?php if (PHASE >= 2): ?>
                <div class="row-form">
                    <div class="span6">
                        <div class="span4"><?= lang("transport_request_transportername") ?>
                        </div>
                        <div class="span8" >
                <?= form_dropdown("safa_transporter_id", $transporters,
                        set_value("safa_transporter_id", $item->safa_transporter_id), "") ?>
                            <span class="bottom" style='color:red' >
    <?= form_error('safa_transporter_id') ?> 
                            </span>
                        </div>
                    </div>
                </div>
<? endif; ?>
            <div class="row-form" >
                <div class="span6" >
                    <div class="span4" ><?= lang("transport_request_res_file") ?></div>
                    <div class="span8" >
                    
                    	<!-- 
                        <div class="input-append file"  >
                            <input type="file" value="" name="userfile"/>
                            <input type="text" name="text_input"  value="<?= $item->attachement ?>" />
                            <button type="button"  class="btn">Browse</button>
                        </div>
                        <div>
                            <a href="<?= base_url('static/ea_files/' . session('internaltrip_file_upload_time') . $item->attachement) ?>" target="_blank"  ><?= $item->attachement ?></a>
                        </div>
                        <span class="bottom" style='color:red' >
                        <?= form_error('userfile') ?> 
                        </span>
 						-->

						<input type="file" value="" name="transport_request_res_file" id="transport_request_res_file"/>
                        <span class="bottom" style='color:red' >
                            <?= form_error('userfile') ?> 
                        </span>
                        <a href="<?= base_url('static/temp/ea_files/' . session('internaltrip_file_upload_time') . $item->attachement) ?>" target="_blank"  ><?= $item->attachement ?></a>
                        
                        
                    </div>
                </div>
<? if (PHASE >= 2): ?>
                    <div class="span6" >
                        <div class="span4" ><?= lang('confirmation_number') ?></div>
                        <div class="span8" >
    <?= form_input("conf_number", set_value("conf_number", $item->confirmation_number),
            "disabled='disabled'") ?>
                        </div>
                    </div>
<? endif; ?>
            </div>
            <div class="row-form" >
                <div class='span12' >
                    <div class='span2' ><?= lang('add_ito_notes') ?></div>
                    <div class='span10' >
                        <textarea name='ea_notes' class="input-full"><?= $item->ea_notes ?></textarea>
                    </div>
                </div>
            </div>
            <div class="toolbar bottom TAC">
                <input class="btn btn-primary" type="submit" name="submit" value="<?= lang('global_edit') ?>" />
           
                <input type="button" value="<?= lang("global_show_segments") ?>" onclick = "location.href = '<?= site_url('ea/trip_internaltrip/index') ?>'"  class="btn btn-primary">
            </div>
<?= form_close() ?>
        </div>
    </div>


    <div class="widget" >


        <div class="widget-header">
            <div class="widget-header-icon Fright">
                <span class="icos-pencil2"></span>
            </div>

            <div class="widget-header-title Fright">
<?= lang('transport_request_internal_passages') ?>
            </div>

            <a href="<?= site_url("ea/trip_internalpassages/add/" . $trip_internal_id) ?>" class="btn  fancybox fancybox.iframe" style="<? if (lang('global_lang') == 'ar'): ?>float:left<? else: ?>float:right<? endif; ?>" ><?= lang('add_passges') ?></a>
        </div>


        <div class="block-fluid" >
            <!--- the part of internal passages---> 
            <table class="myTable" cellpadding="0" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th><?= lang('internalpassage_type') ?></th>
                        <th><?= lang('internalpassage_starthotel') ?></th>
                        <th><?= lang('internalpassage_endhotel') ?></th>
                        <th><?= lang('internalpassage_startdatatime') ?></th>
                        <th><?= lang('internalpassage_enddatatime') ?></th>
                        <th><?= lang('internalpassage_seatscount') ?></th>
                        <th><?= lang('internalpassage_note') ?></th>
                        <th><?= lang('global_actions') ?></th>
                    </tr>
                </thead>
                <tbody>
<? if (isset($internalpassages)) : ?>
                            <? foreach ($internalpassages as $inernalpassage): ?>
                            <tr>
                                <td><?= item("safa_internalsegmenttypes", name(),
                                        array("safa_internalsegmenttype_id" => $inernalpassage->safa_internalsegmenttype_id)); ?></td>
                                <? if ($inernalpassage->safa_internalsegmenttype_id == 1): 
                                
                                	$erp_port='';
                                	if($inernalpassage->erp_port_id) {
                                		$erp_port = item("erp_ports", name(), array("erp_port_id" => $inernalpassage->erp_port_id));
                                	}
                                	
                                	$erp_end_hotel='';
                                	if($inernalpassage->erp_end_hotel_id) {
                                		$erp_end_hotel = item("erp_hotels", name(), array("erp_hotel_id" => $inernalpassage->erp_end_hotel_id));
                                	}
                                	
                                
                                ?>
                                    <td><?= $erp_port; ?></td>
                                    <td><?= $erp_end_hotel; ?></td>
        <? endif; ?>  
                                <? 
                                
                                	if ($inernalpassage->safa_internalsegmenttype_id == 2){ 
                                	$erp_start_hotel='';
                                	if($inernalpassage->erp_start_hotel_id) {
                                		$erp_start_hotel = item("erp_hotels", name(), array("erp_hotel_id" => $inernalpassage->erp_start_hotel_id));
                                	}
                                	
                                	$erp_port='';
                                	if($inernalpassage->erp_port_id) {
                                		$erp_port = item("erp_ports", name(), array("erp_port_id" => $inernalpassage->erp_port_id));
                                	}
                                ?>
                                    <td><?= $erp_start_hotel; ?></td>
                                    <td><?= $erp_port; ?></td>
                                    <? }; ?>
                                    <? if ($inernalpassage->safa_internalsegmenttype_id == 3): 
                                    
                                    $erp_start_hotel='';
                                	if($inernalpassage->erp_start_hotel_id) {
                                		$erp_start_hotel = item("erp_hotels", name(), array("erp_hotel_id" => $inernalpassage->erp_start_hotel_id));
                                	}
                                	
                                	$safa_tourism_place='';
                                	if($inernalpassage->safa_tourism_place_id) {
                                		$safa_tourism_place = item("safa_tourismplaces", name(), array("safa_tourismplace_id" => $inernalpassage->safa_tourism_place_id));
                                	}
                                	
                                    ?>
                                    <td><?= $erp_start_hotel; ?></td>
                                    <td><?= $safa_tourism_place; ?></td>
                                    <? endif; ?>
                                    
                                    <? if ($inernalpassage->safa_internalsegmenttype_id == 4): 
                                    
                                    $erp_start_hotel='';
                                	if($inernalpassage->erp_start_hotel_id) {
                                		$erp_start_hotel = item("erp_hotels", name(), array("erp_hotel_id" => $inernalpassage->erp_start_hotel_id));
                                	}
                                	
                                	$erp_end_hotel='';
                                	if($inernalpassage->erp_end_hotel_id) {
                                		$erp_end_hotel = item("erp_hotels", name(), array("erp_hotel_id" => $inernalpassage->erp_end_hotel_id));
                                	}
                                    
                                    ?>
                                    <td><?= $erp_start_hotel; ?></td>
                                    <td><?= $erp_end_hotel; ?></td>
                                    <? endif; ?>
                                <td>
        <? if (isset($inernalpassage->start_datetime) && $inernalpassage->start_datetime != null && $inernalpassage->start_datetime != '0000-00-00 00:00:00'): ?>
            <? $date = date_create($inernalpassage->start_datetime); ?>
                                        <span><?= date_format($date, "m-d H:i"); ?></span>
                                    <? endif; ?>   
                                </td>
                                <td>
                                        <? if (isset($inernalpassage->end_datetime) && $inernalpassage->end_datetime != null && $inernalpassage->end_datetime != '0000-00-00 00:00:00'): ?>
            <? $date_end = date_create($inernalpassage->end_datetime); ?>    
                                        <span><?= date_format($date_end, "m-d H:i"); ?></span>
                            <? endif; ?> 
                                </td>
                                <td><? if (isset($inernalpassage->seats_count)): ?> <?= $inernalpassage->seats_count ?> <? endif; ?>  </td>
                                <td><? if (isset($inernalpassage->notes)): ?> <?= $inernalpassage->notes ?> <? endif; ?>  </td>
                                <td class="TAC">
                                    <a  class="fancybox fancybox.iframe " href="<?= site_url("ea/trip_internalpassages/edit/" . $inernalpassage->safa_internalsegment_id) ?>"><span class="icon-pencil"></span></a> 
        <? if (!check_id('safa_internalsegment_log', 'safa_internalsegment_id',
                        $inernalpassage->safa_internalsegment_id)): ?>
                                        <a href="<?= site_url("ea/trip_internalpassages/delete/" . $inernalpassage->safa_internalsegment_id . "/" . $trip_internal_id) ?>" onclick="return window.confirm('<?= lang('global_are_you_sure_you_want_to_delete') ?>')" ><span class="icon-trash"></span></a>
        <? endif; ?>
                                </td>
                            </tr>  

    <? endforeach; ?>
<? endif; ?>  
                </tbody>
            </table>  
        </div>
    </div>


</div>


<script type="text/javascript">
$(document).ready(function() {
    var config = {
        '.chosen-select': {width: "100%"},
        '.chosen-select-deselect': {allow_single_deselect: true},
        '.chosen-select-no-single': {disable_search_threshold: 10},
        '.chosen-select-no-results': {no_results_text: 'Oops, nothing found!'},
        '.chosen-select-width': {width: "95%"}
    }
    for (var selector in config) {
        $(selector).chosen(config[selector]);
    }


    // binds form submission and fields to the validation engine
    $("#frm_trip_internaltrip").validationEngine({
            prettySelect : true,
            useSuffix: "_chosen",
            promptPosition : "topRight:-150"
            //promptPosition : "bottomLeft"
    });

 
});


</script>

<script>
    $(document).ready(function() {
        $('.fancybox').fancybox({
            afterClose: function() {
                location.reload();
            }
        });
    });
</script>
<!--<script>-->
<!--    $(document).ready(function() {-->
<!---->
<!--        var uo_id = $("#uo_id").val();-->
<!---->
<!--        $("#uo_id").change(function() {-->
<!--            $.get('<?= site_url("ea/trip_internaltrip/get_ea_trips") ?>/' + $(this).val(), function(data) {-->
<!--                $("#trip_id").html(data);-->
<!--            });-->
<!--        });-->
<!--        $.get('<?= site_url("ea/trip_internaltrip/get_ea_trips") ?>/' + uo_id, function(data) {-->
<!--            $("#trip_id").html(data);-->
<!--<? if (isset($_POST['safa_trip_id'])): ?>-->
<!--                $("#trip_id").val('<?= $this->input->post('safa_trip_id') ?>');-->
<!--<? else: ?>-->
<!--                $("#trip_id").val('<?= $item->safa_trip_id ?>');-->
<!--<? endif; ?>-->
<!--        });-->
<!--    });-->
<!--</script>-->
<script>
    $('.datepicker').datepicker({
        dateFormat: "yy-mm-dd",
    });
</script>
<script>
    $(document).ready(function() {
        $(".myTable").dataTable(
                {bSort: false,
                    bAutoWidth: true,
                    "iDisplayLength": false, // can be removed for basic 10 items per page
                    "sPaginationType": false,
                    "bPaginate": false,
                    "bInfo": false}
        );
    });
</script>
<script>
    $(document).ready(function() {
        /* input file */
        $(".file .btn, .file input:text").click(function() {
            var block = $(this).parent('.file');
            block.find('input:file').change(function() {
                var file_arr = $(this).val().split('\\');
                var file_name = file_arr[file_arr.length - 1];
                block.find('input:text').val(file_name);
            });
        });
    });
</script>
<script type="text/javascript">
    $(function() {
        $('#datetimepicker1').datetimepicker({
            language: 'pt-BR',
            pickTime: false
        });
    });
</script>
<?php if (PHASE >= 2): ?>
    <script>
        $(document).ready(function() {
            $("#safa_ito_id").change(function() {
                if ($("#safa_ito_id").val()) {
                    $("#safa_internaltripstatus_id").val(2);
                } else {
                    $("#safa_internaltripstatus_id").val(1);
                }
            });
            $("#safa_internaltripstatus_id").change(function() {
                if ($("#safa_ito_id").val()) {
                    if ($("#safa_internaltripstatus_id").val() != 2)
    <? if (lang('global_lang') == 'ar'): ?>
                        alert("قم باختيار مؤكد من شركة السياحة");
    <? else: ?>
                        alert("choose confirmed by ea");
    <? endif; ?>
                    $("#safa_internaltripstatus_id").val(2);
                } else {
                    if ($("#safa_internaltripstatus_id").val() != 1)
    <? if (lang('global_lang') == 'ar'): ?>
                        alert("قم باختيار بانتظار شركة العمرة");
    <? else: ?>
                        alert("choose pending uo");
    <? endif; ?>
                    $("#safa_internaltripstatus_id").val(1);
                }
            });
        });
    </script>
<? endif; ?>