<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); ?>
<link href="<?= CSS ?>/common-css.css" rel="stylesheet" type="text/css" />
<? if (lang('global_lang') == 'ar'): ?>
<link href="<?= CSS ?>/common-css-ar.css" rel="stylesheet" type="text/css" />
<? else: ?><? endif ?>
<script type="text/javascript" src='<?= JS ?>/plugins/jquery/jquery-1.9.1.min.js'></script>
<script type="text/javascript" src='<?= JS ?>/plugins/jquery/jquery-ui-1.10.1.custom.min.js'></script>
<script type="text/javascript" src='<?= JS ?>/plugins/bootstrap/bootstrap.min.js'></script>
<link href="<?= CSS_JS ?>/dateandtime/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src='<?= CSS_JS ?>/dateandtime/bootstrap-datetimepicker.min.js'></script>
<style>
    
    .error{
        color:red !important;
    }

    body{
        min-height: 90% !important;
        overflow: auto !important;
        background: white;
    }

    .row-form{
        padding-top: 1px !important;
        padding-bottom: -3px !important;
    }
    .widget-header{
        color:white;
        padding:0px 10px;
    }
</style>
<div id="talab-naql"    class="" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="widget-header">
        <h3 id="myModalLabel"><?= lang('add_internalpassages') ?></h3>
    </div>
    <div class="row-fluid">
        <div class="span12" >
            <?= form_open() ?>
            <? if (isset($num_of_segments)): ?>
            <div class='row-form'>
                <div class="span7">
                    <div class='span4'>
                        <span style='font-weight:bold'  ><?= lang('num_of_segments') ?></span>
                    </div>
                    <div class='span8' >
                        <span class='label' style='background-color:red'  ><?= $num_of_segments ?></span>
                    </div>
                </div>
                <div class="span5">
                    <div class="span4" ><?= lang('internalpassage_type') ?>
                        <font style='color:red' >*</font>
                    </div>
                    <div class="span8" ><?= form_dropdown('internalpassage_type', $safa_internalpassagetypes, set_value('internalpassage_type'), "id='passage_type'") ?>
                        <span class='bottom' style='color:red' >
                            <?= form_error('internalpassage_type') ?>
                        </span>
                    </div>
                </div>

            </div>  
            <? endif ?>
            <div class="row-form" >
                <div class="span7">
                    <div class="span4" ><?= lang('internalpassage_starthotel') ?>
                        <font style='color:red' >*</font>
                    </div>
                    <!------->
                    <div class="span8" id="start_point_con">
                        <?= form_dropdown('internalpassage_hotel_start', $erp_hotels, set_value('internalpassage_hotel_start'), "style='display:none' class='hotel' ") ?>
                        <span class='bottom error'  > 
                            <?= form_error('internalpassage_hotel_start') ?>
                        </span>
                        <?= form_dropdown('internalpassage_torismplace_start', $safa_tourismplaces, set_value('internalpassage_torismplace_strat'), "style='display:none' class='place' ") ?>
                        <span class='bottom error' > 
                            <?= form_error('internalpassage_torismplace_start') ?>
                        </span>
                        <?= form_dropdown('internalpassage_port_start', $erp_ports, set_value('internalpassage_port_start'), "style='display:none' class='port'  ") ?>
                        <span class='bottom error' > 
                            <?= form_error('internalpassage_port_start') ?>
                        </span>
                    </div>
                </div>
                <!------->
                <div class="span5">
                    <div class="span4" >
                        <?= lang('internalpassage_startdatatime') ?>
                        <font  style='color:red'>*</font>
                    </div>
                    <div class="span5" >
                        <div id="datetimepicker1" class="input-append date">
                            <input class="" data-format="yyyy-MM-dd hh:mm" name="startdatetime" id="date" type="text" value="<?= set_value('startdatetime') ?>" style=" direction:ltr"></input>
                            <span class="add-on">
                                <i data-time-icon="icon-time" data-date-icon="icon-calendar">
                                </i>
                            </span>
                        </div>
                        <div class="span2">
                            <span class='bottom' style='color:red'>
                                <?= form_error('startdatetime') ?>
                            </span>

                        </div>
                    </div>
                </div>
            </div>
            <div class="row-form" >
                <div class="span7">
                    <div class="span4" ><?= lang('internalpassage_endhotel') ?>
                        <font style='color:red' >*</font> 
                    </div>
                    <!------->
                    <div class="span4" id="end_point_con"  >
                        <?= form_dropdown('internalpassage_hotel_end', $erp_hotels, set_value('internalpassage_hotel_end'), "style='display:none' class='hotel'") ?>
                        <span class='bottom error' > 
                            <?= form_error('internalpassage_hotel_end') ?>
                        </span>
                        <?= form_dropdown('internalpassages_torismplace_end', $safa_tourismplaces, set_value('internalpassages_torismplace_end'), "style='display:none' class='place' ") ?>
                        <span class='bottom error' > 
                            <?= form_error('internalpassages_torismplace_end') ?>
                        </span>
                        <?= form_dropdown('internalpassage_port_end', $erp_ports, set_value('internalpassage_port_end'), "style='display:none' class='port' ") ?>
                        <span class='bottom error' > 
                            <?= form_error('internalpassage_port_end') ?>
                        </span>


                    </div>
                    <!------->
    <!--                <div class="span2" ><?= lang('internalpassage_enddatatime') ?></div>
                    <div class="span4" ><?= form_input("enddatetime", set_value("enddatetime"), "class='datepicker'") ?>
                       <span class='bottom' style='color:red'>
                    <?= form_error('enddatetime') ?>
                        </span> 
                    </div>-->
                </div>
                <div class="span5" >

                    <div class="span4" >
                        <?= lang('internalpassage_seatscount') ?>
                        <font style='color:red' >*</font>
                    </div>
                    <div class="span8" >
                        <?= form_input('seatscount', set_value('seatscount')) ?>
                        <span class='bottom' style='color:red' >
                            <?= form_error('seatscount') ?>
                        </span>
                    </div>
                </div>
            </div>

            <div class="row-form" >
                <div class="span2" ><?= lang('internalpassage_notes') ?></div>
                <div class="span8" >
                    <textarea name="internalpassage_notes"><?= set_value('internalpassage_notes') ?></textarea>
                </div> 
            </div>
            <div class="toolbar bottom TAC">
                <button name="submit"  class="btn btn-primary"><?= lang("global_add") ?></button>
                <?= form_hidden("internaltrip_id", set_value("internaltrip", $internaltrip_id)) ?>
            </div>
        </div>
    </div>
    <?= form_close() ?>
</div>
</div>
<?= form_close() ?>
<script type="text/javascript">
    $(function() {
        $('#datetimepicker1').datetimepicker({
            language: 'pt-BR',
        });

        $('.bootstrap-datetimepicker-widget').on('click', '.day', function() {
            $('.bootstrap-datetimepicker-widget').hide();
        });

    });
</script>


<script>
    function show_ports(pass_type) {
        $("#start_point_con").children().hide();
        $("#end_point_con").children().hide();
        if (pass_type == 1) {
            //show the data for arrive//
            $("#start_point_con").find('.port').show();
            $("#end_point_con").find('.hotel').show();

        }
        if (pass_type == 2) {
            // show data for leave//
            $("#start_point_con").find('.hotel').show();
            $("#end_point_con").find('.port').show();
            $("#end_point_con").find('.error').show();
        }
        if (pass_type == 3) {
            // show data for place//
            $("#start_point_con").find('.hotel').show();
            $("#end_point_con").find('.place').show();
        }
        if (pass_type == 4) {
            // show data for transport//
            $("#start_point_con").find('.hotel').show();
            $("#end_point_con").find('.hotel').show();
        }
    }
</script>
<script>
    $(document).ready(function() {
        $("#passage_type").change(function() {
            var val_ = $(this).val();
            show_ports(val_);
        });
    });
</script>
<script>
    $(document).ready(function() {
        var pass_type = $("#passage_type").val();
        show_ports(pass_type);

    });
</script>
<script>
    $(document).ready(function() {
        $('.error').show();
    });

</script>



