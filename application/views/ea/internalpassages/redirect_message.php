<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); ?>
<script type='text/javascript' src='<?= JS ?>/plugins/jquery/jquery-1.9.1.min.js'></script>
<script type='text/javascript' src='<?= JS ?>/plugins/jquery/jquery-ui-1.10.1.custom.min.js'></script>
<link href="<?= CSS ?>/stylesheets.css" rel="stylesheet" type="text/css"/>
<style>
    .msg{
        padding:8px 35px 8px 14px;
      
        color:#c09853;text-shadow:0 1px 0 rgba(255,255,255,0.5);background-color:#fcf8e3;border:1px solid #fbeed5;-webkit-border-radius:4px;-moz-border-radius:4px;border-radius:4px;
        color:#468847;
        background-color:#dff0d8;
        border-color:#d6e9c6;
        border: 3px solid rgb(255, 255, 255); 
        text-align: center;  
    }  
</style>
<div id="talab-naql" class="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-header">
        <h3 id="myModalLabel"><? if(isset($action)):?><?=$action?><?endif;?></h3>
    </div>
    <div class="row-fluid">
        <div class="span12" >
            <div class="msg">
                <p><?= $msg ?></p>
                <?
                    $label_name='';
                    if(isset($id))
                        $label_name=lang('global_back');
                    else
                      $label_name=lang('global_add_new_record');  
                 ?>
                <input  type ="button" value="<?=$label_name?>"class="btn btn-primary" onclick="window.location = '<?= $url ?>'">
                <button class="btn btn-primary  close_frame" ><?=lang('global_close')?></button>
            </div> 
        </div>
    </div>
</div> 
<script>
    $('.close_frame').click(function(){
         parent.$.fancybox.close();
    });
</script>


