<script>
    $(document).ready(function() {
        $("#wizard_validate").validationEngine('attach', {promptPosition: "topLeft", prettySelect: true});
    });
</script>
<style>
    .widget-header {
        clear:both;
    }
    .wizerd-div{margin-top: -33px;}
    
    #dv_air_trip td {padding: 2px !important;}
    #div_trip_hotel_group td {padding: 5px !important;}  
    
    .resalt-group  .wizerd-div {
    border-bottom: medium none !important;
    margin: -18px 0 20px!important;
    padding-top: 0;
}
.resalt-group .wizerd-div a {
    background: none repeat scroll 0 0 #FAFAFA;
    border: 1px solid #D5D6D6;
    border-radius: 49px;
    color: #663300;
    display: inline-block;
    margin: -6px 5px -17px -8px;
    padding: 10px 12px 8px;
}
a{ color: #C09853;}
.resalt-group {
    margin: 18px 0.5% 0.5%;
    padding: 0.5%;
    width: 99%;
}
    
</style>

<!--<script type="text/javascript" src="<?= JS ?>/timepicker/jquery-ui-sliderAccess.js"></script>-->
<!--<script type="text/css" href="<?= JS ?>/timepicker/jquery-ui-timepicker-addon.css"></script>-->
<!--<script type="text/javascript" src="<?= JS ?>/timepicker/jquery-ui-timepicker-addon.js"></script>-->
<!--<link rel="stylesheet" media="all" type="text/css" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />-->
<script type="text/javascript" src="<?= JS ?>/datetime_format.js"></script>
<script>
    /*
     * jQuery timepicker addon
     * By: Trent Richardson [http://trentrichardson.com]
     * Version 1.3.1
     * Last Modified: 07/07/2013
     *
     * Copyright 2013 Trent Richardson
     * You may use this project under MIT or GPL licenses.
     * http://trentrichardson.com/Impromptu/GPL-LICENSE.txt
     * http://trentrichardson.com/Impromptu/MIT-LICENSE.txt
     */

    /*jslint evil: true, white: false, undef: false, nomen: false */

    (function($) {

        /*
         * Lets not redefine timepicker, Prevent "Uncaught RangeError: Maximum call stack size exceeded"
         */
        $.ui.timepicker = $.ui.timepicker || {};
        if ($.ui.timepicker.version) {
            return;
        }

        /*
         * Extend jQueryUI, get it started with our version number
         */
        $.extend($.ui, {
            timepicker: {
                version: "1.3.1"
            }
        });

        /*
         * Timepicker manager.
         * Use the singleton instance of this class, $.timepicker, to interact with the time picker.
         * Settings for (groups of) time pickers are maintained in an instance object,
         * allowing multiple different settings on the same page.
         */
        var Timepicker = function() {
            this.regional = []; // Available regional settings, indexed by language code
            this.regional[''] = {// Default regional settings
                currentText: 'Now',
                closeText: 'Done',
                amNames: ['AM', 'A'],
                pmNames: ['PM', 'P'],
                timeFormat: 'HH:mm',
                timeSuffix: '',
                timeOnlyTitle: 'Choose Time',
                timeText: 'Time',
                hourText: 'Hour',
                minuteText: 'Minute',
                secondText: 'Second',
                millisecText: 'Millisecond',
                microsecText: 'Microsecond',
                timezoneText: 'Time Zone',
                isRTL: false
            };
            this._defaults = {// Global defaults for all the datetime picker instances
                showButtonPanel: true,
                timeOnly: false,
                showHour: null,
                showMinute: null,
                showSecond: null,
                showMillisec: null,
                showMicrosec: null,
                showTimezone: null,
                showTime: true,
                stepHour: 1,
                stepMinute: 1,
                stepSecond: 1,
                stepMillisec: 1,
                stepMicrosec: 1,
                hour: 0,
                minute: 0,
                second: 0,
                millisec: 0,
                microsec: 0,
                timezone: null,
                hourMin: 0,
                minuteMin: 0,
                secondMin: 0,
                millisecMin: 0,
                microsecMin: 0,
                hourMax: 23,
                minuteMax: 59,
                secondMax: 59,
                millisecMax: 999,
                microsecMax: 999,
                minDateTime: null,
                maxDateTime: null,
                onSelect: null,
                hourGrid: 0,
                minuteGrid: 0,
                secondGrid: 0,
                millisecGrid: 0,
                microsecGrid: 0,
                alwaysSetTime: true,
                separator: ' ',
                altFieldTimeOnly: true,
                altTimeFormat: null,
                altSeparator: null,
                altTimeSuffix: null,
                pickerTimeFormat: null,
                pickerTimeSuffix: null,
                showTimepicker: true,
                timezoneList: null,
                addSliderAccess: false,
                sliderAccessArgs: null,
                controlType: 'slider',
                defaultValue: null,
                parse: 'strict'
            };
            $.extend(this._defaults, this.regional['']);
        };

        $.extend(Timepicker.prototype, {
            $input: null,
            $altInput: null,
            $timeObj: null,
            inst: null,
            hour_slider: null,
            minute_slider: null,
            second_slider: null,
            millisec_slider: null,
            microsec_slider: null,
            timezone_select: null,
            hour: 0,
            minute: 0,
            second: 0,
            millisec: 0,
            microsec: 0,
            timezone: null,
            hourMinOriginal: null,
            minuteMinOriginal: null,
            secondMinOriginal: null,
            millisecMinOriginal: null,
            microsecMinOriginal: null,
            hourMaxOriginal: null,
            minuteMaxOriginal: null,
            secondMaxOriginal: null,
            millisecMaxOriginal: null,
            microsecMaxOriginal: null,
            ampm: '',
            formattedDate: '',
            formattedTime: '',
            formattedDateTime: '',
            timezoneList: null,
            units: ['hour', 'minute', 'second', 'millisec', 'microsec'],
            support: {},
            control: null,
            /*
             * Override the default settings for all instances of the time picker.
             * @param settings object - the new settings to use as defaults (anonymous object)
             * @return the manager object
             */
            setDefaults: function(settings) {
                extendRemove(this._defaults, settings || {});
                return this;
            },
            /*
             * Create a new Timepicker instance
             */
            _newInst: function($input, opts) {
                var tp_inst = new Timepicker(),
                        inlineSettings = {},
                        fns = {},
                        overrides, i;

                for (var attrName in this._defaults) {
                    if (this._defaults.hasOwnProperty(attrName)) {
                        var attrValue = $input.attr('time:' + attrName);
                        if (attrValue) {
                            try {
                                inlineSettings[attrName] = eval(attrValue);
                            } catch (err) {
                                inlineSettings[attrName] = attrValue;
                            }
                        }
                    }
                }

                overrides = {
                    beforeShow: function(input, dp_inst) {
                        if ($.isFunction(tp_inst._defaults.evnts.beforeShow)) {
                            return tp_inst._defaults.evnts.beforeShow.call($input[0], input, dp_inst, tp_inst);
                        }
                    },
                    onChangeMonthYear: function(year, month, dp_inst) {
// Update the time as well : this prevents the time from disappearing from the $input field.
                        tp_inst._updateDateTime(dp_inst);
                        if ($.isFunction(tp_inst._defaults.evnts.onChangeMonthYear)) {
                            tp_inst._defaults.evnts.onChangeMonthYear.call($input[0], year, month, dp_inst, tp_inst);
                        }
                    },
                    onClose: function(dateText, dp_inst) {
                        if (tp_inst.timeDefined === true && $input.val() !== '') {
                            tp_inst._updateDateTime(dp_inst);
                        }
                        if ($.isFunction(tp_inst._defaults.evnts.onClose)) {
                            tp_inst._defaults.evnts.onClose.call($input[0], dateText, dp_inst, tp_inst);
                        }
                    }
                };
                for (i in overrides) {
                    if (overrides.hasOwnProperty(i)) {
                        fns[i] = opts[i] || null;
                    }
                }

                tp_inst._defaults = $.extend({}, this._defaults, inlineSettings, opts, overrides, {
                    evnts: fns,
                    timepicker: tp_inst // add timepicker as a property of datepicker: $.datepicker._get(dp_inst, 'timepicker');
                });
                tp_inst.amNames = $.map(tp_inst._defaults.amNames, function(val) {
                    return val.toUpperCase();
                });
                tp_inst.pmNames = $.map(tp_inst._defaults.pmNames, function(val) {
                    return val.toUpperCase();
                });

// detect which units are supported
                tp_inst.support = detectSupport(
                        tp_inst._defaults.timeFormat +
                        (tp_inst._defaults.pickerTimeFormat ? tp_inst._defaults.pickerTimeFormat : '') +
                        (tp_inst._defaults.altTimeFormat ? tp_inst._defaults.altTimeFormat : ''));

// controlType is string - key to our this._controls
                if (typeof (tp_inst._defaults.controlType) === 'string') {
                    if (tp_inst._defaults.controlType == 'slider' && typeof (jQuery.ui.slider) === 'undefined') {
                        tp_inst._defaults.controlType = 'select';
                    }
                    tp_inst.control = tp_inst._controls[tp_inst._defaults.controlType];
                }
// controlType is an object and must implement create, options, value methods
                else {
                    tp_inst.control = tp_inst._defaults.controlType;
                }

// prep the timezone options
                var timezoneList = [-720, -660, -600, -570, -540, -480, -420, -360, -300, -270, -240, -210, -180, -120, -60,
                    0, 60, 120, 180, 210, 240, 270, 300, 330, 345, 360, 390, 420, 480, 525, 540, 570, 600, 630, 660, 690, 720, 765, 780, 840];
                if (tp_inst._defaults.timezoneList !== null) {
                    timezoneList = tp_inst._defaults.timezoneList;
                }
                var tzl = timezoneList.length, tzi = 0, tzv = null;
                if (tzl > 0 && typeof timezoneList[0] !== 'object') {
                    for (; tzi < tzl; tzi++) {
                        tzv = timezoneList[tzi];
                        timezoneList[tzi] = {value: tzv, label: $.timepicker.timezoneOffsetString(tzv, tp_inst.support.iso8601)};
                    }
                }
                tp_inst._defaults.timezoneList = timezoneList;

// set the default units
                tp_inst.timezone = tp_inst._defaults.timezone !== null ? $.timepicker.timezoneOffsetNumber(tp_inst._defaults.timezone) :
                        ((new Date()).getTimezoneOffset() * -1);
                tp_inst.hour = tp_inst._defaults.hour < tp_inst._defaults.hourMin ? tp_inst._defaults.hourMin :
                        tp_inst._defaults.hour > tp_inst._defaults.hourMax ? tp_inst._defaults.hourMax : tp_inst._defaults.hour;
                tp_inst.minute = tp_inst._defaults.minute < tp_inst._defaults.minuteMin ? tp_inst._defaults.minuteMin :
                        tp_inst._defaults.minute > tp_inst._defaults.minuteMax ? tp_inst._defaults.minuteMax : tp_inst._defaults.minute;
                tp_inst.second = tp_inst._defaults.second < tp_inst._defaults.secondMin ? tp_inst._defaults.secondMin :
                        tp_inst._defaults.second > tp_inst._defaults.secondMax ? tp_inst._defaults.secondMax : tp_inst._defaults.second;
                tp_inst.millisec = tp_inst._defaults.millisec < tp_inst._defaults.millisecMin ? tp_inst._defaults.millisecMin :
                        tp_inst._defaults.millisec > tp_inst._defaults.millisecMax ? tp_inst._defaults.millisecMax : tp_inst._defaults.millisec;
                tp_inst.microsec = tp_inst._defaults.microsec < tp_inst._defaults.microsecMin ? tp_inst._defaults.microsecMin :
                        tp_inst._defaults.microsec > tp_inst._defaults.microsecMax ? tp_inst._defaults.microsecMax : tp_inst._defaults.microsec;
                tp_inst.ampm = '';
                tp_inst.$input = $input;

                if (tp_inst._defaults.altField) {
                    tp_inst.$altInput = $(tp_inst._defaults.altField).css({
                        cursor: 'pointer'
                    }).focus(function() {
                        $input.trigger("focus");
                    });
                }

                if (tp_inst._defaults.minDate === 0 || tp_inst._defaults.minDateTime === 0) {
                    tp_inst._defaults.minDate = new Date();
                }
                if (tp_inst._defaults.maxDate === 0 || tp_inst._defaults.maxDateTime === 0) {
                    tp_inst._defaults.maxDate = new Date();
                }

// datepicker needs minDate/maxDate, timepicker needs minDateTime/maxDateTime..
                if (tp_inst._defaults.minDate !== undefined && tp_inst._defaults.minDate instanceof Date) {
                    tp_inst._defaults.minDateTime = new Date(tp_inst._defaults.minDate.getTime());
                }
                if (tp_inst._defaults.minDateTime !== undefined && tp_inst._defaults.minDateTime instanceof Date) {
                    tp_inst._defaults.minDate = new Date(tp_inst._defaults.minDateTime.getTime());
                }
                if (tp_inst._defaults.maxDate !== undefined && tp_inst._defaults.maxDate instanceof Date) {
                    tp_inst._defaults.maxDateTime = new Date(tp_inst._defaults.maxDate.getTime());
                }
                if (tp_inst._defaults.maxDateTime !== undefined && tp_inst._defaults.maxDateTime instanceof Date) {
                    tp_inst._defaults.maxDate = new Date(tp_inst._defaults.maxDateTime.getTime());
                }
                tp_inst.$input.bind('focus', function() {
                    tp_inst._onFocus();
                });

                return tp_inst;
            },
            /*
             * add our sliders to the calendar
             */
            _addTimePicker: function(dp_inst) {
                var currDT = (this.$altInput && this._defaults.altFieldTimeOnly) ? this.$input.val() + ' ' + this.$altInput.val() : this.$input.val();

                this.timeDefined = this._parseTime(currDT);
                this._limitMinMaxDateTime(dp_inst, false);
                this._injectTimePicker();
            },
            /*
             * parse the time string from input value or _setTime
             */
            _parseTime: function(timeString, withDate) {
                if (!this.inst) {
                    this.inst = $.datepicker._getInst(this.$input[0]);
                }

                if (withDate || !this._defaults.timeOnly) {
                    var dp_dateFormat = $.datepicker._get(this.inst, 'dateFormat');
                    try {
                        var parseRes = parseDateTimeInternal(dp_dateFormat, this._defaults.timeFormat, timeString, $.datepicker._getFormatConfig(this.inst), this._defaults);
                        if (!parseRes.timeObj) {
                            return false;
                        }
                        $.extend(this, parseRes.timeObj);
                    } catch (err) {
                        $.timepicker.log("Error parsing the date/time string: " + err +
                                "\ndate/time string = " + timeString +
                                "\ntimeFormat = " + this._defaults.timeFormat +
                                "\ndateFormat = " + dp_dateFormat);
                        return false;
                    }
                    return true;
                } else {
                    var timeObj = $.datepicker.parseTime(this._defaults.timeFormat, timeString, this._defaults);
                    if (!timeObj) {
                        return false;
                    }
                    $.extend(this, timeObj);
                    return true;
                }
            },
            /*
             * generate and inject html for timepicker into ui datepicker
             */
            _injectTimePicker: function() {
                var $dp = this.inst.dpDiv,
                        o = this.inst.settings,
                        tp_inst = this,
                        litem = '',
                        uitem = '',
                        show = null,
                        max = {},
                        gridSize = {},
                        size = null,
                        i = 0,
                        l = 0;

// Prevent displaying twice
                if ($dp.find("div.ui-timepicker-div").length === 0 && o.showTimepicker) {
                    var noDisplay = ' style="display:none;"',
                            html = '<div class="ui-timepicker-div' + (o.isRTL ? ' ui-timepicker-rtl' : '') + '"><dl>' + '<dt class="ui_tpicker_time_label"' + ((o.showTime) ? '' : noDisplay) + '>' + o.timeText + '</dt>' +
                            '<dd class="ui_tpicker_time"' + ((o.showTime) ? '' : noDisplay) + '></dd>';

// Create the markup
                    for (i = 0, l = this.units.length; i < l; i++) {
                        litem = this.units[i];
                        uitem = litem.substr(0, 1).toUpperCase() + litem.substr(1);
                        show = o['show' + uitem] !== null ? o['show' + uitem] : this.support[litem];

// Added by Peter Medeiros:
// - Figure out what the hour/minute/second max should be based on the step values.
// - Example: if stepMinute is 15, then minMax is 45.
                        max[litem] = parseInt((o[litem + 'Max'] - ((o[litem + 'Max'] - o[litem + 'Min']) % o['step' + uitem])), 10);
                        gridSize[litem] = 0;

                        html += '<dt class="ui_tpicker_' + litem + '_label"' + (show ? '' : noDisplay) + '>' + o[litem + 'Text'] + '</dt>' +
                                '<dd class="ui_tpicker_' + litem + '"><div class="ui_tpicker_' + litem + '_slider"' + (show ? '' : noDisplay) + '></div>';

                        if (show && o[litem + 'Grid'] > 0) {
                            html += '<div style="padding-left: 1px"><table class="ui-tpicker-grid-label"><tr>';

                            if (litem == 'hour') {
                                for (var h = o[litem + 'Min']; h <= max[litem]; h += parseInt(o[litem + 'Grid'], 10)) {
                                    gridSize[litem]++;
                                    var tmph = $.datepicker.formatTime(this.support.ampm ? 'hht' : 'HH', {hour: h}, o);
                                    html += '<td data-for="' + litem + '">' + tmph + '</td>';
                                }
                            }
                            else {
                                for (var m = o[litem + 'Min']; m <= max[litem]; m += parseInt(o[litem + 'Grid'], 10)) {
                                    gridSize[litem]++;
                                    html += '<td data-for="' + litem + '">' + ((m < 10) ? '0' : '') + m + '</td>';
                                }
                            }

                            html += '</tr></table></div>';
                        }
                        html += '</dd>';
                    }

// Timezone
                    var showTz = o.showTimezone !== null ? o.showTimezone : this.support.timezone;
                    html += '<dt class="ui_tpicker_timezone_label"' + (showTz ? '' : noDisplay) + '>' + o.timezoneText + '</dt>';
                    html += '<dd class="ui_tpicker_timezone" ' + (showTz ? '' : noDisplay) + '></dd>';

// Create the elements from string
                    html += '</dl></div>';
                    var $tp = $(html);

// if we only want time picker...
                    if (o.timeOnly === true) {
                        $tp.prepend('<div class="ui-widget-header ui-helper-clearfix ui-corner-all">' + '<div class="ui-datepicker-title">' + o.timeOnlyTitle + '</div>' + '</div>');
                        $dp.find('.ui-datepicker-header, .ui-datepicker-calendar').hide();
                    }

// add sliders, adjust grids, add events
                    for (i = 0, l = tp_inst.units.length; i < l; i++) {
                        litem = tp_inst.units[i];
                        uitem = litem.substr(0, 1).toUpperCase() + litem.substr(1);
                        show = o['show' + uitem] !== null ? o['show' + uitem] : this.support[litem];

// add the slider
                        tp_inst[litem + '_slider'] = tp_inst.control.create(tp_inst, $tp.find('.ui_tpicker_' + litem + '_slider'), litem, tp_inst[litem], o[litem + 'Min'], max[litem], o['step' + uitem]);

// adjust the grid and add click event
                        if (show && o[litem + 'Grid'] > 0) {
                            size = 100 * gridSize[litem] * o[litem + 'Grid'] / (max[litem] - o[litem + 'Min']);
                            $tp.find('.ui_tpicker_' + litem + ' table').css({
                                width: size + "%",
                                marginLeft: o.isRTL ? '0' : ((size / (-2 * gridSize[litem])) + "%"),
                                marginRight: o.isRTL ? ((size / (-2 * gridSize[litem])) + "%") : '0',
                                borderCollapse: 'collapse'
                            }).find("td").click(function(e) {
                                var $t = $(this),
                                        h = $t.html(),
                                        n = parseInt(h.replace(/[^0-9]/g), 10),
                                        ap = h.replace(/[^apm]/ig),
                                        f = $t.data('for'); // loses scope, so we use data-for

                                if (f == 'hour') {
                                    if (ap.indexOf('p') !== -1 && n < 12) {
                                        n += 12;
                                    }
                                    else {
                                        if (ap.indexOf('a') !== -1 && n === 12) {
                                            n = 0;
                                        }
                                    }
                                }

                                tp_inst.control.value(tp_inst, tp_inst[f + '_slider'], litem, n);

                                tp_inst._onTimeChange();
                                tp_inst._onSelectHandler();
                            }).css({
                                cursor: 'pointer',
                                width: (100 / gridSize[litem]) + '%',
                                textAlign: 'center',
                                overflow: 'hidden'
                            });
                        } // end if grid > 0
                    } // end for loop

// Add timezone options
                    this.timezone_select = $tp.find('.ui_tpicker_timezone').append('<select></select>').find("select");
                    $.fn.append.apply(this.timezone_select,
                            $.map(o.timezoneList, function(val, idx) {
                                return $("<option />").val(typeof val == "object" ? val.value : val).text(typeof val == "object" ? val.label : val);
                            }));
                    if (typeof (this.timezone) != "undefined" && this.timezone !== null && this.timezone !== "") {
                        var local_timezone = (new Date(this.inst.selectedYear, this.inst.selectedMonth, this.inst.selectedDay, 12)).getTimezoneOffset() * -1;
                        if (local_timezone == this.timezone) {
                            selectLocalTimezone(tp_inst);
                        } else {
                            this.timezone_select.val(this.timezone);
                        }
                    } else {
                        if (typeof (this.hour) != "undefined" && this.hour !== null && this.hour !== "") {
                            this.timezone_select.val(o.timezone);
                        } else {
                            selectLocalTimezone(tp_inst);
                        }
                    }
                    this.timezone_select.change(function() {
                        tp_inst._onTimeChange();
                        tp_inst._onSelectHandler();
                    });
// End timezone options

// inject timepicker into datepicker
                    var $buttonPanel = $dp.find('.ui-datepicker-buttonpane');
                    if ($buttonPanel.length) {
                        $buttonPanel.before($tp);
                    } else {
                        $dp.append($tp);
                    }

                    this.$timeObj = $tp.find('.ui_tpicker_time');

                    if (this.inst !== null) {
                        var timeDefined = this.timeDefined;
                        this._onTimeChange();
                        this.timeDefined = timeDefined;
                    }

// slideAccess integration: http://trentrichardson.com/2011/11/11/jquery-ui-sliders-and-touch-accessibility/
                    if (this._defaults.addSliderAccess) {
                        var sliderAccessArgs = this._defaults.sliderAccessArgs,
                                rtl = this._defaults.isRTL;
                        sliderAccessArgs.isRTL = rtl;

                        setTimeout(function() { // fix for inline mode
                            if ($tp.find('.ui-slider-access').length === 0) {
                                $tp.find('.ui-slider:visible').sliderAccess(sliderAccessArgs);

// fix any grids since sliders are shorter
                                var sliderAccessWidth = $tp.find('.ui-slider-access:eq(0)').outerWidth(true);
                                if (sliderAccessWidth) {
                                    $tp.find('table:visible').each(function() {
                                        var $g = $(this),
                                                oldWidth = $g.outerWidth(),
                                                oldMarginLeft = $g.css(rtl ? 'marginRight' : 'marginLeft').toString().replace('%', ''),
                                                newWidth = oldWidth - sliderAccessWidth,
                                                newMarginLeft = ((oldMarginLeft * newWidth) / oldWidth) + '%',
                                                css = {width: newWidth, marginRight: 0, marginLeft: 0};
                                        css[rtl ? 'marginRight' : 'marginLeft'] = newMarginLeft;
                                        $g.css(css);
                                    });
                                }
                            }
                        }, 10);
                    }
// end slideAccess integration

                    tp_inst._limitMinMaxDateTime(this.inst, true);
                }
            },
            /*
             * This function tries to limit the ability to go outside the
             * min/max date range
             */
            _limitMinMaxDateTime: function(dp_inst, adjustSliders) {
                var o = this._defaults,
                        dp_date = new Date(dp_inst.selectedYear, dp_inst.selectedMonth, dp_inst.selectedDay);

                if (!this._defaults.showTimepicker) {
                    return;
                } // No time so nothing to check here

                if ($.datepicker._get(dp_inst, 'minDateTime') !== null && $.datepicker._get(dp_inst, 'minDateTime') !== undefined && dp_date) {
                    var minDateTime = $.datepicker._get(dp_inst, 'minDateTime'),
                            minDateTimeDate = new Date(minDateTime.getFullYear(), minDateTime.getMonth(), minDateTime.getDate(), 0, 0, 0, 0);

                    if (this.hourMinOriginal === null || this.minuteMinOriginal === null || this.secondMinOriginal === null || this.millisecMinOriginal === null || this.microsecMinOriginal === null) {
                        this.hourMinOriginal = o.hourMin;
                        this.minuteMinOriginal = o.minuteMin;
                        this.secondMinOriginal = o.secondMin;
                        this.millisecMinOriginal = o.millisecMin;
                        this.microsecMinOriginal = o.microsecMin;
                    }

                    if (dp_inst.settings.timeOnly || minDateTimeDate.getTime() == dp_date.getTime()) {
                        this._defaults.hourMin = minDateTime.getHours();
                        if (this.hour <= this._defaults.hourMin) {
                            this.hour = this._defaults.hourMin;
                            this._defaults.minuteMin = minDateTime.getMinutes();
                            if (this.minute <= this._defaults.minuteMin) {
                                this.minute = this._defaults.minuteMin;
                                this._defaults.secondMin = minDateTime.getSeconds();
                                if (this.second <= this._defaults.secondMin) {
                                    this.second = this._defaults.secondMin;
                                    this._defaults.millisecMin = minDateTime.getMilliseconds();
                                    if (this.millisec <= this._defaults.millisecMin) {
                                        this.millisec = this._defaults.millisecMin;
                                        this._defaults.microsecMin = minDateTime.getMicroseconds();
                                    } else {
                                        if (this.microsec < this._defaults.microsecMin) {
                                            this.microsec = this._defaults.microsecMin;
                                        }
                                        this._defaults.microsecMin = this.microsecMinOriginal;
                                    }
                                } else {
                                    this._defaults.millisecMin = this.millisecMinOriginal;
                                    this._defaults.microsecMin = this.microsecMinOriginal;
                                }
                            } else {
                                this._defaults.secondMin = this.secondMinOriginal;
                                this._defaults.millisecMin = this.millisecMinOriginal;
                                this._defaults.microsecMin = this.microsecMinOriginal;
                            }
                        } else {
                            this._defaults.minuteMin = this.minuteMinOriginal;
                            this._defaults.secondMin = this.secondMinOriginal;
                            this._defaults.millisecMin = this.millisecMinOriginal;
                            this._defaults.microsecMin = this.microsecMinOriginal;
                        }
                    } else {
                        this._defaults.hourMin = this.hourMinOriginal;
                        this._defaults.minuteMin = this.minuteMinOriginal;
                        this._defaults.secondMin = this.secondMinOriginal;
                        this._defaults.millisecMin = this.millisecMinOriginal;
                        this._defaults.microsecMin = this.microsecMinOriginal;
                    }
                }

                if ($.datepicker._get(dp_inst, 'maxDateTime') !== null && $.datepicker._get(dp_inst, 'maxDateTime') !== undefined && dp_date) {
                    var maxDateTime = $.datepicker._get(dp_inst, 'maxDateTime'),
                            maxDateTimeDate = new Date(maxDateTime.getFullYear(), maxDateTime.getMonth(), maxDateTime.getDate(), 0, 0, 0, 0);

                    if (this.hourMaxOriginal === null || this.minuteMaxOriginal === null || this.secondMaxOriginal === null || this.millisecMaxOriginal === null) {
                        this.hourMaxOriginal = o.hourMax;
                        this.minuteMaxOriginal = o.minuteMax;
                        this.secondMaxOriginal = o.secondMax;
                        this.millisecMaxOriginal = o.millisecMax;
                        this.microsecMaxOriginal = o.microsecMax;
                    }

                    if (dp_inst.settings.timeOnly || maxDateTimeDate.getTime() == dp_date.getTime()) {
                        this._defaults.hourMax = maxDateTime.getHours();
                        if (this.hour >= this._defaults.hourMax) {
                            this.hour = this._defaults.hourMax;
                            this._defaults.minuteMax = maxDateTime.getMinutes();
                            if (this.minute >= this._defaults.minuteMax) {
                                this.minute = this._defaults.minuteMax;
                                this._defaults.secondMax = maxDateTime.getSeconds();
                                if (this.second >= this._defaults.secondMax) {
                                    this.second = this._defaults.secondMax;
                                    this._defaults.millisecMax = maxDateTime.getMilliseconds();
                                    if (this.millisec >= this._defaults.millisecMax) {
                                        this.millisec = this._defaults.millisecMax;
                                        this._defaults.microsecMax = maxDateTime.getMicroseconds();
                                    } else {
                                        if (this.microsec > this._defaults.microsecMax) {
                                            this.microsec = this._defaults.microsecMax;
                                        }
                                        this._defaults.microsecMax = this.microsecMaxOriginal;
                                    }
                                } else {
                                    this._defaults.millisecMax = this.millisecMaxOriginal;
                                    this._defaults.microsecMax = this.microsecMaxOriginal;
                                }
                            } else {
                                this._defaults.secondMax = this.secondMaxOriginal;
                                this._defaults.millisecMax = this.millisecMaxOriginal;
                                this._defaults.microsecMax = this.microsecMaxOriginal;
                            }
                        } else {
                            this._defaults.minuteMax = this.minuteMaxOriginal;
                            this._defaults.secondMax = this.secondMaxOriginal;
                            this._defaults.millisecMax = this.millisecMaxOriginal;
                            this._defaults.microsecMax = this.microsecMaxOriginal;
                        }
                    } else {
                        this._defaults.hourMax = this.hourMaxOriginal;
                        this._defaults.minuteMax = this.minuteMaxOriginal;
                        this._defaults.secondMax = this.secondMaxOriginal;
                        this._defaults.millisecMax = this.millisecMaxOriginal;
                        this._defaults.microsecMax = this.microsecMaxOriginal;
                    }
                }

                if (adjustSliders !== undefined && adjustSliders === true) {
                    var hourMax = parseInt((this._defaults.hourMax - ((this._defaults.hourMax - this._defaults.hourMin) % this._defaults.stepHour)), 10),
                            minMax = parseInt((this._defaults.minuteMax - ((this._defaults.minuteMax - this._defaults.minuteMin) % this._defaults.stepMinute)), 10),
                            secMax = parseInt((this._defaults.secondMax - ((this._defaults.secondMax - this._defaults.secondMin) % this._defaults.stepSecond)), 10),
                            millisecMax = parseInt((this._defaults.millisecMax - ((this._defaults.millisecMax - this._defaults.millisecMin) % this._defaults.stepMillisec)), 10);
                    microsecMax = parseInt((this._defaults.microsecMax - ((this._defaults.microsecMax - this._defaults.microsecMin) % this._defaults.stepMicrosec)), 10);

                    if (this.hour_slider) {
                        this.control.options(this, this.hour_slider, 'hour', {min: this._defaults.hourMin, max: hourMax});
                        this.control.value(this, this.hour_slider, 'hour', this.hour - (this.hour % this._defaults.stepHour));
                    }
                    if (this.minute_slider) {
                        this.control.options(this, this.minute_slider, 'minute', {min: this._defaults.minuteMin, max: minMax});
                        this.control.value(this, this.minute_slider, 'minute', this.minute - (this.minute % this._defaults.stepMinute));
                    }
                    if (this.second_slider) {
                        this.control.options(this, this.second_slider, 'second', {min: this._defaults.secondMin, max: secMax});
                        this.control.value(this, this.second_slider, 'second', this.second - (this.second % this._defaults.stepSecond));
                    }
                    if (this.millisec_slider) {
                        this.control.options(this, this.millisec_slider, 'millisec', {min: this._defaults.millisecMin, max: millisecMax});
                        this.control.value(this, this.millisec_slider, 'millisec', this.millisec - (this.millisec % this._defaults.stepMillisec));
                    }
                    if (this.microsec_slider) {
                        this.control.options(this, this.microsec_slider, 'microsec', {min: this._defaults.microsecMin, max: microsecMax});
                        this.control.value(this, this.microsec_slider, 'microsec', this.microsec - (this.microsec % this._defaults.stepMicrosec));
                    }
                }

            },
            /*
             * when a slider moves, set the internal time...
             * on time change is also called when the time is updated in the text field
             */
            _onTimeChange: function() {
                var hour = (this.hour_slider) ? this.control.value(this, this.hour_slider, 'hour') : false,
                        minute = (this.minute_slider) ? this.control.value(this, this.minute_slider, 'minute') : false,
                        second = (this.second_slider) ? this.control.value(this, this.second_slider, 'second') : false,
                        millisec = (this.millisec_slider) ? this.control.value(this, this.millisec_slider, 'millisec') : false,
                        microsec = (this.microsec_slider) ? this.control.value(this, this.microsec_slider, 'microsec') : false,
                        timezone = (this.timezone_select) ? this.timezone_select.val() : false,
                        o = this._defaults,
                        pickerTimeFormat = o.pickerTimeFormat || o.timeFormat,
                        pickerTimeSuffix = o.pickerTimeSuffix || o.timeSuffix;

                if (typeof (hour) == 'object') {
                    hour = false;
                }
                if (typeof (minute) == 'object') {
                    minute = false;
                }
                if (typeof (second) == 'object') {
                    second = false;
                }
                if (typeof (millisec) == 'object') {
                    millisec = false;
                }
                if (typeof (microsec) == 'object') {
                    microsec = false;
                }
                if (typeof (timezone) == 'object') {
                    timezone = false;
                }

                if (hour !== false) {
                    hour = parseInt(hour, 10);
                }
                if (minute !== false) {
                    minute = parseInt(minute, 10);
                }
                if (second !== false) {
                    second = parseInt(second, 10);
                }
                if (millisec !== false) {
                    millisec = parseInt(millisec, 10);
                }
                if (microsec !== false) {
                    microsec = parseInt(microsec, 10);
                }

                var ampm = o[hour < 12 ? 'amNames' : 'pmNames'][0];

// If the update was done in the input field, the input field should not be updated.
// If the update was done using the sliders, update the input field.
                var hasChanged = (hour != this.hour || minute != this.minute || second != this.second || millisec != this.millisec || microsec != this.microsec
                        || (this.ampm.length > 0 && (hour < 12) != ($.inArray(this.ampm.toUpperCase(), this.amNames) !== -1))
                        || (this.timezone !== null && timezone != this.timezone));

                if (hasChanged) {

                    if (hour !== false) {
                        this.hour = hour;
                    }
                    if (minute !== false) {
                        this.minute = minute;
                    }
                    if (second !== false) {
                        this.second = second;
                    }
                    if (millisec !== false) {
                        this.millisec = millisec;
                    }
                    if (microsec !== false) {
                        this.microsec = microsec;
                    }
                    if (timezone !== false) {
                        this.timezone = timezone;
                    }

                    if (!this.inst) {
                        this.inst = $.datepicker._getInst(this.$input[0]);
                    }

                    this._limitMinMaxDateTime(this.inst, true);
                }
                if (this.support.ampm) {
                    this.ampm = ampm;
                }

// Updates the time within the timepicker
                this.formattedTime = $.datepicker.formatTime(o.timeFormat, this, o);
                if (this.$timeObj) {
                    if (pickerTimeFormat === o.timeFormat) {
                        this.$timeObj.text(this.formattedTime + pickerTimeSuffix);
                    }
                    else {
                        this.$timeObj.text($.datepicker.formatTime(pickerTimeFormat, this, o) + pickerTimeSuffix);
                    }
                }

                this.timeDefined = true;
                if (hasChanged) {
                    this._updateDateTime();
                }
            },
            /*
             * call custom onSelect.
             * bind to sliders slidestop, and grid click.
             */
            _onSelectHandler: function() {
                var onSelect = this._defaults.onSelect || this.inst.settings.onSelect;
                var inputEl = this.$input ? this.$input[0] : null;
                if (onSelect && inputEl) {
                    onSelect.apply(inputEl, [this.formattedDateTime, this]);
                }
            },
            /*
             * update our input with the new date time..
             */
            _updateDateTime: function(dp_inst) {
                dp_inst = this.inst || dp_inst;
//var dt = $.datepicker._daylightSavingAdjust(new Date(dp_inst.selectedYear, dp_inst.selectedMonth, dp_inst.selectedDay)),
                var dt = $.datepicker._daylightSavingAdjust(new Date(dp_inst.currentYear, dp_inst.currentMonth, dp_inst.currentDay)),
                        dateFmt = $.datepicker._get(dp_inst, 'dateFormat'),
                        formatCfg = $.datepicker._getFormatConfig(dp_inst),
                        timeAvailable = dt !== null && this.timeDefined;
                this.formattedDate = $.datepicker.formatDate(dateFmt, (dt === null ? new Date() : dt), formatCfg);
                var formattedDateTime = this.formattedDate;

// if a slider was changed but datepicker doesn't have a value yet, set it
                if (dp_inst.lastVal === "") {
                    dp_inst.currentYear = dp_inst.selectedYear;
                    dp_inst.currentMonth = dp_inst.selectedMonth;
                    dp_inst.currentDay = dp_inst.selectedDay;
                }

                /*
                 * remove following lines to force every changes in date picker to change the input value
                 * Bug descriptions: when an input field has a default value, and click on the field to pop up the date picker.
                 * If the user manually empty the value in the input field, the date picker will never change selected value.
                 */
//if (dp_inst.lastVal !== undefined && (dp_inst.lastVal.length > 0 && this.$input.val().length === 0)) {
// return;
//}

                if (this._defaults.timeOnly === true) {
                    formattedDateTime = this.formattedTime;
                } else if (this._defaults.timeOnly !== true && (this._defaults.alwaysSetTime || timeAvailable)) {
                    formattedDateTime += this._defaults.separator + this.formattedTime + this._defaults.timeSuffix;
                }

                this.formattedDateTime = formattedDateTime;

                if (!this._defaults.showTimepicker) {
                    this.$input.val(this.formattedDate);
                } else if (this.$altInput && this._defaults.timeOnly === false && this._defaults.altFieldTimeOnly === true) {
                    this.$altInput.val(this.formattedTime);
                    this.$input.val(this.formattedDate);
                } else if (this.$altInput) {
                    this.$input.val(formattedDateTime);
                    var altFormattedDateTime = '',
                            altSeparator = this._defaults.altSeparator ? this._defaults.altSeparator : this._defaults.separator,
                            altTimeSuffix = this._defaults.altTimeSuffix ? this._defaults.altTimeSuffix : this._defaults.timeSuffix;

                    if (!this._defaults.timeOnly) {
                        if (this._defaults.altFormat) {
                            altFormattedDateTime = $.datepicker.formatDate(this._defaults.altFormat, (dt === null ? new Date() : dt), formatCfg);
                        }
                        else {
                            altFormattedDateTime = this.formattedDate;
                        }

                        if (altFormattedDateTime) {
                            altFormattedDateTime += altSeparator;
                        }
                    }

                    if (this._defaults.altTimeFormat) {
                        altFormattedDateTime += $.datepicker.formatTime(this._defaults.altTimeFormat, this, this._defaults) + altTimeSuffix;
                    }
                    else {
                        altFormattedDateTime += this.formattedTime + altTimeSuffix;
                    }
                    this.$altInput.val(altFormattedDateTime);
                } else {
                    this.$input.val(formattedDateTime);
                }

                this.$input.trigger("change");
            },
            _onFocus: function() {
                if (!this.$input.val() && this._defaults.defaultValue) {
                    this.$input.val(this._defaults.defaultValue);
                    var inst = $.datepicker._getInst(this.$input.get(0)),
                            tp_inst = $.datepicker._get(inst, 'timepicker');
                    if (tp_inst) {
                        if (tp_inst._defaults.timeOnly && (inst.input.val() != inst.lastVal)) {
                            try {
                                $.datepicker._updateDatepicker(inst);
                            } catch (err) {
                                $.timepicker.log(err);
                            }
                        }
                    }
                }
            },
            /*
             * Small abstraction to control types
             * We can add more, just be sure to follow the pattern: create, options, value
             */
            _controls: {
// slider methods
                slider: {
                    create: function(tp_inst, obj, unit, val, min, max, step) {
                        var rtl = tp_inst._defaults.isRTL; // if rtl go -60->0 instead of 0->60
                        return obj.prop('slide', null).slider({
                            orientation: "horizontal",
                            value: rtl ? val * -1 : val,
                            min: rtl ? max * -1 : min,
                            max: rtl ? min * -1 : max,
                            step: step,
                            slide: function(event, ui) {
                                tp_inst.control.value(tp_inst, $(this), unit, rtl ? ui.value * -1 : ui.value);
                                tp_inst._onTimeChange();
                            },
                            stop: function(event, ui) {
                                tp_inst._onSelectHandler();
                            }
                        });
                    },
                    options: function(tp_inst, obj, unit, opts, val) {
                        if (tp_inst._defaults.isRTL) {
                            if (typeof (opts) == 'string') {
                                if (opts == 'min' || opts == 'max') {
                                    if (val !== undefined) {
                                        return obj.slider(opts, val * -1);
                                    }
                                    return Math.abs(obj.slider(opts));
                                }
                                return obj.slider(opts);
                            }
                            var min = opts.min,
                                    max = opts.max;
                            opts.min = opts.max = null;
                            if (min !== undefined) {
                                opts.max = min * -1;
                            }
                            if (max !== undefined) {
                                opts.min = max * -1;
                            }
                            return obj.slider(opts);
                        }
                        if (typeof (opts) == 'string' && val !== undefined) {
                            return obj.slider(opts, val);
                        }
                        return obj.slider(opts);
                    },
                    value: function(tp_inst, obj, unit, val) {
                        if (tp_inst._defaults.isRTL) {
                            if (val !== undefined) {
                                return obj.slider('value', val * -1);
                            }
                            return Math.abs(obj.slider('value'));
                        }
                        if (val !== undefined) {
                            return obj.slider('value', val);
                        }
                        return obj.slider('value');
                    }
                },
// select methods
                select: {
                    create: function(tp_inst, obj, unit, val, min, max, step) {
                        var sel = '<select class="ui-timepicker-select" data-unit="' + unit + '" data-min="' + min + '" data-max="' + max + '" data-step="' + step + '">',
                                format = tp_inst._defaults.pickerTimeFormat || tp_inst._defaults.timeFormat;

                        for (var i = min; i <= max; i += step) {
                            sel += '<option value="' + i + '"' + (i == val ? ' selected' : '') + '>';
                            if (unit == 'hour') {
                                sel += $.datepicker.formatTime($.trim(format.replace(/[^ht ]/ig, '')), {hour: i}, tp_inst._defaults);
                            }
                            else if (unit == 'millisec' || unit == 'microsec' || i >= 10) {
                                sel += i;
                            }
                            else {
                                sel += '0' + i.toString();
                            }
                            sel += '</option>';
                        }
                        sel += '</select>';

                        obj.children('select').remove();

                        $(sel).appendTo(obj).change(function(e) {
                            tp_inst._onTimeChange();
                            tp_inst._onSelectHandler();
                        });

                        return obj;
                    },
                    options: function(tp_inst, obj, unit, opts, val) {
                        var o = {},
                                $t = obj.children('select');
                        if (typeof (opts) == 'string') {
                            if (val === undefined) {
                                return $t.data(opts);
                            }
                            o[opts] = val;
                        }
                        else {
                            o = opts;
                        }
                        return tp_inst.control.create(tp_inst, obj, $t.data('unit'), $t.val(), o.min || $t.data('min'), o.max || $t.data('max'), o.step || $t.data('step'));
                    },
                    value: function(tp_inst, obj, unit, val) {
                        var $t = obj.children('select');
                        if (val !== undefined) {
                            return $t.val(val);
                        }
                        return $t.val();
                    }
                }
            } // end _controls

        });

        $.fn.extend({
            /*
             * shorthand just to use timepicker..
             */
            timepicker: function(o) {
                o = o || {};
                var tmp_args = Array.prototype.slice.call(arguments);

                if (typeof o == 'object') {
                    tmp_args[0] = $.extend(o, {
                        timeOnly: true
                    });
                }

                return $(this).each(function() {
                    $.fn.datetimepicker.apply($(this), tmp_args);
                });
            },
            /*
             * extend timepicker to datepicker
             */
            datetimepicker: function(o) {
                o = o || {};
                var tmp_args = arguments;

                if (typeof (o) == 'string') {
                    if (o == 'getDate') {
                        return $.fn.datepicker.apply($(this[0]), tmp_args);
                    } else {
                        return this.each(function() {
                            var $t = $(this);
                            $t.datepicker.apply($t, tmp_args);
                        });
                    }
                } else {
                    return this.each(function() {
                        var $t = $(this);
                        $t.datepicker($.timepicker._newInst($t, o)._defaults);
                    });
                }
            }
        });

        /*
         * Public Utility to parse date and time
         */
        $.datepicker.parseDateTime = function(dateFormat, timeFormat, dateTimeString, dateSettings, timeSettings) {
            var parseRes = parseDateTimeInternal(dateFormat, timeFormat, dateTimeString, dateSettings, timeSettings);
            if (parseRes.timeObj) {
                var t = parseRes.timeObj;
                parseRes.date.setHours(t.hour, t.minute, t.second, t.millisec);
                parseRes.date.setMicroseconds(t.microsec);
            }

            return parseRes.date;
        };

        /*
         * Public utility to parse time
         */
        $.datepicker.parseTime = function(timeFormat, timeString, options) {
            var o = extendRemove(extendRemove({}, $.timepicker._defaults), options || {}),
                    iso8601 = (timeFormat.replace(/\'.*?\'/g, '').indexOf('Z') !== -1);

// Strict parse requires the timeString to match the timeFormat exactly
            var strictParse = function(f, s, o) {

// pattern for standard and localized AM/PM markers
                var getPatternAmpm = function(amNames, pmNames) {
                    var markers = [];
                    if (amNames) {
                        $.merge(markers, amNames);
                    }
                    if (pmNames) {
                        $.merge(markers, pmNames);
                    }
                    markers = $.map(markers, function(val) {
                        return val.replace(/[.*+?|()\[\]{}\\]/g, '\\$&');
                    });
                    return '(' + markers.join('|') + ')?';
                };

// figure out position of time elements.. cause js cant do named captures
                var getFormatPositions = function(timeFormat) {
                    var finds = timeFormat.toLowerCase().match(/(h{1,2}|m{1,2}|s{1,2}|l{1}|c{1}|t{1,2}|z|'.*?')/g),
                            orders = {
                                h: -1,
                                m: -1,
                                s: -1,
                                l: -1,
                                c: -1,
                                t: -1,
                                z: -1
                            };

                    if (finds) {
                        for (var i = 0; i < finds.length; i++) {
                            if (orders[finds[i].toString().charAt(0)] == -1) {
                                orders[finds[i].toString().charAt(0)] = i + 1;
                            }
                        }
                    }
                    return orders;
                };

                var regstr = '^' + f.toString()
                        .replace(/([hH]{1,2}|mm?|ss?|[tT]{1,2}|[zZ]|[lc]|'.*?')/g, function(match) {
                            var ml = match.length;
                            switch (match.charAt(0).toLowerCase()) {
                                case 'h':
                                    return ml === 1 ? '(\\d?\\d)' : '(\\d{' + ml + '})';
                                case 'm':
                                    return ml === 1 ? '(\\d?\\d)' : '(\\d{' + ml + '})';
                                case 's':
                                    return ml === 1 ? '(\\d?\\d)' : '(\\d{' + ml + '})';
                                case 'l':
                                    return '(\\d?\\d?\\d)';
                                case 'c':
                                    return '(\\d?\\d?\\d)';
                                case 'z':
                                    return '(z|[-+]\\d\\d:?\\d\\d|\\S+)?';
                                case 't':
                                    return getPatternAmpm(o.amNames, o.pmNames);
                                default: // literal escaped in quotes
                                    return '(' + match.replace(/\'/g, "").replace(/(\.|\$|\^|\\|\/|\(|\)|\[|\]|\?|\+|\*)/g, function(m) {
                                        return "\\" + m;
                                    }) + ')?';
                            }
                        })
                        .replace(/\s/g, '\\s?') +
                        o.timeSuffix + '$',
                        order = getFormatPositions(f),
                        ampm = '',
                        treg;

                treg = s.match(new RegExp(regstr, 'i'));

                var resTime = {
                    hour: 0,
                    minute: 0,
                    second: 0,
                    millisec: 0,
                    microsec: 0
                };

                if (treg) {
                    if (order.t !== -1) {
                        if (treg[order.t] === undefined || treg[order.t].length === 0) {
                            ampm = '';
                            resTime.ampm = '';
                        } else {
                            ampm = $.inArray(treg[order.t].toUpperCase(), o.amNames) !== -1 ? 'AM' : 'PM';
                            resTime.ampm = o[ampm == 'AM' ? 'amNames' : 'pmNames'][0];
                        }
                    }

                    if (order.h !== -1) {
                        if (ampm == 'AM' && treg[order.h] == '12') {
                            resTime.hour = 0; // 12am = 0 hour
                        } else {
                            if (ampm == 'PM' && treg[order.h] != '12') {
                                resTime.hour = parseInt(treg[order.h], 10) + 12; // 12pm = 12 hour, any other pm = hour + 12
                            } else {
                                resTime.hour = Number(treg[order.h]);
                            }
                        }
                    }

                    if (order.m !== -1) {
                        resTime.minute = Number(treg[order.m]);
                    }
                    if (order.s !== -1) {
                        resTime.second = Number(treg[order.s]);
                    }
                    if (order.l !== -1) {
                        resTime.millisec = Number(treg[order.l]);
                    }
                    if (order.c !== -1) {
                        resTime.microsec = Number(treg[order.c]);
                    }
                    if (order.z !== -1 && treg[order.z] !== undefined) {
                        resTime.timezone = $.timepicker.timezoneOffsetNumber(treg[order.z]);
                    }


                    return resTime;
                }
                return false;
            };// end strictParse

// First try JS Date, if that fails, use strictParse
            var looseParse = function(f, s, o) {
                try {
                    var d = new Date('2012-01-01 ' + s);
                    if (isNaN(d.getTime())) {
                        d = new Date('2012-01-01T' + s);
                        if (isNaN(d.getTime())) {
                            d = new Date('01/01/2012 ' + s);
                            if (isNaN(d.getTime())) {
                                throw "Unable to parse time with native Date: " + s;
                            }
                        }
                    }

                    return {
                        hour: d.getHours(),
                        minute: d.getMinutes(),
                        second: d.getSeconds(),
                        millisec: d.getMilliseconds(),
                        microsec: d.getMicroseconds(),
                        timezone: d.getTimezoneOffset() * -1
                    };
                }
                catch (err) {
                    try {
                        return strictParse(f, s, o);
                    }
                    catch (err2) {
                        $.timepicker.log("Unable to parse \ntimeString: " + s + "\ntimeFormat: " + f);
                    }
                }
                return false;
            }; // end looseParse

            if (typeof o.parse === "function") {
                return o.parse(timeFormat, timeString, o);
            }
            if (o.parse === 'loose') {
                return looseParse(timeFormat, timeString, o);
            }
            return strictParse(timeFormat, timeString, o);
        };

        /*
         * Public utility to format the time
         * format = string format of the time
         * time = a {}, not a Date() for timezones
         * options = essentially the regional[].. amNames, pmNames, ampm
         */
        $.datepicker.formatTime = function(format, time, options) {
            options = options || {};
            options = $.extend({}, $.timepicker._defaults, options);
            time = $.extend({
                hour: 0,
                minute: 0,
                second: 0,
                millisec: 0,
                timezone: 0
            }, time);

            var tmptime = format,
                    ampmName = options.amNames[0],
                    hour = parseInt(time.hour, 10);

            if (hour > 11) {
                ampmName = options.pmNames[0];
            }

            tmptime = tmptime.replace(/(?:HH?|hh?|mm?|ss?|[tT]{1,2}|[zZ]|[lc]|('.*?'|".*?"))/g, function(match) {
                switch (match) {
                    case 'HH':
                        return ('0' + hour).slice(-2);
                    case 'H':
                        return hour;
                    case 'hh':
                        return ('0' + convert24to12(hour)).slice(-2);
                    case 'h':
                        return convert24to12(hour);
                    case 'mm':
                        return ('0' + time.minute).slice(-2);
                    case 'm':
                        return time.minute;
                    case 'ss':
                        return ('0' + time.second).slice(-2);
                    case 's':
                        return time.second;
                    case 'l':
                        return ('00' + time.millisec).slice(-3);
                    case 'c':
                        return ('00' + time.microsec).slice(-3);
                    case 'z':
                        return $.timepicker.timezoneOffsetString(time.timezone === null ? options.timezone : time.timezone, false);
                    case 'Z':
                        return $.timepicker.timezoneOffsetString(time.timezone === null ? options.timezone : time.timezone, true);
                    case 'T':
                        return ampmName.charAt(0).toUpperCase();
                    case 'TT':
                        return ampmName.toUpperCase();
                    case 't':
                        return ampmName.charAt(0).toLowerCase();
                    case 'tt':
                        return ampmName.toLowerCase();
                    default:
                        return match.replace(/\'/g, "") || "'";
                }
            });

            tmptime = $.trim(tmptime);
            return tmptime;
        };

        /*
         * the bad hack :/ override datepicker so it doesnt close on select
         // inspired: http://stackoverflow.com/questions/1252512/jquery-datepicker-prevent-closing-picker-when-clicking-a-date/1762378#1762378
         */
        $.datepicker._base_selectDate = $.datepicker._selectDate;
        $.datepicker._selectDate = function(id, dateStr) {
            var inst = this._getInst($(id)[0]),
                    tp_inst = this._get(inst, 'timepicker');

            if (tp_inst) {
                tp_inst._limitMinMaxDateTime(inst, true);
                inst.inline = inst.stay_open = true;
//This way the onSelect handler called from calendarpicker get the full dateTime
                this._base_selectDate(id, dateStr);
                inst.inline = inst.stay_open = false;
                this._notifyChange(inst);
                this._updateDatepicker(inst);
            } else {
                this._base_selectDate(id, dateStr);
            }
        };

        /*
         * second bad hack :/ override datepicker so it triggers an event when changing the input field
         * and does not redraw the datepicker on every selectDate event
         */
        $.datepicker._base_updateDatepicker = $.datepicker._updateDatepicker;
        $.datepicker._updateDatepicker = function(inst) {

// don't popup the datepicker if there is another instance already opened
            var input = inst.input[0];
            if ($.datepicker._curInst && $.datepicker._curInst != inst && $.datepicker._datepickerShowing && $.datepicker._lastInput != input) {
                return;
            }

            if (typeof (inst.stay_open) !== 'boolean' || inst.stay_open === false) {

                this._base_updateDatepicker(inst);

// Reload the time control when changing something in the input text field.
                var tp_inst = this._get(inst, 'timepicker');
                if (tp_inst) {
                    tp_inst._addTimePicker(inst);
                }
            }
        };

        /*
         * third bad hack :/ override datepicker so it allows spaces and colon in the input field
         */
        $.datepicker._base_doKeyPress = $.datepicker._doKeyPress;
        $.datepicker._doKeyPress = function(event) {
            var inst = $.datepicker._getInst(event.target),
                    tp_inst = $.datepicker._get(inst, 'timepicker');

            if (tp_inst) {
                if ($.datepicker._get(inst, 'constrainInput')) {
                    var ampm = tp_inst.support.ampm,
                            tz = tp_inst._defaults.showTimezone !== null ? tp_inst._defaults.showTimezone : tp_inst.support.timezone,
                            dateChars = $.datepicker._possibleChars($.datepicker._get(inst, 'dateFormat')),
                            datetimeChars = tp_inst._defaults.timeFormat.toString()
                            .replace(/[hms]/g, '')
                            .replace(/TT/g, ampm ? 'APM' : '')
                            .replace(/Tt/g, ampm ? 'AaPpMm' : '')
                            .replace(/tT/g, ampm ? 'AaPpMm' : '')
                            .replace(/T/g, ampm ? 'AP' : '')
                            .replace(/tt/g, ampm ? 'apm' : '')
                            .replace(/t/g, ampm ? 'ap' : '') +
                            " " + tp_inst._defaults.separator +
                            tp_inst._defaults.timeSuffix +
                            (tz ? tp_inst._defaults.timezoneList.join('') : '') +
                            (tp_inst._defaults.amNames.join('')) + (tp_inst._defaults.pmNames.join('')) +
                            dateChars,
                            chr = String.fromCharCode(event.charCode === undefined ? event.keyCode : event.charCode);
                    return event.ctrlKey || (chr < ' ' || !dateChars || datetimeChars.indexOf(chr) > -1);
                }
            }

            return $.datepicker._base_doKeyPress(event);
        };

        /*
         * Fourth bad hack :/ override _updateAlternate function used in inline mode to init altField
         */
        $.datepicker._base_updateAlternate = $.datepicker._updateAlternate;
        /* Update any alternate field to synchronise with the main field. */
        $.datepicker._updateAlternate = function(inst) {
            var tp_inst = this._get(inst, 'timepicker');
            if (tp_inst) {
                var altField = tp_inst._defaults.altField;
                if (altField) { // update alternate field too
                    var altFormat = tp_inst._defaults.altFormat || tp_inst._defaults.dateFormat,
                            date = this._getDate(inst),
                            formatCfg = $.datepicker._getFormatConfig(inst),
                            altFormattedDateTime = '',
                            altSeparator = tp_inst._defaults.altSeparator ? tp_inst._defaults.altSeparator : tp_inst._defaults.separator,
                            altTimeSuffix = tp_inst._defaults.altTimeSuffix ? tp_inst._defaults.altTimeSuffix : tp_inst._defaults.timeSuffix,
                            altTimeFormat = tp_inst._defaults.altTimeFormat !== null ? tp_inst._defaults.altTimeFormat : tp_inst._defaults.timeFormat;

                    altFormattedDateTime += $.datepicker.formatTime(altTimeFormat, tp_inst, tp_inst._defaults) + altTimeSuffix;
                    if (!tp_inst._defaults.timeOnly && !tp_inst._defaults.altFieldTimeOnly && date !== null) {
                        if (tp_inst._defaults.altFormat) {
                            altFormattedDateTime = $.datepicker.formatDate(tp_inst._defaults.altFormat, date, formatCfg) + altSeparator + altFormattedDateTime;
                        }
                        else {
                            altFormattedDateTime = tp_inst.formattedDate + altSeparator + altFormattedDateTime;
                        }
                    }
                    $(altField).val(altFormattedDateTime);
                }
            }
            else {
                $.datepicker._base_updateAlternate(inst);
            }
        };

        /*
         * Override key up event to sync manual input changes.
         */
        $.datepicker._base_doKeyUp = $.datepicker._doKeyUp;
        $.datepicker._doKeyUp = function(event) {
            var inst = $.datepicker._getInst(event.target),
                    tp_inst = $.datepicker._get(inst, 'timepicker');

            if (tp_inst) {
                if (tp_inst._defaults.timeOnly && (inst.input.val() != inst.lastVal)) {
                    try {
                        $.datepicker._updateDatepicker(inst);
                    } catch (err) {
                        $.timepicker.log(err);
                    }
                }
            }

            return $.datepicker._base_doKeyUp(event);
        };

        /*
         * override "Today" button to also grab the time.
         */
        $.datepicker._base_gotoToday = $.datepicker._gotoToday;
        $.datepicker._gotoToday = function(id) {
            var inst = this._getInst($(id)[0]),
                    $dp = inst.dpDiv;
            this._base_gotoToday(id);
            var tp_inst = this._get(inst, 'timepicker');
            selectLocalTimezone(tp_inst);
            var now = new Date();
            this._setTime(inst, now);
            $('.ui-datepicker-today', $dp).click();
        };

        /*
         * Disable & enable the Time in the datetimepicker
         */
        $.datepicker._disableTimepickerDatepicker = function(target) {
            var inst = this._getInst(target);
            if (!inst) {
                return;
            }

            var tp_inst = this._get(inst, 'timepicker');
            $(target).datepicker('getDate'); // Init selected[Year|Month|Day]
            if (tp_inst) {
                tp_inst._defaults.showTimepicker = false;
                tp_inst._updateDateTime(inst);
            }
        };

        $.datepicker._enableTimepickerDatepicker = function(target) {
            var inst = this._getInst(target);
            if (!inst) {
                return;
            }

            var tp_inst = this._get(inst, 'timepicker');
            $(target).datepicker('getDate'); // Init selected[Year|Month|Day]
            if (tp_inst) {
                tp_inst._defaults.showTimepicker = true;
                tp_inst._addTimePicker(inst); // Could be disabled on page load
                tp_inst._updateDateTime(inst);
            }
        };

        /*
         * Create our own set time function
         */
        $.datepicker._setTime = function(inst, date) {
            var tp_inst = this._get(inst, 'timepicker');
            if (tp_inst) {
                var defaults = tp_inst._defaults;

// calling _setTime with no date sets time to defaults
                tp_inst.hour = date ? date.getHours() : defaults.hour;
                tp_inst.minute = date ? date.getMinutes() : defaults.minute;
                tp_inst.second = date ? date.getSeconds() : defaults.second;
                tp_inst.millisec = date ? date.getMilliseconds() : defaults.millisec;
                tp_inst.microsec = date ? date.getMicroseconds() : defaults.microsec;

//check if within min/max times..
                tp_inst._limitMinMaxDateTime(inst, true);

                tp_inst._onTimeChange();
                tp_inst._updateDateTime(inst);
            }
        };

        /*
         * Create new public method to set only time, callable as $().datepicker('setTime', date)
         */
        $.datepicker._setTimeDatepicker = function(target, date, withDate) {
            var inst = this._getInst(target);
            if (!inst) {
                return;
            }

            var tp_inst = this._get(inst, 'timepicker');

            if (tp_inst) {
                this._setDateFromField(inst);
                var tp_date;
                if (date) {
                    if (typeof date == "string") {
                        tp_inst._parseTime(date, withDate);
                        tp_date = new Date();
                        tp_date.setHours(tp_inst.hour, tp_inst.minute, tp_inst.second, tp_inst.millisec);
                        tp_date.setMicroseconds(tp_inst.microsec);
                    } else {
                        tp_date = new Date(date.getTime());
                        tp_date.setMicroseconds(date.getMicroseconds());
                    }
                    if (tp_date.toString() == 'Invalid Date') {
                        tp_date = undefined;
                    }
                    this._setTime(inst, tp_date);
                }
            }

        };

        /*
         * override setDate() to allow setting time too within Date object
         */
        $.datepicker._base_setDateDatepicker = $.datepicker._setDateDatepicker;
        $.datepicker._setDateDatepicker = function(target, date) {
            var inst = this._getInst(target);
            if (!inst) {
                return;
            }

            if (typeof (date) === 'string') {
                date = new Date(date);
                if (!date.getTime()) {
                    $.timepicker.log("Error creating Date object from string.");
                }
            }

            var tp_inst = this._get(inst, 'timepicker');
            var tp_date;
            if (date instanceof Date) {
                tp_date = new Date(date.getTime());
                tp_date.setMicroseconds(date.getMicroseconds());
            } else {
                tp_date = date;
            }

// This is important if you are using the timezone option, javascript's Date
// object will only return the timezone offset for the current locale, so we
// adjust it accordingly. If not using timezone option this won't matter..
// If a timezone is different in tp, keep the timezone as is
            if (tp_inst) {
// look out for DST if tz wasn't specified
                if (!tp_inst.support.timezone && tp_inst._defaults.timezone === null) {
                    tp_inst.timezone = tp_date.getTimezoneOffset() * -1;
                }
                date = $.timepicker.timezoneAdjust(date, tp_inst.timezone);
                tp_date = $.timepicker.timezoneAdjust(tp_date, tp_inst.timezone);
            }

            this._updateDatepicker(inst);
            this._base_setDateDatepicker.apply(this, arguments);
            this._setTimeDatepicker(target, tp_date, true);
        };

        /*
         * override getDate() to allow getting time too within Date object
         */
        $.datepicker._base_getDateDatepicker = $.datepicker._getDateDatepicker;
        $.datepicker._getDateDatepicker = function(target, noDefault) {
            var inst = this._getInst(target);
            if (!inst) {
                return;
            }

            var tp_inst = this._get(inst, 'timepicker');

            if (tp_inst) {
// if it hasn't yet been defined, grab from field
                if (inst.lastVal === undefined) {
                    this._setDateFromField(inst, noDefault);
                }

                var date = this._getDate(inst);
                if (date && tp_inst._parseTime($(target).val(), tp_inst.timeOnly)) {
                    date.setHours(tp_inst.hour, tp_inst.minute, tp_inst.second, tp_inst.millisec);
                    date.setMicroseconds(tp_inst.microsec);

// This is important if you are using the timezone option, javascript's Date
// object will only return the timezone offset for the current locale, so we
// adjust it accordingly. If not using timezone option this won't matter..
                    if (tp_inst.timezone != null) {
// look out for DST if tz wasn't specified
                        if (!tp_inst.support.timezone && tp_inst._defaults.timezone === null) {
                            tp_inst.timezone = date.getTimezoneOffset() * -1;
                        }
                        date = $.timepicker.timezoneAdjust(date, tp_inst.timezone);
                    }
                }
                return date;
            }
            return this._base_getDateDatepicker(target, noDefault);
        };

        /*
         * override parseDate() because UI 1.8.14 throws an error about "Extra characters"
         * An option in datapicker to ignore extra format characters would be nicer.
         */
        $.datepicker._base_parseDate = $.datepicker.parseDate;
        $.datepicker.parseDate = function(format, value, settings) {
            var date;
            try {
                date = this._base_parseDate(format, value, settings);
            } catch (err) {
// Hack! The error message ends with a colon, a space, and
// the "extra" characters. We rely on that instead of
// attempting to perfectly reproduce the parsing algorithm.
                if (err.indexOf(":") >= 0) {
                    date = this._base_parseDate(format, value.substring(0, value.length - (err.length - err.indexOf(':') - 2)), settings);
                    $.timepicker.log("Error parsing the date string: " + err + "\ndate string = " + value + "\ndate format = " + format);
                } else {
                    throw err;
                }
            }
            return date;
        };

        /*
         * override formatDate to set date with time to the input
         */
        $.datepicker._base_formatDate = $.datepicker._formatDate;
        $.datepicker._formatDate = function(inst, day, month, year) {
            var tp_inst = this._get(inst, 'timepicker');
            if (tp_inst) {
                tp_inst._updateDateTime(inst);
                return tp_inst.$input.val();
            }
            return this._base_formatDate(inst);
        };

        /*
         * override options setter to add time to maxDate(Time) and minDate(Time). MaxDate
         */
        $.datepicker._base_optionDatepicker = $.datepicker._optionDatepicker;
        $.datepicker._optionDatepicker = function(target, name, value) {
            var inst = this._getInst(target),
                    name_clone;
            if (!inst) {
                return null;
            }

            var tp_inst = this._get(inst, 'timepicker');
            if (tp_inst) {
                var min = null,
                        max = null,
                        onselect = null,
                        overrides = tp_inst._defaults.evnts,
                        fns = {},
                        prop;
                if (typeof name == 'string') { // if min/max was set with the string
                    if (name === 'minDate' || name === 'minDateTime') {
                        min = value;
                    } else if (name === 'maxDate' || name === 'maxDateTime') {
                        max = value;
                    } else if (name === 'onSelect') {
                        onselect = value;
                    } else if (overrides.hasOwnProperty(name)) {
                        if (typeof (value) === 'undefined') {
                            return overrides[name];
                        }
                        fns[name] = value;
                        name_clone = {}; //empty results in exiting function after overrides updated
                    }
                } else if (typeof name == 'object') { //if min/max was set with the JSON
                    if (name.minDate) {
                        min = name.minDate;
                    } else if (name.minDateTime) {
                        min = name.minDateTime;
                    } else if (name.maxDate) {
                        max = name.maxDate;
                    } else if (name.maxDateTime) {
                        max = name.maxDateTime;
                    }
                    for (prop in overrides) {
                        if (overrides.hasOwnProperty(prop) && name[prop]) {
                            fns[prop] = name[prop];
                        }
                    }
                }
                for (prop in fns) {
                    if (fns.hasOwnProperty(prop)) {
                        overrides[prop] = fns[prop];
                        if (!name_clone) {
                            name_clone = $.extend({}, name);
                        }
                        delete name_clone[prop];
                    }
                }
                if (name_clone && isEmptyObject(name_clone)) {
                    return;
                }
                if (min) { //if min was set
                    if (min === 0) {
                        min = new Date();
                    } else {
                        min = new Date(min);
                    }
                    tp_inst._defaults.minDate = min;
                    tp_inst._defaults.minDateTime = min;
                } else if (max) { //if max was set
                    if (max === 0) {
                        max = new Date();
                    } else {
                        max = new Date(max);
                    }
                    tp_inst._defaults.maxDate = max;
                    tp_inst._defaults.maxDateTime = max;
                } else if (onselect) {
                    tp_inst._defaults.onSelect = onselect;
                }
            }
            if (value === undefined) {
                return this._base_optionDatepicker.call($.datepicker, target, name);
            }
            return this._base_optionDatepicker.call($.datepicker, target, name_clone || name, value);
        };

        /*
         * jQuery isEmptyObject does not check hasOwnProperty - if someone has added to the object prototype,
         * it will return false for all objects
         */
        var isEmptyObject = function(obj) {
            var prop;
            for (prop in obj) {
                if (obj.hasOwnProperty(obj)) {
                    return false;
                }
            }
            return true;
        };

        /*
         * jQuery extend now ignores nulls!
         */
        var extendRemove = function(target, props) {
            $.extend(target, props);
            for (var name in props) {
                if (props[name] === null || props[name] === undefined) {
                    target[name] = props[name];
                }
            }
            return target;
        };

        /*
         * Determine by the time format which units are supported
         * Returns an object of booleans for each unit
         */
        var detectSupport = function(timeFormat) {
            var tf = timeFormat.replace(/\'.*?\'/g, '').toLowerCase(), // removes literals
                    isIn = function(f, t) { // does the format contain the token?
                        return f.indexOf(t) !== -1 ? true : false;
                    };
            return {
                hour: isIn(tf, 'h'),
                minute: isIn(tf, 'm'),
                second: isIn(tf, 's'),
                millisec: isIn(tf, 'l'),
                microsec: isIn(tf, 'c'),
                timezone: isIn(tf, 'z'),
                ampm: isIn(tf, 't') && isIn(timeFormat, 'h'),
                iso8601: isIn(timeFormat, 'Z')
            };
        };

        /*
         * Converts 24 hour format into 12 hour
         * Returns 12 hour without leading 0
         */
        var convert24to12 = function(hour) {
            if (hour > 12) {
                hour = hour - 12;
            }

            if (hour === 0) {
                hour = 12;
            }

            return String(hour);
        };

        /*
         * Splits datetime string into date ans time substrings.
         * Throws exception when date can't be parsed
         * Returns [dateString, timeString]
         */
        var splitDateTime = function(dateFormat, dateTimeString, dateSettings, timeSettings) {
            try {
// The idea is to get the number separator occurances in datetime and the time format requested (since time has
// fewer unknowns, mostly numbers and am/pm). We will use the time pattern to split.
                var separator = timeSettings && timeSettings.separator ? timeSettings.separator : $.timepicker._defaults.separator,
                        format = timeSettings && timeSettings.timeFormat ? timeSettings.timeFormat : $.timepicker._defaults.timeFormat,
                        timeParts = format.split(separator), // how many occurances of separator may be in our format?
                        timePartsLen = timeParts.length,
                        allParts = dateTimeString.split(separator),
                        allPartsLen = allParts.length;

                if (allPartsLen > 1) {
                    return [
                        allParts.splice(0, allPartsLen - timePartsLen).join(separator),
                        allParts.splice(0, timePartsLen).join(separator)
                    ];
                }

            } catch (err) {
                $.timepicker.log('Could not split the date from the time. Please check the following datetimepicker options' +
                        "\nthrown error: " + err +
                        "\ndateTimeString" + dateTimeString +
                        "\ndateFormat = " + dateFormat +
                        "\nseparator = " + timeSettings.separator +
                        "\ntimeFormat = " + timeSettings.timeFormat);

                if (err.indexOf(":") >= 0) {
// Hack! The error message ends with a colon, a space, and
// the "extra" characters. We rely on that instead of
// attempting to perfectly reproduce the parsing algorithm.
                    var dateStringLength = dateTimeString.length - (err.length - err.indexOf(':') - 2),
                            timeString = dateTimeString.substring(dateStringLength);

                    return [$.trim(dateTimeString.substring(0, dateStringLength)), $.trim(dateTimeString.substring(dateStringLength))];

                } else {
                    throw err;
                }
            }
            return [dateTimeString, ''];
        };

        /*
         * Internal function to parse datetime interval
         * Returns: {date: Date, timeObj: Object}, where
         * date - parsed date without time (type Date)
         * timeObj = {hour: , minute: , second: , millisec: , microsec: } - parsed time. Optional
         */
        var parseDateTimeInternal = function(dateFormat, timeFormat, dateTimeString, dateSettings, timeSettings) {
            var date;
            var splitRes = splitDateTime(dateFormat, dateTimeString, dateSettings, timeSettings);
            date = $.datepicker._base_parseDate(dateFormat, splitRes[0], dateSettings);
            if (splitRes[1] !== '') {
                var timeString = splitRes[1],
                        parsedTime = $.datepicker.parseTime(timeFormat, timeString, timeSettings);

                if (parsedTime === null) {
                    throw 'Wrong time format';
                }
                return {
                    date: date,
                    timeObj: parsedTime
                };
            } else {
                return {
                    date: date
                };
            }
        };

        /*
         * Internal function to set timezone_select to the local timezone
         */
        var selectLocalTimezone = function(tp_inst, date) {
            if (tp_inst && tp_inst.timezone_select) {
                var now = typeof date !== 'undefined' ? date : new Date();
                tp_inst.timezone_select.val(now.getTimezoneOffset() * -1);
            }
        };

        /*
         * Create a Singleton Insance
         */
        $.timepicker = new Timepicker();

        /**
         * Get the timezone offset as string from a date object (eg '+0530' for UTC+5.5)
         * @param number if not a number this value is returned
         * @param boolean if true formats in accordance to iso8601 "+12:45"
         * @return string
         */
        $.timepicker.timezoneOffsetString = function(tzMinutes, iso8601) {
            if (isNaN(tzMinutes) || tzMinutes > 840) {
                return tzMinutes;
            }

            var off = tzMinutes,
                    minutes = off % 60,
                    hours = (off - minutes) / 60,
                    iso = iso8601 ? ':' : '',
                    tz = (off >= 0 ? '+' : '-') + ('0' + (hours * 101).toString()).slice(-2) + iso + ('0' + (minutes * 101).toString()).slice(-2);

            if (tz == '+00:00') {
                return 'Z';
            }
            return tz;
        };

        /**
         * Get the number in minutes that represents a timezone string
         * @param string formated like "+0500", "-1245"
         * @return number
         */
        $.timepicker.timezoneOffsetNumber = function(tzString) {
            tzString = tzString.toString().replace(':', ''); // excuse any iso8601, end up with "+1245"

            if (tzString.toUpperCase() === 'Z') { // if iso8601 with Z, its 0 minute offset
                return 0;
            }

            if (!/^(\-|\+)\d{4}$/.test(tzString)) { // possibly a user defined tz, so just give it back
                return tzString;
            }

            return ((tzString.substr(0, 1) == '-' ? -1 : 1) * // plus or minus
                    ((parseInt(tzString.substr(1, 2), 10) * 60) + // hours (converted to minutes)
                            parseInt(tzString.substr(3, 2), 10))); // minutes
        };

        /**
         * No way to set timezone in js Date, so we must adjust the minutes to compensate. (think setDate, getDate)
         * @param date
         * @param string formated like "+0500", "-1245"
         * @return date
         */
        $.timepicker.timezoneAdjust = function(date, toTimezone) {
            var toTz = $.timepicker.timezoneOffsetNumber(toTimezone);
            if (!isNaN(toTz)) {
                date.setMinutes(date.getMinutes() * 1 + (date.getTimezoneOffset() * -1 - toTz * 1));
            }
            return date;
        };

        /**
         * Calls `timepicker()` on the `startTime` and `endTime` elements, and configures them to
         * enforce date range limits.
         * n.b. The input value must be correctly formatted (reformatting is not supported)
         * @param Element startTime
         * @param Element endTime
         * @param obj options Options for the timepicker() call
         * @return jQuery
         */
        $.timepicker.timeRange = function(startTime, endTime, options) {
            return $.timepicker.handleRange('timepicker', startTime, endTime, options);
        };

        /**
         * Calls `datetimepicker` on the `startTime` and `endTime` elements, and configures them to
         * enforce date range limits.
         * @param Element startTime
         * @param Element endTime
         * @param obj options Options for the `timepicker()` call. Also supports `reformat`,
         * a boolean value that can be used to reformat the input values to the `dateFormat`.
         * @param string method Can be used to specify the type of picker to be added
         * @return jQuery
         */
        $.timepicker.datetimeRange = function(startTime, endTime, options) {
            $.timepicker.handleRange('datetimepicker', startTime, endTime, options);
        };

        /**
         * Calls `method` on the `startTime` and `endTime` elements, and configures them to
         * enforce date range limits.
         * @param Element startTime
         * @param Element endTime
         * @param obj options Options for the `timepicker()` call. Also supports `reformat`,
         * a boolean value that can be used to reformat the input values to the `dateFormat`.
         * @return jQuery
         */
        $.timepicker.dateRange = function(startTime, endTime, options) {
            $.timepicker.handleRange('datepicker', startTime, endTime, options);
        };

        /**
         * Calls `method` on the `startTime` and `endTime` elements, and configures them to
         * enforce date range limits.
         * @param string method Can be used to specify the type of picker to be added
         * @param Element startTime
         * @param Element endTime
         * @param obj options Options for the `timepicker()` call. Also supports `reformat`,
         * a boolean value that can be used to reformat the input values to the `dateFormat`.
         * @return jQuery
         */
        $.timepicker.handleRange = function(method, startTime, endTime, options) {
            options = $.extend({}, {
                minInterval: 0, // min allowed interval in milliseconds
                maxInterval: 0, // max allowed interval in milliseconds
                start: {}, // options for start picker
                end: {} // options for end picker
            }, options);

            $.fn[method].call(startTime, $.extend({
                onClose: function(dateText, inst) {
                    checkDates($(this), endTime);
                },
                onSelect: function(selectedDateTime) {
                    selected($(this), endTime, 'minDate');
                }
            }, options, options.start));
            $.fn[method].call(endTime, $.extend({
                onClose: function(dateText, inst) {
                    checkDates($(this), startTime);
                },
                onSelect: function(selectedDateTime) {
                    selected($(this), startTime, 'maxDate');
                }
            }, options, options.end));

            checkDates(startTime, endTime);
            selected(startTime, endTime, 'minDate');
            selected(endTime, startTime, 'maxDate');

            function checkDates(changed, other) {
                var startdt = startTime[method]('getDate'),
                        enddt = endTime[method]('getDate'),
                        changeddt = changed[method]('getDate');

                if (startdt !== null) {
                    var minDate = new Date(startdt.getTime()),
                            maxDate = new Date(startdt.getTime());

                    minDate.setMilliseconds(minDate.getMilliseconds() + options.minInterval);
                    maxDate.setMilliseconds(maxDate.getMilliseconds() + options.maxInterval);

                    if (options.minInterval > 0 && minDate > enddt) { // minInterval check
                        endTime[method]('setDate', minDate);
                    }
                    else if (options.maxInterval > 0 && maxDate < enddt) { // max interval check
                        endTime[method]('setDate', maxDate);
                    }
                    else if (startdt > enddt) {
                        other[method]('setDate', changeddt);
                    }
                }
            }

            function selected(changed, other, option) {
                if (!changed.val()) {
                    return;
                }
                var date = changed[method].call(changed, 'getDate');
                if (date !== null && options.minInterval > 0) {
                    if (option == 'minDate') {
                        date.setMilliseconds(date.getMilliseconds() + options.minInterval);
                    }
                    if (option == 'maxDate') {
                        date.setMilliseconds(date.getMilliseconds() - options.minInterval);
                    }
                }
                if (date.getTime) {
                    other[method].call(other, 'option', option, date);
                }
            }
            return $([startTime.get(0), endTime.get(0)]);
        };

        /**
         * Log error or data to the console during error or debugging
         * @param Object err pass any type object to log to the console during error or debugging
         * @return void
         */
        $.timepicker.log = function(err) {
            if (window.console) {
                console.log(err);
            }
        };

        /*
         * Microsecond support
         */
        if (!Date.prototype.getMicroseconds) {
            Date.prototype.microseconds = 0;
            Date.prototype.getMicroseconds = function() {
                return this.microseconds;
            };
            Date.prototype.setMicroseconds = function(m) {
                this.setMilliseconds(this.getMilliseconds() + Math.floor(m / 1000));
                this.microseconds = m % 1000;
                return this;
            };
        }

        /*
         * Keep up with the version
         */
        $.timepicker.version = "1.3.1";

    })(jQuery);

</script>

<style>
    .ui-timepicker-div .ui-widget-header { margin-bottom: 8px; }
    .ui-timepicker-div dl { text-align: left; }
    .ui-timepicker-div dl dt { height: 25px; margin-bottom: -25px; }
    .ui-timepicker-div dl dd { margin: 0 10px 10px 65px; }
    .ui-timepicker-div td { font-size: 90%; }
    .ui-tpicker-grid-label { background: none; border: none; margin: 0; padding: 0; }

    .ui-timepicker-rtl{ direction: rtl; }
    .ui-timepicker-rtl dl { text-align: right; }
    .ui-timepicker-rtl dl dd { margin: 0 65px 10px 10px; }
    .ui-datepicker{
        z-index: 2147483647 !important;
    }
</style>
<script type="text/javascript">
    // EXTERNAL SEGMENT INFO
    function get_gexternalsegment_info(counter) {
        $('#trip_going_trip_code' + counter).blur(function() {
            run_gexternal_segment_processor(counter);
        });
        $('#trip_going_departure_datetime' + counter).blur(function() {
            run_gexternal_segment_processor(counter);
        });
    }
    function run_gexternal_segment_processor(counter) {
        if ($('#trip_going_departure_datetime' + counter).val() && $('#trip_going_trip_code' + counter).val()) {
            $.ajax({
                url: "<?= site_url('ea/trips/autocomplete_es') ?>",
                type: "POST",
                data: {arrival_date: $('#trip_going_departure_datetime' + counter).val(), trip_code: $('#trip_going_trip_code' + counter).val()},
                dataType: "json",
                success: function(data) {
                    update_gexternal_segemnt_row(counter, data);
                }
            });
        }
        else if ($('#trip_going_trip_code' + counter).val()) {
            $.ajax({
                url: "<?= site_url('ea/trips/autocomplete_es') ?>",
                type: "POST",
                data: {trip_code: $('#trip_going_trip_code' + counter).val()},
                dataType: "json",
                success: function(data) {
                    update_gexternal_segemnt_row(counter, data);
                }
            });
        } else
            return;
    }
    function update_gexternal_segemnt_row(counter, data) {
        $('#trip_going_safa_transporter_id' + counter).val(data['airline']);
        $('#trip_going_airport_start' + counter).val(data['start_port_id']);
        $('#trip_going_airport_end' + counter).val(data['end_port_id']);
        if ($('#trip_going_departure_datetime' + counter).val() && !$('#trip_going_arrival_datetime' + counter).val())
            $('#trip_going_arrival_datetime' + counter).val(data['arrival_datetime']);
    }
    // RETURN
    function get_rexternalsegment_info(counter) {
        $('#trip_return_trip_code' + counter).blur(function() {
            run_rexternal_segment_processor(counter);
        });
        $('#trip_return_departure_datetime' + counter).blur(function() {
            run_rexternal_segment_processor(counter);
        });
    }
    function run_rexternal_segment_processor(counter) {
        if ($('#trip_return_departure_datetime' + counter).val() && $('#trip_return_trip_code' + counter).val()) {
            $.ajax({
                url: "<?= site_url('ea/trips/autocomplete_es') ?>",
                type: "POST",
                data: {arrival_date: $('#trip_return_departure_datetime' + counter).val(), trip_code: $('#trip_return_trip_code' + counter).val()},
                dataType: "json",
                success: function(data) {
                    update_rexternal_segemnt_row(counter, data);
                }
            });
        }
        else if ($('#trip_return_trip_code' + counter).val()) {
            $.ajax({
                url: "<?= site_url('ea/trips/autocomplete_es') ?>",
                type: "POST",
                data: {trip_code: $('#trip_return_trip_code' + counter).val()},
                dataType: "json",
                success: function(data) {
                    update_rexternal_segemnt_row(counter, data);
                }
            });
        } else
            return;
    }
    function update_rexternal_segemnt_row(counter, data) {
        $('#trip_return_safa_transporter_id' + counter).val(data['airline']);
        $('#trip_return_airport_start' + counter).val(data['start_port_id']);
        $('#trip_return_airport_end' + counter).val(data['end_port_id']);
        if ($('#trip_return_departure_datetime' + counter).val() && !$('#trip_return_arrival_datetime' + counter).val())
            $('#trip_return_arrival_datetime' + counter).val(data['arrival_datetime']);
    }

    function trip_going_safa_externalsegmenttype_id(counter) {
        $('#trip_going_safa_externalsegmenttype_id' + counter).change(function() {
            _counter = $(this).attr('rel');
            _id = $(this).val();
            if (_id == 3) {
                $.get('<?= site_url('ea/trips/get_saudi_ports/SA') ?>', function(data) {
                    $('#trip_going_airport_start' + _counter).html(data);
                    $('#trip_going_airport_end' + _counter).html(data);
                });
            }
            else if (_id == 1) {
                $.get('<?= site_url('ea/trips/get_saudi_ports/SA') ?>', function(data) {
                    $('#trip_going_airport_end' + _counter).html(data);
                });
                $.get('<?= site_url('ea/trips/get_saudi_ports') ?>', function(data) {
                    $('#trip_going_airport_start' + _counter).html(data);
                });
            }
            else {
                $.get('<?= site_url('ea/trips/get_saudi_ports') ?>', function(data) {
                    $('#trip_going_airport_start' + _counter).html(data);
                    $('#trip_going_airport_end' + _counter).html(data);
                });
            }
        });
    }
    function trip_return_safa_externalsegmenttype_id(counter) {
        $('#trip_return_safa_externalsegmenttype_id' + counter).change(function() {
            _counter = counter;
            _id = $(this).val();
            if (_id == 3) {
                $.get('<?= site_url('ea/trips/get_saudi_ports/SA') ?>', function(data) {
                    $('#trip_return_airport_start' + _counter).html(data);
                    $('#trip_return_airport_end' + _counter).html(data);
                });
            }
            else if (_id == 2) {
                $.get('<?= site_url('ea/trips/get_saudi_ports/SA') ?>', function(data) {
                    $('#trip_return_airport_start' + _counter).html(data);
                });
                $.get('<?= site_url('ea/trips/get_saudi_ports') ?>', function(data) {
                    $('#trip_return_airport_end' + _counter).html(data);
                });
            }
            else {
                $.get('<?= site_url('ea/trips/get_saudi_ports') ?>', function(data) {
                    $('#trip_return_airport_start' + _counter).html(data);
                    $('#trip_return_airport_end' + _counter).html(data);
                });
            }
        });
    }
</script>  
<script type="text/javascript">
    function getXMLHTTP() { //fuction to return the xml http object
        var xmlhttp = false;
        try {
            xmlhttp = new XMLHttpRequest();
        }
        catch (e) {
            try {
                xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
            }
            catch (e) {
                try {
                    xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
                }
                catch (e1) {
                    xmlhttp = false;
                }
            }
        }

        return xmlhttp;
    }
    function getHotel(cityId, counter, contract) {
        var strURL = "<?php echo base_url(); ?>/ea/trips/get_hotels_by_city/" + cityId + "/" + contract;
        var req = getXMLHTTP();
        if (req) {

            req.onreadystatechange = function() {
                if (req.readyState == 4) {
                    // only if "OK"
                    if (req.status == 200) {//dv_hotel
                        document.getElementById('trip_hotel_erp_hotel_id' + counter).innerHTML = req.responseText;
                    } else {
                        alert("There was a problem while using XMLHTTP:\n" + req.statusText);
                    }
                }
            }
            req.open("GET", strURL, true);
            req.send(null);
        }

    }

	function setEntryDate(counter)
	{
		if(counter==1) {
			if(document.getElementById('trip_going_arrival_datetime1')!=null) {
				document.getElementById('trip_hotel_checkin_datetime' + counter).value = document.getElementById('trip_going_arrival_datetime1').value.split(' ')[0];
			}
		} 
	}
    
    function getTourismplace(cityId, counter) {
        var strURL = "<?php echo base_url(); ?>/ea/trips/get_tourismplaces_by_city/" + cityId + "/" + counter;
        var req = getXMLHTTP();
        if (req) {

            req.onreadystatechange = function() {
                if (req.readyState == 4) {
                    // only if "OK"
                    if (req.status == 200) {
                        document.getElementById('dv_tourismplace' + counter).innerHTML = req.responseText;
                    } else {
                        alert("There was a problem while using XMLHTTP:\n" + req.statusText);
                    }
                }
            }
            req.open("GET", strURL, true);
            req.send(null);
        }

    }
</script>
<?php
$id = 0;
$name = FALSE;
$safa_trip_confirm_id = 2;
$safa_uo_contract_ea_id = $select_safa_eas;
$moushrif_name = '';
$phone = '';
$safa_tripstatus_id = 1;
$notes = '';
$date = date('Y-m-d');
$erp_transportertype_id = '';
$travellers_adult_count = 0;
$travellers_child_count = 0;
$travellers_infant_count = 0;
$erp_country_id = NULL;

$are_package_private=false;

$current_ea_contract_private_ea_package_id=0;


$this->input->post('safa_ea_package_id') ? $safa_ea_package_id = $this->input->post('safa_ea_package_id') : $safa_ea_package_id = NULL;

if (isset($item)) {
    if (count($item) > 0) {
        $id = $item->safa_trip_id;
        $name = $item->name;
        $trip_supervisor = $this->db->select('safa_ea_supervisors.*,safa_trip_supervisors.safa_trip_id')->where('safa_trip_id', $item->safa_trip_id)->join('safa_ea_supervisors','safa_ea_supervisors.safa_ea_supervisor_id = safa_trip_supervisors.safa_ea_supervisor_id')->get('safa_trip_supervisors')->row();
        $safa_ea_package_id = $item->safa_ea_package_id;
        $safa_tripstatus_id = $item->safa_tripstatus_id;
        $safa_trip_confirm_id = $item->safa_trip_confirm_id;
//        $safa_ea_season_id = $item->safa_ea_season_id;
        $safa_uo_contract_ea_id = $item->safa_uo_contract_ea_id;

        $notes = $item->notes;
        $date = $item->date;
        $travellers_adult_count = $item->travellers_adult_count;
        $travellers_child_count = $item->travellers_child_count;
        $travellers_infant_count = $item->travellers_infant_count;
        $erp_country_id = $item->erp_country_id;
        $erp_transportertype_id = $item->erp_transportertype_id;
        if ($safa_ea_package_id == NULL) {
            $safa_ea_package_id = $item->safa_ea_package_id;
        }
        
        //---------------- Private Package ------------------------------------
    	$safa_uo_package_id=0;
    	$this->ea_packages_model->safa_ea_package_id = $safa_ea_package_id;
    	$ea_package_row = $this->ea_packages_model->get();
    	if(count($ea_package_row)>0) {
    		$safa_uo_package_id =  $ea_package_row->safa_uo_package_id;
    	}
    	if($safa_uo_package_id==1) {
    			$are_package_private =true;
    	}
    	//---------------------------------------------------------------------
    	
    	
        //---------------- Get Private Package For Current ea_contract --------
        $this->ea_packages_model->safa_ea_package_id = false;
        $this->ea_packages_model->safa_uo_contract_ea_id = $safa_uo_contract_ea_id;
        $current_ea_package_rows = $this->ea_packages_model->get();
        if(count($current_ea_package_rows)>0) {
        	foreach ($current_ea_package_rows as $current_ea_package_row) {
        		if($current_ea_package_row->safa_uo_package_id==1) {
        			$current_ea_contract_private_ea_package_id = $current_ea_package_row->safa_ea_package_id;
        		}
        	}
        }
    	//---------------------------------------------------------------------
    	
        
    }
}
?>


<div class="widget">
    <div class="path-container Fright">
        <div class="path-name Fright">
            <a href="<?php echo site_url() . 'ea/dashboard';
?>"><?php echo lang('global_system_management') ?></a>
        </div>
        <div class="path-arrow Fright"></div>
        <div class="path-name Fright">  <a title='<?= lang('trips') ?>'href="<?= site_url('ea/trips') ?>"><?= lang('trips') ?></a></div>

        <div class="path-arrow Fright"></div>
        <div class="path-name Fright">  <?= lang('add_trip') ?></div>
    </div>



</div>


<div class="widget">


    <div class="widget-header">
        <div class="widget-header-icon Fright"> <span class="icos-pencil2"></span> </div>
        <div class="widget-header-title Fright">
            <?php echo lang('add_trip') ?>
        </div>
    </div>


    <div class="widget-container slidingDiv">                
        <? if (validation_errors()): ?>
            <div class="error">
                <p><?= lang('global_please_fill_the_feilds_correctly') ?></p>
                <?= validation_errors(); ?>
            </div>
        <? endif ?>

        <form action="<?= site_url() . "ea/trips/form/$id"; ?>" method="post" id="wizard_validate">
            <fieldset title="<?php echo lang('step_1') ?>">                          
                <legend><?php echo lang('season_and_ekd') ?></legend>
                <div class="wizerd-div"><a><?php echo lang('season_and_ekd') ?></a></div>
                <div class="span6">
                  <div class="row-fluid">
                    <div class="span6">
                        <div class="span10"><?= lang('name') ?></div>
                        <div class="span10"><?=
                            form_input('name', set_value('name', $name),
                                    "id='name' class='validate[required]' autocomplete='off'")
                            ?></div>
                    </div>
                    <div class="span6">
                        <div class="span10"><?= lang('the_date') ?></div>
                        <div class="span10">
                            <input class="validate[required,custom[date]]" data-format="yyyy-MM-dd" name="date" id="date" type="text" value="<?= $date; ?>">
                        </div>
                    </div>
                </div> 
                    <div class="row-fluid">
                        <div class="span12" style="width: 106%;">
                        <div class="span10"><?= lang('moushrif_name') ?></div>
                        <div class="span10"><?=
                            form_multiselect('safa_ea_supervisor_id[]', $supervisors,set_value('safa_ea_supervisor_id', $trip_supervisors),
                                    "id='safa_ea_supervisor_id'  class='validate[required] chosen-rtl chosen-select'")
                            ?></div>
                    </div><!--
                    <div class="span6">
                        <div class="span4"><?= lang('phone') ?></div>
                        <div class="span8"><?=
                            form_input('phone', set_value('phone', $phone), 'class="validate[required,custom[integer]]"')
                            ?></div>
                    </div>  -->
                </div>
                    <div class="row-fluid" style="margin-top: 2px;">
                    <div class="span6">
                        <div class="span10"><?= lang('travellers_adult_count') ?></div>
                        <div class="span10">
                            <?=
                            form_input('travellers_adult_count',
                                    set_value('travellers_adult_count', $travellers_adult_count),
                                    "id='travellers_adult_count' class='validate[required]'")
                            ?>
                        </div>
                    </div>
                    <div class="span6">
                        <div class="span10"><?= lang('travellers_child_count') ?></div>
                        <div class="span10">
                            <?=
                            form_input('travellers_child_count',
                                    set_value('travellers_child_count', $travellers_child_count),
                                    "id='travellers_child_count' class='validate[required]'")
                            ?>
                        </div>
                    </div>

                </div>
                    <div class="row-fluid">
                    <div class="span6">
                        <div class="span10"><?= lang('notes') ?></div>
                        <div class="span10"><textarea name="notes"  id="notes"><?php echo $notes; ?></textarea></div>
                    </div>
                    <div class="span6">
                        <div class="span10"></div>
                        <div class="span10">
                        </div>
                    </div>
                </div>
                </div>
                <div class="span6">
                  <div class="row-fluid">
                    <div class="span6">
                        <div class="span10"><?= lang('safa_ea_id') ?></div>
                        <div class="span10">
                            <?=
                            form_dropdown('safa_ea_id', $safa_eas, set_value('safa_ea_id', $safa_uo_contract_ea_id),
                                    "id='safa_ea_id' class='validate[required]'")
                            ?>
                        </div>
                    </div>

                    <div class="span6">
                        <div class="span10"><?= lang('safa_ea_package_id') ?></div>
                        <div class="span10">
                            <?=
                            form_dropdown('safa_ea_package_id', array('' => lang('global_select_from_menu')),
                                    set_value('safa_ea_package_id', $safa_ea_package_id),
                                    "id='safa_ea_package_id' class='validate[required]'")
                            ?> <!-- $safa_ea_packages -->  
                            
                            
                            
                            <!-- By Gouda -->
                            <?php 
                            $are_package_private_value=0;
                            if($are_package_private) {
                            	$are_package_private_value=1;
                            }
                            ?>
                            <input type='hidden' name='hdn_safa_package_id'  id='hdn_safa_package_id'  value='<?php echo $safa_uo_package_id; ?>'/>
                            <input type='hidden' name='hdn_are_package_private'  id='hdn_are_package_private'  value='<?php echo $are_package_private_value;?>'/>
                            
                            
                            <a href='javascript:view_package();' >::</a>
	                        <script>
	                        function view_package() {
		                        var package_id= document.getElementById('hdn_safa_package_id').value;
		                        window.open('<?php echo site_url('ea/packages/view');?>/'+package_id);
	                        }
	                        </script>
                        
                        </div>
                        
                    </div>

                </div>
                    <div class="row-fluid">
                    <div class="span6">
                        <div class="span10"><?= lang('safa_tripstatus_id') ?></div>
                        <div class="span10">
                            <?=
                            form_dropdown('safa_tripstatus_id', $safa_tripstatus,
                                    set_value('safa_tripstatus_id', $safa_tripstatus_id),
                                    "id='safa_tripstatus_id' class='validate[required]'")
                            ?>
                        </div>
                    </div>
                    
                    <!-- 
                    <div class="span6">
                        <div class="span10"><?= lang('safa_trip_confirm_id') ?></div>
                        <div class="span10">
                            <?=
                            form_dropdown('safa_trip_confirm_id', $safa_trip_confirm,
                                    set_value('safa_trip_confirm_id', $safa_trip_confirm_id),
                                    "id='safa_trip_confirm_id' class='validate[required]'")
                            ?>
                        </div>
                    </div>
                     -->
                    
                </div>
                    <div class="row-fluid">

                    <div class="span6">
                        <div class="span10"><?= lang('travellers_infant_count') ?></div>
                        <div class="span10">
<?=
form_input('travellers_infant_count', set_value('travellers_infant_count', $travellers_infant_count),
        "id='travellers_infant_count' class='validate[required]'")
?>
                        </div>
                    </div>
                    <div class="span6">
                        <div class="span10"><?= lang('erp_country_id') ?></div>
                        <div class="span10">
<?=
form_dropdown('erp_country_id', ddgen('erp_countries', array('erp_country_id', name())),
        set_value('erp_country_id', $erp_country_id), "id='erp_country_id' class='validate[required]' ")
?>
                        </div>
                    </div>
                </div>
                </div>

				
                
                <script>
                    $('#date').datepicker({dateFormat: "yy-mm-dd"});
                </script>




<div id="" class="content-holder" style="padding-top: 50px;">
                <div class="wizerd-div"><a><?php echo lang('uo_requests') ?></a></div>
                <div class="span12">
                  <div class="row-fluid">
                    


                            <table   id='div_safa_trips_request_group'>
                                    <thead>
                                        <tr>
                                            <th ><?= lang('safa_uo_service_id') ?></th>
                                            <th ><?= lang('remarks') ?></th>
                                            <th class="TAC"> 
                                            <a href='javascript:void(0)' id='hrf_add_safa_trips_requests' class='btn'><?= lang('add') ?></a>
                                                   
                                            </th>
                                        </tr>
										</thead>
										<tbody id="div_safa_trips_request_group_body">
                                        <? $safa_trips_requests_conuter = 0; ?>

                                        <?php
                                        foreach ($safa_trips_requests_rows as $safa_trips_requests_row) {
                                            $safa_trips_requests_id = $safa_trips_requests_row->safa_trips_requests_id;
                                            $safa_uo_service_id = $safa_trips_requests_row->safa_uo_service_id;
                                            $remarks = $safa_trips_requests_row->remarks;
                                            
                                            $safa_trips_requests_conuter = $safa_trips_requests_conuter + 1;
                                            
                                            
                                            ?>
											
											  
                                            <tr>
                                                <td >
													<input type='hidden' name='safa_trips_requests_safa_trips_requests_id<?= $safa_trips_requests_conuter ?>'  id='safa_trips_requests_safa_trips_requests_id<?= $safa_trips_requests_conuter ?>'  value='<?= $safa_trips_requests_id ?>'/>
                                                    
                                                    <select name='safa_trips_requests_safa_uo_service_id<?= $safa_trips_requests_conuter ?>'  id='safa_trips_requests_safa_uo_service_id<?= $safa_trips_requests_conuter ?>' class='validate[required]' rel='<?= $safa_trips_requests_conuter ?>'>
                                                        <?
                                                        foreach ($safa_uo_services as $key => $value) {
                                                            $selected = '';
                                                            if ($safa_uo_service_id == $key) {
                                                                $selected = "selected='selected'";
                                                            }
                                                                echo "<option value='$key' $selected >$value</option>";
                                                        }
                                                        ?>
                                                    </select></td>
                                                <td >
                                                    <input type='text' name='safa_trips_requests_remarks<?= $safa_trips_requests_conuter ?>'  id='safa_trips_requests_remarks<?= $safa_trips_requests_conuter ?>' class='input-huge'  value='<?= $remarks ?>'/>
                                                </td>

                                                <td>
                                                    <a href='javascript:void(0)'  id='hrf_remove_safa_trips_requests' name='<?= $safa_trips_requests_conuter ?>'><span class="icon-trash"></span></a>
                                                </td>


                                            </tr>

										    <?
											}
											?>
								
								</tbody>
								
                                </table>
                                
                                <!-- By Gouda -->
                            	<input type="hidden" name="hdn_safa_trips_requests_conuter" id="hdn_safa_trips_requests_conuter" value="<?php echo $safa_trips_requests_conuter;?>">          
                            
                </div> 
              </div>
			</div>
			
			
			
			
            </fieldset>
			
			
			
			
			
			
			
            <fieldset title="<?= lang('step_2') ?>">
            <legend><?= lang('erp_transportertype_id') ?></legend>
            <div class="wizerd-div"><a><?= lang('erp_transportertype_id') ?></a></div>
                <div class="row-fluid">
                    <div class="row-fluid">
                        <div class="span6">
                            <div class="span4"><?= lang('erp_transportertype_id') ?></div>
                            <div class="span8">
                                <?=
                                form_dropdown('erp_transportertype_id', $erp_transportertypes,
                                        set_value('erp_transportertype_id', $erp_transportertype_id),
                                        'id="erp_transportertype_id" class="validate[required]"')
                                ?>
                                <script type="text/javascript">
                                    document.getElementById('erp_transportertype_id').onchange = function() {
                                        if (this.value == '2') {
                                            document.getElementById('dv_air_trip').style.display = "inline-block";
                                        } else {
                                            document.getElementById('dv_air_trip').style.display = "none";
                                        }
                                    };
                                </script>
                            </div>
                        </div>
                    </div>



                    <div id="dv_air_trip" <?php if ($erp_transportertype_id != 2) { ?> style="display: none;" <?php } else { ?> style="display: inline-block;" <?php } ?>>
                        <div  class="resalt-group">
                        <div class="wizerd-div"><a><?php echo lang('going_trip') ?></a></div>
                        <div class="row-fluid">
                            <table   id='div_trip_going_group'>
                                    <thead>
                                        <tr>
                                            <th ><?= lang('externalsegmenttype') ?></th>
                                            <th ><?= lang('trip_code') ?></th>
                                            <th ><?= lang('airport_departure_datetime') ?></th>
                                            <th ><?= lang('airport_start') ?></th>
                                            <th ><?= lang('airport_end') ?></th>
                                            <th ><?= lang('airport_arrival_datetime') ?></th>
                                            <th ><?= lang('transporter') ?></th>
                                            <th class="TAC"> 
                                            
                                            <a class="btn fancybox fancybox.iframe" href="<?php echo  site_url('flight_availabilities/popup_trip/1/'.$id) ?>"><?php echo lang('flight_availabilities') ?> </a>
                        					 
                                            <a href='javascript:void(0)' id='hrf_add_trip_going' class='btn'><?= lang('add') ?></a>
                                            
<!--<?= lang('global_operations') ?>-->
                                            </th>
                                        </tr>

                                        <? $trip_going_conuter = 0; ?>

                                        <?php
                                        foreach ($trip_going_rows as $trip_going_row) {
                                            $safa_trip_id = $trip_going_row->safa_trip_id;

                                            $safa_trip_going_id = $trip_going_row->safa_trip_externalsegment_id;
                                            $trip_code = $trip_going_row->trip_code;

                                            $start_port_hall_id = $trip_going_row->start_port_hall_id;
                                            $this->erp_port_halls_model->erp_port_hall_id = $start_port_hall_id;
                                            $start_port_hall_row = $this->erp_port_halls_model->get();
                                            if (count($start_port_hall_row) > 0) {
                                                $airport_start = $start_port_hall_row->erp_port_id;
                                            } else {
                                                $airport_start = 0;
                                            }
											$start_ports_name= $trip_going_row->start_ports_name;
                                            
                                            
                                            $end_port_hall_id = $trip_going_row->end_port_hall_id;
                                            $this->erp_port_halls_model->erp_port_hall_id = $end_port_hall_id;
                                            $end_port_hall_row = $this->erp_port_halls_model->get();
                                            if (count($end_port_hall_row) > 0) {
                                                $airport_end = $end_port_hall_row->erp_port_id;
                                            } else {
                                                $airport_end = 0;
                                            }
											$end_ports_name= $trip_going_row->end_ports_name;

                                            $departure_datetime = $trip_going_row->departure_datetime;
                                            $arrival_datetime = $trip_going_row->arrival_datetime;

                                            $safa_transporter_id = $trip_going_row->safa_transporter_id;
                                            $transporter_name= $trip_going_row->transporter_name;
                                            
                                            $safa_externalsegmenttype_id = $trip_going_row->safa_externalsegmenttype_id;
											$safa_externalsegmenttype_name = $trip_going_row->safa_externalsegmenttype_name;
											
                                            $trip_going_conuter = $trip_going_conuter + 1;
                                            
                                            $this->trip_externalsegments_availabilities_model->safa_trip_externalsegment_id= $safa_trip_going_id;
                                            $trip_externalsegments_availabilities_rows=$this->trip_externalsegments_availabilities_model->get();
                                            if(count($trip_externalsegments_availabilities_rows)==0) {
                                            ?>
											
											  
                                            <tr>
                                                <td >

                                                    <select name='trip_going_safa_externalsegmenttype_id<?= $trip_going_conuter ?>'  id='trip_going_safa_externalsegmenttype_id<?= $trip_going_conuter ?>' class='validate[required, custom[trip_going_safa_externalsegmenttype_id], custom[one_arrival_segment]]  trip_going_safa_externalsegmenttype_id' rel='<?= $trip_going_conuter ?>'>
                                                        <?
                                                        foreach ($safa_externalsegmenttypes as $key => $value) {
                                                            $selected = '';
                                                            if ($safa_externalsegmenttype_id == $key) {
                                                                $selected = "selected='selected'";
                                                            }
                                                            if ($key != 2)
                                                                echo "<option value='$key' $selected >$value</option>";
                                                        }
                                                        ?>
                                                    </select></td>
                                                <td >
                                                    <input type='hidden' name='safa_trip_going_id<?= $trip_going_conuter ?>'  id='safa_trip_going_id<?= $trip_going_conuter ?>'  value='<?= $safa_trip_going_id ?>'/>
                                                    <input type='text' name='trip_going_trip_code<?= $trip_going_conuter ?>'  id='trip_going_trip_code<?= $trip_going_conuter ?>' class='validate[required,custom[onlyLetterNumber_]] input-huge'  value='<?= $trip_code ?>'/>
                                                </td>

                                                <td >
                                                    <input type='text' name='trip_going_departure_datetime<?= $trip_going_conuter ?>'  id='trip_going_departure_datetime<?= $trip_going_conuter ?>'   value='<?=
                                                        date('Y-m-d H:i', strtotime($departure_datetime))
                                                        ?>' class='validate[required, custom[trip_going_departure_datetime]] trip_going_departure_datetime datetimepicker' rel="<?= $trip_going_conuter ?>" />
                                                </td>
                                                <td >
                                                    <select name='trip_going_airport_start<?= $trip_going_conuter ?>' id='trip_going_airport_start<?= $trip_going_conuter ?>' class='validate[required]'>
                                                        <?
                                                        if ($safa_externalsegmenttype_id == '1' or $safa_externalsegmenttype_id == '3') {
                                                            $sort_ports = $erp_sa_ports;
                                                        } else {
                                                            $sort_ports = $erp_ports;
                                                        }
                                                        ?>
                                                        <?
                                                        foreach ($sort_ports as $row) {
                                                            $erp_port_id = $row->erp_port_id;
                                                            $erp_port_name = $row->code;

                                                            $selected = '';
                                                            if ($airport_start == $erp_port_id) {
                                                                $selected = "selected='selected'";
                                                            }
                                                            echo "<option value='$erp_port_id' $selected >$erp_port_name</option>";
                                                        }
                                                        ?></select>                               
                                                </td>

                                                <td >
                                                    <select name='trip_going_airport_end<?= $trip_going_conuter ?>' id='trip_going_airport_end<?= $trip_going_conuter ?>'  class='validate[required]'>
                                                        <?
                                                        if ($safa_externalsegmenttype_id == '3') {
                                                            $sort_ports = $erp_sa_ports;
                                                        } else {
                                                            $sort_ports = $erp_ports;
                                                        }

                                                        foreach ($sort_ports as $row) {
                                                            $erp_port_id = $row->erp_port_id;
                                                            $erp_port_name = $row->code;

                                                            $selected = '';
                                                            if ($airport_end == $erp_port_id) {
                                                                $selected = "selected='selected'";
                                                            }
                                                            echo "<option value='$erp_port_id' $selected >$erp_port_name</option>";
                                                        }
                                                        ?>
                                                    </select>

                                                </td>

                                                <td >
                                                    <input type='text' name='trip_going_arrival_datetime<?= $trip_going_conuter ?>'  id='trip_going_arrival_datetime<?= $trip_going_conuter ?>'  value='<?=
                                                        date('Y-m-d H:i', strtotime($arrival_datetime))
                                                        ?>' class='validate[required, custom[garrival_datetime]] datetimepicker' rel="<?= $trip_going_conuter ?>" />
                                                </td>


                                                <td >
                                                    <select name='trip_going_safa_transporter_id<?= $trip_going_conuter ?>' id='trip_going_safa_transporter_id<?= $trip_going_conuter ?>' class='validate[required]'>
                                                        <?
                                                        foreach ($safa_transporters as $key => $value) {
                                                            $selected = '';
                                                            if ($safa_transporter_id == $key) {
                                                                $selected = "selected='selected'";
                                                            }
                                                            echo "<option value='$key' $selected >$value</option>";
                                                        }
                                                        ?>
                                                    </select>

                                                </td>

                                                <td>
                                                    <a href='javascript:void(0)'  id='hrf_remove_trip_going' name='<?= $trip_going_conuter ?>'><span class="icon-trash"></span></a>
                                                </td>


                                            </tr>
											  
											<?php 
                                            } else {
											?>
											<tr>
                                                <td >

                                                    <input type="hidden" name='trip_going_safa_externalsegmenttype_id<?= $trip_going_conuter ?>'  id='trip_going_safa_externalsegmenttype_id<?= $trip_going_conuter ?>' value="<?php echo $safa_externalsegmenttype_id;?>" class='' rel='<?= $trip_going_conuter ?>'>
                                                    <?php echo $safa_externalsegmenttype_name; ?>
                                                        
                                                </td>
                                                <td >
                                                    <input type='hidden' name='safa_trip_going_id<?= $trip_going_conuter ?>'  id='safa_trip_going_id<?= $trip_going_conuter ?>'  value='<?= $safa_trip_going_id ?>'/>
                                                    <input type="hidden" name='trip_going_trip_code<?= $trip_going_conuter ?>'  id='trip_going_trip_code<?= $trip_going_conuter ?>' class=''  value='<?= $trip_code ?>'/>
                                                    
                                                    <?= $trip_code ?>
                                                </td>

                                                <td >
                                                    <input type="hidden" name='trip_going_departure_datetime<?= $trip_going_conuter ?>'  id='trip_going_departure_datetime<?= $trip_going_conuter ?>'   value='<?=
                                                        date('Y-m-d H:i', strtotime($departure_datetime))
                                                        ?>' class='' rel="<?= $trip_going_conuter ?>" />
                                                        
                                                        <?php echo date('Y-m-d H:i', strtotime($departure_datetime)); ?>
                                                </td>
                                                <td >
                                                   <input type="hidden" name='trip_going_airport_start<?= $trip_going_conuter ?>' id='trip_going_airport_start<?= $trip_going_conuter ?>' value="<?php echo $airport_start; ?>" class=''>
                                                   <?php echo $start_ports_name; ?>                                
                                                </td>

                                                <td >
                                                    <input type="hidden"  name='trip_going_airport_end<?= $trip_going_conuter ?>' id='trip_going_airport_end<?= $trip_going_conuter ?>' value="<?php echo $airport_end; ?>" class=''>
                                                    <?php echo $end_ports_name; ?>

                                                </td>

                                                <td >
                                                    <input type="hidden"  name='trip_going_arrival_datetime<?= $trip_going_conuter ?>'  id='trip_going_arrival_datetime<?= $trip_going_conuter ?>'  value='<?=
                                                        date('Y-m-d H:i', strtotime($arrival_datetime))
                                                        ?>' class='' rel="<?= $trip_going_conuter ?>" />
                                                        
                                                        <?= date('Y-m-d H:i', strtotime($arrival_datetime)); ?>
                                                </td>


                                                <td >
                                                    <input type="hidden" name='trip_going_safa_transporter_id<?= $trip_going_conuter ?>' id='trip_going_safa_transporter_id<?= $trip_going_conuter ?>' value="<?php echo $safa_transporter_id; ?>" class=''>
                                                        
													<?php echo $transporter_name; ?>
                                                </td>

                                                <td>
                                                    <a href='javascript:void(0)' class='' id='hrf_remove_trip_going' name='<?= $trip_going_conuter ?>'><span class="icon-trash"></span></a>
                                                </td>


                                            </tr>
											 <?php 
                                            }
											 ?>

    <?
}
?>
                                </table>
                                
                                <!-- By Gouda -->
                            <input type="hidden" name="hdn_trip_going_conuter" id="hdn_trip_going_conuter" value="<?php echo $trip_going_conuter;?>">          
                            
                        </div>
                        </div>
                        <div class="resalt-group">
                            <div class="wizerd-div"><a><?php echo lang('return_trip') ?></a></div>
                        <div class="row-fluid">



                            <div>                       
                                <table   id='div_trip_return_group'>
                                    <thead>
                                        <tr>
                                            <th ><?php echo lang('externalsegmenttype') ?></th>
                                            <th ><?php echo lang('trip_code') ?></th>
                                            <th ><?php echo lang('airport_departure_datetime') ?></th>
                                            <th ><?php echo lang('airport_start') ?></th>
                                            <th ><?php echo lang('airport_end') ?></th>
                                            <th ><?php echo lang('airport_arrival_datetime') ?></th>
                                            <th ><?php echo lang('transporter') ?></th>
                                            <th class="TAC">  
                                            
                                            <a class="btn fancybox fancybox.iframe" href="<?php echo  site_url('flight_availabilities/popup_trip/2/'.$id) ?>"><?php echo lang('flight_availabilities') ?> </a>
                        					
                        					 
                                            <a href='javascript:void(0)' id='hrf_add_trip_return' class='btn '><?= lang('add') ?> </a>
                                            
                                             
                                            </th>
                                        </tr>

                                        <?php $trip_return_conuter = 0; ?>

                                        <?php
                                        foreach ($trip_return_rows as $trip_return_row) {
                                            $safa_trip_id = $trip_return_row->safa_trip_id;

                                            $safa_trip_return_id = $trip_return_row->safa_trip_externalsegment_id;
                                            $trip_code = $trip_return_row->trip_code;
                                            $departure_datetime = $trip_return_row->departure_datetime;
                                            $arrival_datetime = $trip_return_row->arrival_datetime;



                                            $start_port_hall_id = $trip_return_row->start_port_hall_id;
                                            $this->erp_port_halls_model->erp_port_hall_id = $start_port_hall_id;
                                            $start_port_hall_row = $this->erp_port_halls_model->get();
                                            if (count($start_port_hall_row) > 0) {
                                                $airport_start = $start_port_hall_row->erp_port_id;
                                            } else {
                                                $airport_start = 0;
                                            }

                                            $start_ports_name= $trip_return_row->start_ports_name;
                                            
                                            
											
                                            
                                            
                                            $end_port_hall_id = $trip_return_row->end_port_hall_id;
                                            $this->erp_port_halls_model->erp_port_hall_id = $end_port_hall_id;
                                            $end_port_hall_row = $this->erp_port_halls_model->get();
                                            if (count($end_port_hall_row) > 0) {
                                                $airport_end = $end_port_hall_row->erp_port_id;
                                            } else {
                                                $airport_end = 0;
                                            }
											$end_ports_name= $trip_return_row->end_ports_name;
                                            
											
                                            $safa_transporter_id = $trip_return_row->safa_transporter_id;
  											$transporter_name= $trip_return_row->transporter_name;
                                            
                                            $safa_externalsegmenttype_id = $trip_return_row->safa_externalsegmenttype_id;
											$safa_externalsegmenttype_name = $trip_return_row->safa_externalsegmenttype_name;
                                            

                                            $trip_return_conuter = $trip_return_conuter + 1;
                                            
                                            $this->trip_externalsegments_availabilities_model->safa_trip_externalsegment_id= $safa_trip_return_id;
                                            $trip_externalsegments_availabilities_rows=$this->trip_externalsegments_availabilities_model->get();
                                            if(count($trip_externalsegments_availabilities_rows)==0) {
                                            ?>

											 
                                            <tr>

                                                <td >
                                                    <select name='trip_return_safa_externalsegmenttype_id<?= $trip_return_conuter ?>' id='trip_return_safa_externalsegmenttype_id<?= $trip_return_conuter ?>' class='validate[required, custom[trip_return_safa_externalsegmenttype_id]] trip_return_safa_externalsegmenttype_id' rel="<?= $trip_return_conuter ?>">
                                                        <?
                                                        foreach ($safa_externalsegmenttypes as $key => $value) {
                                                            $selected = '';
                                                            if ($safa_externalsegmenttype_id == $key) {
                                                                $selected = "selected='selected'";
                                                            }
                                                            if ($key != 1)
                                                                echo "<option value='$key' $selected >$value</option>";
                                                        }
                                                        ?>
                                                    </select>

                                                </td>
                                                <td >
                                                    <input type='hidden' name='safa_trip_return_id<?= $trip_return_conuter ?>'  id='safa_trip_return_id<?= $trip_return_conuter ?>'  value='<?= $safa_trip_return_id ?>' class='validate[required]'/>
                                                    <input type='text' name='trip_return_trip_code<?= $trip_return_conuter ?>'  id='trip_return_trip_code<?= $trip_return_conuter ?>'  class='validate[required,custom[onlyLetterNumber_]]'  value='<?= $trip_code ?>' />
                                                </td>

                                                <td >
                                                    <input type='text' name='trip_return_departure_datetime<?= $trip_return_conuter ?>'  id='trip_return_departure_datetime<?= $trip_return_conuter ?>'  value='<?=
                                                        date('Y-m-d H:i', strtotime($departure_datetime))
                                                        ?>' class='validate[required, custom[trip_return_departure_datetime]] trip_return_departure_datetime datetimepicker' rel="<?= $trip_return_conuter ?>"/>
                                                </td>
                                                <td >
                                                    <select name='trip_return_airport_start<?= $trip_return_conuter ?>' id='trip_return_airport_start<?= $trip_return_conuter ?>' class='validate[required]'>
                                                        <?
                                                        if ($safa_externalsegmenttype_id == '2' or $safa_externalsegmenttype_id == '3') {
                                                            $sort_ports = $erp_sa_ports;
                                                        } else {
                                                            $sort_ports = $erp_ports;
                                                        }
                                                        ?>
                                                        <?
                                                        foreach ($sort_ports as $row) {
                                                            $erp_port_id = $row->erp_port_id;
                                                            $erp_port_name = $row->code;

                                                            $selected = '';
                                                            if ($airport_start == $erp_port_id) {
                                                                $selected = "selected='selected'";
                                                            }

                                                            echo "<option value='$erp_port_id' $selected>$erp_port_name</option>";
                                                        }
                                                        ?>
                                                    </select>                               
                                                </td>

                                                <td >
                                                    <select name='trip_return_airport_end<?= $trip_return_conuter ?>' id='trip_return_airport_end<?= $trip_return_conuter ?>'  class='validate[required]'>
                                                        <?
                                                        if ($safa_externalsegmenttype_id == '3')
                                                            $sort_ports = $erp_sa_ports;
                                                        else
                                                            $sort_ports = $erp_ports;
                                                        foreach ($sort_ports as $row) {
                                                            $erp_port_id = $row->erp_port_id;
                                                            $erp_port_name = $row->code;

                                                            $selected = '';
                                                            if ($airport_end == $erp_port_id) {
                                                                $selected = "selected='selected'";
                                                            }
                                                            echo "<option value='$erp_port_id' $selected >$erp_port_name</option>";
                                                        }
                                                        ?>
                                                    </select>
                                                </td>

                                                <td >
                                                    <input type='text' name='trip_return_arrival_datetime<?= $trip_return_conuter ?>'  id='trip_return_arrival_datetime<?= $trip_return_conuter ?>' value='<?=
                                                        date('Y-m-d H:i', strtotime($arrival_datetime))
                                                        ?>'  class='validate[required, custom[rarrival_datetime]] datetimepicker' rel="<?= $trip_return_conuter ?>" />
                                                </td>

                                                <td >
                                                    <select name='trip_return_safa_transporter_id<?= $trip_return_conuter ?>' id='trip_return_safa_transporter_id<?= $trip_return_conuter ?>' class='validate[required]' >";
                                                        <?
                                                        foreach ($safa_transporters as $key => $value) {
                                                            $selected = '';
                                                            if ($safa_transporter_id == $key) {
                                                                $selected = "selected='selected'";
                                                            }
                                                            echo "<option value='$key' $selected >$value</option>";
                                                        }
                                                        ?>
                                                    </select>
                                                </td>
                                                <td>
                                                    <a href='javascript:void(0)'  id='hrf_remove_trip_return' name='<?= $trip_return_conuter ?>'><span class="icon-trash"></span></a>
                                                </td>
                                            </tr>
                                            
                                            <?php 
                                            } else { 
                                            ?>
                                            
                                            <tr>
                                                <td >
													<select name='trip_return_safa_externalsegmenttype_id<?= $trip_return_conuter ?>' id='trip_return_safa_externalsegmenttype_id<?= $trip_return_conuter ?>' class='validate[required, custom[trip_return_safa_externalsegmenttype_id]] trip_return_safa_externalsegmenttype_id' rel="<?= $trip_return_conuter ?>" style="display: none;">
                                                        <?
                                                        foreach ($safa_externalsegmenttypes as $key => $value) {
                                                            $selected = '';
                                                            if ($safa_externalsegmenttype_id == $key) {
                                                                $selected = "selected='selected'";
                                                            }
                                                            if ($key != 1)
                                                                echo "<option value='$key' $selected >$value</option>";
                                                        }
                                                        ?>
                                                    </select>
                                                    <!-- 
                                                    <input type="hidden" name='trip_return_safa_externalsegmenttype_id<?= $trip_return_conuter ?>'  id='trip_return_safa_externalsegmenttype_id<?= $trip_return_conuter ?>' value="<?php echo $safa_externalsegmenttype_id;?>" class='' rel='<?= $trip_return_conuter ?>'>
                                                     -->
                                                    <?php echo $safa_externalsegmenttype_name; ?>
                                                        
                                                </td>
                                                <td >
                                                    <input type='hidden' name='safa_trip_return_id<?= $trip_return_conuter ?>'  id='safa_trip_return_id<?= $trip_return_conuter ?>'  value='<?= $safa_trip_return_id ?>'/>
                                                    <input type="hidden" name='trip_return_trip_code<?= $trip_return_conuter ?>'  id='trip_return_trip_code<?= $trip_return_conuter ?>' class=''  value='<?= $trip_code ?>'/>
                                                    
                                                    <?= $trip_code ?>
                                                </td>

                                                <td >
                                                    <input type="hidden" name='trip_return_departure_datetime<?= $trip_return_conuter ?>'  id='trip_return_departure_datetime<?= $trip_return_conuter ?>'   value='<?=
                                                        date('Y-m-d H:i', strtotime($departure_datetime))
                                                        ?>' class="validate[required, custom[trip_return_departure_datetime] trip_return_departure_datetime datetimepicker" rel="<?= $trip_return_conuter ?>" />
                                                        
                                                        <?php echo date('Y-m-d H:i', strtotime($departure_datetime)); ?>
                                                </td>
                                                <td >						
                                                   <input type="hidden" name='trip_return_airport_start<?= $trip_return_conuter ?>' id='trip_return_airport_start<?= $trip_return_conuter ?>' value="<?php echo $airport_start; ?>" class=''>
                                                   <?php echo $start_ports_name; ?>                                
                                                </td>

                                                <td >
                                                    <input type="hidden"  name='trip_return_airport_end<?= $trip_return_conuter ?>' id='trip_return_airport_end<?= $trip_return_conuter ?>' value="<?php echo $airport_end; ?>" class=''>
                                                    <?php echo $end_ports_name; ?>

                                                </td>

                                                <td >
                                                    <input type="hidden"  name='trip_return_arrival_datetime<?= $trip_return_conuter ?>'  id='trip_return_arrival_datetime<?= $trip_return_conuter ?>'  value='<?=
                                                        date('Y-m-d H:i', strtotime($arrival_datetime))
                                                        ?>' class='' rel="<?= $trip_return_conuter ?>" />
                                                        
                                                        <?= date('Y-m-d H:i', strtotime($arrival_datetime)); ?>
                                                </td>


                                                <td >
                                                    <input type="hidden" name='trip_return_safa_transporter_id<?= $trip_return_conuter ?>' id='trip_return_safa_transporter_id<?= $trip_return_conuter ?>' value="<?php echo $safa_transporter_id; ?>" class=''>
                                                        
													<?php echo $transporter_name; ?>
                                                </td>

                                                <td>
                                                    <a href='javascript:void(0)' class='' id='hrf_remove_trip_return' name='<?= $trip_return_conuter ?>'><span class="icon-trash"></span></a>
                                                </td>


                                            </tr>
                                             
                                             <?php 
                                            }
                                             ?>
                                            
                                            
<? } ?>                                            
                                    </table>
                                    
                            <!-- By Gouda -->
                            <input type="hidden" name="hdn_trip_return_conuter" id="hdn_trip_return_conuter" value="<?php echo $trip_return_conuter;?>">          
                            
                                </div>


                    </div>
                        </div>
                    </div>

            </fieldset>

            <fieldset title="<?php echo lang('step_3') ?>">
                <legend><?php echo lang('hotels_and_tourism_places') ?></legend>
                 <div class="wizerd-div"><a><?= lang('hotels_and_tourism_places') ?></a></div>
                 
                <div class="row-fluid" >
                    
                    
                    <div class="resalt-group">
                        <div class="wizerd-div"><a><?php echo lang('hotels') ?></a></div>



                        <div>             
                        	<table id="div_trip_hotel_group">
                                <thead>
                                    <tr>
                                        <th ><?= lang('city') ?></th>
                                        <th ><?= lang('hotel') ?></th>
                                        <th ><?= lang('nights_count') ?></th>
                                        <th ><?= lang('meal') ?></th>
                                        <th ><?= lang('room_count') ?></th>
                                        <!--<th ><?php echo lang('even_room_count') ?></th>-->
                                        <th ><?= lang('checkin_datetime') ?></th>
                                        <th ><?= lang('checkout_datetime') ?></th>
                                        <th class="TAC">  
                                        
                                        <a class="btn  fancybox fancybox.iframe" href="<?php echo  site_url('hotel_availability/search_popup_for_trip/'.$id) ?>"><?php echo lang('hotels_availabilities') ?> </a>
                                        <!-- Commented By Gouda.  -->
                          				 
                                        <a href='javascript:void(0)' id='hrf_add_trip_hotel' class='btn'><?= lang('add') ?> </a>
                                        
                                        </th>
                                    </tr>
									</thead>
									<tbody id="div_trip_hotel_group_body">

                                    <?php
                                    $trip_hotel_conuter = 0;
                                    foreach ($trip_hotel_rows as $trip_hotel_row):
                                        $safa_trip_id = $trip_hotel_row->safa_trip_id;

                                        $safa_trip_hotel_id = $trip_hotel_row->safa_trip_hotel_id;
                                        $nights_count = $trip_hotel_row->nights_count;
                                        $erp_city_id = $trip_hotel_row->erp_city_id;
                                        $erp_hotel_id = $trip_hotel_row->erp_hotel_id;
                                        $erp_meal_id = $trip_hotel_row->erp_meal_id;
                                        $trip_hotel_rooms = $trip_hotel_row->trip_hotel_rooms;
                                        $checkin_datetime = $trip_hotel_row->checkin_datetime;
                                        $checkout_datetime = $trip_hotel_row->checkout_datetime;
                                        $trip_hotel_conuter = $trip_hotel_conuter + 1;
                                        ?>
                                        
                                        <!-- 
                                        <tr>
                                            <td >
                                                <input type='hidden' name='safa_trip_hotel_id<?= $trip_hotel_conuter ?>'  id='safa_trip_hotel_id<?= $trip_hotel_conuter ?>'  value='<?= $safa_trip_hotel_id ?>' />
                                                <select   name='trip_hotel_erp_city_id<?= $trip_hotel_conuter ?>' id='trip_hotel_erp_city_id<?= $trip_hotel_conuter ?>' onchange='getHotel(this.value, <?= $trip_hotel_conuter ?>, $("#safa_ea_id").val())' class='validate[required] trip_hotel_erp_city_id' rel="<?= $trip_hotel_conuter ?>">
    <? foreach ($ksa_cities as $key => $value): ?>
                                                        <option value='<?= $key ?>' <? if ($erp_city_id == $key): ?>selected='selected'<? endif ?>><?= $value ?></option>
    <? endforeach ?>
                                                </select>                               
                                            </td>
                                            <td >
                                                <div id='dv_hotel<?= $trip_hotel_conuter ?>'>
                                                    <select  name='trip_hotel_erp_hotel_id<?= $trip_hotel_conuter ?>' id='trip_hotel_erp_hotel_id<?= $trip_hotel_conuter ?>' class='chzn-select validate[required]'>
                                                        <?
                                                        $erp_hotel_id_ = $this->db->where('erp_hotel_id', $erp_hotel_id)->get('erp_hotels')->row();
                                                        $contract_hotels[$erp_hotel_id_->erp_hotel_id] = $erp_hotel_id_->{name()};
                                                        foreach ($contract_hotels as $key => $value) {
                                                            $selected = '';
                                                            if ($erp_hotel_id == $key) {
                                                                $selected = "selected='selected'";
                                                            }
                                                            echo "<option value='$key' $selected>$value</option>";
                                                        }
                                                        ?>
                                                    </select>  </div>                             
                                            </td>
                                            <td >
                                                <input type='text'   name='trip_hotel_nights_count<?= $trip_hotel_conuter ?>'  id='trip_hotel_nights_count<?= $trip_hotel_conuter ?>' class='validate[required] input-huge'  value='<?= $nights_count ?>' rel="<?= $trip_hotel_conuter ?>"/>
                                            </td>
                                            <td >
                                                <select   name='trip_hotel_erp_meal_id<?= $trip_hotel_conuter ?>' id='trip_hotel_erp_meal_id<?= $trip_hotel_conuter ?>' class='chzn-select validate[required]'>
                                                    <?
                                                    foreach ($erp_meals as $key => $value) {

                                                        $selected = '';
                                                        if ($erp_meal_id == $key) {
                                                            $selected = "selected='selected'";
                                                        }
                                                        echo "<option value='$key' $selected >$value</option>";
                                                    }

                                                    $checkin_datetime = new DateTime($checkin_datetime);
                                                    $checkin_datetime = $checkin_datetime->format('Y-m-d');

                                                    $checkout_datetime = new DateTime($checkout_datetime);
                                                    $checkout_datetime = $checkout_datetime->format('Y-m-d');
                                                    ?>
                                                </select>                              

                                            </td>

                                            <td >
                                                <input type='text'  name='trip_hotel_rooms<?= $trip_hotel_conuter ?>'  id='trip_hotel_rooms<?= $trip_hotel_conuter ?>'   value='<?= $trip_hotel_rooms ?>' class='validate[required]  input-huge'/>
                                            </td>

                                            <td >
                                                <input type='text'   name='trip_hotel_checkin_datetime<?= $trip_hotel_conuter ?>'  id='trip_hotel_checkin_datetime<?= $trip_hotel_conuter ?>'   value='<?= $checkin_datetime ?>' class='validate[required, custom[hotel_checkin_datetime], custom[hotel_checkin_datetime_1]] datepicker' rel="<?= $trip_hotel_conuter ?>"/>
                                            </td>


                                            <td >
                                                <input type='text'  name='trip_hotel_checkout_datetime<?= $trip_hotel_conuter ?>'  id='trip_hotel_checkout_datetime<?= $trip_hotel_conuter ?>'   value='<?= $checkout_datetime ?>' class='validate[required, custom[date], future[#trip_hotel_checkin_datetime<?= $trip_hotel_conuter ?>], custom[trip_hotel_checkout_datetime], custom[trip_hotel_checkout_datetime_]] trip_hotel_checkout_datetime datepicker' rel="<?= $trip_hotel_conuter ?>" />


                                            </td>

                                            <td>
                                                <a href='javascript:void(0)' class='' id='hrf_remove_trip_hotel' name='<?= $trip_hotel_conuter ?>' ><span class="icon-trash"></span></a>
                                            </td>


                                        </tr>
                                        -->
                                        
                                        
                                        
                                        <tr>
                                            <td >
                                                <input type='hidden' name='safa_trip_hotel_id<?= $trip_hotel_conuter ?>'  id='safa_trip_hotel_id<?= $trip_hotel_conuter ?>'  value='<?= $safa_trip_hotel_id ?>' />
                                                
                                                <input type='hidden' name='trip_hotel_erp_city_id<?= $trip_hotel_conuter ?>'  id='trip_hotel_erp_city_id<?= $trip_hotel_conuter ?>'  value='<?= $erp_city_id ?>' class="trip_hotel_erp_city_id" rel="<?= $trip_hotel_conuter ?>"/>
                                                
    												<? foreach ($ksa_cities as $key => $value): ?>
                                                        <? if ($erp_city_id == $key) { echo $value; } ?>
    												<? endforeach ?>
                                            </td>
                                            <td >
                                                <div id='dv_hotel<?= $trip_hotel_conuter ?>'>
                                                <input type='hidden' name='trip_hotel_erp_hotel_id<?= $trip_hotel_conuter ?>'  id='trip_hotel_erp_hotel_id<?= $trip_hotel_conuter ?>'  value='<?= $erp_hotel_id ?>' />
                                                
                                                        <?
                                                        $erp_hotel_id_ = $this->db->where('erp_hotel_id', $erp_hotel_id)->get('erp_hotels')->row();
                                                        $contract_hotels[$erp_hotel_id_->erp_hotel_id] = $erp_hotel_id_->{name()};
                                                        foreach ($contract_hotels as $key => $value) {
                                                            if ($erp_hotel_id == $key) {
                                                                echo $value;
                                                            }
                                                        }
                                                        ?>
                                                     </div>                             
                                            </td>
                                            <td >
                                                <input type='text'   name='trip_hotel_nights_count<?= $trip_hotel_conuter ?>'  id='trip_hotel_nights_count<?= $trip_hotel_conuter ?>' class='validate[required] input-huge'  value='<?= $nights_count ?>' rel="<?= $trip_hotel_conuter ?>"/>
                                                <!-- 
                                                <?= $nights_count ?>
                                                 -->
                                            </td>
                                            <td >
                                                <select   name='trip_hotel_erp_meal_id<?= $trip_hotel_conuter ?>' id='trip_hotel_erp_meal_id<?= $trip_hotel_conuter ?>' class='chzn-select validate[required]'>
                                                    <?
                                                    foreach ($erp_meals as $key => $value) {

                                                        $selected = '';
                                                        if ($erp_meal_id == $key) {
                                                            $selected = "selected='selected'";
                                                        }
                                                        echo "<option value='$key' $selected >$value</option>";
                                                    }

                                                    $checkin_datetime = new DateTime($checkin_datetime);
                                                    $checkin_datetime = $checkin_datetime->format('Y-m-d');

                                                    $checkout_datetime = new DateTime($checkout_datetime);
                                                    $checkout_datetime = $checkout_datetime->format('Y-m-d');
                                                    ?>
                                                </select>  
                                                                            

                                            </td>

                                            <td >
                                            
                                            	<!-- 
                                                <input type='hidden'  name='trip_hotel_rooms<?= $trip_hotel_conuter ?>'  id='trip_hotel_rooms<?= $trip_hotel_conuter ?>'   value='<?= $trip_hotel_rooms ?>' class='validate[required]  input-huge'/>
                                                <?= $trip_hotel_rooms ?>
                                                 -->
                                                 
                                                <input type='text'  name='trip_hotel_rooms<?= $trip_hotel_conuter ?>'  id='trip_hotel_rooms<?= $trip_hotel_conuter ?>'   value='<?= $trip_hotel_rooms ?>' class='validate[required]  input-huge'/>
                                            </td>

                                            <td >
                                                <input type='text'   name='trip_hotel_checkin_datetime<?= $trip_hotel_conuter ?>'  id='trip_hotel_checkin_datetime<?= $trip_hotel_conuter ?>'   value='<?= $checkin_datetime ?>' class='validate[required, custom[hotel_checkin_datetime], custom[hotel_checkin_datetime_1]] datepicker' rel="<?= $trip_hotel_conuter ?>"/>
                                                <!-- 
                                                <?= $checkin_datetime ?>
                                          		 -->
                                            </td>


                                            <td >
                                                <input type='text'  name='trip_hotel_checkout_datetime<?= $trip_hotel_conuter ?>'  id='trip_hotel_checkout_datetime<?= $trip_hotel_conuter ?>'   value='<?= $checkout_datetime ?>' class='validate[required, custom[date], future[#trip_hotel_checkin_datetime<?= $trip_hotel_conuter ?>], custom[trip_hotel_checkout_datetime], custom[trip_hotel_checkout_datetime_]] trip_hotel_checkout_datetime datepicker' rel="<?= $trip_hotel_conuter ?>" />
												<!-- 
                                                <?= $checkout_datetime ?>
												 -->
                                            </td>

                                            <td>
                                                <a href='javascript:void(0)' class='' id='hrf_remove_trip_hotel' name='<?= $trip_hotel_conuter ?>' ><span class="icon-trash"></span></a>
                                            </td>


                                        </tr>
                                        
							<? endforeach ?>
							</tbody>
                            </table>
                            
                            <!-- By Gouda -->
                            <input type="hidden" name="hdn_trip_hotel_counter" id="hdn_trip_hotel_counter" value="<?php echo $trip_hotel_conuter;?>">          
                            
                            </div>

                    </div>

                    <div class="resalt-group">
                        <div class="wizerd-div"><a><?php echo lang('tourism_places') ?></a></div>
                        


                        <div>                       
                            <table  id="div_trip_tourismplace_group">
                                <thead>
                                <tr>
                                    <th ><?= lang('city') ?></th>
                                    <th ><?= lang('tourism_place') ?></th>
                                    <th ><?= lang('visit_date_and_time') ?></th>
                                    <th class="TAC">  <a href='javascript:void(0)' id='hrf_add_trip_tourismplace' class='btn '><?= lang('add') ?></a></th>
                                </tr>
                                </thead>
								<tbody id="div_trip_tourismplace_group_body">
                                <?
                                $trip_tourismplace_conuter = 0;
                                foreach ($trip_tourismplace_rows as $trip_tourismplace_row):
                                    $safa_trip_id = $trip_tourismplace_row->safa_trip_id;
                                    $safa_trip_tourismplace_id = $trip_tourismplace_row->safa_trip_tourismplace_id;
                                    $erp_city_id = $trip_tourismplace_row->erp_city_id;
                                    $safa_tourismplace_id = $trip_tourismplace_row->safa_tourismplace_id;
                                    $datetime = $trip_tourismplace_row->datetime;
                                    $trip_tourismplace_conuter = $trip_tourismplace_conuter + 1;
                                    ?>

                                    <tr>
                                        <td >
                                            <input type='hidden' name='safa_trip_tourismplace_id<?= $trip_tourismplace_conuter ?>'  id='safa_trip_tourismplace_id<?= $trip_tourismplace_conuter ?>'  value='<?= $safa_trip_tourismplace_id ?>'/>

                                            <select name='trip_tourismplace_city_id<?= $trip_tourismplace_conuter ?>' id='trip_tourismplace_city_id<?= $trip_tourismplace_conuter ?>' onchange='getTourismplace(this.value, <?= $trip_tourismplace_conuter ?>)' class='validate[required]'>
    <? foreach ($ksa_cities as $key => $value): ?>
                                                    <option value='<?= $key ?>' <? if ($erp_city_id == $key): ?>selected='selected'<? endif ?>><?= $value ?></option>
    <? endforeach ?>
                                            </select>                               
                                        </td>
                                        <td >
                                            <div id='dv_tourismplace<?= $trip_tourismplace_conuter ?>'> <select name='trip_tourismplace_safa_tourismplace_id<?= $trip_tourismplace_conuter ?>' id='trip_tourismplace_safa_tourismplace_id<?= $trip_tourismplace_conuter ?>' class='validate[required]'>

    <? foreach ($safa_tourismplaces as $key => $value) : ?>
                                                        <option value='<?= $key ?>' <? if ($safa_tourismplace_id == $key): ?>selected='selected'<? endif ?>><?= $value ?></option>
    <? endforeach ?>

                                                </select>
                                            </div>
                                        </td>
                                        <td >
                                            <input type='text' name='trip_tourismplace_datetime<?= $trip_tourismplace_conuter ?>'  id='trip_tourismplace_datetime<?= $trip_tourismplace_conuter ?>'   value='<?=
    date('Y-m-d H:i', strtotime($datetime))
    ?>' class='validate[required, custom[trip_tourismplace_datetime]] trip_tourismplace_datetime datetimepicker' rel="<?= $trip_tourismplace_conuter ?>" />
                                        </td>

                                        <td >
                                            <a href='javascript:void(0)' class='' id='hrf_remove_trip_tourismplace' name='<?= $trip_tourismplace_conuter ?>' ><span class="icon-trash"></span></a>
                                        </td>


                                    </tr>

<? endforeach ?>
</tbody>
                            </table></div>


                    </div>  
                </div>


                

            </fieldset>


		<fieldset title="<?php echo lang('step_4') ?>">                          
                <legend><?php echo lang('travellers') ?></legend>
                <div class="wizerd-div"><a><?php echo lang('travellers') ?></a></div>

                <div class="row-fluid">
                    <div class="span12">
                        <div class="4">
                        <a class="btn btn-primary finish fancybox fancybox.iframe" href="<?php echo  site_url('ea/safa_groups/popup') ?>">
						      <?php echo  lang('add_travellers') ?>
						 </a>
                        </div>
                        <div class="span8">
                        	<?php 
                        	$safa_trip_traveller_ids='';
                        	if(isset($item_travellers)) {
				    			$safa_trip_traveller_ids=$item_travellers;
                        	}
				    		$safa_trip_traveller_names='';
				    		?>
							<!-- 
							<input type="hidden" id="safa_trip_traveller_ids" name="safa_trip_traveller_ids" value="<?php echo $safa_trip_traveller_ids; ?>"/>
				    		 -->
				    		<?php  
				    		echo form_multiselect('safa_trip_traveller_ids[]', $travellers, set_value('safa_trip_traveller_ids', $safa_trip_traveller_ids),
                                    " id='safa_trip_traveller_ids'  class='' readonly='readonly' style='display: none;' ");
        					?>
                         </div>
                         
                         
                         
                         <script>
                         $("#hrf_remove_traveller").live('click', function() {
                             if (!confirm('<?= lang('global_are_you_sure_you_want_to_delete') ?>'))
                                 return false

                            var traveller_id = $(this).attr('name');
                            //alert(traveller_id);
                            $("#safa_trip_traveller_ids").find('option[value="'+traveller_id+'"]').remove();
                             
                             $(this).parent().parent().remove();
                         });
                         </script>
                         <br/>
                        <table  id="tbl_trip_traveller" class="fsTable">
						<?php 
						$this->load->helper('cookie');
						$cols_names = array('group_id', 'display_order', 'no', 'passport_no', 'passport_type_id', 'name_ar', 'name_la', 'erp_country_id', 'nationality_id', 'date_of_birth', 'age', 'passport_issue_date',
						            'passport_expiry_date', 'passport_issuing_country_id', 'passport_issuing_city', 'passport_dpn_count', 'relative_relation_id',
						             'occupation', 'city', 'marital_status_id', 'relative_no', 'dpn_serial_no','educational_level_id', 'birth_country_id', 'birth_city', 'relative_gender_id');
						       $colums = array();
						        foreach ($cols_names as $col) {
						        	if($col=='group_id') {
							        	/*
						        		if($this->input->get("safa_group_id") || ($safa_group_id!=0 && $safa_group_id!=false)) {
							        		continue;
							        	}
							        	*/
						        	} else if($col=='display_order') {
							        	if(!$this->input->get("safa_group_id")) {
							        		continue;
							        	}
						        	} else if($col=='relative_gender_id') {
						        		continue;
						        	}
						        	
						        	$ea_user_id = session('ea_user_id');
						            if ($this->input->cookie('' . $ea_user_id . '_' . $col . '')) {
						                $colums[] = $this->input->cookie('' . $ea_user_id . '_' . $col . '');
						            }
						        }
						
						        if (!empty($colums) && count($colums) >= 1 && is_array($colums)) {
						            $cols = $colums;
						        } else {
						        	/*
						        	if($this->input->get("safa_group_id")) {
						        		$cols = array('display_order', 'passport_no', 'name_la', 'no', 'name_ar');
							        } else {
							        	$cols = array('group_id', 'passport_no', 'name_la', 'no', 'name_ar');
							        }
							        */
						        		$cols = array('group_id', 'passport_no', 'no', 'name_ar');
						        }
						
						?>
                          
                        <thead>
                        <tr>
                        <!-- 
                    	<th><input type="checkbox" class="checkall" id="checkall" onchange='checkUncheck()'/> </th>
                         -->
                         
                        <? if (in_array('display_order', $cols)): ?>
                            <th><?= lang('group_passports_display_order') ?></th>
                        <? endif; ?>
                        <? if (in_array('group_id', $cols)): ?>
                            <th><?= lang('group_passports_group_id') ?></th>
                        <? endif; ?>
                        <? if (in_array('no', $cols)): ?>
                            <th><?= lang('group_passports_no') ?></th>
                        <? endif; ?> 
                        <? if (in_array('passport_no', $cols)): ?>
                            <th><?= lang('group_passports_passport_no') ?></th>
                        <? endif; ?>
                        <? if (in_array('passport_type_id', $cols)): ?>
                            <th><?= lang('group_passports_passport_type_id') ?></th>
                        <? endif; ?>
                        <? if (in_array('name_ar', $cols)): ?>
                            <th><?= lang('group_passports_name_ar') ?></th>
                        <? endif; ?>
                        <? if (in_array('name_la', $cols)): ?>
                            <th><?= lang('group_passports_name_la') ?></th>
                        <? endif; ?>
                        
                        <? if (in_array('erp_country_id', $cols)): ?>
                            <th><?= lang('group_passports_erp_country_id') ?></th>
                        <? endif; ?>
                        <? if (in_array('nationality_id', $cols)): ?>
                            <th><?= lang('group_passports_nationality_id') ?></th>
                        <? endif; ?>
                        <? if (in_array('date_of_birth', $cols)): ?>
                            <th><?= lang('group_passports_date_of_birth') ?></th>
                        <? endif; ?>
                        <? if (in_array('age', $cols)): ?>
                            <th><?= lang('group_passports_age') ?></th>
                        <? endif; ?>
                        <? if (in_array('passport_issue_date', $cols)): ?>
                            <th><?= lang('group_passports_passport_issue_date') ?></th>
                        <? endif; ?>
                        <? if (in_array('passport_expiry_date', $cols)): ?>
                            <th><?= lang('group_passports_passport_expiry_date') ?></th>
                        <? endif; ?>
                        <? if (in_array('passport_issuing_country_id', $cols)): ?>
                            <th><?= lang('group_passports_passport_issuing_country_id') ?></th>
                        <? endif; ?>
                        <? if (in_array('passport_issuing_city', $cols)): ?>
                            <th><?= lang('group_passports_passport_issuing_city') ?></th>
                        <? endif; ?>
                        <? if (in_array('passport_dpn_count', $cols)): ?>
                            <th><?= lang('group_passports_passport_dpn_count') ?></th>
                        <? endif; ?>
                        <? if (in_array('relative_relation_id', $cols)): ?>
                            <th><?= lang('group_passports_relative_relation_id') ?></th>
                        <? endif; ?>
                        <? if (in_array('occupation', $cols)): ?>
                            <th><?= lang('group_passports_occupation') ?></th>
                        <? endif; ?>
                        <? if (in_array('city', $cols)): ?>
                            <th><?= lang('group_passports_city') ?></th>
                        <? endif; ?>
                        <? if (in_array('marital_status_id', $cols)): ?>
                            <th><?= lang('group_passports_marital_status_id') ?></th>
                        <? endif; ?>
                        <? if (in_array('relative_no', $cols)): ?>
                            <th><?= lang('group_passports_relative_no') ?></th>
                        <? endif; ?>
                        <? if (in_array('dpn_serial_no', $cols)): ?>
                            <th><?= lang('group_passports_dpn_serial_no') ?></th>
                        <? endif; ?>
                        <? if (in_array('educational_level_id', $cols)): ?>
                            <th><?= lang('group_passports_educational_level_id') ?></th>
                        <? endif; ?>
                        
                        
                        <? if (in_array('birth_country_id', $cols)): ?>
                            <th><?= lang('group_passports_birth_country_id') ?></th>
                        <? endif; ?>
                        <? if (in_array('birth_city', $cols)): ?>
                            <th><?= lang('group_passports_birth_city') ?></th>
                        <? endif; ?>
                        
                        <? if (in_array('gender_id', $cols)): ?>
                            <th><?= lang('group_passports_gender_id') ?></th>
                        <? endif; ?>
                        <? if (in_array('relative_gender_id', $cols)): ?>
                            <th><?= lang('group_passports_relative_gender_id') ?></th>
                        <? endif; ?>
                        
                        <th><?= lang('global_actions') ?></th>
                    </tr>
                     </thead>
                     <tbody class="sortable"> 
                     
                     <?php 
                     if(isset($item_travellers_rows)) {
                     	foreach($item_travellers_rows as $item_travellers_row) {
                     		
                     ?>
                     <tr  id="row_<?php echo $item_travellers_row->safa_group_passport_id; ?>" group_id="<?= $item_travellers_row->safa_group_id; ?>">
                        	<!-- 
                        	<td><input type="checkbox" name="mutamers[]" id="mutamers[]"  value="<?= $item_travellers_row->safa_group_passport_id ?>" title="<?= $item_travellers_row->first_name_ar ?> <?= $item_travellers_row->second_name_ar ?> <?= $item_travellers_row->third_name_ar ?>"/></td>
                             -->
                                         
                        	<? if (in_array('display_order', $cols)): ?>
                                    <td><?= $item_travellers_row->display_order ?></td>
                                <? endif; ?>
                                <? if (in_array('group_id', $cols)): ?>
                                    <td><?= $item_travellers_row->safa_groups_name ?></td>
                                <? endif; ?>
                                 <? if (in_array('no', $cols)): ?>
                                    <td><?= $item_travellers_row->no ?></td>
                                <? endif; ?> 
                                <? if (in_array('passport_no', $cols)): ?>
                                    <td>
                                    <input type="hidden" name="passport_no_<?php echo $item_travellers_row->safa_group_passport_id; ?>" id="passport_no_<?php echo $item_travellers_row->safa_group_passport_id; ?>" value="<?= $item_travellers_row->passport_no ?>"></input>
                                    <?= $item_travellers_row->passport_no ?>
                                    </td>
                                <? endif; ?>
                                <? if (in_array('passport_type_id', $cols)): ?>
                                    <td><?= $item_travellers_row->erp_passporttypes_name ?></td>
                                <? endif; ?>
                                <? if (in_array('name_ar', $cols)): ?>
                                    <td> 
                                    <input type="hidden" name="name_ar_<?php echo $item_travellers_row->safa_group_passport_id; ?>" id="name_ar_<?php echo $item_travellers_row->safa_group_passport_id; ?>" value="<?= $item_travellers_row->first_name_ar ?> 
                                        <?= $item_travellers_row->second_name_ar ?> 
                                        <?= $item_travellers_row->third_name_ar ?>"></input>
                                    
                                        <?= $item_travellers_row->first_name_ar ?> 
                                        <?= $item_travellers_row->second_name_ar ?> 
                                        <?= $item_travellers_row->third_name_ar ?>
                                    </td>
                                <? endif; ?>  
                                <? if (in_array('name_la', $cols)): ?>
                                    <td> 
                                        <?= $item_travellers_row->first_name_la ?> 
                                        <?= $item_travellers_row->second_name_la ?> 
                                        <?= $item_travellers_row->third_name_la ?>
                                    </td>
                                <? endif; ?>
                                <? if (in_array('erp_country_id', $cols)): ?>
                                    <td><?= $item_travellers_row->erp_countries_name ?></td>
                                <? endif; ?>
                                <? if (in_array('nationality_id', $cols)): ?>
                                    <td><?= $item_travellers_row->erp_nationalities_name ?></td>
                                <? endif; ?>
                                <? if (in_array('date_of_birth', $cols)): ?>
                                    <td><?= $item_travellers_row->date_of_birth ?></td>
                                <? endif; ?>
                                 <? if (in_array('age', $cols)): ?>
                                    <td><?= $item_travellers_row->age ?></td>
                                <? endif; ?>
                                
                                <? if (in_array('passport_issue_date', $cols)): ?>
                                    <td><?= $item_travellers_row->passport_issue_date ?></td>
                                <? endif; ?>
                                <? if (in_array('passport_expiry_date', $cols)): ?>
                                    <td><?= $item_travellers_row->passport_expiry_date ?></td>
                                <? endif; ?>
                                
                                <? if (in_array('passport_issuing_country_id', $cols)): ?>
                                    <td><?= $item_travellers_row->issuing_country_name ?></td>
                                <? endif; ?>
                                <? if (in_array('passport_issuing_city', $cols)): ?>
                                    <td><?= $item_travellers_row->passport_issuing_city ?></td>
                                <? endif; ?>
                                
                                <? if (in_array('passport_dpn_count', $cols)): ?>
                                    <td><?= $item_travellers_row->passport_dpn_count ?></td>
                                <? endif; ?>
                               
                                <? if (in_array('relative_relation_id', $cols)): ?>
                                    <td><?= $item_travellers_row->erp_relations_name ?></td>
                                <? endif; ?>
                               
                                <? if (in_array('occupation', $cols)): ?>
                                    <td><?= $item_travellers_row->occupation ?></td>
                                <? endif; ?>
                                <? if (in_array('city', $cols)): ?>
                                    <td><?= $item_travellers_row->city ?></td>
                                <? endif; ?>
                                <? if (in_array('marital_status_id', $cols)): ?>
                                    <td><?= $item_travellers_row->erp_maritalstatus_name ?></td>
                                <? endif; ?>
                                <? if (in_array('relative_no', $cols)): ?>
                                    <td><?php 
                                    
                                    if($item_travellers_row->relative_no!=0 &&  !empty($item_travellers_row->relative_no)) {
                                    	$this->safa_group_passports_model->safa_group_id= $item_travellers_row->safa_group_id;
                                    	//$this->safa_group_passports_model->no=$item_travellers_row->relative_no;
                                    	$this->safa_group_passports_model->safa_group_passport_id=$item_travellers_row->relative_no;
                                    	
                                    	$relative_row=$this->safa_group_passports_model->get();
                                    	
                                    	if($relative_row) {
                                    		$first_name_ar=$relative_row->first_name_ar;
                                    		$second_name_ar=$relative_row->second_name_ar;
                                    		$third_name_ar=$relative_row->third_name_ar;
                                    		$fourth_name_ar=$relative_row->fourth_name_ar;
                                    		
                                    		echo $first_name_ar.' '.$second_name_ar.' '.$third_name_ar;
                                    	}
                                    }
                                    
                                    ?></td>
                                <? endif; ?>
                                 <? if (in_array('dpn_serial_no', $cols)): ?>
                                    <td><?= $item_travellers_row->dpn_serial_no ?></td>
                                <? endif; ?>
                               <? if (in_array('educational_level_id', $cols)): ?>
                                    <td><?= $item_travellers_row->erp_educationlevels_name ?></td>
                                <? endif; ?>
                                
                               
                               <? if (in_array('birth_country_id', $cols)): ?>
                                    <td><?= $item_travellers_row->birth_country_name ?></td>
                                <? endif; ?>
                                <? if (in_array('birth_city', $cols)): ?>
                                    <td><?= $item_travellers_row->birth_city ?></td>
                                <? endif; ?>
                                
                                <? if (in_array('gender_id', $cols)): ?>
                                    <td><?= $item_travellers_row->gender_id ?></td>
                                <? endif; ?>
                                <? if (in_array('relative_gender_id', $cols)): ?>
                                    <td><?= $item_travellers_row->relative_gender_id ?></td>
                                <? endif; ?>
                                
                                <td class="TAC">

									<a title='<?= lang('mutamer_data') ?>' 
                                       href="<?= site_url("ea/group_passports/show_data") ?>/<?= $item_travellers_row->safa_group_passport_id ?>" class="fancybox fancybox.iframe"
                                       style="">
                                        <span class="icon-user"></span>
                                    </a>
                                   <a href='javascript:void(0)' class='' id='hrf_remove_traveller' name='<?php echo $item_travellers_row->safa_group_passport_id; ?>'><span class="icon-trash"></span></a>

                                </td>
                            </tr>
                     
                        
                              <?php 
                     				}
                     			}
                              ?> 
                                 
                                 
                         </table>
                         
                    </div>
                    
                    
                    
                </div>
                

				<div class='row-fluid' >
				                    <div class="span12">
				                        <button type="submit" id="smt_save" name="smt_save" class="btn btn-primary finish" onclick="return validate_form();">
				<?= lang("global_submit") ?>
                        </button> 

                    </div>
                </div>

          </fieldset>



        </form>
    </div>
</div>


<script type="text/javascript">

	var current_ea_contract_private_ea_package_id = <?php echo $current_ea_contract_private_ea_package_id; ?>

	$.get('<?= site_url('ea/trips/get_ea_packages/') ?>/' + $('#safa_ea_id').val() + '/<?= $safa_ea_package_id ?>', function(data) {
	$('#safa_ea_package_id').html(data);
	});


    $('#safa_ea_id').change(function() {
        $.get('<?= site_url('ea/trips/get_ea_packages/') ?>/' + $(this).val() , function(data) {
//            alert('hello');
            $('#safa_ea_package_id').html(data);
        });

      //------------- Get Private Package Id For This ea_contract ------------------
		$.get('<?= site_url('ea/trips/get_private_package_id_by_ea_contract_for_ajax_table/') ?>/' + $("#safa_ea_id").val() , function(data) {
			current_ea_contract_private_ea_package_id = data;
		});
		//--------------------------------------------------
		
	    
        
        //By Gouda
        $.get('<?= site_url('ea/trips/get_uo_services_by_contract_for_ajax_table/') ?>/' + $(this).val() , function(data) {

        	
        	if(data.length<=10) {

        	    var Parent = document.getElementById('div_safa_trips_request_group_body');
				var counter_delete=0;
        	    while(Parent.hasChildNodes())
        	    {
            	    if(counter_delete > 0) {
        	    		Parent.removeChild(Parent.firstChild);
            	    }
            	    counter_delete++;
        	    }


        	    var counter = <?php echo $safa_trips_requests_count + 1; ?>;
        	    
        		for(i=0;i<data.length;i++){
        		
        		if(i%2==1) {	

				var new_div_safa_trips_request = $(document.createElement('tr')).attr({"id": 'div_safa_trips_request' + counter, 'style': 'width: 900px'});
				new_div_safa_trips_request.html('<td >' +
                            '<input type="hidden" name="safa_trips_requests_safa_trips_requests_id' + counter + '"  id="safa_trips_requests_safa_trips_requests_id' + counter + '"  value="0" />' +
                            '<select name="safa_trips_requests_safa_uo_service_id' + counter + '" id="safa_trips_requests_safa_uo_service_id' + counter + '"   class="validate[required] input-huge">' +
        					<? foreach ($safa_uo_services as $key => $value): ?>
        					                '<option value="<?= $key ?>" ><?= $value ?></option>' +
        					<? endforeach ?>
                    		'</select>' +
                            '</td>' +


                            '<td >' +
                            '<input type="text" name="safa_trips_requests_remarks' + counter + '"  id="safa_trips_requests_remarks' + counter + '"   value="" class="input-huge" rel="' + counter + '" />' +
                            '</td>' +

                            '<td >' +
                            '<a href="javascript:void(0)" class="" id="hrf_remove_safa_trips_requests" name="' + counter + '"><span class="icon-trash"></span></a>' +
                            '</td>'
                            );
                    
                    new_div_safa_trips_request.appendTo("#div_safa_trips_request_group");      

                    document.getElementById('safa_trips_requests_safa_uo_service_id'+counter).value=data[i];
                    
                    counter++;
                    
                    
                
                	}
        		}
        	}

        	
      	});
        
    });

//By Gouda
//--------------------------- Set Hotels And Tourism places for trips From Package -------------------------------------- 
    
    
    //--------- To Get Previuos Value ---------------------
    var previous_safa_ea_package_id = <?php if($safa_ea_package_id==''){echo 0;} else {echo $safa_ea_package_id; } ?>; 
    //----------------------------------------------------
    
    $('#safa_ea_package_id').change(function() {

    	//------------- Show Package Link ------------------
    	$.get('<?= site_url('ea/trips/get_package_id_by_ea_package/') ?>/' + $(this).val() , function(data) {
	        $('#hdn_safa_package_id').val(data);
	    });
		//--------------------------------------------------
		
		
       	//------------- Hotels ----------------------
        <?php 
        if($id!=0) {
        ?>
        if(confirm('<?php echo lang('new_package_will_replace_previous_package_hotels_and_tourismplaces');?>')) {
        	var hotelParent = document.getElementById('div_trip_hotel_group_body');
            while(hotelParent.hasChildNodes()) {
           	hotelParent.removeChild(hotelParent.firstChild);
         	}
			previous_safa_ea_package_id = $('#safa_ea_package_id').val();
         } else {
           	$('#safa_ea_package_id').val(previous_safa_ea_package_id);
            return false;
         } 
        <?php 
    	}
    	?>
    	
        
        $.get('<?= site_url('ea/trips/get_hotels_by_package_for_ajax_table/') ?>/' + $(this).val() , function(data) {

        	if(data==0) {
        		document.getElementById('hdn_are_package_private').value = 1;
            } else {
            	document.getElementById('hdn_are_package_private').value = 0;
            }
        	
        	<?php 
        	if($id!=0) {
        	?>
        	/*
        	if(data==0) {
            	if(confirm('<?php echo lang('are_you_want_to_remove_all_previous_package_hotels');?>')) {
            		var hotelParent = document.getElementById('div_trip_hotel_group_body');
            	    while(hotelParent.hasChildNodes()) {
            	    	hotelParent.removeChild(hotelParent.firstChild);
            	    }
                } 
                return false;
        	}
        	*/
        	<?php 
        	}
        	?>
        	
        	    var hotelParent = document.getElementById('div_trip_hotel_group_body');
        	    while(hotelParent.hasChildNodes()) {
        	    	hotelParent.removeChild(hotelParent.firstChild);
        	    }

        	    var counter = 1;

        	  	//alert(data);
        	    data = $.parseJSON(data);
        	    //alert(data);
        	    
        		for(i=0;i<data.length;i++){
        		
        			//vars
    		    	
    		    	var city_id_value = data[i].erp_city_id;
    		    	var hotel_id_value= data[i].erp_hotel_id;
    		    	var meal_id_value = data[i].erp_meal_id;
    		    	
    		    	var city_name_value= data[i].erp_city_name;
    		    	var hotel_name_value= data[i].erp_hotel_name;
    		    	var meal_name_value= data[i].erp_meal_name;
    		    	
    		    		
        			var new_div_trip_hotel = $(document.createElement('tr'));
    	            new_div_trip_hotel.html('<td >' +
    	                    '<input type="hidden"  name="safa_trip_hotel_id' + counter + '"  id="safa_trip_hotel_id' + counter + '"  value="0"/>' +
    	                   
    			            '<input type="hidden"  name="trip_hotel_erp_city_id' + counter + '"  id="trip_hotel_erp_city_id' + counter + '"  value="'+city_id_value+'"/>' +
    			            city_name_value +
    			            '</td>' +
    	                    '<td >' +
    	                    '<input type="hidden"  name="trip_hotel_erp_hotel_id' + counter + '"  id="trip_hotel_erp_hotel_id' + counter + '"  value="'+hotel_id_value+'"/>' +
    	                    hotel_name_value +
    	                    '</td>' +
    	                    '<td >' +
    	                    '<input type="text" name="trip_hotel_nights_count' + counter + '"  id="trip_hotel_nights_count' + counter + '"class="validate[required] input-huge"  value="" />' +
    	                    '</td>' +
    	                    '<td >' +
    	                    '<input type="hidden"  name="trip_hotel_erp_meal_id' + counter + '"  id="trip_hotel_erp_meal_id' + counter + '"  value="'+meal_id_value+'"/>' +
    	                    meal_name_value +
    	                    '</td>' +
    	                    '<td >' +
    	                    '<input type="text" name="trip_hotel_rooms' + counter + '"   id="trip_hotel_rooms' + counter + '" class=" input-huge"  value=""/> ' +
    	                    '</td>' +
    	                    '<td >' +
    	                    '<div id="trip_hotel_checkin_datetime_' + counter + '" class="input-append date">' +
    	                    '<input type="text" data-format="yyyy-MM-dd" name="trip_hotel_checkin_datetime' + counter + '"  id="trip_hotel_checkin_datetime' + counter + '"   value="" class="validate[required, custom[hotel_checkin_datetime], custom[hotel_checkin_datetime_1]] datepicker"  /> ' +
    	                    '</div>' +
    	                    '</td>' +
    	                    '<td >' +
    	                    '<div id="trip_hotel_checkout_datetime_' + counter + '" class="input-append date">' +
    	                    '<input type="text" data-format="yyyy-MM-dd" name="trip_hotel_checkout_datetime' + counter + '"  id="trip_hotel_checkout_datetime' + counter + '"   value="" class="validate[required, custom[date], future[#trip_hotel_checkin_datetime' + counter + '], custom[trip_hotel_checkout_datetime], custom[trip_hotel_checkout_datetime_]] trip_hotel_checkout_datetime datepicker" rel="' + counter + '" "/> ' +
    	                    '</div>' +
    	                    '</td>' +
    	                    '<td>' +
    	                    '<a href="javascript:void(0)" class="" id="hrf_remove_trip_hotel" name="' + counter + '"><span class="icon-trash"></span></a>' +
    	                    '</td>'
    	                    );

                    		new_div_trip_hotel.appendTo(parent.$("#div_trip_hotel_group"));

                    		$('.datepicker').datepicker({
    			                dateFormat: "yy-mm-dd",
    			                //controlType: 'select',
    			                timeFormat: 'HH:mm'
    			            });

    			            
                    		counter++;
        		}
        	

      	});
      	//--------------------------------------------


      	//------------- Tourismplaces ----------------------
        $.get('<?= site_url('ea/trips/get_tourismplaces_by_package_for_ajax_table/') ?>/' + $(this).val() , function(data) {

        			<?php 
                	if($id!=0) {
                	?>
                	/*
                	if(data==0) {
                    	if(confirm('<?php echo lang('are_you_want_to_remove_all_previous_package_tourismplaces');?>')) {
                    		var tourismplaceParent = document.getElementById('div_trip_tourismplace_group_body');
        	        	    while(tourismplaceParent.hasChildNodes()) {
        	        	    	tourismplaceParent.removeChild(tourismplaceParent.firstChild);
        	        	    }
                        } 
                        return false;
                	} 
                	*/
                	<?php 
                	}
                	?>
                	
					
                	var tourismplaceParent = document.getElementById('div_trip_tourismplace_group_body');
	        	    while(tourismplaceParent.hasChildNodes()) {
	        	    	tourismplaceParent.removeChild(tourismplaceParent.firstChild);
	        	    }
	
	        	    var counter = 1;
	
	        	  	//alert(data);
	        	    data = $.parseJSON(data);
	        	    //alert(data);
	        	    
	        		for(i=0;i<data.length;i++){
	        		
	        			//vars
	    		    	
	    		    	var city_id_value = data[i].erp_city_id;
	    		    	var tourismplace_id_value= data[i].safa_tourismplace_id;
	    		    	
	    		    	var city_name_value= data[i].erp_city_name;
	    		    	var tourismplace_name_value= data[i].safa_tourismplace_name;
	
	
	
	    		    	var new_div_trip_tourismplace = $(document.createElement('tr'))
	                    .attr({"id": 'div_trip_tourismplace' + counter, 'style': 'width: 900px'});
	
	            		new_div_trip_tourismplace.html('<td >' +
	                    '<input type="hidden" name="safa_trip_tourismplace_id' + counter + '"  id="safa_trip_tourismplace_id' + counter + '"  value="0" />' +
	                    '<input type="hidden"  name="trip_tourismplace_city_id' + counter + '"  id="trip_tourismplace_city_id' + counter + '"  value="'+city_id_value+'"/>' +
		    			city_name_value +
						            
	                    '</td>' +
	
	                    '<td >' +
	                    '<input type="hidden"  name="trip_tourismplace_safa_tourismplace_id' + counter + '"  id="trip_tourismplace_safa_tourismplace_id' + counter + '"  value="'+tourismplace_id_value+'"/>' +
	                    tourismplace_name_value +
			            
			            '</td>' +
			                    
	                    '<td >' +
	                    '<input type="text" name="trip_tourismplace_datetime' + counter + '"  id="trip_tourismplace_datetime' + counter + '"   value="" class="validate[required, custom[trip_tourismplace_datetime]] trip_tourismplace_datetime datetimepicker" rel="' + counter + '" />' +
	                    '</td>' +
	                    '<td >' +
	                    '<a href="javascript:void(0)" class="" id="hrf_remove_trip_tourismplace" name="' + counter + '"><span class="icon-trash"></span></a>' +
	                    '</td>'
	                    );
	                    //alert('');
	                    new_div_trip_tourismplace.appendTo("#div_trip_tourismplace_group");
	                    $('.datetimepicker').datetimepicker({
	                        dateFormat: "yy-mm-dd",
	                        //controlType: 'select',
	                        timeFormat: 'HH:mm'
	                    });
						counter++;
	    		    	
	        			
	        		}

      	});
      	//--------------------------------------------
      	


      	
        
    });
//----------------------------------------------------------------------------------------------------------------------- 

	



//---------------------------------- Check If Package Changed When Save ------------------------------------------------------ 
    <?php 
        if($id!=0) {
    ?>
  	$('#smt_save').click(function() {
		
	  if(document.getElementById('hdn_are_package_private').value == 0) {
		  
		$.get('<?= site_url('ea/trips/are_package_changed/') ?>/' + $("#safa_ea_package_id").val() + '/<?= $id ?>' , function(data) {
			
	    	if(data==1) {
	    		
				if(confirm('<?php echo lang('the_current_package_changed_apply_this_changes_to_trip');?>')) {


					
					//--------------------------------------------------------------------------------------------
					//------------- Hotels ----------------------
      
    	
		        	$.get('<?= site_url('ea/trips/get_hotels_by_package_for_ajax_table/') ?>/' + $("#safa_ea_package_id").val() , function(data) {
		
		        		var hotelParent = document.getElementById('div_trip_hotel_group_body');
		        	    while(hotelParent.hasChildNodes()) {
		        	    	hotelParent.removeChild(hotelParent.firstChild);
		        	    }
		
		        	    var counter = 1;
		
		        	  	//alert(data);
		        	    data = $.parseJSON(data);
		        	    //alert(data);
		        	    
		        		for(i=0;i<data.length;i++){
		        		
		        			//vars
		    		    	
		    		    	var city_id_value = data[i].erp_city_id;
		    		    	var hotel_id_value= data[i].erp_hotel_id;
		    		    	var meal_id_value = data[i].erp_meal_id;
		    		    	
		    		    	var city_name_value= data[i].erp_city_name;
		    		    	var hotel_name_value= data[i].erp_hotel_name;
		    		    	var meal_name_value= data[i].erp_meal_name;
		    		    	
		    		    		
		        			var new_div_trip_hotel = $(document.createElement('tr'));
		    	            new_div_trip_hotel.html('<td >' +
		    	                    '<input type="hidden"  name="safa_trip_hotel_id' + counter + '"  id="safa_trip_hotel_id' + counter + '"  value="0"/>' +
		    	                   
		    			            '<input type="hidden"  name="trip_hotel_erp_city_id' + counter + '"  id="trip_hotel_erp_city_id' + counter + '"  value="'+city_id_value+'"/>' +
		    			            city_name_value +
		    			            '</td>' +
		    	                    '<td >' +
		    	                    '<input type="hidden"  name="trip_hotel_erp_hotel_id' + counter + '"  id="trip_hotel_erp_hotel_id' + counter + '"  value="'+hotel_id_value+'"/>' +
		    	                    hotel_name_value +
		    	                    '</td>' +
		    	                    '<td >' +
		    	                    '<input type="text" name="trip_hotel_nights_count' + counter + '"  id="trip_hotel_nights_count' + counter + '"class="validate[required] input-huge"  value="" />' +
		    	                    '</td>' +
		    	                    '<td >' +
		    	                    '<input type="hidden"  name="trip_hotel_erp_meal_id' + counter + '"  id="trip_hotel_erp_meal_id' + counter + '"  value="'+meal_id_value+'"/>' +
		    	                    meal_name_value +
		    	                    '</td>' +
		    	                    '<td >' +
		    	                    '<input type="text" name="trip_hotel_rooms' + counter + '"   id="trip_hotel_rooms' + counter + '" class=" input-huge"  value=""/> ' +
		    	                    '</td>' +
		    	                    '<td >' +
		    	                    '<div id="trip_hotel_checkin_datetime_' + counter + '" class="input-append date">' +
		    	                    '<input type="text" data-format="yyyy-MM-dd" name="trip_hotel_checkin_datetime' + counter + '"  id="trip_hotel_checkin_datetime' + counter + '"   value="" class="validate[required, custom[hotel_checkin_datetime], custom[hotel_checkin_datetime_1]] datepicker"  /> ' +
		    	                    '</div>' +
		    	                    '</td>' +
		    	                    '<td >' +
		    	                    '<div id="trip_hotel_checkout_datetime_' + counter + '" class="input-append date">' +
		    	                    '<input type="text" data-format="yyyy-MM-dd" name="trip_hotel_checkout_datetime' + counter + '"  id="trip_hotel_checkout_datetime' + counter + '"   value="" class="validate[required, custom[date], future[#trip_hotel_checkin_datetime' + counter + '], custom[trip_hotel_checkout_datetime], custom[trip_hotel_checkout_datetime_]] trip_hotel_checkout_datetime datepicker" rel="' + counter + '" "/> ' +
		    	                    '</div>' +
		    	                    '</td>' +
		    	                    '<td>' +
		    	                    '<a href="javascript:void(0)" class="" id="hrf_remove_trip_hotel" name="' + counter + '"><span class="icon-trash"></span></a>' +
		    	                    '</td>'
		    	                    );
		
		                    		new_div_trip_hotel.appendTo(parent.$("#div_trip_hotel_group"));
		
		                    		$('.datepicker').datepicker({
		    			                dateFormat: "yy-mm-dd",
		    			                //controlType: 'select',
		    			                timeFormat: 'HH:mm'
		    			            });
		
		    			            
		                    		counter++;
		        		}
		        	
		
		      	});
		      	//--------------------------------------------
		
		
		      	//------------- Tourismplaces ----------------------
		        $.get('<?= site_url('ea/trips/get_tourismplaces_by_package_for_ajax_table/') ?>/' + $("#safa_ea_package_id").val() , function(data) {
		
		        			
							
		                	var tourismplaceParent = document.getElementById('div_trip_tourismplace_group_body');
			        	    while(tourismplaceParent.hasChildNodes()) {
			        	    	tourismplaceParent.removeChild(tourismplaceParent.firstChild);
			        	    }
			
			        	    var counter = 1;
			
			        	  	//alert(data);
			        	    data = $.parseJSON(data);
			        	    //alert(data);
			        	    
			        		for(i=0;i<data.length;i++){
			        		
			        			//vars
			    		    	
			    		    	var city_id_value = data[i].erp_city_id;
			    		    	var tourismplace_id_value= data[i].safa_tourismplace_id;
			    		    	
			    		    	var city_name_value= data[i].erp_city_name;
			    		    	var tourismplace_name_value= data[i].safa_tourismplace_name;
			
			
			
			    		    	var new_div_trip_tourismplace = $(document.createElement('tr'))
			                    .attr({"id": 'div_trip_tourismplace' + counter, 'style': 'width: 900px'});
			
			            		new_div_trip_tourismplace.html('<td >' +
			                    '<input type="hidden" name="safa_trip_tourismplace_id' + counter + '"  id="safa_trip_tourismplace_id' + counter + '"  value="0" />' +
			                    '<input type="hidden"  name="trip_tourismplace_city_id' + counter + '"  id="trip_tourismplace_city_id' + counter + '"  value="'+city_id_value+'"/>' +
				    			city_name_value +
								            
			                    '</td>' +
			
			                    '<td >' +
			                    '<input type="hidden"  name="trip_tourismplace_safa_tourismplace_id' + counter + '"  id="trip_tourismplace_safa_tourismplace_id' + counter + '"  value="'+tourismplace_id_value+'"/>' +
			                    tourismplace_name_value +
					            
					            '</td>' +
					                    
			                    '<td >' +
			                    '<input type="text" name="trip_tourismplace_datetime' + counter + '"  id="trip_tourismplace_datetime' + counter + '"   value="" class="validate[required, custom[trip_tourismplace_datetime]] trip_tourismplace_datetime datetimepicker" rel="' + counter + '" />' +
			                    '</td>' +
			                    '<td >' +
			                    '<a href="javascript:void(0)" class="" id="hrf_remove_trip_tourismplace" name="' + counter + '"><span class="icon-trash"></span></a>' +
			                    '</td>'
			                    );
			                    //alert('');
			                    new_div_trip_tourismplace.appendTo("#div_trip_tourismplace_group");
			                    $('.datetimepicker').datetimepicker({
			                        dateFormat: "yy-mm-dd",
			                        //controlType: 'select',
			                        timeFormat: 'HH:mm'
			                    });
								counter++;
			    		    	
			        			
			        		}
		
			      	});
			      	//--------------------------------------------
		      	
					//--------------------------------------------------------------------------------------------




		
			   	} else {
			   		document.getElementById('safa_ea_package_id').value = current_ea_contract_private_ea_package_id;
				}

				
	    	}

	    	
	    	
	   }); 
		//return false;
	  } 
	  
  	}); 

  	<?php 
  	}
  	?>
//----------------------------------------------------------------------------------------------------------------------- 

  
  


    
    // TRIP GOING
    $(document).ready(function() {

        var counter = <?php echo $trip_going_count + 1; ?>;
        $("#hrf_add_trip_going").click(function() {
            if (counter > 100) {
                alert("Only 100 trip_going allow");
                return false;
            }

            var new_div_trip_going = $(document.createElement('tr'))
                    .attr("id", 'div_trip_going' + counter);
            new_div_trip_going.html('<td >' +
                    '<select name="trip_going_safa_externalsegmenttype_id' + counter + '" id="trip_going_safa_externalsegmenttype_id' + counter + '" class="validate[required, custom[trip_going_safa_externalsegmenttype_id], custom[one_arrival_segment]] trip_going_safa_externalsegmenttype_id" rel="' + counter + '">' +
<? foreach ($safa_externalsegmenttypes as $key => $value): ?>
    <? if ($key != 2): ?>
                    '<option value="<?= $key ?>"><?= $value ?></option>' +
    <? endif ?>
<? endforeach ?>
            '</select>' +
                    '</td>' +
                    '<td >' +
                    '<input type="hidden" name="safa_trip_going_id' + counter + '"  id="safa_trip_going_id' + counter + '"  value="0"/>' +
                    '<input  type="text" name="trip_going_trip_code' + counter + '"  id="trip_going_trip_code' + counter + '"  class="validate[required,custom[onlyLetterNumber_]]  input-huge"  value=""/>' +
                    '</td>' +
                    '<td >' +
                    '<input type="text" name="trip_going_departure_datetime' + counter + '"  id="trip_going_departure_datetime' + counter + '"   value=""  class="validate[required, custom[trip_going_departure_datetime]] trip_going_departure_datetime datetimepicker" rel="' + counter + '" />' +
                    '</td>' +
                    '<td >' +
                    '<select name="trip_going_airport_start' + counter + '" id="trip_going_airport_start' + counter + '">' +
<?
foreach ($erp_ports as $row) : $erp_port_id = $row->erp_port_id;
    $erp_port_name = $row->code;
    ?>
                '<option value="<?= $erp_port_id ?>"><?= $erp_port_name ?></option>' +
<? endforeach; ?>
            '</select>' +
                    '</td>' +
                    '<td >' +
                    '<select name="trip_going_airport_end' + counter + '" id="trip_going_airport_end' + counter + '"  class="validate[required]">' +
<?
foreach ($erp_sa_ports as $row):
    $erp_port_id = $row->erp_port_id;
    $erp_port_name = $row->code;
    ?>
                '<option value="<?= $erp_port_id ?>"><?= $erp_port_name ?></option>' +
<? endforeach ?>
            '</select>' +
                    '</td>' +
                    '<td >' +
                    '<input type="text" name="trip_going_arrival_datetime' + counter + '"  id="trip_going_arrival_datetime' + counter + '"   class="validate[required, custom[garrival_datetime]] datetimepicker" value="" rel="' + counter + '" />' +
                    '</td>' +
                    '<td >' +
                    '<select name="trip_going_safa_transporter_id' + counter + '" id="trip_going_safa_transporter_id' + counter + '"  class="validate[required]">' +
<? foreach ($safa_transporters as $key => $value): ?>
                '<option value="<?= $key ?>"><?= addslashes($value) ?></option>' +
<? endforeach ?>
            '</select>' +
                    '</td>' +
                    '<td>' +
                    '<a href="javascript:void(0)" id="hrf_remove_trip_going" name="' + counter + '"><span class="icon-trash"></span> </a>' +
                    '</td>'
                    );
                    //alert('');
                    new_div_trip_going.appendTo("#div_trip_going_group");

            trip_going_safa_externalsegmenttype_id(counter);
            get_gexternalsegment_info(counter);
            $('.datetimepicker').datetimepicker({
                dateFormat: "yy-mm-dd",
                //controlType: 'select',
                timeFormat: 'HH:mm'
            });
            

            //By Gouda.
            $("#trip_going_departure_datetime" +counter).val($("#date").val()+" 00:00");

            counter++;
            
        });

        $("#hrf_remove_trip_going").live('click', function() {
            if (!confirm('<?= lang('global_are_you_sure_you_want_to_delete') ?>'))
                return false

              	//By Gouda.
    			/*
                if (counter == 1) {
                alert("No more trip_going to remove");
                return false;
            	}
                */

            counter--;
            var hrf_id = $(this).attr('name');

			//By Gouda
            //$("#div_trip_going" + hrf_id).remove();
            $(this).parent().parent().remove();

        });
    });
    // TRIP RETURN
    $(document).ready(function() {
        var counter = <?php echo $trip_return_count + 1; ?>;
        $("#hrf_add_trip_return").click(function() {

            if (counter > 100) {
                alert("Only 100 trip_return allow");
                return false;
            }

            var new_div_trip_return = $(document.createElement('tr'))
                    .attr("id", 'div_trip_return' + counter);

            new_div_trip_return.html('<td >' +
                    '<select name="trip_return_safa_externalsegmenttype_id' + counter + '" id="trip_return_safa_externalsegmenttype_id' + counter + '" class="validate[required, custom[trip_return_safa_externalsegmenttype_id]] trip_return_safa_externalsegmenttype_id" rel="' + counter + '">' +
<? foreach ($safa_externalsegmenttypes as $key => $value): ?>
    <? if ($key != 1): ?>
                    '<option value="<?= $key ?>"><?= $value ?></option>' +
    <? endif ?>
<? endforeach ?>
            '</select>' +
                    '</td>' +
                    '<td >' +
                    '<input type="hidden" name="safa_trip_return_id' + counter + '"  id="safa_trip_return_id' + counter + '"  value="0"/>' +
                    '<input type="text" name="trip_return_trip_code' + counter + '"   id="trip_return_trip_code' + counter + '" class="validate[required,custom[onlyLetterNumber_]] input-huge"  value=""/>' +
                    '</td>' +
                    '<td >' +
                    '<input type="text" name="trip_return_departure_datetime' + counter + '"  id="trip_return_departure_datetime' + counter + '"   value="" class="validate[required, custom[trip_return_departure_datetime] trip_return_departure_datetime datetimepicker" rel="' + counter + '" />' +
                    '</td>' +
                    '<td >' +
                    '<select name="trip_return_airport_start' + counter + '" id="trip_return_airport_start' + counter + '"  class="validate[required]">' +
<?
foreach ($erp_sa_ports as $row):
    $erp_port_id = $row->erp_port_id;
    $erp_port_name = $row->code;
    ?>
                '<option value="<?= $erp_port_id ?>"><?= $erp_port_name ?></option>' +
<? endforeach ?>
            '</select>' +
                    '</td>' +
                    '<td >' +
                    '<select name="trip_return_airport_end' + counter + '" id="trip_return_airport_end' + counter + '"  class="validate[required]">' +
<?
foreach ($erp_ports as $row):
    $erp_port_id = $row->erp_port_id;
    $erp_port_name = $row->code;
    ?>
                '<option value="<?= $erp_port_id ?>"><?= $erp_port_name ?></option>' +
<? endforeach ?>
            '</select>' +
                    '</td>' +
                    '<td >' +
                    '<input type="text" name="trip_return_arrival_datetime' + counter + '"  id="trip_return_arrival_datetime' + counter + '" value="" class="validate[required, custom[rarrival_datetime]] datetimepicker" rel="' + counter + '" />' +
                    '</td>' +
                    '<td >' +
                    '<select name="trip_return_safa_transporter_id' + counter + '" id="trip_return_safa_transporter_id' + counter + '" class="validate[required]">' +
<? foreach ($safa_transporters as $key => $value): ?>
                '<option value="<?= $key ?>"><?= addslashes($value) ?></option>' +
<? endforeach ?>
            '</select>' +
                    '</td>' +
                    '<td>' +
                    '<a href="javascript:void(0)"  id="hrf_remove_trip_return" name="' + counter + '"><span class="icon-trash"></span> </a>' +
                    '</td>'
                    );
                    new_div_trip_return.appendTo("#div_trip_return_group");
            trip_return_safa_externalsegmenttype_id(counter);
            get_rexternalsegment_info(counter);

            $('.datetimepicker').datetimepicker({
                dateFormat: "yy-mm-dd",
                //controlType: 'select',
                timeFormat: 'HH:mm'
            });
            counter++;
        });
        $("#hrf_remove_trip_return").live('click', function() {
            if (!confirm('<?= lang('global_are_you_sure_you_want_to_delete') ?>'))
                return false

              	//By Gouda.
    			/*
                if (counter == 1) {
                	alert("No more trip_return to remove");
                	return false;
            	}
                */

            counter--;
            var hrf_id = $(this).attr('name');

			//By Gouda
            //$("#div_trip_return" + hrf_id).remove();
            $(this).parent().parent().remove();
            
        });
    });
    // TRIP HOTEL
    $(document).ready(function() {
        var counter = <?php echo $trip_hotel_count + 1; ?>;
        $("#hrf_add_trip_hotel").click(function() {

            if (counter > 100) {
                alert("Only 100 trip_hotel allow");
                return false;
            }

			if(document.getElementById('hdn_are_package_private').value == 0) {
				if(confirm('<?php echo lang('if_you_update_the_packages_hotels_the_current_package_will_be_changed_to_private_package');?>')) {
					document.getElementById('hdn_are_package_private').value =1;
					document.getElementById('safa_ea_package_id').value = current_ea_contract_private_ea_package_id;
				} else {
					return false;
				}
			}
            
            var new_div_trip_hotel = $(document.createElement('tr'));
            new_div_trip_hotel.html('<td >' +
                    '<input type="hidden"  name="safa_trip_hotel_id' + counter + '"  id="safa_trip_hotel_id' + counter + '"  value="0"/>' +
                    '<select name="trip_hotel_erp_city_id' + counter + '" id="trip_hotel_city_id' + counter + '" onchange="getHotel(this.value, ' + counter + ',' + $("#safa_ea_id").val() + '); setEntryDate('+counter+');" class="validate[required] trip_hotel_erp_city_id" rel="' + counter + '">' +
<? foreach ($ksa_cities as $key => $value): ?>
                '<option value="<?= $key ?>"><?= $value ?></option>' +
<? endforeach ?>

            '</select>' +
                    '</td>' +
                    '<td >' +
                    '<div id="dv_hotel' + counter + '">' +
                    '<select name="trip_hotel_erp_hotel_id' + counter + '" id="trip_hotel_erp_hotel_id' + counter + '"  class="validate[required] menu">' +
                    '</select>' +
                    '</div> </td>' +
                    '<td >' +
                    '<input type="text" name="trip_hotel_nights_count' + counter + '"  id="trip_hotel_nights_count' + counter + '"class="validate[required] input-huge"  value="" />' +
                    '</td>' +
                    '<td >' +
                    '<select name="trip_hotel_erp_meal_id' + counter + '" id="trip_hotel_erp_meal_id' + counter + '"   class="validate[required]">' +
                    <? foreach ($erp_meals as $key => $value): ?>
                        '<option value="<?= $key ?>"><?= $value ?></option>' +
                    <? endforeach ?>
                    '</select>' +
                    '</td>' +
                    '<td >' +
                    '<input type="text" name="trip_hotel_rooms' + counter + '"   id="trip_hotel_rooms' + counter + '" class=" input-huge"  value=""/>' +
                    '</td>' +
                    '<td >' +
//                    '<div id="trip_hotel_checkin_datetime_' + counter + '" class="input-append date">' +
                    '<input type="text" data-format="yyyy-MM-dd" name="trip_hotel_checkin_datetime' + counter + '"  id="trip_hotel_checkin_datetime' + counter + '"   value="" class="validate[required, custom[hotel_checkin_datetime], custom[hotel_checkin_datetime_1]] datepicker"  />' +
//                    '<span class="add-on">' +
//                    '<i data-time-icon="icon-time" data-date-icon="icon-calendar">' +
//                    '</i>' +
//                    '</span>' +
//                    '</div>' +
                    '</td>' +
                    '<td >' +
//                    '<div id="trip_hotel_checkout_datetime_' + counter + '" class="input-append date">' +
                    '<input type="text" data-format="yyyy-MM-dd" name="trip_hotel_checkout_datetime' + counter + '"  id="trip_hotel_checkout_datetime' + counter + '"   value="" class="validate[required, custom[date], future[#trip_hotel_checkin_datetime' + counter + '], custom[trip_hotel_checkout_datetime], custom[trip_hotel_checkout_datetime_]] trip_hotel_checkout_datetime datepicker" rel="' + counter + '" "/>' +
//                    '<span class="add-on">' +
//                    '<i data-time-icon="icon-time" data-date-icon="icon-calendar">' +
//                    '</i>' +
//                    '</span>' +
//                    '</div>' +
                    '</td>' +
                    '<td>' +
                    '<a href="javascript:void(0)"  id="hrf_remove_trip_hotel" name="' + counter + '"><span class="icon-trash"></span> </a>' +
                    '</td>'
                    );
                    new_div_trip_hotel.appendTo("#div_trip_hotel_group");
            trip_hotel_erp_city_id1();
            $('.datepicker').datepicker({
                dateFormat: "yy-mm-dd",
                //controlType: 'select',
                timeFormat: 'HH:mm'
            });

            if(document.getElementById('trip_hotel_checkout_datetime' + (counter-1))!=null) {
            	document.getElementById('trip_hotel_checkin_datetime' + counter).value = document.getElementById('trip_hotel_checkout_datetime' + (counter-1)).value;
            }
            
            counter++;
        });
        $("#hrf_remove_trip_hotel").live('click', function() {
            
            if (!confirm('<?= lang('global_are_you_sure_you_want_to_delete') ?>'))
                return false

           // To private package.-----------------------
           if(document.getElementById('hdn_are_package_private').value == 0) {
    			if(confirm('<?php echo lang('if_you_update_the_packages_hotels_the_current_package_will_be_changed_to_private_package');?>')) {
    				document.getElementById('hdn_are_package_private').value =1;
    				document.getElementById('safa_ea_package_id').value = current_ea_contract_private_ea_package_id;
    			} else {
    				return false;
    			}
    		}
			//-----------------------------------------
			
			
    		//By Gouda.
			/*
            if (counter == 1) {
                alert("No more trip_hotel to remove");
                return false;
            }
            */

            counter--;
            
            $(this).parent().parent().remove();
        });
    });
    // TOURISM PLACE
    $(document).ready(function() {
        var counter = <?php echo $trip_tourismplace_count + 1; ?>;
        $("#hrf_add_trip_tourismplace").click(function() {

            if (counter > 100) {
                alert("Only 100 trip_tourismplace allow");
                return false;
            }

            
            if(document.getElementById('hdn_are_package_private').value == 0) {
				if(confirm('<?php echo lang('if_you_update_the_packages_tourismplaces_the_current_package_will_be_changed_to_private_package');?>')) {
					document.getElementById('hdn_are_package_private').value =1;
					document.getElementById('safa_ea_package_id').value = current_ea_contract_private_ea_package_id;
				} else {
					return false;
				}
			}
			
            var new_div_trip_tourismplace = $(document.createElement('tr'))
                    .attr({"id": 'div_trip_tourismplace' + counter, 'style': 'width: 900px'});

            new_div_trip_tourismplace.html('<td >' +
                    '<input type="hidden" name="safa_trip_tourismplace_id' + counter + '"  id="safa_trip_tourismplace_id' + counter + '"  value="0" />' +
                    '<select name="trip_tourismplace_city_id' + counter + '" id="trip_tourismplace_city_id' + counter + '"  onchange="getTourismplace(this.value, ' + counter + ')" class="validate[required]">' +
<? foreach ($ksa_cities as $key => $value): ?>
                '<option value="<?= $key ?>"><?= $value ?></option>' +
<? endforeach ?>
            '</select>' +
                    '</td>' +
                    '<td >' +
                    '<div id="dv_tourismplace' + counter + '"><select name="trip_tourismplace_safa_tourismplace_id' + counter + '" id="trip_tourismplace_safa_tourismplace_id' + counter + '" class="validate[required]">' +
<?php
foreach ($safa_tourismplaces as $key => $value) {
    echo "'<option value=\"$key\">$value</option>'+";
}
?>
            '</select>' +
                    '</div></td>' +
                    '<td >' +
                    '<input type="text" name="trip_tourismplace_datetime' + counter + '"  id="trip_tourismplace_datetime' + counter + '"   value="" class="validate[required, custom[trip_tourismplace_datetime]] trip_tourismplace_datetime datetimepicker" rel="' + counter + '" />' +
                    '</td>' +
                    '<td >' +
                    '<a href="javascript:void(0)" class="" id="hrf_remove_trip_tourismplace" name="' + counter + '"><span class="icon-trash"></span></a>' +
                    '</td>'
                    );
                    //alert('');
                    new_div_trip_tourismplace.appendTo("#div_trip_tourismplace_group");
                    $('.datetimepicker').datetimepicker({
                        dateFormat: "yy-mm-dd",
                        //controlType: 'select',
                        timeFormat: 'HH:mm'
                    });
            counter++;

        });
        $("#hrf_remove_trip_tourismplace").live('click', function() {
            if (!confirm('<?= lang('global_are_you_sure_you_want_to_delete') ?>'))
                return false


             // To private package.-----------------------
                if(document.getElementById('hdn_are_package_private').value == 0) {
         			if(confirm('<?php echo lang('if_you_update_the_packages_tourismplaces_the_current_package_will_be_changed_to_private_package');?>')) {
         				document.getElementById('hdn_are_package_private').value =1;
         				document.getElementById('safa_ea_package_id').value = current_ea_contract_private_ea_package_id;
         			} else {
         				return false;
         			}
         		}
     			//-----------------------------------------
     			
     			
            /*    
            if (counter == 1) {
                alert("No more trip_tourismplace to remove");
                return false;
            }
            */

            counter--;
            var hrf_id = $(this).attr('name');

            // By Gouda.
            //$("#div_trip_tourismplace" + hrf_id).remove();
            $(this).parent().parent().remove();
        });
//   $('.chzn-select').select2();
    });





    // Safa Trips Requests
    $(document).ready(function() {
        //var counter = <?php echo $safa_trips_requests_count + 1; ?>;
        
        $("#hrf_add_safa_trips_requests").click(function() {

        	 var counter = $('#div_safa_trips_request_group_body').children().length+1;
        	 
            if (counter > 100) {
                alert("Only 100 safa_trips_request allow");
                return false;
            }

            var new_div_safa_trips_request = $(document.createElement('tr'))
                    .attr({"id": 'div_safa_trips_request' + counter, 'style': 'width: 900px'});

            new_div_safa_trips_request.html('<td>' +
                    '<input type="hidden" name="safa_trips_requests_safa_trips_requests_id' + counter + '"  id="safa_trips_requests_safa_trips_requests_id' + counter + '"  value="0" />' +
                    '<select name="safa_trips_requests_safa_uo_service_id' + counter + '" id="safa_trips_requests_safa_uo_service_id' + counter + '"   class="validate[required] input-huge">' +
					<? foreach ($safa_uo_services as $key => $value): ?>
					                '<option value="<?= $key ?>"><?= $value ?></option>' +
					<? endforeach ?>
            '</select>' +
                    '</td>' +


                    '<td >' +
                    '<input type="text" name="safa_trips_requests_remarks' + counter + '"  id="safa_trips_requests_remarks' + counter + '"   value="" class="input-huge" rel="' + counter + '" />' +
                    '</td>' +

                    '<td >' +
                    '<a href="javascript:void(0)" class="" id="hrf_remove_safa_trips_requests" name="' + counter + '"><span class="icon-trash"></span></a>' +
                    '</td>'
                    );
            
            new_div_safa_trips_request.appendTo("#div_safa_trips_request_group");      
            counter++;

        });
        $("#hrf_remove_safa_trips_requests").live('click', function() {
            if (!confirm('<?= lang('global_are_you_sure_you_want_to_delete') ?>'))
                return false
            if (counter == 1) {
                //alert("No more safa_trips_request to remove");
                //return false;
            }

            counter--;
            var hrf_id = $(this).attr('name');

            // By Gouda.
            //$("#div_safa_trips_request" + hrf_id).remove();
            $(this).parent().parent().remove();
        });
//   $('.chzn-select').select2();
    });




    
    // Autocomplete Hotel Date
    function trip_hotel_erp_city_id1() {
        $('#trip_hotel_city_id1').change(function() {
            autocomplete_hotel_info($(this).val());
        });
    }
    function autocomplete_hotel_info(city) {
        var additional, segment_rel = 0;

        $(".trip_going_safa_externalsegmenttype_id").each(function(index) {
            if ($(this).val() == 1)
                segment_rel = $(this).attr('rel');
        });

        // SEGMENT INFO
        var latest_arrival_segment_date = $('#trip_going_arrival_datetime' + segment_rel).val();
        var latest_arrival_segment_port = $('#trip_going_airport_end' + segment_rel).val();

        if (latest_arrival_segment_port == 6053) //JED
        {
            if (city == 1) // MEKKA
            {
                additional = 2 * 60 * 60 * 1000;
            }
            else if (city == 2) // MADINAH
            {
                additional = 6 * 60 * 60 * 1000;
            }
        }
        else if (latest_arrival_segment_port == 6056) //MED
        {
            if (city == 1) // MEKKA
            {
                additional = 6 * 60 * 60 * 1000;
            }
            else if (city == 2) // MADINAH
            {
                additional = 2 * 60 * 60 * 1000;
            }
        }
        else if (latest_arrival_segment_port == 6070) //YNB
        {
            if (city == 1) // MEKKA
            {
                additional = 6 * 60 * 60 * 1000;
            }
            else if (city == 2) // MADINAH
            {
                additional = 6 * 60 * 60 * 1000;
            }
        }
        var arrival_datetime = new Date(latest_arrival_segment_date);
        var expected_datetime = new Date(arrival_datetime.getTime() + additional); // EXPECTED
        $('#trip_hotel_checkin_datetime1').val(expected_datetime.format("yyyy-mm-dd"));
    }


</script>



<!-- By Gouda, For Wizard -->
<script type="text/javascript">
    /*initializing the map*/
    $(document).ready(function() {

    $("#wizard_validate").validationEngine('attach',{prettySelect : true, useSuffix: "_chosen", promptPosition : "topRight:-150"});

    $("#wizard_validate").formToWizard({
              callback: function() {
                if( ! $("#wizard_validate").validationEngine('validate')) 
                    return false;
                return true;
              }
        });


var config = {
        '.chosen-select': {width: "100%"},
        '.chosen-select-deselect': {allow_single_deselect: true},
        '.chosen-select-no-single': {disable_search_threshold: 10},
        '.chosen-select-no-results': {no_results_text: 'Oops, nothing found!'},
        '.chosen-select-width': {width: "95%"}
    }
    for (var selector in config) {
        $(selector).chosen(config[selector]);
    }

    });

    $('.datetimepicker').datetimepicker({
        dateFormat: "yy-mm-dd",
        //controlType: 'select',
        timeFormat: 'HH:mm'
    });

    $('.datepicker').datepicker({
        dateFormat: "yy-mm-dd",
        //controlType: 'select',
        timeFormat: 'HH:mm'
    });
</script>

<script>

//By Gouda

if ($(".fsTable").length > 0) {

	 	//By Gouda, TO remove the warning message.
	 	//$.fn.dataTableExt.sErrMode = 'throw';

       $(".fsTable").dataTable({

           bSort: true,
           bAutoWidth: true,
           "iDisplayLength":<?php 
           $ea_user_id = session('ea_user_id');
           if ($this->input->cookie('' . $ea_user_id . '_' . 'group_passports_rows_count_per_page' . '')) {
               echo $this->input->cookie('' . $ea_user_id . '_' . 'group_passports_rows_count_per_page' . '');
           }else {echo 100;}?>, 
           "aLengthMenu": [5, 10, 25, 50, 100, 200, 500], // can be removed for basic 10 items per page
           "bFilter": true,
           "sPaginationType": "full_numbers",
           "aoColumnDefs": [{"bSortable": false,
           "aTargets": [-1, 0]}]

           });
   }

$('[name="tbl_trip_traveller_length"]').change(function() {
	
	var rows_count_per_page=$(this).val();
    
	var dataString = 'rows_count_per_page='+ rows_count_per_page;
    
    $.ajax
    ({
    	type: 'POST',
    	url: '<?php echo base_url().'ea/group_passports/setRowsCountCookie'; ?>',
    	data: dataString,
    	cache: true,
    	success: function(html)
    	{
        	
    	}
    });
	
});

</script>


<script>
$(document).ready(function() {
    $('.fancybox').fancybox({
        afterClose: function() {
            //location.reload();
        }
    });
});
</script>