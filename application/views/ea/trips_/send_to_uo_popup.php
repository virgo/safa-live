<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); ?>
<!--<link href="<?= CSS ?>/stylesheets.css" rel="stylesheet" type="text/css" />
<? if (lang('global_lang') == 'ar'): ?>
    <link href="<?= CSS ?>/stylesheet-ar.css" rel="stylesheet" type="text/css" />
<? else: ?><? endif ?>
<script type="text/javascript" src='<?= JS ?>/plugins/jquery/jquery-1.9.1.min.js'></script>
<script type="text/javascript" src='<?= JS ?>/plugins/jquery/jquery-ui-1.10.1.custom.min.js'></script>
<script type="text/javascript" src='<?= JS ?>/plugins/bootstrap/bootstrap.min.js'></script>
<link href="<?= CSS_JS ?>/dateandtime/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src='<?= CSS_JS ?>/dateandtime/bootstrap-datetimepicker.min.js'></script>-->
<style>

body{
height: auto!important;
overflow: auto !important;
background:none transparent !important;
overflow: hidden;
min-height: 1%;
}
.modal-body{
    width: 96%;
    padding:0 2% ;
}
.row-fluid{
padding-top: 5px !important;
padding-bottom: 5px !important;
}
</style>


<!-- multi selection choosen -->
<link rel="stylesheet" href="<?= NEW_CSS_JS ?>/multi-select-chosen/chosen.css">
<script src="<?= NEW_CSS_JS ?>/multi-select-chosen/chosen.jquery.js" type="text/javascript"></script>

            

    <div class="modal-body">
        <style>
            .updated_msg{
                display:none;
                background-color:#ccee97;
                font-weight: bold;
                text-align: center;
                border:3px solid #cccdc9; 
                padding:20px;  
                margin-bottom:10px;
                border-radius:15px;
                margin:10px; 
            }
            .wizerd-div,.row-form{width: 96%;margin:6% 2%;}
            label{padding: 0;}
           
        </style>
        
        

        <div class="wizerd-div">
            <a><?php echo  lang('send_to_uo') ?></a>

    	</div>
    	
		<?= form_open('', ' name="frm_send_to_uo_popup" id="frm_send_to_uo_popup" method="post"') ?>
                      
        <input type='hidden' name='trip_id'  id='trip_id'  value='<?= $trip_id ?>'/>
 
                    
        <div class="row-fluid">
            
			<table   id='div_safa_trips_request_group'>
                                    <thead>
                                        <tr>
                                            <th ><?= lang('safa_uo_service_id') ?></th>
                                            <th ><?= lang('remarks') ?></th>
                                            <th class="TAC"> 
                                            <a href='javascript:void(0)' id='hrf_add_safa_trips_requests' class='btn'><?= lang('add') ?></a>
                                            </th>
                                        </tr>

                                        <? $safa_trips_requests_conuter = 0; ?>

                                        <?php
                                        foreach ($safa_trips_requests_rows as $safa_trips_requests_row) {
                                            $safa_trips_requests_id = $safa_trips_requests_row->safa_trips_requests_id;
                                            $safa_uo_service_id = $safa_trips_requests_row->safa_uo_service_id;
                                            $remarks = $safa_trips_requests_row->remarks;
                                            
                                            $safa_trips_requests_conuter = $safa_trips_requests_conuter + 1;
                                            
                                            
                                            ?>
											
											  
                                            <tr>
                                                <td >
													<input type='hidden' name='safa_trips_requests_safa_trips_requests_id<?= $safa_trips_requests_conuter ?>'  id='safa_trips_requests_safa_trips_requests_id<?= $safa_trips_requests_conuter ?>'  value='<?= $safa_trips_requests_id ?>'/>
                                                    
                                                    <select name='safa_trips_requests_safa_uo_service_id<?= $safa_trips_requests_conuter ?>'  id='safa_trips_requests_safa_uo_service_id<?= $safa_trips_requests_conuter ?>' class='validate[required]' rel='<?= $safa_trips_requests_conuter ?>'>
                                                        <?
                                                        foreach ($safa_uo_services as $key => $value) {
                                                            $selected = '';
                                                            if ($safa_uo_service_id == $key) {
                                                                $selected = "selected='selected'";
                                                            }
                                                                echo "<option value='$key' $selected >$value</option>";
                                                        }
                                                        ?>
                                                    </select></td>
                                                <td >
                                                    <input type='text' name='safa_trips_requests_remarks<?= $safa_trips_requests_conuter ?>'  id='safa_trips_requests_remarks<?= $safa_trips_requests_conuter ?>' class='input-huge'  value='<?= $remarks ?>'/>
                                                </td>

                                                <td>
                                                    <a href='javascript:void(0)'  id='hrf_remove_safa_trips_requests' name='<?= $safa_trips_requests_conuter ?>'><span class="icon-trash"></span></a>
                                                </td>


                                            </tr>

							    <?
								}
								?>
                                </table>
                                
                                <!-- By Gouda -->
                            	<input type="hidden" name="hdn_safa_trips_requests_conuter" id="hdn_safa_trips_requests_conuter" value="<?php echo $safa_trips_requests_conuter;?>">          
                            


        </div>
        
      
      <div class="toolbar bottom TAC">
          <input  type ="submit" name="smt_send" value="<?= lang('global_submit') ?>" class="btn btn-primary">
      </div>
        
    <?= form_close() ?>
    
 </div>    
 
<script>
// Safa Trips Requests
$(document).ready(function() {
    var counter = <?php echo $safa_trips_requests_count + 1; ?>;
    $("#hrf_add_safa_trips_requests").click(function() {

        if (counter > 100) {
            alert("Only 100 safa_trips_request allow");
            return false;
        }

        var new_div_safa_trips_request = $(document.createElement('tr'))
                .attr({"id": 'div_safa_trips_request' + counter, 'style': 'width: 900px'});

        new_div_safa_trips_request.html('<td >' +
                '<input type="hidden" name="safa_trips_requests_safa_trips_requests_id' + counter + '"  id="safa_trips_requests_safa_trips_requests_id' + counter + '"  value="0" />' +
                '<select name="safa_trips_requests_safa_uo_service_id' + counter + '" id="safa_trips_requests_safa_uo_service_id' + counter + '"   class="validate[required] input-huge">' +
				<? foreach ($safa_uo_services as $key => $value): ?>
				                '<option value="<?= $key ?>"><?= $value ?></option>' +
				<? endforeach ?>
        '</select>' +
                '</td>' +


                '<td >' +
                '<input type="text" name="safa_trips_requests_remarks' + counter + '"  id="safa_trips_requests_remarks' + counter + '"   value="" class="input-huge" rel="' + counter + '" />' +
                '</td>' +

                '<td >' +
                '<a href="javascript:void(0)" class="" id="hrf_remove_safa_trips_requests" name="' + counter + '"><span class="icon-trash"></span></a>' +
                '</td>'
                );
        
        new_div_safa_trips_request.appendTo("#div_safa_trips_request_group");      
        counter++;

    });
    $("#hrf_remove_safa_trips_requests").live('click', function() {
        if (!confirm('<?= lang('global_are_you_sure_you_want_to_delete') ?>'))
            return false
        if (counter == 1) {
            //alert("No more safa_trips_request to remove");
            //return false;
        }

        counter--;
        var hrf_id = $(this).attr('name');

        // By Gouda.
        //$("#div_safa_trips_request" + hrf_id).remove();
        $(this).parent().parent().remove();
    });
//$('.chzn-select').select2();
});


</script>