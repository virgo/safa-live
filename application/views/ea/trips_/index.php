<script>.widget{width:98%;}</script>


<div class="widget">
    <div class="path-container Fright">
    	<div class="path-name Fright">
            <a href="<?php echo  site_url().'ea/dashboard'; 
            
            ?>"><?php echo  lang('global_system_management') ?></a>
        </div>
        <div class="path-arrow Fright"></div>
        <div class="path-name Fright">  <?= lang('trips') ?></div>
    </div>
    
    
        
</div>

<div class="row-fluid">
    <div class="row-fluid">       
                  
      <div class="widget">
         
        
        <div class="widget-header">
        <div class="widget-header-icon Fright"> <span class="icos-pencil2"></span> </div>
        <div class="widget-header-title Fright">
            <?php echo  lang('trips') ?>
        </div>
        <a title='<?= lang('global_add') ?>'href="<?= site_url('ea/trips/form') ?>" class="btn Fleft">
           <?= lang('global_add') ?>
         </a>
       	</div>
                      
        <div class="block-fluid">
            <form action="<?php echo base_url()."ea/trips/index"; ?>" method="GET">
            <table class="fpTable" cellpadding="0" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <!--<th><?php echo lang('season_and_ekd') ?></th>-->
                        <th><?= lang('trip') ?></th>
                        <th><?= lang('travellers_count') ?></th>
                        <th><?= lang('passports_count') ?></th>
                        <th><?= lang('the_date') ?></th>
                        <th><?= lang('global_actions')?></th>
                    </tr>
                </thead>
                <tbody>
                <? foreach($items as $item): ?>
                    <? $safa_trip_id=$item->safa_trip_id;
//                            	$safa_ea_name=$item->safa_ea_ar;
                    $safa_trip_name=$item->name;
                    $date=$item->date;
                    $safa_trip_confirm_id=$item->safa_trip_confirm_id;
                    $safa_trip_confirm_name=lang('trip_confirm');
                    $safa_tripstatus_id=$item->safa_tripstatus_id;
                    $travellers_no = $item->travellers_adult_count + $item->travellers_infant_count + $item->travellers_child_count;

                    $individuals_count=$item->individuals_count;
                    $passports_count=$item->passports_count;
                    
                    
                    if($safa_tripstatus_id==1) {
                            $css_class='label label-info';
                    } else if($safa_tripstatus_id==2) {
                            $css_class='label label-warning';
                    }else if($safa_tripstatus_id==3) {
                            $css_class='label label-success';
                    }
                    ?>
                    <? /*<td><span class='<?= $css_class ?>'><?= $safa_ea_name ?></span></td>*/ ?>
                    <tr>
                        <td><?= $safa_trip_name ?></td>
                        <!-- <td><?= $travellers_no ?></td> -->
                        <td><?= $individuals_count ?></td>
                        <td><?= $passports_count ?></td>
                        <td><?= $date ?></td>
                    <? /*<td class='TAC'>
                    <? if($safa_trip_confirm_id==1): ?>
                            <a href='<?= site_url() ?>/ea/trips/confirm/<?= $safa_trip_id ?>/2' class='btn btn-primary'><?= $safa_trip_confirm_name ?></a>
                    <? elseif($safa_trip_confirm_id==2): ?>
                            <a href='<?= site_url() ?>/ea/trips/confirm/<?= $safa_trip_id ?>/1' class='btn btn-primary'><?= $safa_trip_confirm_name ?></a>
                    <? endif ?>
                    </td>*/ ?>
                    <td class='TAC'>
                        <a href='<?= site_url('trip_details/details') ?>/<?= $safa_trip_id ?>'><span class='icon-search'></span></a> 
                        <a href='<?= site_url('ea/trips/form') ?>/<?= $safa_trip_id ?>'><span class='icon-pencil'></span></a> 
                        <? if($item->internal_trips == 0): ?>
                        <a class="fancybox fancybox.iframe" href='<?= site_url('ea/trips/generate_tit') ?>/<?= $safa_trip_id ?>'><span class='icon-cog'></span></a>
                        <a href='<?= site_url('ea/trips/delete') ?>/<?= $safa_trip_id ?>'  onclick="if( ! confirm('<?= lang('global_are_you_sure_you_want_to_delete') ?>')) return false"><span class='icon-trash'></span></a> 
                        <? else: ?>
                        <a href='<?= site_url('ea/trip_internaltrip/edit') ?>/<?= $item->trip_internaltrip_id ?>'><span class='icon-truck'></span></a>
                        <? endif ?>
                        
                        <a href='<?= site_url('ea/trips/send_to_uo_popup') ?>/<?= $safa_trip_id ?>' title="<?= lang('send_to_uo'); ?>"  class="fancybox fancybox.iframe"><span class='icon-envelope'></span></a> 
                        
                    </td>
                    </tr>
                    <? endforeach ?>
                </tbody>
            </table> 
            </form>      
        </div>
    </div>
          
</div>
</div>
<script>
    $(document).ready(function() {
        $('.fancybox').fancybox({
            afterClose: function() {
            	//By Gouda.
                //location.reload();
            }
        });
    });
</script>


