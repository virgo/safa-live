<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); ?>
<link href="<?= CSS ?>/stylesheets.css" rel="stylesheet" type="text/css" />
<? if (lang('global_lang') == 'ar'): ?>
    <link href="<?= CSS ?>/stylesheet-ar.css" rel="stylesheet" type="text/css" />
<? else: ?><? endif ?>
<script type="text/javascript" src='<?= JS ?>/plugins/jquery/jquery-1.9.1.min.js'></script>
<script type="text/javascript" src='<?= JS ?>/plugins/jquery/jquery-ui-1.10.1.custom.min.js'></script>
<script type="text/javascript" src='<?= JS ?>/plugins/bootstrap/bootstrap.min.js'></script>
<link href="<?= CSS_JS ?>/dateandtime/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src='<?= CSS_JS ?>/dateandtime/bootstrap-datetimepicker.min.js'></script>
<style>

body{
height: auto!important;
overflow: auto !important;
background:none transparent !important;
overflow: hidden;
min-height: 1%;
}

.row-fluid{
padding-top: 5px !important;
padding-bottom: 5px !important;
}
</style>


<div class="row-fluid">
    <div class="row-fluid">       
                  
      <div class="widget">
        <div class="modal-header">
            <?= lang('trip_internaltrip') ?>                      
        </div>
        <div class="modal-body">
            <div class="row-form" >
                <?= form_open() ?>
                <?= lang('trip_ito') ?>: <?= form_dropdown('ito_id', $itos, set_value('ito_id')) ?>
                <br />
                <br />
                <div align="center">
                    <input type="submit" value="<?= lang('global_submit') ?>" class="btn btn-primary" />
                </div>
                <?= form_close() ?>
            </div>
        </div>
    </div>
          
</div>
</div>
