<!-- Add prettify -->
<script type="text/javascript" src="/js/prettify.js"></script>
<div style="margin-bottom:10px; text-align:left; border:3px solid #FFF; background:url(<?= IMAGES ?>/backgrounds/bg_box_head.jpg); padding:5px 10px; height:30px;">
      <div style="float:right; text-align:right; font-size:14px; line-height:30px; color:#333;">
        <a href='<?=  site_url('ea/dashboard')?>'><?= lang('global_system_management') ?></a>
        <span style="color:#80693d"><img src="<?= IMAGES ?>/jbreadcrumb/Chevron-ar.gif" width="5" height="20"></span>
        <a href="<?= $url?>"> <?= lang('trips') ?></a> 
      </div>
</div>
<div class="msg">
    <p><?= $msg ?></p>
    
    <? if($item->internal_trips == 0): ?>
    <a id="hide_this_button" data-fancybox-type="iframe" class="hide_this_button btn btn-primary"  href='<?= site_url('ea/trips/generate_tit') ?>/<?= $item->safa_trip_id ?>'><span class='icon-cog'></span> <?= lang('generate_trip_internaltrip') ?></a>
    <? else: ?>
    <p><?= lang('please_update_your_internal_trip') ?></p>
    <? endif ?>
    <input type="button" value="<?= lang('global_trip_details') ?>"class="btn btn-primary" onclick="window.location = '<?= site_url('trip_details/details/'. $item->safa_trip_id) ?>'">
    <input type="button" value="<?= lang('global_submit') ?>"class="btn btn-primary" onclick="window.location = '<?= $url ?>'">
    <input type="button" value="<?= lang('global_back') ?>" class="btn btn-primary" onclick="window.location ='<?= $back ?>'">
</div>
<style>
    .msg{
        padding:8px 35px 8px 14px;margin-bottom:20px;color:#c09853;text-shadow:0 1px 0 rgba(255,255,255,0.5);background-color:#fcf8e3;border:1px solid #fbeed5;-webkit-border-radius:4px;-moz-border-radius:4px;border-radius:4px;
        color:#468847;background-color:#dff0d8;border-color:#d6e9c6; border: 3px solid rgb(255, 255, 255); text-align: center;
    }  
</style>

<script type="text/javascript">
	$(document).ready(function() {
		$("#hide_this_button").fancybox({
                    afterClose : function () {
                        $('#hide_this_button').hide();
                    }
                });
	});
</script>