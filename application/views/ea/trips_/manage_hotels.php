<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); ?>


<?php 

$erp_city_id= '';
$erp_hotel_id= '';
$nights_count= '';
$erp_meal_id= '';
$odd_room_count= '';
$even_room_count= '';
$checkin_datetime= '';
$checkout_datetime= '';
	
if(count($item)>0) {
	$erp_city_id= $item->erp_city_id;
	$erp_hotel_id= $item->erp_hotel_id;
	$nights_count= $item->nights_count;
	$erp_meal_id= $item->erp_meal_id;
	$odd_room_count= $item->odd_room_count;
	$even_room_count= $item->event_room_count;
	$checkin_datetime= $item->checkin_datetime;
	$checkout_datetime= $item->checkout_datetime;
}
?>


<div class="row-fluid">


<div class="row-form">
<div class="span6">
<div class="span4 "><?php echo lang('city') ?></div>
<div class="span8">
<?php echo form_dropdown('erp_city_id', $erp_cities, set_value('erp_city_id', $erp_city_id)) ?>
</div>
</div>
<div class="span6">
<div class="span4"><?php echo lang('hotel') ?></div>
<div class="span8">
<?php echo form_dropdown('erp_hotel_id', $erp_hotels, set_value('erp_hotel_id', $erp_hotel_id)) ?>
</div>
</div>
</div>

<div class="row-form">
<div class="span6">
<div class="span4 "><?php echo lang('nights_count') ?></div>
<div class="span8">
<?php echo form_input('nights_count', set_value('nights_count',$nights_count)) ?>
</div>
</div>
<div class="span6">
<div class="span4"><?php echo lang('meal') ?></div>
<div class="span8">
<?php echo form_dropdown('erp_meal_id', $erp_meals, set_value('erp_meal_id', $erp_meal_id)) ?>
</div>
</div>
</div>

<div class="row-form">
<div class="span6">
<div class="span4 "><?php echo lang('odd_room_count') ?></div>
<div class="span8">
<?php echo form_input('odd_room_count', set_value('odd_room_count',$odd_room_count)) ?>
</div>
</div>
<div class="span6">
<div class="span4 "><?php echo lang('even_room_count') ?></div>
<div class="span8">
<?php echo form_input('even_room_count', set_value('even_room_count',$even_room_count)) ?>

</div>
</div>
</div>

<div class="row-form">
<div class="span6">
<div class="span4 "><?php echo lang('checkin_datetime') ?></div>
<div class="span8"><input type="text" name="basic_example_3" id="basic_example_5" value="" /></div>
</div>
<div class="span6">
<div class="span4"> <?php echo lang('checkout_datetime') ?></div>
<div class="span8"><input type="text" name="basic_example_3" id="basic_example_6" value="" /></div>
</div>
</div>

            </div>                   
          

<?php $this->load->view("ea/common/footer") ?>
