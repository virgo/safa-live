<!-- By Gouda, For popup -->
<script type="text/javascript" src='<?= CSS_JS ?>/form/form.js'></script>
<link rel="stylesheet" href="<?= CSS ?>/bootstrap/bootstrap.min.new.css"/>

<div class="widget">

    <div class="path-container Fright">
        <div class="path-name Fright">
            <a href="<?php echo site_url() . 'ea/dashboard'; ?>"><?php echo lang('global_system_management') ?></a>
        </div>
        <div class="path-arrow Fright"></div>
        <div class="path-name Fright"> <?php echo lang('safa_umrahgroups_title') ?></div>
    </div>
</div>

<div class="widget">
    <style>
 .widget{width:98%;}

        .updated_msg{
            display:none;
            background-color:#ccee97;
            font-weight: bold;
            text-align: center;
            border:3px solid #cccdc9; 
            padding:20px;  
            margin-bottom:10px;
            border-radius:15px;
            margin:10px; 
        }
        .wizerd-div,.row-form{width: 96%;margin:6% 2%;}
        label{padding: 0;}
    </style>
    <div class='row-fluid' align='center' >
        <div  class='updated_msg' >
            <br><input  type ="button" value="<?= lang('global_submit') ?>"class="btn btn-primary" id='ok' >
        </div> 
    </div>



    <div class="widget-header">
        <div class="widget-header-icon Fright"> <span class="icos-pencil2"></span> </div>
        <div class="widget-header-title Fright">
            <?php echo lang('global_search') ?>
        </div>
        <a href="<?= site_url("ea/safa_umrahgroups/add") ?>" class="btn Fleft"><?= lang('global_add') ?></a>
    </div>

    <div class="block-fluid slidingDiv">
        <?= form_open('ea/safa_umrahgroups/index', 'method="get"') ?>



        <div class="span6">
            <div class="wizerd-div"><a><?= lang('passports_data') ?></a></div>
            <div class="row-fluid"style="margin: 3%;width: 94%;border-left:1px solid #fff">
                <div class="span4" >
                    <div class="span12"> <?= lang("safa_umrahgroups_creation_date") ?></div>
                    <div class="span12">
                        <div class="span2">
                            <?= lang("safa_umrahgroups_from_date") ?>
                        </div>

                        <!-- 
                            <div id="datetimepicker1" class="input-append date">
                        -->    

                        <div id="" class="input-append date span9">
                            <input class="from_creation_date span12" data-format="yyyy-MM-dd" name="from_creation_date" id="from_creation_date" type="text" value="<?= set_value('from_creation_date') ?>"></input>
                            <script>
                                $('.from_creation_date').datepicker({
                                    dateFormat: "yy-mm-dd",
                                    controlType: 'select',
                                    timeFormat: 'HH:mm'
                                });
                            </script>
                            <!-- 
                            <span class="add-on">
                                <i data-time-icon="icon-time" data-date-icon="icon-calendar">
                                </i>
                            </span>
                            -->

                        </div>
                        <span class="bottom" style='color:red' >
                            <?= form_error('from_creation_date') ?> 
                        </span>



                    </div>

                    <div class="span12">
                        <div class="span2">
                            <?= lang("safa_umrahgroups_to_date") ?>
                        </div>
                        <!-- <div id="datetimepicker2" class="input-append date">-->
                        <div id="" class="input-append date span9">

                            <input class="to_creation_date span12" data-format="yyyy-MM-dd" name="to_creation_date" id="to_creation_date" type="text" value="<?= set_value('to_creation_date') ?>" ></input>

                            <script>
                                $('.to_creation_date').datepicker({
                                    dateFormat: "yy-mm-dd",
                                    controlType: 'select',
                                    timeFormat: 'HH:mm'
                                });
                            </script>
                            <!-- 
                            <span class="add-on">
                                <i data-time-icon="icon-time" data-date-icon="icon-calendar">
                                </i>
                            </span>
                            -->
                        </div>
                        <span class="bottom" style='color:red' >
                            <?= form_error('to_creation_date') ?> 
                        </span>    

                    </div>


                </div>

                <div class="span4" >
                    <div class="span12">
                        <?= lang("safa_umrahgroups_no_of_mutamers") ?>:
                    </div>
                    <div class="span12"> 


                        <div class="span12" >
                            <div class="span2">
                                <?= lang("safa_umrahgroups_from_date") ?>
                            </div>
                            <div class="span9"> 
                                <?= form_input('safa_umrahgroups_no_of_mutamers_from', set_value("safa_umrahgroups_no_of_mutamers_from"), "class='input-huge'  ") ?>
                            </div>
                        </div>
                        <div class="span12" >
                            <div class="span2">
                                <?= lang("safa_umrahgroups_to_date") ?>
                            </div>
                            <div class="span9"> 
                                <?= form_input('safa_umrahgroups_no_of_mutamers_to', set_value("safa_umrahgroups_no_of_mutamers_to"), "class='input-huge'  ") ?>
                            </div>
                        </div>



                    </div>
                </div>

                <div class="span4" >
                    <div class="span12">
                        <?= lang("safa_umrahgroups_name") ?>:
                    </div>
                    <div class="span12"> 
                        <?= form_dropdown('safa_umrahgroup_id', ddgen('safa_umrahgroups', array('safa_umrahgroup_id', 'name')), set_value('safa_umrahgroup_id'), ' id="safa_umrahgroup_id" class="select input-huge" style="width:100%;" ') ?>
                    </div>
                </div>

                <div class="span12" style="margin-top: 0;">


                    <div class="span4" >
                        <div class="span12">
                            <?= lang("safa_umrahgroups_contract_id") ?>:
                        </div>
                        <div class="span12"> 
                            <?= form_dropdown("safa_uo_contract_id", $uo_contracts, set_value("safa_uo_contract_id"), " id='safa_uo_contract_id' class='select input-huge'  ") ?>
                        </div>
                    </div>




                    <div class="span4" >
                        <div class="span12">
                            <?= lang("safa_umrahgroups_status_id") ?>:
                        </div>
                        <div class="span12"> 
                            <?= form_dropdown("safa_umrahgroups_status_id", $safa_umrahgroups_status, set_value("safa_umrahgroups_status_id"), " name='safa_umrahgroups_status_id' class='select' style='width:100%;' ") ?>
                        </div>
                    </div>

                </div>
            </div>




        </div>




        <div class="span6">


            <div class="wizerd-div"><a><?= lang("mutamers_data") ?></a></div>

            <div class="row-fluid"style="margin: 3%;width: 94%;">


                <div class="span4">
                    <div class="span10"> <label> <?= lang('mutamer_name') ?>:</label></div>
                    <div class="span11">
                        <?= form_input('mutamer_name', set_value("mutamer_name", ''), "class='input-huge'") ?>
                        <?= form_error('mutamer_name', '<div class="bottom" style="color:red" >', '</div>'); ?>
                    </div>
                    <div class="span12">
                        <div class="span11">  <label><?= lang('group_passports_relative_no') ?>:</label></div>
                        <div class="span11">
                            <?= form_input('relative_no', set_value("relative_no", ''), " ") ?>
                            <?= form_error('relative_no', '<div class="bottom" style="color:red" >', '</div>'); ?>
                        </div>
                    </div>
                </div>





                <div class="span4">

                    <div class="span12">
                        <div class="span11"> <label> <?= lang('group_passports_passport_no') ?>:</label></div>
                        <div class="span11">
                            <?= form_input('passport_no', set_value("passport_no", ''), " ") ?>
                            <?= form_error('passport_no', '<div class="bottom" style="color:red" >', '</div>'); ?>
                        </div>
                    </div>

                    <div class="span12">
                        <div class="span11"> <label> <?= lang('group_passports_nationality_id') ?>:</label></div>
                        <div class="span11">

                            <?= form_dropdown('nationality_id', ddgen('erp_nationalities', array('erp_nationality_id', name())), set_value('nationality_id', ''), "  class='select input-huge' ") ?>
                            <?= form_error('nationality_id', '<div class="bottom" style="color:red" >', '</div>'); ?>
                        </div>
                    </div>


                </div>

                <div class="span4">

                    <div class="span12">
                        <div class="span11"> <label><?= lang('group_passports_age') ?>:</label> </div>
                        <div class="span11">
                            <div class="span2">من</div>
                            <div class="span10">
                                <?= form_input('age', set_value("age", ''), " ") ?>
                                <?= form_error('age', '<div class="bottom" style="color:red" >', '</div>'); ?></div>
                        </div>
                        <div class="span11">
                            <div class="span2">إلى</div>
                            <div class="span10">
                                <?= form_input('age', set_value("age", ''), " ") ?>
                                <?= form_error('age', '<div class="bottom" style="color:red" >', '</div>'); ?></div>
                        </div>
                    </div>



                </div>

            </div>
        </div>

    </div>
    <div class="toolbar bottom TAC"><input style="margin: 10px 0;" type="submit" name="search" value="<?= lang('global_search') ?>" class="btn btn-primary" /></div>
    <?= form_close() ?>

</div>


<div class="widget">

    <div class="widget-header">
        <div class="widget-header-icon Fright"> <span class="icos-pencil2"></span> </div>
        <div class="widget-header-title Fright">
            <?php echo lang('safa_umrahgroups_title') ?>
        </div>
    </div>

    <div class="block-fluid">
        <table cellpadding="0" class="fsTable" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th><?= lang('safa_umrahgroups_th_name') ?></th>
                    <th><?= lang('safa_umrahgroups_contract_id') ?></th>
                    <th><?= lang('safa_umrahgroups_status_id') ?></th>
                    <th><?= lang('safa_umrahgroups_creation_date') ?></th>
                    <th><?= lang('safa_umrahgroups_no_of_mutamers') ?></th>
                    <th><?= lang('global_actions') ?></th>
                </tr>
            </thead>
            <tbody>
                <? if (isset($items)): ?>

                    <? foreach ($items as $item): ?>

                        <tr id="row_<?php echo $item->safa_umrahgroup_id; ?>">
                            <td> <?= $item->name ?></td>
                            <td> 
                                <?php
                                if (name() == 'name_la') {
                                    echo $item->safa_uo_contracts_eas_name;
                                } else {
                                    echo $item->safa_uo_contracts_eas_name;
                                }
                                ?></td>
                            <td> 
                                <?php
                                if (name() == 'name_la') {
                                    echo $item->umrahgroups_status_name_la;
                                } else {
                                    echo $item->umrahgroups_status_name_ar;
                                }
                                ?></td>
                            <td><?= $item->creation_date ?></td>
                            <td><?= $item->no_of_mutamers ?></td>
                            <td class="TAC">
                                <? if (!$item->uasp_id): ?>
                                    <? if ($item->erp_uasp_id == '1'): ?>
                                        <a class="fancybox fancybox.iframe" title="<? lang('babelumrah') ?>" href="<?= site_url("ea/safa_umrahgroups/send_to_babelumrah") ?>/<?= $item->safa_umrahgroup_id ?>"><span class="icon-desktop"></span></a>
                                    <? endif ?>
                                    <? if ($item->erp_uasp_id == '4'): ?>
                                        <a class="fancybox fancybox.iframe" title="<? lang('tawaf') ?>" href="<?= site_url("ea/safa_umrahgroups/send_to_tawaf") ?>/<?= $item->safa_umrahgroup_id ?>"><span class="icon-desktop"></span></a>
                                    <? endif ?>
                                    <? if ($item->erp_uasp_id == '3'): ?>
                                        <a class="fancybox fancybox.iframe" title="<? lang('gama') ?>" href="<?= site_url("ea/safa_umrahgroups/send_to_gama") ?>/<?= $item->safa_umrahgroup_id ?>"><span class="icon-desktop"></span></a>
                                    <? endif ?>
                                <? else: ?>
                                    <? //if ($item->erp_uasp_id == '1'): ?>
                                        <a class="fancybox fancybox.iframe" href="<?= site_url("ea/safa_umrahgroups/getmofa") ?>/<?= $item->safa_umrahgroup_id ?>"><span class="icon-desktop"></span></a>
                                    <? //endif ?>
                                        
                                <? endif ?>
                                <a title="<? lang('show_mutamers') ?>" href="<?= site_url("ea/umrahgroups_passports/passports_index") ?>/<?= $item->safa_umrahgroup_id ?>"><span class="icon-user"></span></a>
                                <a href="<?= site_url("ea/safa_umrahgroups/edit") ?>/<?= $item->safa_umrahgroup_id ?>"><span class="icon-pencil"></span></a>
                                
                                <? if ($this->safa_umrahgroups_model->check_delete_ability($item->safa_umrahgroup_id) == 0): ?> 
                                    <a href="#fModal" name="<?= $item->name ?>"  id="<?= $item->safa_umrahgroup_id ?>" class="delete_item" data-toggle="modal">
                                        <span class="icon-trash"></span></a>
                                <? else: ?><? endif ?>

                                <!-- 
                                <a title="<? lang('show_mutamers') ?>" href="<?= site_url("ea/safa_umrahgroups/ب") ?>/<?= $item->safa_umrahgroup_id ?>"><span class="icon-check"></span></a>
                                -->

                            </td>
                        </tr>

                    <? endforeach; ?>

                <? endif; ?>
            </tbody>
        </table>
        <?= form_close() ?>
    </div>
</div>

<div class="row-fluid">
    <?= $pagination ?>
    <div id="fModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h3 id="myModalLabel"><?= lang('global_system_message') ?></h3>
        </div>        
        <div class="row-fluid">
            <div id="msg" class="row-form">
                <?= lang('global_are_you_sure_you_want_to_delete') ?>
            </div>
        </div>                   
        <div class="modal-footer">
            <button class="btn btn-primary" data-dismiss="modal" aria-hidden="true"id="yes"><?= lang('global_yes') ?></button>
            <button class="btn btn-primary" data-dismiss="modal" aria-hidden="true"id="no"><?= lang('global_no') ?></button>       </div>
    </div>
    <script type="text/javascript">
        $(document).ready(function() {
            $('.fancybox').fancybox({width: '80%'});
        });
        $(document).ready(function() {
            $('.delete_item').click(function() {
                var name = $(this).attr('name');
                var safa_umrahgroup_id = $(this).attr('id');
<? if (lang('global_lang') == 'ar'): ?>
                    $('#msg').text("<?= lang('global_are_you_sure_you_want_to_delete') ?>" + " " + name)
<? else: ?>
                    $('#msg').text(name + " " + "<?= lang('global_are_you_sure_you_want_to_delete') ?>")
<? endif; ?>
                $('#yes').click(function() {
                    var answer = $(this).attr('id');
                    if (answer === 'yes') {
                        $.ajax({
                            type: "POST",
                            dataType: 'json',
                            url: "<?php echo site_url('ea/safa_umrahgroups/delete'); ?>",
                            data: {'safa_umrahgroup_id': safa_umrahgroup_id},
                            success: function(msg) {
                                if (msg.response == true) {
                                    var del = safa_umrahgroup_id;
                                    $("#row_" + del).remove();
<? if (lang('global_lang') == 'ar'): ?>
                                        $('.updated_msg').text("<?= lang('global_delete_confirm') ?>" + " " + name);
                                        $('.updated_msg').append('<br>');
                                        $('.updated_msg').append('<input  type ="button" value="<?= lang('global_submit') ?>"class="btn btn-primary" id="ok" >');
<? else: ?>
                                        $('.updated_msg').text(name + " " + "<?= lang('global_delete_confirm') ?>");
                                        $('.updated_msg').append('<br>');
                                        $('.updated_msg').append('<input  type ="button" value="<?= lang('global_submit') ?>"class="btn btn-primary" id="ok" >');
<? endif; ?>
                                    $('.updated_msg').show();
                                    $('#ok').click(function() {
                                        $('.updated_msg').hide();
                                    });
                                } else if (msg.response == false) {
                                    $('.updated_msg').text(msg.msg);
                                    $('.updated_msg').append('<br>');
                                    $('.updated_msg').append('<input  type ="button" value="<?= lang('global_submit') ?>"class="btn btn-primary" id="ok" >');
                                    $('.updated_msg').show();
                                    $('#ok').click(function() {
                                        $('.updated_msg').hide();
                                    });
                                }
                            }
                        });
                    } else {
                        return FALSE;
                    }
                });
            });
        });
    </script>
    <script type="text/javascript">
        $(function() {
            $('#datetimepicker1').datetimepicker({
                language: 'pt-BR',
                pickTime: false
            });
        });
    </script>
    <script type="text/javascript">
        $(function() {
            $('#datetimepicker2').datetimepicker({
                language: 'pt-BR',
                pickTime: false
            });
        });
    </script>

    <script type="text/javascript">
        $(document).ready(function() {
            $('#safa_uo_contract_id').change(function() {

                var safa_uo_contract_id = $(this).val();

                var dataString = 'safa_uo_contract_id=' + safa_uo_contract_id;

                $.ajax
                        ({
                            type: 'POST',
                            url: '<?php echo base_url() . 'ea/safa_umrahgroups/getUmrahgroupsByContract'; ?>',
                            data: dataString,
                            cache: false,
                            success: function(html)
                            {
                                //alert(html);
                                $("#safa_umrahgroup_id").html(html);
                                $("#safa_umrahgroup_id").trigger("chosen:updated");

                            }
                        });
            });
        });
    </script>