<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); ?>
<script type="text/javascript" src='<?= JS ?>/plugins/bootstrap/bootstrap.min.js'></script>
<link href="<?= CSS_JS ?>/dateandtime/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src='<?= CSS_JS ?>/dateandtime/bootstrap-datetimepicker.min.js'></script>

<div class="row-fluid">
    <div class="widget">
        <div class="block-fluid">
            <?= form_open() ?>
            <input type="hidden" name="contract" value="<?= $contract ?>" class="contract" />
            <div class="row-form">
                <div class="span6">
                    <div class="span4">
                        <?= lang("safa_umrahgroups_end_date") ?>
                    </div>
                    <div class="span8"> 
                        <?= form_input('end_date', set_value('end_date'), 'class="datepicker validate[required]"') ?>
                    </div>
                </div>
            </div>
            <div class="row-form">
                <div class="span6">
                    <div class="span4">
                        <?= lang("safa_umrahgroups_consulate") ?>
                    </div>
                    <div class="span8"> 
                        <?= form_dropdown('consulate', $consulates, set_value('consulate'), 'class="validate[required]"') ?>
                    </div>
                </div>
            </div>
            <div class="row-form">
                <div class="span12">
                    <div class="toolbar bottom TAC">
                        <input type="submit" name="submit" value="<?= lang('global_submit') ?>" class="btn btn-primary"> &nbsp;
                    </div>
                </div>
            </div>
            <?= form_close() ?>     
        </div>
    </div>
</div>
<script type="text/javascript">
    $(function() {
        $('#datetimepicker1').datetimepicker({
            language: 'pt-BR',
            pickTime: false
        });


        $('.contract').change(function() {
            if ($('.contract').val() == '')
                alert("<?= lang('you_must_select_contract') ?>");
            else
                $.get('<?= site_url('ea/safa_umrahgroups/updateGamaMenu') ?>/' + $('.contract').val(), function(data) {
                    $('.dropdown').html(data);
                });
        });
    });
    function icon_refresh() {
        if ($('.contract').val() == '')
            alert("<?= lang('you_must_select_contract') ?>");
        else
            $.get('<?= site_url('ea/safa_umrahgroups/getGamaPackages') ?>/' + $('.contract').val(), function(data) {
                $('.dropdown').html(data);
            });
    }
</script>

