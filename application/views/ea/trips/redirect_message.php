<!-- Add prettify -->
<script type="text/javascript" src="/js/prettify.js"></script>


<div class="widget">
    <div class="path-container Fright">
        <div class="path-name Fright">
        <a href='<?=  site_url('ea/dashboard')?>'><?= lang('global_system_management') ?></a>
        </div>
        <div class="path-arrow Fright">
        </div>        
        <div class="path-name Fright">
        <a href="<?= $url?>"> <?= lang('trips') ?></a>   
        </div>
        <div class="path-arrow Fright">
        </div>        
        <div class="path-name Fright">
        <?= $action ?>
        </div>
    </div>
</div>

<div class="widget">
<div class="msg">
    <p><?= $msg ?></p>
    
    <input type="button" value="<?= lang('global_trip_details') ?>"class="btn btn-primary" onclick="window.location = '<?= site_url('trip_details/details/'. $item->safa_trip_id) ?>'">
    <input type="button" value="<?= lang('global_submit') ?>"class="btn btn-primary" onclick="window.location = '<?= $url ?>'">
    <input type="button" value="<?= lang('global_back') ?>" class="btn btn-primary" onclick="window.location ='<?= $back ?>'">
</div>
</div>

<style>
    .msg{
        padding:8px 35px 8px 14px;margin-bottom:20px;color:#c09853;text-shadow:0 1px 0 rgba(255,255,255,0.5);background-color:#fcf8e3;border:1px solid #fbeed5;-webkit-border-radius:4px;-moz-border-radius:4px;border-radius:4px;
        color:#468847;background-color:#dff0d8;border-color:#d6e9c6; border: 3px solid rgb(255, 255, 255); text-align: center;
    }  
</style>

<script type="text/javascript">
	$(document).ready(function() {
		$("#hide_this_button").fancybox({
                    afterClose : function () {
                        $('#hide_this_button').hide();
                    }
                });
	});
</script>