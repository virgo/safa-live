

<div class="row-fluid">
    <div class="row-fluid">       
                  
      <div class="widget">
        <div class="modal-header">
            <?= lang('trip_internaltrip') ?>                      
        </div>
        <div class="modal-body">
            <div class="row-form" >
                <?= form_open('trip_details/generate_tit/'. $item->safa_trip_id ) ?>
                
                <?php if($services_buy_uo_transporter!=1) {?>
                <?= lang('trip_ito') ?>: <?= form_dropdown('ito_id', $itos, set_value('ito_id'),' class="validate[required]"') ?>
                <?php }?>
                <br />
                <div align="center">
                    <input type="submit" value="<?= lang('global_submit') ?>" name="smt_save_tit" class="btn btn-primary" />
                </div>
                <?= form_close() ?>
            </div>
        </div>
    </div>
          
</div>
</div>
