
<script>.widget{width:98%;}</script>
<div class="widget">
    <div class="path-container Fright">
        <div class="path-name Fright">
            <a href="<?php echo site_url() . 'ea/dashboard'; ?>"><?php echo lang('global_system_management') ?></a>
        </div>
        <div class="path-arrow Fright"></div>
        <div class="path-name Fright"><?= lang('trips') ?></div>
    </div>



</div>

<div class="row-fluid">
    <div class="row-fluid">       

        <div class="widget">


            <div class="widget-header">
                <div class="widget-header-icon Fright"> <span class="icos-pencil2"></span> </div>
                <div class="widget-header-title Fright">
                    <?php echo lang('trips') ?>
                </div>
                <a title='<?= lang('global_add') ?>'href="<?= site_url('ea/trips/manage') ?>" class="btn Fleft">
                    <?= lang('global_add') ?>
                </a>
            </div>



<form action="<?php echo base_url() . "ea/trips/index"; ?>" id="trips_search" name="trips_search" method="GET">
 <?= form_open("","method='get'") ?>
 
<div class="widget-container"><? if(validation_errors()){ ?> <?php echo validation_errors(); ?>
<? } ?>

<div class="row-form">


<div class="span6">
<div class="span4 TAL Pleft10"><?php echo  lang('name') ?>
</div>
<div class="span8"><?php echo form_dropdown('safa_trip_id', $safa_trips,  set_value('safa_trip_id'), "id='safa_trip_id' class=' chosen-select' ") ?>
</div>
</div>


<div class="span6">
<div class="span4 TAL Pleft10"><?php echo  lang('the_date') ?></div>
<div class="span4">
<?php echo form_input('date_from', set_value("date_from")," style='' id='date_from' class=' input-full date' ") ?>
</div>
<div class="span4">
<?php echo form_input('date_to', set_value("date_to")," style='' id='date_to' class=' input-full date' ") ?>
</div>

</div>

</div>

<div class="row-form">
<div class="span6">
<div class="span4 TAL Pleft10"><?php echo  lang('arrival_date') ?> </div>
<div class="span8"><?php echo form_input('arrival_date', set_value("arrival_date")," style='' id='arrival_date' class=' input-full date'") ?>
</div>
</div>

<div class="span6">
<div class="span4 TAL Pleft10"><?php echo  lang('moushrif_name') ?> </div>
<div class="span8"><?php echo form_multiselect('safa_ea_supervisor_ids[]', $supervisors,set_value('safa_ea_supervisor_ids'), "id='safa_ea_supervisor_ids'  class=' chosen-rtl chosen-select'") ?>
</div>
</div>

</div>

<div class="row-form">
<div class="span6">
<div class="span4 TAL Pleft10"><?php echo  lang('safa_tripstatus_id') ?>
</div>
<div class="span8"><?php echo form_dropdown('safa_tripstatus_id', $safa_tripstatus,  set_value('safa_tripstatus_id'), "id='safa_tripstatus_id' class=' chosen-select' ") ?>
</div>
</div>
<div class="span6">
<div class="span4 TAL Pleft10"><?php echo  lang('safa_trip_confirm_id') ?>
</div>
<div class="span8"><?php echo form_dropdown('safa_trip_confirm_id', $safa_trip_confirm,  set_value('safa_trip_confirm_id'), "id='safa_trip_confirm_id' class=' chosen-select' ") ?>
</div>
</div>

</div>

<div class="row-form">
<div class="span6">

<div class="span4 TAL Pleft10"><?php echo  lang('erp_country_id') ?></div>
<div class="span8"><?php echo form_dropdown('erp_country_id', ddgen('erp_countries', array('erp_country_id', name())),  set_value('erp_country_id'), "id='erp_country_id' class=' chosen-select' ") ?>
</div>
</div>
</div>

</div>

<div class=" TAC"><input type="submit" class="btn" name="search" value="<?php echo lang('global_search') ?>"></div>
	
</form>



            <div class="block-fluid">
                
                    <table class="fsTable" cellpadding="0" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th><?= lang('trip') ?></th>
                                <th><?= lang('travellers_count') ?></th>
                                <th><?= lang('passports_count_on_trip') ?></th>
                                <th><?= lang('the_date') ?></th>
                                <th><?= lang('global_actions') ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            if(isset($items)) {
                           	if(ensure($items)) {
                           		$items_arr=$items;
                           	} else {
                           		if(count($items)>0) {
                           			$items_arr[]=$items;
                           		} else {
                           			$items_arr=array();
                           		}
                           	}
                            foreach ($items_arr as $item) {

                                $safa_trip_id = $item->safa_trip_id;
                                $safa_trip_name = $item->name;
                                $date = $item->date;
                                $safa_trip_confirm_id = $item->safa_trip_confirm_id;
                                $safa_trip_confirm_name = lang('trip_confirm');
                                $safa_tripstatus_id = $item->safa_tripstatus_id;

                                $individuals_count = $item->individuals_count;
                                $passports_count = $item->passports_count;


                                if ($safa_tripstatus_id == 1) {
                                    $css_class = 'label label-info';
                                } else if ($safa_tripstatus_id == 2) {
                                    $css_class = 'label label-warning';
                                } else if ($safa_tripstatus_id == 3) {
                                    $css_class = 'label label-success';
                                }
                                ?>

                                <tr>
                                    <td><?= $safa_trip_name ?></td>
                                    <td class="TAC info"><?= $individuals_count ?></td>
                                    <td class="TAC success"><?= $passports_count ?></td>
                                    <td class="TAC warning"><?= $date ?></td>
                                    <? /* <td class='TAC'>
                                      <? if($safa_trip_confirm_id==1): ?>
                                      <a href='<?= site_url() ?>/ea/trips/confirm/<?= $safa_trip_id ?>/2' class='btn btn-primary'><?= $safa_trip_confirm_name ?></a>
                                      <? elseif($safa_trip_confirm_id==2): ?>
                                      <a href='<?= site_url() ?>/ea/trips/confirm/<?= $safa_trip_id ?>/1' class='btn btn-primary'><?= $safa_trip_confirm_name ?></a>
                                      <? endif ?>
                                      </td> */ ?>
                                    <td class='TAC'>
                                        <a href='<?= site_url('trip_details/details') ?>/<?= $safa_trip_id ?>'><span class='icon-search'></span></a> 
                                        <a href='<?= site_url('ea/trips/manage') ?>/<?= $safa_trip_id ?>'><span class='icon-pencil'></span></a> 


                                        <? if ($item->internal_trips == 0) { ?>

                                            <?php
                                            if ($item->erp_transportertype_id == 2) {
                                                $this->flight_availabilities_model->safa_trip_id = $safa_trip_id;
                                                $flight_availabilities_rows = $this->flight_availabilities_model->get();
                                                if (count($flight_availabilities_rows) > 0) {
                                                    if ($passports_count > 0) {
                                                        ?>


                                                        <a class="fancybox fancybox.iframe" href='<?= site_url('trip_details/generate_tit') ?>/<?= $safa_trip_id ?>'><span class='icon-cog'></span></a>
                                                        <?php
                                                    } else {
                                                        ?>
                                                        <a href='javascript:void(0);' title="<?= lang('generate_tit'); ?>"  onclick="alert('<?php echo lang('this_trip_not_have_packages'); ?>')"><span class='icon-cog'></span></a> 
                                                    <?php
                                                    }
                                                } else {
                                                    ?> 
                                                    <a href='javascript:void(0);' title="<?= lang('generate_tit'); ?>"  onclick="alert('<?php echo lang('this_trip_not_have_flight_availability'); ?>')"><span class='icon-cog'></span></a>
                                                    <?php
                                                }
                                            } else {
                                                ?>
                                                <a href='javascript:void(0);' title="<?= lang('generate_tit'); ?>"  onclick="alert('<?php echo lang('this_trip_not_flight'); ?>')"><span class='icon-cog'></span></a>
                                                <?php
                                            }
                                            ?>
                                            <a href='<?= site_url("ea/passport_accommodation_rooms/manage") ?>' rel='<?= $safa_trip_id ?>' id="add_accommodation" ><i class="icon-group"></i></a>

                                            <? if ($item->safa_tripstatus_id != 3 && $item->safa_tripstatus_id != 4) { ?>
                                                <a href='<?= site_url('ea/trips/delete') ?>/<?= $safa_trip_id ?>'  onclick="if (!confirm('<?= lang('global_are_you_sure_you_want_to_delete') ?>'))
                                                                        return false"><span class='icon-trash'></span></a> 
                                               <?php } ?>

                                        <? } else { ?>
                                            <a href='<?= site_url('ea/trip_internaltrip/edit') ?>/<?= $item->trip_internaltrip_id ?>'><span class='icon-truck'></span></a>
                                        <? } ?>


                                        <?php if ($item->erp_transportertype_id == 2) { ?>

                                            <? if ($item->internal_trips == 0) { ?>
                                                <a href='javascript:void(0);' title="<?= lang('send_to_uo'); ?>"  onclick="alert('<?php echo lang('this_trip_not_have_trip_internaltrip'); ?>')"><span class='icon-envelope'></span></a> 
                                            <?php } else if ($item->safa_tripstatus_id == 3) { ?>
                                                <a href='javascript:void(0);' title="<?= lang('send_to_uo'); ?>"  onclick="alert('<?php echo lang('trip_was_sent_to_uo'); ?>')"><span class='icon-envelope'></span></a> 
                                                <?php
                                            } else if ($passports_count == 0) {
                                                ?>
                                                <a href='javascript:void(0);' title="<?= lang('send_to_uo'); ?>"  onclick="alert('<?php echo lang('this_trip_not_have_packages'); ?>')"><span class='icon-envelope'></span></a> 
                                            <?php } else { ?>
                                                <a href='<?= site_url('ea/trips/send_to_uo_popup') ?>/<?= $safa_trip_id ?>' title="<?= lang('send_to_uo'); ?>"  class="fancybox fancybox.iframe"><span class='icon-envelope'></span></a> 
                                                <?php
                                            }
                                            ?>

                                        <?php } else { ?>
                                            <?php
                                            if ($passports_count == 0) {
                                                ?>
                                                <a href='javascript:void(0);' title="<?= lang('send_to_uo'); ?>"  onclick="alert('<?php echo lang('this_trip_not_have_packages'); ?>')"><span class='icon-envelope'></span></a> 
                                            <?php } else { ?>
                                                <a href='<?= site_url('ea/trips/send_to_uo_popup') ?>/<?= $safa_trip_id ?>' title="<?= lang('send_to_uo'); ?>"  class="fancybox fancybox.iframe"><span class='icon-envelope'></span></a> 
                                                    <?php
                                                }
                                            }
                                            ?>
									
									<a href='<?php echo site_url("ea/trips/passports/$safa_trip_id"); ?>' target="_blank" ><span class='icon-user'></span></a>
									
									
									<?php if ($item->safa_tripstatus_id == 3) {?> 
									<span class="icon-info-sign " style="color: #FFA500; position: absolute; left: 5px;" title="<?php echo $item->trip_status ; ?>"></span>
									<?php }?>
									
                                    </td>
                                </tr>
                                <?php
                            }
                            }
                            ?>
                        </tbody>
                    </table> 
                    
            </div>
        </div>

    </div>
</div>
<div id='inset_form' style="display: none;"></div>
<?php echo $pagination; ?>

<script>
    $(document).ready(function() {
        $('.fancybox').fancybox({
            afterClose: function() {
                parent.location.reload(true);
            }
        });

        $('#add_accommodation').click(function() {
            var href = $(this).attr('href');
            var tripid = $(this).attr('rel');
            $('#inset_form').html('<form action="' + href + '" name="accommodation" method="post" style="display:none;"><input type="text" name="safa_trip_id" value="' + tripid + '" /></form>');

            document.forms['accommodation'].submit();

            return false;
        });
    });
</script>

<script>
    if ($(".fsTable").length > 0) {

        //By Gouda, TO remove the warning message.
        //$.fn.dataTableExt.sErrMode = 'throw';

        $(".fsTable").dataTable({
            bSort: true,
            bAutoWidth: true,
            "iDisplayLength":<?php
$ea_user_id = session('ea_user_id');
if ($this->input->cookie('' . $ea_user_id . '_' . 'trips_rows_count_per_page' . '')) {
    echo $this->input->cookie('' . $ea_user_id . '_' . 'trips_rows_count_per_page' . '');
} else {
    echo 20;
}
?>,
            "aLengthMenu": [5, 10, 20, 50, 100, 200, 500], // can be removed for basic 10 items per page
            "bFilter": true,
            "sPaginationType": "full_numbers",
            "aoColumnDefs": [{"bSortable": false,
                    "aTargets": [-1]}]

        });
    }



</script>


<script type="text/javascript">
$(document).ready(function() {
    var config = {
        '.chosen-select': {width: "100%"},
        '.chosen-select-deselect': {allow_single_deselect: true},
        '.chosen-select-no-single': {disable_search_threshold: 10},
        '.chosen-select-no-results': {no_results_text: 'Oops, nothing found!'},
        '.chosen-select-width': {width: "95%"}
    }
    for (var selector in config) {
        $(selector).chosen(config[selector]);
    }


    // binds form submission and fields to the validation engine
    $("#trips_search").validationEngine({
            prettySelect : true,
            useSuffix: "_chosen",
            promptPosition : "topRight:-150"
            //promptPosition : "bottomLeft"
    });


    $('.date').datepicker({dateFormat: "yy-mm-dd"});
    
});

</script>