<script>.widget{width:98%;}</script>


<div class="widget">
<div class="path-container Fright">
<div class="path-name Fright"><a
	href="<?php echo site_url() . 'ea/dashboard'; ?>"><?php echo lang('global_system_management') ?></a>
</div>
<div class="path-arrow Fright"></div>
<div class="path-name Fright"><?= lang('trip_details_travellers') ?></div>
</div>



</div>

<div class="row-fluid">
<div class="row-fluid">

<div class="widget">


<div class="widget-header">
<div class="widget-header-icon Fright"><span class="icos-pencil2"></span>
</div>
<div class="widget-header-title Fright"><?php echo lang('trip_details_travellers') ?>
</div>

</div>

<div class="block-fluid">

<table class="" cellpadding="0" cellspacing="0" width="100%">
	<thead>
		<tr>

			<th><?= lang('trip_details_traveller') ?></th>
			<th><?= lang('trip_details_passport_no') ?></th>

			<!--<th><?= lang('trip_details_visa_no') ?></th>
                                                    <th><?= lang('trip_details_nationality') ?></th>
                                                    <th><?= lang('trip_details_gender') ?></th>
                                                    
                                                    <th><?= lang('group_passports_age') ?></th>
                                                    <th><?= lang('group_passports_occupation') ?></th>
                                                    
                                                    -->

			<th><?= lang('package_period') ?></th>
			<th><?= lang('accomedation') ?></th>
			<th><?= lang('hotelroomsize') ?></th>
		</tr>
	</thead>
	<tbody>
	<?
	$previous_passport_name = '';
	$previous_passport_number = '';
	$previous_total_package_period_days = '';

	$row_class = '';
	$previous_row_class = '';

	foreach ($passports as $passport){
		$passport_name_style="border-top: solid 1px rgb(221, 221, 221)";
		$passport_number_style="border-top: solid 1px rgb(221, 221, 221)";
		$total_package_period_days_style="border-top: solid 1px rgb(221, 221, 221)";
			
		$passport_name = $passport->{'first_' . name()} . ' ' . $passport->{'second_' . name()} . ' ' . $passport->{'third_' . name()} . ' ' . $passport->{'fourth_' . name()};
		$passport_number =$passport->passport_no ;
		$total_package_period_days = $passport->total_package_period_days;

		if($passport_name==$previous_passport_name) {
			$passport_name_style='';
		} else {
			$row_class_arr = array('success','info','warning');
			$row_class = $row_class_arr[array_rand($row_class_arr)];
			if($previous_row_class==$row_class) {
				$row_class='';
			}
			$previous_row_class = $row_class;
		}

		if($passport_number==$previous_passport_number) {
			$passport_number_style='';
		}
		if($previous_total_package_period_days==$previous_total_package_period_days) {
			$total_package_period_days_style='';
		}


		?>
		<tr>

			<td style="border-bottom:0px; <?php echo $passport_name_style;?>" class="<?php echo $row_class;?>">
			<?php

			if($passport_name!=$previous_passport_name) {
				echo $passport_name;
			}
			?></td>
			<td style="border-bottom:0px; <?php echo $passport_number_style;?>" class="<?php echo $row_class;?>">

			<?php
			if($passport_number!=$previous_passport_number) {
				echo $passport_number;
			}
			?></td>
			<!--<td><?= $passport->visa_number ?></td>
                                                        <td><?= $passport->erp_nationalities_name ?></td>
                                                        <td><?= $passport->erp_gender_name ?></td>
                                                        
                                                        
                                                        
                                                        <td>
                                                        
                                                        <?php
					                                    $date_of_birth = strtotime($passport->date_of_birth);
					                                    $date_now = strtotime(date('Y-m-d', time()));
					                                    $secs = $date_now - $date_of_birth; // == return sec in difference
					                                    $days = $secs / 86400;
					                                    $age = $days / 365;
														//$age=round($age);
														//$age=ceil($age);
					                                    $age = floor($age);
					                                    echo $age;
					                                    ?>
                                                        
                                                        </td>
                                                        <td><?= $passport->occupation ?></td>
                                                        
                                                        -->


			<td style="border-bottom:0px; <?php echo $total_package_period_days_style;?>" class="<?php echo $row_class;?>">
			<?php
			//if($total_package_period_days!=$previous_total_package_period_days) {
			if($passport_name!=$previous_passport_name) {
				echo $total_package_period_days;
			}
			?></td>
			<td class="<?php echo $row_class;?>"><?= $passport->erp_city_name ?>
			- <?= $passport->erp_hotel_name ?></td>
			<td class="<?php echo $row_class;?>"><?= $passport->erp_hotelroomsize_name ?></td>


		</tr>
		<?
		$previous_passport_name = $passport_name;
		$previous_passport_number = $passport_number;
		$previous_total_package_period_days = $total_package_period_days;
	}

	?>
	</tbody>
</table>

</div>
</div>

</div>
</div>
<div id='inset_form' style="display: none;"></div>
	<?php //echo $pagination; ?>

<script>
    $(document).ready(function() {
        $('.fancybox').fancybox({
            afterClose: function() {
                parent.location.reload(true);
            }
        });
    });
</script>

<script>
    if ($(".fsTable").length > 0) {

        //By Gouda, TO remove the warning message.
        //$.fn.dataTableExt.sErrMode = 'throw';

        $(".fsTable").dataTable({
            bSort: true,
            bAutoWidth: true,
            "iDisplayLength":<?php
$ea_user_id = session('ea_user_id');
if ($this->input->cookie('' . $ea_user_id . '_' . 'trips_rows_count_per_page' . '')) {
    echo $this->input->cookie('' . $ea_user_id . '_' . 'trips_rows_count_per_page' . '');
} else {
    echo 100;
}
?>,
            "aLengthMenu": [5, 10, 20, 50, 100, 200, 500], // can be removed for basic 10 items per page
            "bFilter": true,
            "sPaginationType": "full_numbers",
            "aoColumnDefs": [{"bSortable": false,
                    "aTargets": [-1]}]

        });
    }



</script>


<script type="text/javascript">
$(document).ready(function() {
    var config = {
        '.chosen-select': {width: "100%"},
        '.chosen-select-deselect': {allow_single_deselect: true},
        '.chosen-select-no-single': {disable_search_threshold: 10},
        '.chosen-select-no-results': {no_results_text: 'Oops, nothing found!'},
        '.chosen-select-width': {width: "95%"}
    }
    for (var selector in config) {
        $(selector).chosen(config[selector]);
    }


    // binds form submission and fields to the validation engine
    $("#trips_passports").validationEngine({
            prettySelect : true,
            useSuffix: "_chosen",
            promptPosition : "topRight:-150"
            //promptPosition : "bottomLeft"
    });


    $('.date').datepicker({dateFormat: "yy-mm-dd"});
    
});

</script>
