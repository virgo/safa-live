<?php
$safa_trip_id = 0;
$name = FALSE;
$date = date('Y-m-d');
$arrival_date = date('Y-m-d');
$safa_tripstatus_id = 1;
$safa_trip_confirm_id = 2;
$erp_country_id = $ea_country_id;
$erp_transportertype_id = '';
$notes = '';
$uo_refusing_reason='';


$screen_title = lang('add_title');

if(isset($item)) {

	$safa_trip_id = $item->safa_trip_id;
	$name = $item->name;
	$date = $item->date;
	$arrival_date = $item->arrival_date;
	$safa_tripstatus_id = $item->safa_tripstatus_id;
	$safa_trip_confirm_id = $item->safa_trip_confirm_id;
	$erp_country_id = $item->erp_country_id;
	$erp_transportertype_id = $item->erp_transportertype_id;
	$notes = $item->notes;
	$uo_refusing_reason = $item->uo_refusing_reason;
	
	$screen_title = lang('edit_title');
}

?>

<div class="widget">

<div class="path-container Fright">
<div class="path-name Fright"><a href="<?php echo  site_url() ?>"><?php echo  lang('global_system_management') ?></a>
</div>
<div class="path-arrow Fright"></div>
<div class="path-name Fright"><a
	href="<?php echo  site_url('ea/trips') ?>"><?php echo  lang('trips') ?></a></div>

<div class="path-arrow Fright"></div>
<div class="path-name Fright"><?php echo  $screen_title; ?></div>

</div>
</div>
<script>.widget{width:98%;}</script>


<?php echo  form_open_multipart(false, 'id="frm_trips" ') ?>


<div class="widget">
<div class="widget-header">

<div class="widget-header-icon Fright"><span class="icos-pencil2"></span>
</div>

<div class="widget-header-title Fright"><?php echo  $screen_title; ?></div>
</div>

<div class="widget-container"><? if(validation_errors()){ ?> <?php echo validation_errors(); ?>
<? } ?>

<div class="row-form">
<div class="span6">
<div class="span4 TAL Pleft10"><?php echo  lang('name') ?> <font
	style='color: red'>*</font></div>
<div class="span8"><?php echo form_input('name', set_value("name", $name)," style='' id='name' class='validate[required] input-full' ") ?>
</div>
</div>

<div class="span6">
<div class="span4 TAL Pleft10"><?php echo  lang('the_date') ?> <font
	style='color: red'>*</font></div>
<div class="span8"><?php echo form_input('the_date', set_value("the_date", $date)," style='' id='the_date' class='validate[required] input-full date' ") ?>
</div>
</div>

</div>

<div class="row-form">
<div class="span6">
<div class="span4 TAL Pleft10"><?php echo  lang('arrival_date') ?> <font
	style='color: red'>*</font></div>
<div class="span8"><?php echo form_input('arrival_date', set_value("arrival_date", $arrival_date)," style='' id='arrival_date' class='validate[required] input-full date'") ?>
</div>
</div>

<div class="span6">
<div class="span4 TAL Pleft10"><?php echo  lang('moushrif_name') ?> <font
	style='color: red'>*</font></div>
<div class="span8"><?php echo form_multiselect('safa_ea_supervisor_id[]', $supervisors,set_value('safa_ea_supervisor_id', $trip_supervisors), "id='safa_ea_supervisor_id'  class='validate[required] chosen-rtl chosen-select'") ?>
</div>
</div>

</div>

<div class="row-form">
<div class="span6">
<div class="span4 TAL Pleft10"><?php echo  lang('safa_tripstatus_id') ?>
<font style='color: red'>*</font></div>
<div class="span8"><?php echo form_dropdown('safa_tripstatus_id', $safa_tripstatus,  set_value('safa_tripstatus_id', $safa_tripstatus_id), "id='safa_tripstatus_id' class='validate[required] chosen-select' ") ?>
</div>
</div>

<?php if($safa_tripstatus_id==5) { ?>
<div class="span6">
<div class="span4"><?php echo lang('uo_refusing_reason'); ?></div>
<div class="span8"><?php echo form_textarea('uo_refusing_reason', set_value("uo_refusing_reason", $uo_refusing_reason)," id='uo_refusing_reason' class='input-full'"); ?>
</div>
</div>
<?php }?>
</div>

<div class="row-form">
<div class="span6">

<div class="span4 TAL Pleft10"><?php echo  lang('erp_country_id') ?> <font
	style='color: red'>*</font></div>
<div class="span8"><?php echo form_dropdown('erp_country_id', ddgen('erp_countries', array('erp_country_id', name())),  set_value('erp_country_id', $erp_country_id), "id='erp_country_id' class='validate[required] chosen-select' ") ?>
</div>
</div>

<div class="span6">
<div class="span4 TAL Pleft10"><?php echo  lang('notes') ?></div>
<div class="span8"><?php echo form_textarea('notes', set_value("notes", $notes)," id='notes' class='input-full'"); ?>
</div>
</div>

</div>

<?php
if($safa_trip_id!=0) {
	?>
<div class="row-form">

<div class="span12">
<div class="span2 TAL Pleft10"><?php echo  lang('travellers') ?></div>
<div class="span10"><a
	class="btn btn-primary finish fancybox fancybox.iframe"
	href="<?php echo  site_url('ea/group_passports/show_by_trip_popup/'.$safa_trip_id) ?>"><?php echo  lang('show') ?></a>
</div>
</div>

</div>
	<?php
}
?></div>




</div>








<div class="widget">
<div class="widget-header">

<div class="widget-header-icon Fright"><span class="icos-pencil2"></span>
</div>

<div class="widget-header-title Fright"><?php echo  lang('erp_transportertype_id'); ?>

</div>
</div>

<div class="widget-container">

<div class="row-form">
<div class="span6">
<div class="span4 TAL Pleft10"><?php echo  lang('erp_transportertype_id') ?>
<font style='color: red'>*</font></div>
<div class="span8"><?php echo  form_dropdown('erp_transportertype_id', $erp_transportertypes, set_value('erp_transportertype_id', $erp_transportertype_id), 'id="erp_transportertype_id" class="validate[required]"'); ?>
</div>
</div>
</div>

<div class="row-form">
<div class="span12">


















<div id="dv_air_trip" <?php if ($erp_transportertype_id != 2) { ?>
	style="display: none; width: 100%" <?php } else { ?>
	style="display: inline-block; width:100%" <?php } ?>>
<div class="resalt-group">
<div class="wizerd-div"><a><?php echo lang('flights') ?></a></div>

<?php
$safa_trip_going_ids=array();
if(isset($going_trip_flights_rows)) {
	foreach ($going_trip_flights_rows as $going_trip_flights_row) {
		$safa_trip_going_ids[$going_trip_flights_row->erp_flight_availability_id]=$going_trip_flights_row->erp_flight_availability_id;
	}
}

$all_flights_ids=array();
if(isset($all_flights_rows)) {
	foreach ($all_flights_rows as $all_flights_row) {
		$all_flights_ids[$all_flights_row->erp_flight_availability_id]=$all_flights_row->erp_flight_availability_id;
	}
}
?> <?php  echo form_multiselect('safa_trip_going_ids[]', $all_flights_ids, set_value('safa_trip_going_ids', $safa_trip_going_ids), " id='safa_trip_going_ids'  class='' readonly='readonly' style='display: none;' "); ?>

<div class="row-fluid">
<table >
	<thead>
		<tr>
			<!--  <th ><?= lang('externalsegmenttype') ?></th> -->
			<!--  <th ><?= lang('trip_code') ?></th> -->
			<!--<th><?= lang('flight') ?></th>
			<th><?= lang('flight_from_to') ?></th>
			<th><?= lang('airport_departure_datetime') ?></th>
			<th ><?= lang('airport_start') ?></th>
            <th ><?= lang('airport_end') ?></th>
			<th><?= lang('airport_arrival_datetime') ?></th>
			<th><?= lang('transporter') ?></th>
			<th><?= lang('seats_count') ?></th>
			-->
			<th width="75%"><?= lang('going_trip') ?></th>
			<th class="TAC">
			<a class="btn" href="javascript:void(0);" onclick="popup_trip();" ><?php echo lang('flight_availabilities') ?>
			</a>
			</th>
		</tr>
	</thead>

	<? $trip_going_conuter = 0; ?>

	<tbody>
	
	
	<tr id="row_<?php echo $this->flight_availabilities_model->erp_flight_availability_id; ?>">
                           
                            
                            <td colspan="2">
                            <input type='hidden'
							name='trip_going_erp_flight_availability_id<?= $trip_going_conuter ?>'
							id='trip_going_erp_flight_availability_id<?= $trip_going_conuter ?>'
							value='<?= $this->flight_availabilities_model->erp_flight_availability_id ?>'
							class='validate[required]' /> 
				
                            <table id='div_trip_going_group'>
                            <thead>
		                    <tr>
		                       	<th><?= lang('erp_path_type_id') ?></th>
		                       	<th><?= lang('flight_availabilities_airlines') ?></th>
						        <th><?= lang('flight_availabilities_flight_number') ?></th>
						        <th><?= lang('flight_availabilities_class') ?></th>
						        <th><?= lang('flight_availabilities_date') ?></th>
						        <th><?= lang('arrival_date') ?></th>
						        <th><?= lang('flight_availabilities_airports') ?></th>
						        <th><?= lang('flight_availabilities_status') ?></th>
						        <th><?= lang('flight_availabilities_seat_count') ?></th>
						        <!--<th><?= lang('flight_availabilities_department_time') ?></th>
						        <th><?= lang('flight_availabilities_arrival_time') ?></th>-->
<!--						        <th><a href="javascript:new_row(1);" class="btn" > <?php echo lang('global_add') ?> </a></th>-->
						        
		                    </tr>
			                </thead>
			                <tbody id="going_flight_tbody">
	
	
	
		<? if (isset($going_trip_flights_rows)) { ?>
			<? foreach ($going_trip_flights_rows as $value) {
                        $this->flight_availabilities_model->erp_flight_availability_id = $value->erp_flight_availability_id;
                        
                        //$this->flight_availabilities_model->erp_path_type_id = $flight_trip_type;
                        
                        $flight_availabilities_details_rows = $this->flight_availabilities_model->get_details_for_trip();
                        
                        $ports='';
                        
                        $safa_externaltriptype_name = '';
                        
                        $start_datetime='';
                        $end_datetime='';
                        $loop_counter=0;
                        
                        $erp_port_id_from = 0;
                        $erp_port_id_to =0;
                        $start_ports_name ='';
                        $end_ports_name ='';
                        
                        
                        
                       	?>
                            
			                
                            <?php 
                            
                            foreach($flight_availabilities_details_rows as $flight_availabilities_details_row) {
                        	if($loop_counter==0) {
                        		$safa_externaltriptype_name = $flight_availabilities_details_row->safa_externaltriptype_name;
                        		$erp_path_type_name = $flight_availabilities_details_row->erp_path_type_name;
                        		
                        		$erp_port_id_from = $flight_availabilities_details_row->erp_port_id_from;
                        		$start_ports_name =$flight_availabilities_details_row->start_ports_name;
                        
                        		$erp_port_id_to = $flight_availabilities_details_row->erp_port_id_to;
                        		$end_ports_name =$flight_availabilities_details_row->end_ports_name;
                        
                        		$ports = $flight_availabilities_details_row->start_ports_name.' - '.$flight_availabilities_details_row->end_ports_name;
                        		$start_datetime = $flight_availabilities_details_row->flight_date .' - '.$flight_availabilities_details_row->departure_time;
                        		$end_datetime = $flight_availabilities_details_row->flight_date .' - '.$flight_availabilities_details_row->arrival_time;
                        	} else {
                        		$safa_externaltriptype_name = $safa_externaltriptype_name.' <br/> '.$flight_availabilities_details_row->safa_externaltriptype_name;
                        		
                        		$erp_port_id_to = $flight_availabilities_details_row->erp_port_id_to;
                        		$end_ports_name =$flight_availabilities_details_row->end_ports_name;
                        
                        		$ports = $ports.' <br/> '.$flight_availabilities_details_row->start_ports_name.' - '.$flight_availabilities_details_row->end_ports_name;
                        		$end_datetime = $flight_availabilities_details_row->flight_date .' - '.$flight_availabilities_details_row->arrival_time;
                        	}
                        	$loop_counter++;
                        	
                        	if($flight_availabilities_details_row->safa_externaltriptype_id==1) {
                            ?>
                            
                            
                            
                            <tr>
                            
                            <!--<td><?php echo $flight_availabilities_details_row->safa_externaltriptype_name; ?>
                             <input type='hidden' name='safa_externaltriptype_name_<?= $value->erp_flight_availability_id ?>'  id='safa_externaltriptype_name_<?= $value->erp_flight_availability_id ?>'  value='<?= $flight_availabilities_details_row->safa_externaltriptype_name ?>'/>
                            </td>
                            
                            --><td><?php echo $flight_availabilities_details_row->erp_path_type_name; ?>
                             <input type='hidden' name='safa_externaltriptype_name_<?= $value->erp_flight_availability_id ?>'  id='safa_externaltriptype_name_<?= $value->erp_flight_availability_id ?>'  value='<?= $flight_availabilities_details_row->erp_path_type_name ?>'/>
                            </td>
                            
                            <td><?php echo $flight_availabilities_details_row->airline ;?></td>
                            <td><?php echo $flight_availabilities_details_row->flight_number ;?></td>
                            <td><?php echo $flight_availabilities_details_row->erp_flight_classes_name ;?></td>
                            
                            <td>
                             
                            <!--<input type='hidden' name='departure_date_<?= $value->erp_flight_availability_id ?>'  id='departure_date_<?= $value->erp_flight_availability_id ?>'  value='<?= $start_datetime ?>'/>
                             <?php echo $start_datetime; ?>
                            -->
                            <input type='hidden' name='departure_date_<?= $value->erp_flight_availability_id ?>'  id='departure_date_<?= $value->erp_flight_availability_id ?>'  value='<?= $flight_availabilities_details_row->flight_date .' - '.$flight_availabilities_details_row->departure_time ?>'/>
                             <?php echo $flight_availabilities_details_row->flight_date .' - '.$flight_availabilities_details_row->departure_time; ?>
                            </td>
                            
                            <td>
                             
                            <!--<input type='hidden' name='departure_date_<?= $value->erp_flight_availability_id ?>'  id='departure_date_<?= $value->erp_flight_availability_id ?>'  value='<?= $start_datetime ?>'/>
                             <?php echo $start_datetime; ?>
                            -->
                            <input type='hidden' name='departure_date_<?= $value->erp_flight_availability_id ?>'  id='departure_date_<?= $value->erp_flight_availability_id ?>'  value='<?= $flight_availabilities_details_row->flight_date .' - '.$flight_availabilities_details_row->departure_time ?>'/>
                             <?php echo $flight_availabilities_details_row->flight_date .' - '.$flight_availabilities_details_row->departure_time; ?>
                            </td>
                            
                            
                            <td>
                            <input type='hidden' name='erp_port_id_from_<?= $value->erp_flight_availability_id ?>'  id='erp_port_id_from_<?= $value->erp_flight_availability_id ?>'  value='<?= $flight_availabilities_details_row->erp_port_id_from ?>'/>
                            <input type='hidden' name='erp_port_id_to_<?= $value->erp_flight_availability_id ?>'  id='erp_port_id_to_<?= $value->erp_flight_availability_id ?>'  value='<?= $flight_availabilities_details_row->erp_port_id_to ?>'/>
                            
                            <input type='hidden' name='start_ports_name_<?= $value->erp_flight_availability_id ?>'  id='start_ports_name_<?= $value->erp_flight_availability_id ?>'  value='<?= $flight_availabilities_details_row->start_ports_name; ?>'/>
                            <input type='hidden' name='end_ports_name_<?= $value->erp_flight_availability_id ?>'  id='end_ports_name_<?= $value->erp_flight_availability_id ?>'  value='<?= $flight_availabilities_details_row->end_ports_name; ?>'/>
                            
                            
                            <?= $flight_availabilities_details_row->start_ports_name ?> - <?= $flight_availabilities_details_row->end_ports_name ?>
                            <input type='hidden' name='ports_name_<?= $value->erp_flight_availability_id ?>'  id='ports_name_<?= $value->erp_flight_availability_id ?>'  value='<?= $flight_availabilities_details_row->start_ports_name ?> - <?= $flight_availabilities_details_row->end_ports_name ?>'/>
                            
                             
                             <!--<?php echo $ports; ?>
                             <input type='hidden' name='ports_name_<?= $value->erp_flight_availability_id ?>'  id='ports_name_<?= $value->erp_flight_availability_id ?>'  value='<?= $ports ?>'/>-->
                            
                            </td>
                            
                            <td><?php echo $flight_availabilities_details_row->erp_flight_status_name ;?></td>
                            
                            
                             <td>
                            <input type='hidden' name='available_seats_count_<?= $value->erp_flight_availability_id ?>'  id='available_seats_count_<?= $value->erp_flight_availability_id ?>'  value='<?= $flight_availabilities_details_row->available_seats_count ?>'/>
                            <input type='text' style="width:50px" name='available_seats_count_text_<?= $value->erp_flight_availability_id ?>'  id='available_seats_count_text_<?= $value->erp_flight_availability_id ?>'  value='<?= $flight_availabilities_details_row->available_seats_count ?>' readonly="readonly" disabled="disabled"/>
                            
							<!-- <?= $value->available_seats_count ?> -->
                            
                            </td>
                            
                            
                            <!--<td>
                            <?php echo $flight_availabilities_details_row->departure_time; ?>
                            </td>
                            <td>
                            <?php echo $flight_availabilities_details_row->arrival_time; ?>
                            </td>
                            
                            
                            
                            --></tr>
                            
                            
                            <?php 
                        		}
                            }
                        	?>
                            
                       
                             
                        <? } ?>
                    <? } ?>
	
	     </tbody>
                            
                            </table>
                            
                            </td>
<!--                            <td><a href='javascript:void(0)' class='' id='hrf_remove_trip_going'-->
<!--								name='<?= $trip_going_conuter ?>'><span class="icon-trash"></span></a>-->
<!--							</td>-->
              
                            </tr>
	
	</tbody>
</table>

<!-- By Gouda --> <input type="hidden" name="hdn_trip_going_conuter"
	id="hdn_trip_going_conuter" value="<?php echo $trip_going_conuter;?>">

</div>




<div class="row-fluid">
<table >
	<thead>
		<tr>
			<!--  <th ><?= lang('externalsegmenttype') ?></th> -->
			<!--  <th ><?= lang('trip_code') ?></th> -->
			<!--<th><?= lang('flight') ?></th>
			<th><?= lang('flight_from_to') ?></th>
			<th><?= lang('airport_departure_datetime') ?></th>
			<th ><?= lang('airport_start') ?></th>
           	<th ><?= lang('airport_end') ?></th>
			<th><?= lang('airport_arrival_datetime') ?></th>
			<th><?= lang('transporter') ?></th>
			<th><?= lang('seats_count') ?></th>

			-->
			<th width="90%"><?= lang('return_trip') ?></th>
			
			
		</tr>
	</thead>

	<? $trip_return_conuter = 0; ?>

	<tbody>
	
	<tr id="row_<?php echo $this->flight_availabilities_model->erp_flight_availability_id; ?>">
                           
                            
                            <td colspan="2">
                            <input type='hidden'
							name='trip_return_erp_flight_availability_id<?= $trip_return_conuter ?>'
							id='trip_return_erp_flight_availability_id<?= $trip_return_conuter ?>'
							value='<?= $this->flight_availabilities_model->erp_flight_availability_id ?>'
							class='validate[required]' /> 
				
                            <table id='div_trip_return_group'>
                            <thead>
		                    <tr>
		                       	<th><?= lang('erp_path_type_id') ?></th>
		                       	<th><?= lang('flight_availabilities_airlines') ?></th>
						        <th><?= lang('flight_availabilities_flight_number') ?></th>
						        <th><?= lang('flight_availabilities_class') ?></th>
						        <th><?= lang('flight_availabilities_date') ?></th>
						        <th><?= lang('arrival_date') ?></th>
						        <th><?= lang('flight_availabilities_airports') ?></th>
						        <th><?= lang('flight_availabilities_status') ?></th>
						        <th><?= lang('flight_availabilities_seat_count') ?></th>
						        <!--<th><?= lang('flight_availabilities_department_time') ?></th>
						        <th><?= lang('flight_availabilities_arrival_time') ?></th>-->
<!--						        <th><a href="javascript:new_row(2);" class="btn" > <?php echo lang('global_add') ?> </a></th>-->
						        
		                    </tr>
			                </thead>
			                <tbody id="return_flight_tbody">
	
			<? if (isset($going_trip_flights_rows)) { ?>
			<? foreach ($going_trip_flights_rows as $value) {
                        $this->flight_availabilities_model->erp_flight_availability_id = $value->erp_flight_availability_id;
                        
                        //$this->flight_availabilities_model->erp_path_type_id = $flight_trip_type;
                        
                        $flight_availabilities_details_rows = $this->flight_availabilities_model->get_details_for_trip();
                        
                        $ports='';
                        
                        $safa_externaltriptype_name = '';
                        
                        $start_datetime='';
                        $end_datetime='';
                        $loop_counter=0;
                        
                        $erp_port_id_from = 0;
                        $erp_port_id_to =0;
                        $start_ports_name ='';
                        $end_ports_name ='';
                        
                        
                        
                       	?>
                            
			                
                            <?php 
                            
                            foreach($flight_availabilities_details_rows as $flight_availabilities_details_row) {
                        	if($loop_counter==0) {
                        		$safa_externaltriptype_name = $flight_availabilities_details_row->safa_externaltriptype_name;
                        		$erp_path_type_name = $flight_availabilities_details_row->erp_path_type_name;
                        		
                        		$erp_port_id_from = $flight_availabilities_details_row->erp_port_id_from;
                        		$start_ports_name =$flight_availabilities_details_row->start_ports_name;
                        
                        		$erp_port_id_to = $flight_availabilities_details_row->erp_port_id_to;
                        		$end_ports_name =$flight_availabilities_details_row->end_ports_name;
                        
                        		$ports = $flight_availabilities_details_row->start_ports_name.' - '.$flight_availabilities_details_row->end_ports_name;
                        		$start_datetime = $flight_availabilities_details_row->flight_date .' - '.$flight_availabilities_details_row->departure_time;
                        		$end_datetime = $flight_availabilities_details_row->flight_date .' - '.$flight_availabilities_details_row->arrival_time;
                        	} else {
                        		$safa_externaltriptype_name = $safa_externaltriptype_name.' <br/> '.$flight_availabilities_details_row->safa_externaltriptype_name;
                        		
                        		$erp_port_id_to = $flight_availabilities_details_row->erp_port_id_to;
                        		$end_ports_name =$flight_availabilities_details_row->end_ports_name;
                        
                        		$ports = $ports.' <br/> '.$flight_availabilities_details_row->start_ports_name.' - '.$flight_availabilities_details_row->end_ports_name;
                        		$end_datetime = $flight_availabilities_details_row->flight_date .' - '.$flight_availabilities_details_row->arrival_time;
                        	}
                        	$loop_counter++;
                        	
                        	if($flight_availabilities_details_row->safa_externaltriptype_id==2) {
                            ?>
                            
                            
                            
                           <tr>
                            
                            <!--<td><?php echo $flight_availabilities_details_row->safa_externaltriptype_name; ?>
                             <input type='hidden' name='safa_externaltriptype_name_<?= $value->erp_flight_availability_id ?>'  id='safa_externaltriptype_name_<?= $value->erp_flight_availability_id ?>'  value='<?= $flight_availabilities_details_row->safa_externaltriptype_name ?>'/>
                            </td>
                            
                            --><td>
                            <input type="hidden" name="trip_going_erp_flight_availability_id<?= $value->erp_flight_availability_id ?>" id="trip_going_erp_flight_availability_id<?= $value->erp_flight_availability_id ?>"  value="<?= $value->erp_flight_availability_id ?>" />
                            
                            <?php echo $flight_availabilities_details_row->erp_path_type_name; ?>
                             <input type='hidden' name='safa_externaltriptype_name_<?= $value->erp_flight_availability_id ?>'  id='safa_externaltriptype_name_<?= $value->erp_flight_availability_id ?>'  value='<?= $flight_availabilities_details_row->erp_path_type_name ?>'/>
                            </td>
                            
                            <td><?php echo $flight_availabilities_details_row->airline ;?></td>
                            <td><?php echo $flight_availabilities_details_row->flight_number ;?></td>
                            <td><?php echo $flight_availabilities_details_row->erp_flight_classes_name ;?></td>
                            
                            <td>
                             
                            <!--<input type='hidden' name='departure_date_<?= $value->erp_flight_availability_id ?>'  id='departure_date_<?= $value->erp_flight_availability_id ?>'  value='<?= $start_datetime ?>'/>
                             <?php echo $start_datetime; ?>
                            -->
                            <input type='hidden' name='departure_date_<?= $value->erp_flight_availability_id ?>'  id='departure_date_<?= $value->erp_flight_availability_id ?>'  value='<?= $flight_availabilities_details_row->flight_date .' - '.$flight_availabilities_details_row->departure_time ?>'/>
                             <?php echo $flight_availabilities_details_row->flight_date .' - '.$flight_availabilities_details_row->departure_time; ?>
                            </td>
                            
                            <td>
                             
                            <!--<input type='hidden' name='departure_date_<?= $value->erp_flight_availability_id ?>'  id='departure_date_<?= $value->erp_flight_availability_id ?>'  value='<?= $start_datetime ?>'/>
                             <?php echo $start_datetime; ?>
                            -->
                            <input type='hidden' name='departure_date_<?= $value->erp_flight_availability_id ?>'  id='departure_date_<?= $value->erp_flight_availability_id ?>'  value='<?= $flight_availabilities_details_row->flight_date .' - '.$flight_availabilities_details_row->departure_time ?>'/>
                             <?php echo $flight_availabilities_details_row->flight_date .' - '.$flight_availabilities_details_row->departure_time; ?>
                            </td>
                            
                            
                            <td>
                            <input type='hidden' name='erp_port_id_from_<?= $value->erp_flight_availability_id ?>'  id='erp_port_id_from_<?= $value->erp_flight_availability_id ?>'  value='<?= $flight_availabilities_details_row->erp_port_id_from ?>'/>
                            <input type='hidden' name='erp_port_id_to_<?= $value->erp_flight_availability_id ?>'  id='erp_port_id_to_<?= $value->erp_flight_availability_id ?>'  value='<?= $flight_availabilities_details_row->erp_port_id_to ?>'/>
                            
                            <input type='hidden' name='start_ports_name_<?= $value->erp_flight_availability_id ?>'  id='start_ports_name_<?= $value->erp_flight_availability_id ?>'  value='<?= $flight_availabilities_details_row->start_ports_name; ?>'/>
                            <input type='hidden' name='end_ports_name_<?= $value->erp_flight_availability_id ?>'  id='end_ports_name_<?= $value->erp_flight_availability_id ?>'  value='<?= $flight_availabilities_details_row->end_ports_name; ?>'/>
                            
                            
                            <?= $flight_availabilities_details_row->start_ports_name ?> - <?= $flight_availabilities_details_row->end_ports_name ?>
                            <input type='hidden' name='ports_name_<?= $value->erp_flight_availability_id ?>'  id='ports_name_<?= $value->erp_flight_availability_id ?>'  value='<?= $flight_availabilities_details_row->start_ports_name ?> - <?= $flight_availabilities_details_row->end_ports_name ?>'/>
                            
                             
                             <!--<?php echo $ports; ?>
                             <input type='hidden' name='ports_name_<?= $value->erp_flight_availability_id ?>'  id='ports_name_<?= $value->erp_flight_availability_id ?>'  value='<?= $ports ?>'/>-->
                            
                            </td>
                            
                            <td><?php echo $flight_availabilities_details_row->erp_flight_status_name ;?></td>
                            
                            
                             <td>
                            <input type='hidden' name='available_seats_count_<?= $value->erp_flight_availability_id ?>'  id='available_seats_count_<?= $value->erp_flight_availability_id ?>'  value='<?= $flight_availabilities_details_row->available_seats_count ?>'/>
                            <input type='text' style="width:50px" name='available_seats_count_text_<?= $value->erp_flight_availability_id ?>'  id='available_seats_count_text_<?= $value->erp_flight_availability_id ?>'  value='<?= $flight_availabilities_details_row->available_seats_count ?>' readonly="readonly" disabled="disabled"/>
                            
							<!-- <?= $value->available_seats_count ?> -->
                            
                            </td>
                            
                            
                            <!--<td>
                            <?php echo $flight_availabilities_details_row->departure_time; ?>
                            </td>
                            <td>
                            <?php echo $flight_availabilities_details_row->arrival_time; ?>
                            </td>
                            
                            
                            
                            --></tr>
                            
                            
                            <?php 
                        		}
                            }
                            
                            ?>
                            
                            
                             
                        <? } ?>
                    <? } ?>
	
	</tbody>
                            
                            </table>
                            
                            </td>
<!--                            <td><a href='javascript:void(0)' class='' id='hrf_remove_trip_return'-->
<!--								name='<?= $trip_return_conuter ?>'><span class="icon-trash"></span></a>-->
<!--							</td>-->
              
                            </tr>
	
	</tbody>
</table>

<!-- By Gouda --> <input type="hidden" name="hdn_trip_return_conuter"
	id="hdn_trip_return_conuter" value="<?php echo $trip_return_conuter;?>">

</div>


</div>


<!-- Commented By Gouda --> 

<!--<div class="resalt-group">
     	<div class="wizerd-div"><a><?php echo lang('return_trip') ?></a></div>
           	<div class="row-fluid">



                            <div>                       
                                <table   id='div_trip_return_group'>
                                    <thead>
                                        <tr>
											  <th ><?php echo lang('externalsegmenttype') ?></th>
                                              <th ><?php echo lang('trip_code') ?></th> 
                                            <th ><?php echo lang('airport_departure_datetime') ?></th>
                                            <th ><?php echo lang('airport_start') ?></th>
                                            <th ><?php echo lang('airport_end') ?></th>
                                            <th ><?php echo lang('airport_arrival_datetime') ?></th>
                                            <th ><?php echo lang('transporter') ?></th>
                                             <th ><?= lang('seats_count') ?></th>
                                            <th class="TAC">  
                                            
                                            <a class="btn fancybox fancybox.iframe" href="<?php echo  site_url('flight_availabilities/popup_trip/2/'.$safa_trip_id) ?>"><?php echo lang('flight_availabilities') ?> </a>
                        					
                                            </th>
                                        </tr>
									</thead>
									
                   <?php $trip_return_conuter = 0; ?>
									
					<tbody>
                    	<? if (isset($return_trip_flights_rows)) { ?>
                        <? foreach ($return_trip_flights_rows as $value) { 
                        $this->flight_availabilities_model->erp_flight_availability_id = $value->erp_flight_availability_id;
                        $this->flight_availabilities_model->erp_path_type_id = 2;
                        
                        $flight_availabilities_details_rows = $this->flight_availabilities_model->get_details_for_trip();
                        
                        $ports='';
                        $start_datetime='';
                        $end_datetime='';
                        $loop_counter=0;
                        
                        $erp_port_id_from = 0;
                        $erp_port_id_to =0;
                        $start_ports_name ='';
                        $end_ports_name ='';
                        
                        $trip_return_conuter = $trip_return_conuter + 1;
                        
                        foreach($flight_availabilities_details_rows as $flight_availabilities_details_row) {
                        	if($loop_counter==0) {
                        		
                        		$erp_port_id_from = $flight_availabilities_details_row->erp_port_id_from;
                        		$start_ports_name =$flight_availabilities_details_row->start_ports_name;
                        
                        		$erp_port_id_to = $flight_availabilities_details_row->erp_port_id_to;
                        		$end_ports_name =$flight_availabilities_details_row->end_ports_name;
                        
                        		$ports = $flight_availabilities_details_row->start_ports_name.' - '.$flight_availabilities_details_row->end_ports_name;
                        		$start_datetime = $flight_availabilities_details_row->flight_date .' - '.$flight_availabilities_details_row->departure_time;
                        		$end_datetime = $flight_availabilities_details_row->flight_date .' - '.$flight_availabilities_details_row->arrival_time;
                        	} else {
                        		$erp_port_id_to = $flight_availabilities_details_row->erp_port_id_to;
                        		$end_ports_name =$flight_availabilities_details_row->end_ports_name;
                        
                        		$ports = $ports.' <br/> '.$flight_availabilities_details_row->start_ports_name.' - '.$flight_availabilities_details_row->end_ports_name;
                        		$end_datetime = $flight_availabilities_details_row->flight_date .' - '.$flight_availabilities_details_row->arrival_time;
                        	}
                        	$loop_counter++;
                        }
                       	?>
                            <tr id="row_<?php echo $trip_return_conuter; ?>">
                            <td>
                            <input type='hidden' name='trip_return_erp_flight_availability_id<?= $trip_return_conuter ?>'  id='trip_return_erp_flight_availability_id<?= $trip_return_conuter ?>'  value='<?= $trip_return_conuter ?>' class='validate[required]'/>
                                                    
                            
                        	<input type='hidden' name='departure_date_<?= $trip_return_conuter ?>'  id='departure_date_<?= $trip_return_conuter ?>'  value='<?= $start_datetime ?>'/>
                             <?php echo $start_datetime; ?>
                             
                            </td>
                            
                            
                             
                            <td>
                            
                             
                            <input type='hidden' name='erp_port_id_from_<?= $trip_return_conuter ?>'  id='erp_port_id_from_<?= $trip_return_conuter ?>'  value='<?= $erp_port_id_from ?>'/>
                            <input type='hidden' name='erp_port_id_to_<?= $trip_return_conuter ?>'  id='erp_port_id_to_<?= $trip_return_conuter ?>'  value='<?= $erp_port_id_to ?>'/>
                            
                            <input type='hidden' name='start_ports_name_<?= $trip_return_conuter ?>'  id='start_ports_name_<?= $trip_return_conuter ?>'  value='<?= $start_ports_name ?>'/>
                            <input type='hidden' name='end_ports_name_<?= $trip_return_conuter ?>'  id='end_ports_name_<?= $trip_return_conuter ?>'  value='<?= $end_ports_name ?>'/>
                            
                            
                            <?= $value->start_ports_name ?> - <?= $value->end_ports_name ?>
                            
                             <?php echo $ports; ?>
                             <input type='hidden' name='ports_name_<?= $trip_return_conuter ?>'  id='ports_name_<?= $trip_return_conuter ?>'  value='<?= $ports ?>'/>
                             
                             
                             <?php echo $start_ports_name ?>
                            </td>
                            
                            
                             <td>
                             <?php echo $end_ports_name ?>
                            
                            </td>
                            
                            <td>
                            
                            <input type='hidden' name='arrival_date_<?= $trip_return_conuter ?>'  id='arrival_date_<?= $trip_return_conuter ?>'  value='<?= $end_datetime ?>'/>
                            <?php echo $end_datetime; ?>
                             
                            </td>
                            
                            <td>
                            <input type='hidden' name='trip_safa_transporter_id_<?= $trip_return_conuter ?>'  id='trip_safa_transporter_id_<?= $trip_return_conuter ?>'  value='<?= $value->safa_transporter_id ?>'/>
                        	<input type='hidden' name='trip_safa_transporter_name_<?= $trip_return_conuter ?>'  id='trip_safa_transporter_name_<?= $trip_return_conuter ?>'  value='<?= $value->safa_transporter_name ?>'/>
                        	<?php echo $value->safa_transporter_name ?> 
                            </td>
                            
                            <td>
                            <input type='hidden' name='available_seats_count_<?= $trip_return_conuter ?>'  id='available_seats_count_<?= $trip_return_conuter ?>'  value='<?= $value->available_seats_count ?>'/>
							  <input type='text' name='available_seats_count_text_<?= $trip_return_conuter ?>'  id='available_seats_count_text_<?= $trip_return_conuter ?>'  value='<?= $value->available_seats_count ?>' readonly="readonly" disabled="disabled"/>
                            
							<?= $value->available_seats_count ?> 
                            </td>
                            
                            <td>
                            <a href='javascript:void(0)' class='' id='hrf_remove_trip_return' name='<?= $trip_return_conuter ?>'><span class="icon-trash"></span></a>
                            </td>
                            
                            </tr>
                        <? } ?>
                    <? } ?>
                </tbody>	
                                        
            </table>
                                    
             By Gouda 
             <input type="hidden" name="hdn_trip_return_conuter" id="hdn_trip_return_conuter" value="<?php echo $trip_return_conuter;?>">          
             </div>
		</div>
 	</div>
 	
 	
 	
 	--></div>

</div>
</div>


</div>




</div>




<div class="widget">
<div class="widget-header">

<div class="widget-header-icon Fright"><span class="icos-pencil2"></span>
</div>

<div class="widget-header-title Fright"><?php echo lang('uo_requests') ?>
</div>
</div>

<div class="widget-container">



<div id="" class="content-holder" style="padding-top: 50px;">

<div class="span12">
<div class="row-fluid">



<table id='div_safa_trips_request_group'>
	<thead>
		<tr>
			<th><?= lang('safa_uo_service_id') ?></th>
			<th><?= lang('remarks') ?></th>
			<th class="TAC"><a href='javascript:void(0)'
				id='hrf_add_safa_trips_requests' class='btn'><?= lang('add') ?></a>

			</th>
		</tr>
	</thead>
	<tbody id="div_safa_trips_request_group_body">
	<? $safa_trips_requests_conuter = 0; ?>

	<?php
	foreach ($safa_trips_requests_rows as $safa_trips_requests_row) {
		$safa_trips_requests_id = $safa_trips_requests_row->safa_trips_requests_id;
		$safa_uo_service_id = $safa_trips_requests_row->safa_uo_service_id;
		$remarks = $safa_trips_requests_row->remarks;

		$safa_trips_requests_conuter = $safa_trips_requests_conuter + 1;


		?>


		<tr>
			<td><input type='hidden'
				name='safa_trips_requests_safa_trips_requests_id<?= $safa_trips_requests_conuter ?>'
				id='safa_trips_requests_safa_trips_requests_id<?= $safa_trips_requests_conuter ?>'
				value='<?= $safa_trips_requests_id ?>' /> <select
				name='safa_trips_requests_safa_uo_service_id<?= $safa_trips_requests_conuter ?>'
				id='safa_trips_requests_safa_uo_service_id<?= $safa_trips_requests_conuter ?>'
				class='safa_trips_requests_safa_uo_service_id validate[required] input-huge' rel='<?= $safa_trips_requests_conuter ?>'>
				<?
				foreach ($safa_uo_services as $key => $value) {
					$selected = '';
					if ($safa_uo_service_id == $key) {
						$selected = "selected='selected'";
					}
					echo "<option value='$key' $selected >$value</option>";
				}
				?>
			</select></td>
			<td><input type='text'
				name='safa_trips_requests_remarks<?= $safa_trips_requests_conuter ?>'
				id='safa_trips_requests_remarks<?= $safa_trips_requests_conuter ?>'
				class='input-huge' value='<?= $remarks ?>' /></td>

			<td><a href='javascript:void(0)' id='hrf_remove_safa_trips_requests'
				name='<?= $safa_trips_requests_conuter ?>'><span class="icon-trash"></span></a>
			</td>


		</tr>

		<?
	}
	?>

	</tbody>

</table>

<!-- By Gouda --> <input type="hidden"
	name="hdn_safa_trips_requests_conuter"
	id="hdn_safa_trips_requests_conuter"
	value="<?php echo $safa_trips_requests_conuter;?>"></div>
</div>
</div>



</div>




</div>





<div class="widget TAC"><input type="submit" class="btn" name="smt_save"
	value="<?php echo lang('global_submit') ?>"
	style="margin: 10px; padding: 5px; height: auto"></div>


	<?php echo  form_close() ?>


<div class="footer"></div>

<script type="text/javascript">
//Safa Trips Requests
$(document).ready(function() {
    var counter = <?php echo $safa_trips_requests_count + 1; ?>;
    
    $("#hrf_add_safa_trips_requests").click(function() {

    	 var counter = $('#div_safa_trips_request_group_body').children().length+1;
    	 
        if (counter > 100) {
            alert("Only 100 safa_trips_request allow");
            return false;
        }

        var new_div_safa_trips_request = $(document.createElement('tr'))
                .attr({"id": 'div_safa_trips_request' + counter, 'style': 'width: 900px'});

        new_div_safa_trips_request.html('<td>' +
                '<input type="hidden" name="safa_trips_requests_safa_trips_requests_id' + counter + '"  id="safa_trips_requests_safa_trips_requests_id' + counter + '"  value="0" />' +
                '<select name="safa_trips_requests_safa_uo_service_id' + counter + '" id="safa_trips_requests_safa_uo_service_id' + counter + '"   class="safa_trips_requests_safa_uo_service_id input-huge validate[required]">' +
				<? foreach ($safa_uo_services as $key => $value): ?>
				                '<option value="<?= $key ?>"><?= $value ?></option>' +
				<? endforeach ?>
        '</select>' +
                '</td>' +


                '<td >' +
                '<input type="text" name="safa_trips_requests_remarks' + counter + '"  id="safa_trips_requests_remarks' + counter + '"   value="" class="input-huge" rel="' + counter + '" />' +
                '</td>' +

                '<td >' +
                '<a href="javascript:void(0)" class="" id="hrf_remove_safa_trips_requests" name="' + counter + '"><span class="icon-trash"></span></a>' +
                '</td>'
                );
        
        new_div_safa_trips_request.appendTo("#div_safa_trips_request_group");      
        counter++;

    });
    $("#hrf_remove_safa_trips_requests").live('click', function() {
        if (!confirm('<?= lang('global_are_you_sure_you_want_to_delete') ?>'))
            return false
        if (counter == 1) {
            //alert("No more safa_trips_request to remove");
            //return false;
        }

        counter--;
        var hrf_id = $(this).attr('name');

        // By Gouda.
        //$("#div_safa_trips_request" + hrf_id).remove();
        $(this).parent().parent().remove();
    });
//$('.chzn-select').select2();
});

</script>

<script type="text/javascript">
$(document).ready(function() {

	var counter = <?php echo $going_trip_flights_rows_count + 1; ?>;
	$("#hrf_remove_trip_going").live('click', function() {
        if (!confirm('<?= lang('global_are_you_sure_you_want_to_delete') ?>'))
            return false

        counter--;
        var hrf_id = $(this).attr('name');

		//By Gouda
        //$("#div_trip_going" + hrf_id).remove();
        $(this).parent().parent().remove();

    });

	var counter = <?php echo $return_trip_flights_rows_count + 1; ?>;
	$("#hrf_remove_trip_return").live('click', function() {
    if (!confirm('<?= lang('global_are_you_sure_you_want_to_delete') ?>'))
        return false

    counter--;
    var hrf_id = $(this).attr('name');

	//By Gouda
    //$("#div_trip_return" + hrf_id).remove();
    $(this).parent().parent().remove();
    
	});

});
</script>


<script type="text/javascript">
$(document).ready(function() {
    var config = {
        '.chosen-select': {width: "100%"},
        '.chosen-select-deselect': {allow_single_deselect: true},
        '.chosen-select-no-single': {disable_search_threshold: 10},
        '.chosen-select-no-results': {no_results_text: 'Oops, nothing found!'},
        '.chosen-select-width': {width: "95%"}
    }
    for (var selector in config) {
        $(selector).chosen(config[selector]);
    }


    // binds form submission and fields to the validation engine
    $("#frm_trips").validationEngine({
            prettySelect : true,
            useSuffix: "_chosen",
            promptPosition : "topRight:-150"
            //promptPosition : "bottomLeft"
    });


    $('.date').datepicker({dateFormat: "yy-mm-dd"});
    
});

</script>


<script>
document.getElementById('erp_transportertype_id').onchange = function() 
{
    if (this.value == '2') {
        document.getElementById('dv_air_trip').style.display = "inline-block";
    } else {
        document.getElementById('dv_air_trip').style.display = "none";
    }
}
</script>

<script type="text/javascript">
$(".safa_trips_requests_safa_uo_service_id").live("change", function(e) {

	var current_safa_trips_requests_safa_uo_service_id = $(this).val();
	var current_select_input_id = $(this).attr('id');
	
	$(".safa_trips_requests_safa_uo_service_id").each(function() {

		if($(this).attr('id')!= current_select_input_id) {
			if(current_safa_trips_requests_safa_uo_service_id ==$(this).val()) {	
		     	alert('<?= lang('service_request_was_inserted_before') ?>');
		     	$('#'+current_select_input_id).val('');
			}
		}
		
	});
	
});

</script>



<script>
var c1 = 0;
var c2 = 100;

function new_row(safa_externaltriptype_id) {
		if(safa_externaltriptype_id==1) {
	        var id = c1;
		    var new_row = function() {/*
		    <tr class="table-row" rel="{$id}">
		    <td>
		    <input type="hidden" name="safa_externaltriptype_id[{$id}]" id="safa_externaltriptype_id[{$id}]"  value="1" />
            
			<?= form_dropdown('erp_path_type_id[{$id}]', $erp_path_types, set_value("erp_path_type_id[{\$id}]"), 'class="validate[required]" style="width:60px" id="erp_path_type_id[{$id}]"') ?></td>
	        <td>
	        <?= form_dropdown('erp_airline_id[{$id}]', $erp_airlines, set_value("erp_airline_id[{\$id}]"), 'class="validate[required]" style="width:60px" id="erp_airline_id[{$id}]"') ?></td>
	        </td>
	        <td><?= form_input('flight_number[{$id}]', set_value("flight_number[{\$id}]"), 'class="validate[required]" style="width:40px" id="flightno[{$id}]"') ?></td>
	        <td><?= form_dropdown('erp_flight_class_id[{$id}]', $erp_flight_classes, set_value("erp_flight_class_id[{\$id}]"), 'class="validate[required]" style="width:50px" id="class[{$id}]"') ?></td>
	        <td><?= form_input('flight_date[{$id}]', set_value("flight_date[{\$id}]"), 'class="validate[required] datetimepicker" style="width:100px" id="datefields[{$id}]"') ?></td>
	        <td><?= form_input('flight_arrival_date[{$id}]', set_value("flight_arrival_date[{\$id}]"), 'class="validate[required] datetimepicker" style="width:100px" id="flight_arrival_date[{$id}]"') ?></td>
	        
	        <td style="width:180px"><?= form_dropdown('erp_port_id_from[{$id}]', $erp_ports, set_value("erp_port_id_from[{\$id}]"), 'class="validate[required]" style="width:60px" id="erp_port_id_from[{$id}]"') ?>
	        <?= form_dropdown('erp_port_id_to[{$id}]', $erp_sa_ports, set_value("erp_port_id_to[{\$id}]"), 'class="validate[required]" style="width:60px" id="erp_port_id_to[{$id}]"') ?></td>
	        
	        <td><?= form_dropdown('erp_flight_status_id[{$id}]', $erp_flight_status, set_value("erp_flight_status_id[{\$id}]"), 'class="validate[required]" style="width:60px" id="status[{$id}]"') ?></td>
	        <td><?= form_input('seats_count[{$id}]', set_value("seats_count[{\$id}]"), 'class="validate[required]" style="width:50px" id="seatcont[{$id}]"') ?></td>
	        <td><a href="javascript:void(0);" onclick="remove_row('{$id}')"><span class="icon-trash"></span> </a></td>
		    </tr>
		    */}.toString().replace(/{\$id}/g, id).slice(14, -3);
	    
	        $('tbody#going_flight_tbody').append(new_row);
	        $('.datetimepicker').datetimepicker({dateFormat: 'yy-mm-dd',timeFormat: 'HH:mm'});
	        c1++;
		} else if(safa_externaltriptype_id==2) {
	        var id = c2;
		    var new_row = function() {/*
		    <tr class="table-row" rel="{$id}">
		    <td>
		    <input type="hidden" name="safa_externaltriptype_id[{$id}]" id="safa_externaltriptype_id[{$id}]"  value="2" />
            
	        <?= form_dropdown('erp_path_type_id[{$id}]', $erp_path_types, set_value("erp_path_type_id[{\$id}]"), 'class="validate[required]" style="width:60px" id="erp_path_type_id[{$id}]"') ?></td>
	        <td>
	        <?= form_dropdown('erp_airline_id[{$id}]', $erp_airlines, set_value("erp_airline_id[{\$id}]"), 'class="validate[required]" style="width:60px" id="erp_airline_id[{$id}]"') ?></td>
	        </td>
	        <td><?= form_input('flight_number[{$id}]', set_value("flight_number[{\$id}]"), 'class="validate[required]" style="width:40px" id="flightno[{$id}]"') ?></td>
	        <td><?= form_dropdown('erp_flight_class_id[{$id}]', $erp_flight_classes, set_value("erp_flight_class_id[{\$id}]"), 'class="validate[required]" style="width:50px" id="class[{$id}]"') ?></td>
	        <td><?= form_input('flight_date[{$id}]', set_value("flight_date[{\$id}]"), 'class="validate[required] datetimepicker" style="width:100px" id="datefields[{$id}]"') ?></td>
	        <td><?= form_input('flight_arrival_date[{$id}]', set_value("flight_arrival_date[{\$id}]"), 'class="validate[required] datetimepicker" style="width:100px" id="flight_arrival_date[{$id}]"') ?></td>
	        
	        <td style="width:180px"><?= form_dropdown('erp_port_id_from[{$id}]', $erp_sa_ports, set_value("erp_port_id_from[{\$id}]"), 'class="validate[required]" style="width:60px" id="erp_port_id_from[{$id}]"') ?>
	        <?= form_dropdown('erp_port_id_to[{$id}]', $erp_ports, set_value("erp_port_id_to[{\$id}]"), 'class="validate[required]" style="width:60px" id="erp_port_id_to[{$id}]"') ?></td>
	        
	        <td><?= form_dropdown('erp_flight_status_id[{$id}]', $erp_flight_status, set_value("erp_flight_status_id[{\$id}]"), 'class="validate[required]" style="width:60px" id="status[{$id}]"') ?></td>
	        <td><?= form_input('seats_count[{$id}]', set_value("seats_count[{\$id}]"), 'class="validate[required]" style="width:50px" id="seatcont[{$id}]"') ?></td>
	        <td><a href="javascript:void(0);" onclick="remove_row('{$id}')"><span class="icon-trash"></span> </a></td>

		    </tr>
		    */}.toString().replace(/{\$id}/g, id).slice(14, -3);
	    
	        $('tbody#return_flight_tbody').append(new_row);
	        $('.datetimepicker').datetimepicker({dateFormat: 'yy-mm-dd',timeFormat: 'HH:mm'});
	        c2++;
		}
    }
	    

	
    function remove_row(id)
    {
        $('tr[rel='+id+']').remove();
    
    }
    
</script>



<script>
$(document).ready(function() {
    $('.fancybox').fancybox({
        afterClose: function() {
            //location.reload();
        }
    });
});
</script>


<script>
function popup_trip()
{
	var arrival_date = $('#arrival_date').val();
	
	$.fancybox({
	   'type': 'iframe', 
	   'width' : 850,
	   'height' : 600,
	   'autoDimensions' : false,
	   'autoScale' : false,
	   'href' : "<?php echo  site_url('flight_availabilities/popup_trip/1/'.$safa_trip_id) ?>/"+arrival_date,
	 });
			
}
</script>