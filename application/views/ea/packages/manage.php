<?php
$safa_uo_package_id = '';
$package_code = '';
$date_type = '';
$start_date = '';
$end_date = '';
$name_ar = '';
$name_la = '';
$sale_currency_id = '';
$transport_type_id = '';
$remarks = '';
$active = '';

$screen_title = lang('add_title');

if (isset($item)) {
    $package_code = $item->package_code;
    $date_type = $item->date_type;
    $start_date = $item->start_date;
    $end_date = $item->end_date;
    $name_ar = $item->name_ar;
    $name_la = $item->name_la;
    $sale_currency_id = $item->sale_currency_id;
    $transport_type_id = $item->transport_type_id;
    $remarks = $item->remarks;

    if ($item->active) {
        $active = "checked='checked'";
    }

    $screen_title = lang('edit_title');
}
?>


<style>
    .wizerd-div {
        border-bottom: medium none !important;
        margin: -18px 0 20px;
        padding-top: 0;
    }
    .wizerd-div a {
        background: none repeat scroll 0 0 #FAFAFA;
        border: 1px solid #D5D6D6;
        border-radius: 49px;
        color: #663300;
        display: inline-block;
        margin: -6px 5px -17px -8px;
        padding: 10px 12px 8px;
    }
    a {
        color: #C09853;
    }
    .resalt-group {
        margin: 18px 0.5% 0.5%;
        padding: 0.5%;
        width: 99%;
    }
    th a.btn, th input[type="button"], th input[type="submit"], th button {
        margin: 0;
        padding: 4px 12px;
    }
    .coll_close, .coll_open {
        margin-top: 0;
    }
    .chosen-container {
        margin-top: 4px;
    }

    /*By Gouda, TO solve problem of dropdown in table*/
    .table-warp {
        overflow-x: visible;
        overflow-y: visible;
    }

</style>


<!-- For Dates -->
<link href="<?= base_url("static/js/jquery.calendars/humanity.calendars.picker.css") ?>" type="text/css" rel="stylesheet">
<script src="<?= base_url("static/js/jquery.calendars/jquery.calendars.js") ?>"></script>
<script src="<?= base_url("static/js/jquery.calendars/jquery.calendars-ar.js") ?>"></script>
<script src="<?= base_url("static/js/jquery.calendars/jquery.calendars.plus.js") ?>"></script>
<script src="<?= base_url("static/js/jquery.calendars/jquery.calendars.picker.js") ?>"></script>
<script src="<?= base_url("static/js/jquery.calendars/jquery.calendars.picker-ar.js") ?>"></script>
<script src="<?= base_url("static/js/jquery.calendars/jquery.calendars.islamic.js") ?>"></script>
<script src="<?= base_url("static/js/jquery.calendars/jquery.calendars.islamic-ar.js") ?>"></script>
<script src="<?= NEW_JS ?>/plugins/jquery.fixedheadertable.js"></script>


<div class="widget">
    <div class="path-container Fright">
        <div class="path-name Fright">
            <a href=""><?= lang('global_system_management') ?></a>
        </div>
        <div class="path-arrow Fright" ></div>
        <div class="path-name Fright"> <a href="<?php echo site_url('uo/packages') ?>"><?php echo lang('title') ?></a></div>

        <div class="path-arrow Fright"></div>
        <div class="path-name Fright"><?php echo $screen_title; ?></div>
    </div>
</div>


<?php echo form_open(false, 'id="frm_packages_manage" ') ?>

<div class="widget">
    <div class="widget-header">
        <div class="widget-header-icon Fright"></div>
        <div class="widget-header-title">
<?php echo $screen_title; ?> </div>
    </div>
    <div class="widget-container packages">
        <div class="resalt-group">
            <div class="wizerd-div"><a><?php echo lang('main_data'); ?></a></div>
            <div class="table-warp">
                <table class="">
                    <thead>
                        <tr>
                            <th><?php echo lang('package_code'); ?></th>
                            <th><?php echo lang('package_name'); ?></th>
                            <th><?php echo lang('currency_transport'); ?></th>
                            <th colspan="2"><?php echo lang('period'); ?></th>


                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="span2"> <font style='color:red' >*</font>
<?php echo form_input('package_code', set_value("package_code", $package_code), " style='' id='package_code' class='validate[required] input-full' ") ?>
                            </td>
                            <td>
                                <div class="span12">
                                    <div class="span12"><label><?php echo lang('name_ar'); ?></label> <font style='color:red' >*</font></div>
                                    <span class="span12">
<?php echo form_input('name_ar', set_value("name_ar", $name_ar), " style='' id='name_ar' class='validate[required] input-full' ") ?>
                                    </span>
                                </div>
                                <div class="span12">
                                    <div class="span12"><label><?php echo lang('name_la'); ?></label> <font style='color:red' >*</font></div>
                                    <div class="span12">
<?php echo form_input('name_la', set_value("name_la", $name_la), " style='' id='name_la' class='validate[required] input-full' ") ?>
                                    </div>
                                </div>
                            </td>
                            <td>

                                <div class="span12">
                                    <div class="span12"><label><?php echo lang('currency'); ?></label></div>
                                    <span class="span12">
<?php echo form_dropdown('sale_currency_id', $erp_currencies, set_value('sale_currency_id', $sale_currency_id), 'class="chosen-select chosen-rtl input-full"  tabindex="4" id="sale_currency_id" ') ?>   
                                    </span>
                                </div>
                                <div class="span12">
                                    <div class="span12"><label><?php echo lang('transport_type_id'); ?></label></div>
                                    <div class="span12">
<?php echo form_dropdown('transport_type_id', $erp_transportertypes, set_value('transport_type_id', $transport_type_id), 'class="chosen-select chosen-rtl input-full"  tabindex="4" id="transport_type_id" ') ?>

                                    </div>
                                </div>


                            </td>
                            <td>
                                <div class="span12"><label class="span3"><?php echo lang('date_type'); ?></label>

<?php
$islamic_checked = "";
$gregorian_checked = "";
if ($date_type == 'islamic') {
    $islamic_checked = "checked='checked'";

    $start_date = $this->hijrigregorianconvert->GregorianToHijri($start_date, 'YYYY-MM-DD');
    $end_date = $this->hijrigregorianconvert->GregorianToHijri($end_date, 'YYYY-MM-DD');
} else {
    $gregorian_checked = "checked='checked'";
}
?>
                                    <label class="span3" ><?php echo lang('islamic'); ?><input onclick="$('.date').calendarsPicker('destroy').val('').calendarsPicker({calendar: $.calendars.instance('islamic', 'ar'), dateFormat: 'yyyy-mm-dd'});" class="pull-right" style="height: 25px; margin-top: 0px;" name="date_type" type="radio" value="islamic" <?php echo $islamic_checked; ?> /></label>
                                    <label class="span3" ><?php echo lang('gregorian'); ?><input onclick="$('.date').calendarsPicker('destroy').val('').calendarsPicker({calendar: $.calendars.instance('gregorian', 'en'), dateFormat: 'yyyy-mm-dd'});" class="pull-right" name="date_type" type="radio" value="gregorian" <?php echo $gregorian_checked; ?>/></label>
                                </div>

                                <div class="span3"><label><?php echo lang('start_date'); ?></label></div>
                                <div class="span8">
<?php echo form_input('start_date', set_value("start_date", $start_date), " style='' id='start_date' class='input-full date' ") ?>

                                </div>
                                <div class="span3"><label><?php echo lang('end_date'); ?></label></div>
                                <div class="span8">
<?php echo form_input('end_date', set_value("end_date", $end_date), " style='' id='end_date' class='input-full date' ") ?>

                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>

            </div>
            <div class="span3"><label><input type="checkbox" name="active" id="active" <?php echo $active; ?>></input><?php echo lang('active'); ?></label><a class="btn"><?php echo lang('available_for'); ?></a><a class="btn"><?php echo lang('not_available_for'); ?></a> </div>

        </div>



<?php $this->load->view('uo/packages/hotels'); ?>

<?php $this->load->view('uo/packages/hotels_prices'); ?>

        <?php $this->load->view('uo/packages/execlusive_nights_prices'); ?>

        <?php $this->load->view('uo/packages/tourism_places'); ?>

        <?php $this->load->view('uo/packages/execlusive_meals_prices'); ?>



        <div class="span11" >
            <div class="span2" style="margin-top: 55px;"><?php echo lang('notes_and_conditions'); ?></div>
            <div class="span10">

<?php echo form_textarea('remarks', set_value("remarks", $remarks), " id='remarks' class='input-huge' style='width: 100%;' ") ?>

            </div>
        </div>
        <div class="span12 TAC">

            <input type="submit" class="btn" name="smt_save" value="<?php echo lang('global_submit') ?>" >


        </div>

    </div>

</div>

<?php echo form_close() ?>


<script type="text/javascript">
    $(document).ready(function() {
        var config = {
            '.chosen-select': {width: "100%"},
            '.chosen-select-deselect': {allow_single_deselect: true},
            '.chosen-select-no-single': {disable_search_threshold: 10},
            '.chosen-select-no-results': {no_results_text: 'Oops, nothing found!'},
            '.chosen-select-width': {width: "95%"}
        }
        for (var selector in config) {
            $(selector).chosen(config[selector]);
        }


        // binds form submission and fields to the validation engine
        $("#frm_packages_manage").validationEngine({
            prettySelect: true,
            useSuffix: "_chosen",
            promptPosition: "topRight:-150"
                    //promptPosition : "bottomLeft"
        });

    });
</script>

<script type="text/javascript">
    isObject = function(a) {
        return (!!a) && (a.constructor === Object);
    };
    $(function() {
        $('.date').calendarsPicker({calendar: $.calendars.instance('gregorian', 'en'), onSelect: customRange, dateFormat: 'yyyy-mm-dd'});
        function customRange(dates) {
            if (this.id == 'datefrom') {
                $('input[name=end_date]').calendarsPicker('option', 'minDate', dates[0] || null);
            } else {
                $('input[name=start_date]').calendarsPicker('option', 'maxDate', dates[0] || null);
            }
        }

        $('.packages').on('change', '.hotels_id', function() {
            $('.chosen_hotels option').remove();
            $('.chosen_hotels').append('<option value=""></option>');
            $('.hotels_id').each(function() {
                $('.chosen_hotels').append('<option value="'+$(this).val() + '">' + $(this).find('option[value=' + $(this).val() + ']').text()+'</option>');
            });
            $('.chosen_hotels').trigger("chosen:updated");
        });
    });
</script>