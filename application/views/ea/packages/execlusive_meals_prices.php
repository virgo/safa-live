<script>execlusive_meals_prices_counter = 0</script>

<div class="resalt-group">
            <div class="wizerd-div"><a><?php echo  lang('execlusive_meals_prices'); ?></a></div>
            <div class="table-warp">
                <table>
                    <thead>
                        <tr>
                        	<th class="span5"><?php echo  lang('hotel'); ?></th>
                            <th class="span5"><?php echo  lang('meal'); ?></th>
                            <th class="span5"><?php echo  lang('meal_price'); ?></th>
                            <th class="span1">
                            <a class="btn Fleft" title="<?php echo  lang('global_add'); ?>" href="javascript:void(0)" onclick="add_execlusive_meals_prices('N' + execlusive_meals_prices_counter++); load_multiselect()">
					        <?php echo  lang('global_add') ?>    
					        </a>
                            </th>

                    </thead>
                    <tbody class='execlusive_meals_prices' id='execlusive_meals_prices'>
                    <? if(isset($item_execlusive_meals_prices)) { ?>
                    <? if(check_array($item_execlusive_meals_prices)) { ?>
                    <? foreach($item_execlusive_meals_prices as $item_execlusive_meals_price) { ?>
                    <tr rel="<?php echo  $item_execlusive_meals_price->safa_package_execlusive_meal_id ?>">
                        
                       <td>
                        <?php 
                         echo  form_dropdown('execlusive_meals_prices_erp_hotel_id['.$item_execlusive_meals_price->safa_package_execlusive_meal_id.']'
                               , $erp_hotels, set_value('execlusive_meals_prices_erp_hotel_id['.$item_execlusive_meals_price->safa_package_execlusive_meal_id.']', $item_execlusive_meals_price->hotel_id)
                               , 'class="chosen-select chosen-rtl input-full validate[required] chosen_hotels" id="execlusive_meals_prices_erp_hotel_id['.$item_execlusive_meals_price->safa_package_execlusive_meal_id.']" tabindex="4" '); 
                        ?>
                        </td>
                        
                        <td>
                        <?php 
                         echo  form_dropdown('execlusive_meals_prices_erp_meal_id['.$item_execlusive_meals_price->safa_package_execlusive_meal_id.']'
                               , $erp_meals, set_value('execlusive_meals_prices_erp_meal_id['.$item_execlusive_meals_price->safa_package_execlusive_meal_id.']', $item_execlusive_meals_price->erp_meal_id)
                               , 'class="chosen-select chosen-rtl input-full validate[required]" id="execlusive_meals_prices_erp_meal_id['.$item_execlusive_meals_price->safa_package_execlusive_meal_id.']" tabindex="4" '); 
                        ?>
                        </td>
                        
                        <td><?php echo  form_input('execlusive_meals_prices_price['.$item_execlusive_meals_price->safa_package_execlusive_meal_id.']'
                                , set_value('execlusive_meals_prices_price['.$item_execlusive_meals_price->safa_package_execlusive_meal_id.']', $item_execlusive_meals_price->price)
                                , 'class="validate[required] input-huge" ') ?>
                        </td>
                        
                        <td class="TAC">
                            <a href="javascript:void(0)" onclick="delete_execlusive_meals_prices(<?php echo  $item_execlusive_meals_price->safa_package_execlusive_meal_id ?>, true)"><span class="icon-trash"></span></a>
                        </td>
                    </tr>
                    <? } ?>
                    <? } ?>
                    <? } ?>
                    </tbody>
                </table>

            </div></div>
 
<script>
function add_execlusive_meals_prices(id) {

	
	var text = [];
	$('#dv_hotels :input').each(function(){
		var input_id = $(this).attr("id");
		//alert(input_name);
		if(input_id !== undefined) {
		    if(input_id.startsWith('hotels_erp_hotel_id')){

			    var input_value = $("#"+input_id+" option:selected").val();
			    var input_text = $("#"+input_id+" option:selected").text();

			    
		        text.push(new Array(input_value,input_text));
		    }
		}
	})
	//text = text.join(' ');
	//alert(text);
	
	var slct_options_erp_hotels = "<option value=''></option>";
	for(var i=0; i<text.length ; i++) {
		slct_options_erp_hotels = slct_options_erp_hotels + "<option value='"+text[i][0]+"'>"+text[i][1]+"</option>";
	}
	//alert(slct_options_erp_hotels);
	
    var new_row = [
            "<tr rel=\"" + id + "\">",
           
            "   <td>",
            "   <select name=\"execlusive_meals_prices_erp_hotel_id[" + id + "]\" class=\"chosen-select chosen-rtl input-full validate[required] \" id=\"execlusive_meals_prices_erp_hotel_id_" + id + "\" tabindex=\"4\">",
            slct_options_erp_hotels,
            "   </select>",
            "   </td>",

            "    <td>",
            "       <select name=\"execlusive_meals_prices_erp_meal_id[" + id + "]\" class=\"chosen-select chosen-rtl input-full validate[required] \" id=\"execlusive_meals_prices_erp_meal_id_" + id + "\" tabindex=\"4\">",
            <? foreach($erp_meals as $key => $value): ?>
            "           <option value=\"<?= $key ?>\"><?= $value ?></option>",
            <? endforeach ?>
            "   </select>",
            
            "    </td>",

            '     <td><?php echo  form_input("execlusive_meals_prices_price[' + id + ']", FALSE, 'class="validate[required] input-huge" ') ?>',
            "    </td>",
            
            
            "    <td class=\"TAC\">",
            "        <a href=\"javascript:void(0)\" onclick=\"delete_execlusive_meals_prices('" + id + "')\"><span class=\"icon-trash\"></span></a>",
            "    </td>",
            "</tr>"
            ].join("\n");

    
        
    $('#execlusive_meals_prices').append(new_row);
    
}
function delete_execlusive_meals_prices(id, database) 
{
    if(typeof database == 'undefined')
    {
        $('.execlusive_meals_prices').find('tr[rel="' + id + '"]').remove();
    }
    else
    {
        $('.execlusive_meals_prices').find('tr[rel=' + id + ']').remove();
        var hidden_input = '<input type="hidden" name="execlusive_meals_prices_remove[]" value="' + id + '" />';
        $('#execlusive_meals_prices').append(hidden_input);
    }
}

</script>

                   