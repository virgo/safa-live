<style>
    .wizerd-div {
    border-bottom: medium none !important;
    margin: -18px 0 20px;
    padding-top: 0;
}
.width90 {width:90% !important}
.wizerd-div a {
    background: none repeat scroll 0 0 #FAFAFA;
    border: 1px solid #D5D6D6;
    border-radius: 49px;
    color: #663300;
    display: inline-block;
    margin: -6px 5px -17px -8px;
    padding: 10px 12px 8px;
}
a {
    color: #C09853;
}
.resalt-group {
    margin: 18px 0.5% 0.5%;
    padding: 0.5%;
    width: 99%;
}
th a.btn, th input[type="button"], th input[type="submit"], th button {
    margin: 0;
    padding: 4px 12px;
}
.coll_close, .coll_open {
    margin-top: 0;
}
.chosen-container {
    margin-top: 4px;
}
.warning {
    color: #C09853;
}
</style>
<? $hgconvert = new Hijrigregorianconvert(); ?>

<div class="widget">
    <div class="path-container Fright">
        <div class="path-name Fright">
            <a href="<?= site_url('uo/dashboard') ?>"><?= lang('global_system_management') ?></a>
        </div>
        <div class="path-arrow Fright"> </div>
        <div class="path-name Fright"><?= lang('menu_packages') ?></div>
    </div>
</div>

<div class="widget">
    <div class="widget-header">
        <div class="widget-header-icon Fright"></div>
        <div class="widget-header-title Fright"><?= lang('menu_packages') ?></div>
    </div>
    
    <div class="widget-container">
        <div class="resalt-group">
            <?= form_open('ea/packages') ?>
            <div class="wizerd-div TAC "><a><?= lang('search_subject') ?></a></div>
            <div class="row-form"><input type="hidden" name="dosearch" value="all" />
                <div class="span4" style="padding-bottom: 61px;">
                    <div class="span2"><label><?= lang('contract') ?></label></div>
                    <div class="span8"><?= form_dropdown('contract_id', $erp_contracts,false,'class="chosen width90 " ') ?></div>
                </div>
                <div class="span4">
                    <div class="span12">
                        <div class="span6"><?= lang('mekka_hotel') ?></div>
                        <div class="span6"><?= form_dropdown('mekka_hotel_id', $erp_mekka_hotels,false,'class="chosen width90" ') ?></div>
                    </div>
                    <div class="span12">
                        <div class="span6"><?= lang('hotel_level') ?></div>
                        <div class="span6"><?= form_dropdown('mekka_hotel_level', $erp_hotellevels,false,'class="chosen width90" ') ?></div>
                    </div>
                    <div class="span12">
                        <div class="span6"><?= lang('haram_space') ?></div>
                        <div class="span6"><input type="text" class="input-huge width90"></div>
                    </div>
                </div>
                <div class="span4">
                    <div class="span12">
                        <div class="span6"><?= lang('madina_hotel') ?></div>
                        <div class="span6"><?= form_dropdown('madina_hotel_id', $erp_madina_hotels,false,'class="chosen width90" ') ?></div>
                    </div>
                    <div class="span12">
                        <div class="span6"><?= lang('hotel_level') ?></div>
                        <div class="span6"><?= form_dropdown('madina_hotel_level', $erp_hotellevels,false,'class="chosen width90" ') ?></div>
                    </div>
                    <div class="span12">
                        <div class="span6"><?= lang('haram_space') ?></div>
                        <div class="span6"><input type="text" class="input-huge"></div>
                    </div>
                </div>
            </div>
         
            <div class="row-form">
                <div class="span4">
                    <div class="span5"><?= lang('transport_type') ?></div>
                    <div class="span7"><?= form_dropdown('transportation_type_id', $erp_transportertypes) ?></div>
                </div>
                <div class="span4">
                    <div class="span2"><?= lang('period') ?></div>
                    <div class="span10">
                        <div class="span6">
                            <div class="span2"><?= lang('start_date') ?></div>
                            <div class="span9"><input type="text" name="fromdate" class="input-huge datepicker"></div>
                                
                        </div>
                        <div class="span6">
                            <div class="span2"><?= lang('end_date') ?></div>
                            <div class="span9"><input type="text" name="todate" class="input-huge datepicker"></div>
                                
                        </div>
                    </div>
                </div>
                <div class="span4">
                    <div class="span4"><?= lang('nationalities') ?></div>
                    <div class="span8"><input name="nationalites" type="text" class="input-huge"></div>
                </div>
            </div>
            <div class="row-fluid TAC"><input type="submit" class="btn" value="<?= lang('global_search') ?>"></div>
            <?= form_close() ?>
        </div>
        <? if(isset($items) && count($items)) :?>
        <div class="resalt-group">
            <div class="wizerd-div TAC"><a><?= lang('search_result') ?></a></div>
            <div class="table-warp">
                <table class="fsTable">
                    <thead>
                        <tr>
                            <th><?= lang('package_code') ?></th>
                            <th><?= lang('package_name') ?></th>
                            <th class="span4" colspan="2"><?= lang('package_date') ?></th>
                            <th><?= lang('mekka_hotel') ?></th>
                            <th><?= lang('madina_hotel') ?></th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <? foreach ($items as $item) :?>
                        <tr>
                            <td><label><a href="<?= site_url('ea/packages/view/'.$item->safa_package_id) ?>"><?= $item->package_code ?></a></label></td>
                            <td><label><a href="<?= site_url('ea/packages/view/'.$item->safa_package_id) ?>"><?= $item->{name()} ?></a></label></td>
                            <td>
                                <div class="span3">هـ <?= lang('start_date') ?></div>
                                <div class="span9"><?= $hgconvert->GregorianToHijri($item->start_date, 'YYYY-MM-DD'); ?></div>
                                <div class="span3">مـ <?= lang('start_date') ?></div>
                                <div class="span9"><?= $item->start_date ?></div>
                            </td>
                            <td>
                                <div class="span3"><?= lang('end_date') ?></div>
                                <div class="span9"><?= $hgconvert->GregorianToHijri($item->end_date, 'YYYY-MM-DD'); ?></div>
                                <div class="span3"><?= lang('end_date') ?></div>
                                <div class="span9"><?= $item->end_date ?></div>
                            </td>
                            <td><label><?= get_package_hotel_city($item->safa_package_id,1) ?></label></td>
                            <td><label><?= get_package_hotel_city($item->safa_package_id,2) ?></label></td>
                            <td><a href="<?= site_url('hotels_reservation_orders/addfrompkg/'.$item->safa_package_id) ?>" rel="<?= $item->safa_package_id ?>" target="_new" class="btn package_fancy"><?= lang('package_reserve') ?></a></td>
                        </tr>
                        <? endforeach ?>
                    </tbody>
                </table>
            </div>
        </div>
        <? else :?>
        <? if(isset($_POST['dosearch'])) :?><?= lang('no_result') ?> <? endif ?>
        <? endif ?>
    </div>
</div>
