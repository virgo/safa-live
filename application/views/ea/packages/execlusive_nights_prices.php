<script>execlusive_nights_prices_counter = 0</script>
   
        <div class="resalt-group">
            <div class="wizerd-div"><a><?php echo  lang('execlusive_nights_prices'); ?></a></div>
            <div class="table-warp">
                <table>
                    <thead>
                        <tr>
                            <!-- <th  class="span2"><?php echo  lang('package_period'); ?></th> -->
                            <th  class="span2"><?php echo  lang('hotel'); ?></th>
                            <th  class="span2"><?php echo  lang('meal'); ?></th>
                            
                            <?php
                            
                            if(isset($erp_hotelroomsizes)) { 
		                    if(check_array($erp_hotelroomsizes)) { 
		                     
		                    foreach($erp_hotelroomsizes as $erp_hotelroomsize) { 
		                    	echo '<th>'.$erp_hotelroomsize->{name()}.'</th>';
		                    }
		                    }
                            }
		                    ?>
		                    
                            <th>
							<a class="btn Fleft" title="<?php echo  lang('global_add'); ?>" href="javascript:void(0)" onclick="add_execlusive_nights_prices('N' + execlusive_nights_prices_counter++); load_multiselect()">
					        <?php echo  lang('global_add') ?>    
					        </a>
							</th>
                        </tr>
                    </thead>
                    <tbody class='execlusive_nights_prices' id='execlusive_nights_prices'>
                    <? if(isset($item_execlusive_nights_prices)) { ?>
                    <? if(check_array($item_execlusive_nights_prices)) { ?>
                    <? foreach($item_execlusive_nights_prices as $item_night) { ?>
                    <tr rel="<?php echo  $item_night->safa_package_execlusive_night_id ?>">
                        
                        <!--     
						<td>
                        <?php 
                         echo  form_dropdown('execlusive_nights_prices_safa_uo_package_id['.$item_night->safa_package_execlusive_night_id.']'
                               , $package_periods, set_value('execlusive_nights_prices_safa_uo_package_id['.$item_night->safa_package_execlusive_night_id.']', $item_night->safa_uo_package_id)
                               , 'class="chosen-select chosen-rtl input-full" id="execlusive_nights_prices_safa_uo_package_id['.$item_night->safa_package_execlusive_night_id.']"  tabindex="4" '); 
                        ?>
                        </td>
                         -->
                         
                        <td>
                        <?php 
                         echo  form_dropdown('execlusive_nights_prices_erp_hotel_id['.$item_night->safa_package_execlusive_night_id.']'
                               , $erp_hotels, set_value('execlusive_nights_prices_erp_hotel_id['.$item_night->safa_package_execlusive_night_id.']', $item_night->hotel_id)
                               , 'class="chosen-select chosen-rtl input-full chosen_hotels" id="execlusive_nights_prices_erp_hotel_id['.$item_night->safa_package_execlusive_night_id.']"  tabindex="4" '); 
                        ?>
                        </td>
                        
                         <td>
                        <?php 
                         echo  form_dropdown('execlusive_nights_prices_erp_meal_id['.$item_night->safa_package_execlusive_night_id.']'
                               , $erp_meals, set_value('execlusive_nights_prices_erp_meal_id['.$item_night->safa_package_execlusive_night_id.']', $item_night->erp_meal_id)
                               , 'class="chosen-select chosen-rtl input-full" id="execlusive_nights_prices_erp_meal_id['.$item_night->safa_package_execlusive_night_id.']" tabindex="4" '); 
                        ?>
                        </td>
                        
                        <?php
                            
                        if(isset($erp_hotelroomsizes)) { 
		                if(check_array($erp_hotelroomsizes)) { 
		                     
		                foreach($erp_hotelroomsizes as $erp_hotelroomsize) {
		                	
		                $erp_hotelroomsize_id = $erp_hotelroomsize->erp_hotelroomsize_id;	
		                
		                ?>
		                <td><?php echo  form_input('execlusive_nights_prices_price['.$item_night->safa_package_execlusive_night_id.']['.$erp_hotelroomsize_id.']'
                                , set_value('execlusive_nights_prices_price['.$item_night->safa_package_execlusive_night_id.']['.$erp_hotelroomsize_id.']', get_nights_price($item_night->safa_package_execlusive_night_id,$erp_hotelroomsize_id))
                                , 'class=" input-huge" ') ?>
                        </td>
		                <?php 
		                }
		                }
                        }
		                ?>
                        
                        <td class="TAC">
                            <a href="javascript:void(0)" onclick="delete_execlusive_nights_prices(<?php echo  $item_night->safa_package_execlusive_night_id ?>, true)"><span class="icon-trash"></span></a>
                        </td>


                        </tr>
                    <? } ?>
                    <? } ?>
                    <? } ?>
                    
                    </tbody>
                </table>

            </div>
        </div>
        
        
<script>
function add_execlusive_nights_prices(id) {

	
	var text = [];
	$('#dv_hotels :input').each(function(){
		var input_id = $(this).attr("id");
		//alert(input_name);
		if(input_id !== undefined) {
		    if(input_id.startsWith('hotels_erp_hotel_id')){

			    var input_value = $("#"+input_id+" option:selected").val();
			    var input_text = $("#"+input_id+" option:selected").text();

			    
		        text.push(new Array(input_value,input_text));
		    }
		}
	})
	//text = text.join(' ');
	//alert(text);
	
	var slct_options_erp_hotels = "<option value=''></option>";
	for(var i=0; i<text.length ; i++) {
		slct_options_erp_hotels = slct_options_erp_hotels + "<option value='"+text[i][0]+"'>"+text[i][1]+"</option>";
	}
	//alert(slct_options_erp_hotels);
	
    var new_row = [
            "<tr rel=\"" + id + "\">",
            "  <!--  <td>",
            "       <select name=\"execlusive_nights_prices_safa_uo_package_id[" + id + "]\" class=\"chosen-select chosen-rtl input-full validate[required]\" id=\"execlusive_nights_prices_safa_uo_package_id_" + id + "\" tabindex=\"4\">",
            <? foreach($package_periods as $key => $value): ?>
            "           <option value=\"<?= $key ?>\"><?= $value ?></option>",
            <? endforeach ?>
            "    </td> -->",
            
            "   <td>",
            "   <select name=\"execlusive_nights_prices_erp_hotel_id[" + id + "]\" class=\"chosen-select chosen-rtl input-full validate[required] \" id=\"execlusive_nights_prices_erp_hotel_id_" + id + "\"  tabindex=\"4\">",
            slct_options_erp_hotels,
            "   </select>",
            "   </td>",
            

            "    <td>",
            "       <select name=\"execlusive_nights_prices_erp_meal_id[" + id + "]\" class=\"chosen-select chosen-rtl input-full\" id=\"execlusive_nights_prices_erp_meal_id_" + id + "\" tabindex=\"4\">",
            <? foreach($erp_meals as $key => $value): ?>
            "           <option value=\"<?= $key ?>\"><?= $value ?></option>",
            <? endforeach ?>
            "    </td>",



            <?php
                    
            if(isset($erp_hotelroomsizes)) { 
            	if(check_array($erp_hotelroomsizes)) { 
                     
                	foreach($erp_hotelroomsizes as $erp_hotelroomsize) {
                	
	                $erp_hotelroomsize_id = $erp_hotelroomsize->erp_hotelroomsize_id;	
	                
	                ?>
                    '     <td><?php echo  form_input("execlusive_nights_prices_price[' + id + '][".$erp_hotelroomsize_id."]", FALSE, 'class="validate[required] input-huge" ') ?>',
                    "    </td>",
                    
           			<?php 
            		}
                }
            }
           ?>
            
            
            "    <td class=\"TAC\">",
            "        <a href=\"javascript:void(0)\" onclick=\"delete_execlusive_nights_prices('" + id + "')\"><span class=\"icon-trash\"></span></a>",
            "    </td>",
            "</tr>"
            ].join("\n");

    
        
    $('#execlusive_nights_prices').append(new_row);
    
}
function delete_execlusive_nights_prices(id, database) 
{
    if(typeof database == 'undefined')
    {
        $('.execlusive_nights_prices').find('tr[rel="' + id + '"]').remove();
    }
    else
    {
        $('.execlusive_nights_prices').find('tr[rel=' + id + ']').remove();
        var hidden_input = '<input type="hidden" name="execlusive_nights_prices_remove[]" value="' + id + '" />';
        $('#execlusive_nights_prices').append(hidden_input);
    }
}

</script>
