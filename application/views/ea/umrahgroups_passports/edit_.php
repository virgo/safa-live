<style>
    
/*    .row-fluid{border-bottom: 1px solid #fff;padding: 8px 0 0;}*/
    label{margin: 5px!important;}
    
    .wizerd-div{ margin: 25px 0 50px;}
</style>
<script>
$(document).ready(function(){
    
        $("input[type=text]").addClass("input-huge");
        
        $(".").formToWizard();

    });
</script>


<div class="row-fluid">
   
<div class="widget">
    <div class="path-container Fright">
    	<div class="path-name Fright">
            <a href="<?php echo  site_url().'ea/dashboard'; 
            
            ?>"><?php echo  lang('global_system_management') ?></a>
        </div>
        
        
        <div class="path-arrow Fright"></div>
        <div class="path-name Fright"> <a href="<?= site_url('ea/safa_umrahgroups/') ?>"> <?= lang('umrahgroups_passports_title') ?></a></div>
        
        <div class="path-arrow Fright"></div>
        <div class="path-name Fright"> 
        <? if ($item->safa_umrahgroup_id !== FALSE) { ?>
                    
                    <?
                    $name = $this->safa_umrahgroups_passports_model->get_umrahgroup_name($item->safa_umrahgroup_id);
                    ?>
                    <a href="<?= site_url('ea/umrahgroups_passports/passports_index/'.$item->safa_umrahgroup_id) ?>"> <?= lang('umrahgroups_passports_group_mutamers_title') ?> <?= $name->name?></a></div>
                <? } else { ?>
                    <?= lang('umrahgroups_passports_title_2') ?> 
                <? } ?>
        
        
        
        <div class="path-arrow Fright"></div>
        <div class="path-name Fright">  <?= lang('umrahgroups_passports_edit_title') ?></div>
    </div>   
        
</div>



    <div class="widget">
             
        
        <div class="widget-header">
        <div class="widget-header-icon Fright"> <span class="icos-pencil2"></span> </div>
        <div class="widget-header-title Fright">
            <?php echo  lang('umrahgroups_passports_edit_title') ?>
        </div>
    	</div>
                         
        <div class="widget-container slidingDiv" style="">
            <?= form_open() ?>
            
            <fieldset>
                <div class="wizerd-div">
                    <a ><?= lang('client_data') ?></a></div>
                <div class="group1" style="float: none;overflow: hidden;">
                        <legend><?= lang('client_data') ?></legend>                      
            <div class="row-fluid span3">
                <div class="span12">
                    <div class="span6"> <label><?= lang('umrahgroups_passports_safa_umrahgroup_id') ?>:</label> </div>
                    <div class="span6">
                        <?= form_dropdown('safa_umrahgroup_id', ddgen('safa_umrahgroups', array('safa_umrahgroup_id', 'name')), set_value('safa_umrahgroup_id',$item->safa_umrahgroup_id), "  class='chosen input-huge' ") ?>
                        
                        <?= form_error('safa_umrahgroup_id', '<div class="bottom" style="color:red" >', '</div>'); ?>
                    </div>
                </div>
                </div>                           
            <div class="row-fluid span3">
                <div class="span12">
                    <div class="span6"><label> <?= lang('umrahgroups_passports_no') ?>:</label> </div>
                    <div class="span6">
                        <?= form_input('no', set_value("no", $item->no), " ") ?>
                        <?= form_error('no', '<div class="bottom" style="color:red" >', '</div>'); ?>
                    </div>
                </div>
            </div>   
            <div class="row-fluid span3">
                <div class="span12">
                    <div class="span6"><label><?= lang('umrahgroups_passports_title_id') ?>:</label>  </div>
                    <div class="span6">
                        <?= form_dropdown('title_id', ddgen('erp_titles', array('erp_title_id', name())), set_value('title_id',$item->title_id), "  class='chosen input-huge' ") ?>
                        <?= form_error('title_id', '<div class="bottom" style="color:red" >', '</div>'); ?>
                    </div>
                </div>
                
            </div>
                         </div> 
                        
                        <div class="Div-Sep">
                            <a></a>
                        </div>
                        <div class="group1" style="float: none;overflow: hidden;">
            
                <div class="row-fluid span3">
                    <div class="span6"> <label><?= lang('umrahgroups_passports_first_name_ar') ?>:</label> </div>
                    <div class="span6">
                        <?= form_input('first_name_ar', set_value("first_name_ar", $item->first_name_ar), " ") ?>
                        <?= form_error('first_name_ar', '<div class="bottom" style="color:red" >', '</div>'); ?>
                    </div>
                </div>
                <div class="row-fluid span3">
                    <div class="span6"><label><?= lang('umrahgroups_passports_second_name_ar') ?>:</label>  </div>
                    <div class="span6">
                        <?= form_input('second_name_ar', set_value("second_name_ar", $item->second_name_ar), " ") ?>
                        <?= form_error('second_name_ar', '<div class="bottom" style="color:red" >', '</div>'); ?>
                    </div>
                </div>
                <div class="row-fluid span3">
                    <div class="span6"> <label><?= lang('umrahgroups_passports_third_name_ar') ?>:</label> </div>
                    <div class="span6">
                        <?= form_input('third_name_ar', set_value("third_name_ar", $item->third_name_ar), " ") ?>
                        <?= form_error('third_name_ar', '<div class="bottom" style="color:red" >', '</div>'); ?>
                    </div>
                </div>
                <div class="row-fluid span3">
                    <div class="span6"><label><?= lang('umrahgroups_passports_fourth_name_ar') ?>:</label>  </div>
                    <div class="span6">
                        <?= form_input('fourth_name_ar', set_value("fourth_name_ar", $item->fourth_name_ar), " ") ?>
                        <?= form_error('fourth_name_ar', '<div class="bottom" style="color:red" >', '</div>'); ?>
                    </div>
                </div>
                
          
                         </div> 
                        
                        <div class="group1" style="float: none;overflow: hidden;">
                <div class="row-fluid span3">
                    <div class="span6"> <label><?= lang('umrahgroups_passports_first_name_la') ?>:</label> </div>
                    <div class="span6">
                        <?= form_input('first_name_la', set_value("first_name_la", $item->first_name_la), " ") ?>
                        <?= form_error('first_name_la', '<div class="bottom" style="color:red" >', '</div>'); ?>
                    </div>
                </div>
                <div class="row-fluid span3">
                    <div class="span6"> <label><?= lang('umrahgroups_passports_second_name_la') ?>:</label> </div>
                    <div class="span6">
                        <?= form_input('second_name_la', set_value("second_name_la", $item->second_name_la), " ") ?>
                        <?= form_error('second_name_la', '<div class="bottom" style="color:red" >', '</div>'); ?>
                    </div>
                </div>
                <div class="row-fluid span3">
                    <div class="span6"> <label><?= lang('umrahgroups_passports_third_name_la') ?>:</label> </div>
                    <div class="span6">
                        <?= form_input('third_name_la', set_value("third_name_la", $item->third_name_la), " ") ?>
                        <?= form_error('third_name_la', '<div class="bottom" style="color:red" >', '</div>'); ?>
                    </div>
                </div>
                <div class="row-fluid span3">
                    <div class="span6"> <label><?= lang('umrahgroups_passports_fourth_name_la') ?>:</label> </div>
                    <div class="span6">
                        <?= form_input('fourth_name_la', set_value("fourth_name_la", $item->fourth_name_la), " ") ?>
                        <?= form_error('fourth_name_la', '<div class="bottom" style="color:red" >', '</div>'); ?>
                    </div>
                </div>
                
                        </div>        
                        
            </fieldset>
                         
            <fieldset>  
                <div class="wizerd-div">
                    <a ><?= lang('passports_details') ?></a></div>
                        <legend><?= lang('passports_details') ?></legend>
                <div class="group1" style="float: none;overflow: hidden;">
            
                <div class="row-fluid span3">
                    <div class="span6"> <label> <?= lang('umrahgroups_passports_nationality_id') ?>:</label></div>
                    <div class="span6">
                        
                        <?= form_dropdown('nationality_id', ddgen('erp_nationalities', array('erp_nationality_id', name())), set_value('nationality_id',$item->nationality_id), "  class='chosen input-huge' ") ?>
                        <?= form_error('nationality_id', '<div class="bottom" style="color:red" >', '</div>'); ?>
                    </div>
                </div>
                <div class="row-fluid span3">
                    <div class="span6"> <label> <?= lang('umrahgroups_passports_previous_nationality_id') ?>:</label></div>
                    <div class="span6">
                        <?= form_dropdown('previous_nationality_id', ddgen('erp_nationalities', array('erp_nationality_id', name())), set_value('previous_nationality_id',$item->previous_nationality_id), "  class='chosen input-huge' ") ?>
                        
                        <?= form_error('previous_nationality_id', '<div class="bottom" style="color:red" >', '</div>'); ?>
                    </div>
                </div>
                <div class="row-fluid span3">
                    <div class="span6"> <label> <?= lang('umrahgroups_passports_passport_no') ?>:</label></div>
                    <div class="span6">
                        <?= form_input('passport_no', set_value("passport_no", $item->passport_no), " ") ?>
                        <?= form_error('passport_no', '<div class="bottom" style="color:red" >', '</div>'); ?>
                    </div>
                </div>
           
                <div class="row-fluid span3">
                    <div class="span6"> <label> <?= lang('umrahgroups_passports_passport_type_id') ?>:</label></div>
                    <div class="span6">
                        <?= form_dropdown('passport_type_id', ddgen('erp_passporttypes', array('erp_passporttype_id', name())), set_value('passport_type_id',$item->passport_type_id), "  class='chosen input-huge' ") ?>
                        
                        <?= form_error('passport_type_id', '<div class="bottom" style="color:red" >', '</div>'); ?>
                    </div>
                </div>
                
            </div>
                     
             <div class="group1" style="overflow: hidden; float: none;">        
            
                <div class="span3">
                    <div class="span6">  <label><?= lang('umrahgroups_passports_passport_issue_date') ?>:</label></div>
                    <div class="span6">
                        <?= form_input('passport_issue_date', set_value("passport_issue_date", $item->passport_issue_date), " class='passport_issue_date' ") ?>
                        <?= form_error('passport_issue_date', '<div class="bottom" style="color:red" >', '</div>'); ?>
                        <script>
                        $('.passport_issue_date').datepicker({
                            dateFormat: "yy-mm-dd",
                            controlType: 'select',
                            timeFormat: 'HH:mm'
                        });
                    </script>
                    </div>
                </div>
                <div class="span3">
                    <div class="span6"> <label><?= lang('umrahgroups_passports_passport_expiry_date') ?>:</label> </div>
                    <div class="span6">
                        <?= form_input('passport_expiry_date', set_value("passport_expiry_date", $item->passport_expiry_date), " class='passport_expiry_date'") ?>
                        <?= form_error('passport_expiry_date', '<div class="bottom" style="color:red" >', '</div>'); ?>
                        <script>
                        $('.passport_expiry_date').datepicker({
                            dateFormat: "yy-mm-dd",
                            controlType: 'select',
                            timeFormat: 'HH:mm'
                        });
                    </script>
                    </div>
                </div>
                <div class="span3">
                    <div class="span6"> <label><?= lang('umrahgroups_passports_passport_issuing_city') ?>:</label> </div>
                    <div class="span6">
                        <?= form_input('passport_issuing_city',  set_value('passport_issuing_city',$item->passport_issuing_city), "  class=' input-huge' ") ?>
                        
                        <?= form_error('passport_issuing_city', '<div class="bottom" style="color:red" >', '</div>'); ?>
                    </div>
                </div>
                <div class="span3">
                    <div class="span6"> <label> <?= lang('umrahgroups_passports_passport_issuing_country_id') ?>:</label></div>
                    <div class="span6">
                        <?= form_dropdown('passport_issuing_country_id', ddgen('erp_countries', array('erp_country_id', name())), set_value('passport_issuing_country_id',$item->passport_issuing_country_id), "  class='chosen input-huge' ") ?>                       
                        <?= form_error('passport_issuing_country_id', '<div class="bottom" style="color:red" >', '</div>'); ?>
                    </div>
                </div>
             </div>  
            <div class="Div-Sep"><a></a></div>
            <div class="group1" style="overflow: hidden; float: none;">
            
                
                <!-- 
                <div class="span3">
                    <div class="span6"> <label><?= lang('umrahgroups_passports_passport_dpn_count') ?>:</label> </div>
                    <div class="span6">
                        <?= form_input('passport_dpn_count', set_value("passport_dpn_count", $item->passport_dpn_count), " ") ?>
                        <?= form_error('passport_dpn_count', '<div class="bottom" style="color:red" >', '</div>'); ?>
                    </div>
                </div>
                <div class="span3">
                    <div class="span6">  <label><?= lang('umrahgroups_passports_dpn_serial_no') ?>:</label></div>
                    <div class="span6">
                        <?= form_input('dpn_serial_no', set_value("dpn_serial_no", $item->dpn_serial_no), " ") ?>
                        <?= form_error('dpn_serial_no', '<div class="bottom" style="color:red" >', '</div>'); ?>
                    </div>
                </div>
                 -->
                
                <div class="span3">
                    <div class="span6">  <label><?= lang('umrahgroups_passports_relative_no') ?>:</label></div>
                    <div class="span6">
                        <?php 
       
			       $this->countries_model->erp_country_id= $item->passport_issuing_country_id;
                   $country_row=$this->countries_model->get();
                   $passport_fullname='full_name_la';
                   if(count($country_row)>0) {
                      if($country_row->arab==1) {
                           $passport_fullname='full_name_ar';
                       }
                   }
                                    
                   $structure = array('safa_group_passport_id', $passport_fullname);
                                   
			       $key = $structure['0'];
			       if (isset($structure['1'])) {
			       		$value = $structure['1'];
			       }
			       $passports_arr=array();
			       $passports_arr['']=lang('global_select_from_menu');
			       $this->safa_umrahgroups_passports_model->safa_umrahgroup_id=$item->safa_umrahgroup_id;
			       $this->safa_umrahgroups_passports_model->safa_umrahgroups_passport_id=false;
			       //$this->safa_group_passports_model->limit=false;
			       
			       $passports=$this->safa_umrahgroups_passports_model->get();
			       foreach ($passports as $passport) {
			            $passports_arr[$passport->$key] = $passport->$value;
			        }
                    ?>
                    	<?= form_dropdown('relative_no', $passports_arr , set_value('relative_no',$item->relative_no), "  class='chosen input-huge' ") ?>                       
                       
                        <?= form_error('relative_no', '<div class="bottom" style="color:red" >', '</div>'); ?>
                    </div>
                </div>
                
                <div class="span3">
                    <div class="span6">  <label><?= lang('umrahgroups_passports_relative_relation_id') ?>:</label></div>
                    <div class="span6">
                        <?= form_dropdown('relative_relation_id', ddgen('erp_relations', array('erp_relation_id', name()),'' ,'' , true), set_value('relative_relation_id',$item->relative_relation_id), "  class='chosen input-huge' ") ?>                       
                        
                        <?= form_error('relative_relation_id', '<div class="bottom" style="color:red" >', '</div>'); ?>
                    </div>
                </div>
                <div class="span3">
                    <div class="span6"> <label> <?= lang('umrahgroups_passports_educational_level_id') ?>:</label></div>
                    <div class="span6">
                        <?= form_dropdown('educational_level_id', ddgen('erp_educationlevels', array('erp_educationlevel_id', name())), set_value('educational_level_id',$item->educational_level_id), "  class='chosen input-huge' ") ?>                       
                        
                        <?= form_error('educational_level_id', '<div class="bottom" style="color:red" >', '</div>'); ?>
                    </div>
                </div>
            </div>    
            
            
            
            <div class="group1" style="overflow: hidden; float: none;">
                <div class="span3">
                    <div class="span6"> <label><?= lang('umrahgroups_passports_occupation') ?>:</label> </div>
                    <div class="span6">
                        <?= form_input('occupation', set_value("occupation", $item->occupation), " ") ?>
                        <?= form_error('occupation', '<div class="bottom" style="color:red" >', '</div>'); ?>
                    </div>
                </div>
                <div class="span3">
                    <div class="span6">  <label><?= lang('umrahgroups_passports_date_of_birth') ?>:</label></div>
                    <div class="span6">
                        <?= form_input('date_of_birth', set_value("date_of_birth", $item->date_of_birth), " class='date_of_birth' ") ?>
                        <?= form_error('date_of_birth', '<div class="bottom" style="color:red" >', '</div>'); ?>
                        <script>
                        $('.date_of_birth').datepicker({
                            dateFormat: "yy-mm-dd",
                            controlType: 'select',
                            timeFormat: 'HH:mm'
                        });
                    	</script>
                    </div>
                </div>
                <div class="span3">
                    <div class="span6"> <label><?= lang('umrahgroups_passports_age') ?>:</label> </div>
                    <div class="span6">
                    	<?php 
                    	$date_of_birth = strtotime($item->date_of_birth);
						$date_now = strtotime( date('Y-m-d', time()));
						$secs = $date_now - $date_of_birth;// == return sec in difference
						$days = $secs / 86400 ;
						$age=$days/365;
						//$age=round($age);
						//$age=ceil($age);
						$age=floor($age);
                    	?>
                        <?= form_input('age', set_value("age", $age), " disabled='disabled' ") ?>
                        <?= form_error('age', '<div class="bottom" style="color:red" >', '</div>'); ?>
                    </div>
                </div>
           
                
                
            </div>
                        
                        </fieldset>
            <fieldset>  
                <div class="wizerd-div">
                    <a><?= lang('country_details') ?></a></div>
                        <legend><?= lang('country_details') ?></legend>
                        
            <div class="group1" style="overflow: hidden;float: none;">
                <div class="span3">
                    <div class="span6">  <label><?= lang('umrahgroups_passports_birth_city') ?>:</label></div>
                    <div class="span6">
                        <?= form_input('birth_city', set_value("birth_city", $item->birth_city), " ") ?>
                        <?= form_error('birth_city', '<div class="bottom" style="color:red" >', '</div>'); ?>
                    </div>
                </div>
                <div class="span3">
                    <div class="span6"> <label> <?= lang('umrahgroups_passports_birth_country_id') ?>:</label></div>
                    <div class="span6">
                        <?= form_dropdown('birth_country_id', ddgen('erp_countries', array('erp_country_id', name())), set_value('birth_country_id',$item->birth_country_id), "  class='chosen input-huge' ") ?>                       
                        
                        <?= form_error('birth_country_id', '<div class="bottom" style="color:red" >', '</div>'); ?>
                    </div>
                </div>
                
                
            </div>
                        <div class="Div-Sep"><a></a></div>
            <div class="group1" style="overflow: hidden;float: none;">
                
                <!-- 
                <div class="span3">
                    <div class="span6">  <label><?= lang('umrahgroups_passports_relative_gender_id') ?>:</label></div>
                    <div class="span6">
                        <?= form_dropdown('relative_gender_id', ddgen('erp_gender', array('erp_gender_id', name())), set_value('relative_gender_id',$item->relative_gender_id), "  class='chosen input-huge' ") ?>                       
                        
                        <?= form_error('relative_gender_id', '<div class="bottom" style="color:red" >', '</div>'); ?>
                    </div>
                </div>
                 -->
                
                <div class="span3">
                    <div class="span6">  <label><?= lang('umrahgroups_passports_gender_id') ?>:</label></div>
                    <div class="span6">
                        <?= form_dropdown('gender_id', ddgen('erp_gender', array('erp_gender_id', name())), set_value('gender_id',$item->gender_id), "  class='chosen input-huge' ") ?>                       
                        
                        <?= form_error('gender_id', '<div class="bottom" style="color:red" >', '</div>'); ?>
                    </div>
                </div>
                <div class="span3">
                    <div class="span6"> <label> <?= lang('umrahgroups_passports_marital_status_id') ?>:</label></div>
                    <div class="span6">
                        <?= form_dropdown('marital_status_id', ddgen('erp_maritalstatus', array('erp_maritalstatus_id', name())), set_value('marital_status_id',$item->marital_status_id), "  class='chosen input-huge' ") ?>                       
                        
                        <?= form_error('marital_status_id', '<div class="bottom" style="color:red" >', '</div>'); ?>
                    </div>
                </div>
           
           <div class="span3">
                    <div class="span6"> <label><?= lang('umrahgroups_passports_erp_country_id') ?>:</label></div>
                    <div class="span6">
                        <?= form_dropdown('erp_country_id', ddgen('erp_countries', array('erp_country_id', name())), set_value('erp_country_id',$item->erp_country_id), "  class='chosen input-huge' ") ?>                       
                        
                        <?= form_error('erp_country_id', '<div class="bottom" style="color:red" >', '</div>'); ?>
                    </div>
                </div>
            
                <div class="span3">
                    <div class="span6">  <label><?= lang('umrahgroups_passports_city') ?>:</label></div>
                    <div class="span6">
                        <?= form_input('city', set_value("city", $item->city), " ") ?>
                        <?= form_error('city', '<div class="bottom" style="color:red" >', '</div>'); ?>
                    </div>
                </div>
                
            </div>
                       
<div class="Div-Sep"><a></a></div>
            
            <div class="toolbar bottom TAC"><input type="submit" name="submit" value="<?= lang('global_submit') ?>" class="btn btn-primary"> &nbsp;
                <!--<input  type ="button" value="<?= lang('global_back') ?>"class="btn btn-primary" onclick="window.location = '<?= site_url('ea/umrahgroups_passports/index') ?>'">-->
                <input type="button" value="<?=  lang('global_back')?>" class="btn btn-primary" onclick="window.history.back()"/>
            </div>
            <?= form_close() ?> 
         </fieldset>    
        </div>
    </div>
</div>


