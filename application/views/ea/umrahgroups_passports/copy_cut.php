<? //print_r($mutamers);exit(); ?>
<div class="row-fluid">
    
    
    <div class="widget">
    <div class="path-container Fright">
    	<div class="path-name Fright">
            <a href="<?php echo  site_url().'ea/dashboard'; 
            
            ?>"><?php echo  lang('global_system_management') ?></a>
        </div>
        <div class="path-arrow Fright"></div>
        <div class="path-name Fright"><a href="<?= site_url('ea/umrahgroups_passports/index') ?>"> <?= lang('umrahgroups_passports_title') ?></a></div>
        <div class="path-arrow Fright"></div>
        <div class="path-name Fright">  <?if($copy_cut=='cut'):?> <?= lang('cut_to_umrahgroups_passports') ?><?else:?><?= lang('copy_to_umrahgroups_passports') ?><?  endif;?></div>
    </div>        
	</div>
    
    <? if ($copy_cut == 'cut') { ?>
        <div class="widget">
               
            
        <div class="widget-header">
        <div class="widget-header-icon Fright"> <span class="icos-pencil2"></span> </div>
        <div class="widget-header-title Fright">
            <?php echo  lang('cut_to_umrahgroups_passports') ?>
        </div>
    	</div>
                                
            <div class="block-fluid slidingDiv">
                <?= form_open('ea/umrahgroups_passports/copy_cut') ?>
                <div class="row-form">
                    <div class="span6">
                        <div class="span4"><?= lang('umrahgroups_passports_group_name') ?><span style="color: red">*</span>:</div>
                        <div class="span8">
                            <?= form_dropdown('safa_umrahgroup', ddgen('safa_umrahgroups', array('safa_umrahgroup_id', 'name')), set_value('safa_umrahgroup'), " name='s_example' class='select' style='width:100%;' ") ?>
                            <?= form_error('safa_umrahgroup', '<div class="bottom" style="color:red" >', '</div>'); ?>
                        </div>
                    </div>
                </div>
                
                <div class="toolbar bottom TAC">
                    <input type="submit" name="add" value="<?= lang('global_submit') ?>" class="btn btn-primary" />
                    <?if($safa_umrahgroup_id !== FALSE):?>
                        <input  type ="button" value="<?= lang('global_back') ?>"class="btn btn-primary" onclick="window.location = '<?= site_url('ea/umrahgroups_passports/passports_index/'.$safa_umrahgroup_id.'') ?>'">
                    <?else:?>
                        <input  type ="button" value="<?= lang('global_back') ?>"class="btn btn-primary" onclick="window.history.back()">
                    <?  endif;?>                    
                    <input type='hidden' name='mutamers' value='<?= $mutamers; ?>' class='btn btn-primary'>
                    <input type="hidden" name="copy_cut_val" value="<?= $copy_cut; ?>" class="btn btn-primary">
                    <?if($safa_umrahgroup_id !== FALSE):?>
                    <input type="hidden" name="safa_umrahgroup_id" value="<?=$safa_umrahgroup_id?>" class="btn btn-primary">
                    <?  endif;?>
                </div>
                <?= form_close() ?>
            </div>
        </div>
    <? } elseif ($copy_cut == 'copy') { ?>
        <div class="widget">
           
            
            <div class="widget-header">
	        <div class="widget-header-icon Fright"> <span class="icos-pencil2"></span> </div>
	        <div class="widget-header-title Fright">
	            <?php echo  lang('copy_to_umrahgroups_passports') ?>
	        </div>
	    	</div>
                                   
            <div class="block-fluid slidingDiv">
                <?= form_open('ea/umrahgroups_passports/copy_cut') ?>
                <div class="row-form">
                    <div class="span6">
                        <div class="span4"><?= lang('umrahgroups_passports_group_name') ?><span style="color: red">*</span>:</div>
                        <div class="span8">
                            <?= form_dropdown('safa_umrahgroup', ddgen('safa_umrahgroups', array('safa_umrahgroup_id', 'name')), set_value('safa_umrahgroup'), " name='s_example' class='select' style='width:100%;' ") ?>
                            <?= form_error('safa_umrahgroup', '<div class="bottom" style="color:red" >', '</div>'); ?>
                        </div>
                    </div>
                </div>
                <div class="toolbar bottom TAC">
                    <input type="submit" name="add" value="<?= lang('global_submit') ?>" class="btn btn-primary" />
                    <?if($safa_umrahgroup_id !== FALSE):?>
                        <input  type ="button" value="<?= lang('global_back') ?>"class="btn btn-primary" onclick="window.location = '<?= site_url('ea/umrahgroups_passports/passports_index/'.$safa_umrahgroup_id.'') ?>'">
                    <?else:?>
                        <input  type ="button" value="<?= lang('global_back') ?>"class="btn btn-primary" onclick="window.history.back()">
                    <?  endif;?>
                    <input type='hidden' name='mutamers' value='<?= $mutamers; ?>' class='btn btn-primary'>
                    <input type="hidden" name="copy_cut_val" value="<?= $copy_cut ?>" class="btn btn-primary">
                    <?if($safa_umrahgroup_id !== FALSE):?>
                        <input type="hidden" name="safa_umrahgroup_id" value="<?=$safa_umrahgroup_id?>" class="btn btn-primary">
                    <?  endif;?>
                </div>
                <?= form_close() ?>
            </div>
        </div>
    <? } ?>
</div>
