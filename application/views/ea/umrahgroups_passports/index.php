<!-- By Gouda, For popup -->
<script type="text/javascript" src='<?= CSS_JS ?>/form/form.js'></script>
<link rel="stylesheet" href="<?= CSS ?>/bootstrap/bootstrap.min.new.css"/>
     
   

<div class="row-fluid">

<div class="widget">
    <div class="path-container Fright">
    	<div class="path-name Fright">
            <a href="<?php echo  site_url().'ea/dashboard'; 
            
            ?>"><?php echo  lang('global_system_management') ?></a>
        </div>
        <div class="path-arrow Fright"></div>
        <div class="path-name Fright"> <a href="<?= site_url('ea/safa_umrahgroups/index') ?>"> <?= lang('umrahgroups_passports_title') ?></a></div>
     <a title='<?= lang('choose_cols') ?>' href="<?= site_url('ea/umrahgroups_passports/columns') ?>" class="fancybox fancybox.iframe btn Fleft">
           <?= lang('choose_cols') ?>
         </a>
    </div>
    
    
        
</div>

    <div class="">
        
    <!-- By Gouda -->
    <!--  
    <div class="widget">
    --> 
        <style>
            .updated_msg{
                display:none;
                background-color:#ccee97;
                font-weight: bold;
                text-align: center;
                border:3px solid #cccdc9; 
                padding:20px;  
                margin-bottom:10px;
                border-radius:15px;
                margin:10px; 
            }
        </style>
        <div class='row-fluid' align='center' >
            <div  class='updated_msg' >
                <br><input  type ="button" value="<?= lang('global_submit') ?>"class="btn btn-primary" id='ok' >
            </div> 
           
        </div>
        <? if ($safa_umrahgroup_id == FALSE) { ?>
            <div class="widget">
	        <div class="widget-header">
                    
	        <div class="widget-header-icon Fright"> <span class="icos-pencil2"></span> </div>
               
	        <div class="widget-header-title Fright">
	            <?php echo  lang('global_search') ?>
                    
	        </div>
                 
	    	</div>
                 
            
            <div class="block-fluid slidingDiv">
                <?= form_open('ea/umrahgroups_passports/index', 'method="get"') ?>
                <div class="row-form">
                    <div class="span6">
                        <div class="span4"><?= lang('umrahgroups_passports_group_name') ?>:</div>
                        <div class="span8">
                            <?= form_dropdown('safa_umrahgroup_id', ddgen('safa_umrahgroups', array('safa_umrahgroup_id', 'name')), set_value('safa_umrahgroup_id'), " name='s_example' class='select' style='width:100%;' ") ?>
                            <?= form_error('safa_umrahgroup_id'); ?>
                        </div>
                    </div>
                </div>
                <div class="row-form">
                    <div class="span6">
                        <div class="span4"> <?= lang('group_passports_mutamer_name') ?>:</div>
                        <div class="span8">
                            <?= form_input('mutamer_name', set_value("mutamer_name"), "id='mutamer_name'") ?>
                            <?= form_error('mutamer_name', '<div class="bottom" style="color:red" >', '</div>'); ?>
                        </div>

                    </div>
                    <div class="span6" >
                        <div class="span4">
                            <?= lang("group_passports_passport_no") ?>:
                        </div>
                        <div class="span8">
                            <?= form_input('passport_no', set_value("passport_no"), "id='search'") ?>
                            <?= form_error('passport_no', '<div class="bottom" style="color:red" >', '</div>'); ?>
                        </div>
                    </div>
                </div>
                
                
                <div class="row-form">
                    
                    
					<div class="span6">
                    <div class="span4"> <label><?= lang('group_passports_age') ?>:</label> </div>
                    <div class="span8">
                        <?= form_input('age', set_value("age", ''), " ") ?>
                        <?= form_error('age', '<div class="bottom" style="color:red" >', '</div>'); ?>
                    </div>
                </div>
                
                <div class="span6">
                    <div class="span4">  <label><?= lang('group_passports_relative_no') ?>:</label></div>
                    <div class="span8">
                        <?= form_input('relative_no', set_value("relative_no", ''), " ") ?>
                        <?= form_error('relative_no', '<div class="bottom" style="color:red" >', '</div>'); ?>
                    </div>
                </div>


                </div>
                
                
                <div class="row-form">
                    
                    
                    <div class="span6">
                	<div class="span4"> <label> <?= lang('group_passports_nationality_id') ?>:</label></div>
                    <div class="span8">
                        
                        <?= form_dropdown('nationality_id', ddgen('erp_nationalities', array('erp_nationality_id', name())), set_value('nationality_id',''), "  class='select input-huge' ") ?>
                        <?= form_error('nationality_id', '<div class="bottom" style="color:red" >', '</div>'); ?>
                    </div>
               		</div>
               
               
                </div>
                
                <div class="toolbar bottom TAC"><input type="submit" name="search" value="<?= lang('global_search') ?>" class="btn btn-primary" /></div>
                <?= form_close() ?>
            </div>
            
            
            
        <? } ?>
    </div>


    <div class="widget">
        
        
        
        
        <div class="widget-header">
        <div class="widget-header-icon Fright"> <span class="icos-pencil2"></span> </div>
        <div class="widget-header-title Fright">
            <? if ($safa_umrahgroup_id !== FALSE && $safa_umrahgroup_id!=0) { ?>
                    <?= lang('umrahgroups_passports_group_mutamers_title') ?>
                    <?
                    $name = $this->safa_umrahgroups_passports_model->get_umrahgroup_name($safa_umrahgroup_id);
                    echo $name->name;
                    ?>
                <? } else { ?>
                    <?= lang('umrahgroups_passports_title_2') ?> 
                <? } ?>
        </div>
    	</div>
               
        
        <?= form_open("ea/umrahgroups_passports/add_umrah_passports", "id= 'form_mutamers' name='form_mutamers'") ?>
        <div class="block-fluid">
            <table cellpadding="0" class="fsTable" cellspacing="0" width="100%" id="tbl_mutamers">
                <thead>
                    <tr>
                    	<th><input type="checkbox" class="checkall" id="checkall"  onchange='checkUncheck()'/></th>
                        
                        <? if (in_array('display_order', $cols)): ?>
                            <th><?= lang('umrahgroups_passports_display_order') ?></th>
                        <? endif; ?>
                        <? if (in_array('safa_umrahgroup_id', $cols)): ?>
                            <th><?= lang('umrahgroups_passports_safa_umrahgroup_id') ?></th>
                        <? endif; ?>
                        <? if (in_array('no', $cols)): ?>
                            <th><?= lang('umrahgroups_passports_no') ?></th>
                        <? endif; ?>
                        <? if (in_array('passport_no', $cols)): ?>
                            <th><?= lang('umrahgroups_passports_passport_no') ?></th>
                        <? endif; ?>
                        <? if (in_array('passport_type_id', $cols)): ?>
                            <th><?= lang('umrahgroups_passports_passport_type_id') ?></th>
                        <? endif; ?>
                           
                        <? if (in_array('name', $cols)): ?>
                            <th><?= lang('name') ?></th>
                        <? endif; ?>
                        
                        <? if (in_array('name_ar', $cols)): ?>
                            <th><?= lang('umrahgroups_passports_name_ar') ?></th>
                        <? endif; ?>
                        <? if (in_array('name_la', $cols)): ?>
                            <th><?= lang('umrahgroups_passports_name_la') ?></th>
                        <? endif; ?>
                        <? if (in_array('erp_country_id', $cols)): ?>
                            <th><?= lang('umrahgroups_passports_erp_country_id') ?></th>
                        <? endif; ?>
                        <? if (in_array('nationality_id', $cols)): ?>
                            <th><?= lang('umrahgroups_passports_nationality_id') ?></th>
                        <? endif; ?>
                        <? if (in_array('date_of_birth', $cols)): ?>
                            <th><?= lang('umrahgroups_passports_date_of_birth') ?></th>
                        <? endif; ?>
                        <? if (in_array('age', $cols)): ?>
                            <th><?= lang('umrahgroups_passports_age') ?></th>
                        <? endif; ?>
                        <? if (in_array('passport_issue_date', $cols)): ?>
                            <th><?= lang('umrahgroups_passports_passport_issue_date') ?></th>
                        <? endif; ?>
                        <? if (in_array('passport_expiry_date', $cols)): ?>
                            <th><?= lang('umrahgroups_passports_passport_expiry_date') ?></th>
                        <? endif; ?>
                        <? if (in_array('passport_issuing_country_id', $cols)): ?>
                            <th><?= lang('umrahgroups_passports_passport_issuing_country_id') ?></th>
                        <? endif; ?>
                        <? if (in_array('passport_issuing_city', $cols)): ?>
                            <th><?= lang('umrahgroups_passports_passport_issuing_city') ?></th>
                        <? endif; ?>
                        <? if (in_array('passport_dpn_count', $cols)): ?>
                            <th><?= lang('umrahgroups_passports_passport_dpn_count') ?></th>
                        <? endif; ?>
                        <? if (in_array('relative_relation_id', $cols)): ?>
                            <th><?= lang('umrahgroups_passports_relative_relation_id') ?></th>
                        <? endif; ?>
                        <? if (in_array('occupation', $cols)): ?>
                            <th><?= lang('umrahgroups_passports_occupation') ?></th>
                        <? endif; ?>
                         <? if (in_array('city', $cols)): ?>
                            <th><?= lang('umrahgroups_passports_city') ?></th>
                        <? endif; ?>
                        <? if (in_array('marital_status_id', $cols)): ?>
                            <th><?= lang('umrahgroups_passports_marital_status_id') ?></th>
                        <? endif; ?>
                        <? if (in_array('relative_no', $cols)): ?>
                            <th><?= lang('umrahgroups_passports_relative_no') ?></th>
                        <? endif; ?>
                        <? if (in_array('dpn_serial_no', $cols)): ?>
                            <th><?= lang('umrahgroups_passports_dpn_serial_no') ?></th>
                        <? endif; ?>
                        <? if (in_array('educational_level_id', $cols)): ?>
                            <th><?= lang('group_passports_educational_level_id') ?></th>
                        <? endif; ?>
                        
                        
                        
                        <? if (in_array('birth_country_id', $cols)): ?>
                            <th><?= lang('umrahgroups_passports_birth_country_id') ?></th>
                        <? endif; ?>
                        <? if (in_array('birth_city', $cols)): ?>
                            <th><?= lang('umrahgroups_passports_birth_city') ?></th>
                        <? endif; ?>
                        <? if (in_array('gender_id', $cols)): ?>
                            <th><?= lang('umrahgroups_passports_gender_id') ?></th>
                        <? endif; ?>
                        <? if (in_array('relative_gender_id', $cols)): ?>
                            <th><?= lang('umrahgroups_passports_relative_gender_id') ?></th>
                        <? endif; ?>
                        
                        
                        <th><?= lang('global_actions') ?></th>
                    </tr>
                </thead>
                <tbody class="sortable">
                    <? if (isset($items)): ?>
                        <? foreach ($items as $item): ?>
                            <tr id="row_<?php echo $item->safa_umrahgroups_passport_id; ?>" umrahgroup_id="<?= $item->safa_umrahgroup_id; ?>">
		                        <td><input type="checkbox" name="mutamers[]" value="<?= $item->safa_umrahgroups_passport_id ?>"/></td>
		                                        
		                        <? if (in_array('display_order', $cols)): ?>
                                    <td><?= $item->display_order ?></td>
                                <? endif; ?>
                                <? if (in_array('safa_umrahgroup_id', $cols)): ?>
                                    <td><?= $item->safa_umrahgroups_name ?></td>
                                <? endif; ?>
                                <? if (in_array('no', $cols)): ?>
                                    <td><?= $item->no ?></td>
                                <? endif; ?> 
                                <? if (in_array('passport_no', $cols)): ?>
                                    <td><?= $item->passport_no ?></td>
                                <? endif; ?>
                                <? if (in_array('passport_type_id', $cols)): ?>
                                    <td><?= $item->erp_passporttypes_name ?></td>
                                <? endif; ?>
                               
								<?php if (in_array('name', $cols)) { 

                                
                                if (name() == 'name_ar') {
                                	$item->full_name_ar = trim($item->full_name_ar);
                                	if(!empty($item->full_name_ar)) {	
                                		$name_by_lang = $item->full_name_ar;
                                	} else {
                                		$name_by_lang = $item->full_name_la;
                                	}
                                } else {	
                                	$item->full_name_la = trim($item->full_name_la);
                                	if(!empty($item->full_name_la)) {	
                                		$name_by_lang = $item->full_name_la;
                                	} else {
                                		$name_by_lang = $item->full_name_ar;
                                	}
                                } 
                                ?>
                                    <td> 
                                        <?= $name_by_lang ?> 
                                    </td>
                                <?php } ?>  
                                
                                <? if (in_array('name_ar', $cols)): ?>
                                    <td> 
                                        <?= $item->full_name_ar ?> 
                                    </td>
                                <? endif; ?>  
                                <? if (in_array('name_la', $cols)): ?>
                                    <td> 
                                        <?= $item->full_name_la ?> 
                                       
                                    </td>
                                <? endif; ?>
                               <? if (in_array('erp_country_id', $cols)): ?>
                                    <td><?= $item->erp_countries_name ?></td>
                                <? endif; ?>
                                <? if (in_array('nationality_id', $cols)): ?>
                                    <td><?= $item->erp_nationalities_name ?></td>
                                <? endif; ?>
                                <? if (in_array('date_of_birth', $cols)): ?>
                                    <td><?= $item->date_of_birth ?></td>
                                <? endif; ?>
                                <? if (in_array('age', $cols)): ?>
                                    <td><?= $item->age ?></td>
                                <? endif; ?>
                                <? if (in_array('passport_issue_date', $cols)): ?>
                                    <td><?= $item->passport_issue_date ?></td>
                                <? endif; ?>
                                <? if (in_array('passport_expiry_date', $cols)): ?>
                                    <td><?= $item->passport_expiry_date ?></td>
                                <? endif; ?>
                                
                                <? if (in_array('passport_issuing_country_id', $cols)): ?>
                                    <td><?= $item->issuing_country_name ?></td>
                                <? endif; ?>
                                <? if (in_array('passport_issuing_city', $cols)): ?>
                                    <td><?= $item->passport_issuing_city ?></td>
                                <? endif; ?>
                                <? if (in_array('passport_dpn_count', $cols)): ?>
                                    <td><?= $item->passport_dpn_count ?></td>
                                <? endif; ?>
                                <? if (in_array('relative_relation_id', $cols)): ?>
                                    <td><?= $item->erp_relations_name ?></td>
                                <? endif; ?>
                                <? if (in_array('occupation', $cols)): ?>
                                    <td><?= $item->occupation ?></td>
                                <? endif; ?>
                                <? if (in_array('city', $cols)): ?>
                                    <td><?= $item->city ?></td>
                                <? endif; ?>
                                 <? if (in_array('marital_status_id', $cols)): ?>
                                    <td><?= $item->erp_maritalstatus_name ?></td>
                                <? endif; ?>
                                <? if (in_array('relative_no', $cols)): ?>
                                    <td>
                                    <?php 
                                    
                                    if($item->relative_no!=0 &&  !empty($item->relative_no)) {
                                    	$this->safa_umrahgroups_passports_model->safa_umrahgroup_id= $item->safa_umrahgroup_id;
                                    	$this->safa_umrahgroups_passports_model->safa_group_passport_id=$item->relative_no;
                                    	
                                    	$relative_row=$this->safa_umrahgroups_passports_model->get();
                                    	
                                    	if($relative_row) {
                                    		$first_name_ar=$relative_row[0]->first_name_ar;
                                    		$second_name_ar=$relative_row[0]->second_name_ar;
                                    		$third_name_ar=$relative_row[0]->third_name_ar;
                                    		$fourth_name_ar=$relative_row[0]->fourth_name_ar;
                                    		
                                    		echo $first_name_ar.' '.$second_name_ar.' '.$third_name_ar;
                                    	}
                                    }
                                    
                                    ?>
                                    </td>
                                <? endif; ?>
                                <? if (in_array('dpn_serial_no', $cols)): ?>
                                    <td><?= $item->dpn_serial_no ?></td>
                                <? endif; ?>
                                
                                <? if (in_array('educational_level_id', $cols)): ?>
                                    <td><?= $item->erp_educationlevels_name ?></td>
                                <? endif; ?>
                                
                                <? if (in_array('birth_country_id', $cols)): ?>
                                    <td><?= $item->birth_country_name ?></td>
                                <? endif; ?>
                                <? if (in_array('birth_city', $cols)): ?>
                                    <td><?= $item->birth_city ?></td>
                                <? endif; ?>
                                
                                
                                
                                <? if (in_array('gender_id', $cols)): ?>
                                    <td><?= $item->gender_id ?></td>
                                <? endif; ?>
                               <? if (in_array('relative_gender_id', $cols)): ?>
                                    <td><?= $item->relative_gender_id ?></td>
                                <? endif; ?>
                                
                                
                                
                                
                                
                                 
                                <td class="TAC">

                                    <a title='<?= lang('mutamer_data') ?>' 
                                       href="<?= site_url("ea/umrahgroups_passports/show_data") ?>/<?= $item->safa_umrahgroups_passport_id ?>" class="fancybox fancybox.iframe"
                                       style="color:black; direction:rtl">
                                        <span class="icon-user"></span>
                                    </a>
                                    
                                    
                                    <?php 
                                    if($item->relative_no!=0) {
                                    	$this->safa_umrahgroups_passports_model->safa_umrahgroups_passport_id = false;
                                    	$this->safa_umrahgroups_passports_model->safa_group_passport_id = $item->relative_no;
                                    	$umrahgroups_passport_compain_row = $this->safa_umrahgroups_passports_model->get();
                                    	if(count($umrahgroups_passport_compain_row)>0) {
                                    		$safa_umrahgroups_passport_id = $umrahgroups_passport_compain_row[0]->safa_umrahgroups_passport_id;
                                    	} else {
                                    		$safa_umrahgroups_passport_id = $item->safa_umrahgroups_passport_id;
                                    	}
                                    } else {
                                    	$safa_umrahgroups_passport_id = $item->safa_umrahgroups_passport_id;
                                    }
                                    ?>
                                    
                                    <a title='<?= lang('mutamer_data') ?>' 
                                       href="<?= site_url("ea/umrahgroups_passports/edit") ?>/<?= $safa_umrahgroups_passport_id ?>" class=""
                                       style="color:black; direction:rtl">
                                        <span class="icon-pencil"></span>
                                    </a>

									<!-- By Gouda -->
									<!-- 
                                    <a href="#fModal" name="<?= $item->first_name_ar ?>"  id="<?= $item->safa_umrahgroups_passport_id ?>" class="delete_item" data-toggle="modal">
                                        <span class="icon-trash"></span></a>
                                    -->
                                         
                                    <a href="<?= site_url('ea/umrahgroups_passports/delete/'.$item->safa_umrahgroup_id.'/'.$item->safa_umrahgroups_passport_id) ?>" onclick="return confirm('<?= lang('global_are_you_sure_you_want_to_delete')?>')"><span class="icon-trash"></span></a>
                            

                                </td>
                            </tr>
                        <? endforeach; ?>
                    <? endif; ?>
                </tbody>
            </table>
        </div>
        <div class="toolbar bottom TAC">
            <a href="#f2Modal"   id='delete_selected' class="btn btn-primary" data-toggle="modal">
                <?= lang('delete_selected') ?></a>
            <a href="#f2Modal"   id='copy_cut_selected' class="btn btn-primary" data-toggle="modal">
                <?= lang('copy_cut') ?></a>
            <a href="#f2Modal"   id='export_selected' class="btn btn-primary" data-toggle="modal">
                <?= lang('export') ?></a>
            <input type="hidden" name="copy_cut_val" value="cut" id='copy_cut_val' class="btn btn-primary">
            <input type="hidden" name="export_val" value="gama" id='export_val' class="btn btn-primary">
            <? if ($safa_umrahgroup_id !== FALSE) { ?>
            <a   class="btn btn-primary" href="<?= site_url('ea/safa_umrahgroups/index') ?>fancybox " ><?= lang('global_back') ?></a>
                <input type="hidden" name="safa_umrahgroup_id" value="<?= $safa_umrahgroup_id ?>" class="btn btn-primary"> 
            <? } ?>
        </div>
    </div>
</div>
<div class="row-fluid">
    <? //= $pagination  ?>
    <div id="fModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h3 id="myModalLabel"><?= lang('global_system_message') ?></h3>
        </div>        
        <div class="row-fluid">
            <div id="msg" class="row-form">
                <?= lang('global_are_you_sure_you_want_to_delete') ?>
            </div>
        </div>                   
        <div class="modal-footer">
            <button class="btn btn-primary" data-dismiss="modal" aria-hidden="true"id="yes"><?= lang('global_yes') ?></button>
            <button class="btn btn-primary" data-dismiss="modal" aria-hidden="true"id="no"><?= lang('global_no') ?></button>       </div>
    </div>
    <div id="f2Modal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h3 id="myModalLabel"><?= lang('global_system_message') ?></h3>
        </div>        
        <div class="row-fluid">
            <div id='msg_body'class="row-form">

            </div>
        </div>                   
        <div id='yes_no'class="modal-footer">
            <button class="btn btn-primary" data-dismiss="modal" aria-hidden="true" id="as"><?= lang('global_yes') ?></button>
            <button class="btn btn-primary" data-dismiss="modal" aria-hidden="true" id="2"><?= lang('global_no') ?></button>       
        </div>
        <div id='sub' class="modal-footer">
            <button class='btn btn-primary' data-dismiss='modal' aria-hidden='true'><?= lang('global_submit') ?></button>
        </div>
    </div>
    <script type="text/javascript">
                $(document).ready(function() {
                    $('.delete_item').click(function() {
                        var name = $(this).attr('name');
                        var safa_umrahgroups_passport_id = $(this).attr('id');
<? if (lang('global_lang') == 'ar'): ?>
                            $('#msg').text("<?= lang('global_are_you_sure_you_want_to_delete') ?>" + " " + name)
<? else: ?>
                            $('#msg').text(name + " " + "<?= lang('global_are_you_sure_you_want_to_delete') ?>")
<? endif; ?>
                        $('#yes').click(function() {
                            var answer = $(this).attr('id');
                            if (answer === 'yes') {
                                $.ajax({
                                    type: "POST",
                                    dataType: 'json',
                                    url: "<?php echo site_url('ea/umrahgroups_passports/delete'); ?>",
                                    data: {'safa_umrahgroups_passport_id': safa_umrahgroups_passport_id},
                                    success: function(msg) {
                                        if (msg.response == true) {
                                            var del = safa_umrahgroups_passport_id;
                                            $("#row_" + del).remove();
<? if (lang('global_lang') == 'ar'): ?>
                                                if (msg.passno)
                                                    $('.updated_msg').text(msg.msg + " " + name + " " + msg.passno);
                                                else
                                                    $('.updated_msg').text(msg.msg + " " + name);
                                                $('.updated_msg').append('<br>');
                                                $('.updated_msg').append('<input  type ="button" value="<?= lang('global_submit') ?>"class="btn btn-primary" id="ok" >');
<? else: ?>
                                                if (msg.passno)
                                                    $('.updated_msg').text(msg.msg + " " + name + " " + msg.passno);
                                                else
                                                    $('.updated_msg').text(msg.msg + " " + name);
                                                $('.updated_msg').append('<br>');
                                                $('.updated_msg').append('<input  type ="button" value="<?= lang('global_submit') ?>"class="btn btn-primary" id="ok" >');
<? endif; ?>
                                            $('.updated_msg').show();
                                            $('#ok').click(function() {
                                                $('.updated_msg').hide();
                                            });
                                        } else if (msg.response == false) {
                                            $('.updated_msg').text(msg.msg);
                                            $('.updated_msg').append('<br>');
                                            $('.updated_msg').append('<input  type ="button" value="<?= lang('global_submit') ?>"class="btn btn-primary" id="ok" >');
                                            $('.updated_msg').show();
                                            $('#ok').click(function() {
                                                $('.updated_msg').hide();
                                            });
                                        }
                                    }
                                });
                            } else {
                                return FALSE;
                            }
                        });
                    });
                });
    </script>
    <script type="text/javascript">
        $(function() {
            $('#datetimepicker1').datetimepicker({
                language: 'pt-BR',
                pickTime: false
            });
        });
    </script>
    <script type="text/javascript">
        $(function() {
            $('#datetimepicker2').datetimepicker({
                language: 'pt-BR',
                pickTime: false
            });
        });
    </script>
    <script>
        $(document).ready(function() {
            $("#search").keypress(function(e) {
                if (e.which == 13)
                {
                    e.preventDefault();
                }
                var searched = $('#search').val();
                $.getJSON("<?php echo site_url('ea/umrahgroups_passports/get_passport_no'); ?>/" + searched, function(result) {
                    var elements = [];
                    $.each(result, function(i, val) {
                        elements.push(val.passport_no)
                    });

                    $('#search').autocomplete({
                        source: elements
                    });
                    $('.ui-helper-hidden-accessible').hide();
                });
            });

            $("#mutamer_name").keypress(function(e) {
                if (e.which == 13)
                {
                    e.preventDefault();
                }
                var searched = $('#mutamer_name').val();
                $.ajax({
                    type: "POST",
                    dataType: 'json',
                    url: "<?php echo site_url('ea/umrahgroups_passports/mutamer_name'); ?>",
                    data: {'searched': "" + searched + ""},
                    success: function(data) {
                        var elements = [];
                        $.each(data, function(i, val) {
                            if (val.first_name_ar)
                                elements.push(val.first_name_ar + " " + val.second_name_ar + " " + val.third_name_ar)
                            else
                                elements.push(val.first_name_la + " " + val.second_name_la + " " + val.third_name_la)
                        });
                        $('#mutamer_name').autocomplete({
                            source: elements
                        });
                        $('.ui-helper-hidden-accessible').hide();

                    }
                });
            });
        });

    </script>
    <script>
        $(document).ready(function() {
            $('.fancybox').fancybox({
                afterClose: function() {
                    location.reload();
                }
            });
        });
    </script>
    <script type='text/javascript' src='<?= JS ?>/custom/table_operation.js'></script>
    <script>

        check_list_delete_submit("mutamers", "fsTable", "form_mutamers", "<?= lang('create_umrah_passports_msg') ?>");

      //By Gouda.
        function checkUncheck() 
        {		
            for (var i = 0; i < document.form_mutamers.elements.length; i++ ) 
            {
                if (document.form_mutamers.elements[i].type == 'checkbox' && document.form_mutamers.elements[i].id !='checkall') 
        	    {		       		    
                	document.form_mutamers.elements[i].checked = document.form_mutamers.elements['checkall'].checked; 
                }

            }
        }
    </script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('#delete_selected').click(function() {
                var flag = 0;
                $(".fsTable").find("[type='checkbox']").each(function(index, object) {
                    if ($(object).attr("class") != "checkall" && $(object).parent().attr("class") == "checked") {
                        flag = 1;
                        return false;
                    }
                });

              //By Gouda
				for (var i = 0; i < document.form_mutamers.elements.length; i++ ) 
	            {
	                if (document.form_mutamers.elements[i].checked ) {
	                	flag = 1;
	                } 
	            }
	            
                if (flag == 1) {
                    $("#form_mutamers").attr("action", "<?= site_url('ea/umrahgroups_passports/delete_all') ?>");
                    $('#msg_body').text("<?= lang('global_delete_selected_records') ?>");
                    $("<br>").appendTo($("#msg_body"));
                    $('#sub').hide();
                    $('#yes_no').show();
                    $('#as').click(function() {
                        document.getElementById('form_mutamers').submit();
                    });
                } else {
                    $('#msg_body').text("<?= lang('global_select_one_record_at_least') ?>");
                    $('#yes_no').hide();
                    $('#sub').show();
                }
//      return false ;    
            });
        });
    </script>

    <script type="text/javascript">
        $(document).ready(function() {
            $('#copy_cut_selected').click(function() {
                var flag = 0;
                $(".fsTable").find("[type='checkbox']").each(function(index, object) {
                    if ($(object).attr("class") != "checkall" && $(object).parent().attr("class") == "checked") {
                        flag = 1;
                        return false;
                    }
                });

              //By Gouda
				for (var i = 0; i < document.form_mutamers.elements.length; i++ ) 
	            {
	                if (document.form_mutamers.elements[i].checked ) {
	                	flag = 1;
	                } 
	            }
	            
                if (flag == 1) {
                    $("#form_mutamers").attr("action", "<?= site_url('ea/umrahgroups_passports/copy_cut') ?>");
                    $('#msg_body').text("<?= lang('umrahgroups_passports_mutamers_confirm_add') ?>");
                    $("<br>").appendTo($("#msg_body"));
                    $('<?= lang('cut_to_umrahgroups_passports') ?>:<input type="radio"  name="copy_cut" checked value="cut">').appendTo($("#msg_body"));

                    $("<span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>").appendTo($("#msg_body"));
                    $('<?= lang('copy_to_umrahgroups_passports') ?>:<input type="radio"  name="copy_cut" value="copy">').appendTo($("#msg_body"));
                    $('#sub').hide();
                    $('#yes_no').show();
                    var copy_cut_val = null;
                    $("input[name='copy_cut']").click(function() {
                        copy_cut_val = $(this).filter(':checked').val();
                        $('input#copy_cut_val').val(copy_cut_val);
                    });
                    $('#as').click(function() {
                        document.getElementById('form_mutamers').submit();
                    });
                } else {
                    $('#msg_body').text("<?= lang('global_select_one_record_at_least') ?>");
                    $('#yes_no').hide();
                    $('#sub').show();
                }
//      return false ;    
            });
        });
    </script>

    <script type="text/javascript">
        $(document).ready(function() {
            $('#export_selected').click(function() {
                var flag = 0;
                $(".fsTable").find("[type='checkbox']").each(function(index, object) {
                    if ($(object).attr("class") != "checkall" && $(object).parent().attr("class") == "checked") {
                        flag = 1;
                        return false;
                    }
                });

              //By Gouda
				for (var i = 0; i < document.form_mutamers.elements.length; i++ ) 
	            {
	                if (document.form_mutamers.elements[i].checked ) {
	                	flag = 1;
	                } 
	            }
	            
                if (flag == 1) {
                    $("#form_mutamers").attr("action", "<?= site_url('ea/umrahgroups_passports/export_mutamers_file') ?>");
                    $('#msg_body').text("<?= lang('umrahgroups_passports_file_type_confirm_add') ?>");
                    $("<br>").appendTo($("#msg_body"));
                    $('<?= lang('export_to_gama') ?>:<input type="radio"  name="export_type" checked value="gama">').appendTo($("#msg_body"));
                    $("<span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>").appendTo($("#msg_body"));
                    $('<?= lang('export_to_babUmrah') ?>:<input type="radio"  name="export_type" value="babUmrah">').appendTo($("#msg_body"));
                    $("<span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>").appendTo($("#msg_body"));
                    $('<?= lang('export_to_wayToUmrah') ?>:<input type="radio"  name="export_type" value="wayToUmrah">').appendTo($("#msg_body"));
                    $("<span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>").appendTo($("#msg_body"));
                    $('<?= lang('export_to_fileFafa') ?>:<input type="radio"  name="export_type" value="FileSafa">').appendTo($("#msg_body"));
                    
                    $('#sub').hide();
                    $('#yes_no').show();
                    var export_val = null;
                    $("input[name='export_type']").click(function() {
                        export_val = $(this).filter(':checked').val();
                        $('input#export_val').val(export_val);
                    });
                    $('#as').click(function() {
                        document.getElementById('form_mutamers').submit();
                    });
                } else {
                    $('#msg_body').text("<?= lang('global_select_one_record_at_least') ?>");
                    $('#yes_no').hide();
                    $('#sub').show();
                }
//      return false ;    
            });
        });
    </script>
    <script>
    	// By Gouda.
        //$(document).ready(function() {
            $(".fsTable tbody tr").css("cursor", "move");
            // Return a helper with preserved width of cells
            var fixHelper = function(e, ui) {
                ui.children().each(function() {
                    $(this).width($(this).width());
                });
                return ui;
            };

            $('.fsTable tbody').sortable({
                update: function() {
                    var stringDiv = "";
                    var umrahgroup_id = $(".fsTable tbody tr").attr("umrahgroup_id");
                    $(".fsTable tbody").children().each(function(i) {
                        var tr = $(this);
                        stringDiv += " " + tr.attr("id") + '=' + i + '&';
                    });

                    $.ajax({
                        type: "POST",
                        url: "<?php echo site_url('ea/umrahgroups_passports/sortable'); ?>/" + umrahgroup_id,
                        data: stringDiv
                    });
                }
            });
            $(".sortable").disableSelection();
        //});




        
      //By Gouda
        if ($(".fsTable").length > 0) {

        	//By Gouda, TO remove the warning message.
        	//$.fn.dataTableExt.sErrMode = 'throw';

               $(".fsTable").dataTable({
                   bSort: true,
                   bAutoWidth: true,
                   "iDisplayLength": <?php 
                   $ea_user_id = session('ea_user_id');
                   if ($this->input->cookie($ea_user_id . '_umrahgroups_passports_rows_count_per_page')) {
                       echo $this->input->cookie($ea_user_id . '_umrahgroups_passports_rows_count_per_page');
                   }else {echo 10;}?>, 
                   "aLengthMenu": [5, 10, 25, 50, 100, 200, 500], // can be removed for basic 10 items per page
                   "bFilter": true,
                   "sPaginationType": "full_numbers",
                   "aoColumnDefs": [{"bSortable": false,
                   "aTargets": [-1, 0]}]

                   });
           }

            
</script>



<script>

$('[name="tbl_mutamers_length"]').change(function() {
	
	var rows_count_per_page=$(this).val();
    //alert(rows_count_per_page);
	var dataString = 'rows_count_per_page='+ rows_count_per_page;
    
    $.ajax
    ({
    	type: 'POST',
    	url: '<?php echo base_url().'ea/umrahgroups_passports/setRowsCountCookie'; ?>',
    	data: dataString,
    	cache: true,
    	success: function(html)
    	{
        	
    	}
    });
	
});


</script>
