
<div class="widget">
    <div class="head dark">
        <div class="icon"><i class="icos-pencil2"></i></div>
        <h2> <?= lang('umrahgroups_passports_edit_title') ?></h2> 
    </div>                        
    <div class="block-fluid">
        <?= form_open() ?>
        <div class="row-form">
            <div class="span6">
                <div class="span4"> <?= lang('umrahgroups_passports_safa_umrahgroup_id') ?>:</div>
                <div class="span8">
                    <?= form_input('safa_umrahgroup_id', set_value("safa_umrahgroup_id", $item->safa_umrahgroup_id), " ") ?>
                    <?= form_error('safa_umrahgroup_id', '<div class="bottom" style="color:red" >', '</div>'); ?>
                </div>
            </div>
            <div class="span6">
                <div class="span4"> <?= lang('umrahgroups_passports_no') ?>:</div>
                <div class="span8">
                    <?= form_input('no', set_value("no", $item->no), " ") ?>
                    <?= form_error('no', '<div class="bottom" style="color:red" >', '</div>'); ?>
                </div>
            </div>
        </div>
        <div class="row-form">
            <div class="span6">
                <div class="span4"> <?= lang('umrahgroups_passports_title_id') ?>:</div>
                <div class="span8">
                    <?= form_input('title_id', set_value("title_id", $item->title_id), " ") ?>
                    <?= form_error('title_id', '<div class="bottom" style="color:red" >', '</div>'); ?>
                </div>
            </div>
            <div class="span6">
                <div class="span4"> <?= lang('umrahgroups_passports_first_name_ar') ?>:</div>
                <div class="span8">
                    <?= form_input('first_name_ar', set_value("first_name_ar", $item->first_name_ar), " ") ?>
                    <?= form_error('first_name_ar', '<div class="bottom" style="color:red" >', '</div>'); ?>
                </div>
            </div>
        </div>
        <div class="row-form">
            <div class="span6">
                <div class="span4"> <?= lang('umrahgroups_passports_second_name_ar') ?>:</div>
                <div class="span8">
                    <?= form_input('second_name_ar', set_value("second_name_ar", $item->second_name_ar), " ") ?>
                    <?= form_error('second_name_ar', '<div class="bottom" style="color:red" >', '</div>'); ?>
                </div>
            </div>
            <div class="span6">
                <div class="span4"> <?= lang('umrahgroups_passports_third_name_ar') ?>:</div>
                <div class="span8">
                    <?= form_input('third_name_ar', set_value("third_name_ar", $item->third_name_ar), " ") ?>
                    <?= form_error('third_name_ar', '<div class="bottom" style="color:red" >', '</div>'); ?>
                </div>
            </div>
        </div>
        <div class="row-form">
            <div class="span6">
                <div class="span4"> <?= lang('umrahgroups_passports_fourth_name_ar') ?>:</div>
                <div class="span8">
                    <?= form_input('fourth_name_ar', set_value("fourth_name_ar", $item->fourth_name_ar), " ") ?>
                    <?= form_error('fourth_name_ar', '<div class="bottom" style="color:red" >', '</div>'); ?>
                </div>
            </div>
            <div class="span6">
                <div class="span4"> <?= lang('umrahgroups_passports_first_name_la') ?>:</div>
                <div class="span8">
                    <?= form_input('first_name_la', set_value("first_name_la", $item->first_name_la), " ") ?>
                    <?= form_error('first_name_la', '<div class="bottom" style="color:red" >', '</div>'); ?>
                </div>
            </div>
        </div>
        <div class="row-form">
            <div class="span6">
                <div class="span4"> <?= lang('umrahgroups_passports_second_name_la') ?>:</div>
                <div class="span8">
                    <?= form_input('second_name_la', set_value("second_name_la", $item->second_name_la), " ") ?>
                    <?= form_error('second_name_la', '<div class="bottom" style="color:red" >', '</div>'); ?>
                </div>
            </div>
            <div class="span6">
                <div class="span4"> <?= lang('umrahgroups_passports_third_name_la') ?>:</div>
                <div class="span8">
                    <?= form_input('third_name_la', set_value("third_name_la", $item->third_name_la), " ") ?>
                    <?= form_error('third_name_la', '<div class="bottom" style="color:red" >', '</div>'); ?>
                </div>
            </div>
        </div>
        <div class="row-form">
            <div class="span6">
                <div class="span4"> <?= lang('umrahgroups_passports_fourth_name_la') ?>:</div>
                <div class="span8">
                    <?= form_input('fourth_name_la', set_value("fourth_name_la", $item->fourth_name_la), " ") ?>
                    <?= form_error('fourth_name_la', '<div class="bottom" style="color:red" >', '</div>'); ?>
                </div>
            </div>
            <div class="span6">
                <div class="span4"> <?= lang('umrahgroups_passports_nationality_id') ?>:</div>
                <div class="span8">
                    <?= form_input('nationality_id', set_value("nationality_id", $item->nationality_id), " ") ?>
                    <?= form_error('nationality_id', '<div class="bottom" style="color:red" >', '</div>'); ?>
                </div>
            </div>
        </div>
        <div class="row-form">
            <div class="span6">
                <div class="span4"> <?= lang('umrahgroups_passports_previous_nationality_id') ?>:</div>
                <div class="span8">
                    <?= form_input('previous_nationality_id', set_value("previous_nationality_id", $item->previous_nationality_id), " ") ?>
                    <?= form_error('previous_nationality_id', '<div class="bottom" style="color:red" >', '</div>'); ?>
                </div>
            </div>
            <div class="span6">
                <div class="span4"> <?= lang('umrahgroups_passports_passport_no') ?>:</div>
                <div class="span8">
                    <?= form_input('passport_no', set_value("passport_no", $item->passport_no), " ") ?>
                    <?= form_error('passport_no', '<div class="bottom" style="color:red" >', '</div>'); ?>
                </div>
            </div>
        </div>
        <div class="row-form">
            <div class="span6">
                <div class="span4"> <?= lang('umrahgroups_passports_passport_type_id') ?>:</div>
                <div class="span8">
                    <?= form_input('passport_type_id', set_value("passport_type_id", $item->passport_type_id), " ") ?>
                    <?= form_error('passport_type_id', '<div class="bottom" style="color:red" >', '</div>'); ?>
                </div>
            </div>
            <div class="span6">
                <div class="span4"> <?= lang('umrahgroups_passports_passport_issue_date') ?>:</div>
                <div class="span8">
                    <?= form_input('passport_issue_date', set_value("passport_issue_date", $item->passport_issue_date), " ") ?>
                    <?= form_error('passport_issue_date', '<div class="bottom" style="color:red" >', '</div>'); ?>
                </div>
            </div>
        </div>
        <div class="row-form">
            <div class="span6">
                <div class="span4"> <?= lang('umrahgroups_passports_passport_expiry_date') ?>:</div>
                <div class="span8">
                    <?= form_input('passport_expiry_date', set_value("passport_expiry_date", $item->passport_expiry_date), " ") ?>
                    <?= form_error('passport_expiry_date', '<div class="bottom" style="color:red" >', '</div>'); ?>
                </div>
            </div>
            <div class="span6">
                <div class="span4"> <?= lang('umrahgroups_passports_passport_issuing_city') ?>:</div>
                <div class="span8">
                    <?= form_input('passport_issuing_city', set_value("passport_issuing_city", $item->passport_issuing_city), " ") ?>
                    <?= form_error('passport_issuing_city', '<div class="bottom" style="color:red" >', '</div>'); ?>
                </div>
            </div>
        </div>
        <div class="row-form">
            <div class="span6">
                <div class="span4"> <?= lang('umrahgroups_passports_passport_issuing_country_id') ?>:</div>
                <div class="span8">
                    <?= form_input('passport_issuing_country_id', set_value("passport_issuing_country_id", $item->passport_issuing_country_id), " ") ?>
                    <?= form_error('passport_issuing_country_id', '<div class="bottom" style="color:red" >', '</div>'); ?>
                </div>
            </div>
            <div class="span6">
                <div class="span4"> <?= lang('umrahgroups_passports_passport_dpn_count') ?>:</div>
                <div class="span8">
                    <?= form_input('passport_dpn_count', set_value("passport_dpn_count", $item->passport_dpn_count), " ") ?>
                    <?= form_error('passport_dpn_count', '<div class="bottom" style="color:red" >', '</div>'); ?>
                </div>
            </div>
        </div>
        <div class="row-form">
            <div class="span6">
                <div class="span4"> <?= lang('umrahgroups_passports_dpn_serial_no') ?>:</div>
                <div class="span8">
                    <?= form_input('dpn_serial_no', set_value("dpn_serial_no", $item->dpn_serial_no), " ") ?>
                    <?= form_error('dpn_serial_no', '<div class="bottom" style="color:red" >', '</div>'); ?>
                </div>
            </div>
            <div class="span6">
                <div class="span4"> <?= lang('umrahgroups_passports_relative_relation_id') ?>:</div>
                <div class="span8">
                    <?= form_input('relative_relation_id', set_value("relative_relation_id", $item->relative_relation_id), " ") ?>
                    <?= form_error('relative_relation_id', '<div class="bottom" style="color:red" >', '</div>'); ?>
                </div>
            </div>
        </div>
        <div class="row-form">
            <div class="span6">
                <div class="span4"> <?= lang('umrahgroups_passports_educational_level_id') ?>:</div>
                <div class="span8">
                    <?= form_input('educational_level_id', set_value("educational_level_id", $item->educational_level_id), " ") ?>
                    <?= form_error('educational_level_id', '<div class="bottom" style="color:red" >', '</div>'); ?>
                </div>
            </div>
            <div class="span6">
                <div class="span4"> <?= lang('umrahgroups_passports_occupation') ?>:</div>
                <div class="span8">
                    <?= form_input('occupation', set_value("occupation", $item->occupation), " ") ?>
                    <?= form_error('occupation', '<div class="bottom" style="color:red" >', '</div>'); ?>
                </div>
            </div>
        </div>
        <div class="row-form">
            <div class="span6">
                <div class="span4"> <?= lang('umrahgroups_passports_date_of_birth') ?>:</div>
                <div class="span8">
                    <?= form_input('date_of_birth', set_value("date_of_birth", $item->date_of_birth), " ") ?>
                    <?= form_error('date_of_birth', '<div class="bottom" style="color:red" >', '</div>'); ?>
                </div>
            </div>
            <div class="span6">
                <div class="span4"> <?= lang('umrahgroups_passports_age') ?>:</div>
                <div class="span8">
                    <?= form_input('age', set_value("age", $item->age), " ") ?>
                    <?= form_error('age', '<div class="bottom" style="color:red" >', '</div>'); ?>
                </div>
            </div>
        </div>
        <div class="row-form">
            <div class="span6">
                <div class="span4"> <?= lang('umrahgroups_passports_birth_city') ?>:</div>
                <div class="span8">
                    <?= form_input('birth_city', set_value("birth_city", $item->birth_city), " ") ?>
                    <?= form_error('birth_city', '<div class="bottom" style="color:red" >', '</div>'); ?>
                </div>
            </div>
            <div class="span6">
                <div class="span4"> <?= lang('umrahgroups_passports_birth_country_id') ?>:</div>
                <div class="span8">
                    <?= form_input('birth_country_id', set_value("birth_country_id", $item->birth_country_id), " ") ?>
                    <?= form_error('birth_country_id', '<div class="bottom" style="color:red" >', '</div>'); ?>
                </div>
            </div>
        </div>
        <div class="row-form">
            <div class="span6">
                <div class="span4"> <?= lang('umrahgroups_passports_relative_no') ?>:</div>
                <div class="span8">
                    <?= form_input('relative_no', set_value("relative_no", $item->relative_no), " ") ?>
                    <?= form_error('relative_no', '<div class="bottom" style="color:red" >', '</div>'); ?>
                </div>
            </div>
            <div class="span6">
                <div class="span4"> <?= lang('umrahgroups_passports_relative_gender_id') ?>:</div>
                <div class="span8">
                    <?= form_input('relative_gender_id', set_value("relative_gender_id", $item->relative_gender_id), " ") ?>
                    <?= form_error('relative_gender_id', '<div class="bottom" style="color:red" >', '</div>'); ?>
                </div>
            </div>
        </div>
        <div class="row-form">
            <div class="span6">
                <div class="span4"> <?= lang('umrahgroups_passports_gender_id') ?>:</div>
                <div class="span8">
                    <?= form_input('gender_id', set_value("gender_id", $item->gender_id), " ") ?>
                    <?= form_error('gender_id', '<div class="bottom" style="color:red" >', '</div>'); ?>
                </div>
            </div>
            <div class="span6">
                <div class="span4"> <?= lang('umrahgroups_passports_marital_status_id') ?>:</div>
                <div class="span8">
                    <?= form_input('marital_status_id', set_value("marital_status_id", $item->marital_status_id), " ") ?>
                    <?= form_error('marital_status_id', '<div class="bottom" style="color:red" >', '</div>'); ?>
                </div>
            </div>
        </div>
        <div class="row-form">
            <div class="span6">
                <div class="span4"> <?= lang('umrahgroups_passports_city') ?>:</div>
                <div class="span8">
                    <?= form_input('city', set_value("city", $item->city), " ") ?>
                    <?= form_error('city', '<div class="bottom" style="color:red" >', '</div>'); ?>
                </div>
            </div>
            <div class="span6">
                <div class="span4"> <?= lang('umrahgroups_passports_erp_country_id') ?>:</div>
                <div class="span8">
                    <?= form_input('erp_country_id', set_value("erp_country_id", $item->erp_country_id), " ") ?>
                    <?= form_error('erp_country_id', '<div class="bottom" style="color:red" >', '</div>'); ?>
                </div>
            </div>
        </div>

        <div class="toolbar bottom TAC"><input type="submit" name="submit" value="<?= lang('global_submit') ?>" class="btn btn-primary"> &nbsp;
            <!--<input  type ="button" value="<?= lang('global_back') ?>"class="btn btn-primary" onclick="window.location = '<?= site_url('ea/umrahgroups_passports/index') ?>'">-->
            <input type="button" value="<?= lang('global_back') ?>" class="btn btn-primary" onclick="window.history.back()"/>
        </div>
        <?= form_close() ?>     
    </div>
</div>



