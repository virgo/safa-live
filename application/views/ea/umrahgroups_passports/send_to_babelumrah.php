<div class="widget">
    <div class="head dark">
        <div class="ifix movement reportscon"><i class="icos-pencil2"></i><h2><?= lang('choose_cols') ?></h2> </div>
    </div> 
    <div class="block-fluid">
        <?= form_open() ?>
        <div class="row-form">
            <div class="span11">
                    Package:
                    <?= form_dropdown('package_id', $packages, set_value('package_id')) ?> <br />
                    
            </div>
        </div>
        <div class="row-form">
            <div class="span11">
                    Expected Departure Date
                    <?= form_input('expected_departure_date', set_value('expected_departure_date')) ?> <br />
                    
            </div>
        </div>
        <div class="row-form">
            <div class="span11">
                    Expected Arrival Date
                    <?= form_input('expected_arrival_date', set_value('expected_arrival_date')) ?> <br />
            </div>
        </div>
        <div class="toolbar bottom TAL">
            <button name="submit"  class="btn btn-primary close_frame"><?= lang("global_submit") ?></button>    
        </div>
        <?= form_close() ?>
    </div>
</div>
<script>
    $('.close_frame').click(function() {
        parent.$.fancybox.close();
    });
</script>
<script type="text/javascript">
    function checkAll(checkId) {

        var inputs = document.getElementsByTagName("input");

        for (var i = 0; i < inputs.length; i++) {
            if($(".checkall").is(':checked')){
                    inputs[i].checked = true;
                    $('span').addClass('checked');
                }else {
                    inputs[i].checked = false;
                    $('span').removeClass('checked');
    }
        }
    }
</script>
        
        
        
        
        
        
        