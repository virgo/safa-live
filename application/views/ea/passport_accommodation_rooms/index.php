<style>
    .widget {width:98%}
    .wizerd-div {
        border-bottom: medium none !important;
        margin: -18px 0 20px;
        padding-top: 0;
    }
    .wizerd-div a {
        background: none repeat scroll 0 0 #FAFAFA;
        border: 1px solid #D5D6D6;
        border-radius: 49px;
        color: #663300;
        display: inline-block;
        margin: -6px 5px -17px -8px;
        padding: 10px 12px 8px;
    }
    a {
        color: #C09853;
    }
    .resalt-group {
        margin: 18px 0.5% 0.5%;
        padding: 0.5%;
        width: 99%;
    }
    th a.btn, th input[type="button"], th input[type="submit"], th button {
        margin: 0;
        padding: 4px 12px;
    }
    .coll_close, .coll_open {
        margin-top: 0;
    }
    .chosen-container {
        margin-top: 4px;
    }
    .warning {
        color: #C09853;
    }
</style>

<div class="widget">
    <div class="path-container Fright">
        <div class="path-name Fright">
            <a href="<?= site_url('ea/dashboard') ?>"><?= lang('global_system_management') ?></a>
        </div>
        <div class="path-arrow Fright"> </div>
        <div class="path-name Fright"><?= lang('passport_accommodation_rooms') ?></div>
    </div>
</div>

<div class="widget">
    <div class="widget-header">
        <div class="widget-header-icon Fright"></div>
        <div class="widget-header-title Fright"><?= lang('passport_accommodation_rooms') ?></div>
        <a class="btn Fleft" href="<?= site_url('ea/passport_accommodation_rooms/manage') ?>"><?= lang('global_add') ?></a>
    </div>
    <div class="widget-container">
        <div class='table-responsive' >
            <table cellpadding="0" class="fsTable" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th><?= lang('safa_trip_id') ?></th>
                        <th><?= lang('safa_package_id') ?></th>
                        <th><?= lang('safa_package_periods_id') ?></th>
                        <th><?= lang('global_actions') ?></th>
                    </tr>
                </thead>
                <tbody>
                    <? if (isset($items)): ?>
                        <? foreach ($items as $item): ?>
                            <tr>
                                <td class="TAC info"><?= $item->safa_trip_name ?></td>
                                <td class="TAC success"><?= $item->safa_package_name ?></td>
                                <td class="TAC success"><?= $item->safa_package_period_name ?></td>
                                
                                <td class="TAC">
                                    <a href="<?= site_url("ea/passport_accommodation_rooms/manage/" . $item->safa_reservation_form_id) ?>" ><span class="icon-pencil"></span></a>
                                    
                                    <?php 
                                    $this->safa_reservation_forms_model->safa_reservation_form_id = $item->safa_reservation_form_id;
									$safa_reservation_forms_row = $this->safa_reservation_forms_model->get();
									if(count($safa_reservation_forms_row)>0) {
										$this->safa_trips_model->safa_trip_id = $safa_reservation_forms_row->safa_trip_id;
										$safa_trips_row = $this->safa_trips_model->get();
										if($safa_trips_row->safa_tripstatus_id!=3 && $safa_trips_row->safa_tripstatus_id!=4) {
									?>
									<a href="<?= site_url('ea/passport_accommodation_rooms/delete/' . $item->safa_reservation_form_id) ?>" onclick="return confirm('<?= lang('global_are_you_sure_you_want_to_delete')?>')"><span class="icon-trash"></span></a>
									<?php 
										}
									}
                                    ?>
                                    
                                </td>
                            </tr>
                        <? endforeach ?>
                    <? endif ?>
                </tbody>
            </table>
<!--            <? if (isset($pagination)): ?><?= $pagination ?><? endif ?>-->
        </div>
    </div>
</div>