<script>
jQuery(function() { 
	menu1.toggle();
})
</script>


<?php
$safa_reservation_form_id = 0;
$safa_ea_id = '';
$safa_trip_id = '';
$safa_package_period_id = '';
$safa_package_id = '';
$item_hotels_select = '';
$item_hotels_arr = array();
$package_periods_arr = array();
$package_hotels_arr = array();

$screen_title = lang('add_title');

if (isset($item)) {

	$safa_reservation_form_id = $item->safa_reservation_form_id;
	$safa_ea_id = $item->safa_ea_id;
	$safa_trip_id = $item->safa_trip_id;
	
	$safa_trip_name = $item->safa_trip_name;
	
	$safa_package_period_id = $item->safa_package_period_id;

	$this->safa_package_periods_model->safa_package_periods_id = $safa_package_period_id;
	$safa_package_row = $this->safa_package_periods_model->get();
	$safa_package_id = $safa_package_row->safa_package_id;

	//----------- Form Selected Hotels Array ------------------------------------
	$item_hotels_select= $item_hotels_select_arr;
	//----------------------------------------------------------------------------------

	//----------- Package Hotels Array for Dropdown ------------------------------------
	$structure = array('erp_hotel_id', 'erp_hotel_name');
	$key = $structure['0'];
	if (isset($structure['1'])) {
		$value = $structure['1'];
	}

	$this->package_hotels_model->safa_package_hotel_id = false ;
	$this->package_hotels_model->safa_package_id = $safa_package_id;
	$package_hotels_rows = $this->package_hotels_model->get();
	foreach ($package_hotels_rows as $package_hotels_row) {
		$item_hotels_arr[$package_hotels_row->$key] = $package_hotels_row->$value;
	}
	//----------------------------------------------------------------------------------

	//----------- Package Periods Array for Dropdown ------------------------------------
	$structure = array('safa_package_periods_id', 'erp_package_period_name');
	$key = $structure['0'];
	if (isset($structure['1'])) {
		$value = $structure['1'];
	}

	$this->safa_package_periods_model->safa_package_periods_id = false ;
	$this->safa_package_periods_model->safa_package_id = $safa_package_id;
	$package_periods_rows = $this->safa_package_periods_model->get();
	foreach ($package_periods_rows as $package_periods_row) {
		$package_periods_arr[$package_periods_row->$key] = $package_periods_row->$value;
	}
	//----------------------------------------------------------------------------------
	//----------- Package Hotels Array for Dropdown ------------------------------------
	$structure = array('erp_hotel_id', 'erp_hotel_name');
	$key = $structure['0'];
	if (isset($structure['1'])) {
		$value = $structure['1'];
	}


	$this->package_hotels_model->safa_package_id = $safa_package_id;
	$package_hotels_rows = $this->package_hotels_model->get_for_accommodation();
	foreach ($package_hotels_rows as $package_hotels_row) {
		$package_hotels_arr[$package_hotels_row->$key] = $package_hotels_row->$value;
	}
	//----------------------------------------------------------------------------------

	$screen_title = lang('edit_title');
        echo "<script type='text/javascript'>
$(document).ready(function() {
   $('input[name=smt_save]').removeAttr('disabled');
});
</script>";

           
        
}
?>

<script type='text/javascript'>
$(document).ready(function() {
   load_drag_drop();
});
</script>

<style>
.widget{width:98%;}
.darg_drop {
	width: 100%;
}

.darg_drop ul {
	margin: 0;
	padding: 0;
	padding-top: 3px;
	list-style: none;
	position: relative;
}

ul li {
	cursor: pointer;
}

.draggable {
	display: inline-block;
	background: linear-gradient(to bottom, rgb(102, 104, 108) 0%,
		rgb(72, 72, 68) 100% ) repeat scroll 0% 0% transparent;
	border: 1px;
	-moz-border-radius: 5px;
	border-radius: 5px;
	width: 92%;
}

.droppable {
	padding-top: 5px;
	padding-left: 5px;
	background: linear-gradient(to bottom, rgb(102, 104, 108) 0%,
		rgb(72, 72, 68) 100% ) repeat scroll 0% 0% transparent;
	min-height: 100px;
	border: 1px;
	-moz-border-radius: 5px;
	border-radius: 5px;
	/*
	width: 150px;
	*/
	margin: 5px;
        padding-bottom: 30px !important;
}
    
  
.droppable li {
	background: #fff;
	margin: 2px;
	-moz-border-radius: 5px;
	border-radius: 5px;
	/*
	width: 120px;
	*/
	padding-left: 3px;
	padding-right: 3px;
	z-index: 1000;
}

.draggable li {
	float: right;
	background: #fff;
	margin: 2px;
	-moz-border-radius: 5px;
	border-radius: 5px;
	/*
	width: 120px;
	*/
	padding-left: 3px;
	padding-right: 3px;
	
	z-index: 1000;
	
}

.ui-state-highlight {
    border: 0px !important;
    box-shadow: 0 0 10px #D2AB67 inset;
}

.ui-accordion .ui-accordion-content {
    overflow: visible !important;
    overflow-y: visible !important;
}

.ui-accordion .ui-accordion-content ul {
    overflow: visible !important;
    overflow-y: visible !important;
}
</style>

<div class="darg_drop">

<div class="widget">

<div class="path-container Fright">
<div class="path-name Fright"><a href="<?= site_url('ea/dashboard') ?>"><?= lang('global_system_management') ?></a>
</div>
<div class="path-arrow Fright"></div>
<div class="path-name Fright"><a
	href="<?php echo site_url('ea/passport_accommodation_rooms') ?>"><?php echo lang('passport_accommodation_rooms') ?></a></div>

<div class="path-arrow Fright"></div>
<div class="path-name Fright"><?php echo $screen_title; ?></div>
</div>
</div>

<?php echo  form_open_multipart(false, 'id="frm_passport_accommodation_rooms" ') ?>

<div class="widget">
<div class="widget-header">
<div class="widget-header-icon Fright"><span class="icos-pencil2"></span>
</div>
<div class="widget-header-title Fright"><?php echo  lang('group_passports') ?>
</div>

</div>

<div class="widget-container">
<div class="table-responsive">


<div class="row-form">
<div class="span6"><input type="hidden" name="safa_reservation_form_id"
	id="safa_reservation_form_id"
	value="<?php echo $safa_reservation_form_id;?>"></input>

<div class="row-form warning">
<div class="span4 TAL Pleft10 "><?php echo  lang('safa_trip_id') ?></div>
<div class="span8">
<?php
if(isset($safa_trip_internaltrips_safa_trip_id)) {
	 echo "<input type='hidden' name='safa_trip_id' id='safa_trip_id' value='$safa_trip_internaltrips_safa_trip_id' />";
	echo $safa_trip_name;
} else {
	echo form_dropdown("safa_trip_id", $safa_trips, set_value("safa_trip_id", $safa_trip_id), " id='safa_trip_id' class='chosen-select validate[required]'  ");
}

?>
</div>
</div>

<div class="row-form ">
<div class="span4 TAL Pleft10 "><?php echo  lang('safa_package_id') ?></div>
<div class="span8"><?php echo  form_dropdown('safa_package_id', $safa_packages, set_value('safa_package_id', $safa_package_id), " class='chosen-select  validate[required]'  id='safa_package_id'  ") ?>

</div>
</div>

<div class="row-form warning">
<div class="span4 TAL Pleft10 "><?php echo  lang('safa_package_periods_id') ?>
</div>
<div class="span8"><?php echo  form_dropdown('safa_package_periods_id', $package_periods_arr, set_value('safa_package_periods_id', $safa_package_period_id), " class='chosen-rtl chosen-select  validate[required]'  id='safa_package_periods_id'   ") ?>
<input type='hidden' name='hdn_safa_package_periods_id'
	id='hdn_safa_package_periods_id'
	value='<?php echo $safa_package_period_id; ?>' /></div>
</div>

<!--
<div class="row-form">
<div class="span4 TAL Pleft10"><?php echo  lang('erp_hotel_ids') ?></div>
<div class="span5"><?php echo form_multiselect('erp_hotel_id[]', $package_hotels_arr, set_value('erp_hotel_id', $item_hotels_select), " id='erp_hotel_id'  class='chosen-rtl chosen-select  validate[required]'   ") ?>
</div>
<?php 
if($safa_reservation_form_id!=0) {
?>
<div class="span3"> 
<a class="btn" title="<?php echo  lang('show_rooms') ?>" href="javascript:void(0);" id="lnk_show_rooms"> <?php echo  lang('show_rooms') ?> </a>
</div>
<?php }?>
</div>
--> <!-- ----------------- Groups Passports ---------------------------- -->
<?php 
if(session('ea_id')) {
?>
<div class="row-form">
<div class="span12">
<div class="span2 TAL Pleft10"><?php echo  lang('groups') ?></div>
<div class="span8"><?= form_multiselect("safa_group_id[]", $safa_groups, set_value("safa_group_id"), " id='safa_group_id' class='chosen-select chosen-rtl'  ") ?>
</div>

<div class="span2"><a href='javascript:void(0);' id="lnk_show_passports"
	class="btn" style="margin: 1px; padding: 1px; width: 77px;"><?php echo lang('show_passports')?></a>
</div>
</div>
</div>
<div class="span12" style="line-height: 16px;">
<div class="" id="ul_draggable_groups_passports" ></div>
</div>
<!-- ------------------------------------------------------------------- -->
<?php 
}
?>


</div>








<div class="span6">
<div class="">
<div class="span12"><script>hotels_counter = 0</script>

<div id="dv_hotels"><!-- 
<table class="" width="100%">
	<thead>

		<tr>
			<td colspan="4" style="text-align: center;"><?= lang('erp_hotel_ids') ?>
			</td>
		</tr>

		<tr>
			<td><?php echo  lang('erp_hotel_id') ?></td>

			<td><?php echo  lang('from_date') ?></td>

			<td><?php echo  lang('to_date') ?></td>

			<td><a class="btn " title="<?php echo  lang('global_add') ?>"
				href="javascript:void(0)"
				onclick="add_hotels('N' + hotels_counter++); "> <?php echo  lang('global_add') ?>
			</a></td>
		</tr>
	</thead>

	<tbody class="tbl_hotels" id='tbl_hotels' style="line-height:16px;">

	

	</tbody>
</table>
 -->



<table class="" width="100%">
	<thead>


		<tr>
			<td class="TAC"><?php echo  lang('erp_hotel_id') ?></td>

			<td class="TAC span2"><?php echo  lang('from_date') ?></td>

			<td class="TAC span2"><?php echo  lang('to_date') ?></td>

		</tr>
	</thead>

	<tbody class="tbl_hotels" id='tbl_hotels' style="line-height: 16px;">
	<?php

	$hotels_counter=0;
	$erp_hotel_id=0;

	if(isset($item_hotels_select_arr)) {
		foreach ($item_hotels_select_arr as $erp_hotel_id) {

			

			//------------------------------------- Dates ------------------------------------
			$from_date='';
			$to_date='';
			$erp_hotel_name='';
			$erp_city_name='';
			
			$this->safa_passport_accommodation_rooms_model->safa_reservation_form_id = $safa_reservation_form_id;
			$this->safa_passport_accommodation_rooms_model->erp_hotel_id = $erp_hotel_id;
			$safa_passport_accommodation_rooms_rows =  $this->safa_passport_accommodation_rooms_model->get();
			
			if(count($safa_passport_accommodation_rooms_rows)>0) {
				$erp_hotel_name = $safa_passport_accommodation_rooms_rows[0]->erp_hotel_name;
				$erp_city_name = $safa_passport_accommodation_rooms_rows[0]->erp_city_name;
				
				$from_date=$safa_passport_accommodation_rooms_rows[0]->from_date;
				$to_date=$safa_passport_accommodation_rooms_rows[0]->to_date;
			}
			//----------------------------------------------------------------------------------

	
	
	
			echo "<tr>
			<td><input type='hidden' name='erp_hotel_id[$hotels_counter]' id='erp_hotel_id$hotels_counter' value='$erp_hotel_id' />
			$erp_hotel_name ($erp_city_name)</td>
			<td>
			";
			
			if(session('ea_id')) {
			
			echo "<input type='text' name='from_date[$hotels_counter]' id='from_date$hotels_counter' value='$from_date' class='validate[required] from_date$hotels_counter'   onchange='get_to_date_for_hotel(this.value,$hotels_counter)' />
            <script>
		    $('.from_date$hotels_counter').datepicker({
		        dateFormat: 'yy-mm-dd',
		        controlType: 'select',
		        timeFormat: 'HH:mm',
		       onClose: function(selectedDate) {\$('.to_date$hotels_counter').datepicker('option', 'minDate', selectedDate)}
		    });
			</script>";
			} else {
				echo "<input type='hidden' name='from_date[$hotels_counter]' id='from_date$hotels_counter' value='$from_date' />";
				echo $from_date;
			}
			
			echo "
			</td>
			
			<td>
			";
			
			
			if(session('ea_id')) {
			
			echo "<input type='text' name='to_date[$hotels_counter]' id='to_date$hotels_counter' value='$to_date' class='validate[required] to_date$hotels_counter'/>
            
		    <script>
		    $('.to_date$hotels_counter').datepicker({
		        dateFormat: 'yy-mm-dd',
		        controlType: 'select',
		        timeFormat: 'HH:mm',
		       onClose: function(selectedDate) {\$('.to_date$hotels_counter').datepicker('option', 'maxDate', selectedDate)}
		    });
			</script>
			";
			} else {
				echo "<input type='hidden' name='to_date[$hotels_counter]' id='to_date$hotels_counter' value='$to_date' />";
				echo $to_date;
			}
			
		    echo "</td>
			</tr>";

			$hotels_counter++;
		}
	}
	?>


	</tbody>
</table>
</div>


<select name="select_hdn_erp_hotelroomsize_id" id="select_hdn_erp_hotelroomsize_id" style="display: none; ">
<? foreach($erp_hotelroomsizes as $key => $value) { ?>
<option value="<?= $key ?>"><?= $value ?></option>
<? } ?>
</select>


<?php 
if(session('ea_id')) {
?>
<div class="span12">
<div class="row-form">
<div class="span12">
<?php echo form_radio('rooms_availability_setting_allowing', 'allow', true) ?> <?php echo lang('rooms_availability_setting_allow') ?></div>
</div>
<div class="row-form">
<div class="span12">
<?php echo form_radio('rooms_availability_setting_allowing', 'disallow', '', ' id="rooms_availability_setting_allowing"') ?> <?php echo lang('rooms_availability_setting_disallow') ?>
</div>
</div>
</div>
<?php 
}
?>

</div>
</div>

<?php 
if(session('ea_id')) {
?>
<div class="span12" id="total_rooms_for_storage"
	style="margin-top: 5px;"><script>accommodation_counter = 0</script>
<table class="" width="100%">
	<thead>



		<tr>
			<td class="TAC warning"><?php echo  lang('erp_hotel_id') ?></td>

			<td class="TAC warning"><?php echo  lang('erp_hotelroomsize_id') ?></td>

			<td class="TAC warning"><?php echo  lang('count') ?></td>

			<td class="TAC warning"><?php echo  lang('group_passports') ?></td>

			<td class="TAC span1 warning"><a class="btn "
				title="<?php echo  lang('global_add') ?>" href="javascript:void(0)"
				onclick="add_accommodation('N' + accommodation_counter++); "> <?php echo  lang('global_add') ?>
			</a></td>
		</tr>
	</thead>

	<tbody class="tbl_accommodation" id='tbl_accommodation'
		style="line-height: 16px;">
		<!-- 
                <tr>
                <td>
                <ul class='droppable'></ul>
                </td>
                </tr>
                 -->


	</tbody>
</table>
</div>

<a style="margin-left: 21px; display: block; margin-bottom: 30px;"
	class='' title='<?php echo  lang('add_rooms_to_table') ?>' href='javascript:void(0);'
	id='lnk_add_all_rooms_rows_to_table'
	onclick="add_all_rooms_rows_to_table()"> <span
	class="glyphicons circle_plus"><i></i></span></a>
	
<?php }?>	
	
	
</div>


</div>


	<?php
	if($safa_reservation_form_id!=0) {
		?> <!--<div class="widget TAC" style="margin-right: 0px;">
<a class="btn" title="<?php echo  lang('add_rooms_to_table') ?>" href="javascript:void(0);" id="lnk_add_rooms_to_table"> <?php echo  lang('add_rooms_to_table') ?></a>
</div>
--><?php 
	}
	?></div>


<div id="rooms_for_storage"><?php 
if($safa_reservation_form_id!=0) {

	if(isset($item_hotels)) {
		if(check_array($item_hotels)) {

			$hotels_counter=0;
			foreach($item_hotels as $item_hotel) {

				//if($hotels_counter==0) {


				echo "<div class='dv_distribution_rooms_groups_passports' id='dv_distribution_rooms_groups_passports$item_hotel->erp_hotel_id'>";
					
				echo " <div class='widget TAC' id='dv_hotel_title_$item_hotel->erp_hotel_id' style='background-color: rgb(233, 226, 212);margin-top: 5px;padding: 2px;margin-right: 0px;'>$item_hotel->erp_hotel_name</div>";
					
				echo " <input type='hidden' name='hdn_erp_hotel_id[]' id='hdn_erp_hotel_id' value='$item_hotel->erp_hotel_id' />";


				if(check_array($erp_hotelroomsizes_rows)) {

					$erp_hotelroomsize_dv_width = (100/ count($erp_hotelroomsizes_rows) )-2 ;

					foreach($erp_hotelroomsizes_rows as $erp_hotelroomsizes_row) {

						$erp_hotelroomsize_id = $erp_hotelroomsizes_row->erp_hotelroomsize_id;
						$erp_hotelroomsize_name = $erp_hotelroomsizes_row->erp_hotelroomsize_name;

							
							
						$this->safa_passport_accommodation_rooms_model->safa_reservation_form_id = $safa_reservation_form_id;
						$this->safa_passport_accommodation_rooms_model->erp_hotel_id = $item_hotel->erp_hotel_id;
						$item_passport_accommodation_rooms = $this->safa_passport_accommodation_rooms_model->get();


						//$dv_style = " style='width: $erp_hotelroomsize_dv_width%; float: left; border: solid 1px; border-color:#595B5B; -moz-border-radius: 5px; border-radius: 5px; background:#F1F1F1; text-align:center ' ";
						$dv_style = " style='float: left; border: solid 1px; border-color:#595B5B; -moz-border-radius: 5px; border-radius: 5px; background:#F3EDCD; text-align:center ' ";
						//							if(count($item_passport_accommodation_rooms)>0) {
						//							echo "<div $dv_style id='dv_table_erp_hotelroomsize$erp_hotelroomsize_id".'_'."$item_hotel->erp_hotel_id' class='dv_table_erp_hotelroomsize$erp_hotelroomsize_id' >".$erp_hotelroomsize_name."<br/>";
						//							}
						//echo " <input type='hidden' name='hdn_safa_group_passport_ids_group[]' id='hdn_safa_group_passport_ids_group' value='$item_passport_accommodation_room->safa_passport_accommodation_room_id' />";
							
							
						$item_passport_accommodation_rooms_counter=0;
							
						if (ensure($item_passport_accommodation_rooms)) {
							foreach ($item_passport_accommodation_rooms as $item_passport_accommodation_room) {

								if($erp_hotelroomsize_id == $item_passport_accommodation_room->erp_hotelroomsize_id) {


									if($item_passport_accommodation_rooms_counter==0) {
										echo "<div $dv_style id='dv_table_erp_hotelroomsize$item_hotel->erp_hotel_id".'_'."$erp_hotelroomsize_id' class='dv_table_erp_hotelroomsize$item_hotel->erp_hotel_id' >".$erp_hotelroomsize_name."<br/>";
									}

									echo " <ul class='droppable' style='width: 98%; margin:2px; z-index:0px; ' alt='$item_passport_accommodation_room->safa_passport_accommodation_room_id' >";
									
									if(session('ea_id')){
									echo "<a href='javascript:void(0);'  class='lnk_li_delete_safa_passport_accommodation_rooms' id='$item_passport_accommodation_room->safa_passport_accommodation_room_id' style='float:left;'><span class='icon-remove' style='margin:3px;color:white;'></span></a>";
									}
									
									echo "
									<input type='hidden' name='hdn_room_availability$item_passport_accommodation_room->safa_passport_accommodation_room_id' id='hdn_room_availability$item_passport_accommodation_room->safa_passport_accommodation_room_id' value='$item_passport_accommodation_room->erp_hotels_availability_room_detail_id' />
									<a href='javascript:void(0);' onclick='room_availability(\"hdn_room_availability$item_passport_accommodation_room->safa_passport_accommodation_room_id\",$item_hotel->erp_hotel_id,$erp_hotelroomsize_id);' class='lnk_room_availability' id='$item_passport_accommodation_room->safa_passport_accommodation_room_id' style='float:right; color:white; margin:1px;' >".lang('room_number')."</a> <span id='spn_hdn_room_availability$item_passport_accommodation_room->safa_passport_accommodation_room_id'  name='spn_hdn_room_availability$item_passport_accommodation_room->safa_passport_accommodation_room_id' style='margin:3px;color:white;'> $item_passport_accommodation_room->availability_room_number</span> <br/>";
									
									echo " <input type='hidden' name='hdn_erp_hotelroomsize_ids[$item_passport_accommodation_room->safa_passport_accommodation_room_id]' id='hdn_erp_hotelroomsize_ids$item_passport_accommodation_room->safa_passport_accommodation_room_id' value='$item_passport_accommodation_room->safa_passport_accommodation_room_id' />";


									$this->safa_group_passport_accommodation_model->safa_passport_accommodation_room_id = $item_passport_accommodation_room->safa_passport_accommodation_room_id;
									$item_group_passport_accommodations = $this->safa_group_passport_accommodation_model->get();

									$safa_group_passport_ids_str='';
									$safa_group_passport_ids_counter=0;

									foreach ($item_group_passport_accommodations as $item_group_passport_accommodation) {

										$safa_group_passport_id = $item_group_passport_accommodation->safa_group_passport_id;
											
										if($safa_group_passport_ids_counter==0) {
											$safa_group_passport_ids_str= $safa_group_passport_id;
										} else {
											$safa_group_passport_ids_str= $safa_group_passport_ids_str.','.$safa_group_passport_id;
										}
										$safa_group_passport_ids_counter++;
											
										//--------------- Full Name -----------------------------
										if (name() == 'name_ar') {
											$item_group_passport_accommodation->first_fourth_name_ar = trim($item_group_passport_accommodation->first_fourth_name_ar);
											if(!empty($item_group_passport_accommodation->first_fourth_name_ar)) {
												$name_by_lang = $item_group_passport_accommodation->first_fourth_name_ar;
											} else {
												$name_by_lang = $item_group_passport_accommodation->first_fourth_name_la;
											}
										} else {
											$item_group_passport_accommodation->first_fourth_name_la = trim($item_group_passport_accommodation->first_fourth_name_la);
											if(!empty($item_group_passport_accommodation->first_fourth_name_la)) {
												$name_by_lang = $item_group_passport_accommodation->first_fourth_name_la;
											} else {
												$name_by_lang = $item_group_passport_accommodation->first_fourth_name_ar;
											}
										}
										//-----------------------------------------------------


										echo "<li> $safa_group_passport_id - $name_by_lang
										<input type='hidden' name='hdn_safa_group_passport_ids[]' id='hdn_safa_group_passport_ids' value='$safa_group_passport_id' />
										<input type='hidden' name='hdn_safa_group_passport_names[]' id='hdn_safa_group_passport_names'  value='$name_by_lang' />
										
										<!-- <a href='javascript:void(0);' onclick=''><span class='icon-trash'></span></a> -->
            
										</li>";

									}

									echo " <input type='hidden' name='hdn_safa_group_passport_ids_group[$item_passport_accommodation_room->safa_passport_accommodation_room_id-$item_hotel->erp_hotel_id]' id='hdn_safa_group_passport_ids_group$item_passport_accommodation_room->safa_passport_accommodation_room_id' value='$safa_group_passport_ids_str' />";

									echo "</ul>";


									$item_passport_accommodation_rooms_counter++;
								}
							}
						}

						if($item_passport_accommodation_rooms_counter > 0) {
							echo '</div>';
						}
					}
				}
					
				echo "</div> <br/><br/>";

				//}

				$hotels_counter++;

			}
		}
	}
}
?></div>

</div>

<div class="widget TAC" style="margin-right: 0px;"><!-- <a class="btn" title="<?php echo  lang('apply') ?>" href="javascript:void(0);" id="lnk_apply"> <?php echo  lang('apply') ?></a>-->
    <input type="submit" class="btn" name="smt_save" disabled="disabled"
	value="<?php echo lang('save') ?>"
	onclick="localStorage.removeItem('rooms_for_storage_<?php echo $safa_reservation_form_id;?>');" />

<!--<a href='javascript:void();' id='load_storage' class="btn"><?php echo lang('load_storage'); ?></a>	-->

<?php if($safa_reservation_form_id==0) {?> 
<a href='javascript:void(0);' id='lnk_copy_first_hotel_to_others' class="btn" style="display: none; "><?php echo lang('copy_first_hotel_to_others'); ?></a>
<?php }?></div>


</div>

<?php echo  form_close() ?></div>

<script>

<?php 
if($safa_reservation_form_id!=0) {
?>
//$("#safa_trip_id").attr('disabled', true).trigger("chosen:updated");
$("#safa_package_id").attr('disabled', true).trigger("chosen:updated");
$("#safa_package_periods_id").attr('disabled', true).trigger("chosen:updated");
<?php }?>
</script>

<script>
function add_all_rooms_rows_to_table()
{
	$('.tbl_accommodation tr').each(function () {
        	add_rooms_to_table($(this).attr('rel'));
    });
}

function get_from_to_date_for_hotel(for_date, erp_hotel_id)
{
	var hdn_hotels_inputs = $('#dv_hotels').find('input');
	var hdn_hotels_inputs_length = $('#dv_hotels').find('input').length;

	var current_hotel_id=0;
	var current_for_date='';
	
	for (var h=0; h<hdn_hotels_inputs_length; h++) {

		if(hdn_hotels_inputs[h].name.startsWith('erp_hotel_id')  ) {
			var current_hotel_id =hdn_hotels_inputs[h].value;
		}
		
		if(hdn_hotels_inputs[h].name.startsWith(for_date)) {
			if(current_hotel_id==erp_hotel_id) {
				current_for_date = hdn_hotels_inputs[h].value;
			}
		}
		
	}

	return current_for_date;
}
</script>

<script>

var counter_for_new_rooms=0;
var str_counter_for_new_rooms='';  

function add_rooms_to_table(id) {

	$('input[name=smt_save]').removeAttr('disabled');
	//---------------------------------- Check For Hotel Rooms Availbility ----------------------------------------
  	var safa_package_id = $("#safa_package_id").val();
  	var erp_hotel_id =$("#erp_hotel_id"+id).val();
  	var erp_hotelroomsize_id =$("#erp_hotelroomsize_id"+id).val();
  	var rooms_count = $("#rooms_count"+id).val();
  	var erp_hotel_from_date = get_from_to_date_for_hotel('from_date',erp_hotel_id);
  	var erp_hotel_to_date = get_from_to_date_for_hotel('to_date',erp_hotel_id);

	//alert (erp_hotel_from_date +'  '+ erp_hotel_to_date);
	
	if(erp_hotel_id!='') {
    	var dataString = 'safa_package_id='+ safa_package_id+'& rooms_count='+ rooms_count+'&erp_hotel_id='+ erp_hotel_id+'&erp_hotelroomsize_id='+ erp_hotelroomsize_id+'&erp_hotel_from_date='+ erp_hotel_from_date+'&erp_hotel_to_date='+ erp_hotel_to_date;
	} else {
		dataString = 'erp_hotelroomsize_id_posted='+erp_hotelroomsize_id+'&'+$("#frm_passport_accommodation_rooms").serialize();
	}
	
	var available_rooms_count=0;
	var hotel_availability_arr = new Array();
  	$.ajax
  	({
	    type: 'POST',
	    url: '<?php echo base_url().'ea/passport_accommodation_rooms/get_available_rooms_ajax'; ?>',
	    data: dataString,
	    cache: false,
	    //This to read js var in ajax
	    async: false,
	    success: function(data)
	    {
	        //alert(data);
	        var data_arr = JSON.parse(data);
	        hotel_availability_arr =  data_arr[0]['hotel_availability_arr'];
	        hotel_availability_arr = $.map(hotel_availability_arr, function (value, key) { return value; });

	        erp_hotels_availability_room_details_arr =  data_arr[0]['erp_hotels_availability_room_details_arr'];
	        erp_hotels_availability_room_details_arr = $.map(erp_hotels_availability_room_details_arr, function (value, key) { return value; });
	          
	        //available_rooms_count = data_arr[0]['available_rooms_count'];
	        //alert(hotel_availability_arr);
	        return true;
	    }
  	});

  	
  	
  	
    for (var a=0; a<hotel_availability_arr.length ; a++ ) {
    	available_rooms_count = hotel_availability_arr[a];

    	//alert(parseInt(available_rooms_count)+' '+parseInt(rooms_count));
    	
    	
	  	if(parseInt(available_rooms_count)<parseInt(rooms_count)) {
			
			//alert(available_rooms_count);
			if ($('input:radio[name=rooms_availability_setting_allowing]:checked').val()=='disallow') {
				alert('<?php echo lang('available_rooms_in_this_periods_less_than_required_rooms');?>');
				return false;
			} else {
				if(!confirm('<?php echo lang('available_rooms_in_this_periods_less_than_required_rooms_are_continue');?>')) {
					return false;
				}
			}
	  	}
    }
  	
	//-------------------------------------------------------------------------------------------------------------
	  	
	$("#lnk_copy_first_hotel_to_others").css( "display", "");
	  	

			
	var hdn_hotels_inputs = $('#dv_hotels').find('input');
	var hdn_hotels_inputs_length = $('#dv_hotels').find('input').length;

	var availability_counter=0;
	for (var h=0; h<hdn_hotels_inputs_length; h++) {
		if(hdn_hotels_inputs[h].name.startsWith('erp_hotel_id')  ) {
			var current_hotel_id =hdn_hotels_inputs[h].value;
		

	if($("#erp_hotel_id"+id).val()!='') {
		if($("#erp_hotel_id"+id).val()!= current_hotel_id) {
			continue;
		}
	}  		
	
	var rooms_count = $("#rooms_count"+id).val();
	if(rooms_count=='') {
		alert('<?php echo lang('please_enter_rooms_count');?>');
		return false;
	}

	var erp_hotelroomsize_id =$("#erp_hotelroomsize_id"+id).val();
	var erp_hotelroomsize_name =$("#erp_hotelroomsize_id"+id+" :selected").text();


	var erp_hotels_availability_room_details_arr_for_hotel = new Array();
	if(erp_hotels_availability_room_details_arr[availability_counter]!=null) {
		var erp_hotels_availability_room_details_arr_for_hotel = $.map(erp_hotels_availability_room_details_arr[availability_counter], function (value, key) { return key+'-'+value; })
	}
	availability_counter++;

	
	for(var i=0; i<rooms_count; i++) {
		
		counter_for_new_rooms++;
		str_counter_for_new_rooms='_new_room'+current_hotel_id+'_'+counter_for_new_rooms;  
		
		//alert($("#dv_table_erp_hotelroomsize"+erp_hotelroomsize_id).length);
		//$("#dv_table_erp_hotelroomsize"+erp_hotelroomsize_id).innerHTML = $("#dv_table_erp_hotelroomsize"+erp_hotelroomsize_id).innerHTML + "<ul id='0' class='droppable ui-sortable ui-droppable'></ul>";
		if(i==0 && $("#dv_table_erp_hotelroomsize"+current_hotel_id+'_'+erp_hotelroomsize_id).length==0) {
			//$("#dv_distribution_rooms_groups_passports").append("<div id='dv_table_erp_hotelroomsize"+erp_hotelroomsize_id+"' style='float: left; border: solid 1px; border-color:#595B5B; -moz-b…; border-radius: 5px; background:#F1F1F1; text-align:center'> "+erp_hotelroomsize_name+"<br/> </div>");
			$("#dv_distribution_rooms_groups_passports"+current_hotel_id).append("<div id='dv_table_erp_hotelroomsize"+current_hotel_id+'_'+erp_hotelroomsize_id+"' class='dv_table_erp_hotelroomsize"+current_hotel_id+'_'+erp_hotelroomsize_id+"' style='float: left; border: solid 1px; border-color:#595B5B; -moz-b…; border-radius: 5px; background:#F1F1F1; text-align:center'> "+erp_hotelroomsize_name+"<br/> </div>");
		}


		var hdn_room_availability_value =  ''
		var spn_hdn_room_availability_value = '';
		if(erp_hotels_availability_room_details_arr_for_hotel[i]!=null) {                                                                   		
		var erp_hotels_availability_room_detail = erp_hotels_availability_room_details_arr_for_hotel[i];
		var erp_hotels_availability_room_detail_arr = erp_hotels_availability_room_detail.split("-");
		var hdn_room_availability_value =  erp_hotels_availability_room_detail_arr[0];
		var spn_hdn_room_availability_value = erp_hotels_availability_room_detail_arr[1];
		}
		
		var ul_html = "<ul id='0' class='droppable' style='width:150px' alt='"+counter_for_new_rooms+"'>";

		ul_html = ul_html + "<a href='javascript:void(0);'  class='lnk_li_delete_safa_passport_accommodation_rooms' id='0' style='float:left;'><span class='icon-remove' ></span></a>";


		ul_html = ul_html + "<input type='hidden' name='hdn_room_availability"+str_counter_for_new_rooms+"' id='hdn_room_availability"+str_counter_for_new_rooms+"' value='"+hdn_room_availability_value+"' />";
		ul_html = ul_html + " <a href='javascript:void(0);' onclick='room_availability(\"hdn_room_availability"+str_counter_for_new_rooms+"\","+current_hotel_id+","+erp_hotelroomsize_id+");' class='lnk_room_availability' id='' style='float:right; color:white; margin:1px;' ><?php echo lang('room_number'); ?></a> <span id='spn_hdn_room_availability"+str_counter_for_new_rooms+"' name='spn_hdn_room_availability"+str_counter_for_new_rooms+"' style='margin:3px;color:white;'> "+spn_hdn_room_availability_value+" </span> <br/>";
		
	  //By Gouda
	    //var hdn_inputs_length = $("#"+id).find('input').length;
	    //var hdn_inputs = $("#"+id).find('input');
	    var hdn_inputs_length = $("#"+id).find('input[id=hdn_safa_group_passport_ids]').length;
	    var hdn_inputs = $("#"+id).find('input[id=hdn_safa_group_passport_ids]');

	    var hdn_inputs_safa_group_passport_names = $("#"+id).find('input[id=hdn_safa_group_passport_names]');
	    
		var safa_group_passport_ids='';
		for(var j=0; j<hdn_inputs_length; j++) {

			if(j<erp_hotelroomsize_id*(i+1) && j >= erp_hotelroomsize_id*(i)) {
				if(!hdn_inputs[i].name.startsWith('hdn_safa_group_passport_ids_group')) {
					if(j==0) {
						safa_group_passport_ids= hdn_inputs[j].value;
					} else {
						safa_group_passport_ids= safa_group_passport_ids+','+ hdn_inputs[j].value;
					}
				
					ul_html=ul_html+"<li> "+ hdn_inputs[j].value +" - "+ hdn_inputs_safa_group_passport_names[j].value +
					"<input type='hidden' name='hdn_safa_group_passport_ids[]' id='hdn_safa_group_passport_ids' value='"+hdn_inputs[j].value+"' />"+
					"</li>";
				}

			}
		}
		
		ul_html = ul_html + " <input type='hidden' name='new_hdn_erp_hotelroomsize_ids["+str_counter_for_new_rooms+"]' id='new_hdn_erp_hotelroomsize_ids"+str_counter_for_new_rooms+"' value='"+erp_hotelroomsize_id+"' /> <input type='hidden' name='hdn_safa_group_passport_ids_group["+str_counter_for_new_rooms+"]' id='hdn_safa_group_passport_ids_group'  value='"+safa_group_passport_ids+"' />";

		ul_html= ul_html +" </ul>";
		
		$("#dv_table_erp_hotelroomsize"+current_hotel_id+'_'+erp_hotelroomsize_id).append(ul_html);
		//$(".dv_table_erp_hotelroomsize"+erp_hotelroomsize_id).append(ul_html);
		
	}
		}
	}
        
	delete_accommodation(id);
	load_drag_drop();
	

	//------------------ By Gouda, to delete new ul rooms -----------------------
	$('.lnk_li_delete_safa_passport_accommodation_rooms').click(function(event){  
		//if(confirm('<?= lang('global_are_you_sure_you_want_to_delete')?>')) {
			
			var divparent = $(this).parent().parent();
			$(this).closest("ul").remove();	
			var safa_passport_accommodation_room_id = $(this).attr('id');
			//alert(safa_passport_accommodation_room_id);
			var hidden_input = '<input type="hidden" name="safa_passport_accommodation_rooms_remove[]" value="' + safa_passport_accommodation_room_id + '" />';
	        $('#tbl_accommodation').append(hidden_input);

	        //alert($(divparent).attr('id'));
	        if($(divparent).find('ul').length==0) {
	        	$(divparent).remove();
	        }
		//}
	});
	//---------------------------------------------------------------------------
}
</script>

<script>
function add_hotels(id) {

	var safa_package_id = $("#safa_package_id").val();
	var safa_trip_id = $("#safa_trip_id").val();
	var safa_package_periods_id = $("#safa_package_periods_id").val();
	
	if(safa_package_id==0) {
		alert('<?php echo  lang('please_select_package_first') ?>');
		return false;
	}

    var new_row = [
            "<tr rel=\"" + id + "\">",

            "    <td>",
            "       <select name=\"erp_hotel_id[" + id + "]\" class=\"chosen-select chosen-rtl input-full\" id=\"erp_hotel_id" + id + "\" tabindex=\"4\" style=\"width:150px;\" >",
            <? foreach($package_hotels_arr as $key => $value): ?>
            "           <option value=\"<?= $key ?>\"><?= $value ?></option>",
            <? endforeach ?>
            "    </td>",
            
            '     <td><?php echo  form_input("from_date[' + id + ']", FALSE, " class=\"validate[required] from_date' + id + '\" id=\"from_date' + id + '\" style=\"width:75px\" onchange=\"setDatesValues(\'' + id + '\', \'from_date\') \" placeholder=\'".lang('entry_date').
            "\' ") ?>',
		    "<script>",
		    '$(".from_date' + id + '").datepicker({',
		    "    dateFormat: 'yy-mm-dd',",
		    "    controlType: 'select',",
		    "    timeFormat: 'HH:mm',",
		    "   onClose: function(selectedDate) {$('.to_date" + id + "').datepicker('option', 'minDate', selectedDate)}",
		    "});",
			"<\/script>",
			"    </td>",
			
			"    <td>",
		    '        <?php echo  form_input("to_date[' + id + ']", FALSE, "class=\"validate[required] to_date' + id + ' \"   id=\"to_date' + id + '\"  style=\"width:75px\"  onchange=\"setDatesValues(\'' + id + '\', \'to_date\') \" placeholder=\'".lang('exit_date').
		            "\' ") ?>',
		    "<script>",
		    '$(".to_date' + id + '").datepicker({',
		    "    dateFormat: 'yy-mm-dd',",
		    "    controlType: 'select',",
		    "    timeFormat: 'HH:mm',",
		    "   onClose: function(selectedDate) {$('.from_date" + id + "').datepicker('option', 'maxDate', selectedDate)}",
		    "});",
			"<\/script>",
		    "    </td>",
           
            
            "    <td  style=\"width:20px;\" >",
            "        <a href=\"javascript:void(0)\" onclick=\"delete_hotels('" + id + "')\"><span class=\"icon-trash\"></span></a>",
            "    </td>",
            "</tr>"


            ].join("\n");

    
        
    $('#tbl_hotels').append(new_row);

    $.get("<?php echo base_url().'ea/passport_accommodation_rooms/get_hotels_by_package_for_table_ajax'; ?>/"+safa_package_id+'/'+safa_trip_id+'/'+safa_package_periods_id, function(data) {
		

		var data_arr = JSON.parse(data);
	    
		$("#erp_hotel_id" + id).html(data_arr[0]['form_dropdown_options']);

		if(id=='N0') {
	    	if(safa_trip_id!=null) {
	    		if(safa_package_periods_id!=null) {
		$("#from_date" + id).val(data_arr[0]['from_date']);
	    $("#to_date" + id).val(data_arr[0]['to_date']);
	    		}
	    	}
		}
		
	    
	});

    if(id=='N0') {
    	if(safa_trip_id!=null) {
        	
//    		if(safa_package_periods_id!=null) {
//		    	$.get("<?php echo base_url().'ea/passport_accommodation_rooms/get_from_to_date_first_hotel_by_trip_and_package_period_ajax'; ?>/"+safa_trip_id+'/'+safa_package_periods_id, function(data) {
//
//		    		var dates_arr = JSON.parse(data);
//		    		$("#from_date" + id).html(dates_arr[0]['from_date']);
//		    		$("#to_date" + id).html(dates_arr[0]['to_date']);
//		    	});
//			} else {
				
		    	$.get("<?php echo base_url().'ea/passport_accommodation_rooms/get_from_date_first_hotel_by_trip_ajax'; ?>/"+safa_trip_id, function(data) {
		    		$("#from_date" + id).val(data);
		    	});
			//}
    	}
    }
    
    load_drag_drop();

}

function delete_hotels(id, database) 
{
    if(typeof database == 'undefined')
    {
        $('.tbl_hotels').find('tr[rel="' + id + '"]').remove();
    }
    else
    {
        $('.tbl_hotels').find('tr[rel=' + id + ']').remove();
        var hidden_input = '<input type="hidden" name="hotels_remove[]" value="' + id + '" />';
        $('#tbl_hotels').append(hidden_input);
    }
    hotels_counter--;
}
</script>

<script>
function add_accommodation(id) {

	

	var safa_package_id = $("#safa_package_id").val();
	var safa_trip_id = $("#safa_trip_id").val();
	var safa_package_periods_id = $("#safa_package_periods_id").val();
	
	if(safa_package_id==0) {
		alert('<?php echo  lang('please_select_package_first') ?>');
		return false;
	}    
	
	if($('.tbl_hotels tr').length==0) {
		alert('<?php echo  lang('please_select_package_has_hotels') ?>');
		return false;
	}

	var hotels_dates=true;
	var hdn_hotels_inputs = $('#dv_hotels').find('input');
	var hdn_hotels_inputs_length = $('#dv_hotels').find('input').length;
	for (var h=0; h<hdn_hotels_inputs_length; h++) {
		if(hdn_hotels_inputs[h].name.startsWith('from_date')  ) {
			var current_hotel_from_date =hdn_hotels_inputs[h].value;
			if(current_hotel_from_date=='') {
				hotels_dates=false;
			}
		}
	}

	for (var h=0; h<hdn_hotels_inputs_length; h++) {
		if(hdn_hotels_inputs[h].name.startsWith('to_date')  ) {
			var current_hotel_to_date =hdn_hotels_inputs[h].value;
			if(current_hotel_to_date=='') {
				hotels_dates=false;
			}
		}
	}

	if(!hotels_dates) {
		alert('<?php echo  lang('please_enter_hotels_from_to_dates') ?>');
		return false;
	}

	
	
    var new_row = [
            "<tr rel=\"" + id + "\">",

            "    <td>",
            "       <select name=\"erp_hotel_id[" + id + "]\" class=\"erp_hotel_id\" id=\"erp_hotel_id" + id + "\" tabindex=\"4\" style=\"width:150px;\" >",
            <? foreach($package_hotels_arr as $key => $value): ?>
            "           <option value=\"<?= $key ?>\"><?= $value ?></option>",
            <? endforeach ?>
            "    </td>",

            
            "    <td class=\"TAC \">",
            "       <select name=\"erp_hotelroomsize_id[" + id + "]\" class=\"chosen-select chosen-rtl input-full\" id=\"erp_hotelroomsize_id" + id + "\" tabindex=\"4\" style=\"width:80px;\" >",
            <? foreach($erp_hotelroomsizes as $key => $value): ?>
            "           <option value=\"<?= $key ?>\"><?= $value ?></option>",
            <? endforeach ?>
            "    </td>",
            
            '     <td class=\"TAC \"><?php echo  form_input("rooms_count[' + id + ']", FALSE, " class=\'validate[required]\' id=\'rooms_count'+id+'\' style=\'width:50px;\' ") ?>',
            "    </td>",

           
            "<td style=\"width:250px;\">  <ul class='droppable' id='" + id +"' alt='"+id+"' > <input type='hidden' name='hdn_safa_group_passport_ids_group[" + id + "]' id='hdn_safa_group_passport_ids_group'  value='' />  </ul> </td>",
            
            "    <td class=\"TAC \" >",

            '<?php //if($safa_reservation_form_id!=0) {?>',
            " <a style=\"margin-left: 21px;display: block;margin-bottom: 30px;\" class='' title='<?php echo  lang('add_rooms_to_table') ?>' href='javascript:void(0);' id='lnk_add_rooms_to_table' onclick=\"add_rooms_to_table('"+id+"')\" > <span class=\"glyphicons circle_plus\"><i></i></span></a>",
            '<?php //}?>',
            
            "        <a href=\"javascript:void(0)\" onclick=\"delete_accommodation('" + id + "')\"><span class=\"icon-trash icon-2x\"><i></i></span></a>",
            "    </td>",
            "</tr>"


            ].join("\n");

    
        
    $('#tbl_accommodation').append(new_row);

	$.get("<?php echo base_url().'ea/passport_accommodation_rooms/get_hotels_by_package_for_table_ajax'; ?>/"+safa_package_id+'/'+safa_trip_id+'/'+safa_package_periods_id, function(data) {
		var data_arr = JSON.parse(data);
		$("#erp_hotel_id" + id).html(data_arr[0]['form_dropdown_options']);
		if(id=='N0') {
	    	if(safa_trip_id!=null) {
	    		if(safa_package_periods_id!=null) {
		$("#from_date" + id).val(data_arr[0]['from_date']);
	    $("#to_date" + id).val(data_arr[0]['to_date']);
	    		}
	    	}
		}
	});

    
    load_drag_drop();

}

function delete_accommodation(id, database) 
{
    if(typeof database == 'undefined')
    {
        $('.tbl_accommodation').find('tr[rel="' + id + '"]').remove();
    }
    else
    {
        $('.tbl_accommodation').find('tr[rel=' + id + ']').remove();
        var hidden_input = '<input type="hidden" name="accommodation_remove[]" value="' + id + '" />';
        $('#tbl_accommodation').append(hidden_input);
    }
}
</script>


<script type="text/javascript">
$('#safa_package_id').change(function(){ 

	$("#lnk_copy_first_hotel_to_others").css( "display", "none");
	
	$("#tbl_accommodation tr").remove();
	
	$("#tbl_hotels tr").remove();
	hotels_counter = 0

	
    var safa_package_id=$(this).val();
	var safa_trip_id=$("#safa_trip_id").val();
	var safa_package_periods_id=$("#safa_package_periods_id").val();
    
	var dataString = 'safa_package_id='+ safa_package_id;


	
    
//    $.ajax
//    ({
//    	type: 'POST',
//    	url: '<?php echo base_url().'ea/passport_accommodation_rooms/get_periods_by_package'; ?>',
//    	data: dataString,
//    	cache: false,
//    	success: function(html)
//    	{
//        	//alert(html);
//    		$("#safa_package_periods_id").html(html);
//    		$("#safa_package_periods_id").trigger("chosen:updated");
//
//    	}
//    });
//
//
	  var hotels_table_dataString = 'safa_package_id='+ safa_package_id+'& safa_trip_id='+ safa_trip_id+'&safa_package_periods_id='+ safa_package_periods_id;
//    $.ajax
//    ({
//    	type: 'POST',
//    	url: '<?php echo base_url().'ea/passport_accommodation_rooms/get_hotels_table_by_package_ajax'; ?>',
//    	data: hotels_table_dataString,
//    	cache: false,
//    	success: function(html)
//    	{
//        	//alert(html);
//        	
//        	var html_arr = JSON.parse(html);
//        	
//    		$("#dv_hotels").html(html_arr[0]['hotels_table']);
//    		$("#rooms_for_storage").html(html_arr[0]['hotels_rooms_for_storage']);
//    		
//    	}
//    });
//    
//    
//    $.ajax
//    ({
//    	type: 'POST',
//    	url: '<?php echo base_url().'ea/passport_accommodation_rooms/get_hotels_by_package'; ?>',
//    	data: dataString,
//    	cache: false,
//    	success: function(html)
//    	{
//        	//alert(html);
//    		$(".erp_hotel_id").html(html);
//    		$(".erp_hotel_id").trigger("chosen:updated");
//
//    	}
//    });


    $.ajax
    ({
    	type: 'POST',
    	url: '<?php echo base_url().'ea/passport_accommodation_rooms/get_all_by_package_ajax'; ?>',
    	data: hotels_table_dataString,
    	cache: false,
    	success: function(data)
    	{
        	//alert(data);
        	
        	var data_arr = JSON.parse(data);
        	
    		$("#dv_hotels").html(data_arr[0]['hotels_table']);
    		$("#rooms_for_storage").html(data_arr[0]['hotels_rooms_for_storage']);

    		$(".erp_hotel_id").html(data_arr[0]['hotels']);
    		$(".erp_hotel_id").trigger("chosen:updated");

    		$("#safa_package_periods_id").html(data_arr[0]['periods']);
    		$("#safa_package_periods_id").trigger("chosen:updated");
    	}
    });
	    
    
});
</script>

<script type="text/javascript">
$('#safa_package_periods_id').change(function(){ 

	$("#lnk_copy_first_hotel_to_others").css( "display", "none");
	
	$("#tbl_accommodation tr").remove();
	
	$("#tbl_hotels tr").remove();
	hotels_counter = 0

	
    var safa_trip_id=$('#safa_trip_id').val();
	var safa_package_id=$('#safa_package_id').val();
	var safa_package_periods_id=$(this).val();
    
	var hotels_table_dataString = 'safa_package_id='+ safa_package_id+'& safa_trip_id='+ safa_trip_id+'&safa_package_periods_id='+ safa_package_periods_id;

    $.ajax
    ({
    	type: 'POST',
    	url: '<?php echo base_url().'ea/passport_accommodation_rooms/get_all_by_package_ajax'; ?>',
    	data: hotels_table_dataString,
    	cache: false,
    	success: function(data)
    	{
        	//alert(data);
        	
        	var data_arr = JSON.parse(data);
        	
    		$("#dv_hotels").html(data_arr[0]['hotels_table']);
    		$("#rooms_for_storage").html(data_arr[0]['hotels_rooms_for_storage']);

    		$(".erp_hotel_id").html(data_arr[0]['hotels']);
    		$(".erp_hotel_id").trigger("chosen:updated");

    		$("#safa_package_periods_id").html(data_arr[0]['periods']);
    		$("#safa_package_periods_id").trigger("chosen:updated");
    	}
    });
	    
    
});
</script>

<script type="text/javascript">
$('#safa_trip_id').change(function(){ 

	$("#lnk_copy_first_hotel_to_others").css( "display", "none");
	
	$("#tbl_accommodation tr").remove();
	
	$("#tbl_hotels tr").remove();
	hotels_counter = 0

	
    var safa_trip_id=$(this).val();
	var safa_package_id=$('#safa_package_id').val();
	var safa_package_periods_id=$("#safa_package_periods_id").val();
    
	var hotels_table_dataString = 'safa_package_id='+ safa_package_id+'& safa_trip_id='+ safa_trip_id+'&safa_package_periods_id='+ safa_package_periods_id;

    $.ajax
    ({
    	type: 'POST',
    	url: '<?php echo base_url().'ea/passport_accommodation_rooms/get_all_by_package_ajax'; ?>',
    	data: hotels_table_dataString,
    	cache: false,
    	success: function(data)
    	{
        	//alert(data);
        	
        	var data_arr = JSON.parse(data);
        	
    		$("#dv_hotels").html(data_arr[0]['hotels_table']);
    		$("#rooms_for_storage").html(data_arr[0]['hotels_rooms_for_storage']);

    		$(".erp_hotel_id").html(data_arr[0]['hotels']);
    		$(".erp_hotel_id").trigger("chosen:updated");

    		$("#safa_package_periods_id").html(data_arr[0]['periods']);
    		$("#safa_package_periods_id").trigger("chosen:updated");
    	}
    });
	    
    
});
</script>


<script type="text/javascript">
$('#lnk_show_passports').click(function(){ 
    
	var safa_group_ids = $('#safa_group_id').val();
	
	var dataString = 'safa_group_ids='+ safa_group_ids;
    
    $.ajax
    ({
    	type: 'POST',
    	url: '<?php echo base_url().'ea/passport_accommodation_rooms/get_passports_by_groups'; ?>',
    	data: dataString,
    	cache: false,
    	success: function(html)
    	{
                $("#ul_draggable_groups_passports").html(html).accordion("refresh");
                
                load_drag_drop();
    	}
    });

});
</script>

<script type="text/javascript">
$('#lnk_apply').click(function(){ 

	
	var form_inputs = $("#frm_passport_accommodation_rooms").serialize();
	
    $.ajax
    ({
    	type: 'POST',
    	url: '<?php echo base_url().'ea/passport_accommodation_rooms/apply_rooms_and_passports_ajax'; ?>',
    	data: form_inputs,
    	cache: false,
    	success: function(html)
    	{
        	//alert(html);
    		$("#dv_distribution_rooms_groups_passports").html(html);
    	}
    });

    $("#tbl_accommodation tr").remove();

});

</script>

<script type="text/javascript">
$(document).ready(function() {
        $("#ul_draggable_groups_passports").accordion();
    var config = {
        '.chosen-select': {width: "100%"},
        '.chosen-select-deselect': {allow_single_deselect: true},
        '.chosen-select-no-single': {disable_search_threshold: 10},
        '.chosen-select-no-results': {no_results_text: 'Oops, nothing found!'},
        '.chosen-select-width': {width: "95%"}
    }
    for (var selector in config) {
        $(selector).chosen(config[selector]);
    }


    // binds form submission and fields to the validation engine
    $("#frm_passport_accommodation_rooms").validationEngine({
            prettySelect : true,
            useSuffix: "_chosen",
            promptPosition : "topRight:-150"
            //promptPosition : "bottomLeft"
    });
    
});

</script>

<script type='text/javascript'>

function load_drag_drop()
{
	 var selectedClass = 'ui-state-highlight',
	    clickDelay = 600,
	    // click time (milliseconds)
	    lastClick, diffClick; // timestamps

	$(".draggable li, .droppable li")
	    // Script to deferentiate a click from a mousedown for drag event
	    .bind('mousedown mouseup', function(e) {
	        if (e.type == "mousedown") {
	            lastClick = e.timeStamp; // get mousedown time
	        } else {
	            diffClick = e.timeStamp - lastClick;
	            if (diffClick < clickDelay) {
	                // add selected class to group draggable objects
	                $(this).toggleClass(selectedClass);
	            }
	        }
                
                if (e.type == "mouseup") {
	            $(this).css({
	                top: 0,
	                left: 0,
                        relative:"relative"
	            });
	        }

            // By Gouda, to exit from selection when go to another ul.
            //alert($('.' + selectedClass).parent().attr('alt')+' - '+$(this).attr('alt'));
            if($('.' + selectedClass).parent().attr('alt')!=$(this).parent().attr('alt')) {
        		$('.' + selectedClass).removeClass(selectedClass);
        	}
        		
	    })
	    .draggable({
	        revertDuration: 10,
	        // grouped items animate separately, so leave this number low
	        //containment: '.darg_drop',
	        start: function(e, ui) {
	            ui.helper.addClass(selectedClass);
	        },
	        stop: function(e, ui) {
	            // reset group positions
	            $('.' + selectedClass).css({
	                top: 0,
	                left: 0,
                        relative:"relative"
	            });
	        },
	        drag: function(e, ui) {
	            // set selected group position to main dragged object
	            // this works because the position is relative to the starting position
	            $('.' + selectedClass).css({
	                top: ui.position.top,
	                left: ui.position.left,
                        relative:"relative"
	            });
	        }
	    });

	
	$(".droppable, .draggable").sortable().droppable({
	drop: function(e, ui) {
	    var items = $(this).find('li').length;
	    var selecteditems = $('.' + selectedClass).length;
	    var selectedparent = $('.' + selectedClass).parent();
	    if ($(this).attr('maxcount') && (items + selecteditems) > $(this).attr('maxcount') ) {
	        if (!confirm('Overlimit room size')) {
	            $('.' + selectedClass).removeClass(selectedClass).css({
	                top: 0,
	                left: 0,
                        relative:"relative"
	            });
	        }
	    }


	    //By Gouda
	    

	    var hdn_inputs_length = $(this).parent().parent().find('input').length;
	    var hdn_inputs = $(this).parent().parent().find('input');
		var are_safa_group_passport_id_exist=false;
		for(var i=0; i<hdn_inputs_length; i++) {
			if(!hdn_inputs[i].name.startsWith('hdn_safa_group_passport_ids_group') && !hdn_inputs[i].name.startsWith('hdn_safa_group_passport_names') && !hdn_inputs[i].name.startsWith('hdn_erp_hotelroomsize_ids') && !hdn_inputs[i].name.startsWith('hdn_room_availability') ) {

				if($('.' + selectedClass).find('input[id=hdn_safa_group_passport_ids]')[0]!== undefined) {
					var current_safa_group_passport_id = $('.' + selectedClass).find('input[id=hdn_safa_group_passport_ids]')[0].value;
					if(current_safa_group_passport_id == hdn_inputs[i].value) {
						//alert($('.' + selectedClass).parent().attr('class'));
						if(!$('.' + selectedClass).parent().attr('class').startsWith('droppable')) {
							are_safa_group_passport_id_exist = true;
						}
					}
				}
			}
		}

	    if(are_safa_group_passport_id_exist) {
	    	if($(this).attr('class').startsWith('droppable')) {
				alert('<?php echo lang('this_safa_group_passport_id_exist');?>');
	    	}
	    	
	    } else {
	    	var parent = $('.' + selectedClass).parent();
	    	
		    $('.' + selectedClass).appendTo($(this)).add(ui.draggable).removeClass(selectedClass).css({
		        top: 0,
		        left: 0,
                relative:"relative"
		    });
	
		    
		    var hdn_inputs_length = parent.find('input').length;
		    var hdn_inputs = parent.find('input');
			var safa_group_passport_ids='';
			for(var i=0; i<hdn_inputs_length; i++) {
				if(!hdn_inputs[i].name.startsWith('hdn_safa_group_passport_ids_group') && !hdn_inputs[i].name.startsWith('hdn_safa_group_passport_names') && !hdn_inputs[i].name.startsWith('hdn_erp_hotelroomsize_ids') && !hdn_inputs[i].name.startsWith('new_hdn_erp_hotelroomsize_ids') && !hdn_inputs[i].name.startsWith('hdn_room_availability') ) {
					if(i==0) {
						safa_group_passport_ids= hdn_inputs[i].value;
					} else {
						safa_group_passport_ids= safa_group_passport_ids+','+ hdn_inputs[i].value;
					}
				}
			}
			//alert(safa_group_passport_ids);
			for(var i=0; i<hdn_inputs_length; i++) {
				if(hdn_inputs[i].name.startsWith('hdn_safa_group_passport_ids_group')) {
					hdn_inputs[i].value=safa_group_passport_ids;
				}
			}
			
	
		    
	
		  //By Gouda
		    var hdn_inputs_length = $(this).find('input').length;
		    var hdn_inputs = $(this).find('input');
			var safa_group_passport_ids='';
			for(var i=0; i<hdn_inputs_length; i++) {
				if(!hdn_inputs[i].name.startsWith('hdn_safa_group_passport_ids_group') && !hdn_inputs[i].name.startsWith('hdn_safa_group_passport_names') && !hdn_inputs[i].name.startsWith('hdn_erp_hotelroomsize_ids') && !hdn_inputs[i].name.startsWith('new_hdn_erp_hotelroomsize_ids') && !hdn_inputs[i].name.startsWith('hdn_room_availability')  ) {
					if(i==0) {
						safa_group_passport_ids= hdn_inputs[i].value;
					} else {
						safa_group_passport_ids= safa_group_passport_ids+','+ hdn_inputs[i].value;
					}
				}
			}
			//alert(safa_group_passport_ids);
			for(var i=0; i<hdn_inputs_length; i++) {
				if(hdn_inputs[i].name.startsWith('hdn_safa_group_passport_ids_group')) {
					hdn_inputs[i].value=safa_group_passport_ids;
				}
			}

	    }
	    
	}

});

}

</script>

<script>
$(document).ready(function() {
$('.lnk_li_delete_safa_passport_accommodation_rooms').click(function(event){  
	if(confirm('<?= lang('global_are_you_sure_you_want_to_delete')?>')) {
		
		var divparent = $(this).parent().parent();
		$(this).closest("ul").remove();	
		var safa_passport_accommodation_room_id = $(this).attr('id');
		//alert(safa_passport_accommodation_room_id);
		var hidden_input = '<input type="hidden" name="safa_passport_accommodation_rooms_remove[]" value="' + safa_passport_accommodation_room_id + '" />';
        $('#tbl_accommodation').append(hidden_input);

        //alert($(divparent).attr('id'));
        if($(divparent).find('ul').length==0) {
        	$(divparent).remove();
        }
	}
});
});
</script>

<script>
$(document).ready(function() {
    $('.fancybox').fancybox({
        afterClose: function() {
            location.reload();
        }
    });
});
</script>



<script type="text/javascript">
setInterval(function(){save_drag_drop()},30000);
function save_drag_drop()
{
	var rooms_for_storage = $('#rooms_for_storage').html();
	var total_rooms_for_storage = $('#total_rooms_for_storage').html();
	
    if (typeof (Storage) !== "undefined") {
        localStorage.rooms_for_storage_<?php echo $safa_reservation_form_id;?> = rooms_for_storage;
        localStorage.total_rooms_for_storage_<?php echo $safa_reservation_form_id;?> = total_rooms_for_storage;

    } else {
        //alert('<?php echo lang('browser_not_support_storage');?>');
    }
}

$(document).ready(function() {
	if (typeof (Storage) !== "undefined") {
		
		if(typeof (localStorage.rooms_for_storage_<?php echo $safa_reservation_form_id;?>)  !=='undefined') {

				if(confirm('<?php echo lang('load_storage');?>')) {
					$('#rooms_for_storage').html(localStorage.rooms_for_storage_<?php echo $safa_reservation_form_id;?>);
					$('#total_rooms_for_storage').html(localStorage.total_rooms_for_storage_<?php echo $safa_reservation_form_id;?>);
					
					load_drag_drop();
				}
			
		}
	}
	
//	$('#load_storage').click(function() {
//	    if (typeof (Storage) !== "undefined") {
//	        $('.darg_drop').html(localStorage.demod);
//	        load_drag_drop();
//	    } else {
//	        alert('<?php echo lang('browser_not_support_storage');?>');
//	    }
//	});

	
});
</script>


<script type="text/javascript">
function get_to_date_for_hotel(from_date, counter)
{
	var safa_package_periods_id=$("#safa_package_periods_id").val();
	var erp_hotel_id = $("#erp_hotel_id" + counter).val();
		
	var dataString = 'safa_package_periods_id='+ safa_package_periods_id+'&from_date='+ from_date+'&erp_hotel_id='+ erp_hotel_id;
    $.ajax
    ({
    	type: 'POST',
    	url: '<?php echo base_url().'ea/passport_accommodation_rooms/get_to_date_by_from_date_and_package_period_ajax'; ?>',
    	data: dataString,
    	cache: false,
    	success: function(html)
    	{
    		$("#to_date" + counter).val(html);

    	}
    });
	
}
</script>


<script type="text/javascript">
function replaceAll(find, replace, str) 
{
  while( str.indexOf(find) > -1) {
    str = str.replace(find, replace);
  }
  return str;
}

$(document).ready(function() {
	
	$('#lnk_copy_first_hotel_to_others').click(function() {

		var hdn_hotels_inputs = $('#dv_hotels').find('input');
		var hdn_hotels_inputs_length = $('#dv_hotels').find('input').length;

		var first_hotel_id = 0;
		var hotel_id_counter = 0;
		for (var h=0; h<hdn_hotels_inputs_length; h++) {
			
			
			if(hdn_hotels_inputs[h].name.startsWith('erp_hotel_id')  ) {
				var current_hotel_id =hdn_hotels_inputs[h].value;
				
				if(hotel_id_counter==0) {
					first_hotel_id = current_hotel_id;
					//alert(hotel_id_counter);
					
				} else {
					
					var not_enough_rooms = false;
					var not_enough_rooms_message='';

					var erp_hotels_availability_room_details_arr = new Array();
					
					$("#select_hdn_erp_hotelroomsize_id option").each(function() {
						//alert(this.text + ' ' + this.value);
						//alert($("#dv_table_erp_hotelroomsize"+first_hotel_id+"_"+this.value).find('ul').length);
							
						    
						if($("#dv_table_erp_hotelroomsize"+first_hotel_id+"_"+this.value).find('ul').length>0) {
							
						//---------------------------------- Check For Hotel Rooms Availbility ----------------------------------------
					  	var safa_package_id = $("#safa_package_id").val();
					  	var erp_hotel_id = current_hotel_id;
					  	var erp_hotelroomsize_id =this.value;
					  	var rooms_count = $("#dv_table_erp_hotelroomsize"+first_hotel_id+"_"+this.value).find('ul').length;
					  	var erp_hotel_from_date = get_from_to_date_for_hotel('from_date',current_hotel_id);
					  	var erp_hotel_to_date = get_from_to_date_for_hotel('to_date',current_hotel_id);
						//alert (erp_hotel_from_date +'  '+ erp_hotel_to_date);
					    var dataString = 'safa_package_id='+ safa_package_id+'& rooms_count='+ rooms_count+'&erp_hotel_id='+ erp_hotel_id+'&erp_hotelroomsize_id='+ erp_hotelroomsize_id+'&erp_hotel_from_date='+ erp_hotel_from_date+'&erp_hotel_to_date='+ erp_hotel_to_date;
						var available_rooms_count=0;
						var hotel_availability_arr = new Array();
						
						
					  	$.ajax
					  	({
						    type: 'POST',
						    url: '<?php echo base_url().'ea/passport_accommodation_rooms/get_available_rooms_ajax'; ?>',
						    data: dataString,
						    cache: false,
						    //This to read js var in ajax
						    async: false,
						    success: function(data)
						    {
						        //alert(data);
						        var data_arr = JSON.parse(data);
						        hotel_availability_arr =  data_arr[0]['hotel_availability_arr'];
						        hotel_availability_arr = $.map(hotel_availability_arr, function (value, key) { return value; });

						        var erp_hotels_availability_room_details_arr_1 =  data_arr[0]['erp_hotels_availability_room_details_arr'];
						        erp_hotels_availability_room_details_arr.push($.map(erp_hotels_availability_room_details_arr_1, function (value, key) { return value; }));
						         
						        //available_rooms_count = data_arr[0]['available_rooms_count'];
						        return true;
						    }
					  	});
					  	//alert(hotel_availability_arr.length);
					    for (var a=0; a<hotel_availability_arr.length ; a++ ) {
					    	available_rooms_count = hotel_availability_arr[a];
					    	//alert(parseInt(available_rooms_count)+' '+parseInt(rooms_count));
						  	if(parseInt(available_rooms_count)<parseInt(rooms_count)) {
						  		not_enough_rooms = true;
						  		not_enough_rooms_message = not_enough_rooms_message +' '+ this.text + ': ' + available_rooms_count +', ';
						  	}
					    }
					    
						}
						
					});


					if(not_enough_rooms)
					if ($('input:radio[name=rooms_availability_setting_allowing]:checked').val()=='disallow') {
						alert('<?php echo lang('available_rooms_in_this_periods_less_than_required_rooms');?> :'+ not_enough_rooms_message);
						return false;
					} else {
						if(!confirm('<?php echo lang('available_rooms_in_this_periods_less_than_required_rooms_are_continue');?> :'+ not_enough_rooms_message)) {
							return false;
						}
					}
					
					//-------------------------------------------------------------------------------------------------------------
					
						
					//alert($("#dv_distribution_rooms_groups_passports"+first_hotel_id).html());
					
					var first_hotel_title_html = $("#dv_hotel_title_"+first_hotel_id).html();
					var current_hotel_title_html = $("#dv_hotel_title_"+current_hotel_id).html();
					
					var first_hotel_div_html = $("#dv_distribution_rooms_groups_passports"+first_hotel_id).html();
					first_hotel_div_html = replaceAll('_new_room'+first_hotel_id, '_new_room'+current_hotel_id, first_hotel_div_html);
					first_hotel_div_html = replaceAll('dv_table_erp_hotelroomsize'+first_hotel_id, 'dv_table_erp_hotelroomsize'+current_hotel_id, first_hotel_div_html);
					first_hotel_div_html = first_hotel_div_html.replace('"'+first_hotel_id+'"', '"'+current_hotel_id+'"');
					first_hotel_div_html = replaceAll(','+first_hotel_id+',', ','+current_hotel_id+',', first_hotel_div_html);

					first_hotel_div_html = first_hotel_div_html.replace(first_hotel_title_html, current_hotel_title_html);
					
					
					$("#dv_distribution_rooms_groups_passports"+current_hotel_id).html(first_hotel_div_html);



					// Hotel Availability
					//-------------------------------------------------------------------------------------------
					
					
						var availability_counter=0;
						$("#select_hdn_erp_hotelroomsize_id option").each(function() {

							//alert(erp_hotels_availability_room_details_arr.length);
							for (var s=0; s<erp_hotels_availability_room_details_arr.length ; s++ ) {
								if(availability_counter==s) {
									var erp_hotels_availability_room_details_arr_for_hotel = $.map(erp_hotels_availability_room_details_arr[s][0], function (value, key) { return value; });
									var erp_hotels_availability_room_details_arr_for_hotel_key_value = $.map(erp_hotels_availability_room_details_arr_for_hotel, function (value, key) { return key+'-'+value; });
									
									//erp_hotels_availability_room_details_arr.push($.map(erp_hotels_availability_room_details_arr_1, function (value, key) { return value; }));
									        
									
									var hdn_room_availability_inputs = $("#dv_table_erp_hotelroomsize"+current_hotel_id+"_"+this.value).find('input');
									var hdn_room_availability_inputs_length = $("#dv_table_erp_hotelroomsize"+current_hotel_id+"_"+this.value).find('input').length;
									var v_index=-1;
										
									for(var v=0; v<hdn_room_availability_inputs_length; v++) {
										v_index++;
										
										if(hdn_room_availability_inputs[v].name.startsWith('hdn_room_availability')) { 
											
											var hdn_room_availability_value =  ''
											var spn_hdn_room_availability_value = '';
											if(erp_hotels_availability_room_details_arr_for_hotel_key_value[v_index]!=null) {                                                                   		
											var erp_hotels_availability_room_detail = erp_hotels_availability_room_details_arr_for_hotel_key_value[v_index];
											var erp_hotels_availability_room_detail_arr = erp_hotels_availability_room_detail.split("-");
											var hdn_room_availability_value =  erp_hotels_availability_room_detail_arr[0];
											var spn_hdn_room_availability_value = erp_hotels_availability_room_detail_arr[1];
											}
				
											//alert(hdn_room_availability_inputs[v].name);
											hdn_room_availability_inputs[v].value = hdn_room_availability_value;
											var current_input_key = hdn_room_availability_inputs[v].name.replace("hdn_room_availability", "");
											$("#spn_hdn_room_availability"+current_input_key).html(spn_hdn_room_availability_value);
											
										} else {
											v_index--;
										}
									}
								}
							}
							availability_counter++;
						});
						
					
					//-------------------------------------------------------------------------------------------
					
					

					
				}
				hotel_id_counter++;
			}
		}  		


		load_drag_drop();
		
	});

	
});

</script>

<script>
function room_availability(hdn_room_availability_id, erp_hotel_id, erp_hotelroomsize_id)
{
	$.fancybox({
        'type': 'iframe', 
        'width' : 400,
        'height' : 600,
        'autoDimensions' : false,
        'autoScale' : false,
        'href' : "<?php echo site_url('ea/passport_accommodation_rooms/room_availability_popup'); ?>"+"/"+hdn_room_availability_id+"/"+erp_hotel_id+"/"+erp_hotelroomsize_id
    });
}
</script>