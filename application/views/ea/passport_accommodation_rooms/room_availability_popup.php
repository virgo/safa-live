
<style>
.widget{width:98%;}
body{
height: auto!important;
overflow: auto !important;
background:none transparent !important;
overflow: hidden;
min-height: 3%;
}
.modal-body{
    width: 96%;
    padding:0 2% ;
}
.row-fluid{
padding-top: 5px !important;
padding-bottom: 5px !important;
}
</style>

<style>
.wizerd-div,.row-form{width: 96%;margin:6% 2%;}
label{padding: 0;}
</style>
        

            

<div class="modal-body" >
        
        <div class="row-fluid" id="dv_rooms_availabilities">
            
        </div>
        
</div>    



<script type="text/javascript">
$(document).ready(function() {

  	var hdn_room_availability_id= '<?php echo $hdn_room_availability_id; ?>';
	var erp_hotel_id= <?php echo $erp_hotel_id; ?>;
	var erp_hotelroomsize_id= <?php echo $erp_hotelroomsize_id; ?>;

	var erp_hotel_from_date = get_from_to_date_for_hotel('from_date',erp_hotel_id);
  	var erp_hotel_to_date = get_from_to_date_for_hotel('to_date',erp_hotel_id);
	
	var dataString = 'hdn_room_availability_id='+ hdn_room_availability_id+'&erp_hotel_id='+ erp_hotel_id+'&erp_hotelroomsize_id='+ erp_hotelroomsize_id+'&erp_hotel_from_date='+ erp_hotel_from_date+'&erp_hotel_to_date='+ erp_hotel_to_date;
    
	$.ajax({
        type: "POST",
        url: "<?php echo site_url('ea/passport_accommodation_rooms/get_rooms_availabilities_ajax'); ?>",
        data: dataString,
    	cache: true,
    	success: function(html)
    	{
        	$("#dv_rooms_availabilities").html(html);
    	}
      });
});
</script>

<script>
function get_from_to_date_for_hotel(for_date, erp_hotel_id)
{
	var hdn_hotels_inputs = parent.$('#dv_hotels').find('input');
	var hdn_hotels_inputs_length = parent.$('#dv_hotels').find('input').length;

	var current_hotel_id=0;
	var current_for_date='';
	
	for (var h=0; h<hdn_hotels_inputs_length; h++) {

		if(hdn_hotels_inputs[h].name.startsWith('erp_hotel_id')  ) {
			var current_hotel_id =hdn_hotels_inputs[h].value;
		}
		
		if(hdn_hotels_inputs[h].name.startsWith(for_date)) {
			if(current_hotel_id==erp_hotel_id) {
				current_for_date = hdn_hotels_inputs[h].value;
			}
		}
		
	}

	return current_for_date;
}
</script>

<script>
function select_room_availability(erp_hotels_availability_room_detail_id, number, erp_hotel_id)
{
	var hdn_inputs_length = parent.$('#dv_distribution_rooms_groups_passports'+erp_hotel_id).find('input').length;
    var hdn_inputs = parent.$('#dv_distribution_rooms_groups_passports'+erp_hotel_id).find('input');
	for(var i=0; i<hdn_inputs_length; i++) {
		//alert(hdn_inputs[i].name);
		if(hdn_inputs[i].name.startsWith('hdn_room_availability') ) {
			//alert(erp_hotels_availability_room_detail_id +'    -'+ hdn_inputs[i].value);
			if(erp_hotels_availability_room_detail_id == hdn_inputs[i].value) {
				alert('<?php echo lang('this_room_exist');?>');
				return false;
			}
		}
	}
    
	var hdn_room_availability_id= '<?php echo $hdn_room_availability_id; ?>';
	parent.$('#'+hdn_room_availability_id).val(erp_hotels_availability_room_detail_id);
	parent.$('#spn_'+hdn_room_availability_id).html(number);
	
	//alert(erp_hotels_availability_room_detail_id);
	window.parent.jQuery.fancybox.close();
}
</script>