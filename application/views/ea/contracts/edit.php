<!-- multi selection choosen -->
<link rel="stylesheet" href="<?= NEW_CSS_JS ?>/multi-select-chosen/chosen.css">
<script src="<?= NEW_CSS_JS ?>/multi-select-chosen/chosen.jquery.js" type="text/javascript"></script>
<script>
    $(document).ready(function() {
        var visapricerow = 1;
        $("#wizard_validate").validationEngine('attach', {promptPosition: "topLeft", prettySelect: true});
        $('.datepicker').datepicker({
            dateFormat: "yy-mm-dd",
            controlType: 'select',
            timeFormat: 'HH:mm'
        });

        $("button[name=addnewprice]").click(function() {
            var new_row = function() {/*
             <tr id="{$id}">
             <td><?= form_input('visa_price[{$id}]', set_value('visa_price')) ?></td>
             <td><?= form_input('visa_date_from[{$id}]', set_value('visa_date_from'), 'class="datepicker"') ?></td>
             <td><?= form_input('visa_date_to[{$id}]', set_value('visa_date_to'), 'class="datepicker"') ?></td>
             <td><?= form_dropdown('visa_price_currency_id[{$id}]', $currencies) ?></td>
             </tr>
             */
            }.toString().replace(/{\$id}/g, 'N' + visapricerow);
            visapricerow++;
            $('#visa_prices tbody').append(new_row);
            $('.datepicker').datepicker({
                dateFormat: "yy-mm-dd",
                controlType: 'select',
                timeFormat: 'HH:mm'
            });
            return false;
        });
    });
</script>
<style>
    .widget-header {
        clear:both;
    }
    .wizerd-div{width:96%; margin: 2%;}
    .row-form {margin: 0 2%;width:96%;}
</style>

<div class="widget">
    <div class="path-container Fright">
        <div class="path-name Fright">
            <a href="<?= site_url('ea/dashboard') ?>"><?= lang('global_system_management') ?></a>
        </div>
        <div class="path-arrow Fright">
        </div>
        <div class="path-name Fright">
            <a href='<?= site_url('ea/contract_approving_phases/view_all') ?>'><?= lang('ea_contracts') ?></a>
        </div>
        <div class="path-arrow Fright">
        </div>
        <div class="path-name Fright">
            <span><?= lang('ea_contracts_edit') ?></span>
        </div>
    </div>
</div>


<div class="widget" >

    <div class="widget-header">
        <div class="widget-header-icon Fright">
            <span class="icos-pencil2"></span>
        </div>
        <div class="widget-header-title Fright">
            <?= lang('contracts_edit') ?>
        </div>
    </div>

    <div class="widget-container ">

            <?= form_open_multipart("", "method='POST' id='wizard_validate' 'autocomplete='off' ") ?>
            <fieldset title="<?= lang('contract_step1') ?>">
                <legend><?= lang('contract_step1') ?></legend>


                <div class="span6">
                    <div class="wizerd-div"><a><?= lang('contract_step1') ?></a></div>
                    <div class="row-fluid"style="margin: 3%;width: 94%;border-left:1px solid #fff">
                        <div class="row-fluid" style="margin-top: 19px;">
                            <div class="span4">
                                <div class="span11"><?= lang('contracts_name_ar') ?>
                                    <? if (name() == 'name_ar'): ?><font style='color:red' >*</font><? endif; ?>
                                </div>
                                <div class="span11">
                                    <input type ='text' name='contract_name_ar' value='<?= set_value('contract_name_ar', $contract_row_item->name_ar) ?>'<? if (name() == 'name_ar') { ?>class="validate[required] input-huge"<? } else { ?> class="input-huge" <?php } ?>/> 
                                    <span class='bottom' style='color:red'>
                                        <?= form_error('contract_name_ar') ?>
                                    </span>
                                </div> </div>
                            <div class="span4">
                                <div class="span11">
                                    <?= lang('contracts_name_la') ?>
                                    <? if (name() == 'name_la'): ?><font style='color:red' >*</font><? endif; ?>
                                </div>
                                <div class="span11"><input type ='text' name='contract_name_la' value='<?= set_value('contract_name_la', $contract_row_item->name_la) ?>'<? if (name() == 'name_la') { ?>class="validate[required] input-huge<? } else { ?> class="input-huge" <?php } ?>/> 
                                                           <span class='bottom' style='color:red'>
                                                               <?= form_error('contract_name_la') ?>
                                    </span>
                                </div> </div>
                            <div class="span4">
                                <div class="span11"><?= lang('contracts_nationality') ?><font style='color:red' >*</font></div>
                                <div class="span11" ><?= form_dropdown('contarct_nationality', ddgen('erp_countries', array('erp_country_id', name())), set_value("contarct_nationality", $item->nationality_id), " class=' validate[required] input-huge val1'  style='width:100%;' ") ?>
                                    <span class='bottom' style='color:red'>
                                        <?= form_error('contarct_nationality') ?>
                                    </span>  
                                </div></div>

                        </div>
                        <div class="row-fluid" >
                            <div class="span4">
                                <div class="span11"><?= lang('contracts_country') ?><font style='color:red' >*</font></div>
                                <div class="span11" ><?= form_dropdown('contarct_country', ddgen('erp_countries', array('erp_country_id', name())), set_value('contarct_country', $item->country_id), " class=' validate[required] input-huge val1'  style='width:100%;' ") ?>
                                    <span class='bottom' style='color:red'>
                                        <?= form_error('contarct_country') ?>
                                    </span>   
                                </div></div>
                            <div class="span4">
                                <div class="span11"><?= lang('contracts_phone') ?><font style='color:red' >*</font></div>
                                <div class="span11"><?= form_input('contarct_phone', set_value('contarct_phone', $item->phone), 'class="validate[required,custom[phone]]  input-huge"') ?>
                                    <span class='bottom' style='color:red'>
                                        <?= form_error('contarct_phone') ?>
                                    </span>  
                                </div></div>
                            <div class="span4">
                                <div class="span11"><?= lang('contracts_fax') ?></div>
                                <div class="span11"><?= form_input('contarct_fax', set_value('contarct_fax', $item->fax), "class=' input-huge'") ?></div>
                            </div>

                        </div>
                        <div class="row-fluid" >
                            <div class="span4">
                                <div class="span11"><?= lang('contracts_email') ?><font style='color:red' >*</font></div>
                                <div class="span11" ><?= form_input('contarct_email', set_value('contarct_email', $item->email), "class='validate[required,custom[email]]  input-huge'  ") ?>
                                    <span class='bottom' style='color:red'>
                                        <?= form_error('contarct_email') ?>
                                    </span>
                                </div> </div>
                            <div class="span4">
                                <div class="span11"><?= lang('contracts_ayata_num') ?></div>
                                <div class="span11"><?= form_input('contarct_ayata_num', set_value('contarct_ayata_num', $item->iata), "class=' input-huge'") ?>
                                    <?= form_error('contarct_ayata_num') ?>
                                </div> </div>
                            <div class="span4">
                                <div class="span11"><?= lang('contracts_agencysymbol') ?><font style='color:red' >*</font></div>
                                <div class="span11"><?= form_input('contarct_agency_symbol', set_value('contarct_agency_symbol', $item->agency_symbol), "class=' validate[required] input-huge'") ?>
                                    <span class='bottom' style='color:red'>
                                        <?= form_error('contarct_agency_symbol') ?>
                                    </span>  
                                </div> </div>
                        </div>
                        <div class="row-fluid">
                            <div class="span4">
                                <div class="span11">
                                    <?= lang('contracts_address') ?>
                                </div>
                                <div class="span11">
                                    <?= form_input('contarct_address', set_value('contarct_address', $item->address), "class=' input-huge'") ?>
                                </div> 
                            </div>
                            <div class="span4">
                                <div class="span11"><?= lang('contracts_ksaaddress') ?></div>
                                <div class="span11">
                                    <?= form_input('contarct_ksa_address', set_value('contarct_ksa_address', $item->ksa_address), "class=' input-huge'") ?>
                                </div>  
                            </div>

                        </div>
                    </div>     

                    <div class="wizerd-div"><a><?= lang('contract_uasp') ?></a></div><br />
                    <div class="row-fluid"style="margin: 3%;width: 94%;border-left:1px solid #fff">
                        <div class="row-fluid">
                            <div class="span4">
                                <div class="span11">
                                    <?= lang('uasp_username') ?>
                                </div>
                                <div class="span11">
                                    <?= form_input('uasp_username', set_value('uasp_username', $item->uasp_username), "class=' input-huge'") ?>
                                </div>
                            </div>
                            <div class="span4">
                                <div class="span11">
                                    <?= lang('uasp_password') ?>
                                </div>
                                <div class="span11">
                                    <?= form_input('uasp_password', set_value('uasp_password', $item->uasp_password), "class=' input-huge'") ?>
                                </div>
                            </div>
                            <div class="span4">
                                <div class="span11">
                                    <?= lang('uasp_eacode') ?>
                                </div>
                                <div class="span11">
                                    <?= form_input('uasp_eacode', set_value('uasp_eacode', $item->uasp_eacode), "class=' input-huge'") ?>
                                </div>
                            </div>
                        </div>
                    </div>     
                </div>

                <div class="span6 ">
                    <div class="wizerd-div"><a><?= lang('contract_step3') ?></a></div>
                    <div class="row-fluid"style="margin: 3%;width: 94%">
                        <div class="row-fluid" style="margin-top: 19px;">
                            <div class="span4">
                                <div class="span12"><?= lang('contracts_resposible_name') ?></div>
                                <div class="span12" ><?= form_input('contarct_responsible_name', set_value('contarct_responsible_name', $item->responsible_name), "class=' input-huge'") ?></div>
                            </div>
                            <div class="span4">
                                <div class="span12"><?= lang('contracts_resposible_phone') ?></div>
                                <div class="span12" ><?= form_input('contarct_responsible_phone', set_value('contarct_responsible_phone', $item->responsible_phone), "class='validate[custom[phone]] input-huge'") ?></div>  
                            </div>
                            <div class="span4">
                                <div class="span12"><?= lang('contracts_resposible_email') ?></div>
                                <div class="span12" ><?= form_input('contarct_responsible_email', set_value('contarct_responsible_email', $item->responsible_email), "class='validate[custom[email]] input-huge'") ?></div>  
                            </div>
                        </div>
                        <div class="row-fluid">
                            <div class="span7"><div class="span10" ><?= lang("contracts_resposible_passport_photo_path") ?></div>
                                <div class="span12" >
                                    <input class="input-huge" type="file" value="" name="responsible_passport_photo_path"/>

                                    <div>
                                        <a href="<?php echo base_url('static/uploads/passports_photos/' . $item->responsible_passport_photo_path) ?>" target="_blank"  ><img src="<?php echo base_url('static/uploads/passports_photos/' . $item->responsible_passport_photo_path) ?>" width="200px" height="200px" ></img></a>
                                    </div>
                                    <span class="bottom" style='color:red' >
                                        <?= form_error('userfile') ?> 
                                    </span>
                                </div>
                            </div>
                            <div class="span7"> <div class="span10" ><?= lang('contract_notes') ?></div>
                                <div class="span12" ><textarea  name="contract_notes"  class="input-huge"  > <?= set_value('contract_notes', $item->notes) ?> </textarea></div> 
                            </div>
                        </div>
                    </div>
                </div>
            </fieldset>

            <div class="row-fluid">
                <div class="span12">
                    <div class="span12">
                        <button name="submit" class="btn btn-primary finish pull-left"style="margin: 5px;" ><?= lang('contract_save') ?></button>
                        <button href="<?= site_url('ea/contract_approving_phases/view_all') ?>"style="margin: 5px;" class="pull-left btn btn-primary" ><?= lang('global_back') ?></button>
                    </div>
                </div>
            </div>

    </div>

</div>


<script type="text/javascript">
    $(document).ready(function() {
        var config = {
            '.chosen-select': {width: "100%"},
            '.chosen-select-deselect': {allow_single_deselect: true},
            '.chosen-select-no-single': {disable_search_threshold: 10},
            '.chosen-select-no-results': {no_results_text: 'Oops, nothing found!'},
            '.chosen-select-width': {width: "95%"}
        }
        for (var selector in config) {
            $(selector).chosen(config[selector]);
        }


        // binds form submission and fields to the validation engine
        $("#frm_contract_approving_phases").validationEngine({
            prettySelect: true,
            useSuffix: "_chosen",
            promptPosition: "topRight:-150"
                    //promptPosition : "bottomLeft"
        });


        $("#can_use_other_uo").change(function() {
            if ($('#can_use_other_uo').attr('checked')) {

                $("input[name='out_services_hostel']").removeAttr("disabled");
                $("input[name='out_services_travel']").removeAttr("disabled");
                $("input[name='out_services_other_services']").removeAttr("disabled");

            } else {
                $("input[name='out_services_hostel']").attr("disabled", "disabled");
                $("input[name='out_services_travel']").attr("disabled", "disabled");
                $("input[name='out_services_other_services']").attr("disabled", "disabled");
            }
        });



    });


</script>

<script type="text/javascript">
    /*initializing the map*/
    $(document).ready(function() {
        $('#transporters_us_values').val(<?= $list_transporter ?>);
        $('#transporters_us_values').trigger("chosen:updated");
//        $("#wizard_validate").formToWizard();
        $('.next').click(function() {
            var parent_ = $("#step_map").parent(); /* init the map */
            if ($(parent_).attr("style") == "display: block;") {
                var alharam = get_alharam();
                initialize(alharam['lat'], alharam['long'], false);
            }
            /*removing the fils from the inputs*/
            var parent_ = $("#step_image").parent();
            if ($(parent_).attr("style") == "display: none;") {
                $("#dv_responsible_passport_photo #responsible_passport_photo_path").val('');
            }
        });

        $('input[name=transport_cycle_from]').click(function() {
            if ($(this).val() == 1) {
                $('div#transport_cycle_from').show();
            } else {
                $('div#transport_cycle_from').hide();
            }
        });

        $('input[name=transporters_us]').click(function() {
            if ($(this).val() == "all") {
                $('#transporters_us_values').val([]);
                $('#transporters_us_values').trigger("chosen:updated");
            }
        });
    });
</script>


