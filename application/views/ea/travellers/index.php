
<?= form_open() ?>
<div class="row-fluid">              
                <div class="widget">
                    <div class="head dark">
                        <div class="icon"><i class="icos-pencil2"></i></div>
                        <h2>Default form elements</h2>
                    </div>                        
                  <div class="block-fluid">

                    
                    <div class="row-form">
                        <div class="span8">
                        <div style="float:left">
                                <div class="span2" style="text-align:center"><?= lang('travellers_name') ?>:</div>
                                    <div class="span10">
                                        <input name="first_<?= name() ?>" value="<?= $this->input->post('first_'.name()) ?>" class="input-mini" placeholder="<?= lang('travellers_first_name') ?>" type="text"> 
                                        <input name="second_<?= name() ?>" value="<?= $this->input->post('second_'.name()) ?>"  class="input-mini" placeholder="<?= lang('travellers_second_name') ?>" type="text">
                                        <input name="third_<?= name() ?>" value="<?= $this->input->post('third_'.name()) ?>"  class="input-mini" placeholder="<?= lang('travellers_third_name') ?>" type="text">
                                        <input name="fourth_<?= name() ?>" value="<?= $this->input->post('fourth_'.name()) ?>"  class="input-mini" placeholder="<?= lang('travellers_last_name') ?>" type="text">
                                    </div>
                                </div>
                        </div>
                    <div class="span4"></div>
                    </div>
                    
                    <div class="row-form">
                        <div class="span6">
                                <div class="span4" style="text-align:center"><?= lang('travellers_passport_number') ?>:</div>
                                <div class="span8"><input type="text"></div>
                        </div>
                        <div class="span6">
                                <div class="span4" style="text-align:center"><?= lang('travellers_nationality') ?> :</div>
                                <div class="span8">
                                 <?= form_dropdown('nationality_id', ddgen('erp_countries', array('erp_country_id', name())), set_value('nationality_id') , " name='s_example' class='select' style='width:100%;' ") ?>
                                </div>
                        </div>
                    </div>
                        
                        <div class="row-form">
                    <div class="span6">
                            <div class="span4" style="text-align:center"><?= lang('travellers_travellers_gender_id') ?>:</div>
                            <div class="span8">
                                
                                <?= form_dropdown('erp_gender_id', ddgen('erp_gender', array('erp_gender_id', name())) , set_value('erp_gender_id') ) ?>
                            
                            </div>
                    </div>
                    <div class="span6">
                            <div class="span4" style="text-align:center"><?=  lang('travellers_birth_date') ?>:</div>
                            <div class="span8">
                                <input name='travellers_birth_date' value="<?= $this->input->post('travellers_birth_date') ?>" id="dp1369322652611" class="datepicker hasDatepicker" type="text">
                            </div>
                    </div>
                        </div>
                        
                        <div class="row-form">
                    <div class="span6">
                            <div class="span4" style="text-align:center"><?= lang('travellers_nationality_id') ?>:</div>
                            <div class="span8"><input type="text"></div>
                    </div>
                    </div>
                    
                    <div class="toolbar bottom TAC">
                      <input type="submit" name="search" value="search" class="btn btn-primary" />
                      <button class="btn btn-primary">إغلاق</button>
                        </div>

                  </div>
                </div>
                
                
                
                <div class="widget">
                <div class="head">
                    <div class="icon"><span class="icosg-target1"></span></div>
                    <h2>Table sorting pagination <?= count($travellers) ?></h2>                       
                </div>                
                    <div class="block-fluid">
                        <div id="DataTables_Table_0_wrapper" class="dataTables_wrapper" role="grid"><div class="dataTables_length" id="DataTables_Table_0_length">
                          <label>إظهر
                            <select aria-controls="DataTables_Table_0" size="1" name="DataTables_Table_0_length"><option selected="selected" value="5">5</option><option value="10">10</option><option value="25">25</option><option value="50">50</option><option value="100">100</option></select>
                        مدخل
                        </label></div><div id="DataTables_Table_0_filter" class="dataTables_filter">
                          <label>بحث: 
                            <input aria-controls="DataTables_Table_0" type="text"></label></div><table style="width: 100%;" aria-describedby="DataTables_Table_0_info" id="DataTables_Table_0" class="fpTable dataTable" cellpadding="0" cellspacing="0" width="100%">
                            <thead>
                                <tr role="row"><th aria-label="" style="width: 24px;" colspan="1" rowspan="1" role="columnheader" class="sorting_disabled">
                                    <div class="checker"><span><input style="opacity: 0;" class="checkall" type="checkbox"></span></div></th><th aria-label="Passport no.: activate to sort column ascending" style="width: 262px;" colspan="1" rowspan="1" aria-controls="DataTables_Table_0" tabindex="0" role="columnheader" class="sorting" width="25%">Passport no.</th><th aria-label="Name: activate to sort column ascending" style="width: 205px;" colspan="1" rowspan="1" aria-controls="DataTables_Table_0" tabindex="0" role="columnheader" class="sorting" width="20%">Name</th><th aria-label="Gender: activate to sort column ascending" style="width: 206px;" colspan="1" rowspan="1" aria-controls="DataTables_Table_0" tabindex="0" role="columnheader" class="sorting" width="20%">Gender</th><th aria-label="Nationality: activate to sort column ascending" style="width: 207px;" colspan="1" rowspan="1" aria-controls="DataTables_Table_0" tabindex="0" role="columnheader" class="sorting" width="20%">Nationality</th><th aria-label="Birth date" style="width: 151px;" colspan="1" rowspan="1" role="columnheader" class="TAC sorting_disabled" width="15%">Birth date</th></tr>
                            </thead>
                            
                        <tbody aria-relevant="all" aria-live="polite" role="alert">                          
                       
                            <? foreach ($travellers as $traveller) : ?>
                            <tr class="even">
                                    <td class="  sorting_1">
                                        <div class="checker">
                                            <span>
                                                <input style="opacity: 0;" name="order[]" value="528" type="checkbox">
                                            </span>
                                        </div>
                                    </td>
                                    <td class=" "><a href="#"><?= $traveller->passport_no ?></a></td>
                                    <td class=" "><?= $traveller->{'first_'.name()}." ".$traveller->{'second_'.name()}." ".$traveller->{'third_'.name()}." ".$traveller->{'fourth_'.name()} ?></td>
                                    <td class=" "><span class="label label-important"><?= $traveller->gender_name?></span></td>
                                    <td class=" "><?= $traveller->country_name ?></td>
                                    <td class=" "><?= $traveller->date_of_birth ?></td>
                            </tr>
                            <? endforeach; ?>
                            
                        </tbody>
                        
                      </table>
                            
                    <div id="DataTables_Table_0_info" class="dataTables_info">إظهار 6 من 7 من 7 مدخل</div><div id="DataTables_Table_0_paginate" class="dataTables_paginate paging_full_numbers"><a id="DataTables_Table_0_first" tabindex="0" class="first paginate_button">الأولى</a><a id="DataTables_Table_0_previous" tabindex="0" class="previous paginate_button">السابق</a><span><a tabindex="0" class="paginate_button">1</a><a tabindex="0" class="paginate_active">2</a></span><a id="DataTables_Table_0_next" tabindex="0" class="next paginate_button paginate_button_disabled">التالي</a><a id="DataTables_Table_0_last" tabindex="0" class="last paginate_button paginate_button_disabled">الأخيرة</a></div></div>                    
                    </div> 
            </div>
            
</div>
<?= form_close() ?>
