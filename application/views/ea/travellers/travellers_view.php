<?= form_open_multipart() ?>
<div class="row-fluid">              
    <div class="widget">
        <div class="head dark">
            <div class="icon"><i class="icos-pencil2"></i></div>
            <h2><?= lang('travellers_passport_reservation_form')?></h2>
        </div>                        
        <div class="block-fluid">

            <!--    
            <div class="row-form">                 
            <div class="span6">
                    <div class="span4" style="text-align:center">Season:</div>
                    <div class="span8"><select name="s_example">
                    <option>option1</option>
                    </select></div>
            </div>
            <div class="span6">
                    <div class="span4" style="text-align:center">Company:</div>
                    <div class="span8"><select name="s_example">
                    <option>option1</option>
                    </select></div>
            </div>    
            </div>
            -->


            <div class="row-form">
                <div class="span4">
                    <div class="span4" style="text-align:center"><?= lang('travellers_passport_number')?>:</div>
                    <div class="span8"><input type="text" name="passport_no"/></div>
                </div>
                <div class="span4">
                    <div class="span4" style="text-align:center"><?= lang('travellers_nationality')?>:</div>
                    <div class="span8">   

                        <?= form_dropdown('nationality_id', ddgen('erp_countries', array('erp_country_id', name())), set_value('nationality_id', '0')," name='s_example' class='select' style='width:100%;' ") ?>

                    </div>
                </div>
                <div class="span4">
                    <div class="span4" style="text-align:center"><?= lang('travellers_gender_id') ?>:</div>
                    <div class="span8">
                        <?= form_dropdown('gender_id', ddgen('erp_gender', array('erp_gender_id', name())), set_value('gender_id', '0')) ?>
                    </div>
                </div>
            </div>  

            <div class="row-form">

                <div class="span8">

                    <div style="float:right" >
                        <div class="span8" style="text-align:right"><?= lang('travellers_personality')?>:</div>
                        <div class="span10">
                            <?= form_dropdown('erp_title_id', ddgen('erp_titles', array('erp_title_id', name())), set_value('erp_title_id', '0')) ?>
                        </div>
                    </div>
                    <div style="float:left">
                        <div class="span2" style="text-align:center"><?= lang('travellers_english_name')?>:</div>
                        <div class="span10">
                            <input type="text" name="first_name_la" class="input-small" placeholder="<?= lang('travellers_first_name')?>"/> 
                            <input type="text" name="second_name_la " class="input-mini" placeholder="<?= lang('travellers_second_name')?>"/>
                            <input type="text" name="third_name_la" class="input-mini" placeholder="<?= lang('travellers_third_name')?>"/>
                            <input type="text" name="fourth_name_la" class="input-mini" placeholder="<?= lang('travellers_last_name')?>"/>
                        </div>
                    </div>
                    <div style=" float:left; ">
                        <div class="span2" style="text-align:center"><?= lang('travellers_arabic_name')?>:</div>
                        <div class="span10">
                            <input type="text" name="first_name_ar" class="input-small" placeholder="<?= lang('travellers_first_name')?>"/> 
                            <input type="text" name="second_name_ar " class="input-mini" placeholder="<?= lang('travellers_second_name')?>"/>
                            <input type="text" name="third_name_ar" class="input-mini" placeholder="<?= lang('travellers_third_name')?>"/>
                            <input type="text" name="fourth_name_ar" class="input-mini" placeholder="<?= lang('travellers_last_name')?>"/>
                        </div>
                    </div>
                </div>
                <div class="span4">
                    <div class="fileupload fileupload-new" data-provides="fileupload">
                        <div style="float:right; margin:0 0 0 5px ">
                            <span class="btn btn-file btn-primary">
                                <span class="fileupload-new"><?= lang('travellers_select_image')?></span>
                                <span class="fileupload-exists"><?= lang('travellers_change')?></span>
                                <input type="file" name="picture" /></span>
                            <a href="#" class="btn fileupload-exists" data-dismiss="fileupload"><?= lang('travellers_remove')?></a>
                        </div>
                        <div class="fileupload-preview thumbnail" style="width: 100px; height: 75px; float:right">
                            <img src="img/noimg.gif"/>
                        </div>
                    </div>
                </div>
            </div> 


            <div class="row-form">
                <div class="span4">
                    <div class="span4" style="text-align:center"><?= lang('travellers_nationality_id')?>:</div>
                    <div class="span8"><input type="text" name="nationality_id"/></div>
                </div>
            </div>

            <div class="row-form">
                <div class="span4">
                    <div class="span4" style="text-align:center"><?= lang('travellers_passport_type')?>:</div>
                    <div class="span8">






                        <?= form_dropdown('passport_type_id', ddgen('erp_passporttypes', array('erp_passporttype_id', name())), set_value('passport_type_id', '0')) ?>


                    </div>
                </div>
                <div class="span4">
                    <div class="span4" style="text-align:center"><?= lang('travellers_education_levels')?>:</div>
                    <div class="span8">

                        <?= form_dropdown('educational_level_id', ddgen('erp_educationlevels', array('erp_educationlevel_id', name())), set_value('educational_level_id', '0')) ?>

                    </div>
                </div>
                <div class="span4">
                    <div class="span4" style="text-align:center"><?= lang('travellers_family_status')?>:</div>
                    <div class="span8">

                        <?= form_dropdown('marital_status_id', ddgen('erp_maritalstatus', array('erp_maritalstatus_id', name())), set_value('marital_status_id', '0')) ?>

                    </div>
                </div>
            </div>

            <div class="row-form">
                <div class="span4">
                    <div class="span4" style="text-align:center"><?= lang('travellers_country')?>:</div>
                    <div class="span8">

                        <?= form_dropdown('erp_country_id', ddgen('erp_countries', array('erp_country_id', name())), set_value('erp_country_id', '0')," name='s_example' class='select' style='width:100%;' ") ?>

                    </div>
                </div>
                <div class="span4">
                    <div class="span4" style="text-align:center"><?= lang('travellers_birth_country')?>:</div>
                    <div class="span8">

                        <?= form_dropdown('birth_country_id', ddgen('erp_countries', array('erp_country_id', name())), set_value('birth_country_id', '0')," name='s_example' class='select' style='width:100%;' ") ?>

                    </div>
                </div>
                <div class="span4">
                    <div class="span4" style="text-align:center"><?= lang('travellers_export_country')?>:</div>
                    <div class="span8">



                        <?= form_dropdown('passport_issuing_country_id', ddgen('erp_countries', array('erp_country_id', name())), set_value('passport_issuing_country_id', '0')," name='s_example' class='select' style='width:100%;' ") ?>

                    </div>
                </div>
            </div>




            <div class="row-form">
                <div class="span4">
                    <div class="span4" style="text-align:center"><?= lang('travellers_birth_place')?>:</div>
                    <div class="span8"><input type="text" name="birth_city" /></div>
                </div>
                <div class="span4">
                    <div class="span4" style="text-align:center"><?= lang('travellers_birth_date')?>:</div>
                    <div class="span8">
                        <input type="text" name="date_of_birth" class="datepicker" />
                    </div>
                </div>
            </div>

            <div class="row-form">
                <div class="span4">
                    <div class="span4" style="text-align:center"><?= lang('travellers_passport_issuing_city')?>:</div>
                    <div class="span8"><input type="text" name="passport_issuing_city"/></div>
                </div>
                <div class="span4">
                    <div class="span4" style="text-align:center"><?= lang('travellers_passport_issue_date')?>:</div>
                    <div class="span8"><input type="text" name="passport_issue_date" class="datepicker"/></div>
                </div>
                <div class="span4">
                    <div class="span4" style="text-align:center"><?= lang('travellers_passport_expiry_date')?>:</div>
                    <div class="span8"><input type="text" name="passport_expiry_date" class="datepicker"/></div>
                </div>
            </div>

            <div class="row-form">
                <div class="span8">
                    <div class="span2" style="text-align:center"><?= lang('travellers_address')?>:</div>
                    <div class="span10"><input type="text" name="address"/></div>
                </div>
                <div class="span4">
                    <div class="span4" style="text-align:center"><?= lang('travellers_phone')?>:</div>
                    <div class="span8"><input type="text" name="mobile"/></div>
                </div>
            </div>

            <div class="row-form">
                <div class="span8">
                    <div class="span2" style="text-align:center"><?= lang('travellers_me7rm')?>:</div>
                    <div class="span10">

                          <!-- //ddgen('safa_trip_travellers', array('safa_trip_traveller_id' , 'first_'.name()), array('safa_trip_id'=> $this->uri->segment(4), 'gender_id' => '1')), set_value('gender_id', '0') -->
                        <?= form_dropdown('relative_persons', $relative_persons, set_value('relative_persons')) ?>             

                    </div>
                </div>
                <div class="span4">
                    <div class="span4" style="text-align:center"><?=lang('travellers_relations')?>:</div>
                    <div class="span8">



                        <?= form_dropdown('relative_relation_id', ddgen('erp_relations', array('erp_relation_id', name())), set_value('relative_relation_id', '0')) ?>                                

                    </div>
                </div>
            </div>


            <div class="row-form">
                <div class="span12">
                    <div class="span2" style="text-align:center"><?= lang('travellers_notes')?>:</div>
                    <div class="span10"><textarea placeholder="<?=lang('travellers_write_note')?>"></textarea></div>
                </div>
            </div>


            <div class="toolbar bottom TAC">
                <input type="submit" class="btn btn-primary" value="<?= lang('global_submit')?>" /> 
                <button class="btn btn-primary"> <?= lang('travellers_delete')?> </button> 
                <button class="btn btn-primary"> <?= lang('travellers_close')?></button>
            </div>

        </div>
    </div>
    <?= form_close() ?>
 