<!DOCTYPE html>
<html lang="<?= lang('global_lang') ?>" xmlns="http://www.w3.org/1999/xhtml" dir="<?= lang('DIR') ?>" style="overflow-x: hidden">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <script src="<?= NEW_CSS_JS . '_' . lang('DIR') ?>/jquery-1.9.1.js"></script>
        <script src="<?= NEW_CSS_JS . '_' . lang('DIR') ?>/jquery-ui.js"></script>
        <link href="<?= NEW_CSS_JS . '_' . lang('DIR') ?>/validationEngine.jquery.css" rel="stylesheet" type="text/css" />
        <script type='text/javascript' src='<?= NEW_JS ?>/plugins/jquery/jquery-ui-1.10.1.custom.min.js'></script>
        <script type='text/javascript' src='<?= NEW_JS ?>/plugins/jquery/jquery-migrate-1.1.1.min.js'></script>
        <script type='text/javascript'src='<?= NEW_JS ?>/plugins/bootstrap/bootstrap.min.js'></script>
        <script type='text/javascript' src='<?= NEW_JS ?>/plugins/bootstrap/bootstrap-dropdown.js'></script>
        <script type='text/javascript' src='<?= NEW_JS ?>/plugins/bootstrap/bootstrap-fileupload.min.js'></script>
        <script type='text/javascript' src='<?= NEW_JS ?>/plugins/pnotify/jquery.pnotify.min.js'></script>
        <script type='text/javascript' src='<?= NEW_JS ?>/plugins/datatables/jquery.dataTables.min.js'></script>
        <script type='text/javascript' src="<?= NEW_JS ?>/plugins/uniform/jquery.uniform.min.js"></script>
        <script type='text/javascript' src="<?= NEW_JS ?>/plugins/scroll/jquery.custom-scrollbar.js"></script>
        <script type='text/javascript' src='<?= NEW_JS ?>/plugins.js'></script>
        <script type='text/javascript' src='<?= JS ?>/plugins/lang-ar.js'></script>
        <script language="javascript" type="text/javascript" src="<?= NEW_CSS_JS ?>/wizard/formToWizard.js"></script>
        <script src="<?= NEW_JS ?>/jquery.validationEngine-<?= name(true) ?>.js" type="text/javascript" charset="utf-8"></script>
        <script src="<?= NEW_JS ?>/jquery.validationEngine.js" type="text/javascript" charset="utf-8"></script>
        <title><?php echo lang('login'); ?></title>
        <meta name="viewport" content="width=device-width">
        <link href="<?= NEW_CSS_JS . '_' . lang('DIR') ?>/common-css.css" rel="stylesheet" type="text/css" />
        <style>
                a{margin: 5px;}

                body{
                    font-family: "GE SS Two Medium";
                    font-size:12px;
                    width:98%;
                }   
                h4, h3{font-weight: 600;}     
                .logo {
                    padding-right: 20px;
                    display: block;
                    text-align: center;
                    margin: 0 auto;

                }
                .banner {
                    background-image: url("<?php echo STATIC_FOLDER_DIR; ?>/front/banner2.png");
                    background-repeat: no-repeat;
                    height: 260px;
                    margin: 0 auto;
                    position: relative;
                    width: 640px;
                    margin-top: 120px;
                }
                .reg {

                    margin-top: 40px;
                }
                .reg2 {

                    margin-top: 20px;
                }
                .reg3 {

                    margin-top: 20px;
                }
                .reg-btn {padding: 5px 8px !important;

                          margin-top: 0px !important;
                          color: #000333;
                          background: #ddc889;
                }
                .reg-btn3 { padding: 0px 4px !important;
                            color: #000333;
                            background: #ddc889;
                }
                .log {
                    margin-right: 10px;
                    margin-top: -110px;
                }
                input, textarea, .uneditable-input{margin-bottom: 3px;}
        </style>
        <style>
            
/*            #note_en{text-align: left!important;}
            .span3.log_img1 {float: left!important;}
.span3.log_t1 {float: left!important;}*/
            
            
        </style>
    </head>
    <body style="padding: 10px;">
        <?= form_open('ea', "id='validate'", FALSE, TRUE) ?> 
        <div class="row-fluid"> 
            <div class=" logo "><img src="<?php echo STATIC_FOLDER_DIR; ?>/front/logo.png" >  </div>
            <div class="span5 TAL log">
                <div class="span11">
                    <div class="span12"><h4><?= lang('wellcom_title') ?></h4></div>
                    <div class="span12">
                        <div class="span5">
                            <div class="span12">
                                <input type="text" name="email" tabindex="1" value="<?= set_value("email") ?>" placeholder="<?= lang('username_email') ?>" class="validate[required] input-huge TAL"/>
                                <span class='bottom' style='color:red'>
                                    <?= form_error('login') ?>
                                </span>
                            </div> 
                        </div>

                        <div class="span5">
                            <div class="span12">
                                <input type="password" name="password" tabindex="2" value="<?= set_value("password", "") ?>" placeholder="<?= lang('login_placeholder_password') ?>" class="validate[required] input-huge TAL"/>
                                <span class='bottom' style='color:red'>
                                    <?= form_error('password') ?>
                                </span>

                            </div>

                            <div class="span8 log_check"><?= lang('login_rememberme') ?><input class="log_check" type="checkbox" name="remember" value="1" <?= set_checkbox('remember', '0'); ?> tabindex="3" /></div>  
                            <div class="span4 log_fbtn"> 
                                <a href="<?php echo site_url('login/forget_password/ea'); ?>" class="btn reg-btn3 "><?= lang("login_forget_password") ?></a>
                            </div>
                        </div>
                        <div class="span2 "style="margin-right: -15px;">
                            <input type="submit" class="btn reg-btn" tabindex="3" value="<?= lang('login_label') ?>"/>
                        </div>    
                    </div> 
                    <?//= validation_errors() ?>
                </div>
            </div>
        </div>
        <?= form_close() ?>

        <?= form_open("ea/register", "id='validate', name='registeration' ") ?>
        <div class="row-fluid">

            <div class="span12">

                <div class="span3 TAL reg2">
                    <div class="span12"> <h3><?= lang('registeration') ?></h3> </div>
                    <div class="span12">
                        <input type ='text' name='name_ar' value='<?= set_value('name_ar') ?>'  class="validate[required] span11 TAL"  placeholder="<?= lang('name_ar') ?>" /> 
                        <span class='bottom' style='color:red'>
                            <?= form_error('name_ar') ?>
                        </span>
                    </div>
                    <div class="span12">
                        <input type ='text' name='name_la' value='<?= set_value('name_la') ?>' class="validate[required] span11 TAL"  placeholder="<?= lang('name_la') ?>" /> 
                        <span class='bottom' style='color:red'>
                            <?= form_error('name_la') ?>
                        </span>

                    </div>
                    <div class="span12">
                        <?= form_dropdown('country', ddgen('erp_countries', array('erp_country_id', name())), set_select('country'), " class='validate[required] span11 TAL' placeholder='" . lang('country') . "' ") ?>
                        <span class='bottom' style='color:red'>
                            <?= form_error('country') ?>
                        </span>
                    </div>
                    <div class="span12">
                        <?= form_input('phone', set_value('phone'), 'class="validate[required,custom[phone]] span11 TAL" placeholder="' . lang('phone') . '"') ?>
                        <span class='bottom' style='color:red'>
                            <?= form_error('phone') ?>
                        </span>  
                    </div>


                    <div class="span12"><?= form_input('fax', set_value('fax'), 'class="validate[required,custom[phone]]  span11 TAL" placeholder="' . lang('fax') . '"') ?>
                        <span class='bottom' style='color:red'>
                            <?= form_error('fax') ?>
                        </span>  
                    </div>

                    <div class="span12">
                        <?= form_input('email', set_value('email'), "class='validate[required,custom[email]] span11 TAL' placeholder='" . lang('email') . "' ") ?>
                        <span class='bottom' style='color:red'>
                            <?= form_error('email') ?>
                        </span>
                    </div>
                    <div class="span12">
                        <?= form_input('username', set_value('username'), "class='validate[required,custom[UserName]] span11 TAL' id='username' placeholder='" . lang('username') . "'") ?>
                        <span class='bottom' style='color:red'>
                            <?= form_error('username') ?>
                        </span>
                    </div>
                  <div class="span12">
                    <div class="span6">
                        <?= form_password('rpassword', set_value('rpassword'), "class='validate[required] span11 TAL' placeholder='" . lang('password') . "' id='password'") ?>
                        <span class='bottom' style='color:red'>
                            <?= form_error('rpassword') ?>
                        </span>
                    </div>

                    <div class="span6">
                        <?= form_password('confpassword', set_value('confpassword'), "class='validate[required,equals[password]] span11 TAL' placeholder='" . lang('repeat_password') . "'") ?>
                        <span class='bottom' style='color:red'>
                            <?= form_error('confpassword') ?>
                        </span>
                    </div>
                  </div>

                    <div class="span12"><h3> <?= lang('umrah_contract_data') ?></h3></div>  
                    <div class="span12">
                        <?php
                        $cont_username = '';
                        $cont_password = '';
                        if (isset($contract_username)) {
                            $cont_username = $contract_username;
                        }
                        if (isset($contract_password)) {
                            $cont_password = $contract_password;
                        }
                        ?>
                        <?= form_input('contract_username', set_value('contract_username', $cont_username), "class='validate[required] span11 TAL' id='contract_username' placeholder='" . lang('contract_username') . "'") ?>
                        <span class='bottom' style='color:red'>
                            <?= form_error('contract_username') ?>
                        </span>
                    </div>
                    <div class="span12">
                        <?= form_password('contract_password', set_value('contract_password', $cont_password), "class='validate[required] span11 TAL' placeholder='" . lang('contract_password') . "'") ?>
                        <span class='bottom' style='color:red'>
                            <?= form_error('contract_password') ?>
                        </span>
                    </div>
                    <div class="span12"><?= lang('accept_license') ?><input type="checkbox" id="chk_accept_license" name="chk_accept_license"> </div>
                    <div class="span11 TAR"> 
                        <input type="submit" value="<?= lang('registeration') ?>" id="smt_registeration" onclick="return check_accepting_license();" class="btn reg-btn">

                    </div>


                </div> 

                <div class="span7"><div class="banner"></div> </div>


                <div id="note_en" class="span2 TAL reg">
                    <div class="span12"><h3><?= lang('what_safa_features') ?></h3></div> 
                    <div class="span12">
                        <div class="span3 log_img1"><img src="<?php echo STATIC_FOLDER_DIR; ?>/front/SafaLiveInterface-1.png"></div><div class="span9 log_t1">
                            <h4 ><?= lang('hotels_management') ?></h4></div>
                    </div>
                    <div class="span12">
                        <div class="span3 log_img1"><img src="<?php echo STATIC_FOLDER_DIR; ?>/front/SafaLiveInterface-2.png"></div><div class="span9">
                            <h4> <?= lang('trips_managment') ?></h4> </div> </div>
                    <div class="span12">
                        <div class="span3 log_img1"><img src="<?php echo STATIC_FOLDER_DIR; ?>/front/SafaLiveInterface-3.png"></div><div class="span9">
                            <h4> <?= lang('link_with_uo_company') ?></h4></div>
                    </div>
                    <div class="span12">
                        <div class="span3 log_img1"><img src="<?php echo STATIC_FOLDER_DIR; ?>/front/SafaLiveInterface-4.png"></div><div class="span9">
                            <h4> <?= lang('umrah_visa_management') ?></h4></div>
                    </div>
                    <div class="span12">
                        <div class="span3 log_img1"><img src="<?php echo STATIC_FOLDER_DIR; ?>/front/SafaLiveInterface-5.png"></div><div class="span9">
                            <h4> <?= lang('transporters_cycle_orders_management') ?></h4></div>
                    </div>
                    <div class="span12">
                        <div class="span3 log_img1"><img src="<?php echo STATIC_FOLDER_DIR; ?>/front/SafaLiveInterface-6.png"></div><div class="span9">
                            <h4> <?= lang('trips_arriving_leaving_follow') ?></h4></div>
                    </div>
                </div>
            </div>
        </div>
        <?= form_close() ?>



        <div class="row-fluid TAC chromeifview">
            <a href="<?php echo site_url('login/set_language/1'); ?>">English  US</a>
            <a href="<?php echo site_url('login/set_language/2'); ?>">العربية</a>
            <a href="<?php echo site_url('login/set_language/3'); ?>">Türk</a>
        </div>



        <script type="text/javascript">
            function check_accepting_license()
            {
                if (!document.getElementById('chk_accept_license').checked) {
                    alert('please accept licence');
                    return false;
                }
            }
            $('#username').change(function()
            {
                var username = $('#username').val();

                var dataString = 'username=' + username;

                $.ajax
                        ({
                            type: 'POST',
                            url: '<?php echo base_url() . 'ea/login/check_username_availability_ajax'; ?>',
                            data: dataString,
                            cache: false,
                            success: function(html)
                            {
                                //alert(html);
                                $("#dv_username_availability").html(html);
                            }
                        });

            });

            $('#smt_registeration').click(function()
            {

                if ($('#hdn_username_not_available_error').val() == 1) {
                    alert('');
                    return false;
                }

            });
            

$("#validate").validationEngine({
prettySelect : true,
useSuffix: "_chosen",
promptPosition : "topRight:-150"
//promptPosition : "bottomLeft"
});
</script>
<? $this->load->view("analytics") ?>
    </body>
</html>
