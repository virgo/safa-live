<!DOCTYPE html>
<html lang="<?= lang('global_lang') ?>" xmlns="http://www.w3.org/1999/xhtml" dir="<?= lang('DIR') ?>" style="overflow-x: hidden">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <script src="<?= NEW_CSS_JS . '_' . lang('DIR') ?>/jquery-1.9.1.js"></script>
        <script src="<?= NEW_CSS_JS . '_' . lang('DIR') ?>/jquery-ui.js"></script>
        <link href="<?= NEW_CSS_JS . '_' . lang('DIR') ?>/validationEngine.jquery.css" rel="stylesheet" type="text/css" />
        <script type='text/javascript' src='<?= NEW_JS ?>/plugins/jquery/jquery-ui-1.10.1.custom.min.js'></script>
        <script type='text/javascript' src='<?= NEW_JS ?>/plugins/jquery/jquery-migrate-1.1.1.min.js'></script>
        <script type='text/javascript'src='<?= NEW_JS ?>/plugins/bootstrap/bootstrap.min.js'></script>
        <script type='text/javascript' src='<?= NEW_JS ?>/plugins/bootstrap/bootstrap-dropdown.js'></script>
        <script type='text/javascript' src='<?= NEW_JS ?>/plugins/bootstrap/bootstrap-fileupload.min.js'></script>
        <script type='text/javascript' src='<?= NEW_JS ?>/plugins/pnotify/jquery.pnotify.min.js'></script>
        <script type='text/javascript' src='<?= NEW_JS ?>/plugins/datatables/jquery.dataTables.min.js'></script>
        <script type='text/javascript' src="<?= NEW_JS ?>/plugins/uniform/jquery.uniform.min.js"></script>
        <script type='text/javascript' src="<?= NEW_JS ?>/plugins/scroll/jquery.custom-scrollbar.js"></script>
        <script type='text/javascript' src='<?= NEW_JS ?>/plugins.js'></script>
        <script type='text/javascript' src='<?= JS ?>/plugins/lang-ar.js'></script>
        <script language="javascript" type="text/javascript" src="<?= NEW_CSS_JS ?>/wizard/formToWizard.js"></script>
        <script src="<?= NEW_JS ?>/jquery.validationEngine-<?= name(true) ?>.js" type="text/javascript" charset="utf-8"></script>
        <script src="<?= NEW_JS ?>/jquery.validationEngine.js" type="text/javascript" charset="utf-8"></script>
        <? $this->load->view("analytics") ?>
        <title><?php echo lang('login'); ?></title>
        <meta name="viewport" content="width=device-width">
            <link href="<?= NEW_CSS_JS . '_' . lang('DIR') ?>/common-css.css" rel="stylesheet" type="text/css" />
            <style>
                a{margin: 5px;}

                body{
                    font-family: "GE SS Two Medium";
                    font-size:12px;
                    width:98%;
                }   
                h4, h3{font-weight: 600;}     
                .logo {
                    padding-right: 20px;
                    display: block;
                    text-align: center;
                    margin: 0 auto;

                }
                .banner {
                    background-image: url("<?php echo STATIC_FOLDER_DIR; ?>/front/banner2.png");
                    background-repeat: no-repeat;
                    height: 260px;
                    margin: 0 auto;
                    position: relative;
                    width: 640px;
                    margin-top: 120px;
                }
                .reg {

                    margin-top: 40px;
                }
                .reg2 {

                    margin-top: 20px;
                }
                .reg3 {

                    margin-top: 20px;
                }
                .reg-btn {padding: 5px 8px !important;

                          margin-top: 0px !important;
                          color: #000333;
                          background: #ddc889;
                }
                .reg-btn3 { padding: 0px 4px !important;
                            color: #000333;
                            background: #ddc889;
                }
                .log {
                    margin-right: 10px;
                    margin-top: -110px;
                }
                .rellog {margin-top: 2.2%;}
                input, textarea, .uneditable-input{margin-bottom: 3px; );
                }
                .nomin {min-height: 0px !important;}
            </style>
            <script>
                var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
                document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
            </script>
            <script>
                  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
                  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
                  <? if(preg_match('/safalive/', site_url())): ?>
                  ga('create', 'UA-47714827-1', 'safalive.com');
                  <? else: ?>
                  ga('create', 'UA-47714827-1', 'umrah-booking.com');            
                  <? endif ?>
                  ga('send', 'pageview');
            </script>
    </head>
    <body style="padding: 10px;">
        <div class="row-fluid"> 
            <div class=" logo "><img src="<?php echo STATIC_FOLDER_DIR; ?>/front/logo.png" >  </div>
        </div>
        <?= form_open(false, "id='validate' name='registeration' method='post'") ?>
        <div class="row-fluid">
            <div class="span12 nomin"></div>
            <div class="span12 nomin">
                <div class="span3 TAL reg2  rellog ">
                    <div class="span12"><h4>مرحبا . قم بادخال بيانات الدخول</h4></div>
                    <div class="span12">
                        <div class="span12">
                            <input type="email" name="email" tabindex="1" value="<?= set_value('email') ?>" placeholder="الايميل" class="validate[required] input-huge TAL">
                        </div> 
                    </div>
                    <div class="span12">
                        <div class="span12">
                            <input type="password" name="password" tabindex="2" value="" placeholder="كلمة السر" class="validate[required] input-huge TAL">
                        </div>
                        <div class="span6 log_check"><input class="log_check" type="checkbox" name="remember" value="1" tabindex="3">تذكرني</div>
                        <div class="span6 Fleft" style="margin-right: -15px;">
                            <input type="submit" class="btn Fleft reg-btn" tabindex="3" value="دخول">
                        </div>
                        <span class="bottom" style="color:red">
                            <?= validation_errors() ?>
                        </span>
                        <div class="span8 log_fbtn " > 
                            <a href="<?= site_url('login/forget_password/hm') ?>" class="btn reg-btn3 Fleft queryarr" >نسيت كلمة المرور</a>
                        </div>
                    </div>
                </div>
                <div class="span7"><div class="banner"></div></div>
                <div id="note_en" class="span2 TAL reg">
                    <div class="span12"><h3><?= lang('what_safa_features') ?></h3></div> 
                    <div class="span12">
                        <div class="span3 log_img1"><img src="<?php echo STATIC_FOLDER_DIR; ?>/front/SafaLiveInterface-1.png"></div><div class="span9 log_t1">
                            <h4 ><?= lang('hotels_management') ?></h4></div>
                    </div>
                    <div class="span12">
                        <div class="span3 log_img1"><img src="<?php echo STATIC_FOLDER_DIR; ?>/front/SafaLiveInterface-2.png"></div><div class="span9">
                            <h4> <?= lang('trips_managment') ?></h4> </div> </div>
                    <div class="span12">
                        <div class="span3 log_img1"><img src="<?php echo STATIC_FOLDER_DIR; ?>/front/SafaLiveInterface-3.png"></div><div class="span9">
                            <h4> الربط مع الوكيل الخارجي</h4></div>
                    </div>
                    <div class="span12">
                        <div class="span3 log_img1"><img src="<?php echo STATIC_FOLDER_DIR; ?>/front/SafaLiveInterface-4.png"></div><div class="span9">
                            <h4> ادارة عقود العمرة</h4></div>
                    </div>
                    <div class="span12">
                        <div class="span3 log_img1"><img src="<?php echo STATIC_FOLDER_DIR; ?>/front/SafaLiveInterface-5.png"></div><div class="span9">
                            <h4> <?= lang('transporters_cycle_orders_management') ?></h4></div>
                    </div>
                    <div class="span12">
                        <div class="span3 log_img1"><img src="<?php echo STATIC_FOLDER_DIR; ?>/front/SafaLiveInterface-6.png"></div><div class="span9">
                            <h4> <?= lang('trips_arriving_leaving_follow') ?></h4></div>
                    </div>
                </div>
              
            </div>
        </div> <?= form_close() ?>
          <div class="row-fluid TAC chromeifview">
            <a href="<?php echo site_url('login/set_language/1'); ?>">English  US</a>
            <a href="<?php echo site_url('login/set_language/2'); ?>">العربية</a>
            <a href="<?php echo site_url('login/set_language/3'); ?>">Türk</a>
        </div>
    </body>
</html>
