    <div class="login" id="login">
        <div class="wrap">
                   <? if (validation_errors()): ?>
                 <div style="direction:<?=lang("global_direction")?>" >
                    <?php echo validation_errors(); ?>
                </div>
         <? endif ?>
            <h1><?=  lang('wellcom_title')?></h1>
            <?=form_open(false,"id='validate'", FALSE, TRUE)?>  
            <div class="row-fluid">
                <div class="input-prepend">
                    <span class="add-on"><i class="icon-user"></i></span>
                    <input type="text" name="email" value="<?=set_value("email")?>" placeholder="<?=  lang('login_placeholder_username')?>" class="validate[required]"/>
                </div>                                                 
                <div class="input-prepend">
                    <span class="add-on"><i class="icon-exclamation-sign"></i></span>
                    <input type="password" name="password" value="<?=set_value("password","")?>" placeholder="<?=  lang('login_placeholder_password')?>" class="validate[required]"/>
                </div>
                <div class="dr"><span></span></div>  
                <div class="input-prepend">
                    <?= form_dropdown('language', $languages, set_value('language',2)) ?>
                </div>
            </div>                
            <div class="row-fluid">
                <div class="span8 remember">                    
                    <input type="checkbox" name="remember" value="1" <?= set_checkbox('remember', '0'); ?>/><?=  lang('login_rememberme')?>                    
                </div>
                <div class="span4    TAR">
                    <input type="submit" class="btn btn-block btn-primary" value="<?=  lang('login_label')?>"/>
                </div>
            </div>
          <?=form_close()?>
            <div class="dr"><span></span></div>
            <div class="row-fluid">
                <div class="span8">                    
                    <button class="btn btn-block" value="<?=lang("login_forget_password")?>" onClick="loginBlock('#forgot');"><?=  lang('login_forget_password_q')?></button>
                </div>
                <div class="span4">
                    <!--<button class="btn btn-warning btn-block" onClick="loginBlock('#sign');">Registration</button>-->
                </div>
            </div>            
        </div>
    </div>
    
    <div class="login" id="sign">
        <div class="wrap">
            <h1>Registration</h1>
            
            <div class="row-fluid">
                <div class="input-prepend">
                    <span class="add-on"><i class="icon-user"></i></span>
                    <input type="text" name="login" placeholder="Login"/>
                </div>
                <div class="input-prepend">
                    <span class="add-on"><i class="icon-envelope"></i></span>
                    <input type="text" name="login" placeholder="E-mail"/>
                </div>                
                <div class="dr"><span></span></div>                
                <div class="input-prepend">
                    <span class="add-on"><i class="icon-exclamation-sign"></i></span>
                    <input type="text" name="password" placeholder="Password"/>
                </div>                          
                <div class="input-prepend">
                    <span class="add-on"><i class="icon-question-sign"></i></span>
                    <input type="text" name="re-password" placeholder="Re-Password"/>
                </div>                                
                <div class="dr"><span></span></div>                                
            </div>
            <div class="row-fluid">
                <div class="span8 remember">
                    <input type="checkbox"/> I agree with terms...
                </div>
                <div class="span4 TAR">
                    <button class="btn btn-block btn-primary">Registration</button>
                </div>
            </div>
            <div class="dr"><span></span></div>
            <div class="row-fluid">
                <div class="span4">                    
                    <button class="btn btn-block" onClick="loginBlock('#login');">Back</button>
                </div>                
            </div>             
        </div>
    </div>    
    
    <div class="login" id="forgot">
        <div class="wrap">
         <? if (validation_errors()): ?>
                 <div>
             <?php echo validation_errors(); ?>
                </div>
         <? endif ?>
            <h1><?=lang("login_forget_password")?></h1>
            <?=form_open("login/forget_password/admin","id='validate'")?>
            <div class="row-fluid">
                <p><?=lang("recover_password")?></p>
                <div class="input-prepend">
                    <span class="add-on"><i class="icon-envelope"></i></span>
                    <input type="text" name="email" placeholder="<?=  lang('login_placeholder_email')?>" value="<?=set_value("email")?>" class="validate[required]"/>
                </div>                                                           
                <div class="dr"><span></span></div>                               
            </div>                   
            <div class="row-fluid">
                <div class="span4">                    
                    <input type="button" value="<?=lang("login_back")?>" class="btn btn-block" onClick="loginBlock('#login')"/>
                </div>                                
                <div class="span4"></div>
                <div class="span4 TAR">
                    <input type="submit" value="<?=lang("login_recover")?>" class="btn btn-block btn-primary"  />
                </div>
            </div>
             <?=form_close()?>
        </div>
    </div> 
<script>
   <?if(isset($email_error)):?>
        document.getElementById("login").style.cssText="left: 70%; opacity: 0; display: none";
        document.getElementById("forgot").style.cssText="opacity: 1; display: block; left: 50%";
       <?else:?>
           document.getElementById("forgot").style.cssText="left: 70%; opacity: 0; display: none";
           document.getElementById("login").style.cssText="opacity: 1; display: block; left: 50%";
   <?endif;?> 
</script>
<? $this->load->view("analytics") ?>