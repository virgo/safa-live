<!--<!DOCTYPE html>

<html lang="<?= lang('global_lang') ?>"	xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title><?= lang("login_forget_password") ?></title>
        <meta name="viewport" content="width=device-width">
            
            <script src="<?= NEW_CSS_JS . '_' . lang('DIR') ?>/jquery-1.9.1.js"></script>
            <script src="<?= NEW_CSS_JS . '_' . lang('DIR') ?>/jquery-ui.js"></script>
            <link href="<?= NEW_CSS_JS . '_' . lang('DIR') ?>/validationEngine.jquery.css" rel="stylesheet" type="text/css" />
            <link href="<?= NEW_CSS_JS . '_' . lang('DIR') ?>/tipsy.css" rel="stylesheet" type="text/css" />
            <script type='text/javascript' src='<?= NEW_JS . '_' . lang('DIR') ?>/plugins/jquery/jquery-ui-1.10.1.custom.min.js'></script>
            <script type='text/javascript' src='<?= NEW_JS . '_' . lang('DIR') ?>/plugins/jquery/jquery-migrate-1.1.1.min.js'></script>
            <script type='text/javascript' src='<?= NEW_JS . '_' . lang('DIR') ?>/plugins/bootstrap/bootstrap.min.js'></script>
            <script type='text/javascript' src='<?= NEW_JS . '_' . lang('DIR') ?>/plugins/bootstrap/bootstrap-dropdown.js'></script>
            <script type='text/javascript' src='<?= NEW_JS . '_' . lang('DIR') ?>/plugins/bootstrap/bootstrap-fileupload.min.js'></script>
            <script type='text/javascript' src='<?= NEW_JS . '_' . lang('DIR') ?>/plugins/pnotify/jquery.pnotify.min.js'></script>
            <script type='text/javascript' src='<?= NEW_JS . '_' . lang('DIR') ?>/plugins/datatables/jquery.dataTables.min.js'></script>
            <script type='text/javascript' src="<?= NEW_JS . '_' . lang('DIR') ?>/plugins/uniform/jquery.uniform.min.js"></script>
            <script type='text/javascript' src="<?= NEW_JS . '_' . lang('DIR') ?>/plugins/scroll/jquery.custom-scrollbar.js"></script>
            <script type='text/javascript' src='<?= NEW_JS . '_' . lang('DIR') ?>/plugins.js'></script>
            <script type='text/javascript' src='<?= JS ?>/plugins/lang-ar.js'></script>
            <script language="javascript" type="text/javascript" src="<?= NEW_CSS_JS . '_' . lang('DIR') ?>/wizard/formToWizard.js"></script>
            <script src="<?= NEW_JS . '_' . lang('DIR') ?>/jquery.validationEngine-<?= name(true) ?>.js" type="text/javascript" charset="utf-8"></script>
            <script src="<?= NEW_JS . '_' . lang('DIR') ?>/jquery.validationEngine.js" type="text/javascript" charset="utf-8"></script>-->
            <style>
                body {background: none;}
                
                a{margin: 5px;}
                .reg-btn {
                    padding: 2px 8px !important;
                    color: #000333;
                    background: #ddc889;
                }
                .logo2 {
                    display: block;
                    text-align: center;
                    margin: 0 auto;
                }
                .banner2 {
                    background-image: url("<?php echo STATIC_FOLDER_DIR; ?>/front/banner2.png");
                    background-repeat: no-repeat;
                    height: 260px;
                    margin: 0 auto;
                    position: relative;
                    width: 640px;
                    margin-top: 10px;
                }
                .forg {
                    margin: 0 auto !important;
                    float: none !important;
                    overflow: hidden;
                }
                .reg2 {
                    margin-top: 70px;
                    margin-bottom: 70px;
                }
                .reg-btn2 {
                    padding: 4px 10px !important;
                    color: #000333;
                    background: #ddc889;
                    text-shadow: none;
                    margin-top: 2px !important;
                }
                .log {
                    margin-right: 10px;
                    margin-top: -110px;
                }

                input, textarea, .uneditable-input{margin-bottom: 3px;}
                .wizerd-div {
                    border-bottom: medium none !important;
                    margin: -10px 0px 20px 40px !important;
                    padding-top: 0;
                }
                .wizerd-div a {
                    background: none repeat scroll 0 0 #FAFAFA;
                    border: 1px solid #D5D6D6;
                    border-radius: 49px;
                    color: #663300;
                    display: inline-block;
                    margin: -30px 5px -17px -8px;
                    padding: 39px 12px 13px;
                    width: 90%;
                    text-align: center;
                }
                a {
                    color: #C09853;
                }
                .resalt-group2 {
                    -moz-box-sizing: border-box;
                    border: 2px solid #E1E1E1;
                    border-radius: 20px;
                    /*float: right;*/
                    margin: 1% 2%;
                    padding: 2%;
                    width: 96%;

                }
                .pad {padding: 10px;}
                .t2 {
                    font-size: 16px;
                    font-weight: 500;
                    margin-top: 35px;
                    color: #444343;
                }
                .f1 {
                    margin-bottom: 15px;
                    margin-top: 10px;
                    font-size: 13px;
                    font-weight: 500;
                    color: #88774A;
                }
                .false {
                    color: #800000;
                    margin-bottom: -15px;
                    margin-top: 15px;
                }
                .wizerd-div a h3 {
                    padding: -10px !important;
                    margin: 0px;
                    font-weight: 600;
                    font-size: 17px;
                    color: #8B742B;
                }
                .foot a {color: #333;}
                .forgot_password {
                    width: 50%;
                    margin: auto;
                }
                .font { font-family: "GE-SS-med" 
                        font-size: 12px;}
                .pad21left {padding-left:21px}
            </style>
    </head>
    <body style="padding: 10px;" align="center">
        <div class="row-fluid">
            <div>
                <div class="forgot_password">
                    <div class="span12"> <div class=" logo2 "><img src="<?php echo STATIC_FOLDER_DIR; ?>/front/logo.png" >  </div></div>
                    <?= form_open(false, "id='validate'") ?>    
                    <div class="span12 pad reg2">
                        <div class="span12 forg TAC resalt-group2 ">
                            <div class="wizerd-div TAL"><a><h3><?= lang("login_forget_password") ?></h3></a></div>
                            <div class="span12" style="padding-left: 30px;">
                                <div class="span10 false">
                                    <? if (validation_errors()): ?>
                                        <?= validation_errors(); ?>
                                    <? endif ?>
                                </div>
                                <div class="span10 t2 TAL"><?= lang('login_placeholder_email') ?></div>
                                <div class="span12">
                                    <div class="span9">
                                        <input type="text" name="email" placeholder="<?= lang('login_placeholder_email') ?>" value="<?= set_value("email") ?>" class="validate[required, custom[email]] span11 TAL"/>
                                    </div> 
                                    <div class="span1"><input type="submit" value="<?= lang("login_recover") ?>" class="btn reg-btn2" /></div>
                                </div>
                                <div class="span10 f1">
<!--                                    <div class="span10 TAL">
                                        <a href="<?= site_url('login/ea'); ?>" ><?= lang("there_is_no_account") ?></a>
                                    </div>  -->
                                </div>
                            </div>
                        </div> 
                        <div class="span12"></div>
                    </div> 
                    <?= form_close() ?>
                </div>
            </div>
        </div> 
        <div class="row-fluid TAC foot font">
            <a href="<?= site_url('login/set_language/1'); ?>">English(US)</a>
            <a href="<?= site_url('login/set_language/2'); ?>">العربية</a>
            <a href="<?= site_url('login/set_language/3'); ?>">Türk</a>
        </div>
    </body>
</html>
