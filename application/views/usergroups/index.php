<style>
    .widget {width:98%}
</style>
<div class="row-fluid">
    <div class="span12">
        <div class="widget">
            <div class="path-container Fright">
                <div class="icon"><i class="icos-pencil2"></i></div>
                <div class="path-name Fright">
                    <a href="<?= site_url($destination.'/dashboard') ?>"><?= lang('global_system_management') ?></a>
                </div><div class="path-arrow Fright"></div>
                <div class="path-name Fright">
                    <span><?= lang('global_usergroups') ?></span>
                </div>
            </div>
            <a title="<?= lang('global_add_new_record') ?>" href="<?= site_url("usergroups/manage") ?>" class="btn Fleft"><?= lang('global_add_new_record') ?></a>
        </div>
    </div>
    <div class="widget">
        <div class="widget-header">
            <div class="widget-header-icon Fright">
                <span class="icos-pencil2"></span>
            </div>
            <div class="widget-header-title Fright">
                <?= lang('global_usergroups') ?>
            </div>
        </div>
        <div class="widget-container">
            <table class="fsTable">
                <thead>
                    <tr>
                        <th>
                            <?= lang('global_usergroups') ?>
                        </th>
                        <th>
                            <?= lang('global_operations') ?>
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <? foreach($items as $item): ?>
                    <tr>
                        <td class="TAC">
                            <?= $item->{name()} ?>
                        </td>
                        <td class="TAC">
                            <a href="<?= site_url('usergroups/manage/'. $item->{$keys[2]}) ?>" title="<?= lang('global_edit') ?>"><span class="icon-pencil"></span></a>
                            <a href="<?= site_url('usergroups/delete/'. $item->{$keys[2]}) ?>" title="<?= lang('global_delete') ?>"><span class="icon-trash"></span></a>
                        </td>
                    </tr>
                    <? endforeach ?>
                </tbody>
            </table>  
        </div>
    </div>
</div>