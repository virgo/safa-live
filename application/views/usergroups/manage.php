<style>
    .widget {width:98%}
</style>
<div class="row-fluid">
    <div class="span12">
        <div class="widget">
            <div class="path-container Fright">
                <div class="icon"><i class="icos-pencil2"></i></div>
                <div class="path-name Fright">
                    <a href="<?= site_url($destination . '/dashboard') ?>"><?= lang('global_system_management') ?></a>
                </div><div class="path-arrow Fright"></div>
                <div class="path-name Fright">
                    <span><?= lang('global_usergroups') ?></span>
                </div>
            </div>
        </div>
    </div>
    <div class="widget">
        <?= form_open() ?>
        <div class="widget-header">
            <div class="widget-header-icon Fright">
                <span class="icos-pencil2"></span>
            </div>
            <div class="widget-header-title Fright">
                <?= lang('global_usergroups') ?>
            </div>
        </div>
        <div class="widget-container">
            <div class="span12">
                <div class="span2">
                    <?= lang('global_name_ar') ?>
                </div>
                <div class="span10">
                    <?= form_input('name_ar', set_value('name_ar', $item->name_ar)) ?>
                </div>
            </div>
            <div class="span12">
                <div class="span2">
                    <?= lang('global_name_la') ?>
                </div>
                <div class="span10">
                    <?= form_input('name_la', set_value('name_la', $item->name_la)) ?>
                </div>
            </div>

            <table class="ffTable">
                <thead>
                    <tr>
                        <th style="width: 5px;">
                            <?= form_checkbox('ss', 1, FALSE, 'class="select_all"') ?>
                        </th>
                        <th>
                            <?= lang('global_usergroups') ?>
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <? foreach ($privileges as $privilege): ?>
                        <tr>
                            <td>
                                <?= form_checkbox('permissions[' . $privilege->{$keys[1]} . ']', 1, set_value('permissions[' . $privilege->{$keys[1]} . ']', in_array($privilege->{$keys[1]}, $permissions)), 'class="perm"') ?>
                            </td>
                            <td>
                                <?= $privilege->{name()} ?> <? if ($privilege->{"description_" . name(TRUE)}): ?> (<?= $privilege->{"description_" . name(TRUE)} ?>) <? endif ?>
                            </td>
                        </tr>
                    <? endforeach ?>
                </tbody>
            </table>
            <input type="submit" value="<?= lang('global_submit') ?>" />
        </div>
        <?= form_close() ?>
    </div>
</div>
<script>
    $(document).ready(function() {
        $('.select_all').click(function() {
            $('.perm').prop('checked', this.checked)
        });
    });
</script>