<?= form_open()?>
<div class="row-fluid">

    <!--
    <div style="margin-bottom:10px; text-align:left; border:3px solid #FFF; background:url(img/backgrounds/bg_box_head.jpg); padding:5px 10px; height:30px;">
          <div style="float:right; text-align:right; font-size:14px; line-height:30px; color:#333;"> <a href="">فيرجو</a> 
              <span style="color:#80693d"><img src="img/jbreadcrumb/Chevron-ar.gif" width="5" height="20"></span> <a href="">البيانات الرئيسية</a> <span style="color:#80693d"><img src="img/jbreadcrumb/Chevron-ar.gif" width="5" height="20"></span> إضافة بيانات الفندق </div>
        </div>-->



    <div class="widget">
        <div class="head dark">
            <div class="icon"><i class="icos-pencil2"></i></div>
            <h2><?= lang("menu_main_data") ?></h2> 
        </div>                        
        <div class=" block-fluid">
            <div class="row-form">
                    <div class="span4">
                        <input type="checkbox" value="1" name="send_email"  onClick="toggle(this, document.getElementById('email'))" <?=($this->noti_setting_model->send_email)?"checked":""?> > <?=lang('send_noti_via_email')?>
                    </div>
                    <div class="span8">
                        <div class="span4"><?=lang('email')?>:</div>
                        <div class="span8">
                            <input type="text" class="input-full" name="email" id="email" <?=($this->noti_setting_model->send_email)?"":"disabled"?> value="<?=$this->noti_setting_model->email?>"/></div>
                    </div>
            </div>

            <div class="row-form">
                    <div class="span4">
                        <input type="checkbox" value="1" name="send_sms" onClick="toggle(this, document.getElementById('mobile'))" <?=($this->noti_setting_model->send_sms)?"checked":""?> ><?=lang('send_noti_via_mobile')?>
                    </div>
                    <div class="span8">
                        <div class="span4"><?=lang('mobile')?>:</div>
                        <div class="span8">
                            <input type="text" class="input-full" name="mobile" id="mobile" <?=($this->noti_setting_model->send_sms)?"":"disabled"?> value="<?=$this->noti_setting_model->phone_no?>" /></div>
                    </div>
            </div>
        </div>
    </div>
</div>




<div class="row-fluid">
    <div class="widget">
        <div class="head dark">
            <div class="icon"><i class="icos-pencil2"></i></div>
            <h2><?= lang('what_is_to_be')?></h2>
        </div>                
        <div class=" block-fluid">

            <select id="contact-location" name="note_me" style="font-size:14px;">
                <option value="1" <?=($this->noti_setting_model->note_me == 1)?"selected":""?> ><?= lang('all_noti')?></option>
                <option value="2" <?=($this->noti_setting_model->note_me == 2)?"selected":""?> ><?= lang('just_noti_from')?></option>
                <option value="3" <?=($this->noti_setting_model->note_me == 3)?"selected":""?> ><?= lang('noti_sended_from_system')?></option>
            </select>

            <div id="2" style="display:<?=($this->noti_setting_model->note_me==2)?"":"none"?>">
                <div class="row-form">
                    <div class="span6">
                        <div class="span4 TAL Pleft10"><?= lang('uo_menu')?></div>
                        <div class="span8">                            
                            <?= form_multiselect('safa_uo_id[]', ddgen('safa_uos', array('safa_uo_id', name())), set_value('safa_uo_id[]', $this->noti_setting_model->uos), " data-placeholder='$select_from_menu' class='chosen-select chosen-rtl input-full' multiple tabindex='1'") ?>                        
                        </div>
                    </div>

                    <div class="span6">
                        <div class="span4 TAL Pleft10"><?= lang('ea_menu')?></div>
                        <div class="span8">
                            
                            <?= form_multiselect('safa_ea_id[]', ddgen('safa_eas', array('safa_ea_id', name())), set_value('safa_ea_id[]', $this->noti_setting_model->eas), " data-placeholder='$select_from_menu' class='chosen-select chosen-rtl input-full' multiple tabindex='2'") ?>                        
                        </div>
                    </div>
                </div>

                <div class="row-form">
                    <div class="span6">
                        <div class="span4 TAL Pleft10"><?= lang('ito_menu')?></div>
                        <div class="span8">                            
                            <?= form_multiselect('safa_ito_id[]', ddgen('safa_itos', array('safa_ito_id', name())), set_value('safa_ito_id[]', $this->noti_setting_model->itos), " data-placeholder='$select_from_menu' class='chosen-select chosen-rtl input-full' multiple tabindex='3'") ?>                        
                        </div>
                    </div>

                    <div class="span6">
                        <div class="span4 TAL Pleft10"><?= lang('hotel_brokers_menu')?></div>
                        <div class="span8">          
                           
                            <select data-placeholder="<?=lang('global_select_from_menu')?>" class="chosen-select chosen-rtl input-full" multiple tabindex="4">
                                <option>لا توجد شركات الان</option>
                            </select>
                            
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>



<div class=" block-fluid">
    <div class="row-form TAC">
        <input type="submit" name='save' class="btn btn-primary" value="<?= lang("global_submit") ?>">
    </div>
</div>
<?= form_close()?>

<!--<div id="fcAddEvent" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="fcAddEventLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="fcAddEventLabel">Add new event</h3>
    </div>
    <div class="modal-body">
        <div class="row-fluid">
            <div class="span3">Title:</div>
            <div class="span9"><input type="text" id="fcAddEventTitle"/></div>
        </div>
    </div>
    <div class="modal-footer">            
        <button class="btn btn-primary" id="fcAddEventButton">Add</button>            
    </div>
</div>-->



<!-- select drop down hide/show -->
<script>
    $(document).ready(function() {
        $('#contact-location').change(function() {
            var location = $(this).val(),
                    div = $('#' + location);

            $('#2').hide();
            div.show();

        });
    });
</script>

<!-- multi selection choosen -->
<link rel="stylesheet" href="<?= JS ?>/plugins/multi-chosen/chosen.css">

<script src="<?= JS ?>/plugins/multi-chosen/chosen.jquery.js" type="text/javascript"></script>

<script type="text/javascript">
    var config = {
        '.chosen-select': {width: "100%"},
        '.chosen-select-deselect': {allow_single_deselect: true},
        '.chosen-select-no-single': {disable_search_threshold: 10},
        '.chosen-select-no-results': {no_results_text: 'Oops, nothing found!'},
        '.chosen-select-width': {width: "95%"}
    }
    for (var selector in config) {
        $(selector).chosen(config[selector]);
    }
</script>

<script type="text/javascript">
//<!--
    function toggle(checkbox_obj, txt_obj) {
        if (checkbox_obj.checked) {
            txt_obj.disabled = 0
        }
        else {
            txt_obj.disabled = 1
        }
    }
//-->
</script>

