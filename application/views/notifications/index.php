<!-- 
<link rel="stylesheet" href="<?= NEW_CSS_JS .'_'. lang('DIR') ?>/bootstrap/bootstrap.min.css" />
 -->

<link rel="stylesheet" href="<?= NEW_CSS_JS .'_'. lang('DIR') ?>/icons.css" />

<!--<div class="row-fluid" >
    <div class="span12" >
        <div class="row-fluid" >
            <div class="row-fluid">
                <div style="margin-bottom:10px; text-align:left; border:3px solid #FFF; background:url(<?php echo  IMAGES ?>/backgrounds/bg_box_head.jpg); padding:5px 10px; height:30px;">
                    <div style="float:right; text-align:right; font-size:14px; line-height:30px; color:#333;"> <a href="<?php echo   site_url('ea/dashboard')?>"> <?php echo  lang('global_system_management') ?></a> 
                            <span style="color:#80693d"><img src="<?php echo  IMAGES ?>/jbreadcrumb/Chevron-ar.gif" width="5" height="20"></span>  <?php echo  lang('notifications') ?> </div>
                </div> 
            </div>-->
           
<div class="widget">
    <div class="path-container Fright">
         
        <div class="path-name Fright">
        <a href="<?= site_url() ?>"><?= lang('global_system_management') ?></a>
        </div>
        <div class="path-arrow Fright">
        </div>
        <div class="path-name Fright">
            <?php echo  lang('notifications') ?>
        </div>
        
    </div>
</div>
            
            
            
            
           
            <div class="row-fluid" >
                <div class="widget">
                                   
                    <div class="widget-header">
			        <div class="widget-header-icon Fright">  </div>
			        <div class="widget-header-title Fright">
			            <?php echo  lang('notifications') ?>
			        </div>
			    	</div>
                    
                    <div class="widget-container">
                        
                        <div class="span12"  style="padding: 10px 0;">
                    <a href="<?php echo site_url("notifications/viewAll"); ?>"  class="btn"><?php echo lang("messages") ?></a>
	                <a href="<?php echo site_url("notifications/viewAllImportant"); ?>"  class="btn"><?php echo lang("important_messages") ?></a>
	                <a href="<?php echo site_url("notifications/viewAllDeleted"); ?>"  class="btn"><?php echo lang("deleted_messages") ?></a>
	            	</div>
                        
                    
                    
                        <table cellpadding="0" class="MyTable" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th><?php echo  lang('message') ?></th>
                                </tr>
                            </thead>
                            <tbody>
      <? if (isset($items)) { ?>
           <? foreach ($items as $item) { ?>
                   <tr>
                   
           <?php 
												
            $mark_as_read= $item->mark_as_read;
        	$mark_as_important= $item->mark_as_important;
        	$is_hide= $item->is_hide;
        	
        	$erp_notification_detail_id= $item->erp_notification_detail_id;
        	
        	$erp_system_events_id= $item->erp_system_events_id;
        	$language_id= $item->language_id;
        	$msg_datetime= $item->msg_datetime;
        	
        	$tags= $item->tags;
        	$tags_arr=explode('*****',$tags);
        	
        	$this->method_call = get_instance();
        	$notification_data=$this->method_call->getNotificationDataFromXml($language_id, $erp_system_events_id);
        	$notification_data_arr=explode('*****',$notification_data);
        	
        	$notification_subject='';
        	$notification_body='';
        	
        	if(isset($notification_data_arr[0])) {
	        	$notification_subject=$notification_data_arr[0];
	        	foreach($tags_arr as $tag) {
	        		$tag_arr=explode('=',$tag);
	        		if(isset($tag_arr[0]) && isset($tag_arr[1])) {
	        			$notification_subject=str_replace($tag_arr[0],$tag_arr[1],$notification_subject);
	        		}
	        	}
	        	
	        	if(isset($notification_data_arr[1])) {
		        	$notification_body=$notification_data_arr[1];
		        	foreach($tags_arr as $tag) {
		        		$tag_arr=explode('#*=',$tag );
		        		if(isset($tag_arr[0]) && isset($tag_arr[1])) {
		        			$notification_body=str_replace($tag_arr[0]. '#*',$tag_arr[1],$notification_body);
		        			//$notification_body = str_replace($tag_arr[0] . '#*', '<b>' . $tag_arr[1] . '</b>', $notification_body);
		        		}
		        	}
	        	}
        	}
        	
           if($mark_as_important==1) {
	        	$style_notification_message='background:#fbf3d7';
	        	$mark_as_important_value=0;
	        	$mark_as_important_link_text=lang('mark_as_un_important');
        	} else {
	        	if($mark_as_read==1) {
	        		$style_notification_message='background:#FFFFFF';
	        	} else {
	        		$style_notification_message='background:# f5f5f5';
	        	}        	
	        	$mark_as_important_value=1;
	        	$mark_as_important_link_text=lang('mark_as_important');
        	}
        	
           if($is_hide==1) {
	        	
	        	$is_hide_value=0;
        	} else {
	        	
	        	$is_hide_value=1;
        	}
        	
        	//$notification_body_brief=substr($notification_body, 0, 650).'...';
        	$notification_body_brief = strstr($notification_body, '</a>', true).'</a> ....';
        	
            echo("
            <td style='$style_notification_message'>
            <div style=''>
            <a class='fancybox' href='".site_url("notifications/read/$erp_notification_detail_id")."'>
            <b>$notification_subject</b>
            <br/>
            $notification_body_brief
            </a>
            </div>
            
            ");
            
            echo "
            
            <script>
            $(document).ready(function()
			{
				$('#notification_message_delete_dv_$erp_notification_detail_id').click(function()
				{
					$.ajax
					({
						type: 'POST',
						url: '". base_url().'notifications/hide'."/$erp_notification_detail_id/$is_hide_value',
						cache: false,
						success: function(html)
						{
							location.reload();
						}
					});
					
				});
				
				$('#notification_message_important_dv_$erp_notification_detail_id').click(function()
				{
					$.ajax
					({
						type: 'POST',
						url: '". base_url().'notifications/markAsImportant'."/$erp_notification_detail_id/$mark_as_important_value',
						cache: false,
						success: function(html)
						{
							location.reload();
						}
					});
					
				});
		
			});
            </script>
            ";
            
            if($is_hide==1) {
            echo"
            <span id='notification_message_delete_dv_$erp_notification_detail_id' class='icos-arrow-right' title='".lang('restore_message')."' style='float:left; cursor: pointer; margin: 3px 3px 3px 3px;'  ></span>
            ";
            } else {
            	echo"
            <span id='notification_message_delete_dv_$erp_notification_detail_id' class='icos-cancel' title='".lang('delete_message')."' style='float:left; cursor: pointer; margin: 3px 3px 3px 3px;'  ></span>
            ";
            }
              echo"
            <span id='notification_message_important_dv_$erp_notification_detail_id' class='icos-star' title='$mark_as_important_link_text' style='float:left; cursor: pointer; margin: 3px 3px 3px 3px;'  ></span>
            ";         

              echo"</td>";
              
                                            ?>
                        </tr>

                                    <? } ?>
                                <? }; ?>
                            </tbody>
                        </table>
                    
                </div>  
            </div>
            <div class="row-fluid">
                <?php echo  $pagination ?>
            </div>
        </div>

<script>
$(document).ready(function() {

    $('.fancybox').click(function(){
        $.get($(this).attr('href'),function(data){
            $.fancybox({content:data});
        });
        return false;
    });
});

</script>


    </div>  
</div>
