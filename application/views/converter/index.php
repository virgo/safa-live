<?= form_open_multipart() ?>
    <?= validation_errors() ?>
    Hardware ID:
    <?= form_input('hardware_id', set_value('hardware_id')) ?><br />

    Version:
    <?= form_input('version', set_value('version')) ?><br />

    UASP Username:
    <?= form_input('username', set_value('username')) ?><br />

    UASP Password:
    <?= form_password('password', set_value('password')) ?><br />

    Destination:
    <?= form_dropdown('destination', ddgen('erp_uasp', array(
        'erp_uasp_id', name()
    )), set_value('destination')) ?><br />
    
    EA_Code:
    <?= form_input('ea_code', set_value('ea_code')) ?><br />
    
    UO_Code:
    <?= form_input('uo_code', set_value('uo_code')) ?><br />
    
    Safa File:
    <?= form_upload('file') ?><br />

    <?= form_submit('submit', 'Submit') ?>
<?= form_close() ?>