

<style>
    table tr th{background-color: #f1f1f1;}
	table td, table th{padding:1px!important;}
	th label,td label {
    display: inline-block!important;
    margin: 1px 0!important;
}
    td table{
        border: none;
        
    }
    td {
        padding: 0!important;
    }
     table tr td{
        padding: 4px!important;
        border-bottom: 1px solid #ddd!important;
        background-color: #fff;
/*        border-right: 1px solid #ddd!important;*/
    }.rotate{background-color: #fff;}
    .rotate label {-webkit-transform: rotate(-90deg); 
-moz-transform: rotate(-90deg);margin: 10px 0!important;
    }
    
    .av-hotel,.ch-hotel,.cl-hotel{min-height: 20px;min-width: 20px;}
/*    .av-hotel:hover,.ch-hotel:hover,.cl-hotel:hover{box-shadow:0px -2px 12px 0px rgba(0,0,0,.14),0px 2px 12px 0px rgba(0,0,0,.14);}*/
    .av-hotel img,.ch-hotel img,.cl-hotel img{cursor: pointer;}
    .av-hotel{background: #B8D776;}
    .ch-hotel{background-color:#DE856D;}
/*    .av-hotel:hover,.ch-hotel:hover,.cl-hotel:hover{
                    -webkit-transition: all .4s ease;
    -moz-transition: all .4s ease;
    -o-transition: all .4s ease;
    transition: all .4s ease;}*/
    .cl-hotel{background-color:#918183;}
    .plan {float: left;}

    
    .hotel{}
    .group{}
    
    label{margin: 0;padding: 5px;}
    .row-form > [class^="span"]{border: none;}
    
    .table-warp .box-shadow-last  {box-shadow: -12px 0 8px -4px rgba(0,0,0, 0.3);}
    .table-warp .box-shadow-first {box-shadow: 12px 0 15px -4px rgba(0,0,0, 0.3)}
    
    .table-warp th label {
    font-family: tahoma;
    font-size: 10px;
    font-weight: 900;
}

</style>
<div class="widget">
    <div class="path-container Fright">
        <div class="path-name Fright">
            <a href=""><?= lang('global_system_management') ?></a>
        </div>
        <div class="path-arrow Fright">
        </div>
        <div class="path-name Fright">
            <a href=""><?= lang('menu_main_hotels') ?></a>
        </div>
        <div class="path-arrow Fright">
        </div>
        <div class="path-name Fright">
          بيان الإتاحة الفندقية
        </div>
    </div>
</div>

<div class="widget">
    <div class="widget-header">
        <div class="widget-header-icon icon-pencil pull-left"></div>
    <div class="widget-header-title pull-left">بيان الإتاحة الفندقية</div>
    </div>
    <div class="widget-container">
      <div class="row-form" style="border: none;">
            <div class="span3">
                
                <div class="span4"><label>الفندق</label></div>
                <div class="span6"><?= form_multiselect('nationalities', $nationalities, set_value('nationalities'), 'class="nationalities"', 'style="width:100%!important;"') ?></div>
            </div>
            <div class="span1" style="padding:0 10px !important; height: 70px;">
                <label class="pull-right" style="width: 34px; height: 21px;">هــ<input class="pull-right" style="height: 25px; margin-top: 0px;" name="هــ" type="radio" value="هــ"/></label>
                <label class="pull-right">م<input  class="pull-right"name="م" type="radio" value="م" /></label></div>
            <div class="span5">
                <div class="span2"><label>الفترة من</label></div>
                <div class="span4"><input class=" input-huge date" data-format="yyyy-MM-dd" name="datetime" 
                                   type="text" value="<?=set_value('datetime')?>" style=" direction:ltr;"></div>
                <div class="span1"><label>إلى</label></div>
                <div class="span4"><input class=" input-huge date" data-format="yyyy-MM-dd" name="datetime" 
                                   type="text" value="<?=set_value('datetime')?>" style=" direction:ltr;"></div>
            </div>
            <div class="span3"><a href="">بحث متقدم</a></div>
        </div>
        
        
        
      <div class="table-warp">
        <table width="100%" border="1">
  <tr>
      <th colspan="2" rowspan="2" bgcolor="#FFFFFF" scope="col"><label>الأنصار الماسي</label></th>
    <th scope="col">م</th>
    <th scope="col">1</th>
    <th scope="col">2</th>
    <th scope="col">3</th>
    <th scope="col">4</th>
    <th scope="col">5</th>
    <th scope="col">6</th>
    <th scope="col">7</th>
    <th scope="col">8</th>
    <th scope="col">9</th>
    <th scope="col">10</th>
    <th scope="col">11</th>
    <th scope="col">12</th>
    <th scope="col">13</th>
    <th scope="col">14</th>
    <th scope="col">15</th>
    <th scope="col">16</th>
    <th scope="col">17</th>
    <th scope="col">18</th>
    <th scope="col">19</th>
    <th scope="col">20</th>
    <th scope="col">21</th>
    <th scope="col">22</th>
    <th scope="col">23</th>
    <th scope="col">24</th>
    <th scope="col">25</th>
    <th scope="col">26</th>
    <th scope="col">27</th>
    <th scope="col">28</th>
    <th scope="col">29</th>
    <th scope="col">30</th>
    </tr>
  <tr>
    <th>هـ</th>
    <th scope="col">1</th>
    <th scope="col">2</th>
    <th scope="col">3</th>
    <th scope="col">4</th>
    <th scope="col">5</th>
    <th scope="col">6</th>
    <th scope="col">7</th>
    <th scope="col">8</th>
    <th scope="col">9</th>
    <th scope="col">10</th>
    <th scope="col">11</th>
    <th scope="col">12</th>
    <th scope="col">13</th>
    <th scope="col">14</th>
    <th scope="col">15</th>
    <th scope="col">16</th>
    <th scope="col">17</th>
    <th scope="col">18</th>
    <th scope="col">19</th>
    <th scope="col">20</th>
    <th scope="col">21</th>
    <th scope="col">22</th>
    <th scope="col">23</th>
    <th scope="col">24</th>
    <th scope="col">25</th>
    <th scope="col">26</th>
    <th scope="col">27</th>
    <th scope="col">28</th>
    <th scope="col">29</th>
    <th scope="col">30</th>
    </tr>
  <tr>
    <th width="1%" rowspan="5" class="rotate"><label>الدور الاول</label></th>
    <th rowspan="2"><img src="../static/img/roomx.png" alt="room-img" />      <label>1</label></th>
    <td>101</td>
    <td colspan="6" class="av-hotel"></td>
    <td></td>
    <td></td>
    <td></td>
    <td colspan="5" class="av-hotel"></td>
    <td></td>
    <td colspan="4" class="ch-hotel group"></td>
    <td colspan="4" class="cl-hotel"></td>
    <td></td>
    <td colspan="5" class="av-hotel"></td>
    <td></td>
    </tr>
  <tr>
    <td>102</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td colspan="6" class="ch-hotel group"></td>
    <td></td>
    <td colspan="5" class="ch-hotel hotel"></td>
    <td></td>
    <td colspan="5" class="av-hotel"></td>
    <td></td>
    <td colspan="5" class="cl-hotel"></td>
    <td></td>
    <td></td>
    </tr>
  <tr>
    <th rowspan="2"><img src="../static/img/roomx.png" alt="room-img" />      <label>2</label></th>
    <td>103</td>
    <td></td>
    <td colspan="7" class="av-hotel"></td>
    <td></td>
    <td colspan="4" class="ch-hotel group"></td>
    <td></td>
    <td colspan="5" class="cl-hotel"></td>
    <td></td>
    <td colspan="5" class="ch-hotel group"></td>
    <td></td>
    <td colspan="4" class="av-hotel"></td>
    </tr>
  <tr>
    <td>104</td>
    <td colspan="5" class="ch-hotel group"></td>
    <td></td>
    <td></td>
    <td colspan="4" class="av-hotel"></td>
    <td></td>
    <td colspan="5" class="ch-hotel hotel"></td>
    <td></td>
    <td></td>
    <td colspan="5" class="av-hotel"></td>
    <td></td>
    <td colspan="4" class="ch-hotel group"></td>
    <td></td>
    </tr>
  <tr>
    <th><img src="../static/img/roomx.png" alt="room-img" />
      <label>3</label></th>
    <td>105</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td colspan="14" class="av-hotel"></td>
    <td></td>
    <td colspan="4" class="ch-hotel hotel"></td>
    </tr>
  <tr>
    <th rowspan="5" class="rotate"><label>الدور الثاني</label></th>
    <th rowspan="3"><img src="../static/img/roomx.png" alt="room-img" />      <label>1</label></th>
    <td>201</td>
    <td colspan="16" class="ch-hotel group"></td>
    <td></td>
    <td></td>
    <td colspan="9" class="ch-hotel hotel"></td>
    <td></td>
    <td></td>
    <td></td>
    </tr>
  <tr>
    <td>202</td>
    <td></td>
    <td colspan="7" class="cl-hotel "></td>
    <td></td>
    <td colspan="5" class="ch-hotel hotel"></td>
    <td></td>
    <td></td>
    <td colspan="6" class="av-hotel"></td>
    <td></td>
    <td></td>
    <td colspan="5" class="av-hotel"></td>
    <td></td>
    </tr>
  <tr>
    <td>203</td>
    <td></td>
    <td></td>
    <td></td>
    <td colspan="11" class="av-hotel"></td>
    <td></td>
    <td colspan="6" class="ch-hotel group"></td>
    <td colspan="5" class="av-hotel"></td>
    <td colspan="4" class="cl-hotel"></td>
    </tr>
  <tr>
    <th rowspan="2"><img src="../static/img/roomx.png" alt="room-img" />      <label>2</label></th>
    <td>204</td>
    <td colspan="8" class="ch-hotel hotel"></td>
    <td></td>
    <td></td>
    <td colspan="7" class="cl-hotel"></td>
    <td></td>
    <td colspan="5" class="cl-hotel"></td>
    <td colspan="5" class="av-hotel"></td>
    <td></td>
    <td></td>
    </tr>
  <tr>
    <td>205</td>
    <td></td>
    <td colspan="6" class="av-hotel"></td>
    <td></td>
    <td></td>
    <td colspan="7" class="av-hotel"></td>
    <td></td>
    <td></td>
    <td></td>
    <td colspan="5" class="av-hotel"></td>
    <td></td>
    <td colspan="5" class="ch-hotel hotel"></td>
    </tr>
</table>

                  

        </div>
        <div class="Div-Sep">
                            <a></a>
</div>
        <div class="table-warp">
        <table width="100%" border="1">
  <tr>
      <th colspan="2" rowspan="2" bgcolor="#FFFFFF" scope="col"><label>إيلاف المشاعر</label></th>
    <th scope="col">م</th>
    <th scope="col">1</th>
    <th scope="col">2</th>
    <th scope="col">3</th>
    <th scope="col">4</th>
    <th scope="col">5</th>
    <th scope="col">6</th>
    <th scope="col">7</th>
    <th scope="col">8</th>
    <th scope="col">9</th>
    <th scope="col">10</th>
    <th scope="col">11</th>
    <th scope="col">12</th>
    <th scope="col">13</th>
    <th scope="col">14</th>
    <th scope="col">15</th>
    <th scope="col">16</th>
    <th scope="col">17</th>
    <th scope="col">18</th>
    <th scope="col">19</th>
    <th scope="col">20</th>
    <th scope="col">21</th>
    <th scope="col">22</th>
    <th scope="col">23</th>
    <th scope="col">24</th>
    <th scope="col">25</th>
    <th scope="col">26</th>
    <th scope="col">27</th>
    <th scope="col">28</th>
    <th scope="col">29</th>
    <th scope="col">30</th>
    </tr>
  <tr>
    <th>هـ</th>
    <th scope="col">1</th>
    <th scope="col">2</th>
    <th scope="col">3</th>
    <th scope="col">4</th>
    <th scope="col">5</th>
    <th scope="col">6</th>
    <th scope="col">7</th>
    <th scope="col">8</th>
    <th scope="col">9</th>
    <th scope="col">10</th>
    <th scope="col">11</th>
    <th scope="col">12</th>
    <th scope="col">13</th>
    <th scope="col">14</th>
    <th scope="col">15</th>
    <th scope="col">16</th>
    <th scope="col">17</th>
    <th scope="col">18</th>
    <th scope="col">19</th>
    <th scope="col">20</th>
    <th scope="col">21</th>
    <th scope="col">22</th>
    <th scope="col">23</th>
    <th scope="col">24</th>
    <th scope="col">25</th>
    <th scope="col">26</th>
    <th scope="col">27</th>
    <th scope="col">28</th>
    <th scope="col">29</th>
    <th scope="col">30</th>
    </tr>
  <tr>
    <th width="1%" rowspan="5" class="rotate"><label>الدور الاول</label></th>
    <th rowspan="2"><img src="../static/img/roomx.png" alt="room-img" />      <label>1</label></th>
    <td>101</td>
    <td colspan="6" class="av-hotel"></td>
    <td></td>
    <td></td>
    <td></td>
    <td colspan="5" class="av-hotel"></td>
    <td></td>
    <td colspan="4" class="ch-hotel group"></td>
    <td colspan="4" class="cl-hotel"></td>
    <td></td>
    <td colspan="5" class="av-hotel"></td>
    <td></td>
    </tr>
  <tr>
    <td>102</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td colspan="6" class="ch-hotel group"></td>
    <td></td>
    <td colspan="5" class="ch-hotel hotel"></td>
    <td></td>
    <td colspan="5" class="av-hotel"></td>
    <td></td>
    <td colspan="5" class="cl-hotel"></td>
    <td></td>
    <td></td>
    </tr>
  <tr>
    <th rowspan="2"><img src="../static/img/roomx.png" alt="room-img" />      <label>2</label></th>
    <td>103</td>
    <td></td>
    <td colspan="7" class="av-hotel"></td>
    <td></td>
    <td colspan="4" class="ch-hotel group"></td>
    <td></td>
    <td colspan="5" class="cl-hotel"></td>
    <td></td>
    <td colspan="5" class="ch-hotel group"></td>
    <td></td>
    <td colspan="4" class="av-hotel"></td>
    </tr>
  <tr>
    <td>104</td>
    <td colspan="5" class="ch-hotel group"></td>
    <td></td>
    <td></td>
    <td colspan="4" class="av-hotel"></td>
    <td></td>
    <td colspan="5" class="ch-hotel hotel"></td>
    <td></td>
    <td></td>
    <td colspan="5" class="av-hotel"></td>
    <td></td>
    <td colspan="4" class="ch-hotel group"></td>
    <td></td>
    </tr>
  <tr>
    <th><img src="../static/img/roomx.png" alt="room-img" />
      <label>3</label></th>
    <td>105</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td colspan="14" class="av-hotel"></td>
    <td></td>
    <td colspan="4" class="ch-hotel hotel"></td>
    </tr>
  <tr>
    <th rowspan="5" class="rotate"><label>الدور الثاني</label></th>
    <th rowspan="3"><img src="../static/img/roomx.png" alt="room-img" />      <label>1</label></th>
    <td>201</td>
    <td colspan="16" class="ch-hotel group"></td>
    <td></td>
    <td></td>
    <td colspan="9" class="ch-hotel hotel"></td>
    <td></td>
    <td></td>
    <td></td>
    </tr>
  <tr>
    <td>202</td>
    <td></td>
    <td colspan="7" class="cl-hotel "></td>
    <td></td>
    <td colspan="5" class="ch-hotel hotel"></td>
    <td></td>
    <td></td>
    <td colspan="6" class="av-hotel"></td>
    <td></td>
    <td></td>
    <td colspan="5" class="av-hotel"></td>
    <td></td>
    </tr>
  <tr>
    <td>203</td>
    <td></td>
    <td></td>
    <td></td>
    <td colspan="11" class="av-hotel"></td>
    <td></td>
    <td colspan="6" class="ch-hotel group"></td>
    <td colspan="5" class="av-hotel"></td>
    <td colspan="4" class="cl-hotel"></td>
    </tr>
  <tr>
    <th rowspan="2"><img src="../static/img/roomx.png" alt="room-img" />      <label>2</label></th>
    <td>204</td>
    <td colspan="8" class="ch-hotel hotel"></td>
    <td></td>
    <td></td>
    <td colspan="7" class="cl-hotel"></td>
    <td></td>
    <td colspan="5" class="cl-hotel"></td>
    <td colspan="5" class="av-hotel"></td>
    <td></td>
    <td></td>
    </tr>
  <tr>
    <td>205</td>
    <td></td>
    <td colspan="6" class="av-hotel"></td>
    <td></td>
    <td></td>
    <td colspan="7" class="av-hotel"></td>
    <td></td>
    <td></td>
    <td></td>
    <td colspan="5" class="av-hotel"></td>
    <td></td>
    <td colspan="5" class="ch-hotel hotel"></td>
    </tr>
</table>

                  

        </div>

<div class="row-fluid ">
                <div class="span4">
                <div class="span1 av-hotel">
                  
                </div>
                <div class="span2 "><label>متاحة</label></div>
                <div class="span1 ch-hotel"></div>
                <div class="span2"><label>محجوزة</label></div>
                <div class="span1 cl-hotel"></div>
                <div class="span2"><label>مغلقة</label></div>
                </div>
    <div class="span4">
        
        <div class="span1"><img src='../static/img/plan/ea.png' alt='room-img' style='width:24px;height:24px;' /></div>
        <div class="span6"><label>إسم العميل (الوكيل)</label></div>
        
        <div class="span1"><img src='../static/img/plan/bkg.png' alt='room-img' style='width:24px;height:24px;' /></div>
        <div class="span3"><label>الحجز بكج</label></div>
    </div>
    <div class="span4">
        
        <div class="span1"><img src='../static/img/plan/hotel.png' alt='room-img' style='width:24px;height:24px;' /></div>
        <div class="span3"><label>حجز غرفة</label></div>
        
        <div class="span1"><img src='../static/img/plan/group.png' alt='room-img' style='width:24px;height:24px;' /></div>
        <div class="span7"><label>تفاصيل المجموعة المحجوز لها</label></div>
    </div>
      </div>
        
        
    </div>
    
    
</div>





    <script type="text/javascript">
    $(function() {
        $('.date').datetimepicker({
            language: 'pt-BR',
            pickTime: false
        });
        $('.nationalities').chosen();
        
        
        
    });
  
</script>

<script type="text/javascript">
$(document).ready(function(){
   $('#advantages').chosen();
   $('#erp_country_id').chosen();
     
});
</script>
<script>
    $('#erp_country_id').change(function() {
        var country_id = $('#erp_country_id').val();
        $.get('get_erp_city/' + country_id, function(data) {
            $('#erp_city_id').html(data);
        });
    });
</script>
<script>
    $(document).ready(function() {
        var country_id = $('#erp_country_id').val();
        $.get('get_erp_city/' + country_id, function(data) {
            $('#erp_city_id').html(data);
        });
    });
</script>
   
<script type="text/javascript">
 /*initializing the map*/
  $(document).ready(function(){
        $("#SignupForm").formToWizard();
        $('.next').click(function(){
          var parent_=$("#step_map").parent(); /* init the map */
          if($(parent_).attr("style")==="display: block;"){
             var alharam= get_alharam();
             initialize(alharam['lat'],alharam['long'],false);    
          }
          /*removing the fils from the inputs*/
          var parent_=$("#step_image").parent();
          if($(parent_).attr("style")==="display: none;"){
             $("#file_upload_images #file_images").val('');
          }
          
        });
        
    });
</script>

<script>
/*the javascript validation  for a form*/
   $(document).ready(function(){
      $("#SignupForm").validationEngine('attach',{
          promptPosition :"topLeft", 
          scroll: false,
          binded:true,
           prettySelect :true,
           useSuffix: "_chosen"
      }); 
      
      
    })  ;
</script>

<!--by fatthy-->
<script>
$(document).ready(function(){
    
    $("table .ch-hotel").append($("<div class='plan'><a href='#' title='clint details'><img src='../static/img/plan/ea.png' alt='room-img' style='width:24px;height:24px;' /></a><a href='#' title='packege details'><img src='../static/img/plan/bkg.png' alt='room-img'  style='width:24px;height:24px;'/></a></div>"));
    $("table .hotel").append($("<a href='#' title='room reservesion'><img src='../static/img/plan/hotel.png' alt='room-img' style='width:24px;height:24px;' /></a>"));
    $("table .group").append($("<a href='#' title='person who reserved this room'><img src='../static/img/plan/group.png' alt='room-img' style='width:24px;height:24px;' /></a>"));
    $('tr').hover(function(){
         
         $(this).children("td:last").toggleClass("box-shadow-last", 500, "easeOutSine");
         $(this).children("td:first").toggleClass("box-shadow-first", 500, "easeOutSine");
    });
    $( "table" ).tooltip();

    });
</script>
