
<script type='text/javascript' src="<?= NEW_CSS_JS ?>/fancybox/jquery.fancybox.pack.js"></script>
<link rel="stylesheet" href="<?= NEW_CSS_JS ?>/fancybox/jquery.fancybox.css" type="text/css" media="screen" />
<link rel="stylesheet" href="<?= NEW_CSS_JS ?>/multi-select-chosen/chosen.css">
<script src="<?= NEW_CSS_JS ?>/multi-select-chosen/chosen.jquery.js" type="text/javascript"></script>
<script src="https://maps.googleapis.com/maps/api/js?AIzaSyC794uPScLtRUa8Uwk9nX8RCjrepoxszxg&sensor=true"></script>

<style>
    input {
        width: 80%!important;
        margin: 0 auto;
        margin-bottom: 9px;
    }
    .row-fluid
    {overflow: hidden;}
    .pull-right{float: right;padding-right: 10px;}
    
@media only screen and (max-width: 800px) {
#flip-scroll .cf:after { visibility: hidden; display: block; font-size: 0; content: " "; clear: both; height: 0; }
#flip-scroll * html .cf { zoom: 1; }
#flip-scroll *:first-child+html .cf { zoom: 1; }
#flip-scroll table { width: 100%; border-collapse: collapse; border-spacing: 0; }
 
#flip-scroll th,
#flip-scroll td { margin: 0; vertical-align: top; }
#flip-scroll th { text-align: left; }
#flip-scroll table { display: block; position: relative; width: 100%; }
#flip-scroll thead { display: block; float: left; }
#flip-scroll tbody { display: block; width: auto; position: relative; overflow-x: auto; white-space: nowrap; }
#flip-scroll thead tr { display: block; }
#flip-scroll th { display: block; text-align: right; }
#flip-scroll tbody tr { display: inline-block; vertical-align: top; }
#flip-scroll td { display: block; min-height: 1.25em; text-align: left; }
 
 
/* sort out borders */
 
#flip-scroll th { border-bottom: 0; border-left: 0; }
#flip-scroll td { border-left: 0; border-right: 0; border-bottom: 0; }
#flip-scroll tbody tr { border-left: 1px solid #babcbf; }
#flip-scroll th:last-child,
#flip-scroll td:last-child { border-bottom: 1px solid #babcbf; }
}

    
</style>
<div class="widget">
    <div class="path-container Fright">
        <div class="path-name Fright">
            <a href=""><?= lang('global_system_management') ?></a>
        </div>
        <div class="path-arrow Fright">
        </div>
        <div class="path-name Fright">
            <a href=""><?= lang('menu_main_hotels') ?></a>
        </div>
        <div class="path-arrow Fright">
        </div>
        <div class="path-name Fright">
            <?= lang('hotel_res_order') ?>
        </div>
    </div>
</div>


 <div class="widget">
     <div class="widget-header">
         
         <div class="widget-header-title pull-right">بيانات الغرف</div>
        <div class="widget-header-icon pull-right"></div>
     </div>
     <div class="widget-container">
        <div class="table-container">
            
            <div class="row-fluid" style="padding: 10px;">
                <div class="span3">
                    <div class="span3"><label>رقم الحجز</lable></div>
                    <div class="span6"><input class="input-huge" type="text" placeholder="1136" disabled=""></div>
                </div>
                <div class="span8">
             <div class="span3">
                 <div class="span3"><label><?= lang('company')?></label></div>
                <div class="span8">
                    <select class="input-huge">
<option>موفينبيك</option>
<option>2</option>
<option>3</option>
<option>4</option>
<option>5</option>
</select>
</div>           
            </div>
            <div class="span2">
                 <label class="radio">
                    <div class="span2">
<input type="radio" name="optionsRadios" id="optionsRadios1" value="option1" checked></div>
                     <div class="span10"><label><?= lang('amra_co')?></label></div>
                    
</label>
            </div>
            <div class="span2">
                 <label class="radio">
                    <div class="span2">
<input type="radio" name="optionsRadios" id="optionsRadios1" value="option2" checked></div>
                     <div class="span10"><label><?= lang('tourism_co')?></label></div>
                    
</label>
            </div>
            <div class="span3">
                <label class="radio">
                    <div class="span2">
<input type="radio" name="optionsRadios" id="optionsRadios1" value="option3" checked></div>
                    <div class="span10"><label><?= lang('markting_co')?></label></div>
                    
</label>
            </div>
                </div>
                
                
            </div>
            <div class="row-fluid" style="padding: 10px;">
                
           <div class="span3">
                    <div class="span2"><label><?= lang('date') ?></label></div>
                    <div class="span6">  <input class=" input-huge" data-format="yyyy-MM-dd" name="datetime" 
                                   id="date" type="text" value="<?=set_value('datetime')?>" style=" direction:ltr;padding-left: 20px;">
                            <i id="date"  class="icon icon-calendar" style=" margin-right:-23px;margin-top: -1px;"></i>
                            
                            </div>
                </div>
                <div class="span3">
                    <div class="span5"><label><?= lang('res_person') ?></label></div>
                    <div class="span6"><input class="input-huge" type="text" placeholder=""></div>
                    
                </div>
                </div>
                
                
            </div>
            <div class="row-fluid"><a class="btn Fleft" href="javascript:void(0)" onclick="add_har('N' + har_counter++)">
            <?= lang('ha_add') ?>
        </a></div>
         
        
         <table class="" cellpadding="0" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        
                        <th  class="numeric"><?= lang('city') ?></th>
                        <th class="numeric"><?= lang('hotel') ?></th>
                        <th class="numeric"><?= lang('entrydate') ?></th>
                        <th class="numeric"><?= lang('leavingdate') ?></th>
                        <th class="numeric"><?= lang('night-num') ?></th>
                        <th class="numeric"><?= lang('roomtype') ?></th>
                        <th class="numeric"><?= lang('count') ?></th>
                        <th class="numeric"><?= lang('mall') ?></th>
                        <th class="numeric"><?= lang('total') ?></th>
                        <th class="numeric"><?= lang('process') ?></th>
                        
                        
                    </tr>
                </thead>
                <tbody class="har" id='har'>
                   
                    <tr>
                       
                        <td class="numeric"><select class="input-huge">
<option>السعودية</option>
<option>الإمارات</option>
<option>مصر</option>

</select></td>
                        <td class="numeric"><select class="input-huge">
<option>موفينبك</option>
<option>مريديان</option>
<option>سوفتيل</option>

</select></td>
                        <td class="numeric">  <input class=" input-huge" data-format="yyyy-MM-dd" name="datetime" 
                                   id="date" type="text" value="<?=set_value('datetime')?>" style=" direction:ltr;padding-left: 20px;">
                           
                            
                           </td>
                        <td class="numeric">  <input class=" input-huge" data-format="yyyy-MM-dd" name="datetime" 
                                   id="date" type="text" value="<?=set_value('datetime')?>" style=" direction:ltr;padding-left: 20px;">
                           
                            
                           </td>
                        <td class="numeric"><input class="input-huge" type="text" placeholder=""></td>
                        <td><select class="input-huge">
<option>فردي</option>
<option>ثنائي</option>
<option>ثلاثي</option>

</select></td>
                        <td class="numeric"><input class="input-huge" type="text" placeholder=""></td>
                        <td class="numeric"><select class="input-huge">
<option>H/D</option>
<option>R/O</option>

</select></td>
                        <td class="numeric"><input class="input-huge" type="text" placeholder="" disabled=""></td>
                        <td class="TAC">
                            <a ><span class="icon-trash"></span></a>
                        </td>
                    </tr>
                  
                </tbody>
            </table>
        </div>
     </div>

<div class="widget">
    <div class="widget-header">
        <div class="widget-header-title pull-right">بيانات الوجبات</div>
        <div class="widget-header-icon pull-right"></div>
    </div>
    <div class="widget-container">
       <div class="row-fluid"><a class="btn Fleft" href="javascript:void(0)" onclick="add_har('N' + har_counter++)">
            <?= lang('ha_add') ?>
        </a></div>
            
            <table cellpadding="0" cellspacing="0" width="40%!important">
                <thead>
                    <tr>
                        
                       
                   <th><?= lang('mall') ?></th>
                        <th><?= lang('count') ?></th>
                        <th><?= lang('total') ?></th>
                        <th><?= lang('process') ?></th>
                        
                        
                    </tr>
                </thead>
                <tbody class="har" id='har'>
                   
 
                        <td><select class="input-huge">
<option>H/D</option>
<option>F/D</option>


</select></td>
                        <td><input class="input-huge" type="text" placeholder=""></td>
                        <td><input class="input-huge" type="text" placeholder="" disabled=""></td>
                        
                        <td class="TAC">
                            <a ><span class="icon-trash"></span></a>
                        </td>
                    </tr>
                  
                </tbody>
            </table>
        </div>
     </div> 
        
  <div class="widget">
    <div class="widget-header">
        <div class="widget-header-title pull-right">مواصفات عامة للفندق المطلوب</div>
        <div class="widget-header-icon pull-right"></div>
    </div>
    <div class="widget-container">
          <table cellpadding="0" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        
                        <th><?= lang('dist_haram') ?></th>
                        <th><?= lang('price_from') ?></th>
                        
                        <th><?= lang('haram_look') ?></th>
                        <th><?= lang('nationalty') ?></th>
                        <th><?= lang('adds') ?></th>
                        <th><?= lang('more-binfit') ?></th>
                        <th><?= lang('process') ?></th>
                        
                        
                    </tr>
                </thead>
                <tbody class="har" id='har'>
                   
                    <tr>
                       
                        <td><div class="span12"><div class="span6">
                        <div class="span9"><input class="input-huge" type="text" placeholder="">
                        </div><div class="span3"><label><?= lang('to')?></label></div>
                    </div>
                    <div class="span6">
                        <div class="span9"><input class="input-huge" type="text" placeholder=""></div>
                        <div class="span3"><label><?= lang('mitter')?></label></div>
                    </div>
                </div></td>
                        <td><div class="span12">
                                <div class="span9">
                        <div class="span5"><input class="input-huge" type="text" placeholder="">
                        </div><div class="span2"><label><?= lang('to')?></label></div>   
                        <div class="span5"><input class="input-huge" type="text" placeholder=""></div> </div>
                        <div class="span2">
                       <select class="input-huge">
<option>ريال</option>
<option>جنيه</option>
<option>دولار</option>

</select>
                        </div>
                    
                </div></td>
                        <td><input type="checkbox"></td>
                        
                        <td><?= form_multiselect('nationalities', $nationalities, set_value('nationalities'), 'id="nationalities"') ?></td>
                        <td><input class="input-huge" type="text" placeholder=""></td>
                        <td><input class="input-huge" type="text" placeholder=""></td>
                        <td class="TAC">
                            <a ><span class="icon-trash"></span></a>
                        </td>
                    </tr>
                  
                </tbody>
            </table>
        
        
         
        
    </div>
    
</div>  
    
</div>
    </div>
<script type="text/javascript">
    $(function() {
        $('#date').datetimepicker({
            language: 'pt-BR',
            pickTime: false
        });
        $('#nationalities').chosen();
    });
</script>
<script>
function add_har(id) {
    var new_row = [
            "<tr rel=\"" + id + "\">",
            "    <td>",
            "       <select name=\"har_floor_id[" + id + "]\" class=\"input-huge\">",
            <? foreach($har_floors as $key => $value): ?>
            "           <option value=\"<?= $key ?>\"><?= $value ?></option>",
            <? endforeach ?>
            "    </td>",
            "    <td>",
            "       <select name=\"har_room_type_id[" + id + "]\" class=\"input-huge\">",
            <? foreach($had_room_types as $key => $value): ?>
            "           <option value=\"<?= $key ?>\"><?= $value ?></option>",
            <? endforeach ?>
            "    </td>",
            '     <td><?= form_input("har_beds[' + id + ']", FALSE, 'class="input-huge small"') ?>',
            "    </td>",
            "    <td>",
            '        <?= form_input("har_rooms_no[' + id + ']", FALSE, "class=\"input-huge small\"") ?>',
            "    </td>",
            "    <td>",
            '        <?= form_input("har_closed_rooms[' + id + ']", FALSE, 'class=\"input-huge small\"') ?>',
            "    </td>",
            "   <td>",
            "       <input name=\"har_etlal[" + id + "]\" type=\"checkbox\" value=\"1\"  />",
            "   </td>",
            "   <td>",
            '       <?= form_input("har_max_beds_count[' + id + ']", false, 'class="validate[required] input-huge small"') ?>',
            "   </td>",
            "   <td>",
            "       <input name=\"har_can_splitting[" + id + "]\" type=\"checkbox\" value=\"1\"  />",
            "   </td>",
            "   <td>",
            '       <?= form_input("har_purchase_price[' + id + ']", false, 'class="validate[required] input-huge small"') ?>',
            "   </td>",
            
            "   <td>",
            '        <input name="har_offer_status[' + id + ']" type="checkbox" value="1"  />',
            "   </td>",
            "    <td class=\"TAC\">",
            "        <a href=\"javascript:void(0)\" onclick=\"delete_har('" + id + "')\"><span class=\"icon-trash\"></span></a>",
            "    </td>",
            "</tr>"
            ].join("\n");
    $('#har').append(new_row);
}
function delete_har(id, database) {
    if(typeof database == 'undefined')
    {
        $('.har').find('tr[rel="' + id + '"]').remove();
    }
    else
    {
        $('.har').find('tr[rel=' + id + ']').remove();
        var hidden_input = '<input type="hidden" name="har_remove[]" value="' + id + '" />';
        $('#har').append(hidden_input);
    }

}
</script>

