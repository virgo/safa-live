<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); ?>
<link href="<?= CSS ?>/stylesheets.css" rel="stylesheet" type="text/css" />
<? if (lang('global_lang') == 'ar'): ?>
    <link href="<?= CSS ?>/stylesheet-ar.css" rel="stylesheet" type="text/css" />
<? else: ?><? endif ?>
<script type="text/javascript" src='<?= JS ?>/plugins/jquery/jquery-1.9.1.min.js'></script>
<script type="text/javascript" src='<?= JS ?>/plugins/jquery/jquery-ui-1.10.1.custom.min.js'></script>
<script type="text/javascript" src='<?= JS ?>/plugins/bootstrap/bootstrap.min.js'></script>
<link href="<?= CSS_JS ?>/dateandtime/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src='<?= CSS_JS ?>/dateandtime/bootstrap-datetimepicker.min.js'></script>
<style>
body{
height: auto!important;
overflow: auto !important;
background:none transparent !important;
overflow: hidden;
min-height: 1%;
}

.row-fluid{
padding-top: 5px !important;
padding-bottom: 5px !important;
}
label, input, button, select, textarea {
    font-size: 14px;
    font-weight: normal;
    line-height: 26px;
}
</style>

<?php echo  form_open_multipart(false, 'id="frm_contract_approving_phases" ') ?>


<div class="">
    

    <div class="modal-header">  
        <div class="row-fluid">
            <div class="span6"><h3>بيانات طلب الحجز من</h3></div>
            
        </div>
        </div>

<div class="modal-body">
    <div class="widget">
        <div class="widget-container">
            <div class="inner-widget">
                <div class="row-form">
                    <div class="row-fluid">
                 <div class="span3">   
                     <div class="span6"><label><?= lang('req_code')?></label></div>
                    <div class="span6"><input class="input-xlarge" id="disabledInput" type="text" placeholder="Disabled input here..." disabled></div>
                 </div>
                <div class="span3">
                    <div class="span6"><label><?= lang('req_date')?></label></div>
                    <div class="span6"><input class="input-xlarge" id="disabledInput" type="text" placeholder="Disabled input here..." disabled></div>
                </div>
                    
                </div>
            </div>
            <div class="row-form">
                <div class="row-fluid">
                <div class="span4"><div class="span7"><label><?= lang('req_room_count')?></label></div>
                    <div class="span5"><input class="input-xlarge" id="disabledInput" type="text" placeholder="Disabled input here..." disabled></div></div>
                <div class="span3"><div class="span4"><label><?= lang('avelibilty')?></label></div>
                    <div class="span6"><input class="input-xlarge" id="disabledInput" type="text" placeholder="Disabled input here..." disabled></div></div>
                <div class="span5"><div class="span6"><label><?= lang('req_for_complet')?></label></div>
                    <div class="span4"><input class="input-xlarge" id="disabledInput" type="text" placeholder="Disabled input here..." disabled></div></div>
            </div>
            </div>
                
            <div class="row-form">
                
                    <div class="span12" style='text-align: center;'><h5 class="">البيانات الرئيسية والتفصيلية للطلب</h5></div>
            
                    
                    <table class="" cellpadding="0" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        
                        <th  class="numeric"><?= lang('city') ?></th>
                        <th class="numeric"><?= lang('hotel') ?></th>
                        <th class="numeric"><?= lang('entrydate') ?></th>
                        <th class="numeric"><?= lang('leavingdate') ?></th>
                        <th class="numeric"><?= lang('night-num') ?></th>
                        <th class="numeric"><?= lang('roomtype') ?></th>
                        <th class="numeric"><?= lang('count') ?></th>
                        <th class="numeric"><?= lang('mall') ?></th>
                        <th class="numeric"><?= lang('total') ?></th>
                       
                        
                        
                    </tr>
                </thead>
                <tbody class="har" id='har'>
                   
                    <tr>
                       
                        <td class=""><input class="input-huge" type="text" placeholder="" disabled="data"></td>
                        <td class=""><input class="input-huge" type="text" placeholder="" disabled="data"></td>
                        <td class=""><input class="input-huge" type="text" placeholder="" disabled="data"></td>
                        <td class=""><input class="input-huge" type="text" placeholder="" disabled="data"></td>
                        <td class=""><input class="input-huge" type="text" placeholder="" disabled="data"></td>
                        <td><input class="input-huge" type="text" placeholder="" disabled="data"></td>
                        <td class=""><input class="input-huge" type="text" placeholder="" disabled="data"></td>
                        <td class=""><input class="input-huge" type="text" placeholder="" disabled="data"></td>
                        <td class=""><input class="input-huge" type="text" placeholder="" disabled="data"></td>
                        
                    </tr>
                  
                </tbody>
            </table>
            <div class="row-fluid">
                <div class="span2"><div class="span5"><label><?= lang('accept')?></label></div>
                    <div class="span6"><input type="radio" name="optionsRadios" id="optionsRadios2" value="option1"></div></div>
                <div class="span2"><div class="span5"><label><?= lang('regect')?></label></div>
                    <div class="span6"><input type="radio" name="optionsRadios" id="optionsRadios2" value="option2"></div></div>
                <div class="span4"><div class="span9"><label><?= lang('pack_req')?></label></div>
                    <div class="span2"><input type="radio" name="optionsRadios" id="optionsRadios2" value="option3"></div></div>
            </div>
            <div class="row-fluid">
                <div class="span12"><div class="span2"><label><?= lang('resson_req')?></label></div>
                    <div class="span10"><textarea rows="3"></textarea></div></div>
                <div class="span9"><div class="span10"><label><?= lang('')?></label></div>
                    
           
        </div>
            </div>
    </div>
</div>
        </div></div>
</div>
<div class="toolbar bottom TAL">

<button class="btn btn-primary"><?= lang('confirm')?></button>
</div>
</div>