<script>.widget{width:98%;}</script>
<style>input {margin-bottom : 4px !important;}</style>
<div class="row-fluid">
    <?= form_open(false, 'id="validate"') ?>
    <div class="span12">
        <div class="widget">
            <div class="path-container Fright">
                <div class="path-name Fright">
                    <a href="<?= site_url($destination.'/dashboard') ?>"><?= lang('global_system_management') ?></a>
                </div>
                <div class="path-arrow Fright"></div>
                <div class="path-name Fright">
                    <?= lang('global_' . $module) ?>
                </div>
            </div>
        </div>
    </div>
    <div class="widget">
        <div class="widget-header">
            <div class="widget-header-icon Fright"><span class="icos-pencil2"></span></div>
            <div class="widget-header-title Fright"><?= lang('global_' . $module) ?></div>
        </div>
        <div class="block-fluid">
            <? if (validation_errors()): ?>
                <?= validation_errors(); ?>
            <? endif ?> 
            <div class="row-form">
                <div class="span6">
                    <div class="span4">
                        <?= lang($module . '_erp_company_type_id') ?>:
                    </div>
                    <div class="span8">
                        <?= form_dropdown('erp_company_type_id', $erp_company_types, set_value("erp_company_type_id", $item->erp_company_type_id), 'class="validate[required] erp_company_type_id"') ?>
                    </div>
                </div>
                <div class="span6">
                    <div class="span4">
                        <?= lang($module . '_erp_company_id') ?>:
                    </div>
                    <div class="span8">
                        <?= form_dropdown('erp_company_id', $erp_companies, set_value("erp_company_id", $item->erp_company_id), 'class="validate[required] erp_company_id"') ?>
                    </div>
                </div>
            </div>
            <div class="row-form">
                <div class="span6">
                    <div class="span4">
                        <?= lang($module . '_from_date') ?>:
                    </div>
                    <div class="span8">
                        <?= form_input('from_date', set_value("from_date", $item->from_date), 'class="datepicker"') ?>
                    </div>
                </div>
                <div class="span6">
                    <div class="span4">
                        <?= lang($module . '_to_date') ?>:
                    </div>
                    <div class="span8">
                        <?= form_input('to_date', set_value("to_date", $item->to_date), 'class="datepicker"') ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="toolbar bottom TAC">
        <input type="submit" name="submit" value="<?= lang('global_submit') ?>" class="btn btn-primary"> &nbsp;
        <input type ="button" 
               value="<?= lang('global_back') ?>"
               class="btn btn-primary" 
               onclick="window.location = '<?= site_url($module . '/report/') ?>'" />
    </div>
    <?= form_close() ?>
</div>
	<!--<a href="" class="col-md-3 glyphicons film"><i></i><strong>film</strong><span>UTF E009</span></a>-->

<? if (isset($items) && $items && is_array($items)): ?>
<div class="row-fluid">
    <div class="widget">
        <div class="widget-header">
            <div class="widget-header-icon Fright">
                <span class="icos-pencil2"></span>
            </div>
            <div class="widget-header-title Fright">
                <?= lang('global_' . $module) ?>
            </div>
        </div>
        <div class="widget-container">
            <div class='table-responsive' >
                <table cellpadding="0" class="fsTable" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th><?= lang($module . '_date') ?></th>
                            <th><?= lang($module . '_credit') ?></th>
                            <th><?= lang($module . '_debit') ?></th>
                            <th><?= lang($module . '_notes') ?></th>
                        </tr>
                    </thead>
                    <tbody><? $credit = $debit = 0; ?>
                        <? foreach ($items as $item): ?>
                        <tr>
                            <td><?= $item->date ?></td>
                            <? if($item->payment_type == 1): ?>
                            <td><?= $item->amount ?> <?= $item->currency ?> (<?= $credit = $item->amount * $item->rate ?>)</td>
                            <td>-</td>
                            <? else: ?>
                            <td>-</td>
                            <td><?= $item->amount ?> <?= $item->currency ?> (<?= $debit = $item->amount * $item->rate ?>)</td>
                            <? endif ?>
                            <td><?= $item->notes ?></td>
                        </tr>
                        <? endforeach ?>
                         <tr style="background-color: #000; color: whitesmoke; text-shadow: none !important;">
                            <td style="background-color: #000; color: whitesmoke; text-shadow: none !important;"><?= lang($module . '_total') ?></td>
                            <td style="background-color: #000; color: whitesmoke; text-shadow: none !important;"><?= $credit ?></td>
                            <td style="background-color: #000; color: whitesmoke; text-shadow: none !important;"><?= $debit ?></td>
                            <td></td>
                           
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<? endif ?>
<script>
    <? if($_POST): ?>
        update_companies('<?=$this->input->post('erp_company_id') ?>');
    <? endif ?>
    $('.erp_company_type_id').change(function() {
        update_companies();
    });
    function update_companies(erp_company_id)
    {
        $.post('<?= site_url('payments/update_companies') ?>', {id: $('.erp_company_type_id').val(), erp_company_id: erp_company_id}, function(data) {
            $('.erp_company_id').html(data);
        });
    }
</script>