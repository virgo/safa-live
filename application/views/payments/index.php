<script>.widget{width:98%;}</script>
<style>
    input {margin-bottom : 4px !important;}
</style>
<div class="span12">
    <div class="widget">
        <a class="btn Fleft" href="<?= site_url($module.'/manage') ?>"><?= lang('global_add_new_record') ?></a>
        <div class="path-container Fright">
            <div class="path-name Fright">
                <a href="<?= site_url($destination.'/dashboard') ?>"><?= lang('global_system_management') ?></a>
            </div>
            <div class="path-arrow Fright"></div>
            <div class="path-name Fright">
                <?= lang('global_'.$module)?>
            </div>
        </div>
    </div>
</div>

<? $this->load->view($module.'/index_search') ?>

<div class="row-fluid">
    <div class="widget">
        <div class="widget-header">
            <div class="widget-header-icon Fright">
                <span class="icos-pencil2"></span>
            </div>
            <div class="widget-header-title Fright">
                <?= lang('global_'.$module)?>
            </div>
        </div>
        <div class="widget-container">
            <div class='table-responsive' >
                <table cellpadding="0" class="fsTable" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th><?= lang($module.'_voucher') ?></th>
                            <th><?= lang($module.'_date') ?></th>
                            <th><?= lang($module.'_payment_type') ?></th>
                            <th><?= lang($module.'_amount') ?></th>
                            <th><?= lang($module.'_erp_company_id') ?></th>
                            <th><?= lang($module.'_notes') ?></th>
                            <th><?= lang('global_actions') ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <? if (ensure($items)): ?>
                            <? foreach ($items as $item): ?>
                                <tr>
                                    <td><?= $item->voucher ?></td>
                                    <td><?= $item->date ?></td>
                                    <td><?= $item->payment ?></td>
                                    <td><?= $item->amount ?> <?= $item->currency ?></td>
                                    <td><?= $item->company ?></td>
                                    <td><?= $item->notes ?></td>
                                    <td class="TAC">
                                        <!--<a href="<?= site_url($module."/log/" . $item->{$table_pk}) ?>" class="popup_win" rel="fancybox"><span class="icon-camera"></span></a>-->      
                                        <a href="<?= site_url($module."/manage/". $item->{$table_pk}) ?>" ><span class="icon-pencil"></span></a>      
                                    </td>
                                </tr>
                            <? endforeach ?>
                        <? endif ?>
                    </tbody>
                </table>
<!--                <?= $pagination ?>-->
            </div>
        </div>
    </div>
</div>
<script>
        $(".popup_win").fancybox();
</script>



