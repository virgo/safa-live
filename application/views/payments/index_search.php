<?= form_open(false, 'method="get"') ?>
<div class="row-fluid">

    <div class="widget">
        <div class="widget-header">
            <div class="widget-header-icon Fright"><span class="icos-pencil2"></span></div>
            <div class="widget-header-title Fright"><?= lang('global_' . $module) ?></div>
        </div>
        <div class="block-fluid">
            <? if (validation_errors()): ?>
                <?= validation_errors(); ?>
            <? endif ?> 
            <div class="row-form">
                <div class="span6">
                    <div class="span4">
                        <?= lang($module . '_erp_company_type_id') ?>:
                    </div>
                    <div class="span8">
                        <?= form_dropdown('erp_company_type_id', $erp_company_types, set_value("erp_company_type_id"), 'class="validate[required] erp_company_type_id"') ?>
                    </div>
                </div>
                <div class="span6">
                    <div class="span4">
                        <?= lang($module . '_erp_company_id') ?>:
                    </div>
                    <div class="span8">
                        <?= form_dropdown('erp_company_id', $erp_companies, set_value("erp_company_id"), 'class="validate[required] erp_company_id"') ?>
                    </div>
                </div>
            </div>
            <div class="row-form">
                <div class="span6">
                    <div class="span4">
                        <?= lang($module . '_from_date') ?>:
                    </div>
                    <div class="span8">
                        <?= form_input('from_date', set_value("from_date"), 'class="datepicker"') ?>
                    </div>
                </div>
                <div class="span6">
                    <div class="span4">
                        <?= lang($module . '_to_date') ?>:
                    </div>
                    <div class="span8">
                        <?= form_input('to_date', set_value("to_date"), 'class="datepicker"') ?>
                    </div>
                </div>
            </div>
            <div class="row-form">
                <div class="span6">
                    <div class="span4">
                        <?= lang($module . '_from_amount') ?>:
                    </div>
                    <div class="span8">
                        <?= form_input('from_amount', set_value("from_amount")) ?>
                    </div>
                </div>
                <div class="span6">
                    <div class="span4">
                        <?= lang($module . '_to_amount') ?>:
                    </div>
                    <div class="span8">
                        <?= form_input('to_amount', set_value("to_amount")) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="toolbar bottom TAC">
        <input type="submit" name="submit" value="<?= lang('global_submit') ?>" class="btn btn-primary"> &nbsp;
    </div>
</div>
<?= form_close() ?>
<script>
    <? if($_GET): ?>
        update_companies('<?= $_GET['erp_company_id'] ?>');
    <? endif ?>
    $('.erp_company_type_id').change(function(){
        update_companies();        
    });
    function update_companies(erp_company_id)
    {
        $.post('<?= site_url('payments/update_companies') ?>', {id: $('.erp_company_type_id').val(), erp_company_id:erp_company_id}, function(data){
            $('.erp_company_id').html(data);
        });
    }
</script>