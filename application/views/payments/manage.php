<script>.widget{width:98 % ; }</script>
<style>
input {margin-bottom : 2px !important;}
</style>
<span class="span8"></span>
<div class="row-fluid">
    <?= form_open(false, 'id="validate"') ?>
    <div class="span12">
        <div class="widget">
            <div class="path-container Fright">
                <div class="path-name Fright">
                    <a href="<?= site_url($destination . '/dashboard') ?>"><?= lang('global_system_management') ?></a>
                </div>
                <div class="path-arrow Fright"></div>
                <div class="path-name Fright">
                    <?= lang('global_' . $module) ?>
                </div>
            </div>
        </div>
    </div>

    <div class="widget">
        <div class="widget-header">
            <div class="widget-header-icon Fright"><span class="icos-pencil2"></span></div>
            <div class="widget-header-title Fright"><?= lang('global_' . $module) ?></div>
        </div>
        <div class="block-fluid">
            <? if (validation_errors()): ?>
            <?= validation_errors(); ?>
            <? endif ?> 

            <div class="row-form">
                <div class="span6">
                    <div class="span4">
                        <?= lang($module . '_payment_type') ?>:
                    </div>
                    <div class="span8">
                        <?= form_dropdown('payment_type', ddgen('crm_payment_types', array('crm_payment_type_id', name())), set_value("payment_type", $item->payment_type), 'class="validate[required] payment_type"') ?>
                    </div>
                </div>

                <div class="span6">
                    <div class="span4">
                        <?= lang($module . '_date') ?>:
                    </div>
                    <div class="span8">
                        <?= form_input('date', set_value("date", $item->date), 'class="validate[required] datetimepicker"') ?>
                    </div>
                </div>
            </div>

            <div class="row-form">
                <div class="span6">
                    <div class="span4">
                        <?= lang($module . '_voucher') ?>:
                    </div>
                    <div class="span8">
                        <?= form_input('voucher', set_value("voucher", $item->voucher), 'class="validate[required] voucher"') ?>
                    </div>
                </div>

                <div class="span6">
                    <div class="span4">
                        <?= lang($module . '_order_no') ?>:
                    </div>
                    <div class="span8">
                        <?= form_input('order_no', set_value("order_no", $item->order_no), 'class="order_no"') ?>
                    </div>
                </div>
            </div>
            <div class="row-form">
                <div class="span6">
                    <div class="span4">
                        <?= lang($module . '_erp_company_type_id') ?>:
                    </div>
                    <div class="span8">
                        <?= form_dropdown('erp_company_type_id', $erp_company_types, set_value("erp_company_type_id", $item->erp_company_type_id), 'class="validate[required] erp_company_type_id"') ?>
                    </div>
                </div>
                <div class="span6">
                    <div class="span4">
                        <?= lang($module . '_erp_company_id') ?>:
                    </div>
                    <div class="span8">
                        <?= form_dropdown('erp_company_id', $erp_companies, set_value("erp_company_id", $item->erp_company_id), 'class="validate[required] erp_company_id"') ?>
                    </div>
                </div>
            </div>
            <div class="row-form">
                <div class="span6">
                    <div class="span4">
                        <?= lang($module . '_amount') ?>:
                    </div>
                    <div class="span8">
                        <?= form_input('amount', set_value("amount", $item->amount), 'class="validate[required] amount"') ?>
                    </div>
                </div>

                <div class="span6">
                    <div class="span4">
                        <?= lang($module . '_erp_currency_id') ?>:
                    </div>
                    <div class="span8">
                        <?= form_dropdown('erp_currency_id', ddgen('erp_currencies', array('erp_currency_id', name())), set_value("erp_currency_id", $item->erp_currency_id), 'class="validate[required] erp_currency_id"') ?>
                    </div>
                </div>
            </div>
            <div class="row-form">
                <div class="span6">
                    <div class="span4">
                        <?= lang($module . '_rate') ?>:
                    </div>
                    <div class="span8">
                        <?= form_input('rate', set_value("rate", $item->rate), 'class="validate[required] rate"') ?>
                    </div>
                </div>
            </div>
            <div class="row-form">
                <div class="span12">
                    <div class="span2">
                        <?= lang($module . '_notes') ?>:
                    </div>
                    <div class="span10">
                        <?= form_textarea('notes', set_value("notes", $item->notes), 'class="span12"') ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="toolbar bottom TAC">
        <input type="submit" name="submit" value="<?= lang('global_submit') ?>" class="btn btn-primary"> &nbsp;
        <input type ="button" 
               value="<?= lang('global_back') ?>"
               class="btn btn-primary" 
               onclick="window.location = '<?= site_url($module . '/index/') ?>'" />

    </div>
    <?= form_close() ?>       
</div>
<script>
            $('.erp_company_type_id').change(function(){
    $.post('<?= site_url('payments/update_companies') ?>', {id: $('.erp_company_type_id').val()}, function(data){
    $('.erp_company_id').html(data);
    });
    });
</script>