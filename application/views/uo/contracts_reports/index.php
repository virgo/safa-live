
<div class="widget">
   
    <div class="path-container Fright">
    	<div class="path-name Fright">
            <a href="<?php echo  site_url() ?>"><?php echo  lang('global_system_management') ?></a>
        </div>
        <div class="path-arrow Fright"></div>
        <div class="path-name Fright"> <?php echo  lang('contract_approving_reports') ?></div>
    </div>
</div>

<div class="widget">
    <div class="widget-header">
        <div class="widget-header-icon Fright"> <span class="icos-pencil2"></span> </div>
        <div class="widget-header-title Fright">
            <?php echo  lang('contract_approving_reports') ?>
        </div>
        
    </div>
     <div class="widget-container">
          <div class="table-warp">
            <table cellpadding="0" cellspacing="0" class="MyTable" >
                <thead>
                    <tr>
                        <th colspan="3"><?php echo  lang('ea_data') ?></th>
                        <th colspan="<?php echo $contract_phases_count;?>" ><?php echo  lang('contract_phases') ?></th>
                    </tr>
                     <tr>
                        <th><?php echo  lang('name_ar') ?></th>
                        <th><?php echo  lang('name_la') ?></th>
                        <th><?php echo  lang('country') ?></th>
                        
                        <th><?php echo  lang('sending_date') ?></th>
                        <th><?php echo  lang('erp_sending_companies_id') ?></th>
                        <th><?php echo  lang('sending_number') ?></th>
                        
                        <? if(ensure($contract_phases)) { ?>
                    	<? foreach($contract_phases as $contract_phase) {?>
                    	<th><?php echo  $contract_phase->{name()} ?></th>
                        <? } ?>
                		<? } ?> 
                        
                        
                    </tr>
                </thead>
                <tbody id="had">
                <? if(ensure($items)) { ?>
                    <? foreach($items as $item) {?>
                    <tr rel="<?php echo  $item->safa_uo_contract_id; ?>">

                        <td>
                            <?php echo   $item->name_ar ; ?>
                        </td>
                        <td>
                            <?php echo   $item->name_la ; ?>
                        </td>
                        <td>
                            <?php 
                            if(name()=='name_la') {
                            	echo   $item->country_name_la ;
                            } else {
                            	echo   $item->country_name_ar ;
                            } 
                            ?>
                        </td>
                        
                        <td>
                            <?php echo   $item->sending_date ; ?>
                        </td>
                        <td>
                            <?php 
                            if($item->erp_sending_companies_id=='0') {
                            	echo   $item->sending_others_text;
                            } else {
                            	echo   $item->erp_sending_companies_name;
                            }
                            ?>
                        </td>
                        <td>
                            <?php echo   $item->sending_number; ?>
                        </td>
                        
                        
                        <?php 
                        
                        if(ensure($contract_phases)) { 
                        	
                        	
	                    	foreach($contract_phases as $contract_phase) {
	                    		
	                    		
	                    		$this->safa_contract_approving_phases_model->safa_uo_contracts_id = $item->safa_uo_contract_id;
	                    		$this->safa_contract_approving_phases_model->safa_contract_phases_id = $contract_phase->safa_contract_phases_id;
		                        
		                        $contract_approving_phases_documents = $this->safa_contract_approving_phases_model->get(); 
	                    		
		                        $cls_span='';
		                        foreach($contract_approving_phases_documents as $contract_approving_phases_document) {
	                    			
		                        	if($contract_approving_phases_document->safa_contract_approving_phases_status_id!=2) {
										if($contract_approving_phases_document->safa_contract_approving_phases_status_id==1 || $contract_approving_phases_document->safa_contract_approving_phases_status_id==0) {
											$cls_span='icon-file';
										} else {
		                    				$cls_span='icon-remove';
		                    				break;
										}
	                    			} else {
	                    				$cls_span='icon-ok';
	                    			}	
	                    		}
	                    		
		                    	echo "<td>
		                    	<a class='fancybox fancybox.iframe' href='".site_url("uo/contract_approving_phases/reportPhase")."/".$item->safa_uo_contract_id."/".$contract_phase->safa_contract_phases_id."' ><span class='$cls_span'></span></a>
		                    	</td>";
                        	}
                        }
        
                        ?>
                        <td>
                            <a class='fancybox fancybox.iframe'  href='<?= site_url('uo/contracts/sending/'.$item->safa_uo_contract_id) ?>'><?php echo lang('send_contract');?></a>
                        </td>
                        
                    </tr>
                    <? } ?>
                <? } ?> 
                </tbody>
            </table>
            
            <div class="row-fluid"><?= $pagination ?></div>
            
        </div>
    </div>
</div>


<script>
$(document).ready(function() {
    $('.fancybox').fancybox({
        afterClose: function() {
            //location.reload();
        }
    });
});
</script>