
<!-- multi selection choosen -->
<!--<link rel="stylesheet" href="<?= NEW_CSS_JS ?>/multi-select-chosen/chosen.css">-->
<!--<script src="<?= NEW_CSS_JS ?>/multi-select-chosen/chosen.jquery.js" type="text/javascript"></script>-->
<script>
    $(document).ready(function() {
        var visapricerow = 2;
        $("#wizard_validate").validationEngine('attach', {promptPosition: "topLeft", prettySelect: true});
        $('.datepicker').datepicker({
            dateFormat: "yy-mm-dd",
            controlType: 'select',
            timeFormat: 'HH:mm'
        });

        $("button[name=addnewprice]").click(function() {
            var new_row = function() {/*
             <tr id="{$id}">
             <td><?= form_input('visa_price[{$id}]', set_value('visa_price'), ' class="validate[required]"') ?></td>
             <td><?= form_input('visa_date_from[{$id}]', set_value('visa_date_from'), 'class="datepicker"') ?></td>
             <td><?= form_input('visa_date_to[{$id}]', set_value('visa_date_to'), 'class="datepicker"') ?></td>
             <td><?= form_dropdown('visa_price_currency_id[{$id}]', $currencies , '',  'class="validate[required]"') ?></td>
             <td><i class="icon-remove" onclick="$(this).parent().parent().remove();" ></i></td>
             
             </tr>
             */
            }.toString().replace(/{\$id}/g, 'N' + visapricerow);
            visapricerow++;
            $('#visa_prices tbody').append(new_row);
            $('.datepicker').datepicker({
                dateFormat: "yy-mm-dd",
                controlType: 'select',
                timeFormat: 'HH:mm',

                beforeShowDay: function(date){
                if(period(date)){
                    return [false];
                }
                return [true];
            });
            return false;
        });
    });
</script>
<style>
    input {margin-bottom : 0px !important;}
    .Div-Sep{
        margin: 5px 0px !important;
        
    }
    .widget-header {
        clear:both;
    }
    .row-form{border: none!important;}
    .row-form > [class^="span"]{
        border: none!important;
    }
    .wizerd-div {
        border-bottom: 1px solid #575858 !important;
        margin: -33px 0 50px;
        padding-top: 0;
    }    

    .Div-Sep {
        border-bottom: 1px solid #DEDEDE !important;
        height: 10px;
        padding-top: 0;
    }
    .Div-Sep a {
        border: 1px solid #DEDEDE;
    }
    .resalt-group .wizerd-div {
        border-bottom: medium none !important;
        margin: -18px 0 20px !important;
        padding-top: 0;
    }
    .resalt-group .wizerd-div a {
        background: none repeat scroll 0 0 #FAFAFA;
        border: 1px solid #D5D6D6;
        border-radius: 49px;
        color: #663300;
        display: inline-block;
        margin: -6px 5px -17px -8px;
        padding: 10px 12px 8px;
    }
    a {
        color: #C09853;
    }
    .resalt-group {
        margin: 18px 0.5% 0.5%;
        padding: 0.5%;
        width: 99%;
    }

</style>

<div class="widget">
    <div class="path-container Fright">
        <div class="path-name Fright">
            <a href="<?= site_url('uo/dashboard') ?>"><?= lang('global_system_management') ?></a>
        </div>
        <div class="path-arrow Fright">
        </div>
        <div class="path-name Fright">
            <a href='<?= site_url('uo/contracts') ?>'><?= lang('node_title') ?></a>
        </div>
        <div class="path-arrow Fright">
        </div>
        <div class="path-name Fright">
            <span><?= lang('contracts_add') ?></span>
        </div>
    </div>
</div>

<div class="widget" >

    <div class="widget-header">
        <div class="widget-header-icon Fright">
            <span class="icos-pencil2"></span>
        </div>
        <div class="widget-header-title Fright">
            <?= lang('contracts_add') ?>
        </div>
    </div>

    <div class="widget-container slidingDiv">
        <div>

            <?= form_open_multipart("", "method='POST' id='wizard_validate' 'autocomplete='off' ") ?>

            <fieldset title="<?= lang('contract_step1') ?>">
                <legend><?= lang('contract_step1') ?></legend>

                <div class="wizerd-div">
                    <a><?= lang('contract_step1') ?></a>
                    
                </div>


                <div class="row-fluid">
                    <div class="row-fluid">
                        <div class="span3" >
                            <div class="span10"><?= lang('contracts_name_ar') ?>
                                <? if (name() == 'name_ar'): ?><font style='color:red' >*</font><? endif; ?>
                            </div>
                            <div class="span10">
                                <input type ='text' name='contract_name_ar' value='<?= set_value('contract_name_ar') ?>'<? if (name() == 'name_ar') { ?>class="validate[required] input-huge"<? } else { ?> class="input-huge" <?php } ?>/> 
                                <span class='bottom' style='color:red'>
                                    <?= form_error('contract_name_ar') ?>
                                </span>
                            </div> 
                        </div>
                        <div class="span3" >
                            <div class="span10">
                                <?= lang('contracts_name_la') ?>
                                <? if (name() == 'name_la'): ?><font style='color:red' >*</font><? endif; ?>
                            </div>
                            <div class="span10"><input type ='text' name='contract_name_la' value='<?= set_value('contract_name_la') ?>'<? if (name() == 'name_la') { ?>class=" validate[required] input-huge<? } else { ?> class="input-huge" <?php } ?>/> 
                                                       <span class='bottom' style='color:red'>
                                                           <?= form_error('contract_name_la') ?>
                                </span>
                            </div> 
                        </div>
                        <div class="span3" >
                            <div class="span10"><?= lang('contracts_nationality') ?><font style='color:red' >*</font></div>
                            <div class="span10" ><?= form_dropdown('contarct_nationality', ddgen('erp_countries', array('erp_country_id', name())), set_value("contarct_nationality"), " class=' validate[required] input-huge val1'  style='width:100%;' ") ?>
                                <span class='bottom' style='color:red'>
                                    <?= form_error('contarct_nationality') ?>
                                </span>  
                            </div>  
                        </div>
                        <div class="span3" >
                            <div class="span10"><?= lang('contracts_country') ?><font style='color:red' >*</font></div>
                            <div class="span10" ><?= form_dropdown('contarct_country', ddgen('erp_countries', array('erp_country_id', name())), set_value('contarct_country'), " class=' validate[required] input-huge val1' id='contarct_country' style='width:100%;' ") ?>
                                <span class='bottom' style='color:red'>
                                    <?= form_error('contarct_country') ?>
                                </span>   
                            </div>  
                        </div>
                    </div>
                    <div class="Div-Sep"><a></a></div>
                    <div class="row-fluid">    
                        <div class="span3" >
                            <div class="span10"><?= lang('contracts_phone') ?><font style='color:red' >*</font></div>
                            <div class="span10"><?= form_input('contarct_phone', set_value('contarct_phone'), 'class="validate[required,custom[phone]] input-huge"') ?>
                                <span class='bottom' style='color:red'>
                                    <?= form_error('contarct_phone') ?>
                                </span>  
                            </div>
                        </div>
                        <div class="span3" >
                            <div class="span10"><?= lang('contracts_fax') ?></div>
                            <div class="span10"><?= form_input('contarct_fax', set_value('contarct_fax'), "class=' input-huge'") ?></div>
                        </div>
                        <div class="span3" >
                            <div class="span10"><?= lang('contracts_email') ?><font style='color:red' >*</font></div>
                            <div class="span10" ><?= form_input('contarct_email', set_value('contarct_email'), "class='validate[required,custom[email]] input-huge'  ") ?>
                                <span class='bottom' style='color:red'>
                                    <?= form_error('contarct_email') ?>
                                </span>
                            </div> 
                        </div>
                        <div class="span3" >
                            <div class="span10"><?= lang('safa_uo_contracts_status_id') ?><font style='color:red' >*</font></div>
                            <div class="span10" ><?= form_dropdown('safa_uo_contracts_status_id', $safa_uo_contracts_status, set_value('safa_uo_contracts_status_id', ''), 'class=" validate[required] input-huge chosen-select input-full chosen-rtl input-huge" style="width:100%;" id="safa_uo_contracts_status_id"') ?>
                                <span class='bottom' style='color:red'>
                                    <?= form_error('safa_uo_contracts_status_id') ?>
                                </span>
                            </div> 
                        </div>
                    </div>
                    <div class="Div-Sep"><a></a></div>
                    <div class="row-fluid">     
                        <div class="span3" >
                            <div class="span10"><?= lang('contracts_ayata_num') ?></div>
                            <div class="span10"><?= form_input('contarct_ayata_num', set_value('contarct_ayata_num'), "class='input-huge'") ?>
                                <?= form_error('contarct_ayata_num') ?>
                            </div> 
                        </div>
                        <div class="span3" >
                            <div class="span10"><?= lang('contracts_agencysymbol') ?><font style='color:red' >*</font></div>
                            <div class="span10"><?= form_input('contarct_agency_symbol', set_value('contarct_agency_symbol'), "class=' validate[required] input-huge'") ?>
                                <span class='bottom' style='color:red'>
                                    <?= form_error('contarct_agency_symbol') ?>
                                </span>  
                            </div> 
                        </div>
                        <div class="span3" >
                            <div class="span10"><?= lang('contracts_agencyname') ?><font style='color:red' >*</font></div>
                            <div class="span10" ><?= form_input('contarct_agency_name', set_value('contarct_agency_name'), "class=' validate[required] input-huge'") ?>
                                <span class='bottom' style='color:red'>
                                    <?= form_error('contarct_agency_name') ?>
                                </span>    
                            </div>  
                        </div>
                        <div class="span3" >
                            <div class="span10"><?= lang('contracts_address') ?></div>
                            <div class="span10"><?= form_input('contarct_address', set_value('contarct_address'), "class='input-huge'") ?></div> 
                        </div>
                    </div>
                    <div class="Div-Sep"><a></a></div>
                    <div class="row-fluid">     
                        <div class="span3" >
                            <div class="span10"><?= lang('contracts_ksaaddress') ?></div>
                            <div class="span10"><?= form_input('contarct_ksa_address', set_value('contarct_ksa_address'), "class='input-huge'") ?></div>  
                        </div> 
                    </div>








                    <!-------- By Gouda -------->


                    <!-- 
<div class="row-form" >
<div class="span2"><?= lang('safa_contract_phases_id_current') ?><font style='color:red' >*</font></div>
<div class="span10" >
                    <?= form_dropdown('safa_contract_phases_id_current', $safa_contract_phases, set_value('safa_contract_phases_id_current', ''), 'class=" validate[required] input-huge chosen-select input-full chosen-rtl input-huge" style="width:100%;" id="safa_contract_phases_id_current"') ?>
    <span class='bottom' style='color:red'>
                    <?= form_error('safa_contract_phases_id_current') ?>
    </span>
</div> 
</div>
                    -->

                    <!-- 
                    <div class="row-form" >
                        <div class="span2"><?= lang('safa_contract_phases_id_next') ?><font style='color:red' >*</font></div>
                        <div class="span10" >
                    <?= form_dropdown('safa_contract_phases_id_next', $safa_contract_phases, set_value('safa_contract_phases_id_next', ''), 'class=" validate[required] input-huge chosen-select input-full chosen-rtl " style="width:100%;" id="safa_contract_phases_id_next"') ?>
                           <span class='bottom' style='color:red'>
                    <?= form_error('safa_contract_phases_id_next') ?>
                           </span>
                        </div> 
                    </div>
                    -->
                    <!-- ------------------- -->


                    <!-- 
                    </div>
                </div>
            </fieldset>
            <fieldset title="<?= lang('contract_step2') ?>">
                <legend><?= lang('contract_step2') ?></legend>
                <div class="inner-widget" >
                         
                    <div class="widget-header">
                        <div class="widget-header-icon Fright">
                            <span class="icos-pencil2"></span>
                        </div>
                        <div class="widget-header-title Fright">
                    <?= lang('contract_step2') ?>
                        </div>
                    </div>
                                        
                    <div class="widget-container"> 
                    
                    -->










                    <div>

                    </div>
                </div>

            </fieldset>
            <fieldset title="<?= lang('contract_step3') ?>" >
                <legend><?= lang('contract_step3') ?></legend>



                <div class="wizerd-div">
                    <a><?= lang('contract_step3') ?></a>
                </div>



                <div class="row-fluid">

                    <div class="span3">
                        <div class="span10"><?= lang('contracts_resposible_name') ?></div>
                        <div class="span10" ><?= form_input('contarct_responsible_name', set_value('contarct_responsible_name'), "  class='input-huge'") ?></div>
                    </div>
                    <div class="span3">
                        <div class="span10"><?= lang('contracts_resposible_phone') ?></div>
                        <div class="span10" ><?= form_input('contarct_responsible_phone', set_value('contarct_responsible_phone'), " class='validate[custom[phone]] input-huge' ") ?></div>  
                    </div>

                    <div class="span3">
                        <div class="span10"><?= lang('contracts_resposible_email') ?></div>
                        <div class="span10" ><?= form_input('contarct_responsible_email', set_value('contarct_responsible_email'), "class='validate[custom[email]] input-huge'") ?></div>  
                    </div>


                    <div class="span3">
                        <div class="span10" ><?= lang("contracts_resposible_passport_photo_path") ?></div>
                        <div class="span10" >
                            <input type="file" value="" name="responsible_passport_photo_path" id="responsible_passport_photo_path"/>


                            <span class="bottom" style='color:red' >
                                <?= form_error('userfile') ?> 
                            </span>

                        </div>
                    </div>

                </div>	
                <div class="Div-Sep"><a></a></div>    	

                <div class="row-fluid" >
                    <div class="span6">
                        <div class="span2" ><?= lang('contract_notes') ?></div>
                        <div class="span10"><textarea  name="contract_notes"    class="input-huge"> <?= set_value('contract_notes') ?> </textarea></div> 
                    </div>
                </div>



            </fieldset>


            <fieldset title="<?= lang('contract_step4') ?>" >
                <legend><?= lang('contract_step4') ?></legend>
                <div class="wizerd-div">
                    <a><?= lang('contract_step4') ?></a>
                </div>
                <div class="resalt-group">
                    <div class="wizerd-div">
                        <a> <?= lang('contract_step4_w1') ?></a>
                    </div>
                    <div class="row-fluid">
                        <div class="span4" >
                            <div class="span10"><?= lang('contracts_username') ?><font style='color:red' >*</font></div>
                            <div class="span10"><?= form_input('contarct_username', set_value('contarct_username'), " class='validate[required,custom[UserName]] input-huge' readonly='readonly' id='contracts_username' ") ?>
                                <span class='bottom' style='color:red'>
    <!--                                <font style="color:gray" > <?= lang('contracts_unique_username') ?></font>-->
                                    <?= form_error('contarct_username') ?>
                                </span>
                            </div> 
                        </div>
                        <div class="span4" >
                            <div class="span10"><?= lang('contracts_password') ?><font style='color:red' >*</font></div>
                            <div class="span10"><?= form_password('contarct_password', set_value('contarct_password'), " class=' validate[required] input-huge' id='contarct_password' ") ?>
                                <span class='bottom' style='color:red'>
                                    <?= form_error('contarct_password') ?>
                                </span>
                            </div>  
                        </div>
                        <div class="span4" >
                            <div class="span10"><?= lang('contracts_repeat_password') ?><font style='color:red' >*</font></div>
                            <div class="span10"><?= form_password('contarct_confpassword', set_value('contarct_confpassword'), " class='validate[required,equals[contarct_password]] input-huge'") ?>
                                <span class='bottom' style='color:red'>
                                    <?= form_error('contarct_confpassword') ?>
                                </span> 
                            </div>
                        </div>

                    </div>
                    <div class="row-fluid">
                        <div class="span12">
                            <div class="span6">
                                <?= lang('contracts_services_can_by_inside') ?> <font style='color:red' >*</font><br />
                                <?= form_checkbox('services_visa', 1, true, " id='services_visa' ") ?><?= lang('contracts_visa') ?><br />
                                <?= form_checkbox('services_hostel', 1, true, " id='services_hostel' ") ?><?= lang('contracts_hostel') ?><br />
                                <?= form_checkbox('services_travel', 1, true, " id='services_travel' ") ?><?= lang('contracts_travel') ?><br />
                                <span class='bottom' style='color:red'><?= form_error('services') ?></span>
                            </div>
                            <div class="span6">
                                <!-- By Gouda -->
                                <!-- 
                                <?= lang('contracts_services_can_by_outside') ?>
                                -->
                                <div class="span12">
                                    <!--<?= form_checkbox("can_use_other_uo", '1', '', " id='can_use_other_uo' ") ?>--><?= lang("contracts_other_uo_company") ?>
                                </div>
                                <br />
                                <?php
                                if (1 == 1) {
                                    $are_disabled = "disabled='disabled'";
                                } else {
                                    $are_disabled = "";
                                }
                                ?>
                                <?= form_checkbox('out_services_hostel', 1, '', " $are_disabled ") ?><?= lang('contracts_hostel') ?><br />
                                <?= form_checkbox('out_services_travel', 1, '', " $are_disabled ") ?><?= lang('contracts_travel') ?><br />
                                <?= form_checkbox('out_services_other_services', 1, '', "  ") ?><?= lang('contracts_other_services') ?><br />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="resalt-group">
                    <div class="wizerd-div">
                        <a><?= lang('contract_step4_w2') ?></a>
                    </div>
                    <div class="row-form">
                        <div class="span6">
                            <div class="span12"><?= form_radio('transport_cycle_from', '1', set_radio('transport_cycle_from', 1)) ?> <?= lang('contracts_transport_cycle_from_uo') ?></div>
                            <div class="span12" id="transport_cycle_from" style="display: none;">
                                <div class="row-form">
                                    <div class="span12"><?= form_radio('transporters_us', 'all', set_radio('transporters_us', 'all', TRUE)) ?> <?= lang('contracts_transporters_us_all') ?></div>
                                </div>
                                <div class="row-form">
                                    <div class="span12"><?= form_radio('transporters_us', 'list', set_radio('transporters_us', 'list')) ?> <?= lang('contracts_transporters_us_selected') ?>
                                        <?= form_multiselect('transporters_us_values[]', $transporters, set_value('transporters_us_values', $transporters), 'class="chosen-select chosen-rtl input-full" multiple tabindex="4" id="transporters_us_values" data-placeholder=" "') ?>   </div>
                                </div>
                            </div>
                        </div>
                        <div class="span6">
                            <div class="span12"><?= form_radio('transport_cycle_from', '2', set_radio('transport_cycle_from', 2, true)) ?> <?= lang('contracts_transport_cycle_from_others') ?></div>
                        </div>

                    </div>
                </div>
                <div class="resalt-group">
                    <div class="wizerd-div">
                        <a> <?= lang('contract_step4_w3') ?></a>
                    </div>
                    <div class="row-form">

                        <div class="row-form">
                            <?= form_checkbox("can_use_other_uo", '1') ?><?= lang("contracts_other_uo_company") ?>
                        </div>
                        <div class="row-form">
                            <?= form_checkbox("can_use_hotel_marking", '1') ?><?= lang("contracts_hotel_markting") ?>
                        </div>
                        <div class="row-form">
                            <?= form_checkbox("can_book_couch", '1') ?><?= lang("contracts_book_couch") ?>
                        </div>
                    </div>
                </div>
                <div class="resalt-group">
                    <div class="wizerd-div">
                        <a><?= lang('contract_step4_w4') ?></a>
                    </div>
                    <div class="row-form">
                        <div class="row-form">
                            <table id="visa_prices" class="table-warp">
                                <tr>
                                    <th><?= lang('contracts_visa_price') ?> <span class='bottom' style='color:red'><?= form_error('visa_price[]') ?></span></th>
                                    <th><?= lang('contracts_date_from') ?></th>
                                    <th><?= lang('contracts_date_to') ?></th>
                                    <th><?= lang('contracts_currency') ?><span class='bottom' style='color:red'><?= form_error('visa_price_currency_id[]') ?></span></th>
                                    <th></th>
                                </tr>
                                <tr>
                                    <td><?= form_input('visa_price[N1]', set_value('visa_price[N1]') , ' class="validate[required]"') ?></td>
                                    <td><?= form_input('visa_date_from[N1]', set_value('visa_date_from[N1]'), 'class="datepicker" class="visa_date_from"') ?></td>
                                    <td><?= form_input('visa_date_to[N1]', set_value('visa_date_to[N1]'), 'class="datepicker" class="visa_date_to"') ?></td>
                                    <td><?= form_dropdown('visa_price_currency_id[N1]', $currencies, set_value('visa_price_currency_id[N1]') , ' class="validate[required]"') ?></td>
                                    <td><i class="icon-remove" onclick="$(this).parent().parent().remove();" ></i></td>
                                </tr>
                            </table>
                            <span class="12">
                                <button name="addnewprice" class="btn centralize"><?php echo lang('add_new_price');?></button>
                            </span>

                        </div>
                        <div class="row-form">

                            <div class="span4"><?= lang('contracts_max_limit_visa') ?></div>
                            <div class="span8"><?= form_input('max_limit_visa', set_value('max_limit_visa')) ?></div>

                        </div>
                        <div class="row-form">
                            <div class="span3">
                                <div class="span10"><?= lang('contracts_credit_balance') ?> </div>
                                <div class="span10"><?= form_input('credit_balance', set_value('credit_balance')) ?>
                                    <span class='bottom' style='color:red'><?= form_error('credit_balance') ?></span>
                                </div>
                                <div class="span10"><?= lang('contracts_currency') ?> </div>
                                <div class="span10"><?= form_dropdown('credit_balance_currency_id', $currencies) ?>
                                    <span class='bottom' style='color:red'><?= form_error('credit_balance_currency_id') ?></span>
                                </div>
                            </div>
                            <div class="span3">
                                <div class="span10"><?= lang('contracts_withdrowal') ?></div>
                                <div class="span10"><?= form_input('withdrowal', set_value('withdrowal'), 'disabled') ?></div>
                                <div class="span10"><?= lang('contracts_currency') ?></div>
                                <div class="span10"><?= form_dropdown('withdrowal_currency_id', $currencies, '', 'disabled') ?>  <a href="#"><?= lang('contracts_withdrowal_report') ?></a></div>
                            </div>
                            <div class="span3">
                                <div class="span10"><?= lang('contracts_rest') ?></div>
                                <div class="span10"><?= form_input('rest', set_value('rest'), 'disabled') ?></div>
                                <div class="span10"><?= lang('contracts_currency') ?></div>
                                <div class="span10"><?= form_dropdown('rest_currency_id', $currencies, '', 'disabled') ?></div>
                            </div>
                        </div>















                        
                            
                        
                        <div class="row-fluid"style="margin: 3%;width: 94%;border-left:1px solid #fff">
                           <div class="result-group"><div class="wizerd-div">    <a><?= lang('contract_uasp') ?></a></div><br />
                            <div class="row-fluid">
                                <div class="span4">
                                    <div class="span11">
                                        <?= lang('uasp_username') ?>
                                    </div>
                                    <div class="span11">
                                        <?= form_input('uasp_username', set_value('uasp_username'), "class=' input-huge'") ?>
                                    </div>
                                </div>
                                <div class="span4">
                                    <div class="span11">
                                        <?= lang('uasp_password') ?>
                                    </div>
                                    <div class="span11">
                                        <?= form_input('uasp_password', set_value('uasp_password'), "class=' input-huge'") ?>
                                    </div>
                                </div>
                                <div class="span4">
                                    <div class="span11">
                                        <?= lang('uasp_eacode') ?>
                                    </div>
                                    <div class="span11">
                                        <?= form_input('uasp_eacode', set_value('uasp_eacode'), "class=' input-huge'") ?>
                                    </div>
                                </div>
                            </div>
                        </div></div>






                        <div class=" resalt-group ">
                            <div class="span4"><?= form_checkbox('can_over_limit', 1) ?><?= lang('contracts_can_over_limit') ?></div>
                            <div class="span8  " style="margin-top: 5px; color: #DC143C;"><?= lang('can_over_limit_description') ?>
                            </div>
                        </div>


						<div class=" resalt-group ">
                            <div class="span12"><?= form_checkbox('add_passports_accomodation_without_visa', 1, 1) ?><?= lang('add_passports_accomodation_without_visa') ?></div>
                            </div>
                        </div>


                    </div>


<div class='row-form'>

            <div class="span12">
                <button name="submit" class="btn btn-primary finish" ><?= lang('contract_save') ?></button>
                <a href="<?= site_url('uo/contracts/index') ?>" class="btn btn-primary" style="line-height: 10px; width: 40px; margin-top: 10px;" ><?= lang('global_back') ?></a>
            </div>
        </div>
<?= form_close() ?> 
                </div>    
        </div>
        



        </fieldset>

    </div>


</div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        var config = {
            '.chosen-select': {width: "100%"},
            '.chosen-select-deselect': {allow_single_deselect: true},
            '.chosen-select-no-single': {disable_search_threshold: 10},
            '.chosen-select-no-results': {no_results_text: 'Oops, nothing found!'},
            '.chosen-select-width': {width: "95%"}
        }
        for (var selector in config) {
            $(selector).chosen(config[selector]);
        }


        // binds form submission and fields to the validation engine
        $("#frm_contract_approving_phases").validationEngine({
            prettySelect: true,
            useSuffix: "_chosen",
            promptPosition: "topRight:-150"
                    //promptPosition : "bottomLeft"
        });



        $("#can_use_other_uo").change(function() {
            if ($('#can_use_other_uo').attr('checked')) {

                $("input[name='out_services_hostel']").removeAttr("disabled");
                $("input[name='out_services_travel']").removeAttr("disabled");
                $("input[name='out_services_other_services']").removeAttr("disabled");

            } else {
                $("input[name='out_services_hostel']").attr("disabled", "disabled");
                $("input[name='out_services_travel']").attr("disabled", "disabled");
                $("input[name='out_services_other_services']").attr("disabled", "disabled");
            }
        });




        $("#services_hostel").change(function() {
            if ($('#services_hostel').attr('checked')) {
                $("input[name='out_services_hostel']").removeAttr("checked");
                $("input[name='out_services_hostel']").attr("disabled", "disabled");
            } else {
                $("input[name='out_services_hostel']").removeAttr("disabled");
            }
        });

        $("#services_travel").change(function() {
            if ($('#services_travel').attr('checked')) {
                $("input[name='out_services_travel']").removeAttr("checked");
                $("input[name='out_services_travel']").attr("disabled", "disabled");
            } else {
                $("input[name='out_services_travel']").removeAttr("disabled");
            }
        });



    });


</script>
<script type="text/javascript">
    /*initializing the map*/
    $(document).ready(function() {

        $("#wizard_validate").formToWizard();
        $('.next').click(function() {
            var parent_ = $("#step_map").parent(); /* init the map */
            if ($(parent_).attr("style") == "display: block;") {
                var alharam = get_alharam();
                initialize(alharam['lat'], alharam['long'], false);
            }
            /*removing the fils from the inputs*/
            var parent_ = $("#step_image").parent();
            if ($(parent_).attr("style") == "display: none;") {
                $("#file_upload_images #file_images").val('');
            }

        });

        $('input[name=transport_cycle_from]').click(function() {
            if ($(this).val() == 2) {
                $('div#transport_cycle_from').show();
            } else {
                $('div#transport_cycle_from').hide();
            }
        });


        $('input[name=transporters_us]').click(function() {
            if ($(this).val() == "all") {
                $('#transporters_us_values').val([]);
                $("#transporters_us_values").attr('disabled', true).trigger("chosen:updated");
            } else {
            	$("#transporters_us_values").attr('disabled', false).trigger("chosen:updated");
            }
        });

    });
</script>



<!-- By Gouda -->

<script>

//var excludedays = { "period": [{ "from": "2/5/2014", "to": "2/11/2014" }, { "from": "2/15/2014", "to": "2/18/2014" }]};

function period(date){

	var excludedays_arr = new Array();
	$('#visa_prices tbody tr').each(function () {
		var tr_id = $(this).attr('id');
		
		//if(tr_id!=undefined && tr_id!='N1') {
		if(tr_id!=undefined) {
			var visa_date_from = $('input:text[name="visa_date_from['+tr_id+']"]').val();
			var visa_date_to = $('input:text[name="visa_date_to['+tr_id+']"]').val();
			
			//alert(visa_date_from +'    -    '+ visa_date_to);
			excludedays_arr.push({ from: visa_date_from, to: visa_date_to });
		}

		
	});

	//console.log(excludedays_arr);
	var excludedays = { period: excludedays_arr};
	
	//console.log(excludedays);
	
	
   var i, num, period, start, startArray, end, endArray;
   num = excludedays.period.length;
   for(i=0;i<num;i++){
	   period = excludedays.period[i];
	   //console.log(period.from);
	   //console.log(period.to);
		
       startArray = period.from.split('-');
       start = new Date(startArray[0], (startArray[1] - 1), startArray[2]);

       console.log(startArray[0]);
       console.log(startArray[1]);
       console.log(startArray[2]);
       
       endArray = period.to.split('-');
       end = new Date(endArray[0], (endArray[1] - 1), endArray[2]);       
       if(date>=start && date<=end){
           return true;
       }
   }
   return false;
}
</script>



<script>
    $(document).ready(function() {
        $("#contarct_country").change(function() {
            $.get('<?= site_url("uo/contracts/get_contracts_username_by_country_ajax") ?>/' + $(this).val(), function(data) {
                $("#contracts_username").val(data);
            });
        });
    });
</script>
