<div class="span12">
    <div class="widget">
        <a title="<?= lang('global_add_new_record') ?>" href="<?= site_url("uo/contracts/add") ?>" class="btn Fleft"><?= lang('global_add') ?></a>

        <div class="path-container Fright">
            <div class="path-name Fright">
                <a href="<?= site_url('uo/dashboard') ?>"><?= lang('global_system_management') ?></a>
            </div>
            <div class="path-arrow Fright">
            </div>
            <div class="path-name Fright">
                <span><?= lang('node_title') ?></span>
            </div>
        </div>

    </div>
</div>



<div class='row-fluid'>
    <?= $this->load->view('uo/contracts/search') ?>
</div>
<div class='row-fluid'>
        <div class='widget'>
            
            
            <div class="dialog" id="deleting_error" style="display: none;" title="Error">
                <p><?= lang('global_select_one_record_at_least') ?></p>                
            </div>
          
     
	        <div class="widget-header">
	
	            <div class="widget-header-icon Fright">
	                <span class="icos-pencil2"></span>
	            </div>
	
	            <div class="widget-header-title Fright">
	                <?= lang('uo_contracts_title') ?>
	            </div>
	
	        </div>
            
            
            <div class="widget-container">
            <div class='table-responsive' >
                <?= form_open("uo/contracts/delete_all", "id='form_delete'") ?>
                <table class="fsTable">
                    <thead>
                        <tr>
                            <th><?= lang('contracts_name') ?></th>
                            <th><?= lang('contracts_country') ?></th>
                            <th><?= lang('contracts_phone') ?></th>
                            <th><?= lang('contracts_address') ?></th>
                            <th><?= lang('global_actions') ?></th>
                        </tr>
                    </thead>
                    <tbody>

                        <? if ($items && is_array($items) && count($items)): ?>
                            <? $name = name(); ?>
                            <? foreach ($items as $item): ?>
                                <tr>
                                    <td><?= $item->$name ?></td>
                                    <td><?= $item->country_name ?></td>
                                    <td><?= $item->phone ?></td>
                                    <td><?= $item->address ?></td>
                                    <td align="center" >
                                        <a href="<?= site_url("uo/contracts/edit") ?>/<?= $item->safa_uo_contract_id ?>"><span class="icon-pencil"></span></a> 
                                        <a href="<?= site_url("uo/contracts/itos") ?>/<?= $item->safa_uo_contract_id ?>" name="<?= $item->name_ar?>"><span class="icon-wrench" title="<?= lang('contract_itos')?>"></span></a>  
                                        <a href="<?= site_url("uo/contracts/hotels") ?>/<?= $item->safa_uo_contract_id ?>" name="<?= $item->name_ar?>"><span class="icon-home" title="<?= lang('contract_hotels')?>"></span></a> 
                                        <a class='fancybox fancybox.iframe' href="<?= site_url("uo/contract_approving_phases/index") ?>/<?= $item->safa_uo_contract_id ?>" ><span class="icon-file" title="<?= lang('contract_approving')?>"></span></a>   
	                                    <a class='fancybox fancybox.iframe' href="<?= site_url("uo/contract_approving_phases/approve") ?>/<?= $item->safa_uo_contract_id ?>" ><span class="icon-list-alt" title="<?= lang('view_ea_documents')?>"></span></a>   
	                                    <a class='fancybox fancybox.iframe' href="<?= site_url("uo/contract_approving_phases/history") ?>/<?= $item->safa_uo_contract_id ?>" ><span class="icon-retweet" title="<?= lang('contract_phases_history')?>"></span></a>   
	                                    <a class='fancybox fancybox.iframe' href="<?= site_url("uo/contracts/resend_email_popup") ?>/<?= $item->safa_uo_contract_id ?>"  onclick="return confirm('<?= lang('are_you_want_to_send_contract_data_email')?>')" ><span class="icon-envelope" title="<?= lang('resend_email_with_contract_data')?>"></span></a> 
                                        
                                    </td>
                                </tr>
                            <? endforeach; ?>
                        <? endif; ?>
                    </tbody>
                </table>
                <?= form_close() ?>
                 
<!--                <div><?= $pagination ?></div>-->
                 
            </div>
        </div>
        </div>
</div>

<script>
$(document).ready(function() {
    $('.fancybox').fancybox({
        afterClose: function() {
            //location.reload();
        }
    });
});

</script>