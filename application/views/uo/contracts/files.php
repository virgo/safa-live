<br />
    <? foreach ($data as $phaserow) : ?>
    <div class='widget' style="width: 95%;">
        <div class='row-fluid'>
            <div class="widget-header-title Fright">
                <?= $phaserow['phase']->phase_name . "   ( " . $phaserow['phase']->status_name . " )" ?>
            </div>
        </div>
        <? foreach ($phaserow['files'] as $file) : ?>
            <div class="widget-container">
                <div class='row-form' >
                    <a href="<?= base_url($file->document_path) ?>"><?= $file->{name()} ?></a>
                </div>
            </div>
        <? endforeach ?>
        </div>
    <? endforeach ?>
<br />



