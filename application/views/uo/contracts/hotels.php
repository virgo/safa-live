<div class='row-fluid'>

        <div class='widget'>

            
            
            
            <div class="widget-header">
	            <div class="widget-header-icon Fright">
	                <span class="icos-pencil2"></span>
	            </div>
	            <div class="widget-header-title Fright">
	                <?= lang('connect_contract') ?> <?= item("safa_uo_contracts", name(), array("safa_uo_contracts.safa_uo_contract_id" => $this->uri->segment("4"))) ?>  <?= lang('hotels') ?> 
	            </div>
	        </div>
	        
            
            <div class='table-responsive' > 
                <?= form_open("uo/contracts/delete_all", "id='form_delete'") ?>
                <table cellpadding="0" class="fpTable" cellspacing="0">
                    <thead>
                        <tr>
                            <th><?= lang('contracts_hotels_arabic_name') ?></th>
                            <th><?= lang('contracts_hotels_english_name')?></th>
                            <th><?= lang('contracts_hotels_city')?></th>
                            <th><?= lang('contracts_hotels_hotel_level')?></th>
                            <th><?= lang('global_actions')?></th>
                        </tr>
                    </thead>
                    <tbody>

                        <? if ($hotels && is_array($hotels) && count($hotels)): ?>
                            <? foreach ($hotels as $item): ?>
                                <tr id="row_<?php echo $item->erp_hotel_id; ?>">
                                    <td><?= $item->name_ar ?></td>
                                    <td><?= $item->name_la?></td>
                                    <td><? if (lang('global_lang') == 'ar'): ?><?= $item->city_name ?><? endif ?></td>
                                    <td><? if (lang('global_lang') == 'ar'): ?><?= $item->level_name ?><? endif ?></td>
                                    <td align="center">
                                        <? if ($this->contracts_model->check_contract_hotel($item->erp_hotel_id, $this->uri->segment("4")) > 0): ?>
                                            <a  style="color:red"  contract_id="<?= $this->uri->segment("4") ?>" name="<?= $item->name_ar ?>" id="<?= $item->erp_hotel_id ?>"  onclick="remove_item(this)"  href="javascript:void(0)"><?= lang('global_delete') ?></a>
                                        <? else : ?>
                                            <a   contract_id="<?= $this->uri->segment("4") ?>" name="<?= $item->name_ar ?>" id="<?= $item->erp_hotel_id ?>" onclick="add_item(this)" href="javascript:void(0)"><?= lang('global_add') ?></a>
                                        <? endif; ?>

                                    </td>
                                </tr>
                            <? endforeach; ?>
                        <? endif; ?>
                    </tbody>
                </table>
                <?= form_close() ?>


            </div>
        </div>
</div>
 
 <?= $pagination ?>
 
<div class="TAC">
    <input  type ="button" value="<?= lang('global_back') ?>"class="btn btn-primary" onclick="window.location = '<?= site_url('uo/contracts/index') ?>'">
</div>

<script>
    function remove_item(btn) {
        var name = $(btn).attr('name');
        var answer = confirm("<?= lang('global_are_you_sure_you_want_to_delete') ?>" + " " + name);
        if (answer === true) {
            var erp_hotel_id = $(btn).attr('id');
            var safa_uo_contract_id = $(btn).attr('contract_id');
            $.ajax({
                type: "POST",
                dataType: 'json',
                url: "<?php echo site_url('uo/contracts/delete_hotel'); ?>",
                data: {'erp_hotel_id': $(btn).attr('id'), 'safa_uo_contract_id': $(btn).attr('contract_id')},
                success: function(msg) {
                    if (msg.response === true) {
                        var del = erp_hotel_id;
<? if (lang('global_lang') == 'ar'): ?>
                            notify("<?= lang('global_deleted_message') ?>",name);
                    <? else: ?>
                            notify("<?= lang('global_deleted_message') ?>",name);
                    <? endif; ?>
                        $("#" + erp_hotel_id).html('<?= lang('global_add') ?>');
                        $("#" + erp_hotel_id).attr('onclick', 'add_item(this)');
                        $("#" + erp_hotel_id).removeAttr('style');
                    } else if (msg.response === false) {
                        alert("error");
                    }
                }
            });
        } else {
            return FALSE;
        }
    }
    function add_item(btn){
       var id = $(btn).attr('id');
        var name = $(btn).attr('name');
        var answer = confirm("<?= lang('global_are_you_sure_you_want_to_add') ?>" + " " + name);
        if (answer === true) {
            var erp_hotel_id = $(btn).attr('id');
            var safa_uo_contract_id =$(btn).attr('contract_id');
            $.ajax({
                type: "POST",
                dataType: 'json',
                url: "<?php echo site_url('uo/contracts/add_hotel'); ?>",
                data:{'erp_hotel_id': $(btn).attr('id'), 'safa_uo_contract_id': $(btn).attr('contract_id')},
                success: function(msg) {
                    if (msg.response === true) {
                         <? if (lang('global_lang') == 'ar'): ?>
                            notify("<?= lang('global_added_successfully') ?>",name);
                        <? else: ?>
                            notify("<?= lang('global_added_successfully') ?>",name);
<? endif; ?>                
                            $("#" + erp_hotel_id).html('<?= lang('global_delete') ?>');
                            $("#" + erp_hotel_id).attr('onclick','remove_item(this)');
                            $("#" + erp_hotel_id).css({'color':'red'});
                            $("#" + erp_hotel_id).attr('onclick','remove_item(this)');
                            
                    } else if (msg.response === false) {
                        alert("error");
                    }
             }
          });
       }
     }  
    </script>
 
    

    
  