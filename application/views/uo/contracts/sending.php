<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); ?>
<link href="<?= CSS ?>/stylesheets.css" rel="stylesheet" type="text/css" />
<? if (lang('global_lang') == 'ar'): ?>
    <link href="<?= CSS ?>/stylesheet-ar.css" rel="stylesheet" type="text/css" />
<? else: ?><? endif ?>
<script type="text/javascript" src='<?= JS ?>/plugins/jquery/jquery-1.9.1.min.js'></script>
<script type="text/javascript" src='<?= JS ?>/plugins/jquery/jquery-ui-1.10.1.custom.min.js'></script>
<script type="text/javascript" src='<?= JS ?>/plugins/bootstrap/bootstrap.min.js'></script>
<link href="<?= CSS_JS ?>/dateandtime/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src='<?= CSS_JS ?>/dateandtime/bootstrap-datetimepicker.min.js'></script>
<style>
body{
height: auto!important;
overflow: auto !important;
background:none transparent !important;
overflow: hidden;
min-height: 1%;
}

.row-fluid{
padding-top: 5px !important;
padding-bottom: 5px !important;
}
</style>




<?php echo  form_open_multipart(false, 'id="frm_sending" ') ?>



<div class="">
    
	<div class="modal-header">  
       
        </div>
        
    <div class="modal-body">  
        <? if(validation_errors()){ ?>
            <?php echo validation_errors(); ?>        
        <? } ?>
        
        <div class="row-fluid">
            <div class="span6">
                <div class="span4 TAL Pleft10">
                    <?php echo  lang('contract_name') ?>
                </div>
                <div class="span8">
                    <?php echo form_input('contract_name', set_value("contract_name", $item->{name()})," style='' id='contract_name' class=' input-full' readonly='readonly' ") ?>
                </div>
			</div>
        </div>
        
        <div class="row-fluid">
            <div class="span6">
                <div class="span4 TAL Pleft10">
                    <?php echo  lang('sending_number') ?>
                </div>
                <div class="span8">
                    <?php echo form_input('sending_number', set_value("sending_number", $item->sending_number)," style='' id='sending_number' class=' input-full'  ") ?>
                </div>
			</div>
        </div>
        
        <div class="row-fluid">
            <div class="span6">
                <div class="span4 TAL Pleft10">
                    <?php echo  lang('sending_date') ?>
                </div>
                <div class="span8">
                    <?php echo form_input('sending_date', set_value("sending_date", $item->sending_date)," style='' id='sending_date' class='sending_date'  ") ?>
                    
                    <script>
                        $('.sending_date').datepicker({
                            dateFormat: "yy-mm-dd",
                            controlType: 'select',
                            timeFormat: 'HH:mm'
                        });
                    </script>
                </div>
			</div>
        </div>
        
        <div class="row-fluid">
            <div class="span6">
                <div class="span4 TAL Pleft10">
                    <?php echo  lang('erp_sending_companies_id') ?>
                </div>
                <div class="span8">
                <?php 
                $selected_erp_sending_companies_id='';
                if($item->sending_number!='') {
	                if($item->erp_sending_companies_id==0) {
	                	$selected_erp_sending_companies_id='-';
	                } else {
	                	$selected_erp_sending_companies_id = $item->erp_sending_companies_id;
	                }
                } 
                ?>
                    <?php echo  form_dropdown('erp_sending_companies_id', $erp_sending_companies, set_value('erp_sending_companies_id', $selected_erp_sending_companies_id), 'class="validate[required] input-full chosen-rtl " style="width:100%;" id="erp_sending_companies_id"') ?>
                </div>
			</div>
			
			<div class="span6" >
                
                <div class="span8">
                <?php 
                $style_sending_others_text='display: none;';
                if($selected_erp_sending_companies_id=='-') {
                	$style_sending_others_text='display: block;';
                }
                ?>
                    <?php echo form_input('sending_others_text', set_value("sending_others_text", $item->sending_others_text)," style='$style_sending_others_text' id='sending_others_text' class='input-full' ") ?>
                </div>
			</div>
        </div>
        
        
        
        
                
    </div>
</div>



<div class="toolbar bottom TAL">
    <input type="submit" class="btn" name="smt_save" value="<?php echo lang('global_submit') ?>" style="margin:10px;padding: 5px;height: auto">
</div>


<?php echo  form_close() ?>


<div class="footer"></div>



<script type="text/javascript">
$(document).ready(function() {
    var config = {
        '.chosen-select': {width: "100%"},
        '.chosen-select-deselect': {allow_single_deselect: true},
        '.chosen-select-no-single': {disable_search_threshold: 10},
        '.chosen-select-no-results': {no_results_text: 'Oops, nothing found!'},
        '.chosen-select-width': {width: "95%"}
    }
    for (var selector in config) {
        $(selector).chosen(config[selector]);
    }


    // binds form submission and fields to the validation engine
    $("#frm_sending").validationEngine({
            prettySelect : true,
            useSuffix: "_chosen",
            promptPosition : "topRight:-150"
            //promptPosition : "bottomLeft"
    });

 
});




$('#erp_sending_companies_id').change(function(){ 
    //alert('');
    var erp_sending_companies_id=$(this).val();
    
	if(erp_sending_companies_id=='-') {
		$('#sending_others_text').css("display", "block");
	} else {
		$('#sending_others_text').css("display", "none");
	}
    
});
</script>
