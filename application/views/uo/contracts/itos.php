<div class='row-fluid'>
    <div class="span12" >

        <div class='widget'>

            
            
            <div class="widget-header">
	            <div class="widget-header-icon Fright">
	                <span class="icos-pencil2"></span>
	            </div>
	            <div class="widget-header-title Fright">
	                <?= lang('connect_contract') ?> <?= item("safa_uo_contracts", name(), array("safa_uo_contracts.safa_uo_contract_id" => $this->uri->segment("4"))) ?>  <?= lang('itos') ?>  
	            </div>
	        </div>
            
            
            <div class='table-responsive' > 
                <?= form_open("uo/contracts/delete_all", "id='form_delete'") ?>
                <table cellpadding="0" class="fpTable" cellspacing="0">
                    <thead>
                        <tr>
                            <th><?= lang('contracts_ito_name') ?></th>
                            <th><?= lang('global_actions')?></th>
                        </tr>
                    </thead>
                    <tbody>

                        <? if ($itos && is_array($itos) && count($itos)): ?>
                            <? foreach ($itos as $item): ?>
                                <tr id="row_<?php echo $item->safa_ito_id; ?>">
                                    <td><?= $item->name_ar ?></td>
                                    <td align="center">
                                        <? if ($this->contracts_model->check_contract_ito($item->safa_ito_id, $this->uri->segment("4")) > 0): ?>
                                            <a  style="color:red"  contract_id="<?= $this->uri->segment("4") ?>" name="<?= $item->name_ar ?>" id="<?= $item->safa_ito_id ?>"  onclick="remove_item(this)"  href="javascript:void(0)"><?= lang('global_delete') ?></a>
                                        <? else : ?>
                                            <a   contract_id="<?= $this->uri->segment("4") ?>" name="<?= $item->name_ar ?>" id="<?= $item->safa_ito_id ?>" onclick="add_item(this)" href="javascript:void(0)"><?= lang('global_add') ?></a>
                                        <? endif; ?>

                                    </td>
                                </tr>
                            <? endforeach; ?>
                        <? endif; ?>
                    </tbody>
                </table>
                <?= form_close() ?>


            </div>
        </div>
    </div>
</div>
<div class="TAC">
    <input  type ="button" value="<?= lang('global_back') ?>"class="btn btn-primary" onclick="window.location = '<?= site_url('uo/contracts/index') ?>'">
</div>

<script>
    function remove_item(btn) {
        var name = $(btn).attr('name');
        var answer = confirm("<?= lang('global_are_you_sure_you_want_to_delete') ?>" + " " + name);
        if (answer === true) {
            var safa_ito_id = $(btn).attr('id');
            var safa_uo_contract_id = $(btn).attr('contract_id');
            $.ajax({
                type: "POST",
                dataType: 'json',
                url: "<?php echo site_url('uo/contracts/delete_ito'); ?>",
                data: {'safa_ito_id': $(btn).attr('id'), 'safa_uo_contract_id': $(btn).attr('contract_id')},
                success: function(msg) {
                    if (msg.response === true) {
                        var del = safa_ito_id;
<? if (lang('global_lang') == 'ar'): ?>
                            notify("<?= lang('global_deleted_message') ?>",name);
                    <? else: ?>
                            notify("<?= lang('global_deleted_message') ?>",name);
                    <? endif; ?>
                        $("#" + safa_ito_id).html('<?= lang('global_add') ?>');
                        $("#" + safa_ito_id).attr('onclick', 'add_item(this)');
                        $("#" + safa_ito_id).removeAttr('style');
                    } else if (msg.response === false) {
                        alert("error");
                    }
                }
            });
        } else {
            return FALSE;
        }
    }
    function add_item(btn){
       var id = $(btn).attr('id');
        var name = $(btn).attr('name');
        var answer = confirm("<?= lang('global_are_you_sure_you_want_to_add') ?>" + " " + name);
        if (answer === true) {
            var safa_ito_id = $(btn).attr('id');
            var safa_uo_contract_id =$(btn).attr('contract_id');
            $.ajax({
                type: "POST",
                dataType: 'json',
                url: "<?php echo site_url('uo/contracts/add_ito'); ?>",
                data:{'safa_ito_id': $(btn).attr('id'), 'safa_uo_contract_id': $(btn).attr('contract_id')},
                success: function(msg) {
                    if (msg.response === true) {
                         <? if (lang('global_lang') == 'ar'): ?>
                            notify("<?= lang('global_added_successfully') ?>",name);
                        <? else: ?>
                            notify("<?= lang('global_added_successfully') ?>",name);
<? endif; ?>                
                            $("#" + safa_ito_id).html('<?= lang('global_delete') ?>');
                            $("#" + safa_ito_id).attr('onclick','remove_item(this)');
                            $("#" + safa_ito_id).css({'color':'red'});
                            $("#" + safa_ito_id).attr('onclick','remove_item(this)');
                            
                    } else if (msg.response === false) {
                        alert("error");
                    }
             }
          });
       }
     }  
    </script>
 
    

    
  