<div class="widget">
    
    <div class="path-container Fright">
    	<div class="path-name Fright">
            <a href="<?php echo  site_url() ?>"><?php echo  lang('global_system_management') ?></a>
        </div>
        <div class="path-arrow Fright"></div>
        <div class="path-name Fright"> <a href="<?php echo  site_url('uo/contract_phases') ?>"><?php echo  lang('menu_title') ?></a></div>
        
        <div class="path-arrow Fright"></div>
        <div class="path-name Fright"><?php echo  lang('edit_title') ?></div>
    
    </div>
</div>


<?php echo  form_open_multipart(false, 'id="frm_contract_phases" ') ?>


<div class="widget">
    <div class="widget-header">

        <div class="widget-header-icon Fright">
            <span class="icos-pencil2"></span>
        </div>

        <div class="widget-header-title Fright">
            <?php echo  lang('main_data') ?>
            
        </div>
    </div>

    <div class="widget-container">  
        <? if(validation_errors()){ ?>
            <?php echo validation_errors(); ?>        
        <? } ?>
        
        <div class="row-form">
            <div class="span6">
                <div class="span4 TAL Pleft10">
                    <?php echo  lang('name_ar') ?>
                </div>
                <div class="span8">
                    <?php echo form_input('name_ar', set_value("name_ar", $item->name_ar)," style='' id='name_ar' class='validate[required] input-full' ") ?>
                </div>
			</div>
        
            <div class="span6">
                <div class="span4 TAL Pleft10">
                    <?php echo  lang('name_la') ?>
                </div>
                <div class="span8">
                    <?php echo form_input('name_la', set_value("name_la", $item->name_la)," style='' id='name_la' class='validate[required] input-full' ") ?>
                </div>
			</div>
        
        </div>
        
        
        <div class="row-form">
            <div class="span6">
                <div class="span4 TAL Pleft10">
                <?php echo  lang('nationalities') ?>
                </div>
                <div class="span8">
                    <?php echo  form_multiselect('nationalities[]', $nationalities, set_value('nationalities', $item_nationalities), 'class="chosen-select chosen-rtl input-full" multiple tabindex="4" id="nationalities" data-placeholder="'.lang('global_all').'"') ?>
                </div>
            </div>
            
            <div class="span6">
                <div class="span4 TAL Pleft10">
                    <a id="all_nationalities_lnk" href="javascript:void()" ><?php echo  lang('all_nationalities') ?></a>
                </div>
                <div class="span4 TAL Pleft10">
                    <a id="arabic_nationalities_lnk" href="javascript:void()" ><?php echo  lang('arabic_nationalities') ?></a>
                </div>
                <div class="span4 TAL Pleft10">
                    <a id="foreign_nationalities_lnk" href="javascript:void()" ><?php echo  lang('foreign_nationalities') ?></a>
                </div>
                
			</div>
            
       </div>
                
    </div>
</div>

<? $this->load->view('uo/contract_phases/documents') ?>


<div class="widget TAC">
    <input type="submit" class="btn" name="smt_send" value="<?php echo lang('save') ?>" style="margin:10px;padding: 5px;height: auto">
</div>


<?php echo  form_close() ?>


<div class="footer"></div>


<script type="text/javascript">
$(document).ready(function() {
    var config = {
        '.chosen-select': {width: "100%"},
        '.chosen-select-deselect': {allow_single_deselect: true},
        '.chosen-select-no-single': {disable_search_threshold: 10},
        '.chosen-select-no-results': {no_results_text: 'Oops, nothing found!'},
        '.chosen-select-width': {width: "95%"}
    }
    for (var selector in config) {
        $(selector).chosen(config[selector]);
    }


    // binds form submission and fields to the validation engine
    $("#frm_contract_phases").validationEngine({
            prettySelect : true,
            useSuffix: "_chosen",
            promptPosition : "topRight:-150"
            //promptPosition : "bottomLeft"
    });


 
});


</script>


<script type="text/javascript">
$(document).ready(function() {
   
    $("#all_nationalities_lnk").click(function(){
    	$("#nationalities").val([<?php foreach($nationalities_rows as $nationalities_row) { echo("'$nationalities_row->erp_nationality_id',"); }?>]);
    	$("#nationalities").trigger("chosen:updated");
	});
    $("#arabic_nationalities_lnk").click(function(){
    	$("#nationalities").val([<?php foreach($nationalities_rows as $nationalities_row) { if($nationalities_row->arabian==1){echo("'$nationalities_row->erp_nationality_id',");} }?>]);
    	$("#nationalities").trigger("chosen:updated");
	});
    $("#foreign_nationalities_lnk").click(function(){
    	$("#nationalities").val([<?php foreach($nationalities_rows as $nationalities_row) { if($nationalities_row->arabian==0){echo("'$nationalities_row->erp_nationality_id',");} }?>]);
    	$("#nationalities").trigger("chosen:updated");
	});
 
});


</script>