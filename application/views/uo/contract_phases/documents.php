<div class="widget" >
    <script>documents_counter = 0</script>
    <div class="widget-header">
        <div class="widget-header-icon Fright"> <span class="icos-pencil2"></span> </div>
        <div class="widget-header-title Fright"> <?php echo  lang('phase_documents') ?></div>
        
    </div>

    <div class="widget-container">
        <div class="table-responsive" >
            <table cellpadding="0" cellspacing="0">
                <thead>
                    <tr>
                        <th><?php echo  lang('document_name_ar') ?></th>
                        <th><?php echo  lang('document_name_la') ?></th>
                        <th><?php echo  lang('nationalities') ?></th>
                        <th><?php echo  lang('actions') ?></th>
                        <th ><a class="btn" href="javascript:void(0)" onclick="add_documents('N' + documents_counter++); load_multiselect()">
            <?php echo  lang('add') ?>
        </a></th>
                    </tr>
                </thead>
                <tbody class="documents" id='documents'>
                <? if(isset($item_documents)) { ?>
                    <? if(check_array($item_documents)) { ?>
                    <? foreach($item_documents as $item_document) { ?>
                    <tr rel="<?php echo  $item_document->safa_contract_phases_documents_id ?>">
                        
                        <td><?php echo  form_input('documents_name_ar['.$item_document->safa_contract_phases_documents_id.']'
                                , set_value('documents_name_ar['.$item_document->safa_contract_phases_documents_id.']', $item_document->name_ar)
                                , 'class="input-huge" style="width:250px"') ?>
                        </td>
                        <td>
                            <?php echo  form_input('documents_name_la['.$item_document->safa_contract_phases_documents_id.']'
                                    , set_value('documents_name_la['.$item_document->safa_contract_phases_documents_id.']', $item_document->name_la)
                                    , 'class="input-huge" style="width:250px"') ?>
                        </td>
                        <td>
                            <?php 
                            $item_documents_nationalities_selected=array();
                            if(isset($item_documents_nationalities[$item_document->safa_contract_phases_documents_id])) {
                            	$item_documents_nationalities_selected=$item_documents_nationalities[$item_document->safa_contract_phases_documents_id];
                            }
                            
                            echo  form_multiselect('documents_nationalities['.$item_document->safa_contract_phases_documents_id.'][]'
                                    , $nationalities, set_value('documents_nationalities['.$item_document->safa_contract_phases_documents_id.']', $item_documents_nationalities_selected)
                                    , 'class="chosen-select chosen-rtl input-full" id="documents_nationalities_'.$item_document->safa_contract_phases_documents_id.'" multiple tabindex="4" ') ?>
                        </td>
                        <td>
                            <style>
                                .cls_foreign_nationalities_lnk,.cls_arabic_nationalities_lnk,.cls_all_nationalities_lnk{margin: 0 7px;}
                            </style>
                        
                        
		                    <a class="cls_all_nationalities_lnk" href="javascript:void()" id="<?php echo $item_document->safa_contract_phases_documents_id;?>" ><?php echo  lang('all_nationalities') ?></a>
		               
		               
		                    <a class="cls_arabic_nationalities_lnk" href="javascript:void()" id="<?php echo $item_document->safa_contract_phases_documents_id;?>" ><?php echo  lang('arabic_nationalities') ?></a>
		               
		              
		                    <a class="cls_foreign_nationalities_lnk" href="javascript:void()" id="<?php echo $item_document->safa_contract_phases_documents_id;?>" ><?php echo  lang('foreign_nationalities') ?></a>
		             
                        
                        </td>
                        
                        <td class="TAC">
                            <a href="javascript:void(0)" onclick="delete_documents(<?php echo  $item_document->safa_contract_phases_documents_id ?>, true)"><span class="icon-trash"></span></a>
                        </td>
                    </tr>
                    <? } ?>
                    <? } ?>
                    <? } ?>
                </tbody>
            </table>
        </div>

    </div>
</div>

<script>
function load_multiselect() {
	var config = {
            '.chosen-select': {width: "100%"},
            '.chosen-select-deselect': {allow_single_deselect: true},
            '.chosen-select-no-single': {disable_search_threshold: 10},
            '.chosen-select-no-results': {no_results_text: 'Oops, nothing found!'},
            '.chosen-select-width': {width: "95%"}
        }
        for (var selector in config) {
            $(selector).chosen(config[selector]);
        }
}
function add_documents(id) {

	
	
    var new_row = [
            "<tr rel=\"" + id + "\">",
            '     <td><?php echo  form_input("documents_name_ar[' + id + ']", FALSE, 'class="validate[required] input-huge" style=\"width:250px\"') ?>',
            "    </td>",
            "    <td>",
            '        <?php echo  form_input("documents_name_la[' + id + ']", FALSE, "class=\"validate[required] input-huge\" style=\"width:250px\" ") ?>',
            "    </td>",
            "    <td>",
            "       <select name=\"documents_nationalities[" + id + "][]\" class=\"chosen-select chosen-rtl input-full\" id=\"documents_nationalities_" + id + "\" multiple tabindex=\"4\">",
            <? foreach($nationalities as $key => $value): ?>
            "           <option value=\"<?= $key ?>\"><?= $value ?></option>",
            <? endforeach ?>
            "    </td>",
            "    <td>",

            "<a class=\"cls_all_nationalities_lnk\" href=\"javascript:void()\" id=\""+id+"\" ><?php echo  lang('all_nationalities') ?></a>",


	        "    <a class=\"cls_arabic_nationalities_lnk\" href=\"javascript:void()\" id=\""+id+"\" ><?php echo  lang('arabic_nationalities') ?></a>",

	        "    <a class=\"cls_foreign_nationalities_lnk\" href=\"javascript:void()\"  id=\""+id+"\" ><?php echo  lang('foreign_nationalities') ?></a>",


			"<script type=\"text/javascript\">load_nationalities_lnks_event();<\/script>",
	        
	        
            "    </td>",
            "    <td class=\"TAC\">",
            "        <a href=\"javascript:void(0)\" onclick=\"delete_documents('" + id + "')\"><span class=\"icon-trash\"></span></a>",
            "    </td>",
            "</tr>"
            ].join("\n");

    
        
    $('#documents').append(new_row);
    
}
function delete_documents(id, database) 
{
    if(typeof database == 'undefined')
    {
        $('.documents').find('tr[rel="' + id + '"]').remove();
    }
    else
    {
        $('.documents').find('tr[rel=' + id + ']').remove();
        var hidden_input = '<input type="hidden" name="documents_remove[]" value="' + id + '" />';
        $('#documents').append(hidden_input);
    }
}

</script>

<script type="text/javascript">
function load_nationalities_lnks_event()
{
    $(".cls_all_nationalities_lnk").click(function(){
    	$("#documents_nationalities_"+$(this).attr('id')).val([<?php foreach($nationalities_rows as $nationalities_row) { echo("'$nationalities_row->erp_nationality_id',"); }?>]);
    	$("#documents_nationalities_"+$(this).attr('id')).trigger("chosen:updated");
	});
    $(".cls_arabic_nationalities_lnk").click(function(){
    	$("#documents_nationalities_"+$(this).attr('id')).val([<?php foreach($nationalities_rows as $nationalities_row) { if($nationalities_row->arabian==1){echo("'$nationalities_row->erp_nationality_id',");} }?>]);
    	$("#documents_nationalities_"+$(this).attr('id')).trigger("chosen:updated");
	});
    $(".cls_foreign_nationalities_lnk").click(function(){
    	$("#documents_nationalities_"+$(this).attr('id')).val([<?php foreach($nationalities_rows as $nationalities_row) { if($nationalities_row->arabian==0){echo("'$nationalities_row->erp_nationality_id',");} }?>]);
    	$("#documents_nationalities_"+$(this).attr('id')).trigger("chosen:updated");
	});
 
}
</script>

<script type="text/javascript">
$(document).ready(function() {
	
    $(".cls_all_nationalities_lnk").click(function(){
    	$("#documents_nationalities_"+$(this).attr('id')).val([<?php foreach($nationalities_rows as $nationalities_row) { echo("'$nationalities_row->erp_nationality_id',"); }?>]);
    	$("#documents_nationalities_"+$(this).attr('id')).trigger("chosen:updated");
	});
    $(".cls_arabic_nationalities_lnk").click(function(){
    	$("#documents_nationalities_"+$(this).attr('id')).val([<?php foreach($nationalities_rows as $nationalities_row) { if($nationalities_row->arabian==1){echo("'$nationalities_row->erp_nationality_id',");} }?>]);
    	$("#documents_nationalities_"+$(this).attr('id')).trigger("chosen:updated");
	});
    $(".cls_foreign_nationalities_lnk").click(function(){
    	$("#documents_nationalities_"+$(this).attr('id')).val([<?php foreach($nationalities_rows as $nationalities_row) { if($nationalities_row->arabian==0){echo("'$nationalities_row->erp_nationality_id',");} }?>]);
    	$("#documents_nationalities_"+$(this).attr('id')).trigger("chosen:updated");
	});
 
});
</script>