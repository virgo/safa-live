<div class="widget">
    <a class="btn Fleft" href="<?php echo  site_url('uo/contract_phases/add') ?>">
      <?php echo  lang('global_add') ?>
    </a>
    <div class="path-container Fright">
    	<div class="path-name Fright">
            <a href="<?= site_url('uo/dashboard') ?>"><?= lang('global_system_management') ?></a>
        </div>
        <div class="path-arrow Fright"></div>
        <div class="path-name Fright"> <?php echo  lang('menu_title') ?></div>
    </div>
</div>

<div class="widget">
    <div class="widget-header">
        <div class="widget-header-icon Fright"> <span class="icos-pencil2"></span> </div>
        <div class="widget-header-title Fright">
            <?php echo  lang('title') ?>
        </div>
        
    </div>
     <div class="widget-container">
          <div class="table-responsive">
            <table cellpadding="0" cellspacing="0" class="fsTable" >
                <thead>
                    <tr>
                    	<th><?php echo  lang('id') ?></th>
                        <th><?php echo  lang('name_ar') ?></th>
                        <th><?php echo  lang('name_la') ?></th>
                        <th><?php echo  lang('actions') ?></th>
                    </tr>
                </thead>
                <tbody id="had">
                <? if(ensure($items)) { ?>
                    <? foreach($items as $item) {?>
                    <tr rel="<?php echo  $item->safa_contract_phases_id ?>">
						<td>
                            <?php echo   $item->safa_contract_phases_id ; ?>
                        </td>
                        <td>
                            <?php echo   $item->name_ar ; ?>
                        </td>
                        <td>
                            <?php echo   $item->name_la ; ?>
                        </td>
                        
                        <td class="TAC">
                        <a href="<?= site_url('uo/contract_phases/edit/' . $item->safa_contract_phases_id) ?>"><span class="icon-pencil"></span></a>
                        <a href="<?= site_url('uo/contract_phases/delete/' . $item->safa_contract_phases_id) ?>" onclick="return confirm('<?= lang('global_are_you_sure_you_want_to_delete')?>')"><span class="icon-trash"></span></a>
                        </td>
                    </tr>
                    <? } ?>
                <? } ?> 
                </tbody>
            </table>
        </div>
    </div>
</div>

