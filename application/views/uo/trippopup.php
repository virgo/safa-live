<link type="text/css" rel="stylesheet" href="http://localhost/safa-live/static/new_template/css-js_rtl/common-css.css">

<style>
    h1 , h2 , p {padding:0px ; margin:0px; -webkit-margin:0px; -moz-margin:0px;}

    .popupborder {
        min-height: 330px;
        background: rgba(209,170,103,0.2);
        border: 2px solid #d1aa67;
        border-radius: 10px;
        width: 40%;
    }
    .popuph h1{
        color: #d1aa67;
        font-size: 30px;
        padding: 10px 20px;
    }
    .popuph h2{
        color: #888;
        font-size: 15px;
        padding: 0px 5px 5px 20px;
        display: block;
        float: left;
    }
    .countryfrmto{font-size: 60px;
                  display: block;
                  padding: 0px 5px 5px 20px;
                  color: #d1aa67;
                  text-align: center;
    }
    .down2 p{
        color: #888;
        font-size: 15px;
        padding: 0px ;
        display: block;
    }
    .green{color:green !important;}
    .clear{clear:both;}
    .imgofpopup img{
        width: 270px;
        margin-top: 14px;
    }
    table td {
        border: 0px; 
        border-bottom: 0px;
        border-right: 0px;
        border-left: 0px;
        border-top: 0px;
    }
    .fntsiz10 {font-size: 11px;}
</style>
<body>
    <!--the whole popup-->
    <div class=" popupborder">

        <!--HEAD-->
        <div class="span12 popuph">
            <h1>EgyptAir Flight 936</h1>
            <h2 class="green">On time </h2><h2>- arrives in 18 mins </h2>
        </div>
        <!--HEAD END-->

        <div class="clear"></div>


        <!--Middle-->
        <div class="span12 popupmiddle">
            <div class="span3">
                <h1 class="countryfrmto">CAI</h1>
            </div>
            <div class="span6 imgofpopup">
                <img id="thepic" src="<?= base_url('static/images') ?>/plan/semi-arrived.png" alt="status">

            </div>
            <div class="span3">
                <h1 class="countryfrmto">TIP</h1>
            </div>
        </div>
        <!--END MIDDLE-->
        <!--START END-->
        <div class="span12">
            <div class="span3 down2">
                <p>Departed Madinah,</p>
                <p>Tuesday, March 11</p>
                <div class="khatt span12"></div>
                <table class="popuptable fntsiz10">
                    <tr>
                        <td>Times</td>
                        <td>Terminal</td>
                        <td>Gate</td>
                    </tr>
                    <tr>
                        <td>2:30 PM</td>
                        <td>-</td>
                        <td>-</td>
                    </tr>
                </table>


            </div>
            <div class="span5" style="text-indent:-999px;">
                sdaf
            </div>
            <div class="span3 down2">
                <p>Departed Madinah,</p>
                <p>Tuesday, March 11</p>
                <div class="khatt span12 "></div>
                <table class="fntsiz10">
                    <tr>
                        <td>Times</td>
                        <td>Terminal</td>
                        <td>Gate</td>
                    </tr>
                    <tr>
                        <td>2:30 PM</td>
                        <td>-</td>
                        <td>-</td>
                    </tr>
                </table>


            </div>
        </div>

        <!--END END-->
    </div>
    <!--End whole div-->
</body>

<script>
    var status = 1;

    if (status == 1) {

        _status = "<?= base_url('static/images') ?>/plan/going-arrived.png";
    } else if (status == 2) {
        _status = "<?= base_url('static/images') ?>/plan/onway-arrived.png";

    } else if (status == 3) {
        _status = "<?= base_url('static/images') ?>/plan/half-arrived.png";

    } else if (status == 4) {

        _status = "<?= base_url('static/images') ?>/plan/semi-arrived.png";
    } else if (status == 5) {
        _status = "<?= base_url('static/images') ?>/plan/arrived-arrived.png";

    } else {
        _status = "<?= base_url('static/images') ?>/plan/late-arrived.png";
    }

    console.log(document.getElementById("thepic").src = _status);


</script>
