
<div class=" row-fluid" >
    <div class="span12" >
        <div style="margin-bottom:10px; text-align:left; border:3px solid #FFF; background:url(<?= IMAGES ?>/backgrounds/bg_box_head.jpg); padding:5px 10px; height:30px;"><a href="<?= site_url("uo/uo_users/add") ?>" class="btn btn-primary"><?= lang('global_add_new_record') ?></a>
            <div style="float:right; text-align:right; font-size:14px; line-height:30px; color:#333;"> 
                <a href="<?= site_url('uo/dashboard') ?>"><?= lang('global_system_management') ?></a> 
                <span style="color:#80693d"><img src="<?= IMAGES ?>/jbreadcrumb/Chevron-ar.gif" width="5" height="20"></span>
                <?= lang('uo_users') ?>
            </div>
        </div>
    </div>
</div>


<div class='row-fluid'>
    <div class='span12'>
        <div class='widget'>
            <div class='head dark'>
                <div class="icon"><span class="icos-cube1"></div>
                <h2><?= lang('uo_users') ?></h2>
            </div>
            <div class='block-fluid' >
                <table class="myTable" width="100%" >
                    <thead>
                        <tr>
                            <th>
                               <?if(name()=='name_ar'):?>
                                    <?= lang('uo_users_name_ar') ?>
                                <?else:?>
                                    <?= lang('uo_users_name_la') ?>
                                <?endif;?>
                            </th>
                            <th><?= lang('uo_users_username') ?></th>
                            <th><?= lang('global_actions')?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <? if (isset($items)): ?>
                            <? foreach ($items as $item): ?>
                                <tr>
                                    <td><? if (name() == 'name_ar'): ?><?= $item->name_ar ?><? else: ?><?= $item->name_la ?><? endif; ?></td>
                                    <td><?= $item->username?></td>
                                    <td class="TAC">
                                        <a href="<?= site_url("uo/uo_users/edit") ?>/<?= $item->safa_uo_user_id ?>"><span class="icon-pencil"></span></a>
                                    </td>
                                </tr>
                            <? endforeach; ?>
                        <? endif; ?>
                    </tbody>
                </table>
            </div>
        </div>
        <?= $pagination ?>
    </div>
</div>

<script>
                                            $(document).ready(function() {
                                                $(".myTable").dataTable(
                                                        {bSort: true,
                                                            bAutoWidth: true,
                                                            "iDisplayLength": false, // can be removed for basic 10 items per page
                                                            "sPaginationType": false,
                                                            "bPaginate": false,
                                                            "bInfo": false}
                                                );
                                            });
</script>
