<div class="row-fluid" >
    <div class="span12" >
        <div style="margin-bottom:10px; text-align:left; border:3px solid #FFF; background:url(<?= IMAGES ?>/backgrounds/bg_box_head.jpg); padding:5px 10px; height:30px;">
            <div style="float:right; text-align:right; font-size:14px; line-height:30px; color:#333;"> 
                <a href="<?= site_url('admin/dashboard') ?>"><?= lang('global_system_management') ?></a> 
                <span style="color:#80693d"><img src="<?= IMAGES ?>/jbreadcrumb/Chevron-ar.gif" width="5" height="20"></span>
                <a href="<?= site_url('uo/uo_users') ?>"><?= lang('uo_users') ?></a>
                <span style="color:#80693d"><img src="<?= IMAGES ?>/jbreadcrumb/Chevron-ar.gif" width="5" height="20"></span>
                <?= lang('edit_uo_users') ?>
            </div>
        </div>
    </div>
</div>
<div class="row-fluid">
    <div class="span12" >
        <div class="widget">
            <div class="head dark">
                <div class="icon"><i class="icos-pencil2"></i></div>
                <h2><?= lang('edit_uo_users') ?></h2> 
            </div>                        
            <div class="block-fluid">
                <?= form_open("","autocomplete='off'") ?>

                <div class="row-form">
                    <div class="span6">
                        <div class="span4"><?= lang('uo_users_name_ar') ?><? if (name() == 'name_ar'): ?><font style="color:red" >*</font><? endif; ?></div>
                        <div class="span8" >
                            <?= form_input('name_ar', set_value("name_ar",$item->name_ar)) ?>
                            <span class="bottom" style="color:red" >
                                <?= form_error('name_ar') ?>
                            </span>
                        </div>
                    </div>
                    <div class="span6">
                        <div class="span4"><?= lang('uo_users_name_la') ?><? if (name() == 'name_la'): ?><font style="color:red" >*</font><? endif; ?></div>
                        <div class="span8" >
                            <?= form_input('name_la', set_value("name_la",$item->name_la)) ?>
                            <span class="bottom" style="color:red" ><?= form_error('name_la') ?></span> 
                        </div>    
                    </div>
                </div>

                <div class="row-form">
                    <div class="span6">
                        <div class="span4"><?= lang('uo_users_username') ?><font style="color:red" >*</font></div>
                        <div class="span8" >
                            <?= form_input('username', set_value('username',$item->username)) ?>
                            <span class="bottom" style="color:red" ><?= form_error('username') ?></span> 
                        </div>
                    </div>
                    <div class="span6" >
                       <div class="span4"><?= lang('uo_users_safa_uo_usergroup_id') ?><font style="color:red" >*</font></div>
                       <div class="span8">
                                <?=form_dropdown('safa_uo_usergroup_id',ddgen('safa_uo_usergroups',array('safa_uo_usergroup_id',name())),set_value('safa_uo_usergroup_id',$item->safa_uo_usergroup_id))?>
                             <span class="bottom" style="color:red">
                                <?= form_error('safa_uo_usergroup_id') ?>
                            </span>
                       </div>
                    </div>
                 </div>
                <div class="row-form" >
                    <div class="span6" >
                      <div class="span4"><?= lang('uo_users_email') ?></div>
                        <div class="span8" >
                           <?= form_input('email', set_value('email',$item->email)) ?>
                            <span class="bottom" style="color:red">
                                <?= form_error('email') ?>
                            </span>
                             
                        </div>
                    </div>
                    <div class="span6" >
                                <div class="span4"><?= lang('uo_users_mobile') ?></div>
                                <div class="span8" >
                                    <?= form_input('mobile',set_value('mobile',$item->mobile)) ?>
                                    <span class="bottom" style="color:red">
                                        <?= form_error('mobile') ?>
                                    </span>

                                </div>
                            </div>
                </div>
                <div class="row-form">
                    <div class="span6">
                        <div class="span4"><?= lang('uo_users_password') ?><font style="color:red" >*</font></div>
                        <div class="span8" >
                            <?= form_password('password', set_value('password')) ?>
                            <span class="bottom" style="color:red">
                                <?= form_error('password') ?>
                            </span>
                        </div>
                    </div>
                    <div class="span6">
                        <div class="span4"><?= lang('uo_users_confirm_password') ?><font style="color:red" >*</font></div>
                        <div class="span8">
                            <?= form_password('passconf', set_value('passconf')) ?>
                            <span class="bottom" style="color:red" >
                                <?= form_error('passconf') ?>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="toolbar bottom TAC">
                    <button class="btn btn-primary"><?= lang('global_submit') ?></button>
                    <a  class="btn btn-primary" href="<?= site_url('uo/uo_users') ?>" ><?= lang('global_back') ?></a>
                </div>

                <?= form_close() ?> 
            </div>
        </div>  
    </div>
</div>  

