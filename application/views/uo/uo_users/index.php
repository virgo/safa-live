<div class="span12">
    <div class="widget">
       

        <div class="path-container Fright">
            <div class="path-name Fright">
                <a href=""><?= lang('global_system_management') ?></a>
            </div>
            <div class="path-arrow Fright">
            </div>
            <div class="path-name Fright">
                <?= lang('uo_users') ?>
            </div>
        </div>

    </div>
</div>

<div class="row-fluid">
    <div class="widget">
        <style>
            .updated_msg{
                display:none;
                background-color:#ccee97;
                font-weight: bold;
                text-align: center;
                border:3px solid #cccdc9; 
                padding:20px;  
                margin-bottom:10px;
                border-radius:15px;
                margin:10px; 
            }
        </style>

        <div class='row-fluid' align='center' >
            <div  class='updated_msg' >
                <br><input  type ="button" value="<?= lang('global_submit') ?>"class="btn btn-primary" id='ok' >
            </div> 
        </div>


        <div class="widget-header">

            <div class="widget-header-icon Fright">
                <span class="icos-pencil2"></span>
            </div>

            <div class="widget-header-title Fright">
                <?= lang('menu_external_agent_Users') ?>
            </div>
 <a class="btn Fleft" href="<?= site_url('uo/uo_users/add') ?>"><?= lang('global_add_new_record') ?></a>
        </div>
        <div class="widget-container">
            <div class='table-responsive' >
                <table cellpadding="0" class="fsTable" cellspacing="0">
                    <thead>
                        <tr>
                            <th>
                               <?if(name()=='name_ar'):?>
                                    <?= lang('uo_users_name_ar') ?>
                                <?else:?>
                                    <?= lang('uo_users_name_la') ?>
                                <?endif;?>
                            </th>
                            <th><?= lang('uo_users_username') ?></th>
                            <th><?= lang('global_actions')?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <? if (isset($items)): ?>
                            <? foreach ($items as $item): ?>
                                <tr>
                                    <td><? if (name() == 'name_ar'): ?><?= $item->name_ar ?><? else: ?><?= $item->name_la ?><? endif; ?></td>
                                    <td><?= $item->username?></td>
                                    <td class="TAC">
                                        <a href="<?= site_url("uo/uo_users/edit") ?>/<?= $item->safa_uo_user_id ?>"><span class="icon-pencil"></span></a>
                                    </td>
                                </tr>
                            <? endforeach; ?>
                        <? endif; ?>
                    </tbody>
                </table>
            </div>
    </div>
    </div>
</div>
<script type='text/javascript' src='<?= JS ?>/custom/table_operation.js'></script>
<script>
                                            $(document).ready(function() {
                                                $(".myTable").dataTable(
                                                        {bSort: true,
                                                            bAutoWidth: true,
                                                            "iDisplayLength": false, // can be removed for basic 10 items per page
                                                            "sPaginationType": false,
                                                            "bPaginate": false,
                                                            "bInfo": false}
                                                );
                                            });
</script>
