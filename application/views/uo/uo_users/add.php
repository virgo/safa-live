
<style>input {margin-bottom : 4px !important;}</style>
<div class="span12">
    <div class="widget">
        <div class="path-container Fright">
            <div class="path-name Fright">
                <a href=""><?= lang('global_system_management') ?></a>
            </div>
            <div class="path-arrow Fright">
            </div>
            <div class="path-name Fright">
                <a href="<?= site_url('uo/uo_users/index') ?>"><?= lang('menu_external_agent_Users') ?></a>
            </div>
            <div class='path-arrow Fright'>
            </div>
            <div class="path-name Fright">
                <?= lang('add_uo_users') ?>
            </div>
        </div>
    </div>
</div>


<div class="widget">
    <div class="widget-header">

        <div class="widget-header-icon Fright">
            <span class="icos-pencil2"></span>
        </div>

        <div class="widget-header-title Fright">
            <?= lang('add_uo_users') ?> 
        </div>

    </div>  

    <div class="widget-container">                               
        <div class="block-fluid">
            <?= form_open("", "autocomplete='off' id='users_uo'" ) ?>

            <div class="row-form">
                <div class="span6">
                    <div class="span4 TAL Pleft10"><?= lang('uo_users_name_ar') ?><? if (name() == 'name_ar'): ?><font style="color:red" >*</font><? endif; ?></div>
                    <div class="span8" >
                        <?= form_input('name_ar', set_value("name_ar"),"class='input-huge validate[required]' ") ?>
                        <?= form_error('name_ar', '<div class="bottom" style="color:red" >', '</div>'); ?>
                    </div>
                </div>
                <div class="span6">
                    <div class="span4 TAL Pleft10"><?= lang('uo_users_name_la') ?><? if (name() == 'name_la'): ?><font style="color:red" >*</font><? endif; ?></div>
                    <div class="span8" >
                        <?= form_input('name_la', set_value("name_la"),"class='input-huge ' ") ?>
                        <?= form_error('name_la', '<div class="bottom" style="color:red" >', '</div>'); ?>
                    </div>    
                </div>
            </div>

            <div class="row-form">
                <div class="span6">
                    <div class="span4 TAL Pleft10"><?= lang('uo_users_username') ?> ( <?= lang('uo_users_email') ?> )<font style="color:red" >*</font></div>
                    <div class="span8" >
                        <?= form_input('email', set_value('email'),"class='input-huge validate[required]' ") ?>
                        <?= form_error('email', '<div class="bottom" style="color:red" >', '</div>'); ?>
                    </div>
                </div>
                <div class="span6" >
                    <div class="span4 TAL Pleft10"><?= lang('uo_users_safa_uo_usergroup_id') ?><font style="color:red" >*</font></div>
                    <div class="span8">
                        <?= form_dropdown('safa_uo_usergroup_id', ddgen('safa_uo_usergroups', array('safa_uo_usergroup_id', name()), array('safa_uo_id'=>session('uo_id')))+ddgen('safa_uo_usergroups', array('safa_uo_usergroup_id', name()), array('safa_uo_usergroup_id'=>3),FALSE,TRUE), set_value('safa_uo_usergroup_id'),"class=' select input-huge' style='width:90%'") ?>
                        <?= form_error('safa_uo_usergroup_id', '<div class="bottom" style="color:red" >', '</div>'); ?>
                    </div>
                </div>
            </div>
            <div class="row-form">
                <div class="span6">
                    <div class="span4 TAL Pleft10"><?= lang('uo_users_password') ?><font style="color:red" >*</font></div>
                    <div class="span8" >
                        <?= form_password('password', set_value('password'),"class='input-huge validate[required]' ") ?>
                        <?= form_error('password', '<div class="bottom" style="color:red" >', '</div>'); ?>
                    </div>
                </div>
                <div class="span6">
                    <div class="span4 TAL Pleft10"><?= lang('uo_users_confirm_password') ?><font style="color:red" >*</font></div>
                    <div class="span8">
                        <?= form_password('passconf', set_value('passconf'),"class='input-huge validate[required]' ") ?>
                        <?= form_error('passconf', '<div class="bottom" style="color:red" >', '</div>'); ?>
                    </div>
                </div>
            </div>
            
            
            <div class="row-form">
                <div class="span6">
                    <div class="span4 TAL Pleft10"><?= lang('safa_uo_user_type_id') ?></div>
                    <div class="span8" >
                        <?= form_dropdown("safa_uo_user_type_id", $safa_uo_user_types, set_value("safa_uo_user_type_id"), "  id='safa_uo_user_type_id' class='select'  style='width:90%'") ?>
                            <span class="bottom" style='color:red' >
                                <?= form_error('safa_uo_user_type_id') ?> 
                            </span>
                    </div>
                </div>
                <div class="span6" >
                    <div class="span4 TAL Pleft10"><?= lang('uo_users_mobile') ?></div>
                    <div class="span8" >
                        <?= form_input('mobile', set_value('mobile'),"class=input-huge") ?>
                        <?= form_error('mobile', '<div class="bottom" style="color:red" >', '</div>'); ?>
                    </div>
                </div>
            </div>
            
            <div class="row-form" id="dv_user_info" style="display:none;">
                <div class="span6" id="dv_user_info_city">
                    <div class="span4 TAL Pleft10"><?= lang('erp_city_id') ?></div>
                    <div class="span8" >
                        <?= form_dropdown("erp_city_id", $erp_cities, set_value("erp_city_id"), "  id='erp_city_id' class='select'  style='width:90%' ") ?>
                            <span class="bottom" style='color:red' >
                                <?= form_error('erp_city_id') ?> 
                            </span>
                    </div>
                </div>
                
                <div class="span6" id="dv_user_info_port_type">
                    <div class="span4 TAL Pleft10"><?= lang('port_type') ?></div>
                    <div class="span8" >
                        <?= form_dropdown("port_type", $port_types, set_value("port_type"), "  id='port_type' class='select'  style='width:90%'") ?>
                            <span class="bottom" style='color:red' >
                                <?= form_error('port_type') ?> 
                            </span>
                    </div>
                </div>
                
            </div>
            
            
            <div class="toolbar bottom TAC">
                <input type="submit" name="submit" value="<?= lang('global_submit') ?>" class="btn btn-primary"> &nbsp;
                <input  type ="button" value="<?= lang('global_back') ?>"class="btn btn-primary" onclick="window.location = '<?= site_url('uo/uo_users/index') ?>'">
            </div>

            <?= form_close() ?> 
        </div>
    </div>
</div>

<script>
        $("#users_uo").validationEngine({
        prettySelect: true,
        useSuffix: "_chosen",
        promptPosition: "topRight:-150"
//promptPosition : "bottomLeft"
    });
    
</script>

<script>

$("#safa_uo_user_type_id").change(function () {
	if($(this).val()==2 || $(this).val()==3) {
		$("#dv_user_info").show();
		$("#dv_user_info_city").show();
		$("#dv_user_info_port_type").show();
	} else if($(this).val()==1) {
		$("#dv_user_info").show();
		$("#dv_user_info_city").show();
		$("#dv_user_info_port_type").hide();
	} else {
		$("#dv_user_info").hide();
	}
});

</script>
    

