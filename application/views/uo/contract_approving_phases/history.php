<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); ?>
<link href="<?= CSS ?>/stylesheets.css" rel="stylesheet" type="text/css" />
<? if (lang('global_lang') == 'ar'): ?>
    <link href="<?= CSS ?>/stylesheet-ar.css" rel="stylesheet" type="text/css" />
<? else: ?><? endif ?>
<script type="text/javascript" src='<?= JS ?>/plugins/jquery/jquery-1.9.1.min.js'></script>
<script type="text/javascript" src='<?= JS ?>/plugins/jquery/jquery-ui-1.10.1.custom.min.js'></script>
<script type="text/javascript" src='<?= JS ?>/plugins/bootstrap/bootstrap.min.js'></script>
<link href="<?= CSS_JS ?>/dateandtime/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src='<?= CSS_JS ?>/dateandtime/bootstrap-datetimepicker.min.js'></script>
<style>
body{
height: auto!important;
overflow: auto !important;
background:none transparent !important;
overflow: hidden;
min-height: 1%;
}

.row-fluid{
padding-top: 5px !important;
padding-bottom: 5px !important;
}
</style>




<?php echo  form_open_multipart(false, 'id="frm_contract_approving_phases" ') ?>


<div class="">
    

     <div class="modal-header">  
       
        </div>
        
    <div class="modal-body">  
      
        <? if(validation_errors()){ ?>
            <?php echo validation_errors(); ?>        
        <? } ?>
        <!-- 
        <div class="row-fluid">
            <div class="span6">
                <div class="span4 TAL Pleft10">
                    <?php echo  lang('contract_code') ?>
                </div>
                <div class="span8">
                    <?php echo form_input('contract_code', set_value("contract_code", '')," style='' id='contract_code' class='validate[required] input-full' ") ?>
                </div>
			</div>
        </div>
         -->
        <div class="row-fluid">
            <div class="span6">
                <div class="span4 TAL Pleft10">
                <?php echo  form_input(array('name' => 'safa_uo_contracts_id', 'type'=>'hidden','value'=>$item->safa_uo_contract_id, 'id' =>'safa_uo_contracts_id'));
                ?>
                    <?php echo  lang('contract_name') ?>
                </div>
                <div class="span8">
                    <?php echo form_input('contract_name', set_value("contract_name", $item->{name()})," style='' id='contract_name' class=' input-full' readonly='readonly' ") ?>
                </div>
			</div>
        </div>
        
        <div class="row-fluid">
            <div class="span6">
                <div class="span4 TAL Pleft10">
                    <?php echo  lang('current_phase') ?>
                </div>
                <div class="span8">
                <?php 
                if(name()=='name_ar') {
                	$safa_contract_phases_name_current=$item->safa_contract_phases_name_ar_current;
                } else {
                	$safa_contract_phases_name_current=$item->safa_contract_phases_name_la_current;
                }
                ?>
                    <?php echo form_input('current_phase', set_value("current_phase", $safa_contract_phases_name_current)," style='' id='current_phase' class=' input-full'  readonly='readonly' ") ?>
                </div>
			</div>
        </div>
        
        
        <div class="row-fluid">
            <div class="span6">
                <div class="span4 TAL Pleft10">
                    <?php echo  lang('contracts_status') ?>
                </div>
                <div class="span8">
                <?php 
                if(name()=='name_ar') {
                	$safa_uo_contracts_status_name=$item->safa_uo_contracts_status_name_ar;
                } else {
                	$safa_uo_contracts_status_name=$item->safa_uo_contracts_status_name_la;
                }
                ?>
                    <?php echo form_input('contracts_status', set_value("contracts_status", $safa_uo_contracts_status_name)," style='' id='contracts_status' class=' input-full'  readonly='readonly' ") ?>
                </div>
			</div>
        </div>
        
   		<div class="row-fluid">
            <div class="span6">
                <div class="span4 TAL Pleft10">
                    <?php echo  lang('previous_phases') ?>
                </div>
                <div class="span8">
                    
                   <?php echo  form_dropdown('previous_phases', $contract_phases, set_value('previous_phases', $item->safa_contract_phases_id_next), 'class="validate[required] input-full chosen-rtl " style="width:100%;" id="previous_phases"') ?>
                </div>
			</div>
        </div>
        
        
        <div class="row-fluid">
            <div class="span10">
                <div class="span4 TAL Pleft10">
                    <?php echo  lang('required_documents') ?>
                </div>
                <div class="span12">


<div class="">
        <div class="table-responsive"  id="dv_required_documents">
            <table cellpadding="0" cellspacing="0">
                <thead>
                    <tr>
                        <th><?php echo  lang('document_name') ?></th>
                        <th><?php echo  lang('upload_file') ?></th>
                        <th><?php echo  lang('status') ?></th>
                        <th><?php echo  lang('refuse_reason') ?></th>
                    </tr>
                </thead>
                <tbody id="had">
                <? if(ensure($required_documents)) { ?>
                    <? foreach($required_documents as $required_document) {
                    
                    	$safa_contract_approving_phases_id = $required_document->safa_contract_approving_phases_id;
                        $safa_contract_phases_documents_id = $required_document->safa_contract_phases_documents_id;
                    	$refuse_reason= $required_document->refuse_reason;
                    	$safa_contract_approving_phases_status_id= $required_document->safa_contract_approving_phases_status_id;
                    ?>
                    <tr rel="<?php echo  $safa_contract_approving_phases_id; ?>">

                        <td>
                        <?php echo  form_input(array('name' => "safa_contract_approving_phases_id_$safa_contract_approving_phases_id", 'type'=>'hidden','value'=>$safa_contract_approving_phases_id, 'id' =>"safa_contract_approving_phases_id_$safa_contract_approving_phases_id"));
                		?>
                            <?php echo   $required_document->{name()} ; ?>
                        </td>
                        <td>
                          <?php 
                          
                          if($required_document->document_path!='') {
                          	$document_path=$required_document->document_path;
	                          $document_file_name_arr=explode('/', $document_path);
	                          $document_file_name=end($document_file_name_arr);
	                          
	                          echo "<a target='_blank' href='".base_url().$document_path."'>$document_file_name</a>";
                          } else {
                          	 //echo form_upload("document_file_$safa_contract_phases_documents_id", "","  style='' id='document_file_$safa_contract_phases_documents_id'  ") ;
                          }
                          
                          ?> 
                        </td>
                        <td>
                            <?php echo  form_dropdown("safa_contract_approving_phases_status_id_$safa_contract_approving_phases_id", $safa_contract_approving_phases_status, set_value("safa_contract_approving_phases_status_id_$safa_contract_approving_phases_id", $safa_contract_approving_phases_status_id), 'class="validate[required] input-full chosen-rtl " style="width:100%;" id=safa_contract_approving_phases_status_id_"'.$safa_contract_approving_phases_id.'"') ?>
                        </td>
                        <td>
                            <?php echo form_input("refuse_reason_$safa_contract_approving_phases_id", set_value("refuse_reason_$safa_contract_approving_phases_id", $refuse_reason)," style='' id='refuse_reason_$safa_contract_approving_phases_id' class=' input-full'  ") ?>
                        </td>
                        
                    </tr>
                    <? } ?>
                <? } ?> 
                </tbody>
            </table>
    </div>
</div>
    
    
       
                </div>
                
			</div>
        </div>
        
                
    </div>
</div>



<div class="toolbar bottom TAL">
    <input type="submit" class="btn" name="smt_save" value="<?php echo lang('save') ?>" style="margin:10px;padding: 5px;height: auto">
</div>


<?php echo  form_close() ?>


<div class="footer"></div>



<script type="text/javascript">
$(document).ready(function() {
    var config = {
        '.chosen-select': {width: "100%"},
        '.chosen-select-deselect': {allow_single_deselect: true},
        '.chosen-select-no-single': {disable_search_threshold: 10},
        '.chosen-select-no-results': {no_results_text: 'Oops, nothing found!'},
        '.chosen-select-width': {width: "95%"}
    }
    for (var selector in config) {
        $(selector).chosen(config[selector]);
    }


    // binds form submission and fields to the validation engine
    $("#frm_contract_approving_phases").validationEngine({
            prettySelect : true,
            useSuffix: "_chosen",
            promptPosition : "topRight:-150"
            //promptPosition : "bottomLeft"
    });

 
});


</script>

<script type="text/javascript">
	
    $('#previous_phases').change(function()
     {
        var previous_phases_id=$('#previous_phases').val();
        var safa_uo_contracts_id=$('#safa_uo_contracts_id').val();
            
        var dataString = 'previous_phases_id='+ previous_phases_id+'&safa_uo_contracts_id='+ safa_uo_contracts_id;
            
            $.ajax
            ({
            type: 'POST',
            url: '<?php echo base_url().'uo/contract_approving_phases/getPhaseDocumentsTable'; ?>',
            data: dataString,
            cache: false,
            success: function(html)
            {
                //alert(html);
            $("#dv_required_documents").html(html);
            }
            });
            
     });
    
    
</script>