<script src="<?= NEW_CSS_JS ?>/jquery-1.9.1.js"></script>
<script src="<?= NEW_CSS_JS ?>/jquery-ui.js"></script>
<!-- accordion menu -->
<link rel="stylesheet" href="<?= NEW_CSS_JS ?>/accordionmenu.css" type="text/css" media="screen" />
 
<!-- multi selection choosen -->
<link rel="stylesheet" href="<?= NEW_CSS_JS ?>/multi-select-chosen/chosen.css">
<script src="<?= NEW_CSS_JS ?>/multi-select-chosen/chosen.jquery.js" type="text/javascript"></script>


        
<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); ?>
<link href="<?= CSS ?>/stylesheets.css" rel="stylesheet" type="text/css" />
<? if (lang('global_lang') == 'ar'): ?>
    <link href="<?= CSS ?>/stylesheet-ar.css" rel="stylesheet" type="text/css" />
<? else: ?><? endif ?>
<link href="<?= CSS_JS ?>/dateandtime/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css" />
<style>
body{
height: auto!important;
overflow: auto !important;
background:none transparent !important;
overflow: hidden;
min-height: 1%;
}

.row-fluid{
padding-top: 5px !important;
padding-bottom: 5px !important;
}

</style>




<?php echo  form_open_multipart(false, 'id="frm_contract_approving_phases" ') ?>


<div class="">
    

     
	<div class="modal-header">  
       
    </div>
    
    <br/>
        
    <div class="modal-body">   
        <? if(validation_errors()){ ?>
            <?php echo validation_errors(); ?>        
        <? } ?>
        <!-- 
        <div class="row-fluid">
            <div class="span6">
                <div class="span4 TAL Pleft10">
                    <?php echo  lang('contract_code') ?>
                </div>
                <div class="span8">
                    <?php echo form_input('contract_code', set_value("contract_code", '')," style='' id='contract_code' class='validate[required] input-full' ") ?>
                </div>
			</div>
        </div>
         -->
        <div class="row-fluid">
            <div class="span6">
                <div class="span4 TAL Pleft10">
                <?php echo  form_input(array('name' => 'safa_uo_contracts_id', 'type'=>'hidden','value'=>$item->safa_uo_contract_id, 'id' =>'safa_uo_contracts_id'));
                ?>
                    <?php echo  lang('contract_name') ?>
                </div>
                <div class="span8">
                    <?php echo form_input('contract_name', set_value("contract_name", $item->{name()})," style='' id='contract_name' class=' input-full' readonly='readonly' ") ?>
                </div>
			</div>
        </div>
        
        <div class="row-fluid">
            <div class="span6">
                <div class="span4 TAL Pleft10">
                    <?php echo  lang('current_phase') ?>
                </div>
                <div class="span8">
                <?php 
                if(name()=='name_ar') {
                	$safa_contract_phases_name_current=$item->safa_contract_phases_name_ar_current;
                } else {
                	$safa_contract_phases_name_current=$item->safa_contract_phases_name_la_current;
                }
                ?>
                    <?php echo form_input('current_phase', set_value("current_phase", $safa_contract_phases_name_current)," style='' id='current_phase' class=' input-full'  readonly='readonly' ") ?>
                </div>
			</div>
        </div>
        
        
        <div class="row-fluid">
            <div class="span6">
                <div class="span4 TAL Pleft10">
                    <?php echo  lang('contracts_status') ?>
                </div>
                <div class="span8">
                <?php 
                if(name()=='name_ar') {
                	$safa_uo_contracts_status_name=$item->safa_uo_contracts_status_name_ar;
                } else {
                	$safa_uo_contracts_status_name=$item->safa_uo_contracts_status_name_la;
                }
                ?>
                    <?php echo form_input('contracts_status', set_value("contracts_status", $safa_uo_contracts_status_name)," style='' id='contracts_status' class=' input-full'  readonly='readonly' ") ?>
                </div>
			</div>
        </div>
        
   		<div class="row-fluid">
            <div class="span6">
                <div class="span4 TAL Pleft10">
                    <?php echo  lang('next_phase') ?>
                </div>
                <div class="span8">
                    <?php echo  form_dropdown('next_phase', $contract_phases, set_value('safa_contract_phases_id', $item->safa_contract_phases_id_next), 'class="validate[required] input-full chosen-rtl " style="width:100%;" id="next_phase"') ?>
                </div>
			</div>
        </div>
        
        
        <div class="row-fluid">
            <div class="span6">
                <div class="span4 TAL Pleft10">
                    <?php echo  lang('required_documents') ?>
                </div>
                <div class="span8">
                    <?php echo  form_multiselect('required_documents[]', $required_documents, set_value('required_documents', $required_documents_selected), 'class="chosen-select chosen-rtl input-full" multiple tabindex="4" id="required_documents" data-placeholder=" "') ?>   
                </div>
                <div class="span4 TAL Pleft10">
                    <a id="remove_all_required_documents_lnk" href="javascript:void()" ><?php echo  lang('remove_all') ?></a>
                </div>
			</div>
        </div>
        
                
    </div>
</div>



<div class="toolbar bottom TAL">
    <input type="submit" class="btn" name="smt_save" value="<?php echo lang('save') ?>" style="margin:10px;padding: 5px;height: auto">
</div>


<?php echo  form_close() ?>


<div class="footer"></div>



<script type="text/javascript">
$(document).ready(function() {
    var config = {
        '.chosen-select': {width: "100%"},
        '.chosen-select-deselect': {allow_single_deselect: true},
        '.chosen-select-no-single': {disable_search_threshold: 10},
        '.chosen-select-no-results': {no_results_text: 'Oops, nothing found!'},
        '.chosen-select-width': {width: "95%"}
    }
    for (var selector in config) {
        $(selector).chosen(config[selector]);
    }


    // binds form submission and fields to the validation engine
    $("#frm_contract_approving_phases").validationEngine({
            prettySelect : true,
            useSuffix: "_chosen",
            promptPosition : "topRight:-150"
            //promptPosition : "bottomLeft"
    });

 
});


</script>

<script type="text/javascript">
	
    $('#next_phase').change(function()
     {
        var next_phase_id=$('#next_phase').val();
        var safa_uo_contracts_id=$('#safa_uo_contracts_id').val();
            
        var dataString = 'next_phase_id='+ next_phase_id+'&safa_uo_contracts_id='+ safa_uo_contracts_id;
            
            $.ajax
            ({
            type: 'POST',
            url: '<?php echo base_url().'uo/contract_approving_phases/getPhaseDocuments'; ?>',
            data: dataString,
            cache: false,
            success: function(html)
            {
                //alert(html);
            $("#required_documents").html(html);
            $("#required_documents").trigger("chosen:updated");
            }
            });
            
     });


    $("#remove_all_required_documents_lnk").click(function(){
    	$("#required_documents").val([]);
    	$("#required_documents").trigger("chosen:updated");
	});
    
</script>