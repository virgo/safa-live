<script src="<?= NEW_TEMPLATE ?>/dashboard/js/html5-trunk.js"></script>
<link href="<?= NEW_TEMPLATE ?>/dashboard/icomoon/style.css" rel="stylesheet">
<link href="<?= NEW_TEMPLATE ?>/dashboard/css/<?= lang('DIR') ?>/main.css" rel="stylesheet">
<link href="<?= NEW_TEMPLATE ?>/dashboard/css/nvd-charts.css" rel="stylesheet"> 

<div class="dashboard-wrapper">
<div class="main-container">
<div class="navbar hidden-desktop">
<div class="navbar-inner">
<div class="container"><a class="btn btn-navbar" data-toggle="collapse"
	data-target=".navbar-responsive-collapse" data-original-title=""> <span
	class="icon-bar"></span> <span class="icon-bar"></span> <span
	class="icon-bar"></span> </a>
<div class="nav-collapse collapse navbar-responsive-collapse">
<ul class="nav">
	<li><a href="index.html" data-original-title="">Dashboard</a></li>

	<li><a href="reports.html" data-original-title="">Reports</a></li>
	<li><a href="forms.html" data-original-title="">Basic Forms</a></li>
	<li><a href="extended-forms.html" data-original-title="">Extended Forms</a>
	</li>
	<li><a href="form-wizards.html" data-original-title="">Form Wizard</a>
	</li>
	<li><a href="graphs.html" data-original-title="">Flot Charts</a></li>
	<li><a href="google-charts.html" data-original-title="">Google Charts</a>
	</li>
	<li><a href="animated-charts.html" data-original-title="">Animated
	Charts</a></li>
	<li><a href="ui-elements.html" data-original-title="">General Elements</a>
	</li>
	<li><a href="clients-list.html" data-original-title="">Clients List</a>
	</li>
	<li><a href="messages.html" data-original-title="">Messages</a></li>
	<li><a href="timeline.html" data-original-title="">Timeline</a></li>
	<li><a href="pricing.html" data-original-title="">Pricing Plans</a></li>
	<li><a href="grid.html" data-original-title="">Grid Layout</a></li>
	<li><a href="icons.html" data-original-title="">Buttons &amp; Icons</a>
	</li>
	<li><a href="typography.html" data-original-title="">Typography</a></li>
	<li><a href="tables.html" data-original-title="">Static Tables</a></li>
	<li><a href="dynamic-tables.html" data-original-title="">Dynamic Tables</a>
	</li>
	<li><a href="gallery.html" data-original-title="">Gallery</a></li>
	<li><a href="invoice.html" data-original-title="">Invoice</a></li>
	<li><a href="calendar.html" data-original-title="">Calendar</a></li>
	<li><a href="profile.html" data-original-title="">Profile</a></li>
	<li><a href="error.html" data-original-title="">404 Error</a></li>
	<li><a href="faq.html" data-original-title="">Faq</a></li>
	<li><a href="login.html" data-original-title="">Login</a></li>
</ul>
</div>
</div>
</div>
</div>

<div class="page-header">
<div class="Fright">
<h2>لوحة البيانات</h2>
</div>
<div class="Fleft">
<ul class="stats">

	<li class="color-second"><span data-icon="" aria-hidden="true"
		class="fs1"></span>
	<div id="date-time" class="details">
	<span class="big">
 	<?php echo date("F j, Y"); ?>
	</span>
	<span>
	<?php echo date("g:i a");?>
	</span>
	</div>
	</li>
</ul>
</div>
<div class="clearfix"></div>

</div>
<div style="" class="row-fluid ">
<div class="metro-nav"><a style="margin-top: -10px;margin-left: 5px;"
	class="quick-action-btn span2 input-bottom-margin"
	data-original-title=""> <span data-icon="" aria-hidden="true"
	class="fs1"></span>
<p class="no-margin"></p>
<div class="label label-important"><?php echo lang('briefs'); ?></div>
</a>
<div style="margin-top: -10px;margin-right: 8px;"
	class="metro-nav-block nav-block-blue double "><a href="#"
	data-original-title="">
<div data-icon="" aria-hidden="true" class="fs1"></div>
<div class="info"><?php echo $total_mofa_count_for_ksa;?></div>
<div class="brand"><?php echo lang('total_mofa_count_for_saudi'); ?></div>
</a></div>

<div style="margin-top: -10px;margin-right: 8px;"
	class="metro-nav-block nav-block-red double "><a href="#"
	data-original-title="">
<div data-icon="" aria-hidden="true" class="fs1"></div>
<div class="info"><?php echo $total_mofa_count;?></div>
<div class="brand"><?php echo lang('total_mofa_count'); ?></div>
</a></div>
<div style="margin-top: -10px;margin-right: 8px;"
	class="metro-nav-block nav-block-yellow double "><a href="#"
	data-original-title="">
<div data-icon="" aria-hidden="true" class="fs1"></div>
<div class="info"><?php echo $leave_in_24_hours_count;?></div>
<div class="brand"><?php echo lang('leave_in_24_hours'); ?></div>
</a></div>
</div>
</div>

<!--<div class="row-fluid">


<div class="metro-nav"><a data-original-title="You have 1134 messages"
	class="quick-action-btn span2 input-bottom-margin"> <span data-icon=""
	aria-hidden="true" class="fs1"></span>
<p class="no-margin">Inbox</p>
<div class="label label-important">134</div>
</a> <a data-original-title="You have 39 attachments"
	class="quick-action-btn span2 input-bottom-margin"> <span data-icon=""
	aria-hidden="true" class="fs1"></span>
<p class="no-margin">Attachments</p>
<div class="label label-info">39</div>
</a> <a data-original-title="You sent 4695 emails"
	class="quick-action-btn span2 input-bottom-margin"> <span data-icon=""
	aria-hidden="true" class="fs1"></span>
<p class="no-margin">Sent</p>
<div class="label label-success">4695</div>
</a> <a data-original-title="5993 filters"
	class="quick-action-btn span2 input-bottom-margin"> <span data-icon=""
	aria-hidden="true" class="fs1"></span>
<p class="no-margin">Filters</p>
<div class="label label-warning">5993</div>
</a> <a data-original-title="1333 chats"
	class="quick-action-btn span2 input-bottom-margin"> <span data-icon=""
	aria-hidden="true" class="fs1"></span>
<p class="no-margin">Chats</p>
<div class="label label-important">1333</div>
</a> <a data-original-title="You have 499 alert messages"
	class="quick-action-btn span2 input-bottom-margin"> <span data-icon=""
	aria-hidden="true" class="fs1"></span>
<p class="no-margin">Alerts</p>
<div class="label label-info">449</div>
</a></div>
</div>-->


<!-- By Gouda, To Close menu On this screen --> <script>
jQuery(function() { 
	menu1.toggle();
})
</script> <!-- arraving trips section --> <?= form_open('', array('method' => 'GET')) ?>

<!-- this field to hold values (today, twm) --> <input type="hidden"
	name="departure" value="<?=$this->input->get('departure')?>"
	class="btn " /> <input type="hidden" name="arrival"
	value="<?=$this->input->get('arrival')?>" class="btn " />

<div class="row-fluid">
<div class="span12">
<div class="widget">
<div class="widget-header">
<div class="title"><span data-icon="" aria-hidden="true" class="fs1"></span>
<?= lang('arriving_form_name').$this->arriving_pane ?></div>
<div class="Fleft"><a
	href="<?= site_url("uo/dashboard/?arrival=today&departure=".$this->input->get('departure')) ?>"
	class="btn btn-primary"><?= lang('today') ?></a> <a
	href="<?= site_url("uo/dashboard/?arrival=twm&departure=".$this->input->get('departure')) ?>"
	class="btn btn-success"><?= lang('twm') ?></a> <? if ( count($arriving)) : ?>
<input type="submit" name="print_arr_report" value="<?=lang('print')?>"
	class="btn " /> <input type="submit" name="export_arr_report"
	value="<?=lang('export')?>" class="btn " /> <? endif; ?></div>
</div>
<div class="widget-body">


<table class=" table-condensed table-bordered no-margin">
	<thead>
		<tr>

			<th class="span1"><?= lang('trip') ?></th>

			<th class="span1"><?= lang('trip_id') ?></th>

			<th class="span1 hidden-phone"><?= lang('arr_date') ?></th>

			<th class="span1 hidden-phone"><?= lang('arr_time') ?></th>

			<th class="span1 hidden-phone"><?= lang('contract') ?></th>

			<th class="span1 hidden-phone"><?= lang('count') ?></th>

			<th class="span1 hidden-phone"><?= lang('hotel') ?></th>

			<th class="span1 hidden-phone"><?= lang('trans_company') ?></th>

			<th class="span1 hidden-phone"><?= lang('port') ?></th>

			<th class="span1 hidden-phone"><?= lang('nationality') ?></th>

			<th class="span1 hidden-phone"><?= lang('supervisor') ?></th>

			<th class="span1 hidden-phone"><?= lang('status') ?></th>

		</tr>
	</thead>
	<tbody>

	<? foreach ($arriving as $val): ?>
		<tr>

			<td class=" success">
			<?php 
                            if($val['safa_trip_internaltrip_id']=='') {
                            
								if($val['trip_name']!='') {
									echo $val['trip_name'];
								} else {
									echo $val['trip_title'];
								}
                            } else {
                            	
                            	if($val['trip_name']!='') {
									$safa_trip_internaltrip_link_text= $val['trip_name'];
								} else {
									$safa_trip_internaltrip_link_text=  $val['trip_title'];
								}
                            ?>
                            <a href="<?= site_url("safa_trip_internaltrips/manage/". $val['safa_trip_internaltrip_id']) ?>" target="_blank"> <?php echo $safa_trip_internaltrip_link_text; ?> </a>
                            <?php 
                            }
                            
							?>
			</td>
			<?php 
			$status_color = $this->flight_states->getFlightColor($val['fs_flight_id']);
			?>
			<td class=" info"><span style="background-color:#<?php echo $status_color;?>" class="label">
			<?= $val['flight_number'] ?> </span></td>

			<td class="hidden-phone warning"><?= get_date($val['arrival_date']) ?></td>

			<td class="hidden-phone error"><?= get_time($val['arrival_time']) ?></td>

			<td class="hidden-phone success"><?= $val['cn'] ?></td>

			<td class="hidden-phone info">
			<!--<a
				href="<?= site_url("trip_details/details/". $val['trip_id']) ?>"
				target="_blank"> <?= ($val['travellers'])?($val['travellers']):($val['travellers_adult_count']+$val['travellers_child_count']+$val['travellers_infant_count']) ?>
			</a>
			
			-->
			<?= $val['seats_count'] ?>
			</td>

			<td class="hidden-phone warning"><?= $val['hotel_name'] ?></td>

			<td class="hidden-phone error"><?= $val['transporter'] ?></td>

			<td class="hidden-phone success">
			<?= $val['start_port_name'] ?> -  <?= $val['end_port_name'] ?> (<?= $val['end_port_hall_name'] ?>)
			</td>

			<td class="hidden-phone info"><?= $val['nationality'] ?></td>

			<td class="hidden-phone warning">
			<?php 
			if($val['supervisor']!='') {
				echo $val['supervisor'];
			} else {
				echo $val['trip_supervisors'];
			}
			?>
			</td>
			<?php 
			$status_name='';
			$status_row = $this->flight_states->getStatus($val['fs_flight_id']);
			if(count($status_row)>0) {
				$status_name = $status_row->{name()} ;
			}
			?>
			<td class="hidden-phone error"><?= $status_name ?></td>

		</tr>
		<? endforeach; ?>

	</tbody>
</table>
</div>
</div>
</div>
</div>
<div class="row-fluid">
<div class="span12">
<div class="widget">
<div class="widget-header">
<div class="title"><span data-icon="" aria-hidden="true" class="fs1"></span>
		<?= lang('departing_form_name').$this->departing_pane ?></div>
<div class="Fleft"><a
	href="<?= site_url("uo/dashboard/?arrival=".$this->input->get('arrival')."&departure=today") ?>"
	class="btn btn-primary"><?= lang('today') ?></a> <a
	href="<?= site_url("uo/dashboard/?arrival=".$this->input->get('arrival')."&departure=twm") ?>"
	class="btn btn-success"><?= lang('twm') ?></a> <? if (count($departing)) :?>
<input type="submit" name="print_dep_report" value="<?=lang('print')?>"
	class="btn " /> <input type="submit" name="export_dep_report"
	value="<?=lang('export')?>" class="btn " /> <? endif;?></div>


</div>

<div class="widget-body">


<table class=" table-condensed table-bordered no-margin">
	<thead>
		<tr>

			<th class="span1"><?= lang('trip') ?></th>

			<th class="span1"><?= lang('trip_id') ?></th>

			<th class="span1 hidden-phone"><?= lang('dep_date') ?></th>

			<th class="span1 hidden-phone"><?= lang('dep_time') ?></th>

			<th class="span1 hidden-phone"><?= lang('contract') ?></th>

			<th class="span1 hidden-phone"><?= lang('count') ?></th>

			<th class="span1 hidden-phone"><?= lang('hotel') ?></th>

			<th class="span1 hidden-phone"><?= lang('trans_company') ?></th>

			<th class="span1 hidden-phone"><?= lang('port') ?></th>

			<th class="span1 hidden-phone"><?= lang('nationality') ?></th>

			<th class="span1 hidden-phone"><?= lang('supervisor') ?></th>

			<th class="span1 hidden-phone"><?= lang('status') ?></th>


		</tr>
	</thead>


	<tbody>


	<? foreach ($departing as $val): ?>
		<tr>

			<td class="success">
			<?php 
                            if($val['safa_trip_internaltrip_id']=='') {
                            
								if($val['trip_name']!='') {
									echo $val['trip_name'];
								} else {
									echo $val['trip_title'];
								}
                            } else {
                            	
                            	if($val['trip_name']!='') {
									$safa_trip_internaltrip_link_text= $val['trip_name'];
								} else {
									$safa_trip_internaltrip_link_text=  $val['trip_title'];
								}
                            ?>
                            <a href="<?= site_url("safa_trip_internaltrips/manage/". $val['safa_trip_internaltrip_id']) ?>" target="_blank"> <?php echo $safa_trip_internaltrip_link_text; ?> </a>
                            <?php 
                            }
                            
							?>
			</td>

			<?php 
			$status_color = $this->flight_states->getFlightColor($val['fs_flight_id']);
			?>
			<td class=" info"><span style="background-color:#<?php echo $status_color;?>" class="label">
			<?= $val['flight_number'] ?> </span></td>

			<td class="hidden-phone warning"><?= get_date($val['flight_date']) ?></td>

			<td class="hidden-phone error"><?= get_time($val['departure_time']) ?></td>

			<td class="hidden-phone success"><?= $val['cn'] ?></td>

			<td class="hidden-phone info"><!--<a
				href="<?= site_url("trip_details/details/". $val['trip_id']) ?>"
				target="_blank"> <?= ($val['travellers'])?($val['travellers']):($val['travellers_adult_count']+$val['travellers_child_count']+$val['travellers_infant_count']) ?>
			</a>-->
			<?= $val['seats_count'] ?>
			</td>

			<td class="hidden-phone warning"><?= $val['hotel_name'] ?></td>

			<td class="hidden-phone error"><?= $val['transporter'] ?></td>

			<td class="hidden-phone ">
			<?= $val['start_port_name'] ?> (<?= $val['start_port_hall_name'] ?>) -  <?= $val['end_port_name'] ?>
			
			</td>

			<td class="hidden-phone success"><?= $val['nationality'] ?></td>

			<td class="hidden-phone info">
			
			<?php 
			if($val['supervisor']!='') {
				echo $val['supervisor'];
			} else {
				echo $val['trip_supervisors'];
			}
			?>
			</td>

			<?php 
			$status_name='';
			$status_row = $this->flight_states->getStatus($val['fs_flight_id']);
			if(count($status_row)>0) {
				$status_name = $status_row->{name()} ;
			}
			?>
			<td class="hidden-phone error"><?= $status_name ?></td>


		</tr>
		<? endforeach; ?>

	</tbody>
</table>

</div>
</div>
</div>
</div>
		<?= form_close() ?>








<div class="row-fluid" data-brackets-id="12989">
<div class="span6" data-brackets-id="12990">
<div class="widget no-margin" data-brackets-id="12991">
<div class="widget-header" data-brackets-id="12992">
<div class="title" data-brackets-id="12993"><span data-icon=""
	aria-hidden="true" class="fs1" data-brackets-id="12994"></span><?php echo lang('trips_count_uo'); ?>
</div>
</div>
<div class="widget-body" data-brackets-id="12995">
<div id="pie_chart" data-brackets-id="12996">
<?php 
$this->gcharts->load('LineChart');

$this->gcharts->DataTable('uo_trips')
              ->addColumn('number', lang('uo_trips_count'), 'count')
              ->addColumn('number', lang('uo_trips_count'), 'official');

for($a = 1; $a < 12; $a++)
{
	$this->safa_trips_model->safa_uo_id= $this->uo_id ;
	$this->safa_trips_model->date_from = $cur_year.'-'.$a.'-01';
	$this->safa_trips_model->date_to = $cur_year.'-'.($a+1).'-01';
	$uo_trips_rows_count= $this->safa_trips_model->get(true);
	
    $data = array(
        $a,             //Count
        $uo_trips_rows_count
        //rand(800,1000), //Line 1's data
        //rand(800,1000)  //Line 2's data
    );

    $this->gcharts->DataTable('uo_trips')->addRow($data);
    
    //echo "<script>alert('$uo_trips_rows_count');</script>";
}

$config = array(
    'title' => lang('uo_trips')
);

$this->gcharts->LineChart('uo_trips')->setConfig($config);


echo $this->gcharts->LineChart('uo_trips')->outputInto('uo_trips_div');
echo $this->gcharts->div(490, 300);

if($this->gcharts->hasErrors())
{
    echo $this->gcharts->getErrors();
}
?>
	
	</div>
</div>
</div>
</div>

<div class="span6" data-brackets-id="13088" style=" margin-left: 0px;margin-right: 20px;">
<div class="widget" data-brackets-id="13089">
<div class="widget-header" data-brackets-id="13090">
<div class="title" data-brackets-id="13091"><span data-icon=""
	aria-hidden="true" class="fs1" data-brackets-id="13092"></span> <?php echo lang('trips_count_from_ea'); ?>
</div>
</div>
<div class="widget-body" data-brackets-id="13093">
<div style="direction: ltr;" id="area_chart" data-brackets-id="13094">

<?php 
$this->gcharts->load('PieChart');

$slice1 = rand(0,40);
$slice2 = rand(0,40);
$slice3 = rand(0,40);
$slice4 = rand(0,40);

$this->gcharts->DataTable('ea_trips')
              ->addColumn('string', 'ea_trips', 'ea_trip')
              ->addColumn('string', 'Amount', 'amount')
              //->addRow(array('Pizza', $slice1))
              //->addRow(array('Beer', $slice2))
              //->addRow(array('Steak', $slice3))
              //->addRow(array('Bacon', $slice4))
              ;
              
foreach($eas_rows as $eas_row) {
	$this->gcharts->DataTable('ea_trips')->addRow(array($eas_row->ea_name, $eas_row->safa_trips_count));
}

$config = array(
    'title' => lang('ea'),
    'is3D' => TRUE
);

$this->gcharts->PieChart('ea_trips')->setConfig($config);



echo $this->gcharts->PieChart('ea_trips')->outputInto('ea_trips_div');
echo $this->gcharts->div(490,300);

if($this->gcharts->hasErrors())
{
    echo $this->gcharts->getErrors();
}

?>
	
	
</div>
</div>
</div>
</div>
</div>

<div class="row-fluid">
<div class="span12">
<div class="widget">
<div class="widget-header">
<div class="title"><span data-icon="" aria-hidden="true" class="fs1"></span><?php echo lang('trips_count_from_countries');?>
</div>
</div>
<div class="widget-body">
<div id="discrete_bar_chart">

<?php 
$this->gcharts->load('ColumnChart');

$this->gcharts->DataTable('country_trips');

$chart_rows_arr=array();
$chart_rows_arr[] = lang('countries');

$this->gcharts->DataTable('country_trips')
              ->addColumn('string', lang('country_trips'), lang('country_trips'));
              
foreach($uo_trips_countries as $uo_trips_country) {
	$this->gcharts->DataTable('country_trips')->addColumn('number', $uo_trips_country->country_name, '');
	
	
	$this->safa_trips_model->date_from = $cur_year.'-01-01';
	$this->safa_trips_model->date_to = $cur_year.'-12-01';
	$this->safa_trips_model->erp_country_id = $uo_trips_country->erp_country_id;
	
	$country_trips_count = $this->safa_trips_model->get(true);
	
	//echo "<script>alert('$uo_trips_country->erp_country_id');</script>";
	//echo $this->db->last_query(); exit;
	$chart_rows_arr[] = $country_trips_count;
}              

//print_r($chart_rows_arr); exit;

$this->gcharts->DataTable('country_trips')->addRow($chart_rows_arr);

$config = array(
    'title' => lang('uo_trips_count')
);

$this->gcharts->ColumnChart('country_trips')->setConfig($config);


echo $this->gcharts->ColumnChart('country_trips')->outputInto('country_trips_div');
echo $this->gcharts->div(1000, 300);
if($this->gcharts->hasErrors())
{
    echo $this->gcharts->getErrors();
}
?>
	
	
	
</div>
</div>
</div>
</div>
</div>



</div>

<script src="<?= NEW_TEMPLATE ?>/dashboard/js/bootstrap.js"></script> <script
	src="<?= NEW_TEMPLATE ?>/dashboard/js/moment.js"></script> <!-- Google Visualization JS -->

