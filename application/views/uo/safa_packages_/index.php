<style>
    .wizerd-div {
        border-bottom: medium none !important;
        margin: -18px 0 20px;
        padding-top: 0;
    }
    .wizerd-div a {
        background: none repeat scroll 0 0 #FAFAFA;
        border: 1px solid #D5D6D6;
        border-radius: 49px;
        color: #663300;
        display: inline-block;
        margin: -6px 5px -17px -8px;
        padding: 10px 12px 8px;
    }
    a {
        color: #C09853;
    }
    .resalt-group {
        margin: 18px 0.5% 0.5%;
        padding: 0.5%;
        width: 99%;
    }
    th a.btn, th input[type="button"], th input[type="submit"], th button {
        margin: 0;
        padding: 4px 12px;
    }
    .coll_close, .coll_open {
        margin-top: 0;
    }
    .chosen-container {
        margin-top: 4px;
    }
    .warning {
        color: #C09853;
    }
</style>

<div class="widget">
    <div class="path-container Fright">
        <div class="path-name Fright">
            <a href="<?= site_url('uo/dashboard') ?>"><?= lang('global_system_management') ?></a>
        </div>
        <div class="path-arrow Fright"> </div>
        <div class="path-name Fright"><?= lang('menu_packages') ?></div>
    </div>
</div>

<div class="widget">
    <div class="widget-header">
        <div class="widget-header-icon Fright"></div>
        <div class="widget-header-title Fright"><?= lang('menu_packages') ?></div>
        <a class="btn Fleft" href="<?= site_url('uo/packages/manage') ?>"><?= lang('global_add') ?></a>
    </div>
    <div class="widget-container">
        <div class='table-responsive' >
            <table cellpadding="0" class="" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th><?= lang('package_code') ?></th>
                        <th><?= lang('package_name') ?></th>
                        <th><?= lang('global_actions') ?></th>
                    </tr>
                </thead>
                <tbody>
                    <? if (isset($items)): ?>
                        <? foreach ($items as $item): ?>
                            <tr>
                                <td class="TAC info"><?= $item->package_code ?></td>
                                <td class="TAC success"><?= $item->{name()} ?></td>
                                <td class="TAC">
                                    <a href="<?= site_url("uo/packages/manage/" . $item->safa_package_id) ?>" ><span class="icon-pencil"></span></a>
                                    <a href="<?= site_url("uo/packages/view/" . $item->safa_package_id) ?>" ><span class="icon-list-alt"></span></a>
                                </td>
                            </tr>
                        <? endforeach ?>
                    <? endif ?>
                </tbody>
            </table>
            <? if (isset($pagination)): ?><?= $pagination ?><? endif ?>
        </div>
    </div>
</div>