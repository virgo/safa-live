<?php
$safa_uo_package_id = '';
$package_code = '';
$date_type = '';
$start_date = '';
$end_date = '';
$name_ar = '';
$name_la = '';
$sale_currency_id = '';
$transport_type_id = '';
$remarks = '';
$active = '';

$screen_title = lang('view_title');

if (isset($item)) {
    $package_code = $item->package_code;
    $date_type = $item->date_type;
    $start_date = $item->start_date;
    $end_date = $item->end_date;
    $name_ar = $item->name_ar;
    $name_la = $item->name_la;
    $sale_currency_id = $item->sale_currency_id;
    $transport_type_id = $item->safa_packages_transportation_type_id;
    $remarks = $item->remarks;
}
?>


<style>
    .wizerd-div {
        border-bottom: medium none !important;
        margin: -18px 0 20px;
        padding-top: 0;
    }
    .wizerd-div a {
        background: none repeat scroll 0 0 #FAFAFA;
        border: 1px solid #D5D6D6;
        border-radius: 49px;
        color: #663300;
        display: inline-block;
        margin: -6px 5px -17px -8px;
        padding: 10px 12px 8px;
    }
    a {
        color: #C09853;
    }
    .resalt-group {
        margin: 18px 0.5% 0.5%;
        padding: 0.5%;
        width: 99%;
    }
    th a.btn, th input[type="button"], th input[type="submit"], th button {
        margin: 0;
        padding: 4px 12px;
    }
    .coll_close, .coll_open {
        margin-top: 0;
    }
    .chosen-container {
        margin-top: 4px;
    }
    .warning{
        color: #C09853;
    }
</style>

<div class="widget">
    <div class="path-container Fright">
        <div class="path-name Fright">
            <a href=""><?= lang('global_system_management') ?></a>
        </div>
        <div class="path-arrow Fright" ></div>
        <div class="path-name Fright"> <a href="<?php echo site_url('uo/packages') ?>"><?php echo lang('title') ?></a></div>

        <div class="path-arrow Fright"></div>
        <div class="path-name Fright"><?php echo $screen_title; ?></div>
    </div>
</div>

<div class="widget">
    <div class="widget-header">
        <div class="widget-header-icon Fright"></div>
        <div class="widget-header-title"><?php echo $screen_title; ?></div>
    </div>
    <div class="widget-container">
        <div class="resalt-group">
            <div class="wizerd-div"><a><?php echo lang('main_data'); ?></a></div>
            <div class="table-warp">
                <table class="">
                    <thead>
                        <tr>
                            <th><?php echo lang('package_code'); ?></th>
                            <th><?php echo lang('package_name'); ?></th>
                            <th><?php echo lang('currency_transport'); ?></th>
                            <th colspan="2"><?php echo lang('period'); ?></th>


                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="span2"> <?= $package_code ?></td>
                            <td>
                                <div class="span12">
                                    <div class="span12"><label><?= lang('name_ar'); ?></label> </div>
                                    <span class="span12">
                                        <label class="warning"><?= $name_ar ?></label>
                                    </span>
                                </div>
                                <div class="span12">
                                    <div class="span12"><label><?php echo lang('name_la'); ?></label> </div>
                                    <div class="span12"><label class="warning"><?= $name_la ?></label></div>
                                </div>
                            </td>
                            <td>

                                <div class="span12">
                                    <div class="span12"><label><?php echo lang('currency'); ?></label></div>
                                    <span class="span12"><label class="warning"><?= $erp_currencies[$sale_currency_id] ?></label>
                                    </span>
                                </div>
                                <div class="span12">
                                    <div class="span12"><label><?php echo lang('transport_type_id'); ?></label></div>
                                    <div class="span12"><label class="warning"><?= $erp_transportertypes[$transport_type_id] ?></label>
                                    </div>
                                </div>


                            </td>
                            <td>
                                <div class="span12"><label class="span3"><?php echo lang('date_type'); ?></label>

                                    <?php
                                    $islamic_checked = "";
                                    $gregorian_checked = "";
                                    if ($date_type == 'islamic') {
                                        $islamic_checked = "checked='checked'";

                                        $start_date = $this->hijrigregorianconvert->GregorianToHijri($start_date, 'YYYY-MM-DD');
                                        $end_date = $this->hijrigregorianconvert->GregorianToHijri($end_date, 'YYYY-MM-DD');
                                    } else {
                                        $gregorian_checked = "checked='checked'";
                                    }
                                    ?>

                                    <label class="span3" ><?php echo lang($date_type); ?></label>

                                </div>

                                <div class="span3"><label><?php echo lang('start_date'); ?></label></div>
                                <div class="span8"><label class="warning"><?= $start_date ?></label></div>
                                <div class="span3"><label><?php echo lang('end_date'); ?></label></div>
                                <div class="span8"><label class="warning"><?= $end_date ?></label></div>
                            </td>
                        </tr>
                    </tbody>
                </table>

            </div>

        </div>

        <div class="resalt-group" id="dv_hotels">
            <div class="wizerd-div"><a><?php echo lang('hotels'); ?></a></div>
            <div class="table-warp">
                <table>
                    <thead>
                        <tr>
                            <th class="span5"><?php echo lang('city'); ?></th>
                            <th class="span5"><?php echo lang('hotel'); ?></th>
                            <th class="span5"><?php echo lang('meal'); ?></th>
                        </tr>
                    </thead>
                    <tbody class='hotels' id='hotels'>
                        <? if (isset($item_hotels)) { ?>
                            <? if (check_array($item_hotels)) { ?>
                                <? foreach ($item_hotels as $item_hotel) { ?>
                                    <tr rel="<?php echo $item_hotel->safa_uo_package_hotel_id ?>">
                                        <td><?= $erp_cities[$item_hotel->erp_city_id] ?></td>
                                        <td><?= $erp_hotels[$item_hotel->erp_hotel_id] ?></td>
                                        <td><?= $erp_meals[$item_hotel->erp_meal_id] ?></td>
                                    </tr>
                                <? } ?>
                            <? } ?>
                        <? } ?>
                    </tbody>
                </table>
            </div>
        </div>

        <div class="resalt-group">
            <div class="wizerd-div"><a><?php echo lang('hotels_prices'); ?></a></div>
            <div class="table-warp">
                <table>
                    <thead>
                        <tr>
                            <th  class="span2"><?php echo lang('package_period'); ?></th>

                            <?php
                            if (isset($erp_hotelroomsizes)) {
                                if (check_array($erp_hotelroomsizes)) {

                                    foreach ($erp_hotelroomsizes as $erp_hotelroomsize) {
                                        echo '<th>' . $erp_hotelroomsize->{name()} . '</th>';
                                    }
                                }
                            }
                            ?>

                        </tr>
                    </thead>
                    <tbody class='hotels_prices' id='hotels_prices'>
                        <? if (isset($item_hotels_prices)) { ?>
                            <? if (check_array($item_hotels_prices)) { ?>
                                <? foreach ($item_hotels_prices as $item_hotels_price) { ?>
                                    <tr rel="<?php echo $item_hotels_price->safa_package_periods_id ?>">

                                        <td><?= $package_periods[$item_hotels_price->package_period_id] ?></td>


                                        <?php
                                        if (isset($erp_hotelroomsizes)) {
                                            if (check_array($erp_hotelroomsizes)) {

                                                foreach ($erp_hotelroomsizes as $erp_hotelroomsize) {

                                                    $erp_hotelroomsize_id = $erp_hotelroomsize->erp_hotelroomsize_id;
                                                    ?>
                                                    <td><?= get_roomsize_price($item_hotels_price->safa_package_periods_id, $erp_hotelroomsize_id) ?></td>
                                                    <?php
                                                }
                                            }
                                        }
                                        ?>
                                    </tr>
                                <? } ?>
                            <? } ?>
                        <? } ?>

                    </tbody>
                </table>

            </div>
        </div>



        <div class="resalt-group">
            <div class="wizerd-div"><a><?php echo lang('execlusive_nights_prices'); ?></a></div>
            <div class="table-warp">
                <table>
                    <thead>
                        <tr>
                            <th  class="span2"><?php echo lang('hotel'); ?></th>
                            <th  class="span2"><?php echo lang('meal'); ?></th>

                            <?php
                            if (isset($erp_hotelroomsizes)) {
                                if (check_array($erp_hotelroomsizes)) {

                                    foreach ($erp_hotelroomsizes as $erp_hotelroomsize) {
                                        echo '<th>' . $erp_hotelroomsize->{name()} . '</th>';
                                    }
                                }
                            }
                            ?>

                        </tr>
                    </thead>
                    <tbody class='execlusive_nights_prices' id='execlusive_nights_prices'>
                        <? if (isset($item_execlusive_nights_prices)) { ?>
                            <? if (check_array($item_execlusive_nights_prices)) { ?>
                                <? foreach ($item_execlusive_nights_prices as $item_night) { ?>
                                    <tr>
                                        <td><?= $erp_hotels[$item_night->hotel_id] ?></td>
                                        <td><?= $erp_meals[$item_night->erp_meal_id] ?></td>

                                        <?php
                                        if (isset($erp_hotelroomsizes)) {
                                            if (check_array($erp_hotelroomsizes)) {

                                                foreach ($erp_hotelroomsizes as $erp_hotelroomsize) {

                                                    $erp_hotelroomsize_id = $erp_hotelroomsize->erp_hotelroomsize_id;
                                                    ?>
                                                    <td><?= get_nights_price($item_night->safa_package_execlusive_night_id, $erp_hotelroomsize_id) ?></td>
                                                    <?php
                                                }
                                            }
                                        }
                                        ?>



                                    </tr>
                                <? } ?>
                            <? } ?>
                        <? } ?>

                    </tbody>
                </table>

            </div>
        </div>

        <? if (check_array($item_tourism_places)) { ?>
            <div class="resalt-group">
                <div class="wizerd-div"><a><?php echo lang('tourism_places'); ?></a></div>
                <div class="table-warp">
                    <table>
                        <thead>
                            <tr>
                                <th class="span5"><?php echo lang('city'); ?></th>
                                <th class="span5"><?php echo lang('tourism_place'); ?></th>
                        </thead>
                        <tbody class='tourism_places' id='tourism_places'>
                            <? foreach ($item_tourism_places as $item_tourism_place) { ?>
                                <tr>
                                    <td><?= $erp_cities[$item_tourism_place_city_id] ?></td>
                                    <td><?= $safa_tourismplaces[$item_tourism_place->safa_tourismplace_id] ?></td>
                                </tr>
                            <? } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        <? } ?>
        <? if (isset($item_execlusive_meals_prices) && check_array($item_execlusive_meals_prices)) : ?>
            <div class="resalt-group">
                <div class="wizerd-div"><a><?php echo lang('execlusive_meals_prices'); ?></a></div>
                <div class="table-warp">
                    <table>
                        <thead>
                            <tr>
                                <th class="span5"><?php echo lang('hotel'); ?></th>
                                <th class="span5"><?php echo lang('meal'); ?></th>
                                <th class="span5"><?php echo lang('meal_price'); ?></th>
                        </thead>
                        <tbody class='execlusive_meals_prices' id='execlusive_meals_prices'>
                            <? foreach ($item_execlusive_meals_prices as $item_execlusive_meals_price) { ?>
                                <tr>
                                    <td><?= $erp_hotels[$item_execlusive_meals_price->hotel_id] ?></td>
                                    <td><?= $erp_meals[$item_execlusive_meals_price->erp_meal_id] ?></td>
                                    <td><?= $item_execlusive_meals_price->price ?></td>

                                </tr>
                            <? } ?>
                        </tbody>
                    </table>

                </div>
            </div>
        <? endif ?>       

        <? if (strlen($remarks)) : ?>
            <div class="resalt-group">
                <div class="wizerd-div"><a><?php echo lang('notes_and_conditions'); ?></a></div>
                <div class="table-warp">
                    <div class="span11" >

                        <div class="span10"><?= $remarks ?></div>
                    </div>
                </div>
            </div>
        <? endif ?>
    </div>
</div>
