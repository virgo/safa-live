<script>hotels_prices_counter = 0</script>
<div class="resalt-group">
    <div class="wizerd-div"><a><?php echo lang('hotels_prices'); ?></a></div>
    <div class="table-warp">
        <table>
            <thead>
                <tr>
                    <th  class="span2"><?php echo lang('package_period'); ?></th>
                    <?php
                    if (isset($erp_hotelroomsizes)) {
                        if (check_array($erp_hotelroomsizes)) {
                            foreach ($erp_hotelroomsizes as $erp_hotelroomsize) {
                                echo '<th>' . $erp_hotelroomsize->{name()} . '</th>';
                            }
                        }
                    }
                    ?>
                    <th>
                        <a class="btn Fleft" title="<?php echo lang('global_add'); ?>" href="javascript:void(0)" onclick="add_hotels_prices('N' + hotels_prices_counter++);load_multiselect()">
                            <?= lang('global_add') ?>    
                        </a>
                    </th>
                </tr>
            </thead>
            <tbody class='hotels_prices' id='hotels_prices'>
                <? if (isset($item_hotels_prices)) { ?>
                    <? if (check_array($item_hotels_prices)) { ?>
                        <? foreach ($item_hotels_prices as $item_hotels_price) { ?>
                            <tr rel="<?php echo $item_hotels_price->safa_package_periods_id ?>">
                                <td>
                                    <?php
                                    echo form_dropdown('hotels_prices_safa_uo_package_id[' . $item_hotels_price->safa_package_periods_id . ']'
                                            , $package_periods, set_value('hotels_prices_safa_uo_package_id[' . $item_hotels_price->safa_package_periods_id . ']', $item_hotels_price->safa_package_periods_id)
                                            , 'class="chosen-select chosen-rtl input-full" id="hotels_prices_safa_uo_package_id[' . $item_hotels_price->safa_package_periods_id . ']"  tabindex="4" ');
                                    ?>
                                </td>
                                <?php
                                if (isset($erp_hotelroomsizes)) {
                                    if (check_array($erp_hotelroomsizes)) {

                                        foreach ($erp_hotelroomsizes as $erp_hotelroomsize) {

                                            $erp_hotelroomsize_id = $erp_hotelroomsize->erp_hotelroomsize_id;
                                            ?>
                                            <td><?php
                                            echo form_number('hotels_prices_price[' . $item_hotels_price->safa_package_periods_id . '][' . $erp_hotelroomsize_id . ']'
                                                    , set_value('hotels_prices_price[' . $item_hotels_price->safa_package_periods_id . '][' . $erp_hotelroomsize_id . ']', get_roomsize_price($item_hotels_price->safa_package_periods_id, $erp_hotelroomsize_id))
                                                    , 'class=" input-huge" readonly="readonly" rprel="'.$erp_hotelroomsize_id.'"')
                                            ?>
                                            </td>
                                            <?php
                                        }
                                    }
                                }
                                ?>
                                <td class="TAC">
                                    <a href="javascript:void(0)" onclick="delete_hotels_prices(<?php echo $item_hotels_price->safa_package_periods_id ?>, true)"><span class="icon-trash"></span></a>
                                </td>
                            </tr>
                        <? } ?>
                    <? } ?>
                <? } ?>
            </tbody>
        </table>
    </div>
</div>
<script>
    function add_hotels_prices(id) {
        var text = [];
        $('#dv_hotels :input').each(function() {
            var input_id = $(this).attr("id");
            //alert(input_name);
            if (input_id !== undefined) {
                if (input_id.startsWith('hotels_erp_hotel_id')) {

                    var input_value = $("#" + input_id + " option:selected").val();
                    var input_text = $("#" + input_id + " option:selected").text();


                    text.push(new Array(input_value, input_text));
                }
            }
        })

        var slct_options_erp_hotels = "<option value=''></option>";
        for (var i = 0; i < text.length; i++) {
            slct_options_erp_hotels = slct_options_erp_hotels + "<option value='" + text[i][0] + "'>" + text[i][1] + "</option>";
        }

        var new_row = [
                "<tr rel=\"" + id + "\">",
                "    <td>",
                "       <select name=\"hotels_prices_safa_uo_package_id[" + id + "]\" class=\"chosen-select chosen-rtl input-full validate[required]\"  id=\"hotels_prices_safa_uo_package_id_" + id + "\" tabindex=\"4\">",
<? foreach ($package_periods as $key => $value): ?>
            "           <option value=\"<?= $key ?>\"><?= $value ?></option>",
<? endforeach ?>
        "    </td>",
<?php
if (isset($erp_hotelroomsizes)) {
    if (check_array($erp_hotelroomsizes)) {

        foreach ($erp_hotelroomsizes as $erp_hotelroomsize) {

            $erp_hotelroomsize_id = $erp_hotelroomsize->erp_hotelroomsize_id;
            ?>
                    '     <td><?php echo form_number("hotels_prices_price[' + id + '][" . $erp_hotelroomsize_id . "]", '0', 'class="validate[required] input-huge"  readonly="readonly" rprel="'.$erp_hotelroomsize_id.'"') ?>',
                            "    </td>",
            <?php
        }
    }
}
?>
        "    <td class=\"TAC\">",
                "        <a href=\"javascript:void(0)\" onclick=\"delete_hotels_prices('" + id + "')\"><span class=\"icon-trash\"></span></a>",
                "    </td>",
                "</tr>"
        ].join("\n");
        $('#hotels_prices').append(new_row);
    }
    function delete_hotels_prices(id, database) {
        if (typeof database == 'undefined')
        {
            $('.hotels_prices').find('tr[rel="' + id + '"]').remove();
        }
        else
        {
            $('.hotels_prices').find('tr[rel=' + id + ']').remove();
            var hidden_input = '<input type="hidden" name="hotels_prices_remove[]" value="' + id + '" />';
            $('#hotels_prices').append(hidden_input);
        }
    }
</script>
