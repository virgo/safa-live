
    <div class="block-fluid">
    <table class="fsTable" cellpadding="0" cellspacing="0" width="100%">
        <thead>
            <tr>
                
                <th><?= lang('trip') ?></th>
                
                <? if (in_array('code', $cols)): ?>
                    <th><?= lang('trip_id') ?></th>
                <? endif; ?>

                <? if (in_array('departure_date', $cols)): ?>
                    <th><?= lang('dep_date') ?></th>
                <? endif; ?>

                <? if (in_array('departure_time', $cols)): ?>
                    <th> <?= lang('dep_time') ?></th>
                <? endif; ?>                                   

                <? if (in_array('contract', $cols)): ?>
                    <th><?= lang('contract') ?></th>
                <? endif; ?>

                <? if (in_array('cn', $cols)): ?>
                    <th><?= lang('count') ?></th>
                <? endif; ?>

                <? if (in_array('hotel', $cols)): ?>
                    <th><?= lang('hotel') ?></th>
                <? endif; ?>

                <? if (in_array('trans', $cols)): ?>
                    <th><?= lang('trans_company') ?></th>
                <? endif; ?>

                <? if (in_array('port', $cols)): ?>
                    <th><?= lang('port') ?></th>
                <? endif; ?>

                <? if (in_array('nationality', $cols)): ?>
                    <th><?= lang('nationality') ?></th>
                <? endif; ?>

                <? if (in_array('supervisor', $cols)): ?>
                    <th><?= lang('supervisor') ?></th>
                <? endif; ?>                             


            </tr>
        </thead>



        <tbody>                        


            <? foreach ($trips_ds as $val): ?>
                <tr>
                    
                    <td>
                    <?php 
			if($val['trip_name']!='') {
				echo $val['trip_name'];
			} else {
				echo $val['trip_title'];
			}
			?>
                    </td>
                    <? if (in_array('code', $cols)): ?>
                         <?php 
						$status_color = $this->flight_states->getFlightColor($val['fs_flight_id']);
						?>
						<td class=""><span style="background-color:#<?php echo $status_color;?>" class="label">
						<?= $val['flight_number'] ?> </span></td>
                    <? endif; ?>

                    <? if (in_array('departure_date', $cols)): ?>
                        <td><?= get_date($val['flight_date']) ?></td>
                    <? endif; ?>

                    <? if (in_array('departure_time', $cols)): ?>
                        <td><?= get_time($val['departure_time']) ?></td>
                    <? endif; ?>

                    <? if (in_array('contract', $cols)): ?>
                        <td><?= $val['cn'] ?></td>
                    <? endif; ?>

                    <? if (in_array('cn', $cols)): ?>
                        <td>
<!--                            <?= ($val['travellers'])?($val['travellers']):($val['travellers_adult_count']+$val['travellers_child_count']+$val['travellers_infant_count']) ?>-->
<?= $val['seats_count'] ?>
                        </td>
                    <? endif; ?>

                    <? if (in_array('hotel', $cols)): ?>
                        <td><?= $val['hotel_name'] ?></td>
                    <? endif; ?>

                    <? if (in_array('trans', $cols)): ?>
                        <td><?= $val['transporter'] ?></td>
                    <? endif; ?>

                    <? if (in_array('port', $cols)): ?>
                        <td> <?= $val['start_port_name'] ?> (<?= $val['start_port_hall_name'] ?>) - <?= $val['end_port_name'] ?>
                    
                    </td>
                    <? endif; ?>

                    <? if (in_array('nationality', $cols)): ?>
                        <td><?= $val['nationality'] ?></td>
                    <? endif; ?>

                    <? if (in_array('supervisor', $cols)): ?>
                        <td>
                        <?php 
			if($val['supervisor']!='') {
				echo $val['supervisor'];
			} else {
				echo $val['trip_supervisors'];
			}
			?>
                        </td>
                    <? endif; ?>

                </tr>
            <? endforeach; ?>

        </tbody>
    </table>  
    </div>
    <?= ($print_statement)?$print_statement:"" ?>