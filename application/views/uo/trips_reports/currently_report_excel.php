<? if ($this->input->get('print_arr_report')) : ?>
    <div class="table-responsive">
        <table class="fxTable" cellpadding="0" cellspacing="0">
            <thead>
                <tr>
                    <th><?= lang('trip') ?></th>
                    
                    <th><?= lang('trip_id') ?></th>

                    <th><?= lang('arr_date') ?></th>

                    <th><?= lang('arr_time') ?></th>

                    <th><?= lang('contract') ?></th>

                    <th><?= lang('count') ?></th>

                    <th><?= lang('hotel') ?></th>

                    <th><?= lang('trans_company') ?></th>

                    <th><?= lang('port') ?></th>

                    <th><?= lang('nationality') ?></th>

                    <th><?= lang('supervisor') ?></th>
                    
                    <th><?= lang('status') ?></th> 


                </tr>
            </thead>
            <tbody>

                <? foreach ($arriving as $val): ?>
                    <tr>

                        <td>
                        <?php 
			if($val['trip_name']!='') {
				echo $val['trip_name'];
			} else {
				echo $val['trip_title'];
			}
			?>
                        </td>
                        
                        <?php 
						$status_color = $this->flight_states->getFlightColor($val['fs_flight_id']);
						?>
						<td class=" info"><span style="background-color:#<?php echo $status_color;?>" class="label">
						<?= $val['flight_number'] ?> </span></td>

                        <td><?= get_date($val['arrival_date']) ?></td>

                        <td><?= get_time($val['arrival_time']) ?></td>

                        <td><?= $val['cn'] ?></td>

                        <td><!--
                            <?= ($val['travellers'])?($val['travellers']):($val['travellers_adult_count']+$val['travellers_child_count']+$val['travellers_infant_count']) ?>
                            -->
                            <?= $val['seats_count'] ?>
                            
                        </td>

                        <td><?= $val['hotel_name'] ?></td>

                        <td><?= $val['transporter'] ?></td>

                        <td>
                        <?= $val['start_port_name'] ?>
								 - 
								<?= $val['end_port_name'] ?>
								
							(<?= $val['end_port_hall_name'] ?>)
                                
                        
                        </td>
						<td><?= $val['nationality'] ?></td>

                        <td>
                        <?php 
			if($val['supervisor']!='') {
				echo $val['supervisor'];
			} else {
				echo $val['trip_supervisors'];
			}
			?>
                        </td>      
                        
                        <?php 
			$status_name='';
			$status_row = $this->flight_states->getStatus($val['fs_flight_id']);
			if(count($status_row)>0) {
				$status_name = $status_row->{name()} ;
			}
			?>
			<td class="hidden-phone error"><?= $status_name ?></td>

                    </tr>
                <? endforeach; ?>

            </tbody>
        </table>     
<? endif; ?>

        
        
        
<? if ($this->input->get('print_dep_report')) : ?>

        <table class="fxTable" cellpadding="0" cellspacing="0">
            <thead>
                <tr>
                    <th><?= lang('trip') ?></th>
                    
                    <th><?= lang('trip_id') ?></th>

                    <th><?= lang('dep_date') ?></th>

                    <th> <?= lang('dep_time') ?></th>

                    <th><?= lang('contract') ?></th>

                    <th><?= lang('count') ?></th>

                    <th><?= lang('hotel') ?></th>

                    <th><?= lang('trans_company') ?></th>

                    <th><?= lang('port') ?></th>

                    <th><?= lang('nationality') ?></th>

                    <th><?= lang('supervisor') ?></th>     
                    
                    <th><?= lang('status') ?></th> 


                </tr>
            </thead>


            <tbody>                        


                <? foreach ($departing as $val): ?>
                    <tr>
                        
                        <td><?= $val['trip_name'] ?></td>
                        
                        <?php 
						$status_color = $this->flight_states->getFlightColor($val['fs_flight_id']);
						?>
						<td class=" info"><span style="background-color:#<?php echo $status_color;?>" class="label">
						<?= $val['flight_number'] ?> </span></td>

                        <td><?= get_date($val['flight_date']) ?></td>

                        <td><?= get_time($val['departure_time']) ?></td>

                        <td><?= $val['cn'] ?></td>

                        <td>
<!--                            <?= ($val['travellers'])?($val['travellers']):($val['travellers_adult_count']+$val['travellers_child_count']+$val['travellers_infant_count']) ?>-->
<?= $val['seats_count'] ?>
                        </td>

                        <td><?= $val['hotel_name'] ?></td>

                        <td><?= $val['transporter'] ?></td>

                        <td>
                        
			<?= $val['start_port_name'] ?>
			
			(<?= $val['start_port_hall_name'] ?>)
			 - 
			<?= $val['end_port_name'] ?>

			</td>

                        <td><?= $val['nationality'] ?></td>

                        <td><?= $val['supervisor'] ?></td>
                        
                        <?php 
			$status_name='';
			$status_row = $this->flight_states->getStatus($val['fs_flight_id']);
			if(count($status_row)>0) {
				$status_name = $status_row->{name()} ;
			}
			?>
			<td class="hidden-phone error"><?= $status_name ?></td>

                    </tr>
                <? endforeach; ?>

            </tbody>
        </table>  
    </div>
<? endif;?>

<?= ($print_statement)?$print_statement:"" ?>