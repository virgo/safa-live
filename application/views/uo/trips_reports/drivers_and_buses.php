<div class="widget">
    <div class="path-container Fright">
    	<div class="path-name Fright">
            <a href="<?php echo  site_url().'uo/dashboard'; 
            
            ?>"><?php echo  lang('global_system_management') ?></a>
        </div>
        <div class="path-arrow Fright"></div>
        <div class="path-name Fright">
        <?php 
        $ref = $this->input->server('HTTP_REFERER', TRUE);
        ?>  
        
        <a href="<?php echo $ref; ?>"><?php echo  lang('previous_screen') ?></a>
            </div>
        
        <div class="path-arrow Fright"></div>
        <div class="path-name Fright">  <?= lang('drivers_and_buses') ?></div>
    </div>
            
</div>

<?php echo  form_open_multipart(false, 'id="frm_drivers_and_buses" ') ?>

<input type="hidden" name="ref" value="<?php echo $ref;?>" />
<input type="hidden" name="previous_screen" value="<?php echo  lang('previous_screen') ?>" />
        
<div class="widget" >
    <script>drivers_and_buses_counter = 0</script>
    <div class="widget-header">
    
        <div class="widget-header-icon Fright"> <span class="icos-pencil2"></span> </div>
        <div class="widget-header-title Fright"> <?php echo  lang('drivers_and_buses') ?></div>
        
        
        
        <a class="btn Fleft" title="<?php echo  lang('global_add') ?>" href="javascript:void(0)" onclick="add_drivers_and_buses('N' + drivers_and_buses_counter++); load_multiselect()">
        <?php echo  lang('global_add') ?>    
        </a>
        
        
    </div>

    <div class="widget-container" id="container_drivers_and_buses">
        <div class="table-container" style="min-height: 400px">
            <table cellpadding="0" cellspacing="0">
                <thead>
                    <tr>
                        <th><?php echo  lang('driver') ?>
                       
                        <a href="javascript:void(0);" onclick="add_driver();"  class="btn Fleft " > <span class="icon-plus"></span></a>
                        </th>
                        <th><?php echo  lang('driver_data') ?></th>
                        <th><?php echo  lang('bus') ?>
                        <a href="javascript:void(0);" onclick="add_bus();"  class="btn Fleft " > <span class="icon-plus"></span></a>
                        </th>
                        <th><?php echo  lang('bus_data') ?></th>
                        <th><?php echo  lang('actions') ?></th>
                    </tr>
                </thead>
                <tbody class="drivers_and_buses" id='drivers_and_buses'>
                <? if(isset($item_drivers_and_buses)) { ?>
                    <? if(check_array($item_drivers_and_buses)) { ?>
                    <? foreach($item_drivers_and_buses as $item_driver_and_bus) { ?>
                    <tr rel="<?php echo  $item_driver_and_bus->safa_internalsegments_drivers_and_buses_id ?>">
                        
                        <td>
                            <?php 
                            
                            echo  form_dropdown('drivers_and_buses_safa_transporters_drivers['.$item_driver_and_bus->safa_internalsegments_drivers_and_buses_id.']'
                                    , $safa_transporters_drivers, set_value('drivers_and_buses_safa_transporters_drivers['.$item_driver_and_bus->safa_internalsegments_drivers_and_buses_id.']', $item_driver_and_bus->safa_transporters_drivers_id)
                                    , 'class="chosen-select chosen-rtl input-full drivers_and_buses_safa_transporters_drivers" id="drivers_and_buses_safa_transporters_drivers_'.$item_driver_and_bus->safa_internalsegments_drivers_and_buses_id.'" tabindex="4" ') ?>
                        </td>
                        
                        <td>
                        <script>
			             $("#drivers_and_buses_safa_transporters_drivers_<?php echo $item_driver_and_bus->safa_internalsegments_drivers_and_buses_id;?>").change(function(){
			             var safa_transporters_drivers_id=$(this).val();
			             var dataString = "safa_transporters_drivers_id="+ safa_transporters_drivers_id;
			             $.ajax
			             ({
			             	type: "POST",
			             	url: "<?php echo base_url().'uo/all_movements/getDriverData'; ?>",
			             	data: dataString,
			             	cache: false,
			             	success: function(html)
			             	{
			                 	//alert(html);
			             		$("#driver_data_<?php echo $item_driver_and_bus->safa_internalsegments_drivers_and_buses_id;?>").html(html);
			             	}
			             });
			            });
			        	</script>
                            <div id="driver_data_<?php echo $item_driver_and_bus->safa_internalsegments_drivers_and_buses_id;?>">
                            <?php 
                            
                            $this->transporters_drivers_model->safa_transporters_drivers_id=$item_driver_and_bus->safa_transporters_drivers_id;
                            $driver_row=$this->transporters_drivers_model->get();
                            if(count($driver_row)>0) {
                            //echo lang(name()).':'. $driver_row->{name()};
                            echo lang('phone').':'. $driver_row->phone;
                            }
                            ?>
                            </div>
                        </td>
                       <td>
                            <?php 
                            
                            echo  form_dropdown('drivers_and_buses_safa_transporters_buses['.$item_driver_and_bus->safa_internalsegments_drivers_and_buses_id.']'
                                    , $safa_transporters_buses, set_value('drivers_and_buses_safa_transporters_buses['.$item_driver_and_bus->safa_internalsegments_drivers_and_buses_id.']', $item_driver_and_bus->safa_transporters_buses_id)
                                    , 'class="chosen-select chosen-rtl input-full drivers_and_buses_safa_transporters_buses" id="drivers_and_buses_safa_transporters_buses_'.$item_driver_and_bus->safa_internalsegments_drivers_and_buses_id.'" tabindex="4" ') ?>
                        </td>
                        <td>
                        <script>
			             $("#drivers_and_buses_safa_transporters_buses_<?php echo $item_driver_and_bus->safa_internalsegments_drivers_and_buses_id;?>").change(function(){
			             var safa_transporters_buses_id=$(this).val();
			             var dataString = "safa_transporters_buses_id="+ safa_transporters_buses_id;
			             $.ajax
			             ({
			             	type: "POST",
			             	url: "<?php echo base_url().'uo/all_movements/getBusData'; ?>",
			             	data: dataString,
			             	cache: false,
			             	success: function(html)
			             	{
			                 	//alert(html);
			             		$("#bus_data_<?php echo $item_driver_and_bus->safa_internalsegments_drivers_and_buses_id;?>").html(html);
			             	}
			             });
			            });
			        	</script>
                            <div id="bus_data_<?php echo $item_driver_and_bus->safa_internalsegments_drivers_and_buses_id;?>">
                            <?php 
                            
                            $this->transporters_buses_model->safa_transporters_buses_id=$item_driver_and_bus->safa_transporters_buses_id;
                            $this->transporters_buses_model->join =true;
                            $this->transporters_buses_model->auto_generate=false;
                            $bus_row=$this->transporters_buses_model->get();
                            
                            if(count($bus_row)>0) {
                            //echo lang('bus_no').':'. $bus_row->bus_no;
                            echo lang('safa_buses_brands_id').':'. $bus_row->buses_brands_name.'<br/>';
                            echo lang('safa_buses_models_id').':'. $bus_row->buses_models_name.'<br/>';
                            echo lang('industry_year').':'. $bus_row->industry_year.'<br/>';
                            echo lang('passengers_count').':'. $bus_row->passengers_count.'<br/>';
                            }
                            ?>
                            </div>
                        </td>
                        
                        
                        <td class="TAC">
                            <a href="javascript:void(0)" onclick="delete_drivers_and_buses(<?php echo  $item_driver_and_bus->safa_internalsegments_drivers_and_buses_id ?>, true)"><span class="icon-trash"></span></a>
                        </td>
                    </tr>
                    <? } ?>
                    <? } ?>
                    <? } ?>
                </tbody>
            </table>
        </div>


    </div>
</div>



<div class="widget TAC">
    <input type="submit" class="btn" name="smt_save" value="<?php echo lang('global_submit') ?>" style="margin:10px;padding: 5px;height: auto">
</div>


<?php echo  form_close() ?>


<script>
load_multiselect();
function load_multiselect() {
	var config = {
            '.chosen-select': {width: "100%"},
            '.chosen-select-deselect': {allow_single_deselect: true},
            '.chosen-select-no-single': {disable_search_threshold: 10},
            '.chosen-select-no-results': {no_results_text: 'Oops, nothing found!'},
            '.chosen-select-width': {width: "95%"}
        }
        for (var selector in config) {
            $(selector).chosen(config[selector]);
        }
}
function add_drivers_and_buses(id) {

	
    var new_row = [
            "<tr rel=\"" + id + "\">",
           
            "    <td>",
            "       <select name=\"drivers_and_buses_safa_transporters_drivers[" + id + "]\" class=\"chosen-select chosen-rtl input-full  validate[required] drivers_and_buses_safa_transporters_drivers\" id=\"drivers_and_buses_safa_transporters_drivers_" + id + "\" tabindex=\"4\">",
            <? foreach($safa_transporters_drivers as $key => $value): ?>
            "           <option value=\"<?= $key ?>\"><?= $value ?></option>",
            <? endforeach ?>
            "   </select>",
            "</td>",
            "    <td>",

            "<script>",
            '$("#drivers_and_buses_safa_transporters_drivers_' + id + '").change(function(){',
			'check_for_redundency($(this).val(),$(this).attr("id"));',
            'var safa_transporters_drivers_id=$(this).val();',
            'var dataString = "safa_transporters_drivers_id="+ safa_transporters_drivers_id;',
            '$.ajax',
            '({',
            '	type: "POST",',
            '	url: "<?php echo base_url().'uo/all_movements/getDriverData'; ?>",',
            '	data: dataString,',
            '	cache: false,',
            '	success: function(html)',
            '	{',
            '    	//alert(html);',
            '		$("#driver_data_' + id + '").html(html);',
            '	}',
            '});',
            "});",
        	"<\/script>",
            
            "    <div id=\"driver_data_" + id + "\"> </div> ",
            "    </td>",
            "    <td>",
            "       <select name=\"drivers_and_buses_safa_transporters_buses[" + id + "]\" class=\"chosen-select chosen-rtl input-full validate[required] drivers_and_buses_safa_transporters_buses\" id=\"drivers_and_buses_safa_transporters_buses_" + id + "\" tabindex=\"4\">",
            <? foreach($safa_transporters_buses as $key => $value): ?>
            "           <option value=\"<?= $key ?>\"><?= $value ?></option>",
            <? endforeach ?>
            "   </select>",
            "    </td>",

            "    <td>",

            "<script>",
            '$("#drivers_and_buses_safa_transporters_buses_' + id + '").change(function(){',
            'var safa_transporters_buses_id=$(this).val();',
            'var dataString = "safa_transporters_buses_id="+ safa_transporters_buses_id;',
            '$.ajax',
            '({',
            '	type: "POST",',
            '	url: "<?php echo base_url().'uo/all_movements/getBusData'; ?>",',
            '	data: dataString,',
            '	cache: false,',
            '	success: function(html)',
            '	{',
            '    	//alert(html);',
            '		$("#bus_data_' + id + '").html(html);',
            '	}',
            '});',
            "});",
        	"<\/script>",
        	
            "    <div id=\"bus_data_" + id + "\"> </div> ",
            "    </td>",
            
            "    <td class=\"TAC\">",
            "        <a href=\"javascript:void(0)\" onclick=\"delete_drivers_and_buses('" + id + "')\"><span class=\"icon-trash\"></span></a>",
            "    </td>",
            "</tr>"
            ].join("\n");

    
        
    $('#drivers_and_buses').append(new_row);
    
}
function delete_drivers_and_buses(id, database) 
{
    if(typeof database == 'undefined')
    {
        $('.drivers_and_buses').find('tr[rel="' + id + '"]').remove();
    }
    else
    {
        $('.drivers_and_buses').find('tr[rel=' + id + ']').remove();
        var hidden_input = '<input type="hidden" name="drivers_and_buses_remove[]" value="' + id + '" />';
        $('#drivers_and_buses').append(hidden_input);
    }
}

</script>

<script type="text/javascript">
$(document).ready(function() {
    var config = {
        '.chosen-select': {width: "100%"},
        '.chosen-select-deselect': {allow_single_deselect: true},
        '.chosen-select-no-single': {disable_search_threshold: 10},
        '.chosen-select-no-results': {no_results_text: 'Oops, nothing found!'},
        '.chosen-select-width': {width: "95%"}
    }
    for (var selector in config) {
        $(selector).chosen(config[selector]);
    }


    // binds form submission and fields to the validation engine
    $("#frm_drivers_and_buses").validationEngine({
            prettySelect : true,
            useSuffix: "_chosen",
            promptPosition : "topRight:-150"
            //promptPosition : "bottomLeft"
    });

 
});


</script> 

<script type="text/javascript">
function check_for_redundency(this_value, this_id) 
{

	var current_drivers_and_buses_safa_transporters_drivers = this_value;
	var current_select_input_id = this_id;

	$(".drivers_and_buses_safa_transporters_drivers").each(function() {
		if($(this).attr('id')!= current_select_input_id) {
			if(current_drivers_and_buses_safa_transporters_drivers ==$(this).val()) {	
		     	alert('<?= lang('drivers_and_buses_safa_transporters_drivers_was_inserted_before') ?>');
				
		     	$('#'+this_id).closest('tr').remove();
			}
		}
	});
	
}

</script>

<script>
function add_driver()
{
	$.fancybox({
	   'type': 'iframe', 
	   'width' : 500,
	   'height' : 300,
	   'autoDimensions' : false,
	   'autoScale' : false,
	   'href' : "<?php echo site_url('transporters/add_driver_popup/'.$safa_transporters_id); ?>/",

	   afterClose: function() {
           //location.reload();

       jQuery.ajaxSetup({async:false});
           
       var safa_transporter_id='<?php echo $safa_transporters_id?>';
       var dataString = "safa_transporter_id="+ safa_transporter_id;
       $.ajax
       ({
       	type: "POST",
       	url: "<?php echo base_url().'transporters/getBusesDriversByTransporterIdAjax'; ?>",
       	data: dataString,
       	cache: false,
       	success: function(html)
       	{
           	//alert(html);
           	var data_arr = JSON.parse(html);
               	
           	$(".drivers_and_buses_safa_transporters_drivers").html(data_arr[0]['drivers']);
       		$(".drivers_and_buses_safa_transporters_drivers").trigger("chosen:updated");

       		$(".drivers_and_buses_safa_transporters_buses").html(data_arr[0]['buses']);
       		$(".drivers_and_buses_safa_transporters_buses").trigger("chosen:updated");
       	}
       });
           
       }
    
	 });
			
}

function add_bus()
{
	$.fancybox({
	   'type': 'iframe', 
	   'width' : 500,
	   'height' : 300,
	   'autoDimensions' : false,
	   'autoScale' : false,
	   'href' : "<?php echo site_url('transporters/add_bus_popup/'.$safa_transporters_id); ?>/",

	   afterClose: function() {
           //location.reload();

       jQuery.ajaxSetup({async:false});
           
       var safa_transporter_id='<?php echo $safa_transporters_id?>';
       var dataString = "safa_transporter_id="+ safa_transporter_id;
       $.ajax
       ({
       	type: "POST",
       	url: "<?php echo base_url().'transporters/getBusesDriversByTransporterIdAjax'; ?>",
       	data: dataString,
       	cache: false,
       	success: function(html)
       	{
           	//alert(html);
           	var data_arr = JSON.parse(html);
               	
           	$(".drivers_and_buses_safa_transporters_drivers").html(data_arr[0]['drivers']);
       		$(".drivers_and_buses_safa_transporters_drivers").trigger("chosen:updated");

       		$(".drivers_and_buses_safa_transporters_buses").html(data_arr[0]['buses']);
       		$(".drivers_and_buses_safa_transporters_buses").trigger("chosen:updated");
       	}
       });
           
       }
    
	 });
			
}
</script>