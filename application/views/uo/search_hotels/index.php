<div class="span12">
    <div class="widget">
        <div class="path-container Fright">
            <div class="path-name Fright">
                <a href=""><?= lang('global_system_management') ?></a>
            </div>
            <div class="path-arrow Fright">
            </div>
            <div class="path-name Fright">
                <?= lang('hotel_data') ?>
            </div>
        </div>


    </div>
</div>


<div class="span2">
    <div class="search-widget">
        <div class="widget-header">

            <div class="widget-header-icon Fright">
                <span class="icos-pencil2"></span>
            </div>

            <div class="widget-header-title Fright">
                <?= lang('search_hotels') ?>
            </div>

        </div>



        <?= form_open('uo/search_hotels/index', 'method="get"') ?>

        <div class="widget-container">

            <div class="row-form">
                <div class="span12">
                    <div class="span12">
                        <?= form_dropdown('erp_country_id', ddgen('erp_countries', array('erp_country_id', name())), set_value('erp_country_id'), " id='erp_country_id' name='s_example' class='select input-full' style='width:100%;'") ?>
                        <?= form_error('erp_country_id', '<div class="bottom" style="color:red" >', '</div>'); ?> 
                    </div>
                </div>
            </div> 


            <div class="row-form">
                <div class="span12">
                    <div class="span12">
                        <?= form_dropdown('erp_hotellevel_id', ddgen('erp_hotellevels', array('erp_hotellevel_id', name())), set_value('erp_hotellevel_id'), "name='s_example' class='select input-full' style='width:100%;'placeholder='sdfsd'") ?>
                        <?= form_error('erp_hotellevel_id', '<div class="bottom" style="color:red" >', '</div>'); ?> 
                    </div>
                </div>
            </div>


            <div class="row-form">
                <?= form_input('name_ar', set_value("name_ar"), " placeholder='الاسم باللغة العربية'", "class='input-full'") ?>
                <?= form_error('name_ar', '<div class="bottom" style="color:red" >', '</div>'); ?>
            </div>

            <div class="row-form">
                <?= form_input('name_la', set_value("name_la"), "placeholder='الاسم باللغة الانجليزية'", 'style=width:100px') ?>
                <?= form_error('name_la', '<div class="bottom" style="color:red" >', '</div>') ?>
            </div>


            <div class="row-form TAC">
                <div class="span12">
                    <div class="toolbar bottom TAC"><input type="submit" name="search" value="<?= lang('global_search') ?>" class="btn btn-primary" /></div>
                </div>
            </div>
        </div>


        <?= form_close() ?>




    </div>
</div>
<div class="span10">
    <div class="widget">


        <? if (isset($items)): ?>
            <? foreach ($items as $item): ?>
                <div class="widget-header-icon Fright">
                    <span class="icos-pencil2"></span>
                </div>



                <div class="widget-container">


                    <div class="widget-header">

                        <div class="widget-header-icon Fright">
                            <span class="icos-pencil2"></span>
                        </div>

                        <div class="widget-header-title Fright">
                            <? if (lang('global_lang') == 'ar'): ?><a href="<?= site_url("uo/search_hotels/hotel_details") ?>/<?= $item->erp_hotel_id ?>" style="text-decoration:none"><?= $item->name_ar ?></a><? else: ?><?= $item->name_la ?><? endif ?>
                                     <? if ($item->erp_hotellevel_id == '1'): ?>
                                            <img src="<?= IMAGES ?>/star.png" style="width:15px;height: 11px"/>
                                        <? endif ?>

                                        <? if ($item->erp_hotellevel_id == '2'): ?>
                                            <img src="<?= IMAGES ?>/star.png" style="width:15px;height: 11px"/><img src="<?= IMAGES ?>/star.png" style="width:15px;height: 11px"/>
                                        <? endif ?>

                                        <? if ($item->erp_hotellevel_id == '3'): ?>
                                            <img src="<?= IMAGES ?>/star.png" style="width:15px;height: 11px"/><img src="<?= IMAGES ?>/star.png" style="width:15px;height: 11px"/><img src="<?= IMAGES ?>/star.png" style="width:15px;height: 11px"/>
                                        <? endif ?> 

                                        <? if ($item->erp_hotellevel_id == '4'): ?>
                                            <img src="<?= IMAGES ?>/star.png" style="width:15px;height: 11px"/><img src="<?= IMAGES ?>/star.png" style="width:15px;height: 11px"/><img src="<?= IMAGES ?>/star.png" style="width:15px;height: 11px"/><img src="<?= IMAGES ?>/star.png" style="width:15px;height: 11px"/>
                                        <? endif ?>   

                                        <? if ($item->erp_hotellevel_id == '5'): ?>
                                            <img src="<?= IMAGES ?>/star.png" style="width:15px;height: 11px"/><img src="<?= IMAGES ?>/star.png" style="width:15px;height: 11px"/><img src="<?= IMAGES ?>/star.png" style="width:15px;height: 11px"/><img src="<?= IMAGES ?>/star.png" style="width:15px;height: 11px"/><img src="<?= IMAGES ?>/star.png" style="width:15px;height: 11px"/>
                                        <? endif ?>   

                                        <? if ($item->erp_hotellevel_id == '6'): ?>
                                            <img src="<?= IMAGES ?>/star.png" style="width:15px;height: 11px"/><img src="<?= IMAGES ?>/star.png" style="width:15px;height: 11px"/><img src="<?= IMAGES ?>/star.png" style="width:15px;height: 11px"/><img src="<?= IMAGES ?>/star.png" style="width:15px;height: 11px"/><img src="<?= IMAGES ?>/star.png" style="width:15px;height: 11px"/><img src="<?= IMAGES ?>/star.png" style="width:15px;height: 11px"/>
                                        <? endif ?> 

                                        <? if ($item->erp_hotellevel_id == '7'): ?>
                                            <img src="<?= IMAGES ?>/star.png" style="width:15px;height: 11px"/><img src="<?= IMAGES ?>/star.png" style="width:15px;height: 11px"/><img src="<?= IMAGES ?>/star.png" style="width:15px;height: 11px"/><img src="<?= IMAGES ?>/star.png" style="width:15px;height: 11px"/><img src="<?= IMAGES ?>/star.png" style="width:15px;height: 11px"/><img src="<?= IMAGES ?>/star.png" style="width:15px;height: 11px"/><img src="<?= IMAGES ?>/star.png" style="width:15px;height: 11px"/>
                                        <? endif ?>    


                                        <? if ($item->sub_level_id): ?>
                                            (<? if (lang('global_lang') == 'ar'): ?><?= $item->hotel_sublevel_name_ar ?><? else: ?><?= $item->hotel_sublevel_name_la ?><? endif ?>)
                                        <? else: ?>
                                            (<?= lang('no_sub_level') ?>)
                                        <? endif ?>
                        
                        
                        </div>

                    </div>

                    <div class="inner-widget" >
                        <div class="span3" style='width:200px;height:150px'>
                              <? if ($item->hotel_photospath): ?>
                                <a href="<?= site_url("uo/search_hotels/hotel_details") ?>/<?= $item->erp_hotel_id ?>"><img src="<?= base_url('/static/hotel_album/' . $item->hotel_photospath) ?> " style="width:195px;height: 127px;margin:0 auto;vertical-align: middle"/></a>
                            <? else: ?>
                                <img src="<?= IMAGES ?>/noimg.gif"/>
                            <? endif ?>
                        </div>

                        <div class="span9 table-responsive">
                            <table cellpadding="0" class="myTable" cellspacing="0">
                                <tr>
                                <td>                       
                                    <div class="path-container Fright">
                                        <div class="path-name Fright">
                                            <? if ($item->country_id): ?>
                                                <? if (lang('global_lang') == 'ar'): ?><?= $item->country_name_ar ?><? else: ?><?= $item->country_name_la ?><? endif ?>
                                            <? else: ?>
                                                <?= lang('country_unknown') ?>
                                            <? endif ?>
                                        </div>
                                        <div class="path-arrow Fright">
                                        </div>
                                        <div class="path-name Fright">
                                            <? if (lang('global_lang') == 'ar'): ?><?= $item->city_name_ar ?><? else: ?><?= $item->city_name_la ?><? endif ?>
                                            (<a class="various fancybox.iframe" href="<?= site_url('uo/search_hotels/map') ?>?lat=<?= $item->googel_map_latitude ?>&amp;long=<?= $item->googel_map_langtitude ?>"><?= lang('map') ?></a>)
                                        </div>
                                    </div>
                                </td>



                            </table>

                            <div class="span11 inner-widget">
                                <? if ($item->remark): ?>
                                    <span style="padding: 15px;line-height: 25px;padding-right:10px"><?= $item->remark ?></span>
                                <? else: ?>
                                    <span style="padding: 15px;"><?= lang('no_remark') ?></span>
                                <? endif; ?>
                            </div>

                            <span class="" style="float:left;"><a href="" style="color:black;"><?= lang('build_your_hotel') ?></a></span>
                        </div>
                    </div>

                </div>

            <? endforeach; ?>

        <? endif; ?>


    </div>

</div>
<?= $pagination ?>



<script type="text/javascript">
    $(document).ready(function() {
        $(".various").fancybox();
    });
</script>


