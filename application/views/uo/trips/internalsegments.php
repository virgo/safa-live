<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <style>
            .yellow{
                /*background-color:#fffc00;*/
            }
            span{
                font-weight:bold;
                font-size:15px;
            }
            .tb_cen td{
                font-weight:bold;
                font-size:15px;
                text-align:center;
            }
            .tb_cen th{
                font-weight:bold;
                font-size:20px;
                text-align:center;
            }
        </style>
        
        <script src="<?= NEW_CSS_JS .'_'. lang('DIR') ?>/jquery-1.9.1.js"></script>
        <script src="<?= NEW_CSS_JS .'_'. lang('DIR') ?>/jquery-ui.js"></script>
        
    </head>
    <body><div style="margin: 0 auto;display: table;">
        <table align="right" cellpadding="0"  cellspacing="0" border="0" width="630px" dir="rtl">
            <tr>
                <td colspan="2">
                    <img src="<?php echo site_url(); ?>static/img/uo_companies_logos/<?php echo $uo_dir; ?>/logo.png" style="max-width:660px;max-height: 91px;" />
                </td>
            </tr>
            <tr><td><br /></td></tr>
            <tr>
                <td colspan="2">
                    <span class="yellow"><?= lang('confirmation_number') ?>: </span>&nbsp;<span> <?php
                        if (isset($internalsegments[0])) {
                        echo $safa_uo_code.'-'.$internalsegments[0]->confirmation_number;
                        }
                        ?> </span><br />
                    <span class="yellow"><?= lang('the_date') ?>:   </span>&nbsp;<span> <?php
                        if (isset($internalsegments[0])) {
                        echo $internalsegments[0]->datetime;
                        }
                        ?> </span><br />
                    <span class="yellow"><?= lang('fax') ?>:  </span>&nbsp;<span> <?php
                        if (isset($internalsegments[0])) {
                        echo $internalsegments[0]->uo_fax;
                        }
                        ?> </span><br />
                </td>
            </tr>
            <tr><td><br /></td></tr>
            <tr>
                <td><span class="yellow">السادة/ </span>&nbsp;<span> <?php
                        if (isset($internalsegments[0])) {
                        echo $internalsegments[0]->safa_ito_name;
                        }
                        ?></span><br /></td>
                <td><span> المحترمين </span></td>
            </tr>
            <tr>
                <td><span class="yellow"> عناية الأستاذ/  </span>&nbsp;<span> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; </span><br /></td>
                <td><span> المحترم </span></td>
            </tr>
            <tr><td><br /></td></tr>
            <tr>
                <td colspan="2">
                    <span style="padding-right:260px;"><span class="yellow">تحية طيبة وبعد ،،،،</span></span>
                    <br />
                    <span class="yellow"> يرجى التكرم بتأكيد حجز الخدمة التالية :  عدد </span> &nbsp;
                    <?php foreach ($buses as $bus) :?>
                    <span><?php echo $bus->bus_count ?></span> &nbsp;
                    <span class="yellow"> باص </span> &nbsp;
                    <span> <?php echo $bus->safa_buses_brand_name ?>  </span> &nbsp;
                    <span class="yellow"> موديل </span> &nbsp;
                    <span>  <?php echo $bus->safa_buses_model_name ?>  </span> &nbsp;
                    <span> ) </span> &nbsp;
                    <span class="yellow"> لعدد </span> &nbsp;
                    <span> <?php echo $bus->seats_count ?> </span> &nbsp;
                    <span class="yellow"> راكب </span> &nbsp;
                    <span> ( .</span><br />
                    <?php endforeach ?>
                    
                    
                    <?php 
                    $safa_buses_brand_names='';
                    if (isset($arriving_segment_bus_brands)) { 
					     		if (check_array($arriving_segment_bus_brands)) {
					     			 
					     			$arriving_segment_bus_brands_counter=0;
			                  		foreach ($arriving_segment_bus_brands as $arriving_segment_bus_brand) {
			                  			$safa_buses_brand_names = $safa_buses_brand_names.'('.$arriving_segment_bus_brand->bus_count.')  '.$arriving_segment_bus_brand->safa_buses_brand_name.' - '.$arriving_segment_bus_brand->safa_buses_model_name.' <br/> ';
			                  			
			                  			$arriving_segment_bus_brands_arr = array(
										'safa_transporters_buses_id'=>$arriving_segment_bus_brands[$arriving_segment_bus_brands_counter]->safa_buses_models_id, 
										'seats_count'=>$arriving_segment_bus_brands[$arriving_segment_bus_brands_counter]->seats_count
										);
			                  		}
					     		}
                            } 
                    ?>
                    
                </td>
            </tr>
            <tr><td><br /></td></tr>
            <tr>
                <td colspan="2" >
                    <table class="tb_cen" cellpadding="0" cellspacing="0" border="1" width="100%" id="tbl_internal_segments">
                        <thead>
                        <tr>
                            <th rowspan="2">م</th>
                            <th rowspan="2"><?php echo lang('internalpassage_type') ?></th>
                            <th rowspan="2">التاريخ</th>
                            <th colspan="2">الانتقال</th>
                            <th rowspan="2">الحافلات</th>
                            <th rowspan="2">رقم الرحلة</th>
                            <th rowspan="2">الساعة</th>
                            </tr>
                        
                        <tr>
                            <td >من</td>
                            <td >إلى</td>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $departure_start_date = '';
                        $departure_start_time = '';

                        if (ensure($internalsegments)) {
                        $serial = 0;


						$segment_bus_brands_different=false;
						
                        foreach ($internalsegments as $internalsegment) {
                        $serial++;
                        ?>
                                <?php
                                $start_datetime_arr = explode(' ', $internalsegment->start_datetime);
                                $end_datetime_arr = explode(' ', $internalsegment->end_datetime);

                                ?>
                        <tr>
                            <td ><?= $serial ?></td>
                            <td> <?php echo item("safa_internalsegmenttypes", name(), array("safa_internalsegmenttype_id" => $internalsegment->safa_internalsegmenttype_id)); ?></td>
                            <td ><?php
                                if (isset($start_datetime_arr[0])) {
                                if($start_datetime_arr[0]!='0000-00-00') {
                                echo get_date($start_datetime_arr[0]);
                                }
                                }
                                ?></td>
                            <td ><?= str_replace('فندق', '', $this->trip_details_model->get_starting_point($internalsegment)) ?> 

                                <br/> 

                                <?php
                                if($internalsegment->safa_internalsegmenttype_id == '1') {
                                if($internalsegment->erp_port_halls_to_name!=''){echo '('.$internalsegment->erp_port_halls_to_name.')';
                                }
                                }
                                ?> </td>
                            <td ><?= str_replace('فندق', '', $this->trip_details_model->get_ending_point($internalsegment)) ?> 
                                <br/> 

                                <?php
                                if($internalsegment->safa_internalsegmenttype_id == '2') {
                                if($internalsegment->erp_port_halls_from_name!=''){echo '('.$internalsegment->erp_port_halls_from_name.')';
                                }
                                }
                                ?>  </td>
                            
                            
                            <td>
                            <?php 
                            
                            
                            $segment_bus_brands = $this->internalsegments_drivers_and_buses_model->get_distinct_bus_brands_by_safa_internalsegment($internalsegment->safa_internalsegment_id);
                            $segment_bus_brands_arr = array();
                            $segment_bus_brands_counter=0;
                            
                            
                            
		                            $safa_buses_brand_names='';
		                            if (isset($segment_bus_brands)) { 
							     		if (check_array($segment_bus_brands)) {
					                  		foreach ($segment_bus_brands as $segment_bus_brand) {
					                  			$safa_buses_brand_names = $safa_buses_brand_names.'('.$segment_bus_brand->bus_count.')  '.$segment_bus_brand->safa_buses_brand_name.' - '.$segment_bus_brand->safa_buses_model_name.' <br/> ';
					                  			
					                  			$segment_bus_brands_arr = array(
												'safa_transporters_buses_id'=>$segment_bus_brands[$segment_bus_brands_counter]->safa_buses_models_id, 
												'seats_count'=>$segment_bus_brands[$segment_bus_brands_counter]->seats_count
												);
					                  		}
							     		}
		                            } 

					                    if(isset($arriving_segment_bus_brands_arr)) {
				                          	if($arriving_segment_bus_brands_arr!=$segment_bus_brands_arr) {
					                            echo $safa_buses_brand_names;
					                            $segment_bus_brands_different=true;
				                            }
			                            }
		                            
                            ?>
                            </td>
                            
                            <td ><?php
                                if ($internalsegment->flight_number != '') {
                                $flight_number_arr = explode('---', $internalsegment->flight_number);
                                echo $flight_number_arr[1] . ' <br/>' . $flight_number_arr[0];
                                }
                                ?> &nbsp;</td>
                                
                                
                            <td >
                        <?php
                        if (isset($start_datetime_arr[1])) {

                        echo get_time($internalsegment->start_datetime);
                        }
                        ?> &nbsp;</td>
                        </tr>


                    <?php
                    }
                    }
                    ?>    
					</tbody>
                    </table>
                    
                    
                    <?php 
                    if(isset($segment_bus_brands_different)) {
                    if(!$segment_bus_brands_different) {?>
                    <!-- 
                    <script type="text/javascript">
                    $("#tbl_internal_segments tbody tr").each(function() {
                        $(this).find("td:eq(5)").remove();
                    });
                    $("#tbl_internal_segments thead tr").each(function() {
                        $(this).find("th:eq(4)").remove();
                    });
                    </script>
                     -->
                    <?php 
                    }
                    }
                    ?>
                    
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <?php
                    $departure_start_date = '';
                    $departure_start_time = '';

                    if (isset($internalsegments[0])) {
                        $departure_start_date = $internalsegments[0]->departure_datetime;
                        $departure_start_time = get_time($internalsegments[0]->departure_datetime);
                    }

                    if (isset($internalsegments[0])) {
                        $departure_start_date_arr = explode(' ', $internalsegments[0]->departure_datetime);
                        $departure_start_date = $departure_start_date_arr[0];
                    }
                    ?>      
                    <br />
                    &nbsp;
                    <span class="yellow"> الحجز باسم</span>
                    <span>/  <?php if (isset($internalsegments[0])) {echo $internalsegments[0]->safa_uo_contract_name;} ?> –  </span>
                    <span class="yellow">
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; مشرف المجموعة /
                    </span>
                    <span> <?php if (isset($internalsegments[0])) { echo $internalsegments[0]->trip_supervisors; } ?>   <?php if (isset($internalsegments[0])) { echo $internalsegments[0]->trip_supervisors_phone; } ?></span> <br /><br />
                    <span class="yellow">ملاحظة : اقلاع الطائرة الساعة </span>
                    <span><?= $departure_start_time ?></span>&nbsp;&nbsp;&nbsp;
                    <span class="yellow">تاريخ</span>
                    <span><?= $departure_start_date ?></span>   <br /><br />
                    <span class="yellow"> <?php if (isset($internalsegments[0])) { echo $internalsegments[0]->status_notes; }?> </span>
                    
        </td>
    </tr>
    <tr>
        <td align="left"  colspan="2">
            <br /><br />
            <span class="yellow">مع فائق شكري وتقديري</span>
            <br />
            <span style="padding-right:500px;"> </span>
            <br /><br />
            <span style="padding-right:560px;"><span class="yellow">الحجز و المبيعات</span></span>
            <span></span>
        </td>
    </tr>
    <tr><td><br /><br /><br /><br /><br /></td></tr>
    <tr>
        <td colspan="2">
            <table border="0" width="100%">
                <tr>

                     
                    <td >
                    <?php echo file_get_contents('static/img/uo_companies_logos/'. $uo_dir.'/footer.html'); ?>
                	</td>
                </tr>
            </table>
        </td>
    </tr>
</table></div>
</body>
</html>