

<div class="widget">
<div class="path-container Fright">
<div class="path-name Fright"><a
	href="<?php echo site_url() . 'uo/dashboard'; ?>"><?php echo lang('global_system_management') ?></a>
</div>
<div class="path-arrow Fright"></div>
<div class="path-name Fright"><?= lang('trips') ?></div>
</div>



</div>

<div class="row-fluid">
<div class="row-fluid">

<div class="widget">


<div class="widget-header">
<div class="widget-header-icon Fright"><span class="icos-pencil2"></span>
</div>
<div class="widget-header-title Fright"><?php echo lang('trips') ?></div>
<!--<a title='<?= lang('global_add') ?>'href="<?= site_url('uo/trips/manage') ?>" class="btn Fleft">
                    <?= lang('global_add') ?>
                </a>
            --></div>



<form action="<?php echo base_url() . "uo/trips/index"; ?>"
	id="trips_search" name="trips_search" method="GET"><?= form_open("","method='get'") ?>

<div class="widget-container"><? if(validation_errors()){ ?> <?php echo validation_errors(); ?>
                    <? } ?>

<div class="row-form">


<div class="span6">
<div class="span4 TAL Pleft10"><?php echo  lang('name') ?></div>
<div class="span8"><?php echo form_dropdown('safa_trip_id', $safa_trips,  set_value('safa_trip_id'), "id='safa_trip_id' class=' chosen-select' ") ?>
</div>
</div>


<div class="span6">
<div class="span4 TAL Pleft10"><?php echo  lang('the_date') ?></div>
<div class="span4"><?php echo form_input('date_from', set_value("date_from")," style='' id='date_from' class=' input-full date' ") ?>
</div>
<div class="span4"><?php echo form_input('date_to', set_value("date_to")," style='' id='date_to' class=' input-full date' ") ?>
</div>

</div>

</div>

<div class="row-form">
<div class="span6">
<div class="span4 TAL Pleft10"><?php echo  lang('arrival_date') ?></div>
<div class="span8"><?php echo form_input('arrival_date', set_value("arrival_date")," style='' id='arrival_date' class=' input-full date'") ?>
</div>
</div>

<!--<div class="span6">
<div class="span4 TAL Pleft10"><?php echo  lang('moushrif_name') ?> </div>
<div class="span8"><?php echo form_multiselect('safa_ea_supervisor_ids[]', $supervisors,set_value('safa_ea_supervisor_ids'), "id='safa_ea_supervisor_ids'  class=' chosen-rtl chosen-select'") ?>
</div>
</div>

-->
<div class="span6">

<div class="span4 TAL Pleft10"><?php echo  lang('erp_country_id') ?></div>
<div class="span8"><?php echo form_dropdown('erp_country_id', ddgen('erp_countries', array('erp_country_id', name())),  set_value('erp_country_id'), "id='erp_country_id' class=' chosen-select' ") ?>
</div>
</div>


</div>

<div class="row-form">
<div class="span6">
<div class="span4 TAL Pleft10"><?php echo  lang('safa_tripstatus_id') ?>
</div>
<div class="span8"><?php echo form_dropdown('safa_tripstatus_id', $safa_tripstatus,  set_value('safa_tripstatus_id'), "id='safa_tripstatus_id' class=' chosen-select' ") ?>
</div>
</div>
<div class="span6">
<div class="span4 TAL Pleft10"><?php echo  lang('safa_trip_confirm_id') ?>
</div>
<div class="span8"><?php echo form_dropdown('safa_trip_confirm_id', $safa_trip_confirm,  set_value('safa_trip_confirm_id'), "id='safa_trip_confirm_id' class=' chosen-select' ") ?>
</div>
</div>

</div>



</div>

<div class=" TAC"><input type="submit" class="btn" name="search"
	value="<?php echo lang('global_search') ?>"></div>

</form>



<div class="block-fluid">

<table class="fsTable" cellpadding="0" cellspacing="0" width="100%">
	<thead>
		<tr>
			<th rowspan="2"><?= lang('trip') ?></th>
			<th rowspan="2"><?= lang('safa_uo_contract_name') ?></th>
			<th rowspan="2"><?= lang('packages') ?></th>
			<th rowspan="2"><?= lang('internal_transportation') ?></th>
			<th colspan="3"><?= lang('external_transportation') ?></th>
			<th rowspan="2"><?= lang('mofa') ?></th>
			<th rowspan="2"><?= lang('visa_order_issued') ?></th>
			<th rowspan="2"><?= lang('visa_number') ?></th>
			<th rowspan="2"><?= lang('global_actions')?></th>
		</tr>
		<tr>
			<th><?= lang('flights') ?></th>
			<th><?= lang('buses') ?></th>
			<th style="border-left: 1px solid #DDDDDD;"><?= lang('ships') ?></th>
		</tr>
	</thead>
	<tbody>
	<?
	if(isset($items)) {
		if(ensure($items)) {
			$items_arr=$items;
		} else {
			if(count($items)>0) {
				$items_arr[]=$items;
			} else {
				$items_arr=array();
			}
		}

		foreach ($items_arr as $item) {
			$safa_trip_id = $item->safa_trip_id;
			$safa_trip_name = $item->name;
			$flights_icon = '';
			$buses_icon = '';
			$ships_icon = '';
			$erp_transportertype_id = $item->erp_transportertype_id;
			if ($erp_transportertype_id == 1) {
				$buses_icon = "<span class='icon-ok'>";
			} else if ($erp_transportertype_id == 2) {
				$flights_icon = "<span class='icon-ok'>";
			} else if ($erp_transportertype_id == 3) {
				$ships_icon = "<span class='icon-ok'>";
			}

			$safa_trip_name=$item->name;

			//$safa_uo_contract_name = $item->safa_uo_contract_name;
			$safa_uo_contract_name ='';
			
			$flights_icon='';
			$buses_icon='';
			$ships_icon='';
			$erp_transportertype_id = $item->erp_transportertype_id;
			if($erp_transportertype_id==1) {
				$buses_icon = "<span class='icon-ok'>";
			} else if($erp_transportertype_id==2) {
				$flights_icon = "<span class='icon-ok'>";
			} else if($erp_transportertype_id==3) {
				$ships_icon = "<span class='icon-ok'>";
			}

			$individuals_count=$item->individuals_count;
			$mofa_count=$item->mofa_count;
			$visa_order_count=$item->visa_order_count;
			$visa_number_count=$item->visa_number_count;

			$trip_safa_packages='';
			$this->safa_trips_model->safa_trip_id = $safa_trip_id;
			$trip_safa_packages_rows = $this->safa_trips_model->get_safa_packages();
			foreach($trip_safa_packages_rows as $trip_safa_packages_row) {
				$trip_safa_packages = $trip_safa_packages."<a href='".site_url("safa_packages/view/$trip_safa_packages_row->safa_package_id")."' target='_blank' >".$trip_safa_packages_row->safa_package_name.'</a> - ('.$trip_safa_packages_row->erp_package_period_name.')<br/>';
			}

			$trip_internaltrip_id=$item->trip_internaltrip_id;


			$mofa_ico='';
			$visa_order_ico='';
			$visa_number_ico='';

			$mofa_text='';
			$visa_order_text='';
			$visa_number_text='';

			if($individuals_count>0) {
				$mofa_text="($individuals_count/$mofa_count)";
				if($individuals_count==$mofa_count) {
					$mofa_ico = "<span class='icon-ok'>";
				} else {
					$mofa_ico = "<span class='icon-remove'>";
				}
			}

			if($individuals_count>0) {
				$visa_order_text="($individuals_count/$visa_order_count)";
				if($individuals_count==$visa_order_count) {
					$visa_order_ico = "<span class='icon-ok'>";
				} else {
					$visa_order_ico = "<span class='icon-remove'>";
				}
			}

			if($individuals_count>0) {
				$visa_number_text="($individuals_count/$visa_number_count)";
				if($individuals_count==$visa_number_count) {
					$visa_number_ico = "<span class='icon-ok'>";
				} else {
					$visa_number_ico = "<span class='icon-remove'>";
				}
			}
			?>
		<tr>
			<td class=" warning"><?php echo $safa_trip_name; ?></td>
			<td class=" success"><?php echo $safa_uo_contract_name; ?></td>
			
			<td class="TAC info"><?php echo $trip_safa_packages; ?></td>

			<!-- <td></td> -->
			<td class="TAC"><!--                        <?php echo "<a href='".site_url("uo/trip_internaltrip/edit/$trip_internaltrip_id")."' target='_blank' >".$trip_internaltrip_id.'</a>'; ?>-->
			<?php echo $trip_internaltrip_id; ?></td>

			<td class="TAC success"><?php echo $flights_icon; ?></td>
			<td class="TAC info"><?php echo $buses_icon; ?></td>
			<td class="TAC error"><?php echo $ships_icon; ?></td>

			<td class="TAC success"><?php echo "$mofa_text"; ?></td>
			<td class="TAC info"><?php echo "$visa_order_text"; ?></td>
			<td class="TAC warning"><?php echo "$visa_number_text"; ?></td>
                        <td style="width:20px">
                            <div style="position: relative; display:block; width: 50%" >
                                <a href='<?php echo site_url("trip_details/details/$safa_trip_id"); ?>' target="_blank"><span class='icon-search'></span></a> <!--                        <a href='<?php echo site_url("uo/trips/passports/$safa_trip_id"); ?>' target="_blank" ><span class='icon-user'></span></a>-->
                                <a class="dropdown-toggle" data-toggle="dropdown" href="#"><span class="icon-folder-close"></span></a>
                                <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
                                        <li><a href='<?php echo site_url("uo/trips/passports/$safa_trip_id"); ?>' target="_blank"><?php echo lang('passports_accomedation_report');?></a>
                                        </li>

                                        <li><a href='<?php echo site_url("uo/trips/internalsegments/$safa_trip_id"); ?>' target="_blank"><?php echo lang('internalsegments_report');?></a>
                                        </li>
                                        <li><a href='<?php echo site_url("trip_details/trip_hotel_report/$safa_trip_id"); ?>' target="_blank"><?php echo lang('reservation_report');?></a>
                                        </li>
                                        <li><a href='<?php echo site_url("trip_details/trip_packages_rooms/$safa_trip_id"); ?>' target="_blank"><?php echo lang('trip_programs_report');?></a>
                                        </li>
                                        <li><a href='<?php echo site_url("trip_details/trip_voucher/$safa_trip_id"); ?>' target="_blank"><?php echo lang('trip_voucher');?></a>
                                        </li>
                                </ul>
                                
                                
                               
                            </div>
                            
                            <div style="position: relative; display:block;  width: 50%">
                                
                                
                                <a class="dropdown-toggle_accomedation" data-toggle="dropdown" href="#"><span class="icon-user"></span></a>
                                <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
                                        <?php 
                                        foreach($safa_reservation_forms_rows as $safa_reservation_forms_row) {
                                        	if($safa_reservation_forms_row->safa_trip_id == $safa_trip_id) {
                                        		$safa_reservation_form_id = $safa_reservation_forms_row->safa_reservation_form_id;
                                        		$accommodation_form_text = $safa_reservation_forms_row->safa_trip_name .' - '. $safa_reservation_forms_row->erp_package_period_name;
		                                        ?>
		                                        <li><a href='<?php echo site_url("ea/passport_accommodation_rooms/manage/$safa_reservation_form_id"); ?>' target="_blank"><?php echo $accommodation_form_text;?></a></li>
												<?php 
                                        	}
                                        }
										?>
                                </ul>
                                
                                
                            </div>
			</td>
		</tr>
		<? }
	}
	?>
	</tbody>
</table>

</div>
</div>

</div>
</div>
<div id='inset_form'
	style="display: none;"></div>
<!--	<?php echo $pagination; ?>-->

<script>
    $(document).ready(function() {
        $('.fancybox').fancybox({
            afterClose: function() {
                parent.location.reload(true);
            }
        });

        $('#add_accommodation').click(function() {
            var href = $(this).attr('href');
            var tripid = $(this).attr('rel');
            $('#inset_form').html('<form action="' + href + '" name="accommodation" method="post" style="display:none;"><input type="text" name="safa_trip_id" value="' + tripid + '" /></form>');

            document.forms['accommodation'].submit();

            return false;
        });
    });
</script>

<script>
    if ($(".fsTable").length > 0) {

        //By Gouda, TO remove the warning message.
        //$.fn.dataTableExt.sErrMode = 'throw';

        $(".fsTable").dataTable({
            bSort: true,
            bAutoWidth: true,
            "iDisplayLength":<?php
$ea_user_id = session('ea_user_id');
if ($this->input->cookie('' . $ea_user_id . '_' . 'trips_rows_count_per_page' . '')) {
    echo $this->input->cookie('' . $ea_user_id . '_' . 'trips_rows_count_per_page' . '');
} else {
    echo 20;
}
?>,
            "aLengthMenu": [5, 10, 20, 50, 100, 200, 500], // can be removed for basic 10 items per page
            "bFilter": true,
            "sPaginationType": "full_numbers",
            "aoColumnDefs": [{"bSortable": false,
                    "aTargets": [-1]}]

        });
    }



</script>


<script type="text/javascript">
$(document).ready(function() {
    var config = {
        '.chosen-select': {width: "100%"},
        '.chosen-select-deselect': {allow_single_deselect: true},
        '.chosen-select-no-single': {disable_search_threshold: 10},
        '.chosen-select-no-results': {no_results_text: 'Oops, nothing found!'},
        '.chosen-select-width': {width: "95%"}
    }
    for (var selector in config) {
        $(selector).chosen(config[selector]);
    }


    // binds form submission and fields to the validation engine
    $("#trips_search").validationEngine({
            prettySelect : true,
            useSuffix: "_chosen",
            promptPosition : "topRight:-150"
            //promptPosition : "bottomLeft"
    });


    $('.date').datepicker({dateFormat: "yy-mm-dd"});
    
});

</script>

<script>
$('.dropdown-toggle').dropdown();

$('.dropdown-toggle_accomedation').dropdown();
</script>
