<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); ?>
<!-- the script for datepicker --->
<script type="text/javascript" src='<?= JS ?>/plugins/bootstrap/bootstrap.min.js'></script>
<link href="<?= CSS_JS ?>/dateandtime/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src='<?= CSS_JS ?>/dateandtime/bootstrap-datetimepicker.min.js'></script>
<!-- the script for datepicker --->

<!-- By Gouda, For File Input -->
<script type="text/javascript" src='<?= JS ?>/plugins/cookies/jquery.cookies.2.2.0.min.js'></script>
<script type="text/javascript" src='<?= JS ?>/plugins/hoverintent/jquery.hoverIntent.minified.js'></script>    
<script type="text/javascript" src='<?= JS ?>/actions.js'></script>
    
<style>
    body{overflow: hidden;}
    input {margin-bottom : 4px !important;}</style>
<div class="row-fluid">
    
    <div class="widget">
    <div class="path-container Fright">
        <div class="icon"><i class="icos-pencil2"></i></div> 
         <div class="path-name Fright">
               <a href="<?= site_url('uo/dashboard') ?>"><?= lang('parent_node_title') ?></a>
        </div>    
        <div class="path-arrow Fright">
        </div>
            <div class="path-name Fright">
                <a href="<?= site_url('uo/trip_internaltrip/index') ?>"><?= lang('node_title') ?></a>
        	</div>
        	<div class="path-arrow Fright">
	        </div>
	        <div class="path-name Fright">
                <span><?= lang('add_transport_request') ?></span>
        	</div>
    </div>     
    </div>
    
    
    <div class="widget">
       

        
        <div class="widget-header">
	        <div class="widget-header-icon Fright">
	            <span class="icos-pencil2"></span>
	        </div>
	
	        <div class="widget-header-title Fright">
	           <?= lang('add_transport_request') ?>
	        </div>
		</div>
		
		
        
        <?= form_open_multipart(false, 'id="frm_trip_internaltrip" ') ?>
        <div class="block-fluid">
        
        <div class="row-form" >
                <div class="span6" >
                    <div class="span4"><?= lang("trip_code") ?>
                        <font style='color:red'>*</font>   
                    </div>
                    <div class="span8">
                        <!--<?= form_input("serial", set_value("serial"), "class='validate[required]'") ?>
                        <span class="bottom" style='color:red' >
                            <?= form_error('serial') ?> 
                        </span>
                        -->
                        <?= form_input("safa_uo_code", set_value("safa_uo_code", $safa_uo_code), "class='validate[required]' readonly='readonly' ") ?>
                        <?= form_input("serial", set_value("serial", $safa_uo_serial), "class='validate[required]'") ?>
                        <span class="bottom" style='color:red' >
                            <?= form_error('serial') ?> 
                        </span>
                        
                    </div>
                </div>
                <div class="span6">
                    <div class="span4">
                        <?= lang("buses_count") ?>
                        <font style='color:red'>*</font>
                    </div>
                    <div class="span8" >
                        <?= form_input("buses_count", set_value("buses_count"), "class='validate[required]'") ?>
                        <span class="bottom" style='color:red' >
                            <?= form_error('buses_count') ?> 
                        </span>
                    </div>
                </div>
            </div>
        
            <div class="row-form" >
                <div class="span6" >
                    <div class="span4">
                        <?= lang("transport_request_date") ?>
                        <font style='color:red'>*</font>
                    </div>
                    <div class="span8"> 
                        <div >
                            <input class="datetime validate[required]" data-format="yyyy-MM-dd" name="datetime" id="date" type="text" value="<?=set_value('datetime', date('Y-m-d', time()) )?>" ></input>
                            
                            <script>
	                        $('.datetime').datepicker({
	                            dateFormat: "yy-mm-dd",
	                            controlType: 'select',
	                            timeFormat: 'HH:mm'
	                        });
	                    	</script>
                            
                           
                            <!-- 
                            <span class="add-on">
                                <i data-time-icon="icon-time" data-date-icon="icon-calendar">
                                </i>
                            </span>
                             -->
                        </div>
                        <span class="bottom" style='color:red' >
                            <?= form_error('datetime') ?> 
                        </span>
                    </div>


                </div>
                
                <div class="span6" >
                    <div class="span4"><?= lang("transport_request_contract") ?>
                        <font style='color:red'>*</font>
                    </div>
                    <div class="span8">
                        <?= form_dropdown("safa_uo_contract_id", $safa_uo_contracts, set_value("safa_uo_contract_id"), " id='safa_uo_contract_id' class='validate[required] input-huge '") ?>
                        <span class="bottom" style='color:red' >
                            <?= form_error('safa_uo_contract_id') ?> 
                        </span>
                    </div>
                </div>
                
                
            </div>
            <div class="row-form" >
            
            <!--<div class="span6" >
                    <div class="span4"><?= lang("uo_ea_id") ?>
                        <font style='color:red'>*</font>
                    </div>
                    <div class="span8">
                        <?= form_dropdown("uo_contract_id", $uo_contracts, set_value("uo_contract_id"), " id='contract_id' class='input-huge'") ?>
                        <span class="bottom" style='color:red' >
                            <?= form_error('uo_contract_id') ?> 
                        </span>
                        
                        
                        <?= form_dropdown("uo_ea_id", $uo_eas, set_value("uo_ea_id"), " id='uo_ea_id' class='input-huge'") ?>
                        <span class="bottom" style='color:red' >
                            <?= form_error('uo_ea_id') ?> 
                        </span>
                    </div>
                </div>
                
                --><div class="span6" >
                    <div class="span4">
                        <?= lang("transport_request_trip_id") ?>
                        
                    </div>
                    <div class="span8">
                        <?= form_dropdown("safa_trip_id", array("" => lang('global_select_from_menu')), set_value("safa_trip_id"), "id='trip_id' class='input-huge'") ?>
                        <span class="bottom" style='color:red' >
                            <?= form_error('safa_trip_id') ?> 
                        </span>
                    </div>
                </div>
               
            </div>
            <div class="row-form" >
                <div class="span6" >
                    <div class="span4"><?= lang("transport_request_status") ?>
                        <font style='color:red'>*</font>   
                    </div>
                    <div class="span8">
                        <?= form_dropdown("safa_internaltripstatus_id", $safa_intrernaltripstatus, set_value("safa_internaltripstatus_id"), "id='safa_internaltripstatus_id' class='validate[required]'") ?>
                        <span class="bottom" style='color:red' >
                            <?= form_error('safa_internaltripstatus_id') ?> 
                        </span>
                    </div>
                </div>
                <div class="span6">
                    <div class="span4">
                        <?= lang("transport_request_res_code") ?>
                        
                    </div>
                    <div class="span8" >
                        <?= form_input("operator_reference", set_value("operator_reference")) ?>
                        <span class="bottom" style='color:red' >
                            <?= form_error('operator_reference') ?> 
                        </span>
                    </div>
                </div>
            </div>
            <?php if (PHASE>=2):?>
           <div class="row-form" >
                  <div class="span6">
                    <div class="span4"><?= lang("transport_request_transportername") ?>
                    </div>
                    <div class="span8" >
                        <?= form_dropdown("safa_transporter_id", $transporters, set_value("safa_transporter_id")) ?>
                        <span class="bottom" style='color:red' >
                            <?= form_error('safa_transporter_id') ?> 
                        </span>
                    </div>
                </div>
                
                
                
                 <div class="span6" >
                    <div class="span4">
                        <?= lang("transport_request_opertator") ?>
                           <?if(PHASE==1):?>
                                    <font style='color:red'>*</font>
                           <?  endif;?>
                    </div>
                    <div class="span8" >
                        <?= form_dropdown("safa_ito_id", $safa_ito, set_value("safa_ito_id"), " id='safa_ito_id' class='input-huge'") ?>
                        <span class="bottom" style='color:red' >
                            <?= form_error('safa_ito_id') ?> 
                        </span>
                    </div>
                </div>
                
            </div>
            <?  endif;?>
            <div class="row-form" >
                <div class="span6" >
                    <div class="span4" ><?= lang("transport_request_res_file") ?></div>
                    <div class="span8">
                        <div class="input-append file"  >
                            <input type="file" value="" name="userfile"/>
                            <input type="text" name="text_input"  value="<?=set_value('text_input')?>" />
                            <button type="button"  class="btn">Browse</button>
                        </div>
                        <span class="bottom" style='color:red' >
                            <?= form_error('userfile') ?> 
                        </span>
                    </div> 
                </div>
                
                <div class="span6" >
                    <div class="span4"><?= lang("confirmation_number") ?>
                        <font style='color:red'>*</font>   
                    </div>
                    <div class="span8">
                        <?= form_input("confirmation_number", set_value("confirmation_number", $confirmation_number ), "class='validate[required]'") ?>
                        <span class="bottom" style='color:red' >
                            <?= form_error('confirmation_number') ?> 
                        </span>
                    </div>
                </div>
                
            </div>

        </div>
        <div class="toolbar bottom TAC">
            <input type="submit"  class="btn btn-primary" name="submit" value="<?= lang('global_submit') ?>" />
            <a href='<?= site_url('uo/trip_internaltrip/index') ?>'  class="btn btn-primary" /><?= lang('global_back') ?></a>
        </div>
        <?= form_close() ?>
    </div>
</div>  
</div>
<!-- 
<script type="text/javascript">
    $(function() {
        $('#datetimepicker1').datetimepicker({
            language: 'pt-BR',
            pickTime: false
        });
    });
</script>
 -->
 
<script type="text/javascript">
$(document).ready(function() {
    var config = {
        '.chosen-select': {width: "100%"},
        '.chosen-select-deselect': {allow_single_deselect: true},
        '.chosen-select-no-single': {disable_search_threshold: 10},
        '.chosen-select-no-results': {no_results_text: 'Oops, nothing found!'},
        '.chosen-select-width': {width: "95%"}
    }
    for (var selector in config) {
        $(selector).chosen(config[selector]);
    }


    // binds form submission and fields to the validation engine
    $("#frm_trip_internaltrip").validationEngine({
            prettySelect : true,
            useSuffix: "_chosen",
            promptPosition : "topRight:-150"
            //promptPosition : "bottomLeft"
    });

 
});


</script> 
 
<script>
    $(document).ready(function() {
        $("#safa_uo_contract_id").change(function() {
            //sending the request to get the trips//
            $.get('<?= site_url("uo/trip_internaltrip/get_uo_contract_trips") ?>/' + $(this).val(), function(data) {
                $("#trip_id").html(data);
            });
        });
    });

    $('#safa_uo_contract_id').live('change', function() { 
        var safa_uo_contract_id = $("#safa_uo_contract_id").val();
        $.get('<?= site_url("uo/trip_internaltrip/get_uo_contract_trips") ?>/' + safa_uo_contract_id, function(data) {
            $("#trip_id").html(data);
            $("#trip_id").val('<?= $this->input->post('safa_trip_id') ?>');
        });
    });
</script>
<script>
    // filter with validation //
    $(document).ready(function() {
       /* initializing  the default value of the contracts by first items*/ 
        $("#safa_uo_contract_id").val($(this).find('option').eq(1).val());
        var safa_uo_contract_id = $("#safa_uo_contract_id").val();

        $.get('<?= site_url("uo/trip_internaltrip/get_uo_contract_trips") ?>/' + safa_uo_contract_id, function(data) {
            $("#trip_id").html(data);
            $("#trip_id").val('<?= $this->input->post('safa_trip_id') ?>');
        });

       
        
        
    });
</script>
<script>
    $(document).ready(function() {
        /* input file */
        $(".file .btn, .file input:text").click(function() {
            var block = $(this).parent('.file');
                block.find('input:file').change(function() {
                var file_arr = $(this).val().split('\\');
                var file_name = file_arr[file_arr.length - 1];
                block.find('input:text').val(file_name);
            });
        });
    });
</script>
<?php if (PHASE>1):?>
<script>
    $(document).ready(function() {
        $("#safa_ito_id").change(function() {
          if($("#safa_ito_id").val()){
               $("#safa_internaltripstatus_id").val(4);
            }else{
               $("#safa_internaltripstatus_id").val(3);
            }
            });
        $("#safa_internaltripstatus_id").change(function() {
            if($("#safa_ito_id").val()){
              if($("#safa_internaltripstatus_id").val()!=4)
                   <? if (lang('global_lang') == 'ar'): ?>
                       alert("قم باختيار مؤكد من شركة العمرة");
                   <?else:?>
                       alert("choose confirmed by uo"); 
                   <?  endif;?>  
                   $("#safa_internaltripstatus_id").val(4);
            }else{
               if($("#safa_internaltripstatus_id").val()!=3)
                   <? if (lang('global_lang') == 'ar'): ?>
                       alert("قم باختيار بانتظار مشغل النقل");
                   <?else:?>
                       alert("choose pending ito");
                   <?  endif;?>    
                   $("#safa_internaltripstatus_id").val(3);
            }
        }); 
    });
    
            $("#frm_trip_internaltrip").validationEngine({
        prettySelect: true,
        useSuffix: "_chosen",
        promptPosition: "topRight:-150"
//promptPosition : "bottomLeft"
    });
</script> 
<?  endif;?>

