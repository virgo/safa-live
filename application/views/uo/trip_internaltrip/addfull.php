<div class="row-fluid">
    <div class="widget">
        <div class="path-container Fright">
            <div class="icon"><i class="icos-pencil2"></i></div> 
            <div class="path-name Fright">
                <a href="<?= site_url('uo/dashboard') ?>"><?= lang('parent_node_title') ?></a>
            </div>    
            <div class="path-arrow Fright">
            </div>
            <div class="path-name Fright">
                <a href="<?= site_url('uo/trip_internaltrip/index') ?>"><?= lang('node_title') ?></a>
            </div>
            <div class="path-arrow Fright">
            </div>
            <div class="path-name Fright">
                <span><?= lang('add_transport_request') ?></span>
            </div>
        </div>     
    </div>

    <div class="widget">
        <div class="widget-header">
            <div class="widget-header-icon Fright">
                <span class="icos-pencil2"></span>
            </div>
            <div class="widget-header-title Fright">
                <?= lang('add_transport_request') ?>
            </div>
        </div>

        <?= form_open_multipart(false, 'id="frm_trip_internaltrip" ') ?>
        <div class="block-fluid">
            <div class="row-form" >
                <div class="span6" >
                    <div class="span4">
                        <?= lang("transport_request_date") ?>
                        <font style='color:red'>*</font>
                    </div>
                    <div class="span8"> 
                        <div >
                            <input class="datetime validate[required]" data-format="yyyy-MM-dd" name="datetime" id="date" type="text" value="<?= set_value('datetime', date('Y-m-d', time())) ?>" ></input>

                            <script>
                                $('.datetime').datepicker({
                                    dateFormat: "yy-mm-dd",
                                    controlType: 'select',
                                    timeFormat: 'HH:mm'
                                });
                            </script>


                            <!-- 
                            <span class="add-on">
                                <i data-time-icon="icon-time" data-date-icon="icon-calendar">
                                </i>
                            </span>
                            -->
                        </div>
                        <span class="bottom" style='color:red' >
                            <?= form_error('datetime') ?> 
                        </span>
                    </div>


                </div>
                <div class="span6" >
                    <div class="span4"><?= lang("transport_request_contract") ?>
                        <font style='color:red'>*</font>
                    </div>
                    <div class="span8">
                        <?= form_dropdown("safa_uo_contract_id", $safa_uo_contracts, set_value("safa_uo_contract_id"), " id='safa_uo_contract_id' class=' chosen-select chosen-rtl validate[required] input-huge '") ?>
                        <span class="bottom" style='color:red' >
                            <?= form_error('safa_uo_contract_id') ?> 
                        </span>
                    </div>
                </div>
            </div>
            <div class="row-form" >
                <div class="span6" >
                    <div class="span4"><?= lang("trip_code") ?>
                        <font style='color:red'>*</font>
                    </div>
                    <div class="span8">
                        <!--<?= form_input("trip_title", set_value("trip_title"), " id='trip_title' class=' input-huge '") ?>
                        <span class="bottom" style='color:red' >
                        <?= form_error('trip_title') ?> 
                        </span>
                        -->
                        <?= form_input("safa_uo_code", set_value("safa_uo_code", $safa_uo_code), "class='validate[required]' readonly='readonly' ") ?>
                        <?= form_input("serial", set_value("serial", $safa_uo_serial), "class='validate[required]'") ?>
                        <span class="bottom" style='color:red' >
                            <?= form_error('serial') ?> 
                        </span>


                    </div>
                </div>
                <div class="span6" >
                    <div class="span4">
                        <?= lang("transport_request_opertator") ?>
                        <? if (PHASE == 1): ?>
                        <font style='color:red'>*</font>
                        <? endif; ?>
                    </div>
                    <div class="span8" >
                        <?= form_dropdown("safa_ito_id", $safa_ito, set_value("safa_ito_id"), " id='safa_ito_id' class=' chosen-select chosen-rtl input-huge '") ?>
                        <span class="bottom" style='color:red' >
                            <?= form_error('safa_ito_id') ?> 
                        </span>
                    </div>
                </div>
            </div>
            <div class="row-form" >
                <div class="span6" >
                    <div class="span4"><?= lang("transport_trip_supervisors") ?>
                    </div>
                    <div class="span8">
                        <?= form_input("trip_supervisors", set_value("trip_supervisors"), " id='trip_supervisors' class=' input-huge '") ?>
                        <span class="bottom" style='color:red' >
                            <?= form_error('trip_supervisors') ?> 
                        </span>
                    </div>
                </div>
                <div class="span6" >
                    <div class="span4">
                        <?= lang("transport_trip_supervisors_phone") ?>
                    </div>
                    <div class="span8" >
                        <?= form_input("trip_supervisors_phone", set_value("trip_supervisors_phone"), " id='trip_supervisors_phone' class='input-huge '") ?>
                        <span class="bottom" style='color:red' >
                            <?= form_error('safa_ito_id') ?> 
                        </span>
                    </div>
                </div>
            </div>
            <div class="row-form" >
                <div class="span6" >
                    <div class="span4"><?= lang("transport_request_status") ?>
                        <font style='color:red'>*</font>   
                    </div>
                    <div class="span8">
                        <?= form_dropdown("safa_internaltripstatus_id", $safa_intrernaltripstatus, set_value("safa_internaltripstatus_id", 2), "id='safa_internaltripstatus_id' class='chosen-select chosen-rtl validate[required]'") ?>
                        <span class="bottom" style='color:red' >
                            <?= form_error('safa_internaltripstatus_id') ?> 
                        </span>
                    </div>
                </div>
                <div class="span6">
                    <div class="span4">
                        <?= lang("transport_request_res_code") ?>
                    </div>
                    <div class="span8" >
                        <?= form_input("operator_reference", set_value("operator_reference")) ?>
                        <span class="bottom" style='color:red' >
                            <?= form_error('operator_reference') ?> 
                        </span>
                    </div>
                </div>
            </div>
            <?php if (PHASE >= 2): ?>
            <div class="row-form" >
                <div class="span6">
                    <div class="span4"><?= lang("transport_request_transportername") ?>
                    </div>
                    <div class="span8" >
                        <?= form_dropdown("safa_transporter_id", $transporters, set_value("safa_transporter_id"), "class='chosen-select chosen-rtl'") ?>
                        <span class="bottom" style='color:red' >
                            <?= form_error('safa_transporter_id') ?> 
                        </span>
                    </div>
                </div>
            </div>
            <? endif; ?>
            <div class="row-form" >
                <div class="span6" >
                    <div class="span4" ><?= lang("transport_request_res_file") ?></div>
                    <div class="span8">
                        <input type="file" value="" name="userfile"/>
                        <span class="bottom" style='color:red' >
                            <?= form_error('userfile') ?> 
                        </span>
                    </div> 
                </div>
                <div class="span6" >
                    <div class="span4" ><?= lang('confirmation_number') ?></div>
                    <div class="span8" >

                        <?= form_input("confirmation_number", set_value("confirmation_number"), "") ?>
                        <span class="bottom" style='color:red' >
                            <?= form_error('confirmation_number') ?> 
                        </span>
                    </div>
                </div>
            </div>

            <div class="row-form" >
                <div class="span12"><?= lang("travellers_seats") ?></div>
                <div class="clear clearfix"></div>
                <div class="span4" >
                    <div class="span4" ><?= lang("adult_seats") ?></div>
                    <div class="span8">
                        <?= form_input("adult_seats", set_value("adult_seats"), " onchange='fix_seats_count();' ") ?>
                        <span class="bottom" style='color:red' >
                            <?= form_error('adult_seats') ?> 
                        </span>
                    </div> 
                </div>
                <div class="span4" >
                    <div class="span4" ><?= lang('child_seats') ?></div>
                    <div class="span8" >
                        <?= form_input("child_seats", set_value("child_seats"), " onchange='fix_seats_count();' ") ?>
                        <span class="bottom" style='color:red' >
                            <?= form_error('child_seats') ?> 
                        </span>
                    </div>
                </div>
                <div class="span3" >
                    <div class="span4" ><?= lang('baby_seats') ?></div>
                    <div class="span8" >

                        <?= form_input("baby_seats", set_value("baby_seats"), " onchange='fix_seats_count();' ") ?>
                        <span class="bottom" style='color:red' >
                            <?= form_error('baby_seats') ?> 
                        </span>
                    </div>
                </div>
            </div>
        </div>

        <div class="row-fluid">
            <div class="resalt-group">
                <div class="wizerd-div"><a><?= lang('flight_trip') ?></a></div>
                <div class="label"><?= lang('going') ?></div>
                <table id='div_trip_going_group'>
                    <thead>
                        <tr>
                            <th><?= lang('erp_path_type_id') ?></th>
                            <th><?= lang('flight_availabilities_airlines') ?></th>
                            <th><?= lang('flight_availabilities_flight_number') ?></th>
                            <th><?= lang('flight_availabilities_date') ?></th>
                            <th><?= lang('arrival_date') ?></th>
                            <th><?= lang('flight_availabilities_airports') ?></th>
                            <th><?= lang('flight_availabilities_seat_count') ?></th>
                            <th><a href="javascript:new_row(1);" class="btn" > <?php echo lang('global_add') ?> </a></th>

                        </tr>
                    </thead>
                    <tbody id="going_flight_tbody">



                        <? if (isset($going_trip_flights_rows)) { ?>
                        <?
                        foreach ($going_trip_flights_rows as $value) {
                        $this->flight_availabilities_model->erp_flight_availability_id = $value->erp_flight_availability_id;

                        //$this->flight_availabilities_model->erp_path_type_id = $flight_trip_type;

                        $flight_availabilities_details_rows = $this->flight_availabilities_model->get_details_for_trip();

                        $ports = '';

                        $safa_externaltriptype_name = '';

                        $start_datetime = '';
                        $end_datetime = '';
                        $loop_counter = 0;

                        $erp_port_id_from = 0;
                        $erp_port_id_to = 0;
                        $start_ports_name = '';
                        $end_ports_name = '';
                        ?>


                        <?php
                        foreach ($flight_availabilities_details_rows as $flight_availabilities_details_row) {
                        if ($loop_counter == 0) {
                        $safa_externaltriptype_name = $flight_availabilities_details_row->safa_externaltriptype_name;
                        $erp_path_type_name = $flight_availabilities_details_row->erp_path_type_name;

                        $erp_port_id_from = $flight_availabilities_details_row->erp_port_id_from;
                        $start_ports_name = $flight_availabilities_details_row->start_ports_name;

                        $erp_port_id_to = $flight_availabilities_details_row->erp_port_id_to;
                        $end_ports_name = $flight_availabilities_details_row->end_ports_name;

                        $ports = $flight_availabilities_details_row->start_ports_name . ' - ' . $flight_availabilities_details_row->end_ports_name;
                        $start_datetime = $flight_availabilities_details_row->flight_date . ' - ' . $flight_availabilities_details_row->departure_time;
                        $end_datetime = $flight_availabilities_details_row->flight_date . ' - ' . $flight_availabilities_details_row->arrival_time;
                        } else {
                        $safa_externaltriptype_name = $safa_externaltriptype_name . ' <br/> ' . $flight_availabilities_details_row->safa_externaltriptype_name;

                        $erp_port_id_to = $flight_availabilities_details_row->erp_port_id_to;
                        $end_ports_name = $flight_availabilities_details_row->end_ports_name;

                        $ports = $ports . ' <br/> ' . $flight_availabilities_details_row->start_ports_name . ' - ' . $flight_availabilities_details_row->end_ports_name;
                        $end_datetime = $flight_availabilities_details_row->flight_date . ' - ' . $flight_availabilities_details_row->arrival_time;
                        }
                        $loop_counter++;

                        if ($flight_availabilities_details_row->safa_externaltriptype_id == 1) {
                        ?>



                        <tr>

                                                                                <!--<td><?php echo $flight_availabilities_details_row->safa_externaltriptype_name; ?>
                                                                                 <input type='hidden' name='safa_externaltriptype_name_<?= $value->erp_flight_availability_id ?>'  id='safa_externaltriptype_name_<?= $value->erp_flight_availability_id ?>'  value='<?= $flight_availabilities_details_row->safa_externaltriptype_name ?>'/>
                                                                                </td>

                            --><td><?php echo $flight_availabilities_details_row->erp_path_type_name; ?>
                                <input type='hidden' name='safa_externaltriptype_name_<?= $value->erp_flight_availability_id ?>'  id='safa_externaltriptype_name_<?= $value->erp_flight_availability_id ?>'  value='<?= $flight_availabilities_details_row->erp_path_type_name ?>'/>
                            </td>

                            <td><?php echo $flight_availabilities_details_row->airline; ?></td>
                            <td><?php echo $flight_availabilities_details_row->flight_number; ?></td>


                            <td>

                                                                                <!--<input type='hidden' name='departure_date_<?= $value->erp_flight_availability_id ?>'  id='departure_date_<?= $value->erp_flight_availability_id ?>'  value='<?= $start_datetime ?>'/>
                                <?php echo $start_datetime; ?>
                                -->
                                <input type='hidden' name='departure_date_<?= $value->erp_flight_availability_id ?>'  id='departure_date_<?= $value->erp_flight_availability_id ?>'  value='<?= $flight_availabilities_details_row->flight_date . ' - ' . $flight_availabilities_details_row->departure_time ?>'/>
                                <?php echo $flight_availabilities_details_row->flight_date . ' - ' . $flight_availabilities_details_row->departure_time; ?>
                            </td>


                            <td>
                                <input type='hidden' name='erp_port_id_from_<?= $value->erp_flight_availability_id ?>'  id='erp_port_id_from_<?= $value->erp_flight_availability_id ?>'  value='<?= $flight_availabilities_details_row->erp_port_id_from ?>'/>
                                <input type='hidden' name='erp_port_id_to_<?= $value->erp_flight_availability_id ?>'  id='erp_port_id_to_<?= $value->erp_flight_availability_id ?>'  value='<?= $flight_availabilities_details_row->erp_port_id_to ?>'/>

                                <input type='hidden' name='start_ports_name_<?= $value->erp_flight_availability_id ?>'  id='start_ports_name_<?= $value->erp_flight_availability_id ?>'  value='<?= $flight_availabilities_details_row->start_ports_name; ?>'/>
                                <input type='hidden' name='end_ports_name_<?= $value->erp_flight_availability_id ?>'  id='end_ports_name_<?= $value->erp_flight_availability_id ?>'  value='<?= $flight_availabilities_details_row->end_ports_name; ?>'/>


                                <?= $flight_availabilities_details_row->start_ports_name ?> - <?= $flight_availabilities_details_row->end_ports_name ?>
                                <input type='hidden' name='ports_name_<?= $value->erp_flight_availability_id ?>'  id='ports_name_<?= $value->erp_flight_availability_id ?>'  value='<?= $flight_availabilities_details_row->start_ports_name ?> - <?= $flight_availabilities_details_row->end_ports_name ?>'/>



                            </td>

                            <td>
                                <input type='hidden' name='available_seats_count_<?= $value->erp_flight_availability_id ?>'  id='available_seats_count_<?= $value->erp_flight_availability_id ?>'  value='<?= $flight_availabilities_details_row->available_seats_count ?>'/>
                                <input type='text' style="width:50px" name='available_seats_count_text_<?= $value->erp_flight_availability_id ?>'  id='available_seats_count_text_<?= $value->erp_flight_availability_id ?>'  value='<?= $flight_availabilities_details_row->available_seats_count ?>' readonly="readonly" disabled="disabled"/>

                                <!-- <?= $value->available_seats_count ?> -->

                            </td>


                                                                                <!--<td>
                            <?php echo $flight_availabilities_details_row->departure_time; ?>
                                                                                </td>
                                                                                <td>
                            <?php echo $flight_availabilities_details_row->arrival_time; ?>
                                                                                </td>



                            --></tr>


                        <?php
                        }
                        }
                        ?>



                        <? } ?>
                        <? } ?>

                    </tbody>

                </table>
                <div class="clear clearfix" style="height: 5px;"> </div>
                <div class="label clear"><?= lang('return') ?></div>
                <table id='div_trip_return_group'>
                    <thead>
                        <tr>
                            <th><?= lang('erp_path_type_id') ?></th>
                            <th><?= lang('flight_availabilities_airlines') ?></th>
                            <th><?= lang('flight_availabilities_flight_number') ?></th>
                            <th><?= lang('flight_availabilities_date') ?></th>
                            <th><?= lang('arrival_date') ?></th>
                            <th><?= lang('flight_availabilities_airports') ?></th>
                            <th><?= lang('flight_availabilities_seat_count') ?></th>

                            <th><a href="javascript:new_row(2);" class="btn" > <?php echo lang('global_add') ?> </a></th>

                        </tr>
                    </thead>
                    <tbody id="return_flight_tbody">

                        <? if (isset($going_trip_flights_rows)) { ?>
                        <?
                        foreach ($going_trip_flights_rows as $value) {
                        $this->flight_availabilities_model->erp_flight_availability_id = $value->erp_flight_availability_id;

                        //$this->flight_availabilities_model->erp_path_type_id = $flight_trip_type;

                        $flight_availabilities_details_rows = $this->flight_availabilities_model->get_details_for_trip();

                        $ports = '';

                        $safa_externaltriptype_name = '';

                        $start_datetime = '';
                        $end_datetime = '';
                        $loop_counter = 0;

                        $erp_port_id_from = 0;
                        $erp_port_id_to = 0;
                        $start_ports_name = '';
                        $end_ports_name = '';
                        ?>


                        <?php
                        foreach ($flight_availabilities_details_rows as $flight_availabilities_details_row) {
                        if ($loop_counter == 0) {
                        $safa_externaltriptype_name = $flight_availabilities_details_row->safa_externaltriptype_name;
                        $erp_path_type_name = $flight_availabilities_details_row->erp_path_type_name;

                        $erp_port_id_from = $flight_availabilities_details_row->erp_port_id_from;
                        $start_ports_name = $flight_availabilities_details_row->start_ports_name;

                        $erp_port_id_to = $flight_availabilities_details_row->erp_port_id_to;
                        $end_ports_name = $flight_availabilities_details_row->end_ports_name;

                        $ports = $flight_availabilities_details_row->start_ports_name . ' - ' . $flight_availabilities_details_row->end_ports_name;
                        $start_datetime = $flight_availabilities_details_row->flight_date . ' - ' . $flight_availabilities_details_row->departure_time;
                        $end_datetime = $flight_availabilities_details_row->flight_date . ' - ' . $flight_availabilities_details_row->arrival_time;
                        } else {
                        $safa_externaltriptype_name = $safa_externaltriptype_name . ' <br/> ' . $flight_availabilities_details_row->safa_externaltriptype_name;

                        $erp_port_id_to = $flight_availabilities_details_row->erp_port_id_to;
                        $end_ports_name = $flight_availabilities_details_row->end_ports_name;

                        $ports = $ports . ' <br/> ' . $flight_availabilities_details_row->start_ports_name . ' - ' . $flight_availabilities_details_row->end_ports_name;
                        $end_datetime = $flight_availabilities_details_row->flight_date . ' - ' . $flight_availabilities_details_row->arrival_time;
                        }
                        $loop_counter++;

                        if ($flight_availabilities_details_row->safa_externaltriptype_id == 2) {
                        ?>



                        <tr>

                                <!--<td><?php echo $flight_availabilities_details_row->safa_externaltriptype_name; ?>
                                 <input type='hidden' name='safa_externaltriptype_name_<?= $value->erp_flight_availability_id ?>'  id='safa_externaltriptype_name_<?= $value->erp_flight_availability_id ?>'  value='<?= $flight_availabilities_details_row->safa_externaltriptype_name ?>'/>
                                </td>

                            --><td>
                                <input type="hidden" name="trip_going_erp_flight_availability_id<?= $value->erp_flight_availability_id ?>" id="trip_going_erp_flight_availability_id<?= $value->erp_flight_availability_id ?>"  value="<?= $value->erp_flight_availability_id ?>" />

                                <?php echo $flight_availabilities_details_row->erp_path_type_name; ?>
                                <input type='hidden' name='safa_externaltriptype_name_<?= $value->erp_flight_availability_id ?>'  id='safa_externaltriptype_name_<?= $value->erp_flight_availability_id ?>'  value='<?= $flight_availabilities_details_row->erp_path_type_name ?>'/>
                            </td>

                            <td><?php echo $flight_availabilities_details_row->airline; ?></td>
                            <td><?php echo $flight_availabilities_details_row->flight_number; ?></td>
                            <td><?php echo $flight_availabilities_details_row->erp_flight_classes_name; ?></td>

                            <td>

                                <!--<input type='hidden' name='departure_date_<?= $value->erp_flight_availability_id ?>'  id='departure_date_<?= $value->erp_flight_availability_id ?>'  value='<?= $start_datetime ?>'/>
                                <?php echo $start_datetime; ?>
                                -->
                                <input type='hidden' name='departure_date_<?= $value->erp_flight_availability_id ?>'  id='departure_date_<?= $value->erp_flight_availability_id ?>'  value='<?= $flight_availabilities_details_row->flight_date . ' - ' . $flight_availabilities_details_row->departure_time ?>'/>
                                <?php echo $flight_availabilities_details_row->flight_date . ' - ' . $flight_availabilities_details_row->departure_time; ?>
                            </td>

                            <td>

                                <!--<input type='hidden' name='departure_date_<?= $value->erp_flight_availability_id ?>'  id='departure_date_<?= $value->erp_flight_availability_id ?>'  value='<?= $start_datetime ?>'/>
                                <?php echo $start_datetime; ?>
                                -->
                                <input type='hidden' name='departure_date_<?= $value->erp_flight_availability_id ?>'  id='departure_date_<?= $value->erp_flight_availability_id ?>'  value='<?= $flight_availabilities_details_row->flight_date . ' - ' . $flight_availabilities_details_row->departure_time ?>'/>
                                <?php echo $flight_availabilities_details_row->flight_date . ' - ' . $flight_availabilities_details_row->departure_time; ?>
                            </td>


                            <td>
                                <input type='hidden' name='erp_port_id_from_<?= $value->erp_flight_availability_id ?>'  id='erp_port_id_from_<?= $value->erp_flight_availability_id ?>'  value='<?= $flight_availabilities_details_row->erp_port_id_from ?>'/>
                                <input type='hidden' name='erp_port_id_to_<?= $value->erp_flight_availability_id ?>'  id='erp_port_id_to_<?= $value->erp_flight_availability_id ?>'  value='<?= $flight_availabilities_details_row->erp_port_id_to ?>'/>

                                <input type='hidden' name='start_ports_name_<?= $value->erp_flight_availability_id ?>'  id='start_ports_name_<?= $value->erp_flight_availability_id ?>'  value='<?= $flight_availabilities_details_row->start_ports_name; ?>'/>
                                <input type='hidden' name='end_ports_name_<?= $value->erp_flight_availability_id ?>'  id='end_ports_name_<?= $value->erp_flight_availability_id ?>'  value='<?= $flight_availabilities_details_row->end_ports_name; ?>'/>


                                <?= $flight_availabilities_details_row->start_ports_name ?> - <?= $flight_availabilities_details_row->end_ports_name ?>
                                <input type='hidden' name='ports_name_<?= $value->erp_flight_availability_id ?>'  id='ports_name_<?= $value->erp_flight_availability_id ?>'  value='<?= $flight_availabilities_details_row->start_ports_name ?> - <?= $flight_availabilities_details_row->end_ports_name ?>'/>


                                <!--<?php echo $ports; ?>
                                <input type='hidden' name='ports_name_<?= $value->erp_flight_availability_id ?>'  id='ports_name_<?= $value->erp_flight_availability_id ?>'  value='<?= $ports ?>'/>-->

                            </td>

                            <td><?php echo $flight_availabilities_details_row->erp_flight_status_name; ?></td>


                            <td>
                                <input type='hidden' name='available_seats_count_<?= $value->erp_flight_availability_id ?>'  id='available_seats_count_<?= $value->erp_flight_availability_id ?>'  value='<?= $flight_availabilities_details_row->available_seats_count ?>'/>
                                <input type='text' style="width:50px" name='available_seats_count_text_<?= $value->erp_flight_availability_id ?>'  id='available_seats_count_text_<?= $value->erp_flight_availability_id ?>'  value='<?= $flight_availabilities_details_row->available_seats_count ?>' readonly="readonly" disabled="disabled"/>

                                <!-- <?= $value->available_seats_count ?> -->

                            </td>


                                <!--<td>
                            <?php echo $flight_availabilities_details_row->departure_time; ?>
                                </td>
                                <td>
                            <?php echo $flight_availabilities_details_row->arrival_time; ?>
                                </td>



                            --></tr>


                        <?php
                        }
                        }
                        ?>



                        <? } ?>
                        <? } ?>

                    </tbody>

                </table>
            </div>      
        </div>

        <div class="row-fluid">
            <fieldset class="resalt-group">
                <div class="wizerd-div"><a> <?= lang('hotels') ?></a></div>
                <table id='div_hotels'>
                    <thead>
                        <tr>
                            <th><?= lang('city') ?></th>
                            <th><?=
                                lang('hotel')
                                ?></th>
                            <th><?=
                                lang('entry_date')
                                ?></th>
                            <th><?=
                                lang('nights_count')
                                ?></th>
                            <th><?=
                                lang('exit_date')
                                ?></th>

                            <th><a href="javascript:void(0)" onclick="new_hotel();" class="btn" > <?php echo lang('global_add') ?> </a></th>

                        </tr>
                    </thead>
                    <tbody class="internalhotels">

                    </tbody>
                </table>
            </fieldset>
        </div>

        <div class="row-fluid">
            <fieldset class="resalt-group">
                <div class="wizerd-div"><a> <?= lang('tourismplaces') ?></a></div>
                <table id='tourismplaces'>
                    <thead>
                        <tr>
                            <th><?= lang('tourismplace') ?></th>
                            <th><?=
                                lang('the_date')
                                ?></th>
                            <th><a href="javascript:void(0)" onclick="new_tourismplace();" class="btn" > <?php echo lang('global_add') ?> </a></th>

                        </tr>
                    </thead>
                    <tbody class="tourismplaces">

                    </tbody>
                </table>
            </fieldset>
        </div>

        <div class="toolbar bottom TAC">
            <input type="submit"  class="btn btn-primary" name="submit" value="<?= lang('global_submit') ?>" />
            <a href='<?= site_url('uo/trip_internaltrip/index') ?>'  class="btn btn-primary" /><?= lang('global_back') ?></a>
        </div>
        <?= form_close() ?>
    </div>
</div>  
</div>
<!-- 
<script type="text/javascript">
    $(function() {
        $('#datetimepicker1').datetimepicker({
            language: 'pt-BR',
            pickTime: false
        });
    });
</script>
-->

<script type="text/javascript">
    $(document).ready(function() {
        var config = {
            '.chosen-select': {width: "100%"},
            '.chosen-select-deselect': {allow_single_deselect: true},
            '.chosen-select-no-single': {disable_search_threshold: 10},
            '.chosen-select-no-results': {no_results_text: 'Oops, nothing found!'},
            '.chosen-select-width': {width: "95%"}
        }
        for (var selector in config) {
            $(selector).chosen(config[selector]);
        }


        // binds form submission and fields to the validation engine
        $("#frm_trip_internaltrip").validationEngine({
            prettySelect: true,
            useSuffix: "_chosen",
            promptPosition: "topRight:-150"
                    //promptPosition : "bottomLeft"
        });


    });


</script> 

<script>
    $(document).ready(function() {
        $("#uo_ea_id").change(function() {
            //sending the request to get the trips//
            $.get('<?= site_url("uo/trip_internaltrip/get_uo_contract_trips") ?>/' + $(this).val(), function(data) {
                $("#trip_id").html(data);
            });
        });
    });

    $('#uo_ea_id').live('change', function() {
        var uo_ea_id = $("#uo_ea_id").val();
        $.get('<?= site_url("uo/trip_internaltrip/get_uo_contract_trips") ?>/' + uo_ea_id, function(data) {
            $("#trip_id").html(data);
            $("#trip_id").val('<?= $this->input->post('safa_trip_id') ?>');
        });
    });
</script>
<script>
    // filter with validation //
    $(document).ready(function() {
        /* initializing  the default value of the contracts by first items*/
        $("#uo_ea_id").val($(this).find('option').eq(1).val());
        var uo_ea_id = $("#uo_ea_id").val();

        $.get('<?= site_url("uo/trip_internaltrip/get_uo_contract_trips") ?>/' + uo_ea_id, function(data) {
            $("#trip_id").html(data);
            $("#trip_id").val('<?= $this->input->post('safa_trip_id') ?>');
        });




    });
</script>
<script>
    $(document).ready(function() {
        /* input file */
        $(".file .btn, .file input:text").click(function() {
            var block = $(this).parent('.file');
            block.find('input:file').change(function() {
                var file_arr = $(this).val().split('\\');
                var file_name = file_arr[file_arr.length - 1];
                block.find('input:text').val(file_name);
            });
        });
    });
</script>
<?php if (PHASE > 1): ?>
<script>
    $(document).ready(function() {
        $("#safa_ito_id").change(function() {
            if ($("#safa_ito_id").val()) {
                $("#safa_internaltripstatus_id").val(4);
            } else {
                $("#safa_internaltripstatus_id").val(3);
            }
        });
        $("#safa_internaltripstatus_id").change(function() {
            if ($("#safa_ito_id").val()) {
                if ($("#safa_internaltripstatus_id").val() != 4)
                        < ? if (lang('global_lang') == 'ar'): ? >
                    alert("قم باختيار مؤكد من شركة العمرة");
            < ? else: ? >
                    alert("choose confirmed by uo");
                    < ? endif; ? >
                    $("#safa_internaltripstatus_id").val(4);
        } else {
        if ($("#safa_internaltripstatus_id").val() != 3)
                < ? if (lang('global_lang') == 'ar'): ? >
            alert("قم باختيار بانتظار مشغل النقل");
        < ? else: ? >
                alert("choose pending ito");
                < ? endif; ? >
                $("#safa_internaltripstatus_id").val(3);
        }
    });
    });
</script> 
<? endif; ?>

<script>
            var c1 = 0;
    var c2 = 100;


    function new_row(safa_externaltriptype_id) {


        if (safa_externaltriptype_id == 1) {
            var id = c1;
            var new_row = function() {/*
             <tr class="table-row" rel="{$id}">
             <td>
             <input type="hidden" name="safa_externaltriptype_id[{$id}]" id="safa_externaltriptype_id[{$id}]"  value="1" />
             
<?= form_dropdown('erp_path_type_id[{$id}]', $erp_path_types, set_value("erp_path_type_id[{\$id}]", 2), 'class="validate[required] erp_path_type" style="width:60px" pathrel="{$id}" id="erp_path_type_id[{$id}]"') ?></td>
             <td>
             
             <table style='border:0px;'><tr><td style='border:0px;'>
<?= form_dropdown('erp_airline_id[{$id}]', $erp_airlines, set_value("erp_airline_id[{\$id}]"), ' class=" chosen-select chosen-rtl validate[required]" style="width:60px" id="erp_airline_id[{$id}]"') ?></td>
             
             </td><td style='border:0px;'>
             <a href="javascript:void(0);" onclick="add_transporter({$id});"  class="btn Fleft " > <span class="icon-plus"></span></a>
             </td></tr>
             </table>
             
             </td>
             <td><?= form_input('flight_number[{$id}]', set_value("flight_number[{\$id}]"), 'class="validate[required]" style="width:40px" id="flightno[{$id}]"') ?></td>
             <td><?= form_input('flight_date[{$id}]', set_value("flight_date[{\$id}]"), 'class="validate[required] datetimepicker" style="width:100px" id="datefields[{$id}]"') ?></td>
             <td><?= form_input('flight_arrival_date[{$id}]', set_value("flight_arrival_date[{\$id}]"), 'class="validate[required] datetimepicker" arriverel="{$id}" style="width:100px" id="flight_arrival_date[{$id}]"') ?></td>
             <td style="width:180px"><?= form_dropdown('erp_port_id_from[{$id}]', $erp_ports, set_value("erp_port_id_from[{\$id}]"), 'class="validate[required]" style="width:60px" id="erp_port_id_from[{$id}]"') ?>
<?= form_dropdown('erp_port_id_to[{$id}]', $erp_sa_ports, set_value("erp_port_id_to[{\$id}]"), 'class="validate[required]" style="width:60px" id="erp_port_id_to[{$id}]"') ?></td>
             
             <td><?= form_input('seats_count[{$id}]', set_value("seats_count[{\$id}]"), 'class="validate[required] seatcount" style="width:50px" id="seatcont[{$id}]"') ?></td>
             <td><a href="javascript:void(0);" onclick="remove_row('{$id}')"><span class="icon-trash"></span> </a></td>
             </tr>
             */
            }.toString().replace(/{\$id}/g, id).slice(14, -3);

            $('tbody#going_flight_tbody').append(new_row);
            $('.datetimepicker').datetimepicker({dateFormat: 'yy-mm-dd', timeFormat: 'HH:mm'});
            c1++;
        } else if (safa_externaltriptype_id == 2) {
            var id = c2;
            var new_row = function() {/*
             <tr class="table-row" rel="{$id}">
             <td>
             <input type="hidden" name="safa_externaltriptype_id[{$id}]" id="safa_externaltriptype_id[{$id}]"  value="2" />
             
<?= form_dropdown('erp_path_type_id[{$id}]', $erp_path_types, set_value("erp_path_type_id[{\$id}]"), 'class="validate[required]" style="width:60px" id="erp_path_type_id[{$id}]"') ?></td>
             <td>
             
             
             <table style='border:0px;'><tr><td style='border:0px;'>
<?= form_dropdown('erp_airline_id[{$id}]', $erp_airlines, set_value("erp_airline_id[{\$id}]"), 'class="  chosen-select chosen-rtl  validate[required]" style="width:60px" id="erp_airline_id[{$id}]"') ?></td>
             
             </td><td style='border:0px;'>
             <a href="javascript:void(0);" onclick="add_transporter();"  class="btn Fleft " > <span class="icon-plus"></span></a>
             </td></tr>
             </table>
             
             </td>
             <td><?= form_input('flight_number[{$id}]', set_value("flight_number[{\$id}]"), 'class="validate[required]" style="width:40px" id="flightno[{$id}]"') ?></td>
             <td><?= form_input('flight_date[{$id}]', set_value("flight_date[{\$id}]"), 'class="validate[required] datetimepicker" style="width:100px" id="datefields[{$id}]"') ?></td>
             <td><?= form_input('flight_arrival_date[{$id}]', set_value("flight_arrival_date[{\$id}]"), 'class="validate[required] datetimepicker" style="width:100px" id="flight_arrival_date[{$id}]"') ?></td>
             
             <td style="width:180px"><?= form_dropdown('erp_port_id_from[{$id}]', $erp_sa_ports, set_value("erp_port_id_from[{\$id}]"), 'class="validate[required]" style="width:60px" id="erp_port_id_from[{$id}]"') ?>
<?= form_dropdown('erp_port_id_to[{$id}]', $erp_ports, set_value("erp_port_id_to[{\$id}]"), 'class="validate[required]" style="width:60px" id="erp_port_id_to[{$id}]"') ?></td>
             
             <td><?= form_input('seats_count[{$id}]', set_value("seats_count[{\$id}]"), 'class="validate[required] seatcount" style="width:50px" id="seatcont[{$id}]"') ?></td>
             <td><a href="javascript:void(0);" onclick="remove_row('{$id}')"><span class="icon-trash"></span> </a></td>
             
             </tr>
             */
            }.toString().replace(/{\$id}/g, id).slice(14, -3);

            $('tbody#return_flight_tbody').append(new_row);
            $('.datetimepicker').datetimepicker({dateFormat: 'yy-mm-dd', timeFormat: 'HH:mm'});
            c2++;
        }
        load_multiselect();

        fix_seats_count();



    }

    function load_multiselect() {
        var config = {
            '.chosen-select': {width: "100%"},
            '.chosen-select-deselect': {allow_single_deselect: true},
            '.chosen-select-no-single': {disable_search_threshold: 10},
            '.chosen-select-no-results': {no_results_text: 'Oops, nothing found!'},
            '.chosen-select-width': {width: "95%"}
        }
        for (var selector in config) {
            $(selector).chosen(config[selector]);
        }
    }

    function fix_seats_count() {
        var adultseat = parseInt($('input[name=adult_seats]').val());
        var childseat = parseInt($('input[name=child_seats]').val());
        var babyseat = parseInt($('input[name=baby_seats]').val());
        if (isNaN(adultseat))
            adultseat = 0;
        if (isNaN(childseat))
            childseat = 0;
        if (isNaN(babyseat))
            babyseat = 0;
        var seats = adultseat + childseat + babyseat;
        $('.seatcount').val(seats);
    }

    function remove_row(id)
    {
        $('tr[rel=' + id + ']').remove();

    }

</script>

<script>
    var h = 0;
    var t = 0;
    function new_hotel() {

        var id = h;
        if (!$('tbody#going_flight_tbody tr').length || !$('tbody#going_flight_tbody tr select.erp_path_type option[value=2]:selected').length) {
            alert('<?php echo lang('you_should_add_arrival_trip_first'); ?>');
            return false;
        } else {
            if (id === 0) {
                var arriverel = $('tbody#going_flight_tbody tr select.erp_path_type option[value=2]:selected').parent().attr('pathrel');
                var arrivedate = ($('input[arriverel=' + arriverel + ']')).val();
            } else {
                var newrel = id - 1;
                var arrivedate = $('input[endrel=' + newrel + ']').val();
            }
        }

        var new_row = function() {/*
         <tr class="table-row" hrel="{$id}">
         <td>           
<?= form_dropdown('erp_city_id[{$id}]', $cities, set_value("erp_city_id[{\$id}]"), 'class="validate[required] " cityrel="{$id}" id="erp_city_id[{$id}]"') ?></td>
         <td>
         
         <table style='border:0px;'><tr><td style='border:0px;'>
<?= form_dropdown('erp_hotel_id[{$id}]', $hotels, set_value("erp_hotel_id[{\$id}]"), 'class="validate[required] chosen-select" hotelrel="{$id}" id="erp_hotel_id[{$id}]"') ?>
         </td><td style='border:0px;'>
         <a href="javascript:void(0);" onclick="add_hotel({$id});"  class="btn Fleft " > <span class="icon-plus"></span></a>
         </td></tr>
         </table>
         
         </td>
         
         
         <td><?= form_input('arrival_date[{$id}]', set_value("arrival_date[{\$id}]", "{arrivedate}"), 'class="validate[required] " startrel="{$id}" readonly="readonly" style="width:100px" id="arrival_date[{$id}]"') ?></td>
         <td><?= form_input('nights[{$id}]', set_value("nights[{\$id}]"), 'class="validate[required]" nightsrel="{$id}" onchange="fix_dates();" style="width:50px" id="class[{$id}]"') ?></td>
         <td><?= form_input('exit_date[{$id}]', set_value("exit_date[{\$id}]"), 'class="validate[required] " endrel="{$id}" style="width:100px" readonly="readonly" id="exit_date[{$id}]"') ?></td>
         <td><a href="javascript:void(0);" onclick="remove_hrow('{$id}')"><span class="icon-trash"></span> </a></td>
         </tr>
         */
        }.toString().replace(/{\$id}/g, id).replace(/{arrivedate}/g, arrivedate).slice(14, -3);

        $('tbody.internalhotels').append(new_row);
        $('.datetimepicker').datetimepicker({dateFormat: 'yy-mm-dd', timeFormat: 'HH:mm'});
        $('.chosen-select').chosen({width: "100%"});
        h++;

    }

    function remove_hrow(id)
    {
        $('tr[hrel=' + id + ']').remove();

    }

    $('tbody.internalhotels').on('change', 'select[cityrel]', function() {
        var rowrel = $(this).attr('cityrel');
        var cityid = $(this).val();
        $.get('<?= site_url('uo/trip_internaltrip/get_hotel_by_city/') ?>/' + cityid, function(data) {
            $('select[hotelrel=' + rowrel + ']').html(data);
            $('select[hotelrel=' + rowrel + ']').trigger("chosen:updated");
        });
    });


    function new_tourismplace() {
        var id = t;
        var new_row = function() {/*
         <tr class="table-row" trel="{$id}">
         <td><?= form_dropdown('safa_tourismplace_id[{$id}]', $safa_tourismplaces, set_value("safa_tourismplace_id[{\$id}]"), 'class="validate[required] " tourismrel="{$id}" id="safa_tourismplace_id[{$id}]"') ?></td>
         <td><?= form_input('tourism_date[{$id}]', set_value("tourism_date[{\$id}]"), 'class="validate[required] datetimepicker" tourdaterel="{$id}" readonly="readonly" style="width:150px" id="tourism_date[{$id}]"') ?></td>
         <td><a href="javascript:void(0);" onclick="remove_trow('{$id}')"><span class="icon-trash"></span> </a></td>
         </tr>
         */
        }.toString().replace(/{\$id}/g, id).slice(14, -3);

        $('tbody.tourismplaces').append(new_row);
        var selectrel = $('select[cityrel] option[value=1]:selected').parent().attr('cityrel');
        $('input[tourdaterel=' + id + ']').datetimepicker({
            dateFormat: 'yy-mm-dd',
            minDate: new Date($('input[startrel=' + selectrel + ']').val()),
            maxDate: new Date($('input[endrel=' + selectrel + ']').val())
        }).val(addDays(new Date($('input[startrel=' + selectrel + ']').val()), parseInt($('input[nightsrel=' + selectrel + ']').val()) / 2) + " 07:00");
        t++;
    }

    function remove_trow(id)
    {
        $('tr[trel=' + id + ']').remove();

    }

    $('tbody.tourismplaces').on('change', 'select[tourismrel]', function() {
        var trow = $(this).attr('tourismrel');
        if ($(this).val() == 1) {
            var selectrel = $('select[cityrel] option[value=1]:selected').parent().attr('cityrel');

        } else {
            var selectrel = $('select[cityrel] option[value=2]:selected').parent().attr('cityrel');
        }
        $('input[tourdaterel=' + trow + ']').datetimepicker("destroy").datetimepicker({
            dateFormat: 'yy-mm-dd',
            minDate: new Date($('input[startrel=' + selectrel + ']').val()),
            maxDate: new Date($('input[endrel=' + selectrel + ']').val())
        }).val(addDays(new Date($('input[startrel=' + selectrel + ']').val()), parseInt($('input[nightsrel=' + selectrel + ']').val()) / 2) + " 07:00");
    });

</script>

<script type="text/javascript">

    function fix_dates() {

        var arriverel = $('tbody#going_flight_tbody tr select.erp_path_type option[value=2]:selected').parent().attr('pathrel');
        var arrivedate = ($('input[arriverel=' + arriverel + ']')).val();
        arrivedatearray = arrivedate.split(' ');
        arrivedate = arrivedatearray[0];
        var enddate = '';
        var first = true;
        $('tbody.internalhotels tr[hrel]').each(function() {
            var rel = $(this).attr('hrel');

            if (!first)
                arrivedate = enddate;

            first = false;

            $('input[startrel=' + rel + ']').val(arrivedate);
            var arrdate = new Date(arrivedate);
            enddate = addDays(arrdate, parseInt($('input[nightsrel=' + rel + ']').val()));
            $('input[endrel=' + rel + ']').val(enddate);
        });

    }

    $('tbody#going_flight_tbody').on('change', 'input[arriverel]', function() {
        fix_dates();
    });

    function addDays(myDate, days)
    {
        var date_standard = new Date(myDate.getTime() + days * 24 * 60 * 60 * 1000);
        var month = (date_standard.getMonth() + 1);
        var return_date = date_standard.getFullYear() + "-" + ('0' + month).slice(-2) + "-" + ('0' + date_standard.getDate()).slice(-2);
        return return_date;
    }

    function substractDays(myDate, days)
    {
        var date_standard = new Date(myDate.getTime() - days * 24 * 60 * 60 * 1000);
        var month = (date_standard.getMonth() + 1);
        var return_date = date_standard.getFullYear() + "-" + ('0' + month).slice(-2) + "-" + ('0' + date_standard.getDate()).slice(-2);
        return return_date;
    }


</script>

<script>
    function add_hotel(hotel_counter)
    {
        $.fancybox({
            'type': 'iframe',
            'width': 500,
            'height': 300,
            'autoDimensions': false,
            'autoScale': false,
            'href': "<?php echo site_url('uo/trip_internaltrip/add_hotel_popup'); ?>/" + hotel_counter,
            afterClose: function() {
                //location.reload();

                $('#div_hotels tr').each(function() {
                    var rowrel = $(this).attr('hrel');
                    //alert(rowrel);
                    var cityid = $('select[cityrel=' + rowrel + ']').val();
                    //alert(cityid);
                    if (typeof cityid !== 'undefined') {
                        $.get('<?= site_url('uo/trip_internaltrip/get_hotel_by_city/') ?>/' + cityid, function(data) {
                            $('select[hotelrel=' + rowrel + ']').html(data);
                            $('select[hotelrel=' + rowrel + ']').trigger("chosen:updated");
                        });
                    }
                });

            }

        });

    }
</script>

<script>
    function add_transporter()
    {
        $.fancybox({
            'type': 'iframe',
            'width': 500,
            'height': 300,
            'autoDimensions': false,
            'autoScale': false,
            'href': "<?php echo site_url('uo/trip_internaltrip/add_transporter_popup'); ?>/",
            afterClose: function() {
                //location.reload();

                var transporter_counter = 0;
                $('#div_trip_going_group tr').each(function() {
                    $.get('<?= site_url('uo/trip_internaltrip/get_transporters/') ?>', function(data) {
                        $('#erp_airline_id[' + transporter_counter + ']').html(data);
                        $('#erp_airline_id[' + transporter_counter + ']').trigger("chosen:updated");
                    });

                    transporter_counter++;
                });


                var transporter_counter = 0;
                $('#div_trip_return_group tr').each(function() {

                    $.get('<?= site_url('uo/trip_internaltrip/get_transporters/') ?>', function(data) {
                        $('#erp_airline_id[' + transporter_counter + ']').html(data);
                        $('#erp_airline_id[' + transporter_counter + ']').trigger("chosen:updated");
                    });

                    transporter_counter++;
                });

            }

        });

    }
</script>