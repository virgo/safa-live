<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); ?>


<div class="row-fluid">
    <div class="widget">
        <div class="path-container Fright">
            <div class="icon"><i class="icos-pencil2"></i></div> 
            <div class="path-name Fright">
                <a href="<?= site_url('uo/dashboard') ?>"><?= lang('parent_node_title') ?></a>
            </div>    
            <div class="path-arrow Fright">
            </div>
            <div class="path-name Fright">
                <a href="<?= site_url('uo/trip_internaltrip/index') ?>"><?= lang('node_title') ?></a>
            </div>
            <div class="path-arrow Fright">
            </div>
            <div class="path-name Fright">
                <span><?= lang('edit_trip_internaltrip') ?></span>
            </div>
        </div>     
    </div>

    <div class="widget">
        <div class="widget-header">
            <div class="widget-header-icon Fright">
                <span class="icos-pencil2"></span>
            </div>

            <div class="widget-header-title Fright">
                <?= lang('edit_trip_internaltrip') ?>
            </div>
        </div>

        <?= form_open_multipart(false, 'id="frm_trip_internaltrip" ') ?>
        <div class="block-fluid">
        
        <div class="row-form" >
                <div class="span6" >
                    <div class="span4"><?= lang("trip_code") ?>
                        <font style='color:red'>*</font>   
                    </div>
                    <div class="span8">
                        <!--<?= form_input("serial", set_value("serial", $item->serial), "disabled='disabled'") ?>
                        <span class="bottom" style='color:red' >
                            <?= form_error('serial') ?> 
                        </span>
                    	-->
                    	<?= form_input("safa_uo_code", set_value("safa_uo_code", $safa_uo_code), "class='validate[required]' readonly='readonly' ") ?>
                        <?= form_input("serial", set_value("serial", $item->serial), "class='validate[required]'  readonly='readonly' ") ?>
                        <span class="bottom" style='color:red' >
                            <?= form_error('serial') ?> 
                        </span>
                        
                    </div>
                </div>
                <div class="span6">
                    <div class="span4">
                        <?= lang("buses_count") ?>
                        <font style='color:red'>*</font>
                    </div>
                    <div class="span8" >
                        <?= form_input("buses_count", set_value("buses_count", $item->buses_count)) ?>
                        <span class="bottom" style='color:red' >
                            <?= form_error('buses_count') ?> 
                        </span>
                    </div>
                </div>
            </div>
        
        
            <div class="row-form" >
                <div class="span6">
                    <div class="span4">
                        <?= lang("transport_request_date") ?>
                        <font style='color:red'>*</font>
                    </div>
                    <div class="span8"> 
                        <div >
                            <input class="datetime" data-format="yyyy-MM-dd" name="datetime" id="date" type="text" value="<?= set_value('datetime',
                                $item->datetime) ?>" ></input>
                            <script>
                                $('.datetime').datepicker({
                                    dateFormat: "yy-mm-dd",
                                    controlType: 'select',
                                    timeFormat: 'HH:mm'
                                });
                            </script>
                        </div>
                        <span class="bottom" style='color:red' >
<?= form_error('datetime') ?> 
                        </span>
                    </div>
                </div>
                
                <div class="span6" >
                    <div class="span4"><?= lang("transport_request_contract") ?>
                        <font style='color:red'>*</font>
                    </div>
                    <div class="span8">
                        <?= form_dropdown("safa_uo_contract_id", $safa_uo_contracts, set_value("safa_uo_contract_id", $item->safa_uo_contract_id), " id='safa_uo_contract_id' class='validate[required] input-huge '") ?>
                        <span class="bottom" style='color:red' >
                            <?= form_error('safa_uo_contract_id') ?> 
                        </span>
                    </div>
                </div>
                
                
            </div>
            <div class="row-form" >
            
            <!--<div class="span6">
                    <div class="span4"><?= lang("uo_ea_id") ?>
                        <font style='color:red'>*</font>
                    </div>
                    <div class="span8" >
                            <?= form_dropdown("uo_contract_id", $uo_contracts,
                                    set_value("uo_contract_id", $contract_id), "id='contract_id'", "class='input-huge'") ?>
                        <span class="bottom" style='color:red' >
						<?= form_error('uo_contract_id') ?> 
                        </span>
                    </div>
                
                <div class="span8" >
                            <?= form_dropdown("uo_ea_id", $uo_eas,
                                    set_value("uo_ea_id", $uo_ea_id), "id='uo_ea_id'", "class='input-huge'") ?>
                        <span class="bottom" style='color:red' >
						<?= form_error('uo_ea_id') ?> 
                        </span>
                    </div>
                
                </div>
                
                --><div class="span6" >
                    <div class="span4">
<?= lang("transport_request_trip_id") ?>
                        
                    </div>
                    <div class="span8">
                            <?= form_dropdown("safa_trip_id",
                                    $ea_trips, set_value("safa_trip_id", $item->safa_trip_id),
                                    "id='trip_id'", "class='input-huge'") ?>
                        <span class="bottom" style='color:red' >
<?= form_error('safa_trip_id') ?> 
                        </span>
                    </div>
                </div>
                
                
                
                
            </div>
            <div class="row-form" >
                <div class="span6" >
                    <div class="span4">
                            <?= lang("transport_request_status") ?>
                        <font style='color:red'>*</font>
                    </div>
                    <div class="span8">
<?= form_dropdown("safa_internaltripstatus_id", $safa_intrernaltripstatus,
        set_value("safa_internaltripstatus_id", $item->safa_internaltripstatus_id), "id='safa_internaltripstatus_id'",
        "class='input-huge'") ?>
                        <span class="bottom" style='color:red' >
<?= form_error('safa_internaltripstatus_id') ?> 
                        </span>
                    </div>
                </div>
                <div class="span6">
                    <div class="span4">
<?= lang("transport_request_res_code") ?>
                        
                    </div>
                    <div class="span8" >
            <?= form_input("operator_reference",
                    set_value("operator_reference", $item->operator_reference)) ?>
                        <span class="bottom" style='color:red' >
<?= form_error('operator_reference') ?> 
                        </span>
                    </div>
                </div>
            </div>
                            <?php if (PHASE >= 2): ?>
                <div class="row-form">
                    <div class="span6">
                        <div class="span4"><?= lang("transport_request_transportername") ?>
                            <font style='color:red'>*</font>
                        </div>
                        <div class="span8" >
    <?= form_dropdown("safa_transporter_id", $transporters,
            set_value("safa_transporter_id", $item->safa_transporter_id), "", "class='input-huge'") ?>
                            <span class="bottom" style='color:red' >
    <?= form_error('safa_transporter_id') ?> 
                            </span>
                        </div>
                    </div>
                    
                    
                
                <div class="span6" >
                    <div class="span4">
                        <?= lang("transport_request_opertator") ?>
                        
                    </div>
                    <div class="span8" >
<?= form_dropdown("safa_ito_id", $safa_ito, set_value("safa_ito_id", $item->safa_ito_id),
        "id='safa_ito_id'", "class='input-huge'") ?>
                        <span class="bottom" style='color:red' >
<?= form_error('safa_ito_id') ?> 
                        </span>
                    </div>
                </div>
                
                    
                    
                </div>
<? endif; ?>
            <div class="row-form" >
                <div class="span6" >
                    <div class="span4" ><?= lang("transport_request_res_file") ?></div>
                    <div class="span8" >
                        <!--<div class="input-append file"  >
                            <input type="file" value="" name="userfile" />
                            <input type="text" name="text_input" value="<?= set_value('text_input', $item->attachement) ?>" />
                            <button type="button"  class="btn">Browse</button>
                        </div>
                        <div>
                            <a href="<?= base_url('static/uo_files/' . session('internaltrip_file_upload_time') . $item->attachement) ?>" target="_blank"  ><?= $item->attachement ?></a>
                        </div>
                        <span class="bottom" style='color:red' >
                        <?= form_error('userfile') ?> 
                        </span>

                    -->
                    	<input type="file" value="" name="transport_request_res_file" id="transport_request_res_file"/>
                        <span class="bottom" style='color:red' >
                            <?= form_error('userfile') ?> 
                        </span>
                        <a href="<?= base_url('static/temp/uo_files/' . $item->attachement) ?>" target="_blank"  ><?= $item->attachement ?></a>
                        
                    
                    </div>
                </div>
                            <? if (PHASE > 1): ?>
                    <div class="span6" >
                        <div class="span4" ><?= lang('confirmation_number') ?></div>
                        <div class="span8" >
                    <?
                    $arr_conf = explode('-', $item->confirmation_number);
                    $serial = (count($arr_conf) > 1 ? $arr_conf[1] : $arr_conf[0])
                    ?>
    <?= form_input("conf_number", set_value("conf_number", $serial),
            "disabled='disabled'") ?>
                            <span class="bottom" style='color:red' >
    <?= form_error('conf_number') ?> 
                            </span>
                        </div>
                    </div>
<? endif; ?> 
            </div>     
            <div class="toolbar bottom TAC">
                <input class="btn btn-primary" type="submit" name="submit" value="<?= lang('global_edit') ?>" />
                <a href='<?= site_url('uo/trip_internaltrip/index') ?>'    class="btn btn-primary"><?= lang("global_show_segments") ?></a>
            </div>
<?= form_close() ?>
        </div>
    </div>
    <div class="widget"> 





        <div class="widget-header">
            <div class="widget-header-icon Fright">
                <span class="icos-pencil2"></span>
            </div>

            <div class="widget-header-title Fright">
<?= lang('transport_request_internal_passages') ?>
            </div>

            <a href="<?= site_url("uo/trip_internalpassages/add/" . $trip_internal_id) ?>" class="btn  fancybox fancybox.iframe" style="<? if (lang('global_lang') == 'ar'): ?>float:left<? else: ?>float:right<? endif; ?>" ><?= lang('add_passges') ?></a>
        </div>



        <div class="table-responsive" >
            <!--- the part of internal passages---> 
            <table class="myTable" cellpadding="0" cellspacing="0">
                <thead>
                    <tr>
                        <th><?= lang('internalpassage_type') ?></th>
                        <th><?= lang('internalpassage_starthotel') ?></th>
                        <th><?= lang('internalpassage_endhotel') ?></th>
                        <th><?= lang('internalpassage_startdatatime') ?></th>
                        <th><?= lang('internalpassage_enddatatime') ?></th>
                        <th><?= lang('internalpassage_seatscount') ?></th>
                        <th><?= lang('internalpassage_note') ?></th>
                        <th><?= lang('global_actions') ?></th>
                    </tr>
                </thead>
                <tbody>
                        <? if (isset($internalpassages)) : ?>
                            <? foreach ($internalpassages as $inernalpassage): ?>
                            <tr>
                                <td><?= item("safa_internalsegmenttypes", name(),
                                array("safa_internalsegmenttype_id" => $inernalpassage->safa_internalsegmenttype_id)); ?></td>
                                <? if ($inernalpassage->safa_internalsegmenttype_id == 1): ?>
                                    <td><?= item(" erp_ports", name(), array("erp_port_id" => $inernalpassage->erp_port_id)); ?></td>
                                    <td><?= item("erp_hotels", name(), array("erp_hotel_id" => $inernalpassage->erp_end_hotel_id)); ?></td>
                                <? endif; ?>  
                                    <? if ($inernalpassage->safa_internalsegmenttype_id == 2): ?>
                                    <td><?= item("erp_hotels", name(), array("erp_hotel_id" => $inernalpassage->erp_start_hotel_id)); ?></td>
                                    <td><?= item(" erp_ports", name(), array("erp_port_id" => $inernalpassage->erp_port_id)); ?></td>
                                    <? endif; ?>
                                    <? if ($inernalpassage->safa_internalsegmenttype_id == 3): ?>
                                    <td><?= item("erp_hotels", name(), array("erp_hotel_id" => $inernalpassage->erp_start_hotel_id)); ?></td>
                                    <td><?= item("safa_tourismplaces", name(),
                                    array("safa_tourismplace_id" => $inernalpassage->safa_tourism_place_id)); ?></td>
                                    <? endif; ?>
                                    <? if ($inernalpassage->safa_internalsegmenttype_id == 4): ?>
                                    <td><?= item("erp_hotels", name(), array("erp_hotel_id" => $inernalpassage->erp_start_hotel_id)); ?></td>
                                    <td><?= item(" erp_hotels", name(), array("erp_hotel_id" => $inernalpassage->erp_end_hotel_id)); ?></td>
        <? endif; ?>
                                <td>
        <? if (isset($inernalpassage->start_datetime) && $inernalpassage->start_datetime != null): ?>
                                        <? $date = date_create($inernalpassage->start_datetime); ?>
                                        <span><?= date_format($date, "m-d H:i"); ?></span>
                                        <? endif; ?>   
                                </td>
                                <td>
        <? if (isset($inernalpassage->end_datetime) && $inernalpassage->end_datetime != null): ?>
                                <? $date_end = date_create($inernalpassage->end_datetime); ?>    
                                        <span><?= date_format($date_end, "m-d H:i"); ?></span>
                            <? endif; ?> 
                                </td>
                                <td><? if (isset($inernalpassage->seats_count)): ?> <?= $inernalpassage->seats_count ?> <? endif; ?>  </td>
                                <td><? if (isset($inernalpassage->notes)): ?> <?= $inernalpassage->notes ?> <? endif; ?>  </td>
                                <td class="TAC">
                                    <a  class="fancybox fancybox.iframe " href="<?= site_url("uo/trip_internalpassages/edit/" . $inernalpassage->safa_internalsegment_id) ?>"><span class="icon-pencil"></span></a> 
        <? if (!check_id('safa_internalsegment_log', 'safa_internalsegment_id',
                        $inernalpassage->safa_internalsegment_id)): ?>
                                        <a href="<?= site_url("uo/trip_internalpassages/delete/" . $inernalpassage->safa_internalsegment_id . "/" . $trip_internal_id) ?>" onclick="return window.confirm('<?= lang('global_are_you_sure_you_want_to_delete') ?>')" ><span class="icon-trash"></span></a>
        <? endif; ?>
                                </td>
                            </tr>  

    <? endforeach; ?>
<? endif; ?>  
                </tbody>
            </table>  
        </div>
    </div>

</div>

<script type="text/javascript">
$(document).ready(function() {
    var config = {
        '.chosen-select': {width: "100%"},
        '.chosen-select-deselect': {allow_single_deselect: true},
        '.chosen-select-no-single': {disable_search_threshold: 10},
        '.chosen-select-no-results': {no_results_text: 'Oops, nothing found!'},
        '.chosen-select-width': {width: "95%"}
    }
    for (var selector in config) {
        $(selector).chosen(config[selector]);
    }

    // binds form submission and fields to the validation engine
    $("#frm_trip_internaltrip").validationEngine({
            prettySelect : true,
            useSuffix: "_chosen",
            promptPosition : "topRight:-150"
            //promptPosition : "bottomLeft"
    });

 
});
</script>


<script>
    $(document).ready(function() {
        $('.fancybox').fancybox({
            afterClose: function() {
                location.reload();
            }
        });
    });
</script>
<script>

$('#safa_uo_contract_id').live('change', function() { 
    var safa_uo_contract_id = $("#safa_uo_contract_id").val();
    $.get('<?= site_url("uo/trip_internaltrip/get_uo_contract_trips") ?>/' + safa_uo_contract_id, function(data) {
        $("#trip_id").html(data);
        $("#trip_id").val('<?= $this->input->post('safa_trip_id') ?>');
    });
});

    $(document).ready(function() {

        var safa_uo_contract_id = $("#safa_uo_contract_id").val();

        $("#safa_uo_contract_id").change(function() {
            $.get('<?= site_url("uo/trip_internaltrip/get_uo_contract_trips") ?>/' + $(this).val(), function(data) {
                $("#trip_id").html(data);
            });
        });
        $.get('<?= site_url("uo/trip_internaltrip/get_uo_contract_trips") ?>/' + safa_uo_contract_id, function(data) {
            $("#trip_id").html(data);
<? if (isset($_POST['safa_trip_id'])): ?>
                $("#trip_id").val('<?= $this->input->post('safa_trip_id') ?>');
<? else: ?>
                $("#trip_id").val('<?= $item->safa_trip_id ?>');
<? endif; ?>
        });
    });
</script>
<script>
    $('.datepicker').datepicker({
        dateFormat: "yy-mm-dd",
    });
</script>
<script>
    $(document).ready(function() {
        $(".myTable").dataTable(
                {bSort: false,
                    bAutoWidth: true,
                    "iDisplayLength": false, // can be removed for basic 10 items per page
                    "sPaginationType": false,
                    "bPaginate": false,
                    "bInfo": false}
        );
    });
</script>
<script>
    $(document).ready(function() {
        /* input file */
        $(".file .btn, .file input:text").click(function() {
            var block = $(this).parent('.file');
            block.find('input:file').change(function() {
                var file_arr = $(this).val().split('\\');
                var file_name = file_arr[file_arr.length - 1];
                block.find('input:text').val(file_name);
            });
        });
    });
</script>
<script type="text/javascript">
    $(function() {
        $('#datetimepicker1').datetimepicker({
            language: 'pt-BR',
            pickTime: false
        });
    });
</script>
<?php if (PHASE > 1): ?>
    <script>
        $(document).ready(function() {
            $("#safa_ito_id").change(function() {
                if ($("#safa_ito_id").val()) {
                    $("#safa_internaltripstatus_id").val(4);
                } else {
                    $("#safa_internaltripstatus_id").val(3);
                }
            });
            $("#safa_internaltripstatus_id").change(function() {
                if ($("#safa_ito_id").val()) {
                    if ($("#safa_internaltripstatus_id").val() != 4)
    <? if (lang('global_lang') == 'ar'): ?>
                        alert("قم باختيار مؤكد من شركة العمرة");
    <? else: ?>
                        alert("choose confirmed by uo");
    <? endif; ?>
                    $("#safa_internaltripstatus_id").val(4);
                } else {
                    if ($("#safa_internaltripstatus_id").val() != 3)
    <? if (lang('global_lang') == 'ar'): ?>
                        alert("قم باختيار بانتظار مشغل النقل");
    <? else: ?>
                        alert("choose pending ito");
    <? endif; ?>
                    $("#safa_internaltripstatus_id").val(3);
                }
            });
        });
    </script> 
<? endif; ?>