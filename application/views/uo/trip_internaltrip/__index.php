
<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); ?>

<div class="row-fluid">
<div style="margin-bottom:10px; text-align:left; border:3px solid #FFF; background:url(<?= IMAGES ?>/backgrounds/bg_box_head.jpg); padding:5px 10px; height:30px;">
    <a title="<?= lang('global_add_new_record') ?>" href="<?= site_url("uo/trip_internaltrip/add") ?>" class="btn btn-primary"><?= lang('global_add_new_record')?></a>
    <div style="float:right; text-align:right; font-size:14px; line-height:30px; color:#333;"><a href="<?=  site_url('uo/dashboard')?>"><?=lang('parent_node_title')?></a> <span style="color:#80693d"> <img src="<?= IMAGES ?>/jbreadcrumb/Chevron-ar.gif" width="5" height="20"></span> <?=lang('node_title')?> </div>
 </div>
    <div class="widget">
        <div class="head dark">
           <div class="icon"><i class="icos-search"></i></div>
            <h2><?= lang('global_search') ?></h2>  
        </div>
        <?= form_open("","method='get'") ?>
        <div class="block-fluid">
            <div class="row-form">
                <div class="span6">
                    <div class="span2"><?= lang('transport_request_status') ?></div>
                    <div class="span10">
                        <?= form_dropdown("safa_tripstatus_id", $safa_intrernaltripstatus, set_value("safa_tripstatus_id")) ?>
                    </div>
                </div>
                <div class="span6">
                    <div class="span2"><?= lang('transport_request_opertator') ?></div>
                    <div class="span10">
                        <?= form_dropdown("safa_ito_id", $safa_ito, set_value("safa_ito_id")) ?>
                    </div>
                </div>
            </div>
            <div class="row-form">
                <div class="span6">
                    <div class="span2"><?= lang('transport_request_contract') ?></div>
                    <div class="span10">
                        <?= form_dropdown("uo_contract_id", $uo_contracts, set_value("uo_contract_id"), " id='contract_id' ") ?>
                    </div>
                </div>
                <div class="span6">
                    <div class="span2"><?= lang('transport_request_trip_id') ?></div>
                    <div class="span10">
                        <?= form_dropdown("safa_trip_id", array("0" => lang('global_select_from_menu')), set_value("safa_trip_id"), "id='trip_id'") ?> 
                    </div>
                </div>
            </div>
            <?php if (PHASE >=2 ):?>
            <div class="row-form">
                <div class="span6">
                    <div class="span2"><?= lang('transport_request_transportername') ?></div>
                    <div class="span10">
                        <?= form_dropdown("safa_transporter_id",$transporters, set_value("safa_transporter_id"), "  ") ?>
                    </div>
                </div> 
            </div>
            <?  endif;?>
            <div class="toolbar bottom TAC">
                <input type="submit"  class="btn btn-primary" name="search" value="<?= lang('global_search') ?>" />
            </div>
        </div>
        <?= form_close() ?>
    </div>
    <div class="widget">
        <div class="head dark">
            <div class="icon"><i class="icos-pencil2"></i></div>
            <h2><?= lang('transport_requests') ?></h2>                       
        </div>                
        <div class="block-fluid">
            <table class='myTable' width='100%'>
                <thead>
                    <tr>
                        <th><?= lang('transport_request_res_code') ?></th>
                        <th><?= lang('transport_request_trip_id') ?></th>
                        <th><?= lang('transport_request_contract') ?></th>                        
                        <th><?= lang('transport_request_opertator') ?></th>
                        <?php if (PHASE>=2):?>
                        <th><?= lang('transport_request_transportername') ?></th>
                        <?  endif;?>
                        <th><?= lang('transport_request_status') ?></th>
                        <?if(PHASE>1):?>
                            <th><?= lang('confirmation_number') ?></th>
                        <?endif;?>
                        <th><?= lang('transport_request_internalpassage_num') ?></th>
                        <th><?= lang('global_actions') ?></th>
                    </tr>
                </thead>
                <tbody>
                    
                    <? if (isset($items)): ?>
                        <? foreach ($items as $item): ?>
                            <tr>
                                <td><span class='label'style='background-color:<?=item("safa_internaltripstatus",'color',array('safa_internaltripstatus_id'=>$item->safa_internaltripstatus_id))?>' ><?= $item->operator_reference?></span></td>
                                <td><?= $item->trip_name ?></td>
                                <td><?=$item->contract_name?></td>
                                <td><?=$item->ito_name?></td>
                                 <?php if (PHASE>=2):?>
                                <td><?=$item->transportername?></td>
                                <?  endif;?>
                                <td><?=$item->status_name?></td>
                                 <?if(PHASE>1):?>
                                    <td><?=$item->confirmation_number?></td>
                                <?endif;?>
                                <td><?=$item->num_segments?></td>
                                <td class='TAC'> 
                                    <a title='<?=lang('global_edit')?>' href="<?=  site_url('uo/trip_internaltrip/edit/'.$item->safa_trip_internaltrip_id)?>"><span class="icon-pencil"></span></a>
                                    <?if(check_id("safa_internalsegments","safa_trip_internaltrip_id",$item->safa_trip_internaltrip_id)==0):?>
                                         <a title='<?=lang('global_delete')?>' href="<?=  site_url('uo/trip_internaltrip/delete/'.$item->safa_trip_internaltrip_id)?> " onclick="return window.confirm('<?=lang('global_are_you_sure_you_want_to_delete')?>')"   ><span class="icon-trash"></span></a>
                                    <?endif;?>
                                </td>
                            </tr> 
                        <? endforeach; ?> 
                    <? endif; ?>
                </tbody>
            </table>
        </div> 
        <div class="block-fluid">
            <table width="100%">
                <thead>
                    <tr>
                        <?if(isset($safa_intrernaltripstatus_color)):?>
                        <?$name=name()?>
                        <?foreach($safa_intrernaltripstatus_color as $status):?>
                        <th><span class='label'style='background-color:<?=$status->color?>'><?=$status->$name?></span><?//$status->code?></th>
                        <?  endforeach;?>
                        <?endif;?>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>  
        </div>     
    </div>
    <div class='row-fluid' >
       <?=$pagination?> 
    </div>
</div>

<script>
    $(document).ready(function() {
        $("#contract_id").change(function() {
            //sending the request to get the trips//
            $.get('<?= site_url("uo/trip_internaltrip/get_ea_trips") ?>/' + $(this).val(), function(data) {
                $("#trip_id").html(data);
            });
        });
        var contract_id=$("#contract_id").val();
        $.get('<?= site_url("uo/trip_internaltrip/get_ea_trips") ?>/' + contract_id, function(data) {
              $("#trip_id").html(data);
             $("#trip_id").val('<?=$this->input->get('safa_trip_id')?>')
        });
    });
</script>
<script>
    $(document).ready(function() {
        $(".myTable").dataTable(
                {bSort:true,
                    bAutoWidth: true,
                    "iDisplayLength": false, // can be removed for basic 10 items per page
                    "sPaginationType": false,
                    "bPaginate": false,
                    "bInfo": false}
        );
    });
</script>

 
