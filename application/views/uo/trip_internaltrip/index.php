
<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); ?>
<div class="row-fluid">

 
 <div class="span12">
    <div class="widget">
    <div class="path-container Fright">
        <div class="icon"><i class="icos-pencil2"></i></div> 
         <div class="path-name Fright">
               <a href="<?= site_url('uo/dashboard') ?>"><?= lang('parent_node_title') ?></a>
        </div>    
        <div class="path-arrow Fright">
            </div>
            <div class="path-name Fright">
                <span><?= lang('node_title') ?></span>
        </div>
    </div> 
    
    <a title="<?= lang('global_add_new_record') ?>" href="<?= site_url("uo/trip_internaltrip/add") ?>" class="btn Fleft"><?= lang('global_add_new_record')?></a>
    
    </div> 
</div>
 
 
    <div class="widget">
        
        
        <div class="widget-header">
	
	            <div class="widget-header-icon Fright">
	                <span class="icos-pencil2"></span>
	            </div>
	
	            <div class="widget-header-title Fright">
	                <?= lang('global_search') ?>
	            </div>
		</div>
        
        
        <?= form_open("","method='get'") ?>
        <div class="block-fluid">
            <div class="row-form">
                <div class="span6">
                    <div class="span2"><?= lang('transport_request_status') ?></div>
                    <div class="span10">
                        <?= form_dropdown("safa_tripstatus_id", $safa_intrernaltripstatus, set_value("safa_tripstatus_id")) ?>
                    </div>
                </div>
                <div class="span6">
                    <div class="span2"><?= lang('transport_request_opertator') ?></div>
                    <div class="span10">
                        <?= form_dropdown("safa_ito_id", $safa_ito, set_value("safa_ito_id"),"class='input-huge'") ?>
                    </div>
                </div>
            </div>
            <div class="row-form">
                <div class="span6">
                    <div class="span2"><?= lang('transport_request_contract') ?></div>
                    <div class="span10">
                        <?= form_dropdown("safa_uo_contract_id", $safa_uo_contracts, set_value("safa_uo_contract_id"), " id='safa_uo_contract_id' class='validate[required] input-huge '") ?>
                    </div>
                </div>
                <div class="span6">
                    <div class="span2"><?= lang('transport_request_trip_id') ?></div>
                    <div class="span10">
                        <?= form_dropdown("safa_trip_id", array("0" => lang('global_select_from_menu')), set_value("safa_trip_id"), "id='trip_id' class='input-huge'") ?> 
                    </div>
                </div>
            </div>
            <?php if (PHASE >=2 ):?>
            <div class="row-form">
                <div class="span6">
                    <div class="span2"><?= lang('transport_request_transportername') ?></div>
                    <div class="span10">
                        <?= form_dropdown("safa_transporter_id",$transporters, set_value("safa_transporter_id"), "class='input-huge'") ?>
                    </div>
                </div> 
            </div>
            <?  endif;?>
            <div class="toolbar bottom TAC">
                <input type="submit"  class="btn btn-primary" name="search" value="<?= lang('global_search') ?>" style="height: 28px" />
            </div>
        </div>
        <?= form_close() ?>
    </div>
    <div class="widget">
        
        
        
        <div class="widget-header">
	
	            <div class="widget-header-icon Fright">
	                <span class="icos-pencil2"></span>
	            </div>
	
	            <div class="widget-header-title Fright">
	                <?= lang('transport_requests') ?>
	            </div>
		</div>
        <div class="widget-container">           
        <div class="table-responsive">
            <table class='fsTable'>
                <thead>
                    <tr>
                    	<th><?= lang('serial') ?></th>
                        <th><?= lang('confirmation_number') ?></th>
                        <th><?= lang('transport_request_trip_id') ?></th>
                        <th><?= lang('transport_request_contract') ?></th>                        
                        <th><?= lang('transport_request_opertator') ?></th>
                        <?php if (PHASE>=2):?>
                        <th><?= lang('transport_request_transportername') ?></th>
                        <?  endif;?>
                        <th><?= lang('transport_request_status') ?></th>
                        <?if(PHASE>1):?>
                            <th><?= lang('transport_request_res_code') ?></th>
                        <?endif;?>
                        <th><?= lang('transport_request_internalpassage_num') ?></th>
                        <th><?= lang('global_actions') ?></th>
                    </tr>
                </thead>
                <tbody>
                    
                    <? if (isset($items)): ?>
                        <? foreach ($items as $item): ?>
                            <tr>
                            	<td><?= $item->serial ?></td>
                                <td><span class='label'style='background-color:<?=item("safa_internaltripstatus",'color',array('safa_internaltripstatus_id'=>$item->safa_internaltripstatus_id))?>'>
                                        <? $arr_conf= explode('-',$item->confirmation_number); if(count($arr_conf)>1): echo $arr_conf[1]; else: echo $arr_conf[0];  endif;  ?>
                                    </span>
                                </td>
                                <td><?= $item->trip_name ?></td>
                                <td>
                                <?php
                                echo $item->contract_name;
                                
//                                if($item->safa_uo_id!='') {
//                                	$this->contracts_model->by_eas_id = $item->erp_company_id;
//                                	$this->contracts_model->safa_uo_id = session('uo_id');
//                                	$contracts_rows = $this->contracts_model->search();
//                                } else {
//                                	$this->contracts_model->by_eas_id = $item->safa_ea_id;
//                                	$this->contracts_model->safa_uo_id = session('uo_id');
//                                	
//									$contracts_rows = $this->contracts_model->search();
//                                }
//
//                                if(count($contracts_rows)>0) {
//                                	if(name()=='name_ar'){
//										echo $contracts_rows[0]->contract_name;
//									} else {
//										echo $contracts_rows[0]->contract_name;
//									}
//                                }
                                ?>
                                </td>
                                <td><?=$item->ito_name?></td>
                                 <?php if (PHASE>=2):?>
                                <td><?=$item->transportername?></td>
                                <?  endif;?>
                                <td><?=$item->status_name?></td>
                                 <?if(PHASE>1):?>
                                    <td><?=$item->operator_reference?></td>
                                <?endif;?>
                                <td><?=$item->num_segments?></td>
                                <td class='TAC' title="<?= htmlspecialchars($item->ito_notes,ENT_QUOTES) ?>" > 
                                    <a title='<?=lang('global_edit')?>' href="<?=  site_url('uo/trip_internaltrip/edit/'.$item->safa_trip_internaltrip_id)?>"><span class="icon-pencil"></span></a>
                                    <!--<?if(check_id("safa_internalsegments","safa_trip_internaltrip_id",$item->safa_trip_internaltrip_id)==0):?>
                                         <a title='<?=lang('global_delete')?>' href="<?=  site_url('uo/trip_internaltrip/delete/'.$item->safa_trip_internaltrip_id)?> "  name ='<?= $item->contract_name ?>' onclick="return window.confirm('<?=lang('global_are_you_sure_you_want_to_delete')?>') " ><span class="icon-trash"></span></a>
                                    <?endif;?>
                                    -->
                                    <a title='<?=lang('global_delete')?>' href="<?=  site_url('uo/trip_internaltrip/delete/'.$item->safa_trip_internaltrip_id)?> "  name ='<?= $item->contract_name ?>' onclick="return window.confirm('<?=lang('global_are_you_sure_you_want_to_delete')?>') " ><span class="icon-trash"></span></a>
                                    
                                    <a title='<?=lang('global_print')?>' target="_blank" href="<?=  site_url('uo/trip_internaltrip/internalsegments_report/'.$item->safa_trip_internaltrip_id)?>"><span class="icon-print"></span></a>
                                </td>
                            </tr> 
                        <? endforeach; ?> 
                    <? endif; ?>
                </tbody>
            </table>
        </div> 
        
   <div class="table-responsive">
            <table>
                <thead>
                    <tr>
                        <?if(isset($safa_intrernaltripstatus_color)):?>
                        <?$name=name()?>
                        <?foreach($safa_intrernaltripstatus_color as $status):?>
                        <th><span class='label'style='background-color:<?=$status->color?>'><?=$status->$name?></span><?//$status->code?></th>
                        <?  endforeach;?>
                        <?endif;?>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>  
        </div>  
        </div>
    </div>
    <div class='row-fluid' >
<!--       <?=$pagination?> -->
    </div>
</div>

<script>
    $(document).ready(function() {
        $("#safa_uo_contract_id").change(function() {
            //sending the request to get the trips//
            $.get('<?= site_url("uo/trip_internaltrip/get_uo_contract_trips") ?>/' + $(this).val(), function(data) {
                $("#trip_id").html(data);
            });
        });
        var safa_uo_contract_id=$("#safa_uo_contract_id").val();
        $.get('<?= site_url("uo/trip_internaltrip/get_uo_contract_trips") ?>/' + safa_uo_contract_id, function(data) {
              $("#trip_id").html(data);
             $("#trip_id").val('<?=$this->input->get('safa_trip_id')?>')
        });
    });
</script>
<script>
    $(document).ready(function() {
        $(".myTable").dataTable(
                {bSort:false,
                    bAutoWidth: true,
                    "iDisplayLength": false, // can be removed for basic 10 items per page
                    "sPaginationType": false,
                    "bPaginate": false,
                    "bInfo": false}
        );
    });
</script>

 
