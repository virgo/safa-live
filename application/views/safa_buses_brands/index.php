<div class="widget">
    <a class="btn Fleft" href="<?php echo  site_url('safa_buses_brands/manage') ?>">
      <?php echo  lang('global_add') ?>
    </a>
    <div class="path-container Fright">
    	<div class="path-name Fright">
            <a href="<?= site_url('uo/dashboard') ?>"><?= lang('global_system_management') ?></a>
        </div>
        <div class="path-arrow Fright"></div>
        <div class="path-name Fright"> <?php echo  lang('title') ?></div>
    </div>
</div>

<div class="widget">
    <div class="widget-header">
        <div class="widget-header-icon Fright"> <span class="icos-pencil2"></span> </div>
        <div class="widget-header-title Fright">
            <?php echo  lang('title') ?>
        </div>
        
    </div>
     <div class="widget-container">
          <div class="table-responsive">
            <table cellpadding="0" cellspacing="0" class="fsTable" >
                <thead>
                    <tr>
                        <th><?php echo  lang('name_ar') ?></th>
                        <th><?php echo  lang('name_la') ?></th>
                        <th><?php echo  lang('actions') ?></th>
                    </tr>
                </thead>
                <tbody id="had">
                <? if(ensure($items)) { ?>
                    <? foreach($items as $item) {?>
                    <tr rel="<?php echo  $item->safa_buses_brands_id ?>">
						
                        <td>
                            <?php echo   $item->name_ar ; ?>
                        </td>
                        <td>
                            <?php echo   $item->name_la ; ?>
                        </td>
                        
                        <td class="TAC">
                        <a href="<?= site_url('safa_buses_brands/manage/' . $item->safa_buses_brands_id) ?>"><span class="icon-pencil"></span></a>
                        <a href="<?= site_url('safa_buses_brands/delete/' . $item->safa_buses_brands_id) ?>" onclick="return confirm('<?= lang('global_are_you_sure_you_want_to_delete')?>')"><span class="icon-trash"></span></a>
                        </td>
                    </tr>
                    <? } ?>
                <? } ?> 
                </tbody>
            </table>
        </div>
    </div>
</div>

