<?php
$name_ar = '';
$name_la = '';

$screen_title = lang('add_title');

if (isset($item)) {
    $name_ar = $item->name_ar;
    $name_la = $item->name_la;
    
    $screen_title = lang('edit_title');
}
?>
<script>.widget{width:98%;}</script>

<div class="widget">

    <div class="path-container Fright">
        <div class="path-name Fright">
            <a href="<?php echo site_url() ?>"><?php echo lang('global_system_management') ?></a>
        </div>
        <div class="path-arrow Fright"></div>
        <div class="path-name Fright"> <a href="<?php echo site_url('safa_buses_brands') ?>"><?php echo lang('title') ?></a></div>

        <div class="path-arrow Fright"></div>
        <div class="path-name Fright"><?php echo $screen_title; ?></div>

    </div>
</div>


<?php echo form_open_multipart(false, 'id="frm_safa_buses_brands" ') ?>


<div class="widget">
    <div class="widget-header">

        <div class="widget-header-icon Fright">
            <span class="icos-pencil2"></span>
        </div>

        <div class="widget-header-title Fright">
<?php echo $screen_title; ?>

        </div>
    </div>

    <div class="widget-container">  
<? if (validation_errors()) { ?>
    <?php echo validation_errors(); ?>        
        <? } ?>

        <div class="row-form">
            <div class="span6">
                <div class="span4 TAL Pleft10">
<?php echo lang('name_ar') ?> <font style='color:red' >*</font>
                </div>
                <div class="span8">
                    <?php echo form_input('name_ar', set_value("name_ar", $name_ar), " style='' id='name_ar' class='validate[required] input-full' ") ?>
                </div>
            </div>

            <div class="span6">
                <div class="span4 TAL Pleft10">
<?php echo lang('name_la') ?> <font style='color:red' >*</font>
                </div>
                <div class="span8">
                    <?php echo form_input('name_la', set_value("name_la", $name_la), " style='' id='name_la' class='validate[required] input-full' ") ?>
                </div>
            </div>

        </div>
        
        <script>models_counter = 0</script>
        <div class="row-form">
        <div class="table-responsive">
            <table cellpadding="0" cellspacing="0">
                <thead>
                    <tr>
                        <th><?php echo lang('name_ar') ?></th>
                        <th><?php echo lang('name_la') ?></th>
                        <th><a class="btn " title="<?php echo lang('global_add') ?>" href="javascript:void(0)" onclick="add_models('N' + models_counter++);">
                                <?php echo lang('global_add') ?>    
                        </a></th>
                    </tr>
                </thead>
                <tbody class="models" id='models'>
                    <? if (isset($item_models)) { ?>
                        <? if (check_array($item_models)) { ?>
                            <? foreach ($item_models as $item_model) { ?>
                                <tr rel="<?php echo $item_model->safa_buses_models_id ?>">
                                    
                                    <td><?php
                                        echo form_input('model_name_ar[' . $item_model->safa_buses_models_id . ']'
                                                , set_value('model_name_ar[' . $item_model->safa_buses_models_id . ']', $item_model->name_ar)
                                                , ' class="validate[required] input-huge" style="width:250px"')
                                        ?>
                                    </td>

									<td><?php
                                        echo form_input('model_name_la[' . $item_model->safa_buses_models_id . ']'
                                                , set_value('model_name_la[' . $item_model->safa_buses_models_id . ']', $item_model->name_la)
                                                , ' class="validate[required] input-huge" style="width:250px"')
                                        ?>
                                    </td>



                                    <td class="TAC">
                                        <a href="javascript:void(0)" onclick="delete_models(<?php echo $item_model->safa_buses_models_id ?>, true)"><span class="icon-trash"></span></a>
                                    </td>
                                </tr>
        <? } ?>
    <? } ?>
<? } ?>
                </tbody>
            </table>
        </div>
        </div>
        

    </div>
</div>


<div class="widget TAC">
    <input type="submit" class="btn" name="smt_send" value="<?php echo lang('global_submit') ?>" style="margin:10px;padding: 5px;height: auto">
</div>


<?php echo form_close() ?>


<div class="footer"></div>


<script type="text/javascript">
    $(document).ready(function() {
        var config = {
            '.chosen-select': {width: "100%"},
            '.chosen-select-deselect': {allow_single_deselect: true},
            '.chosen-select-no-single': {disable_search_threshold: 10},
            '.chosen-select-no-results': {no_results_text: 'Oops, nothing found!'},
            '.chosen-select-width': {width: "95%"}
        }
        for (var selector in config) {
            $(selector).chosen(config[selector]);
        }

        // binds form submission and fields to the validation engine
        $("#frm_safa_buses_brands").validationEngine({
            prettySelect: true,
            useSuffix: "_chosen",
            promptPosition: "topRight:-150"
                    //promptPosition : "bottomLeft"
        });
    });

</script>

<script type="text/javascript">

function add_models(id) {

        var new_row = [
                "<tr rel=\"" + id + "\">",

                '     <td><?php echo form_input("model_name_ar[' + id + ']", FALSE, 'class="validate[required] input-huge" ') ?>',
     			"    </td>",
     			'     <td><?php echo form_input("model_name_la[' + id + ']", FALSE, 'class="validate[required] input-huge" ') ?>',
     			"    </td>",
                "    <td class=\"TAC\">",
                "        <a href=\"javascript:void(0)\" onclick=\"delete_models('" + id + "')\"><span class=\"icon-trash\"></span></a>",
                "    </td>",
                "</tr>"
        ].join("\n");

        $('#models').append(new_row);

    }


    function delete_models(id, database)
    {
        if (typeof database == 'undefined')
        {
            $('.models').find('tr[rel="' + id + '"]').remove();
        }
        else
        {
            $('.models').find('tr[rel=' + id + ']').remove();
            var hidden_input = '<input type="hidden" name="buses_models_remove[]" value="' + id + '" />';
            $('#models').append(hidden_input);
        }
    }

</script>