<link href="<?= NEW_CSS_JS . '_' . lang('DIR') ?>/new_search/css/style.css" rel="stylesheet" type="text/css" />
<link href="<?= NEW_CSS_JS . '_' . lang('DIR') ?>/new_search/css/extensions.css" rel="stylesheet" type="text/css" />
<div class="container container_12">
 <div class="grid_4" id="leftcolumn">
        <div class="box radius">
            <!-- Search form -->
            <div class="padded">
                <h2 class="headline radius" style="
    text-align: center;
    font-size: 20px;
">بحث الفنادق</h2>
               <form action="http://localhost/safa-live/search_hotel" method="get" accept-charset="utf-8" autocomplete="off">                    <!-- calendar image container -->
                    <span style="display:none;"><img class="trigger" id="trigger" alt="" src="img/blank.gif"></span>
                    <fieldset class="radius">
                        <p>
                            <label class="" for="destination">المدينة</label><br>
                            <select id="sortOrder" name="sortOrder">
                                <option title="اختر المدينه" value="PRICE_ASC">اختر المدينه</option>
                                <option title="Sort by price - low to high" value="PRICE_DESC">Price - high to low</option>
                                <option title="Sort by star rating - high to low" value="STAR_RATING_DESC">Star rating - high to low</option>
                                <option title="Sort by star rating - low to high" value="STAR_RATING_ASC">Star rating - low to high</option>
                                <option title="Sort by guest rating" value="GUEST_RATING_ASC">Guest rating</option>
                            </select>
                        </p>
                        <hr class="thin">
                        <p>
                            <label class="" for="startdate">مستوى الفندق</label><br><select id="sortOrder" name="sortOrder">
                                <option title="Sort by price - low to high" value="PRICE_ASC">اختر المستوي</option>
                                <option title="Sort by price - low to high" value="PRICE_DESC">Price - high to low</option>
                                <option title="Sort by star rating - high to low" value="STAR_RATING_DESC">Star rating - high to low</option>
                                <option title="Sort by star rating - low to high" value="STAR_RATING_ASC">Star rating - low to high</option>
                                <option title="Sort by guest rating" value="GUEST_RATING_ASC">Guest rating</option>
                            </select>
                        </p>
                        <hr class="thin">
                        <p>
                            <label class="" for="enddate">الاسم باللغة العربية</label><br>
                            <input type="text" id="destination" class="full" value="" name="destination"><span class="nights"></span>
                        </p>
                        <hr class="thin">
                        <p>
                            <label class="" for="sortOrder">الاسم باللغة الانجليزية</label><br>
                            <input type="text" id="destination" class="full" value="" name="destination">
                        </p>
                        <input type="submit" class="btn btn-custom radius fr" value="بحث »">
                    </fieldset>
                </form>
            </div>
            <!-- End of Search form -->
        </div>
    </div>
    <div class="grid_8 searchpanel box radius">
        <h3 class="">20 فندق / 16 فندق متاح</h3>

        <p><span class="fr"><a href="#">&lt; الصفحه التاليه</a> | <a href="#">الصفحه السابقه</a></span> <b>عرض 1 – 10</b></p>
        <p class="box padded">
         اظهار حسب:  ·  · <a href="#">الافضل</a> · <a href="#">نجوم (1..5)</a> ·  <a href="#">نجوم (4..1)</a> · <a href="#">الاراء</a>
        </p>

        <!-- Search item -->
        <div class="grid_8 searchpanel">
            <div class="infopanel padded">
                <a class="previewimg nyroModal fl" href="imgs/hotels/room-1.jpg" title="Hotel Name"><img src="http://localhost/safa-live/static/img/imgs/hotels/room-1t.jpg" alt=""></a>
                <small class="fr tr">
                    <a href="#">13 مشاهده</a><br><b>6.9</b> / 10</small>

                <h3><a href="#">فندق الواحه</a> 
                    <img src="http://localhost/safa-live/static/img/imgs/star.png" alt="*">
                    <img src="http://localhost/safa-live/static/img/imgs/star.png" alt="*">
                    <img src="http://localhost/safa-live/static/img/imgs/star.png" alt="*">
                    <img src="http://localhost/safa-live/static/img/imgs/star.png" alt="*"></h3>
                <div class="address">مكه, المملكه السعوديه [ <a href="#">علي الخريطه</a> ]</div>
                <p>أفخم وأحدث الفنادق بمكة المكرمة.يقع الفندق بجانب الحرم مباشرة، من جهة باب الملك عبدالعزيز (عكس جهة الصفا والمروة) .يعتبر هذا الموقع افضل موقع مرتب ومميز بجانب الحرم.
الفندق هو أحد فنادق مشروع وقف الملك عبدالعزيز، وهو مشروع ضخم يتكون من مركز تجاري كبير ومجموعة من الأبراج الملحقة بالمركز. <a href="#">المزيد »</a></p>
                <div class="clear">&nbsp;</div>
            </div>
            <hr class="thin">
            <!-- End of Search item -->

            <!-- Search item --><!-- End of Search item -->

            <!-- Search item -->

            <!-- End of Search item -->

            <!-- Search item --><!-- End of Search item -->

            <!-- Search item --><!-- End of Search item -->

            <p class="box padded"><span class="fr"><a href="#">الصفحه السابقه&gt;</a></span><a href="#">&lt; الصفحه التاليه</a></p>

            <p>&nbsp;</p>
            <!-- Shortcut buttons -->
            <div class="fr">
                <button class="box radius" title="Print this page" onclick="window.print();"><img src="http://localhost/safa-live/static/img/imgs/print.gif" alt="Print"></button>
                <button class="box radius" title="Email to a friend"><img src="http://localhost/safa-live/static/img/imgs/email.gif" alt="Email"></button>
                <button class="box radius" title="Bookmark"><img src="http://localhost/safa-live/static/img/imgs/bookmark.gif" alt="Bookmark"></button>
            </div>
            <!-- End of Shortcut buttons -->
            <div class="clear">&nbsp;</div>
        </div>
    </div>
</div>
</div>  
