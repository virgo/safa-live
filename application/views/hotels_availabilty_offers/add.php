<style>
.coll_open {
cursor:pointer;
}
.coll_close {
cursor:pointer;
}
</style>

<div class="widget">
    
    <div class="path-container Fright">
    	<div class="path-name Fright">
            <a href="<?php echo  site_url() ?>"><?php echo  lang('global_system_management') ?></a>
        </div>
        <div class="path-arrow Fright"></div>
        <div class="path-name Fright"> <a href="<?php echo  site_url('hotels_availabilty_offers') ?>"><?php echo  lang('title') ?></a></div>
        
        <div class="path-arrow Fright"></div>
        <div class="path-name Fright"><?php echo  lang('add_title') ?></div>
    
    </div>
</div>


<?php echo  form_open_multipart(false, 'id="frm_hotels_availabilty_offers" ') ?>


<div class="widget">
    <div class="widget-header">
		
        <div class="widget-header-icon Fright">
            <span class="icos-pencil2"></span>
        </div>

        <div class="widget-header-title Fright">
            <?php echo  lang('main_data') ?>
            
        </div>
        
    </div>

    <div class="widget-container" id="container_main_data">  
        <? if(validation_errors()){ ?>
            <?php echo validation_errors(); ?>        
        <? } ?>
        
        <div class="row-form">
            <div class="span6">
                <div class="span4 TAL Pleft10">
                    <?php echo  lang('name_ar') ?>
                </div>
                <div class="span8">
                    <?= form_input('name_ar', set_value('name_ar', ''), 'class="validate[required] input-huge"')  ?>
                </div>
			</div>
        
        <div class="span6">
                <div class="span4 TAL Pleft10">
                    <?php echo  lang('name_la') ?>
                </div>
                <div class="span8">
                    <?= form_input('name_la', set_value('name_la', ''), 'class="validate[required] input-huge"')  ?>
                </div>
			</div>
        
        </div>
        
       
        
        
        <div class="row-form">
            <div class="span6">
                <div class="span4 TAL Pleft10">
                    <?php echo  lang('nights_count') ?>
                </div>
                <div class="span8">
                    <?= form_input('nights_count', set_value('nights_count', ''), 'class="validate[required] input-huge"')  ?>
                </div>
			</div>
                        
       </div>
       
       
       <div class="row-form">
            
            <div class="span6" >
                <div class="span4 TAL Pleft10">
                <?php echo  lang('pricing_methodology') ?>
                </div>
                <div class="span8" >
                    <?php echo form_radio('pricing_methodology', '1', true, 'id="pricing_methodology"'); ?> <?php echo  lang('exclude_week_end_days') ?><br/>
                </div>
            </div>
            
            <div class="span6">
            <?php echo form_radio('pricing_methodology', '2', false, 'id="pricing_methodology"'); ?><?php echo  lang('include_week_end_days') ?><br/>
            
            
            
            	<div class="span4 TAL Pleft10">
                <?php echo form_radio('rdo_discount', 'discount_percent', false, 'id="rdo_discount" disabled="disabled" '); ?> <?php echo  lang('discount_percent') ?><br/>
                </div>
                <div class="span8">
                    <?= form_input('discount_percent', set_value('discount_percent', ''), 'class=" input-small" disabled="disabled"')  ?> %
                </div>
			
                <div class="span4 TAL Pleft10">
                    <?php echo form_radio('rdo_discount', 'discount_fixed', false, 'id="rdo_discount" disabled="disabled"'); ?> <?php echo  lang('discount_fixed') ?><br/>
                </div>
                <div class="span8">
                    <?= form_input('discount_fixed', set_value('discount_fixed', ''), 'class=" input-small" disabled="disabled"')  ?>
                </div>
            
            
            
            </div>
            
       </div>
       
             
        <div class="row-form">
            <div class="span6">
                <div class="span4 TAL Pleft10">
                    <?php echo  lang('rounding_value') ?>
                </div>
                <div class="span8">
                    <?php echo form_input('rounding_value', set_value("rounding_value")," style='' id='rounding_value' class=' input-small' ") ?>
                </div>
			</div>
            
        
        </div>
            
                
    </div>
</div>



<div class="widget TAC">
    <input type="submit" class="btn" name="smt_save" value="<?php echo lang('save') ?>" style="margin:10px;padding: 5px;height: auto">
</div>


<?php echo  form_close() ?>


<div class="footer"></div>


<script type="text/javascript">
$(document).ready(function() {
    var config = {
        '.chosen-select': {width: "100%"},
        '.chosen-select-deselect': {allow_single_deselect: true},
        '.chosen-select-no-single': {disable_search_threshold: 10},
        '.chosen-select-no-results': {no_results_text: 'Oops, nothing found!'},
        '.chosen-select-width': {width: "95%"}
    }
    for (var selector in config) {
        $(selector).chosen(config[selector]);
    }


    // binds form submission and fields to the validation engine
    $("#frm_hotels_availabilty_offers").validationEngine({
            prettySelect : true,
            useSuffix: "_chosen",
            promptPosition : "topRight:-150"
            //promptPosition : "bottomLeft"
    });

    $('#pricing_methodology').live('change', function() { 
        
    	if(jQuery('input[name=pricing_methodology]:checked' ).val()==2) {
    		$('#frm_hotels_availabilty_offers').find(':radio[name=rdo_discount]').attr('disabled', false).trigger("chosen:updated");

    		if(jQuery('input[name=rdo_discount]:checked' ).val()=='discount_percent') {
    			$('#frm_hotels_availabilty_offers').find(':text[name=discount_percent]').attr('disabled', false).trigger("chosen:updated"); 
        		} else if(jQuery('input[name=rdo_discount]:checked' ).val()=='discount_fixed') {
        			$('#frm_hotels_availabilty_offers').find(':text[name=discount_fixed]').attr('disabled', false).trigger("chosen:updated");
        		}
    		 
        } else {
        	$('#frm_hotels_availabilty_offers').find(':radio[name=rdo_discount]').attr('disabled', true).trigger("chosen:updated");

        	$('#frm_hotels_availabilty_offers').find(':text[name=discount_percent]').attr('disabled', true).trigger("chosen:updated"); 
    		$('#frm_hotels_availabilty_offers').find(':text[name=discount_fixed]').attr('disabled', true).trigger("chosen:updated");
        }
        
    });

	$('#rdo_discount').live('change', function() { 
        
    	if(jQuery('input[name=rdo_discount]:checked' ).val()=='discount_percent') {
    		$('#frm_hotels_availabilty_offers').find(':text[name=discount_percent]').attr('disabled', false).trigger("chosen:updated"); 
    		$('#frm_hotels_availabilty_offers').find(':text[name=discount_fixed]').attr('disabled', true).trigger("chosen:updated");
        } else {
        	$('#frm_hotels_availabilty_offers').find(':text[name=discount_percent]').attr('disabled', true).trigger("chosen:updated");
        	$('#frm_hotels_availabilty_offers').find(':text[name=discount_fixed]').attr('disabled', false).trigger("chosen:updated");
        }
        
    });

     
});


</script>
