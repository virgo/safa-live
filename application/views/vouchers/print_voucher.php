<?= $this->load->view('admin/print_header') ?>
<style>
    div {
        font-size: 14px;
    }
    table {
        font-size: 14px;
    }
</style>
<? for($i=1;$i<=1;$i++): ?>
<h2><center><?=ddmenu('vouchers_defs',$item->voucher_defs_id)?></center></h2>
<h3><center><?= lang('no') ?> <span style="color: red;"><? printf('%06d', $item->voucher_number);?></span></center></h3>

<div style="border: #CCC thin solid;padding: 5px;margin-left: auto;margin-right: auto; width: 600px;">
    <div align="left" style="margin-left:30px;background-color: #DDD;padding-top: 10px;height: 30px;width: 100px;float: left;text-align: center;">
        <?=$item->amount?> <?=$currency_short?>
    </div>
    <div align="right">
        <?= lang('voucher_date') ?> : <span style="color: blue;"><?=$item->voucher_date?></span>
    </div>
    <br />
    <? if($payment_request): ?>
    <div align="right">
        <?if($vouchers_def->main_type == '1') echo lang('receive_from'); else echo lang('receive_to'); ?> <?=lang('respectable')?> : <span style="color: blue;"><?=dd2menu('clients',$payment_request->client_id)?></span>
    </div>
    <? else: ?>
    <div align="right">
        <?if($vouchers_def->main_type == '1') echo lang('receive_from'); else echo lang('receive_to'); ?> <?=lang('respectable')?> : <span style="color: blue;">..................................................................................</span>
    </div>
    <? endif; ?>
    <div align="right">
        <?= lang('total_money') ?> : <span style="color: blue;"><?= convert_num($item->amount) ?> <?=ddmenu('currencies',$item->currency)?> <?= lang('only') ?></span>
    </div>
    <br />
    <div align="right">
        <span style="color: blue;"><?= $item->description ?></span>
    </div>
    <? if($item->remarks): ?>
<!--    <br /><?= lang('remarks') ?> :-->
    <div align="right">
        <?= $item->remarks ?>
    </div>
    <? endif; ?>
    <br />
    <table align="center" width="100%">
        <tr>
            <th><?= lang('receiver') ?></th>
            <th><?= lang('accounting') ?></th>
        </tr>
        <tr>
            <th>...............</th>
            <th>...............</th>
        </tr>
    </table>
    <br /><br />
</div>
<?endfor;?>
<script>
    window.print();
//    history.back();
</script>
<?= $this->load->view('admin/print_footer') ?>