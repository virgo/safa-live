<h1><?= lang('global_vouchers') ?> 
 <a class="button" href="<?= site_url('accounting/vouchers/add' ) ?>"><?= lang('global_add') ?></a></h1>
</h1>
<table width="100%" class="table">
    <tr>
        <th> <?= lang('voucher_number') ?></th>
        <th> <?= lang('voucher_defs_id') ?></th>
        <th> <?= lang('voucher_date') ?></th>
        <th> <?= lang('amount') ?></th>
        <th> <?= lang('currency') ?></th>
        <th> <?= lang('account') ?></th>
        <th> <?= lang('cash_account') ?></th>
        <th> <?= lang('branch_id') ?></th>
        <th> <?= lang('user_id') ?></th>
        <th><?= lang('global_operations') ?></th>
    </tr>
    <? if($items && is_array($items) && count($items)): ?>
    <? foreach($items as $item): ?>
    <? if($item->branch_id == session('branch_id') || permission('manage_accounting')): ?>
    <tr>
        <td><?= $item->voucher_number ?></td>
        <td><?=ddmenu('vouchers_defs',$item->voucher_defs_id)?></td>
        <td><?= $item->voucher_date ?></td>
        <td><?= $item->amount ?></td>
        <td><?=ddmenu('currencies',$item->currency)?></td>
        <td><?=ddmenu('accounts',$item->account)?></td>
        <td><?=ddmenu('accounts',$item->cash_account)?></td>
        <td><?=$this->accounting->getSetting('branches', 'id', $item->branch_id, name())?></td>
        <td><?=$this->accounting->getSetting('users', 'id', $item->user_id, 'username')?></td>
        <td>
            <a class="button" href="<?= site_url('accounting/vouchers/add/'. $item->id ) ?>"><?= lang('global_edit') ?></a> 
            <a class="button" href="<?= site_url('accounting/vouchers/print_voucher/'. $item->id ) ?>" target="_blank"><?= lang('global_print') ?></a> 
            <?/* if(permission('manage_accounting')): ?>
            <a class="button" onclick="return confirm('Are you sure to delete')"   href="<?= site_url('accounting/vouchers/delete/' . $item->id) ?>"><?= lang('delete') ?></a> 
            <? endif;*/ ?>
        </td>
    </tr>
    <? endif; ?>
    <? endforeach ?>
    
    <? else: ?>
            <tr>
                <td colspan="10">
        <?= lang('global_no_entries') ?>
                </td>
            </tr>
    <? endif ?>
 
</table>
<?= $pagination ?>
