<div id="tabs">
    <div id="tabs-1" class="package_ground">
        <span style="" class="title_style"><?= lang('global_vouchers') ?></span>
        <div class="box box-gray">
            <div class="box-content">
                <? if (validation_errors()): ?>
                    <div class="error">
                        <?php echo validation_errors(); ?>
                    </div>
                <? endif ?>

<?= form_open() ?>

  <div class="form-item">
                    <label class="label_color">
                        <?= lang('voucher_defs_id') ?>
                        <span class="required">*</span>
                    </label>        

                    <?= form_dropdown('voucher_defs_id', ddgen('vouchers_defs',array('id',  name())), set_value('voucher_defs_id'), 'class="text_input" id="voucher_defs_id"') ?>
                </div> 


                <div class="form-item">
                    <label class="label_color">
                        <?= lang('voucher_date') ?>
                        <span class="required">*</span>
                    </label>        

                    <?= form_input('voucher_date', set_value('voucher_date',date('Y-m-d')), 'class="text_input" id="date1"') ?>
                </div> 

                <div class="form-item">
                    <label class="label_color">
                        <?= lang('voucher_number') ?>
                        <span class="required">*</span>
                    </label>        

                    <?= form_input('voucher_number', set_value('voucher_number'), 'class="text_input" id="voucher_number"') ?>
                </div>
                
                
                <div class="form-item">
                    <label class="label_color">
                        <?= lang('payment_request_number') ?>
                    </label>        

                    <?= form_input('payment_request_number', set_value('payment_request_number',$payment_request), 'class="text_input" id="payment_request_number"') ?>
                </div>

                
                
                <div class="form-item">
                    <label class="label_color">
                        <?= lang('account') ?>
                        <span class="required">*</span>
                    </label>        

                    <?= form_input('account_name', set_value('account_name'), 'class="text_input" id="account_name"') ?>
                    <input type="hidden" value="<?=set_value('account', '0')?>" id="account" name="account" />
                </div> 
                
<? if(permission('manage_accounting')): ?>
                <div class="form-item">
                    <label class="label_color">
                        <?= lang('cash_account') ?>
                        <span class="required">*</span>
                    </label>        
                   <?//= form_dropdown('cash_account', $accounts, set_value('cash_account', $cash_account), 'class="text_input"') ?>
                    <?= form_input('cash_account_name', set_value('cash_account_name',$cash_account), 'class="text_input" id="cash_account_name"') ?>
                    <input type="hidden" value="<?=set_value('cash_account', $cash_account)?>" id="cash_account" name="cash_account" />
                </div> 
<? elseif(permission('branch_vouchers')): ?>
                       
                   <?= form_hidden('cash_account', $cash_account, 'class="text_input" readonly="readonly"') ?>
                
<? endif ?>

                


 <div class="form-item">
                    <label class="label_color">
                        <?= lang('amount') ?>
                        <span class="required">*</span>
                    </label>        
                    <?= form_input('amount', set_value('amount'), 'class="text_input" id="amount"') ?>
                </div> 
                
                
                <div class="form-item">
                    <label class="label_color">
                        <?= lang('currency') ?>
                        <span class="required">*</span>
                    </label>        
                   <?= form_dropdown('currency', $currencies, set_value('currency',$this->config->item('default_currency')), 'class="text_input" id="currency"') ?>
                    <span id="currency_text"></span>
                </div> 


                <div class="form-item">
                    <label class="label_color">
                        <?= lang('rate') ?>
                        <span class="required">*</span>
                    </label>        
                 <?= form_input('rate', set_value('rate','1.0000'), 'class="text_input" id="rate"') ?>
                </div> 
<div class="form-item">
                    <label class="label_color">
                        <?= lang('description') ?>
                    </label>        
                    <?= form_textarea('description', set_value('description'), 'class="text_input" id="description"') ?>
                </div> 

                <div class="form-item">
                    <label class="label_color">
                        <?= lang('remarks') ?>
                    </label>        
                    <?= form_textarea('remarks', set_value('remarks'), 'class="text_input"') ?>
                </div> 


                <div class="submit-row" id="center_buttons">
                    <input type="submit" class="button" value="<?= lang('global_submit') ?>" />
                    <input type="button" value="<?= lang('global_back') ?>" class="button" onclick="window.location='<?= site_url('admin/vouchers/index') ?>'" />
                </div>

            </div>
        </div>
    </div>
</div>
<?= form_close() ?>
<script>
    $(document).ready(function(){
        getRate();
        update_payment_request();
        $(function(){
            $( "#account_name" ).autocomplete({
                source: "<?=site_url('admin/accounts/get_accounts/2')?>"+$(this).val(),
                minLength:1,
                select: function( event, ui ) {
                    $('#account').val(ui.item.id);
                }
            });
            $( "#cash_account_name" ).autocomplete({
                source: "<?=site_url('admin/accounts/get_accounts/2')?>"+$(this).val(),
                minLength:1,
                select: function( event, ui ) {
                    $('#cash_account').val(ui.item.id);
                }
            });
        });
        $( "#account_name" ).blur(function(){
            if($( "#account_name" ).val() == '') {
                $('#account').val('0');
            }
        });
        $( "#cash_account_name" ).blur(function(){
            if($( "#cash_account_name" ).val() == '') {
                $('#cash_account').val('0');
            }
        });
        $(function() {
            $( "#date1" ).datepicker({dateFormat: "yy-mm-dd"});
        });
        $('#voucher_defs_id').change(function(){
                $.post('<?= url('admin/vouchers/ajax') ?>/' + $('#voucher_defs_id').val(), function(data){
                $('#voucher_number').val(data);
            });
        });
        $( "#currency" ).change(function(){
            getRate();
        });
        $( "#payment_request_number" ).change(function(){
            update_payment_request();
        });
    });
    function update_payment_request() {
            if($( "#payment_request_number" ).val() == '') {
                document.getElementById('account_name').removeAttribute('readonly');
                document.getElementById('amount').removeAttribute('readonly');
//                document.getElementById('currency').removeAttribute('disabled');
                $('#currency_text').html('');
                $('#currency').show();
                
                $('#account_name').val('');
                $('#account').val('0');
                $('#amount').val('');
//                $('#currency').val('');
                $('#description').val('');
            }
            else {
                document.getElementById('account_name').setAttribute('readonly','readonly');
                document.getElementById('amount').setAttribute('readonly','readonly');
//                document.getElementById('currency').setAttribute('disabled','disabled');
                $('#currency').hide();
                
                $('#account_name').val('');
                $('#account').val('0');
                $('#amount').val('');
//                $('#currency').val('');
                $('#description').val('');
                
                $.post('<?= url('admin/vouchers/get_payment_request') ?>/' + $('#payment_request_number').val(), function(data){
                    
                    var obj = jQuery.parseJSON(data);
                    if(obj.voucher_id != '0') {
                        alert('<?=lang('paid')?> '+obj.payment_date);
                    }
                    else {
                        $('#account_name').val(obj.account_name);
                        $('#account').val(obj.account_id);
                        $('#amount').val(obj.amount);
                        $('#currency').val(obj.currency_id);
                        $('#description').val('<?= lang('for_reservation') ?> '+obj.code);
                        $.post('<?= url('admin/vouchers/get_currency_name') ?>/' + $('#currency').val(), function(data1){
                            if(data1)
                                $('#currency_text').html(data1);
                        });
                        getRate();
//                        if(obj.voucher_id != '0') {
//                            alert('<?=lang('paid')?> '+obj.payment_date);
//                        }
                    }
                });
            }        
    }
    function getRate() {
        $.post('<?= url('admin/vouchers/get_rate') ?>/' + $('#currency').val(), function(data){
            if(data)
                $('#rate').val(parseFloat(data).toFixed(4));
            if($('#rate').val() == 'NaN')
                $('#rate').val('1');
        });
    }
</script>


