<div class="widget">
    <div class="path-container Fright">
        <div class="path-name Fright">
            <a href="<?= site_url() ?>"><?= lang('global_system_management') ?></a>
        </div>
        <div class="path-arrow Fright"></div>
        <div class="path-name Fright"> <?= lang('lang_translate') ?></div>
    </div>
</div>


<form id="send_search" action="" method="post">
    <div class="widget">
        <div class="widget-header">

            <div class="widget-header-title Fright">
                <?= lang('lang_translate') ?>
            </div>
            <div class="Fleft span7" >
                <div class="span3">
                    <?= form_dropdown('file_id', $files,  set_value('file_id',1)) ?>
                </div>
            </div>
        </div>

        <div class="widget-container">
            <? if (isset($datasaved)) : ?>
                <div class="span10 messagebox" algin="center" style="border-radius: 5px;margin: 10px;padding: 10px;background: none repeat scroll 0 0 #468847;color: #DFF0D8;" ><?= $datasaved ?></div>
            <? endif ?>
            <div class="row-fluid">
                <div class="span12">
                    <div class="span6 ">
                        <?= $fromlang->name ?>
                    </div>
                    <div class="span6">
                        <?= $tolang->name ?>
                    </div>
                </div>
            </div>
            <div class="lang-data">
                <? foreach ($fromvalues['phrases'] as $key => $fromvalue) : ?>
                    <div class="row-fluid">
                        <div class="span12">
                            <div class="span3 ">
                                <input value="<?= $fromvalues['keys'][$key] ?>" disabled="disabled" class=" input-full chosen-rtl" style="width:100%;" />
                            </div>
                            <div class="span3 ">
                                <input value="<?= $fromvalue ?>" disabled="disabled" class=" input-full chosen-rtl" style="width:100%;" />
                            </div>
                            <div class="span6">
                                <?= form_input("row[$key]", set_value("row[$key]", (isset($tovalues['phrases'][$key])) ? $tovalues['phrases'][$key] : ''), 'class=" input-full chosen-rtl" style="width:100%;" ') ?>
                            </div>
                        </div>
                    </div>
                <? endforeach ?>
            </div>
            <div class="row-fluid">

                <div class="span12" align="center">
                    <input type="submit" class="btn start" value="<?= lang('lang_save') ?>"  />
                </div>

            </div>

        </div>
    </div>

</form>

<script type="text/javascript">
    $(function() {
        var fromlang = '<?= $fromlang->erp_language_id ?>';
        var tolang = '<?= $tolang->erp_language_id ?>';

        $('select[name=file_id]').change(function() {
            var fileid = $(this).val();
            $('.lang-data').html('<img class="loading" src="<?= base_url('static/new_template/img/select/spinner.gif') ?>" />');
            $('.messagebox').fadeOut();
            $.get('<?= site_url('language_update/getfilerows') ?>/' + fileid + '/' + fromlang + '/' + tolang, function(data) {
                $('.lang-data').html(data);
            });
        });
        
        $('.messagebox').click(function(){
            $(this).fadeOut();
        });
    });
</script>