<div class="widget">
    <div class="path-container Fright">
        <div class="path-name Fright">
            <a href="<?= site_url() ?>"><?= lang('global_system_management') ?></a>
        </div>
        <div class="path-arrow Fright"></div>
        <div class="path-name Fright"> <?= lang('lang_translate') ?></div>
    </div>
</div>


<form id="send_search" action="" method="post">

    <div class="widget">
        <div class="widget-header">
            <div class="widget-header-icon Fright">
                <span class="icos-pencil2"></span>
            </div>
            <div class="widget-header-title Fright">
                <?= lang('lang_translate') ?>
            </div>
        </div>

        <div class="widget-container">
            <div class="row-fluid">
                <div class="span3">
                    <div class="span4 TAL Pleft10">
                        <label><?= lang('lang_from') ?>:</label>
                    </div>
                    <div class="span8">
                        <?= form_dropdown('from', $from_lang, set_value('from'), 'class="validate[required] input-full chosen-rtl" style="width:100%;" ') ?><?= form_error('from') ?>
                    </div>
                </div>
                <div class="span3">
                    <div class="span4 TAL Pleft10">
                        <label><?= lang('lang_to') ?>:</label>
                    </div>
                    <div class="span8">
                        <?= form_dropdown('to', $to_lang, set_value('to'), 'class="validate[required] input-full chosen-rtl" style="width:100%;"') ?><?= form_error('to') ?>
                    </div>
                </div>
                <div class="span1">
                    <div class="span12" align="center">
                        <input type="submit" class="btn start" value="<?= lang('lang_start') ?>"  />
                    </div>
                </div>
            </div>

        </div>
    </div>

</form>