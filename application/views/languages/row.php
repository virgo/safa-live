                    <? foreach ($fromvalues['phrases'] as $key => $fromvalue) : ?>
                    <div class="row-fluid">
                        <div class="span12">
                            <div class="span3 ">
                                <input value="<?= $fromvalues['keys'][$key] ?>" disabled="disabled" class=" input-full chosen-rtl" style="width:100%;" />
                            </div>
                            <div class="span3 ">
                                <input value="<?= $fromvalue ?>" disabled="disabled" class=" input-full chosen-rtl" style="width:100%;" />
                            </div>
                            <div class="span6">
                                <?= form_input("row[$key]", set_value("row[$key]", (isset($tovalues['phrases'][$key])) ? $tovalues['phrases'][$key] : ''), 'class=" input-full chosen-rtl" style="width:100%;" ') ?>
                            </div>
                        </div>
                    </div>
                    <? endforeach ?>