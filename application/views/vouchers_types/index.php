<?= $this->load->view('admin/header') ?>
<h1><?= lang('vouchers_types') ?>
 <a class="button" href="<?= site_url('admin/vouchers_types/add' ) ?>"><?= lang('global_add') ?></a>
</h1>
<table width="100%" class="table">
    <tr>
        <th><?=lang('id')?></th>
        <th><?=lang('name_la')?></th>
        <th><?=lang('name_ar')?></th>
        <th><?= lang('global_operations') ?></th>
    </tr>
    <? if($items && is_array($items) && count($items)): ?>
    <? foreach($items as $item): ?>
    <tr>
        <td><?= $item->id ?></td>
        <td><?= $item->name_la ?></td>
        <td><?= $item->name_ar?></td>
        <td>
            <a class="button" href="<?= site_url('admin/vouchers_types/edit/'. $item->id ) ?>"><?= lang('global_edit') ?></a> 
            <?if($item->id > 2):?> 
            <a class="button" onclick="return confirm('Are you sure to delete <?=$item->name_la ?>?')"   href="<?= site_url('admin/vouchers_types/delete/' . $item->id) ?>"><?= lang('delete') ?></a> 
            <?endif;?>
        </td>
    </tr>
    <? endforeach ?>
    
   <? else: ?>
            <tr>
                <td colspan="3">
        <?= lang('global_no_entries') ?>
                </td>
            </tr>
    <? endif ?>
 
</table>
<?= $pagination ?>
<?= $this->load->view('admin/footer') ?>
