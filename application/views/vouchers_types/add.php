<?= $this->load->view('admin/header') ?>
<div id="tabs">

    <div id="tabs-1" class="package_ground">
        <span style="" class="title_style"><?= lang('vouchers_types') ?></span>
        <div class="box box-gray">
            <div class="box-content">
                <? if (validation_errors()): ?>
                    <div class="error">
                        <?php echo validation_errors(); ?>
                    </div>
                <? endif ?>
                <?= form_open() ?>


                <div class="form-item">
                    <label class="label_color">
                        <?= lang('name_ar') ?>
                        <span class="required">*</span>
                    </label>        
                    <?= form_input('name_ar', set_value('name_ar'), 'class="text_input"') ?>
                </div> 

                <div class="form-item">
                    <label class="label_color">
                        <?= lang('name_la') ?>
                        <span class="required">*</span>
                    </label>        
                    <?= form_input('name_la', set_value('name_la'), 'class="text_input"') ?>
                </div> 




                <div class="submit-row" id="center_buttons">
                    <input type="submit" class="button" value="<?= lang('global_submit') ?>" />
                    <input type="button" value="<?= lang('global_back') ?>" class="button" onclick="window.location='<?= site_url('admin/vouchers_types/index') ?>'" />
                </div>

            </div>
        </div>
    </div>
</div>
<?= form_close() ?>
