<!DOCTYPE html>
<html lang="<?= lang('global_lang') ?>"	xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title></title>
        <meta name="viewport" content="width=device-width">

            <style>
                div.bot_container{
                    width: 320px;
                    height: 200px;
                    margin:0px auto;}
                div.bot{
                    position: relative;
                    background: url(<?= base_url() ?>/static/front/logo.png);
                    width: 320px;
                    height: 95px;
                    top: 90px;
                    animation: bot_float ease 2s 1;
                    -webkit-animation: bot_float ease 2s 1;
                    -moz-animation: bot_float ease 2s 1;
                }

                @keyframes bot_float { 50% { top : 0px; } 100% { top : 90px; } }
                @-webkit-keyframes bot_float { 50% { top : 0px; } 100% { top : 90px; } }
                @-moz-keyframes bot_float { 50% { top : 0px; } 100% { top : 90px; } }

                div.bot_shadow{
                    position: relative;
                    height: 10px;
                    background: #999;
                    opacity: 0.1;
                    border-radius: 100%;
                    margin: 0px;
                    top: 100px;
                    animation: shadow_react ease 2s 1;
                    -webkit-animation: shadow_react ease 2s 1;
                    -moz-animation: shadow_react ease 2s 1;

                }
                @keyframes shadow_react {50% {margin: 0px 20px ; opacity: 0.7;}
                                         100% {margin: 0px ; opacity: 0.1;}}
                                         @-webkit-keyframes shadow_react {50% {margin: 0px 20px ; opacity: 0.7;} 100% {margin: 0px ; opacity: 0.1;}}
                                         @-moz-keyframes shadow_react {50% {margin: 0px 20px ; opacity: 0.7;} 100% {margin: 0px ; opacity: 0.1;}}

                                         .errorpgtitle{
                                             font-family: Arial, sans-serif;
                                             font-size: 100px;
                                             text-align: center;
                                             display: block;
                                             margin: 30px 0px 10px 0px;
                                             color: #000000;
                                             font-variant: small-caps;
                                         }
                                         .errorpgfont {
                                             font-family: Arial, sans-serif;
                                             font-size: 20px;
                                             text-align: center;
                                             display: block;
                                             margin: auto;
                                             color: #FFFFFF;
                                             width: 30%;

                                             border-radius: 40px;
                                             border: #999 solid 2px;
                                             background: #999;
                                         }

                </style>
        </head>
        <body>
            <div class="bot_container">
                <div class="bot"></div>
                <div class="bot_shadow"></div>
            </div>
            <div class="errorpgtitle"> 
                <? if (isset($title)): ?>
                <?= $title ?>
                <? endif ?>
            </div>
            <? if (isset($message)): ?>
            <div class="errorpgfont">
                <?= $message ?>
            </div>
            <? endif ?>
        </body>
    </html>
