<div class="row-fluid">
    <div class="span12">
        <div class="widget">
            <div class="head dark">
                <div class="icon"><i class="icos-pencil2" align="right"> </i></div>
                <h2><?= lang('safa_uo_contracts_add') ?></h2> 
            </div>
            <div class="block-fluid">
                <?= form_open() ?>

                <? if (validation_errors()): ?>
                    <td class="red-left" style="line-height:3px">
                        <div style="font:normal 10px tahoma">
                            <?php echo validation_errors(); ?>
                        </div>
                    </td>   
                    <td class="red-right"><a class="close-red"><img src="<?= IMAGES ?>/table/icon_close_red.gif"   alt="" /></a></td>
                <? endif ?>

                <div class="row-form">  
                    <div class="span2"><?= lang('safa_uo_contracts_safa_uo_contract_id') ?></div>
                       <?= form_dropdown('safa_uo_id', ddgen('safa_uos', array('safa_uo_id', name())), set_value('safa_uo_id'), " class='input-small' ") ?>
                </div>

               <div class="row-form">
                    <div class="span2"><?= lang('safa_uo_contracts_name_la') ?></div>
                    <?= form_input('name_la', set_value("name_la"), " class='input-small' ") ?>
                </div>    
                
               <div class="row-form">
                    <div class="span2"><?= lang('safa_uo_contracts_name_ar') ?></div>
                    <?= form_input('name_ar', set_value("name_ar"), " class='input-small' ") ?>
                </div>    
                    
                    
                <div class="row-form">
                    <div class="span2"><?= lang('safa_uo_contracts_contract_username') ?></div>
                    <?= form_input('contract_username', set_value("contract_username"), " class='input-small' ") ?>
                </div>

                <div class="row-form">
                    <div class="span2"><?= lang('safa_uo_contracts_contract_password') ?></div>
                    <?= form_password('contract_password', set_value('contract_password'), " class='input-small'") ?>   
                </div> 

                <div class="row-form">
                    <div class="span2"><?= lang('safa_uo_contrcts_repeat_password') ?></div>
                    <input type="password" name="passconf" class="input-small" value="<?php echo set_value('passconf'); ?>"></input>
                </div>   


                <div class="row-form" style="text-align:center">
                    <input type="button" name="back" value="<?= lang('global_back') ?>" class="btn btn-primary" onclick="window.location = '<?= site_url('safa_uo_contracts/index') ?>'">
                    <input type="submit" name="submit" value="<?= lang('global_submit') ?>" class="btn btn-primary">
                </div>
                <?= form_close() ?>  
            </div>
        </div>
    </div>
</div>
<script type='text/javascript' src='<?= MODULE_JS ?>/poll/add.js'></script>
