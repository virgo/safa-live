
<div class='row-fluid' style="display:<? if (isset($_GET['search'])): ?>block<? else: ?>none<? endif; ?>" id="search_panal">
    <?= $this->load->view('safa_uo_contracts/search') ?>
</div>
<div  class="row-fluid" >
    <div class="span6" id="show_fileds" >
    </div>
</div>
<div class='row-fluid'>
    <div class='span12'>
        <div class='widget'>
            <div class="dialog" id="deleting_error" style="display: none;" title="Error">
                <p><?= lang('global_select_one_record_at_least') ?></p>                
            </div>
            <div class='head dark'>
                <div class="icon"><span class="icos-cube1"></span></span></div>
                <h2>Safa UOS</h2>
                <ul class="buttons">
                    <li>
                        <a href="#"><span class="icos-arrow-down5"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="<?= site_url("safa_uo_contracts/add") ?>"><?= lang('global_add_new_record') ?></a></li>
                            <li><a id="delete_all" href="javascript:void(0)"><?= lang('global_delete_selected_records') ?></a></li>
                            <li class="divider"></li>
                            <li><? if (isset($_GET["search"])): ?><a href="<?= site_url('safa_uo_contracts/index') ?>"><?= lang('global_full_list') ?></a><? else: ?><a href="javascript:void(0)" id="show_hide_search"><?= lang('global_search') ?></a><? endif; ?></li>
                            <li><a href="javascript:void(0)" id="show_hide_fields">Custom Columns</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
            <div class='block-fluid' > 
                <?= form_open("safa_uo_contracts/delete_all", "id='form_delete'") ?>
                <table class="table" width="100%" >
                    <thead>
                        <tr>
                            <th><input type="checkbox" class="checkall" ></th>

                            <th><?= lang('safa_uo_contracts_contract_username')?></th>
                        </tr>
                    </thead>
                    <tbody>
                       <? if($items && is_array($items) && count($items)): ?>
                            <? foreach ($items as $item): ?>
                                <tr>
                                    <td><input type="checkbox" name="delete_items[]" value="<?= $item->safa_uo_contract_id ?>"/></td>
                                    <td><?= $item->contract_username?></td>

                                    <td>
                                        <div class="btn-group">
                                            <a href="<?= site_url("safa_uo_contracts/edit") ?>/<?= $item->safa_uo_contract_id ?>" class="btn"><?= lang('global_edit') ?></a>
                                            <button class="btn dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                            <ul class="dropdown-menu">
                                                <li><a href="<?= site_url("safa_uo_contracts/edit") ?>/<?= $item->safa_uo_contract_id ?>"><?= lang('global_edit') ?></a></li>
                                                <li><a href="<?= site_url("safa_uo_contracts/delete") ?>/<?= $item->safa_uo_contract_id ?>"><?= lang('global_delete') ?></a></li>
                                            </ul>
                                        </div>  
                                    </td>
                                </tr>
                            <? endforeach; ?>
                        <? endif; ?>
                    </tbody>
                </table>
                <?= form_close() ?> 
            </div>
        </div>
    </div>
</div>
<div class="row-fluid">
    <?= $pagination ?>
    <script>
     check_list_delete_submit("delete_all", "table", "form_delete", <?= lang('global_are_you_sure_you_want_to_delete') ?>, "deleting_error");
     $(document).ready(function() {
     $("#show_hide_fields").click(function() {
     createPlugin("show_fields_plugin", "<?= site_url('plugins/show_fields/get_view_plugin') ?>/<?= $module_name ?>", "show_fileds", "<?= JS . '/plugins/show_fields.js' ?>");
       });
       });
    </script>
    
