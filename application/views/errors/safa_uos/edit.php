<div class="row-fluid">
    <div class="span12">
        <div class="widget">
            <div class="head dark">
                <div class="icon"><i class="icos-pencil2" align="right"> </i></div>
                <h2><?= lang('safa_uos_add') ?></h2> 
            </div>
            <div class="block-fluid">
                <?= form_open() ?>

                <? if (validation_errors()): ?>
                    <td class="red-left" style="line-height:3px">
                        <div style="font:normal 10px tahoma">
                            <?php echo validation_errors(); ?>
                        </div>
                    </td>   
                    <td class="red-right"><a class="close-red"><img src="<?= IMAGES ?>/table/icon_close_red.gif"   alt="" /></a></td>
                <? endif ?>
                <div class="row-form">
                    <div class="span2"><?= lang('safa_uos_name_ar') ?></div>
                    <?= form_input('name_ar', set_value("name_ar",$item->name_ar), " class='input-small'  ") ?>
                </div>

                <div class="row-form">
                    <div class="span2"><?= lang('safa_uos_name_la') ?></div>
                    <?= form_input('name_la', set_value("name_la",$item->name_la), " class='input-small'  ") ?>
                </div>

                <div class="row-form">
                    <div class="span2"><?= lang('safa_uos_code') ?></div>
                    <?= form_input('code', set_value("code",$item->code), " class='input-small'  ") ?>
                </div>

                <div class="row-form">
                    <div class="span2"><?= lang('safa_uos_phone') ?></div>
                    <?= form_input('phone', set_value("phone",$item->phone), " class='input-small'  ") ?>
                </div>

                <div class="row-form">
                    <div class="span2"><?= lang('safa_uos_mobile') ?></div>
                    <?= form_input('mobile', set_value("mobile",$item->mobile), " class='input-small'  ") ?>
                </div>

                <div class="row-form">
                    <div class="span2"><?= lang('safa_uos_email') ?></div>
                    <?= form_input('email', set_value("email",$item->mobile), " class='input-small'  ") ?>
                </div>

                <div class="row-form">
                    <div class="span2"><?= lang('safa_uos_fax') ?></div>
                    <?= form_input('fax', set_value("fax",$item->fax), " class='input-small'  ") ?>
                </div>

                <div class="row-form" style="text-align:center">
                    <input type="button" name="back" value="<?= lang('global_back') ?>" class="btn btn-primary" onclick="window.location = '<?= site_url('safa_uos/index') ?>'">
                    <input type="submit" name="submit" value="<?= lang('global_submit') ?>" class="btn btn-primary">
                </div>
                <?= form_close() ?>  
            </div>
        </div>
    </div>
</div>
<script type='text/javascript' src='<?= MODULE_JS ?>/poll/add.js'></script>
