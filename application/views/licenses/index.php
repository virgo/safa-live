<div class="span12">
    <div class="widget">
        <a class="btn Fleft" href="<?= site_url($module.'/manage') ?>"><?= lang('global_add_new_record') ?></a>
        <div class="path-container Fright">
            <div class="path-name Fright">
                <a href=""><?= lang('global_system_management') ?></a>
            </div>
            <div class="path-arrow Fright"></div>
            <div class="path-name Fright">
                <?= lang($module.'_title') ?>
            </div>
        </div>
    </div>
</div>

<div class="row-fluid">
    <div class="widget">
        <div class="widget-header">
            <div class="widget-header-icon Fright">
                <span class="icos-pencil2"></span>
            </div>
            <div class="widget-header-title Fright">
                <?= lang($module.'_title') ?>
            </div>
        </div>
        <div class="widget-container">
            <div class='table-responsive' >
                <table cellpadding="0" class="" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th><?= lang($module.'_name') ?></th>
                            <th><?= lang($module.'_code') ?></th>
                            <th><?= lang('global_actions') ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <? if (ensure($items)): ?>
                            <? foreach ($items as $item): ?>
                                <tr>
                                    <td><?= $item->name ?></td>
                                    <td><?= $item->code ?></td>
                                    <td class="TAC">
                                        <a href="<?= site_url($module."/manage/" . $item->{$table_pk}) ?>" ><span class="icon-pencil"></span></a>      
                                        <? if( ! $item->delete): ?>
                                        <a onclick="return confirm('<?= lang('global_are_you_sure_you_want_to_delete') ?>')" href="<?= site_url($module."/delete/" . $item->{$table_pk}) ?>"><span class="icon-trash"></span></a>
                                        <? endif ?>
                                    </td>
                                </tr>
                            <? endforeach ?>
                        <? endif ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>



<script type='text/javascript' src='<?= JS ?>/custom/table_operation.js'></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('.delete_item').click(function() {
            var name = $(this).attr('name');
            var safa_ea_user_id = $(this).attr('id');
<? if (lang('global_lang') == 'ar'): ?>
                $('#msg').text("<?= lang('global_are_you_sure_you_want_to_delete') ?>" + " " + name)
<? else: ?>
                $('#msg').text(name + " " + "<?= lang('global_are_you_sure_you_want_to_delete') ?>")
<? endif; ?>
            $('#yes').click(function() {
                var answer = $(this).attr('id');
                if (answer === 'yes') {
                    $.ajax({
                        type: "POST",
                        dataType: 'json',
                        url: "<?php echo site_url('ea/ea_users/delete'); ?>",
                        data: {'safa_ea_user_id': safa_ea_user_id},
                        success: function(msg) {
                            if (msg.response == true) {
                                var del = safa_ea_user_id;
                                $("#row_" + del).remove();
<? if (lang('global_lang') == 'ar'): ?>
                                    $('.updated_msg').text("<?= lang('global_delete_confirm') ?>" + " " + name);
                                    $('.updated_msg').append('<br>');
                                    $('.updated_msg').append('<input  type ="button" value="<?= lang('global_submit') ?>"class="btn btn-primary" id="ok" >');
<? else: ?>
                                    $('.updated_msg').text(name + " " + "<?= lang('global_delete_confirm') ?>");
                                    $('.updated_msg').append('<br>');
                                    $('.updated_msg').append('<input  type ="button" value="<?= lang('global_submit') ?>"class="btn btn-primary" id="ok" >');
<? endif; ?>
                                $('.updated_msg').show();
                                $('#ok').click(function() {
                                    $('.updated_msg').hide();
                                });
                            } else if (msg.response == false) {
                                $('.updated_msg').text(msg.msg);
                                $('.updated_msg').append('<br>');
                                $('.updated_msg').append('<input  type ="button" value="<?= lang('global_submit') ?>"class="btn btn-primary" id="ok" >');
                                $('.updated_msg').show();
                                $('#ok').click(function() {
                                    $('.updated_msg').hide();

                                });
                            }
                        }
                    });
                } else {
                    return FALSE;
                }
            });
        });
    });
</script>

