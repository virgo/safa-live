<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Erp_hotel_speakinglanguage_model extends CI_Model {

    public $erp_hotel_speakinglanguage_id = FALSE;
    public $name_ar = FALSE;
    public $name_la = FALSE;
    public $custom_select = FALSE;
    public $limit = FALSE;
    public $offset = FALSE;
    public $order_by = FALSE;

    function __construct() {
        parent::__construct();
    }

    function get($rows_no = FALSE) {

        if ($this->custom_select !== FALSE) {
            $this->db->select('erp_hotel_speakinglanguage_id');
            $this->db->select($this->custom_select);
        }

        if ($this->erp_hotel_speakinglanguage_id !== FALSE)
            $this->db->where('erp_hotel_speakinglanguage.erp_hotel_speakinglanguage_id', $this->erp_hotel_speakinglanguage_id);

        if ($this->name_ar !== FALSE)
            $this->db->where('erp_hotel_speakinglanguage.name_ar', $this->name_ar);

        if ($this->name_la !== FALSE)
            $this->db->where('erp_hotel_speakinglanguage.name_la', $this->name_la);


        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('erp_hotel_speakinglanguage');
        if ($rows_no)
            return $query->num_rows();

        if ($this->erp_hotel_speakinglanguage_id)
            return $query->row();
        else
            return $query->result();
    }

    function save() {
        if ($this->erp_hotel_speakinglanguage_id !== FALSE)
            $this->db->set('erp_hotel_speakinglanguage.erp_hotel_speakinglanguage_id', $this->erp_hotel_speakinglanguage_id);

        if ($this->name_ar !== FALSE)
            $this->db->set('erp_hotel_speakinglanguage.name_ar', $this->name_ar);

        if ($this->name_la !== FALSE)
            $this->db->set('erp_hotel_speakinglanguage.name_la', $this->name_la);



        if ($this->erp_hotel_speakinglanguage_id) {
            $this->db->where('erp_hotel_speakinglanguage.erp_hotel_speakinglanguage_id', $this->erp_hotel_speakinglanguage_id)->update('erp_hotel_speakinglanguage');
        } else {
            $this->db->insert('erp_hotel_speakinglanguage');
            return $this->db->insert_id();
        }
    }

    function delete() {
        if ($this->erp_hotel_speakinglanguage_id !== FALSE)
            $this->db->where('erp_hotel_speakinglanguage.erp_hotel_speakinglanguage_id', $this->erp_hotel_speakinglanguage_id);

        if ($this->name_ar !== FALSE)
            $this->db->where('erp_hotel_speakinglanguage.name_ar', $this->name_ar);

        if ($this->name_la !== FALSE)
            $this->db->where('erp_hotel_speakinglanguage.name_la', $this->name_la);



        $this->db->delete('erp_hotel_speakinglanguage');
        return $this->db->affected_rows();
    }

    function search($rows_no = FALSE) {
        if ($this->custom_select !== FALSE) {
            $this->db->select('erp_hotel_speakinglanguage_id');
            $this->db->select($this->custom_select);
        }

        if ($this->erp_hotel_speakinglanguage_id !== FALSE)
            $this->db->where('erp_hotel_speakinglanguage.erp_hotel_speakinglanguage_id', $this->erp_hotel_speakinglanguage_id);

        if ($this->name_ar !== FALSE)
            $this->db->where('erp_hotel_speakinglanguage.name_ar', $this->name_ar);

        if ($this->name_la !== FALSE)
            $this->db->where('erp_hotel_speakinglanguage.name_la', $this->name_la);


        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('erp_hotel_speakinglanguage');
        if ($rows_no)
            return $query->num_rows();

        return $query->result();
    }

}

/* End of file erp_hotel_speakinglanguage_model.php */
/* Location: ./application/models/erp_hotel_speakinglanguage_model.php */