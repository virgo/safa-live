<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Erp_hotels_reservation_orders_log_model extends CI_Model {

    public $tbl_name = "erp_hotels_reservation_orders_log";
    public $erp_hotels_reservation_orders_log_id = false;
    public $erp_hotels_reservation_orders_id = false;
    public $owner_erp_company_types_id = false;
    public $owner_erp_company_id = false;
    public $erp_company_types_id = false;
    public $erp_company_id = false;
    public $the_date = false;
    public $order_date = false;
    public $arrival_date = false;
    public $departure_date = false;
    public $contact_person = false;
    public $erp_cities_id = false;
    public $erp_hotels_id = false;
    public $erp_hotels_id_alternative = false;
    public $distance_from = false;
    public $distance_to = false;
    public $look_to_haram = false;
    public $safa_uo_contract_id = false;

    /*
      public $price_from = false;
      public $price_to = false;
      public $erp_currencies_id = false;
     */
    public $master_erp_hotelroomsizes_id = false;

    /**
     * erp_hotels_reservation_orders_log_nationalities table fields
     */
    public $nationalities_erp_hotels_reservation_orders_nationalities_log_id = false;
    public $nationalities_erp_hotels_reservation_orders_log_id = false;
    public $nationalities_erp_nationalities_id = false;

    /**
     * erp_hotels_reservation_orders_log_hotel_advantages table fields
     */
    public $advantages_erp_hotels_reservation_orders_hotel_advantages_log_id = false;
    public $advantages_erp_hotels_reservation_orders_log_id = false;
    public $advantages_erp_hotel_advantages_id = false;

    /**
     * erp_hotels_reservation_orders_log_room_services table fields
     */
    public $room_services_erp_hotels_reservation_orders_room_services_log_id = false;
    public $room_services_erp_hotels_reservation_orders_log_id = false;
    public $room_services_erp_room_services_id = false;

    /**
     * erp_hotels_reservation_orders_log_rooms table fields
     */
    public $rooms_erp_hotels_reservation_orders_rooms_log_id = false;
    public $rooms_erp_hotels_reservation_orders_log_id = false;
    public $rooms_erp_housingtypes_id = false;
    public $rooms_erp_hotelroomsizes_id = false;
    public $rooms_rooms_count = false;
    public $rooms_price_from = false;
    public $rooms_price_to = false;
    public $rooms_erp_currencies_id = false;
    public $rooms_nights_count = false;
    public $rooms_entry_date = false;
    public $rooms_exit_date = false;
    public $rooms_erp_meals_id = false;

    /**
     * erp_hotels_reservation_orders_log_meals table fields
     */
    public $meals_erp_hotels_reservation_orders_meals_log_id = false;
    public $meals_erp_hotels_reservation_orders_log_id = false;
    public $meals_erp_meals_id = false;
    public $meals_meals_count = false;
    public $custom_select = false;
    public $limit = false;
    public $offset = false;
    public $order_by = false;

    function __construct() {
        parent::__construct();
    }

    function save() {
        if ($this->erp_hotels_reservation_orders_log_id !== false) {
            $this->db->set('erp_hotels_reservation_orders_log.erp_hotels_reservation_orders_log_id', $this->erp_hotels_reservation_orders_log_id);
        }

        if ($this->erp_hotels_reservation_orders_id !== false) {
            $this->db->set('erp_hotels_reservation_orders_log.erp_hotels_reservation_orders_id', $this->erp_hotels_reservation_orders_id);
        }

        if ($this->owner_erp_company_types_id !== false) {
            $this->db->set('erp_hotels_reservation_orders_log.owner_erp_company_types_id', $this->owner_erp_company_types_id);
        }
        if ($this->owner_erp_company_id !== false) {
            $this->db->set('erp_hotels_reservation_orders_log.owner_erp_company_id', $this->owner_erp_company_id);
        }
        if ($this->erp_company_types_id !== false) {
            $this->db->set('erp_hotels_reservation_orders_log.erp_company_types_id', $this->erp_company_types_id);
        }
        if ($this->erp_company_id !== false) {
            $this->db->set('erp_hotels_reservation_orders_log.erp_company_id', $this->erp_company_id);
        }
        if ($this->the_date !== false) {
            $this->db->set('erp_hotels_reservation_orders_log.the_date', $this->the_date);
        }
        if ($this->order_date !== false) {
            $this->db->set('erp_hotels_reservation_orders_log.order_date', $this->order_date);
        }
        if ($this->arrival_date !== false) {
            $this->db->set('erp_hotels_reservation_orders_log.arrival_date', $this->arrival_date);
        }
        if ($this->departure_date !== false) {
            $this->db->set('erp_hotels_reservation_orders_log.departure_date', $this->departure_date);
        }
        if ($this->contact_person !== false) {
            $this->db->set('erp_hotels_reservation_orders_log.contact_person', $this->contact_person);
        }
        if ($this->erp_cities_id !== false) {
            $this->db->set('erp_hotels_reservation_orders_log.erp_cities_id', $this->erp_cities_id);
        }
        if ($this->erp_hotels_id !== false) {
            $this->db->set('erp_hotels_reservation_orders_log.erp_hotels_id', $this->erp_hotels_id);
        }
        if ($this->erp_hotels_id_alternative !== false) {
            $this->db->set('erp_hotels_reservation_orders_log.erp_hotels_id_alternative', $this->erp_hotels_id_alternative);
        }
        if ($this->distance_from !== false) {
            $this->db->set('erp_hotels_reservation_orders_log.distance_from', $this->distance_from);
        }
        if ($this->distance_to !== false) {
            $this->db->set('erp_hotels_reservation_orders_log.distance_to', $this->distance_to);
        }
        if ($this->look_to_haram !== false) {
            $this->db->set('erp_hotels_reservation_orders_log.look_to_haram', $this->look_to_haram);
        }
        if ($this->safa_uo_contract_id !== false) {
            $this->db->set('erp_hotels_reservation_orders_log.safa_uo_contract_id', $this->safa_uo_contract_id);
        }
        /*
          if ($this->price_from !== false) {
          $this->db->set('erp_hotels_reservation_orders_log.price_from', $this->price_from);
          }
          if ($this->price_to !== false) {
          $this->db->set('erp_hotels_reservation_orders_log.price_to', $this->price_to);
          }
          if ($this->erp_currencies_id !== false) {
          $this->db->set('erp_hotels_reservation_orders_log.erp_currencies_id', $this->erp_currencies_id);
          }
         */

        if ($this->erp_hotels_reservation_orders_log_id) {
            $this->db->where('erp_hotels_reservation_orders_log.erp_hotels_reservation_orders_log_id', $this->erp_hotels_reservation_orders_log_id)->update('erp_hotels_reservation_orders_log');
        } else {
            $this->db->insert('erp_hotels_reservation_orders_log');
            return $this->db->insert_id();
        }
    }

    function saveNationalities() {
        if ($this->nationalities_erp_hotels_reservation_orders_nationalities_log_id !== false) {
            $this->db->set('erp_hotels_reservation_orders_nationalities_log.erp_hotels_reservation_orders_nationalities_log_id', $this->nationalities_erp_hotels_reservation_orders_nationalities_log_id);
        }
        if ($this->nationalities_erp_hotels_reservation_orders_log_id !== false) {
            $this->db->set('erp_hotels_reservation_orders_nationalities_log.erp_hotels_reservation_orders_log_id', $this->nationalities_erp_hotels_reservation_orders_log_id);
        }
        if ($this->nationalities_erp_nationalities_id !== false) {
            $this->db->set('erp_hotels_reservation_orders_nationalities_log.erp_nationalities_id', $this->nationalities_erp_nationalities_id);
        }

        if ($this->nationalities_erp_hotels_reservation_orders_nationalities_log_id) {
            $this->db->where('erp_hotels_reservation_orders_nationalities_log.erp_hotels_reservation_orders_nationalities_log_id', $this->nationalities_erp_hotels_reservation_orders_nationalities_log_id)->update('erp_hotels_reservation_orders_nationalities_log');
        } else {
            $this->db->insert('erp_hotels_reservation_orders_nationalities_log');
            return $this->db->insert_id();
        }
    }

    function saveHotelAdvantages() {
        if ($this->advantages_erp_hotels_reservation_orders_hotel_advantages_log_id !== false) {
            $this->db->set('erp_hotels_reservation_orders_hotel_advantages_log.erp_hotels_reservation_orders_hotel_advantages_log_id', $this->advantages_erp_hotels_reservation_orders_hotel_advantages_log_id);
        }
        if ($this->advantages_erp_hotels_reservation_orders_log_id !== false) {
            $this->db->set('erp_hotels_reservation_orders_hotel_advantages_log.erp_hotels_reservation_orders_log_id', $this->advantages_erp_hotels_reservation_orders_log_id);
        }
        if ($this->advantages_erp_hotel_advantages_id !== false) {
            $this->db->set('erp_hotels_reservation_orders_hotel_advantages_log.erp_hotel_advantages_id', $this->advantages_erp_hotel_advantages_id);
        }

        if ($this->advantages_erp_hotels_reservation_orders_hotel_advantages_log_id) {
            $this->db->where('erp_hotels_reservation_orders_hotel_advantages_log.erp_hotels_reservation_orders_hotel_advantages_log_id', $this->advantages_erp_hotels_reservation_orders_hotel_advantages_log_id)->update('erp_hotels_reservation_orders_hotel_advantages_log');
        } else {
            $this->db->insert('erp_hotels_reservation_orders_hotel_advantages_log');
            return $this->db->insert_id();
        }
    }

    function saveRoomServices() {
        if ($this->room_services_erp_hotels_reservation_orders_room_services_log_id !== false) {
            $this->db->set('erp_hotels_reservation_orders_room_services_log.erp_hotels_reservation_orders_room_services_log_id', $this->room_services_erp_hotels_reservation_orders_room_services_log_id);
        }
        if ($this->room_services_erp_hotels_reservation_orders_log_id !== false) {
            $this->db->set('erp_hotels_reservation_orders_room_services_log.erp_hotels_reservation_orders_log_id', $this->room_services_erp_hotels_reservation_orders_log_id);
        }
        if ($this->room_services_erp_room_services_id !== false) {
            $this->db->set('erp_hotels_reservation_orders_room_services_log.erp_room_services_id', $this->room_services_erp_room_services_id);
        }

        if ($this->room_services_erp_hotels_reservation_orders_room_services_log_id) {
            $this->db->where('erp_hotels_reservation_orders_room_services_log.erp_hotels_reservation_orders_room_services_log_id', $this->room_services_erp_hotels_reservation_orders_room_services_log_id)->update('erp_hotels_reservation_orders_room_services_log');
        } else {
            $this->db->insert('erp_hotels_reservation_orders_room_services_log');
            return $this->db->insert_id();
        }
    }

    function saveRooms() {
        if ($this->rooms_erp_hotels_reservation_orders_rooms_log_id !== false) {
            $this->db->set('erp_hotels_reservation_orders_rooms_log.erp_hotels_reservation_orders_rooms_log_id', $this->rooms_erp_hotels_reservation_orders_rooms_log_id);
        }
        if ($this->rooms_erp_hotels_reservation_orders_log_id !== false) {
            $this->db->set('erp_hotels_reservation_orders_rooms_log.erp_hotels_reservation_orders_log_id', $this->rooms_erp_hotels_reservation_orders_log_id);
        }

        if ($this->rooms_erp_housingtypes_id !== false) {
            $this->db->set('erp_hotels_reservation_orders_rooms_log.erp_housingtypes_id', $this->rooms_erp_housingtypes_id);
        }
        if ($this->rooms_erp_hotelroomsizes_id !== false) {
            $this->db->set('erp_hotels_reservation_orders_rooms_log.erp_hotelroomsizes_id', $this->rooms_erp_hotelroomsizes_id);
        }
        if ($this->rooms_rooms_count !== false) {
            $this->db->set('erp_hotels_reservation_orders_rooms_log.rooms_count', $this->rooms_rooms_count);
        }
        if ($this->rooms_price_from !== false) {
            $this->db->set('erp_hotels_reservation_orders_rooms_log.price_from', $this->rooms_price_from);
        }
        if ($this->rooms_price_to !== false) {
            $this->db->set('erp_hotels_reservation_orders_rooms_log.price_to', $this->rooms_price_to);
        }
        if ($this->rooms_erp_currencies_id !== false) {
            $this->db->set('erp_hotels_reservation_orders_rooms_log.erp_currencies_id', $this->rooms_erp_currencies_id);
        }
        if ($this->rooms_nights_count !== false) {
            $this->db->set('erp_hotels_reservation_orders_rooms_log.nights_count', $this->rooms_nights_count);
        }
        if ($this->rooms_entry_date !== false) {
            $this->db->set('erp_hotels_reservation_orders_rooms_log.entry_date', $this->rooms_entry_date);
        }
        if ($this->rooms_exit_date !== false) {
            $this->db->set('erp_hotels_reservation_orders_rooms_log.exit_date', $this->rooms_exit_date);
        }
        if ($this->rooms_erp_meals_id !== false) {
            $this->db->set('erp_hotels_reservation_orders_rooms_log.erp_meals_id', $this->rooms_erp_meals_id);
        }

        if ($this->rooms_erp_hotels_reservation_orders_rooms_log_id) {
            $this->db->where('erp_hotels_reservation_orders_rooms_log.erp_hotels_reservation_orders_rooms_log_id', $this->rooms_erp_hotels_reservation_orders_rooms_log_id)->update('erp_hotels_reservation_orders_rooms_log');
        } else {
            $this->db->insert('erp_hotels_reservation_orders_rooms_log');
            return $this->db->insert_id();
        }
    }

    function saveMeals() {
        if ($this->meals_erp_hotels_reservation_orders_meals_log_id !== false) {
            $this->db->set('erp_hotels_reservation_orders_meals_log.erp_hotels_reservation_orders_meals_log_id', $this->meals_erp_hotels_reservation_orders_meals_log_id);
        }
        if ($this->meals_erp_hotels_reservation_orders_log_id !== false) {
            $this->db->set('erp_hotels_reservation_orders_meals_log.erp_hotels_reservation_orders_log_id', $this->meals_erp_hotels_reservation_orders_log_id);
        }
        if ($this->meals_erp_meals_id !== false) {
            $this->db->set('erp_hotels_reservation_orders_meals_log.erp_meals_id', $this->meals_erp_meals_id);
        }
        if ($this->meals_meals_count !== false) {
            $this->db->set('erp_hotels_reservation_orders_meals_log.meals_count', $this->meals_meals_count);
        }
        if ($this->meals_erp_hotels_reservation_orders_meals_log_id) {
            $this->db->where('erp_hotels_reservation_orders_meals_log.erp_hotels_reservation_orders_meals_log_id', $this->meals_erp_hotels_reservation_orders_meals_log_id)->update('erp_hotels_reservation_orders_meals_log');
        } else {
            $this->db->insert('erp_hotels_reservation_orders_meals_log');
            return $this->db->insert_id();
        }
    }

    function delete() {
        if ($this->erp_hotels_reservation_orders_log_id !== false) {
            $this->db->where('erp_hotels_reservation_orders_log.erp_hotels_reservation_orders_log_id', $this->erp_hotels_reservation_orders_log_id);
        }
        if ($this->erp_hotels_reservation_orders_id !== false) {
            $this->db->where('erp_hotels_reservation_orders_log.erp_hotels_reservation_orders_id', $this->erp_hotels_reservation_orders_id);
        }
        if ($this->owner_erp_company_types_id !== false) {
            $this->db->where('erp_hotels_reservation_orders_log.owner_erp_company_types_id', $this->owner_erp_company_types_id);
        }
        if ($this->owner_erp_company_id !== false) {
            $this->db->where('erp_hotels_reservation_orders_log.owner_erp_company_id', $this->owner_erp_company_id);
        }
        if ($this->erp_company_types_id !== false) {
            $this->db->where('erp_hotels_reservation_orders_log.erp_company_types_id', $this->erp_company_types_id);
        }
        if ($this->erp_company_id !== false) {
            $this->db->where('erp_hotels_reservation_orders_log.erp_company_id', $this->erp_company_id);
        }
        if ($this->the_date !== false) {
            $this->db->where('erp_hotels_reservation_orders_log.the_date', $this->the_date);
        }
        if ($this->order_date !== false) {
            $this->db->where('erp_hotels_reservation_orders_log.order_date', $this->order_date);
        }
        if ($this->arrival_date !== false) {
            $this->db->where('erp_hotels_reservation_orders_log.arrival_date', $this->arrival_date);
        }
        if ($this->departure_date !== false) {
            $this->db->where('erp_hotels_reservation_orders_log.departure_date', $this->departure_date);
        }
        if ($this->contact_person !== false) {
            $this->db->where('erp_hotels_reservation_orders_log.contact_person', $this->contact_person);
        }
        if ($this->erp_cities_id !== false) {
            $this->db->where('erp_hotels_reservation_orders_log.erp_cities_id', $this->erp_cities_id);
        }
        if ($this->erp_hotels_id !== false) {
            $this->db->where('erp_hotels_reservation_orders_log.erp_hotels_id', $this->erp_hotels_id);
        }
        if ($this->erp_hotels_id_alternative !== false) {
            $this->db->where('erp_hotels_reservation_orders_log.erp_hotels_id_alternative', $this->erp_hotels_id_alternative);
        }
        if ($this->distance_from !== false) {
            $this->db->where('erp_hotels_reservation_orders_log.distance_from', $this->distance_from);
        }
        if ($this->distance_to !== false) {
            $this->db->where('erp_hotels_reservation_orders_log.distance_to', $this->distance_to);
        }
        if ($this->look_to_haram !== false) {
            $this->db->where('erp_hotels_reservation_orders_log.look_to_haram', $this->look_to_haram);
        }
        if ($this->safa_uo_contract_id !== false) {
            $this->db->where('erp_hotels_reservation_orders_log.safa_uo_contract_id', $this->safa_uo_contract_id);
        }

        /*
          if ($this->price_from !== false) {
          $this->db->where('erp_hotels_reservation_orders_log.price_from', $this->price_from);
          }
          if ($this->price_to !== false) {
          $this->db->where('erp_hotels_reservation_orders_log.price_to', $this->price_to);
          }
          if ($this->erp_currencies_id !== false) {
          $this->db->where('erp_hotels_reservation_orders_log.erp_currencies_id', $this->erp_currencies_id);
          }
         */

        $this->db->delete('erp_hotels_reservation_orders_log');
        return $this->db->affected_rows();
    }

    function deleteNationalities() {
        if ($this->nationalities_erp_hotels_reservation_orders_nationalities_log_id !== false) {
            $this->db->where('erp_hotels_reservation_orders_nationalities_log.erp_hotels_reservation_orders_nationalities_log_id', $this->nationalities_erp_hotels_reservation_orders_nationalities_log_id);
        }
        if ($this->nationalities_erp_hotels_reservation_orders_log_id !== false) {
            $this->db->where('erp_hotels_reservation_orders_nationalities_log.erp_hotels_reservation_orders_log_id', $this->nationalities_erp_hotels_reservation_orders_log_id);
        }
        if ($this->nationalities_erp_nationalities_id !== false) {
            $this->db->where('erp_hotels_reservation_orders_nationalities_log.erp_nationalities_id', $this->nationalities_erp_nationalities_id);
        }

        $this->db->delete('erp_hotels_reservation_orders_nationalities_log');
        return $this->db->affected_rows();
    }

    function deleteHotelAdvantages() {
        if ($this->advantages_erp_hotels_reservation_orders_hotel_advantages_log_id !== false) {
            $this->db->where('erp_hotels_reservation_orders_hotel_advantages_log.erp_hotels_reservation_orders_hotel_advantages_log_id', $this->advantages_erp_hotels_reservation_orders_hotel_advantages_log_id);
        }
        if ($this->advantages_erp_hotels_reservation_orders_log_id !== false) {
            $this->db->where('erp_hotels_reservation_orders_hotel_advantages_log.erp_hotels_reservation_orders_log_id', $this->advantages_erp_hotels_reservation_orders_log_id);
        }
        if ($this->advantages_erp_hotel_advantages_id !== false) {
            $this->db->where('erp_hotels_reservation_orders_hotel_advantages_log.erp_hotel_advantages_id', $this->advantages_erp_hotel_advantages_id);
        }

        $this->db->delete('erp_hotels_reservation_orders_hotel_advantages_log');
        return $this->db->affected_rows();
    }

    function deleteRoomServices() {
        if ($this->room_services_erp_hotels_reservation_orders_room_services_log_id !== false) {
            $this->db->where('erp_hotels_reservation_orders_room_services_log.erp_hotels_reservation_orders_room_services_log_id', $this->room_services_erp_hotels_reservation_orders_room_services_log_id);
        }
        if ($this->room_services_erp_hotels_reservation_orders_log_id !== false) {
            $this->db->where('erp_hotels_reservation_orders_room_services_log.erp_hotels_reservation_orders_log_id', $this->room_services_erp_hotels_reservation_orders_log_id);
        }
        if ($this->room_services_erp_room_services_id !== false) {
            $this->db->where('erp_hotels_reservation_orders_room_services_log.erp_room_services_id', $this->room_services_erp_room_services_id);
        }

        $this->db->delete('erp_hotels_reservation_orders_room_services_log');
        return $this->db->affected_rows();
    }

    function deleteRooms() {
        if ($this->rooms_erp_hotels_reservation_orders_rooms_log_id !== false) {
            $this->db->where('erp_hotels_reservation_orders_rooms_log.erp_hotels_reservation_orders_rooms_log_id', $this->rooms_erp_hotels_reservation_orders_rooms_log_id);
        }
        if ($this->rooms_erp_hotels_reservation_orders_log_id !== false) {
            $this->db->where('erp_hotels_reservation_orders_rooms_log.erp_hotels_reservation_orders_log_id', $this->rooms_erp_hotels_reservation_orders_log_id);
        }

        if ($this->rooms_erp_housingtypes_id !== false) {
            $this->db->where('erp_hotels_reservation_orders_rooms_log.erp_housingtypes_id', $this->rooms_erp_housingtypes_id);
        }
        if ($this->rooms_erp_hotelroomsizes_id !== false) {
            $this->db->where('erp_hotels_reservation_orders_rooms_log.erp_hotelroomsizes_id', $this->rooms_erp_hotelroomsizes_id);
        }
        if ($this->rooms_rooms_count !== false) {
            $this->db->where('erp_hotels_reservation_orders_rooms_log.rooms_count', $this->rooms_rooms_count);
        }
        if ($this->rooms_price_from !== false) {
            $this->db->where('erp_hotels_reservation_orders_rooms_log.price_from', $this->rooms_price_from);
        }
        if ($this->rooms_price_to !== false) {
            $this->db->where('erp_hotels_reservation_orders_rooms_log.price_to', $this->rooms_price_to);
        }
        if ($this->rooms_erp_currencies_id !== false) {
            $this->db->where('erp_hotels_reservation_orders_rooms_log.erp_currencies_id', $this->rooms_erp_currencies_id);
        }
        if ($this->rooms_nights_count !== false) {
            $this->db->where('erp_hotels_reservation_orders_rooms_log.nights_count', $this->rooms_nights_count);
        }
        if ($this->rooms_entry_date !== false) {
            $this->db->where('erp_hotels_reservation_orders_rooms_log.entry_date', $this->rooms_entry_date);
        }
        if ($this->rooms_exit_date !== false) {
            $this->db->where('erp_hotels_reservation_orders_rooms_log.exit_date', $this->rooms_exit_date);
        }
        if ($this->rooms_erp_meals_id !== false) {
            $this->db->where('erp_hotels_reservation_orders_rooms_log.erp_meals_id', $this->rooms_erp_meals_id);
        }

        $this->db->delete('erp_hotels_reservation_orders_rooms_log');
        return $this->db->affected_rows();
    }

    function deleteMeals() {
        if ($this->meals_erp_hotels_reservation_orders_meals_log_id !== false) {
            $this->db->where('erp_hotels_reservation_orders_meals_log.erp_hotels_reservation_orders_meals_log_id', $this->meals_erp_hotels_reservation_orders_meals_log_id);
        }
        if ($this->meals_erp_hotels_reservation_orders_log_id !== false) {
            $this->db->where('erp_hotels_reservation_orders_meals_log.erp_hotels_reservation_orders_log_id', $this->meals_erp_hotels_reservation_orders_log_id);
        }
        if ($this->meals_erp_meals_id !== false) {
            $this->db->where('erp_hotels_reservation_orders_meals_log.erp_meals_id', $this->meals_erp_meals_id);
        }
        if ($this->meals_meals_count !== false) {
            $this->db->where('erp_hotels_reservation_orders_meals_log.meals_count', $this->meals_meals_count);
        }

        $this->db->delete('erp_hotels_reservation_orders_meals_log');
        return $this->db->affected_rows();
    }

    function get($rows_no = false) {

        if ($this->custom_select !== false) {
            $this->db->select('erp_hotels_reservation_orders_log_id');
            $this->db->select($this->custom_select);
        }

        if ($this->erp_hotels_reservation_orders_log_id !== false) {
            $this->db->where('erp_hotels_reservation_orders_log.erp_hotels_reservation_orders_log_id', $this->erp_hotels_reservation_orders_log_id);
        }
        if ($this->erp_hotels_reservation_orders_id !== false) {
            $this->db->where('erp_hotels_reservation_orders_log.erp_hotels_reservation_orders_id', $this->erp_hotels_reservation_orders_id);
        }
        if ($this->owner_erp_company_types_id !== false) {
            $this->db->where('erp_hotels_reservation_orders_log.owner_erp_company_types_id', $this->owner_erp_company_types_id);
        }
        if ($this->owner_erp_company_id !== false) {
            $this->db->where('erp_hotels_reservation_orders_log.owner_erp_company_id', $this->owner_erp_company_id);
        }
        if ($this->erp_company_types_id !== false) {
            $this->db->where('erp_hotels_reservation_orders_log.erp_company_types_id', $this->erp_company_types_id);
        }
        if ($this->erp_company_id !== false) {
            $this->db->where('erp_hotels_reservation_orders_log.erp_company_id', $this->erp_company_id);
        }
        if ($this->the_date !== false) {
            $this->db->where('erp_hotels_reservation_orders_log.the_date', $this->the_date);
        }
        if ($this->order_date !== false) {
            $this->db->where('erp_hotels_reservation_orders_log.order_date', $this->order_date);
        }
        if ($this->arrival_date !== false) {
            $this->db->where('erp_hotels_reservation_orders_log.arrival_date', $this->arrival_date);
        }
        if ($this->departure_date !== false) {
            $this->db->where('erp_hotels_reservation_orders_log.departure_date', $this->departure_date);
        }
        if ($this->contact_person !== false) {
            $this->db->where('erp_hotels_reservation_orders_log.contact_person', $this->contact_person);
        }
        if ($this->erp_cities_id !== false) {
            $this->db->where('erp_hotels_reservation_orders_log.erp_cities_id', $this->erp_cities_id);
        }
        if ($this->erp_hotels_id !== false) {
            $this->db->where('erp_hotels_reservation_orders_log.erp_hotels_id', $this->erp_hotels_id);
        }
        if ($this->erp_hotels_id_alternative !== false) {
            $this->db->where('erp_hotels_reservation_orders_log.erp_hotels_id_alternative', $this->erp_hotels_id_alternative);
        }
        if ($this->distance_from !== false) {
            $this->db->where('erp_hotels_reservation_orders_log.distance_from', $this->distance_from);
        }
        if ($this->distance_to !== false) {
            $this->db->where('erp_hotels_reservation_orders_log.distance_to', $this->distance_to);
        }
        if ($this->look_to_haram !== false) {
            $this->db->where('erp_hotels_reservation_orders_log.look_to_haram', $this->look_to_haram);
        }
        if ($this->safa_uo_contract_id !== false) {
            $this->db->where('erp_hotels_reservation_orders_log.safa_uo_contract_id', $this->safa_uo_contract_id);
        }
        /*
          if ($this->price_from !== false) {
          $this->db->where('erp_hotels_reservation_orders_log.price_from', $this->price_from);
          }
          if ($this->price_to !== false) {
          $this->db->where('erp_hotels_reservation_orders_log.price_to', $this->price_to);
          }
          if ($this->erp_currencies_id !== false) {
          $this->db->where('erp_hotels_reservation_orders_log.erp_currencies_id', $this->erp_currencies_id);
          }
         */


        if ($this->master_erp_hotelroomsizes_id !== false) {
            $this->db->where('erp_hotels_reservation_orders_rooms_log.erp_hotelroomsizes_id', $this->master_erp_hotelroomsizes_id);
        }


        //$this->db->select("erp_hotels_reservation_orders_log.*, erp_hotels.name_ar as hotel_name_ar, erp_hotels.name_la as hotel_name_la, IFNULL(erp_hotels_reservation_orders_rooms_log.total_rooms_count, 0) as total_rooms_count, GROUP_CONCAT(DISTINCT orders_rooms2.erp_hotelroomsizes_id separator ',') AS room_sizes_string, GROUP_CONCAT(DISTINCT orders_rooms2.rooms_count separator ',') AS room_count_string ",false);

        $this->db->select("erp_hotels_reservation_orders_log.*, erp_hotels.name_ar as hotel_name_ar, erp_hotels.name_la as hotel_name_la, IFNULL(erp_hotels_reservation_orders_rooms_log.total_rooms_count, 0) as total_rooms_count, 
        GROUP_CONCAT(DISTINCT orders_rooms2.erp_hotelroomsizes_id separator ',') AS room_sizes_string, GROUP_CONCAT(DISTINCT orders_rooms2.rooms_count separator ',') AS room_count_string, 
        safa_uos.name_ar as uo_name_ar, safa_uos.name_la as uo_name_la, safa_eas.name_ar as ea_name_ar, safa_eas.name_la as ea_name_la, safa_hms.name_ar as hm_name_ar, safa_hms.name_la as hm_name_la, 
        to_safa_uos.name_ar as to_uo_name_ar, to_safa_uos.name_la as to_uo_name_la, to_safa_eas.name_ar as to_ea_name_ar, to_safa_eas.name_la as to_ea_name_la, to_safa_hms.name_ar as to_hm_name_ar, to_safa_hms.name_la as to_hm_name_la, 
        
        orders_rooms2.entry_date as room_entry_date, 
		, orders_rooms2.exit_date as room_exit_date, orders_rooms2.erp_housingtypes_id as room_erp_housingtypes_id, orders_rooms2.erp_hotelroomsizes_id as room_erp_hotelroomsizes_id 
        ", false);


        /*
          if ($this->owner_erp_company_types_id !== false) {
          if ($this->owner_erp_company_types_id == 2) {
          $this->db->select("erp_hotels_reservation_orders_log.*, erp_hotels.name_ar as hotel_name_ar, erp_hotels.name_la as hotel_name_la, IFNULL(erp_hotels_reservation_orders_rooms_log.total_rooms_count, 0) as total_rooms_count, GROUP_CONCAT(DISTINCT orders_rooms2.erp_hotelroomsizes_id separator ',') AS room_sizes_string, GROUP_CONCAT(DISTINCT orders_rooms2.rooms_count separator ',') AS room_count_string, safa_uos.name_ar as company_name_ar, safa_uos.name_la as company_name_la  ",false);
          } else if ($this->owner_erp_company_types_id == 3) {
          $this->db->select("erp_hotels_reservation_orders_log.*, erp_hotels.name_ar as hotel_name_ar, erp_hotels.name_la as hotel_name_la, IFNULL(erp_hotels_reservation_orders_rooms_log.total_rooms_count, 0) as total_rooms_count, GROUP_CONCAT(DISTINCT orders_rooms2.erp_hotelroomsizes_id separator ',') AS room_sizes_string, GROUP_CONCAT(DISTINCT orders_rooms2.rooms_count separator ',') AS room_count_string, safa_eas.name_ar as company_name_ar, safa_eas.name_la as company_name_la ",false);
          } else if ($this->owner_erp_company_types_id == 5) {
          $this->db->select("erp_hotels_reservation_orders_log.*, erp_hotels.name_ar as hotel_name_ar, erp_hotels.name_la as hotel_name_la, IFNULL(erp_hotels_reservation_orders_rooms_log.total_rooms_count, 0) as total_rooms_count, GROUP_CONCAT(DISTINCT orders_rooms2.erp_hotelroomsizes_id separator ',') AS room_sizes_string, GROUP_CONCAT(DISTINCT orders_rooms2.rooms_count separator ',') AS room_count_string, safa_hms.name_ar as company_name_ar, safa_hms.name_la as company_name_la ",false);
          }
          }
         */

        $this->db->from('erp_hotels_reservation_orders_log');
        $this->db->join('erp_hotels', 'erp_hotels.erp_hotel_id=erp_hotels_reservation_orders_log.erp_hotels_id', 'left');
        $this->db->join('(select erp_hotels_reservation_orders_log_id,SUM(rooms_count) as total_rooms_count from erp_hotels_reservation_orders_rooms_log group by erp_hotels_reservation_orders_log_id) as erp_hotels_reservation_orders_rooms_log', 'erp_hotels_reservation_orders_rooms_log.erp_hotels_reservation_orders_log_id=erp_hotels_reservation_orders_log.erp_hotels_reservation_orders_log_id', 'left');
        $this->db->join('erp_hotels_reservation_orders_rooms_log as orders_rooms2', 'orders_rooms2.erp_hotels_reservation_orders_log_id=erp_hotels_reservation_orders_log.erp_hotels_reservation_orders_log_id', 'left');

        $this->db->join('safa_uos', 'safa_uos.safa_uo_id=erp_hotels_reservation_orders_log.owner_erp_company_id', 'left');
        $this->db->join('safa_eas', 'safa_eas.safa_ea_id=erp_hotels_reservation_orders_log.owner_erp_company_id', 'left');
        $this->db->join('safa_hms', 'safa_hms.safa_hm_id=erp_hotels_reservation_orders_log.owner_erp_company_id', 'left');

        $this->db->join('safa_uos as to_safa_uos', 'to_safa_uos.safa_uo_id=erp_hotels_reservation_orders_log.erp_company_id', 'left');
        $this->db->join('safa_eas as to_safa_eas', 'to_safa_eas.safa_ea_id=erp_hotels_reservation_orders_log.erp_company_id', 'left');
        $this->db->join('safa_hms as to_safa_hms', 'to_safa_hms.safa_hm_id=erp_hotels_reservation_orders_log.erp_company_id', 'left');


        $this->db->group_by('erp_hotels_reservation_orders_log.erp_hotels_reservation_orders_log_id');

        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get();

        $query_text = $this->db->last_query();
        //print_r($query_text); exit;


        if ($rows_no)
            return $query->num_rows();

        if ($this->erp_hotels_reservation_orders_log_id)
            return $query->row();
        else
            return $query->result();
    }

    function getNationalities($rows_no = false) {

        if ($this->custom_select !== false) {
            $this->db->select('erp_hotels_reservation_orders_nationalities_log_id');
            $this->db->select($this->custom_select);
        }

        if ($this->nationalities_erp_hotels_reservation_orders_nationalities_log_id !== false) {
            $this->db->where('erp_hotels_reservation_orders_nationalities_log.erp_hotels_reservation_orders_nationalities_log_id', $this->nationalities_erp_hotels_reservation_orders_nationalities_log_id);
        }
        if ($this->nationalities_erp_hotels_reservation_orders_log_id !== false) {
            $this->db->where('erp_hotels_reservation_orders_nationalities_log.erp_hotels_reservation_orders_log_id', $this->nationalities_erp_hotels_reservation_orders_log_id);
        }
        if ($this->nationalities_erp_nationalities_id !== false) {
            $this->db->where('erp_hotels_reservation_orders_nationalities_log.erp_nationalities_id', $this->nationalities_erp_nationalities_id);
        }

        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('erp_hotels_reservation_orders_nationalities_log');
        if ($rows_no)
            return $query->num_rows();

        if ($this->nationalities_erp_hotels_reservation_orders_nationalities_log_id)
            return $query->row();
        else
            return $query->result();
    }

    function getHotelAdvantages($rows_no = false) {
        if ($this->custom_select !== false) {
            $this->db->select('erp_hotels_reservation_orders_hotel_advantages_log_id');
            $this->db->select($this->custom_select);
        }

        if ($this->advantages_erp_hotels_reservation_orders_hotel_advantages_log_id !== false) {
            $this->db->where('erp_hotels_reservation_orders_hotel_advantages_log.erp_hotels_reservation_orders_hotel_advantages_log_id', $this->advantages_erp_hotels_reservation_orders_hotel_advantages_log_id);
        }
        if ($this->advantages_erp_hotels_reservation_orders_log_id !== false) {
            $this->db->where('erp_hotels_reservation_orders_hotel_advantages_log.erp_hotels_reservation_orders_log_id', $this->advantages_erp_hotels_reservation_orders_log_id);
        }
        if ($this->advantages_erp_hotel_advantages_id !== false) {
            $this->db->where('erp_hotels_reservation_orders_hotel_advantages_log.erp_hotel_advantages_id', $this->advantages_erp_hotel_advantages_id);
        }

        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('erp_hotels_reservation_orders_hotel_advantages_log');

        $query_text = $this->db->last_query();

        if ($rows_no)
            return $query->num_rows();

        if ($this->advantages_erp_hotels_reservation_orders_hotel_advantages_log_id)
            return $query->row();
        else
            return $query->result();
    }

    function getRoomServices($rows_no = false) {
        if ($this->custom_select !== false) {
            $this->db->select('erp_hotels_reservation_orders_room_services_log_id');
            $this->db->select($this->custom_select);
        }

        if ($this->room_services_erp_hotels_reservation_orders_room_services_log_id !== false) {
            $this->db->where('erp_hotels_reservation_orders_room_services_log.erp_hotels_reservation_orders_room_services_log_id', $this->room_services_erp_hotels_reservation_orders_room_services_log_id);
        }
        if ($this->room_services_erp_hotels_reservation_orders_log_id !== false) {
            $this->db->where('erp_hotels_reservation_orders_room_services_log.erp_hotels_reservation_orders_log_id', $this->room_services_erp_hotels_reservation_orders_log_id);
        }
        if ($this->room_services_erp_room_services_id !== false) {
            $this->db->where('erp_hotels_reservation_orders_room_services_log.erp_room_services_id', $this->room_services_erp_room_services_id);
        }

        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('erp_hotels_reservation_orders_room_services_log');
        if ($rows_no)
            return $query->num_rows();

        if ($this->room_services_erp_hotels_reservation_orders_room_services_log_id)
            return $query->row();
        else
            return $query->result();
    }

    function getRooms($rows_no = false) {
        if ($this->custom_select !== false) {
            $this->db->select('erp_hotels_reservation_orders_rooms_log_id');
            $this->db->select($this->custom_select);
        }

        if ($this->rooms_erp_hotels_reservation_orders_rooms_log_id !== false) {
            $this->db->where('erp_hotels_reservation_orders_rooms_log.erp_hotels_reservation_orders_rooms_log_id', $this->rooms_erp_hotels_reservation_orders_rooms_log_id);
        }
        if ($this->rooms_erp_hotels_reservation_orders_log_id !== false) {
            $this->db->where('erp_hotels_reservation_orders_rooms_log.erp_hotels_reservation_orders_log_id', $this->rooms_erp_hotels_reservation_orders_log_id);
        }

        /*
          if ($this->rooms_erp_housingtypes_id !== false) {
          $this->db->where('erp_hotels_reservation_orders_rooms_log.erp_housingtypes_id', $this->rooms_erp_housingtypes_id);
          }
          if ($this->rooms_erp_hotelroomsizes_id !== false) {
          $this->db->where('erp_hotels_reservation_orders_rooms_log.erp_hotelroomsizes_id', $this->rooms_erp_hotelroomsizes_id);
          }
          if ($this->rooms_rooms_count !== false) {
          $this->db->where('erp_hotels_reservation_orders_rooms_log.rooms_count', $this->rooms_rooms_count);
          }
          if ($this->rooms_price_from !== false) {
          $this->db->where('erp_hotels_reservation_orders_rooms_log.price_from', $this->rooms_price_from);
          }
          if ($this->rooms_price_to !== false) {
          $this->db->where('erp_hotels_reservation_orders_rooms_log.price_to', $this->rooms_price_to);
          }
          if ($this->rooms_erp_currencies_id !== false) {
          $this->db->where('erp_hotels_reservation_orders_rooms_log.erp_currencies_id', $this->rooms_erp_currencies_id);
          }
          if ($this->rooms_nights_count !== false) {
          $this->db->where('erp_hotels_reservation_orders_rooms_log.nights_count', $this->rooms_nights_count);
          }
          if ($this->rooms_entry_date !== false) {
          $this->db->where('erp_hotels_reservation_orders_rooms_log.entry_date', $this->rooms_entry_date);
          }
          if ($this->rooms_exit_date !== false) {
          $this->db->where('erp_hotels_reservation_orders_rooms_log.exit_date', $this->rooms_exit_date);
          }
          if ($this->rooms_erp_meals_id !== false) {
          $this->db->where('erp_hotels_reservation_orders_rooms_log.erp_meals_id', $this->rooms_erp_meals_id);
          }
         */

        $this->db->select("erp_hotels_reservation_orders_rooms_log.*, erp_hotelroomsizes.name_ar as hotelroomsize_name_ar, erp_hotelroomsizes.name_la as hotelroomsize_name_la", false);
        $this->db->from('erp_hotels_reservation_orders_rooms_log');
        $this->db->join('erp_hotelroomsizes', 'erp_hotelroomsizes.erp_hotelroomsize_id=erp_hotels_reservation_orders_rooms_log.erp_hotelroomsizes_id', 'left');


        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get();

        $query_text = $this->db->last_query();

        if ($rows_no)
            return $query->num_rows();

        if ($this->rooms_erp_hotels_reservation_orders_rooms_log_id)
            return $query->row();
        else
            return $query->result();
    }

    function getMeals($rows_no = false) {
        if ($this->custom_select !== false) {
            $this->db->select('erp_hotels_reservation_orders_meals_log_id');
            $this->db->select($this->custom_select);
        }

        if ($this->meals_erp_hotels_reservation_orders_meals_log_id !== false) {
            $this->db->where('erp_hotels_reservation_orders_meals_log.erp_hotels_reservation_orders_meals_log_id', $this->meals_erp_hotels_reservation_orders_meals_log_id);
        }
        if ($this->meals_erp_hotels_reservation_orders_log_id !== false) {
            $this->db->where('erp_hotels_reservation_orders_meals_log.erp_hotels_reservation_orders_log_id', $this->meals_erp_hotels_reservation_orders_log_id);
        }

        /*
          if ($this->meals_erp_meals_id !== false) {
          $this->db->where('erp_hotels_reservation_orders_meals_log.erp_meals_id', $this->meals_erp_meals_id);
          }
          if ($this->meals_meals_count !== false) {
          $this->db->where('erp_hotels_reservation_orders_meals_log.meals_count', $this->meals_meals_count);
          }
         */

        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('erp_hotels_reservation_orders_meals_log');
        if ($rows_no)
            return $query->num_rows();

        if ($this->meals_erp_hotels_reservation_orders_meals_log_id)
            return $query->row();
        else
            return $query->result();
    }

    function getCompaniesByCompanyType($erp_company_type_id) {

        if ($erp_company_type_id == 1) {
            $this->db->select('erp_admins.erp_admin_id as id, erp_admins.username as name_ar, erp_admins.username as name_la');
            $this->db->from('erp_admins');
        } else if ($erp_company_type_id == 2) {
            $this->db->select('safa_uos.safa_uo_id as id, safa_uos.name_ar, safa_uos.name_la');
            $this->db->from('safa_uos');
            $this->db->where('safa_uos.disabled', 0);
            //$this->db->where('safa_uos.', '');
        } else if ($erp_company_type_id == 3) {
            $this->db->select('safa_eas.safa_ea_id as id, safa_eas.name_ar, safa_eas.name_la');
            $this->db->from('safa_eas');
            $this->db->where('safa_eas.disabled', 0);
        } else if ($erp_company_type_id == 4) {
            $this->db->select('safa_itos.safa_ito_id as id, safa_itos.name_ar, safa_itos.name_la');
            $this->db->from('safa_itos');
            $this->db->where('safa_itos.disabled', 0);
        } else if ($erp_company_type_id == 5) {
            $this->db->select('safa_hms.safa_hm_id as id, safa_hms.name_ar, safa_hms.name_la');
            $this->db->from('safa_hms');
        } else {
            $this->db->select('safa_eas.safa_ea_id as id, safa_eas.name_ar, safa_eas.name_la');
            $this->db->from('safa_eas');
        }


        $query = $this->db->get();

        //$query_text=$this->db->last_query();
        //echo $query_text; exit;

        return $query->result();
    }

    function getMaxId() {
        $this->db->select_max('erp_hotels_reservation_orders_log_id');
        $query = $this->db->get('erp_hotels_reservation_orders_log');
        $row = $query->row();
        if (isset($row)) {
            return $row->erp_hotels_reservation_orders_log_id;
        } else {
            return 0;
        }
    }

}

/* End of file erp_hotels_reservation_orders_log_model.php */
/* Location: ./application/models/erp_hotels_reservation_orders_log_model.php */