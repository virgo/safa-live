<?php

class Trip_externalsegments_model extends CI_Model {

    /**
     * store this safa_trip_externalsegments table name.
     *
     * @var string
     * @access public
     */
    public $table = 'safa_trip_externalsegments';

    /**
     * Constructor
     *
     * @access public
     */
    function Main_model() {
        parent::__construct();
    }

    function get_trip_externalsegments_by_trip_id($safa_externaltriptype_id, $trip_id) {
        $this->db->select("safa_trip_externalsegments.*, start_ports." . name() . " as start_ports_name, end_ports." . name() . " as end_ports_name , safa_transporters." . name() . " as transporter_name, safa_externalsegmenttypes." . name() . " as safa_externalsegmenttype_name ");
        $this->db->from('safa_trip_externalsegments');
        $this->db->join('erp_port_halls as start_port_halls', 'safa_trip_externalsegments.start_port_hall_id = start_port_halls.erp_port_hall_id', 'left');
        $this->db->join('erp_port_halls as end_port_halls', 'safa_trip_externalsegments.end_port_hall_id = end_port_halls.erp_port_hall_id', 'left');
        $this->db->join('erp_ports as start_ports', 'start_port_halls.erp_port_id = start_ports.erp_port_id', 'left');
        $this->db->join('erp_ports as end_ports', 'end_port_halls.erp_port_id = end_ports.erp_port_id', 'left');

        $this->db->join('safa_transporters', 'safa_transporters.safa_transporter_id = safa_trip_externalsegments.safa_transporter_id', 'left');
        $this->db->join('safa_externalsegmenttypes', 'safa_externalsegmenttypes.safa_externalsegmenttype_id = safa_trip_externalsegments.safa_externalsegmenttype_id', 'left');


        $this->db->where("safa_trip_id", $trip_id);
        $this->db->where("safa_externaltriptype_id", $safa_externaltriptype_id);
        $query = $this->db->get();
        return $query->result();
    }

    function delete_trip_externalsegments_by_trip_id($safa_externaltriptype_id, $trip_id) {
        $this->db->where("safa_trip_id", $trip_id);
        $this->db->where("safa_externaltriptype_id", $safa_externaltriptype_id);
        return $this->db->delete("safa_trip_externalsegments");
    }

    function insert($data) {
        $this->db->insert('safa_trip_externalsegments', $data);
        return $this->db->insert_id();
    }

    function update($trip_externalsegment_id, $data) {
        $this->db->where('safa_trip_externalsegment_id', $trip_externalsegment_id);
        $this->db->update('safa_trip_externalsegments', $data);
    }

    function delete_by_trip_and_id($trip_id, $trip_externalsegment_id) {
        $this->db->where("safa_trip_id", $trip_id);
        $this->db->where("safa_trip_externalsegment_id", $trip_externalsegment_id);
        return $this->db->delete("safa_trip_externalsegments");
    }

}

?>