<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Contracts_settings_model extends CI_Model {

    public $safa_uo_contract_setting_id = FALSE;
    public $safa_uo_contract_id = FALSE;
    public $services_buy_uo_visa = FALSE;
    public $services_buy_uo_hotels = FALSE;
    public $services_buy_uo_transporter = FALSE;
    public $services_buy_other_hotels = FALSE;
    public $services_buy_other_transporter = FALSE;
    public $services_buy_other_services = FALSE;
    public $transport_cycle_from = FALSE;
    public $transporters_us = FALSE;
    public $can_use_other_uo = FALSE;
    public $can_use_hotel_marking = FALSE;
    public $can_book_couch = FALSE;
    public $visa_price = FALSE;
    public $visa_price_currency_id = FALSE;
    public $max_limit_visa = FALSE;
    public $credit_balance = FALSE;
    public $credit_balance_currency_id = FALSE;
    public $can_over_limit = FALSE;
    public $add_passports_accomodation_without_visa = FALSE;
    
    public $custom_select = FALSE;
    public $limit = FALSE;
    public $offset = FALSE;
    public $order_by = FALSE;
    public $by_eas_id = FALSE;

    function __construct() {
        parent::__construct();
        // if the uo_has loged in// 
    }

    function get($rows_no = FALSE) {

        if ($this->custom_select !== FALSE) {
            $this->db->select('safa_uo_contract_id');
            $this->db->select($this->custom_select);
        }

        if ($this->safa_uo_contract_setting_id !== FALSE)
            $this->db->where('safa_uo_contracts_settings.safa_uo_contract_setting_id', $this->safa_uo_contract_setting_id);

        if ($this->safa_uo_contract_id !== FALSE)
            $this->db->where('safa_uo_contracts_settings.safa_uo_contract_id ', $this->safa_uo_contract_id);

        if ($this->services_buy_uo_visa !== FALSE)
            $this->db->where('safa_uo_contracts_settings.services_buy_uo_visa', $this->services_buy_uo_visa);

        if ($this->services_buy_uo_hotels !== FALSE)
            $this->db->where('safa_uo_contracts_settings.services_buy_uo_hotels', $this->services_buy_uo_hotels);

        if ($this->services_buy_uo_transporter !== FALSE)
            $this->db->like('safa_uo_contracts_settings.services_buy_uo_transporter', $this->services_buy_uo_transporter);

        if ($this->services_buy_other_hotels !== FALSE)
            $this->db->like('safa_uo_contracts_settings.services_buy_other_hotels', $this->services_buy_other_hotels);

        if ($this->services_buy_other_transporter !== FALSE)
            $this->db->where('safa_uo_contracts_settings.services_buy_other_transporter', $this->services_buy_other_transporter);

        if ($this->services_buy_other_services !== FALSE)
            $this->db->where('safa_uo_contracts_settings.services_buy_other_services', $this->services_buy_other_services);

        if ($this->transport_cycle_from !== FALSE)
            $this->db->where('safa_uo_contracts_settings.transport_cycle_from', $this->transport_cycle_from);

        if ($this->transporters_us !== FALSE)
            $this->db->where('safa_uo_contracts_settings.transporters_us', $this->transporters_us);

        if ($this->can_use_other_uo !== FALSE)
            $this->db->where('safa_uo_contracts_settings.can_use_other_uo', $this->can_use_other_uo);

        if ($this->can_use_hotel_marking !== FALSE)
            $this->db->where('safa_uo_contracts_settings.can_use_hotel_marking', $this->can_use_hotel_marking);

        if ($this->can_book_couch !== FALSE)
            $this->db->where('safa_uo_contracts_settings.can_book_couch', $this->can_book_couch);

        if ($this->visa_price !== FALSE)
            $this->db->where('safa_uo_contracts_settings.visa_price', $this->visa_price);

        if ($this->visa_price_currency_id !== FALSE)
            $this->db->where('safa_uo_contracts_settings.visa_price_currency_id', $this->visa_price_currency_id);

        if ($this->max_limit_visa !== FALSE)
            $this->db->where('safa_uo_contracts_settings.max_limit_visa', $this->max_limit_visa);

        if ($this->credit_balance !== FALSE)
            $this->db->where('safa_uo_contracts_settings.credit_balance', $this->credit_balance);


        if ($this->credit_balance_currency_id !== FALSE)
            $this->db->where('safa_uo_contracts_settings.credit_balance_currency_id', $this->credit_balance_currency_id);

        if ($this->can_over_limit !== FALSE)
            $this->db->where('safa_uo_contracts_settings.can_over_limit', $this->can_over_limit);

        if ($this->add_passports_accomodation_without_visa !== FALSE)
            $this->db->where('safa_uo_contracts_settings.add_passports_accomodation_without_visa', $this->add_passports_accomodation_without_visa);
            

        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('safa_uo_contracts_settings');

        $query_text = $this->db->last_query();
        //echo $query_text; exit;

        if ($rows_no)
            return $query->num_rows();

        if ($this->safa_uo_contract_setting_id || $this->safa_uo_contract_id)
            return $query->row();
        else
            return $query->result();
    }

    function save() {

        if ($this->safa_uo_contract_id !== FALSE)
            $this->db->set('safa_uo_contracts_settings.safa_uo_contract_id', $this->safa_uo_contract_id);

        if ($this->services_buy_uo_visa !== FALSE)
            $this->db->set('safa_uo_contracts_settings.services_buy_uo_visa', $this->services_buy_uo_visa);

        if ($this->services_buy_uo_hotels !== FALSE)
            $this->db->set('safa_uo_contracts_settings.services_buy_uo_hotels', $this->services_buy_uo_hotels);

        if ($this->services_buy_uo_transporter !== FALSE)
            $this->db->set('safa_uo_contracts_settings.services_buy_uo_transporter', $this->services_buy_uo_transporter);

        if ($this->services_buy_other_hotels !== FALSE)
            $this->db->set('safa_uo_contracts_settings.services_buy_other_hotel', $this->services_buy_other_hotels);

        if ($this->services_buy_other_transporter !== FALSE)
            $this->db->set('safa_uo_contracts_settings.services_buy_other_transporter', $this->services_buy_other_transporter);

        if ($this->services_buy_other_services !== FALSE)
            $this->db->set('safa_uo_contracts_settings.services_buy_other_services', $this->services_buy_other_services);

        if ($this->transport_cycle_from !== FALSE)
            $this->db->set('safa_uo_contracts_settings.transport_cycle_from', $this->transport_cycle_from);

        if ($this->transporters_us !== FALSE)
            $this->db->set('safa_uo_contracts_settings.transporters_us', $this->transporters_us);

        if ($this->can_use_other_uo !== FALSE)
            $this->db->set('safa_uo_contracts_settings.can_use_other_uo', $this->can_use_other_uo);

        if ($this->can_use_hotel_marking !== FALSE)
            $this->db->set('safa_uo_contracts_settings.can_use_hotel_marking', $this->can_use_hotel_marking);

        if ($this->can_book_couch !== FALSE)
            $this->db->set('safa_uo_contracts_settings.can_book_couch', $this->can_book_couch);

        if ($this->visa_price !== FALSE)
            $this->db->set('safa_uo_contracts_settings.visa_price', $this->visa_price);

        if ($this->visa_price_currency_id !== FALSE)
            $this->db->set('safa_uo_contracts_settings.visa_price_currency_id', $this->visa_price_currency_id);

        if ($this->max_limit_visa !== FALSE)
            $this->db->set('safa_uo_contracts_settings.max_limit_visa', $this->max_limit_visa);

        if ($this->credit_balance !== FALSE)
            $this->db->set('safa_uo_contracts_settings.credit_balance', $this->credit_balance);

        if ($this->credit_balance_currency_id !== FALSE)
            $this->db->set('safa_uo_contracts_settings.credit_balance_currency_id', $this->credit_balance_currency_id);

        if ($this->can_over_limit !== FALSE)
            $this->db->set('safa_uo_contracts_settings.can_over_limit', $this->can_over_limit);

        if ($this->add_passports_accomodation_without_visa !== FALSE)
            $this->db->set('safa_uo_contracts_settings.add_passports_accomodation_without_visa', $this->add_passports_accomodation_without_visa);
        
        if ($this->safa_uo_contract_setting_id) {
            $this->db->where('safa_uo_contracts_settings.safa_uo_contract_setting_id', $this->safa_uo_contract_setting_id)->update('safa_uo_contracts_settings');
        } else {
            $this->db->insert('safa_uo_contracts_settings');
            return $this->db->insert_id();
        }
    }

    function delete() {
        if ($this->safa_uo_contract_id !== FALSE)
            $this->db->where('safa_uo_contracts_settings.safa_uo_contract_id', $this->safa_uo_contract_id);

        if ($this->safa_uo_contract_setting_id !== FALSE)
            $this->db->where('safa_uo_contracts_settings.safa_uo_contract_setting_id', $this->safa_uo_contract_setting_id);


        $this->db->delete('safa_uo_contracts_settings');
        return $this->db->affected_rows();
    }

}

/* End of file contracts_model.php */
/* Location: ./application/models/contracts_model.php */