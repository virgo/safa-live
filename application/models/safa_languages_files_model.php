<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Safa_languages_files_model extends CI_Model {

    public $safa_file_id = FALSE;
    public $file_name = FALSE;
    public $custom_select = FALSE;
    public $limit = FALSE;
    public $offset = FALSE;
    public $order_by = FALSE;

    function __construct() {
        parent::__construct();
    }

    function get($rows_no = FALSE) {

        if ($this->custom_select !== FALSE) {
            $this->db->select('safa_file_id');
            $this->db->select($this->custom_select);
        }

        if ($this->safa_file_id !== FALSE)
            $this->db->where('safa_languages_files.safa_file_id', $this->safa_file_id);

        if ($this->file_name !== FALSE)
            $this->db->where('safa_languages_files.file_name', $this->file_name);


        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('safa_languages_files');
        if ($rows_no)
            return $query->num_rows();

        if ($this->safa_file_id)
            return $query->row();
        else
            return $query->result();
    }

    function save() {
        if ($this->safa_file_id !== FALSE)
            $this->db->set('safa_languages_files.safa_file_id', $this->safa_file_id);

        if ($this->file_name !== FALSE)
            $this->db->set('safa_languages_files.file_name', $this->file_name);



        if ($this->safa_file_id) {
            $this->db->where('safa_languages_files.safa_file_id', $this->safa_file_id)->update('safa_languages_files');
        } else {
            $this->db->insert('safa_languages_files');
            return $this->db->insert_id();
        }
    }

    function delete() {
        if ($this->safa_file_id !== FALSE)
            $this->db->where('safa_languages_files.safa_file_id', $this->safa_file_id);

        if ($this->file_name !== FALSE)
            $this->db->where('safa_languages_files.file_name', $this->file_name);



        $this->db->delete('safa_languages_files');
        return $this->db->affected_rows();
    }

    function search($rows_no = FALSE) {
        if ($this->custom_select !== FALSE) {
            $this->db->select('safa_file_id');
            $this->db->select($this->custom_select);
        }

        if ($this->safa_file_id !== FALSE)
            $this->db->where('safa_languages_files.safa_file_id', $this->safa_file_id);

        if ($this->file_name !== FALSE)
            $this->db->where('safa_languages_files.file_name', $this->file_name);


        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('safa_languages_files');
        if ($rows_no)
            return $query->num_rows();

        return $query->result();
    }

}

/* End of file safa_languages_files_model.php */
/* Location: ./application/models/safa_languages_files_model.php */