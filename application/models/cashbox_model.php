<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Cashbox_model extends CI_Model {

    public $crm_cashbox_id = FALSE;
    public $erp_currency_id = FALSE;
    public $name = FALSE;
    public $custom_select = FALSE;
    public $limit = FALSE;
    public $offset = FALSE;
    public $order_by = FALSE;

    function __construct() {
        parent::__construct();
    }

    function get($rows_no = FALSE) {

        if ($this->custom_select !== FALSE) {
            $this->db->select('crm_cashbox_id');
            $this->db->select($this->custom_select);
        }

        if ($this->crm_cashbox_id !== FALSE)
            $this->db->where('crm_cashbox.crm_cashbox_id', $this->crm_cashbox_id);

        if ($this->erp_currency_id !== FALSE)
            $this->db->where('crm_cashbox.erp_currency_id', $this->erp_currency_id);

        if ($this->name !== FALSE)
            $this->db->where('crm_cashbox.name', $this->name);


        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('crm_cashbox');
        if ($rows_no)
            return $query->num_rows();

        if ($this->crm_cashbox_id)
            return $query->row();
        else
            return $query->result();
    }

    function save() {
        if ($this->crm_cashbox_id !== FALSE)
            $this->db->set('crm_cashbox.crm_cashbox_id', $this->crm_cashbox_id);

        if ($this->erp_currency_id !== FALSE)
            $this->db->set('crm_cashbox.erp_currency_id', $this->erp_currency_id);

        if ($this->name !== FALSE)
            $this->db->set('crm_cashbox.name', $this->name);



        if ($this->crm_cashbox_id) {
            $this->db->where('crm_cashbox.crm_cashbox_id', $this->crm_cashbox_id)->update('crm_cashbox');
        } else {
            $this->db->insert('crm_cashbox');
            return $this->db->insert_id();
        }
    }

    function delete() {
        if ($this->crm_cashbox_id !== FALSE)
            $this->db->where('crm_cashbox.crm_cashbox_id', $this->crm_cashbox_id);

        if ($this->erp_currency_id !== FALSE)
            $this->db->where('crm_cashbox.erp_currency_id', $this->erp_currency_id);

        if ($this->name !== FALSE)
            $this->db->where('crm_cashbox.name', $this->name);



        $this->db->delete('crm_cashbox');
        return $this->db->affected_rows();
    }

    function search($rows_no = FALSE) {
        if ($this->custom_select !== FALSE) {
            $this->db->select('crm_cashbox_id');
            $this->db->select($this->custom_select);
        }

        if ($this->crm_cashbox_id !== FALSE)
            $this->db->where('crm_cashbox.crm_cashbox_id', $this->crm_cashbox_id);

        if ($this->erp_currency_id !== FALSE)
            $this->db->where('crm_cashbox.erp_currency_id', $this->erp_currency_id);

        if ($this->name !== FALSE)
            $this->db->where('crm_cashbox.name', $this->name);


        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('crm_cashbox');
        if ($rows_no)
            return $query->num_rows();

        return $query->result();
    }

}

/* End of file cashbox_model.php */
/* Location: ./application/models/cashbox_model.php */