<?php

class Trip_hotel_model extends CI_Model {

    /**
     * store this safa_trip_hotel table name.
     *
     * @var string
     * @access public
     */
    public $table = 'safa_trip_hotels';

    /**
     * Constructor
     *
     * @access public
     */
    function Main_model() {
        parent::__construct();
    }

    function get_trip_hotels_by_trip_id($trip_id) {
        $this->db->select('*, (SELECT SUM(rooms_count) FROM safa_trip_hotel_rooms WHERE safa_trip_hotel_rooms.safa_trip_hotel_id = th.safa_trip_hotel_id) AS trip_hotel_rooms');
        $this->db->from('safa_trip_hotels th');
        $this->db->join('erp_hotels h', 'h.erp_hotel_id = th.erp_hotel_id');
        $this->db->where("safa_trip_id", $trip_id);
        $this->db->order_by("th.checkin_datetime", 'ASC');
        $query = $this->db->get();
        return $query->result();
    }

    function delete_trip_hotel_by_trip_id($trip_id) {
        $this->db->where("safa_trip_id", $trip_id);
        return $this->db->delete("safa_trip_hotels");
    }

    function insert($data) {
        $rooms = $data['trip_hotel_rooms'];
        unset($data['trip_hotel_rooms']);
        $this->db->insert('safa_trip_hotels', $data);
        $inserted_id = $this->db->insert_id();

        $this->db->insert('safa_trip_hotel_rooms', array(
            'safa_trip_hotel_id' => $inserted_id,
            'erp_hotelroomsize_id' => 1,
            'rooms_count' => (int) $rooms
        ));

        $safa_trip_hotel_room_id = $this->db->insert_id();
        return $safa_trip_hotel_room_id;
        //return $inserted_id;        
    }

    function update($trip_hotel_id, $data) {
        $data2['rooms_count'] = $data['trip_hotel_rooms'];
        unset($data['trip_hotel_rooms']);

        $this->db->where('safa_trip_hotel_id', $trip_hotel_id);
        $this->db->update('safa_trip_hotels', $data);

        $this->db->where('safa_trip_hotel_id', $trip_hotel_id);
        $this->db->update('safa_trip_hotel_rooms', $data2);
    }

    function delete_by_trip_and_id($trip_id, $trip_hotel_id) {
        $this->db->where("safa_trip_hotel_id", $trip_hotel_id);
        $this->db->delete("safa_trip_hotel_rooms");

        $this->db->where("safa_trip_id", $trip_id);
        $this->db->where("safa_trip_hotel_id", $trip_hotel_id);
        return $this->db->delete("safa_trip_hotels");
    }

}
