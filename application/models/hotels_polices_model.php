<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Hotels_polices_model extends CI_Model {

    public $erp_hotels_policies_id = FALSE;
    public $name_ar = FALSE;
    public $name_la = FALSE;
    public $custom_select = FALSE;
    public $limit = FALSE;
    public $offset = FALSE;
    public $order_by = FALSE;

    function __construct() {
        parent::__construct();
    }

    function get($rows_no = FALSE) {

        if ($this->custom_select !== FALSE) {
            $this->db->select('erp_hotels_policies_id');
            $this->db->select($this->custom_select);
        }

        if ($this->erp_hotels_policies_id !== FALSE)
            $this->db->where('erp_hotels_policies.erp_hotels_policies_id', $this->erp_hotels_policies_id);

        if ($this->name_ar !== FALSE)
            $this->db->where('erp_hotels_policies.name_ar', $this->name_ar);

        if ($this->name_la !== FALSE)
            $this->db->where('erp_hotels_policies.name_la', $this->name_la);



        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('erp_hotels_policies');
        if ($rows_no)
            return $query->num_rows();

        if ($this->erp_hotels_policies_id)
            return $query->row();
        else
            return $query->result();
    }

    function save() {

        if ($this->erp_hotels_policies_id !== FALSE)
            $this->db->set('erp_hotels_policies.erp_hotels_policies_id', $this->erp_hotels_policies_id);

        if ($this->name_ar !== FALSE)
            $this->db->set('erp_hotels_policies.name_ar', $this->name_ar);

        if ($this->name_la !== FALSE)
            $this->db->set('erp_hotels_policies.name_la', $this->name_la);


        if ($this->erp_hotels_policies_id) {
            $this->db->where('erp_hotels_policies.erp_hotels_policies_id', $this->erp_hotels_policies_id)->update('erp_hotels_policies');
        } else {
            $this->db->insert('erp_hotels_policies');
            return $this->db->insert_id();
        }
    }

    function delete() {

        if ($this->erp_hotels_policies_id !== FALSE)
            $this->db->where('erp_hotels_policies.erp_hotels_policies_id', $this->erp_hotels_policies_id);

        if ($this->name_ar !== FALSE)
            $this->db->where('erp_hotels_policies.name_ar', $this->name_ar);

        if ($this->name_la !== FALSE)
            $this->db->where('erp_hotels_policies.name_la', $this->name_la);


        $this->db->delete('erp_hotels_policies');
        return $this->db->affected_rows();
    }

    function search($rows_no = FALSE) {
        if ($this->custom_select !== FALSE) {
            $this->db->select('erp_hotels_policies_id');
            $this->db->select($this->custom_select);
        }

        if ($this->erp_hotels_policies_id !== FALSE)
            $this->db->where('erp_hotels_policies.erp_hotels_policies_id', $this->erp_hotels_policies_id);

        if ($this->name_ar !== FALSE)
            $this->db->where('erp_hotels_policies.name_ar', $this->name_ar);


        if ($this->name_la !== FALSE)
            $this->db->where('erp_hotels_policies.name_la', $this->name_la);

        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('erp_hotels_policies');
        if ($rows_no)
            return $query->num_rows();

        return $query->result();
    }

}

/* End of file hotels_polices_model.php */
/* Location: ./application/models/hotels_polices_model.php */