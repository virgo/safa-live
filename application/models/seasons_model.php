<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Seasons_model extends CI_Model {

    public $erp_season_id = FALSE;
    public $name_ar = FALSe;
    public $name_la = FALSE;
    public $start_date = FALSE;
    public $end_date = FALSE;
    public $custom_select = FALSE;
    public $limit = FALSE;
    public $offset = FALSE;
    public $order_by = FALSE;

    function __construct() {
        parent::__construct();
    }

    function get($rows_no = FALSE) {

        if ($this->custom_select !== FALSE) {
            $this->db->select('erp_season_id');
            $this->db->select($this->custom_select);
        }

        if ($this->erp_season_id !== FALSE)
            $this->db->where('erp_seasons.erp_season_id', $this->erp_season_id);

        if ($this->name_ar !== FALSE)
            $this->db->where('erp_seasons.name_ar', $this->name_ar);

        if ($this->name_la !== FALSE)
            $this->db->where('erp_seasons.name_la', $this->name_la);

        if ($this->start_date !== FALSE)
            $this->db->where('erp_seasons.start_date', $this->start_date);

        if ($this->end_date !== FALSE)
            $this->db->where('erp_seasons.end_date', $this->end_date);


        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('erp_seasons');
        if ($rows_no)
            return $query->num_rows();

        if ($this->erp_season_id)
            return $query->row();
        else
            return $query->result();
    }

    function save() {

        if ($this->erp_season_id !== FALSE)
            $this->db->set('erp_seasons.erp_season_id', $this->erp_season_id);

        if ($this->name_ar !== FALSE)
            $this->db->set('erp_seasons.name_ar', $this->name_ar);

        if ($this->name_la !== FALSE)
            $this->db->set('erp_seasons.name_la', $this->name_la);

        if ($this->start_date !== FALSE)
            $this->db->set('erp_seasons.start_date', $this->start_date);

        if ($this->end_date !== FALSE)
            $this->db->set('erp_seasons.end_date', $this->end_date);


        if ($this->erp_season_id) {
            $this->db->where('erp_seasons.erp_season_id', $this->erp_season_id)->update('erp_seasons');
        } else {
            $this->db->insert('erp_seasons');
            return $this->db->insert_id();
        }
    }

    function delete() {

        if ($this->erp_season_id !== FALSE)
            $this->db->where('erp_seasons.erp_season_id', $this->erp_season_id);

        if ($this->name_ar !== FALSE)
            $this->db->where('erp_seasons.name_ar', $this->name_ar);

        if ($this->name_la !== FALSE)
            $this->db->where('erp_seasons.name_la', $this->name_la);

        if ($this->start_date !== FALSE)
            $this->db->where('erp_seasons.start_date', $this->start_date);

        if ($this->end_date !== FALSE)
            $this->db->where('erp_seasons.end_date', $this->end_date);


        $this->db->delete('erp_seasons');
        return $this->db->affected_rows();
    }

    function search($rows_no = FALSE) {
        if ($this->custom_select !== FALSE) {
            $this->db->select('erp_season_id');
            $this->db->select($this->custom_select);
        }

        if ($this->erp_season_id !== FALSE)
            $this->db->where('erp_seasons.erp_season_id', $this->erp_season_id);

        if ($this->name_ar !== FALSE)
            $this->db->where('erp_seasons.name_ar', $this->name_ar);

        if ($this->name_la !== FALSE)
            $this->db->where('erp_seasons.name_la' . $this->name_la);

        if ($this->start_date !== FALSE)
            $this->db->where('erp_seasons.start_date', $this->start_date);

        if ($this->end_date !== FALSE)
            $this->db->where('erp_seasons.end_date', $this->end_date);


        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('erp_seasons');
        if ($rows_no)
            return $query->num_rows();

        return $query->result();
    }

}

/* End of file seasons_model.php */
/* Location: ./application/models/seasons_model.php */