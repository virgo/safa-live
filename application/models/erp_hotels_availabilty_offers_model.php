<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Erp_hotels_availabilty_offers_model extends CI_Model {

    public $tbl_name = "erp_hotels_availabilty_offers";
    public $erp_hotels_availabilty_offers_id = false;
    public $availabilty_offer_owner_type = false;
    public $availabilty_offer_owner_id = false;
    public $name_ar = false;
    public $name_la = false;
    public $nights_count = false;
    public $pricing_methodology = false;
    public $discount_percent = false;
    public $discount_fixed = false;
    public $rounding_value = false;
    public $custom_select = false;
    public $limit = false;
    public $offset = false;
    public $order_by = false;

    function __construct() {
        parent::__construct();
    }

    function save() {
        if ($this->erp_hotels_availabilty_offers_id !== false) {
            $this->db->set('erp_hotels_availabilty_offers.erp_hotels_availabilty_offers_id', $this->erp_hotels_availabilty_offers_id);
        }

        if ($this->availabilty_offer_owner_type !== false) {
            $this->db->set('erp_hotels_availabilty_offers.availabilty_offer_owner_type', $this->availabilty_offer_owner_type);
        }
        if ($this->availabilty_offer_owner_id !== false) {
            $this->db->set('erp_hotels_availabilty_offers.availabilty_offer_owner_id', $this->availabilty_offer_owner_id);
        }

        if ($this->name_ar !== false) {
            $this->db->set('erp_hotels_availabilty_offers.name_ar', $this->name_ar);
        }
        if ($this->name_la !== false) {
            $this->db->set('erp_hotels_availabilty_offers.name_la', $this->name_la);
        }
        if ($this->nights_count !== false) {
            $this->db->set('erp_hotels_availabilty_offers.nights_count', $this->nights_count);
        }

        if ($this->pricing_methodology !== false) {
            $this->db->set('erp_hotels_availabilty_offers.pricing_methodology', $this->pricing_methodology);
        }
        if ($this->discount_percent !== false) {
            $this->db->set('erp_hotels_availabilty_offers.discount_percent', $this->discount_percent);
        }
        if ($this->discount_fixed !== false) {
            $this->db->set('erp_hotels_availabilty_offers.discount_fixed', $this->discount_fixed);
        }
        if ($this->rounding_value !== false) {
            $this->db->set('erp_hotels_availabilty_offers.rounding_value', $this->rounding_value);
        }


        if ($this->erp_hotels_availabilty_offers_id) {
            $this->db->where('erp_hotels_availabilty_offers.erp_hotels_availabilty_offers_id', $this->erp_hotels_availabilty_offers_id)->update('erp_hotels_availabilty_offers');
        } else {
            $this->db->insert('erp_hotels_availabilty_offers');
            return $this->db->insert_id();
        }
    }

    function delete() {
        if ($this->erp_hotels_availabilty_offers_id !== false) {
            $this->db->where('erp_hotels_availabilty_offers.erp_hotels_availabilty_offers_id', $this->erp_hotels_availabilty_offers_id);
        }
        if ($this->availabilty_offer_owner_type !== false) {
            $this->db->where('erp_hotels_availabilty_offers.availabilty_offer_owner_type', $this->availabilty_offer_owner_type);
        }
        if ($this->availabilty_offer_owner_id !== false) {
            $this->db->where('erp_hotels_availabilty_offers.availabilty_offer_owner_id', $this->availabilty_offer_owner_id);
        }


        if ($this->name_ar !== false) {
            $this->db->where('erp_hotels_availabilty_offers.name_ar', $this->name_ar);
        }
        if ($this->name_la !== false) {
            $this->db->where('erp_hotels_availabilty_offers.name_la', $this->name_la);
        }
        if ($this->nights_count !== false) {
            $this->db->where('erp_hotels_availabilty_offers.nights_count', $this->nights_count);
        }

        if ($this->pricing_methodology !== false) {
            $this->db->where('erp_hotels_availabilty_offers.pricing_methodology', $this->pricing_methodology);
        }
        if ($this->discount_percent !== false) {
            $this->db->where('erp_hotels_availabilty_offers.discount_percent', $this->discount_percent);
        }
        if ($this->discount_fixed !== false) {
            $this->db->where('erp_hotels_availabilty_offers.discount_fixed', $this->discount_fixed);
        }
        if ($this->rounding_value !== false) {
            $this->db->where('erp_hotels_availabilty_offers.rounding_value', $this->rounding_value);
        }


        $this->db->delete('erp_hotels_availabilty_offers');
        return $this->db->affected_rows();
    }

    function get($rows_no = false) {
        if ($this->custom_select !== false) {
            $this->db->select('erp_hotels_availabilty_offers_id');
            $this->db->select($this->custom_select);
        }

        if ($this->erp_hotels_availabilty_offers_id !== false) {
            $this->db->where('erp_hotels_availabilty_offers.erp_hotels_availabilty_offers_id', $this->erp_hotels_availabilty_offers_id);
        }
        if ($this->availabilty_offer_owner_type !== false) {
            $this->db->where('erp_hotels_availabilty_offers.availabilty_offer_owner_type', $this->availabilty_offer_owner_type);
        }
        if ($this->availabilty_offer_owner_id !== false) {
            $this->db->where('erp_hotels_availabilty_offers.availabilty_offer_owner_id', $this->availabilty_offer_owner_id);
        }
        if ($this->name_ar !== false) {
            $this->db->where('erp_hotels_availabilty_offers.name_ar', $this->name_ar);
        }
        if ($this->name_la !== false) {
            $this->db->where('erp_hotels_availabilty_offers.name_la', $this->name_la);
        }
        if ($this->nights_count !== false) {
            $this->db->where('erp_hotels_availabilty_offers.nights_count', $this->nights_count);
        }
        if ($this->pricing_methodology !== false) {
            $this->db->where('erp_hotels_availabilty_offers.pricing_methodology', $this->pricing_methodology);
        }
        if ($this->discount_percent !== false) {
            $this->db->where('erp_hotels_availabilty_offers.discount_percent', $this->discount_percent);
        }
        if ($this->discount_fixed !== false) {
            $this->db->where('erp_hotels_availabilty_offers.discount_fixed', $this->discount_fixed);
        }
        if ($this->rounding_value !== false) {
            $this->db->where('erp_hotels_availabilty_offers.rounding_value', $this->rounding_value);
        }

        $this->db->from('erp_hotels_availabilty_offers');

        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get();
        if ($rows_no)
            return $query->num_rows();

        if ($this->erp_hotels_availabilty_offers_id)
            return $query->row();
        else
            return $query->result();
    }

    function getCompaniesByCompanyType($erp_company_type_id) {

        if ($erp_company_type_id == 1) {
            $this->db->select('erp_admins.erp_admin_id as id, erp_admins.username as name_ar, erp_admins.username as name_la');
            $this->db->from('erp_admins');
        } else if ($erp_company_type_id == 2) {
            $this->db->select('safa_uos.safa_uo_id as id, safa_uos.name_ar, safa_uos.name_la');
            $this->db->from('safa_uos');
            $this->db->where('safa_uos.disabled', 0);
            //$this->db->where('safa_uos.', '');
        } else if ($erp_company_type_id == 3) {
            $this->db->select('safa_eas.safa_ea_id as id, safa_eas.name_ar, safa_eas.name_la');
            $this->db->from('safa_eas');
            $this->db->where('safa_eas.disabled', 0);
        } else if ($erp_company_type_id == 4) {
            $this->db->select('safa_itos.safa_ito_id as id, safa_itos.name_ar, safa_itos.name_la');
            $this->db->from('safa_itos');
            $this->db->where('safa_itos.disabled', 0);
        } else if ($erp_company_type_id == 5) {
            $this->db->select('safa_hms.safa_hm_id as id, safa_hms.name_ar, safa_hms.name_la');
            $this->db->from('safa_hms');
        } else {
            $this->db->select('safa_eas.safa_ea_id as id, safa_eas.name_ar, safa_eas.name_la');
            $this->db->from('safa_eas');
        }


        $query = $this->db->get();

        //$query_text=$this->db->last_query();
        //echo $query_text; exit;

        return $query->result();
    }

    function getMaxId() {
        $this->db->select_max('erp_hotels_availabilty_offers_id');
        $query = $this->db->get('erp_hotels_availabilty_offers');
        $row = $query->row();
        if (isset($row)) {
            return $row->erp_hotels_availabilty_offers_id;
        } else {
            return 0;
        }
    }

}

/* End of file erp_hotels_availabilty_offers_model.php */
/* Location: ./application/models/erp_hotels_availabilty_offers_model.php */