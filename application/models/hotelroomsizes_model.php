<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Hotelroomsizes_model extends CI_Model {

    public $erp_hotelroomsize_id = FALSE;
    public $name_ar = FALSE;
    public $name_la = FALSE;
    public $beds_count = FALSE;
    public $custom_select = FALSE;
    public $limit = FALSE;
    public $offset = FALSE;
    public $order_by = FALSE;

    function __construct() {
        parent::__construct();
    }

    function get($rows_no = FALSE) {

        if ($this->custom_select !== FALSE) {
            $this->db->select('erp_hotelroomsize_id');
            $this->db->select($this->custom_select);
        }

        if ($this->erp_hotelroomsize_id !== FALSE)
            $this->db->where('erp_hotelroomsizes.erp_hotelroomsize_id', $this->erp_hotelroomsize_id);

        if ($this->name_ar !== FALSE)
            $this->db->where('erp_hotelroomsizes.name_ar', $this->name_ar);

        if ($this->name_la !== FALSE)
            $this->db->where('erp_hotelroomsizes.name_la', $this->name_la);

        if ($this->beds_count !== FALSE)
            $this->db->where('erp_hotelroomsizes.beds_count', $this->beds_count);


        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('erp_hotelroomsizes');
        if ($rows_no)
            return $query->num_rows();

        if ($this->erp_hotelroomsize_id)
            return $query->row();
        else
            return $query->result();
    }

    function save() {
        if ($this->erp_hotelroomsize_id !== FALSE)
            $this->db->set('erp_hotelroomsizes.erp_hotelroomsize_id', $this->erp_hotelroomsize_id);

        if ($this->name_ar !== FALSE)
            $this->db->set('erp_hotelroomsizes.name_ar', $this->name_ar);

        if ($this->name_la !== FALSE)
            $this->db->set('erp_hotelroomsizes.name_la', $this->name_la);

        if ($this->beds_count !== FALSE)
            $this->db->set('erp_hotelroomsizes.beds_count', $this->beds_count);



        if ($this->erp_hotelroomsize_id) {
            $this->db->where('erp_hotelroomsizes.erp_hotelroomsize_id', $this->erp_hotelroomsize_id)->update('erp_hotelroomsizes');
        } else {
            $this->db->insert('erp_hotelroomsizes');
            return $this->db->insert_id();
        }
    }

    function delete() {
        if ($this->erp_hotelroomsize_id !== FALSE)
            $this->db->where('erp_hotelroomsizes.erp_hotelroomsize_id', $this->erp_hotelroomsize_id);

        if ($this->name_ar !== FALSE)
            $this->db->where('erp_hotelroomsizes.name_ar', $this->name_ar);

        if ($this->name_la !== FALSE)
            $this->db->where('erp_hotelroomsizes.name_la', $this->name_la);

        if ($this->beds_count !== FALSE)
            $this->db->where('erp_hotelroomsizes.beds_count', $this->beds_count);



        $this->db->delete('erp_hotelroomsizes');
        return $this->db->affected_rows();
    }

    function search($rows_no = FALSE) {
        if ($this->custom_select !== FALSE) {
            $this->db->select('erp_hotelroomsize_id');
            $this->db->select($this->custom_select);
        }

        if ($this->erp_hotelroomsize_id !== FALSE)
            $this->db->where('erp_hotelroomsizes.erp_hotelroomsize_id', $this->erp_hotelroomsize_id);

        if ($this->name_ar !== FALSE)
            $this->db->where('erp_hotelroomsizes.name_ar', $this->name_ar);

        if ($this->name_la !== FALSE)
            $this->db->where('erp_hotelroomsizes.name_la', $this->name_la);

        if ($this->beds_count !== FALSE)
            $this->db->where('erp_hotelroomsizes.beds_count', $this->beds_count);


        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('erp_hotelroomsizes');
        if ($rows_no)
            return $query->num_rows();

        return $query->result();
    }

}

/* End of file hotelroomsizes_model.php */
/* Location: ./application/models/hotelroomsizes_model.php */