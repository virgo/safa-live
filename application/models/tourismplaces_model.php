<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Tourismplaces_model extends CI_Model {

    public $safa_tourismplace_id = FALSE;
    public $name_ar = FALSE;
    public $name_la = FALSE;
    public $erp_country_id = FALSE;
    public $erp_city_id = FALSE;
    public $custom_select = FALSE;
    public $limit = FALSE;
    public $offset = FALSE;
    public $order_by = FALSE;
    public $join = FALSE;

    function __construct() {
        parent::__construct();
    }

    function get($rows_no = FALSE) {

        if ($this->custom_select !== FALSE) {
            $this->db->select('safa_tourismplace_id');
            $this->db->select($this->custom_select);
        }

        if ($this->safa_tourismplace_id !== FALSE)
            $this->db->where('safa_tourismplaces.safa_tourismplace_id', $this->safa_tourismplace_id);

        if ($this->name_ar !== FALSE)
            $this->db->where('safa_tourismplaces.name_ar', $this->name_ar);

        if ($this->name_la !== FALSE)
            $this->db->where('safa_tourismplaces.name_la', $this->name_la);

        if ($this->erp_city_id !== FALSE)
            $this->db->where('safa_tourismplaces.erp_city_id', $this->erp_city_id);

        if ($this->erp_country_id !== FALSE)
            $this->db->where('erp_cities.erp_country_id', $this->erp_country_id);


        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        if ($this->join !== FALSE) {
            $this->db->select('safa_tourismplaces.*,erp_cities.erp_city_id as city_id,
            erp_cities.name_ar as city_name_ar,erp_cities.name_la as city_name_la');
            $this->db->join('erp_cities', 'safa_tourismplaces.erp_city_id = erp_cities.erp_city_id', 'left');
        }

        $query = $this->db->get('safa_tourismplaces');
        if ($rows_no)
            return $query->num_rows();

        if ($this->safa_tourismplace_id)
            return $query->row();
        else
            return $query->result();
    }

    function save() {
        if ($this->safa_tourismplace_id !== FALSE)
            $this->db->set('safa_tourismplaces.safa_tourismplace_id', $this->safa_tourismplace_id);

        if ($this->name_ar !== FALSE)
            $this->db->set('safa_tourismplaces.name_ar', $this->name_ar);

        if ($this->name_la !== FALSE)
            $this->db->set('safa_tourismplaces.name_la', $this->name_la);

        if ($this->erp_city_id !== FALSE)
            $this->db->set('safa_tourismplaces.erp_city_id', $this->erp_city_id);



        if ($this->safa_tourismplace_id) {
            $this->db->where('safa_tourismplaces.safa_tourismplace_id', $this->safa_tourismplace_id)->update('safa_tourismplaces');
        } else {
            $this->db->insert('safa_tourismplaces');
            return $this->db->insert_id();
        }
    }

    function delete() {
        if ($this->safa_tourismplace_id !== FALSE)
            $this->db->where('safa_tourismplaces.safa_tourismplace_id', $this->safa_tourismplace_id);

        if ($this->name_ar !== FALSE)
            $this->db->where('safa_tourismplaces.name_ar', $this->name_ar);

        if ($this->name_la !== FALSE)
            $this->db->where('safa_tourismplaces.name_la', $this->name_la);

        if ($this->erp_city_id !== FALSE)
            $this->db->where('safa_tourismplaces.erp_city_id', $this->erp_city_id);



        $this->db->delete('safa_tourismplaces');
        return $this->db->affected_rows();
    }

    function search($rows_no = FALSE) {
        if ($this->custom_select !== FALSE) {
            $this->db->select('safa_tourismplace_id');
            $this->db->select($this->custom_select);
        }

        if ($this->safa_tourismplace_id !== FALSE)
            $this->db->where('safa_tourismplaces.safa_tourismplace_id', $this->safa_tourismplace_id);

        if ($this->name_ar !== FALSE)
            $this->db->where('safa_tourismplaces.name_ar', $this->name_ar);

        if ($this->name_la !== FALSE)
            $this->db->where('safa_tourismplaces.name_la', $this->name_la);

        if ($this->erp_city_id !== FALSE)
            $this->db->where('safa_tourismplaces.erp_city_id', $this->erp_city_id);


        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('safa_tourismplaces');
        if ($rows_no)
            return $query->num_rows();

        return $query->result();
    }

    function get_by_city($city_id) {
        $this->db->select('*');
        $this->db->from('safa_tourismplaces');
        $this->db->where('erp_city_id', $city_id);
        $query = $this->db->get();
        return $query->result();
    }

    function get_cities($erp_country_id) {
        $this->db->select('erp_cities.erp_city_id,erp_cities.name_ar,erp_cities.name_la');
        $this->db->where('erp_cities.erp_country_id', $erp_country_id);
        return $this->db->get('erp_cities')->result();
    }

    function check_delete_ability($id) {
        $this->db->select('count(safa_uo_contracts_tourismplaces.safa_tourismplace_id)as uo_contracts,count(safa_uo_package_tourismplaces.safa_tourismplace_id) as uo_package,
                count(safa_ea_package_tourismplaces.safa_tourismplace_id) as ea_package,count(safa_trip_tourismplaces.safa_tourismplace_id) as trips,
                count(safa_internalsegments.safa_tourism_place_id) as internalsegments ');
        $this->db->from('safa_tourismplaces');
        $this->db->join('safa_uo_contracts_tourismplaces', 'safa_tourismplaces.safa_tourismplace_id = safa_uo_contracts_tourismplaces.safa_tourismplace_id', 'left');
        $this->db->join('safa_uo_package_tourismplaces', 'safa_tourismplaces.safa_tourismplace_id = safa_uo_package_tourismplaces.safa_tourismplace_id', 'left');
        $this->db->join('safa_ea_package_tourismplaces', 'safa_tourismplaces.safa_tourismplace_id = safa_ea_package_tourismplaces.safa_tourismplace_id', 'left');
        $this->db->join('safa_trip_tourismplaces', 'safa_tourismplaces.safa_tourismplace_id = safa_trip_tourismplaces.safa_tourismplace_id', 'left');
        $this->db->join('safa_internalsegments', 'safa_tourismplaces.safa_tourismplace_id = safa_internalsegments.safa_tourism_place_id', 'left');
        $this->db->group_by('safa_tourismplaces.safa_tourismplace_id');
        $this->db->where('safa_tourismplaces.safa_tourismplace_id', $id);
        $query = $this->db->get();
        $flag = 0;

//        print_r($query->result());
        foreach ($query->result() as $row => $table) {
            foreach ($table as $col => $value) {
                if ($value > 0) {
                    $flag = 1;
                    break;
                } else {
                    continue;
                }
            }
        }
        return $flag;
    }

    function get_countery_id($erp_city_id) {
        $this->db->select('erp_cities.erp_country_id');
        $this->db->where('erp_cities.erp_city_id', $erp_city_id);
        return $this->db->get('erp_cities')->row();
    }

    function get_city_id($safa_tourismplace_id) {
        $this->db->select('safa_tourismplaces.erp_city_id');
        $this->db->where('safa_tourismplaces.safa_tourismplace_id', $safa_tourismplace_id);
        return $this->db->get('safa_tourismplaces')->row();
    }

    function get_countery_ksa() {
        $this->db->select('erp_countries.erp_country_id');
        $this->db->where('erp_countries.erp_country_id', 966);
        return $this->db->get('erp_countries')->row();
    }

//    function get_allTourismplaces_inCountry($id){
//            $this->db->select('safa_tourismplaces.*,erp_cities.erp_city_id as city_id,
//            erp_cities.name_ar as city_name_ar,erp_cities.name_la as city_name_la,erp_cities.erp_country_id');
//            $this->db->join('erp_cities', 'safa_tourismplaces.erp_city_id = erp_cities.erp_city_id', 'left');
////            $this->db->group_by('erp_cities.erp_country_id');
//            $this->db->where('erp_cities.erp_country_id',$id);
//            $query = $this->db->get('safa_tourismplaces');
//            return $query->result();
//    }
}

/* End of file tourismplaces_model.php */
/* Location: ./application/models/tourismplaces_model.php */