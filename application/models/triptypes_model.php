<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Triptypes_model extends CI_Model {

    public $safa_triptype_id = FALSE;
    public $name_ar = FALSE;
    public $name_la = FALSE;
    public $code = FALSE;
    public $custom_select = FALSE;
    public $limit = FALSE;
    public $offset = FALSE;
    public $order_by = FALSE;

    function __construct() {
        parent::__construct();
    }

    function get($rows_no = FALSE) {

        if ($this->custom_select !== FALSE) {
            $this->db->select('safa_triptype_id');
            $this->db->select($this->custom_select);
        }

        if ($this->safa_triptype_id !== FALSE)
            $this->db->where('safa_triptypes.safa_triptype_id', $this->safa_triptype_id);

        if ($this->name_ar !== FALSE)
            $this->db->where('safa_triptypes.name_ar', $this->name_ar);

        if ($this->name_la !== FALSE)
            $this->db->where('safa_triptypes.name_la', $this->name_la);

        if ($this->code !== FALSE)
            $this->db->where('safa_triptypes.code', $this->code);


        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('safa_triptypes');
        if ($rows_no)
            return $query->num_rows();

        if ($this->safa_triptype_id)
            return $query->row();
        else
            return $query->result();
    }

    function save() {
        if ($this->safa_triptype_id !== FALSE)
            $this->db->set('safa_triptypes.safa_triptype_id', $this->safa_triptype_id);

        if ($this->name_ar !== FALSE)
            $this->db->set('safa_triptypes.name_ar', $this->name_ar);

        if ($this->name_la !== FALSE)
            $this->db->set('safa_triptypes.name_la', $this->name_la);

        if ($this->code !== FALSE)
            $this->db->set('safa_triptypes.code', $this->code);



        if ($this->safa_triptype_id) {
            $this->db->where('safa_triptypes.safa_triptype_id', $this->safa_triptype_id)->update('safa_triptypes');
        } else {
            $this->db->insert('safa_triptypes');
            return $this->db->insert_id();
        }
    }

    function delete() {
        if ($this->safa_triptype_id !== FALSE)
            $this->db->where('safa_triptypes.safa_triptype_id', $this->safa_triptype_id);

        if ($this->name_ar !== FALSE)
            $this->db->where('safa_triptypes.name_ar', $this->name_ar);

        if ($this->name_la !== FALSE)
            $this->db->where('safa_triptypes.name_la', $this->name_la);

        if ($this->code !== FALSE)
            $this->db->where('safa_triptypes.code', $this->code);



        $this->db->delete('safa_triptypes');
        return $this->db->affected_rows();
    }

    function search($rows_no = FALSE) {
        if ($this->custom_select !== FALSE) {
            $this->db->select('safa_triptype_id');
            $this->db->select($this->custom_select);
        }

        if ($this->safa_triptype_id !== FALSE)
            $this->db->where('safa_triptypes.safa_triptype_id', $this->safa_triptype_id);

        if ($this->name_ar !== FALSE)
            $this->db->where('safa_triptypes.name_ar', $this->name_ar);

        if ($this->name_la !== FALSE)
            $this->db->where('safa_triptypes.name_la', $this->name_la);

        if ($this->code !== FALSE)
            $this->db->where('safa_triptypes.code', $this->code);


        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('safa_triptypes');
        if ($rows_no)
            return $query->num_rows();

        return $query->result();
    }

}

/* End of file triptypes_model.php */
/* Location: ./application/models/triptypes_model.php */