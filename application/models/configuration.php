<?php

/**
 * Configuration Class
 * @author Muhammad El-Saeed <m.elsaeed@virgotel.com>
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Configuration extends CI_Model {

    function __construct() {
        parent::__construct();

        foreach($this->db->get('erp_settings')->result() as $item)
            $this->config->set_item($item->key,$item->value);
        $this->config->set_item('webmaster_email', 'safa@umrah-booking.com');
        $this->config->set_item('title', lang("global_ksa_agent_sysyem_title"));

//        if (isset($_COOKIE['style']) && strlen($_COOKIE['style']))
//            define('STYLE', $_COOKIE['style']);
        if((int) session('language')) {
            $language = $this->db->where('erp_language_id', session('language'))->get('erp_languages')->row();
            $lang  = $language->path;
        } else {
            $lang  = 'arabic';
        }
        
        $this->config->set_item('language', $lang);

        $this->config->set_item('per_page', '20');

        $this->config->set_item('start_moving', '07:00');
        $this->lang->load('global');
        //$this->config->load('safa');
        if ($this->input->get('debug') == 1)
            $this->output->enable_profiler(TRUE);

        



        //$this->check_session();
//        $this->load->library('license');
//        $this->license->check_license();
    }
}
