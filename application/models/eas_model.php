<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Eas_model extends CI_Model {

    public $safa_ea_id = FALSE;
    public $name_ar = FALSE;
    public $name_la = FALSE;
    public $erp_country_id = FALSE;
    public $logo = FALSE;
    public $phone = FALSE;
    public $mobile = FALSE;
    public $fax = FALSE;
    public $email = FALSE;
    public $custom_select = FALSE;
    public $limit = FALSE;
    public $offset = FALSE;
    public $order_by = FALSE;
    public $direct_contracts;
    public $disabled = FALSE;
    public $name = FALSE;
    public $indirect_contracts;
    public $join;

    function __construct() {
        parent::__construct();
    }

    function get($rows_no = FALSE) {

        if ($this->custom_select !== FALSE) {
            $this->db->select('safa_ea_id');
            $this->db->select($this->custom_select);
        }

        if ($this->name !== FALSE)
            $this->db->like('safa_eas.name_ar', $this->name)->like('safa_eas.name_la', $this->name);

        if ($this->safa_ea_id !== FALSE)
            $this->db->where('safa_eas.safa_ea_id', $this->safa_ea_id);

        if ($this->disabled !== FALSE)
            $this->db->where('safa_eas.disabled', $this->disabled);

        if ($this->name_ar !== FALSE)
            $this->db->where('safa_eas.name_ar', $this->name_ar);

        if ($this->name_la !== FALSE)
            $this->db->where('safa_eas.name_la', $this->name_la);

        if ($this->erp_country_id !== FALSE)
            $this->db->where('erp_countries.erp_country_id', $this->erp_country_id);

        if ($this->logo !== FALSE)
            $this->db->where('safa_eas.logo', $this->logo);

        if ($this->phone !== FALSE)
            $this->db->where('safa_eas.phone', $this->phone);

        if ($this->mobile !== FALSE)
            $this->db->where('safa_eas.mobile', $this->mobile);

        if ($this->fax !== FALSE)
            $this->db->where('safa_eas.fax', $this->fax);

        if ($this->email !== FALSE)
            $this->db->where('safa_eas.email', $this->email);

        if ($this->join)
            $this->db->select('erp_countries.name_ar as country_name,erp_countries.erp_country_id,safa_eas.name_ar as arabic_name,safa_eas.name_la as english_name,safa_eas.safa_ea_id ,
            safa_eas.logo,safa_eas.phone,safa_eas.mobile,safa_eas.fax,safa_eas.email,safa_eas.erp_country_id');
        $this->db->join('erp_countries', 'safa_eas.erp_country_id = erp_countries.erp_country_id');

        if ($this->direct_contracts)
            $this->db->select('safa_eas.*, (SELECT COUNT(*) FROM safa_uo_contracts_eas WHERE safa_uo_contracts_eas.safa_ea_id = safa_eas.safa_ea_id AND is_original_owner = 1) AS direct_contracts 
             ');
        if ($this->indirect_contracts)
            $this->db->select('safa_eas.*, (SELECT COUNT(*) FROM safa_uo_contracts_eas WHERE safa_uo_contracts_eas.safa_ea_id = safa_eas.safa_ea_id AND is_original_owner = 0) AS indirect_contracts 
             ');

        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

//           $this->db->select("ea1.* ,
//            (select count(safa_uo_contracts_eas.safa_uo_contract_ea_id) from safa_uo_contracts_eas where safa_uo_contracts_eas.safa_ea_id=ea1.safa_ea_id) as num_contracts,
//            (select count(safa_trips.safa_trip_id) from safa_trips 
//            join safa_ea_packages on safa_ea_packages.safa_ea_package_id=safa_trips.safa_ea_package_id
//            join safa_uo_packages on safa_uo_packages.safa_uo_package_id=safa_ea_packages.safa_uo_package_id
//            join safa_eas on safa_eas.safa_es_id=safa_ea_packages.safa_ea_id 
//            where safa_ea_packages.safa_ea_id=ea1.safa_ea_id) as trips ");   
//         $this->db->from("safa_eas as ea1");

        $query = $this->db->get('safa_eas');
        if ($rows_no)
            return $query->num_rows();

        if ($this->safa_ea_id)
            return $query->row();
        else
            return $query->result();
    }

    function save() {
        if ($this->safa_ea_id !== FALSE)
            $this->db->set('safa_eas.safa_ea_id', $this->safa_ea_id);

        if ($this->name_ar !== FALSE)
            $this->db->set('safa_eas.name_ar', $this->name_ar);

        if ($this->name_la !== FALSE)
            $this->db->set('safa_eas.name_la', $this->name_la);

        if ($this->erp_country_id !== FALSE)
            $this->db->set('safa_eas.erp_country_id', $this->erp_country_id);

        if ($this->logo !== FALSE)
            $this->db->set('safa_eas.logo', $this->logo);

        if ($this->phone !== FALSE)
            $this->db->set('safa_eas.phone', $this->phone);

        if ($this->disabled !== FALSE)
            $this->db->set('safa_eas.disabled', $this->disabled);

        if ($this->mobile !== FALSE)
            $this->db->set('safa_eas.mobile', $this->mobile);

        if ($this->fax !== FALSE)
            $this->db->set('safa_eas.fax', $this->fax);

        if ($this->email !== FALSE)
            $this->db->set('safa_eas.email', $this->email);

        if ($this->safa_ea_id) {
            return $this->db->where('safa_eas.safa_ea_id', $this->safa_ea_id)->update('safa_eas');
        } else {
            $this->db->insert('safa_eas');
            return $this->db->insert_id();
        }
    }

    function delete() {
        if ($this->safa_ea_id !== FALSE)
            $this->db->where('safa_eas.safa_ea_id', $this->safa_ea_id);

        if ($this->name_ar !== FALSE)
            $this->db->where('safa_eas.name_ar', $this->name_ar);

        if ($this->name_la !== FALSE)
            $this->db->where('safa_eas.name_la', $this->name_la);

        if ($this->erp_country_id !== FALSE)
            $this->db->where('safa_eas.erp_country_id', $this->erp_country_id);

        if ($this->logo !== FALSE)
            $this->db->where('safa_eas.logo', $this->logo);

        if ($this->phone !== FALSE)
            $this->db->where('safa_eas.phone', $this->phone);

        if ($this->mobile !== FALSE)
            $this->db->where('safa_eas.mobile', $this->mobile);

        if ($this->fax !== FALSE)
            $this->db->where('safa_eas.fax', $this->fax);
        if ($this->disabled !== FALSE)
            $this->db->where('safa_eas.disabled', $this->disabled);
        if ($this->email !== FALSE)
            $this->db->where('safa_eas.email', $this->email);



        $this->db->delete('safa_eas');
        return $this->db->affected_rows();
    }

    function search($rows_no = FALSE) {
        if ($this->custom_select !== FALSE) {
            $this->db->select('safa_ea_id');
            $this->db->select($this->custom_select);
        }

        if ($this->name !== FALSE)
            $this->db->like('safa_eas.name_ar', $this->name)->or_like('safa_eas.name_la', $this->name);

        if ($this->safa_ea_id !== FALSE)
            $this->db->where('safa_eas.safa_ea_id', $this->safa_ea_id);

        if ($this->name_ar !== FALSE)
            $this->db->where('safa_eas.name_ar', $this->name_ar);

        if ($this->name_la !== FALSE)
            $this->db->where('safa_eas.name_la', $this->name_la);

        if ($this->erp_country_id !== FALSE)
            $this->db->where('safa_eas.erp_country_id', $this->erp_country_id);

        if ($this->logo !== FALSE)
            $this->db->where('safa_eas.logo', $this->logo);

        if ($this->phone !== FALSE)
            $this->db->where('safa_eas.phone', $this->phone);

        if ($this->mobile !== FALSE)
            $this->db->where('safa_eas.mobile', $this->mobile);

        if ($this->fax !== FALSE)
            $this->db->where('safa_eas.fax', $this->fax);

        if ($this->email !== FALSE)
            $this->db->where('safa_eas.email', $this->email);

        if ($this->disabled !== FALSE)
            $this->db->where('safa_eas.disabled', $this->disabled);

        if ($this->join)
            $this->db->select('erp_countries.name_ar as country_name,erp_countries.erp_country_id,safa_eas.name_ar as arabic_name,safa_eas.name_la as english_name,safa_eas.safa_ea_id ,
            safa_eas.logo,safa_eas.phone,safa_eas.mobile,safa_eas.fax,safa_eas.email,safa_eas.erp_country_id');
        $this->db->join('erp_countries', 'safa_eas.erp_country_id = erp_countries.erp_country_id');


        if ($this->direct_contracts)
            $this->db->select('safa_eas.*, (SELECT COUNT(*) FROM safa_uo_contracts_eas WHERE safa_uo_contracts_eas.safa_ea_id = safa_eas.safa_ea_id AND is_original_owner = 1) AS direct_contracts 
             ');

        if ($this->indirect_contracts)
            $this->db->select('safa_eas.*, (SELECT COUNT(*) FROM safa_uo_contracts_eas WHERE safa_uo_contracts_eas.safa_ea_id = safa_eas.safa_ea_id AND is_original_owner = 0) AS indirect_contracts 
             ');



        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('safa_eas');
        if ($rows_no)
            return $query->num_rows();

        return $query->result();
    }

    function check_delete_ability($id) {

        $this->db->select('count(safa_ea_users.safa_ea_id)as ea_users,count(safa_ea_branches.safa_ea_id) as ea_branches,
                count(safa_ea_seasons.safa_ea_id) as seasons,count(safa_uo_contracts_eas.safa_ea_id) as contracts');
        $this->db->from('safa_eas');
        $this->db->join('safa_ea_users', 'safa_eas.safa_ea_id = safa_ea_users.safa_ea_id', 'left');
        $this->db->join('safa_ea_branches', 'safa_eas.safa_ea_id = safa_ea_branches.safa_ea_id', 'left');
        $this->db->join('safa_ea_seasons', 'safa_eas.safa_ea_id = safa_ea_seasons.safa_ea_id', 'left');
        $this->db->join('safa_uo_contracts_eas', 'safa_eas.safa_ea_id = safa_uo_contracts_eas.safa_ea_id', 'left');
        $this->db->group_by('safa_eas.safa_ea_id');
        $this->db->where('safa_eas.safa_ea_id', $id);
        $query = $this->db->get();
        $flag = 0;

//        print_r($query->result());
        foreach ($query->result() as $row => $table) {
            foreach ($table as $col => $value) {
                if ($value > 0) {
                    $flag = 1;
                    break;
                } else {
                    continue;
                }
            }
        }
        return $flag;
    }

    function check_users_delete_ability($id) {
        $this->db->select('safa_ea_users.safa_ea_user_id,safa_ea_users.safa_ea_id,safa_ea_users.safa_ea_usergroup_id ');
        $this->db->from('safa_ea_users');
        $this->db->where('safa_ea_users.safa_ea_id', $id);
        $this->db->where('safa_ea_users.safa_ea_usergroup_id', 1);


        $query = $this->db->get();

        if ($this->safa_ea_id)
            return $query->row();
        else
            return $query->result();
    }

}

/* End of file eas_model.php */
/* Location: ./application/models/eas_model.php */