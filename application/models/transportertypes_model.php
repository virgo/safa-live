<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Transportertypes_model extends CI_Model {

    public $erp_transportertype_id = FALSE;
    public $name_ar = FALSE;
    public $name_la = FALSE;
    public $code = FALSE;
    public $custom_select = FALSE;
    public $limit = FALSE;
    public $offset = FALSE;
    public $order_by = FALSE;

    function __construct() {
        parent::__construct();
    }

    function get($rows_no = FALSE) {

        if ($this->custom_select !== FALSE) {
            $this->db->select('erp_transportertype_id');
            $this->db->select($this->custom_select);
        }

        if ($this->erp_transportertype_id !== FALSE)
            $this->db->where('erp_transportertypes.erp_transportertype_id', $this->erp_transportertype_id);

        if ($this->name_ar !== FALSE)
            $this->db->where('erp_transportertypes.name_ar', $this->name_ar);

        if ($this->name_la !== FALSE)
            $this->db->where('erp_transportertypes.name_la', $this->name_la);

        if ($this->code !== FALSE)
            $this->db->where('erp_transportertypes.code', $this->code);


        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('erp_transportertypes');
        if ($rows_no)
            return $query->num_rows();

        if ($this->erp_transportertype_id)
            return $query->row();
        else
            return $query->result();
    }

    function save() {
        if ($this->erp_transportertype_id !== FALSE)
            $this->db->set('erp_transportertypes.erp_transportertype_id', $this->erp_transportertype_id);

        if ($this->name_ar !== FALSE)
            $this->db->set('erp_transportertypes.name_ar', $this->name_ar);

        if ($this->name_la !== FALSE)
            $this->db->set('erp_transportertypes.name_la', $this->name_la);

        if ($this->code !== FALSE)
            $this->db->set('erp_transportertypes.code', $this->code);



        if ($this->erp_transportertype_id) {
            $this->db->where('erp_transportertypes.erp_transportertype_id', $this->erp_transportertype_id)->update('erp_transportertypes');
        } else {
            $this->db->insert('erp_transportertypes');
            return $this->db->insert_id();
        }
    }

    function delete() {
        if ($this->erp_transportertype_id !== FALSE)
            $this->db->where('erp_transportertypes.erp_transportertype_id', $this->erp_transportertype_id);

        if ($this->name_ar !== FALSE)
            $this->db->where('erp_transportertypes.name_ar', $this->name_ar);

        if ($this->name_la !== FALSE)
            $this->db->where('erp_transportertypes.name_la', $this->name_la);

        if ($this->code !== FALSE)
            $this->db->where('erp_transportertypes.code', $this->code);



        $this->db->delete('erp_transportertypes');
        return $this->db->affected_rows();
    }

    function search($rows_no = FALSE) {
        if ($this->custom_select !== FALSE) {
            $this->db->select('erp_transportertype_id');
            $this->db->select($this->custom_select);
        }

        if ($this->erp_transportertype_id !== FALSE)
            $this->db->where('erp_transportertypes.erp_transportertype_id', $this->erp_transportertype_id);

        if ($this->name_ar !== FALSE)
            $this->db->where('erp_transportertypes.name_ar', $this->name_ar);

        if ($this->name_la !== FALSE)
            $this->db->where('erp_transportertypes.name_la', $this->name_la);

        if ($this->code !== FALSE)
            $this->db->where('erp_transportertypes.code', $this->code);


        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('erp_transportertypes');
        if ($rows_no)
            return $query->num_rows();

        return $query->result();
    }

}

/* End of file transportertypes_model.php */
/* Location: ./application/models/transportertypes_model.php */