<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Hotels_availability_room_details_model extends CI_Model {

    public $erp_hotels_availability_room_detail_id = FALSE;
    public $erp_hotels_availability_sub_room_id = FALSE;
    public $erp_hotels_availability_peroid_id = FALSE;
    public $number = FALSE;
    public $from_date = FALSE;
    public $to_date = FALSE;
    public $status = FALSE;
    public $status_not_equal = FALSE;
    
    public $safa_trip_hotel_room_id = FALSE;
    
    //By Gouda.
    public $erp_hotels_availability_master_id = FALSE;
    public $erp_hotelroomsize_id = FALSE;
    
    public $custom_select = FALSE;
    public $limit = FALSE;
    public $offset = FALSE;
    public $order_by = FALSE;

    function __construct() {
        parent::__construct();
    }

    function get($rows_no = FALSE) {

        if ($this->custom_select !== FALSE) {
            $this->db->select('erp_hotels_availability_room_detail_id');
            $this->db->select($this->custom_select);
        }

        if ($this->erp_hotels_availability_room_detail_id !== FALSE)
            $this->db->where('erp_hotels_availability_room_details.erp_hotels_availability_room_detail_id', $this->erp_hotels_availability_room_detail_id);

        if ($this->erp_hotels_availability_sub_room_id !== FALSE)
            $this->db->where('erp_hotels_availability_room_details.erp_hotels_availability_sub_room_id', $this->erp_hotels_availability_sub_room_id);

        if ($this->erp_hotels_availability_peroid_id !== FALSE)
            $this->db->where('erp_hotels_availability_room_details.erp_hotels_availability_peroid_id', $this->erp_hotels_availability_peroid_id);
          
        if ($this->number !== FALSE)
            $this->db->where('erp_hotels_availability_room_details.number', $this->number);

        if ($this->from_date !== FALSE)
            $this->db->where('erp_hotels_availability_room_details.from_date', $this->from_date);

        if ($this->to_date !== FALSE)
            $this->db->where('erp_hotels_availability_room_details.to_date', $this->to_date);

        if ($this->status !== FALSE)
            $this->db->where('erp_hotels_availability_room_details.status', $this->status);

        if ($this->safa_trip_hotel_room_id !== FALSE)
            $this->db->where('erp_hotels_availability_room_details.safa_trip_hotel_room_id', $this->safa_trip_hotel_room_id);



        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('erp_hotels_availability_room_details');
        if ($rows_no)
            return $query->num_rows();

        if ($this->erp_hotels_availability_room_detail_id)
            return $query->row();
        else
            return $query->result();
    }
    
	function get_joined($rows_no = FALSE) 
	{

        if ($this->custom_select !== FALSE) {
            $this->db->select('erp_hotels_availability_room_detail_id');
            $this->db->select($this->custom_select);
        }

        if ($this->erp_hotels_availability_room_detail_id !== FALSE)
            $this->db->where('erp_hotels_availability_room_details.erp_hotels_availability_room_detail_id', $this->erp_hotels_availability_room_detail_id);

        if ($this->erp_hotels_availability_sub_room_id !== FALSE)
            $this->db->where('erp_hotels_availability_room_details.erp_hotels_availability_sub_room_id', $this->erp_hotels_availability_sub_room_id);

        if ($this->erp_hotels_availability_peroid_id !== FALSE)
            $this->db->where('erp_hotels_availability_room_details.erp_hotels_availability_peroid_id', $this->erp_hotels_availability_peroid_id);
            
        if ($this->number !== FALSE)
            $this->db->where('erp_hotels_availability_room_details.number', $this->number);

        if ($this->from_date !== FALSE)
            $this->db->where('erp_hotels_availability_room_details.from_date', $this->from_date);

        if ($this->to_date !== FALSE)
            $this->db->where('erp_hotels_availability_room_details.to_date', $this->to_date);

        if ($this->status !== FALSE)
            $this->db->where('erp_hotels_availability_room_details.status', $this->status);

        if ($this->status_not_equal !== FALSE)
            $this->db->where('erp_hotels_availability_room_details.status!=', $this->status_not_equal);
            
        if ($this->safa_trip_hotel_room_id !== FALSE)
            $this->db->where('erp_hotels_availability_room_details.safa_trip_hotel_room_id', $this->safa_trip_hotel_room_id);

        if ($this->erp_hotels_availability_master_id !== FALSE)
            $this->db->where('erp_hotels_availability_master.erp_hotels_availability_master_id', $this->erp_hotels_availability_master_id);
        
        if ($this->erp_hotelroomsize_id !== FALSE)
            $this->db->where('erp_hotels_availability_rooms.erp_hotelroomsize_id', $this->erp_hotelroomsize_id);

        $this->db->from('erp_hotels_availability_room_details');
        $this->db->join('erp_hotels_availability_sub_rooms', 'erp_hotels_availability_sub_rooms.erp_hotels_availability_sub_room_id = erp_hotels_availability_room_details.erp_hotels_availability_sub_room_id');
        $this->db->join('erp_hotels_availability_rooms', 'erp_hotels_availability_rooms.erp_hotels_availability_rooms_id = erp_hotels_availability_sub_rooms.erp_hotels_availability_rooms_id');
        $this->db->join('erp_hotels_availability_master', 'erp_hotels_availability_master.erp_hotels_availability_master_id = erp_hotels_availability_rooms.erp_hotels_availability_master_id');
           

        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get();
        if ($rows_no)
            return $query->num_rows();

        if ($this->erp_hotels_availability_room_detail_id)
            return $query->row();
        else
            return $query->result();
    }
	
    function save() 
    {
        if ($this->erp_hotels_availability_room_detail_id !== FALSE)
            $this->db->set('erp_hotels_availability_room_details.erp_hotels_availability_room_detail_id', $this->erp_hotels_availability_room_detail_id);

        if ($this->erp_hotels_availability_sub_room_id !== FALSE)
            $this->db->set('erp_hotels_availability_room_details.erp_hotels_availability_sub_room_id', $this->erp_hotels_availability_sub_room_id);
        
        if ($this->number !== FALSE)
            $this->db->set('erp_hotels_availability_room_details.number', $this->number);
        if ($this->from_date !== FALSE)
            $this->db->set('erp_hotels_availability_room_details.from_date', $this->from_date);
        if ($this->to_date !== FALSE)
            $this->db->set('erp_hotels_availability_room_details.to_date', $this->to_date);
        if ($this->status !== FALSE)
            $this->db->set('erp_hotels_availability_room_details.status', $this->status);
        if ($this->safa_trip_hotel_room_id !== FALSE)
            $this->db->set('erp_hotels_availability_room_details.safa_trip_hotel_room_id', $this->safa_trip_hotel_room_id);


        if ($this->erp_hotels_availability_room_detail_id) {
            $this->db->where('erp_hotels_availability_room_details.erp_hotels_availability_room_detail_id', $this->erp_hotels_availability_room_detail_id)->update('erp_hotels_availability_room_details');
        } else {
            $this->db->insert('erp_hotels_availability_room_details');
            return $this->db->insert_id();
        }
    }

    function delete() {
        if ($this->erp_hotels_availability_room_detail_id !== FALSE)
            $this->db->where('erp_hotels_availability_room_details.erp_hotels_availability_room_detail_id', $this->erp_hotels_availability_room_detail_id);

       if ($this->safa_trip_hotel_room_id !== FALSE)
            $this->db->where('erp_hotels_availability_room_details.safa_trip_hotel_room_id', $this->safa_trip_hotel_room_id);

       if ($this->erp_hotels_availability_sub_room_id !== FALSE)
            $this->db->where('erp_hotels_availability_room_details.erp_hotels_availability_sub_room_id', $this->erp_hotels_availability_sub_room_id);
        
        if ($this->erp_hotels_availability_peroid_id !== FALSE)
            $this->db->where('erp_hotels_availability_room_details.erp_hotels_availability_peroid_id', $this->erp_hotels_availability_peroid_id);
            
       if ($this->status !== FALSE)
            $this->db->where('erp_hotels_availability_room_details.status', $this->status);
        
       $this->db->limit($this->limit);     
            
       $this->db->delete('erp_hotels_availability_room_details');
       
       //echo $this->db->last_query(); exit;
       
       return $this->db->affected_rows();
    }

    function get_reserved_by_erp_hotels_availability_rooms_id($erp_hotels_availability_rooms_id) {
        $this->db->select('erp_hotels_availability_room_details.*');
        $this->db->from('erp_hotels_availability_room_details');
        $this->db->join('erp_hotels_availability_sub_rooms', 'erp_hotels_availability_sub_rooms.erp_hotels_availability_sub_room_id = erp_hotels_availability_room_details.erp_hotels_availability_sub_room_id');
        $this->db->where('erp_hotels_availability_sub_rooms.erp_hotels_availability_rooms_id', $erp_hotels_availability_rooms_id);
        $this->db->where('erp_hotels_availability_room_details.status', 2);

        $query = $this->db->get();
        //echo $this->db->last_query(); exit;

        return $query->result();
    }

}

/* End of file hotels_availability_rooms_model.php */
/* Location: ./application/models/hotels_availability_rooms_model.php */