<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Trip_internaltrip_model extends CI_Model {

    public $safa_trip_internaltrip_id = FALSE;
    public $safa_trip_id = FALSE;
    public $safa_ito_id = FALSE;
    public $safa_uo_id = FALSE;
    public $safa_ea_id = FALSE;
    public $owner_erp_company_type_id = FALSE;
    
    public $safa_transporter_id = FALSE;
    public $safa_tripstatus_id = FALSE;
    public $safa_internaltripstatus_id = FALSE;
    public $operator_reference = FALSE;
    public $attachement = FALSE;
    public $datetime = FALSE;
    public $ito_notes = FALSE;
    public $ea_notes = FALSE;
    public $safa_uo_contract_id = FALSE;
    public $custom_select = FALSE;
    public $limit = FALSE;
    public $offset = FALSE;
    public $order_by = FALSE;
    public $ea_id = FALSE;
    public $uo_id = FALSE;
    public $ito_id = FALSE;
    public $confirmation_number = FALSE;
    public $trip_title = FALSE;
    public $trip_supervisors = FALSE;
    public $trip_supervisors_phone = FALSE;
    public $adult_seats = FALSE;
    public $child_seats = FALSE;
    public $baby_seats = FALSE;
    /* search utilities */
    public $by_contract = FALSE;
    public $erp_company_type_id = FALSE;
    public $erp_company_id = FALSE;
    public $erp_company_name = FALSE;

    public $serial = FALSE;
    public $buses_count = FALSE;
    
    public $notes = FALSE;
    
    public $erp_transportertype_id = FALSE;
    public $safa_internaltrip_type_id = FALSE;
    
    public $erp_flight_availability_id = FALSE;
    public $erp_cruise_availability_id = FALSE;
    
    public $arrival_date_from = FALSE;
    public $arrival_date_to = FALSE;
    public $leaving_date_from = FALSE;
    public $leaving_date_to = FALSE;
    
    public $uo_email_sending_datetime = FALSE;
    
    public $where_condition = FALSE;
    
    public $deleted = FALSE;
    public $delete_reason = FALSE;
    
    function __construct() {
        parent::__construct();
    }

    function get($rows_no = FALSE) 
    {
    	$this->db->distinct();
    	
        if ($this->custom_select !== FALSE) {
            $this->db->select($this->custom_select);
        }

        if ($this->safa_trip_internaltrip_id !== FALSE)
            $this->db->where('safa_trip_internaltrips.safa_trip_internaltrip_id', $this->safa_trip_internaltrip_id);

        if ($this->safa_trip_id !== FALSE)
            $this->db->where('safa_trip_internaltrips.safa_trip_id', $this->safa_trip_id);

        if ($this->safa_ito_id !== FALSE)
            $this->db->where('safa_trip_internaltrips.safa_ito_id', $this->safa_ito_id);

        if ($this->owner_erp_company_type_id !== FALSE)
            $this->db->where('safa_trip_internaltrips.owner_erp_company_type_id', $this->owner_erp_company_type_id);
            
        if ($this->safa_transporter_id !== FALSE)
            $this->db->where('safa_trip_internaltrips.safa_transporter_id', $this->safa_transporter_id);

        if ($this->safa_tripstatus_id !== FALSE)
            $this->db->where('safa_trip_internaltrips.safa_internaltripstatus_id', $this->safa_tripstatus_id);

        if ($this->safa_internaltripstatus_id !== FALSE)
            $this->db->where('safa_trip_internaltrips.safa_internaltripstatus_id', $this->safa_internaltripstatus_id);

        if ($this->operator_reference !== FALSE)
            $this->db->where('safa_trip_internaltrips.operator_reference', $this->operator_reference);

        if ($this->attachement !== FALSE)
            $this->db->where('safa_trip_internaltrips.attachement', $this->attachement);

        if ($this->datetime !== FALSE)
            $this->db->where('safa_trip_internaltrips.datetime', $this->datetime);
        if ($this->ito_notes !== FALSE)
            $this->db->where('safa_trip_internaltrips.ito_notes', $this->ito_notes);
        if ($this->ea_notes !== FALSE)
            $this->db->where('safa_trip_internaltrips.ea_notes', $this->ea_notes);

        if ($this->confirmation_number !== FALSE)
            $this->db->where('safa_trip_internaltrips.confirmation_number', $this->confirmation_number);
        /* search utility */
        if ($this->by_contract !== FALSE)
            $this->db->where('safa_uo_contracts.safa_uo_contract_id', $this->by_contract);
        /* search utility */


        if ($this->erp_company_type_id !== FALSE)
            $this->db->where('safa_trip_internaltrips.erp_company_type_id', $this->erp_company_type_id);

        if ($this->erp_company_id !== FALSE)
            $this->db->where('safa_trip_internaltrips.erp_company_id', $this->erp_company_id);

        if ($this->erp_company_name !== FALSE)
            $this->db->where('safa_trip_internaltrips.erp_company_name', $this->erp_company_name);
        
        
        if ($this->safa_uo_contract_id !== FALSE)
            $this->db->where('safa_trip_internaltrips.safa_uo_contract_id', $this->safa_uo_contract_id);
        
        if ($this->trip_title !== FALSE)
            $this->db->where('safa_trip_internaltrips.trip_title', $this->trip_title);

        if ($this->trip_supervisors !== FALSE)
            $this->db->where('safa_trip_internaltrips.trip_supervisors', $this->trip_supervisors);

        if ($this->trip_supervisors_phone !== FALSE)
            $this->db->where('safa_trip_internaltrips.trip_supervisors_phone', $this->trip_supervisors_phone);

        if ($this->notes !== FALSE)
            $this->db->where('safa_trip_internaltrips.notes', $this->notes);
            
        if ($this->adult_seats !== FALSE)
            $this->db->where('safa_trip_internaltrips.adult_seats', $this->adult_seats);

        if ($this->child_seats !== FALSE)
            $this->db->where('safa_trip_internaltrips.child_seats', $this->child_seats);

        if ($this->baby_seats !== FALSE)
            $this->db->where('safa_trip_internaltrips.baby_seats', $this->baby_seats);

            
		if ($this->serial !== FALSE)
            $this->db->where('safa_trip_internaltrips.serial', $this->serial);
            
            
        if ($this->arrival_date_from || $this->arrival_date_to || $this->leaving_date_from || $this->leaving_date_to){
        	$this->db->join('safa_internalsegments', 'safa_internalsegments.safa_trip_internaltrip_id=safa_trip_internaltrips.safa_trip_internaltrip_id', 'left');
        }    
        if ($this->arrival_date_from){
        	//$this->db->join('safa_internalsegments', 'safa_internalsegments.safa_trip_internaltrip_id=safa_trip_internaltrips.safa_trip_internaltrip_id', 'left');
        	$this->db->where("(safa_internalsegments.start_datetime >= '$this->arrival_date_from' and safa_internalsegments.safa_internalsegmenttype_id=1)");
        }
		if ($this->arrival_date_to) {
			//$this->db->join('safa_internalsegments', 'safa_internalsegments.safa_trip_internaltrip_id=safa_trip_internaltrips.safa_trip_internaltrip_id', 'left');
			$this->db->where("(safa_internalsegments.start_datetime <= '$this->arrival_date_to' and safa_internalsegments.safa_internalsegmenttype_id=1)");
		}
		if ($this->leaving_date_from) {
			//$this->db->join('safa_internalsegments', 'safa_internalsegments.safa_trip_internaltrip_id=safa_trip_internaltrips.safa_trip_internaltrip_id', 'left');
			$this->db->where("(safa_internalsegments.start_datetime >= '$this->leaving_date_from' and safa_internalsegments.safa_internalsegmenttype_id=2)");
		}
		if ($this->leaving_date_to) {
			//$this->db->join('safa_internalsegments', 'safa_internalsegments.safa_trip_internaltrip_id=safa_trip_internaltrips.safa_trip_internaltrip_id', 'left');
			$this->db->where("(safa_internalsegments.start_datetime <= '$this->leaving_date_to' and safa_internalsegments.safa_internalsegmenttype_id=2)");
		}
        
        $this->db->where("(safa_trip_internaltrips.deleted IS NULL OR safa_trip_internaltrips.deleted=0)");

        if($this->where_condition) {
        	$this->db->where($this->where_condition);
        }
            
        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);
        if ($this->ea_id !== FALSE) {
            $this->db->select('safa_trip_internaltrips.*,safa_trips.name as trip_name, safa_itos.' . name() . ' as ito_name ,safa_internaltripstatus.' . name() . ' as status_name,
            				   safa_uo_contracts_eas.' . name() . ' as contract_name ,  safa_uo_contracts.' . name() . ' as safa_uo_contract_name,
                              (select count(*) from safa_internalsegments where 
                               safa_internalsegments.safa_trip_internaltrip_id=safa_trip_internaltrips.safa_trip_internaltrip_id) as  num_segments,
                               safa_transporters.' . name() . ' as transportername, safa_uos.safa_uo_id, safa_uos.active as safa_uo_active
                               , erp_transportertypes.' . name() . ' as erp_transportertype_name
                               ');
            $this->db->join("safa_trips", "safa_trip_internaltrips.safa_trip_id = safa_trips.safa_trip_id", 'left');
            
            
            $this->db->join('safa_transporters', 'safa_trip_internaltrips.safa_transporter_id = safa_transporters.safa_transporter_id', 'left');
            $this->db->join('safa_internaltripstatus', 'safa_internaltripstatus.safa_internaltripstatus_id = safa_trip_internaltrips.safa_internaltripstatus_id', 'left');
            $this->db->join('safa_itos', 'safa_itos.safa_ito_id = safa_trip_internaltrips.safa_ito_id', 'left');
            $this->db->join('safa_uo_contracts', 'safa_uo_contracts.safa_uo_contract_id = safa_trip_internaltrips.safa_uo_contract_id', 'left');
            $this->db->join('safa_uo_contracts_eas', 'safa_uo_contracts_eas.safa_uo_contract_id = safa_uo_contracts.safa_uo_contract_id', 'left');
            $this->db->join('safa_uos', 'safa_uo_contracts.safa_uo_id = safa_uos.safa_uo_id', 'left');
            
            $this->db->where('(safa_trip_internaltrips.safa_ea_id='.$this->ea_id.' or safa_uo_contracts_eas.safa_ea_id='. $this->ea_id .')');
            
        } elseif ($this->uo_id !== FALSE) {
            $this->db->select('safa_trip_internaltrips.*,safa_trips.name as trip_name,safa_itos.' . name() . ' as ito_name,safa_internaltripstatus.' . name() . ' as status_name, safa_uo_contracts.' . name() . ' as contract_name ,(select count(*) from safa_internalsegments where 
                               safa_internalsegments.safa_trip_internaltrip_id=safa_trip_internaltrips.safa_trip_internaltrip_id) as  num_segments,
                               safa_transporters.' . name() . ' as transportername
                               , erp_transportertypes.' . name() . ' as erp_transportertype_name
                               ');
            $this->db->join("safa_trips", "safa_trip_internaltrips.safa_trip_id = safa_trips.safa_trip_id", 'left');
            
            //$this->db->join('safa_uo_contracts_eas', 'safa_uo_contracts_eas.safa_ea_id = safa_trip_internaltrips.erp_company_id', 'left');
            //$this->db->join('safa_uo_contracts', 'safa_uo_contracts_eas.safa_uo_contract_id = safa_uo_contracts.safa_uo_contract_id', 'left');
            $this->db->join('safa_uo_contracts', 'safa_uo_contracts.safa_uo_contract_id = safa_trip_internaltrips.safa_uo_contract_id', 'left');
            
            $this->db->join('safa_transporters', 'safa_trip_internaltrips.safa_transporter_id= safa_transporters.safa_transporter_id', 'left');
            $this->db->join('safa_internaltripstatus', 'safa_internaltripstatus.safa_internaltripstatus_id=safa_trip_internaltrips.safa_internaltripstatus_id', 'left');
            $this->db->join('safa_itos', 'safa_itos.safa_ito_id=safa_trip_internaltrips.safa_ito_id', 'left');

            
            $this->db->where('(safa_trip_internaltrips.safa_uo_id='.$this->uo_id.' or safa_uo_contracts.safa_uo_id='. $this->uo_id .')');
                        
        } elseif ($this->ito_id !== FALSE) {
            $this->db->select('safa_trip_internaltrips.*, safa_trip_internaltrip_uos.' . name() . ' as umoperatorname, safa_contract_uos.' . name() . ' as safa_contract_uo_name, safa_trips.name as trip_name,safa_internaltripstatus.' . name() . ' as status_name,
                              (select count(*) from safa_internalsegments where 
                               safa_internalsegments.safa_trip_internaltrip_id=safa_trip_internaltrips.safa_trip_internaltrip_id) as  num_segments,
                               safa_transporters.' . name() . ' as transportername, concat(safa_uos.' . name() . '," ( ",safa_uo_contracts.' . name() . '," )" )  as contract_name
                               , erp_transportertypes.' . name() . ' as erp_transportertype_name, safa_uos.active as safa_uo_active
                               ', false);
            $this->db->join("safa_trips", "safa_trip_internaltrips.safa_trip_id = safa_trips.safa_trip_id", 'left');
            $this->db->join('safa_uo_contracts', 'safa_trip_internaltrips.safa_uo_contract_id = safa_uo_contracts.safa_uo_contract_id', 'left');
            $this->db->join('safa_uos safa_contract_uos', 'safa_contract_uos.safa_uo_id= safa_uo_contracts.safa_uo_id', 'left');
            $this->db->join('safa_uos safa_trip_internaltrip_uos', 'safa_trip_internaltrip_uos.safa_uo_id= safa_trip_internaltrips.safa_uo_id', 'left');
            
            $this->db->join('safa_uos', 'safa_uos.safa_uo_id= safa_uo_contracts.safa_uo_id', 'left');
            
            $this->db->join('safa_transporters', 'safa_trip_internaltrips.safa_transporter_id= safa_transporters.safa_transporter_id', 'left');
            $this->db->join('safa_internaltripstatus', 'safa_internaltripstatus.safa_internaltripstatus_id=safa_trip_internaltrips.safa_internaltripstatus_id', 'left');
            
            $this->db->where('safa_trip_internaltrips.safa_ito_id', $this->ito_id);
             
            $this->db->where('( (safa_trip_internaltrips.safa_internaltripstatus_id=4) or (safa_trip_internaltrips.safa_internaltripstatus_id=2 AND safa_internaltrip_type_id=1) or (safa_trip_internaltrips.safa_ea_id is null AND safa_trip_internaltrips.safa_uo_id is null) or (safa_trip_internaltrips.safa_internaltripstatus_id=5) or (safa_trip_internaltrips.owner_erp_company_type_id=5))');
        } 
        
        $this->db->join('erp_transportertypes', 'erp_transportertypes.erp_transportertype_id=safa_trip_internaltrips.erp_transportertype_id', 'left');
            
        $this->db->order_by('serial', 'desc');
        
        $this->db->from('safa_trip_internaltrips');
        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);
        $query = $this->db->get();
        
        //echo $this->db->last_query(); exit;
        
        if ($rows_no)
            return $query->num_rows();

        if ($this->safa_trip_internaltrip_id)
            return $query->row();
        else
            return $query->result();
    }

    function save() {
        if ($this->safa_trip_internaltrip_id !== FALSE)
            $this->db->set('safa_trip_internaltrips.safa_trip_internaltrip_id', $this->safa_trip_internaltrip_id);

        if ($this->safa_trip_id !== FALSE)
            $this->db->set('safa_trip_internaltrips.safa_trip_id', $this->safa_trip_id);

        if ($this->safa_ito_id !== FALSE)
            $this->db->set('safa_trip_internaltrips.safa_ito_id', $this->safa_ito_id);

		if ($this->safa_uo_id !== FALSE)
            $this->db->set('safa_trip_internaltrips.safa_uo_id', $this->safa_uo_id);
            
        if ($this->safa_ea_id !== FALSE)
            $this->db->set('safa_trip_internaltrips.safa_ea_id', $this->safa_ea_id);

        if ($this->owner_erp_company_type_id !== FALSE)
            $this->db->set('safa_trip_internaltrips.owner_erp_company_type_id', $this->owner_erp_company_type_id);
        
        if ($this->safa_transporter_id !== FALSE)
            $this->db->set('safa_trip_internaltrips.safa_transporter_id', $this->safa_transporter_id);

        if ($this->safa_tripstatus_id !== FALSE)
            $this->db->set('safa_trip_internaltrips.safa_tripstatus_id', $this->safa_tripstatus_id);

        if ($this->safa_internaltripstatus_id !== FALSE)
            $this->db->set('safa_trip_internaltrips.safa_internaltripstatus_id', $this->safa_internaltripstatus_id);

        if ($this->operator_reference !== FALSE)
            $this->db->set('safa_trip_internaltrips.operator_reference', $this->operator_reference);

        if ($this->attachement !== FALSE)
            $this->db->set('safa_trip_internaltrips.attachement', $this->attachement);

        if ($this->datetime !== FALSE)
            $this->db->set('safa_trip_internaltrips.datetime', $this->datetime);

        if ($this->confirmation_number !== FALSE)
            $this->db->set('safa_trip_internaltrips.confirmation_number', $this->confirmation_number);

        if ($this->ito_notes !== FALSE)
            $this->db->set('safa_trip_internaltrips.ito_notes', $this->ito_notes);

        if ($this->ea_notes !== FALSE)
            $this->db->set('safa_trip_internaltrips.ea_notes', $this->ea_notes);

        if ($this->safa_uo_contract_id !== FALSE)
            $this->db->set('safa_trip_internaltrips.safa_uo_contract_id', $this->safa_uo_contract_id);
        
        if ($this->trip_title !== FALSE)
            $this->db->set('safa_trip_internaltrips.trip_title', $this->trip_title);

        if ($this->trip_supervisors !== FALSE)
            $this->db->set('safa_trip_internaltrips.trip_supervisors', $this->trip_supervisors);

        if ($this->trip_supervisors_phone !== FALSE)
            $this->db->set('safa_trip_internaltrips.trip_supervisors_phone', $this->trip_supervisors_phone);

        if ($this->erp_company_type_id !== FALSE)
            $this->db->set('safa_trip_internaltrips.erp_company_type_id', $this->erp_company_type_id);

        if ($this->erp_company_id !== FALSE)
            $this->db->set('safa_trip_internaltrips.erp_company_id', $this->erp_company_id);

		if ($this->serial !== FALSE)
            $this->db->set('safa_trip_internaltrips.serial', $this->serial);
            
        if ($this->buses_count !== FALSE)
            $this->db->set('safa_trip_internaltrips.buses_count', $this->buses_count);
            
            
        if ($this->erp_company_name !== FALSE)
            $this->db->set('safa_trip_internaltrips.erp_company_name', $this->erp_company_name);

        if ($this->adult_seats !== FALSE)
            $this->db->set('safa_trip_internaltrips.adult_seats', $this->adult_seats);

        if ($this->child_seats !== FALSE)
            $this->db->set('safa_trip_internaltrips.child_seats', $this->child_seats);

        if ($this->baby_seats !== FALSE)
            $this->db->set('safa_trip_internaltrips.baby_seats', $this->baby_seats);

        if ($this->safa_internaltrip_type_id !== FALSE)
            $this->db->set('safa_trip_internaltrips.safa_internaltrip_type_id', $this->safa_internaltrip_type_id);

        if ($this->erp_transportertype_id !== FALSE)
            $this->db->set('safa_trip_internaltrips.erp_transportertype_id', $this->erp_transportertype_id);
           
        if ($this->notes !== FALSE)
            $this->db->set('safa_trip_internaltrips.notes', $this->notes);
        
        if ($this->erp_flight_availability_id !== FALSE)
            $this->db->set('safa_trip_internaltrips.erp_flight_availability_id', $this->erp_flight_availability_id);

        if ($this->erp_cruise_availability_id !== FALSE)
            $this->db->set('safa_trip_internaltrips.erp_cruise_availability_id', $this->erp_cruise_availability_id);
         
        if ($this->uo_email_sending_datetime !== FALSE)
            $this->db->set('safa_trip_internaltrips.uo_email_sending_datetime', $this->uo_email_sending_datetime);
         
            
        if ($this->deleted !== FALSE)
            $this->db->set('safa_trip_internaltrips.deleted', $this->deleted);

        if ($this->delete_reason !== FALSE)
            $this->db->set('safa_trip_internaltrips.delete_reason', $this->delete_reason);
        
        if ($this->safa_trip_internaltrip_id) {
            $this->db->where('safa_trip_internaltrips.safa_trip_internaltrip_id', $this->safa_trip_internaltrip_id)->update('safa_trip_internaltrips');
        } else {
            $this->db->insert('safa_trip_internaltrips');
            return $this->db->insert_id();
        }
    }

    function delete() {
        if ($this->safa_trip_internaltrip_id !== FALSE)
            $this->db->where('safa_trip_internaltrips.safa_trip_internaltrip_id', $this->safa_trip_internaltrip_id);

        if ($this->safa_trip_id !== FALSE)
            $this->db->where('safa_trip_internaltrips.safa_trip_id', $this->safa_trip_id);

        if ($this->safa_ito_id !== FALSE)
            $this->db->where('safa_trip_internaltrips.safa_ito_id', $this->safa_ito_id);

        if ($this->safa_transporter_id !== FALSE)
            $this->db->where('safa_trip_internaltrips.safa_transporter_id', $this->safa_transporter_id);

        if ($this->safa_tripstatus_id !== FALSE)
            $this->db->where('safa_trip_internaltrips.safa_tripstatus_id', $this->safa_tripstatus_id);

        if ($this->safa_internaltripstatus_id !== FALSE)
            $this->db->where('safa_trip_internaltrips.safa_internaltripstatus_id', $this->safa_internaltripstatus_id);

        if ($this->operator_reference !== FALSE)
            $this->db->where('safa_trip_internaltrips.operator_reference', $this->operator_reference);

        if ($this->attachement !== FALSE)
            $this->db->where('safa_trip_internaltrips.attachement', $this->attachement);

        if ($this->datetime !== FALSE)
            $this->db->where('safa_trip_internaltrips.datetime', $this->datetime);

        if ($this->confirmation_number !== FALSE)
            $this->db->where('safa_trip_internaltrips.confirmation_number', $this->confirmation_number);

        if ($this->ito_notes !== FALSE)
            $this->db->where('safa_trip_internaltrips.ito_notes', $this->ito_notes);

        if ($this->ea_notes !== FALSE)
            $this->db->where('safa_trip_internaltrips.ea_notes', $this->ea_notes);

        if ($this->safa_uo_contract_id !== FALSE)
            $this->db->where('safa_trip_internaltrips.safa_uo_contract_id', $this->safa_uo_contract_id);
        
        if ($this->trip_title !== FALSE)
            $this->db->where('safa_trip_internaltrips.trip_title', $this->trip_title);

        if ($this->trip_supervisors !== FALSE)
            $this->db->where('safa_trip_internaltrips.trip_supervisors', $this->trip_supervisors);

        if ($this->trip_supervisors_phone !== FALSE)
            $this->db->where('safa_trip_internaltrips.trip_supervisors_phone', $this->trip_supervisors_phone);
        
        if ($this->adult_seats !== FALSE)
            $this->db->where('safa_trip_internaltrips.adult_seats', $this->adult_seats);

        if ($this->child_seats !== FALSE)
            $this->db->where('safa_trip_internaltrips.child_seats', $this->child_seats);

        if ($this->baby_seats !== FALSE)
            $this->db->where('safa_trip_internaltrips.baby_seats', $this->baby_seats);

        if ($this->notes !== FALSE)
            $this->db->where('safa_trip_internaltrips.notes', $this->notes);
        
        $this->db->delete('safa_trip_internaltrips');
        return $this->db->affected_rows();
    }

    function search($rows_no = FALSE) {
        if ($this->custom_select !== FALSE) {
            $this->db->select($this->custom_select);
        }

        if ($this->safa_trip_internaltrip_id !== FALSE)
            $this->db->where('safa_trip_internaltrips.safa_trip_internaltrip_id', $this->safa_trip_internaltrip_id);

        if ($this->safa_trip_id !== FALSE)
            $this->db->where('safa_trip_internaltrips.safa_trip_id', $this->safa_trip_id);

        if ($this->safa_ito_id !== FALSE)
            $this->db->where('safa_trip_internaltrips.safa_ito_id', $this->safa_ito_id);

        if ($this->safa_transporter_id !== FALSE)
            $this->db->where('safa_trip_internaltrips.safa_transporter_id', $this->safa_transporter_id);

        if ($this->safa_tripstatus_id !== FALSE)
            $this->db->where('safa_trip_internaltrips.safa_tripstatus_id', $this->safa_tripstatus_id);

        if ($this->safa_internaltripstatus_id !== FALSE)
            $this->db->where('safa_trip_internaltrips.safa_internaltripstatus_id', $this->safa_internaltripstatus_id);

        if ($this->operator_reference !== FALSE)
            $this->db->where('safa_trip_internaltrips.operator_reference', $this->operator_reference);

        if ($this->attachement !== FALSE)
            $this->db->where('safa_trip_internaltrips.attachement', $this->attachement);

        if ($this->datetime !== FALSE)
            $this->db->where('safa_trip_internaltrips.datetime', $this->datetime);

        if ($this->confirmation_number !== FALSE)
            $this->db->where('safa_trip_internaltrips.confirmation_number', $this->confirmation_number);

        if ($this->ito_notes !== FALSE)
            $this->db->where('safa_trip_internaltrips.ito_notes', $this->ito_notes);

        if ($this->ea_notes !== FALSE)
            $this->db->where('safa_trip_internaltrips.ea_notes', $this->ea_notes);

        if ($this->safa_uo_contract_id !== FALSE)
            $this->db->where('safa_trip_internaltrips.safa_uo_contract_id', $this->safa_uo_contract_id);
        
        if ($this->trip_title !== FALSE)
            $this->db->where('safa_trip_internaltrips.trip_title', $this->trip_title);

        if ($this->trip_supervisors !== FALSE)
            $this->db->where('safa_trip_internaltrips.trip_supervisors', $this->trip_supervisors);

        if ($this->trip_supervisors_phone !== FALSE)
            $this->db->where('safa_trip_internaltrips.trip_supervisors_phone', $this->trip_supervisors_phone);
        
        if ($this->adult_seats !== FALSE)
            $this->db->where('safa_trip_internaltrips.adult_seats', $this->adult_seats);

        if ($this->child_seats !== FALSE)
            $this->db->where('safa_trip_internaltrips.child_seats', $this->child_seats);

        if ($this->baby_seats !== FALSE)
            $this->db->where('safa_trip_internaltrips.baby_seats', $this->baby_seats);

        if ($this->notes !== FALSE)
            $this->db->where('safa_trip_internaltrips.notes', $this->notes);
        
        $this->db->where("(safa_trip_internaltrips.deleted IS NULL OR safa_trip_internaltrips.deleted=0)");
        
        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);
        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);
        $query = $this->db->get('safa_trip_internaltrips');
        if ($rows_no)
            return $query->num_rows();

        if ($this->safa_trip_internaltrip_id)
            return $query->row();
        else
            return $query->result();
    }

    function get_ea_contracts() {
        $this->db->select("safa_uo_contracts.safa_uo_contract_id as contract_id, safa_uo_contracts_eas.safa_uo_contract_ea_id, safa_uo_contracts." . name() . ",safa_uos." . name() . " as safa_uos_name,safa_uo_contracts_eas." . name() . " as safa_uo_contracts_eas_name");
        $this->db->from("safa_uo_contracts");
        $this->db->join("safa_uo_contracts_eas", "safa_uo_contracts_eas.safa_uo_contract_id=safa_uo_contracts.safa_uo_contract_id");
        $this->db->join("safa_uos", "safa_uos.safa_uo_id=safa_uo_contracts.safa_uo_id");
        $this->db->where("safa_uo_contracts_eas.disabled=0");
        $this->db->where("safa_uo_contracts_eas.safa_ea_id", $this->ea_id); //from a seasion;
        return $this->db->get()->result();
    }

    function get_uo_contracts() {
        $this->db->select("safa_uo_contracts.safa_uo_contract_id as contract_id, safa_uo_contracts." . name());
        $this->db->from("safa_uo_contracts");

        $this->db->where("safa_uo_contracts.safa_uo_id", $this->uo_id); //from a seasion;
        return $this->db->get()->result();
    }
    //By Gouda
    //--------------------------------------
	function get_uo_eas() {
		$this->db->distinct();
        $this->db->select("safa_uo_contracts_eas.safa_ea_id , safa_uo_contracts." . name()." as safa_uo_contract_name, safa_eas.".name()." as safa_ea_name");
        $this->db->from("safa_uo_contracts_eas");
		$this->db->join('safa_eas', 'safa_eas.safa_ea_id=safa_uo_contracts_eas.safa_ea_id');
        $this->db->join('safa_uo_contracts', 'safa_uo_contracts.safa_uo_contract_id=safa_uo_contracts_eas.safa_uo_contract_id');
        
		$this->db->where("safa_uo_contracts.safa_uo_id", $this->uo_id); //from a seasion;
        return $this->db->get()->result();
    }

	function get_ea_uos() {
        $this->db->select("safa_uo_contracts.safa_uo_contract_id ,safa_uo_contracts_eas." . name() . " as safa_uo_contracts_eas_name");
        $this->db->from("safa_uo_contracts");
        $this->db->join("safa_uo_contracts_eas", "safa_uo_contracts_eas.safa_uo_contract_id=safa_uo_contracts.safa_uo_contract_id");
        $this->db->join("safa_uos", "safa_uos.safa_uo_id=safa_uo_contracts.safa_uo_id");
        $this->db->where("safa_uo_contracts_eas.disabled=0");
        $this->db->where("safa_uo_contracts_eas.safa_ea_id", $this->ea_id); //from a seasion;
        return $this->db->get()->result();
    }
    //---------------------------------------------------
    
    function get_uo_contracts_itos() {
        $this->db->select('distinct safa_itos.safa_ito_id,safa_itos.' . name(), false);
        $this->db->from('safa_itos');
        $this->db->join('safa_uo_contracts_itos', 'safa_uo_contracts_itos.safa_ito_id=safa_itos.safa_ito_id')
                ->join('safa_uo_contracts', 'safa_uo_contracts.safa_uo_contract_id=safa_uo_contracts_itos.safa_uo_contract_id')
                ->where('safa_uo_contracts.safa_uo_id', $this->uo_id);
                if($this->uo_id!='') {
                	$this->db->where('(safa_itos.safa_uo_id IS NULL OR safa_itos.safa_uo_id='.$this->uo_id.')');
                }
        $result = $this->db->get()->result();
        return $result;
    }

    function get_ea_contracts_itos() {
        $this->db->select('distinct safa_itos.safa_ito_id,safa_itos.' . name(), false);
        $this->db->from('safa_itos');
        $this->db->join('safa_uo_contracts_itos', 'safa_uo_contracts_itos.safa_ito_id=safa_itos.safa_ito_id')
                ->join('safa_uo_contracts_eas', 'safa_uo_contracts_eas.safa_uo_contract_id=safa_uo_contracts_itos.safa_uo_contract_id')
                ->where('safa_uo_contracts_eas.safa_ea_id', $this->ea_id);
        $itos = $this->db->get()->result();
        return $itos;
    }

    function get_ito_contracts() {
        $this->db->select('safa_uo_contracts.safa_uo_contract_id as contract_id,safa_uo_contracts.' . name().',  concat(safa_uos.' . name() . '," ( ",safa_uo_contracts.' . name() . '," )" )  as contract_name');
        $this->db->from("safa_uo_contracts");
        $this->db->join("safa_uo_contracts_itos", "safa_uo_contracts_itos.safa_uo_contract_id=safa_uo_contracts.safa_uo_contract_id");
        $this->db->join('safa_uos', 'safa_uos.safa_uo_id= safa_uo_contracts.safa_uo_id', 'left');
            
        $this->db->where("safa_uo_contracts_itos.safa_ito_id", $this->ito_id); //from a seasion;
        return $this->db->get()->result();
    }

    function get_ea_tourismplaces() {
        $this->db->select("erp_hotels.erp_hotel_id,erp_hotels." . name());
        $this->db->from("erp_hotels");
        $this->db->join("safa_uo_contracts_hotels", "erp_hotels.erp_hotel_id=safa_uo_contracts_hotels.erp_hotel_id", "right");
        $this->db->join("safa_uo_contracts_eas", "safa_uo_contracts_hotels.safa_uo_contract_id=safa_uo_contracts_eas.safa_uo_contract_id", "left");
        $this->db->where("safa_uo_contracts_eas.safa_ea_id", $this->ea_id); //from a seasion;
        $result = $this->db->get()->result(); //notused//
    }

    function get_uo_contract_trips($safa_uo_contract_id) {
        $this->db->select("safa_trips.safa_trip_id,safa_trips.name");
        
        $this->db->from("safa_trips");
        $this->db->join('safa_uo_contracts_eas', 'safa_uo_contracts_eas.safa_ea_id=safa_trips.safa_ea_id');
        $this->db->where("safa_uo_contracts_eas.safa_uo_contract_id", $safa_uo_contract_id); 
       
        return $this->db->get()->result();
    }

    function get_ea_hotels($trip_id) {
        $this->db->select("erp_hotels.erp_hotel_id,erp_hotels." . name());
        $this->db->from("erp_hotels");
        $this->db->join("safa_uo_contracts_hotels", "erp_hotels.erp_hotel_id=safa_uo_contracts_hotels.erp_hotel_id");
        $this->db->join("safa_uo_contracts", "safa_uo_contracts.safa_uo_contract_id=safa_uo_contracts_hotels.safa_uo_contract_id");
        $this->db->join("safa_uo_contracts_eas", "safa_uo_contracts_eas.safa_uo_contract_id=safa_uo_contracts.safa_uo_contract_id");
        $this->db->join("safa_trips", "safa_trips.safa_ea_id=safa_uo_contracts_eas.safa_ea_id");
        $this->db->where("safa_trips.safa_trip_id", $trip_id); //not used//
        $result = $this->db->get()->result();
        return $result;
    }

    function get_ea_tourism_places($trip) {
        $this->db->select("safa_tourismplaces.safa_tourismplace_id,safa_tourismplaces." . name());
        $this->db->from("safa_tourismplaces");
        $this->db->join("safa_uo_contracts_tourismplaces", "safa_tourismplaces.safa_tourismplace_id=safa_uo_contracts_tourismplaces.safa_tourismplace_id");
        $this->db->join("safa_uo_contracts", "safa_uo_contracts.safa_uo_contract_id=safa_uo_contracts_tourismplaces.safa_uo_contract_id");
        $this->db->join("safa_uo_contracts_eas", "safa_uo_contracts_eas.safa_uo_contract_id=safa_uo_contracts.safa_uo_contract_id");
        $this->db->join("safa_ea_packages", "safa_ea_packages.safa_uo_contract_ea_id=safa_uo_contracts_eas.safa_uo_contract_ea_id");
        $this->db->join("safa_trips", "safa_trips.safa_ea_package_id=safa_ea_packages.safa_ea_package_id");
        $this->db->where("safa_trips.safa_trip_id", $trip); //not used//
        $result = $this->db->get()->result();
        return $result;
    }

    function get_trip_hotels($trip_id) {
        $this->db->select("erp_hotels.erp_hotel_id,erp_hotels." . name());
        $this->db->from("erp_hotels");
        $this->db->order_by("erp_hotels." . name());
        if($trip_id!=0) {
        $this->db->join("safa_passport_accommodation_rooms", "safa_passport_accommodation_rooms.erp_hotel_id=erp_hotels.erp_hotel_id");
        $this->db->join("safa_reservation_forms", "safa_passport_accommodation_rooms.safa_reservation_form_id=safa_reservation_forms.safa_reservation_form_id");
        
        $this->db->where("safa_reservation_forms.safa_trip_id", $trip_id);
        }
        $results = $this->db->get()->result();
        return $results;
    }

    function get_trip_tourismplace($trip_id) {

        $this->db->select("safa_tourismplaces.safa_tourismplace_id,safa_tourismplaces." . name());
        $this->db->from("safa_tourismplaces");
        $this->db->join("safa_package_tourismplaces", "safa_package_tourismplaces.safa_tourismplace_id = safa_tourismplaces.safa_tourismplace_id");
        $this->db->join("safa_package_periods", "safa_package_periods.safa_package_id = safa_package_tourismplaces.safa_package_id");
        $this->db->join("safa_group_passport_accommodation", "safa_group_passport_accommodation.safa_package_periods_id = safa_package_periods.safa_package_periods_id");
        $this->db->join("safa_group_passports", "safa_group_passport_accommodation.safa_group_passport_id = safa_group_passports.safa_group_passport_id");
        
        $this->db->where("safa_group_passports.safa_trip_id", $trip_id);
        $this->db->group_by('safa_tourismplaces.safa_tourismplace_id');
        $result = $this->db->get()->result();
        return $result;
    }

    function get_trip_contracts($trip_id) {
        $this->db->select("safa_uo_contracts_eas.safa_uo_contract_id");
        $this->db->from("safa_uo_contracts_eas");
        $result = $this->db->get()->row()->safa_uo_contract_id;
        return $result;
    }

    function get_transporters_withContracts() {
        $this->db->select("safa_transporters.safa_transporter_id as transporter_id,safa_transporters." . name() . ",safa_transporters.erp_transportertype_id");
        $this->db->from("safa_transporters");
        //$this->db->join("safa_uo_contracts_transporters", "safa_transporters.safa_transporter_id=safa_uo_contracts_transporters.safa_transporter_id");
        //$this->db->join("safa_uo_contracts_eas", "safa_uo_contracts_transporters.safa_uo_contract_id = safa_uo_contracts_eas.safa_uo_contract_id");
        $this->db->where("safa_transporters.erp_transportertype_id", 1);
        //$this->db->where("safa_uo_contracts_eas.safa_ea_id", session('ea_id'));
        return $this->db->get()->result();
    }

    function get_ito_transporters_withContracts() {
        $this->db->distinct();
        $this->db->select("safa_transporters.safa_transporter_id as transporter_id,safa_transporters." . name() . ",safa_transporters.erp_transportertype_id");
        $this->db->from("safa_transporters");
        $this->db->join("safa_uo_contracts_transporters", "safa_transporters.safa_transporter_id=safa_uo_contracts_transporters.safa_transporter_id");
        $this->db->join("safa_uo_contracts_itos", "safa_uo_contracts_itos.safa_uo_contract_id=safa_uo_contracts_transporters.safa_uo_contract_id");
        $this->db->where("safa_transporters.erp_transportertype_id", 1);
        $this->db->where("safa_uo_contracts_itos.safa_ito_id", $this->ito_id); //from a seasion;
        return $this->db->get()->result();
    }

    function get_uo_transporters_withContracts() {
        $this->db->select("safa_transporters.safa_transporter_id as transporter_id,safa_transporters." . name() . ",safa_transporters.erp_transportertype_id");
        $this->db->from("safa_transporters");
        //$this->db->join("safa_uo_contracts_transporters", "safa_transporters.safa_transporter_id=safa_uo_contracts_transporters.safa_transporter_id");
        //$this->db->join("safa_uo_contracts", "safa_uo_contracts_transporters.safa_uo_contract_id = safa_uo_contracts.safa_uo_contract_id");
        $this->db->where("safa_transporters.erp_transportertype_id", 1);
        $this->db->where('(safa_transporters.safa_uo_id IS NULL OR safa_transporters.safa_uo_id='.$this->uo_id.')');
        return $this->db->get()->result();
    }
    
    function get_trip_contract_id($trip_id = false) {
        $this->db->select('safa_uo_contracts_eas.safa_uo_contract_id');
        $this->db->from('safa_uo_contracts_eas');
        $this->db->join('safa_trips', 'safa_trips.safa_ea_id=safa_uo_contracts_eas.safa_ea_id');
        $this->db->where('safa_trips.safa_trip_id', $trip_id);
        $result = $this->db->get()->row();
        return $result;
    }

    function getCompaniesByCompanyType($erp_company_type_id) {

        if ($erp_company_type_id == 1) {
            $this->db->select('erp_admins.erp_admin_id as id, erp_admins.username as name_ar, erp_admins.username as name_la');
            $this->db->from('erp_admins');
        } else if ($erp_company_type_id == 2) {
            $this->db->select('safa_uos.safa_uo_id as id, safa_uos.name_ar, safa_uos.name_la');
            $this->db->from('safa_uos');
            $this->db->where('safa_uos.disabled', 0);
            //$this->db->where('safa_uos.', '');
        } else if ($erp_company_type_id == 3) {
            $this->db->select('safa_eas.safa_ea_id as id, safa_eas.name_ar, safa_eas.name_la');
            $this->db->from('safa_eas');
            $this->db->where('safa_eas.disabled', 0);
        } else if ($erp_company_type_id == 4) {
            $this->db->select('safa_itos.safa_ito_id as id, safa_itos.name_ar, safa_itos.name_la');
            $this->db->from('safa_itos');
            $this->db->where('safa_itos.disabled', 0);
        } else if ($erp_company_type_id == 5) {
            $this->db->select('safa_hms.safa_hm_id as id, safa_hms.name_ar, safa_hms.name_la');
            $this->db->from('safa_hms');
        } else {
            $this->db->select('safa_eas.safa_ea_id as id, safa_eas.name_ar, safa_eas.name_la');
            $this->db->from('safa_eas');
        }


        $query = $this->db->get();

        //$query_text=$this->db->last_query();
        //echo $query_text; exit;

        return $query->result();
    }

    function get_max_serial_by_uo($safa_uo_id=0)
    {
    	$this->db->select('max(serial) as max_serial');
        $this->db->from('safa_trip_internaltrips');
        $this->db->join('safa_uo_contracts', 'safa_uo_contracts.safa_uo_contract_id = safa_trip_internaltrips.safa_uo_contract_id', 'left');
        $this->db->where('(safa_trip_internaltrips.safa_uo_id='.$safa_uo_id.' or safa_uo_contracts.safa_uo_id='. $safa_uo_id .')');
        $query = $this->db->get();
        
        //$echo =  $this->db->last_query();
        return $query->row();
    }

}

/* End of file trip_internaltrip_model.php */
/* Location: ./application/models/trip_internaltrip_model.php */
