<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Ea_branches_model extends CI_Model {

    public $safa_ea_branch_id = FALSE;
    public $name_ar = FALSE;
    public $name_la = FALSE;
    public $address = FALSE;
    public $safa_ea_id = FALSE;
    public $erp_city_id = FALSE;
    public $phone = FALSE;
    public $fax = FALSE;
    public $mobile = FALSE;
    public $email = FALSE;
    public $is_main = FALSE;
    public $custom_select = FALSE;
    public $limit = FALSE;
    public $offset = FALSE;
    public $order_by = FALSE;

    function __construct() {
        parent::__construct();
    }

    function get($rows_no = FALSE) {

        if ($this->custom_select !== FALSE) {
            $this->db->select('safa_ea_branch_id');
            $this->db->select($this->custom_select);
        }

        if ($this->safa_ea_branch_id !== FALSE)
            $this->db->where('safa_ea_branches.safa_ea_branch_id', $this->safa_ea_branch_id);

        if ($this->name_ar !== FALSE)
            $this->db->where('safa_ea_branches.name_ar', $this->name_ar);

        if ($this->name_la !== FALSE)
            $this->db->where('safa_ea_branches.name_la', $this->name_la);

        if ($this->address !== FALSE)
            $this->db->where('safa_ea_branches.address', $this->address);

        if ($this->safa_ea_id !== FALSE)
            $this->db->where('safa_ea_branches.safa_ea_id', $this->safa_ea_id);

        if ($this->erp_city_id !== FALSE)
            $this->db->where('safa_ea_branches.erp_city_id', $this->erp_city_id);

        if ($this->phone !== FALSE)
            $this->db->where('safa_ea_branches.phone', $this->phone);

        if ($this->fax !== FALSE)
            $this->db->where('safa_ea_branches.fax', $this->fax);

        if ($this->mobile !== FALSE)
            $this->db->where('safa_ea_branches.mobile', $this->mobile);

        if ($this->email !== FALSE)
            $this->db->where('safa_ea_branches.email', $this->email);

        if ($this->is_main !== FALSE)
            $this->db->where('safa_ea_branches.is_main', $this->is_main);


        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('safa_ea_branches');
        if ($rows_no)
            return $query->num_rows();

        if ($this->safa_ea_branch_id)
            return $query->row();
        else
            return $query->result();
    }

    function save() {
        if ($this->safa_ea_branch_id !== FALSE)
            $this->db->set('safa_ea_branches.safa_ea_branch_id', $this->safa_ea_branch_id);

        if ($this->name_ar !== FALSE)
            $this->db->set('safa_ea_branches.name_ar', $this->name_ar);

        if ($this->name_la !== FALSE)
            $this->db->set('safa_ea_branches.name_la', $this->name_la);

        if ($this->address !== FALSE)
            $this->db->set('safa_ea_branches.address', $this->address);

        if ($this->safa_ea_id !== FALSE)
            $this->db->set('safa_ea_branches.safa_ea_id', $this->safa_ea_id);

        if ($this->erp_city_id !== FALSE)
            $this->db->set('safa_ea_branches.erp_city_id', $this->erp_city_id);

        if ($this->phone !== FALSE)
            $this->db->set('safa_ea_branches.phone', $this->phone);

        if ($this->fax !== FALSE)
            $this->db->set('safa_ea_branches.fax', $this->fax);

        if ($this->mobile !== FALSE)
            $this->db->set('safa_ea_branches.mobile', $this->mobile);

        if ($this->email !== FALSE)
            $this->db->set('safa_ea_branches.email', $this->email);

        if ($this->is_main !== FALSE)
            $this->db->set('safa_ea_branches.is_main', $this->is_main);



        if ($this->safa_ea_branch_id) {
            $this->db->where('safa_ea_branches.safa_ea_branch_id', $this->safa_ea_branch_id)->update('safa_ea_branches');
        } else {
            $this->db->insert('safa_ea_branches');
            return $this->db->insert_id();
        }
    }

    function delete() {
        if ($this->safa_ea_branch_id !== FALSE)
            $this->db->where('safa_ea_branches.safa_ea_branch_id', $this->safa_ea_branch_id);

        if ($this->name_ar !== FALSE)
            $this->db->where('safa_ea_branches.name_ar', $this->name_ar);

        if ($this->name_la !== FALSE)
            $this->db->where('safa_ea_branches.name_la', $this->name_la);

        if ($this->address !== FALSE)
            $this->db->where('safa_ea_branches.address', $this->address);

        if ($this->safa_ea_id !== FALSE)
            $this->db->where('safa_ea_branches.safa_ea_id', $this->safa_ea_id);

        if ($this->erp_city_id !== FALSE)
            $this->db->where('safa_ea_branches.erp_city_id', $this->erp_city_id);

        if ($this->phone !== FALSE)
            $this->db->where('safa_ea_branches.phone', $this->phone);

        if ($this->fax !== FALSE)
            $this->db->where('safa_ea_branches.fax', $this->fax);

        if ($this->mobile !== FALSE)
            $this->db->where('safa_ea_branches.mobile', $this->mobile);

        if ($this->email !== FALSE)
            $this->db->where('safa_ea_branches.email', $this->email);

        if ($this->is_main !== FALSE)
            $this->db->where('safa_ea_branches.is_main', $this->is_main);



        $this->db->delete('safa_ea_branches');
        return $this->db->affected_rows();
    }

    function search($rows_no = FALSE) {
        if ($this->custom_select !== FALSE) {
            $this->db->select('safa_ea_branch_id');
            $this->db->select($this->custom_select);
        }

        if ($this->safa_ea_branch_id !== FALSE)
            $this->db->where('safa_ea_branches.safa_ea_branch_id', $this->safa_ea_branch_id);

        if ($this->name_ar !== FALSE)
            $this->db->where('safa_ea_branches.name_ar', $this->name_ar);

        if ($this->name_la !== FALSE)
            $this->db->where('safa_ea_branches.name_la', $this->name_la);

        if ($this->address !== FALSE)
            $this->db->where('safa_ea_branches.address', $this->address);

        if ($this->safa_ea_id !== FALSE)
            $this->db->where('safa_ea_branches.safa_ea_id', $this->safa_ea_id);

        if ($this->erp_city_id !== FALSE)
            $this->db->where('safa_ea_branches.erp_city_id', $this->erp_city_id);

        if ($this->phone !== FALSE)
            $this->db->where('safa_ea_branches.phone', $this->phone);

        if ($this->fax !== FALSE)
            $this->db->where('safa_ea_branches.fax', $this->fax);

        if ($this->mobile !== FALSE)
            $this->db->where('safa_ea_branches.mobile', $this->mobile);

        if ($this->email !== FALSE)
            $this->db->where('safa_ea_branches.email', $this->email);

        if ($this->is_main !== FALSE)
            $this->db->where('safa_ea_branches.is_main', $this->is_main);


        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('safa_ea_branches');
        if ($rows_no)
            return $query->num_rows();

        return $query->result();
    }

}

/* End of file ea_branches_model.php */
/* Location: ./application/models/ea_branches_model.php */