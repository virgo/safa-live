<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Trip_travellers_model extends CI_Model {

    public $safa_trip_traveller_id = FALSE;
    public $safa_group_passport_id = FALSE;
    public $safa_trip_id = FALSE;
    public $custom_select = FALSE;
    public $limit = FALSE;
    public $offset = FALSE;
    public $order_by = FALSE;

    function __construct() {
        parent::__construct();
    }

    function get($rows_no = FALSE) {

        if ($this->custom_select !== FALSE) {
            $this->db->select('safa_trip_traveller_id');
            $this->db->select($this->custom_select);
        }

        if ($this->safa_trip_traveller_id !== FALSE)
            $this->db->where('safa_trip_travellers.safa_trip_traveller_id', $this->safa_trip_traveller_id);

        if ($this->safa_group_passport_id !== FALSE)
            $this->db->where('safa_trip_travellers.safa_group_passport_id', $this->safa_group_passport_id);

        if ($this->safa_trip_id !== FALSE)
            $this->db->where('safa_trip_travellers.safa_trip_id', $this->safa_trip_id);


        $this->db->select('safa_group_passports.*, safa_groups.name as safa_groups_name, CONCAT( first_name_ar, " ", second_name_ar, " ", third_name_ar, " ", fourth_name_ar ) as full_name_ar, CONCAT( first_name_la, " ", second_name_la, " ", third_name_la, " ", fourth_name_la ) as full_name_la,  
        erp_passporttypes.' . name() . ' as erp_passporttypes_name, 
        erp_countries.' . name() . ' as erp_countries_name, erp_nationalities.' . name() . ' as erp_nationalities_name, 
        issuing_country.' . name() . ' as issuing_country_name, birth_country.' . name() . ' as birth_country_name, erp_relations.' . name() . ' as erp_relations_name, 

        erp_maritalstatus.' . name() . ' as erp_maritalstatus_name, erp_educationlevels.' . name() . ' as erp_educationlevels_name', FALSE);

        $this->db->join('safa_group_passports', 'safa_group_passports.safa_group_passport_id = safa_trip_travellers.safa_group_passport_id', 'left');

        $this->db->join('safa_groups', 'safa_groups.safa_group_id = safa_group_passports.safa_group_id', 'left');

        $this->db->join('erp_passporttypes', 'safa_group_passports.passport_type_id = erp_passporttypes.erp_passporttype_id', 'left');
        $this->db->join('erp_countries', 'safa_group_passports.erp_country_id = erp_countries.erp_country_id', 'left');
        $this->db->join('erp_nationalities', 'safa_group_passports.nationality_id = erp_nationalities.erp_nationality_id', 'left');
        $this->db->join('erp_countries as issuing_country', 'safa_group_passports.passport_issuing_country_id =issuing_country.erp_country_id', 'left');
        $this->db->join('erp_countries as birth_country', 'safa_group_passports.birth_country_id =birth_country.erp_country_id', 'left');

        $this->db->join('erp_relations', 'safa_group_passports.relative_relation_id = erp_relations.erp_relation_id', 'left');
        $this->db->join('erp_maritalstatus', 'safa_group_passports.marital_status_id = erp_maritalstatus.erp_maritalstatus_id', 'left');
        $this->db->join('erp_educationlevels', 'safa_group_passports.educational_level_id = erp_educationlevels.erp_educationlevel_id', 'left');


        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('safa_trip_travellers');
        if ($rows_no)
            return $query->num_rows();

        if ($this->safa_trip_traveller_id)
            return $query->row();
        else
            return $query->result();
    }

    function save() {

        if ($this->safa_trip_traveller_id !== FALSE)
            $this->db->set('safa_trip_travellers.safa_trip_traveller_id', $this->safa_trip_traveller_id);

        if ($this->safa_group_passport_id !== FALSE)
            $this->db->set('safa_trip_travellers.safa_group_passport_id', $this->safa_group_passport_id);

        if ($this->safa_trip_id !== FALSE)
            $this->db->set('safa_trip_travellers.safa_trip_id', $this->safa_trip_id);

        if ($this->safa_trip_traveller_id) {
            $this->db->where('safa_trip_travellers.safa_trip_traveller_id', $this->safa_trip_traveller_id)->update('safa_trip_travellers');
        } else {
            $this->db->insert('safa_trip_travellers');
            return $this->db->insert_id();
        }
    }

    function delete() {
        if ($this->safa_trip_traveller_id !== FALSE)
            $this->db->where('safa_trip_travellers.safa_trip_traveller_id', $this->safa_trip_traveller_id);

        if ($this->safa_group_passport_id !== FALSE)
            $this->db->where('safa_trip_travellers.safa_group_passport_id', $this->safa_group_passport_id);

        if ($this->safa_trip_id !== FALSE)
            $this->db->where('safa_trip_travellers.safa_trip_id', $this->safa_trip_id);


        if ($this->safa_trip_traveller_id || $this->safa_group_passport_id || $this->safa_trip_id)
            $this->db->delete('safa_trip_travellers');
        return $this->db->affected_rows();
    }

    function get_safa_group_passports() {
        $returndata = array();
        if (session('ea_id')) {
            $this->db->select("safa_group_passport_id, CONCAT(first_name_ar, ' ', second_name_ar, ' ', third_name_ar, ' ', fourth_name_ar) as name", false);
            $this->db->join('safa_groups', 'safa_group_passports.safa_group_id = safa_groups.safa_group_id', 'left');
            $this->db->where('safa_groups.safa_ea_id', session('ea_id'));
            $query = $this->db->get('safa_group_passports');
            $passports = $query->result();
            foreach ($passports as $passport) {
                $returndata[$passport->safa_group_passport_id] = $passport->name;
            }
        }
        return $returndata;
    }

}

/* End of file trip_travellers_model.php */
/* Location: ./application/models/trip_travellers_model.php */