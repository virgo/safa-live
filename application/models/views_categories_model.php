<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Views_categories_model extends CI_Model {

    public $views_categories_id = FALSE;
    public $name = FALSE;
    public $module_name = FALSE;
    public $table = FALSE;
    public $settings_table = FALSE;
    public $session = FALSE;
    public $custom_select = FALSE;
    public $limit = FALSE;
    public $offset = FALSE;
    public $order_by = FALSE;

    function __construct() {
        parent::__construct();
    }

    function get($rows_no = FALSE) {

        if ($this->custom_select !== FALSE) {
            $this->db->select('views_categories_id');
            $this->db->select($this->custom_select);
        }

        if ($this->views_categories_id !== FALSE)
            $this->db->where('views_categories.views_categories_id', $this->views_categories_id);

        if ($this->name !== FALSE)
            $this->db->where('views_categories.name', $this->name);

        if ($this->module_name !== FALSE)
            $this->db->where('views_categories.module_name', $this->module_name);

        if ($this->table !== FALSE)
            $this->db->where('views_categories.table', $this->table);

        if ($this->settings_table !== FALSE)
            $this->db->where('views_categories.settings_table', $this->settings_table);

        if ($this->session !== FALSE)
            $this->db->where('views_categories.session', $this->session);


        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('views_categories');
        if ($rows_no)
            return $query->num_rows();

        if ($this->views_categories_id)
            return $query->row();
        else
            return $query->result();
    }

    function save() {
        if ($this->views_categories_id !== FALSE)
            $this->db->set('views_categories.views_categories_id', $this->views_categories_id);

        if ($this->name !== FALSE)
            $this->db->set('views_categories.name', $this->name);

        if ($this->module_name !== FALSE)
            $this->db->set('views_categories.module_name', $this->module_name);

        if ($this->table !== FALSE)
            $this->db->set('views_categories.table', $this->table);

        if ($this->settings_table !== FALSE)
            $this->db->set('views_categories.settings_table', $this->settings_table);

        if ($this->session !== FALSE)
            $this->db->set('views_categories.session', $this->session);



        if ($this->views_categories_id) {
            $this->db->where('views_categories.views_categories_id', $this->views_categories_id)->update('views_categories');
        } else {
            $this->db->insert('views_categories');
            return $this->db->insert_id();
        }
    }

    function delete() {
        if ($this->views_categories_id !== FALSE)
            $this->db->where('views_categories.views_categories_id', $this->views_categories_id);

        if ($this->name !== FALSE)
            $this->db->where('views_categories.name', $this->name);

        if ($this->module_name !== FALSE)
            $this->db->where('views_categories.module_name', $this->module_name);

        if ($this->table !== FALSE)
            $this->db->where('views_categories.table', $this->table);

        if ($this->settings_table !== FALSE)
            $this->db->where('views_categories.settings_table', $this->settings_table);

        if ($this->session !== FALSE)
            $this->db->where('views_categories.session', $this->session);



        $this->db->delete('views_categories');
        return $this->db->affected_rows();
    }

    function search($rows_no = FALSE) {
        if ($this->custom_select !== FALSE) {
            $this->db->select('views_categories_id');
            $this->db->select($this->custom_select);
        }

        if ($this->views_categories_id !== FALSE)
            $this->db->where('views_categories.views_categories_id', $this->views_categories_id);

        if ($this->name !== FALSE)
            $this->db->where('views_categories.name', $this->name);

        if ($this->module_name !== FALSE)
            $this->db->where('views_categories.module_name', $this->module_name);

        if ($this->table !== FALSE)
            $this->db->where('views_categories.table', $this->table);

        if ($this->settings_table !== FALSE)
            $this->db->where('views_categories.settings_table', $this->settings_table);

        if ($this->session !== FALSE)
            $this->db->where('views_categories.session', $this->session);


        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('views_categories');
        if ($rows_no)
            return $query->num_rows();

        return $query->result();
    }

}

/* End of file views_categories_model.php */
/* Location: ./application/models/views_categories_model.php */