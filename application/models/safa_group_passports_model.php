<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Safa_group_passports_model extends CI_Model {

    public $safa_group_passport_id = FALSE;
    public $safa_group_id = FALSE;
    public $offline_id = FALSE;
    public $no = FALSE;
    public $display_order = FALSE;
    public $title_id = FALSE;
    public $first_name_la = FALSE;
    public $second_name_la = FALSE;
    public $third_name_la = FALSE;
    public $fourth_name_la = FALSE;
    public $first_name_ar = FALSE;
    public $second_name_ar = FALSE;
    public $third_name_ar = FALSE;
    public $fourth_name_ar = FALSE;
    public $picture = FALSE;
    public $nationality_id = FALSE;
    public $previous_nationality_id = FALSE;
    public $passport_no = FALSE;
    public $passport_type_id = FALSE;
    public $passport_issue_date = FALSE;
    public $passport_expiry_date = FALSE;
    public $passport_issuing_city = FALSE;
    public $passport_issuing_country_id = FALSE;
    public $passport_dpn_count = FALSE;
    public $dpn_serial_no = FALSE;
    public $relative_relation_id = FALSE;
    public $educational_level_id = FALSE;
    public $occupation = FALSE;
    public $email_address = FALSE;
    public $street_name = FALSE;
    public $date_of_birth = FALSE;
    public $age = FALSE;
    public $age_from = FALSE;
    public $age_to = FALSE;
    public $birth_city = FALSE;
    public $birth_country_id = FALSE;
    public $relative_no = FALSE;
    public $relative_gender_id = FALSE;
    public $gender_id = FALSE;
    public $marital_status_id = FALSE;
    public $religion = FALSE;
    public $zip_code = FALSE;
    public $city = FALSE;
    public $region_name = FALSE;
    public $erp_country_id = FALSE;
    public $id_number = FALSE;
    public $job2 = FALSE;
    public $address = FALSE;
    public $remarks = FALSE;
    public $canceled = FALSE;
    public $mobile = FALSE;
    public $sim_serial = FALSE;
    public $notes = FALSE;
    public $entry_datetime = FALSE;
    public $mofa_date = FALSE;
    public $edate = FALSE;
    public $visa_date = FALSE;
    public $border_number = FALSE;
            
    public $safa_trip_id = FALSE;
    public $safa_trip_internaltrip_id = FALSE;
    
    public $safa_uo_id = FALSE;
    
    public $safa_group_passport_id_arr = FALSE;
	public $where_condition = FALSE;
    
    public $custom_select = FALSE;
    public $limit = FALSE;
    public $offset = FALSE;
    public $order_by = FALSE;
    public $ea_id = FALSE;

    function __construct() {
        parent::__construct();
    }

    function get($rows_no = FALSE) {
//        echo $this->first_name_ar.'///'.$this->third_name_ar;exit();
        if ($this->custom_select !== FALSE) {
            $this->db->select('safa_group_passport_id');
            $this->db->select($this->custom_select);
        }

        if ($this->safa_group_passport_id !== FALSE)
            $this->db->where('safa_group_passports.safa_group_passport_id', $this->safa_group_passport_id);

        if ($this->safa_trip_id !== FALSE)
            $this->db->where('safa_group_passports.safa_trip_id', $this->safa_trip_id);

        if ($this->safa_trip_internaltrip_id !== FALSE)
            $this->db->where('safa_group_passports.safa_trip_internaltrip_id', $this->safa_trip_internaltrip_id);
            
        if ($this->safa_group_id !== FALSE)
            $this->db->where('safa_group_passports.safa_group_id', $this->safa_group_id);
            
        if ($this->offline_id !== FALSE)
            $this->db->where('safa_group_passports.offline_id', $this->offline_id);

        if ($this->no !== FALSE)
            $this->db->where('safa_group_passports.no', $this->no);

        if ($this->display_order !== FALSE)
            $this->db->where('safa_group_passports.display_order', $this->display_order);

        if ($this->title_id !== FALSE)
            $this->db->where('safa_group_passports.title_id', $this->title_id);

        if ($this->first_name_la !== false)
            $this->db->like('safa_group_passports.first_name_la', $this->first_name_la, 'none');


        if ($this->second_name_la !== false)
            $this->db->or_like('safa_group_passports.second_name_la', $this->second_name_la, 'none');

        if ($this->third_name_la !== false)
            $this->db->or_like('safa_group_passports.third_name_la', $this->third_name_la, 'none');

        if ($this->fourth_name_la !== false)
            $this->db->or_like('safa_group_passports.fourth_name_la', $this->fourth_name_la, 'none');

        if ($this->first_name_ar !== false)
            $this->db->like('safa_group_passports.first_name_ar', $this->first_name_ar, 'none');

        if ($this->second_name_ar !== false)
            $this->db->or_like('safa_group_passports.second_name_ar', $this->second_name_ar, 'none');

        if ($this->third_name_ar !== false)
            $this->db->or_like('safa_group_passports.third_name_ar', $this->third_name_ar, 'none');

        if ($this->fourth_name_ar !== false)
            $this->db->or_like('safa_group_passports.fourth_name_ar', $this->fourth_name_ar, 'none');

        if ($this->picture !== FALSE)
            $this->db->where('safa_group_passports.picture', $this->picture);

        if ($this->nationality_id !== FALSE)
            $this->db->where('safa_group_passports.nationality_id', $this->nationality_id);

        if ($this->previous_nationality_id !== FALSE)
            $this->db->where('safa_group_passports.previous_nationality_id', $this->previous_nationality_id);

        if ($this->passport_no !== FALSE)
            $this->db->where('safa_group_passports.passport_no', $this->passport_no);

        if ($this->passport_type_id !== FALSE)
            $this->db->where('safa_group_passports.passport_type_id', $this->passport_type_id);

        if ($this->passport_issue_date !== FALSE)
            $this->db->where('safa_group_passports.passport_issue_date', $this->passport_issue_date);

        if ($this->passport_expiry_date !== FALSE)
            $this->db->where('safa_group_passports.passport_expiry_date', $this->passport_expiry_date);

        if ($this->passport_issuing_city !== FALSE)
            $this->db->where('safa_group_passports.passport_issuing_city', $this->passport_issuing_city);

        if ($this->passport_issuing_country_id !== FALSE)
            $this->db->where('safa_group_passports.passport_issuing_country_id', $this->passport_issuing_country_id);

        if ($this->passport_dpn_count !== FALSE)
            $this->db->where('safa_group_passports.passport_dpn_count', $this->passport_dpn_count);

        if ($this->dpn_serial_no !== FALSE)
            $this->db->where('safa_group_passports.dpn_serial_no', $this->dpn_serial_no);

        if ($this->relative_relation_id !== FALSE)
            $this->db->where('safa_group_passports.relative_relation_id', $this->relative_relation_id);

        if ($this->educational_level_id !== FALSE)
            $this->db->where('safa_group_passports.educational_level_id', $this->educational_level_id);

        if ($this->occupation !== FALSE)
            $this->db->where('safa_group_passports.occupation', $this->occupation);

        if ($this->email_address !== FALSE)
            $this->db->where('safa_group_passports.email_address', $this->email_address);

        if ($this->street_name !== FALSE)
            $this->db->where('safa_group_passports.street_name', $this->street_name);

        if ($this->date_of_birth !== FALSE)
            $this->db->where('safa_group_passports.date_of_birth', $this->date_of_birth);

        if ($this->age !== FALSE)
            $this->db->where('safa_group_passports.age', $this->age);

        if ($this->age_from !== FALSE)
            $this->db->where('safa_group_passports.age >= ' . $this->age_from);

        if ($this->age_to !== FALSE)
            $this->db->where('safa_group_passports.age <= ' . $this->age_to);

        if ($this->birth_city !== FALSE)
            $this->db->where('safa_group_passports.birth_city', $this->birth_city);

        if ($this->birth_country_id !== FALSE)
            $this->db->where('safa_group_passports.birth_country_id', $this->birth_country_id);

        if ($this->relative_no !== FALSE)
            $this->db->where('safa_group_passports.relative_no', $this->relative_no);

        if ($this->relative_gender_id !== FALSE)
            $this->db->where('safa_group_passports.relative_gender_id', $this->relative_gender_id);

        if ($this->gender_id !== FALSE)
            $this->db->where('safa_group_passports.gender_id', $this->gender_id);

        if ($this->marital_status_id !== FALSE)
            $this->db->where('safa_group_passports.marital_status_id', $this->marital_status_id);

        if ($this->religion !== FALSE)
            $this->db->where('safa_group_passports.religion', $this->religion);

        if ($this->zip_code !== FALSE)
            $this->db->where('safa_group_passports.zip_code', $this->zip_code);

        if ($this->city !== FALSE)
            $this->db->where('safa_group_passports.city', $this->city);

        if ($this->region_name !== FALSE)
            $this->db->where('safa_group_passports.region_name', $this->region_name);

        if ($this->erp_country_id !== FALSE)
            $this->db->where('safa_group_passports.erp_country_id', $this->erp_country_id);

        if ($this->id_number !== FALSE)
            $this->db->where('safa_group_passports.id_number', $this->id_number);

        if ($this->job2 !== FALSE)
            $this->db->where('safa_group_passports.job2', $this->job2);

        if ($this->address !== FALSE)
            $this->db->where('safa_group_passports.address', $this->address);

        if ($this->remarks !== FALSE)
            $this->db->where('safa_group_passports.remarks', $this->remarks);

        if ($this->canceled !== FALSE)
            $this->db->where('safa_group_passports.canceled', $this->canceled);

        if ($this->mobile !== FALSE)
            $this->db->where('safa_group_passports.mobile', $this->mobile);

        if ($this->sim_serial !== FALSE)
            $this->db->where('safa_group_passports.sim_serial', $this->sim_serial);

        if ($this->notes !== FALSE)
            $this->db->where('safa_group_passports.notes', $this->notes);

        if ($this->entry_datetime !== FALSE)
            $this->db->where('safa_group_passports.entry_datetime', $this->entry_datetime);


        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);


        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $this->db->select("safa_group_passports.*");
        $this->db->join('safa_groups', 'safa_groups.safa_group_id = safa_group_passports.safa_group_id', 'left');
        $this->db->where('safa_groups.safa_ea_id', $this->ea_id); ///from session
        $this->db->order_by("safa_group_passports.display_order", "asc");
        $query = $this->db->get('safa_group_passports');

        //echo $query_text=$this->db->last_query(); //exit;


        if ($rows_no)
            return $query->num_rows();

        if ($this->safa_group_passport_id)
            return $query->row();
        else
            return $query->result();
    }

    function get_for_table($rows_no = FALSE) {
//        echo $this->first_name_ar.'///'.$this->third_name_ar;exit();
        if ($this->custom_select !== FALSE) {
            $this->db->select('safa_group_passport_id');
            $this->db->select($this->custom_select);
        }

        if ($this->safa_group_passport_id !== FALSE)
            $this->db->where('safa_group_passports.safa_group_passport_id', $this->safa_group_passport_id);

        if ($this->safa_group_id !== FALSE)
            $this->db->where('safa_group_passports.safa_group_id', $this->safa_group_id);

        if ($this->safa_trip_id !== FALSE)
            $this->db->where('safa_group_passports.safa_trip_id', $this->safa_trip_id);
        
        if ($this->safa_trip_internaltrip_id !== FALSE)
            $this->db->where('safa_group_passports.safa_trip_internaltrip_id', $this->safa_trip_internaltrip_id);
           
        if ($this->offline_id !== FALSE)
            $this->db->where('safa_group_passports.offline_id', $this->offline_id);

        if ($this->no !== FALSE)
            $this->db->where('safa_group_passports.no', $this->no);

        if ($this->display_order !== FALSE)
            $this->db->where('safa_group_passports.display_order', $this->display_order);

        if ($this->title_id !== FALSE)
            $this->db->where('safa_group_passports.title_id', $this->title_id);

        if ($this->first_name_la !== false)
            $this->db->like('safa_group_passports.first_name_la', $this->first_name_la, 'none');


        if ($this->second_name_la !== false)
            $this->db->or_like('safa_group_passports.second_name_la', $this->second_name_la, 'none');

        if ($this->third_name_la !== false)
            $this->db->or_like('safa_group_passports.third_name_la', $this->third_name_la, 'none');

        if ($this->fourth_name_la !== false)
            $this->db->or_like('safa_group_passports.fourth_name_la', $this->fourth_name_la, 'none');

        if ($this->first_name_ar !== false)
            $this->db->like('safa_group_passports.first_name_ar', $this->first_name_ar, 'none');

        if ($this->second_name_ar !== false)
            $this->db->or_like('safa_group_passports.second_name_ar', $this->second_name_ar, 'none');

        if ($this->third_name_ar !== false)
            $this->db->or_like('safa_group_passports.third_name_ar', $this->third_name_ar, 'none');

        if ($this->fourth_name_ar !== false)
            $this->db->or_like('safa_group_passports.fourth_name_ar', $this->fourth_name_ar, 'none');

        if ($this->picture !== FALSE)
            $this->db->where('safa_group_passports.picture', $this->picture);

        if ($this->nationality_id !== FALSE)
            $this->db->where('safa_group_passports.nationality_id', $this->nationality_id);

        if ($this->previous_nationality_id !== FALSE)
            $this->db->where('safa_group_passports.previous_nationality_id', $this->previous_nationality_id);

        if ($this->passport_no !== FALSE)
            $this->db->where('safa_group_passports.passport_no', $this->passport_no);

        if ($this->passport_type_id !== FALSE)
            $this->db->where('safa_group_passports.passport_type_id', $this->passport_type_id);

        if ($this->passport_issue_date !== FALSE)
            $this->db->where('safa_group_passports.passport_issue_date', $this->passport_issue_date);

        if ($this->passport_expiry_date !== FALSE)
            $this->db->where('safa_group_passports.passport_expiry_date', $this->passport_expiry_date);

        if ($this->passport_issuing_city !== FALSE)
            $this->db->where('safa_group_passports.passport_issuing_city', $this->passport_issuing_city);

        if ($this->passport_issuing_country_id !== FALSE)
            $this->db->where('safa_group_passports.passport_issuing_country_id', $this->passport_issuing_country_id);

        if ($this->passport_dpn_count !== FALSE)
            $this->db->where('safa_group_passports.passport_dpn_count', $this->passport_dpn_count);

        if ($this->dpn_serial_no !== FALSE)
            $this->db->where('safa_group_passports.dpn_serial_no', $this->dpn_serial_no);

        if ($this->relative_relation_id !== FALSE)
            $this->db->where('safa_group_passports.relative_relation_id', $this->relative_relation_id);

        if ($this->educational_level_id !== FALSE)
            $this->db->where('safa_group_passports.educational_level_id', $this->educational_level_id);

        if ($this->occupation !== FALSE)
            $this->db->where('safa_group_passports.occupation', $this->occupation);

        if ($this->email_address !== FALSE)
            $this->db->where('safa_group_passports.email_address', $this->email_address);

        if ($this->street_name !== FALSE)
            $this->db->where('safa_group_passports.street_name', $this->street_name);

        if ($this->date_of_birth !== FALSE)
            $this->db->where('safa_group_passports.date_of_birth', $this->date_of_birth);

        if ($this->age !== FALSE)
            $this->db->where('safa_group_passports.age', $this->age);

        if ($this->age_from !== FALSE)
            $this->db->where('safa_group_passports.age >= ' . $this->age_from);

        if ($this->age_to !== FALSE)
            $this->db->where('safa_group_passports.age <= ' . $this->age_to);

        if ($this->birth_city !== FALSE)
            $this->db->where('safa_group_passports.birth_city', $this->birth_city);

        if ($this->birth_country_id !== FALSE)
            $this->db->where('safa_group_passports.birth_country_id', $this->birth_country_id);

        if ($this->relative_no !== FALSE)
            $this->db->where('safa_group_passports.relative_no', $this->relative_no);

        if ($this->relative_gender_id !== FALSE)
            $this->db->where('safa_group_passports.relative_gender_id', $this->relative_gender_id);

        if ($this->gender_id !== FALSE)
            $this->db->where('safa_group_passports.gender_id', $this->gender_id);

        if ($this->marital_status_id !== FALSE)
            $this->db->where('safa_group_passports.marital_status_id', $this->marital_status_id);

        if ($this->religion !== FALSE)
            $this->db->where('safa_group_passports.religion', $this->religion);

        if ($this->zip_code !== FALSE)
            $this->db->where('safa_group_passports.zip_code', $this->zip_code);

        if ($this->city !== FALSE)
            $this->db->where('safa_group_passports.city', $this->city);

        if ($this->region_name !== FALSE)
            $this->db->where('safa_group_passports.region_name', $this->region_name);

        if ($this->erp_country_id !== FALSE)
            $this->db->where('safa_group_passports.erp_country_id', $this->erp_country_id);

        if ($this->id_number !== FALSE)
            $this->db->where('safa_group_passports.id_number', $this->id_number);

        if ($this->job2 !== FALSE)
            $this->db->where('safa_group_passports.job2', $this->job2);

        if ($this->address !== FALSE)
            $this->db->where('safa_group_passports.address', $this->address);

        if ($this->remarks !== FALSE)
            $this->db->where('safa_group_passports.remarks', $this->remarks);

        if ($this->canceled !== FALSE)
            $this->db->where('safa_group_passports.canceled', $this->canceled);

        if ($this->mobile !== FALSE)
            $this->db->where('safa_group_passports.mobile', $this->mobile);

        if ($this->sim_serial !== FALSE)
            $this->db->where('safa_group_passports.sim_serial', $this->sim_serial);

        if ($this->notes !== FALSE)
            $this->db->where('safa_group_passports.notes', $this->notes);

        if ($this->entry_datetime !== FALSE)
            $this->db->where('safa_group_passports.entry_datetime', $this->entry_datetime);

        if ($this->where_condition !== FALSE)
            $this->db->where($this->where_condition);
       
        if ($this->safa_group_passport_id_arr !== FALSE)
            $this->db->where_in('safa_group_passports.safa_group_passport_id', $this->safa_group_passport_id_arr);
         

        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);


        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

            
        $this->db->distinct();    
        $this->db->select('safa_group_passports.*, safa_groups.name as safa_groups_name, CONCAT( first_name_ar, " ", second_name_ar, " ", third_name_ar, " ", fourth_name_ar ) as full_name_ar, CONCAT( first_name_la, " ", second_name_la, " ", third_name_la, " ", fourth_name_la ) as full_name_la,   
        CONCAT( first_name_ar, " ", fourth_name_ar ) as first_fourth_name_ar, CONCAT( first_name_la, " ", fourth_name_la ) as first_fourth_name_la, erp_passporttypes.' . name() . ' as erp_passporttypes_name, 
        erp_countries.' . name() . ' as erp_countries_name, erp_nationalities.' . name() . ' as erp_nationalities_name, 
        issuing_country.' . name() . ' as issuing_country_name, birth_country.' . name() . ' as birth_country_name, erp_relations.' . name() . ' as erp_relations_name, 

        erp_maritalstatus.' . name() . ' as erp_maritalstatus_name, erp_educationlevels.' . name() . ' as erp_educationlevels_name, erp_gender.' . name() . ' as erp_gender_name,
        (SELECT DATE_FORMAT(NOW(), "%Y") - DATE_FORMAT(date_of_birth, "%Y") - (DATE_FORMAT(NOW(), "00-%m-%d") < DATE_FORMAT(date_of_birth, "00-%m-%d"))) AS current_age
        
        ', FALSE);

        $this->db->join('safa_groups', 'safa_groups.safa_group_id = safa_group_passports.safa_group_id', 'left');

        $this->db->join('erp_passporttypes', 'safa_group_passports.passport_type_id = erp_passporttypes.erp_passporttype_id', 'left');
        $this->db->join('erp_countries', 'safa_group_passports.erp_country_id = erp_countries.erp_country_id', 'left');
        $this->db->join('erp_nationalities', 'safa_group_passports.nationality_id = erp_nationalities.erp_nationality_id', 'left');
        $this->db->join('erp_countries as issuing_country', 'safa_group_passports.passport_issuing_country_id =issuing_country.erp_country_id', 'left');
        $this->db->join('erp_countries as birth_country', 'safa_group_passports.birth_country_id =birth_country.erp_country_id', 'left');

        $this->db->join('erp_relations', 'safa_group_passports.relative_relation_id = erp_relations.erp_relation_id', 'left');
        $this->db->join('erp_maritalstatus', 'safa_group_passports.marital_status_id = erp_maritalstatus.erp_maritalstatus_id', 'left');
        $this->db->join('erp_educationlevels', 'safa_group_passports.educational_level_id = erp_educationlevels.erp_educationlevel_id', 'left');
		$this->db->join('erp_gender', 'erp_gender.erp_gender_id = safa_group_passports.gender_id', 'left');
        

		
		if ($this->safa_uo_id !== FALSE) {
			
			$this->db->join('safa_group_passport_accommodation', 'safa_group_passport_accommodation.safa_group_passport_id = safa_group_passports.safa_group_passport_id', 'left');
			$this->db->join('safa_package_periods', 'safa_package_periods.safa_package_periods_id = safa_group_passport_accommodation.safa_package_periods_id', 'left');
			$this->db->join('safa_packages', 'safa_packages.safa_package_id = safa_package_periods.safa_package_id', 'left');
		}
		
		if ($this->safa_uo_id !== FALSE) {
			$this->db ->where('safa_packages.erp_company_type_id', 2);
			$this->db ->where('safa_packages.erp_company_id',$this->safa_uo_id);
		}
    
		if($this->ea_id) {
        	$this->db->where('safa_groups.safa_ea_id', $this->ea_id); ///from session
		}
		
        $this->db->order_by("safa_group_passports.display_order", "asc");
        $query = $this->db->get('safa_group_passports');

        //echo $query_text=$this->db->last_query(); exit;


        if ($rows_no)
            return $query->num_rows();

        if ($this->safa_group_passport_id)
            return $query->row();
        else
            return $query->result();
    }

    
	function get_for_accomedation($rows_no = FALSE) {
//        echo $this->first_name_ar.'///'.$this->third_name_ar;exit();
        if ($this->custom_select !== FALSE) {
            $this->db->select('safa_group_passport_id');
            $this->db->select($this->custom_select);
        }

        if ($this->safa_group_passport_id !== FALSE)
            $this->db->where('safa_group_passports.safa_group_passport_id', $this->safa_group_passport_id);

        if ($this->safa_group_id !== FALSE)
            $this->db->where('safa_group_passports.safa_group_id', $this->safa_group_id);

        if ($this->safa_trip_id !== FALSE)
            $this->db->where('safa_reservation_forms.safa_trip_id', $this->safa_trip_id);
            
        if ($this->safa_trip_internaltrip_id !== FALSE)
            $this->db->where('safa_group_passports.safa_trip_internaltrip_id', $this->safa_trip_internaltrip_id);
       
        if ($this->offline_id !== FALSE)
            $this->db->where('safa_group_passports.offline_id', $this->offline_id);

        if ($this->no !== FALSE)
            $this->db->where('safa_group_passports.no', $this->no);

        if ($this->display_order !== FALSE)
            $this->db->where('safa_group_passports.display_order', $this->display_order);

        if ($this->title_id !== FALSE)
            $this->db->where('safa_group_passports.title_id', $this->title_id);

        if ($this->first_name_la !== false)
            $this->db->like('safa_group_passports.first_name_la', $this->first_name_la, 'none');


        if ($this->second_name_la !== false)
            $this->db->or_like('safa_group_passports.second_name_la', $this->second_name_la, 'none');

        if ($this->third_name_la !== false)
            $this->db->or_like('safa_group_passports.third_name_la', $this->third_name_la, 'none');

        if ($this->fourth_name_la !== false)
            $this->db->or_like('safa_group_passports.fourth_name_la', $this->fourth_name_la, 'none');

        if ($this->first_name_ar !== false)
            $this->db->like('safa_group_passports.first_name_ar', $this->first_name_ar, 'none');

        if ($this->second_name_ar !== false)
            $this->db->or_like('safa_group_passports.second_name_ar', $this->second_name_ar, 'none');

        if ($this->third_name_ar !== false)
            $this->db->or_like('safa_group_passports.third_name_ar', $this->third_name_ar, 'none');

        if ($this->fourth_name_ar !== false)
            $this->db->or_like('safa_group_passports.fourth_name_ar', $this->fourth_name_ar, 'none');

        if ($this->picture !== FALSE)
            $this->db->where('safa_group_passports.picture', $this->picture);

        if ($this->nationality_id !== FALSE)
            $this->db->where('safa_group_passports.nationality_id', $this->nationality_id);

        if ($this->previous_nationality_id !== FALSE)
            $this->db->where('safa_group_passports.previous_nationality_id', $this->previous_nationality_id);

        if ($this->passport_no !== FALSE)
            $this->db->where('safa_group_passports.passport_no', $this->passport_no);

        if ($this->passport_type_id !== FALSE)
            $this->db->where('safa_group_passports.passport_type_id', $this->passport_type_id);

        if ($this->passport_issue_date !== FALSE)
            $this->db->where('safa_group_passports.passport_issue_date', $this->passport_issue_date);

        if ($this->passport_expiry_date !== FALSE)
            $this->db->where('safa_group_passports.passport_expiry_date', $this->passport_expiry_date);

        if ($this->passport_issuing_city !== FALSE)
            $this->db->where('safa_group_passports.passport_issuing_city', $this->passport_issuing_city);

        if ($this->passport_issuing_country_id !== FALSE)
            $this->db->where('safa_group_passports.passport_issuing_country_id', $this->passport_issuing_country_id);

        if ($this->passport_dpn_count !== FALSE)
            $this->db->where('safa_group_passports.passport_dpn_count', $this->passport_dpn_count);

        if ($this->dpn_serial_no !== FALSE)
            $this->db->where('safa_group_passports.dpn_serial_no', $this->dpn_serial_no);

        if ($this->relative_relation_id !== FALSE)
            $this->db->where('safa_group_passports.relative_relation_id', $this->relative_relation_id);

        if ($this->educational_level_id !== FALSE)
            $this->db->where('safa_group_passports.educational_level_id', $this->educational_level_id);

        if ($this->occupation !== FALSE)
            $this->db->where('safa_group_passports.occupation', $this->occupation);

        if ($this->email_address !== FALSE)
            $this->db->where('safa_group_passports.email_address', $this->email_address);

        if ($this->street_name !== FALSE)
            $this->db->where('safa_group_passports.street_name', $this->street_name);

        if ($this->date_of_birth !== FALSE)
            $this->db->where('safa_group_passports.date_of_birth', $this->date_of_birth);

        if ($this->age !== FALSE)
            $this->db->where('safa_group_passports.age', $this->age);

        if ($this->age_from !== FALSE)
            $this->db->where('safa_group_passports.age >= ' . $this->age_from);

        if ($this->age_to !== FALSE)
            $this->db->where('safa_group_passports.age <= ' . $this->age_to);

        if ($this->birth_city !== FALSE)
            $this->db->where('safa_group_passports.birth_city', $this->birth_city);

        if ($this->birth_country_id !== FALSE)
            $this->db->where('safa_group_passports.birth_country_id', $this->birth_country_id);

        if ($this->relative_no !== FALSE)
            $this->db->where('safa_group_passports.relative_no', $this->relative_no);

        if ($this->relative_gender_id !== FALSE)
            $this->db->where('safa_group_passports.relative_gender_id', $this->relative_gender_id);

        if ($this->gender_id !== FALSE)
            $this->db->where('safa_group_passports.gender_id', $this->gender_id);

        if ($this->marital_status_id !== FALSE)
            $this->db->where('safa_group_passports.marital_status_id', $this->marital_status_id);

        if ($this->religion !== FALSE)
            $this->db->where('safa_group_passports.religion', $this->religion);

        if ($this->zip_code !== FALSE)
            $this->db->where('safa_group_passports.zip_code', $this->zip_code);

        if ($this->city !== FALSE)
            $this->db->where('safa_group_passports.city', $this->city);

        if ($this->region_name !== FALSE)
            $this->db->where('safa_group_passports.region_name', $this->region_name);

        if ($this->erp_country_id !== FALSE)
            $this->db->where('safa_group_passports.erp_country_id', $this->erp_country_id);

        if ($this->id_number !== FALSE)
            $this->db->where('safa_group_passports.id_number', $this->id_number);

        if ($this->job2 !== FALSE)
            $this->db->where('safa_group_passports.job2', $this->job2);

        if ($this->address !== FALSE)
            $this->db->where('safa_group_passports.address', $this->address);

        if ($this->remarks !== FALSE)
            $this->db->where('safa_group_passports.remarks', $this->remarks);

        if ($this->canceled !== FALSE)
            $this->db->where('safa_group_passports.canceled', $this->canceled);

        if ($this->mobile !== FALSE)
            $this->db->where('safa_group_passports.mobile', $this->mobile);

        if ($this->sim_serial !== FALSE)
            $this->db->where('safa_group_passports.sim_serial', $this->sim_serial);

        if ($this->notes !== FALSE)
            $this->db->where('safa_group_passports.notes', $this->notes);

        if ($this->entry_datetime !== FALSE)
            $this->db->where('safa_group_passports.entry_datetime', $this->entry_datetime);

		if ($this->where_condition !== FALSE)
            $this->db->where($this->where_condition);
       
        if ($this->safa_group_passport_id_arr !== FALSE)
            $this->db->where_in('safa_group_passports.safa_group_passport_id', $this->safa_group_passport_id_arr);
            
            
        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);


        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

            
        $this->db->distinct();    
        $this->db->select('safa_group_passports.*,  safa_groups.name as safa_groups_name, CONCAT( first_name_ar, " ", second_name_ar, " ", third_name_ar, " ", fourth_name_ar ) as full_name_ar, CONCAT( first_name_la, " ", second_name_la, " ", third_name_la, " ", fourth_name_la ) as full_name_la,   
        CONCAT( first_name_ar, " ", fourth_name_ar ) as first_fourth_name_ar, CONCAT( first_name_la, " ", fourth_name_la ) as first_fourth_name_la, erp_passporttypes.' . name() . ' as erp_passporttypes_name, 
        erp_countries.' . name() . ' as erp_countries_name, erp_nationalities.' . name() . ' as erp_nationalities_name, 
        issuing_country.' . name() . ' as issuing_country_name, birth_country.' . name() . ' as birth_country_name, erp_relations.' . name() . ' as erp_relations_name, 

        erp_maritalstatus.' . name() . ' as erp_maritalstatus_name, erp_educationlevels.' . name() . ' as erp_educationlevels_name, erp_gender.' . name() . ' as erp_gender_name,
        erp_hotelroomsizes.' . name() . ' as erp_hotelroomsize_name,
        erp_hotels.' . name() . ' as erp_hotel_name,
        erp_cities.' . name() . ' as erp_city_name,
        (select sum(city_days) from erp_package_periods_cities where safa_package_periods.erp_package_period_id = erp_package_periods_cities.erp_package_period_id) as total_package_period_days,
        (SELECT DATE_FORMAT(NOW(), "%Y") - DATE_FORMAT(date_of_birth, "%Y") - (DATE_FORMAT(NOW(), "00-%m-%d") < DATE_FORMAT(date_of_birth, "00-%m-%d"))) AS current_age
        ', FALSE);

        $this->db->join('safa_groups', 'safa_groups.safa_group_id = safa_group_passports.safa_group_id', 'left');

        $this->db->join('erp_passporttypes', 'safa_group_passports.passport_type_id = erp_passporttypes.erp_passporttype_id', 'left');
        $this->db->join('erp_countries', 'safa_group_passports.erp_country_id = erp_countries.erp_country_id', 'left');
        $this->db->join('erp_nationalities', 'safa_group_passports.nationality_id = erp_nationalities.erp_nationality_id', 'left');
        $this->db->join('erp_countries as issuing_country', 'safa_group_passports.passport_issuing_country_id =issuing_country.erp_country_id', 'left');
        $this->db->join('erp_countries as birth_country', 'safa_group_passports.birth_country_id =birth_country.erp_country_id', 'left');

        $this->db->join('erp_relations', 'safa_group_passports.relative_relation_id = erp_relations.erp_relation_id', 'left');
        $this->db->join('erp_maritalstatus', 'safa_group_passports.marital_status_id = erp_maritalstatus.erp_maritalstatus_id', 'left');
        $this->db->join('erp_educationlevels', 'safa_group_passports.educational_level_id = erp_educationlevels.erp_educationlevel_id', 'left');
		$this->db->join('erp_gender', 'erp_gender.erp_gender_id = safa_group_passports.gender_id', 'left');
        

		$this->db->join('safa_group_passport_accommodation', 'safa_group_passport_accommodation.safa_group_passport_id = safa_group_passports.safa_group_passport_id', 'left');
		$this->db->join('safa_passport_accommodation_rooms', 'safa_passport_accommodation_rooms.safa_passport_accommodation_room_id = safa_group_passport_accommodation.safa_passport_accommodation_room_id', 'left');
		$this->db->join('safa_reservation_forms', 'safa_reservation_forms.safa_reservation_form_id = safa_passport_accommodation_rooms.safa_reservation_form_id', 'left');
		
		$this->db->join('erp_hotelroomsizes', 'erp_hotelroomsizes.erp_hotelroomsize_id = safa_passport_accommodation_rooms.erp_hotelroomsize_id', 'left');
		$this->db->join('erp_hotels', 'erp_hotels.erp_hotel_id = safa_passport_accommodation_rooms.erp_hotel_id', 'left');
		$this->db->join('erp_cities', 'erp_cities.erp_city_id = erp_hotels.erp_city_id', 'left');
		
		$this->db->join('safa_package_periods', 'safa_package_periods.safa_package_periods_id = safa_group_passport_accommodation.safa_package_periods_id', 'left');
		$this->db->join('safa_packages', 'safa_packages.safa_package_id = safa_package_periods.safa_package_id', 'left');
		
		
		if ($this->safa_uo_id !== FALSE) {
			$this->db ->where('safa_packages.erp_company_type_id', 2);
			$this->db ->where('safa_packages.erp_company_id',$this->safa_uo_id);
		}
    
		if($this->ea_id) {
        	$this->db->where('safa_groups.safa_ea_id', $this->ea_id); ///from session
		}
		
        $this->db->order_by("safa_group_passports.safa_group_passport_id", "asc");
        $query = $this->db->get('safa_group_passports');

        //echo $query_text=$this->db->last_query(); exit;


        if ($rows_no)
            return $query->num_rows();

        if ($this->safa_group_passport_id)
            return $query->row();
        else
            return $query->result();
    }
    
    function save() {
        if ($this->safa_group_passport_id !== FALSE)
            $this->db->set('safa_group_passports.safa_group_passport_id', $this->safa_group_passport_id);

        if ($this->safa_group_id !== FALSE)
            $this->db->set('safa_group_passports.safa_group_id', $this->safa_group_id);

        if ($this->safa_trip_id !== FALSE)
            $this->db->set('safa_group_passports.safa_trip_id', $this->safa_trip_id);

        if ($this->safa_trip_internaltrip_id !== FALSE)
            $this->db->set('safa_group_passports.safa_trip_internaltrip_id', $this->safa_trip_internaltrip_id);
       
        if ($this->offline_id !== FALSE)
            $this->db->set('safa_group_passports.offline_id', $this->offline_id);

        if ($this->no !== FALSE)
            $this->db->set('safa_group_passports.no', $this->no);

        if ($this->display_order !== FALSE)
            $this->db->set('safa_group_passports.display_order', $this->display_order);

        if ($this->title_id !== FALSE)
            $this->db->set('safa_group_passports.title_id', $this->title_id);

        if ($this->first_name_la !== FALSE)
            $this->db->set('safa_group_passports.first_name_la', $this->first_name_la);

        if ($this->second_name_la !== FALSE)
            $this->db->set('safa_group_passports.second_name_la', $this->second_name_la);

        if ($this->third_name_la !== FALSE)
            $this->db->set('safa_group_passports.third_name_la', $this->third_name_la);

        if ($this->fourth_name_la !== FALSE)
            $this->db->set('safa_group_passports.fourth_name_la', $this->fourth_name_la);

        if ($this->first_name_ar !== FALSE)
            $this->db->set('safa_group_passports.first_name_ar', $this->first_name_ar);

        if ($this->second_name_ar !== FALSE)
            $this->db->set('safa_group_passports.second_name_ar', $this->second_name_ar);

        if ($this->third_name_ar !== FALSE)
            $this->db->set('safa_group_passports.third_name_ar', $this->third_name_ar);

        if ($this->fourth_name_ar !== FALSE)
            $this->db->set('safa_group_passports.fourth_name_ar', $this->fourth_name_ar);

        if ($this->picture !== FALSE)
            $this->db->set('safa_group_passports.picture', $this->picture);

        if ($this->nationality_id !== FALSE)
            $this->db->set('safa_group_passports.nationality_id', $this->nationality_id);

        if ($this->previous_nationality_id !== FALSE)
            $this->db->set('safa_group_passports.previous_nationality_id', $this->previous_nationality_id);

        if ($this->passport_no !== FALSE)
            $this->db->set('safa_group_passports.passport_no', $this->passport_no);

        if ($this->passport_type_id !== FALSE)
            $this->db->set('safa_group_passports.passport_type_id', $this->passport_type_id);

        if ($this->passport_issue_date !== FALSE)
            $this->db->set('safa_group_passports.passport_issue_date', $this->passport_issue_date);

        if ($this->passport_expiry_date !== FALSE)
            $this->db->set('safa_group_passports.passport_expiry_date', $this->passport_expiry_date);

        if ($this->passport_issuing_city !== FALSE)
            $this->db->set('safa_group_passports.passport_issuing_city', $this->passport_issuing_city);

        if ($this->passport_issuing_country_id !== FALSE)
            $this->db->set('safa_group_passports.passport_issuing_country_id', $this->passport_issuing_country_id);

        if ($this->passport_dpn_count !== FALSE)
            $this->db->set('safa_group_passports.passport_dpn_count', $this->passport_dpn_count);

        if ($this->dpn_serial_no !== FALSE)
            $this->db->set('safa_group_passports.dpn_serial_no', $this->dpn_serial_no);

        if ($this->relative_relation_id !== FALSE)
            $this->db->set('safa_group_passports.relative_relation_id', $this->relative_relation_id);

        if ($this->educational_level_id !== FALSE)
            $this->db->set('safa_group_passports.educational_level_id', $this->educational_level_id);

        if ($this->occupation !== FALSE)
            $this->db->set('safa_group_passports.occupation', $this->occupation);

        if ($this->email_address !== FALSE)
            $this->db->set('safa_group_passports.email_address', $this->email_address);

        if ($this->street_name !== FALSE)
            $this->db->set('safa_group_passports.street_name', $this->street_name);

        if ($this->date_of_birth !== FALSE)
            $this->db->set('safa_group_passports.date_of_birth', $this->date_of_birth);

        if ($this->age !== FALSE)
            $this->db->set('safa_group_passports.age', $this->age);

        if ($this->birth_city !== FALSE)
            $this->db->set('safa_group_passports.birth_city', $this->birth_city);

        if ($this->birth_country_id !== FALSE)
            $this->db->set('safa_group_passports.birth_country_id', $this->birth_country_id);

        if ($this->relative_no !== FALSE)
            $this->db->set('safa_group_passports.relative_no', $this->relative_no);

        if ($this->relative_gender_id !== FALSE)
            $this->db->set('safa_group_passports.relative_gender_id', $this->relative_gender_id);

        if ($this->gender_id !== FALSE)
            $this->db->set('safa_group_passports.gender_id', $this->gender_id);

        if ($this->marital_status_id !== FALSE)
            $this->db->set('safa_group_passports.marital_status_id', $this->marital_status_id);

        if ($this->religion !== FALSE)
            $this->db->set('safa_group_passports.religion', $this->religion);

        if ($this->zip_code !== FALSE)
            $this->db->set('safa_group_passports.zip_code', $this->zip_code);

        if ($this->city !== FALSE)
            $this->db->set('safa_group_passports.city', $this->city);

        if ($this->region_name !== FALSE)
            $this->db->set('safa_group_passports.region_name', $this->region_name);

        if ($this->erp_country_id !== FALSE)
            $this->db->set('safa_group_passports.erp_country_id', $this->erp_country_id);

        if ($this->id_number !== FALSE)
            $this->db->set('safa_group_passports.id_number', $this->id_number);

        if ($this->job2 !== FALSE)
            $this->db->set('safa_group_passports.job2', $this->job2);

        if ($this->address !== FALSE)
            $this->db->set('safa_group_passports.address', $this->address);

        if ($this->remarks !== FALSE)
            $this->db->set('safa_group_passports.remarks', $this->remarks);

        if ($this->canceled !== FALSE)
            $this->db->set('safa_group_passports.canceled', $this->canceled);

        if ($this->mobile !== FALSE)
            $this->db->set('safa_group_passports.mobile', $this->mobile);

        if ($this->sim_serial !== FALSE)
            $this->db->set('safa_group_passports.sim_serial', $this->sim_serial);

        if ($this->notes !== FALSE)
            $this->db->set('safa_group_passports.notes', $this->notes);

        if ($this->entry_datetime !== FALSE)
            $this->db->set('safa_group_passports.entry_datetime', $this->entry_datetime);

        if ($this->safa_group_passport_id) {
            $this->db->where('safa_group_passports.safa_group_passport_id', $this->safa_group_passport_id)->update('safa_group_passports');
        } else {
            $this->db->insert('safa_group_passports');
            return $this->db->insert_id();
        }
        
        //echo $this->db->last_query(); //exit;
    }

    function delete() {

        if ($this->safa_group_passport_id !== FALSE)
            $this->db->where('safa_group_passports.safa_group_passport_id', $this->safa_group_passport_id);

        $this->db->delete('safa_group_passports');

        return $this->db->affected_rows();
    }

    function delete_by_relative_no($relative_no = 0) {
        $this->db->where('safa_group_passports.relative_no', $relative_no);
        $this->db->delete('safa_group_passports');
        return $this->db->affected_rows();
    }

    function search($rows_no = FALSE) {


        if ($this->custom_select !== FALSE) {
            $this->db->select('safa_group_passport_id');
            $this->db->select($this->custom_select);
        }

        if ($this->safa_group_passport_id !== FALSE)
            $this->db->where('safa_group_passports.safa_group_passport_id', $this->safa_group_passport_id);

        if ($this->safa_group_id !== FALSE)
            $this->db->where('safa_group_passports.safa_group_id', $this->safa_group_id);

        if ($this->offline_id !== FALSE)
            $this->db->where('safa_group_passports.offline_id', $this->offline_id);

        if ($this->no !== FALSE)
            $this->db->where('safa_group_passports.no', $this->no);

        if ($this->display_order !== FALSE)
            $this->db->where('safa_group_passports.display_order', $this->display_order);

        if ($this->title_id !== FALSE)
            $this->db->where('safa_group_passports.title_id', $this->title_id);

        if ($this->first_name_la !== FALSE)
            $this->db->like('safa_group_passports.first_name_la', $this->first_name_la, 'none');

        if ($this->second_name_la !== FALSE)
            $this->db->or_like('safa_group_passports.second_name_la', $this->second_name_la, 'none');

        if ($this->third_name_la !== FALSE)
            $this->db->or_like('safa_group_passports.third_name_la', $this->third_name_la, 'none');

        if ($this->fourth_name_la !== FALSE)
            $this->db->or_like('safa_group_passports.fourth_name_la', $this->fourth_name_la, 'none');

        if ($this->first_name_ar !== FALSE)
            $this->db->like('safa_group_passports.first_name_ar', $this->first_name_ar, 'none');

        if ($this->second_name_ar !== FALSE)
            $this->db->or_like('safa_group_passports.second_name_ar', $this->second_name_ar, 'none');

        if ($this->third_name_ar !== FALSE)
            $this->db->or_like('safa_group_passports.third_name_ar', $this->third_name_ar, 'none');

        if ($this->fourth_name_ar !== FALSE)
            $this->db->or_like('safa_group_passports.fourth_name_ar', $this->fourth_name_ar, 'none');

        if ($this->picture !== FALSE)
            $this->db->where('safa_group_passports.picture', $this->picture);

        if ($this->nationality_id !== FALSE)
            $this->db->where('safa_group_passports.nationality_id', $this->nationality_id);

        if ($this->previous_nationality_id !== FALSE)
            $this->db->where('safa_group_passports.previous_nationality_id', $this->previous_nationality_id);

        if ($this->passport_no !== FALSE)
            $this->db->where('safa_group_passports.passport_no', $this->passport_no);

        if ($this->passport_type_id !== FALSE)
            $this->db->where('safa_group_passports.passport_type_id', $this->passport_type_id);

        if ($this->passport_issue_date !== FALSE)
            $this->db->where('safa_group_passports.passport_issue_date', $this->passport_issue_date);

        if ($this->passport_expiry_date !== FALSE)
            $this->db->where('safa_group_passports.passport_expiry_date', $this->passport_expiry_date);

        if ($this->passport_issuing_city !== FALSE)
            $this->db->where('safa_group_passports.passport_issuing_city', $this->passport_issuing_city);

        if ($this->passport_issuing_country_id !== FALSE)
            $this->db->where('safa_group_passports.passport_issuing_country_id', $this->passport_issuing_country_id);

        if ($this->passport_dpn_count !== FALSE)
            $this->db->where('safa_group_passports.passport_dpn_count', $this->passport_dpn_count);

        if ($this->dpn_serial_no !== FALSE)
            $this->db->where('safa_group_passports.dpn_serial_no', $this->dpn_serial_no);

        if ($this->relative_relation_id !== FALSE)
            $this->db->where('safa_group_passports.relative_relation_id', $this->relative_relation_id);

        if ($this->educational_level_id !== FALSE)
            $this->db->where('safa_group_passports.educational_level_id', $this->educational_level_id);

        if ($this->occupation !== FALSE)
            $this->db->where('safa_group_passports.occupation', $this->occupation);

        if ($this->email_address !== FALSE)
            $this->db->where('safa_group_passports.email_address', $this->email_address);

        if ($this->street_name !== FALSE)
            $this->db->where('safa_group_passports.street_name', $this->street_name);

        if ($this->date_of_birth !== FALSE)
            $this->db->where('safa_group_passports.date_of_birth', $this->date_of_birth);

        if ($this->age !== FALSE)
            $this->db->where('safa_group_passports.age', $this->age);

        if ($this->birth_city !== FALSE)
            $this->db->where('safa_group_passports.birth_city', $this->birth_city);

        if ($this->birth_country_id !== FALSE)
            $this->db->where('safa_group_passports.birth_country_id', $this->birth_country_id);

        if ($this->relative_no !== FALSE)
            $this->db->where('safa_group_passports.relative_no', $this->relative_no);

        if ($this->relative_gender_id !== FALSE)
            $this->db->where('safa_group_passports.relative_gender_id', $this->relative_gender_id);

        if ($this->gender_id !== FALSE)
            $this->db->where('safa_group_passports.gender_id', $this->gender_id);

        if ($this->marital_status_id !== FALSE)
            $this->db->where('safa_group_passports.marital_status_id', $this->marital_status_id);

        if ($this->religion !== FALSE)
            $this->db->where('safa_group_passports.religion', $this->religion);

        if ($this->zip_code !== FALSE)
            $this->db->where('safa_group_passports.zip_code', $this->zip_code);

        if ($this->city !== FALSE)
            $this->db->where('safa_group_passports.city', $this->city);

        if ($this->region_name !== FALSE)
            $this->db->where('safa_group_passports.region_name', $this->region_name);

        if ($this->erp_country_id !== FALSE)
            $this->db->where('safa_group_passports.erp_country_id', $this->erp_country_id);

        if ($this->id_number !== FALSE)
            $this->db->where('safa_group_passports.id_number', $this->id_number);

        if ($this->job2 !== FALSE)
            $this->db->where('safa_group_passports.job2', $this->job2);

        if ($this->address !== FALSE)
            $this->db->where('safa_group_passports.address', $this->address);

        if ($this->remarks !== FALSE)
            $this->db->where('safa_group_passports.remarks', $this->remarks);

        if ($this->canceled !== FALSE)
            $this->db->where('safa_group_passports.canceled', $this->canceled);

        if ($this->mobile !== FALSE)
            $this->db->where('safa_group_passports.mobile', $this->mobile);

        if ($this->sim_serial !== FALSE)
            $this->db->where('safa_group_passports.sim_serial', $this->sim_serial);

        if ($this->notes !== FALSE)
            $this->db->where('safa_group_passports.notes', $this->notes);

        if ($this->entry_datetime !== FALSE)
            $this->db->where('safa_group_passports.entry_datetime', $this->entry_datetime);


        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('safa_group_passports');
        if ($rows_no)
            return $query->num_rows();

        return $query->result();
    }

    function calc_next_no_field($safa_group_id, $i) {
        return $this->db->where("safa_group_id", $safa_group_id)->get('safa_group_passports')->num_rows() + $i;
    }

    function get_related_persons($id) {

        //$this->db->select('safa_trip_traveller_id,'.name());

        $query = "SELECT * FROM safa_trip_travellers WHERE safa_trip_id = $id";

        $relative_travellers = $this->db->query($query)->result_array();

        return $relative_travellers;
    }

    function get_ea_contracts() {
        $this->db->select("safa_uo_contracts.safa_uo_contract_id as contract_id,safa_uo_contracts." . name() . ",safa_uos." . name() . " as safa_uos_name,safa_uo_contracts_eas." . name() . " as safa_uo_contracts_eas_name");
        $this->db->from("safa_uo_contracts");
        $this->db->join("safa_uo_contracts_eas", "safa_uo_contracts_eas.safa_uo_contract_id=safa_uo_contracts.safa_uo_contract_id");
        $this->db->join("safa_uos", "safa_uos.safa_uo_id=safa_uo_contracts.safa_uo_id");
        $this->db->where("safa_uo_contracts_eas.safa_ea_id", $this->ea_id); //from a seasion;
        $this->db->where("safa_uo_contracts_eas.disabled=0");

        return $this->db->get()->result();
    }

    function get_safa_uo_contract_id($safa_umrahgroup_id) {
        $this->db->select('safa_uo_contracts.safa_uo_contract_id,safa_uo_contracts.' . name());
        $this->db->where('safa_umrahgroups.safa_umrahgroup_id', $safa_umrahgroup_id);
        $this->db->join("safa_umrahgroups", "safa_uo_contracts.safa_uo_contract_id=safa_umrahgroups.safa_uo_contract_id");
        return $this->db->get('safa_uo_contracts')->row();
    }

    //By Gouda
    function get_safa_umrahgroups($safa_uo_contract_id) {
        $this->db->select('safa_umrahgroups.safa_umrahgroup_id,safa_umrahgroups.name');
        //$this->db->join("safa_umrahgroups", "safa_uo_contracts.safa_uo_contract_id=safa_umrahgroups.safa_uo_contract_id");
        $this->db->where('safa_umrahgroups.safa_uo_contract_id', $safa_uo_contract_id);

        return $this->db->get('safa_umrahgroups')->result();

        //echo $query_text=$this->db->last_query();
    }

    function get_by_relative_no($relative_no = 0) {
        $this->db->select('safa_group_passports.*, safa_groups.name as safa_groups_name, CONCAT( first_name_ar, " ", second_name_ar, " ", third_name_ar, " ", fourth_name_ar ) as full_name_ar, CONCAT( first_name_la, " ", second_name_la, " ", third_name_la, " ", fourth_name_la ) as full_name_la,   
        erp_passporttypes.' . name() . ' as erp_passporttypes_name, 
        erp_countries.' . name() . ' as erp_countries_name, erp_nationalities.' . name() . ' as erp_nationalities_name, 
        issuing_country.' . name() . ' as issuing_country_name, birth_country.' . name() . ' as birth_country_name, erp_relations.' . name() . ' as erp_relations_name, 

        erp_maritalstatus.' . name() . ' as erp_maritalstatus_name, erp_educationlevels.' . name() . ' as erp_educationlevels_name, erp_gender.' . name() . ' as erp_gender_name ', FALSE);

        $this->db->join('safa_groups', 'safa_groups.safa_group_id = safa_group_passports.safa_group_id', 'left');

        $this->db->join('erp_passporttypes', 'safa_group_passports.passport_type_id = erp_passporttypes.erp_passporttype_id', 'left');
        $this->db->join('erp_countries', 'safa_group_passports.erp_country_id = erp_countries.erp_country_id', 'left');
        $this->db->join('erp_nationalities', 'safa_group_passports.nationality_id = erp_nationalities.erp_nationality_id', 'left');
        $this->db->join('erp_countries as issuing_country', 'safa_group_passports.passport_issuing_country_id =issuing_country.erp_country_id', 'left');
        $this->db->join('erp_countries as birth_country', 'safa_group_passports.birth_country_id =birth_country.erp_country_id', 'left');

        $this->db->join('erp_relations', 'safa_group_passports.relative_relation_id = erp_relations.erp_relation_id', 'left');
        $this->db->join('erp_maritalstatus', 'safa_group_passports.marital_status_id = erp_maritalstatus.erp_maritalstatus_id', 'left');
        $this->db->join('erp_educationlevels', 'safa_group_passports.educational_level_id = erp_educationlevels.erp_educationlevel_id', 'left');
        $this->db->join('erp_gender', 'safa_group_passports.gender_id = erp_gender.erp_gender_id', 'left');

        $this->db->where('safa_group_passports.relative_no', $relative_no);
        $query = $this->db->get('safa_group_passports');
        return $query->result();
    }

    function relative_no($passport) {
        $this->db->select('safa_group_passports.relative_no');
        $this->db->where('safa_group_passports.passport_no', $passport);
        return $this->db->get('safa_group_passports')->row();
    }

    function get_group_name($safa_group_id) {
        $this->db->select('safa_groups.name');
        $this->db->where('safa_groups.safa_group_id', $safa_group_id);
        return $this->db->get('safa_groups')->row();
    }

    function check_delete_dpncount_ability($id) {
        $this->db->select('safa_group_passports.passport_dpn_count,safa_group_passports.safa_group_id');
        $this->db->from('safa_group_passports');
        $this->db->where('safa_group_passports.safa_group_passport_id', $id);
        $row = $this->db->get()->row();
        $flag = 0;
        if (count($row) > 0) {
            if ($row->passport_dpn_count > 0)
                $flag = 1;
        }
        return $flag;
    }

    function check_umrah_passports_for_passport($id) {
        $this->db->select('count(*) as rows_count');
        $this->db->from('safa_umrahgroups_passports');
        $this->db->where('safa_umrahgroups_passports.safa_group_passport_id', $id);
        $row = $this->db->get()->row();

        if ($row->rows_count > 0) {
            return true;
        } else {
            return false;
        }
    }

    function check_trip_travellers_for_passport($id) {
        $this->db->select('count(*) as rows_count');
        $this->db->from('safa_trip_travellers');
        $this->db->where('safa_trip_travellers.safa_group_passport_id', $id);
        $row = $this->db->get()->row();

        if ($row->rows_count > 0) {
            return true;
        } else {
            return false;
        }
    }

    function check_delete_mehrem_ability($id) {
        $this->db->select('safa_group_passports.no,safa_group_passports.safa_group_passport_id, safa_group_passports.safa_group_id');
        $this->db->from('safa_group_passports');
        $this->db->where('safa_group_passports.safa_group_passport_id', $id);
        $row = $this->db->get()->row();
//        if ($row->no>0) {

        $relative_no = 0;
        $safa_group_id = 0;
        if (count($row) > 0) {
            $relative_no = $row->safa_group_passport_id;
            $safa_group_id = $row->safa_group_id;
        }

        $this->db->select('safa_group_passports.relative_no');
        $this->db->from('safa_group_passports');
        $this->db->where('safa_group_passports.relative_no', $relative_no);
        $this->db->where('safa_group_passports.relative_no!=', 0);
        $this->db->where('safa_group_passports.safa_group_id', $safa_group_id);
        $query = $this->db->get();
//        }
        $flag = 0;
//        print_r($query->result());die();
        foreach ($query->result() as $row => $table) {
            foreach ($table as $col => $value) {
                if ($value > 0) {
                    $flag = 1;
                    break;
                } else {
                    continue;
                }
            }
        }
        return $flag;
    }

    function get_his_mehrem_passport_no($id) {
        $this->db->select('safa_group_passports.relative_no,safa_group_passports.safa_group_id');
        $this->db->from('safa_group_passports');
        $this->db->where('safa_group_passports.safa_group_passport_id', $id);
        $query = $this->db->get()->row();
//        if ($query->relative_no>0) {
        $this->db->select('safa_group_passports.passport_no');
        $this->db->from('safa_group_passports');
        $this->db->where('safa_group_passports.safa_group_passport_id', $query->relative_no);
        $this->db->where('safa_group_passports.safa_group_id', $query->safa_group_id);
        return $this->db->get()->row();
//        }
//        else
//            return FALSE;
    }

    function get_mehrem_to_passports_no($id) {
        $this->db->select('safa_group_passports.no,safa_group_passports.safa_group_id');
        $this->db->from('safa_group_passports');
        $this->db->where('safa_group_passports.safa_group_passport_id', $id);
        $query = $this->db->get()->row();
        if ($query->no > 0) {
            $this->db->select('safa_group_passports.passport_no');
            $this->db->from('safa_group_passports');
            $this->db->where('safa_group_passports.relative_no', $query->no);
            $this->db->where('safa_group_passports.safa_group_id', $query->safa_group_id);
            return $this->db->get()->result();
        } else
            return FALSE;
    }

    function check_mutamer_dpn_serial_no($mutamer_id) {

        $this->db->select('safa_group_passports.passport_no,safa_group_passports.passport_dpn_count,
                safa_group_passports.safa_group_id,safa_group_passports.dpn_serial_no');
        $this->db->where('safa_group_passports.safa_group_passport_id', $mutamer_id);
        $row = $this->db->get('safa_group_passports')->row();
        $flag = 0;
        if (count($row) > 0) {
            if ($row->passport_dpn_count == 0 && $row->dpn_serial_no > 0)
                $flag = 1;
        }
        return $flag;
    }

    function update_dpn_count($mutamer_id) {
        $this->db->select('safa_group_passports.passport_no,safa_group_passports.passport_dpn_count,
                safa_group_passports.safa_group_id,safa_group_passports.dpn_serial_no');
        $this->db->where('safa_group_passports.safa_group_passport_id', $mutamer_id);
        $row = $this->db->get('safa_group_passports')->row();
        if ($row->passport_dpn_count == 0 && $row->dpn_serial_no > 0) {
            $this->db->select('safa_group_passports.safa_group_passport_id,safa_group_passports.passport_no,safa_group_passports.passport_dpn_count,safa_group_passports.dpn_serial_no');
            $this->db->where('safa_group_passports.safa_group_id', $row->safa_group_id);
            $this->db->where('safa_group_passports.passport_no', $row->passport_no);
            $this->db->where('safa_group_passports.dpn_serial_no', 0);
            $dpn_counts = $this->db->get('safa_group_passports')->row();
            if (count($dpn_counts) > 0) {
                $this->db->set('passport_dpn_count', $dpn_counts->passport_dpn_count - 1, FALSE);
            } else {
                $this->db->set('passport_dpn_count', 0, FALSE);
            }
            $this->db->where('passport_dpn_count !=', 0);

            //By Gouda
            $this->db->where('safa_group_passports.safa_group_id', $row->safa_group_id);
            $this->db->where('safa_group_passports.passport_no', $row->passport_no);
            $this->db->where('safa_group_passports.dpn_serial_no', 0);

            $saved = $this->db->update('safa_group_passports');
            return $saved;
        }
    }

    public function getDisplay_order($group_id) {
        $sql = "SELECT safa_group_passport_id, display_order FROM safa_group_passports where safa_group_id=" . $group_id . "";
        $query = $this->db->query($sql);
        if (!empty($query) && $query->num_rows() > 0) {
            $result = $query->result_array();
            $list = array();
            foreach ($query->result() as $row) {
                $list[$row->safa_group_passport_id] = $row->display_order;
            }
            return $list;
        } else {
            return FALSE;
        }
    }

    function switchOrder($key, $newRank) {
        $this->db->set('display_order', $newRank, FALSE);
        $this->db->where('safa_group_passport_id', $key);
        $this->db->update('safa_group_passports');
    }

    //By Gouda
    function getMaxId() {
        $this->db->select_max('safa_group_passport_id');
        $query = $this->db->get('safa_group_passports');
        $row = $query->row();
        if (isset($row)) {
            return $row->safa_group_passport_id;
        } else {
            return 0;
        }
    }

    function get_uos_by_trip($safa_trip_id=0)
    {
    	$this->db->distinct();
    	$this->db->select("safa_packages.erp_company_id as safa_uo_id");
    	$this->db->from("safa_group_passports");
    	$this->db->join('safa_group_passport_accommodation', 'safa_group_passport_accommodation.safa_group_passport_id = safa_group_passports.safa_group_passport_id', 'left');
		$this->db->join('safa_package_periods', 'safa_package_periods.safa_package_periods_id = safa_group_passport_accommodation.safa_package_periods_id', 'left');
		$this->db->join('safa_packages', 'safa_packages.safa_package_id = safa_package_periods.safa_package_id', 'left');
    	$this->db->where('safa_group_passports.safa_trip_id', $safa_trip_id);
    	$this->db ->where('safa_packages.erp_company_type_id', 2);
      	
      	$query = $this->db->get();
        //echo $query_text=$this->db->last_query(); exit;
        return $query->result();
    }
    
    function get_by_groups_arr($safa_group_ids_arr) 
    {        
        $this->db->select('safa_group_passports.*, safa_groups.name as safa_groups_name, CONCAT( first_name_ar, " ", second_name_ar, " ", third_name_ar, " ", fourth_name_ar ) as full_name_ar, CONCAT( first_name_la, " ", second_name_la, " ", third_name_la, " ", fourth_name_la ) as full_name_la,   
        CONCAT( first_name_ar, " ", fourth_name_ar ) as first_fourth_name_ar, CONCAT( first_name_la, " ", fourth_name_la ) as first_fourth_name_la, erp_passporttypes.' . name() . ' as erp_passporttypes_name, 
        
        erp_passporttypes.' . name() . ' as erp_passporttypes_name, 
        erp_countries.' . name() . ' as erp_countries_name, erp_nationalities.' . name() . ' as erp_nationalities_name, 
        issuing_country.' . name() . ' as issuing_country_name, birth_country.' . name() . ' as birth_country_name, erp_relations.' . name() . ' as erp_relations_name, 

        erp_maritalstatus.' . name() . ' as erp_maritalstatus_name, erp_educationlevels.' . name() . ' as erp_educationlevels_name', FALSE);

        $this->db->from("safa_group_passports");
        
        $this->db->join('safa_groups', 'safa_groups.safa_group_id = safa_group_passports.safa_group_id', 'left');

        $this->db->join('erp_passporttypes', 'safa_group_passports.passport_type_id = erp_passporttypes.erp_passporttype_id', 'left');
        $this->db->join('erp_countries', 'safa_group_passports.erp_country_id = erp_countries.erp_country_id', 'left');
        $this->db->join('erp_nationalities', 'safa_group_passports.nationality_id = erp_nationalities.erp_nationality_id', 'left');
        $this->db->join('erp_countries as issuing_country', 'safa_group_passports.passport_issuing_country_id =issuing_country.erp_country_id', 'left');
        $this->db->join('erp_countries as birth_country', 'safa_group_passports.birth_country_id =birth_country.erp_country_id', 'left');

        $this->db->join('erp_relations', 'safa_group_passports.relative_relation_id = erp_relations.erp_relation_id', 'left');
        $this->db->join('erp_maritalstatus', 'safa_group_passports.marital_status_id = erp_maritalstatus.erp_maritalstatus_id', 'left');
        $this->db->join('erp_educationlevels', 'safa_group_passports.educational_level_id = erp_educationlevels.erp_educationlevel_id', 'left');
    	
    	
    	$this->db->where_in('safa_group_passports.safa_group_id', $safa_group_ids_arr);
    	
    	$this->db->order_by("safa_group_passports.display_order", "asc");
    	
        $query = $this->db->get();

        //$query_text=$this->db->last_query();
        //echo $query_text; exit;

        return $query->result();
    }
    
	function get_max_no_by_group($safa_group_id=0)
    {
    	$this->db->select('max(no) as max_no');
        $this->db->from('safa_group_passports');
        $this->db->where('safa_group_passports.safa_group_id', $safa_group_id);
        $query = $this->db->get();
        return $query->row();
    }
    
}

/* End of file safa_group_passports_model.php */
/* Location: ./application/models/travellers_model.php */
