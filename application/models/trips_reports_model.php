<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class trips_reports_model extends CI_Model {

    public $erp_hotel_id = false;
    public $safa_uo_contract_id = false;
    public $erp_transportertype_id = false;
    public $nationality_id = false;
    public $erp_city_id = false;
    public $end_erp_city_id = false;
    public $erp_port_id = false;
    public $safa_transporter_id = false;
    public $safa_trip_confirm_id = false;
    public $from_date = false;
    public $to_date = false;
    public $from_time = false;
    public $to_time = false;
    public $from_count = false;
    public $to_count = false;
    public $ea_country_id = false;
    //four filters of currently trips
    public $arriving_today = false;
    public $arriving_twm = false;
    public $departing_today = false;
    public $departing_twm = false;
    //all movement
    public $safa_ito_id = false;
    public $safa_internalsegmenttype_id = false;
    public $safa_internalsegmentestatus_id = false;
    public $operator_reference = false;
    public $from_erp_hotel_id = false;
    public $to_erp_hotel_id = false;
    /*     * ***********trips movents utilities************************* */
    public $uo_id = false;
    public $uo_user_id = false;
    /*     * ******************************* */
    public $ito_id = false;
    public $has_no_bus = false;
    public $ea_id = false;
    public $custom_select = false;
    public $limit = false;
    public $offset = false;
    public $order_by = false;

    function __construct() {
        parent::__construct();
    }

    /**
     * Deprecated function
     */
    function arriving_reports_by_trips($uo_id = false, $filters = false) {


        //arriving_twm
        if ($filters) {

            if ($this->arriving_twm) {

                $this->db->where("datediff(erp_flight_availabilities_detail.arrival_date, now()) = ", 1);
            } else {

                $this->db->where("datediff(erp_flight_availabilities_detail.arrival_date, now()) = ", 0);
            }
        }


        //erp_city_id
        if ($this->erp_city_id) {
            $this->db->where("(select erp_city_id from safa_trip_hotels, erp_hotels where safa_trip_hotels.safa_trip_id=safa_trips.`safa_trip_id` and erp_hotels.erp_hotel_id=safa_trip_hotels.erp_hotel_id order by checkin_datetime asc limit 1) = ", $this->erp_city_id, false);
        }

        //erp_hotel_id
        if ($this->erp_hotel_id !== FALSE) {
            $this->db->where('(select erp_hotels.erp_hotel_id from safa_trip_hotels, erp_hotels where safa_trip_hotels.safa_trip_id=safa_trips.`safa_trip_id` and erp_hotels.erp_hotel_id=safa_trip_hotels.erp_hotel_id order by checkin_datetime asc limit 1) = ', $this->erp_hotel_id, false, false);
        }

        //nationality_id
        if ($this->nationality_id !== FALSE) {
            $this->db->where('safa_uo_contracts.nationality_id', $this->nationality_id);
        }

        //safa_uo_contract_id
        if ($this->safa_uo_contract_id !== FALSE) {
            $this->db->where('safa_uo_contracts.safa_uo_contract_id', $this->safa_uo_contract_id);
        }


        //erp_port_id (port name)
        if ($this->erp_port_id !== FALSE) {
            $this->db->where("(select erp_ports.erp_port_id from erp_ports, erp_port_halls where erp_ports.erp_port_id = erp_port_halls.erp_port_id and erp_ports.erp_port_id = erp_flight_availabilities_detail.erp_port_id_to) = ", $this->erp_port_id, false);
        }


        //safa_transporter_id
        if ($this->safa_transporter_id !== FALSE) {
            //$this->db->where('(SELECT safa_transporter_id FROM safa_transporters WHERE safa_transporters.safa_transporter_id=safa_trip_externalsegments.safa_transporter_id) = ', $this->safa_transporter_id, false, false);
        }


        if ($this->safa_trip_confirm_id) {
            $this->db->where('safa_trips.safa_trip_confirm_id', $this->safa_trip_confirm_id);
        }



        if ($this->from_date !== FALSE) {
            $this->db->where('date( erp_flight_availabilities_detail.arrival_date ) >= ', $this->from_date);
        } else {
            $this->db->where('date( erp_flight_availabilities_detail.arrival_date ) >= ', date("Y-m-d"));
        }

        if ($this->to_date !== FALSE) {
            $this->db->where('date( erp_flight_availabilities_detail.arrival_date ) <= ', $this->to_date);
        }

        //from time
        if ($this->from_time !== FALSE) {
            $this->db->where('time(erp_flight_availabilities_detail.arrival_time) >= ', $this->from_time);
        }
        //to time
        if ($this->to_time !== FALSE) {
            $this->db->where('time(erp_flight_availabilities_detail.arrival_time) <= ', $this->to_time);
        }


        //$from_count
        if ($this->from_count !== FALSE) {
            $this->db->where('(SELECT COUNT(*) FROM safa_group_passports WHERE safa_group_passports.safa_trip_id = safa_trips.safa_trip_id) >=', $this->from_count);
        }

        if ($this->to_count !== FALSE) {
            $this->db->where('(SELECT COUNT(*) FROM safa_group_passports WHERE safa_group_passports.safa_trip_id = safa_trips.safa_trip_id) <=', $this->to_count);
        }

        $this->db->order_by('erp_flight_availabilities_detail.arrival_date', 'asc');

        $this->db->distinct();

//		$this->db->select(SLDB . ".safa_trips.safa_trip_id as trip_id,
//                         safa_eas." . name() . " as cn,
//                         
//                         (SELECT " . name() . " FROM " . SLDB . ".erp_countries WHERE " . SLDB . ".erp_countries.erp_country_id = " . SLDB . ".safa_uo_contracts.nationality_id) as nationality,
//                         (SELECT COUNT(*) FROM " . SLDB . ".safa_trip_travellers WHERE " . SLDB . ".safa_trip_travellers.safa_trip_id = " . SLDB . ".safa_trips.safa_trip_id) AS travellers,
//                         (select CONCAT(`name_ar`,\"/\", `mobile_sa`) from safa_ea_supervisors join safa_trip_supervisors on safa_trip_supervisors.safa_ea_supervisor_id = safa_ea_supervisors.safa_ea_supervisor_id where safa_trip_supervisors.safa_trip_id= safa_trips.safa_trip_id limit 1) as supervisor,                         (select " . name() . " from " . SLDB . ".safa_trip_hotels, " . SLDB . ".erp_hotels where safa_trip_hotels.safa_trip_id=" . SLDB . ".safa_trips.`safa_trip_id` and " . SLDB . ".erp_hotels.erp_hotel_id= " . SLDB . ".safa_trip_hotels.erp_hotel_id order by " . SLDB . ".safa_trip_hotels.checkin_datetime asc limit 1) as hotel_name,
//                         " . SLDB . ".erp_flight_availabilities_detail.arrival_date,
//                         (SELECT " . name() . " FROM " . SLDB . ".safa_transporters WHERE safa_transporters.safa_transporter_id=safa_trip_externalsegments.safa_transporter_id) as transporter
//                        ,(SELECT " . SLDB . ".erp_ports." . name() . " FROM " . SLDB . ".erp_ports left join erp_port_halls ON(erp_ports.erp_port_id = erp_port_halls.erp_port_id) WHERE erp_port_halls.erp_port_hall_id = safa_trip_externalsegments.start_port_hall_id) as port_name
//                        , (SELECT " . SLDB . ".erp_port_halls." . name() . " FROM " . SLDB . ".erp_port_halls WHERE erp_port_halls.erp_port_hall_id = safa_trip_externalsegments.end_port_hall_id) AS port_hall_name
//                        ," . SLDB . ".safa_trip_externalsegments.trip_code
//                        ," . FSDB . ".fs_status.color as status_color
//                        ," . FSDB . ".fs_status." . name() . " as status_name,
//                        
//                        (select count(safa_group_passports.safa_group_passport_id) from safa_group_passports      where  safa_group_passports.safa_trip_id=safa_trips.safa_trip_id  and (SELECT DATE_FORMAT(FROM_DAYS(TO_DAYS(NOW())-TO_DAYS(safa_group_passports.date_of_birth)), '%Y')+0 AS age FROM safa_group_passports as inner_safa_group_passports where inner_safa_group_passports.safa_group_passport_id =safa_group_passports.safa_group_passport_id) > 12 group by (safa_group_passports.safa_trip_id)) as travellers_adult_count,
//			            (select count(safa_group_passports.safa_group_passport_id) from safa_group_passports      where  safa_group_passports.safa_trip_id=safa_trips.safa_trip_id  and (SELECT DATE_FORMAT(FROM_DAYS(TO_DAYS(NOW())-TO_DAYS(safa_group_passports.date_of_birth)), '%Y')+0 AS age FROM safa_group_passports as inner_safa_group_passports where inner_safa_group_passports.safa_group_passport_id =safa_group_passports.safa_group_passport_id) > 2 and (SELECT DATE_FORMAT(FROM_DAYS(TO_DAYS(NOW())-TO_DAYS(safa_group_passports.date_of_birth)), '%Y')+0 AS age FROM safa_group_passports as inner_safa_group_passports where inner_safa_group_passports.safa_group_passport_id =safa_group_passports.safa_group_passport_id) < 12   group by (safa_group_passports.safa_trip_id)) as travellers_child_count,
//			            (select count(safa_group_passports.safa_group_passport_id) from safa_group_passports      where  safa_group_passports.safa_trip_id=safa_trips.safa_trip_id  and (SELECT DATE_FORMAT(FROM_DAYS(TO_DAYS(NOW())-TO_DAYS(safa_group_passports.date_of_birth)), '%Y')+0 AS age FROM safa_group_passports as inner_safa_group_passports where inner_safa_group_passports.safa_group_passport_id =safa_group_passports.safa_group_passport_id) < 2  group by (safa_group_passports.safa_trip_id)) as travellers_infant_count
//            
//                        
//                        , safa_trips.name as trip_name
//                            ", false);


        $this->db->select(SLDB . ".safa_trips.safa_trip_id as trip_id,
                         safa_eas." . name() . " as cn,
                         
                         (SELECT " . name() . " FROM " . SLDB . ".erp_countries WHERE " . SLDB . ".erp_countries.erp_country_id = " . SLDB . ".safa_uo_contracts.nationality_id) as nationality,
                         (SELECT COUNT(*) FROM " . SLDB . ".safa_group_passports WHERE " . SLDB . ".safa_group_passports.safa_trip_id = " . SLDB . ".safa_trips.safa_trip_id) AS travellers,
                         (select CONCAT(`name_ar`,\"/\", `mobile_sa`) from safa_ea_supervisors join safa_trip_supervisors on safa_trip_supervisors.safa_ea_supervisor_id = safa_ea_supervisors.safa_ea_supervisor_id where safa_trip_supervisors.safa_trip_id= safa_trips.safa_trip_id limit 1) as supervisor,                         
                         (SELECT " . name() . " FROM safa_transporters WHERE safa_transporters.code=" . FSDB . ".fs_airlines.iata) as transporter,                         
                         
                         (select " . name() . " from safa_reservation_forms, safa_passport_accommodation_rooms, erp_hotels where safa_reservation_forms.safa_trip_id =safa_trips.safa_trip_id and safa_passport_accommodation_rooms.safa_reservation_form_id =safa_reservation_forms.safa_reservation_form_id and erp_hotels.erp_hotel_id=safa_passport_accommodation_rooms.erp_hotel_id order by safa_passport_accommodation_rooms.from_date desc limit 1) as hotel_name,
                         
                         " . SLDB . ".erp_flight_availabilities_detail.arrival_date, " . SLDB . ".erp_flight_availabilities_detail.arrival_time , " . SLDB . ".erp_flight_availabilities_detail.flight_number ,
                         (SELECT " . SLDB . ".erp_ports." . name() . " FROM " . SLDB . ".erp_ports left join erp_port_halls ON(erp_ports.erp_port_id = erp_port_halls.erp_port_id) WHERE erp_ports.erp_port_id = erp_flight_availabilities_detail.erp_port_id_from limit 1) as port_name
                        , (SELECT " . SLDB . ".erp_port_halls." . name() . " FROM " . SLDB . ".erp_port_halls WHERE erp_port_halls.erp_port_id = erp_flight_availabilities_detail.erp_port_id_to limit 1) AS port_hall_name
                        
                        ," . FSDB . ".fs_status.color as status_color
                        ," . FSDB . ".fs_status." . name() . " as status_name,
                        
                        (select count(safa_group_passports.safa_group_passport_id) from safa_group_passports      where  safa_group_passports.safa_trip_id=safa_trips.safa_trip_id  and (SELECT DATE_FORMAT(FROM_DAYS(TO_DAYS(NOW())-TO_DAYS(safa_group_passports.date_of_birth)), '%Y')+0 AS age FROM safa_group_passports as inner_safa_group_passports where inner_safa_group_passports.safa_group_passport_id =safa_group_passports.safa_group_passport_id) > 12 group by (safa_group_passports.safa_trip_id)) as travellers_adult_count,
			            (select count(safa_group_passports.safa_group_passport_id) from safa_group_passports      where  safa_group_passports.safa_trip_id=safa_trips.safa_trip_id  and (SELECT DATE_FORMAT(FROM_DAYS(TO_DAYS(NOW())-TO_DAYS(safa_group_passports.date_of_birth)), '%Y')+0 AS age FROM safa_group_passports as inner_safa_group_passports where inner_safa_group_passports.safa_group_passport_id =safa_group_passports.safa_group_passport_id) > 2 and (SELECT DATE_FORMAT(FROM_DAYS(TO_DAYS(NOW())-TO_DAYS(safa_group_passports.date_of_birth)), '%Y')+0 AS age FROM safa_group_passports as inner_safa_group_passports where inner_safa_group_passports.safa_group_passport_id =safa_group_passports.safa_group_passport_id) < 12   group by (safa_group_passports.safa_trip_id)) as travellers_child_count,
			            (select count(safa_group_passports.safa_group_passport_id) from safa_group_passports      where  safa_group_passports.safa_trip_id=safa_trips.safa_trip_id  and (SELECT DATE_FORMAT(FROM_DAYS(TO_DAYS(NOW())-TO_DAYS(safa_group_passports.date_of_birth)), '%Y')+0 AS age FROM safa_group_passports as inner_safa_group_passports where inner_safa_group_passports.safa_group_passport_id =safa_group_passports.safa_group_passport_id) < 2  group by (safa_group_passports.safa_trip_id)) as travellers_infant_count
            
                        
                        , safa_trips.name as trip_name
                            ", false);


        $this->db->where(SLDB . '.erp_flight_availabilities_detail.safa_externaltriptype_id', 1);

        $this->db->where(SLDB . '.safa_trips.safa_trip_confirm_id', 1);

        $this->db->where('safa_uo_id', $uo_id);


        //$this->db->where(SLDB . '.safa_trips.safa_trip_confirm_id <> ', '2', false);

        $this->db->from(SLDB . '.safa_trips');
        $this->db->join('erp_flight_availabilities', 'safa_trips.safa_trip_id = erp_flight_availabilities.safa_trip_id');
        $this->db->join(SLDB . '.erp_flight_availabilities_detail', 'erp_flight_availabilities_detail.erp_flight_availability_id = erp_flight_availabilities.erp_flight_availability_id');

        $this->db->join(FSDB . '.fs_flights', FSDB . '.fs_flights.fs_flight_id = ' . SLDB . '.erp_flight_availabilities_detail.fs_flight_id', 'left');

        $this->db->join(FSDB . '.fs_status', FSDB . '.fs_status.fs_status_id = ' . FSDB . '.fs_flights.fs_status_id', 'left');

        $this->db->join(FSDB . '.fs_airlines', FSDB . '.fs_airlines.fs_airline_id = ' . SLDB . '.erp_flight_availabilities_detail.erp_airline_id', 'left');

        $this->db->join('safa_uo_contracts_eas', 'safa_trips.safa_ea_id = safa_uo_contracts_eas.safa_ea_id', 'left');

        $this->db->join(SLDB . '.safa_uo_contracts', "safa_uo_contracts.safa_uo_contract_id = safa_uo_contracts_eas.safa_uo_contract_id", 'left');

        $this->db->join('safa_eas', 'safa_trips.safa_ea_id=safa_eas.safa_ea_id', 'left');

        $query = $this->db->get();

        //echo $this->db->last_query(); exit;

        return $query->result_array();
    }

    /**
     * Deprecated function
     */
    function departing_reports_by_trips($uo_id = false, $filters = false) {


        //arriving_twm
        if ($filters) {

            if ($this->departing_twm) {

                $this->db->where("datediff(erp_flight_availabilities_detail.flight_date, now()) = ", 1);
            } else {

                $this->db->where("datediff(erp_flight_availabilities_detail.flight_date, now()) = ", 0);
            }
        }


        //erp_city_id
        if ($this->erp_city_id) {
            $this->db->where("(select erp_city_id from safa_trip_hotels, erp_hotels where safa_trip_hotels.safa_trip_id=safa_trips.`safa_trip_id` and erp_hotels.erp_hotel_id=safa_trip_hotels.erp_hotel_id order by checkin_datetime asc limit 1) = ", $this->erp_city_id, false);
        }

        //erp_hotel_id
        if ($this->erp_hotel_id !== FALSE) {
            $this->db->where('(select erp_hotels.erp_hotel_id from safa_trip_hotels, erp_hotels where safa_trip_hotels.safa_trip_id=safa_trips.`safa_trip_id` and erp_hotels.erp_hotel_id=safa_trip_hotels.erp_hotel_id order by checkin_datetime asc limit 1) = ', $this->erp_hotel_id, false, false);
        }

        //nationality_id
        if ($this->nationality_id !== FALSE) {
            $this->db->where('safa_uo_contracts.nationality_id', $this->nationality_id);
        }

        //safa_uo_contract_id
        if ($this->safa_uo_contract_id !== FALSE) {
            $this->db->where('safa_uo_contracts.safa_uo_contract_id', $this->safa_uo_contract_id);
        }


        //erp_port_id (port name)
        if ($this->erp_port_id !== FALSE) {
            $this->db->where("(select erp_ports.erp_port_id from erp_ports, erp_port_halls where erp_ports.erp_port_id = erp_port_halls.erp_port_id and erp_ports.erp_port_id = erp_flight_availabilities_detail.erp_port_id_to) = ", $this->erp_port_id, false);
        }


        //safa_transporter_id
        if ($this->safa_transporter_id !== FALSE) {
            //$this->db->where('(SELECT safa_transporter_id FROM safa_transporters WHERE safa_transporters.safa_transporter_id=safa_trip_externalsegments.safa_transporter_id) = ', $this->safa_transporter_id, false, false);
        }


        if ($this->safa_trip_confirm_id) {
            $this->db->where('safa_trips.safa_trip_confirm_id', $this->safa_trip_confirm_id);
        }



        if ($this->from_date !== FALSE) {
            $this->db->where('date( erp_flight_availabilities_detail.flight_date ) >= ', $this->from_date);
        } else {
            $this->db->where('date( erp_flight_availabilities_detail.flight_date ) >= ', date("Y-m-d"));
        }

        if ($this->to_date !== FALSE) {
            $this->db->where('date( erp_flight_availabilities_detail.flight_date ) <= ', $this->to_date);
        }

        //from time
        if ($this->from_time !== FALSE) {
            $this->db->where('time(erp_flight_availabilities_detail.departure_time) >= ', $this->from_time);
        }
        //to time
        if ($this->to_time !== FALSE) {
            $this->db->where('time(erp_flight_availabilities_detail.departure_time) <= ', $this->to_time);
        }


        //$from_count
        if ($this->from_count !== FALSE) {
            $this->db->where('(SELECT COUNT(*) FROM safa_group_passports WHERE safa_group_passports.safa_trip_id = safa_trips.safa_trip_id) >=', $this->from_count);
        }

        if ($this->to_count !== FALSE) {
            $this->db->where('(SELECT COUNT(*) FROM safa_group_passports WHERE safa_group_passports.safa_trip_id = safa_trips.safa_trip_id) <=', $this->to_count);
        }

        $this->db->order_by('erp_flight_availabilities_detail.flight_date', 'asc');

        $this->db->distinct();


        $this->db->select(SLDB . ".safa_trips.safa_trip_id as trip_id,
                         safa_eas." . name() . " as cn,
                         
                         (SELECT " . name() . " FROM " . SLDB . ".erp_countries WHERE " . SLDB . ".erp_countries.erp_country_id = " . SLDB . ".safa_uo_contracts.nationality_id) as nationality,
                         (SELECT COUNT(*) FROM " . SLDB . ".safa_group_passports WHERE " . SLDB . ".safa_group_passports.safa_trip_id = " . SLDB . ".safa_trips.safa_trip_id) AS travellers,
                         (select CONCAT(`name_ar`,\"/\", `mobile_sa`) from safa_ea_supervisors join safa_trip_supervisors on safa_trip_supervisors.safa_ea_supervisor_id = safa_ea_supervisors.safa_ea_supervisor_id where safa_trip_supervisors.safa_trip_id= safa_trips.safa_trip_id limit 1) as supervisor,                         

                         (SELECT " . name() . " FROM safa_transporters WHERE safa_transporters.code=" . FSDB . ".fs_airlines.iata) as transporter,                         
                         
                         (select " . name() . " from safa_reservation_forms, safa_passport_accommodation_rooms, erp_hotels where safa_reservation_forms.safa_trip_id =safa_trips.safa_trip_id and safa_passport_accommodation_rooms.safa_reservation_form_id =safa_reservation_forms.safa_reservation_form_id and erp_hotels.erp_hotel_id=safa_passport_accommodation_rooms.erp_hotel_id order by safa_passport_accommodation_rooms.from_date desc limit 1) as hotel_name,
                         
                         " . SLDB . ".erp_flight_availabilities_detail.flight_date, " . SLDB . ".erp_flight_availabilities_detail.departure_time , " . SLDB . ".erp_flight_availabilities_detail.flight_number ,
                         (SELECT " . SLDB . ".erp_ports." . name() . " FROM " . SLDB . ".erp_ports left join erp_port_halls ON(erp_ports.erp_port_id = erp_port_halls.erp_port_id) WHERE erp_ports.erp_port_id = erp_flight_availabilities_detail.erp_port_id_from limit 1) as port_name
                        , (SELECT " . SLDB . ".erp_port_halls." . name() . " FROM " . SLDB . ".erp_port_halls WHERE erp_port_halls.erp_port_id = erp_flight_availabilities_detail.erp_port_id_to limit 1) AS port_hall_name
                        
                        ," . FSDB . ".fs_status.color as status_color
                        ," . FSDB . ".fs_status." . name() . " as status_name,
                        
                        (select count(safa_group_passports.safa_group_passport_id) from safa_group_passports      where  safa_group_passports.safa_trip_id=safa_trips.safa_trip_id  and (SELECT DATE_FORMAT(FROM_DAYS(TO_DAYS(NOW())-TO_DAYS(safa_group_passports.date_of_birth)), '%Y')+0 AS age FROM safa_group_passports as inner_safa_group_passports where inner_safa_group_passports.safa_group_passport_id =safa_group_passports.safa_group_passport_id) > 12 group by (safa_group_passports.safa_trip_id)) as travellers_adult_count,
			            (select count(safa_group_passports.safa_group_passport_id) from safa_group_passports      where  safa_group_passports.safa_trip_id=safa_trips.safa_trip_id  and (SELECT DATE_FORMAT(FROM_DAYS(TO_DAYS(NOW())-TO_DAYS(safa_group_passports.date_of_birth)), '%Y')+0 AS age FROM safa_group_passports as inner_safa_group_passports where inner_safa_group_passports.safa_group_passport_id =safa_group_passports.safa_group_passport_id) > 2 and (SELECT DATE_FORMAT(FROM_DAYS(TO_DAYS(NOW())-TO_DAYS(safa_group_passports.date_of_birth)), '%Y')+0 AS age FROM safa_group_passports as inner_safa_group_passports where inner_safa_group_passports.safa_group_passport_id =safa_group_passports.safa_group_passport_id) < 12   group by (safa_group_passports.safa_trip_id)) as travellers_child_count,
			            (select count(safa_group_passports.safa_group_passport_id) from safa_group_passports      where  safa_group_passports.safa_trip_id=safa_trips.safa_trip_id  and (SELECT DATE_FORMAT(FROM_DAYS(TO_DAYS(NOW())-TO_DAYS(safa_group_passports.date_of_birth)), '%Y')+0 AS age FROM safa_group_passports as inner_safa_group_passports where inner_safa_group_passports.safa_group_passport_id =safa_group_passports.safa_group_passport_id) < 2  group by (safa_group_passports.safa_trip_id)) as travellers_infant_count
            
                        
                        , safa_trips.name as trip_name
                            ", false);


        $this->db->where(SLDB . '.erp_flight_availabilities_detail.safa_externaltriptype_id', 2);

        $this->db->where(SLDB . '.safa_trips.safa_trip_confirm_id', 1);

        $this->db->where('safa_uo_id', $uo_id);


        //$this->db->where(SLDB . '.safa_trips.safa_trip_confirm_id <> ', '2', false);

        $this->db->from(SLDB . '.safa_trips');
        $this->db->join('erp_flight_availabilities', 'safa_trips.safa_trip_id = erp_flight_availabilities.safa_trip_id');
        $this->db->join(SLDB . '.erp_flight_availabilities_detail', 'erp_flight_availabilities_detail.erp_flight_availability_id = erp_flight_availabilities.erp_flight_availability_id');

        $this->db->join(FSDB . '.fs_flights', FSDB . '.fs_flights.fs_flight_id = ' . SLDB . '.erp_flight_availabilities_detail.fs_flight_id', 'left');

        $this->db->join(FSDB . '.fs_status', FSDB . '.fs_status.fs_status_id = ' . FSDB . '.fs_flights.fs_status_id', 'left');

        $this->db->join(FSDB . '.fs_airlines', FSDB . '.fs_airlines.fs_airline_id = ' . SLDB . '.erp_flight_availabilities_detail.erp_airline_id', 'left');

        $this->db->join('safa_uo_contracts_eas', 'safa_trips.safa_ea_id = safa_uo_contracts_eas.safa_ea_id', 'left');

        $this->db->join(SLDB . '.safa_uo_contracts', "safa_uo_contracts.safa_uo_contract_id = safa_uo_contracts_eas.safa_uo_contract_id", 'left');

        $this->db->join('safa_eas', 'safa_trips.safa_ea_id=safa_eas.safa_ea_id', 'left');

        $query = $this->db->get();

        //echo $this->db->last_query(); exit;

        return $query->result_array();
    }

    function arriving_reports($uo_id = false, $filters = false) {


        //arriving_twm
        if ($filters) {

            if ($this->arriving_twm) {
                $this->db->where("datediff(erp_flight_availabilities_detail.arrival_date, now()) = ", 1);
            } else {
                $this->db->where("datediff(erp_flight_availabilities_detail.arrival_date, now()) = ", 0);
            }
        }


        //erp_city_id
        if ($this->erp_city_id) {
            $this->db->where("(select erp_hotels.erp_city_id from safa_internalsegments, erp_hotels, safa_trip_internaltrips where safa_trip_internaltrips.safa_trip_internaltrip_id = safa_internalsegments.safa_trip_internaltrip_id AND erp_hotels.erp_hotel_id=safa_internalsegments.erp_end_hotel_id order by start_datetime asc limit 1) = ", $this->erp_city_id, false);
        }

        //erp_hotel_id
        if ($this->erp_hotel_id !== FALSE) {
            $this->db->where('(select erp_hotels.erp_hotel_id from safa_internalsegments, safa_trip_internaltrips, erp_hotels where  safa_trip_internaltrips.safa_trip_internaltrip_id = safa_internalsegments.safa_trip_internaltrip_id and erp_hotels.erp_hotel_id=safa_internalsegments.erp_end_hotel_id order by start_datetime asc limit 1) = ', $this->erp_hotel_id, false, false);
        }

        //nationality_id
        if ($this->nationality_id !== FALSE) {
            $this->db->where('safa_uo_contracts.nationality_id', $this->nationality_id);
        }

        //safa_uo_contract_id
        if ($this->safa_uo_contract_id !== FALSE) {
            $this->db->where('safa_uo_contracts.safa_uo_contract_id', $this->safa_uo_contract_id);
        }


        //erp_port_id (port name)
        if ($this->erp_port_id !== FALSE) {
            $this->db->where("(select erp_ports.erp_port_id from erp_ports, erp_port_halls where erp_ports.erp_port_id = erp_port_halls.erp_port_id and erp_ports.erp_port_id = erp_flight_availabilities_detail.erp_port_id_to) = ", $this->erp_port_id, false);
        }


        //safa_transporter_id
        if ($this->safa_transporter_id !== FALSE) {
            $this->db->where('(SELECT safa_transporter_id FROM safa_transporters WHERE safa_transporters.code=erp_flight_availabilities_detail.airline_code) = ', $this->safa_transporter_id, false, false);
        }


        if ($this->safa_trip_confirm_id) {
            $this->db->where('safa_trips.safa_trip_confirm_id', $this->safa_trip_confirm_id);
        }



        if ($this->from_date !== FALSE) {
            $this->db->where('date( erp_flight_availabilities_detail.arrival_date ) >= ', $this->from_date);
        } else {
            $this->db->where('date( erp_flight_availabilities_detail.arrival_date ) >= ', date("Y-m-d"));
        }

        if ($this->to_date !== FALSE) {
            $this->db->where('date( erp_flight_availabilities_detail.arrival_date ) <= ', $this->to_date);
        }

        //from time
        if ($this->from_time !== FALSE) {
            $this->db->where('time(erp_flight_availabilities_detail.arrival_time) >= ', $this->from_time);
        }
        //to time
        if ($this->to_time !== FALSE) {
            $this->db->where('time(erp_flight_availabilities_detail.arrival_time) <= ', $this->to_time);
        }


        //$from_count
        if ($this->from_count !== FALSE) {
            $this->db->where('erp_flight_availabilities_detail.seats_count >=', $this->from_count);
        }

        if ($this->to_count !== FALSE) {
            $this->db->where('erp_flight_availabilities_detail.seats_count <=', $this->to_count);
        }

        if ($this->uo_id !== FALSE) {
            $this->db->where('safa_uo_contracts.safa_uo_id', $this->uo_id);
        }

        if ($this->ito_id !== FALSE) {
            $this->db->where('safa_trip_internaltrips.safa_ito_id', $this->ito_id);
        }

        $this->db->order_by('erp_flight_availabilities_detail.arrival_date asc, erp_flight_availabilities_detail.arrival_time asc');

        $this->db->distinct();



        $this->db->select(SLDB . ".safa_trips.safa_trip_id as trip_id,
                         safa_uo_contracts." . name() . " as cn,
                         
                         (SELECT " . name() . " FROM " . SLDB . ".erp_countries WHERE " . SLDB . ".erp_countries.erp_country_id = " . SLDB . ".safa_uo_contracts.nationality_id) as nationality,
                         erp_flight_availabilities_detail.seats_count,
                         
                         (select CONCAT(`name_ar`,\"/\", `mobile_sa`) from safa_ea_supervisors join safa_trip_supervisors on safa_trip_supervisors.safa_ea_supervisor_id = safa_ea_supervisors.safa_ea_supervisor_id where safa_trip_supervisors.safa_trip_id= safa_trips.safa_trip_id limit 1) as supervisor,                         
                         (SELECT " . name() . " FROM safa_transporters WHERE safa_transporters.code=erp_flight_availabilities_detail.airline_code) as transporter,                         
                         
                         (select " . name() . " from safa_internalsegments, erp_hotels, safa_trip_internaltrips safa_trip_internaltrips_inner where safa_trip_internaltrips_inner.safa_trip_internaltrip_id = safa_trip_internaltrips.safa_trip_internaltrip_id and safa_trip_internaltrips.safa_trip_internaltrip_id = safa_internalsegments.safa_trip_internaltrip_id AND erp_hotels.erp_hotel_id=safa_internalsegments.erp_end_hotel_id order by safa_internalsegments.start_datetime desc limit 1) as hotel_name,
                         
                         " . SLDB . ".erp_flight_availabilities_detail.arrival_date, " . SLDB . ".erp_flight_availabilities_detail.arrival_time  , CONCAT(`erp_flight_availabilities_detail`.`flight_number`,\" \", `erp_flight_availabilities_detail`.`airline_code`) as flight_number
                         , (SELECT " . SLDB . ".erp_ports." . name() . " FROM " . SLDB . ".erp_ports  WHERE erp_ports.erp_port_id = erp_flight_availabilities_detail.erp_port_id_from) as start_port_name
                         , (SELECT " . SLDB . ".erp_port_halls." . name() . " FROM " . SLDB . ".erp_port_halls  WHERE erp_port_halls.erp_port_hall_id = erp_flight_availabilities_detail.erp_port_hall_id_from) as start_port_hall_name
                         
                        ,(SELECT " . SLDB . ".erp_ports." . name() . " FROM " . SLDB . ".erp_ports  WHERE erp_ports.erp_port_id = erp_flight_availabilities_detail.erp_port_id_to) as end_port_name
                        , (SELECT " . SLDB . ".erp_port_halls." . name() . " FROM " . SLDB . ".erp_port_halls  WHERE erp_port_halls.erp_port_hall_id = erp_flight_availabilities_detail.erp_port_hall_id_to) as end_port_hall_name
                         
                        
                        ,(select count(safa_group_passports.safa_group_passport_id) from safa_group_passports      where  safa_group_passports.safa_trip_id=safa_trips.safa_trip_id  and (SELECT DATE_FORMAT(FROM_DAYS(TO_DAYS(NOW())-TO_DAYS(safa_group_passports.date_of_birth)), '%Y')+0 AS age FROM safa_group_passports as inner_safa_group_passports where inner_safa_group_passports.safa_group_passport_id =safa_group_passports.safa_group_passport_id) > 12 group by (safa_group_passports.safa_trip_id)) as travellers_adult_count,
			(select count(safa_group_passports.safa_group_passport_id) from safa_group_passports      where  safa_group_passports.safa_trip_id=safa_trips.safa_trip_id  and (SELECT DATE_FORMAT(FROM_DAYS(TO_DAYS(NOW())-TO_DAYS(safa_group_passports.date_of_birth)), '%Y')+0 AS age FROM safa_group_passports as inner_safa_group_passports where inner_safa_group_passports.safa_group_passport_id =safa_group_passports.safa_group_passport_id) > 2 and (SELECT DATE_FORMAT(FROM_DAYS(TO_DAYS(NOW())-TO_DAYS(safa_group_passports.date_of_birth)), '%Y')+0 AS age FROM safa_group_passports as inner_safa_group_passports where inner_safa_group_passports.safa_group_passport_id =safa_group_passports.safa_group_passport_id) < 12   group by (safa_group_passports.safa_trip_id)) as travellers_child_count,
			(select count(safa_group_passports.safa_group_passport_id) from safa_group_passports      where  safa_group_passports.safa_trip_id=safa_trips.safa_trip_id  and (SELECT DATE_FORMAT(FROM_DAYS(TO_DAYS(NOW())-TO_DAYS(safa_group_passports.date_of_birth)), '%Y')+0 AS age FROM safa_group_passports as inner_safa_group_passports where inner_safa_group_passports.safa_group_passport_id =safa_group_passports.safa_group_passport_id) < 2  group by (safa_group_passports.safa_trip_id)) as travellers_infant_count
            
                        
                        , safa_trips.name as trip_name
                        , safa_trip_internaltrips.trip_title 
                        , safa_trip_internaltrips.safa_trip_internaltrip_id
                        , CONCAT(`safa_trip_internaltrips`.`trip_supervisors`,\"/\", `safa_trip_internaltrips`.`trip_supervisors_phone`)  as trip_supervisors
                        ,  erp_flight_availabilities_detail.fs_flight_id
                            
                        ", false);


        $this->db->where(SLDB . '.erp_flight_availabilities_detail.safa_externaltriptype_id', 1);

        if ($uo_id)
            $this->db->where('safa_trip_internaltrips.safa_uo_id', $uo_id);

        $this->db->where("(safa_trip_internaltrips.deleted IS NULL OR safa_trip_internaltrips.deleted=0)");


        $this->db->from(SLDB . '.erp_flight_availabilities_detail');
        $this->db->join(SLDB . '.erp_flight_availabilities', 'erp_flight_availabilities_detail.erp_flight_availability_id = erp_flight_availabilities.erp_flight_availability_id');

        $this->db->join('safa_trip_internaltrips', 'safa_trip_internaltrips.erp_flight_availability_id = erp_flight_availabilities.erp_flight_availability_id', 'left');
        $this->db->join('safa_internalsegments', 'safa_internalsegments.safa_trip_internaltrip_id = safa_trip_internaltrips.safa_trip_internaltrip_id', 'left');

        $this->db->join('safa_trips', 'safa_trips.safa_trip_id = safa_trip_internaltrips.safa_trip_id', 'left');


        //$this->db->join(FSDB . '.fs_flights', FSDB . '.fs_flights.fs_flight_id = ' . SLDB . '.erp_flight_availabilities_detail.fs_flight_id', 'left');
        //$this->db->join(FSDB . '.fs_status', FSDB . '.fs_status.fs_status_id = ' . FSDB . '.fs_flights.fs_status_id', 'left');
        //$this->db->join(FSDB . '.fs_airlines', FSDB . '.fs_airlines.fs_airline_id = ' . SLDB . '.erp_flight_availabilities_detail.erp_airline_id', 'left');

        $this->db->join(SLDB . '.safa_uo_contracts', "safa_uo_contracts.safa_uo_contract_id = safa_trip_internaltrips.safa_uo_contract_id", 'left');

        $this->db->join('safa_itos', 'safa_trip_internaltrips.safa_ito_id=safa_itos.safa_ito_id', 'left');

        //$this->db->where('(safa_trip_internaltrips.erp_company_id = safa_uo_contracts_eas.safa_ea_id AND safa_trip_internaltrips.erp_company_type_id=3 AND safa_uo_contracts_eas.safa_uo_contract_id = safa_uo_contracts.safa_uo_contract_id )');        
        //$this->db->from('safa_uo_contracts_eas');
        //$this->db->join('safa_eas', 'safa_trips.safa_ea_id=safa_eas.safa_ea_id', 'left');
        //$this->db->where('(safa_trips.safa_trip_id = safa_trip_internaltrips.safa_trip_id OR safa_trips.safa_trip_id IS NULL)');        
        //$this->db->where('(safa_trip_internaltrips.erp_company_id = safa_eas.safa_ea_id AND safa_trip_internaltrips.erp_company_type_id=3)');        
        //$this->db->from('safa_eas');


        if (session('gov_id')) {
            $this->db->join('safa_uos', 'safa_uos.safa_uo_id = safa_trip_internaltrips.safa_uo_id');
            $this->db->select('safa_uos.' . name() . ' as uo_name');
            $this->db->join('safa_govs', 'safa_govs.erp_country_id = safa_uo_contracts.country_id');
            $this->db->where('safa_govs.safa_gov_id', session('gov_id'));
        } elseif(session('ea_id')) {
            $this->db->join('safa_uo_contracts_eas', 'safa_uo_contracts_eas.safa_uo_contract_id = safa_uo_contracts.safa_uo_contract_id');
            $this->db->where('safa_uo_contracts_eas.safa_ea_id', session('ea_id'));
        }

        $query = $this->db->get();

        //echo $this->db->last_query(); exit;

        return $query->result_array();
    }

    function departing_reports($uo_id = false, $filters = false) {


        //departing_twm
        if ($filters) {

            if ($this->departing_twm) {

                $this->db->where("datediff(erp_flight_availabilities_detail.flight_date, now()) = ", 1);
            } else {

                $this->db->where("datediff(erp_flight_availabilities_detail.flight_date, now()) = ", 0);
            }
        }



        //erp_city_id
        if ($this->erp_city_id) {
            $this->db->where("(select erp_hotels.erp_city_id from safa_internalsegments, erp_hotels, safa_trip_internaltrips where safa_trip_internaltrips.safa_trip_internaltrip_id = safa_internalsegments.safa_trip_internaltrip_id AND erp_hotels.erp_hotel_id=safa_internalsegments.erp_start_hotel_id order by start_datetime asc limit 1) = ", $this->erp_city_id, false);
        }

        //erp_hotel_id
        if ($this->erp_hotel_id !== FALSE) {
            $this->db->where('(select erp_hotels.erp_hotel_id from safa_internalsegments, safa_trip_internaltrips, erp_hotels where  safa_trip_internaltrips.safa_trip_internaltrip_id = safa_internalsegments.safa_trip_internaltrip_id and erp_hotels.erp_hotel_id=safa_internalsegments.erp_start_hotel_id order by start_datetime asc limit 1) = ', $this->erp_hotel_id, false, false);
        }

        //nationality_id
        if ($this->nationality_id !== FALSE) {
            $this->db->where('safa_uo_contracts.nationality_id', $this->nationality_id);
        }

        //safa_uo_contract_id
        if ($this->safa_uo_contract_id !== FALSE) {
            $this->db->where('safa_uo_contracts.safa_uo_contract_id', $this->safa_uo_contract_id);
        }


        //erp_port_id (port name)
        if ($this->erp_port_id !== FALSE) {
            $this->db->where("(select erp_ports.erp_port_id from erp_ports, erp_port_halls where erp_ports.erp_port_id = erp_port_halls.erp_port_id and erp_ports.erp_port_id = erp_flight_availabilities_detail.erp_port_id_from) = ", $this->erp_port_id, false);
        }


        //safa_transporter_id
        if ($this->safa_transporter_id !== FALSE) {
            $this->db->where('(SELECT safa_transporter_id FROM safa_transporters WHERE safa_transporters.code=erp_flight_availabilities_detail.airline_code) = ', $this->safa_transporter_id, false, false);
        }


        if ($this->safa_trip_confirm_id) {
            $this->db->where('safa_trips.safa_trip_confirm_id', $this->safa_trip_confirm_id);
        }



        if ($this->from_date !== FALSE) {
            $this->db->where('date( erp_flight_availabilities_detail.flight_date ) >= ', $this->from_date);
        } else {
            $this->db->where('date( erp_flight_availabilities_detail.flight_date ) >= ', date("Y-m-d"));
        }

        if ($this->to_date !== FALSE) {
            $this->db->where('date( erp_flight_availabilities_detail.flight_date ) <= ', $this->to_date);
        }

        //from time
        if ($this->from_time !== FALSE) {
            $this->db->where('time(erp_flight_availabilities_detail.departure_time) >= ', $this->from_time);
        }
        //to time
        if ($this->to_time !== FALSE) {
            $this->db->where('time(erp_flight_availabilities_detail.departure_time) <= ', $this->to_time);
        }


        //$from_count
        if ($this->from_count !== FALSE) {
            $this->db->where('erp_flight_availabilities_detail.seats_count >=', $this->from_count);
        }

        if ($this->to_count !== FALSE) {
            $this->db->where('erp_flight_availabilities_detail.seats_count <=', $this->to_count);
        }

        if ($this->uo_id !== FALSE) {
            $this->db->where('safa_uo_contracts.safa_uo_id', $this->uo_id);
        }

        if ($this->ito_id !== FALSE) {
            $this->db->where('safa_trip_internaltrips.safa_ito_id', $this->ito_id);
        }


        $this->db->distinct();



        $this->db->select(SLDB . ".safa_trips.safa_trip_id as trip_id,
                         safa_uo_contracts." . name() . " as cn,
						                         
                         (SELECT " . name() . " FROM " . SLDB . ".erp_countries WHERE " . SLDB . ".erp_countries.erp_country_id = " . SLDB . ".safa_uo_contracts.nationality_id) as nationality,
                         (SELECT COUNT(*) FROM " . SLDB . ".safa_group_passports WHERE " . SLDB . ".safa_group_passports.safa_trip_id = " . SLDB . ".safa_trips.safa_trip_id) AS travellers,
                         (select CONCAT(`name_ar`,\"/\", `mobile_sa`) from safa_ea_supervisors join safa_trip_supervisors on safa_trip_supervisors.safa_ea_supervisor_id = safa_ea_supervisors.safa_ea_supervisor_id where safa_trip_supervisors.safa_trip_id= safa_trips.safa_trip_id limit 1) as supervisor,                         
                         erp_flight_availabilities_detail.seats_count,
                         
                         (SELECT " . name() . " FROM safa_transporters WHERE safa_transporters.code= erp_flight_availabilities_detail.airline_code) as transporter,                         
                         
                         (select " . name() . " from safa_internalsegments, erp_hotels, safa_trip_internaltrips safa_trip_internaltrips_inner where safa_trip_internaltrips_inner.safa_trip_internaltrip_id = safa_trip_internaltrips.safa_trip_internaltrip_id and safa_trip_internaltrips.safa_trip_internaltrip_id = safa_internalsegments.safa_trip_internaltrip_id AND erp_hotels.erp_hotel_id=safa_internalsegments.erp_end_hotel_id order by safa_internalsegments.start_datetime desc limit 1) as hotel_name,
                         
                         " . SLDB . ".erp_flight_availabilities_detail.flight_date, " . SLDB . ".erp_flight_availabilities_detail.departure_time , CONCAT(`erp_flight_availabilities_detail`.`flight_number`,\" \", erp_flight_availabilities_detail.airline_code) as flight_number,
                         
                         
                         , (SELECT " . SLDB . ".erp_ports." . name() . " FROM " . SLDB . ".erp_ports  WHERE erp_ports.erp_port_id = erp_flight_availabilities_detail.erp_port_id_from) as start_port_name
                         , (SELECT " . SLDB . ".erp_port_halls." . name() . " FROM " . SLDB . ".erp_port_halls  WHERE erp_port_halls.erp_port_hall_id = erp_flight_availabilities_detail.erp_port_hall_id_from) as start_port_hall_name
                         
                         ,(SELECT " . SLDB . ".erp_ports." . name() . " FROM " . SLDB . ".erp_ports  WHERE erp_ports.erp_port_id = erp_flight_availabilities_detail.erp_port_id_to) as end_port_name
                         , (SELECT " . SLDB . ".erp_port_halls." . name() . " FROM " . SLDB . ".erp_port_halls  WHERE erp_port_halls.erp_port_hall_id = erp_flight_availabilities_detail.erp_port_hall_id_to) as end_port_hall_name
                         
                        
                        
                        ,(select count(safa_group_passports.safa_group_passport_id) from safa_group_passports      where  safa_group_passports.safa_trip_id=safa_trips.safa_trip_id  and (SELECT DATE_FORMAT(FROM_DAYS(TO_DAYS(NOW())-TO_DAYS(safa_group_passports.date_of_birth)), '%Y')+0 AS age FROM safa_group_passports as inner_safa_group_passports where inner_safa_group_passports.safa_group_passport_id =safa_group_passports.safa_group_passport_id) > 12 group by (safa_group_passports.safa_trip_id)) as travellers_adult_count,
			            (select count(safa_group_passports.safa_group_passport_id) from safa_group_passports      where  safa_group_passports.safa_trip_id=safa_trips.safa_trip_id  and (SELECT DATE_FORMAT(FROM_DAYS(TO_DAYS(NOW())-TO_DAYS(safa_group_passports.date_of_birth)), '%Y')+0 AS age FROM safa_group_passports as inner_safa_group_passports where inner_safa_group_passports.safa_group_passport_id =safa_group_passports.safa_group_passport_id) > 2 and (SELECT DATE_FORMAT(FROM_DAYS(TO_DAYS(NOW())-TO_DAYS(safa_group_passports.date_of_birth)), '%Y')+0 AS age FROM safa_group_passports as inner_safa_group_passports where inner_safa_group_passports.safa_group_passport_id =safa_group_passports.safa_group_passport_id) < 12   group by (safa_group_passports.safa_trip_id)) as travellers_child_count,
			            (select count(safa_group_passports.safa_group_passport_id) from safa_group_passports      where  safa_group_passports.safa_trip_id=safa_trips.safa_trip_id  and (SELECT DATE_FORMAT(FROM_DAYS(TO_DAYS(NOW())-TO_DAYS(safa_group_passports.date_of_birth)), '%Y')+0 AS age FROM safa_group_passports as inner_safa_group_passports where inner_safa_group_passports.safa_group_passport_id =safa_group_passports.safa_group_passport_id) < 2  group by (safa_group_passports.safa_trip_id)) as travellers_infant_count
            
                        
                        , safa_trips.name as trip_name
                        , safa_trip_internaltrips.trip_title
                        , safa_trip_internaltrips.safa_trip_internaltrip_id 
                        , CONCAT(`safa_trip_internaltrips`.`trip_supervisors`,\"/\", `safa_trip_internaltrips`.`trip_supervisors_phone`)  as trip_supervisors
                        ,  erp_flight_availabilities_detail.fs_flight_id
                        
                            ", false);


        $this->db->where(SLDB . '.erp_flight_availabilities_detail.safa_externaltriptype_id', 2);

        if ($uo_id)
            $this->db->where('safa_trip_internaltrips.safa_uo_id', $uo_id);

        $this->db->where("(safa_trip_internaltrips.deleted IS NULL OR safa_trip_internaltrips.deleted=0)");


        $this->db->from(SLDB . '.erp_flight_availabilities_detail');
        $this->db->join(SLDB . '.erp_flight_availabilities', 'erp_flight_availabilities_detail.erp_flight_availability_id = erp_flight_availabilities.erp_flight_availability_id');

        $this->db->join('safa_trip_internaltrips', 'safa_trip_internaltrips.erp_flight_availability_id = erp_flight_availabilities.erp_flight_availability_id', 'left');
        $this->db->join('safa_internalsegments', 'safa_internalsegments.safa_trip_internaltrip_id = safa_trip_internaltrips.safa_trip_internaltrip_id', 'left');

        $this->db->join('safa_trips', 'safa_trips.safa_trip_id = safa_trip_internaltrips.safa_trip_id', 'left');

        $this->db->join(SLDB . '.safa_uo_contracts', "safa_uo_contracts.safa_uo_contract_id = safa_trip_internaltrips.safa_uo_contract_id", 'left');

        $this->db->join('safa_itos', 'safa_trip_internaltrips.safa_ito_id=safa_itos.safa_ito_id', 'left');

        $this->db->order_by('erp_flight_availabilities_detail.flight_date desc, erp_flight_availabilities_detail.departure_time asc');


        if (session('gov_id')) {
            $this->db->join('safa_uos', 'safa_uos.safa_uo_id = safa_trip_internaltrips.safa_uo_id');
            $this->db->select('safa_uos.' . name() . ' as uo_name');
            $this->db->join('safa_govs', 'safa_govs.erp_country_id = safa_uo_contracts.country_id');
            $this->db->where('safa_govs.safa_gov_id', session('gov_id'));
        } elseif(session('ea_id')) {
            $this->db->join('safa_uo_contracts_eas', 'safa_uo_contracts_eas.safa_uo_contract_id = safa_uo_contracts.safa_uo_contract_id');
            $this->db->where('safa_uo_contracts_eas.safa_ea_id', session('ea_id'));
        }


        $query = $this->db->get();

        return $query->result_array();
    }

    /**
     * Deprecated function
     */
    function departing_reports_($uo_id, $filters = false) {

        if ($filters) {
            //departing_twm
            if ($this->departing_twm) {

                $this->db->where("datediff(departure_datetime, now()) = ", 1);
            } else {

                $this->db->where("datediff(departure_datetime, now()) = ", 0);
            }
        }


        //erp_city_id
        if ($this->erp_city_id) {
            $this->db->where("(select erp_city_id from safa_trip_hotels, erp_hotels where safa_trip_hotels.safa_trip_id=safa_trips.`safa_trip_id` and erp_hotels.erp_hotel_id=safa_trip_hotels.erp_hotel_id order by checkin_datetime asc limit 1) = ", $this->erp_city_id, false);
        }



        if ($this->safa_trip_confirm_id) {
            $this->db->where('safa_trips.safa_trip_confirm_id', $this->safa_trip_confirm_id);
        }

        //erp_hotel_id
        if ($this->erp_hotel_id !== FALSE) {
            $this->db->where('(select erp_hotels.erp_hotel_id from safa_trip_hotels, erp_hotels where safa_trip_hotels.safa_trip_id=safa_trips.`safa_trip_id` and erp_hotels.erp_hotel_id=safa_trip_hotels.erp_hotel_id order by checkin_datetime desc limit 1) = ', $this->erp_hotel_id, false, false);
        }

        //nationality_id
        if ($this->nationality_id !== FALSE) {
            $this->db->where('safa_uo_contracts.nationality_id', $this->nationality_id);
        }

        //safa_uo_contract_id
        if ($this->safa_uo_contract_id !== FALSE) {
            $this->db->where('safa_uo_contracts.safa_uo_contract_id', $this->safa_uo_contract_id);
        }


        //safa_transporter_id
        if ($this->safa_transporter_id !== FALSE) {
            $this->db->where('(SELECT safa_transporter_id FROM safa_transporters WHERE safa_transporters.safa_transporter_id=safa_trip_externalsegments.safa_transporter_id) = ', $this->safa_transporter_id, false, false);
        }

        //erp_port_id (port name)
        if ($this->erp_port_id !== FALSE) {
            $this->db->where("(select erp_ports.erp_port_id from erp_ports, erp_port_halls where erp_ports.erp_port_id = erp_port_halls.erp_port_id and erp_port_halls.erp_port_hall_id = safa_trip_externalsegments.start_port_hall_id) = ", $this->erp_port_id, false);
        }


        if ($this->from_date !== FALSE) {
            $this->db->where('date( safa_trip_externalsegments.departure_datetime ) >= ', $this->from_date);
        } else {
            $this->db->where('date( safa_trip_externalsegments.departure_datetime ) >= ', date("Y-m-d"));
        }

        if ($this->to_date !== FALSE) {
            $this->db->where('date( safa_trip_externalsegments.departure_datetime ) <= ', $this->to_date);
        }

        //from time
        if ($this->from_time !== FALSE) {
            $this->db->where('time(safa_trip_externalsegments.departure_datetime) >= ', $this->from_time);
        }

        //to time
        if ($this->to_time !== FALSE) {
            $this->db->where('time(safa_trip_externalsegments.departure_datetime) <= ', $this->to_time);
        }

        //$from_count
        if ($this->from_count !== FALSE) {
            $this->db->where('(SELECT COUNT(*) FROM safa_trip_travellers WHERE safa_trip_travellers.safa_trip_id = safa_trips.safa_trip_id) >=', $this->from_count);
        }

        if ($this->to_count !== FALSE) {
            $this->db->where('(SELECT COUNT(*) FROM safa_trip_travellers WHERE safa_trip_travellers.safa_trip_id = safa_trips.safa_trip_id) <=', $this->to_count);
        }

        $this->db->order_by('safa_trip_externalsegments.departure_datetime', 'asc');


        $this->db->distinct();
        $this->db->select("safa_trip_externalsegments.safa_trip_externalsegment_id,
                         safa_trips.safa_trip_id as trip_id, 
                         safa_eas." . name() . " as cn,
                         (SELECT " . name() . " FROM erp_countries WHERE erp_countries.erp_country_id = safa_uo_contracts.nationality_id) as nationality,
                         (SELECT COUNT(*) FROM safa_trip_travellers WHERE safa_trip_travellers.safa_trip_id = safa_trips.safa_trip_id) AS travellers,
                         (select CONCAT(`name_ar`,\"/\", `mobile_sa`) from safa_ea_supervisors join safa_trip_supervisors on safa_trip_supervisors.safa_ea_supervisor_id = safa_ea_supervisors.safa_ea_supervisor_id where safa_trip_supervisors.safa_trip_id= safa_trips.safa_trip_id limit 1) as supervisor,
                         (select " . name() . " from safa_trip_hotels, erp_hotels where safa_trip_hotels.safa_trip_id=safa_trips.`safa_trip_id` and erp_hotels.erp_hotel_id=safa_trip_hotels.erp_hotel_id order by checkin_datetime desc limit 1) as hotel_name,
                         safa_trip_externalsegments.departure_datetime as departure_datetime,
                         (SELECT " . name() . " FROM safa_transporters WHERE safa_transporters.safa_transporter_id=safa_trip_externalsegments.safa_transporter_id) as transporter,                         
                         (SELECT erp_ports." . name() . " FROM erp_ports left join erp_port_halls ON(erp_ports.erp_port_id = erp_port_halls.erp_port_id) WHERE erp_port_halls.erp_port_hall_id = safa_trip_externalsegments.start_port_hall_id) as port_name,
                         ,(SELECT erp_port_halls." . name() . " FROM erp_port_halls WHERE erp_port_halls.erp_port_hall_id = safa_trip_externalsegments.start_port_hall_id) AS port_hall_name
                         ,trip_code
                         ," . FSDB . ".fs_status.color as status_color
                         ," . FSDB . ".fs_status." . name() . " as status_name,
                         
                        (select count(safa_group_passports.safa_group_passport_id) from safa_group_passports      where  safa_group_passports.safa_trip_id=safa_trips.safa_trip_id  and (SELECT DATE_FORMAT(FROM_DAYS(TO_DAYS(NOW())-TO_DAYS(safa_group_passports.date_of_birth)), '%Y')+0 AS age FROM safa_group_passports as inner_safa_group_passports where inner_safa_group_passports.safa_group_passport_id =safa_group_passports.safa_group_passport_id) > 12 group by (safa_group_passports.safa_trip_id)) as travellers_adult_count,
			            (select count(safa_group_passports.safa_group_passport_id) from safa_group_passports      where  safa_group_passports.safa_trip_id=safa_trips.safa_trip_id  and (SELECT DATE_FORMAT(FROM_DAYS(TO_DAYS(NOW())-TO_DAYS(safa_group_passports.date_of_birth)), '%Y')+0 AS age FROM safa_group_passports as inner_safa_group_passports where inner_safa_group_passports.safa_group_passport_id =safa_group_passports.safa_group_passport_id) > 2 and (SELECT DATE_FORMAT(FROM_DAYS(TO_DAYS(NOW())-TO_DAYS(safa_group_passports.date_of_birth)), '%Y')+0 AS age FROM safa_group_passports as inner_safa_group_passports where inner_safa_group_passports.safa_group_passport_id =safa_group_passports.safa_group_passport_id) < 12   group by (safa_group_passports.safa_trip_id)) as travellers_child_count,
			            (select count(safa_group_passports.safa_group_passport_id) from safa_group_passports      where  safa_group_passports.safa_trip_id=safa_trips.safa_trip_id  and (SELECT DATE_FORMAT(FROM_DAYS(TO_DAYS(NOW())-TO_DAYS(safa_group_passports.date_of_birth)), '%Y')+0 AS age FROM safa_group_passports as inner_safa_group_passports where inner_safa_group_passports.safa_group_passport_id =safa_group_passports.safa_group_passport_id) < 2  group by (safa_group_passports.safa_trip_id)) as travellers_infant_count
            
                        
                         
                         ,safa_trips.name as trip_name
                         
                         ", false);

        $this->db->where('safa_trip_externalsegments.safa_externalsegmenttype_id', 2);

        $this->db->where(SLDB . '.safa_trips.safa_trip_confirm_id', 1);

        $this->db->where('safa_uo_id', $uo_id);

        $this->db->join(FSDB . '.fs_flights', FSDB . '.fs_flights.fs_flight_id = ' . SLDB . '.safa_trip_externalsegments.fs_flight_id', 'left');

        $this->db->join(FSDB . '.fs_status', FSDB . '.fs_status.fs_status_id = ' . FSDB . '.fs_flights.fs_status_id', 'left');

        $this->db->join('safa_trips', 'safa_trips.safa_trip_id = safa_trip_externalsegments.safa_trip_id', 'left');


        $this->db->join('safa_uo_contracts_eas', 'safa_trips.safa_ea_id = safa_uo_contracts_eas.safa_ea_id', 'left');
        $this->db->join('safa_uo_contracts', "safa_uo_contracts.safa_uo_contract_id = safa_uo_contracts_eas.safa_uo_contract_id", 'left');
        $this->db->join('safa_eas', 'safa_uo_contracts_eas.safa_ea_id=safa_eas.safa_ea_id', 'left');


        $query = $this->db->get('safa_trip_externalsegments');

        return $query->result_array();
    }

    //end function arriving_reports

    function all_movements($filters = FALSE) {

        //from_date
        if ($this->from_date !== false) {
            $this->db->where('date( safa_internalsegments.start_datetime)  >= ', $this->from_date);
        } else {
            $this->db->where('date( safa_internalsegments.start_datetime)  >= ', date("Y-m-d"));
        }

        //to_date
        if ($this->to_date !== false) {
            $this->db->where('date( safa_internalsegments.end_datetime ) <= ', $this->to_date);
        }

        //from time
        if ($this->from_time !== FALSE) {
            $this->db->where('time(safa_internalsegments.start_datetime) >= ', $this->from_time);
        }

        //to time
        if ($this->to_time !== FALSE) {
            $this->db->where('time(safa_internalsegments.end_datetime) <= ', $this->to_time);
        }

        //from_count
        if ($this->from_count !== FALSE) {
            $this->db->where('seats_count >=', $this->from_count);
        }

        //to_count
        if ($this->to_count !== FALSE) {
            $this->db->where('seats_count <=', $this->to_count);
        }

        //safa_ito_id
        if ($this->safa_ito_id) {
            $this->db->where('safa_trip_internaltrips.safa_ito_id', $this->safa_ito_id);
        }

        /*         * ****************************************************************** */
        //safa_internalsegmentstatus_id
        //City Condition Updated By Gouda.
        if ($this->safa_internalsegmenttype_id == 1) {

            $this->db->where('safa_internalsegments.safa_internalsegmenttype_id', $this->safa_internalsegmenttype_id);
            //1 == arriving
            if ($this->end_erp_city_id) {

                //$this->db->where("(select erp_hotels.erp_city_id from erp_hotels where safa_internalsegments.erp_end_hotel_id = erp_hotels.erp_hotel_id ) = ", $this->end_erp_city_id, false);
            }
        } elseif ($this->safa_internalsegmenttype_id == 2) {

            $this->db->where('safa_internalsegments.safa_internalsegmenttype_id', $this->safa_internalsegmenttype_id);
            //2 == departing
            if ($this->erp_city_id) {

                //$this->db->where("(select erp_hotels.erp_city_id from erp_hotels where safa_internalsegments.erp_start_hotel_id = erp_hotels.erp_hotel_id ) = ", $this->erp_city_id, false);
            }
        } elseif ($this->safa_internalsegmenttype_id == 3) {

            $this->db->where('safa_internalsegments.safa_internalsegmenttype_id', $this->safa_internalsegmenttype_id);
            //3 == mazar
            if ($this->erp_city_id) {

                //$this->db->where("(select erp_hotels.erp_city_id from erp_hotels where safa_internalsegments.erp_start_hotel_id = erp_hotels.erp_hotel_id ) = ", $this->erp_city_id, false);
            }
        } elseif ($this->safa_internalsegmenttype_id == 4) {

            //4 == movement
            $this->db->where('safa_internalsegments.safa_internalsegmenttype_id', $this->safa_internalsegmenttype_id);
            if ($this->erp_city_id) {

                //$this->db->where("(select erp_hotels.erp_city_id from erp_hotels where safa_internalsegments.erp_start_hotel_id = erp_hotels.erp_hotel_id ) = ", $this->erp_city_id, false);
            }

            if ($this->end_erp_city_id) {

                //$this->db->where("(select erp_hotels.erp_city_id from erp_hotels where safa_internalsegments.erp_end_hotel_id = erp_hotels.erp_hotel_id ) = ", $this->end_erp_city_id, false);
            }
        }



        if ($this->erp_city_id) {
            $this->db->having("start_city_id", $this->erp_city_id, false);
        }
        if ($this->end_erp_city_id) {
            $this->db->having("end_city_id", $this->end_erp_city_id, false);
        }

        /*         * ******************************************************************** */




        //operator_reference
        if ($this->operator_reference) {
            $this->db->like('operator_reference', $this->operator_reference);
        }


        if ($this->safa_uo_contract_id) {
            $this->db->where('safa_uo_contracts.safa_uo_contract_id', $this->safa_uo_contract_id);
        }



        if ($this->from_erp_hotel_id) {
            $this->db->where('safa_internalsegments.erp_start_hotel_id', $this->from_erp_hotel_id);
        }

        if ($this->to_erp_hotel_id) {
            $this->db->where('safa_internalsegments.erp_end_hotel_id', $this->to_erp_hotel_id);
        }

        if ($this->erp_port_id) {
            $this->db->where('safa_internalsegments.erp_port_id', $this->erp_port_id);
        }


        if ($this->safa_internalsegmentestatus_id) {
            $this->db->where("safa_internalsegmentestatus_id", $this->safa_internalsegmentestatus_id);
        }




        /*         * ************************* */
        if ($this->uo_id) {
            $this->db->where('safa_uo_contracts.safa_uo_id', $this->uo_id);


            if ($this->uo_user_id)
                $this->db->where('safa_internalsegments.safa_uo_user_id', $this->uo_user_id);
            /*             * ***************************************************** */
        } else if ($this->ito_id) {
            $this->db->where('safa_itos.safa_ito_id', $this->ito_id);
            if ($this->has_no_bus) {
                $this->db->join('safa_internalsegments_drivers_and_buses', 'safa_internalsegments.safa_internalsegment_id = safa_internalsegments_drivers_and_buses.safa_internalsegments_id', 'left');
                $this->db->where('safa_internalsegments_drivers_and_buses.safa_internalsegments_id is null');
            }
        } else if ($this->ea_id) {
            $this->db->where('(safa_trip_internaltrips.safa_ea_id=' . $this->ea_id . ' or safa_uo_contracts_eas.safa_ea_id=' . $this->ea_id . ')');
        }


        $this->db->where("(safa_trip_internaltrips.deleted IS NULL OR safa_trip_internaltrips.deleted=0)");


        //(select erp_port_hall_id FROM erp_port_halls, erp_ports WHERE erp_port_halls.erp_port_id = safa_internalsegments.erp_port_id AND erp_ports.erp_port_id = erp_port_halls.erp_port_id) as erp_port_hall_id
        $this->db->distinct();
        $this->db->select("safa_internalsegments.*
            
        					, (SELECT erp_cities.erp_city_id
                                FROM erp_hotels left join erp_cities
                                ON (erp_hotels.erp_city_id = erp_cities.erp_city_id)
                                WHERE erp_hotels.erp_hotel_id = safa_internalsegments.erp_start_hotel_id
                                LIMIT 1) as start_city_id
                                
                            , (SELECT erp_cities.name_ar
                                FROM erp_hotels left join erp_cities
                                ON (erp_hotels.erp_city_id = erp_cities.erp_city_id)
                                WHERE erp_hotels.erp_hotel_id = safa_internalsegments.erp_start_hotel_id
                                LIMIT 1) as start_city_name
                                
                             , (SELECT erp_cities.erp_city_id
                                FROM erp_hotels left join erp_cities
                                ON (erp_hotels.erp_city_id = erp_cities.erp_city_id)
                                WHERE erp_hotels.erp_hotel_id = safa_internalsegments.erp_end_hotel_id
                                LIMIT 1) as end_city_id
                                
                             , (SELECT erp_cities.name_ar
                                FROM erp_hotels left join erp_cities
                                ON (erp_hotels.erp_city_id = erp_cities.erp_city_id)
                                WHERE erp_hotels.erp_hotel_id = safa_internalsegments.erp_end_hotel_id
                                LIMIT 1) as end_city_name
                                                       
                            
                            , (select safa_itos." . name() . " from safa_itos where safa_trip_internaltrips.safa_ito_id = safa_itos.safa_ito_id LIMIT 1) as operator_name
                            , safa_trip_internaltrips.operator_reference
                            , safa_internalsegmenttypes." . name() . " as segment_name
                            , (SELECT safa_internalsegment_log.safa_uo_agent_id FROM safa_internalsegment_log WHERE safa_internalsegment_log.safa_internalsegment_id = safa_internalsegments.safa_internalsegment_id order by safa_internalsegment_log_id desc limit 1) as safa_uo_agent_id
                            , (SELECT safa_internalsegment_log.notes FROM safa_internalsegment_log WHERE safa_internalsegment_log.safa_internalsegment_id = safa_internalsegments.safa_internalsegment_id order by safa_internalsegment_log_id desc limit 1) as safa_uo_agent_notes
                            
                            , safa_itos.safa_ito_id
                            , (SELECT " . name() . " FROM safa_internalsegmentstatus WHERE safa_internalsegmentstatus_id = safa_internalsegmentestatus_id) as status
                            , ( select safa_trips.safa_trip_id from safa_trips where safa_trips.safa_trip_id=safa_trip_internaltrips.safa_trip_id)  as trip_id
                            , ( select safa_trips.name from safa_trips where safa_trips.safa_trip_id=safa_trip_internaltrips.safa_trip_id) as trip_name, 
                            (select count(safa_group_passports.safa_group_passport_id) from safa_group_passports      where  safa_group_passports.safa_trip_id=safa_trips.safa_trip_id  group by (safa_group_passports.safa_trip_id)) as individuals_count, 
            				(select count(safa_group_passports.safa_group_passport_id) from safa_group_passports      where  safa_group_passports.safa_trip_id=safa_trips.safa_trip_id and safa_group_passports.dpn_serial_no=0 group by (safa_group_passports.safa_trip_id)) as passports_count

            				,(select CONCAT(`name_ar`,\"/\", `mobile_sa`) from safa_ea_supervisors join safa_trip_supervisors on safa_trip_supervisors.safa_ea_supervisor_id = safa_ea_supervisors.safa_ea_supervisor_id where safa_trip_supervisors.safa_trip_id= safa_trips.safa_trip_id limit 1) as supervisor,                         
                          	,(select `name_ar` from safa_ea_supervisors join safa_trip_supervisors on safa_trip_supervisors.safa_ea_supervisor_id = safa_ea_supervisors.safa_ea_supervisor_id where safa_trip_supervisors.safa_trip_id= safa_trips.safa_trip_id limit 1) as supervisor_name,                         
                          	,(select `mobile_sa` from safa_ea_supervisors join safa_trip_supervisors on safa_trip_supervisors.safa_ea_supervisor_id = safa_ea_supervisors.safa_ea_supervisor_id where safa_trip_supervisors.safa_trip_id= safa_trips.safa_trip_id limit 1) as supervisor_phone,                         
                          	
            				`erp_flight_availabilities_detail`.`flight_number`,
                          	`safa_transporters`.  " . name() . " as transporter_name
                            ,`safa_trip_internaltrips`.`buses_count`
            				, safa_uo_contracts." . name() . " as contract_name
                            , safa_itos." . name() . " as operator_name
                            , safa_trip_internaltrips.operator_reference
                            , safa_internalsegmenttypes." . name() . " as segment_name
                            , safa_internalsegments.safa_internalsegmenttype_id as segment_id
                            , safa_itos.safa_ito_id
                            , safa_uos." . name() . " as uo_name
                            , safa_trip_internaltrips.trip_title, safa_trip_internaltrips.trip_supervisors, safa_trip_internaltrips.trip_supervisors_phone
                            , safa_trip_internaltrips.adult_seats, safa_trip_internaltrips.child_seats, safa_trip_internaltrips.baby_seats
                            
                         	, safa_trip_internaltrips.trip_title 
	                        , CONCAT(`safa_trip_internaltrips`.`trip_supervisors`,\"/\", `safa_trip_internaltrips`.`trip_supervisors_phone`)  as trip_supervisors
	                        , `safa_trip_internaltrips`.`trip_supervisors`  as trip_supervisors_name
	                       , `safa_trip_internaltrips`.`trip_supervisors_phone`  as trip_supervisors_phone
	                       
	                        ,  safa_trip_internaltrips.serial as safa_trip_internaltrip_serial
                            ,  erp_flight_availabilities_detail.fs_flight_id
                        	, safa_transporters.code as safa_transporters_code
                        	, safa_trip_internaltrips.confirmation_number 
                            ", false);

        $this->db->from('safa_internalsegments');
        $this->db->join('safa_trip_internaltrips', 'safa_internalsegments.safa_trip_internaltrip_id=safa_trip_internaltrips.safa_trip_internaltrip_id');

        $this->db->join('safa_trips', 'safa_trips.safa_trip_id=safa_trip_internaltrips.safa_trip_id', 'left');

        $this->db->join('safa_uo_contracts', 'safa_trip_internaltrips.safa_uo_contract_id = safa_uo_contracts.safa_uo_contract_id', 'left');
        $this->db->join('safa_uos', 'safa_uos.safa_uo_id = safa_uo_contracts.safa_uo_id', 'left');
        $this->db->join('safa_uo_contracts_eas', 'safa_uo_contracts_eas.safa_uo_contract_id = safa_uo_contracts.safa_uo_contract_id', 'left');


        $this->db->join('safa_internalsegmenttypes', 'safa_internalsegments.safa_internalsegmenttype_id=safa_internalsegmenttypes.safa_internalsegmenttype_id', 'left');


        $this->db->join('erp_flight_availabilities', 'erp_flight_availabilities.erp_flight_availability_id=safa_trip_internaltrips.erp_flight_availability_id', 'left');
        $this->db->join('erp_flight_availabilities_detail', 'erp_flight_availabilities_detail.erp_flight_availability_id = erp_flight_availabilities.erp_flight_availability_id', 'left');
        $this->db->join('safa_transporters', 'safa_transporters.code = erp_flight_availabilities_detail.airline_code', 'left');

        //$this->db->join(FSDB . '.fs_airlines', FSDB . '.fs_airlines.iata = ' . SLDB . '.erp_flight_availabilities_detail.airline_code', 'left');
        //$this->db->join(FSDB . '.fs_flights', FSDB . '.fs_flights.fs_flight_id = ' . SLDB . '.erp_flight_availabilities_detail.fs_flight_id', 'left');
        //$this->db->join(FSDB . '.fs_status', FSDB . '.fs_status.fs_status_id = ' . FSDB . '.fs_flights.fs_status_id', 'left');


        $this->db->where('(safa_trip_internaltrips.safa_ito_id = safa_itos.safa_ito_id OR safa_trip_internaltrips.safa_ito_id IS NULL)');

        //By Gouda
        $this->db->where("safa_trip_internaltrips.operator_reference!=''");

        $this->db->group_by('safa_internalsegments.safa_internalsegment_id');

        if (session('gov_id')) {
            $this->db->select('safa_uos.' . name() . ' as uo_name');
            $this->db->join('safa_govs', 'safa_govs.erp_country_id = safa_uo_contracts.country_id');
            $this->db->where('safa_govs.safa_gov_id', session('gov_id'));
        }


        $this->db->from('safa_itos');



        $this->db->order_by('start_datetime', 'asc');

        $query = $this->db->get();

        //echo $this->db->last_query();exit();

        return $query->result_array();
    }

    function all_movements_ito($ito_id, $direction = false, $arr_filters = false, $dep_filters = false, $without_driver = false) {

        if ($without_driver) {
            $this->db->where("ito_driver_info IS NULL", NULL);
        }

        if ($direction) {

            $this->db->where("safa_internalsegmenttypes.safa_internalsegmenttype_id", "$direction");
        }

        //use these filters to get something about this day or tomorowo
        if ($arr_filters) {
            //arriving_twm
            if ($this->arriving_twm) {

                $this->db->where("datediff(start_datetime, now()) = ", 1);
            } else {

                $this->db->where("datediff(start_datetime, now()) = ", 0);
            }
        }


        if ($dep_filters) {
            //departing_twm
            if ($this->departing_twm) {

                $this->db->where("datediff(end_datetime, now()) = ", 1);
            } else {

                $this->db->where("datediff(end_datetime, now()) = ", 0);
            }
        }


        //City Condition Updated By Gouda.

        /*
          if ($this->erp_city_id) {
          $this->db->where("(select erp_city_id from safa_trip_hotels, erp_hotels where safa_trip_hotels.safa_trip_id=safa_trips.`safa_trip_id` and erp_hotels.erp_hotel_id=safa_trip_hotels.erp_hotel_id order by checkin_datetime asc limit 1) = ", $this->erp_city_id, false);
          }
         */

        if ($this->erp_city_id) {
            $this->db->having("start_city_id", $this->erp_city_id, false);
        }
        if ($this->end_erp_city_id) {
            $this->db->having("end_city_id", $this->end_erp_city_id, false);
        }

        //from_date
        if ($this->from_date !== false) {
            $this->db->where('start_datetime >= ', $this->from_date);
        }

        //to_date
        if ($this->to_date !== false) {
            $this->db->where('start_datetime <= ', $this->to_date);
        }

        //from time
        if ($this->from_time !== false) {
            $this->db->where('time(start_datetime) >= ', $this->from_time);
        }

        //to time
        if ($this->to_time !== false) {
            $this->db->where('time(start_datetime) <= ', $this->to_time);
        }

        //from_count
        if ($this->from_count !== false) {
            $this->db->where('seats_count >=', $this->from_count);
        }
        //to_count
        if ($this->to_count !== false) {
            $this->db->where('seats_count <=', $this->to_count);
        }

        $this->db->where('safa_trip_internaltrips.safa_ito_id', $ito_id);
        
        if ($this->uo_id) {
            $this->db->where('safa_uo_contracts.safa_uo_id', $this->uo_id);
        }

        //safa_internalsegmentstatus_id
        if ($this->safa_internalsegmenttype_id) {
            $this->db->where('safa_internalsegments.safa_internalsegmenttype_id', $this->safa_internalsegmenttype_id);
        }

        //operator_reference
        if ($this->operator_reference) {
            $this->db->where('operator_reference', $this->operator_reference);
        }


        if ($this->safa_uo_contract_id) {
            $this->db->where('safa_uo_contracts.safa_uo_contract_id', $this->safa_uo_contract_id);
        }

        if ($this->from_erp_hotel_id) {
            $this->db->where('safa_internalsegments.erp_start_hotel_id', $this->from_erp_hotel_id);
        }

        if ($this->to_erp_hotel_id) {
            $this->db->where('safa_internalsegments.erp_end_hotel_id', $this->to_erp_hotel_id);
        }

        if ($this->erp_port_id) {
            $this->db->where('safa_internalsegments.erp_port_id', $this->erp_port_id);
        }

        //    safa_trips.safa_trip_id as trip_id,
        //    safa_uo_contracts.".name()." as cn,
        //    (SELECT ".name()." FROM erp_countries WHERE erp_countries.erp_country_id = safa_uo_contracts.nationality_id) as nationality,
        //    (SELECT COUNT(*) FROM safa_trip_travellers WHERE safa_trip_travellers.safa_trip_id = safa_trips.safa_trip_id) AS travellers,
        //    (select CONCAT(`name_ar`,\"/\", `mobile_sa`) from safa_trip_supervisors where safa_trip_id= safa_trips.safa_trip_id limit 1) as supervisor,
        //    (select ".name()." from safa_trip_hotels, erp_hotels where safa_trip_hotels.safa_trip_id=safa_trips.`safa_trip_id` and erp_hotels.erp_hotel_id=safa_trip_hotels.erp_hotel_id order by checkin_datetime asc limit 1) as hotel_name,
        //    safa_trip_externalsegments.arrival_datetime,
        //    (SELECT ".name()." FROM safa_transporters WHERE safa_transporters.safa_transporter_id=safa_trip_externalsegments.safa_transporter_id) as transporter,
        //    (SELECT erp_ports.".name()." FROM erp_ports left join erp_port_halls ON(erp_ports.erp_port_id = erp_port_halls.erp_port_id) WHERE erp_port_halls.erp_port_hall_id = safa_trip_externalsegments.end_port_hall_id) as port_name,
        //        trip_code

        $this->db->distinct();
        $this->db->select("safa_internalsegments.*
            
                            , (SELECT erp_cities.name_ar
                                FROM erp_hotels left join erp_cities
                                ON (erp_hotels.erp_city_id = erp_cities.erp_city_id)
                                WHERE erp_hotels.erp_hotel_id = safa_internalsegments.erp_start_hotel_id
                                LIMIT 1) as start_city_name
                                
                            , (SELECT erp_cities.erp_city_id
                                FROM erp_hotels left join erp_cities
                                ON (erp_hotels.erp_city_id = erp_cities.erp_city_id)
                                WHERE erp_hotels.erp_hotel_id = safa_internalsegments.erp_start_hotel_id
                                LIMIT 1) as start_city_id
                                
                             , (SELECT erp_cities.name_ar
                                FROM erp_hotels left join erp_cities
                                ON (erp_hotels.erp_city_id = erp_cities.erp_city_id)
                                WHERE erp_hotels.erp_hotel_id = safa_internalsegments.erp_end_hotel_id
                                LIMIT 1) as end_city_name

                             , (SELECT erp_cities.erp_city_id
                                FROM erp_hotels left join erp_cities
                                ON (erp_hotels.erp_city_id = erp_cities.erp_city_id)
                                WHERE erp_hotels.erp_hotel_id = safa_internalsegments.erp_end_hotel_id
                                LIMIT 1) as end_city_id
                                
                            , safa_uo_contracts." . name() . " as contract_name
                            , safa_itos." . name() . " as operator_name
                            , safa_trip_internaltrips.operator_reference
                            , safa_internalsegmenttypes." . name() . " as segment_name
                            , safa_internalsegments.safa_internalsegmenttype_id as segment_id
                            , safa_itos.safa_ito_id
                            , safa_uos." . name() . " as uo_name
                            
                             
                            , (select CONCAT(`name_ar`,\"/\", `mobile_sa`) from safa_ea_supervisors join safa_trip_supervisors on safa_trip_supervisors.safa_ea_supervisor_id = safa_ea_supervisors.safa_ea_supervisor_id where safa_trip_supervisors.safa_trip_id= safa_trips.safa_trip_id limit 1) as supervisor
                            , (SELECT " . name() . " FROM erp_countries WHERE erp_countries.erp_country_id = safa_uo_contracts.nationality_id) as nationality
                            , (SELECT COUNT(*) FROM safa_trip_travellers WHERE safa_trip_travellers.safa_trip_id = safa_trips.safa_trip_id) AS travellers
                            
                            
                            ,(select " . name() . " from safa_internalsegments, erp_hotels, safa_trip_internaltrips safa_trip_internaltrips_inner where safa_trip_internaltrips_inner.safa_trip_internaltrip_id = safa_trip_internaltrips.safa_trip_internaltrip_id and safa_trip_internaltrips.safa_trip_internaltrip_id = safa_internalsegments.safa_trip_internaltrip_id AND erp_hotels.erp_hotel_id=safa_internalsegments.erp_end_hotel_id order by safa_internalsegments.start_datetime desc limit 1) as hotel_name,
                         
                            ," . SLDB . ".erp_flight_availabilities_detail.arrival_date, 
                            " . SLDB . ".erp_flight_availabilities_detail.arrival_time  , 
                            CONCAT(`erp_flight_availabilities_detail`.`flight_number`,\" \", `erp_flight_availabilities_detail`.`airline_code`) as flight_number
                            
	                         , (SELECT " . SLDB . ".erp_ports." . name() . " FROM " . SLDB . ".erp_ports  WHERE erp_ports.erp_port_id = erp_flight_availabilities_detail.erp_port_id_from) as start_port_name
	                         , (SELECT " . SLDB . ".erp_port_halls." . name() . " FROM " . SLDB . ".erp_port_halls  WHERE erp_port_halls.erp_port_hall_id = erp_flight_availabilities_detail.erp_port_hall_id_from) as start_port_hall_name
	                         
	                        ,(SELECT " . SLDB . ".erp_ports." . name() . " FROM " . SLDB . ".erp_ports  WHERE erp_ports.erp_port_id = erp_flight_availabilities_detail.erp_port_id_to) as end_port_name
	                        , (SELECT " . SLDB . ".erp_port_halls." . name() . " FROM " . SLDB . ".erp_port_halls  WHERE erp_port_halls.erp_port_hall_id = erp_flight_availabilities_detail.erp_port_hall_id_to) as end_port_hall_name
                         
                        
                            , safa_internalsegments.start_datetime arrival_datetime
                            , safa_internalsegments.end_datetime departure_datetime
                            , (SELECT " . name() . " FROM safa_internalsegmentstatus WHERE safa_internalsegmentstatus_id = safa_internalsegmentestatus_id limit 1) as status
                            , safa_trips.safa_trip_id as trip_id 
                            , safa_trips.name as trip_name
                            
                            , safa_trip_internaltrips.trip_title 
                        	, CONCAT(`safa_trip_internaltrips`.`trip_supervisors`,\"/\", `safa_trip_internaltrips`.`trip_supervisors_phone`)  as trip_supervisors
                        	,erp_flight_availabilities_detail.seats_count
                        	
                            ", false);





        $this->db->from('safa_internalsegments');
        $this->db->join('safa_trip_internaltrips', 'safa_internalsegments.safa_trip_internaltrip_id=safa_trip_internaltrips.safa_trip_internaltrip_id');

        $this->db->join('safa_trips', 'safa_trips.safa_trip_id=safa_trip_internaltrips.safa_trip_id', 'left');

        $this->db->join('safa_uo_contracts', 'safa_trip_internaltrips.safa_uo_contract_id = safa_uo_contracts.safa_uo_contract_id', 'left');
        $this->db->join('safa_uos', 'safa_uos.safa_uo_id = safa_uo_contracts.safa_uo_id', 'left');


        $this->db->join('safa_internalsegmenttypes', 'safa_internalsegments.safa_internalsegmenttype_id=safa_internalsegmenttypes.safa_internalsegmenttype_id', 'left');

        $this->db->where('(safa_trip_internaltrips.safa_ito_id = safa_itos.safa_ito_id)');

        $this->db->group_by('safa_internalsegments.safa_internalsegment_id');

        $this->db->join('erp_flight_availabilities', 'erp_flight_availabilities.erp_flight_availability_id=safa_trip_internaltrips.erp_flight_availability_id', 'left');
        $this->db->join('erp_flight_availabilities_detail', 'erp_flight_availabilities_detail.erp_flight_availability_id = erp_flight_availabilities.erp_flight_availability_id', 'left');
        $this->db->join('safa_transporters', 'safa_transporters.code = erp_flight_availabilities_detail.airline_code', 'left');

        //$this->db->join(FSDB . '.fs_flights', FSDB . '.fs_flights.fs_flight_id = ' . SLDB . '.erp_flight_availabilities_detail.fs_flight_id', 'left');
        //$this->db->join(FSDB . '.fs_status', FSDB . '.fs_status.fs_status_id = ' . FSDB . '.fs_flights.fs_status_id', 'left');
        //$this->db->join(FSDB . '.fs_airlines', FSDB . '.fs_airlines.fs_airline_id = ' . SLDB . '.erp_flight_availabilities_detail.erp_airline_id', 'left');


        $this->db->from('safa_itos');


        $this->db->order_by('start_datetime', 'desc');


        $query = $this->db->get();

        //echo $this->db->last_query(); exit;

        return $query->result_array();
    }

    function uos_options($ito_id) {

        //select safa_uos.safa_uo_id, safa_uos.name_ar
        //
		//from safa_uo_contracts_itos
        //
		//left join safa_uo_contracts
        //
		//on safa_uo_contracts.safa_uo_contract_id = safa_uo_contracts_itos.safa_uo_contract_id
        //
		//left join safa_uos
        //
		//on safa_uos.safa_uo_id = safa_uo_contracts.safa_uo_id
        //
		//where safa_uo_contracts_itos.safa_ito_id = 1
        //
		//group by safa_uos.safa_uo_id

        $ds = $this->db->select('safa_uos.safa_uo_id, safa_uos.' . name())->where('safa_uo_contracts_itos.safa_ito_id', $ito_id)->join('safa_uo_contracts', 'safa_uo_contracts.safa_uo_contract_id = safa_uo_contracts_itos.safa_uo_contract_id')->join('safa_uos', 'safa_uos.safa_uo_id = safa_uo_contracts.safa_uo_id')->group_by('safa_uos.safa_uo_id')->get('safa_uo_contracts_itos')->result();

        $arr = array('0' => lang('global_select_from_menu'));


        if (count($ds)) {
            foreach ($ds as $row) {

                $arr[$row->safa_uo_id] = $row->{name()};
            }
        }

        return $arr;
    }

    function uo_contracts_options($ito_id) {

        //SELECT *
        //FROM `safa_uo_contracts_itos`
        //JOIN `safa_uo_contracts` ON `safa_uo_contracts`.`safa_uo_contract_id` = `safa_uo_contracts_itos`.`safa_uo_contract_id`
        //WHERE `safa_uo_contracts_itos`.`safa_ito_id` =1
        //SELECT safa_uo_contracts.safa_uo_contract_id, safa_uo_id, name_ar, name_la
        //FROM safa_ea_itos
        //LEFT JOIN safa_uo_contracts ON safa_uo_contracts.safa_uo_contract_id = safa_ea_itos.safa_uo_contract_id
        //WHERE safa_ito_id =1

        $ds = $this->db->where('safa_ito_id', $ito_id)->join('safa_uo_contracts', 'safa_uo_contracts.safa_uo_contract_id = safa_uo_contracts_itos.safa_uo_contract_id')->get('safa_uo_contracts_itos')->result();

        $arr = array('0' => lang('global_select_from_menu'));

        foreach ($ds as $row) {

            $arr[$row->safa_uo_contract_id] = $row->{name()};
        }

        return $arr;
    }

    function get_hotels() {
        $this->db->select("erp_hotel_id, " . name());

        $hotels = $this->db->get('erp_hotels');

        $indexed_r = array();

        foreach ($hotels->result_array() as $item):

            $indexed_r[$item['erp_hotel_id']] = $item[name()];

        endforeach;

        return $indexed_r;
    }

    function get_related_hotels_to_contracts($uo_id, $city_id = false) {


        $this->db->order_by("erp_hotels." . name(), 'asc');
        $this->db->select(" erp_hotels." . name() . ", erp_hotels.erp_hotel_id", false);
        $this->db->join('safa_trip_hotels', "erp_hotels.erp_hotel_id=safa_trip_hotels.erp_hotel_id", 'left');
        $this->db->join('safa_trips', 'safa_trip_hotels.safa_trip_id = safa_trips.safa_trip_id', 'left');

        //        $this->db->join('safa_ea_packages', 'safa_ea_packages.safa_ea_package_id = safa_trips.safa_ea_package_id', 'left');
        //        $this->db->join('safa_uo_contracts_eas', 'safa_uo_contracts_eas.safa_uo_contract_ea_id = safa_ea_packages.safa_uo_contract_ea_id', 'left');
        //        $this->db->join('safa_uo_contracts', "safa_uo_contracts.safa_uo_contract_id = safa_uo_contracts_eas.safa_uo_contract_id", 'left');

        $this->db->join('safa_group_passports', 'safa_group_passports.safa_trip_id = safa_trips.safa_trip_id', 'left');
        $this->db->join('safa_group_passport_accommodation', 'safa_group_passport_accommodation.safa_group_passport_id = safa_group_passports.safa_group_passport_id', 'left');
        $this->db->join('safa_package_periods', 'safa_package_periods.safa_package_periods_id = safa_group_passport_accommodation.safa_package_periods_id', 'left');
        $this->db->join('safa_packages', 'safa_packages.safa_package_id = safa_package_periods.safa_package_id', 'left');


        if ($uo_id) {
            //$this->db->where('safa_uo_id', $uo_id);
            $this->db->where('safa_packages.erp_company_type_id', 2);
            $this->db->where('safa_packages.erp_company_id', $uo_id);
        }

        if ($city_id) {
            $this->db->where('erp_hotels.erp_city_id', $city_id);
        }

        $query = $this->db->get('erp_hotels');

        $hotels = $query->result_array();

        $indexed_r = array('0' => lang('global_select_from_menu'));

        foreach ($hotels as $item):

            $indexed_r[$item['erp_hotel_id']] = $item[name()];

        endforeach;

        return $indexed_r;
    }

    function get_related_hotels_to_itos($ito_id, $city_id = false) {

        $this->db->order_by("erp_hotels." . name(), 'asc');

        $this->db->select(" distinct(safa_trip_hotels.erp_hotel_id)
                            ,erp_hotels." . name() . ", erp_hotels.erp_hotel_id", false);

        if ($ito_id) {
            $this->db->where('safa_trip_internaltrips.safa_ito_id', $ito_id);
        }

        if ($city_id) {
            $this->db->where('erp_hotels.erp_city_id', $city_id);
        }

        $this->db->join('safa_trip_hotels', "safa_trip_hotels.safa_trip_id = safa_trip_internaltrips.safa_trip_id", 'left');

        $this->db->join('erp_hotels', "erp_hotels.erp_hotel_id = safa_trip_hotels.erp_hotel_id", 'left');

        $query = $this->db->get('safa_trip_internaltrips');

        //echo($this->db->last_query());

        $hotels = $query->result_array();

        $indexed_r = array('0' => lang('global_select_from_menu'));

        foreach ($hotels as $item):

            $indexed_r[$item['erp_hotel_id']] = $item[name()];

        endforeach;

        return $indexed_r;
    }

    function get_places() {

        $this->db->select("safa_tourismplace_id, " . name());

        $places = $this->db->get('safa_tourismplaces');

        $indexed_r = array();

        foreach ($places->result_array() as $item):

            $indexed_r[$item['safa_tourismplace_id']] = $item[name()];

        endforeach;

        return $indexed_r;
    }

    function get_ports() {

        $this->db->select("erp_port_id, " . name());

        $ports = $this->db->get('erp_ports');

        $indexed_r = array();

        foreach ($ports->result_array() as $item):

            $indexed_r[$item['erp_port_id']] = $item[name()];

        endforeach;

        return $indexed_r;
    }

    /*
      function get_ports() {

      $this->db->select("erp_port_halls.erp_port_hall_id, erp_port_halls.".name());

      $this->db->join('erp_ports', 'erp_port_halls.erp_port_id = erp_ports.erp_port_id');

      $ports = $this->db->get('erp_port_halls');

      $indexed_r = array();

      foreach ($ports->result_array() as $item):

      $indexed_r[$item['erp_port_hall_id']] = $item[name()];

      endforeach;

      return $indexed_r;
      }
     */

    function get_endtime($time) {
        $diff = $this->db->query("select timediff('$time', now()) as x")->result_array();
        return $diff[0]['x'];
        //       exit();
    }

    function counts($type, $from, $to) {
        if (session('gov_id')) {

            switch ($type) {
                case 'ea':
                    $query = $this->db->query("SELECT Count(safa_trip_internaltrips.safa_trip_internaltrip_id) AS trip_count,
(sum(safa_trip_internaltrips.adult_seats) + sum(safa_trip_internaltrips.child_seats) + sum(safa_trip_internaltrips.baby_seats)) AS total_passports,
safa_eas.name_ar as name
FROM safa_trip_internaltrips
JOIN safa_internalsegments ON safa_internalsegments.safa_trip_internaltrip_id = safa_trip_internaltrips.safa_uo_contract_id
JOIN safa_uo_contracts ON safa_uo_contracts.safa_uo_contract_id = safa_trip_internaltrips.safa_uo_contract_id
JOIN safa_uo_contracts_eas ON safa_uo_contracts_eas.safa_uo_contract_id = safa_uo_contracts.safa_uo_contract_id
JOIN safa_eas ON safa_uo_contracts_eas.safa_ea_id = safa_eas.safa_ea_id 
JOIN safa_govs ON safa_govs.erp_country_id = safa_uo_contracts.country_id
WHERE safa_internalsegments.safa_internalsegmenttype_id = 1 AND
safa_internalsegments.start_datetime BETWEEN date('$from') AND date('$to')  AND
safa_govs.safa_gov_id = ".session('gov_id')."
GROUP BY safa_uo_contracts_eas.safa_ea_id");

                    break;
                case 'uo':
$query = $this->db->query("SELECT count(safa_trip_internaltrips.safa_trip_internaltrip_id) as trip_count,
(sum(safa_trip_internaltrips.adult_seats) + sum(safa_trip_internaltrips.child_seats) + sum(safa_trip_internaltrips.baby_seats)) as total_passports,
safa_uos.name_ar as name
FROM safa_trip_internaltrips
JOIN safa_internalsegments ON safa_internalsegments.safa_trip_internaltrip_id = safa_trip_internaltrips.safa_uo_contract_id
JOIN safa_uo_contracts ON safa_uo_contracts.safa_uo_contract_id = safa_trip_internaltrips.safa_uo_contract_id
JOIN safa_uos ON safa_uo_contracts.safa_uo_id = safa_uos.safa_uo_id
JOIN safa_govs ON safa_govs.erp_country_id = safa_uo_contracts.country_id
WHERE safa_internalsegments.safa_internalsegmenttype_id = 1 AND
safa_internalsegments.start_datetime BETWEEN date('$from') AND date('$to')  AND
safa_govs.safa_gov_id = ".session('gov_id')."
GROUP BY safa_uo_contracts.safa_uo_id");

                    break;
                case 'trans':
$query = $this->db->query("SELECT Count(safa_trip_internaltrips.safa_trip_internaltrip_id) AS trip_count,
(sum(safa_trip_internaltrips.adult_seats) + sum(safa_trip_internaltrips.child_seats) + sum(safa_trip_internaltrips.baby_seats)) AS total_passports,
erp_transportertypes.name_ar AS name
FROM safa_trip_internaltrips
JOIN safa_internalsegments ON safa_internalsegments.safa_trip_internaltrip_id = safa_trip_internaltrips.safa_uo_contract_id
JOIN safa_uo_contracts ON safa_uo_contracts.safa_uo_contract_id = safa_trip_internaltrips.safa_uo_contract_id
JOIN safa_govs ON safa_govs.erp_country_id = safa_uo_contracts.country_id
INNER JOIN erp_transportertypes ON safa_trip_internaltrips.erp_transportertype_id = erp_transportertypes.erp_transportertype_id
WHERE safa_internalsegments.safa_internalsegmenttype_id = 1 AND
safa_internalsegments.start_datetime BETWEEN date('$from') AND date('$to')  AND
safa_govs.safa_gov_id = ".session('gov_id')."
GROUP BY erp_transportertypes.erp_transportertype_id");

                    break;
            }

//echo $this->db->last_query();

            return $query->result();
        } else
            return array();
    }

}
