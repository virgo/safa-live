<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Hotels_nationalities_model extends CI_Model {

    public $erp_hotels_nationalities_id = FALSE;
    public $erp_nationality_id = FALSE;
    public $erp_hotels_availability_master_id = FALSE;
    public $custom_select = FALSE;
    public $limit = FALSE;
    public $offset = FALSE;
    public $order_by = FALSE;

    function __construct() {
        parent::__construct();
    }

    function get($rows_no = FALSE) {

        if ($this->custom_select !== FALSE) {
            $this->db->select('erp_hotels_availability_nationalities_id');
            $this->db->select($this->custom_select);
        }

        if ($this->erp_hotels_nationalities_id !== FALSE)
            $this->db->where('erp_hotels_availability_nationalities.erp_hotels_availability_nationalities_id', $this->erp_hotels_nationalities_id);

        if ($this->erp_nationality_id !== FALSE)
            $this->db->where('erp_hotels_availability_nationalities.erp_nationality_id', $this->erp_nationality_id);

        if ($this->erp_hotels_availability_master_id !== FALSE)
            $this->db->where('erp_hotels_availability_nationalities.erp_hotels_availability_master_id', $this->erp_hotels_availability_master_id);


        $this->db->select("erp_hotels_availability_nationalities.*, erp_nationalities." . name() . " as erp_nationality_name", false);

        $this->db->join('erp_nationalities', 'erp_nationalities.erp_nationality_id=erp_hotels_availability_nationalities.erp_nationality_id', 'left');


        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('erp_hotels_availability_nationalities');
        if ($rows_no)
            return $query->num_rows();

        if ($this->erp_hotels_nationalities_id)
            return $query->row();
        else
            return $query->result();
    }

    function save() {
        if ($this->erp_hotels_nationalities_id !== FALSE)
            $this->db->set('erp_hotels_availability_nationalities.erp_hotels_availability_nationalities_id', $this->erp_hotels_nationalities_id);

        if ($this->erp_nationality_id !== FALSE)
            $this->db->set('erp_hotels_availability_nationalities.erp_nationality_id', $this->erp_nationality_id);

        if ($this->erp_hotels_availability_master_id !== FALSE)
            $this->db->set('erp_hotels_availability_nationalities.erp_hotels_availability_master_id', $this->erp_hotels_availability_master_id);



        if ($this->erp_hotels_nationalities_id) {
            $this->db->where('erp_hotels_availability_nationalities.erp_hotels_nationalities_id', $this->erp_hotels_nationalities_id)->update('erp_hotels_availability_nationalities');
        } else {
            $this->db->insert('erp_hotels_availability_nationalities');
            return $this->db->insert_id();
        }
    }

    function delete() {
        if ($this->erp_hotels_nationalities_id !== FALSE)
            $this->db->where('erp_hotels_availability_nationalities.erp_hotels_availability_nationalities_id', $this->erp_hotels_nationalities_id);

        if ($this->erp_nationality_id !== FALSE)
            $this->db->where('erp_hotels_availability_nationalities.erp_nationality_id', $this->erp_nationality_id);

        if ($this->erp_hotels_availability_master_id !== FALSE)
            $this->db->where('erp_hotels_availability_nationalities.erp_hotels_availability_master_id', $this->erp_hotels_availability_master_id);



        $this->db->delete('erp_hotels_availability_nationalities');
        return $this->db->affected_rows();
    }

    function search($rows_no = FALSE) {
        if ($this->custom_select !== FALSE) {
            $this->db->select('erp_hotels_availability_nationalities_id');
            $this->db->select($this->custom_select);
        }

        if ($this->erp_hotels_nationalities_id !== FALSE)
            $this->db->where('erp_hotels_availability_nationalities.erp_hotels_availability_nationalities_id', $this->erp_hotels_nationalities_id);

        if ($this->erp_nationality_id !== FALSE)
            $this->db->where('erp_hotels_availability_nationalities.erp_nationality_id', $this->erp_nationality_id);

        if ($this->erp_hotels_availability_master_id !== FALSE)
            $this->db->where('erp_hotels_availability_nationalities.erp_hotels_availability_master_id', $this->erp_hotels_availability_master_id);


        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('erp_hotels_availability_nationalities');
        if ($rows_no)
            return $query->num_rows();

        return $query->result();
    }

}

/* End of file hotels_nationalities_model.php */
/* Location: ./application/models/hotels_nationalities_model.php */