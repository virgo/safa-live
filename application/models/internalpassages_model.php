<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Internalpassages_model extends CI_Model {

    public $safa_internalsegment_id = FALSE;
    public $safa_internalsegmenttype_id = FALSE;
    public $safa_trip_internaltrip_id = FALSE;
    public $safa_internalsegmentestatus_id = FALSE;
    public $start_datetime = FALSE;
    public $end_datetime = FALSE;
    public $erp_port_id = FALSE;
    public $erp_start_hotel_id = FALSE;
    public $erp_end_hotel_id = FALSE;
    public $safa_tourism_place_id = FALSE;
    public $seats_count = FALSE;
    public $notes = FALSE;
    public $status_notes = FALSE;
    public $ito_driver_info = FALSE;
    public $ito_vehicle_info = FALSE;
    public $is_external = FALSE;
    public $safa_uo_user_id = FALSE;
    public $custom_select = FALSE;
    public $limit = FALSE;
    public $offset = FALSE;
    public $order_by = FALSE;

    function __construct() {
        parent::__construct();
    }

    function get($rows_no = FALSE) {

        if ($this->custom_select !== FALSE) {
            $this->db->select('safa_internalsegment_id');
            $this->db->select($this->custom_select);
        }

        $this->db->select("safa_internalsegments.*, (SELECT safa_internalsegment_log.safa_uo_agent_id FROM safa_internalsegment_log WHERE safa_internalsegment_log.safa_internalsegment_id = safa_internalsegments.safa_internalsegment_id order by safa_internalsegment_log_id desc limit 1) as safa_uo_agent_id");
        
        if ($this->safa_internalsegment_id !== FALSE)
            $this->db->where('safa_internalsegments.safa_internalsegment_id', $this->safa_internalsegment_id);

        if ($this->safa_internalsegmenttype_id !== FALSE)
            $this->db->where('safa_internalsegments.safa_internalsegmenttype_id', $this->safa_internalsegmenttype_id);

        if ($this->safa_trip_internaltrip_id !== FALSE)
            $this->db->where('safa_internalsegments.safa_trip_internaltrip_id', $this->safa_trip_internaltrip_id);

        if ($this->safa_internalsegmentestatus_id !== FALSE)
            $this->db->where('safa_internalsegments.safa_internalsegmentestatus_id', $this->safa_internalsegmentestatus_id);

        if ($this->start_datetime !== FALSE)
            $this->db->where('safa_internalsegments.start_datetime', $this->start_datetime);

        if ($this->end_datetime !== FALSE)
            $this->db->where('safa_internalsegments.end_datetime', $this->end_datetime);

        if ($this->erp_port_id !== FALSE)
            $this->db->where('safa_internalsegments.erp_port_id', $this->erp_port_id);

        if ($this->erp_start_hotel_id !== FALSE)
            $this->db->where('safa_internalsegments.erp_start_hotel_id', $this->erp_start_hotel_id);

        if ($this->erp_end_hotel_id !== FALSE)
            $this->db->where('safa_internalsegments.erp_end_hotel_id', $this->erp_end_hotel_id);

        if ($this->safa_tourism_place_id !== FALSE)
            $this->db->where('safa_internalsegments.safa_tourism_place_id', $this->safa_tourism_place_id);

        if ($this->seats_count !== FALSE)
            $this->db->where('safa_internalsegments.seats_count', $this->seats_count);

        if ($this->notes !== FALSE)
            $this->db->where('safa_internalsegments.notes', $this->notes);

        if ($this->ito_driver_info !== FALSE)
            $this->db->where('safa_internalsegments.ito_driver_info', $this->ito_driver_info);

        if ($this->ito_vehicle_info !== FALSE)
            $this->db->where('safa_internalsegments.ito_vehicle_info', $this->ito_vehicle_info);

        if ($this->is_external !== FALSE)
            $this->db->where('safa_internalsegments.is_external', $this->is_external);

        if ($this->safa_uo_user_id !== FALSE)
            $this->db->where('safa_internalsegments.safa_uo_user_id', $this->safa_uo_user_id);

        if ($this->status_notes !== FALSE)
            $this->db->where('safa_internalsegments.status_notes', $this->status_notes);

        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('safa_internalsegments');
        
        //echo $this->db->last_query(); exit;
        
        if ($rows_no)
            return $query->num_rows();

        if ($this->safa_internalsegment_id)
            return $query->row();
        else
            return $query->result();
    }

    function get_hotels($rows_no = FALSE) {

        if ($this->custom_select !== FALSE) {
            $this->db->select('safa_internalsegment_id');
            $this->db->select($this->custom_select);
        }

         $this->db->select("safa_internalsegments.*, erp_cities.".name()." as erp_city_name, erp_hotels.".name()." as erp_hotel_name, (SELECT safa_internalsegment_log.safa_uo_agent_id FROM safa_internalsegment_log WHERE safa_internalsegment_log.safa_internalsegment_id = safa_internalsegments.safa_internalsegment_id order by safa_internalsegment_log_id desc limit 1) as safa_uo_agent_id, erp_hotels.erp_city_id");
         $this->db->from('safa_internalsegments');
         $this->db->join('erp_hotels','erp_hotels.erp_hotel_id = safa_internalsegments.erp_end_hotel_id');
         $this->db->join('erp_cities','erp_cities.erp_city_id = erp_hotels.erp_city_id', 'left');
         
        if ($this->safa_internalsegment_id !== FALSE)
            $this->db->where('safa_internalsegments.safa_internalsegment_id', $this->safa_internalsegment_id);

        //if ($this->safa_internalsegmenttype_id !== FALSE)
            $this->db->where_in('safa_internalsegments.safa_internalsegmenttype_id', array(1,4));

        if ($this->safa_trip_internaltrip_id !== FALSE)
            $this->db->where('safa_internalsegments.safa_trip_internaltrip_id', $this->safa_trip_internaltrip_id);

        if ($this->safa_internalsegmentestatus_id !== FALSE)
            $this->db->where('safa_internalsegments.safa_internalsegmentestatus_id', $this->safa_internalsegmentestatus_id);

        if ($this->start_datetime !== FALSE)
            $this->db->where('safa_internalsegments.start_datetime', $this->start_datetime);

        if ($this->end_datetime !== FALSE)
            $this->db->where('safa_internalsegments.end_datetime', $this->end_datetime);

        if ($this->erp_port_id !== FALSE)
            $this->db->where('safa_internalsegments.erp_port_id', $this->erp_port_id);

        if ($this->erp_start_hotel_id !== FALSE)
            $this->db->where('safa_internalsegments.erp_start_hotel_id', $this->erp_start_hotel_id);

        if ($this->erp_end_hotel_id !== FALSE)
            $this->db->where('safa_internalsegments.erp_end_hotel_id', $this->erp_end_hotel_id);

        if ($this->safa_tourism_place_id !== FALSE)
            $this->db->where('safa_internalsegments.safa_tourism_place_id', $this->safa_tourism_place_id);

        if ($this->seats_count !== FALSE)
            $this->db->where('safa_internalsegments.seats_count', $this->seats_count);

        if ($this->notes !== FALSE)
            $this->db->where('safa_internalsegments.notes', $this->notes);

        if ($this->ito_driver_info !== FALSE)
            $this->db->where('safa_internalsegments.ito_driver_info', $this->ito_driver_info);

        if ($this->ito_vehicle_info !== FALSE)
            $this->db->where('safa_internalsegments.ito_vehicle_info', $this->ito_vehicle_info);

        if ($this->is_external !== FALSE)
            $this->db->where('safa_internalsegments.is_external', $this->is_external);

        if ($this->safa_uo_user_id !== FALSE)
            $this->db->where('safa_internalsegments.safa_uo_user_id', $this->safa_uo_user_id);

        if ($this->status_notes !== FALSE)
            $this->db->where('safa_internalsegments.status_notes', $this->status_notes);

        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get();
        
        //echo $this->db->last_query(); exit;
        
        if ($rows_no)
            return $query->num_rows();

        if ($this->safa_internalsegment_id)
            return $query->row();
        else
            return $query->result();
    }
    
    function get_tourismplaces($rows_no = FALSE) {

        if ($this->custom_select !== FALSE) {
            $this->db->select('safa_internalsegment_id');
            $this->db->select($this->custom_select);
        }

         $this->db->select("safa_internalsegments.*, safa_tourismplaces.".name()." as safa_tourismplace_name, (SELECT safa_internalsegment_log.safa_uo_agent_id FROM safa_internalsegment_log WHERE safa_internalsegment_log.safa_internalsegment_id = safa_internalsegments.safa_internalsegment_id order by safa_internalsegment_log_id desc limit 1) as safa_uo_agent_id, safa_tourismplaces.erp_city_id");
         $this->db->from('safa_internalsegments');
         $this->db->join('safa_tourismplaces','safa_tourismplaces.safa_tourismplace_id = safa_internalsegments.safa_tourism_place_id');
         
        if ($this->safa_internalsegment_id !== FALSE)
            $this->db->where('safa_internalsegments.safa_internalsegment_id', $this->safa_internalsegment_id);

        //if ($this->safa_internalsegmenttype_id !== FALSE)
            $this->db->where_in('safa_internalsegments.safa_internalsegmenttype_id', array(3));

        if ($this->safa_trip_internaltrip_id !== FALSE)
            $this->db->where('safa_internalsegments.safa_trip_internaltrip_id', $this->safa_trip_internaltrip_id);

        if ($this->safa_internalsegmentestatus_id !== FALSE)
            $this->db->where('safa_internalsegments.safa_internalsegmentestatus_id', $this->safa_internalsegmentestatus_id);

        if ($this->start_datetime !== FALSE)
            $this->db->where('safa_internalsegments.start_datetime', $this->start_datetime);

        if ($this->end_datetime !== FALSE)
            $this->db->where('safa_internalsegments.end_datetime', $this->end_datetime);

        if ($this->erp_port_id !== FALSE)
            $this->db->where('safa_internalsegments.erp_port_id', $this->erp_port_id);

        if ($this->erp_start_hotel_id !== FALSE)
            $this->db->where('safa_internalsegments.erp_start_hotel_id', $this->erp_start_hotel_id);

        if ($this->erp_end_hotel_id !== FALSE)
            $this->db->where('safa_internalsegments.erp_end_hotel_id', $this->erp_end_hotel_id);

        if ($this->safa_tourism_place_id !== FALSE)
            $this->db->where('safa_internalsegments.safa_tourism_place_id', $this->safa_tourism_place_id);

        if ($this->seats_count !== FALSE)
            $this->db->where('safa_internalsegments.seats_count', $this->seats_count);

        if ($this->notes !== FALSE)
            $this->db->where('safa_internalsegments.notes', $this->notes);

        if ($this->ito_driver_info !== FALSE)
            $this->db->where('safa_internalsegments.ito_driver_info', $this->ito_driver_info);

        if ($this->ito_vehicle_info !== FALSE)
            $this->db->where('safa_internalsegments.ito_vehicle_info', $this->ito_vehicle_info);

        if ($this->is_external !== FALSE)
            $this->db->where('safa_internalsegments.is_external', $this->is_external);

        if ($this->safa_uo_user_id !== FALSE)
            $this->db->where('safa_internalsegments.safa_uo_user_id', $this->safa_uo_user_id);

        if ($this->status_notes !== FALSE)
            $this->db->where('safa_internalsegments.status_notes', $this->status_notes);

        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get();
        
        //echo $this->db->last_query(); exit;
        
        if ($rows_no)
            return $query->num_rows();

        if ($this->safa_internalsegment_id)
            return $query->row();
        else
            return $query->result();
    }
    
    function save() {
        if ($this->safa_internalsegment_id !== FALSE)
            $this->db->set('safa_internalsegments.safa_internalsegment_id', $this->safa_internalsegment_id);

        if ($this->safa_internalsegmenttype_id !== FALSE)
            $this->db->set('safa_internalsegments.safa_internalsegmenttype_id', $this->safa_internalsegmenttype_id);

        if ($this->safa_trip_internaltrip_id !== FALSE)
            $this->db->set('safa_internalsegments.safa_trip_internaltrip_id', $this->safa_trip_internaltrip_id);

        if ($this->safa_internalsegmentestatus_id !== FALSE)
            $this->db->set('safa_internalsegments.safa_internalsegmentestatus_id', $this->safa_internalsegmentestatus_id);

        if ($this->start_datetime !== FALSE)
            $this->db->set('safa_internalsegments.start_datetime', $this->start_datetime);

        if ($this->end_datetime !== FALSE)
            $this->db->set('safa_internalsegments.end_datetime', $this->end_datetime);

        if ($this->erp_port_id !== FALSE)
            $this->db->set('safa_internalsegments.erp_port_id', $this->erp_port_id);

        if ($this->erp_start_hotel_id !== FALSE)
            $this->db->set('safa_internalsegments.erp_start_hotel_id', $this->erp_start_hotel_id);

        if ($this->erp_end_hotel_id !== FALSE)
            $this->db->set('safa_internalsegments.erp_end_hotel_id', $this->erp_end_hotel_id);

        if ($this->safa_tourism_place_id !== FALSE)
            $this->db->set('safa_internalsegments.safa_tourism_place_id', $this->safa_tourism_place_id);

        if ($this->seats_count !== FALSE)
            $this->db->set('safa_internalsegments.seats_count', $this->seats_count);

        if ($this->notes !== FALSE)
            $this->db->set('safa_internalsegments.notes', $this->notes);

        if ($this->ito_driver_info !== FALSE)
            $this->db->set('safa_internalsegments.ito_driver_info', $this->ito_driver_info);

        if ($this->status_notes !== FALSE)
            $this->db->set('safa_internalsegments.status_notes', $this->status_notes);

        if ($this->ito_vehicle_info !== FALSE)
            $this->db->set('safa_internalsegments.ito_vehicle_info', $this->ito_vehicle_info);

        if ($this->is_external !== FALSE)
            $this->db->set('safa_internalsegments.is_external', $this->is_external);
        if ($this->safa_uo_user_id !== FALSE)
            $this->db->set('safa_internalsegments.safa_uo_user_id', $this->safa_uo_user_id);



        if ($this->safa_internalsegment_id) {
            $this->db->where('safa_internalsegments.safa_internalsegment_id', $this->safa_internalsegment_id)->update('safa_internalsegments');
            return $this->db->affected_rows();
        } else {
            $this->db->insert('safa_internalsegments');
            return $this->db->insert_id();
        }
    }

    function delete() {
        if ($this->safa_internalsegment_id !== FALSE)
            $this->db->where('safa_internalsegments.safa_internalsegment_id', $this->safa_internalsegment_id);

        if ($this->safa_internalsegmenttype_id !== FALSE)
            $this->db->where('safa_internalsegments.safa_internalsegmenttype_id', $this->safa_internalsegmenttype_id);

        if ($this->safa_trip_internaltrip_id !== FALSE)
            $this->db->where('safa_internalsegments.safa_trip_internaltrip_id', $this->safa_trip_internaltrip_id);

        if ($this->safa_internalsegmentestatus_id !== FALSE)
            $this->db->where('safa_internalsegments.safa_internalsegmentestatus_id', $this->safa_internalsegmentestatus_id);

        if ($this->start_datetime !== FALSE)
            $this->db->where('safa_internalsegments.start_datetime', $this->start_datetime);

        if ($this->end_datetime !== FALSE)
            $this->db->where('safa_internalsegments.end_datetime', $this->end_datetime);

        if ($this->erp_port_id !== FALSE)
            $this->db->where('safa_internalsegments.erp_port_id', $this->erp_port_id);

        if ($this->erp_start_hotel_id !== FALSE)
            $this->db->where('safa_internalsegments.erp_start_hotel_id', $this->erp_start_hotel_id);

        if ($this->erp_end_hotel_id !== FALSE)
            $this->db->where('safa_internalsegments.erp_end_hotel_id', $this->erp_end_hotel_id);

        if ($this->safa_tourism_place_id !== FALSE)
            $this->db->where('safa_internalsegments.safa_tourism_place_id', $this->safa_tourism_place_id);

        if ($this->seats_count !== FALSE)
            $this->db->where('safa_internalsegments.seats_count', $this->seats_count);

        if ($this->notes !== FALSE)
            $this->db->where('safa_internalsegments.notes', $this->notes);

        if ($this->ito_driver_info !== FALSE)
            $this->db->where('safa_internalsegments.ito_driver_info', $this->ito_driver_info);

        if ($this->ito_vehicle_info !== FALSE)
            $this->db->where('safa_internalsegments.ito_vehicle_info', $this->ito_vehicle_info);

        if ($this->is_external !== FALSE)
            $this->db->where('safa_internalsegments.is_external', $this->is_external);

        if ($this->safa_uo_user_id !== FALSE)
            $this->db->where('safa_internalsegments.safa_uo_user_id', $this->safa_uo_user_id);

        if ($this->status_notes !== FALSE)
            $this->db->where('safa_internalsegments.status_notes', $this->status_notes);



        $this->db->delete('safa_internalsegments');
        return $this->db->affected_rows();
    }

    function search($rows_no = FALSE) {
        if ($this->custom_select !== FALSE) {
            $this->db->select('safa_internalsegment_id');
            $this->db->select($this->custom_select);
        }

        if ($this->safa_internalsegment_id !== FALSE)
            $this->db->where('safa_internalsegments.safa_internalsegment_id', $this->safa_internalsegment_id);

        if ($this->safa_internalsegmenttype_id !== FALSE)
            $this->db->where('safa_internalsegments.safa_internalsegmenttype_id', $this->safa_internalsegmenttype_id);

        if ($this->safa_trip_internaltrip_id !== FALSE)
            $this->db->where('safa_internalsegments.safa_trip_internaltrip_id', $this->safa_trip_internaltrip_id);

        if ($this->safa_internalsegmentestatus_id !== FALSE)
            $this->db->where('safa_internalsegments.safa_internalsegmentestatus_id', $this->safa_internalsegmentestatus_id);

        if ($this->start_datetime !== FALSE)
            $this->db->where('safa_internalsegments.start_datetime', $this->start_datetime);

        if ($this->end_datetime !== FALSE)
            $this->db->where('safa_internalsegments.end_datetime', $this->end_datetime);

        if ($this->erp_port_id !== FALSE)
            $this->db->where('safa_internalsegments.erp_port_id', $this->erp_port_id);

        if ($this->erp_start_hotel_id !== FALSE)
            $this->db->where('safa_internalsegments.erp_start_hotel_id', $this->erp_start_hotel_id);

        if ($this->erp_end_hotel_id !== FALSE)
            $this->db->where('safa_internalsegments.erp_end_hotel_id', $this->erp_end_hotel_id);

        if ($this->safa_tourism_place_id !== FALSE)
            $this->db->where('safa_internalsegments.safa_tourism_place_id', $this->safa_tourism_place_id);

        if ($this->seats_count !== FALSE)
            $this->db->where('safa_internalsegments.seats_count', $this->seats_count);

        if ($this->notes !== FALSE)
            $this->db->where('safa_internalsegments.notes', $this->notes);

        if ($this->ito_driver_info !== FALSE)
            $this->db->where('safa_internalsegments.ito_driver_info', $this->ito_driver_info);

        if ($this->ito_vehicle_info !== FALSE)
            $this->db->where('safa_internalsegments.ito_vehicle_info', $this->ito_vehicle_info);
        if ($this->safa_uo_user_id !== FALSE)
            $this->db->where('safa_internalsegments.safa_uo_user_id', $this->safa_uo_user_id);

        if ($this->is_external !== FALSE)
            $this->db->where('safa_internalsegments.is_external', $this->is_external);

        if ($this->status_notes !== FALSE)
            $this->db->where('safa_internalsegments.status_notes', $this->status_notes);


        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('safa_internalsegments');
        if ($rows_no)
            return $query->num_rows();

        return $query->result();
    }

    function getVehicle_type($vehicle_id) {
        $this->db->select('safa_vehicle_id,' . name() . '');
        $this->db->where('safa_vehicles.safa_vehicle_id', $vehicle_id);
        return $this->db->get('safa_vehicles')->row();
    }

}

/* End of file internal_passages_model.php */
/* Location: ./application/models/internal_passages_model.php */