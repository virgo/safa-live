<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Accounts_model extends CI_Model {

    public $id = FALSE;
    public $firm_id = FALSE;
    public $number = FALSE;
    public $name_ar = FALSE;
    public $name_la = FALSE;
    public $kind = FALSE;
    public $parent = FALSE;
    public $final_account = FALSE;
    public $security_level = FALSE;
    public $currency = FALSE;
    public $cash_first = FALSE;
    public $cash_total_credit = FALSE;
    public $cash_total_debit = FALSE;
    public $cash_current = FALSE;
    public $currency_cash_first = FALSE;
    public $currency_cash_current = FALSE;
    public $gold_support = FALSE;
    public $carat = FALSE;
    public $gold_first = FALSE;
    public $gold_total_credit = FALSE;
    public $gold_total_debit = FALSE;
    public $gold_current = FALSE;
    public $limit = FALSE;
    public $offset = FALSE;

    function __construct() {
        parent::__construct();
    }

    function get($rows_no = FALSE) {

        if ($this->id)
            $this->db->where('id', $this->id);

//       if ($this->firm_id)
        $this->db->where('firm_id', $this->session->userdata('firm_id'));

        if ($this->number)
            $this->db->where('number', $this->number);

        if ($this->name_ar)
            $this->db->where('name_ar', $this->name_ar);

        if ($this->name_la)
            $this->db->where('name_la', $this->name_la);

        if ($this->kind)
            $this->db->where('kind', $this->kind);

        if ($this->parent)
            $this->db->where('parent', $this->parent);

        if ($this->final_account)
            $this->db->where('final_account', $this->final_account);

        if ($this->security_level)
            $this->db->where('security_level', $this->security_level);

        if ($this->currency)
            $this->db->where('currency', $this->currency);

        if ($this->cash_first)
            $this->db->where('cash_first', $this->cash_first);

        if ($this->cash_total_credit)
            $this->db->where('cash_total_credit', $this->cash_total_credit);

        if ($this->cash_total_debit)
            $this->db->where('cash_total_debit', $this->cash_total_debit);

        if ($this->cash_current)
            $this->db->where('cash_current', $this->cash_current);

        if ($this->currency_cash_first)
            $this->db->where('currency_cash_first', $this->currency_cash_first);


        if ($this->currency_cash_current)
            $this->db->where('currency_cash_current', $this->currency_cash_current);


        if ($this->gold_support)
            $this->db->where('gold_support', $this->gold_support);

        if ($this->carat)
            $this->db->where('carat', $this->carat);

        if ($this->gold_first)
            $this->db->where('gold_first', $this->gold_first);

        if ($this->gold_total_credit)
            $this->db->where('gold_total_credit', $this->gold_total_credit);

        if ($this->gold_total_debit)
            $this->db->where('gold_total_debit', $this->gold_total_debit);

        if ($this->gold_current)
            $this->db->where('gold_current', $this->gold_current);


        $query = $this->db->get('accounts', $this->limit, $this->offset);

        if ($rows_no)
            return $query->num_rows();
        else
        if ($this->id)
            return $query->row();
        else
            return $query->result();
    }

    function save() {

//       if ($this->id)
//            $this->db->set('id', $this->id);
//       if ($this->firm_id)
        $this->db->set('firm_id', $this->session->userdata('firm_id'));


        if ($this->number)
            $this->db->set('number', $this->number);

        if ($this->name_ar)
            $this->db->set('name_ar', $this->name_ar);

        if ($this->name_la)
            $this->db->set('name_la', $this->name_la);

        if ($this->kind)
            $this->db->set('kind', $this->kind);

        if ($this->parent)
            $this->db->set('parent', $this->parent);

        if ($this->final_account)
            $this->db->set('final_account', $this->final_account);

        if ($this->security_level)
            $this->db->set('security_level', $this->security_level);

        if ($this->currency)
            $this->db->set('currency', $this->currency);

        if ($this->cash_first)
            $this->db->set('cash_first', $this->cash_first);

        if ($this->cash_total_credit)
            $this->db->set('cash_total_credit', $this->cash_total_credit);

        if ($this->cash_total_debit)
            $this->db->set('cash_total_debit', $this->cash_total_debit);

        if ($this->cash_current)
            $this->db->set('cash_current', $this->cash_current);

        if ($this->currency_cash_first)
            $this->db->set('currency_cash_first', $this->currency_cash_first);


        if ($this->currency_cash_current)
            $this->db->set('currency_cash_current', $this->currency_cash_current);


        if ($this->gold_support)
            $this->db->set('gold_support', $this->gold_support);

        if ($this->carat)
            $this->db->set('carat', $this->carat);

        if ($this->gold_first)
            $this->db->set('gold_first', $this->gold_first);

        if ($this->gold_total_credit)
            $this->db->set('gold_total_credit', $this->gold_total_credit);

        if ($this->gold_total_debit)
            $this->db->set('gold_total_debit', $this->gold_total_debit);

        if ($this->gold_current)
            $this->db->set('gold_current', $this->gold_current);



        if ($this->id) {
            $this->db->where('id', $this->id)->update('accounts');
            return $this->id;
        } else {
            $this->db->insert('accounts');
            return $this->db->insert_id();
        }
    }

    function delete() {
        if ($this->firm_id)
            $this->db->where('firm_id', $this->firm_id);
        else
            $this->db->where('firm_id', $this->session->userdata('firm_id'));
        $this->db->where('id', $this->id)->delete('accounts');
        return $this->db->affected_rows();
    }

    function get_tree($parent = '0', $output = '') {
        $result = $this->db->where('parent', $parent)->get('accounts')->result();
        if ($result) {
            $output .= '<ul>';
            foreach ($result as $item) {
                $output .= '<li>' . '<a href="' . site_url('admin/accounts/edit/' . $item->id) . '">' . $item->id . '</a>';
                $output .= $this->get_tree($item->parent);
                $output .= '</li>';
            }
            $output .= '</ul>';
        }
        return $output;
    }

    function getlast() {
        return $this->db->limit('1')->order_by('id', 'DESC')->get('accounts')->row();
    }

}

/* End of file accounts.php */
/* Location: ./application/models/accounts_model.php */