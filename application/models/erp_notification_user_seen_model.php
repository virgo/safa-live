<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Erp_notification_user_seen_model extends CI_Model {

    public $erp_notification_user_seen_id = FALSE;
    public $erp_notification_detail_id = FALSE;
    public $user_id = FALSE;
    public $mark_as_read = FALSE;
    public $mark_as_important = FALSE;
    public $is_hide = FALSE;
    public $read_datetime = FALSE;
    public $is_archived = FALSE;
    public $folder_id = FALSE;
    public $custom_select = FALSE;
    public $limit = FALSE;
    public $offset = FALSE;
    public $order_by = FALSE;
    public $join = FALSE;

    function __construct() {
        parent::__construct();
    }

    function get($rows_no = FALSE) {

        if ($this->custom_select !== FALSE) {
            $this->db->select('erp_notification_user_seen_id');
            $this->db->select($this->custom_select);
        }

        if ($this->erp_notification_user_seen_id !== FALSE)
            $this->db->where('erp_notification_user_seen.erp_notification_user_seen_id', $this->erp_notification_user_seen_id);

        if ($this->erp_notification_detail_id !== FALSE)
            $this->db->where('erp_notification_user_seen.erp_notification_detail_id', $this->erp_notification_detail_id);

        if ($this->user_id !== FALSE)
            $this->db->where('erp_notification_user_seen.user_id', $this->user_id);


        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        if ($this->join !== FALSE) {
            /*
              $this->db->select('erp_notification_user_seen.*,erp_countries.erp_country_id as country_id,
              erp_countries.name_ar as country_name_ar,erp_countries.name_la as country_name_la');
              $this->db->join('erp_countries', 'erp_notification_user_seen.erp_country_id = erp_countries.erp_country_id', 'left');
             */
        }

        $query = $this->db->get('erp_notification_user_seen');
        if ($rows_no)
            return $query->num_rows();

        if ($this->erp_notification_user_seen_id)
            return $query->row();
        else
            return $query->result();
    }

    function save() {
        if ($this->erp_notification_user_seen_id !== FALSE)
            $this->db->set('erp_notification_user_seen.erp_notification_user_seen_id', $this->erp_notification_user_seen_id);

        if ($this->erp_notification_detail_id !== FALSE)
            $this->db->set('erp_notification_user_seen.erp_notification_detail_id', $this->erp_notification_detail_id);

        if ($this->user_id !== FALSE)
            $this->db->set('erp_notification_user_seen.user_id', $this->user_id);

        if ($this->mark_as_read !== FALSE)
            $this->db->set('erp_notification_user_seen.mark_as_read', $this->mark_as_read);

        if ($this->mark_as_important !== FALSE)
            $this->db->set('erp_notification_user_seen.mark_as_important', $this->mark_as_important);


        if ($this->is_hide !== FALSE)
            $this->db->set('erp_notification_user_seen.is_hide', $this->is_hide);

        if ($this->read_datetime !== FALSE)
            $this->db->set('erp_notification_user_seen.read_datetime', $this->read_datetime);

        if ($this->is_archived !== FALSE)
            $this->db->set('erp_notification_user_seen.is_archived', $this->is_archived);

        if ($this->folder_id !== FALSE)
            $this->db->set('erp_notification_user_seen.folder_id', $this->folder_id);



        if ($this->erp_notification_user_seen_id) {
            $this->db->where('erp_notification_user_seen.erp_notification_user_seen_id', $this->erp_notification_user_seen_id)->update('erp_notification_user_seen');
        } else {
            $this->db->insert('erp_notification_user_seen');
            return $this->db->insert_id();
        }
    }

    function delete() {
        if ($this->erp_notification_user_seen_id !== FALSE)
            $this->db->where('erp_notification_user_seen.erp_notification_user_seen_id', $this->erp_notification_user_seen_id);

        $this->db->delete('erp_notification_user_seen');
        return $this->db->affected_rows();
    }

}

/* End of file cities_model.php */
/* Location: ./application/models/cities_model.php */