<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Port_halls_model extends CI_Model {

    public $erp_port_hall_id = FALSE;
    public $erp_port_id = FALSE;
    public $name_ar = FALSE;
    public $name_la = FALSE;
    public $code = FALSE;
    public $country_code = FALSE;
    
    public $custom_select = FALSE;
    public $limit = FALSE;
    public $offset = FALSE;
    public $order_by = FALSE;
    public $join;

    function __construct() {
        parent::__construct();
    }

    function get($rows_no = FALSE) {

    	$this->db->select('erp_port_halls.*');
        if ($this->custom_select !== FALSE) {
            $this->db->select('erp_port_hall_id');
            $this->db->select($this->custom_select);
        }

        if ($this->erp_port_hall_id !== FALSE)
            $this->db->where('erp_port_halls.erp_port_hall_id', $this->erp_port_hall_id);

        if ($this->erp_port_id !== FALSE)
            $this->db->where('erp_ports.erp_port_id', $this->erp_port_id);

        if ($this->name_ar !== FALSE)
            $this->db->where('erp_port_halls.name_ar', $this->name_ar);

        if ($this->name_la !== FALSE)
            $this->db->where('erp_port_halls.name_la', $this->name_la);

        if ($this->code !== FALSE)
            $this->db->where('erp_port_halls.code', $this->code);

        if ($this->join) {
            $this->db->select('erp_ports.name_ar as port_name,erp_ports.name_la as port_english_name,erp_ports.erp_port_id,erp_port_halls.name_ar as arabic_name,erp_port_halls.name_la as english_name,erp_port_halls.erp_port_hall_id ,
         	erp_port_halls.code , erp_port_halls.erp_port_id');
            
        }

        $this->db->join('erp_ports', 'erp_port_halls.erp_port_id = erp_ports.erp_port_id','left');
        
        if ($this->country_code !== FALSE)
        $this->db->where('erp_ports.country_code', $this->country_code);
        
        
        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('erp_port_halls');
        
        //echo $this->db->last_query(); exit;
        
        if ($rows_no)
            return $query->num_rows();

        if ($this->erp_port_hall_id)
            return $query->row();
        else
            return $query->result();
    }

    function save() {

        if ($this->erp_port_hall_id !== FALSE)
            $this->db->set('erp_port_halls.erp_port_hall_id', $this->erp_port_hall_id);

        if ($this->erp_port_id !== FALSE)
            $this->db->set('erp_port_halls.erp_port_id', $this->erp_port_id);

        if ($this->name_ar !== FALSE)
            $this->db->set('erp_port_halls.name_ar', $this->name_ar);

        if ($this->name_la !== FALSE)
            $this->db->set('erp_port_halls.name_la', $this->name_la);

        if ($this->code !== FALSE)
            $this->db->set('erp_port_halls.code', $this->code);

        if ($this->erp_port_hall_id) {
            $this->db->where('erp_port_halls.erp_port_hall_id', $this->erp_port_hall_id)->update('erp_port_halls');
        } else {
            $this->db->insert('erp_port_halls');
            return $this->db->insert_id();
        }
    }

    function delete() {

        if ($this->erp_port_hall_id !== FALSE)
            $this->db->where('erp_port_halls.erp_port_hall_id', $this->erp_port_hall_id);

        if ($this->erp_port_id !== FALSE)
            $this->db->where('erp_port_halls.erp_port_id', $this->erp_port_id);

        if ($this->name_ar !== FALSE)
            $this->db->where('erp_port_halls.name_ar', $this->name_ar);

        if ($this->name_la !== FALSE)
            $this->db->where('erp_port_halls.name_la', $this->name_la);

        if ($this->code !== FALSE)
            $this->db->where('erp_port_halls.code', $this->code);

        $this->db->delete('erp_port_halls');
        return $this->db->affected_rows();
    }

    function search($rows_no = FALSE) {
        if ($this->custom_select !== FALSE) {
            $this->db->select('erp_port_hall_id');
            $this->db->select($this->custom_select);
        }

        if ($this->erp_port_hall_id !== FALSE)
            $this->db->where('erp_port_halls.erp_port_hall_id', $this->erp_port_hall_id);

        if ($this->erp_port_id !== FALSE)
            $this->db->where('erp_port_halls.erp_port_id', $this->erp_port_id);

        if ($this->name_ar !== FALSE)
            $this->db->where('erp_port_halls.name_ar', $this->name_ar);

        if ($this->name_la !== FALSE)
            $this->db->where('erp_port_halls.name_la', $this->name_la);

        if ($this->code !== FALSE)
            $this->db->where('erp_port_halls.code', $this->code);

        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('erp_port_halls');
        if ($rows_no)
            return $query->num_rows();

        return $query->result();
    }

    function check_delete_ability($id) {

        $this->db->select('count(safa_trip_externalsegments.start_port_hall_id)as trip_externalsegments_start,count(safa_trip_externalsegments.end_port_hall_id)as trip_externalsegments_end');
        $this->db->from('erp_port_halls');
        $this->db->join('safa_trip_externalsegments', 'erp_port_halls.erp_port_hall_id = safa_trip_externalsegments.start_port_hall_id or erp_port_halls.erp_port_hall_id = safa_trip_externalsegments.end_port_hall_id', 'left');
        $this->db->group_by('erp_port_halls.erp_port_hall_id');
        $this->db->where('erp_port_halls.erp_port_hall_id', $id);
        $query = $this->db->get();
        $flag = 0;

//        print_r($query->result());
        foreach ($query->result() as $row => $table) {
            foreach ($table as $col => $value) {
                if ($value > 0) {
                    $flag = 1;
                    break;
                } else {
                    continue;
                }
            }
        }
        return $flag;
    }

    function get_erp_ports() {
        $this->db->select('CONCAT(erp_ports.code , " , " , erp_ports.' . name() . '," , ",erp_cities.' . name() . ') as name_code,erp_port_id', FALSE);
        $this->db->from('erp_ports');
        $this->db->join('erp_cities', 'erp_ports.erp_city_id=erp_cities.erp_city_id', 'left');
        $result = $this->db->get()->result();
        return $result;
    }

}

/* End of file airporthalls_model.php */
/* Location: ./application/models/airporthalls_model.php */