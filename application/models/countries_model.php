<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Countries_model extends CI_Model {

    public $erp_country_id = FALSE;
    public $erp_nationality_id = FALSE;
    public $nat_code = FALSE;
    public $country_code = FALSE;
    public $name_ar = FALSE;
    public $name_la = FALSE;
    public $arab = FALSE;
    public $orientation = FALSE;
    public $custom_select = FALSE;
    public $limit = FALSE;
    public $offset = FALSE;
    public $order_by = FALSE;

    function __construct() {
        parent::__construct();
    }

    function get($rows_no = FALSE) {

        if ($this->custom_select !== FALSE) {
            $this->db->select('erp_country_id');
            $this->db->select($this->custom_select);
        }

        if ($this->erp_country_id !== FALSE)
            $this->db->where('erp_countries.erp_country_id', $this->erp_country_id);

        if ($this->erp_nationality_id !== FALSE)
            $this->db->where('erp_countries.erp_nationality_id', $this->erp_nationality_id);

        if ($this->nat_code !== FALSE)
            $this->db->where('erp_countries.nat_code', $this->nat_code);

        if ($this->country_code !== FALSE)
            $this->db->where('erp_countries.country_code', $this->country_code);

        if ($this->name_ar !== FALSE)
            $this->db->where('erp_countries.name_ar', $this->name_ar);

        if ($this->name_la !== FALSE)
            $this->db->where('erp_countries.name_la', $this->name_la);

        if ($this->arab !== FALSE)
            $this->db->where('erp_countries.arab', $this->arab);


        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('erp_countries');
        if ($rows_no)
            return $query->num_rows();

        if ($this->erp_country_id)
            return $query->row();
        else
            return $query->result();
    }

    function save() {
        if ($this->erp_country_id !== FALSE)
            $this->db->set('erp_countries.erp_country_id', $this->erp_country_id);

        if ($this->erp_nationality_id !== FALSE)
            $this->db->set('erp_countries.erp_nationality_id', $this->erp_nationality_id);

        if ($this->nat_code !== FALSE)
            $this->db->set('erp_countries.nat_code', $this->nat_code);

        if ($this->country_code !== FALSE)
            $this->db->set('erp_countries.country_code', $this->country_code);

        if ($this->name_ar !== FALSE)
            $this->db->set('erp_countries.name_ar', $this->name_ar);

        if ($this->name_la !== FALSE)
            $this->db->set('erp_countries.name_la', $this->name_la);

        if ($this->arab !== FALSE)
            $this->db->set('erp_countries.arab', $this->arab);



        if ($this->erp_country_id) {
            $this->db->where('erp_countries.erp_country_id', $this->erp_country_id)->update('erp_countries');
        } else {
            $this->db->insert('erp_countries');
            return $this->db->insert_id();
        }
    }

    function delete() {
        if ($this->erp_country_id !== FALSE)
            $this->db->where('erp_countries.erp_country_id', $this->erp_country_id);

        if ($this->erp_nationality_id !== FALSE)
            $this->db->where('erp_countries.erp_nationality_id', $this->erp_nationality_id);

        if ($this->nat_code !== FALSE)
            $this->db->where('erp_countries.nat_code', $this->nat_code);

        if ($this->country_code !== FALSE)
            $this->db->where('erp_countries.country_code', $this->country_code);

        if ($this->name_ar !== FALSE)
            $this->db->where('erp_countries.name_ar', $this->name_ar);

        if ($this->name_la !== FALSE)
            $this->db->where('erp_countries.name_la', $this->name_la);

        if ($this->arab !== FALSE)
            $this->db->where('erp_countries.arab', $this->arab);



        $this->db->delete('erp_countries');
        return $this->db->affected_rows();
    }

    function search($rows_no = FALSE) {
        if ($this->custom_select !== FALSE) {
            $this->db->select('erp_country_id');
            $this->db->select($this->custom_select);
        }

        if ($this->erp_country_id !== FALSE)
            $this->db->where('erp_countries.erp_country_id', $this->erp_country_id);

        if ($this->erp_nationality_id !== FALSE)
            $this->db->where('erp_countries.erp_nationality_id', $this->erp_nationality_id);

        if ($this->nat_code !== FALSE)
            $this->db->where('erp_countries.nat_code', $this->nat_code);

        if ($this->country_code !== FALSE)
            $this->db->where('erp_countries.country_code', $this->country_code);

        if ($this->name_ar !== FALSE)
            $this->db->where('erp_countries.name_ar', $this->name_ar);

        if ($this->name_la !== FALSE)
            $this->db->where('erp_countries.name_la', $this->name_la);

        if ($this->arab !== FALSE)
            $this->db->where('erp_countries.arab', $this->arab);


        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('erp_countries');
        if ($rows_no)
            return $query->num_rows();

        return $query->result();
    }

}

/* End of file countries_model.php */
/* Location: ./application/models/countries_model.php */