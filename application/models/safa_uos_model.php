<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Safa_uos_model extends CI_Model {

    public $safa_uo_id = FALSE;
    public $name_ar = FALSE;
    public $name_la = FALSE;
    public $code = FALSE;
    public $phone = FALSE;
    public $mobile = FALSE;
    public $email = FALSE;
    public $fax = FALSE;
    public $deleted = FALSE;
    public $erp_uasp_id = FALSE;
    public $disabled = FALSE;
    public $custom_select = FALSE;
    public $limit = FALSE;
    public $offset = FALSE;
    public $order_by = FALSE;
    public $join = FALSE;

    function __construct() {
        parent::__construct();
    }

    function get($rows_no = FALSE) {

        if ($this->custom_select !== FALSE) {
            $this->db->select('safa_uo_id');
            $this->db->select($this->custom_select);
        }
        if ($this->deleted !== FALSE)
            $this->db->where('uo1.deleted', $this->deleted);

        if ($this->disabled !== FALSE)
            $this->db->where('uo1.disabled', $this->deleted);

        if ($this->safa_uo_id !== FALSE)
            $this->db->where('uo1.safa_uo_id', $this->safa_uo_id);

        if ($this->name_ar !== FALSE)
            $this->db->where('uo1.name_ar', $this->name_ar);

        if ($this->name_la !== FALSE)
            $this->db->where('uo1.name_la', $this->name_la);

        if ($this->code !== FALSE)
            $this->db->where('uo1.code', $this->code);

        if ($this->phone !== FALSE)
            $this->db->where('uo1.phone', $this->phone);

        if ($this->mobile !== FALSE)
            $this->db->where('uo1.mobile', $this->mobile);

        if ($this->email !== FALSE)
            $this->db->where('uo1.email', $this->email);

        if ($this->fax !== FALSE)
            $this->db->where('uo1.fax', $this->fax);

        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);
        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $this->db->select("uo1.* ,
             (select count(safa_uo_contracts.safa_uo_contract_id) from safa_uo_contracts where safa_uo_contracts.safa_uo_id=uo1.safa_uo_id) as num_contracts,
            (select count(safa_trips.safa_trip_id) from safa_trips 
            join safa_group_passports on safa_group_passports.safa_trip_id=safa_trips.safa_trip_id
            
            join safa_group_passport_accommodation on safa_group_passport_accommodation.safa_group_passport_id=safa_group_passports.safa_group_passport_id
            join safa_package_periods on safa_package_periods.safa_package_periods_id=safa_group_passport_accommodation.safa_package_periods_id
            
            join safa_packages on safa_packages.safa_package_id=safa_package_periods.safa_package_id
            join safa_uos on safa_uos.safa_uo_id=safa_packages.erp_company_id 
            where safa_packages.erp_company_id=uo1.safa_uo_id and safa_packages.erp_company_type_id=2) as num_trips ");
        $this->db->from("safa_uos as uo1");
        $query = $this->db->get();
        
        //echo $this->db->last_query(); exit;
        
        
        if ($rows_no)
            return $query->num_rows();

        if ($this->safa_uo_id)
            return $query->row();
        else
            return $query->result();
    }

    function save() {
        if ($this->safa_uo_id !== FALSE)
            $this->db->set('safa_uos.safa_uo_id', $this->safa_uo_id);

        if ($this->name_ar !== FALSE)
            $this->db->set('safa_uos.name_ar', $this->name_ar);

        if ($this->name_la !== FALSE)
            $this->db->set('safa_uos.name_la', $this->name_la);

        if ($this->code !== FALSE)
            $this->db->set('safa_uos.code', $this->code);

        if ($this->erp_uasp_id !== FALSE)
            $this->db->set('safa_uos.erp_uasp_id', $this->erp_uasp_id);

        if ($this->phone !== FALSE)
            $this->db->set('safa_uos.phone', $this->phone);

        if ($this->mobile !== FALSE)
            $this->db->set('safa_uos.mobile', $this->mobile);

        if ($this->email !== FALSE)
            $this->db->set('safa_uos.email', $this->email);

        if ($this->fax !== FALSE)
            $this->db->set('safa_uos.fax', $this->fax);
        if ($this->disabled !== FALSE)
            $this->db->set('safa_uos.disabled', $this->disabled);



        if ($this->safa_uo_id) {
            return $this->db->where('safa_uos.safa_uo_id', $this->safa_uo_id)->update('safa_uos');
        } else {
            $this->db->insert('safa_uos');
            return $this->db->insert_id();
        }
    }

    function delete() {
        if ($this->safa_uo_id !== FALSE)
            $this->db->where('safa_uos.safa_uo_id', $this->safa_uo_id);

        if ($this->name_ar !== FALSE)
            $this->db->where('safa_uos.name_ar', $this->name_ar);

        if ($this->name_la !== FALSE)
            $this->db->where('safa_uos.name_la', $this->name_la);

        if ($this->code !== FALSE)
            $this->db->where('safa_uos.code', $this->code);

        if ($this->phone !== FALSE)
            $this->db->where('safa_uos.phone', $this->phone);

        if ($this->mobile !== FALSE)
            $this->db->where('safa_uos.mobile', $this->mobile);

        if ($this->email !== FALSE)
            $this->db->where('safa_uos.email', $this->email);

        if ($this->fax !== FALSE)
            $this->db->where('safa_uos.fax', $this->fax);

        if ($this->disabled !== FALSE)
            $this->db->where('safa_uos.disabled', $this->deleted);




        $this->db->delete('safa_uos');
        return $this->db->affected_rows();
    }

    function search($rows_no = FALSE) {
        if ($this->custom_select !== FALSE) {
            $this->db->select('safa_uo_id');
            $this->db->select($this->custom_select);
        }

        if ($this->safa_uo_id !== FALSE)
            $this->db->where('safa_uo.safa_uo_id', $this->safa_uo_id);
        if ($this->name_ar !== FALSE)
            $this->db->where('safa_uos.name_ar', $this->name_ar);

        if ($this->name_la !== FALSE)
            $this->db->where('safa_uos.name_la', $this->name_la);

        if ($this->code !== FALSE)
            $this->db->where('safa_uos.code', $this->code);

        if ($this->phone !== FALSE)
            $this->db->where('safa_uos.phone', $this->phone);

        if ($this->mobile !== FALSE)
            $this->db->where('safa_uos.mobile', $this->mobile);

        if ($this->email !== FALSE)
            $this->db->where('safa_uos.email', $this->email);

        if ($this->fax !== FALSE)
            $this->db->where('safa_uos.fax', $this->fax);

        if ($this->disabled !== FALSE)
            $this->db->where('safa_uos.disabled', $this->deleted);



        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('safa_uos');
        if ($rows_no)
            return $query->num_rows();

        return $query->result();
    }

    function check_delete_ability($id) {
        $this->db->select('safa_uo_contracts.safa_uo_contract_id,safa_uo_contracts.safa_uo_id ');
        $this->db->from('safa_uos');
        $this->db->join('safa_uo_contracts', 'safa_uos.safa_uo_id = safa_uo_contracts.safa_uo_id');
        $this->db->where('safa_uos.safa_uo_id', $id);


        $query = $this->db->get();

        if ($this->safa_uo_id)
            return $query->row();
        else
            return $query->result();
    }

    function check_users_delete_ability($id) {
        $this->db->select('safa_uo_users.safa_uo_user_id,safa_uo_users.safa_uo_id,safa_uo_users.safa_uo_usergroup_id ');
        $this->db->from('safa_uo_users');
        $this->db->where('safa_uo_users.safa_uo_id', $id);
        $this->db->where('safa_uo_users.safa_uo_usergroup_id', 1);


        $query = $this->db->get();

        if ($this->safa_uo_id)
            return $query->row();
        else
            return $query->result();
    }

    function delete_disable() {

        $this->db->where('safa_uo_id', $this->safa_uo_id);
        $this->db->set('deleted', '1');
        $deleted = $this->db->update('safa_uos');
        return $deleted;
    }

    function delete_enable() {
        $this->db->where('safa_uo_id', $this->safa_uo_id);
        $this->db->set('deleted', '0');
        $deleted = $this->db->update('safa_uos');
        return $deleted;
    }

    // this two functions  are responisble for activete  and diactivate from the syetem // 
}
