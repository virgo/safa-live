<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Ea_payments_model extends CI_Model {

    public $crm_ea_payment_id = FALSE;
    public $crm_cashbox_id = FALSE;
    public $crm_ea_license_id = FALSE;
    public $crm_package_id = FALSE;
    public $erp_admin_id = FALSE;
    public $payment_type = FALSE;
    public $amount = FALSE;
    public $erp_currency_id = FALSE;
    public $voucher = FALSE;
    public $notes = FALSE;
    public $date = FALSE;
    public $credit = FALSE;
    public $submission_datetime = FALSE;
    public $custom_select = FALSE;
    public $limit = FALSE;
    public $offset = FALSE;
    public $order_by = FALSE;

    function __construct() {
        parent::__construct();
    }

    function get($rows_no = FALSE) {

        if ($this->custom_select !== FALSE) {
            $this->db->select('crm_ea_payment_id');
            $this->db->select($this->custom_select);
        }

        if ($this->crm_ea_payment_id !== FALSE)
            $this->db->where('crm_ea_payments.crm_ea_payment_id', $this->crm_ea_payment_id);

        if ($this->crm_cashbox_id !== FALSE)
            $this->db->where('crm_ea_payments.crm_cashbox_id', $this->crm_cashbox_id);

        if ($this->crm_ea_license_id !== FALSE)
            $this->db->where('crm_ea_payments.crm_ea_license_id', $this->crm_ea_license_id);

        if ($this->crm_package_id !== FALSE)
            $this->db->where('crm_ea_payments.crm_package_id', $this->crm_package_id);

        if ($this->erp_admin_id !== FALSE)
            $this->db->where('crm_ea_payments.erp_admin_id', $this->erp_admin_id);

        if ($this->payment_type !== FALSE)
            $this->db->where('crm_ea_payments.payment_type', $this->payment_type);

        if ($this->amount !== FALSE)
            $this->db->where('crm_ea_payments.amount', $this->amount);

        if ($this->erp_currency_id !== FALSE)
            $this->db->where('crm_ea_payments.erp_currency_id', $this->erp_currency_id);

        if ($this->voucher !== FALSE)
            $this->db->where('crm_ea_payments.voucher', $this->voucher);

        if ($this->notes !== FALSE)
            $this->db->where('crm_ea_payments.notes', $this->notes);

        if ($this->date !== FALSE)
            $this->db->where('crm_ea_payments.date', $this->date);

        if ($this->credit !== FALSE)
            $this->db->where('crm_ea_payments.credit', $this->credit);

        if ($this->submission_datetime !== FALSE)
            $this->db->where('crm_ea_payments.submission_datetime', $this->submission_datetime);


        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('crm_ea_payments');
        if ($rows_no)
            return $query->num_rows();

        if ($this->crm_ea_payment_id)
            return $query->row();
        else
            return $query->result();
    }

    function save() {
        if ($this->crm_ea_payment_id !== FALSE)
            $this->db->set('crm_ea_payments.crm_ea_payment_id', $this->crm_ea_payment_id);

        if ($this->crm_cashbox_id !== FALSE)
            $this->db->set('crm_ea_payments.crm_cashbox_id', $this->crm_cashbox_id);

        if ($this->crm_ea_license_id !== FALSE)
            $this->db->set('crm_ea_payments.crm_ea_license_id', $this->crm_ea_license_id);

        if ($this->crm_package_id !== FALSE)
            $this->db->set('crm_ea_payments.crm_package_id', $this->crm_package_id);

        if ($this->erp_admin_id !== FALSE)
            $this->db->set('crm_ea_payments.erp_admin_id', $this->erp_admin_id);

        if ($this->payment_type !== FALSE)
            $this->db->set('crm_ea_payments.payment_type', $this->payment_type);

        if ($this->amount !== FALSE)
            $this->db->set('crm_ea_payments.amount', $this->amount);

        if ($this->erp_currency_id !== FALSE)
            $this->db->set('crm_ea_payments.erp_currency_id', $this->erp_currency_id);

        if ($this->voucher !== FALSE)
            $this->db->set('crm_ea_payments.voucher', $this->voucher);

        if ($this->notes !== FALSE)
            $this->db->set('crm_ea_payments.notes', $this->notes);

        if ($this->date !== FALSE)
            $this->db->set('crm_ea_payments.date', $this->date);

        if ($this->credit !== FALSE)
            $this->db->set('crm_ea_payments.credit', $this->credit);

        if ($this->submission_datetime !== FALSE)
            $this->db->set('crm_ea_payments.submission_datetime', $this->submission_datetime);



        if ($this->crm_ea_payment_id) {
            $this->db->where('crm_ea_payments.crm_ea_payment_id', $this->crm_ea_payment_id)->update('crm_ea_payments');
            return $this->crm_ea_payment_id;
        } else {
            $this->db->insert('crm_ea_payments');
            return $this->db->insert_id();
        }
    }

    function delete() {
        if ($this->crm_ea_payment_id !== FALSE)
            $this->db->where('crm_ea_payments.crm_ea_payment_id', $this->crm_ea_payment_id);

        if ($this->crm_cashbox_id !== FALSE)
            $this->db->where('crm_ea_payments.crm_cashbox_id', $this->crm_cashbox_id);

        if ($this->crm_ea_license_id !== FALSE)
            $this->db->where('crm_ea_payments.crm_ea_license_id', $this->crm_ea_license_id);

        if ($this->crm_package_id !== FALSE)
            $this->db->where('crm_ea_payments.crm_package_id', $this->crm_package_id);

        if ($this->erp_admin_id !== FALSE)
            $this->db->where('crm_ea_payments.erp_admin_id', $this->erp_admin_id);

        if ($this->payment_type !== FALSE)
            $this->db->where('crm_ea_payments.payment_type', $this->payment_type);

        if ($this->amount !== FALSE)
            $this->db->where('crm_ea_payments.amount', $this->amount);

        if ($this->erp_currency_id !== FALSE)
            $this->db->where('crm_ea_payments.erp_currency_id', $this->erp_currency_id);

        if ($this->voucher !== FALSE)
            $this->db->where('crm_ea_payments.voucher', $this->voucher);

        if ($this->notes !== FALSE)
            $this->db->where('crm_ea_payments.notes', $this->notes);

        if ($this->date !== FALSE)
            $this->db->where('crm_ea_payments.date', $this->date);

        if ($this->credit !== FALSE)
            $this->db->where('crm_ea_payments.credit', $this->credit);

        if ($this->submission_datetime !== FALSE)
            $this->db->where('crm_ea_payments.submission_datetime', $this->submission_datetime);



        $this->db->delete('crm_ea_payments');
        return $this->db->affected_rows();
    }

    function search($rows_no = FALSE) {
        if ($this->custom_select !== FALSE) {
            $this->db->select('crm_ea_payment_id');
            $this->db->select($this->custom_select);
        }

        if ($this->crm_ea_payment_id !== FALSE)
            $this->db->where('crm_ea_payments.crm_ea_payment_id', $this->crm_ea_payment_id);

        if ($this->crm_cashbox_id !== FALSE)
            $this->db->where('crm_ea_payments.crm_cashbox_id', $this->crm_cashbox_id);

        if ($this->crm_ea_license_id !== FALSE)
            $this->db->where('crm_ea_payments.crm_ea_license_id', $this->crm_ea_license_id);

        if ($this->crm_package_id !== FALSE)
            $this->db->where('crm_ea_payments.crm_package_id', $this->crm_package_id);

        if ($this->erp_admin_id !== FALSE)
            $this->db->where('crm_ea_payments.erp_admin_id', $this->erp_admin_id);

        if ($this->payment_type !== FALSE)
            $this->db->where('crm_ea_payments.payment_type', $this->payment_type);

        if ($this->amount !== FALSE)
            $this->db->where('crm_ea_payments.amount', $this->amount);

        if ($this->erp_currency_id !== FALSE)
            $this->db->where('crm_ea_payments.erp_currency_id', $this->erp_currency_id);

        if ($this->voucher !== FALSE)
            $this->db->where('crm_ea_payments.voucher', $this->voucher);

        if ($this->notes !== FALSE)
            $this->db->where('crm_ea_payments.notes', $this->notes);

        if ($this->date !== FALSE)
            $this->db->where('crm_ea_payments.date', $this->date);

        if ($this->credit !== FALSE)
            $this->db->where('crm_ea_payments.credit', $this->credit);

        if ($this->submission_datetime !== FALSE)
            $this->db->where('crm_ea_payments.submission_datetime', $this->submission_datetime);


        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('crm_ea_payments');
        if ($rows_no)
            return $query->num_rows();

        return $query->result();
    }

}

/* End of file ea_payments_model.php */
/* Location: ./application/models/ea_payments_model.php */