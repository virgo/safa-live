<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Safa_group_passports_flight_availabilties_model extends CI_Model 
{

    public $safa_group_passports_flight_availabilty_id = FALSE;
    public $erp_flight_availabilty_id = FALSE;
    public $safa_group_passport_id = FALSE;
    
    public $custom_select = FALSE;
    public $limit = FALSE;
    public $offset = FALSE;
    public $order_by = FALSE;

    function __construct() {
        parent::__construct();
    }

    function save() {
        if ($this->safa_group_passports_flight_availabilty_id !== FALSE) {
            $this->db->set('safa_group_passports_flight_availabilties.safa_group_passports_flight_availabilty_id', $this->safa_group_passports_flight_availabilty_id);
        }
        if ($this->erp_flight_availabilty_id !== FALSE) {
            $this->db->set('safa_group_passports_flight_availabilties.erp_flight_availabilty_id', $this->erp_flight_availabilty_id);
        }
        if ($this->safa_group_passport_id !== FALSE) {
            $this->db->set('safa_group_passports_flight_availabilties.safa_group_passport_id', $this->safa_group_passport_id);
        }
        
        if ($this->safa_group_passports_flight_availabilty_id) {
            $this->db->where('safa_group_passports_flight_availabilties.safa_group_passports_flight_availabilty_id', $this->safa_group_passports_flight_availabilty_id)->update('safa_group_passports_flight_availabilties');
        } else {
            $this->db->insert('safa_group_passports_flight_availabilties');
            return $this->db->insert_id();
        }
    }

    function delete() {
        if ($this->safa_group_passports_flight_availabilty_id !== FALSE) {
            $this->db->where('safa_group_passports_flight_availabilties.safa_group_passports_flight_availabilty_id', $this->safa_group_passports_flight_availabilty_id);
        }
    	if ($this->erp_flight_availabilty_id !== FALSE) {
            $this->db->where('safa_group_passports_flight_availabilties.erp_flight_availabilty_id', $this->erp_flight_availabilty_id);
        }
        if ($this->safa_group_passport_id !== FALSE) {
            $this->db->where('safa_group_passports_flight_availabilties.safa_group_passport_id', $this->safa_group_passport_id);
        }


        $this->db->delete('safa_group_passports_flight_availabilties');
        return $this->db->affected_rows();
    }

    function get($rows_no = FALSE) {

        if ($this->custom_select !== FALSE) {
            $this->db->select('safa_group_passports_flight_availabilty_id');
            $this->db->select($this->custom_select);
        }

        if ($this->safa_group_passports_flight_availabilty_id !== FALSE) {
            $this->db->where('safa_group_passports_flight_availabilties.safa_group_passports_flight_availabilty_id', $this->safa_group_passports_flight_availabilty_id);
        }
    if ($this->erp_flight_availabilty_id !== FALSE) {
            $this->db->where('safa_group_passports_flight_availabilties.erp_flight_availabilty_id', $this->erp_flight_availabilty_id);
        }
        if ($this->safa_group_passport_id !== FALSE) {
            $this->db->where('safa_group_passports_flight_availabilties.safa_group_passport_id', $this->safa_group_passport_id);
        }
        
        
        if ($this->order_by && is_array($this->order_by)) {
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);
        } else {
            $this->db->order_by('safa_group_passports_flight_availabilty_id');
        }

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('safa_group_passports_flight_availabilties');
        if ($rows_no)
            return $query->num_rows();

        if ($this->safa_group_passports_flight_availabilty_id)
            return $query->row();
        else
            return $query->result();
    }

}

/* End of file safa_group_passports_flight_availabilties_model.php */
/* Location: ./application/models/safa_group_passports_flight_availabilties_model.php */