<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Internalpassagetypes_model extends CI_Model {

    public $safa_internalsegmenttype_id = FALSE;
    public $name_ar = FALSE;
    public $name_la = FALSE;
    public $description_ar = FALSE;
    public $description_la = FALSE;
    public $code = FALSE;
    public $color = FALSE;
    public $custom_select = FALSE;
    public $limit = FALSE;
    public $offset = FALSE;
    public $order_by = FALSE;

    function __construct() {
        parent::__construct();
    }

    function get($rows_no = FALSE) {

        if ($this->custom_select !== FALSE) {
            $this->db->select('safa_internalsegmenttype_id');
            $this->db->select($this->custom_select);
        }

        if ($this->safa_internalsegmenttype_id !== FALSE)
            $this->db->where('safa_internalsegmenttypes.safa_internalsegmenttype_id', $this->safa_internalsegmenttype_id);

        if ($this->name_ar !== FALSE)
            $this->db->where('safa_internalsegmenttypes.name_ar', $this->name_ar);

        if ($this->name_la !== FALSE)
            $this->db->where('safa_internalsegmenttypes.name_la', $this->name_la);

        if ($this->description_ar !== FALSE)
            $this->db->where('safa_internalsegmenttypes.description_ar', $this->description_ar);

        if ($this->description_la !== FALSE)
            $this->db->where('safa_internalsegmenttypes.description_la', $this->description_la);

        if ($this->code !== FALSE)
            $this->db->where('safa_internalsegmenttypes.code', $this->code);

        if ($this->color !== FALSE)
            $this->db->where('safa_internalsegmenttypes.color', $this->color);


        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('safa_internalsegmenttypes');
        if ($rows_no)
            return $query->num_rows();

        if ($this->safa_internalsegmenttype_id)
            return $query->row();
        else
            return $query->result();
    }

    function save() {
        if ($this->safa_internalsegmenttype_id !== FALSE)
            $this->db->set('safa_internalsegmenttypes.safa_internalsegmenttype_id', $this->safa_internalsegmenttype_id);

        if ($this->name_ar !== FALSE)
            $this->db->set('safa_internalsegmenttypes.name_ar', $this->name_ar);

        if ($this->name_la !== FALSE)
            $this->db->set('safa_internalsegmenttypes.name_la', $this->name_la);

        if ($this->description_ar !== FALSE)
            $this->db->set('safa_internalsegmenttypes.description_ar', $this->description_ar);

        if ($this->description_la !== FALSE)
            $this->db->set('safa_internalsegmenttypes.description_la', $this->description_la);

        if ($this->code !== FALSE)
            $this->db->set('safa_internalsegmenttypes.code', $this->code);

        if ($this->color !== FALSE)
            $this->db->set('safa_internalsegmenttypes.color', $this->color);



        if ($this->safa_internalsegmenttype_id) {
            $this->db->where('safa_internalsegmenttypes.safa_internalsegmenttype_id', $this->safa_internalsegmenttype_id)->update('safa_internalsegmenttypes');
        } else {
            $this->db->insert('safa_internalsegmenttypes');
            return $this->db->insert_id();
        }
    }

    function delete() {
        if ($this->safa_internalsegmenttype_id !== FALSE)
            $this->db->where('safa_internalsegmenttypes.safa_internalsegmenttype_id', $this->safa_internalsegmenttype_id);

        if ($this->name_ar !== FALSE)
            $this->db->where('safa_internalsegmenttypes.name_ar', $this->name_ar);

        if ($this->name_la !== FALSE)
            $this->db->where('safa_internalsegmenttypes.name_la', $this->name_la);

        if ($this->description_ar !== FALSE)
            $this->db->where('safa_internalsegmenttypes.description_ar', $this->description_ar);

        if ($this->description_la !== FALSE)
            $this->db->where('safa_internalsegmenttypes.description_la', $this->description_la);

        if ($this->code !== FALSE)
            $this->db->where('safa_internalsegmenttypes.code', $this->code);

        if ($this->color !== FALSE)
            $this->db->where('safa_internalsegmenttypes.color', $this->color);



        $this->db->delete('safa_internalsegmenttypes');
        return $this->db->affected_rows();
    }

    function search($rows_no = FALSE) {
        if ($this->custom_select !== FALSE) {
            $this->db->select('safa_internalsegmenttype_id');
            $this->db->select($this->custom_select);
        }

        if ($this->safa_internalsegmenttype_id !== FALSE)
            $this->db->where('safa_internalsegmenttypes.safa_internalsegmenttype_id', $this->safa_internalsegmenttype_id);

        if ($this->name_ar !== FALSE)
            $this->db->where('safa_internalsegmenttypes.name_ar', $this->name_ar);

        if ($this->name_la !== FALSE)
            $this->db->where('safa_internalsegmenttypes.name_la', $this->name_la);

        if ($this->description_ar !== FALSE)
            $this->db->where('safa_internalsegmenttypes.description_ar', $this->description_ar);

        if ($this->description_la !== FALSE)
            $this->db->where('safa_internalsegmenttypes.description_la', $this->description_la);

        if ($this->code !== FALSE)
            $this->db->where('safa_internalsegmenttypes.code', $this->code);

        if ($this->color !== FALSE)
            $this->db->where('safa_internalsegmenttypes.color', $this->color);


        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('safa_internalsegmenttypes');
        if ($rows_no)
            return $query->num_rows();

        return $query->result();
    }

}

/* End of file internalpassagetypes_model.php */
/* Location: ./application/models/internalpassagetypes_model.php */