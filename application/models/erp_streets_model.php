<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Erp_streets_model extends CI_Model {

    public $erp_street_id = FALSE;
    public $name_ar = FALSE;
    public $name_la = FALSE;
    public $geoarea_id = FALSE;
    public $custom_select = FALSE;
    public $limit = FALSE;
    public $offset = FALSE;
    public $order_by = FALSE;

    function __construct() {
        parent::__construct();
    }

    function get($rows_no = FALSE) {

        if ($this->custom_select !== FALSE) {
            $this->db->select('erp_street_id');
            $this->db->select($this->custom_select);
        }

        if ($this->erp_street_id !== FALSE)
            $this->db->where('erp_streets.erp_street_id', $this->erp_street_id);

        if ($this->name_ar !== FALSE)
            $this->db->where('erp_streets.name_ar', $this->name_ar);

        if ($this->name_la !== FALSE)
            $this->db->where('erp_streets.name_la', $this->name_la);

        if ($this->geoarea_id !== FALSE)
            $this->db->where('erp_streets.geoarea_id', $this->geoarea_id);


        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('erp_streets');
        if ($rows_no)
            return $query->num_rows();

        if ($this->erp_street_id)
            return $query->row();
        else
            return $query->result();
    }

    function save() {
        if ($this->erp_street_id !== FALSE)
            $this->db->set('erp_streets.erp_street_id', $this->erp_street_id);

        if ($this->name_ar !== FALSE)
            $this->db->set('erp_streets.name_ar', $this->name_ar);

        if ($this->name_la !== FALSE)
            $this->db->set('erp_streets.name_la', $this->name_la);

        if ($this->geoarea_id !== FALSE)
            $this->db->set('erp_streets.geoarea_id', $this->geoarea_id);



        if ($this->erp_street_id) {
            $this->db->where('erp_streets.erp_street_id', $this->erp_street_id)->update('erp_streets');
        } else {
            $this->db->insert('erp_streets');
            return $this->db->insert_id();
        }
    }

    function delete() {
        if ($this->erp_street_id !== FALSE)
            $this->db->where('erp_streets.erp_street_id', $this->erp_street_id);

        if ($this->name_ar !== FALSE)
            $this->db->where('erp_streets.name_ar', $this->name_ar);

        if ($this->name_la !== FALSE)
            $this->db->where('erp_streets.name_la', $this->name_la);

        if ($this->geoarea_id !== FALSE)
            $this->db->where('erp_streets.geoarea_id', $this->geoarea_id);
        $this->db->delete('erp_streets');
        return $this->db->affected_rows();
    }

    function search($rows_no = FALSE) {
        if ($this->custom_select !== FALSE) {
            $this->db->select('erp_street_id');
            $this->db->select($this->custom_select);
        }

        if ($this->erp_street_id !== FALSE)
            $this->db->where('erp_streets.erp_street_id', $this->erp_street_id);

        if ($this->name_ar !== FALSE)
            $this->db->where('erp_streets.name_ar', $this->name_ar);

        if ($this->name_la !== FALSE)
            $this->db->where('erp_streets.name_la', $this->name_la);

        if ($this->geoarea_id !== FALSE)
            $this->db->where('erp_streets.geoarea_id', $this->geoarea_id);


        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('erp_streets');
        if ($rows_no)
            return $query->num_rows();

        return $query->result();
    }

}

/* End of file erp_streets_model.php */
/* Location: ./application/models/erp_streets_model.php */