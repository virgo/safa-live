<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Hotels_availability_model extends CI_Model {

    public $erp_hotels_availability_master_id = FALSE;
    public $safa_company_id = FALSE;
    public $erp_company_type_id = FALSE;
    public $erp_country_id = FALSE;
    public $erp_city_id = FALSE;
    public $erp_hotel_id = FALSE;
    public $season_id = FALSE;
    public $date_from = FALSE;
    public $date_to = FALSE;
    public $purchase_currency_id = FALSE;
    public $sale_currency_id = FALSE;
    public $availability_status = FALSE;
    public $notes = FALSE;
    public $invoice_number = FALSE;
    public $invoice_image = FALSE;
    public $custom_select = FALSE;
    public $limit = FALSE;
    public $offset = FALSE;
    public $order_by = FALSE;
    public $join = FALSE;

    function __construct() {
        parent::__construct();
    }

    function get($rows_no = FALSE) {

        if ($this->custom_select !== FALSE) {
            $this->db->select('erp_hotels_availability_master_id');
            $this->db->select($this->custom_select);
        }

        if ($this->erp_hotels_availability_master_id !== FALSE)
            $this->db->where('erp_hotels_availability_master.erp_hotels_availability_master_id', $this->erp_hotels_availability_master_id);

        if ($this->safa_company_id !== FALSE)
            $this->db->where('erp_hotels_availability_master.safa_company_id', $this->safa_company_id);

        if ($this->erp_company_type_id !== FALSE)
            $this->db->where('erp_hotels_availability_master.erp_company_type_id', $this->erp_company_type_id);

        if ($this->erp_country_id !== FALSE)
            $this->db->where('erp_hotels_availability_master.erp_country_id', $this->erp_country_id);

        if ($this->erp_city_id !== FALSE)
            $this->db->where('erp_hotels_availability_master.erp_city_id', $this->erp_city_id);

        if ($this->erp_hotel_id !== FALSE)
            $this->db->where('erp_hotels_availability_master.erp_hotel_id', $this->erp_hotel_id);

        if ($this->season_id !== FALSE)
            $this->db->where('erp_hotels_availability_master.season_id', $this->season_id);

        if ($this->date_from !== FALSE)
            $this->db->where('erp_hotels_availability_master.date_from', $this->date_from);

        if ($this->date_to !== FALSE)
            $this->db->where('erp_hotels_availability_master.date_to', $this->date_to);

        if ($this->purchase_currency_id !== FALSE)
            $this->db->where('erp_hotels_availability_master.purchase_currency_id', $this->purchase_currency_id);

        if ($this->sale_currency_id !== FALSE)
            $this->db->where('erp_hotels_availability_master.sale_currency_id', $this->sale_currency_id);

        if ($this->availability_status !== FALSE)
            $this->db->where('erp_hotels_availability_master.availability_status', $this->availability_status);
        
        if ($this->join) {
            $this->db->select('erp_hotels_availability_master.*,erp_countries.' . name() . ' as country, erp_cities.' . name() . ' as city,erp_hotels.' . name() . ' as hotel, erp_seasons.' . name() . ' as season');
            $this->db->join('erp_cities', 'erp_cities.erp_city_id = erp_hotels_availability_master.erp_city_id');
            $this->db->join('erp_countries', 'erp_countries.erp_country_id = erp_cities.erp_country_id', 'left');
            $this->db->join('erp_hotels', 'erp_hotels.erp_hotel_id = erp_hotels_availability_master.erp_hotel_id');
            $this->db->join('erp_seasons', 'erp_seasons.erp_season_id = erp_hotels_availability_master.season_id', 'left');
        }


        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('erp_hotels_availability_master');
        if ($rows_no)
            return $query->num_rows();

        if ($this->erp_hotels_availability_master_id)
            return $query->row();
        else
            return $query->result();
    }

    function save() {
        if ($this->erp_hotels_availability_master_id !== FALSE)
            $this->db->set('erp_hotels_availability_master.erp_hotels_availability_master_id', $this->erp_hotels_availability_master_id);

        if ($this->safa_company_id !== FALSE)
            $this->db->set('erp_hotels_availability_master.safa_company_id', $this->safa_company_id);

        if ($this->erp_company_type_id !== FALSE)
            $this->db->set('erp_hotels_availability_master.erp_company_type_id', $this->erp_company_type_id);

        if ($this->erp_country_id !== FALSE)
            $this->db->set('erp_hotels_availability_master.erp_country_id', $this->erp_country_id);

        if ($this->erp_city_id !== FALSE)
            $this->db->set('erp_hotels_availability_master.erp_city_id', $this->erp_city_id);

        if ($this->erp_hotel_id !== FALSE)
            $this->db->set('erp_hotels_availability_master.erp_hotel_id', $this->erp_hotel_id);

        if ($this->season_id !== FALSE)
            $this->db->set('erp_hotels_availability_master.season_id', $this->season_id);

        if ($this->date_from !== FALSE)
            $this->db->set('erp_hotels_availability_master.date_from', $this->date_from);

        if ($this->date_to !== FALSE)
            $this->db->set('erp_hotels_availability_master.date_to', $this->date_to);

        if ($this->purchase_currency_id !== FALSE)
            $this->db->set('erp_hotels_availability_master.purchase_currency_id', $this->purchase_currency_id);

        if ($this->sale_currency_id !== FALSE)
            $this->db->set('erp_hotels_availability_master.sale_currency_id', $this->sale_currency_id);

        if ($this->invoice_number !== FALSE)
            $this->db->set('erp_hotels_availability_master.invoice_number', $this->invoice_number);

        if ($this->invoice_image !== FALSE)
            $this->db->set('erp_hotels_availability_master.invoice_image', $this->invoice_image);

        if ($this->availability_status !== FALSE)
            $this->db->set('erp_hotels_availability_master.availability_status', $this->availability_status);
        
        if ($this->notes !== FALSE)
            $this->db->set('erp_hotels_availability_master.notes', $this->notes);


        if ($this->erp_hotels_availability_master_id) {
            $this->db->where('erp_hotels_availability_master.erp_hotels_availability_master_id', $this->erp_hotels_availability_master_id)->update('erp_hotels_availability_master');
            return $this->erp_hotels_availability_master_id;
        } else {
            $this->db->insert('erp_hotels_availability_master');
            return $this->db->insert_id();
        }
    }

    function delete() {
        if ($this->erp_hotels_availability_master_id !== FALSE)
            $this->db->where('erp_hotels_availability_master.erp_hotels_availability_master_id', $this->erp_hotels_availability_master_id);

        if ($this->safa_company_id !== FALSE)
            $this->db->where('erp_hotels_availability_master.safa_company_id', $this->safa_company_id);

        if ($this->erp_company_type_id !== FALSE)
            $this->db->where('erp_hotels_availability_master.erp_company_type_id', $this->erp_company_type_id);

        if ($this->erp_country_id !== FALSE)
            $this->db->where('erp_hotels_availability_master.erp_country_id', $this->erp_country_id);

        if ($this->erp_city_id !== FALSE)
            $this->db->where('erp_hotels_availability_master.erp_city_id', $this->erp_city_id);

        if ($this->erp_hotel_id !== FALSE)
            $this->db->where('erp_hotels_availability_master.erp_hotel_id', $this->erp_hotel_id);

        if ($this->season_id !== FALSE)
            $this->db->where('erp_hotels_availability_master.season_id', $this->season_id);

        if ($this->date_from !== FALSE)
            $this->db->where('erp_hotels_availability_master.date_from', $this->date_from);

        if ($this->date_to !== FALSE)
            $this->db->where('erp_hotels_availability_master.date_to', $this->date_to);

        if ($this->purchase_currency_id !== FALSE)
            $this->db->where('erp_hotels_availability_master.purchase_currency_id', $this->purchase_currency_id);

        if ($this->sale_currency_id !== FALSE)
            $this->db->where('erp_hotels_availability_master.sale_currency_id', $this->sale_currency_id);

        if ($this->availability_status !== FALSE)
            $this->db->where('erp_hotels_availability_master.availability_status', $this->availability_status);

        $this->db->delete('erp_hotels_availability_master');
        return $this->db->affected_rows();
    }

    function search($rows_no = FALSE) {
        if ($this->custom_select !== FALSE) {
            $this->db->select('erp_hotels_availability_master_id');
            $this->db->select($this->custom_select);
        }

        if ($this->erp_hotels_availability_master_id !== FALSE)
            $this->db->where('erp_hotels_availability_master.erp_hotels_availability_master_id', $this->erp_hotels_availability_master_id);

        if ($this->safa_company_id !== FALSE)
            $this->db->where('erp_hotels_availability_master.safa_company_id', $this->safa_company_id);

        if ($this->erp_company_type_id !== FALSE)
            $this->db->where('erp_hotels_availability_master.erp_company_type_id', $this->erp_company_type_id);

        if ($this->erp_country_id !== FALSE)
            $this->db->where('erp_hotels_availability_master.erp_country_id', $this->erp_country_id);

        if ($this->erp_city_id !== FALSE)
            $this->db->where('erp_hotels_availability_master.erp_city_id', $this->erp_city_id);

        if ($this->erp_hotel_id !== FALSE)
            $this->db->where('erp_hotels_availability_master.erp_hotel_id', $this->erp_hotel_id);

        if ($this->season_id !== FALSE)
            $this->db->where('erp_hotels_availability_master.season_id', $this->season_id);

        if ($this->date_from !== FALSE)
            $this->db->where('erp_hotels_availability_master.date_from', $this->date_from);

        if ($this->date_to !== FALSE)
            $this->db->where('erp_hotels_availability_master.date_to', $this->date_to);

        if ($this->purchase_currency_id !== FALSE)
            $this->db->where('erp_hotels_availability_master.purchase_currency_id', $this->purchase_currency_id);

        if ($this->sale_currency_id !== FALSE)
            $this->db->where('erp_hotels_availability_master.sale_currency_id', $this->sale_currency_id);

        if ($this->availability_status !== FALSE)
            $this->db->where('erp_hotels_availability_master.availability_status', $this->availability_status);
        
        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('erp_hotels_availability_master');
        if ($rows_no)
            return $query->num_rows();

        return $query->result();
    }

}

/* End of file hotels availability_model.php */
/* Location: ./application/models/hotels availability_model.php */