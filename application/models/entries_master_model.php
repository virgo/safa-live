<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Entries_master_model extends CI_Model {

    public $id = FALSE;
    public $simple = FALSE;
    public $firm_id = FALSE;
    public $entry_number = FALSE;
    public $master_date = FALSE;
    public $currency = FALSE;
    public $rate = FALSE;
    public $transaction_entry = FALSE;
    public $transaction_kind = FALSE;
    public $transaction_id = FALSE;
    public $limit = FALSE;
    public $offset = FALSE;

    function __construct() {
        parent::__construct();
    }

    function get($rows_no = FALSE) {

        if ($this->id)
            $this->db->where('id', $this->id);

        if ($this->simple !== FALSE)
            $this->db->where('simple', $this->simple);

//        if ($this->firm_id)
        $this->db->where('firm_id', session('firm_id'));

        if ($this->entry_number)
            $this->db->where('entry_number', $this->entry_number);

        if ($this->master_date)
            $this->db->where('master_date', $this->master_date);

        if ($this->currency)
            $this->db->where('currency', $this->currency);

        if ($this->rate)
            $this->db->where('rate', $this->rate);

        if ($this->transaction_entry !== FALSE)
            $this->db->where('transaction_entry', $this->transaction_entry);

        if ($this->transaction_kind)
            $this->db->where('transaction_kind', $this->transaction_kind);

        if ($this->transaction_id)
            $this->db->where('transaction_id', $this->transaction_id);

        $this->db->select('*
            , (SELECT count(*) FROM entries_details WHERE entries_master.id = entries_details.entry_id) as num_rows
            ');
        $this->db->order_by('master_date', 'desc');
        $this->db->order_by('entry_number', 'desc');
        $query = $this->db->get('entries_master', $this->limit, $this->offset);

        if ($rows_no)
            return $query->num_rows();
        else
        if ($this->id || $this->entry_number)
            return $query->row();
        else
            return $query->result();
    }

    function save() {

        if ($this->id)
            $this->db->set('id', $this->id);

        if ($this->simple !== FALSE)
            $this->db->set('simple', $this->simple);

//        if ($this->firm_id)
        $this->db->set('firm_id', session('firm_id'));

        if ($this->entry_number)
            $this->db->set('entry_number', $this->entry_number);

        if ($this->master_date)
            $this->db->set('master_date', $this->master_date);

        if ($this->currency)
            $this->db->set('currency', $this->currency);

        if ($this->rate)
            $this->db->set('rate', $this->rate);

        if ($this->transaction_entry !== FALSE)
            $this->db->set('transaction_entry', $this->transaction_entry);

        if ($this->transaction_kind)
            $this->db->set('transaction_kind', $this->transaction_kind);

        if ($this->transaction_id)
            $this->db->set('transaction_id', $this->transaction_id);

        if ($this->id) {
            $this->db->where('id', $this->id)->update('entries_master');
        } else {
            $this->db->insert('entries_master');
            return $this->db->insert_id();
        }
    }

    function delete() {
        if ($this->firm_id)
            $this->db->where('firm_id', $this->firm_id);
        else
            $this->db->where('firm_id', session('firm_id'));
        if ($this->id)
            $this->db->where('id', $this->id)->delete('entries_master');
        return $this->db->affected_rows();
    }

}
