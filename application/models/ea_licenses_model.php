<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Ea_licenses_model extends CI_Model {

    public $crm_ea_license_id = FALSE;
    public $safa_ea_id = FALSE;
    public $crm_license_id = FALSE;
    public $over_credit = FALSE;
    public $status = FALSE;
    public $custom_select = FALSE;
    public $limit = FALSE;
    public $offset = FALSE;
    public $order_by = FALSE;

    function __construct() {
        parent::__construct();
    }

    function get($rows_no = FALSE) {

        if ($this->custom_select !== FALSE) {
            $this->db->select('crm_ea_license_id');
            $this->db->select($this->custom_select);
        }

        if ($this->crm_ea_license_id !== FALSE)
            $this->db->where('crm_ea_licenses.crm_ea_license_id', $this->crm_ea_license_id);

        if ($this->safa_ea_id !== FALSE)
            $this->db->where('crm_ea_licenses.safa_ea_id', $this->safa_ea_id);

        if ($this->crm_license_id !== FALSE)
            $this->db->where('crm_ea_licenses.crm_license_id', $this->crm_license_id);

        if ($this->over_credit !== FALSE)
            $this->db->where('crm_ea_licenses.over_credit', $this->over_credit);

        if ($this->status !== FALSE)
            $this->db->where('crm_ea_licenses.status', $this->status);


        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('crm_ea_licenses');
        if ($rows_no)
            return $query->num_rows();

        if ($this->crm_ea_license_id)
            return $query->row();
        else
            return $query->result();
    }

    function save() {
        if ($this->crm_ea_license_id !== FALSE)
            $this->db->set('crm_ea_licenses.crm_ea_license_id', $this->crm_ea_license_id);

        if ($this->safa_ea_id !== FALSE)
            $this->db->set('crm_ea_licenses.safa_ea_id', $this->safa_ea_id);

        if ($this->crm_license_id !== FALSE)
            $this->db->set('crm_ea_licenses.crm_license_id', $this->crm_license_id);

        if ($this->over_credit !== FALSE)
            $this->db->set('crm_ea_licenses.over_credit', $this->over_credit);

        if ($this->status !== FALSE)
            $this->db->set('crm_ea_licenses.status', $this->status);



        if ($this->crm_ea_license_id) {
            $this->db->where('crm_ea_licenses.crm_ea_license_id', $this->crm_ea_license_id)->update('crm_ea_licenses');
        } else {
            $this->db->insert('crm_ea_licenses');
            return $this->db->insert_id();
        }
    }

    function delete() {
        if ($this->crm_ea_license_id !== FALSE)
            $this->db->where('crm_ea_licenses.crm_ea_license_id', $this->crm_ea_license_id);

        if ($this->safa_ea_id !== FALSE)
            $this->db->where('crm_ea_licenses.safa_ea_id', $this->safa_ea_id);

        if ($this->crm_license_id !== FALSE)
            $this->db->where('crm_ea_licenses.crm_license_id', $this->crm_license_id);

        if ($this->over_credit !== FALSE)
            $this->db->where('crm_ea_licenses.over_credit', $this->over_credit);

        if ($this->status !== FALSE)
            $this->db->where('crm_ea_licenses.status', $this->status);



        $this->db->delete('crm_ea_licenses');
        return $this->db->affected_rows();
    }

    function search($rows_no = FALSE) {
        if ($this->custom_select !== FALSE) {
            $this->db->select('crm_ea_license_id');
            $this->db->select($this->custom_select);
        }

        if ($this->crm_ea_license_id !== FALSE)
            $this->db->where('crm_ea_licenses.crm_ea_license_id', $this->crm_ea_license_id);

        if ($this->safa_ea_id !== FALSE)
            $this->db->where('crm_ea_licenses.safa_ea_id', $this->safa_ea_id);

        if ($this->crm_license_id !== FALSE)
            $this->db->where('crm_ea_licenses.crm_license_id', $this->crm_license_id);

        if ($this->over_credit !== FALSE)
            $this->db->where('crm_ea_licenses.over_credit', $this->over_credit);

        if ($this->status !== FALSE)
            $this->db->where('crm_ea_licenses.status', $this->status);


        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('crm_ea_licenses');
        if ($rows_no)
            return $query->num_rows();

        return $query->result();
    }

}

/* End of file ea_licenses_model.php */
/* Location: ./application/models/ea_licenses_model.php */