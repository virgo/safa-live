<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Uoprivileges_model extends CI_Model {

    public $safa_uoprivilege_id = FALSE;
    public $name_ar = FALSE;
    public $name_la = FALSE;
    public $description_ar = FALSE;
    public $description_la = FALSE;
    public $role_name = FALSE;
    public $custom_select = FALSE;
    public $limit = FALSE;
    public $offset = FALSE;
    public $order_by = FALSE;

    function __construct() {
        parent::__construct();
    }

    function get($rows_no = FALSE) {

        if ($this->custom_select !== FALSE) {
            $this->db->select('safa_uoprivilege_id');
            $this->db->select($this->custom_select);
        }

        if ($this->safa_uoprivilege_id !== FALSE)
            $this->db->where('safa_uoprivileges.safa_uoprivilege_id', $this->safa_uoprivilege_id);

        if ($this->name_ar !== FALSE)
            $this->db->where('safa_uoprivileges.name_ar', $this->name_ar);

        if ($this->name_la !== FALSE)
            $this->db->where('safa_uoprivileges.name_la', $this->name_la);

        if ($this->description_ar !== FALSE)
            $this->db->where('safa_uoprivileges.description_ar', $this->description_ar);

        if ($this->description_la !== FALSE)
            $this->db->where('safa_uoprivileges.description_la', $this->description_la);

        if ($this->role_name !== FALSE)
            $this->db->where('safa_uoprivileges.role_name', $this->role_name);

        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('safa_uoprivileges');
        if ($rows_no)
            return $query->num_rows();

        if ($this->safa_uoprivilege_id)
            return $query->row();
        else
            return $query->result();
    }

    function save() {
        if ($this->safa_uoprivilege_id !== FALSE)
            $this->db->set('safa_uoprivileges.safa_uoprivilege_id', $this->safa_uoprivilege_id);

        if ($this->name_ar !== FALSE)
            $this->db->set('safa_uoprivileges.name_ar', $this->name_ar);

        if ($this->name_la !== FALSE)
            $this->db->set('safa_uoprivileges.name_la', $this->name_la);

        if ($this->description_ar !== FALSE)
            $this->db->set('safa_uoprivileges.description_ar', $this->description_ar);

        if ($this->description_la !== FALSE)
            $this->db->set('safa_uoprivileges.description_la', $this->description_la);

        if ($this->role_name !== FALSE)
            $this->db->set('safa_uoprivileges.role_name', $this->role_name);



        if ($this->safa_uoprivilege_id) {
            $this->db->where('safa_uoprivileges.safa_uoprivilege_id', $this->safa_uoprivilege_id)->update('safa_uoprivileges');
        } else {
            $this->db->insert('safa_uoprivileges');
            return $this->db->insert_id();
        }
    }

    function delete() {
        if ($this->safa_uoprivilege_id !== FALSE)
            $this->db->where('safa_uoprivileges.safa_uoprivilege_id', $this->safa_uoprivilege_id);

        if ($this->name_ar !== FALSE)
            $this->db->where('safa_uoprivileges.name_ar', $this->name_ar);

        if ($this->name_la !== FALSE)
            $this->db->where('safa_uoprivileges.name_la', $this->name_la);

        if ($this->description_ar !== FALSE)
            $this->db->where('safa_uoprivileges.description_ar', $this->description_ar);

        if ($this->description_la !== FALSE)
            $this->db->where('safa_uoprivileges.description_la', $this->description_la);

        if ($this->role_name !== FALSE)
            $this->db->where('safa_uoprivileges.role_name', $this->role_name);



        $this->db->delete('safa_uoprivileges');
        return $this->db->affected_rows();
    }

    function search($rows_no = FALSE) {
        if ($this->custom_select !== FALSE) {
            $this->db->select('safa_uoprivilege_id');
            $this->db->select($this->custom_select);
        }

        if ($this->safa_uoprivilege_id !== FALSE)
            $this->db->where('safa_uoprivileges.safa_uoprivilege_id', $this->safa_uoprivilege_id);

        if ($this->name_ar !== FALSE)
            $this->db->where('safa_uoprivileges.name_ar', $this->name_ar);

        if ($this->name_la !== FALSE)
            $this->db->where('safa_uoprivileges.name_la', $this->name_la);

        if ($this->description_ar !== FALSE)
            $this->db->where('safa_uoprivileges.description_ar', $this->description_ar);

        if ($this->description_la !== FALSE)
            $this->db->where('safa_uoprivileges.description_la', $this->description_la);

        if ($this->role_name !== FALSE)
            $this->db->where('safa_uoprivileges.role_name', $this->role_name);


        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('safa_uoprivileges');
        if ($rows_no)
            return $query->num_rows();

        return $query->result();
    }

    function check_delete_ability($id) {
        $this->db->select('count(safa_uo_usergroups_uoprivileges.safa_uoprivilege_id)as uoprivilege_id');
        $this->db->from('safa_uoprivileges');
        $this->db->join('safa_uo_usergroups_uoprivileges', 'safa_uoprivileges.safa_uoprivilege_id = safa_uo_usergroups_uoprivileges.safa_uoprivilege_id', 'left');
        $this->db->group_by('safa_uoprivileges.safa_uoprivilege_id');
        $this->db->where('safa_uoprivileges.safa_uoprivilege_id', $id);
        $query = $this->db->get();
        $flag = 0;
        foreach ($query->result() as $row => $table) {
            foreach ($table as $col => $value) {
                if ($value > 0) {
                    $flag = 1;
                    break;
                } else {
                    continue;
                }
            }
        }
        return $flag;
    }

}

/* End of file uoprivileges_model.php */
/* Location: ./application/models/uoprivileges_model.php */