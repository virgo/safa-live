<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Messages_model extends CI_Model {

    public $tbl_name = "erp_messages";
    public $erp_messages_id = FALSE;
    public $erp_importance_id = FALSE;
    public $sender_type_id = FALSE;
    public $sender_id = FALSE;
    public $sender_user_id = FALSE;
    public $message_subject = FALSE;
    public $message_body = FALSE;
    public $message_datetime = FALSE;
    public $parent_id = FALSE;

    /**
     * Detail table fields
     */
    public $detail_erp_messages_detail_id = FALSE;
    public $detail_erp_messages_id = FALSE;
    public $detail_receiver_type = FALSE;
    public $detail_receiver_id = FALSE;

    /**
     * attachements table fields
     */
    public $attachements_erp_messages_attachements_id = FALSE;
    public $attachements_erp_messages_id = FALSE;
    public $attachements_path = FALSE;
    public $is_seen = FALSE;
    public $is_deleted = FALSE;
    public $is_archived = FALSE;
    public $seen_datetime = FALSE;
    public $folder_id = FALSE;
    public $custom_select = FALSE;
    public $limit = FALSE;
    public $offset = FALSE;
    public $order_by = FALSE;

    function __construct() {
        parent::__construct();
    }

    function save() {
        if ($this->erp_messages_id !== FALSE)
            $this->db->set('erp_messages.erp_messages_id', $this->erp_messages_id);

        if ($this->erp_importance_id !== FALSE)
            $this->db->set('erp_messages.erp_importance_id', $this->erp_importance_id);

        if ($this->sender_type_id !== FALSE)
            $this->db->set('erp_messages.sender_type_id', $this->sender_type_id);

        if ($this->sender_id !== FALSE)
            $this->db->set('erp_messages.sender_id', $this->sender_id);

        if ($this->sender_user_id !== FALSE)
            $this->db->set('erp_messages.sender_user_id', $this->sender_user_id);

        if ($this->message_subject !== FALSE)
            $this->db->set('erp_messages.message_subject', $this->message_subject);

        if ($this->message_body !== FALSE)
            $this->db->set('erp_messages.message_body', $this->message_body);

        if ($this->message_datetime !== FALSE)
            $this->db->set('erp_messages.message_datetime', $this->message_datetime);

        if ($this->parent_id !== FALSE)
            $this->db->set('erp_messages.parent_id', $this->parent_id);



        if ($this->erp_messages_id) {
            $this->db->where('erp_messages.erp_messages_id', $this->erp_messages_id)->update('erp_messages');
        } else {
            $this->db->insert('erp_messages');
            return $this->db->insert_id();
        }
    }

    function saveDetails() {
        if ($this->detail_erp_messages_detail_id !== FALSE)
            $this->db->set('erp_messages_detail.erp_messages_detail_id', $this->detail_erp_messages_detail_id);

        if ($this->detail_erp_messages_id !== FALSE)
            $this->db->set('erp_messages_detail.erp_messages_id', $this->detail_erp_messages_id);

        if ($this->detail_receiver_type !== FALSE)
            $this->db->set('erp_messages_detail.receiver_type', $this->detail_receiver_type);

        if ($this->detail_receiver_id !== FALSE)
            $this->db->set('erp_messages_detail.receiver_id', $this->detail_receiver_id);


        if ($this->detail_erp_messages_detail_id) {
            $this->db->where('erp_messages_detail.erp_messages_detail_id', $this->detail_erp_messages_detail_id)->update('erp_messages_detail');
        } else {
            $this->db->insert('erp_messages_detail');
            return $this->db->insert_id();
        }
    }

    function saveAttachements() {
        if ($this->attachements_erp_messages_attachements_id !== FALSE)
            $this->db->set('erp_messages_attachements.erp_messages_attachements_id', $this->attachements_erp_messages_attachements_id);

        if ($this->attachements_erp_messages_id !== FALSE)
            $this->db->set('erp_messages_attachements.erp_messages_id', $this->attachements_erp_messages_id);


        if ($this->attachements_path !== FALSE)
            $this->db->set('erp_messages_attachements.path', $this->attachements_path);


        if ($this->attachements_erp_messages_attachements_id) {
            $this->db->where('erp_messages_attachements.erp_messages_attachements_id', $this->attachements_erp_messages_attachements_id)->update('erp_messages_attachements');
        } else {
            $this->db->insert('erp_messages_attachements');
            return $this->db->insert_id();
        }
    }

    function getByReceiverTypeAndReceiverIdAndUserId($receiver_type = false, $receiver_id = false, $user_id = false) {

        $this->db->select('erp_messages.*,erp_messages_detail.*,erp_messages_user_seen.mark_as_read,erp_messages_user_seen.mark_as_important');
        $this->db->from('erp_messages');
        $this->db->join('erp_messages_detail', 'erp_messages.erp_messages_id = erp_messages_detail.erp_messages_id');
        $this->db->join('erp_messages_user_seen', 'erp_messages_detail.erp_messages_detail_id = erp_messages_user_seen.erp_messages_detail_id', 'left');

        //$this->db->where('erp_messages.notification_type', 'automatic');
        $this->db->where('(erp_messages_user_seen.is_hide IS NULL or erp_messages_user_seen.is_hide=0)');

        if ($receiver_type !== false)
            $this->db->where('erp_messages_detail.receiver_type', $receiver_type);

        if ($receiver_id !== false)
            $this->db->where('erp_messages_detail.receiver_id', $receiver_id);

        if ($user_id !== false)
            $this->db->where("(erp_messages_user_seen.user_id=$user_id or erp_messages_user_seen.user_id is null)");


        $this->db->order_by('erp_messages.erp_messages_id', 'desc');

        $this->db->limit(10);


        $query = $this->db->get();

        $query_text = $this->db->last_query();

        return $query->result();
    }

    function getUnreadedByReceiverTypeAndReceiverIdAndUserId($receiver_type = false, $receiver_id = false, $user_id = false, $rows_no = false) {
        $this->db->select('count(`erp_messages`.`erp_messages_id`) as unreaded_messages_count')->from('erp_messages');
        $this->db->join('erp_messages_detail', 'erp_messages.erp_messages_id = erp_messages_detail.erp_messages_id');
        $this->db->join('erp_messages_user_seen', 'erp_messages_detail.erp_messages_detail_id = erp_messages_user_seen.erp_messages_detail_id', 'left');

        $this->db->where("`erp_messages`.`erp_messages_id` NOT IN (SELECT `erp_messages`.`erp_messages_id` FROM `erp_messages`
        inner join `erp_messages_detail` on `erp_messages`.`erp_messages_id` = `erp_messages_detail`.`erp_messages_id`
        left outer join `erp_messages_user_seen` on `erp_messages_detail`.`erp_messages_detail_id` = `erp_messages_user_seen`.`erp_messages_detail_id`
        where `erp_messages_detail`.`receiver_type`='$receiver_type' and
        `erp_messages_detail`.`receiver_id`='$receiver_id' and
        `erp_messages_user_seen`.`user_id`='$user_id' and 
        (erp_messages_user_seen.mark_as_read =1 or erp_messages_user_seen.is_hide=1) 
        )", NULL, FALSE);

        $this->db->where('erp_messages_detail.receiver_type', $receiver_type);
        $this->db->where('erp_messages_detail.receiver_id', $receiver_id);
        $this->db->where("(erp_messages_user_seen.user_id=$user_id or erp_messages_user_seen.user_id is null)");


        $query = $this->db->get();

        //$query_text=$this->db->last_query();

        return $query->result();
    }

    function getByReceiverTypeAndReceiverIdAndUserIdForDisplay($receiver_type = false, $receiver_id = false, $user_id = false, $rows_no = false, $mark_as_important = false, $is_hide = false) {

        $this->db->select('erp_messages.*,erp_messages_detail.*,erp_messages_user_seen.mark_as_read,erp_messages_user_seen.mark_as_important,erp_messages_user_seen.is_hide ');
        $this->db->from('erp_messages');
        $this->db->join('erp_messages_detail', 'erp_messages.erp_messages_id = erp_messages_detail.erp_messages_id');
        $this->db->join('erp_messages_user_seen', 'erp_messages_detail.erp_messages_detail_id = erp_messages_user_seen.erp_messages_detail_id', 'left');

        //$this->db->where('erp_messages.notification_type', 'automatic');
        if ($is_hide == false) {
            $this->db->where('(erp_messages_user_seen.is_hide IS NULL or erp_messages_user_seen.is_hide=0)');
        } else {
            $this->db->where('erp_messages_user_seen.is_hide', $is_hide);
        }

        if ($receiver_type !== false)
            $this->db->where('erp_messages_detail.receiver_type', $receiver_type);

        if ($receiver_id !== false)
            $this->db->where('erp_messages_detail.receiver_id', $receiver_id);

        if ($user_id !== false)
            $this->db->where('erp_messages_user_seen.user_id', $user_id);

        if ($mark_as_important !== false)
            $this->db->where('erp_messages_user_seen.mark_as_important', $mark_as_important);

        $this->db->order_by('erp_messages.erp_messages_id', 'desc');


        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get();

        //$query_text=$this->db->last_query();
        //echo $query_text; exit;

        if ($rows_no)
            return $query->num_rows();

        return $query->result();
    }

    function getByDetailId($erp_messages_detail_id = false, $rows_no = false) {

        $this->db->select('erp_messages.*, erp_messages_detail.*');
        $this->db->from('erp_messages');
        $this->db->join('erp_messages_detail', 'erp_messages.erp_messages_id = erp_messages_detail.erp_messages_id');

        if ($erp_messages_detail_id !== false)
            $this->db->where('erp_messages_detail.erp_messages_detail_id', $erp_messages_detail_id);

        $query = $this->db->get();
        return $query->row();
    }

    function getServerDatetime() {
        $this->db->select('NOW() as current_datetime');
        $this->db->from('erp_messages');
        $query = $this->db->get();
        return $query->result();
    }

    function getCompaniesByCompanyType($erp_company_type_id) {

        if ($erp_company_type_id == 1) {
            $this->db->select('erp_admins.erp_admin_id as id, erp_admins.username as name_ar, erp_admins.username as name_la');
            $this->db->from('erp_admins');
        } else if ($erp_company_type_id == 2) {
            $this->db->select('safa_uos.safa_uo_id as id, safa_uos.name_ar, safa_uos.name_la');
            $this->db->from('safa_uos');
            $this->db->where('safa_uos.disabled', 0);
            //$this->db->where('safa_uos.', '');
        } else if ($erp_company_type_id == 3) {
            $this->db->select('safa_eas.safa_ea_id as id, safa_eas.name_ar, safa_eas.name_la');
            $this->db->from('safa_eas');
            $this->db->where('safa_eas.disabled', 0);
        } else if ($erp_company_type_id == 4) {

            $this->db->select('safa_hms.safa_hm_id as id, safa_hms.name_ar, safa_hms.name_la');
            $this->db->from('safa_hms');
        } else if ($erp_company_type_id == 5) {
            $this->db->select('safa_itos.safa_ito_id as id, safa_itos.name_ar, safa_itos.name_la');
            $this->db->from('safa_itos');
            $this->db->where('safa_itos.disabled', 0);
        } else {
            $this->db->select('safa_eas.safa_ea_id as id, safa_eas.name_ar, safa_eas.name_la');
            $this->db->from('safa_eas');
        }


        $query = $this->db->get();

        //$query_text=$this->db->last_query();
        //echo $query_text; exit;

        return $query->result();
    }

    function getCompaniesByCompanyTypeAndCountries($erp_company_type_id, $countries_arr) {

        if ($erp_company_type_id == 1) {
            $this->db->select('erp_admins.erp_admin_id as id, erp_admins.username as name_ar, erp_admins.username as name_la');
            $this->db->from('erp_admins');
        } else if ($erp_company_type_id == 2) {
            $this->db->select('safa_uos.safa_uo_id as id, safa_uos.name_ar, safa_uos.name_la');
            $this->db->from('safa_uos');
            $this->db->where('safa_uos.disabled', 0);
            //$this->db->where('safa_uos.', '');
        } else if ($erp_company_type_id == 3) {
            $this->db->select('safa_eas.safa_ea_id as id, safa_eas.name_ar, safa_eas.name_la');
            $this->db->from('safa_eas');
            $this->db->where('safa_eas.disabled', 0);
            if (count($countries_arr) > 0) {
                $this->db->where_in('safa_eas.erp_country_id', $countries_arr);
            }
        } else if ($erp_company_type_id == 4) {


            $this->db->select('safa_hms.safa_hm_id as id, safa_hms.name_ar, safa_hms.name_la');
            $this->db->from('safa_hms');
        } else if ($erp_company_type_id == 5) {
            $this->db->select('safa_itos.safa_ito_id as id, safa_itos.name_ar, safa_itos.name_la');
            $this->db->from('safa_itos');
            $this->db->where('safa_itos.disabled', 0);
            if (count($countries_arr) > 0) {
                $this->db->where_in('safa_itos.erp_country_id', $countries_arr);
            }
        } else {
            $this->db->select('safa_eas.safa_ea_id as id, safa_eas.name_ar, safa_eas.name_la');
            $this->db->from('safa_eas');
            if (count($countries_arr) > 0) {
                $this->db->where_in('safa_eas.erp_country_id', $countries_arr);
            }
        }


        $query = $this->db->get();

        //$query_text=$this->db->last_query();
        //echo $query_text; exit;

        return $query->result();
    }

    function getAttachementsByMessageId($erp_messages_id = false) {
        $this->db->select('erp_messages_attachements.*');
        $this->db->from('erp_messages_attachements');

        if ($erp_messages_id !== false)
            $this->db->where('erp_messages_attachements.erp_messages_id', $erp_messages_id);

        $query = $this->db->get();
        return $query->result();
    }

}

/* End of file countries_model.php */
/* Location: ./application/models/countries_model.php */