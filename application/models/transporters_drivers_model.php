<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Transporters_drivers_model extends CI_Model {

    public $safa_transporters_drivers_id = FALSE;
    public $name_ar = FALSE;
    public $name_la = FALSE;
    public $phone = FALSE;
    public $safa_transporters_id = FALSE;
    public $custom_select = FALSE;
    public $limit = FALSE;
    public $offset = FALSE;
    public $order_by = FALSE;
    public $join;

    function __construct() {
        parent::__construct();
    }

    function get($rows_no = FALSE) {
        if ($this->custom_select !== FALSE) {
            $this->db->select('safa_transporters_drivers_id');
            $this->db->select($this->custom_select);
        }

        if ($this->safa_transporters_drivers_id !== FALSE)
            $this->db->where('safa_transporters_drivers.safa_transporters_drivers_id', $this->safa_transporters_drivers_id);

        if ($this->name_ar !== FALSE)
            $this->db->where('safa_transporters_drivers.name_ar', $this->name_ar);

        if ($this->name_la !== FALSE)
            $this->db->where('safa_transporters_drivers.name_la', $this->name_la);

        if ($this->phone !== FALSE)
            $this->db->where('safa_transporters_drivers.phone', $this->phone);

        if ($this->safa_transporters_id !== FALSE)
            $this->db->where('safa_transporters_drivers.safa_transporters_id', $this->safa_transporters_id);

        if ($this->join) {
            $this->db->select('safa_transporters.name_ar as transporter_name_ar, safa_transporters.name_la as transporter_name_la, safa_transporters_drivers.*');
            $this->db->join('safa_transporters', 'safa_transporters_drivers.safa_transporters_id = safa_transporters.safa_transporter_id');
        }


        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('safa_transporters_drivers');

        //echo $query_text=$this->db->last_query();

        if ($rows_no)
            return $query->num_rows();

        if ($this->safa_transporters_drivers_id)
            return $query->row();
        else
            return $query->result();
    }

    function save() {

        if ($this->safa_transporters_drivers_id !== FALSE)
            $this->db->set('safa_transporters_drivers.safa_transporters_drivers_id', $this->safa_transporters_drivers_id);

        if ($this->name_ar !== FALSE)
            $this->db->set('safa_transporters_drivers.name_ar', $this->name_ar);

        if ($this->name_la !== FALSE)
            $this->db->set('safa_transporters_drivers.name_la', $this->name_la);

        if ($this->phone !== FALSE)
            $this->db->set('safa_transporters_drivers.phone', $this->phone);

        if ($this->safa_transporters_id !== FALSE)
            $this->db->set('safa_transporters_drivers.safa_transporters_id', $this->safa_transporters_id);


        if ($this->safa_transporters_drivers_id) {
            $this->db->where('safa_transporters_drivers.safa_transporters_drivers_id', $this->safa_transporters_drivers_id)->update('safa_transporters_drivers');
        } else {
            $this->db->insert('safa_transporters_drivers');
            return $this->db->insert_id();
        }
    }

    function delete() {
        if ($this->safa_transporters_drivers_id !== FALSE)
            $this->db->where('safa_transporters_drivers.safa_transporters_drivers_id', $this->safa_transporters_drivers_id);

        if ($this->name_ar !== FALSE)
            $this->db->where('safa_transporters_drivers.name_ar', $this->name_ar);

        if ($this->name_la !== FALSE)
            $this->db->where('safa_transporters_drivers.name_la', $this->name_la);

        if ($this->phone !== FALSE)
            $this->db->where('safa_transporters_drivers.phone', $this->phone);

        if ($this->safa_transporters_id !== FALSE)
            $this->db->where('safa_transporters_drivers.safa_transporters_id', $this->safa_transporters_id);


        $this->db->delete('safa_transporters_drivers');
        return $this->db->affected_rows();
    }

}

/* End of file transporter_drivers_model.php */
/* Location: ./application/models/transporter_drivers_model.php */