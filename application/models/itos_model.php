<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Itos_model extends CI_Model {

    public $safa_ito_id = FALSE;
    public $name_ar = FALSE;
    public $name_la = FALSE;
    public $username = FALSE;
    public $password = FALSE;
    public $erp_country_id = FALSE;
    public $reference_code = FALSE;
    public $custom_select = FALSE;
    public $limit = FALSE;
    public $offset = FALSE;
    public $order_by = FALSE;
    public $disabled = FALSE;
    public $join;

    function __construct() {
        parent::__construct();
    }

    function get($rows_no = FALSE) {

        if ($this->custom_select !== FALSE) {
            $this->db->select('safa_ito_id');
            $this->db->select($this->custom_select);
        }

        if ($this->safa_ito_id !== FALSE)
            $this->db->where('safa_itos.safa_ito_id', $this->safa_ito_id);

        if ($this->name_ar !== FALSE)
            $this->db->where('safa_itos.name_ar', $this->name_ar);

        if ($this->name_la !== FALSE)
            $this->db->where('safa_itos.name_la', $this->name_la);

        if ($this->username !== FALSE)
            $this->db->where('safa_itos.username', $this->username);

        if ($this->password !== FALSE)
            $this->db->where('safa_itos.password', $this->password);

        if ($this->erp_country_id !== FALSE)
            $this->db->where('erp_countries.erp_country_id', $this->erp_country_id);

        if ($this->reference_code !== FALSE)
            $this->db->where('safa_itos.reference_code', $this->reference_code);
        if ($this->disabled !== FALSE)
            $this->db->where('safa_itos.disabled', $this->disabled);
        if ($this->join) {
            $this->db->select('erp_countries.name_ar as country_name,erp_countries.erp_country_id,safa_itos.name_ar as arabic_name,safa_itos.name_la as english_name,safa_itos.safa_ito_id ,
             safa_itos.erp_country_id,safa_itos.reference_code');
            $this->db->join('erp_countries', 'safa_itos.erp_country_id = erp_countries.erp_country_id');
        }


        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('safa_itos');
        if ($rows_no)
            return $query->num_rows();

        if ($this->safa_ito_id)
            return $query->row();
        else
            return $query->result();
    }

    function save() {
        if ($this->safa_ito_id !== FALSE)
            $this->db->set('safa_itos.safa_ito_id', $this->safa_ito_id);

        if ($this->name_ar !== FALSE)
            $this->db->set('safa_itos.name_ar', $this->name_ar);

        if ($this->name_la !== FALSE)
            $this->db->set('safa_itos.name_la', $this->name_la);

        if ($this->username !== FALSE)
            $this->db->set('safa_itos.username', $this->username);

        if ($this->password !== FALSE)
            $this->db->set('safa_itos.password', $this->password);

        if ($this->erp_country_id !== FALSE)
            $this->db->set('safa_itos.erp_country_id', $this->erp_country_id);

        if ($this->reference_code !== FALSE)
            $this->db->set('safa_itos.reference_code', $this->reference_code);

        if ($this->disabled !== FALSE)
            $this->db->set('safa_itos.disabled', $this->disabled);


        if ($this->safa_ito_id) {
            return $this->db->where('safa_itos.safa_ito_id', $this->safa_ito_id)->update('safa_itos');
        } else {
            $this->db->insert('safa_itos');
            return $this->db->insert_id();
        }
    }

    function delete() {
        if ($this->safa_ito_id !== FALSE)
            $this->db->where('safa_itos.safa_ito_id', $this->safa_ito_id);

        if ($this->name_ar !== FALSE)
            $this->db->where('safa_itos.name_ar', $this->name_ar);

        if ($this->name_la !== FALSE)
            $this->db->where('safa_itos.name_la', $this->name_la);

        if ($this->username !== FALSE)
            $this->db->where('safa_itos.username', $this->username);

        if ($this->password !== FALSE)
            $this->db->where('safa_itos.password', $this->password);

        if ($this->erp_country_id !== FALSE)
            $this->db->where('safa_itos.erp_country_id', $this->erp_country_id);

        if ($this->disabled !== FALSE)
            $this->db->where('safa_itos.disabled', $this->disabled);

        if ($this->reference_code !== FALSE)
            $this->db->where('safa_itos.reference_code', $this->reference_code);



        $this->db->delete('safa_itos');
        return $this->db->affected_rows();
    }

    function search($rows_no = FALSE) {
        if ($this->custom_select !== FALSE) {
            $this->db->select('safa_ito_id');
            $this->db->select($this->custom_select);
        }

        if ($this->safa_ito_id !== FALSE)
            $this->db->where('safa_itos.safa_ito_id', $this->safa_ito_id);

        if ($this->name_ar !== FALSE)
            $this->db->where('safa_itos.name_ar', $this->name_ar);

        if ($this->name_la !== FALSE)
            $this->db->where('safa_itos.name_la', $this->name_la);

        if ($this->username !== FALSE)
            $this->db->where('safa_itos.username', $this->username);

        if ($this->password !== FALSE)
            $this->db->where('safa_itos.password', $this->password);

        if ($this->erp_country_id !== FALSE)
            $this->db->where('safa_itos.erp_country_id', $this->erp_country_id);

        if ($this->disabled !== FALSE)
            $this->db->where('safa_itos.disabled', $this->disabled);

        if ($this->reference_code !== FALSE)
            $this->db->where('safa_itos.reference_code', $this->reference_code);


        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('safa_itos');
        if ($rows_no)
            return $query->num_rows();

        return $query->result();
    }

    function check_login($username, $password) {
        $this->db->where('username', $username);
        $this->db->where('password', $password);
        return $this->db->get('safa_itos')->row();
    }

    function update_user_info($user_id) {

        if (session('language') == 'arabic') {
            $data = array(
                'username' => session('username'),
                'password' => session('password'),
                'erp_language_id' => 2
            );

            $this->db->where('safa_ito_id', $user_id);
            $this->db->update('safa_itos', $data);
        } else {
            $data = array(
                'username' => session('username'),
                'password' => session('password'),
                'erp_language_id' => 1
            );

            $this->db->where('safa_ito_id', $user_id);
            $this->db->update('safa_itos', $data);
        }
    }

    function getlanguage($user_id) {
        $this->db->select('erp_languages.name');
        $this->db->from('safa_itos');
        $this->db->join('erp_languages', 'safa_itos.erp_language_id = erp_languages.erp_language_id');
        $this->db->where('safa_itos.safa_ito_id', $user_id);


        $query = $this->db->get();

        if ($this->safa_ito_id)
            return $query->row();
        else
            return $query->result();
    }

    function check_delete_ability($id) {

        $this->db->select('count(safa_ea_itos.safa_ito_id)as ea_ito,count(safa_trip_internaltrips.safa_ito_id) as trip_internaltrips,
                   count(safa_ito_users.safa_ito_id)as ito_users,count(safa_uo_contracts_itos.safa_ito_id)as contracts ');
        $this->db->from('safa_itos');
        $this->db->join('safa_ea_itos', 'safa_itos.safa_ito_id = safa_ea_itos.safa_ito_id', 'left');
        $this->db->join('safa_trip_internaltrips', 'safa_itos.safa_ito_id = safa_trip_internaltrips.safa_ito_id', 'left');
        $this->db->join('safa_ito_users', 'safa_itos.safa_ito_id = safa_ito_users.safa_ito_id', 'left');
        $this->db->join('safa_uo_contracts_itos', 'safa_itos.safa_ito_id = safa_uo_contracts_itos.safa_ito_id', 'left');
        $this->db->group_by('safa_itos.safa_ito_id');
        $this->db->where('safa_itos.safa_ito_id', $id);
        $query = $this->db->get();
        $flag = 0;

//        print_r($query->result());
        foreach ($query->result() as $row => $table) {
            foreach ($table as $col => $value) {
                if ($value > 0) {
                    $flag = 1;
                    break;
                } else {
                    continue;
                }
            }
        }
        return $flag;
    }

    function check_users_delete_ability($id) {
        $this->db->select('safa_ito_users.safa_ito_user_id,safa_ito_users.safa_ito_id,safa_ito_users.safa_ito_usergroup_id ');
        $this->db->from('safa_ito_users');
        $this->db->where('safa_ito_users.safa_ito_id', $id);
        $this->db->where('safa_ito_users.safa_ito_usergroup_id', 1);


        $query = $this->db->get();

        if ($this->safa_ito_id)
            return $query->row();
        else
            return $query->result();
    }

    //By Gouda
	function add_contracts() {
        $native_query = "insert into safa_uo_contracts_itos(safa_uo_contract_id, safa_ito_id)
            select safa_uo_contracts.safa_uo_contract_id," . $this->db->escape($this->safa_ito_id) . "
               from safa_uo_contracts";
        $this->db->query($native_query);
        return $this->db->affected_rows();
    }
    
}

/* End of file itos_model.php */
/* Location: ./application/models/itos_model.php */