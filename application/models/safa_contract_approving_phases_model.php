<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Safa_contract_approving_phases_model extends CI_Model {

    public $tbl_name = "safa_contract_approving_phases";
    public $safa_contract_approving_phases_id = FALSE;
    public $safa_uo_contracts_id = FALSE;
    public $safa_contract_phases_id = FALSE;
    public $safa_contract_phases_documents_id = FALSE;
    public $safa_contract_approving_phases_status_id = FALSE;
    public $document_path = FALSE;
    public $refuse_reason = FALSE;
    public $custom_select = FALSE;
    public $limit = FALSE;
    public $offset = FALSE;
    public $order_by = FALSE;
    public $group_by = FALSE;

    function __construct() {
        parent::__construct();
    }

    function save() {
        if ($this->safa_contract_approving_phases_id !== FALSE) {
            $this->db->set('safa_contract_approving_phases.safa_contract_approving_phases_id', $this->safa_contract_approving_phases_id);
        }
        if ($this->safa_uo_contracts_id !== FALSE) {
            $this->db->set('safa_contract_approving_phases.safa_uo_contracts_id', $this->safa_uo_contracts_id);
        }
        if ($this->safa_contract_phases_id !== FALSE) {
            $this->db->set('safa_contract_approving_phases.safa_contract_phases_id', $this->safa_contract_phases_id);
        }
        if ($this->safa_contract_phases_documents_id !== FALSE) {
            $this->db->set('safa_contract_approving_phases.safa_contract_phases_documents_id', $this->safa_contract_phases_documents_id);
        }

        if ($this->safa_contract_approving_phases_status_id !== FALSE) {
            $this->db->set('safa_contract_approving_phases.safa_contract_approving_phases_status_id', $this->safa_contract_approving_phases_status_id);
        }
        if ($this->document_path !== FALSE) {
            $this->db->set('safa_contract_approving_phases.document_path', $this->document_path);
        }
        if ($this->refuse_reason !== FALSE) {
            $this->db->set('safa_contract_approving_phases.refuse_reason', $this->refuse_reason);
        }

        if ($this->safa_contract_approving_phases_id) {
            $this->db->where('safa_contract_approving_phases.safa_contract_approving_phases_id', $this->safa_contract_approving_phases_id)->update('safa_contract_approving_phases');
        } else {
            $this->db->insert('safa_contract_approving_phases');
            return $this->db->insert_id();
        }
    }

    function get($rows_no = FALSE) {

        if ($this->custom_select !== FALSE) {
            $this->db->select('safa_contract_approving_phases_id');
            $this->db->select($this->custom_select);
        }

        $this->db->from('safa_contract_approving_phases');
        $this->db->join('safa_contract_phases_documents', 'safa_contract_approving_phases.safa_contract_phases_documents_id=safa_contract_phases_documents.safa_contract_phases_documents_id', 'left');

        if ($this->safa_contract_approving_phases_id !== FALSE) {
            $this->db->where('safa_contract_approving_phases.safa_contract_approving_phases_id', $this->safa_contract_approving_phases_id);
        }
        if ($this->safa_uo_contracts_id !== FALSE) {
            $this->db->where('safa_contract_approving_phases.safa_uo_contracts_id', $this->safa_uo_contracts_id);
        }
        if ($this->safa_contract_phases_id !== FALSE) {
            $this->db->where('safa_contract_approving_phases.safa_contract_phases_id', $this->safa_contract_phases_id);
        }
        if ($this->safa_contract_phases_documents_id !== FALSE) {
            $this->db->where('safa_contract_approving_phases.safa_contract_phases_documents_id', $this->safa_contract_phases_documents_id);
        }

        if ($this->group_by !== FALSE) {
            $this->db->group_by($this->group_by);
        }

        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get();
        $query_text = $this->db->last_query();

        if ($rows_no) {
            return $query->num_rows();
        }
        if ($this->safa_contract_approving_phases_id) {
            return $query->row();
        } else {
            return $query->result();
        }
    }

    function delete() {
        if ($this->safa_contract_approving_phases_id !== FALSE) {
            $this->db->where('safa_contract_approving_phases.safa_contract_approving_phases_id', $this->safa_contract_approving_phases_id);
        }
        if ($this->safa_uo_contracts_id !== FALSE) {
            $this->db->where('safa_contract_approving_phases.safa_uo_contracts_id', $this->safa_uo_contracts_id);
        }
        if ($this->safa_contract_phases_id !== FALSE) {
            $this->db->where('safa_contract_approving_phases.safa_contract_phases_id', $this->safa_contract_phases_id);
        }
        if ($this->safa_contract_phases_documents_id !== FALSE) {
            $this->db->where('safa_contract_approving_phases.safa_contract_phases_documents_id', $this->safa_contract_phases_documents_id);
        }

        $this->db->delete('safa_contract_approving_phases');
        return $this->db->affected_rows();
    }

    function get_contract_phases($contract_id = false) {
        $this->db->select("safa_contract_approving_phases.safa_contract_phases_id,
            safa_contract_phases." . name() . " as phase_name,
            safa_contract_approving_phases_status." . name() . " as status_name");
        $this->db->where('safa_uo_contracts_id', $contract_id);
        $this->db->group_by('safa_contract_phases_id');
        $this->db->join('safa_contract_phases', 'safa_contract_phases.safa_contract_phases_id = safa_contract_approving_phases.safa_contract_phases_id');
        $this->db->join('safa_contract_approving_phases_status', 'safa_contract_approving_phases_status.safa_contract_approving_phases_status_id = safa_contract_approving_phases.safa_contract_approving_phases_status_id');
        $phases_result = $this->db->get($this->tbl_name)->result();
        return $phases_result;
    }

    function get_contract_phase_file($contract_id = false, $phase_id = false) {
        $this->db->select('safa_contract_approving_phases.document_path,
            safa_contract_phases_documents.' . name());
        $this->db->where('safa_contract_approving_phases.safa_uo_contracts_id', $contract_id);
        $this->db->where('safa_contract_approving_phases.safa_contract_phases_id', $phase_id);
        $this->db->join('safa_contract_phases_documents', 'safa_contract_phases_documents.safa_contract_phases_documents_id = safa_contract_approving_phases.safa_contract_phases_documents_id');
        $phases_result = $this->db->get($this->tbl_name)->result();
        return $phases_result;
    }

}

/* End of file safa_contract_approving_phases_model.php */
/* Location: ./application/models/safa_contract_approving_phases_model.php */