<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Hotels_meals_model extends CI_Model {

    public $erp_meal_id = FALSE;
    public $name_ar = FALSE;
    public $name_la = FALSE;
    public $description_ar = FALSE;
    public $description_la = FALSE;
    public $code = FALSE;
    public $custom_select = FALSE;
    public $limit = FALSE;
    public $offset = FALSE;
    public $order_by = FALSE;

    function __construct() {
        parent::__construct();
    }

    function get($rows_no = FALSE) {

        if ($this->custom_select !== FALSE) {
            $this->db->select('erp_meal_id');
            $this->db->select($this->custom_select);
        }

        if ($this->erp_meal_id !== FALSE)
            $this->db->where('erp_meals.erp_meal_id', $this->erp_meal_id);

        if ($this->name_ar !== FALSE)
            $this->db->where('erp_meals.name_ar', $this->name_ar);

        if ($this->name_la !== FALSE)
            $this->db->where('erp_meals.name_la', $this->name_la);

        if ($this->description_ar)
            $this->db->where('erp_meals.description_ar', $this->description_ar);

        if ($this->description_la)
            $this->db->where('erp_meals.description_la', $this->description_la);

        if ($this->code)
            $this->db->where('erp_meals.code', $this->code);



        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('erp_meals');
        if ($rows_no)
            return $query->num_rows();

        if ($this->erp_meal_id)
            return $query->row();
        else
            return $query->result();
    }

    function save() {

        if ($this->erp_meal_id !== FALSE)
            $this->db->set('erp_meals.erp_meal_id', $this->erp_meal_id);

        if ($this->name_ar !== FALSE)
            $this->db->set('erp_meals.name_ar', $this->name_ar);

        if ($this->name_la !== FALSE)
            $this->db->set('erp_meals.name_la', $this->name_la);

        if ($this->description_ar !== FALSE)
            $this->db->set('erp_meals.description_ar', $this->description_ar);

        if ($this->description_la !== FALSE)
            $this->db->set('erp_meals.description_la', $this->description_la);

        if ($this->code !== FALSE)
            $this->db->set('erp_meals.code', $this->code);


        if ($this->erp_meal_id) {
            $this->db->where('erp_meals.erp_meal_id ', $this->erp_meal_id)->update('erp_meals');
        } else {
            $this->db->insert('erp_meals');
            return $this->db->insert_id();
        }
    }

    function delete() {

        if ($this->erp_meal_id !== FALSE)
            $this->db->where('erp_meals.erp_meal_id', $this->erp_meal_id);

        if ($this->name_ar !== FALSE)
            $this->db->where('erp_meals.name_ar', $this->name_ar);

        if ($this->name_la !== FALSE)
            $this->db->where('erp_meals.name_la', $this->name_la);

        if ($this->description_ar !== FALSE)
            $this->db->where('description_ar', $this->description_ar);

        if ($this->description_la !== FALSE)
            $this->db->where('description_la', $this->description_la);

        if ($this->code !== FALSE)
            $this->db->where('code', $this->code);


        $this->db->delete('erp_meals');
        return $this->db->affected_rows();
    }

    function search($rows_no = FALSE) {
        if ($this->custom_select !== FALSE) {
            $this->db->select('erp_meal_id');
            $this->db->select($this->custom_select);
        }

        if ($this->erp_meal_id !== FALSE)
            $this->db->where('erp_meals.erp_meal_id', $this->erp_meal_id);

        if ($this->name_ar !== FALSE)
            $this->db->where('erp_meals.name_ar', $this->name_ar);

        if ($this->name_la !== FALSE)
            $this->db->where('erp_meals.name_la', $this->name_la);

        if ($this->description_ar !== FALSE)
            $this->db->where('erp_meals.description_ar', $this->description_ar);


        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('erp_meals');
        if ($rows_no)
            return $query->num_rows();

        return $query->result();
    }

}

/* End of file hotels_meals_model.php */
/* Location: ./application/models/hotels_levels_model.php */