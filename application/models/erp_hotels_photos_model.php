<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Erp_hotels_photos_model extends CI_Model {

    public $erp_hotels_photos_id = FALSE;
    public $hotel_photospath = FALSE;
    public $erp_hotel_id = FALSE;
    public $custom_select = FALSE;
    public $limit = FALSE;
    public $offset = FALSE;
    public $order_by = FALSE;

    function __construct() {
        parent::__construct();
    }

    function get($rows_no = FALSE) {

        if ($this->custom_select !== FALSE) {
            $this->db->select('erp_hotels_photos_id');
            $this->db->select($this->custom_select);
        }

        if ($this->erp_hotels_photos_id !== FALSE)
            $this->db->where('erp_hotels_photos.erp_hotels_photos_id', $this->erp_hotels_photos_id);

        if ($this->hotel_photospath !== FALSE)
            $this->db->where('erp_hotels_photos.hotel_photospath', $this->hotel_photospath);

        if ($this->erp_hotel_id !== FALSE)
            $this->db->where('erp_hotels_photos.erp_hotel_id', $this->erp_hotel_id);


        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('erp_hotels_photos');
        if ($rows_no)
            return $query->num_rows();

        if ($this->erp_hotels_photos_id)
            return $query->row();
        else
            return $query->result();
    }

    function save() {
        if ($this->erp_hotels_photos_id !== FALSE)
            $this->db->set('erp_hotels_photos.erp_hotels_photos_id', $this->erp_hotels_photos_id);

        if ($this->hotel_photospath !== FALSE)
            $this->db->set('erp_hotels_photos.hotel_photospath', $this->hotel_photospath);

        if ($this->erp_hotel_id !== FALSE)
            $this->db->set('erp_hotels_photos.erp_hotel_id', $this->erp_hotel_id);



        if ($this->erp_hotels_photos_id) {
            $this->db->where('erp_hotels_photos.erp_hotels_photos_id', $this->erp_hotels_photos_id)->update('erp_hotels_photos');
        } else {
            $this->db->insert('erp_hotels_photos');
            return $this->db->insert_id();
        }
    }

    function delete() {
        if ($this->erp_hotels_photos_id !== FALSE)
            $this->db->where('erp_hotels_photos.erp_hotels_photos_id', $this->erp_hotels_photos_id);

        if ($this->hotel_photospath !== FALSE)
            $this->db->where('erp_hotels_photos.hotel_photospath', $this->hotel_photospath);

        if ($this->erp_hotel_id !== FALSE)
            $this->db->where('erp_hotels_photos.erp_hotel_id', $this->erp_hotel_id);



        $this->db->delete('erp_hotels_photos');
        return $this->db->affected_rows();
    }

    function search($rows_no = FALSE) {
        if ($this->custom_select !== FALSE) {
            $this->db->select('erp_hotels_photos_id');
            $this->db->select($this->custom_select);
        }

        if ($this->erp_hotels_photos_id !== FALSE)
            $this->db->where('erp_hotels_photos.erp_hotels_photos_id', $this->erp_hotels_photos_id);

        if ($this->hotel_photospath !== FALSE)
            $this->db->where('erp_hotels_photos.hotel_photospath', $this->hotel_photospath);

        if ($this->erp_hotel_id !== FALSE)
            $this->db->where('erp_hotels_photos.erp_hotel_id', $this->erp_hotel_id);


        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('erp_hotels_photos');
        if ($rows_no)
            return $query->num_rows();

        return $query->result();
    }

}

/* End of file Erp_hotels_photos_model.php */
/* Location: ./application/models/Erp_hotels_photos_model.php */