<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Erp_hotel_advantages_details_model extends CI_Model {

    public $erp_hotel_advantages_details_id = FALSE;
    public $erp_hotel_id = FALSE;
    public $erp_hotel_advantages_id = FALSE;
    public $erp_hotel_advantages_details = FALSE;
    public $custom_select = FALSE;
    public $limit = FALSE;
    public $offset = FALSE;
    public $order_by = FALSE;
    public $join = FALSE;

    function __construct() {
        parent::__construct();
    }

    function get($rows_no = FALSE) {

        if ($this->custom_select !== FALSE) {
            $this->db->select('erp_hotel_id');
            $this->db->select($this->custom_select);
        }

        if ($this->erp_hotel_id !== FALSE)
            $this->db->where('erp_hotel_advantages_details.erp_hotel_id', $this->erp_hotel_id);



        if ($this->erp_hotel_advantages_details_id !== FALSE)
            $this->db->where('erp_hotel_advantages_details.erp_hotel_advantages_details_id', $this->erp_hotel_advantages_details_id);

        if ($this->erp_hotel_advantages_id !== FALSE)
            $this->db->where('erp_hotel_advantages_details.erp_hotel_advantages_id', $this->erp_hotel_advantages_id);

        if ($this->erp_hotel_advantages_details !== FALSE)
            $this->db->where('erp_hotel_advantages_details.erp_hotel_advantages_details', $this->erp_hotel_advantages_details);


        if ($this->join !== FALSE) {
            $this->db->select('erp_hotel_advantages.' . name());
            $this->db->join('erp_hotel_advantages', 'erp_hotel_advantages.erp_hotel_advantages_id = erp_hotel_advantages_details.erp_hotel_advantages_id');
        }

        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('erp_hotel_advantages_details');
        if ($rows_no)
            return $query->num_rows();

        if ($this->erp_hotel_advantages_details_id)
            return $query->row();
        else
            return $query->result();
    }

    function save() {
        if ($this->erp_hotel_id !== FALSE)
            $this->db->set('erp_hotel_advantages_details.erp_hotel_id', $this->erp_hotel_id);


        if ($this->erp_hotel_advantages_details_id !== FALSE)
            $this->db->set('erp_hotel_advantages_details.erp_hotel_advantages_details_id', $this->erp_hotel_advantages_details_id);


        if ($this->erp_hotel_advantages_id !== FALSE)
            $this->db->set('erp_hotel_advantages_details.erp_hotel_advantages_id', $this->erp_hotel_advantages_id);

        if ($this->erp_hotel_advantages_details !== FALSE)
            $this->db->set('erp_hotel_advantages_details.erp_hotel_advantages_details', $this->erp_hotel_advantages_details);

        if ($this->erp_hotel_advantages_details_id) {
            $this->db->where('erp_hotel_advantages_details.erp_hotel_advantages_id', $this->erp_hotel_advantages_id)->update('erp_hotel_advantages_details');
        } else {
            $this->db->insert('erp_hotel_advantages_details');
            return $this->db->insert_id();
        }
    }

    function delete() {
        if ($this->erp_hotel_id !== FALSE)
            $this->db->where('erp_hotel_advantages_details.erp_hotel_id', $this->erp_hotel_id);


        if ($this->erp_hotel_advantages_details_id !== FALSE)
            $this->db->where('erp_hotel_advantages_details.erp_hotel_advantages_details_id', $this->erp_hotel_advantages_details_id);


        if ($this->erp_hotel_advantages_id !== FALSE)
            $this->db->where('erp_hotel_advantages_details.erp_hotel_advantages_id', $this->erp_hotel_advantages_id);

        if ($this->erp_hotel_advantages_details !== FALSE)
            $this->db->where('erp_hotel_advantages_details.erp_hotel_advantages_details', $this->erp_hotel_advantages_details);



        $this->db->delete('erp_hotel_advantages_details');
        return $this->db->affected_rows();
    }

    function search($rows_no = FALSE) {
        if ($this->custom_select !== FALSE) {
            $this->db->select('erp_hotel_id');
            $this->db->select($this->custom_select);
        }

        if ($this->erp_hotel_id !== FALSE)
            $this->db->where('erp_hotel_advantages_details.erp_hotel_id', $this->erp_hotel_id);

        if ($this->erp_hotel_advantages_id !== FALSE)
            $this->db->where('erp_hotel_advantages_details.erp_hotel_advantages_id', $this->erp_hotel_advantages_id);

        if ($this->erp_hotel_advantages_details_id !== FALSE)
            $this->db->where('erp_hotel_advantages_details.erp_hotel_advantages_details_id', $this->erp_hotel_advantages_details_id);



        if ($this->erp_hotel_advantages_details !== FALSE)
            $this->db->where('erp_hotel_advantages_details.erp_hotel_advantages_details', $this->erp_hotel_advantages_details);

        if ($this->erp_hotel_advantages_details_id !== FALSE)
            $this->db->where('erp_hotel_advantages_details.erp_hotel_advantages_details_id', $this->erp_hotel_advantages_details_id);



        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('erp_hotel_advantages_details');
        if ($rows_no)
            return $query->num_rows();

        return $query->result();
    }

}

/* End of file Erp_hotel_advantages_details_model.php */
/* Location: ./application/models/Erp_hotel_advantages_details_model.php */