<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Erp_admins_model extends CI_Model {

    public $erp_admin_id = FALSE;
    public $username = FALSE;
    public $password = FALSE;
    public $email = FALSE;
    public $custom_select = FALSE;
    public $limit = FALSE;
    public $offset = FALSE;
    public $order_by = FALSE;

    function __construct() {
        parent::__construct();
    }

    function get($rows_no = FALSE) {

        if ($this->custom_select !== FALSE) {
            $this->db->select('erp_admin_id');
            $this->db->select($this->custom_select);
        }

        if ($this->erp_admin_id !== FALSE)
            $this->db->where('erp_admins.erp_admin_id', $this->erp_admin_id);

        if ($this->username !== FALSE)
            $this->db->where('erp_admins.username', $this->username);

        if ($this->password !== FALSE)
            $this->db->where('erp_admins.password', $this->password);

        if ($this->email !== FALSE)
            $this->db->where('erp_admins.email', $this->email);

        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('erp_admins');
        if ($rows_no)
            return $query->num_rows();

        if ($this->id)
            return $query->row();
        else
            return $query->result();
    }

    function save() {
        if ($this->erp_admin_id !== FALSE)
            $this->db->set('erp_admins.erp_admin_id', $this->erp_admin_id);

        if ($this->username !== FALSE)
            $this->db->set('erp_admins.username', $this->username);

        if ($this->password !== FALSE)
            $this->db->set('erp_admins.password', $this->password);

        if ($this->email !== FALSE)
            $this->db->set('erp_admins.email', $this->email);

        if ($this->erp_admin_id) {
            $this->db->where('erp_admins.erp_admin_id', $this->erp_admin_id)->update('erp_admins');
        } else {
            $this->db->insert('erp_admins');
            return $this->db->insert_id();
        }
    }

    function delete() {
        if ($this->erp_admin_id !== FALSE)
            $this->db->where('erp_admins.erp_admin_id', $this->erp_admin_id);

        if ($this->username !== FALSE)
            $this->db->where('erp_admins.username', $this->username);

        if ($this->password !== FALSE)
            $this->db->where('erp_admins.password', $this->password);

        if ($this->email !== FALSE)
            $this->db->where('erp_admins.email', $this->email);

        $this->db->delete('erp_admins');
        return $this->db->affected_rows();
    }

    function search($rows_no = FALSE) {
        if ($this->custom_select !== FALSE) {
            $this->db->select('erp_admin_id');
            $this->db->select($this->custom_select);
        }

        if ($this->erp_admin_id !== FALSE)
            $this->db->where('erp_admins.erp_admin_id', $this->erp_admin_id);

        if ($this->username !== FALSE)
            $this->db->where('erp_admins.username', $this->username);

        if ($this->password !== FALSE)
            $this->db->where('erp_admins.password', $this->password);

        if ($this->email !== FALSE)
            $this->db->where('erp_admins.email', $this->email);

        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('erp_admins');
        if ($rows_no)
            return $query->num_rows();

        return $query->result();
    }

}

/* End of file erp_admins_model.php */
/* Location: ./application/models/erp_admins_model.php */