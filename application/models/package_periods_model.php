<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Package_periods_model extends CI_Model {

    public $tbl_name = "erp_package_periods";
    public $erp_package_period_id = FALSE;
    public $erp_company_type_id = FALSE;
    public $erp_company_id = FALSE;
    public $name_ar = FALSE;
    public $name_la = FALSE;
    public $makkah_days = FALSE;
    public $madinah_days = FALSE;
    public $custom_select = FALSE;
    public $limit = FALSE;
    public $offset = FALSE;
    public $order_by = FALSE;

    function __construct() {
        parent::__construct();
    }

    function save() {
        if ($this->erp_package_period_id !== FALSE) {
            $this->db->set('erp_package_periods.erp_package_period_id', $this->erp_package_period_id);
        }
        if ($this->erp_company_type_id !== FALSE) {
            $this->db->set('erp_package_periods.erp_company_type_id', $this->erp_company_type_id);
        }
        if ($this->erp_company_id !== FALSE) {
            $this->db->set('erp_package_periods.erp_company_id', $this->erp_company_id);
        }
        if ($this->name_ar !== FALSE) {
            $this->db->set('erp_package_periods.name_ar', $this->name_ar);
        }
        if ($this->name_la !== FALSE) {
            $this->db->set('erp_package_periods.name_la', $this->name_la);
        }


        if ($this->erp_package_period_id) {
            $this->db->where('erp_package_periods.erp_package_period_id', $this->erp_package_period_id)->update('erp_package_periods');
        } else {
            $this->db->insert('erp_package_periods');
            return $this->db->insert_id();
        }
    }

    function delete() {
        if ($this->erp_package_period_id !== FALSE) {
            $this->db->where('erp_package_periods.erp_package_period_id', $this->erp_package_period_id);
        }
        if ($this->erp_company_type_id !== FALSE) {
            $this->db->where('erp_package_periods.erp_company_type_id', $this->erp_company_type_id);
        }
        if ($this->erp_company_id !== FALSE) {
            $this->db->where('erp_package_periods.erp_company_id', $this->erp_company_id);
        }
        if ($this->name_ar !== FALSE) {
            $this->db->where('erp_package_periods.name_ar', $this->name_ar);
        }
        if ($this->name_la !== FALSE) {
            $this->db->where('erp_package_periods.name_la', $this->name_la);
        }


        $this->db->delete('erp_package_periods');
        return $this->db->affected_rows();
    }

    function get($rows_no = FALSE) {


        $this->db->select('erp_package_periods.*,'
                . '(select city_days from erp_package_periods_cities where erp_package_periods_cities.erp_package_period_id = erp_package_periods.erp_package_period_id && erp_city_id = 1) as makkah_days,'
                . '(select city_days from erp_package_periods_cities where erp_package_periods_cities.erp_package_period_id = erp_package_periods.erp_package_period_id && erp_city_id = 2) as madinah_days');
        //$this->db->select($this->custom_select);


        if ($this->erp_package_period_id !== FALSE) {
            $this->db->where('erp_package_periods.erp_package_period_id', $this->erp_package_period_id);
        }
        if ($this->erp_company_type_id !== FALSE) {
            $this->db->where('erp_package_periods.erp_company_type_id', $this->erp_company_type_id);
        }
        if ($this->erp_company_id !== FALSE) {
            $this->db->where('erp_package_periods.erp_company_id', $this->erp_company_id);
        }
        if ($this->name_ar !== FALSE) {
            $this->db->where('erp_package_periods.name_ar', $this->name_ar);
        }
        if ($this->name_la !== FALSE) {
            $this->db->where('erp_package_periods.name_la', $this->name_la);
        }


        if ($this->order_by && is_array($this->order_by)) {
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);
        } else {
            $this->db->order_by('erp_package_period_id');
        }

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('erp_package_periods');
        if ($rows_no)
            return $query->num_rows();

        if ($this->erp_package_period_id)
            return $query->row();
        else
            return $query->result();
    }

}

/* End of file erp_package_periods_model.php */
/* Location: ./application/models/erp_package_periods_model.php */