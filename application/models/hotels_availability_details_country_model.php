<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Hotels_availability_details_country_model extends CI_Model {

    public $erp_hotels_availability_detail_country_id = FALSE;
    public $erp_hotels_availability_detail_id = FALSE;
    public $erp_country_id = FALSE;
    public $status = FALSE;
    public $custom_select = FALSE;
    public $limit = FALSE;
    public $offset = FALSE;
    public $order_by = FALSE;

    function __construct() {
        parent::__construct();
    }

    function get($rows_no = FALSE) {

        if ($this->custom_select !== FALSE) {
            $this->db->select('erp_hotels_availability_detail_country_id');
            $this->db->select($this->custom_select);
        }

        if ($this->erp_hotels_availability_detail_country_id !== FALSE)
            $this->db->where('erp_hotels_availability_detail_countries.erp_hotels_availability_detail_country_id', $this->erp_hotels_availability_detail_country_id);

        if ($this->erp_hotels_availability_detail_id !== FALSE)
            $this->db->where('erp_hotels_availability_detail_countries.erp_hotels_availability_detail_id', $this->erp_hotels_availability_detail_id);

        if ($this->erp_country_id !== FALSE)
            $this->db->where('erp_hotels_availability_detail_countries.erp_country_id', $this->erp_country_id);

        if ($this->status !== FALSE)
            $this->db->where('erp_hotels_availability_detail_countries.status', $this->status);

        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('erp_hotels_availability_detail_countries');
        if ($rows_no)
            return $query->num_rows();

        if ($this->erp_hotels_availability_detail_country_id)
            return $query->row();
        else
            return $query->result();
    }

    function save() {

        if ($this->erp_hotels_availability_detail_id !== FALSE)
            $this->db->set('erp_hotels_availability_detail_countries.erp_hotels_availability_detail_id', $this->erp_hotels_availability_detail_id);

        if ($this->erp_country_id !== FALSE)
            $this->db->set('erp_hotels_availability_detail_countries.erp_country_id', $this->erp_country_id);

        if ($this->status !== FALSE)
            $this->db->set('erp_hotels_availability_detail_countries.status', $this->status);


        $this->db->insert('erp_hotels_availability_detail_countries');
        return $this->db->insert_id();
    }

    function delete() {
        if ($this->erp_hotels_availability_detail_country_id !== FALSE)
            $this->db->where('erp_hotels_availability_detail_countries.erp_hotels_availability_detail_country_id', $this->erp_hotels_availability_detail_country_id);

        if ($this->erp_hotels_availability_detail_id !== FALSE)
            $this->db->where('erp_hotels_availability_detail_countries.erp_hotels_availability_detail_id', $this->erp_hotels_availability_detail_id);

        if ($this->erp_country_id !== FALSE)
            $this->db->where('erp_hotels_availability_detail_countries.erp_country_id', $this->erp_country_id);

        if ($this->status !== FALSE)
            $this->db->where('erp_hotels_availability_detail_countries.status', $this->status);



        $this->db->delete('erp_hotels_availability_detail_countries');
        return $this->db->affected_rows();
    }

}

/* End of file hotels_availability_details_model.php */
/* Location: ./application/models/hotels_availability_details_model.php */