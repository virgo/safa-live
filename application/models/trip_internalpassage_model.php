<?php

class Trip_internalpassage_model extends CI_Model {

    /**
     * store this safa_trip_internalpassages table name.
     *
     * @var string
     * @access public
     */
    public $table = 'safa_trip_internalpassages';

    /**
     * Constructor
     *
     * @access public
     */
    function Main_model() {
        parent::__construct();
    }

    function get_trip_internalpassages_by_trip_id($trip_id) {
        $this->db->select('*');
        $this->db->from('safa_trip_internalpassages');
        $this->db->where("safa_trip_id", $trip_id);
        $query = $this->db->get();
        return $query->result();
    }

    function delete_trip_internalpassage_by_trip_id($trip_id) {
        $this->db->where("safa_trip_id", $trip_id);
        return $this->db->delete("safa_trip_internalpassages");
    }

    function insert($data) {
        $this->db->insert('safa_trip_internalpassages', $data);
    }

    function update($trip_internalpassage_id, $data) {
        $this->db->where('safa_internalpassage_id', $trip_internalpassage_id);
        $this->db->update('safa_trip_internalpassages', $data);
    }

    function delete_by_trip_and_id($trip_id, $trip_internalpassage_id) {
        $this->db->where("safa_trip_id", $trip_id);
        $this->db->where("safa_internalpassage_id", $trip_internalpassage_id);
        return $this->db->delete("safa_trip_internalpassages");
    }

}

?>