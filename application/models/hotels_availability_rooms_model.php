<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Hotels_availability_rooms_model extends CI_Model {

    public $erp_hotels_availability_rooms_id = FALSE;
    public $erp_floor_id = FALSE;
    public $erp_hotelroomsize_id = FALSE;
    public $rooms_beds_count = FALSE;
    public $max_beds_count = FALSE;
    public $rooms_no = FALSE;
    public $closed_rooms_beds_count = FALSE;
    public $erp_hotels_availability_view_id = FALSE;
    public $can_spiltting = FALSE;
    public $purchase_price = FALSE;
    public $offer_status = FALSE;
    public $erp_hotels_availability_master_id = FALSE;
    public $erp_housingtype_id = FALSE;
    public $custom_select = FALSE;
    public $limit = FALSE;
    public $offset = FALSE;
    public $order_by = FALSE;

    function __construct() {
        parent::__construct();
    }

    function get($rows_no = FALSE) {

        if ($this->custom_select !== FALSE) {
            $this->db->select('erp_hotels_availability_rooms_id');
            $this->db->select($this->custom_select);
        }

        if ($this->erp_hotels_availability_rooms_id !== FALSE)
            $this->db->where('erp_hotels_availability_rooms.erp_hotels_availability_rooms_id', $this->erp_hotels_availability_rooms_id);

        if ($this->erp_housingtype_id !== FALSE)
            $this->db->where('erp_hotels_availability_rooms.erp_housingtype_id', $this->erp_housingtype_id);

        if ($this->erp_floor_id !== FALSE)
            $this->db->where('erp_hotels_availability_rooms.erp_floor_id', $this->erp_floor_id);

        if ($this->erp_hotelroomsize_id !== FALSE)
            $this->db->where('erp_hotels_availability_rooms.erp_hotelroomsize_id', $this->erp_hotelroomsize_id);

        if ($this->rooms_beds_count !== FALSE)
            $this->db->where('erp_hotels_availability_rooms.rooms_beds_count', $this->rooms_beds_count);

        if ($this->max_beds_count !== FALSE)
            $this->db->where('erp_hotels_availability_rooms.max_beds_count', $this->max_beds_count);

        if ($this->rooms_no !== FALSE)
            $this->db->where('erp_hotels_availability_rooms.rooms_no', $this->rooms_no);

        if ($this->closed_rooms_beds_count !== FALSE)
            $this->db->where('erp_hotels_availability_rooms.closed_rooms_beds_count', $this->closed_rooms_beds_count);

        if ($this->erp_hotels_availability_view_id !== FALSE)
            $this->db->where('erp_hotels_availability_rooms.erp_hotels_availability_view_id', $this->erp_hotels_availability_view_id);

        if ($this->can_spiltting !== FALSE)
            $this->db->where('erp_hotels_availability_rooms.can_spiltting', $this->can_spiltting);

        if ($this->purchase_price !== FALSE)
            $this->db->where('erp_hotels_availability_rooms.purchase_price', $this->purchase_price);

        if ($this->offer_status !== FALSE)
            $this->db->where('erp_hotels_availability_rooms.offer_status', $this->offer_status);

        if ($this->erp_hotels_availability_master_id !== FALSE)
            $this->db->where('erp_hotels_availability_rooms.erp_hotels_availability_master_id', $this->erp_hotels_availability_master_id);


        $this->db->select("erp_hotels_availability_rooms.*, erp_hotelroomsizes." . name() . " as hotelroomsize_name, erp_housingtypes." . name() . " as erp_housingtypes_name ", false);
        $this->db->from('erp_hotels_availability_rooms');
        $this->db->join('erp_hotelroomsizes', 'erp_hotelroomsizes.erp_hotelroomsize_id=erp_hotels_availability_rooms.erp_hotelroomsize_id', 'left');

        $this->db->join('erp_housingtypes', 'erp_housingtypes.erp_housingtype_id=erp_hotels_availability_rooms.erp_housingtype_id', 'left');

        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get();
        if ($rows_no)
            return $query->num_rows();

        if ($this->erp_hotels_availability_rooms_id)
            return $query->row();
        else
            return $query->result();
    }

    function get_sub_rooms($id) {
        $this->db->where('erp_hotels_availability_rooms_id', $id);
        $query = $this->db->get('erp_hotels_availability_sub_rooms');
        return $query->result();
    }

    function save() {
        if ($this->erp_hotels_availability_rooms_id !== FALSE)
            $this->db->set('erp_hotels_availability_rooms.erp_hotels_availability_rooms_id', $this->erp_hotels_availability_rooms_id);

        if ($this->erp_housingtype_id !== FALSE)
            $this->db->set('erp_hotels_availability_rooms.erp_housingtype_id', $this->erp_housingtype_id);

        if ($this->erp_floor_id !== FALSE)
            $this->db->set('erp_hotels_availability_rooms.erp_floor_id', $this->erp_floor_id);

        if ($this->erp_hotelroomsize_id !== FALSE)
            $this->db->set('erp_hotels_availability_rooms.erp_hotelroomsize_id', $this->erp_hotelroomsize_id);

        if ($this->rooms_beds_count !== FALSE)
            $this->db->set('erp_hotels_availability_rooms.rooms_beds_count', $this->rooms_beds_count);

        if ($this->max_beds_count !== FALSE)
            $this->db->set('erp_hotels_availability_rooms.max_beds_count', $this->max_beds_count);

        if ($this->rooms_no !== FALSE)
            $this->db->set('erp_hotels_availability_rooms.rooms_no', $this->rooms_no);

        if ($this->closed_rooms_beds_count !== FALSE)
            $this->db->set('erp_hotels_availability_rooms.closed_rooms_beds_count', $this->closed_rooms_beds_count);

        if ($this->erp_hotels_availability_view_id !== FALSE)
            $this->db->set('erp_hotels_availability_rooms.erp_hotels_availability_view_id', $this->erp_hotels_availability_view_id);

        if ($this->can_spiltting !== FALSE)
            $this->db->set('erp_hotels_availability_rooms.can_spiltting', $this->can_spiltting);

        if ($this->purchase_price !== FALSE)
            $this->db->set('erp_hotels_availability_rooms.purchase_price', $this->purchase_price);

        if ($this->offer_status !== FALSE)
            $this->db->set('erp_hotels_availability_rooms.offer_status', $this->offer_status);

        if ($this->erp_hotels_availability_master_id !== FALSE)
            $this->db->set('erp_hotels_availability_rooms.erp_hotels_availability_master_id', $this->erp_hotels_availability_master_id);

        if ($this->erp_hotels_availability_rooms_id) {
            $this->db->where('erp_hotels_availability_rooms.erp_hotels_availability_rooms_id', $this->erp_hotels_availability_rooms_id)->update('erp_hotels_availability_rooms');
        } else {
            $this->db->insert('erp_hotels_availability_rooms');
            return $this->db->insert_id();
        }
    }

    function delete() {
        if ($this->erp_hotels_availability_rooms_id !== FALSE)
            $this->db->where('erp_hotels_availability_rooms.erp_hotels_availability_rooms_id', $this->erp_hotels_availability_rooms_id);

        if ($this->erp_floor_id !== FALSE)
            $this->db->where('erp_hotels_availability_rooms.erp_floor_id', $this->erp_floor_id);

        if ($this->erp_hotelroomsize_id !== FALSE)
            $this->db->where('erp_hotels_availability_rooms.erp_hotelroomsize_id', $this->erp_hotelroomsize_id);

        if ($this->rooms_beds_count !== FALSE)
            $this->db->where('erp_hotels_availability_rooms.rooms_beds_count', $this->rooms_beds_count);

        if ($this->max_beds_count !== FALSE)
            $this->db->where('erp_hotels_availability_rooms.max_beds_count', $this->max_beds_count);

        if ($this->rooms_no !== FALSE)
            $this->db->where('erp_hotels_availability_rooms.rooms_no', $this->rooms_no);

        if ($this->closed_rooms_beds_count !== FALSE)
            $this->db->where('erp_hotels_availability_rooms.closed_rooms_beds_count', $this->closed_rooms_beds_count);

        if ($this->erp_hotels_availability_view_id !== FALSE)
            $this->db->where('erp_hotels_availability_rooms.erp_hotels_availability_view_id', $this->erp_hotels_availability_view_id);

        if ($this->can_spiltting !== FALSE)
            $this->db->where('erp_hotels_availability_rooms.can_spiltting', $this->can_spiltting);

        if ($this->purchase_price !== FALSE)
            $this->db->where('erp_hotels_availability_rooms.purchase_price', $this->purchase_price);

        if ($this->offer_status !== FALSE)
            $this->db->where('erp_hotels_availability_rooms.offer_status', $this->offer_status);

        if ($this->erp_hotels_availability_master_id !== FALSE)
            $this->db->where('erp_hotels_availability_rooms.erp_hotels_availability_master_id', $this->erp_hotels_availability_master_id);



        $this->db->delete('erp_hotels_availability_rooms');
        return $this->db->affected_rows();
    }

    function search($rows_no = FALSE) {
        if ($this->custom_select !== FALSE) {
            $this->db->select('erp_hotels_availability_rooms_id');
            $this->db->select($this->custom_select);
        }

        if ($this->erp_hotels_availability_rooms_id !== FALSE)
            $this->db->where('erp_hotels_availability_rooms.erp_hotels_availability_rooms_id', $this->erp_hotels_availability_rooms_id);

        if ($this->erp_floor_id !== FALSE)
            $this->db->where('erp_hotels_availability_rooms.erp_floor_id', $this->erp_floor_id);

        if ($this->erp_hotelroomsize_id !== FALSE)
            $this->db->where('erp_hotels_availability_rooms.erp_hotelroomsize_id', $this->erp_hotelroomsize_id);

        if ($this->rooms_beds_count !== FALSE)
            $this->db->where('erp_hotels_availability_rooms.rooms_beds_count', $this->rooms_beds_count);

        if ($this->max_beds_count !== FALSE)
            $this->db->where('erp_hotels_availability_rooms.max_beds_count', $this->max_beds_count);

        if ($this->rooms_no !== FALSE)
            $this->db->where('erp_hotels_availability_rooms.rooms_no', $this->rooms_no);

        if ($this->closed_rooms_beds_count !== FALSE)
            $this->db->where('erp_hotels_availability_rooms.closed_rooms_beds_count', $this->closed_rooms_beds_count);

        if ($this->erp_hotels_availability_view_id !== FALSE)
            $this->db->where('erp_hotels_availability_rooms.erp_hotels_availability_view_id', $this->erp_hotels_availability_view_id);

        if ($this->can_spiltting !== FALSE)
            $this->db->where('erp_hotels_availability_rooms.can_spiltting', $this->can_spiltting);

        if ($this->purchase_price !== FALSE)
            $this->db->where('erp_hotels_availability_rooms.purchase_price', $this->purchase_price);

        if ($this->offer_status !== FALSE)
            $this->db->where('erp_hotels_availability_rooms.offer_status', $this->offer_status);

        if ($this->erp_hotels_availability_master_id !== FALSE)
            $this->db->where('erp_hotels_availability_rooms.erp_hotels_availability_master_id', $this->erp_hotels_availability_master_id);


        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('erp_hotels_availability_rooms');
        if ($rows_no)
            return $query->num_rows();

        return $query->result();
    }

}

/* End of file hotels_availability_rooms_model.php */
/* Location: ./application/models/hotels_availability_rooms_model.php */