<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Trip_tourismplaces_model extends CI_Model {

    public $safa_trip_tourismplace_id = FALSE;
    public $safa_trip_id = FALSE;
    public $safa_tourismplace_id = FALSE;
    public $datetime = FALSE;
    public $custom_select = FALSE;
    public $limit = FALSE;
    public $offset = FALSE;
    public $order_by = FALSE;

    function __construct() {
        parent::__construct();
    }

    function get($rows_no = FALSE) {

        if ($this->custom_select !== FALSE) {
            $this->db->select('safa_trip_tourismplace_id');
            $this->db->select($this->custom_select);
        }

        if ($this->safa_trip_tourismplace_id !== FALSE)
            $this->db->where('safa_trip_tourismplaces.safa_trip_tourismplace_id', $this->safa_trip_tourismplace_id);

        if ($this->safa_trip_id !== FALSE)
            $this->db->where('safa_trip_tourismplaces.safa_trip_id', $this->safa_trip_id);

        if ($this->safa_tourismplace_id !== FALSE)
            $this->db->where('safa_trip_tourismplaces.safa_tourismplace_id', $this->safa_tourismplace_id);

        if ($this->datetime !== FALSE)
            $this->db->where('safa_trip_tourismplaces.datetime', $this->datetime);


        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('safa_trip_tourismplaces');
        if ($rows_no)
            return $query->num_rows();

        if ($this->safa_trip_tourismplace_id)
            return $query->row();
        else
            return $query->result();
    }

    function save() {
        if ($this->safa_trip_tourismplace_id !== FALSE)
            $this->db->set('safa_trip_tourismplaces.safa_trip_tourismplace_id', $this->safa_trip_tourismplace_id);

        if ($this->safa_trip_id !== FALSE)
            $this->db->set('safa_trip_tourismplaces.safa_trip_id', $this->safa_trip_id);

        if ($this->safa_tourismplace_id !== FALSE)
            $this->db->set('safa_trip_tourismplaces.safa_tourismplace_id', $this->safa_tourismplace_id);

        if ($this->datetime !== FALSE)
            $this->db->set('safa_trip_tourismplaces.datetime', $this->datetime);



        if ($this->safa_trip_tourismplace_id) {
            $this->db->where('safa_trip_tourismplaces.safa_trip_tourismplace_id', $this->safa_trip_tourismplace_id)->update('safa_trip_tourismplaces');
        } else {
            $this->db->insert('safa_trip_tourismplaces');
            return $this->db->insert_id();
        }
    }

    function delete() {
        if ($this->safa_trip_tourismplace_id !== FALSE)
            $this->db->where('safa_trip_tourismplaces.safa_trip_tourismplace_id', $this->safa_trip_tourismplace_id);

        if ($this->safa_trip_id !== FALSE)
            $this->db->where('safa_trip_tourismplaces.safa_trip_id', $this->safa_trip_id);

        if ($this->safa_tourismplace_id !== FALSE)
            $this->db->where('safa_trip_tourismplaces.safa_tourismplace_id', $this->safa_tourismplace_id);

        if ($this->datetime !== FALSE)
            $this->db->where('safa_trip_tourismplaces.datetime', $this->datetime);



        $this->db->delete('safa_trip_tourismplaces');
        return $this->db->affected_rows();
    }

    function search($rows_no = FALSE) {
        if ($this->custom_select !== FALSE) {
            $this->db->select('safa_trip_tourismplace_id');
            $this->db->select($this->custom_select);
        }

        if ($this->safa_trip_tourismplace_id !== FALSE)
            $this->db->where('safa_trip_tourismplaces.safa_trip_tourismplace_id', $this->safa_trip_tourismplace_id);

        if ($this->safa_trip_id !== FALSE)
            $this->db->where('safa_trip_tourismplaces.safa_trip_id', $this->safa_trip_id);

        if ($this->safa_tourismplace_id !== FALSE)
            $this->db->where('safa_trip_tourismplaces.safa_tourismplace_id', $this->safa_tourismplace_id);

        if ($this->datetime !== FALSE)
            $this->db->where('safa_trip_tourismplaces.datetime', $this->datetime);


        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('safa_trip_tourismplaces');
        if ($rows_no)
            return $query->num_rows();

        return $query->result();
    }

}

/* End of file trip_tourismplaces_model.php */
/* Location: ./application/models/trip_tourismplaces_model.php */