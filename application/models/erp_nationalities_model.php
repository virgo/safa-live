<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Erp_nationalities_model extends CI_Model {

    public $country_id = FALSE;
    public $erp_nationality_id = FALSE;
    public $name_ar = FALSE;
    public $name_la = FALSE;
    public $arabian = FALSE;
    public $custom_select = FALSE;
    public $limit = FALSE;
    public $offset = FALSE;
    public $order_by = FALSE;

    function __construct() {
        parent::__construct();
    }

    function get($rows_no = FALSE) {

        if ($this->custom_select !== FALSE) {
            $this->db->select('erp_nationality_id');
            $this->db->select($this->custom_select);
        }

        if ($this->country_id !== FALSE)
            $this->db->where('erp_nationalities.country_id', $this->country_id);

        if ($this->erp_nationality_id !== FALSE)
            $this->db->where('erp_nationalities.erp_nationality_id', $this->erp_nationality_id);

        if ($this->name_ar !== FALSE)
            $this->db->where('erp_nationalities.name_ar', $this->name_ar);

        if ($this->name_la !== FALSE)
            $this->db->where('erp_nationalities.name_la', $this->name_la);

        if ($this->arabian !== FALSE)
            $this->db->where('erp_nationalities.arabian', $this->arabian);


        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('erp_nationalities');
        if ($rows_no)
            return $query->num_rows();

        if ($this->country_id)
            return $query->row();
        else
            return $query->result();
    }

    function save() {
        if ($this->country_id !== FALSE)
            $this->db->set('erp_nationalities.country_id', $this->country_id);

        if ($this->erp_nationality_id !== FALSE)
            $this->db->set('erp_nationalities.erp_nationality_id', $this->erp_nationality_id);


        if ($this->name_ar !== FALSE)
            $this->db->set('erp_nationalities.name_ar', $this->name_ar);

        if ($this->name_la !== FALSE)
            $this->db->set('erp_nationalities.name_la', $this->name_la);

        if ($this->arabian !== FALSE)
            $this->db->set('erp_nationalities.arabian', $this->arabian);



        if ($this->country_id) {
            $this->db->where('erp_nationalities.country_id', $this->country_id)->update('erp_nationalities');
        } else {
            $this->db->insert('erp_nationalities');
            return $this->db->insert_id();
        }
    }

    function delete() {
        if ($this->country_id !== FALSE)
            $this->db->where('erp_nationalities.country_id', $this->country_id);

        if ($this->erp_nationality_id !== FALSE)
            $this->db->where('erp_nationalities.erp_nationality_id', $this->erp_nationality_id);


        if ($this->name_ar !== FALSE)
            $this->db->where('erp_nationalities.name_ar', $this->name_ar);

        if ($this->name_la !== FALSE)
            $this->db->where('erp_nationalities.name_la', $this->name_la);

        if ($this->arabian !== FALSE)
            $this->db->where('erp_nationalities.arabian', $this->arabian);



        $this->db->delete('erp_nationalities');
        return $this->db->affected_rows();
    }

    function search($rows_no = FALSE) {
        if ($this->custom_select !== FALSE) {
            $this->db->select('country_id');
            $this->db->select($this->custom_select);
        }

        if ($this->country_id !== FALSE)
            $this->db->where('erp_nationalities.country_id', $this->country_id);

        if ($this->erp_nationality_id !== FALSE)
            $this->db->where('erp_nationalities.erp_nationality_id', $this->erp_nationality_id);


        if ($this->name_ar !== FALSE)
            $this->db->where('erp_nationalities.name_ar', $this->name_ar);

        if ($this->name_la !== FALSE)
            $this->db->where('erp_nationalities.name_la', $this->name_la);

        if ($this->arabian !== FALSE)
            $this->db->where('erp_nationalities.arabian', $this->arabian);


        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('erp_nationalities');
        if ($rows_no)
            return $query->num_rows();

        return $query->result();
    }

}

/* End of file nationalities_model.php */
/* Location: ./application/models/nationalities_model.php */