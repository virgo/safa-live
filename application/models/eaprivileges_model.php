<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Eaprivileges_model extends CI_Model {

    public $safa_eaprivilege_id = FALSE;
    public $name_ar = FALSE;
    public $description_ar = FALSE;
    public $name_la = FALSE;
    public $description_la = FALSE;
    public $role_name = FALSE;
    public $custom_select = FALSE;
    public $limit = FALSE;
    public $offset = FALSE;
    public $order_by = FALSE;

    function __construct() {
        parent::__construct();
    }

    function get($rows_no = FALSE) {

        if ($this->custom_select !== FALSE) {
            $this->db->select('safa_eaprivilege_id');
            $this->db->select($this->custom_select);
        }

        if ($this->safa_eaprivilege_id !== FALSE)
            $this->db->where('safa_eaprivileges.safa_eaprivilege_id', $this->safa_eaprivilege_id);

        if ($this->name_ar !== FALSE)
            $this->db->where('safa_eaprivileges.name_ar', $this->name_ar);

        if ($this->description_ar !== FALSE)
            $this->db->where('safa_eaprivileges.description_ar', $this->description_ar);

        if ($this->name_la !== FALSE)
            $this->db->where('safa_eaprivileges.name_la', $this->name_la);

        if ($this->description_la !== FALSE)
            $this->db->where('safa_eaprivileges.description_la', $this->description_la);

        if ($this->role_name !== FALSE)
            $this->db->where('safa_eaprivileges.role_name', $this->role_name);


        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('safa_eaprivileges');
        if ($rows_no)
            return $query->num_rows();

        if ($this->safa_eaprivilege_id)
            return $query->row();
        else
            return $query->result();
    }

    function save() {
        if ($this->safa_eaprivilege_id !== FALSE)
            $this->db->set('safa_eaprivileges.safa_eaprivilege_id', $this->safa_eaprivilege_id);

        if ($this->name_ar !== FALSE)
            $this->db->set('safa_eaprivileges.name_ar', $this->name_ar);

        if ($this->description_ar !== FALSE)
            $this->db->set('safa_eaprivileges.description_ar', $this->description_ar);

        if ($this->name_la !== FALSE)
            $this->db->set('safa_eaprivileges.name_la', $this->name_la);

        if ($this->description_la !== FALSE)
            $this->db->set('safa_eaprivileges.description_la', $this->description_la);

        if ($this->role_name !== FALSE)
            $this->db->set('safa_eaprivileges.role_name', $this->role_name);



        if ($this->safa_eaprivilege_id) {
            $this->db->where('safa_eaprivileges.safa_eaprivilege_id', $this->safa_eaprivilege_id)->update('safa_eaprivileges');
        } else {
            $this->db->insert('safa_eaprivileges');
            return $this->db->insert_id();
        }
    }

    function delete() {
        if ($this->safa_eaprivilege_id !== FALSE)
            $this->db->where('safa_eaprivileges.safa_eaprivilege_id', $this->safa_eaprivilege_id);

        if ($this->name_ar !== FALSE)
            $this->db->where('safa_eaprivileges.name_ar', $this->name_ar);

        if ($this->description_ar !== FALSE)
            $this->db->where('safa_eaprivileges.description_ar', $this->description_ar);

        if ($this->name_la !== FALSE)
            $this->db->where('safa_eaprivileges.name_la', $this->name_la);

        if ($this->description_la !== FALSE)
            $this->db->where('safa_eaprivileges.description_la', $this->description_la);

        if ($this->role_name !== FALSE)
            $this->db->where('safa_eaprivileges.role_name', $this->role_name);



        $this->db->delete('safa_eaprivileges');
        return $this->db->affected_rows();
    }

    function search($rows_no = FALSE) {
        if ($this->custom_select !== FALSE) {
            $this->db->select('safa_eaprivilege_id');
            $this->db->select($this->custom_select);
        }

        if ($this->safa_eaprivilege_id !== FALSE)
            $this->db->where('safa_eaprivileges.safa_eaprivilege_id', $this->safa_eaprivilege_id);

        if ($this->name_ar !== FALSE)
            $this->db->where('safa_eaprivileges.name_ar', $this->name_ar);

        if ($this->description_ar !== FALSE)
            $this->db->where('safa_eaprivileges.description_ar', $this->description_ar);

        if ($this->name_la !== FALSE)
            $this->db->where('safa_eaprivileges.name_la', $this->name_la);

        if ($this->description_la !== FALSE)
            $this->db->where('safa_eaprivileges.description_la', $this->description_la);

        if ($this->role_name !== FALSE)
            $this->db->where('safa_eaprivileges.role_name', $this->role_name);


        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('safa_eaprivileges');
        if ($rows_no)
            return $query->num_rows();

        return $query->result();
    }

    function check_delete_ability($id) {
        $this->db->select('count(safa_ea_usergroups_eaprivileges.safa_eaprivilege_id)as eaprivilege_id');
        $this->db->from('safa_eaprivileges');
        $this->db->join('safa_ea_usergroups_eaprivileges', 'safa_eaprivileges.safa_eaprivilege_id = safa_ea_usergroups_eaprivileges.safa_eaprivilege_id', 'left');
        $this->db->group_by('safa_eaprivileges.safa_eaprivilege_id');
        $this->db->where('safa_eaprivileges.safa_eaprivilege_id', $id);
        $query = $this->db->get();
        $flag = 0;
        foreach ($query->result() as $row => $table) {
            foreach ($table as $col => $value) {
                if ($value > 0) {
                    $flag = 1;
                    break;
                } else {
                    continue;
                }
            }
        }
        return $flag;
    }

}

/* End of file eaprivileges_model.php */
/* Location: ./application/models/eaprivileges_model.php */