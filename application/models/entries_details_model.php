<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Entries_details_model extends CI_Model {

    public $id = FALSE;
    public $entry_id = FALSE;
    public $firm_id = FALSE;
    public $entry_number = FALSE;
    public $entry_date = FALSE;
    public $amount = FALSE;
    public $debit = FALSE;
    public $account = FALSE;
    public $second_account = FALSE;
    public $description = FALSE;
    public $remarks = FALSE;
    public $voucher = FALSE;
    public $currency = FALSE;
    public $rate = FALSE;
    public $carat = FALSE;
    public $entry_date_from = FALSE;
    public $entry_date_to = FALSE;
    public $limit = FALSE;
    public $offset = FALSE;

    function __construct() {
        parent::__construct();
    }

    function get($rows_no = FALSE) {

        if ($this->id)
            $this->db->where('entries_details.id', $this->id);

        if ($this->entry_id)
            $this->db->where('entries_details.entry_id', $this->entry_id);

//        if ($this->firm_id)
        $this->db->where('entries_details.firm_id', $this->session->userdata('firm_id'));

        if ($this->entry_number)
            $this->db->where('entries_details.entry_number', $this->entry_number);

        if ($this->entry_date)
            $this->db->where('entries_details.entry_date', $this->entry_date);

        if ($this->amount)
            $this->db->where('entries_details.amount', $this->amount);

        if ($this->debit !== FALSE)
            $this->db->where('entries_details.debit', $this->debit);

        if ($this->account)
            $this->db->where('entries_details.account', $this->account);

        if ($this->second_account)
            $this->db->where('entries_details.second_account', $this->second_account);

        if ($this->description)
            $this->db->where('entries_details.description', $this->description);

        if ($this->remarks)
            $this->db->where('entries_details.remarks', $this->remarks);

        if ($this->voucher)
            $this->db->where('entries_details.voucher', $this->voucher);

        if ($this->currency)
            $this->db->where('entries_details.currency', $this->currency);

        if ($this->rate)
            $this->db->where('entries_details.rate', $this->rate);

        if ($this->carat)
            $this->db->where('entries_details.carat', $this->carat);

        if ($this->entry_date_from)
            $this->db->where("entry_date >= '" . $this->entry_date_from . "'", FALSE, FALSE);

        if ($this->entry_date_to)
            $this->db->where("entry_date <= '" . $this->entry_date_to . "'", FALSE, FALSE);

        $this->db->join('entries_master', 'entries_master.id = entries_details.entry_id');
        $this->db->select('entries_details.*
            , entries_master.transaction_entry
            , entries_master.simple
            , entries_master.transaction_kind
            , entries_master.transaction_id
            ');
        $this->db->order_by('entry_date');
        $this->db->order_by('id');
        $query = $this->db->get('entries_details', $this->limit, $this->offset);

        if ($rows_no)
            return $query->num_rows();
        else
        if ($this->id)
            return $query->row();
        else
            return $query->result();
    }

    function save() {

        if ($this->id)
            $this->db->set('id', $this->id);

        if ($this->entry_id)
            $this->db->set('entry_id', $this->entry_id);

//        if ($this->firm_id)
        $this->db->set('firm_id', $this->session->userdata('firm_id'));

        if ($this->entry_number)
            $this->db->set('entry_number', $this->entry_number);

        if ($this->entry_date)
            $this->db->set('entry_date', $this->entry_date);

        if ($this->amount)
            $this->db->set('amount', $this->amount);

        if ($this->debit !== FALSE)
            $this->db->set('debit', $this->debit);

        if ($this->account)
            $this->db->set('account', $this->account);

        if ($this->second_account)
            $this->db->set('second_account', $this->second_account);

        if ($this->description)
            $this->db->set('description', $this->description);

        if ($this->remarks)
            $this->db->set('remarks', $this->remarks);

        if ($this->voucher)
            $this->db->set('voucher', $this->voucher);

        if ($this->currency)
            $this->db->set('currency', $this->currency);

        if ($this->rate)
            $this->db->set('rate', $this->rate);

        if ($this->carat)
            $this->db->set('carat', $this->carat);


        if ($this->id) {
            $this->db->where('id', $this->id)->update('entries_details');
        } else {
            $this->db->insert('entries_details');
            return $this->db->insert_id();
        }
    }

    function delete() {
        if ($this->firm_id)
            $this->db->where('firm_id', $this->firm_id);
        else
            $this->db->where('firm_id', $this->session->userdata('firm_id'));
        if ($this->id)
            $this->db->where('id', $this->id)->delete('entries_details');
        return $this->db->affected_rows();
    }

    function search() {
        if ($this->entry_date_from)
            $this->db->where("entry_date >= '" . $this->entry_date_from . "'", FALSE, FALSE);

        if ($this->entry_date_to)
            $this->db->where("entry_date <= '" . $this->entry_date_to . "'", FALSE, FALSE);

        if ($this->account)
            $this->db->where('account', $this->account);

//        $this->db->join('entries_master', 'entries_master.id = entries_details.entry_id');
//        $this->db->select('entries_details.*
//            , entries_master.transaction_entry
//            , entries_master.simple
//            ');
        $query = $this->db->get('entries_details', $this->limit, $this->offset);
        return $query->result();
    }

}
