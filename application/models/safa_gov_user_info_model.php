<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Safa_gov_user_info_model extends CI_Model {

    public $safa_gov_user_info_id = FALSE;
    public $safa_gov_user_id = FALSE;
    public $erp_city_id = FALSE;
    public $port_type = FALSE;
   
    public $custom_select = FALSE;
    public $limit = FALSE;
    public $offset = FALSE;
    public $order_by = FALSE;
    public $join = FALSE;

    function __construct() {
        parent::__construct();
    }

    function get($rows_no = FALSE) {

        if ($this->custom_select !== FALSE) {
            $this->db->select('safa_gov_user_info_id');
            $this->db->select($this->custom_select);
        }

        if ($this->safa_gov_user_info_id !== FALSE)
            $this->db->where('safa_gov_user_info.safa_gov_user_info_id', $this->safa_gov_user_info_id);

        if ($this->safa_gov_user_id !== FALSE)
            $this->db->where('safa_gov_user_info.safa_gov_user_id', $this->safa_gov_user_id);

        if ($this->erp_city_id !== FALSE)
            $this->db->where('safa_gov_user_info.erp_city_id', $this->erp_city_id);

        if ($this->port_type !== FALSE)
            $this->db->where('safa_gov_user_info.port_type', $this->port_type);

        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('safa_gov_user_info');
        if ($rows_no)
            return $query->num_rows();

        if ($this->safa_gov_user_info_id || $this->safa_gov_user_id )
            return $query->row();
        else
            return $query->result();
    }

    function save() 
    {
        if ($this->safa_gov_user_info_id !== FALSE)
            $this->db->set('safa_gov_user_info.safa_gov_user_info_id', $this->safa_gov_user_info_id);

        if ($this->safa_gov_user_id !== FALSE)
            $this->db->set('safa_gov_user_info.safa_gov_user_id', $this->safa_gov_user_id);

        if ($this->erp_city_id !== FALSE)
            $this->db->set('safa_gov_user_info.erp_city_id', $this->erp_city_id);

        if ($this->port_type !== FALSE)
            $this->db->set('safa_gov_user_info.port_type', $this->port_type);
            

        if ($this->safa_gov_user_info_id) {
            $this->db->where('safa_gov_user_info.safa_gov_user_info_id', $this->safa_gov_user_info_id)->update('safa_gov_user_info');
        } else {
            $this->db->insert('safa_gov_user_info');
            return $this->db->insert_id();
        }
    }

    function delete() {
        if ($this->safa_gov_user_info_id !== FALSE)
            $this->db->where('safa_gov_user_info.safa_gov_user_info_id', $this->safa_gov_user_info_id);

        if ($this->safa_gov_user_id !== FALSE)
            $this->db->where('safa_gov_user_info.safa_gov_user_id', $this->safa_gov_user_id);

        if ($this->erp_city_id !== FALSE)
            $this->db->where('safa_gov_user_info.erp_city_id', $this->erp_city_id);

        if ($this->port_type !== FALSE)
            $this->db->where('safa_gov_user_info.port_type', $this->port_type);
            

        $this->db->delete('safa_gov_user_info');
        return $this->db->affected_rows();
    }

    function search($rows_no = FALSE) 
    {
        if ($this->custom_select !== FALSE) {
            $this->db->select('safa_gov_user_info_id');
            $this->db->select($this->custom_select);
        }

        if ($this->safa_gov_user_info_id !== FALSE)
            $this->db->where('safa_gov_user_info.safa_gov_user_info_id', $this->safa_gov_user_info_id);

        if ($this->safa_gov_user_id !== FALSE)
            $this->db->where('safa_gov_user_info.safa_gov_user_id', $this->safa_gov_user_id);

        if ($this->erp_city_id !== FALSE)
            $this->db->where('safa_gov_user_info.erp_city_id', $this->erp_city_id);

        if ($this->port_type !== FALSE)
            $this->db->where('safa_gov_user_info.port_type', $this->port_type);
            
        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('safa_gov_user_info');
        if ($rows_no)
            return $query->num_rows();

        return $query->result();
    }

}

/* End of file gov_users_model.php */
/* Location: ./application/models/gov_users_model.php */