<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Safa_umrahgroups_model extends CI_Model {

    public $safa_umrahgroup_id = FALSE;
    public $name = FALSE;
    public $creation_date = FALSE;
    public $from_creation_date = FALSE;
    public $to_creation_date = FALSE;
    public $safa_ea_id = FALSE;
    public $safa_uo_contract_id = FALSE;
    public $safa_umrahgroups_status_id = FALSE;
    public $no_of_mutamers_from = FALSE;
    public $no_of_mutamers_to = FALSE;
    public $custom_select = FALSE;
    public $limit = FALSE;
    public $offset = FALSE;
    public $order_by = FALSE;
    public $join = FALSE;
    public $ea_id = FALSE;

    function __construct() {
        parent::__construct();
    }

    function get($rows_no = FALSE) {

        if ($this->custom_select !== FALSE) {
            $this->db->select('safa_umrahgroup_id');
            $this->db->select($this->custom_select);
        }

        if ($this->safa_umrahgroup_id !== FALSE)
            $this->db->where('safa_umrahgroups.safa_umrahgroup_id', $this->safa_umrahgroup_id);

        if ($this->name !== FALSE)
            $this->db->where('safa_umrahgroups.name', $this->name);

        if ($this->creation_date !== FALSE)
            $this->db->where('safa_umrahgroups.creation_date', $this->creation_date);

        if ($this->from_creation_date !== FALSE)
            $this->db->where('safa_umrahgroups.creation_date >=', $this->from_creation_date);

        if ($this->to_creation_date !== FALSE)
            $this->db->where('safa_umrahgroups.creation_date <=', $this->to_creation_date);

        if ($this->safa_ea_id !== FALSE)
            $this->db->where('safa_umrahgroups.safa_ea_id', $this->safa_ea_id);

        if ($this->safa_uo_contract_id !== FALSE)
            $this->db->where('safa_umrahgroups.safa_uo_contract_id', $this->safa_uo_contract_id);

        if ($this->safa_umrahgroups_status_id !== FALSE)
            $this->db->where('safa_umrahgroups.safa_umrahgroups_status_id', $this->safa_umrahgroups_status_id);


        if ($this->order_by && is_array($this->order_by)) {
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);
        } else {
            $this->db->order_by('safa_umrahgroups.creation_date', 'desc');
        }

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $this->db->select('safa_umrahgroups.*,count(safa_umrahgroups_passports.safa_umrahgroup_id)
             as no_of_mutamers,  safa_eas.name_ar as ea_name_ar, safa_eas.name_la as ea_name_la, safa_umrahgroups_status.name_ar as umrahgroups_status_name_ar, safa_umrahgroups_status.name_la as umrahgroups_status_name_la,safa_uo_contracts_eas.' . name() . ' as safa_uo_contracts_eas_name, safa_uos.erp_uasp_id');
        $this->db->join('safa_umrahgroups_passports', 'safa_umrahgroups.safa_umrahgroup_id = safa_umrahgroups_passports.safa_umrahgroup_id', 'left');

        //By Gouda
        $this->db->join('safa_eas', 'safa_umrahgroups.safa_ea_id = safa_eas.safa_ea_id', 'left');
        $this->db->join('safa_umrahgroups_status', 'safa_umrahgroups.safa_umrahgroups_status_id = safa_umrahgroups_status.safa_umrahgroups_status_id', 'left');
        
        $this->db->join("safa_uo_contracts", "safa_umrahgroups.safa_uo_contract_id=safa_uo_contracts.safa_uo_contract_id", 'left');
        /**
         * @author Muhammad El-Saeed <m.elsaeed@virgotel.com>
         */
        $this->db->join("safa_uos", "safa_uo_contracts.safa_uo_id = safa_uos.safa_uo_id", 'left');
        $this->db->join("safa_uo_contracts_eas", "safa_uo_contracts_eas.safa_uo_contract_id=safa_uo_contracts.safa_uo_contract_id", 'left');
        $this->db->where('safa_umrahgroups.safa_ea_id', $this->ea_id);
        $this->db->group_by('safa_umrahgroups.safa_umrahgroup_id');

        //By Gouda
        if ($this->no_of_mutamers_from !== FALSE)
            $this->db->having('no_of_mutamers >=', $this->no_of_mutamers_from);
        if ($this->no_of_mutamers_to !== FALSE)
            $this->db->having('no_of_mutamers <=', $this->no_of_mutamers_to);
        $query = $this->db->get('safa_umrahgroups');

        if ($rows_no)
            return $query->num_rows();

        if ($this->safa_umrahgroup_id)
            return $query->row();
        else
            return $query->result();
    }

    function save() {
        if ($this->safa_umrahgroup_id !== FALSE)
            $this->db->set('safa_umrahgroups.safa_umrahgroup_id', $this->safa_umrahgroup_id);

        if ($this->name !== FALSE)
            $this->db->set('safa_umrahgroups.name', $this->name);

        if ($this->creation_date !== FALSE)
            $this->db->set('safa_umrahgroups.creation_date', $this->creation_date);

        if ($this->safa_ea_id !== FALSE)
            $this->db->set('safa_umrahgroups.safa_ea_id', $this->safa_ea_id);

        if ($this->safa_uo_contract_id !== FALSE)
            $this->db->set('safa_umrahgroups.safa_uo_contract_id', $this->safa_uo_contract_id);

        if ($this->safa_umrahgroups_status_id !== FALSE)
            $this->db->set('safa_umrahgroups.safa_umrahgroups_status_id', $this->safa_umrahgroups_status_id);


        if ($this->safa_umrahgroup_id) {
            $this->db->where('safa_umrahgroups.safa_umrahgroup_id', $this->safa_umrahgroup_id)->update('safa_umrahgroups');
        } else {
            $this->db->insert('safa_umrahgroups');
            return $this->db->insert_id();
        }
    }

    function delete() {
        if ($this->safa_umrahgroup_id !== FALSE)
            $this->db->where('safa_umrahgroups.safa_umrahgroup_id', $this->safa_umrahgroup_id);

        if ($this->name !== FALSE)
            $this->db->where('safa_umrahgroups.name', $this->name);

        if ($this->creation_date !== FALSE)
            $this->db->where('safa_umrahgroups.creation_date', $this->creation_date);

        if ($this->safa_ea_id !== FALSE)
            $this->db->where('safa_umrahgroups.safa_ea_id', $this->safa_ea_id);

        if ($this->safa_uo_contract_id !== FALSE)
            $this->db->where('safa_umrahgroups.safa_uo_contract_id', $this->safa_uo_contract_id);

        if ($this->safa_umrahgroups_status_id !== FALSE)
            $this->db->where('safa_umrahgroups.safa_umrahgroups_status_id', $this->safa_umrahgroups_status_id);


        $this->db->delete('safa_umrahgroups');
        return $this->db->affected_rows();
    }

    function search($rows_no = FALSE) {
        if ($this->custom_select !== FALSE) {
            $this->db->select('safa_umrahgroup_id');
            $this->db->select($this->custom_select);
        }

        if ($this->safa_umrahgroup_id !== FALSE)
            $this->db->where('safa_umrahgroups.safa_umrahgroup_id', $this->safa_umrahgroup_id);

        if ($this->name !== FALSE)
            $this->db->where('safa_umrahgroups.name', $this->name);

        if ($this->creation_date !== FALSE)
            $this->db->where('safa_umrahgroups.creation_date', $this->creation_date);

        if ($this->from_creation_date !== FALSE)
            $this->db->where('safa_umrahgroups.creation_date >=', $this->from_creation_date);

        if ($this->to_creation_date !== FALSE)
            $this->db->where('safa_umrahgroups.creation_date <=', $this->to_creation_date);

        if ($this->safa_ea_id !== FALSE)
            $this->db->where('safa_umrahgroups.safa_ea_id', $this->safa_ea_id);

        if ($this->safa_uo_contract_id !== FALSE)
            $this->db->where('safa_umrahgroups.safa_uo_contract_id', $this->safa_uo_contract_id);

        if ($this->safa_umrahgroups_status_id !== FALSE)
            $this->db->where('safa_umrahgroups.safa_umrahgroups_status_id', $this->safa_umrahgroups_status_id);

        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('safa_umrahgroups');
        if ($rows_no)
            return $query->num_rows();

        return $query->result();
    }

    function check_delete_ability($id) {
        $this->db->select('count(safa_umrahgroups_passports.safa_umrahgroup_id)as safa_umrahgroups,');
        $this->db->from('safa_umrahgroups');
        $this->db->join('safa_umrahgroups_passports', 'safa_umrahgroups.safa_umrahgroup_id = safa_umrahgroups_passports.safa_umrahgroup_id', 'left');
        $this->db->group_by('safa_umrahgroups.safa_umrahgroup_id');
        $this->db->where('safa_umrahgroups.safa_umrahgroup_id', $id);
        $query = $this->db->get();
        $flag = 0;
        foreach ($query->result() as $row => $table) {
            foreach ($table as $col => $value) {
                if ($value > 0) {
                    $flag = 1;
                    break;
                } else {
                    continue;
                }
            }
        }
        return $flag;
    }

    function get_ea_contracts() {
        $this->db->select("safa_uo_contracts.safa_uo_contract_id as contract_id,safa_uo_contracts." . name() . ",safa_uos." . name() . " as safa_uos_name,safa_uo_contracts_eas." . name() . " as safa_uo_contracts_eas_name");
        $this->db->from("safa_uo_contracts");
        $this->db->join("safa_uo_contracts_eas", "safa_uo_contracts_eas.safa_uo_contract_id=safa_uo_contracts.safa_uo_contract_id");
        $this->db->join("safa_uos", "safa_uos.safa_uo_id=safa_uo_contracts.safa_uo_id");
        $this->db->where("safa_uo_contracts_eas.disabled=0");

        $this->db->where("safa_uo_contracts_eas.safa_ea_id", $this->ea_id); //from a seasion;
        return $this->db->get()->result();
    }

    function get_group_info($id, $ea_id) {
        $this->db->where('safa_umrahgroups.safa_umrahgroup_id', $id);
        $this->db->where('safa_umrahgroups.safa_ea_id', $ea_id);
        $query = $this->db->get('safa_umrahgroups');
        return $query->row();
    }

}

/* End of file safa_umrahgroups_model.php */
/* Location: ./application/models/ea_usergroups_model.php */

