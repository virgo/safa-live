<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Crm_packages extends CI_Model {

    public $crm_package_id = FALSE;
    public $erp_country_id = FALSE;
    public $name = FALSE;
    public $amount = FALSE;
    public $credit = FALSE;
    public $additional_credit = FALSE;
    public $erp_currency_id = FALSE;
    public $custom_select = FALSE;
    public $limit = FALSE;
    public $offset = FALSE;
    public $order_by = FALSE;

    function __construct() {
        parent::__construct();
    }

    function get($rows_no = FALSE) {

        if ($this->custom_select !== FALSE) {
            $this->db->select('crm_package_id');
            $this->db->select($this->custom_select);
        }

        if ($this->crm_package_id !== FALSE)
            $this->db->where('crm_packages.crm_package_id', $this->crm_package_id);

        if ($this->erp_currency_id !== FALSE)
            $this->db->where('crm_packages.erp_currency_id', $this->erp_currency_id);

        if ($this->erp_country_id !== FALSE)
            $this->db->where('crm_packages.erp_country_id', $this->erp_country_id);

        if ($this->name !== FALSE)
            $this->db->where('crm_packages.name', $this->name);

        if ($this->amount !== FALSE)
            $this->db->where('crm_packages.amount', $this->amount);

        if ($this->credit !== FALSE)
            $this->db->where('crm_packages.credit', $this->credit);

        if ($this->additional_credit !== FALSE)
            $this->db->where('crm_packages.additional_credit', $this->additional_credit);


        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('crm_packages');
        if ($rows_no)
            return $query->num_rows();

        if ($this->crm_package_id)
            return $query->row();
        else
            return $query->result();
    }

    function save() {
        if ($this->crm_package_id !== FALSE)
            $this->db->set('crm_packages.crm_package_id', $this->crm_package_id);

        if ($this->erp_country_id !== FALSE)
            $this->db->set('crm_packages.erp_country_id', $this->erp_country_id);

        if ($this->name !== FALSE)
            $this->db->set('crm_packages.name', $this->name);

        if ($this->amount !== FALSE)
            $this->db->set('crm_packages.amount', $this->amount);

        if ($this->credit !== FALSE)
            $this->db->set('crm_packages.credit', $this->credit);

        if ($this->additional_credit !== FALSE)
            $this->db->set('crm_packages.additional_credit', $this->additional_credit);

        if ($this->erp_currency_id !== FALSE)
            $this->db->set('crm_packages.erp_currency_id', $this->erp_currency_id);

        if ($this->crm_package_id) {
            $this->db->where('crm_packages.crm_package_id', $this->crm_package_id)->update('crm_packages');
        } else {
            $this->db->insert('crm_packages');
            return $this->db->insert_id();
        }
    }

    function delete() {
        if ($this->crm_package_id !== FALSE)
            $this->db->where('crm_packages.crm_package_id', $this->crm_package_id);

        if ($this->erp_currency_id !== FALSE)
            $this->db->where('crm_packages.erp_currency_id', $this->erp_currency_id);

        if ($this->erp_country_id !== FALSE)
            $this->db->where('crm_packages.erp_country_id', $this->erp_country_id);

        if ($this->name !== FALSE)
            $this->db->where('crm_packages.name', $this->name);

        if ($this->amount !== FALSE)
            $this->db->where('crm_packages.amount', $this->amount);

        if ($this->credit !== FALSE)
            $this->db->where('crm_packages.credit', $this->credit);

        if ($this->additional_credit !== FALSE)
            $this->db->where('crm_packages.additional_credit', $this->additional_credit);



        $this->db->delete('crm_packages');
        return $this->db->affected_rows();
    }

    function search($rows_no = FALSE) {
        if ($this->custom_select !== FALSE) {
            $this->db->select('crm_package_id');
            $this->db->select($this->custom_select);
        }

        if ($this->crm_package_id !== FALSE)
            $this->db->where('crm_packages.crm_package_id', $this->crm_package_id);

        if ($this->erp_currency_id !== FALSE)
            $this->db->where('crm_packages.erp_currency_id', $this->erp_currency_id);

        if ($this->erp_country_id !== FALSE)
            $this->db->where('crm_packages.erp_country_id', $this->erp_country_id);

        if ($this->name !== FALSE)
            $this->db->where('crm_packages.name', $this->name);

        if ($this->amount !== FALSE)
            $this->db->where('crm_packages.amount', $this->amount);

        if ($this->credit !== FALSE)
            $this->db->where('crm_packages.credit', $this->credit);

        if ($this->additional_credit !== FALSE)
            $this->db->where('crm_packages.additional_credit', $this->additional_credit);


        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('crm_packages');
        if ($rows_no)
            return $query->num_rows();

        return $query->result();
    }

}

/* End of file packages_model.php */
/* Location: ./application/models/packages_model.php */