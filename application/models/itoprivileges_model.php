<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Itoprivileges_model extends CI_Model {

    public $safa_itoprivilege_id = FALSE;
    public $name_ar = FALSE;
    public $description_ar = FALSE;
    public $name_la = FALSE;
    public $description_la = FALSE;
    public $role_name = FALSE;
    public $custom_select = FALSE;
    public $limit = FALSE;
    public $offset = FALSE;
    public $order_by = FALSE;

    function __construct() {
        parent::__construct();
    }

    function get($rows_no = FALSE) {

        if ($this->custom_select !== FALSE) {
            $this->db->select('safa_itoprivilege_id');
            $this->db->select($this->custom_select);
        }

        if ($this->safa_itoprivilege_id !== FALSE)
            $this->db->where('safa_itoprivileges.safa_itoprivilege_id', $this->safa_itoprivilege_id);

        if ($this->name_ar !== FALSE)
            $this->db->where('safa_itoprivileges.name_ar', $this->name_ar);

        if ($this->description_ar !== FALSE)
            $this->db->where('safa_itoprivileges.description_ar', $this->description_ar);

        if ($this->name_la !== FALSE)
            $this->db->where('safa_itoprivileges.name_la', $this->name_la);

        if ($this->description_la !== FALSE)
            $this->db->where('safa_itoprivileges.description_la', $this->description_la);

        if ($this->role_name !== FALSE)
            $this->db->where('safa_itoprivileges.role_name', $this->role_name);


        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('safa_itoprivileges');
        if ($rows_no)
            return $query->num_rows();

        if ($this->safa_itoprivilege_id)
            return $query->row();
        else
            return $query->result();
    }

    function save() {
        if ($this->safa_itoprivilege_id !== FALSE)
            $this->db->set('safa_itoprivileges.safa_itoprivilege_id', $this->safa_itoprivilege_id);

        if ($this->name_ar !== FALSE)
            $this->db->set('safa_itoprivileges.name_ar', $this->name_ar);

        if ($this->description_ar !== FALSE)
            $this->db->set('safa_itoprivileges.description_ar', $this->description_ar);

        if ($this->name_la !== FALSE)
            $this->db->set('safa_itoprivileges.name_la', $this->name_la);

        if ($this->description_la !== FALSE)
            $this->db->set('safa_itoprivileges.description_la', $this->description_la);

        if ($this->role_name !== FALSE)
            $this->db->set('safa_itoprivileges.role_name', $this->role_name);



        if ($this->safa_itoprivilege_id) {
            $this->db->where('safa_itoprivileges.safa_itoprivilege_id', $this->safa_itoprivilege_id)->update('safa_itoprivileges');
        } else {
            $this->db->insert('safa_itoprivileges');
            return $this->db->insert_id();
        }
    }

    function delete() {
        if ($this->safa_itoprivilege_id !== FALSE)
            $this->db->where('safa_itoprivileges.safa_itoprivilege_id', $this->safa_itoprivilege_id);

        if ($this->name_ar !== FALSE)
            $this->db->where('safa_itoprivileges.name_ar', $this->name_ar);

        if ($this->description_ar !== FALSE)
            $this->db->where('safa_itoprivileges.description_ar', $this->description_ar);

        if ($this->name_la !== FALSE)
            $this->db->where('safa_itoprivileges.name_la', $this->name_la);

        if ($this->description_la !== FALSE)
            $this->db->where('safa_itoprivileges.description_la', $this->description_la);

        if ($this->role_name !== FALSE)
            $this->db->where('safa_itoprivileges.role_name', $this->role_name);



        $this->db->delete('safa_itoprivileges');
        return $this->db->affected_rows();
    }

    function search($rows_no = FALSE) {
        if ($this->custom_select !== FALSE) {
            $this->db->select('safa_itoprivilege_id');
            $this->db->select($this->custom_select);
        }

        if ($this->safa_itoprivilege_id !== FALSE)
            $this->db->where('safa_itoprivileges.safa_itoprivilege_id', $this->safa_itoprivilege_id);

        if ($this->name_ar !== FALSE)
            $this->db->where('safa_itoprivileges.name_ar', $this->name_ar);

        if ($this->description_ar !== FALSE)
            $this->db->where('safa_itoprivileges.description_ar', $this->description_ar);

        if ($this->name_la !== FALSE)
            $this->db->where('safa_itoprivileges.name_la', $this->name_la);

        if ($this->description_la !== FALSE)
            $this->db->where('safa_itoprivileges.description_la', $this->description_la);

        if ($this->role_name !== FALSE)
            $this->db->where('safa_itoprivileges.role_name', $this->role_name);


        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('safa_itoprivileges');
        if ($rows_no)
            return $query->num_rows();

        return $query->result();
    }

    function check_delete_ability($id) {
        $this->db->select('count(safa_ito_usergroups_itoprivileges.safa_itoprivilege_id)as itoprivilege_id');
        $this->db->from('safa_itoprivileges');
        $this->db->join('safa_ito_usergroups_itoprivileges', 'safa_itoprivileges.safa_itoprivilege_id = safa_ito_usergroups_itoprivileges.safa_itoprivilege_id', 'left');
        $this->db->group_by('safa_itoprivileges.safa_itoprivilege_id');
        $this->db->where('safa_itoprivileges.safa_itoprivilege_id', $id);
        $query = $this->db->get();
        $flag = 0;
        foreach ($query->result() as $row => $table) {
            foreach ($table as $col => $value) {
                if ($value > 0) {
                    $flag = 1;
                    break;
                } else {
                    continue;
                }
            }
        }
        return $flag;
    }

}

/* End of file eaprivileges_model.php */
/* Location: ./application/models/eaprivileges_model.php */