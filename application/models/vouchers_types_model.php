<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Vouchers_types_model extends CI_Model {

    public $id = FALSE;
    public $name_ar = FALSE;
    public $name_la = FALSE;
    public $limit = FALSE;
    public $offset = FALSE;

    function __construct() {
        parent::__construct();
    }

    function get($rows_no = FALSE) {

        if ($this->id)
            $this->db->where('id', $this->id);

        if ($this->name_ar !== FALSE)
            $this->db->where('name_ar', $this->name_ar);

        if ($this->name_la !== FALSE)
            $this->db->where('name_la', $this->name_la);

        if ($rows_no)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('vouchers_types');

        if ($rows_no)
            return $query->num_rows();
        else
        if ($this->id)
            return $query->row();
        else
            return $query->result();
    }

    function save() {

        if ($this->id)
            $this->db->set('id', $this->id);


        if ($this->name_ar !== FALSE)
            $this->db->set('name_ar', $this->name_ar);

        if ($this->name_la !== FALSE)
            $this->db->set('name_la', $this->name_la);

        if ($this->id) {

            $this->db->where('id', $this->id)->update('vouchers_types');
            return $this->id;
        } else {

            $this->db->insert('vouchers_types');
            return $this->db->insert_id();
        }
    }

    function delete() {
        $this->db->where('id', $this->id)->delete('vouchers_types');
        return $this->db->affected_rows();
    }

}

/* End of file vouchers_types.php */
/* Location: ./application/models/vouchers_types_model.php */