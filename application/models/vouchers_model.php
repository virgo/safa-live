<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class vouchers_model extends CI_Model {

    public $id = FALSE;
    public $firm_id = FALSE;
    public $voucher_number = FALSE;
    public $voucher_defs_id = FALSE;
    public $voucher_date = FALSE;
    public $currency = FALSE;
    public $rate = FALSE;
    public $cash_account = FALSE;
    public $account = FALSE;
    public $amount = FALSE;
    public $cash_balance = FALSE;
    public $opening = FALSE;
    public $description = FALSE;
    public $remarks = FALSE;
    public $entry_number = FALSE;
    public $dealing_with_gold = FALSE;
    public $opening_credit = FALSE;
    public $gold_caliber_id = FALSE;
    public $branch_id = FALSE;
    public $user_id = FALSE;
    public $edit_user_id = FALSE;
    public $payment_request_id = FALSE;
    public $payment_request_no = FALSE;
    public $payment_date = FALSE;
    public $limit = FALSE;
    public $offset = FALSE;

    function __construct() {
        parent::__construct();
    }

    function get($rows_no = FALSE) {

        if ($this->id)
            $this->db->where('vouchers.id', $this->id);

//       if ($this->firm_id)
        $this->db->where('vouchers.firm_id', session('firm_id'));

        if ($this->voucher_number)
            $this->db->where('vouchers.voucher_number', $this->voucher_number);

        if ($this->voucher_defs_id)
            $this->db->where('vouchers.voucher_defs_id', $this->voucher_defs_id);

        if ($this->voucher_date)
            $this->db->where('vouchers.voucher_date', $this->voucher_date);

        if ($this->currency)
            $this->db->where('vouchers.currency', $this->currency);

        if ($this->rate)
            $this->db->where('vouchers.rate', $this->rate);

        if ($this->cash_account)
            $this->db->where('vouchers.cash_account', $this->cash_account);

        if ($this->account)
            $this->db->where('vouchers.account', $this->account);

        if ($this->amount)
            $this->db->where('vouchers.amount', $this->amount);

        if ($this->description)
            $this->db->where('vouchers.description', $this->description);

        if ($this->remarks)
            $this->db->where('vouchers.remarks', $this->remarks);

        if ($this->entry_number)
            $this->db->where('vouchers.entry_number', $this->entry_number);

        if ($this->branch_id)
            $this->db->where('vouchers.branch_id', $this->branch_id);
        if ($this->user_id)
            $this->db->where('vouchers.user_id', $this->user_id);
        if ($this->edit_user_id)
            $this->db->where('vouchers.edit_user_id', $this->edit_user_id);

        $this->db->select('vouchers.*
            ,vouchers_defs.main_type
            ,vouchers_defs.cash_account as main_cash_account
            ,vouchers_defs.currency as main_currency');
        $this->db->join('vouchers_defs', 'vouchers.voucher_defs_id = vouchers_defs.id');
        $this->db->order_by('vouchers.voucher_date', 'desc');
        $this->db->order_by('vouchers.voucher_number', 'desc');
        $query = $this->db->get('vouchers', $this->limit, $this->offset);

        if ($rows_no)
            return $query->num_rows();
        else
        if ($this->id || $this->voucher_number)
            return $query->row();
        else
            return $query->result();
    }

    function save() {

        if ($this->id)
            $this->db->set('id', $this->id);

//        if ($this->firm_id)
        $this->db->set('firm_id', session('firm_id'));

        if ($this->voucher_number)
            $this->db->set('voucher_number', $this->voucher_number);

        if ($this->voucher_defs_id)
            $this->db->set('voucher_defs_id', $this->voucher_defs_id);

        if ($this->voucher_date)
            $this->db->set('voucher_date', $this->voucher_date);

        if ($this->currency)
            $this->db->set('currency', $this->currency);

        if ($this->rate)
            $this->db->set('rate', $this->rate);

        if ($this->cash_account)
            $this->db->set('cash_account', $this->cash_account);

        if ($this->account)
            $this->db->set('account', $this->account);

        if ($this->amount)
            $this->db->set('amount', $this->amount);

        if ($this->cash_balance !== FALSE)
            $this->db->set('cash_balance', $this->cash_balance);

        if ($this->opening !== FALSE)
            $this->db->set('opening', $this->opening);

        if ($this->description)
            $this->db->set('description', $this->description);

        if ($this->remarks)
            $this->db->set('remarks', $this->remarks);

        if ($this->entry_number)
            $this->db->set('entry_number', $this->entry_number);

        if ($this->dealing_with_gold !== FALSE)
            $this->db->set('dealing_with_gold', $this->dealing_with_gold);

        if ($this->opening_credit)
            $this->db->set('opening_credit', $this->opening_credit);

        if ($this->gold_caliber_id)
            $this->db->set('gold_caliber_id', $this->gold_caliber_id);

        if ($this->branch_id || !permission('manage_accounting'))
            $this->db->set('branch_id', $this->branch_id);
        if ($this->user_id)
            $this->db->set('user_id', $this->user_id);
        if ($this->edit_user_id)
            $this->db->set('edit_user_id', $this->edit_user_id);



        if ($this->id) {
            $this->db->where('id', $this->id)->update('vouchers');
        } else {
            $this->db->insert('vouchers');
            return $this->db->insert_id();
        }
    }

    function delete() {
        if ($this->firm_id)
            $this->db->where('firm_id', $this->firm_id);
        else
            $this->db->where('firm_id', session('firm_id'));
        $this->db->where('id', $this->id)->delete('vouchers');
        return $this->db->affected_rows();
    }

    function getPaymentRequest() {

        if ($this->id)
            $this->db->where('voucher_id', $this->id);
        if ($this->payment_request_id)
            $this->db->where('res_payments_id', $this->payment_request_id);
        if ($this->payment_request_no)
            $this->db->where('no', $this->payment_request_no);

        $this->db->where('firm_id', session('firm_id'));

        $query = $this->db->get('res_payments')->row();

        if ($query)
            return $query;
        else
            return FALSE;
    }

    function updatePaymentRequest() {

        if ($this->payment_request_id !== FALSE)
            $this->db->where('res_payments_id', $this->payment_request_id);
        if ($this->payment_request_no !== FALSE)
            $this->db->where('no', $this->payment_request_no);

        $this->db->where('firm_id', session('firm_id'));

        if ($this->id !== FALSE)
            $this->db->set('voucher_id', $this->id);

        if ($this->payment_date !== FALSE)
            $this->db->set('payment_date', $this->payment_date);

        $query = $this->db->update('res_payments');

        if ($query)
            return $query;
        else
            return FALSE;
    }

}

/* End of file vouchers.php */
/* Location: ./application/models/vouchers_model.php */