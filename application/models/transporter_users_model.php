<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Transporter_users_model extends CI_Model {

    public $safa_transporter_user_id = FALSE;
    public $name_ar = FALSE;
    public $name_la = FALSE;
    public $username = FALSE;
    public $password = FALSE;
    public $safa_transporter_id = FALSE;
    public $custom_select = FALSE;
    public $limit = FALSE;
    public $offset = FALSE;
    public $order_by = FALSE;
    public $join;

    function __construct() {
        parent::__construct();
    }

    function get($rows_no = FALSE) {

        if ($this->custom_select !== FALSE) {
            $this->db->select('safa_transporter_user_id');
            $this->db->select($this->custom_select);
        }

        if ($this->safa_transporter_user_id !== FALSE)
            $this->db->where('safa_transporter_users.safa_transporter_user_id', $this->safa_transporter_user_id);

        if ($this->name_ar !== FALSE)
            $this->db->where('safa_transporter_users.name_ar', $this->name_ar);

        if ($this->name_la !== FALSE)
            $this->db->where('safa_transporter_users.name_la', $this->name_la);

        if ($this->username !== FALSE)
            $this->db->where('safa_transporter_users.username', $this->username);

        if ($this->password !== FALSE)
            $this->db->where('safa_transporter_users.password', $this->password);

        if ($this->safa_transporter_id !== FALSE)
            $this->db->where('safa_transporters.safa_transporter_id', $this->safa_transporter_id);

        if ($this->join) {
            $this->db->select('safa_transporters.name_ar as transporter_name,safa_transporters.safa_transporter_id,safa_transporter_users.name_ar as arabic_name
                ,safa_transporter_users.name_la as english_name,safa_transporter_users.safa_transporter_id , safa_transporter_users.safa_transporter_user_id ,
            safa_transporter_users.username,safa_transporter_users.password');
            $this->db->join('safa_transporters', 'safa_transporter_users.safa_transporter_id = safa_transporters.safa_transporter_id');
        }


        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('safa_transporter_users');
        if ($rows_no)
            return $query->num_rows();

        if ($this->safa_transporter_user_id)
            return $query->row();
        else
            return $query->result();
    }

    function save() {

        if ($this->safa_transporter_user_id !== FALSE)
            $this->db->set('safa_transporter_users.safa_transporter_user_id', $this->safa_transporter_user_id);

        if ($this->name_ar !== FALSE)
            $this->db->set('safa_transporter_users.name_ar', $this->name_ar);

        if ($this->name_la !== FALSE)
            $this->db->set('safa_transporter_users.name_la', $this->name_la);

        if ($this->username !== FALSE)
            $this->db->set('safa_transporter_users.username', $this->username);

        if ($this->password !== FALSE)
            $this->db->set('safa_transporter_users.password', $this->password);

        if ($this->safa_transporter_id !== FALSE)
            $this->db->set('safa_transporter_users.safa_transporter_id', $this->safa_transporter_id);


        if ($this->safa_transporter_user_id) {
            $this->db->where('safa_transporter_users.safa_transporter_user_id', $this->safa_transporter_user_id)->update('safa_transporter_users');
        } else {
            $this->db->insert('safa_transporter_users');
            return $this->db->insert_id();
        }
    }

    function delete() {

        if ($this->safa_transporter_user_id !== FALSE)
            $this->db->where('safa_transporter_users.safa_transporter_user_id', $this->safa_transporter_user_id);

        if ($this->name_ar !== FALSE)
            $this->db->where('safa_transporter_users.name_ar', $this->name_ar);

        if ($this->name_la !== FALSE)
            $this->db->where('safa_transporter_users.name_la', $this->name_la);

        if ($this->username !== FALSE)
            $this->db->where('safa_transporter_users.username', $this->username);

        if ($this->password !== FALSE)
            $this->db->where('safa_transporter_users.password', $this->password);

        if ($this->safa_transporter_id !== FALSE)
            $this->db->where('safa_transporter_users.safa_transporter_id', $this->safa_transporter_id);


        $this->db->delete('safa_transporter_users');
        return $this->db->affected_rows();
    }

    function search($rows_no = FALSE) {
        if ($this->custom_select !== FALSE) {
            $this->db->select('safa_transporter_user_id');
            $this->db->select($this->custom_select);
        }

        if ($this->safa_transporter_user_id !== FALSE)
            $this->db->where('safa_transporter_users.safa_transporter_user_id', $this->safa_transporter_user_id);

        if ($this->name_ar !== FALSE)
            $this->db->where('safa_transporter_users.name_ar', $this->name_ar);

        if ($this->name_la !== FALSE)
            $this->db->where('safa_transporter_users.name_la', $this->name_la);

        if ($this->username !== FALSE)
            $this->db->where('safa_transporter_users.username', $this->username);

        if ($this->password !== FALSE)
            $this->db->where('safa_transporter_users.password', $this->password);

        if ($this->safa_transporter_id !== FALSE)
            $this->db->where('safa_transporter_users.safa_transporter_id', $this->safa_transporter_id);

        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('safa_transporter_users');
        if ($rows_no)
            return $query->num_rows();

        return $query->result();
    }

}

/* End of file transporter_users_model.php */
/* Location: ./application/models/transporter_users_model.php */