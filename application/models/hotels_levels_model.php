<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Hotels_levels_model extends CI_Model {

    public $erp_hotellevel_id = FALSE;
    public $name_ar = FALSE;
    public $name_la = FALSE;
    public $code = FALSE;
    public $custom_select = FALSE;
    public $limit = FALSE;
    public $offset = FALSE;
    public $order_by = FALSE;

    function __construct() {
        parent::__construct();
    }

    function get($rows_no = FALSE) {

        if ($this->custom_select !== FALSE) {
            $this->db->select('erp_hotellevel_id');
            $this->db->select($this->custom_select);
        }

        if ($this->erp_hotellevel_id !== FALSE)
            $this->db->where('erp_hotellevels.erp_hotellevel_id', $this->erp_hotellevel_id);

        if ($this->name_ar !== FALSE)
            $this->db->where('erp_hotellevels.name_ar', $this->name_ar);

        if ($this->name_la !== FALSE)
            $this->db->where('erp_hotellevels.name_la', $this->name_la);

        if ($this->code !== FALSE)
            $this->db->where('erp_hotellevels.code', $this->code);


        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('erp_hotellevels');
        if ($rows_no)
            return $query->num_rows();

        if ($this->erp_hotellevel_id)
            return $query->row();
        else
            return $query->result();
    }

    function save() {
        if ($this->erp_hotellevel_id !== FALSE)
            $this->db->set('erp_hotellevels.erp_hotellevel_id', $this->erp_hotellevel_id);

        if ($this->name_ar !== FALSE)
            $this->db->set('erp_hotellevels.name_ar', $this->name_ar);

        if ($this->name_la !== FALSE)
            $this->db->set('erp_hotellevels.name_la', $this->name_la);

        if ($this->code !== FALSE)
            $this->db->set('erp_hotellevels.code', $this->code);



        if ($this->erp_hotellevel_id) {
            $this->db->where('erp_hotellevels.erp_hotellevel_id', $this->erp_hotellevel_id)->update('erp_hotellevels');
        } else {
            $this->db->insert('erp_hotellevels');
            return $this->db->insert_id();
        }
    }

    function delete() {
        if ($this->erp_hotellevel_id !== FALSE)
            $this->db->where('erp_hotellevels.erp_hotellevel_id', $this->erp_hotellevel_id);

        if ($this->name_ar !== FALSE)
            $this->db->where('erp_hotellevels.name_ar', $this->name_ar);

        if ($this->name_la !== FALSE)
            $this->db->where('erp_hotellevels.name_la', $this->name_la);

        if ($this->code !== FALSE)
            $this->db->where('erp_hotellevels.code', $this->code);

        $this->db->delete('erp_hotellevels');
        return $this->db->affected_rows();
    }

    function search($rows_no = FALSE) {
        if ($this->custom_select !== FALSE) {
            $this->db->select('erp_hotellevel_id');
            $this->db->select($this->custom_select);
        }

        if ($this->erp_hotellevel_id !== FALSE)
            $this->db->where('erp_hotellevels.erp_hotellevel_id', $this->erp_hotellevel_id);

        if ($this->name_ar !== FALSE)
            $this->db->where('erp_hotellevels.name_ar', $this->name_ar);

        if ($this->name_la !== FALSE)
            $this->db->where('erp_hotellevels.name_la', $this->name_la);

        if ($this->code !== FALSE)
            $this->db->where('erp_hotellevels.code', $this->code);


        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('erp_hotellevels');
        if ($rows_no)
            return $query->num_rows();

        return $query->result();
    }

//    function check_delete_ability($id=false){
//        $records=$this->db->where('erp_hotellevels.erp_hotellevel_id',$id)
//                ->join('erp_hotels','erp_hotels.erp_hotellevel_id=erp_hotellevels.erp_hotellevel_id')
//                ->get('erp_hotellevels');
//        return $records->num_rows === 0 ;
//    } 
    ######################################################

    function check_delete_ability($id) {

        $this->db->select('count(erp_hotels.erp_hotellevel_id)as hotellevel_id');
        $this->db->from('erp_hotellevels');
        $this->db->join('erp_hotels', 'erp_hotellevels.erp_hotellevel_id = erp_hotels.erp_hotellevel_id', 'left');
        $this->db->group_by('erp_hotellevels.erp_hotellevel_id');
        $this->db->where('erp_hotellevels.erp_hotellevel_id', $id);
        $query = $this->db->get();
        $flag = 0;
        foreach ($query->result() as $row => $table) {
            foreach ($table as $col => $value) {
                if ($value > 0) {
                    $flag = 1;
                    break;
                } else {
                    continue;
                }
            }
        }
        return $flag;
    }

}

/* End of file hotellevels_model.php */
/* Location: ./application/models/hotellevels_model.php */