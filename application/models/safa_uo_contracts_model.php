<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Safa_uo_contracts_model extends CI_Model {

    public $safa_uo_contract_id = FALSE;
    public $safa_uo_id = FALSE;
    public $contract_username = FALSE;
    public $contract_password = FALSE;
    public $name_ar = FALSE;
    public $name_la = FALSE;
    public $custom_select = FALSE;
    public $limit = FALSE;
    public $offset = FALSE;
    public $order_by = FALSE;

    function __construct() {
        parent::__construct();
    }

    function get($rows_no = FALSE) {

        if ($this->custom_select !== FALSE) {
            $this->db->select('safa_uo_contract_id');
            $this->db->select($this->custom_select);
        }

        if ($this->safa_uo_contract_id !== FALSE)
            $this->db->where('safa_uo_contracts.safa_uo_contract_id', $this->safa_uo_contract_id);

        if ($this->safa_uo_id !== FALSE)
            $this->db->where('safa_uo_contracts.safa_uo_id', $this->safa_uo_id);

        if ($this->contract_username !== FALSE)
            $this->db->where('safa_uo_contracts.contract_username', $this->contract_username);

        if ($this->contract_password !== FALSE)
            $this->db->where('safa_uo_contracts.contract_password', $this->contract_password);

        if ($this->name_ar !== FALSE)
            $this->db->where('safa_uo_contracts.name_ar', $this->name_ar);

        if ($this->name_la !== FALSE)
            $this->db->where('safa_uo_contracts.name_la', $this->name_la);


        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('safa_uo_contracts');
        if ($rows_no)
            return $query->num_rows();

        if ($this->safa_uo_contract_id)
            return $query->row();
        else
            return $query->result();
    }

    function save() {
        if ($this->safa_uo_contract_id !== FALSE)
            $this->db->set('safa_uo_contracts.safa_uo_contract_id', $this->safa_uo_contract_id);

        if ($this->safa_uo_id !== FALSE)
            $this->db->set('safa_uo_contracts.safa_uo_id', $this->safa_uo_id);

        if ($this->contract_username !== FALSE)
            $this->db->set('safa_uo_contracts.contract_username', $this->contract_username);

        if ($this->contract_password !== FALSE)
            $this->db->set('safa_uo_contracts.contract_password', $this->contract_password);

        if ($this->name_ar !== FALSE)
            $this->db->set('safa_uo_contracts.name_ar', $this->name_ar);

        if ($this->name_la !== FALSE)
            $this->db->set('safa_uo_contracts.name_la', $this->name_la);


        if ($this->safa_uo_contract_id) {
            $this->db->where('safa_uo_contracts.safa_uo_contract_id', $this->safa_uo_contract_id)->update('safa_uo_contracts');
        } else {
            $this->db->insert('safa_uo_contracts');
            return $this->db->insert_id();
        }
    }

    function delete() {

        if ($this->safa_uo_contract_id !== FALSE)
            $this->db->where('safa_uo_contracts.safa_uo_contract_id', $this->safa_uo_contract_id);

        if ($this->safa_uo_id !== FALSE)
            $this->db->where('safa_uo_contracts.safa_uo_id', $this->safa_uo_id);

        if ($this->contract_username !== FALSE)
            $this->db->where('safa_uo_contracts.contract_username', $this->contract_username);

        if ($this->contract_password !== FALSE)
            $this->db->where('safa_uo_contracts.contract_password', $this->contract_password);

        if ($this->name_ar !== FALSE)
            $this->db->where('safa_uo_contracts.name_ar', $this->name_ar);

        if ($this->name_la !== FALSE)
            $this->db->where('safa_uo_contracts.name_la', $this->name_la);




        $this->db->delete('safa_uo_contracts');
        return $this->db->affected_rows();
    }

    function search($rows_no = FALSE) {
        if ($this->custom_select !== FALSE) {
            $this->db->select('safa_uo_contract_id');
            $this->db->select($this->custom_select);
        }

        if ($this->safa_uo_contract_id !== FALSE)
            $this->db->where('safa_uo_contracts.safa_uo_contract_id', $this->safa_uo_contract_id);

        if ($this->safa_uo_id !== FALSE)
            $this->db->where('safa_uo_contracts.safa_uo_id', $this->safa_uo_id);

        if ($this->contract_username !== FALSE)
            $this->db->where('safa_uo_contracts.contract_username', $this->contract_username);

        if ($this->contract_password !== FALSE)
            $this->db->where('safa_uo_contracts.contract_password', $this->contract_password);

        if ($this->name_ar !== FALSE)
            $this->db->where('safa_uo_contracts.name_ar', $this->name_ar);

        if ($this->name_la !== FALSE)
            $this->db->where('safa_uo_contracts.name_la', $this->name_la);



        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('safa_uo_contracts');
        if ($rows_no)
            return $query->num_rows();

        return $query->result();
    }

}

/* End of file safa_uo_contracts_model.php */
/* Location: ./application/models/safa_uo_contracts_model.php */