<?php

if (!defined('BASEPATH'))
exit('No direct script access allowed');

class Safa_uos_departments_model extends CI_Model
{

	public $safa_uos_department_id = FALSE;
	public $safa_uo_id = FALSE;
	public $name_ar = FALSE;
	public $name_la = FALSE;
	public $email = FALSE;
	
	public $custom_select = FALSE;
	public $limit = FALSE;
	public $offset = FALSE;
	public $order_by = FALSE;

	function __construct()
	{
		parent::__construct();
	}

	function get($rows_no = FALSE)
	{

		if ($this->custom_select !== FALSE) {
			$this->db->select('safa_uos_department_id');
			$this->db->select($this->custom_select);
		}

		if ($this->safa_uos_department_id !== FALSE)
		$this->db->where('safa_uos_departments.safa_uos_department_id', $this->safa_uos_department_id);

		if ($this->safa_uo_id !== FALSE)
		$this->db->where('safa_uos_departments.safa_uo_id', $this->safa_uo_id);

		if ($this->name_ar !== FALSE)
		$this->db->where('safa_uos_departments.name_ar', $this->name_ar);

		if ($this->name_la !== FALSE)
		$this->db->where('safa_uos_departments.name_la', $this->name_la);

		if ($this->email !== FALSE)
		$this->db->where('safa_uos_departments.email', $this->email);


		if ($this->order_by && is_array($this->order_by))
		$this->db->order_by($this->order_by['0'], $this->order_by['1']);

		if (!$rows_no && $this->limit)
		$this->db->limit($this->limit, $this->offset);

		$query = $this->db->get('safa_uos_departments');
		if ($rows_no)
		return $query->num_rows();

		if ($this->safa_uos_department_id)
		return $query->row();
		else
		return $query->result();
	}

	function save()
	{
		if ($this->safa_uos_department_id !== FALSE)
		$this->db->set('safa_uos_departments.safa_uos_department_id', $this->safa_uos_department_id);

		if ($this->safa_uo_id !== FALSE)
		$this->db->set('safa_uos_departments.safa_uo_id', $this->safa_uo_id);

		if ($this->name_ar !== FALSE)
		$this->db->set('safa_uos_departments.name_ar', $this->name_ar);

		if ($this->name_la !== FALSE)
		$this->db->set('safa_uos_departments.name_la', $this->name_la);

		if ($this->email !== FALSE)
		$this->db->set('safa_uos_departments.email', $this->email);


		if ($this->safa_uos_department_id) {
			$this->db->where('safa_uos_departments.safa_uos_department_id', $this->safa_uos_department_id)->update('safa_uos_departments');
		} else {
			$this->db->insert('safa_uos_departments');
			return $this->db->insert_id();
		}
	}

	function delete()
	{
		if ($this->safa_uos_department_id !== FALSE)
		$this->db->where('safa_uos_departments.safa_uos_department_id', $this->safa_uos_department_id);

		if ($this->safa_uo_id !== FALSE)
		$this->db->where('safa_uos_departments.safa_uo_id', $this->safa_uo_id);

		if ($this->name_ar !== FALSE)
		$this->db->where('safa_uos_departments.name_ar', $this->name_ar);

		if ($this->name_la !== FALSE)
		$this->db->where('safa_uos_departments.name_la', $this->name_la);

		if ($this->email !== FALSE)
		$this->db->where('safa_uos_departments.email', $this->email);

		$this->db->delete('safa_uos_departments');
		return $this->db->affected_rows();
	}

}

/* End of file safa_uos_departments_model.php */
/* Location: ./application/models/safa_uos_departments_model.php */