<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class notification_model extends CI_Model {

    public $tbl_name = "erp_notification";
    public $erp_notification_id = FALSE;
    public $notification_type = FALSE;
    public $erp_importance_id = FALSE;
    public $sender_id = FALSE;
    public $erp_system_events_id = FALSE;
    public $language_id = FALSE;
    public $msg_datetime = FALSE;
    public $tags = FALSE;
    public $parent_id = FALSE;

    /**
     * Detail table fields
     */
    public $detail_erp_notification_detail_id = FALSE;
    public $detail_erp_notification_id = FALSE;
    public $detail_receiver_type = FALSE;
    public $detail_receiver_id = FALSE;
    public $custom_select = FALSE;
    public $limit = FALSE;
    public $offset = FALSE;
    public $order_by = FALSE;

    function __construct() {
        parent::__construct();
    }

    function save() {
        if ($this->erp_notification_id !== FALSE)
            $this->db->set('erp_notification.erp_notification_id', $this->erp_notification_id);

        if ($this->notification_type !== FALSE)
            $this->db->set('erp_notification.notification_type', $this->notification_type);

        if ($this->erp_importance_id !== FALSE)
            $this->db->set('erp_notification.erp_importance_id', $this->erp_importance_id);

        if ($this->sender_type_id !== FALSE)
            $this->db->set('erp_notification.sender_type_id', $this->sender_type_id);

        if ($this->sender_id !== FALSE)
            $this->db->set('erp_notification.sender_id', $this->sender_id);

        if ($this->erp_system_events_id !== FALSE)
            $this->db->set('erp_notification.erp_system_events_id', $this->erp_system_events_id);

        if ($this->language_id !== FALSE)
            $this->db->set('erp_notification.language_id', $this->language_id);

        if ($this->msg_datetime !== FALSE)
            $this->db->set('erp_notification.msg_datetime', $this->msg_datetime);

        if ($this->tags !== FALSE)
            $this->db->set('erp_notification.tags', $this->tags);

        if ($this->parent_id !== FALSE)
            $this->db->set('erp_notification.parent_id', $this->parent_id);



        if ($this->erp_notification_id) {
            $this->db->where('erp_notification.erp_notification_id', $this->erp_notification_id)->update('erp_notification');
        } else {
            $this->db->insert('erp_notification');
            return $this->db->insert_id();
        }
    }

    function saveDetails() {
        if ($this->detail_erp_notification_detail_id !== FALSE)
            $this->db->set('erp_notification_detail.erp_notification_detail_id', $this->detail_erp_notification_detail_id);

        if ($this->detail_erp_notification_id !== FALSE)
            $this->db->set('erp_notification_detail.erp_notification_id', $this->detail_erp_notification_id);

        if ($this->detail_receiver_type !== FALSE)
            $this->db->set('erp_notification_detail.receiver_type', $this->detail_receiver_type);

        if ($this->detail_receiver_id !== FALSE)
            $this->db->set('erp_notification_detail.receiver_id', $this->detail_receiver_id);


        if ($this->detail_erp_notification_detail_id) {
            $this->db->where('erp_notification_detail.erp_notification_detail_id', $this->detail_erp_notification_detail_id)->update('erp_notification_detail');
        } else {
            $this->db->insert('erp_notification_detail');
            return $this->db->insert_id();
        }
    }

    function getByReceiverTypeAndReceiverIdAndUserId($receiver_type = false, $receiver_id = false, $user_id = false) {

        $this->db->select('erp_notification.*,erp_notification_detail.*,erp_notification_user_seen.mark_as_read,erp_notification_user_seen.mark_as_important');
        $this->db->from('erp_notification');
        $this->db->join('erp_notification_detail', 'erp_notification.erp_notification_id = erp_notification_detail.erp_notification_id');
        $this->db->join('erp_notification_user_seen', 'erp_notification_detail.erp_notification_detail_id = erp_notification_user_seen.erp_notification_detail_id', 'left');

        $this->db->where('erp_notification.notification_type', 'automatic');
        $this->db->where('(erp_notification_user_seen.is_hide IS NULL or erp_notification_user_seen.is_hide=0)');

        if ($receiver_type !== false)
            $this->db->where('erp_notification_detail.receiver_type', $receiver_type);

        if ($receiver_id !== false)
            $this->db->where('erp_notification_detail.receiver_id', $receiver_id);

        if ($user_id !== false)
            $this->db->where("(erp_notification_user_seen.user_id=$user_id or erp_notification_user_seen.user_id is null)");


        $this->db->order_by('erp_notification.erp_notification_id', 'desc');

        $this->db->limit(10);


        $query = $this->db->get();

        $query_text = $this->db->last_query();

        return $query->result();
    }

    function getUnreadedByReceiverTypeAndReceiverIdAndUserId($receiver_type = false, $receiver_id = false, $user_id = false, $rows_no = false) {
        $this->db->select('count(`erp_notification`.`erp_notification_id`) as unreaded_notifications_count')->from('erp_notification');
        $this->db->join('erp_notification_detail', 'erp_notification.erp_notification_id = erp_notification_detail.erp_notification_id');
        $this->db->join('erp_notification_user_seen', 'erp_notification_detail.erp_notification_detail_id = erp_notification_user_seen.erp_notification_detail_id', 'left');

        $this->db->where("`erp_notification`.`erp_notification_id` NOT IN (SELECT `erp_notification`.`erp_notification_id` FROM `erp_notification`
        inner join `erp_notification_detail` on `erp_notification`.`erp_notification_id` = `erp_notification_detail`.`erp_notification_id`
        left outer join `erp_notification_user_seen` on `erp_notification_detail`.`erp_notification_detail_id` = `erp_notification_user_seen`.`erp_notification_detail_id`
        where `erp_notification`.`notification_type`='automatic' and
        `erp_notification_detail`.`receiver_type`='$receiver_type' and
        `erp_notification_detail`.`receiver_id`='$receiver_id' and
        `erp_notification_user_seen`.`user_id`='$user_id' and 
        (erp_notification_user_seen.mark_as_read =1 or erp_notification_user_seen.is_hide=1) 
        )", NULL, FALSE);

        $this->db->where('erp_notification_detail.receiver_type', $receiver_type);
        $this->db->where('erp_notification_detail.receiver_id', $receiver_id);
        $this->db->where("(erp_notification_user_seen.user_id=$user_id or erp_notification_user_seen.user_id is null)");


        $query = $this->db->get();

        //$query_text=$this->db->last_query();

        return $query->result();
    }

    function getByReceiverTypeAndReceiverIdAndUserIdForDisplay($receiver_type = false, $receiver_id = false, $user_id = false, $rows_no = false, $mark_as_important = false, $is_hide = false) {

        $this->db->select('erp_notification.*,erp_notification_detail.*,erp_notification_user_seen.mark_as_read,erp_notification_user_seen.mark_as_important,erp_notification_user_seen.is_hide ');
        $this->db->from('erp_notification');
        $this->db->join('erp_notification_detail', 'erp_notification.erp_notification_id = erp_notification_detail.erp_notification_id');
        $this->db->join('erp_notification_user_seen', 'erp_notification_detail.erp_notification_detail_id = erp_notification_user_seen.erp_notification_detail_id', 'left');

        $this->db->where('erp_notification.notification_type', 'automatic');
        if ($is_hide == false) {
            $this->db->where('(erp_notification_user_seen.is_hide IS NULL or erp_notification_user_seen.is_hide=0)');
        } else {
            $this->db->where('erp_notification_user_seen.is_hide', $is_hide);
        }

        if ($receiver_type !== false)
            $this->db->where('erp_notification_detail.receiver_type', $receiver_type);

        if ($receiver_id !== false)
            $this->db->where('erp_notification_detail.receiver_id', $receiver_id);

        if ($user_id !== false)
            $this->db->where('erp_notification_user_seen.user_id', $user_id);

        if ($mark_as_important !== false)
            $this->db->where('erp_notification_user_seen.mark_as_important', $mark_as_important);

        $this->db->order_by('erp_notification.erp_notification_id', 'desc');


        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get();

        //$query_text=$this->db->last_query();
        //echo $query_text; exit;

        if ($rows_no)
            return $query->num_rows();

        return $query->result();
    }

    function getByDetailId($erp_notification_detail_id = false, $rows_no = false) {

        $this->db->select('erp_notification.*, erp_notification_detail.*');
        $this->db->from('erp_notification');
        $this->db->join('erp_notification_detail', 'erp_notification.erp_notification_id = erp_notification_detail.erp_notification_id');

        if ($erp_notification_detail_id !== false)
            $this->db->where('erp_notification_detail.erp_notification_detail_id', $erp_notification_detail_id);

        $query = $this->db->get();
        return $query->row();
    }

    function getServerDatetime() {
        $this->db->select('NOW() as current_datetime');
        $this->db->from('erp_notification');
        $query = $this->db->get();
        return $query->result();
    }

}

/* End of file countries_model.php */
/* Location: ./application/models/countries_model.php */