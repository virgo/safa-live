<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Erp_companies_hotels_rooms_model extends CI_Model {

    public $erp_companies_hotels_rooms_id = FALSE;
    public $erp_companies_hotels_id = FALSE;
    public $section = FALSE;
    public $rooms_count = FALSE;
    public $beds_count = FALSE;
    public $available_from = FALSE;
    public $available_to = FALSE;
    public $erp_hotel_floor_id = FALSE;
    public $custom_select = FALSE;
    public $limit = FALSE;
    public $offset = FALSE;
    public $order_by = FALSE;

    function __construct() {
        parent::__construct();
    }

    function get($rows_no = FALSE) {

        if ($this->custom_select !== FALSE) {
            $this->db->select('erp_companies_hotels_rooms_id');
            $this->db->select($this->custom_select);
        }

        if ($this->erp_companies_hotels_rooms_id !== FALSE)
            $this->db->where('erp_companies_hotels_rooms.erp_companies_hotels_rooms_id', $this->erp_companies_hotels_rooms_id);

        if ($this->erp_companies_hotels_id !== FALSE)
            $this->db->where('erp_companies_hotels_rooms.erp_companies_hotels_id', $this->erp_companies_hotels_id);

        if ($this->section !== FALSE)
            $this->db->where('erp_companies_hotels_rooms.section', $this->section);

        if ($this->rooms_count !== FALSE)
            $this->db->where('erp_companies_hotels_rooms.rooms_count', $this->rooms_count);

        if ($this->beds_count !== FALSE)
            $this->db->where('erp_companies_hotels_rooms.beds_count', $this->beds_count);

        if ($this->available_from !== FALSE)
            $this->db->where('erp_companies_hotels_rooms.available_from', $this->available_from);

        if ($this->available_to !== FALSE)
            $this->db->where('erp_companies_hotels_rooms.available_to', $this->available_to);

        if ($this->erp_hotel_floor_id !== FALSE)
            $this->db->where('erp_companies_hotels_rooms.erp_hotel_floor_id', $this->erp_hotel_floor_id);


        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('erp_companies_hotels_rooms');
        if ($rows_no)
            return $query->num_rows();

        if ($this->erp_companies_hotels_rooms_id)
            return $query->row();
        else
            return $query->result();
    }

    function save() {
        if ($this->erp_companies_hotels_rooms_id !== FALSE)
            $this->db->set('erp_companies_hotels_rooms.erp_companies_hotels_rooms_id', $this->erp_companies_hotels_rooms_id);

        if ($this->erp_companies_hotels_id !== FALSE)
            $this->db->set('erp_companies_hotels_rooms.erp_companies_hotels_id', $this->erp_companies_hotels_id);

        if ($this->section !== FALSE)
            $this->db->set('erp_companies_hotels_rooms.section', $this->section);

        if ($this->rooms_count !== FALSE)
            $this->db->set('erp_companies_hotels_rooms.rooms_count', $this->rooms_count);

        if ($this->beds_count !== FALSE)
            $this->db->set('erp_companies_hotels_rooms.beds_count', $this->beds_count);

        if ($this->available_from !== FALSE)
            $this->db->set('erp_companies_hotels_rooms.available_from', $this->available_from);

        if ($this->available_to !== FALSE)
            $this->db->set('erp_companies_hotels_rooms.available_to', $this->available_to);

        if ($this->erp_hotel_floor_id !== FALSE)
            $this->db->set('erp_companies_hotels_rooms.erp_hotel_floor_id', $this->erp_hotel_floor_id);



        if ($this->erp_companies_hotels_rooms_id) {
            $this->db->where('erp_companies_hotels_rooms.erp_companies_hotels_rooms_id', $this->erp_companies_hotels_rooms_id)->update('erp_companies_hotels_rooms');
        } else {
            $this->db->insert('erp_companies_hotels_rooms');
            return $this->db->insert_id();
        }
    }

    function delete() {
        if ($this->erp_companies_hotels_rooms_id !== FALSE)
            $this->db->where('erp_companies_hotels_rooms.erp_companies_hotels_rooms_id', $this->erp_companies_hotels_rooms_id);

        if ($this->erp_companies_hotels_id !== FALSE)
            $this->db->where('erp_companies_hotels_rooms.erp_companies_hotels_id', $this->erp_companies_hotels_id);

        if ($this->section !== FALSE)
            $this->db->where('erp_companies_hotels_rooms.section', $this->section);

        if ($this->rooms_count !== FALSE)
            $this->db->where('erp_companies_hotels_rooms.rooms_count', $this->rooms_count);

        if ($this->beds_count !== FALSE)
            $this->db->where('erp_companies_hotels_rooms.beds_count', $this->beds_count);

        if ($this->available_from !== FALSE)
            $this->db->where('erp_companies_hotels_rooms.available_from', $this->available_from);

        if ($this->available_to !== FALSE)
            $this->db->where('erp_companies_hotels_rooms.available_to', $this->available_to);

        if ($this->erp_hotel_floor_id !== FALSE)
            $this->db->where('erp_companies_hotels_rooms.erp_hotel_floor_id', $this->erp_hotel_floor_id);



        $this->db->delete('erp_companies_hotels_rooms');
        return $this->db->affected_rows();
    }

    function search($rows_no = FALSE) {
        if ($this->custom_select !== FALSE) {
            $this->db->select('erp_companies_hotels_rooms_id');
            $this->db->select($this->custom_select);
        }

        if ($this->erp_companies_hotels_rooms_id !== FALSE)
            $this->db->where('erp_companies_hotels_rooms.erp_companies_hotels_rooms_id', $this->erp_companies_hotels_rooms_id);

        if ($this->erp_companies_hotels_id !== FALSE)
            $this->db->where('erp_companies_hotels_rooms.erp_companies_hotels_id', $this->erp_companies_hotels_id);

        if ($this->section !== FALSE)
            $this->db->where('erp_companies_hotels_rooms.section', $this->section);

        if ($this->rooms_count !== FALSE)
            $this->db->where('erp_companies_hotels_rooms.rooms_count', $this->rooms_count);

        if ($this->beds_count !== FALSE)
            $this->db->where('erp_companies_hotels_rooms.beds_count', $this->beds_count);

        if ($this->available_from !== FALSE)
            $this->db->where('erp_companies_hotels_rooms.available_from', $this->available_from);

        if ($this->available_to !== FALSE)
            $this->db->where('erp_companies_hotels_rooms.available_to', $this->available_to);

        if ($this->erp_hotel_floor_id !== FALSE)
            $this->db->where('erp_companies_hotels_rooms.erp_hotel_floor_id', $this->erp_hotel_floor_id);


        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('erp_companies_hotels_rooms');
        if ($rows_no)
            return $query->num_rows();

        return $query->result();
    }

    function count_details() {
        if ($this->erp_companies_hotels_id != false)
            $this->db->where('erp_companies_hotels_id', $this->erp_companies_hotels_id);
        return $this->db->from('erp_companies_hotels_rooms')->count_all_results();
    }

}

/* End of file erp_companies_hotels_rooms_model.php */
/* Location: ./application/models/erp_companies_hotels_rooms_model.php */