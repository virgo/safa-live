<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Contracts_model extends CI_Model {

    public $safa_uo_contract_id = FALSE;
    public $safa_uo_id = FALSE;
    public $contract_username = FALSE;
    public $contract_password = FALSE;
    public $name_ar = FALSE;
    public $name_la = FALSE;
    public $address = FALSE;
    public $phone = FALSE;
    public $ksa_address = FALSE;
    public $iata = FALSE;
    public $ksa_phone = FALSE;
    public $agency_symbol = FALSE;
    public $agency_name = FALSE;
    public $email = FALSE;
    // By Gouda --------------
    public $safa_uo_contracts_status_id = FALSE;
    public $safa_contract_phases_id_current = FALSE;
    public $safa_contract_phases_id_next = FALSE;
    public $uo_contracts_eas_name_ar = FALSE;
    public $uo_contracts_eas_name_la = FALSE;
    public $safa_ea_id = FALSE;
    public $uo_contracts_eas_disabled = FALSE;
    public $uo_contracts_eas_safa_uo_contract_ea_id = FALSE;
    public $uo_contracts_eas_is_original_owner = FALSE;
    //------------------------

    public $country_id = FALSE;
    public $fax = FALSE;
    public $city_id = FALSE;
    public $responsible_name = FALSE;
    public $nationality_id = FALSE;
    public $responsible_phone = FALSE;
    public $responsible_email = FALSE;
    public $responsible_passport_photo_path = FALSE;
    public $notes = FALSE;
    public $custom_select = FALSE;
    public $limit = FALSE;
    public $offset = FALSE;
    public $order_by = FALSE;
    public $by_eas_id = FALSE;
    public $uasp_username = FALSE;
    public $uasp_password = FALSE;
    public $uasp_eacode = FALSE;
    // By Gouda --------------
    public $sending_number = FALSE;
    public $sending_date = FALSE;
    public $erp_sending_companies_id = FALSE;
    public $sending_others_text = FALSE;

    function __construct() {
        parent::__construct();
        // if the uo_has loged in// 
    }

    function get($rows_no = FALSE) {

        if ($this->custom_select !== FALSE) {
            $this->db->select('safa_uo_contract_id');
            $this->db->select($this->custom_select);
        }

        if ($this->safa_uo_contract_id !== FALSE)
            $this->db->where('safa_uo_contracts.safa_uo_contract_id', $this->safa_uo_contract_id);

        if ($this->safa_uo_id !== FALSE)
            $this->db->where('safa_uo_contracts.safa_uo_id', $this->safa_uo_id);

        if ($this->contract_username !== FALSE)
            $this->db->where('safa_uo_contracts.contract_username', $this->contract_username);

        if ($this->contract_password !== FALSE)
            $this->db->where('safa_uo_contracts.contract_password', $this->contract_password);

        if ($this->name_ar !== FALSE)
            $this->db->like('safa_uo_contracts.name_ar', $this->name_ar, 'both');

        if ($this->name_la !== FALSE)
            $this->db->like('safa_uo_contracts.name_la', $this->name_la, 'both');

        if ($this->address !== FALSE)
            $this->db->where('safa_uo_contracts.address', $this->address);

        if ($this->phone !== FALSE)
            $this->db->where('safa_uo_contracts.phone', $this->phone);

        if ($this->ksa_address !== FALSE)
            $this->db->where('safa_uo_contracts.ksa_address', $this->ksa_address);

        if ($this->iata !== FALSE)
            $this->db->where('safa_uo_contracts.iata', $this->iata);

        if ($this->ksa_phone !== FALSE)
            $this->db->where('safa_uo_contracts.ksa_phone', $this->ksa_phone);

        if ($this->agency_symbol !== FALSE)
            $this->db->where('safa_uo_contracts.agency_symbol', $this->agency_symbol);

        if ($this->agency_name !== FALSE)
            $this->db->where('safa_uo_contracts.agency_name', $this->agency_name);

        if ($this->email !== FALSE)
            $this->db->where('safa_uo_contracts.email', $this->email);

        //By Gouda -----------------------    
        if ($this->safa_uo_contracts_status_id !== FALSE)
            $this->db->where('safa_uo_contracts.safa_uo_contracts_status_id', $this->safa_uo_contracts_status_id);

        if ($this->safa_contract_phases_id_current !== FALSE)
            $this->db->where('safa_uo_contracts.safa_contract_phases_id_current', $this->safa_contract_phases_id_current);

        if ($this->safa_contract_phases_id_next !== FALSE)
            $this->db->where('safa_uo_contracts.safa_contract_phases_id_next', $this->safa_contract_phases_id_next);
        //--------------------------------    

        if ($this->country_id !== FALSE)
            $this->db->where('safa_uo_contracts.country_id', $this->country_id);

        if ($this->fax !== FALSE)
            $this->db->where('safa_uo_contracts.fax', $this->fax);

        if ($this->city_id !== FALSE)
            $this->db->where('safa_uo_contracts.city_id', $this->city_id);

        if ($this->responsible_name !== FALSE)
            $this->db->where('safa_uo_contracts.responsible_name', $this->responsible_name);

        if ($this->nationality_id !== FALSE)
            $this->db->where('safa_uo_contracts.nationality_id', $this->nationality_id);

        if ($this->responsible_phone !== FALSE)
            $this->db->where('safa_uo_contracts.responsible_phone', $this->responsible_phone);

        if ($this->responsible_email !== FALSE)
            $this->db->where('safa_uo_contracts.responsible_email', $this->responsible_email);

        if ($this->responsible_passport_photo_path !== FALSE)
            $this->db->where('safa_uo_contracts.responsible_passport_photo_path', $this->responsible_passport_photo_path);

        if ($this->notes !== FALSE)
            $this->db->where('safa_uo_contracts.notes', $this->notes);


        if ($this->sending_number !== FALSE)
            $this->db->where('safa_uo_contracts.sending_number', $this->sending_number);
        if ($this->sending_date !== FALSE)
            $this->db->where('safa_uo_contracts.sending_date', $this->sending_date);
        if ($this->erp_sending_companies_id !== FALSE)
            $this->db->where('safa_uo_contracts.erp_sending_companies_id', $this->erp_sending_companies_id);
        if ($this->sending_others_text !== FALSE)
            $this->db->where('safa_uo_contracts.sending_others_text', $this->sending_others_text);

            
        $this->db->select('safa_uo_contracts.*, erp_countries.'.name().' as country_name', false);
        $this->db->from('safa_uo_contracts');
        $this->db->join('erp_countries', 'erp_countries.erp_country_id=safa_uo_contracts.country_id', 'left');
           
            

        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get();

        $query_text = $this->db->last_query();
        //echo $query_text; exit;

        if ($rows_no)
            return $query->num_rows();

        if ($this->safa_uo_contract_id)
            return $query->row();
        else
            return $query->result();
    }

    function save() {

        if ($this->safa_uo_contract_id !== FALSE)
            $this->db->set('safa_uo_contracts.safa_uo_contract_id', $this->safa_uo_contract_id);

        if ($this->safa_uo_id !== FALSE)
            $this->db->set('safa_uo_contracts.safa_uo_id', $this->safa_uo_id);

        if ($this->contract_username !== FALSE)
            $this->db->set('safa_uo_contracts.contract_username', $this->contract_username);

        if ($this->contract_password !== FALSE)
            $this->db->set('safa_uo_contracts.contract_password', $this->contract_password);

        if ($this->uasp_username !== FALSE)
            $this->db->set('safa_uo_contracts.uasp_username', $this->uasp_username);
        if ($this->uasp_password !== FALSE)
            $this->db->set('safa_uo_contracts.uasp_password', $this->uasp_password);
        if ($this->uasp_eacode !== FALSE)
            $this->db->set('safa_uo_contracts.uasp_eacode', $this->uasp_eacode);

        if ($this->name_ar !== FALSE)
            $this->db->set('safa_uo_contracts.name_ar', $this->name_ar);

        if ($this->name_la !== FALSE)
            $this->db->set('safa_uo_contracts.name_la', $this->name_la);

        if ($this->address !== FALSE)
            $this->db->set('safa_uo_contracts.address', $this->address);

        if ($this->phone !== FALSE)
            $this->db->set('safa_uo_contracts.phone', $this->phone);

        if ($this->ksa_address !== FALSE)
            $this->db->set('safa_uo_contracts.ksa_address', $this->ksa_address);

        if ($this->iata !== FALSE)
            $this->db->set('safa_uo_contracts.iata', $this->iata);

        if ($this->ksa_phone !== FALSE)
            $this->db->set('safa_uo_contracts.ksa_phone', $this->ksa_phone);

        if ($this->agency_symbol !== FALSE)
            $this->db->set('safa_uo_contracts.agency_symbol', $this->agency_symbol);

        if ($this->agency_name !== FALSE)
            $this->db->set('safa_uo_contracts.agency_name', $this->agency_name);

        if ($this->email !== FALSE)
            $this->db->set('safa_uo_contracts.email', $this->email);

        //By Gouda --------------------    
        if ($this->safa_uo_contracts_status_id !== FALSE)
            $this->db->set('safa_uo_contracts.safa_uo_contracts_status_id', $this->safa_uo_contracts_status_id);

        if ($this->safa_contract_phases_id_current !== FALSE)
            $this->db->set('safa_uo_contracts.safa_contract_phases_id_current', $this->safa_contract_phases_id_current);

        if ($this->safa_contract_phases_id_next !== FALSE)
            $this->db->set('safa_uo_contracts.safa_contract_phases_id_next', $this->safa_contract_phases_id_next);

        //----------------------------

        if ($this->country_id !== FALSE)
            $this->db->set('safa_uo_contracts.country_id', $this->country_id);

        if ($this->fax !== FALSE)
            $this->db->set('safa_uo_contracts.fax', $this->fax);

        if ($this->city_id !== FALSE)
            $this->db->set('safa_uo_contracts.city_id', $this->city_id);

        if ($this->responsible_name !== FALSE)
            $this->db->set('safa_uo_contracts.responsible_name', $this->responsible_name);

        if ($this->nationality_id !== FALSE)
            $this->db->set('safa_uo_contracts.nationality_id', $this->nationality_id);

        if ($this->responsible_phone !== FALSE)
            $this->db->set('safa_uo_contracts.responsible_phone', $this->responsible_phone);

        if ($this->responsible_email !== FALSE)
            $this->db->set('safa_uo_contracts.responsible_email', $this->responsible_email);

        if ($this->responsible_passport_photo_path !== FALSE)
            $this->db->set('safa_uo_contracts.responsible_passport_photo_path', $this->responsible_passport_photo_path);

        if ($this->notes !== FALSE)
            $this->db->set('safa_uo_contracts.notes', $this->notes);


        // By Gouda.
        if ($this->sending_number !== FALSE)
            $this->db->set('safa_uo_contracts.sending_number', $this->sending_number);
        if ($this->sending_date !== FALSE)
            $this->db->set('safa_uo_contracts.sending_date', $this->sending_date);
        if ($this->erp_sending_companies_id !== FALSE)
            $this->db->set('safa_uo_contracts.erp_sending_companies_id', $this->erp_sending_companies_id);
        if ($this->sending_others_text !== FALSE)
            $this->db->set('safa_uo_contracts.sending_others_text', $this->sending_others_text);

        if ($this->safa_uo_contract_id) {
            $this->db->where('safa_uo_contracts.safa_uo_contract_id', $this->safa_uo_contract_id)->update('safa_uo_contracts');
        } else {
            $this->db->insert('safa_uo_contracts');
            return $this->db->insert_id();
        }
    }

    function delete() {
        if ($this->safa_uo_contract_id !== FALSE)
            $this->db->where('safa_uo_contracts.safa_uo_contract_id', $this->safa_uo_contract_id);

        if ($this->safa_uo_id !== FALSE)
            $this->db->where('safa_uo_contracts.safa_uo_id', $this->safa_uo_id);

        if ($this->contract_username !== FALSE)
            $this->db->where('safa_uo_contracts.contract_username', $this->contract_username);

        if ($this->contract_password !== FALSE)
            $this->db->where('safa_uo_contracts.contract_password', $this->contract_password);

        if ($this->name_ar !== FALSE)
            $this->db->where('safa_uo_contracts.name_ar', $this->name_ar);

        if ($this->name_la !== FALSE)
            $this->db->where('safa_uo_contracts.name_la', $this->name_la);

        if ($this->address !== FALSE)
            $this->db->where('safa_uo_contracts.address', $this->address);

        if ($this->phone !== FALSE)
            $this->db->where('safa_uo_contracts.phone', $this->phone);

        if ($this->ksa_address !== FALSE)
            $this->db->where('safa_uo_contracts.ksa_address', $this->ksa_address);

        if ($this->iata !== FALSE)
            $this->db->where('safa_uo_contracts.iata', $this->iata);

        if ($this->ksa_phone !== FALSE)
            $this->db->where('safa_uo_contracts.ksa_phone', $this->ksa_phone);

        if ($this->agency_symbol !== FALSE)
            $this->db->where('safa_uo_contracts.agency_symbol', $this->agency_symbol);

        if ($this->agency_name !== FALSE)
            $this->db->where('safa_uo_contracts.agency_name', $this->agency_name);

        if ($this->email !== FALSE)
            $this->db->where('safa_uo_contracts.email', $this->email);

        //By Gouda -----------------------    
        if ($this->safa_uo_contracts_status_id !== FALSE)
            $this->db->where('safa_uo_contracts.safa_uo_contracts_status_id', $this->safa_uo_contracts_status_id);

        if ($this->safa_contract_phases_id_current !== FALSE)
            $this->db->where('safa_uo_contracts.safa_contract_phases_id_current', $this->safa_contract_phases_id_current);

        if ($this->safa_contract_phases_id_next !== FALSE)
            $this->db->where('safa_uo_contracts.safa_contract_phases_id_next', $this->safa_contract_phases_id_next);

        //--------------------------------    


        if ($this->country_id !== FALSE)
            $this->db->where('safa_uo_contracts.country_id', $this->country_id);

        if ($this->fax !== FALSE)
            $this->db->where('safa_uo_contracts.fax', $this->fax);

        if ($this->city_id !== FALSE)
            $this->db->where('safa_uo_contracts.city_id', $this->city_id);

        if ($this->responsible_name !== FALSE)
            $this->db->where('safa_uo_contracts.responsible_name', $this->responsible_name);

        if ($this->nationality_id !== FALSE)
            $this->db->where('safa_uo_contracts.nationality_id', $this->nationality_id);

        if ($this->responsible_phone !== FALSE)
            $this->db->where('safa_uo_contracts.responsible_phone', $this->responsible_phone);

        if ($this->responsible_email !== FALSE)
            $this->db->where('safa_uo_contracts.responsible_email', $this->responsible_email);

        if ($this->responsible_passport_photo_path !== FALSE)
            $this->db->where('safa_uo_contracts.responsible_passport_photo_path', $this->responsible_passport_photo_path);


        if ($this->notes !== FALSE)
            $this->db->where('safa_uo_contracts.notes', $this->notes);



        $this->db->delete('safa_uo_contracts');
        return $this->db->affected_rows();
    }

    function search($rows_no = FALSE) {// used for the use of contracts when we get the external agents uos //
        if ($this->custom_select !== FALSE) {
            $this->db->select('safa_uo_contract_id');
            $this->db->select($this->custom_select);
        }

        if ($this->safa_uo_contract_id !== FALSE)
            $this->db->where('safa_uo_contracts.safa_uo_contract_id', $this->safa_uo_contract_id);

        if ($this->safa_uo_id !== FALSE)
            $this->db->where('safa_uo_contracts.safa_uo_id', $this->safa_uo_id);

        if ($this->contract_username !== FALSE)
            $this->db->where('safa_uo_contracts.contract_username', $this->contract_username);

        if ($this->contract_password !== FALSE)
            $this->db->where('safa_uo_contracts.contract_password', $this->contract_password);

        if ($this->name_ar !== FALSE)
            $this->db->like('safa_uo_contracts.name_ar', $this->name_ar, 'both');

        if ($this->name_la !== FALSE)
            $this->db->like('safa_uo_contracts.name_la', $this->name_la, 'both');

        if ($this->address !== FALSE)
            $this->db->like('safa_uo_contracts.address', $this->address, 'both');

        if ($this->phone !== FALSE)
            $this->db->like('safa_uo_contracts.phone', $this->phone);

        if ($this->ksa_address !== FALSE)
            $this->db->like('safa_uo_contracts.ksa_address', $this->ksa_address);

        if ($this->iata !== FALSE)
            $this->db->like('safa_uo_contracts.iata', $this->iata);

        if ($this->ksa_phone !== FALSE)
            $this->db->like('safa_uo_contracts.ksa_phone', $this->ksa_phone);

        if ($this->agency_symbol !== FALSE)
            $this->db->like('safa_uo_contracts.agency_symbol', $this->agency_symbol);

        if ($this->agency_name !== FALSE)
            $this->db->like('safa_uo_contracts.agency_name', $this->agency_name);

        if ($this->email !== FALSE)
            $this->db->like('safa_uo_contracts.email', $this->email);

        //By Gouda -----------------------    
        if ($this->safa_uo_contracts_status_id !== FALSE)
            $this->db->where('safa_uo_contracts.safa_uo_contracts_status_id', $this->safa_uo_contracts_status_id);

        if ($this->safa_contract_phases_id_current !== FALSE)
            $this->db->where('safa_uo_contracts.safa_contract_phases_id_current', $this->safa_contract_phases_id_current);

        if ($this->safa_contract_phases_id_next !== FALSE)
            $this->db->where('safa_uo_contracts.safa_contract_phases_id_next', $this->safa_contract_phases_id_next);


        if ($this->safa_ea_id !== FALSE)
            $this->db->where('safa_eas.safa_ea_id', $this->safa_ea_id);

        //--------------------------------    


        if ($this->country_id !== FALSE)
            $this->db->like('safa_uo_contracts.country_id', $this->country_id);

        if ($this->fax !== FALSE)
            $this->db->like('safa_uo_contracts.fax', $this->fax);

        if ($this->city_id !== FALSE)
            $this->db->like('safa_uo_contracts.city_id', $this->city_id);

        if ($this->responsible_name !== FALSE)
            $this->db->like('safa_uo_contracts.responsible_name', $this->responsible_name);

        if ($this->nationality_id !== FALSE)
            $this->db->like('safa_uo_contracts.nationality_id', $this->nationality_id);

        if ($this->responsible_phone !== FALSE)
            $this->db->like('safa_uo_contracts.responsible_phone', $this->responsible_phone);

        if ($this->responsible_email !== FALSE)
            $this->db->where('safa_uo_contracts.responsible_email', $this->responsible_email);

        if ($this->responsible_passport_photo_path !== FALSE)
            $this->db->where('safa_uo_contracts.responsible_passport_photo_path', $this->responsible_passport_photo_path);

        if ($this->uo_contracts_eas_disabled !== FALSE)
            $this->db->where('safa_uo_contracts_eas.disabled', $this->uo_contracts_eas_disabled);



        if ($this->notes !== FALSE)
            $this->db->like('safa_uo_contracts.notes', $this->notes);
        /*         * ************************************************************** */
        $name = name();
        $uo_name = '';
        $ea_name = '';
        $contract_name = 'safa_uo_contracts.' . $name;
        if ($this->safa_uo_id == false)
            $uo_name = 'safa_uos.' . $name . ", '\t|\t' ,";
        if ($this->by_eas_id == false)
            $ea_name = 'safa_eas.' . $name . ",'\t|\t',";
        else
            $this->db->where('safa_uo_contracts_eas.safa_ea_id', $this->by_eas_id);
        /*         * implementing  the num of contracts********************************* */
        $contact_contract_name = $uo_name . $ea_name . $contract_name;
        $this->db->select('CONCAT(' . $contact_contract_name . ') as contract_name,safa_uos.' . name() . ' as safa_uos_name, safa_uo_contracts.phone,
        safa_uo_contracts.address,safa_uo_contracts.safa_uo_contract_id, safa_uo_contracts.name_ar, 
        safa_uo_contracts.name_la, safa_uo_contracts_status.name_ar as safa_uo_contracts_status_name_ar, 
        safa_uo_contracts_status.name_la as safa_uo_contracts_status_name_la, 
        safa_contract_phases_current.name_ar as safa_contract_phases_name_ar_current, 
        safa_contract_phases_current.name_la as safa_contract_phases_name_la_current, 
        safa_contract_phases_next.name_ar as safa_contract_phases_name_ar_next, 
        safa_contract_phases_next.name_la as safa_contract_phases_name_la_next , 
        safa_uo_contracts.safa_contract_phases_id_current, 
        safa_contract_phases_next.safa_contract_phases_id as safa_contract_phases_id_next, 
        safa_eas.safa_ea_id, safa_uo_contracts.safa_uo_id, 
        erp_countries.name_ar as country_name_ar, erp_countries.name_la as country_name_la, 
        safa_uo_contracts_settings.services_buy_other_hotel, safa_uo_contracts_settings.services_buy_uo_hotels, safa_uo_contracts_settings.services_buy_uo_transporter, 
        safa_uo_contracts_eas.safa_uo_contract_ea_id, safa_uo_contracts_eas.name_ar as safa_uo_contracts_eas_name_ar, safa_uo_contracts_eas.name_la as safa_uo_contracts_eas_name_la,
        safa_uo_contracts.sending_number, safa_uo_contracts.sending_date, safa_uo_contracts.erp_sending_companies_id, safa_uo_contracts.sending_others_text, erp_sending_companies.' . name() . ' as erp_sending_companies_name ', false);
        $this->db->from('safa_uo_contracts');
        $this->db->join('safa_uos', 'safa_uos.safa_uo_id=safa_uo_contracts.safa_uo_id', 'left');
        $this->db->join('safa_uo_contracts_eas', 'safa_uo_contracts_eas.safa_uo_contract_id=safa_uo_contracts.safa_uo_contract_id', 'left');
        $this->db->join('safa_eas', 'safa_uo_contracts_eas.safa_ea_id=safa_eas.safa_ea_id', 'left');
        $this->db->join('safa_uo_contracts_status', 'safa_uo_contracts.safa_uo_contracts_status_id=safa_uo_contracts_status.safa_uo_contracts_status_id', 'left');
        $this->db->join('safa_contract_phases as safa_contract_phases_current', 'safa_uo_contracts.safa_contract_phases_id_current=safa_contract_phases_current.safa_contract_phases_id', 'left');
        $this->db->join('safa_contract_phases as safa_contract_phases_next', 'safa_uo_contracts.safa_contract_phases_id_next=safa_contract_phases_next.safa_contract_phases_id', 'left');
        $this->db->join('erp_countries', 'erp_countries.erp_country_id=safa_uo_contracts.country_id', 'left');
        $this->db->join('safa_uo_contracts_settings', 'safa_uo_contracts_settings.safa_uo_contract_id=safa_uo_contracts.safa_uo_contract_id', 'left');
        $this->db->join('erp_sending_companies', 'erp_sending_companies.erp_sending_companies_id=safa_uo_contracts.erp_sending_companies_id', 'left');

        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);
        $query = $this->db->get();

        if ($rows_no)
            return $query->num_rows();

        return $query->result();
    }

    function add_hotels() {
        /* add add hotels to ea */
        $native_query = "insert into safa_uo_contracts_hotels(erp_hotel_id,safa_uo_contract_id)
            select erp_hotels.erp_hotel_id," . $this->db->escape($this->safa_uo_contract_id) . "
               from erp_hotels";
        $this->db->query($native_query);
        return $this->db->affected_rows();
    }

    function add_tourismplaces() {
        $native_query = "insert into safa_uo_contracts_tourismplaces(safa_tourismplace_id,safa_uo_contract_id)
            (select safa_tourismplaces.safa_tourismplace_id," . $this->db->escape($this->safa_uo_contract_id) . "
            from safa_tourismplaces)";
        $this->db->query($native_query);
        return $this->db->affected_rows();
    }

    function add_itos() {
        $native_query = "insert into safa_uo_contracts_itos(safa_ito_id,safa_uo_contract_id)
            select safa_itos.safa_ito_id," . $this->db->escape($this->safa_uo_contract_id) . "
               from safa_itos";
        $this->db->query($native_query);
        return $this->db->affected_rows();
    }

    function add_ea_itos() {
        $native_query = "insert into safa_ea_itos(safa_ito_id,safa_uo_contract_id)
            select safa_itos.safa_ito_id," . $this->db->escape($this->safa_uo_contract_id) . "
           from safa_itos";
        $this->db->query($native_query);
        return $this->db->affected_rows();
    }

    function add_defulat_umrahpackage() {
        $native_query = "insert into safa_packages_uo_contracts(safa_package_id,safa_uo_contract_id)
            select safa_packages.safa_package_id," . $this->db->escape($this->safa_uo_contract_id) . "
               from safa_packages where safa_packages.erp_company_type_id=2 and safa_packages.erp_company_id=" . $this->safa_uo_id;
        $this->db->query($native_query);
        return $this->db->affected_rows();
    }

    function check_contract_existance($contract_id) {
        //to do_be_contained// 
    }

//    function get_external_agents($uo_id=FALSE){ /*notused */
//        if($uo_id==false)
//              $uo_id=$this->safa_uo_id;
//      $this->db->select('safa_eas.safa_ea_id,safa_eas.'.name());
//      $this->db->from('safa_eas');
//      $this->db->join('safa_uo_contracts_eas','safa_uo_contracts_eas.safa_ea_id=safa_eas.safa_ea_id');
//      $this->db->join('safa_uo_contracts','safa_uo_contracts.safa_uo_contract_id=safa_uo_contracts_eas.safa_uo_contract_id');
//      $this->db->join('safa_uos','safa_uos.safa_uo_id=safa_uo_contracts.safa_uo_id');
//      $this->db->where('safa_uos.safa_uo_id',$uo_id);
//      $result=$this->db->get()->result();
//      return $result;  
//  }
    function get_externalagents() {
        $this->db->select("safa_ea_id," . name());
        $this->db->from("safa_eas");
        $query = $this->db->get()->result();
        return $query;
    }

    function add_ea_packages($uo_ea_contract_id = false, $season_id = false) {
        $native_query = "insert into safa_ea_packages(safa_uo_package_id,safa_uo_contract_ea_id,safa_ea_season_id)
            select safa_uo_packages.safa_uo_package_id," . $this->db->escape($uo_ea_contract_id) . "," . $this->db->escape($season_id) . "
               from safa_uo_packages where safa_uo_packages.safa_uo_id=" . $this->safa_uo_id;
        $this->db->query($native_query);
        return $this->db->affected_rows();
    }

    # function to check if contracts have ito to show add and delete butttons (shreen)

    function check_contract_ito($safa_ito_id, $safa_uo_contract_id) {

        $this->db->where("safa_ito_id", $safa_ito_id);
        $this->db->where("safa_uo_contract_id", $safa_uo_contract_id);
        $query = $this->db->get("safa_uo_contracts_itos");

        if ($query->num_rows > 0) {

            return $query->num_rows;
        }
    }

    # function to check if contracts have hotel to show add and delete butttons (shreen)

    function check_contract_hotel($erp_hotel_id, $safa_uo_contract_id) {

        $this->db->where("erp_hotel_id", $erp_hotel_id);
        $this->db->where("safa_uo_contract_id", $safa_uo_contract_id);
        $query = $this->db->get("safa_uo_contracts_hotels");

        if ($query->num_rows > 0) {

            return $query->num_rows;
        }
    }

//    function check_delete_contract_ito($safa_ito_id , $safa_uo_contract_id){
//        
//        $this->db->where("safa_ito_id",$safa_ito_id);
//        $this->db->where("safa_contarct_id" ,$safa_uo_contract_id);
//        $query = $this->db->delete('safa_uo_contracts_itos');
//        
//        if($query->num_rows > 0){
//            
//            return $query->num_rows;
//        }
//    }

    function get_contract_name($safa_uo_contract_id) {

        $this->db->where("safa_uo_contract_id", $safa_uo_contract_id);
        $query = $this->db->get("safa_uo_contracts_itos");
    }

    //By Gouda
    function save_contract_ea() {
        if ($this->safa_uo_contract_id !== FALSE)
            $this->db->set('safa_uo_contracts_eas.safa_uo_contract_id', $this->safa_uo_contract_id);

        if ($this->uo_contracts_eas_disabled !== FALSE)
            $this->db->set('safa_uo_contracts_eas.disabled', $this->uo_contracts_eas_disabled);

        if ($this->by_eas_id !== FALSE)
            $this->db->set('safa_uo_contracts_eas.safa_ea_id', $this->by_eas_id);

        if ($this->uo_contracts_eas_is_original_owner !== FALSE)
            $this->db->set('safa_uo_contracts_eas.is_original_owner', $this->uo_contracts_eas_is_original_owner);

        if ($this->uo_contracts_eas_name_ar !== FALSE)
            $this->db->set('safa_uo_contracts_eas.name_ar', $this->uo_contracts_eas_name_ar);

        if ($this->uo_contracts_eas_name_la !== FALSE)
            $this->db->set('safa_uo_contracts_eas.name_la', $this->uo_contracts_eas_name_la);


        if ($this->uo_contracts_eas_safa_uo_contract_ea_id) {
            $this->db->where('safa_uo_contracts_eas.safa_uo_contract_ea_id', $this->uo_contracts_eas_safa_uo_contract_ea_id)->update('safa_uo_contracts_eas');
        } else {
            $this->db->insert('safa_uo_contracts_eas');
            return $this->db->insert_id();
        }
    }

    function disable_contract_ea_by_contract_id() {
        if ($this->uo_contracts_eas_disabled !== FALSE)
            $this->db->set('safa_uo_contracts_eas.disabled', $this->uo_contracts_eas_disabled);

        $this->db->where('safa_uo_contracts_eas.safa_uo_contract_id', $this->safa_uo_contract_id)->update('safa_uo_contracts_eas');
    }

    function get_contract_ea($rows_no = FALSE) {

        if ($this->custom_select !== FALSE) {
            $this->db->select('safa_uo_contract_ea_id');
            $this->db->select($this->custom_select);
        }

        if ($this->uo_contracts_eas_safa_uo_contract_ea_id !== FALSE)
            $this->db->where('safa_uo_contracts_eas.safa_uo_contract_ea_id', $this->uo_contracts_eas_safa_uo_contract_ea_id);

        if ($this->safa_uo_contract_id !== FALSE)
            $this->db->where('safa_uo_contracts_eas.safa_uo_contract_id', $this->safa_uo_contract_id);

        if ($this->uo_contracts_eas_disabled !== FALSE)
            $this->db->where('safa_uo_contracts_eas.disabled', $this->uo_contracts_eas_disabled);

        if ($this->by_eas_id !== FALSE)
            $this->db->where('safa_uo_contracts_eas.safa_ea_id', $this->by_eas_id);

        if ($this->uo_contracts_eas_is_original_owner !== FALSE)
            $this->db->where('safa_uo_contracts_eas.is_original_owner', $this->uo_contracts_eas_is_original_owner);

        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('safa_uo_contracts_eas');

        $query_text = $this->db->last_query();
        //echo $query_text; exit;

        if ($rows_no)
            return $query->num_rows();

        if ($this->safa_uo_contract_id)
            return $query->row();
        else
            return $query->result();
    }

    function delete_contract_ea() {

        if ($this->uo_contracts_eas_safa_uo_contract_ea_id !== FALSE)
            $this->db->where('safa_uo_contracts_eas.safa_uo_contract_ea_id', $this->uo_contracts_eas_safa_uo_contract_ea_id);

        if ($this->safa_uo_contract_id !== FALSE)
            $this->db->where('safa_uo_contracts_eas.safa_uo_contract_id', $this->safa_uo_contract_id);

        if ($this->by_eas_id !== FALSE)
            $this->db->where('safa_uo_contracts_eas.safa_ea_id', $this->by_eas_id);

        if ($this->uo_contracts_eas_disabled !== FALSE)
            $this->db->where('safa_uo_contracts_eas.disabled', $this->uo_contracts_eas_disabled);



        $this->db->delete('safa_uo_contracts_eas');
        return $this->db->affected_rows();
    }

}

/* End of file contracts_model.php */
/* Location: ./application/models/contracts_model.php */