<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Fs_model extends CI_Model {

    public $fs_flight_id = FALSE;
    public $request = FALSE;
    public $fs_airports = FALSE;
    public $fs_airlines = FALSE;
    public $fs_equipments = FALSE;
    public $fs_states = FALSE;
    public $fs_resources = FALSE;
    public $result = FALSE;

    function get() {
        
        if ($this->fs_flight_id)
            $this->db->where('fs_flight_id', $this->fs_flight_id);

        if ($this->airline)
            $this->db->where('airline', $this->airline);

        if ($this->flight)
            $this->db->where('flight', $this->flight);

        if ($this->date)
            $this->db->where('date', $this->date);

        $query = $this->db->get(FSDB . '.fs_flights');
        $this->result->flight = $query->row();

        if ($this->result->flight) {
            $this->result->airports = $this->get_airports();
            $this->result->equipments = $this->get_equipments();
            $this->result->states = $this->get_states();
            return $this->result;
        } else
            return FALSE;
    }

    function save() {
        if( ! isset($this->request->flightStatuses))
            return;
        if (isset($this->request->flightStatuses[0]->carrierFsCode))
        if($flight = $this->db->where('airline', $this->request->flightStatuses[0]->carrierFsCode)
                    ->where('date', date('Y-m-d', strtotime($this->request->flightStatuses[0]->departureDate->dateUtc)))
                    ->where('flight', $this->request->flightStatuses[0]->flightNumber)
                    ->get(FSDB . '.fs_flights')
                    ->row())
        $this->fs_flight_id = $flight->fs_flight_id;
        
        if (isset($this->request->flightStatuses[0]->carrierFsCode))
            $this->db->set('airline', $this->request->flightStatuses[0]->carrierFsCode);
        if (isset($this->request->flightStatuses[0]->flightNumber))
            $this->db->set('flight', $this->request->flightStatuses[0]->flightNumber);
        if (isset($this->request->flightStatuses[0]->departureDate->dateUtc))
            $this->db->set('date', $this->request->flightStatuses[0]->departureDate->dateUtc);
        if (isset($this->request->appendix->airports))
            $this->fs_airports = $this->request->appendix->airports;
        if (isset($this->request->appendix->airlines))
            $this->fs_airlines = $this->request->appendix->airlines;
        if (isset($this->request->appendix->equipments))
            $this->fs_equipments = $this->request->appendix->equipments;
        if (isset($this->request->flightStatuses))
            $this->fs_states = $this->request->flightStatuses;

        $fs_airline_id = $this->update_airlines();
        $this->db->set('fs_airline_id', $fs_airline_id);
        
        $this->db->set('last_update', date('Y-m-d H:i'));
        if (isset($this->request->flightStatuses) && is_array($this->request->flightStatuses) && count($this->request->flightStatuses)) {
            $flightStatus = $this->request->flightStatuses;
            $this->db->set('fs_status_id', $flightStatus['0']->status);
        }
       
        if (isset($this->request)) {
            $fs_flights['request'] = json_encode($this->request);
            if ($this->fs_flight_id !== FALSE) {
                $this->db->where('fs_flight_id', $this->fs_flight_id);
                $this->db->update(FSDB . '.fs_flights', $fs_flights);
                $fs_flight_id = $this->fs_flight_id;
                $this->update_airports($fs_flight_id);
                $this->update_states($fs_flight_id);
                $this->update_equipments($fs_flight_id);
//                $this->update_resources($fs_flight_id);
            } else {
                $this->db->insert(FSDB . '.fs_flights', $fs_flights);
                $fs_flight_id = $this->db->insert_id();
                $this->update_airports($fs_flight_id);
                $this->update_states($fs_flight_id);
                $this->update_equipments($fs_flight_id);
//                $this->update_resources($fs_flight_id);
            }
        }
        return $fs_flight_id;
    }

    function update_airports($fs_flight_id = FALSE) {
        $this->db->where('fs_flight_id', $fs_flight_id)->delete(FSDB . '.fs_airports');
        if (!isset($this->fs_airports))
            return;
        if (isset($this->fs_airports) && is_array($this->fs_airports) && count($this->fs_airports))
            foreach ($this->fs_airports as $fs_airport) {
                $this->db->set('fs_flight_id', $fs_flight_id);
                if (isset($fs_airport->iata))
                    $this->db->set('iata', $fs_airport->iata);
                if (isset($fs_airport->icao))
                    $this->db->set('icao', $fs_airport->icao);
                if (isset($fs_airport->name))
                    $this->db->set('name', $fs_airport->name);
                if (isset($fs_airport->city))
                    $this->db->set('city', $fs_airport->city);
                if (isset($fs_airport->cityCode))
                    $this->db->set('cityCode', $fs_airport->cityCode);
                if (isset($fs_airport->countryCode))
                    $this->db->set('countryCode', $fs_airport->countryCode);
                if (isset($fs_airport->countryName))
                    $this->db->set('countryName', $fs_airport->countryName);
                if (isset($fs_airport->regionName))
                    $this->db->set('regionName', $fs_airport->regionName);
                if (isset($fs_airport->timeZoneRegionName))
                    $this->db->set('timeZoneRegionName', $fs_airport->timeZoneRegionName);
                if (isset($fs_airport->latitude))
                    $this->db->set('latitude', $fs_airport->latitude);
                if (isset($fs_airport->longitude))
                    $this->db->set('longitude', $fs_airport->longitude);
                if (isset($fs_airport->elevationFeet))
                    $this->db->set('elevationFeet', $fs_airport->elevationFeet);
                if (isset($fs_airport->classification))
                    $this->db->set('classification', $fs_airport->classification);

                $this->db->insert(FSDB . '.fs_airports');
            }
    }

    function update_states($fs_flight_id = FALSE) {
        $this->db->where('fs_flight_id', $fs_flight_id)->delete(FSDB . '.fs_states');
        if (!isset($this->fs_states))
            return;

        if (isset($this->request->flightStatuses) && is_array($this->request->flightStatuses)) {
            foreach ($this->request->flightStatuses as $flight_status) {
                $this->db->set('fs_flight_id', $fs_flight_id);
                if (isset($flight_status->departureAirportFsCode))
                    $this->db->set('departureAirportFsCode', $flight_status->departureAirportFsCode);
                if (isset($flight_status->arrivalAirportFsCode))
                    $this->db->set('arrivalAirportFsCode', $flight_status->arrivalAirportFsCode);
                if (isset($flight_status->departureDate->dateLocal))
                    $this->db->set('departureDate', $flight_status->departureDate->dateLocal);
                if (isset($flight_status->arrivalDate->dateLocal))
                    $this->db->set('arrivalDate', $flight_status->arrivalDate->dateLocal);
                if (isset($flight_status->departureDate->dateUtc))
                    $this->db->set('departureDateUTC', $flight_status->departureDate->dateUtc);
                if (isset($flight_status->arrivalDate->dateUtc))
                    $this->db->set('arrivalDateUTC', $flight_status->arrivalDate->dateUtc);
                if (isset($flight_status->status))
                    $this->db->set('status', $flight_status->status);
                if (isset($flight_status->schedule->flightType))
                    $this->db->set('flightType', $flight_status->schedule->flightType);
                if (isset($flight_status->operationalTimes->publishedDeparture->dateLocal))
                    $this->db->set('publishedDeparture', $flight_status->operationalTimes->publishedDeparture->dateLocal);
                if (isset($flight_status->operationalTimes->scheduledGateDeparture->dateLocal))
                    $this->db->set('scheduledGateDeparture', $flight_status->operationalTimes->scheduledGateDeparture->dateLocal);
                if (isset($flight_status->operationalTimes->estimatedGateDeparture->dateLocal))
                    $this->db->set('estimatedGateDeparture', $flight_status->operationalTimes->estimatedGateDeparture->dateLocal);
                if (isset($flight_status->operationalTimes->actualGateDeparture->dateLocal))
                    $this->db->set('actualGateDeparture', $flight_status->operationalTimes->actualGateDeparture->dateLocal);
                if (isset($flight_status->operationalTimes->scheduledGateArrival->dateLocal))
                    $this->db->set('scheduledGateArrival', $flight_status->operationalTimes->scheduledGateArrival->dateLocal);
                if (isset($flight_status->operationalTimes->estimatedGateArrival->dateLocal))
                    $this->db->set('estimatedGateArrival', $flight_status->operationalTimes->estimatedGateArrival->dateLocal);
                if (isset($flight_status->operationalTimes->publishedDeparture->dateLocal))
                    $this->db->set('actualRunwayArrival', $flight_status->operationalTimes->publishedDeparture->dateLocal);
                if (isset($flight_status->operationalTimes->publishedDeparture->dateUtc))
                    $this->db->set('publishedDepartureUTC', $flight_status->operationalTimes->publishedDeparture->dateUtc);
                if (isset($flight_status->operationalTimes->scheduledGateDeparture->dateUtc))
                    $this->db->set('scheduledGateDepartureUTC', $flight_status->operationalTimes->scheduledGateDeparture->dateUtc);
                if (isset($flight_status->operationalTimes->estimatedGateDeparture->dateUtc))
                    $this->db->set('estimatedGateDepartureUTC', $flight_status->operationalTimes->estimatedGateDeparture->dateUtc);
                if (isset($flight_status->operationalTimes->actualGateDeparture->dateUtc))
                    $this->db->set('actualGateDepartureUTC', $flight_status->operationalTimes->actualGateDeparture->dateUtc);
                if (isset($flight_status->operationalTimes->scheduledGateArrival->dateUtc))
                    $this->db->set('scheduledGateArrivalUTC', $flight_status->operationalTimes->scheduledGateArrival->dateUtc);
                if (isset($flight_status->operationalTimes->estimatedGateArrival->dateUtc))
                    $this->db->set('estimatedGateArrivalUTC', $flight_status->operationalTimes->estimatedGateArrival->dateUtc);
                if (isset($flight_status->operationalTimes->publishedDeparture->dateUtc))
                    $this->db->set('actualRunwayArrivalUTC', $flight_status->operationalTimes->publishedDeparture->dateUtc);
                if (isset($flight_status->flightDurations->scheduledBlockMinutes))
                    $this->db->set('scheduledBlockMinutes', $flight_status->flightDurations->scheduledBlockMinutes);
                if (isset($flight_status->flightDurations->blockMinutes))
                    $this->db->set('blockMinutes', $flight_status->flightDurations->blockMinutes);
                if (isset($flight_status->flightId))
                    $this->db->set('flightId', $flight_status->flightId);
                if (isset($flight_status->delays->departureGateDelayMinutes))
                    $this->db->set('departureGateDelayMinutes', $flight_status->delays->departureGateDelayMinutes);
                if (isset($flight_status->delays->arrivalGateDelayMinutes))
                    $this->db->set('arrivalGateDelayMinutes', $flight_status->delays->arrivalGateDelayMinutes);

                $this->db->insert(FSDB . '.fs_states');

                if (isset($flight_status->airportResources))
                    $this->fs_resources = $flight_status->airportResources;

                $this->update_resources($fs_flight_id);
            }
        }
    }

    function update_equipments($fs_flight_id = FALSE) {
        $this->db->where('fs_flight_id', $fs_flight_id)->delete(FSDB . '.fs_equipments');
        if (!isset($this->fs_equipments))
            return;
        if (isset($this->fs_equipments) && is_array($this->fs_equipments) && count($this->fs_equipments))
            foreach ($this->fs_equipments as $equipments) {
                $this->db->set('fs_flight_id', $fs_flight_id);
                if (isset($equipments->iata))
                    $this->db->set('iata', $equipments->iata);
                if (isset($equipments->name))
                    $this->db->set('name', $equipments->name);
                if (isset($equipments->turboProp))
                    $this->db->set('turboProp', $equipments->turboProp);
                if (isset($equipments->jet))
                    $this->db->set('jet', $equipments->jet);
                if (isset($equipments->widebody))
                    $this->db->set('widebody', $equipments->widebody);
                if (isset($equipments->regional))
                    $this->db->set('regional', $equipments->regional);
                $this->db->insert(FSDB . '.fs_equipments');
            }
    }

    function update_resources($fs_flight_id = FALSE) {
        $this->db->where('fs_flight_id', $fs_flight_id)->delete(FSDB . '.fs_resources');
        if (isset($this->fs_resources)) {
            $this->db->set('fs_flight_id', $fs_flight_id);
            if (isset($this->fs_resources->departureTerminal))
                $this->db->set('departureTerminal', $this->fs_resources->departureTerminal);
            if (isset($this->fs_resources->arrivalTerminal))
                $this->db->set('arrivalTerminal', $this->fs_resources->arrivalTerminal);
            $this->db->insert(FSDB . '.fs_resources');
        }
    }
    
    function update_airlines() {
        if ($this->fs_airlines) {
            foreach ($this->fs_airlines as $airline){
                $get_airline = $this->db->where('iata', $airline->iata)->get(FSDB.'.fs_airlines')->row();
                if($get_airline)
                    return $get_airline->fs_airline_id;
                
                $this->db->query("INSERT INTO " . FSDB . ".fs_airlines (iata, icao, airline, name) 
                    VALUES('" . $airline->iata . "', '" . $airline->icao . "', '" . $airline->airline . "', '" . $airline->name . "')");
                return $this->db->insert_id();
            }
        }
    }

    function update($fs_flight_id) {
        $this->db->where('fs_flight_id', $fs_flight_id)->delete(FSDB . '.fs_airlines');
        $this->db->where('fs_flight_id', $fs_flight_id)->delete(FSDB . '.fs_airports');
        $this->db->where('fs_flight_id', $fs_flight_id)->delete(FSDB . '.fs_equipments');
        $this->db->where('fs_flight_id', $fs_flight_id)->delete(FSDB . '.fs_states');
        if ($this->fs_airlines) {
            foreach ($this->fs_airlines as $airline) {
                $this->db->query("INSERT INTO " . FSDB . ".fs_airlines(fs_flight_id, airline) 
                    VALUES(" . $fs_flight_id . ",'" . $airline->name . "')");
            }
        }
        if ($this->fs_airports) {
            foreach ($this->fs_airports as $airport) {
                $this->db->query("INSERT INTO " . FSDB . ".fs_airports(
                        fs_flight_id
                        , iata
                        , icao
                        , name
                        , city
                        , cityCode
                        , countryCode
                        , countryName
                        , regionName
                        , timeZoneRegionName
                        , latitude
                        , longitude
                        , elevationFeet
                        , classification
                    )
                    VALUES(
                        " . $fs_flight_id . "
                        , '" . $airport->iata . "'
                        , '" . $airport->icao . "'
                        , '" . $airport->name . "'
                        , '" . $airport->city . "'
                        , '" . $airport->cityCode . "'
                        , '" . $airport->countryCode . "'
                        , '" . $airport->countryName . "'
                        , '" . $airport->regionName . "'
                        , '" . $airport->timeZoneRegionName . "'
                        , '" . $airport->latitude . "'
                        , '" . $airport->longitude . "'
                        , '" . $airport->elevationFeet . "'
                        , '" . $airport->classification . "'
                    )");
            }
        }
        if ($this->fs_equipments) {
            foreach ($this->fs_equipments as $equipment) {
                $this->db->query("INSERT INTO " . FSDB . ".fs_equipments(
                        fs_flight_id
                        , iata
                        , name
                        , turboProp
                        , jet
                        , widebody
                        , regional
                    )
                    VALUES(
                          '" . $fs_flight_id . "'
                        , '" . $equipment->iata . "'
                        , '" . $equipment->name . "'
                        , '" . $equipment->turboProp . "'
                        , '" . $equipment->jet . "'
                        , '" . $equipment->widebody . "'
                        , '" . $equipment->regional . "'
                    )");
            }
        }
        if ($this->fs_states) {
            foreach ($this->fs_states as $state) {
                if (!$state->operationalTimes->publishedDeparture->dateUtc)
                    $state->operationalTimes->publishedDeparture->dateUtc = NULL;
                if (!$state->operationalTimes->scheduledGateDeparture->dateUtc)
                    $state->operationalTimes->scheduledGateDeparture->dateUtc = NULL;
                if (!$state->operationalTimes->estimatedGateDeparture->dateUtc)
                    $state->operationalTimes->estimatedGateDeparture->dateUtc = NULL;
                if (!$state->operationalTimes->actualGateDeparture->dateUtc)
                    $state->operationalTimes->actualGateDeparture->dateUtc = NULL;
                if (!$state->operationalTimes->scheduledGateArrival->dateUtc)
                    $state->operationalTimes->scheduledGateArrival->dateUtc = NULL;
                if (!$state->operationalTimes->estimatedGateArrival->dateUtc)
                    $state->operationalTimes->estimatedGateArrival->dateUtc = NULL;
                if (!$state->operationalTimes->actualRunwayArrival->dateUtc)
                    $state->operationalTimes->actualRunwayArrival->dateUtc = NULL;

                $this->db->query("INSERT INTO " . FSDB . ".fs_states(
                        fs_flight_id
                        , departureAirportFsCode
                        , arrivalAirportFsCode
                        , departureDate
                        , arrivalDate
                        , status
                        , flightType
                        , publishedDeparture
                        , scheduledGateDeparture
                        , estimatedGateDeparture
                        , actualGateDeparture
                        , scheduledGateArrival
                        , estimatedGateArrival
                        , actualRunwayArrival
                        , flightDurations
                    )
                    VALUES(
                          '" . $fs_flight_id . "'
                        , '" . $state->departureAirportFsCode . "'
                        , '" . $state->arrivalAirportFsCode . "'
                        , '" . $state->departureDate->dateUtc . "'
                        , '" . $state->arrivalDate->dateUtc . "'
                        , '" . $state->status . "'
                        , '" . $state->schedule->flightType . "'
                        , '" . $state->operationalTimes->publishedDeparture->dateUtc . "'
                        , '" . $state->operationalTimes->scheduledGateDeparture->dateUtc . "'
                        , '" . $state->operationalTimes->estimatedGateDeparture->dateUtc . "'
                        , '" . $state->operationalTimes->actualGateDeparture->dateUtc . "'
                        , '" . $state->operationalTimes->scheduledGateArrival->dateUtc . "'
                        , '" . $state->operationalTimes->estimatedGateArrival->dateUtc . "'
                        , '" . $state->operationalTimes->actualRunwayArrival->dateUtc . "'
                        , '" . $state->flightDurations->scheduledBlockMinutes . "'
                    )");
            }
        }
    }

    function get_airports() {
        $this->fs_airports = $this->db->where('fs_flight_id', $this->fs_flight_id)->get(FSDB . ".fs_airports")->result();
    }

    function get_equipments() {
        $this->fs_equipments = $this->db->where('fs_flight_id', $this->fs_flight_id)->get(FSDB . ".fs_equipments")->result();
    }

    function get_resources() {
        $this->fs_equipments = $this->db->where('fs_flight_id', $this->fs_flight_id)->get(FSDB . ".fs_resources")->result();
    }

    function get_states() {
        $this->fs_states = $this->db->where('fs_flight_id', $this->fs_flight_id)->get(FSDB . ".fs_states")->result();
    }

    function get_null_segments() {
        $this->db->select('erp_flight_availabilities_detail.*, erp_flight_availabilities_detail.airline_code as code');
//        $this->db->where('(`fs_flight_id` IS NULL  OR (fs_flight_id = "0" AND ( fs_latest_check <= "'.date('Y-m-d H:i:s', strtotime('-1 day')).'" OR fs_latest_check IS NULL)))', false, false);
//        $this->db->where('(`fs_flight_id` IS NULL  AND ( fs_latest_check <= "'.date('Y-m-d H:i:s', strtotime('-1 day')).'" OR fs_latest_check IS NULL))', false, false);
        $this->db->where('(`fs_flight_id` IS NULL)', false, false);
        $this->db->where('flight_date >=', date('Y-m-d', strtotime('yesterday')));
        $this->db->where('flight_date <=', date('Y-m-d', strtotime('+1 days')));
        $query = $this->db->get('erp_flight_availabilities_detail');
        return $query->result();
    }

    function get_hashed_segments() {
        $this->db->select('erp_flight_availabilities_detail.*, erp_flight_availabilities_detail.airline_code as code');
        $this->db->where('fs_flight_id', 0);
        $this->db->where('flight_date', date('Y-m-d'));
        $this->db->where('fs_latest_check <=', date('Y-m-d', strtotime('yesterday')));
        $query = $this->db->get('erp_flight_availabilities_detail');
        return $query->result();
    }

    function get_flight($code, $flight_number, $date) {
        return $this->db->where('fs_flights.airline', $code)
                        ->where('fs_flights.flight',  $flight_number)
                        ->where('date', $date)
                        ->get(FSDB . '.fs_flights', 1)
                        ->row();
    }

    function update_fd_info($fd_id, $flight_id) {
        $flight = $this->db->where('fs_flights.fs_flight_id', $flight_id)
                           ->join(FSDB . '.fs_flights', FSDB . '.fs_flights.fs_flight_id = ' . FSDB . '.fs_states.fs_flight_id')
                           ->get(FSDB . '.fs_states', 1)
                           ->row();
        
        if ($flight) {
            if ($flight->departureAirportFsCode) {
                $dport_id = $this->getPortId($flight->departureAirportFsCode);
            }
            if ($flight->arrivalAirportFsCode) {
                $aport_id = $this->getPortId($flight->arrivalAirportFsCode);
            }
            if($dport_id)
                $this->db->set('erp_port_id_from', $dport_id);
            if($aport_id)
                $this->db->set('erp_port_id_to', $aport_id);
            if ($flight->departureDate)
                $this->db->set('flight_date', $flight->departureDate);
            if ($flight->arrivalDate)
                $this->db->set('arrival_date', $flight->arrivalDate);
            if ($flight_id)
                $this->db->set('fs_flight_id', $flight->fs_flight_id);
                $this->db->set('fs_checks', 'fs_checks+1', false);
                $this->db->set('fs_latest_check', date('Y-m-d H:i:s'));
            
            $this->db->where('erp_flight_availabilities_detail_id', $fd_id)
                    ->update(SLDB . '.erp_flight_availabilities_detail');
        }
    }
    
    public function getPortId($code) {
        $erp_port = $this->db->where('erp_ports.code', $code)
                            ->get('erp_ports')
                            ->row();
        if( ! $erp_port) {
            $fs_port = $this->db->where('iata', $code)->get(FSDB . '.fs_airports')->row();
            if( ! $fs_port)
                return FALSE;
            $this->db->set('code', $fs_port->iata);
            $this->db->set('name_ar', $fs_port->name);
            $this->db->set('name_la', $fs_port->name);
            $this->db->set('country_code', $fs_port->countryCode);
            $this->db->set('country_name', $fs_port->countryName);
            $this->db->set('safa_transportertype_id', 2);
            $this->db->insert('erp_ports');
            return  $this->db->insert_id();
        }
        return $erp_port->erp_port_id;
    }
}

/* End of file hotels.php */
/* Location: ./application/models/hotels_model.php */