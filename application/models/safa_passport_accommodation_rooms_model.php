<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Safa_passport_accommodation_rooms_model extends CI_Model {

	public $safa_passport_accommodation_room_id = FALSE;
	public $from_date = FALSE;
	public $to_date = FALSE;
	public $closed = FALSE;
	public $erp_hotelroomsize_id = FALSE;
	public $erp_hotel_id = FALSE;
	public $erp_hotels_availability_room_detail_id = FALSE;
	public $safa_reservation_form_id = FALSE;

	public $safa_trip_id = FALSE;
	public $erp_hotels_availability_master_id = FALSE;
	
	
    public $custom_select = FALSE;
    public $limit = FALSE;
    public $offset = FALSE;
    public $order_by = FALSE;

    function __construct() 
    {
        parent::__construct();
    }

    function get($rows_no = FALSE) 
    {

        if ($this->custom_select !== FALSE) {
            $this->db->select('safa_passport_accommodation_room_id');
            $this->db->select($this->custom_select);
        }

        if ($this->safa_passport_accommodation_room_id !== FALSE)
            $this->db->where('safa_passport_accommodation_rooms.safa_passport_accommodation_room_id', $this->safa_passport_accommodation_room_id);
        if ($this->from_date !== FALSE)
            $this->db->where('safa_passport_accommodation_rooms.from_date', $this->from_date);
        if ($this->to_date !== FALSE)
            $this->db->where('safa_passport_accommodation_rooms.to_date', $this->to_date);
        if ($this->closed !== FALSE)
            $this->db->where('safa_passport_accommodation_rooms.closed', $this->closed);
        if ($this->erp_hotelroomsize_id !== FALSE)
            $this->db->where('safa_passport_accommodation_rooms.erp_hotelroomsize_id', $this->erp_hotelroomsize_id);
        if ($this->erp_hotel_id !== FALSE)
            $this->db->where('safa_passport_accommodation_rooms.erp_hotel_id', $this->erp_hotel_id);
        if ($this->erp_hotels_availability_room_detail_id !== FALSE)
            $this->db->where('safa_passport_accommodation_rooms.erp_hotels_availability_room_detail_id', $this->erp_hotels_availability_room_detail_id);
        if ($this->safa_reservation_form_id !== FALSE)
            $this->db->where('safa_passport_accommodation_rooms.safa_reservation_form_id', $this->safa_reservation_form_id);
        
         if ($this->safa_trip_id !== FALSE) {
            $this->db->where('safa_reservation_forms.safa_trip_id', $this->safa_trip_id);
         }
            
        $this->db->select('safa_passport_accommodation_rooms.*, erp_hotels_availability_room_details.number as availability_room_number, erp_hotels.' . name() . ' as erp_hotel_name, erp_cities.' . name() . ' as erp_city_name');
		$this->db->from('safa_passport_accommodation_rooms');
       	$this->db->join('erp_hotels_availability_room_details', 'erp_hotels_availability_room_details.erp_hotels_availability_room_detail_id = safa_passport_accommodation_rooms.erp_hotels_availability_room_detail_id', 'left');
        $this->db->join('safa_reservation_forms', 'safa_reservation_forms.safa_reservation_form_id = safa_passport_accommodation_rooms.safa_reservation_form_id', 'left');
            
        $this->db->join('erp_hotels', 'erp_hotels.erp_hotel_id = safa_passport_accommodation_rooms.erp_hotel_id', 'left');
        $this->db->join('erp_cities', 'erp_cities.erp_city_id = erp_hotels.erp_city_id', 'left');
        
        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get();
        if ($rows_no)
            return $query->num_rows();

        if ($this->safa_passport_accommodation_room_id)
            return $query->row();
        else
            return $query->result();
    }

	function get_first_entry_date_for_trip($safa_trip_id) 
    {
    	$this->db->distinct();
        $this->db->select('safa_passport_accommodation_rooms.from_date, safa_passport_accommodation_rooms.to_date, safa_passport_accommodation_rooms.erp_hotel_id, erp_hotels.' . name() . ' as erp_hotel_name, erp_cities.' . name() . ' as erp_city_name');
        $this->db->where('safa_reservation_forms.safa_trip_id', $safa_trip_id);
            
		$this->db->from('safa_passport_accommodation_rooms');
        $this->db->join('safa_reservation_forms', 'safa_reservation_forms.safa_reservation_form_id = safa_passport_accommodation_rooms.safa_reservation_form_id', 'left');
        $this->db->join('erp_hotels', 'erp_hotels.erp_hotel_id = safa_passport_accommodation_rooms.erp_hotel_id', 'left');
        $this->db->join('erp_cities', 'erp_cities.erp_city_id = erp_hotels.erp_city_id', 'left');
          
        $this->db->order_by('safa_passport_accommodation_rooms.from_date', 'asc');

        $this->db->limit(1);

        $query = $this->db->get();
        
        return $query->row();
        
    }
    
    function save() 
    {
        if ($this->safa_passport_accommodation_room_id !== FALSE)
            $this->db->set('safa_passport_accommodation_rooms.safa_passport_accommodation_room_id', $this->safa_passport_accommodation_room_id);

         if ($this->from_date !== FALSE)
            $this->db->set('safa_passport_accommodation_rooms.from_date', $this->from_date);
        if ($this->to_date !== FALSE)
            $this->db->set('safa_passport_accommodation_rooms.to_date', $this->to_date);
        if ($this->closed !== FALSE)
            $this->db->set('safa_passport_accommodation_rooms.closed', $this->closed);
        if ($this->erp_hotelroomsize_id !== FALSE)
            $this->db->set('safa_passport_accommodation_rooms.erp_hotelroomsize_id', $this->erp_hotelroomsize_id);
        if ($this->erp_hotel_id !== FALSE)
            $this->db->set('safa_passport_accommodation_rooms.erp_hotel_id', $this->erp_hotel_id);
        if ($this->erp_hotels_availability_room_detail_id !== FALSE)
            $this->db->set('safa_passport_accommodation_rooms.erp_hotels_availability_room_detail_id', $this->erp_hotels_availability_room_detail_id);
        if ($this->safa_reservation_form_id !== FALSE)
            $this->db->set('safa_passport_accommodation_rooms.safa_reservation_form_id', $this->safa_reservation_form_id);
        

        if ($this->safa_passport_accommodation_room_id) {
            $this->db->where('safa_passport_accommodation_rooms.safa_passport_accommodation_room_id', $this->safa_passport_accommodation_room_id)->update('safa_passport_accommodation_rooms');
        } else {
            $this->db->insert('safa_passport_accommodation_rooms');
            return $this->db->insert_id();
        }
    }

    function delete() 
    {
        if ($this->safa_passport_accommodation_room_id !== FALSE)
            $this->db->where('safa_passport_accommodation_rooms.safa_passport_accommodation_room_id', $this->safa_passport_accommodation_room_id);
        if ($this->erp_hotel_id !== FALSE)
            $this->db->where('safa_passport_accommodation_rooms.erp_hotel_id', $this->erp_hotel_id);
        if ($this->erp_hotels_availability_room_detail_id !== FALSE)
            $this->db->where('safa_passport_accommodation_rooms.erp_hotels_availability_room_detail_id', $this->erp_hotels_availability_room_detail_id);
        if ($this->safa_reservation_form_id !== FALSE)
            $this->db->where('safa_passport_accommodation_rooms.safa_reservation_form_id', $this->safa_reservation_form_id);
        
       
        $this->db->delete('safa_passport_accommodation_rooms');
        return $this->db->affected_rows();
    }

	function get_hotels() 
    {

    	$this->db->distinct();
        $this->db->select('safa_passport_accommodation_rooms.erp_hotel_id ,erp_hotels.erp_city_id , erp_cities.' . name() . ' as erp_city_name, erp_hotels.' . name() . ' as erp_hotel_name, safa_passport_accommodation_rooms.from_date, safa_passport_accommodation_rooms.to_date ');

        if ($this->safa_passport_accommodation_room_id !== FALSE)
            $this->db->where('safa_passport_accommodation_rooms.safa_passport_accommodation_room_id', $this->safa_passport_accommodation_room_id);
        if ($this->from_date !== FALSE)
            $this->db->where('safa_passport_accommodation_rooms.from_date', $this->from_date);
        if ($this->to_date !== FALSE)
            $this->db->where('safa_passport_accommodation_rooms.to_date', $this->to_date);
        if ($this->closed !== FALSE)
            $this->db->where('safa_passport_accommodation_rooms.closed', $this->closed);
        if ($this->erp_hotelroomsize_id !== FALSE)
            $this->db->where('safa_passport_accommodation_rooms.erp_hotelroomsize_id', $this->erp_hotelroomsize_id);
        if ($this->erp_hotel_id !== FALSE)
            $this->db->where('safa_passport_accommodation_rooms.erp_hotel_id', $this->erp_hotel_id);
        if ($this->erp_hotels_availability_room_detail_id !== FALSE)
            $this->db->where('safa_passport_accommodation_rooms.erp_hotels_availability_room_detail_id', $this->erp_hotels_availability_room_detail_id);
        if ($this->safa_reservation_form_id !== FALSE)
            $this->db->where('safa_passport_accommodation_rooms.safa_reservation_form_id', $this->safa_reservation_form_id);

    	if ($this->safa_trip_id !== FALSE) {
            $this->db->where('safa_reservation_forms.safa_trip_id', $this->safa_trip_id);
         }
         
        $this->db->from('safa_passport_accommodation_rooms');
        $this->db->join('erp_hotels', 'erp_hotels.erp_hotel_id = safa_passport_accommodation_rooms.erp_hotel_id', 'left');
        $this->db->join('erp_cities', 'erp_cities.erp_city_id = erp_hotels.erp_city_id', 'left');
        $this->db->join('safa_reservation_forms', 'safa_reservation_forms.safa_reservation_form_id = safa_passport_accommodation_rooms.safa_reservation_form_id', 'left');
        
        $query = $this->db->get();
        
        //echo $this->db->last_query(); exit;
        
		return $query->result();
    }
    
    function get_hotelroomsizes() 
    {
    	$this->db->distinct();
        $this->db->select('safa_passport_accommodation_rooms.erp_hotelroomsize_id , erp_hotelroomsizes.' . name() . ' as erp_hotelroomsize_name  ');

        if ($this->safa_passport_accommodation_room_id !== FALSE)
            $this->db->where('safa_passport_accommodation_rooms.safa_passport_accommodation_room_id', $this->safa_passport_accommodation_room_id);
        if ($this->from_date !== FALSE)
            $this->db->where('safa_passport_accommodation_rooms.from_date', $this->from_date);
        if ($this->to_date !== FALSE)
            $this->db->where('safa_passport_accommodation_rooms.to_date', $this->to_date);
        if ($this->closed !== FALSE)
            $this->db->where('safa_passport_accommodation_rooms.closed', $this->closed);
        if ($this->erp_hotelroomsize_id !== FALSE)
            $this->db->where('safa_passport_accommodation_rooms.erp_hotelroomsize_id', $this->erp_hotelroomsize_id);
        if ($this->erp_hotel_id !== FALSE)
            $this->db->where('safa_passport_accommodation_rooms.erp_hotel_id', $this->erp_hotel_id);
        if ($this->erp_hotels_availability_room_detail_id !== FALSE)
            $this->db->where('safa_passport_accommodation_rooms.erp_hotels_availability_room_detail_id', $this->erp_hotels_availability_room_detail_id);
        if ($this->safa_reservation_form_id !== FALSE)
            $this->db->where('safa_passport_accommodation_rooms.safa_reservation_form_id', $this->safa_reservation_form_id);

    	if ($this->safa_trip_id !== FALSE) {
            $this->db->where('safa_reservation_forms.safa_trip_id', $this->safa_trip_id);
         }
         
    	if($this->erp_hotels_availability_master_id !== FALSE) {
         	$this->db->where('erp_hotels_availability_rooms.erp_hotels_availability_master_id', $this->erp_hotels_availability_master_id);
         }
         
        $this->db->from('safa_passport_accommodation_rooms');
        $this->db->join('erp_hotelroomsizes', 'erp_hotelroomsizes.erp_hotelroomsize_id = safa_passport_accommodation_rooms.erp_hotelroomsize_id', 'left');
        $this->db->join('safa_reservation_forms', 'safa_reservation_forms.safa_reservation_form_id = safa_passport_accommodation_rooms.safa_reservation_form_id', 'left');
         
        $this->db->join('erp_hotels_availability_room_details', 'erp_hotels_availability_room_details.erp_hotels_availability_room_detail_id = safa_passport_accommodation_rooms.erp_hotels_availability_room_detail_id', 'left');
        $this->db->join('erp_hotels_availability_sub_rooms', 'erp_hotels_availability_sub_rooms.erp_hotels_availability_sub_room_id = erp_hotels_availability_room_details.erp_hotels_availability_sub_room_id', 'left');
        $this->db->join('erp_hotels_availability_rooms', 'erp_hotels_availability_rooms.erp_hotels_availability_rooms_id = erp_hotels_availability_sub_rooms.erp_hotels_availability_rooms_id', 'left');
           
        
        $query = $this->db->get();
        
        //echo $this->db->last_query(); exit;
        
		return $query->result();
    }
    
    function get_linked_with_availability($rows_no = FALSE) 
    {

        if ($this->custom_select !== FALSE) {
            $this->db->select('safa_passport_accommodation_room_id');
            $this->db->select($this->custom_select);
        }

        if ($this->safa_passport_accommodation_room_id !== FALSE)
            $this->db->where('safa_passport_accommodation_rooms.safa_passport_accommodation_room_id', $this->safa_passport_accommodation_room_id);
        if ($this->from_date !== FALSE)
            $this->db->where('safa_passport_accommodation_rooms.from_date', $this->from_date);
        if ($this->to_date !== FALSE)
            $this->db->where('safa_passport_accommodation_rooms.to_date', $this->to_date);
        if ($this->closed !== FALSE)
            $this->db->where('safa_passport_accommodation_rooms.closed', $this->closed);
        if ($this->erp_hotelroomsize_id !== FALSE)
            $this->db->where('safa_passport_accommodation_rooms.erp_hotelroomsize_id', $this->erp_hotelroomsize_id);
        if ($this->erp_hotel_id !== FALSE)
            $this->db->where('safa_passport_accommodation_rooms.erp_hotel_id', $this->erp_hotel_id);
        //if ($this->erp_hotels_availability_room_detail_id !== FALSE)
            //$this->db->where('safa_passport_accommodation_rooms.erp_hotels_availability_room_detail_id', $this->erp_hotels_availability_room_detail_id);
            
            $this->db->where('safa_passport_accommodation_rooms.erp_hotels_availability_room_detail_id IS NOT NULl ');
            
        if ($this->safa_reservation_form_id !== FALSE)
            $this->db->where('safa_passport_accommodation_rooms.safa_reservation_form_id', $this->safa_reservation_form_id);
        
         if ($this->safa_trip_id !== FALSE) {
            $this->db->where('safa_reservation_forms.safa_trip_id', $this->safa_trip_id);
         }
            
        $this->db->select('safa_passport_accommodation_rooms.*, erp_hotels_availability_room_details.number as availability_room_number');
		$this->db->from('safa_passport_accommodation_rooms');
       	$this->db->join('erp_hotels_availability_room_details', 'erp_hotels_availability_room_details.erp_hotels_availability_room_detail_id = safa_passport_accommodation_rooms.erp_hotels_availability_room_detail_id', 'left');
        $this->db->join('safa_reservation_forms', 'safa_reservation_forms.safa_reservation_form_id = safa_passport_accommodation_rooms.safa_reservation_form_id', 'left');
            
        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get();
        
        //echo $this->db->last_query(); exit;
        
        if ($rows_no)
            return $query->num_rows();

        if ($this->safa_passport_accommodation_room_id)
            return $query->row();
        else
            return $query->result();
    }
    
    function get_without_availability($rows_no = FALSE) 
    {

        if ($this->custom_select !== FALSE) {
            $this->db->select('safa_passport_accommodation_room_id');
            $this->db->select($this->custom_select);
        }

        if ($this->safa_passport_accommodation_room_id !== FALSE)
            $this->db->where('safa_passport_accommodation_rooms.safa_passport_accommodation_room_id', $this->safa_passport_accommodation_room_id);
        if ($this->from_date !== FALSE)
            $this->db->where('safa_passport_accommodation_rooms.from_date', $this->from_date);
        if ($this->to_date !== FALSE)
            $this->db->where('safa_passport_accommodation_rooms.to_date', $this->to_date);
        if ($this->closed !== FALSE)
            $this->db->where('safa_passport_accommodation_rooms.closed', $this->closed);
        if ($this->erp_hotelroomsize_id !== FALSE)
            $this->db->where('safa_passport_accommodation_rooms.erp_hotelroomsize_id', $this->erp_hotelroomsize_id);
        if ($this->erp_hotel_id !== FALSE)
            $this->db->where('safa_passport_accommodation_rooms.erp_hotel_id', $this->erp_hotel_id);
        //if ($this->erp_hotels_availability_room_detail_id !== FALSE)
            //$this->db->where('safa_passport_accommodation_rooms.erp_hotels_availability_room_detail_id', $this->erp_hotels_availability_room_detail_id);
            
            $this->db->where('safa_passport_accommodation_rooms.erp_hotels_availability_room_detail_id IS NULl ');
            
        if ($this->safa_reservation_form_id !== FALSE)
            $this->db->where('safa_passport_accommodation_rooms.safa_reservation_form_id', $this->safa_reservation_form_id);
        
         if ($this->safa_trip_id !== FALSE) {
            $this->db->where('safa_reservation_forms.safa_trip_id', $this->safa_trip_id);
         }
            
        $this->db->select('safa_passport_accommodation_rooms.*, erp_hotels_availability_room_details.number as availability_room_number');
		$this->db->from('safa_passport_accommodation_rooms');
       	$this->db->join('erp_hotels_availability_room_details', 'erp_hotels_availability_room_details.erp_hotels_availability_room_detail_id = safa_passport_accommodation_rooms.erp_hotels_availability_room_detail_id', 'left');
        $this->db->join('safa_reservation_forms', 'safa_reservation_forms.safa_reservation_form_id = safa_passport_accommodation_rooms.safa_reservation_form_id', 'left');
            
        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get();
        
        //echo $this->db->last_query(); exit;
        
        if ($rows_no)
            return $query->num_rows();

        if ($this->safa_passport_accommodation_room_id)
            return $query->row();
        else
            return $query->result();
    }
    
    function get_hotels_availability_master($rows_no = FALSE) 
    {

        if ($this->custom_select !== FALSE) {
            $this->db->select('safa_passport_accommodation_room_id');
            $this->db->select($this->custom_select);
        }

        if ($this->safa_passport_accommodation_room_id !== FALSE)
            $this->db->where('safa_passport_accommodation_rooms.safa_passport_accommodation_room_id', $this->safa_passport_accommodation_room_id);
        if ($this->from_date !== FALSE)
            $this->db->where('safa_passport_accommodation_rooms.from_date', $this->from_date);
        if ($this->to_date !== FALSE)
            $this->db->where('safa_passport_accommodation_rooms.to_date', $this->to_date);
        if ($this->closed !== FALSE)
            $this->db->where('safa_passport_accommodation_rooms.closed', $this->closed);
        if ($this->erp_hotelroomsize_id !== FALSE)
            $this->db->where('safa_passport_accommodation_rooms.erp_hotelroomsize_id', $this->erp_hotelroomsize_id);
        if ($this->erp_hotel_id !== FALSE)
            $this->db->where('safa_passport_accommodation_rooms.erp_hotel_id', $this->erp_hotel_id);
        //if ($this->erp_hotels_availability_room_detail_id !== FALSE)
            //$this->db->where('safa_passport_accommodation_rooms.erp_hotels_availability_room_detail_id', $this->erp_hotels_availability_room_detail_id);
            
            $this->db->where('safa_passport_accommodation_rooms.erp_hotels_availability_room_detail_id IS NOT NULl ');
            
        if ($this->safa_reservation_form_id !== FALSE)
            $this->db->where('safa_passport_accommodation_rooms.safa_reservation_form_id', $this->safa_reservation_form_id);
        
         if ($this->safa_trip_id !== FALSE) {
            $this->db->where('safa_reservation_forms.safa_trip_id', $this->safa_trip_id);
         }

        $this->db->distinct();
        $this->db->select('erp_hotels_availability_master.erp_hotels_availability_master_id, erp_hotels_availability_master.invoice_number, erp_hotels_availability_master.invoice_image');
		$this->db->from('safa_passport_accommodation_rooms');
       	$this->db->join('erp_hotels_availability_room_details', 'erp_hotels_availability_room_details.erp_hotels_availability_room_detail_id = safa_passport_accommodation_rooms.erp_hotels_availability_room_detail_id', 'left');
        $this->db->join('safa_reservation_forms', 'safa_reservation_forms.safa_reservation_form_id = safa_passport_accommodation_rooms.safa_reservation_form_id', 'left');
        $this->db->join('erp_hotels_availability_sub_rooms', 'erp_hotels_availability_sub_rooms.erp_hotels_availability_sub_room_id = erp_hotels_availability_room_details.erp_hotels_availability_sub_room_id', 'left');
        $this->db->join('erp_hotels_availability_rooms', 'erp_hotels_availability_rooms.erp_hotels_availability_rooms_id = erp_hotels_availability_sub_rooms.erp_hotels_availability_rooms_id', 'left');
        $this->db->join('erp_hotels_availability_master', 'erp_hotels_availability_master.erp_hotels_availability_master_id = erp_hotels_availability_rooms.erp_hotels_availability_master_id', 'left');
           
        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get();
        if ($rows_no)
            return $query->num_rows();

        if ($this->safa_passport_accommodation_room_id)
            return $query->row();
        else
            return $query->result();
    }
    
}

/* End of file uo_packages_model.php */
/* Location: ./application/models/uo_packages_model.php */