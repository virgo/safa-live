<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Safa_reservation_forms_model extends CI_Model {

    public $safa_reservation_form_id = FALSE;
    public $safa_ea_id = FALSE;
    public $safa_trip_id = FALSE;
    public $safa_package_period_id = FALSE;
    
    public $custom_select = FALSE;
    public $limit = FALSE;
    public $offset = FALSE;
    public $order_by = FALSE;

    function __construct() 
    {
        parent::__construct();
    }

    function get($rows_no = FALSE) 
    {

        if ($this->custom_select !== FALSE) {
            $this->db->select('safa_reservation_form_id');
            $this->db->select($this->custom_select);
        }

        if ($this->safa_reservation_form_id !== FALSE)
            $this->db->where('safa_reservation_forms.safa_reservation_form_id', $this->safa_reservation_form_id);

        if ($this->safa_ea_id !== FALSE)
            $this->db->where('safa_reservation_forms.safa_ea_id', $this->safa_ea_id);

        if ($this->safa_trip_id !== FALSE)
            $this->db->where('safa_reservation_forms.safa_trip_id', $this->safa_trip_id);
            
        if ($this->safa_package_period_id !== FALSE)
            $this->db->where('safa_reservation_forms.safa_package_period_id', $this->safa_package_period_id);

		$this->db->select('safa_reservation_forms.*, safa_trips.name as safa_trip_name, safa_packages.'.name().' as safa_package_name, erp_package_periods.'.name().' as safa_package_period_name ');
		$this->db->join('safa_trips', 'safa_trips.safa_trip_id = safa_reservation_forms.safa_trip_id');
        $this->db->join('safa_package_periods', 'safa_package_periods.safa_package_periods_id = safa_reservation_forms.safa_package_period_id');
        $this->db->join('erp_package_periods', 'erp_package_periods.erp_package_period_id = safa_package_periods.erp_package_period_id');
        $this->db->join('safa_packages', 'safa_packages.safa_package_id = safa_package_periods.safa_package_id');
        
        $this->db->from('safa_reservation_forms');
            
        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get();
        if ($rows_no)
            return $query->num_rows();

        if ($this->safa_reservation_form_id)
            return $query->row();
        else
            return $query->result();
    }

    function save() 
    {
        if ($this->safa_reservation_form_id !== FALSE)
            $this->db->set('safa_reservation_forms.safa_reservation_form_id', $this->safa_reservation_form_id);

        if ($this->safa_ea_id !== FALSE)
            $this->db->set('safa_reservation_forms.safa_ea_id', $this->safa_ea_id);
            
        if ($this->safa_trip_id !== FALSE)
            $this->db->set('safa_reservation_forms.safa_trip_id', $this->safa_trip_id);

        if ($this->safa_package_period_id !== FALSE)
            $this->db->set('safa_reservation_forms.safa_package_period_id', $this->safa_package_period_id);

      

        if ($this->safa_reservation_form_id) {
            $this->db->where('safa_reservation_forms.safa_reservation_form_id', $this->safa_reservation_form_id)->update('safa_reservation_forms');
        } else {
            $this->db->insert('safa_reservation_forms');
            return $this->db->insert_id();
        }
    }

    function delete() 
    {
        if ($this->safa_reservation_form_id !== FALSE)
            $this->db->where('safa_reservation_forms.safa_reservation_form_id', $this->safa_reservation_form_id);

        if ($this->safa_ea_id !== FALSE)
            $this->db->where('safa_reservation_forms.safa_ea_id', $this->safa_ea_id);
            
        if ($this->safa_trip_id !== FALSE)
            $this->db->where('safa_reservation_forms.safa_trip_id', $this->safa_trip_id);

        if ($this->safa_package_period_id !== FALSE)
            $this->db->where('safa_reservation_forms.safa_package_period_id', $this->safa_package_period_id);

       
        $this->db->delete('safa_reservation_forms');
        return $this->db->affected_rows();
    }

	
    
    
}

/* End of file uo_packages_model.php */
/* Location: ./application/models/uo_packages_model.php */