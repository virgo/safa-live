<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Contracts_settings_visa_prices_model extends CI_Model {

    public $safa_uo_contract_setting_visa_price_id = FALSE;
    public $safa_uo_contract_id = FALSE;
    public $safa_uo_contract_setting_id = FALSE;
    public $erp_currency_id = FALSE;
    public $price = FALSE;
    public $fromdate = FALSE;
    public $todate = FALSE;
    public $custom_select = FALSE;
    public $limit = FALSE;
    public $offset = FALSE;
    public $order_by = FALSE;
    public $by_eas_id = FALSE;

    function __construct() {
        parent::__construct();
        // if the uo_has loged in// 
    }

    function get($rows_no = FALSE) {

        if ($this->custom_select !== FALSE) {
            $this->db->select('safa_uo_contract_setting_visa_price_id');
            $this->db->select($this->custom_select);
        }

        if ($this->safa_uo_contract_setting_visa_price_id !== FALSE)
            $this->db->where('safa_uo_contracts_settings_visa_prices.safa_uo_contract_setting_visa_price_id', $this->safa_uo_contract_setting_visa_price_id);

        if ($this->safa_uo_contract_id !== FALSE)
            $this->db->where('safa_uo_contracts_settings_visa_prices.safa_uo_contract_id ', $this->safa_uo_contract_id);

        if ($this->safa_uo_contract_setting_id !== FALSE)
            $this->db->where('safa_uo_contracts_settings_visa_prices.safa_uo_contract_setting_id', $this->safa_uo_contract_setting_id);

        if ($this->erp_currency_id !== FALSE)
            $this->db->where('safa_uo_contracts_settings_visa_prices.erp_currency_id', $this->erp_currency_id);

        if ($this->price !== FALSE)
            $this->db->where('safa_uo_contracts_settings_visa_prices.price', $this->price);

        if ($this->fromdate !== FALSE)
            $this->db->where('safa_uo_contracts_settings_visa_prices.fromdate', $this->fromdate);

        if ($this->todate !== FALSE)
            $this->db->where('safa_uo_contracts_settings_visa_prices.todate', $this->todate);


        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('safa_uo_contracts_settings_visa_prices');

        $query_text = $this->db->last_query();
        //echo $query_text; exit;

        if ($rows_no)
            return $query->num_rows();

        if ($this->safa_uo_contract_setting_visa_price_id || $this->safa_uo_contract_setting_visa_price_id)
            return $query->row();
        else
            return $query->result();
    }

    function save() {

        if ($this->safa_uo_contract_id !== FALSE)
            $this->db->set('safa_uo_contracts_settings_visa_prices.safa_uo_contract_id ', $this->safa_uo_contract_id);

        if ($this->safa_uo_contract_setting_id !== FALSE)
            $this->db->set('safa_uo_contracts_settings_visa_prices.safa_uo_contract_setting_id', $this->safa_uo_contract_setting_id);

        if ($this->erp_currency_id !== FALSE)
            $this->db->set('safa_uo_contracts_settings_visa_prices.erp_currency_id', $this->erp_currency_id);

        if ($this->price !== FALSE)
            $this->db->set('safa_uo_contracts_settings_visa_prices.price', $this->price);

        if ($this->fromdate !== FALSE)
            $this->db->set('safa_uo_contracts_settings_visa_prices.fromdate', $this->fromdate);

        if ($this->todate !== FALSE)
            $this->db->set('safa_uo_contracts_settings_visa_prices.todate', $this->todate);


        if ($this->safa_uo_contract_setting_visa_price_id) {
            $this->db->where('safa_uo_contracts_settings_visa_prices.safa_uo_contract_setting_visa_price_id', $this->safa_uo_contract_setting_visa_price_id)->update('safa_uo_contracts_settings_visa_prices');
        } else {
            $this->db->insert('safa_uo_contracts_settings_visa_prices');
            return $this->db->insert_id();
        }
    }

    function delete() {
        if ($this->safa_uo_contract_setting_visa_price_id !== FALSE)
            $this->db->where('safa_uo_contracts_settings_visa_prices.safa_uo_contract_setting_visa_price_id', $this->safa_uo_contract_setting_visa_price_id);

        if ($this->safa_uo_contract_id !== FALSE)
            $this->db->where('safa_uo_contracts_settings_visa_prices.safa_uo_contract_id ', $this->safa_uo_contract_id);

        if ($this->safa_uo_contract_setting_id !== FALSE)
            $this->db->where('safa_uo_contracts_settings_visa_prices.safa_uo_contract_setting_id', $this->safa_uo_contract_setting_id);

        if ($this->erp_currency_id !== FALSE)
            $this->db->where('safa_uo_contracts_settings_visa_prices.erp_currency_id', $this->erp_currency_id);


        $this->db->delete('safa_uo_contracts_settings_visa_prices');
        return $this->db->affected_rows();
    }

}

/* End of file contracts_model.php */
/* Location: ./application/models/contracts_model.php */