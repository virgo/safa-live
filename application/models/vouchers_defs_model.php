<?php

if (!defined('BASEPATH'))
    exit('No direct script usersess allowed');

/**
 * @property CI_Loader $load
 * @property CI_Form_validation $form_validation
 * @property CI_Input $input
 * @property CI_Email $email
 * @property CI_DB_active_record $db
 * @property CI_DB_forge $dbforge
 * @property CI_DB_driver $db
 * @property CI_Table $table
 * @property CI_Session $session
 * @property CI_FTP $ftp
 * ....
 */
class Vouchers_defs_model extends CI_Model {

    public $id = FALSE;
    public $firm_id = FALSE;
    public $main_type = FALSE;
    public $name_ar = FALSE;
    public $name_la = FALSE;
    public $cash_account = FALSE;
    public $currency = FALSE;
    public $rate = FALSE;
    public $notes = FALSE;
    public $limit = FALSE;
    public $offset = FALSE;

    function __construct() {
        parent::__construct();
    }

    function get($rows_no = FALSE) {
        if ($this->id)
            $this->db->where('id', $this->id);

        if ($this->main_type)
            $this->db->where('main_type', $this->main_type);

        if ($this->name_ar)
            $this->db->where('name_ar', $this->name_ar);

        if ($this->name_la)
            $this->db->where('name_la', $this->name_la);

        if ($this->cash_account)
            $this->db->where('cash_account', $this->cash_account);

        if ($this->currency)
            $this->db->where('currency', $this->currency);

        if ($this->rate)
            $this->db->where('rate', $this->rate);

        if ($this->notes)
            $this->db->where('notes', $this->notes);

        $this->db->where('firm_id', session('firm_id'));


        $query = $this->db->get('vouchers_defs', $this->limit, $this->offset);
        if ($rows_no)
            return $query->num_rows();
        else
        if ($this->id)
            return $query->row();
        else
            return $query->result();
    }

    function save() {

        if ($this->id)
            $this->db->set('id', $this->id);

        if ($this->main_type)
            $this->db->set('main_type', $this->main_type);

        if ($this->name_ar)
            $this->db->set('name_ar', $this->name_ar);

        if ($this->name_la)
            $this->db->set('name_la', $this->name_la);

        if ($this->currency)
            $this->db->set('currency', $this->currency);

        if ($this->cash_account)
            $this->db->set('cash_account', $this->cash_account);

        if ($this->rate)
            $this->db->set('rate', $this->rate);

        if ($this->notes)
            $this->db->set('notes', $this->notes);


        if ($this->firm_id == '0')
            $this->db->set('firm_id', $this->firm_id);
        else
            $this->db->set('firm_id', session('firm_id'));


        if ($this->id) {
            $this->db->where('id', $this->id)->update('vouchers_defs');
            return $this->id;
        } else {
            $this->db->insert('vouchers_defs');
            return $this->db->insert_id();
        }
    }

    function delete() {
        if ($this->firm_id)
            $this->db->where('firm_id', $this->firm_id);
        else
            $this->db->where('firm_id', session('firm_id'));
        $this->db->where('id', $this->id);
        return $this->db->delete('vouchers_defs');
    }

}

/* End of file users.php */
/* Location: ./application/models/users_model.php */