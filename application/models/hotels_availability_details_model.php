<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Hotels_availability_details_model extends CI_Model {

    public $erp_hotels_availability_detail_id = FALSE;
    public $erp_hotels_availability_master_id = FALSE;
    public $erp_period_id = FALSE;
    public $erp_housingtype_id = FALSE;
    public $erp_hotelroomsize_id = FALSE;
    public $erp_hotels_availability_view_id = FALSE;
    public $sale_price = FALSE;
    public $offer_status = FALSE;
    public $notes = FALSE;
    public $individual = FALSE;
    public $start_date = FALSE;
    public $end_date = FALSE;
    public $erp_room_service_id = FALSE;
    public $custom_select = FALSE;
    public $limit = FALSE;
    public $offset = FALSE;
    public $order_by = FALSE;
    public $join = FALSE;

    function __construct() {
        parent::__construct();
    }

    function get($rows_no = FALSE) {

        if ($this->custom_select !== FALSE) {
            $this->db->select('erp_hotels_availability_detail_id');
            $this->db->select($this->custom_select);
        }

        if ($this->erp_hotels_availability_detail_id !== FALSE)
            $this->db->where('erp_hotels_availability_detail.erp_hotels_availability_detail_id', $this->erp_hotels_availability_detail_id);

        if ($this->erp_hotels_availability_master_id !== FALSE)
            $this->db->where('erp_hotels_availability_detail.erp_hotels_availability_master_id', $this->erp_hotels_availability_master_id);

        if ($this->erp_period_id !== FALSE)
            $this->db->where('erp_hotels_availability_detail.erp_period_id', $this->erp_period_id);

        if ($this->erp_housingtype_id !== FALSE)
            $this->db->where('erp_hotels_availability_detail.erp_housingtype_id', $this->erp_housingtype_id);

        if ($this->erp_hotelroomsize_id !== FALSE)
            $this->db->where('erp_hotels_availability_detail.erp_hotelroomsize_id', $this->erp_hotelroomsize_id);

        if ($this->erp_hotels_availability_view_id !== FALSE)
            $this->db->where('erp_hotels_availability_detail.erp_hotels_availability_view_id', $this->erp_hotels_availability_view_id);

        if ($this->sale_price !== FALSE)
            $this->db->where('erp_hotels_availability_detail.sale_price', $this->sale_price);

        if ($this->offer_status !== FALSE)
            $this->db->where('erp_hotels_availability_detail.offer_status', $this->offer_status);

        if ($this->individual !== FALSE)
            $this->db->where('erp_hotels_availability_detail.individual', $this->individual);

        if ($this->start_date !== FALSE)
            $this->db->where('erp_hotels_availability_detail.start_date', $this->start_date);

        if ($this->end_date !== FALSE)
            $this->db->where('erp_hotels_availability_detail.end_date', $this->end_date);

        if ($this->erp_room_service_id !== FALSE)
            $this->db->where('erp_hotels_availability_detail.erp_room_service_id', $this->erp_room_service_id);

        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        if ($this->join) {
            $this->db->select('erp_hotels_availability_detail.*,erp_periods.' . name() . ' as period,erp_housingtypes.' . name() . ' as housingtype,erp_hotelroomsizes.' . name() . ' as roomsize');
            $this->db->join('erp_periods', 'erp_periods.erp_period_id = erp_hotels_availability_detail.erp_period_id','left');
            $this->db->join('erp_housingtypes', 'erp_housingtypes.erp_housingtype_id = erp_hotels_availability_detail.erp_housingtype_id','left');
            $this->db->join('erp_hotelroomsizes', 'erp_hotelroomsizes.erp_hotelroomsize_id = erp_hotels_availability_detail.erp_hotelroomsize_id','left');
        }

        $query = $this->db->get('erp_hotels_availability_detail');
        if ($rows_no)
            return $query->num_rows();

        if ($this->erp_hotels_availability_detail_id)
            return $query->row();
        else
            return $query->result();
    }

    function save() {
        if ($this->erp_hotels_availability_detail_id !== FALSE)
            $this->db->set('erp_hotels_availability_detail.erp_hotels_availability_detail_id', $this->erp_hotels_availability_detail_id);

        if ($this->erp_hotels_availability_master_id !== FALSE)
            $this->db->set('erp_hotels_availability_detail.erp_hotels_availability_master_id', $this->erp_hotels_availability_master_id);

        if ($this->erp_period_id !== FALSE)
            $this->db->set('erp_hotels_availability_detail.erp_period_id', $this->erp_period_id);

        if ($this->erp_housingtype_id !== FALSE)
            $this->db->set('erp_hotels_availability_detail.erp_housingtype_id', $this->erp_housingtype_id);

        if ($this->erp_hotelroomsize_id !== FALSE)
            $this->db->set('erp_hotels_availability_detail.erp_hotelroomsize_id', $this->erp_hotelroomsize_id);

        if ($this->erp_hotels_availability_view_id !== FALSE)
            $this->db->set('erp_hotels_availability_detail.erp_hotels_availability_view_id', $this->erp_hotels_availability_view_id);

        if ($this->sale_price !== FALSE)
            $this->db->set('erp_hotels_availability_detail.sale_price', $this->sale_price);

        if ($this->offer_status !== FALSE)
            $this->db->set('erp_hotels_availability_detail.offer_status', $this->offer_status);

        if ($this->notes !== FALSE)
            $this->db->set('erp_hotels_availability_detail.notes', $this->notes);

        if ($this->individual !== FALSE)
            $this->db->set('erp_hotels_availability_detail.individual', $this->individual);

        if ($this->start_date !== FALSE)
            $this->db->set('erp_hotels_availability_detail.start_date', $this->start_date);

        if ($this->end_date !== FALSE)
            $this->db->set('erp_hotels_availability_detail.end_date', $this->end_date);

        if ($this->erp_room_service_id !== FALSE)
            $this->db->set('erp_hotels_availability_detail.erp_room_service_id', $this->erp_room_service_id);

        if ($this->erp_hotels_availability_detail_id) {
            $this->db->where('erp_hotels_availability_detail.erp_hotels_availability_detail_id', $this->erp_hotels_availability_detail_id)->update('erp_hotels_availability_detail');
        } else {
            $this->db->insert('erp_hotels_availability_detail');
            return $this->db->insert_id();
        }
    }

    function delete() {
        if ($this->erp_hotels_availability_detail_id !== FALSE)
            $this->db->where('erp_hotels_availability_detail.erp_hotels_availability_detail_id', $this->erp_hotels_availability_detail_id);

        if ($this->erp_hotels_availability_master_id !== FALSE)
            $this->db->where('erp_hotels_availability_detail.erp_hotels_availability_master_id', $this->erp_hotels_availability_master_id);

        if ($this->erp_period_id !== FALSE)
            $this->db->where('erp_hotels_availability_detail.erp_period_id', $this->erp_period_id);

        if ($this->erp_housingtype_id !== FALSE)
            $this->db->where('erp_hotels_availability_detail.erp_housingtype_id', $this->erp_housingtype_id);

        if ($this->erp_hotelroomsize_id !== FALSE)
            $this->db->where('erp_hotels_availability_detail.erp_hotelroomsize_id', $this->erp_hotelroomsize_id);

        if ($this->erp_hotels_availability_view_id !== FALSE)
            $this->db->where('erp_hotels_availability_detail.erp_hotels_availability_view_id', $this->erp_hotels_availability_view_id);

        if ($this->sale_price !== FALSE)
            $this->db->where('erp_hotels_availability_detail.sale_price', $this->sale_price);

        if ($this->offer_status !== FALSE)
            $this->db->where('erp_hotels_availability_detail.offer_status', $this->offer_status);

        if ($this->individual !== FALSE)
            $this->db->where('erp_hotels_availability_detail.individual', $this->individual);

        $this->db->delete('erp_hotels_availability_detail');
        return $this->db->affected_rows();
    }

    function search($rows_no = FALSE) {
        if ($this->custom_select !== FALSE) {
            $this->db->select('erp_hotels_availability_detail_id');
            $this->db->select($this->custom_select);
        }

        if ($this->erp_hotels_availability_detail_id !== FALSE)
            $this->db->where('erp_hotels_availability_detail.erp_hotels_availability_detail_id', $this->erp_hotels_availability_detail_id);

        if ($this->erp_hotels_availability_master_id !== FALSE)
            $this->db->where('erp_hotels_availability_detail.erp_hotels_availability_master_id', $this->erp_hotels_availability_master_id);

        if ($this->erp_period_id !== FALSE)
            $this->db->where('erp_hotels_availability_detail.erp_period_id', $this->erp_period_id);

        if ($this->erp_housingtype_id !== FALSE)
            $this->db->where('erp_hotels_availability_detail.erp_housingtype_id', $this->erp_housingtype_id);

        if ($this->erp_hotelroomsize_id !== FALSE)
            $this->db->where('erp_hotels_availability_detail.erp_hotelroomsize_id', $this->erp_hotelroomsize_id);

        if ($this->erp_hotels_availability_view_id !== FALSE)
            $this->db->where('erp_hotels_availability_detail.erp_hotels_availability_view_id', $this->erp_hotels_availability_view_id);

        if ($this->sale_price !== FALSE)
            $this->db->where('erp_hotels_availability_detail.sale_price', $this->sale_price);

        if ($this->offer_status !== FALSE)
            $this->db->where('erp_hotels_availability_detail.offer_status', $this->offer_status);

        if ($this->individual !== FALSE)
            $this->db->where('erp_hotels_availability_detail.individual', $this->individual);

        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('erp_hotels_availability_detail');
        if ($rows_no)
            return $query->num_rows();

        return $query->result();
    }

}

/* End of file hotels_availability_details_model.php */
/* Location: ./application/models/hotels_availability_details_model.php */