<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Poll_model extends CI_Model {

    public $id = FALSE;
    public $title = FALSE;
    public $start_date = FALSE;
    public $end_date = FALSE;
    public $time = FALSE;
    public $tell = FALSE;
    public $number = FALSE;
    public $email = FALSE;
    public $password = FALSE;
    public $textarea = FALSE;
    public $multi = FALSE;
    public $select = FALSE;
    public $checkbox = FALSE;
    public $radiobuttons = FALSE;
    public $custom_select = FALSE;
    public $limit = FALSE;
    public $offset = FALSE;
    public $order_by = FALSE;

    function __construct() {
        parent::__construct();
    }

    function get($rows_no = FALSE) {

        if ($this->custom_select !== FALSE) {
            $this->db->select('id');
            $this->db->select($this->custom_select);
        }
        if ($this->id !== FALSE)
            $this->db->where('id', $this->id);

        if ($this->title !== FALSE)
            $this->db->where('title', $this->title);

        if ($this->start_date !== FALSE)
            $this->db->where('start_date', $this->start_date);

        if ($this->end_date !== FALSE)
            $this->db->where('end_date', $this->end_date);

        if ($this->time !== FALSE)
            $this->db->where('time', $this->time);

        if ($this->tell !== FALSE)
            $this->db->where('tell', $this->tell);

        if ($this->number !== FALSE)
            $this->db->where('number', $this->number);

        if ($this->email !== FALSE)
            $this->db->where('email', $this->email);

        if ($this->password !== FALSE)
            $this->db->where('password', $this->password);

        if ($this->textarea !== FALSE)
            $this->db->where('textarea', $this->textarea);

        if ($this->multi !== FALSE)
            $this->db->where('multi', $this->multi);

        if ($this->select !== FALSE)
            $this->db->where('drop', $this->select);

        if ($this->radiobuttons !== FALSE)
            $this->db->where('radio', $this->radiobuttons);

        if ($this->checkbox !== FALSE)
            $this->db->where('box', $this->checkbox);

        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('polls');
        if ($rows_no)
            return $query->num_rows();

        if ($this->id)
            return $query->row();
        else
            return $query->result();
    }

    function save() {
        if ($this->title)
            $this->db->set('title', $this->title);

        if ($this->start_date)
            $this->db->set('start_date', $this->start_date);

        if ($this->end_date)
            $this->db->set('end_date', $this->end_date);

        if ($this->time)
            $this->db->set('time', $this->time);

        if ($this->tell)
            $this->db->set('tell', $this->tell);

        if ($this->number)
            $this->db->set('number', $this->number);

        if ($this->email)
            $this->db->set('email', $this->email);

        if ($this->password)
            $this->db->set('password', $this->password);

        if ($this->textarea)
            $this->db->set('textarea', $this->textarea);

        if ($this->multi)
            $this->db->set('multi', $this->multi);

        if ($this->select !== FALSE)
            $this->db->set('drop', $this->select);

        if ($this->radiobuttons !== FALSE)
            $this->db->set('radio', $this->radiobuttons);

        if ($this->checkbox !== FALSE)
            $this->db->set('box', $this->checkbox);


        if ($this->id) {
            $this->db->where('id', $this->id)->update('polls');
        } else {
            $this->db->insert('polls');
            return $this->db->insert_id();
        }
    }

    function delete() {
        if ($this->id !== FALSE)
            $this->db->where('id', $this->id);

        if ($this->title !== FALSE)
            $this->db->where('title', $this->title);

        if ($this->start_date !== FALSE)
            $this->db->where('start_date', $this->start_date);

        if ($this->end_date !== FALSE)
            $this->db->where('end_date', $this->end_date);

        if ($this->time !== FALSE)
            $this->db->where('time', $this->time);

        if ($this->tell)
            $this->db->where('tell', $this->tell);

        if ($this->number !== FALSE)
            $this->db->where('number', $this->number);

        if ($this->email !== FALSE)
            $this->db->where('email', $this->email);

        if ($this->password !== FALSE)
            $this->db->where('password', $this->password);

        if ($this->textarea !== FALSE)
            $this->db->where('textarea', $this->textarea);

        if ($this->multi !== FALSE)
            $this->db->where('multi', $this->multi);

        if ($this->select !== FALSE)
            $this->db->where('drop', $this->select);

        if ($this->radiobuttons !== FALSE)
            $this->db->where('radio', $this->radiobuttons);

        if ($this->checkbox !== FALSE)
            $this->db->where('box', $this->checkbox);

        $this->db->delete('polls');
        return $this->db->affected_rows();
    }

    function search($rows_no = FALSE) {
        if ($this->custom_select) {
            $this->db->select('id');
            $this->db->select($this->custom_select);
        }
        if ($this->id)
            $this->db->like('id', $this->id);

        if ($this->title)
            $this->db->like('title', $this->title);

        if ($this->start_date)
            $this->db->or_like('start_date', $this->start_date);

        if ($this->end_date)
            $this->db->or_like('end_date', $this->end_date);

        if ($this->time)
            $this->db->or_like('time', $this->time);

        if ($this->tell)
            $this->db->or_like('tell', $this->tell);

        if ($this->number)
            $this->db->or_like('number', $this->number);

        if ($this->email)
            $this->db->or_like('email', $this->email);

        if ($this->password)
            $this->db->or_like('password', $this->password);

        if ($this->textarea)
            $this->db->or_like('textarea', $this->textarea);

        if ($this->multi)
            $this->db->or_like('multi', $this->multi);

        if ($this->select)
            $this->db->or_like('drop', $this->select);

        if ($this->radiobuttons !== FALSE)
            $this->db->or_like('radio', $this->radiobuttons);

        if ($this->checkbox !== FALSE)
            $this->db->or_like('box', $this->checkbox);

        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('polls');
        if ($rows_no)
            return $query->num_rows();

        return $query->result();
    }

}

/* End of file poll_model.php */
/* Location: ./application/models/poll_model.php */