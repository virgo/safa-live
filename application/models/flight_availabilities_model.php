<?php

/**
 * Flight Availabilities Search Query
 */
class Flight_availabilities_model extends CI_Model {

    //Details.
    public $safa_ea_id = FALSE;
    public $flight_number = FALSE;
    public $flight_date = FALSE;
    public $arrival_date = FALSE;
    
    public $erp_flight_availabilities_detail_id = FALSE;
    //safa_trip_externalsegments_availabilities
    public $safa_trip_externalsegments_availability_id = FALSE;
    public $safa_trip_externalsegment_id = FALSE;
    public $seats_used_count = FALSE;
    //By Gouda, For Trips
    public $erp_flight_availability_id = FALSE;
    public $start_erp_country_id = FALSE;
    public $end_erp_country_id = FALSE;
    public $erp_path_type_id = FALSE;
    public $safa_trip_id = FALSE;
    public $safa_group_passport_ids_arr = FALSE;
    public $pnr = FALSE;
    
    
    public $custom_select = FALSE;
    public $limit = FALSE;
    public $offset = FALSE;
    public $order_by = FALSE;
    public $join = FALSE;

    function __construct() {
        parent::__construct();
    }

    function get_for_trip($rows_no = FALSE) {

        if ($this->custom_select !== FALSE) {
            $this->db->select('erp_flight_availability_id');
            $this->db->select($this->custom_select);
        }

        
        if ($this->erp_flight_availability_id !== FALSE)
            $this->db->where('erp_flight_availabilities.erp_flight_availability_id', $this->erp_flight_availability_id);
        
        if ($this->safa_ea_id !== FALSE)
            $this->db->where('erp_flight_availabilities.safa_ea_id', $this->safa_ea_id);

        if ($this->safa_trip_id !== FALSE)
            $this->db->where('erp_flight_availabilities.safa_trip_id', $this->safa_trip_id);

        if ($this->pnr !== FALSE)
            $this->db->where('erp_flight_availabilities.pnr', $this->pnr);
            
        if ($this->erp_path_type_id !== FALSE)
            $this->db->where('erp_flight_availabilities_detail.erp_path_type_id', $this->erp_path_type_id);

        if ($this->flight_number !== FALSE)
            $this->db->where('erp_flight_availabilities_detail.flight_number', $this->flight_number);

        if ($this->flight_date !== FALSE)
            $this->db->where('erp_flight_availabilities_detail.flight_date', $this->flight_date);

        //By Gouda, TO execlude private PNRs    
        $this->db->where('erp_flight_availabilities.private is null ');

        if ($this->order_by && is_array($this->order_by)) {
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);
        } else {
            $this->db->order_by('erp_flight_availabilities.erp_flight_availability_id', 'desc');
        }


        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $this->db->distinct();
        $this->db->select("erp_flight_availabilities.*,  erp_flight_availabilities_detail.seats_count as available_seats_count
        , safa_transporters.safa_transporter_id,  safa_transporters." . name() . " as safa_transporter_name", FALSE);

        $this->db->from('erp_flight_availabilities');

        $this->db->join('erp_flight_availabilities_detail', 'erp_flight_availabilities.erp_flight_availability_id = erp_flight_availabilities_detail.erp_flight_availability_id', 'left');
        $this->db->join(FSDB . '.fs_airlines', FSDB . '.fs_airlines.fs_airline_id = erp_flight_availabilities_detail.erp_airline_id', 'left');
        $this->db->join('safa_transporters', 'erp_flight_availabilities_detail.airline_code = safa_transporters.code', 'left');
		$this->db->join('safa_externaltriptypes',  ' safa_externaltriptypes.safa_externaltriptype_id  = erp_flight_availabilities_detail.safa_externaltriptype_id', 'left');
        
        //By Gouda
        //$this->db->group_by('erp_flight_availabilities_detail.flight_number, erp_flight_availabilities_detail.flight_date, erp_flight_availabilities_detail.departure_time, erp_flight_availabilities_detail.arrival_time, erp_flight_availabilities_detail.erp_port_id_from, erp_flight_availabilities_detail.erp_port_id_to , erp_flight_availabilities_detail.erp_airline_id');
        $this->db->group_by('erp_flight_availabilities.erp_flight_availability_id');

        $query = $this->db->get();

        //echo $query_text=$this->db->last_query(); exit;

        if ($rows_no)
            return $query->num_rows();

        if ($this->erp_flight_availabilities_detail_id)
            return $query->row();
        else
            return $query->result();
    }

    function get_for_safa_group_passports($rows_no = FALSE) {

        if ($this->custom_select !== FALSE) {
            $this->db->select('erp_flight_availability_id');
            $this->db->select($this->custom_select);
        }

        if ($this->safa_ea_id !== FALSE)
            $this->db->where('erp_flight_availabilities.safa_ea_id', $this->safa_ea_id);

        if ($this->safa_trip_id !== FALSE)
            $this->db->where('erp_flight_availabilities.safa_trip_id', $this->safa_trip_id);

        if ($this->erp_path_type_id !== FALSE)
            $this->db->where('erp_flight_availabilities_detail.erp_path_type_id', $this->erp_path_type_id);

        if ($this->flight_number !== FALSE)
            $this->db->where('erp_flight_availabilities_detail.flight_number', $this->flight_number);

        if ($this->flight_date !== FALSE)
            $this->db->where('erp_flight_availabilities_detail.flight_date', $this->flight_date);

        if ($this->safa_group_passport_ids_arr !== FALSE) {
            $this->db->where_in('safa_group_passports.safa_group_passport_id', $this->safa_group_passport_ids_arr);
        }

        //By Gouda, TO execlude private PNRs    
        $this->db->where('erp_flight_availabilities.private is null ');

        $this->db->having('available_seats_count > 0');


        if ($this->order_by && is_array($this->order_by)) {
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);
        } else {
            $this->db->order_by('erp_flight_availabilities.erp_flight_availability_id', 'desc');
        }


        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $this->db->distinct();
        $this->db->select("erp_flight_availabilities.*, (erp_flight_availabilities_detail.seats_count - (select count(*) from safa_group_passports_flight_availabilties where safa_group_passports_flight_availabilties.erp_flight_availabilty_id =  erp_flight_availabilities.erp_flight_availability_id)) as available_seats_count
        , safa_transporters.safa_transporter_id,  safa_transporters." . name() . " as safa_transporter_name", FALSE);

        $this->db->from('erp_flight_availabilities');

        $this->db->join('erp_flight_availabilities_detail', 'erp_flight_availabilities.erp_flight_availability_id = erp_flight_availabilities_detail.erp_flight_availability_id', 'left');
        $this->db->join(FSDB . '.fs_airlines', FSDB . '.fs_airlines.fs_airline_id = erp_flight_availabilities_detail.erp_airline_id');
        $this->db->join('safa_transporters', FSDB . '.fs_airlines.iata = safa_transporters.code');

        $this->db->join('safa_group_passports', 'safa_group_passports.safa_trip_id = erp_flight_availabilities.safa_trip_id');



        //By Gouda
        //$this->db->group_by('erp_flight_availabilities_detail.flight_number, erp_flight_availabilities_detail.flight_date, erp_flight_availabilities_detail.departure_time, erp_flight_availabilities_detail.arrival_time, erp_flight_availabilities_detail.erp_port_id_from, erp_flight_availabilities_detail.erp_port_id_to , erp_flight_availabilities_detail.erp_airline_id');
        $this->db->group_by('erp_flight_availabilities.erp_flight_availability_id');

        $query = $this->db->get();

        //echo $query_text=$this->db->last_query(); exit;

        if ($rows_no)
            return $query->num_rows();

        if ($this->erp_flight_availabilities_detail_id)
            return $query->row();
        else
            return $query->result();
    }

    function get_details_for_trip($rows_no = FALSE) {

        if ($this->erp_flight_availability_id !== FALSE)
            $this->db->where('erp_flight_availabilities.erp_flight_availability_id', $this->erp_flight_availability_id);

        if ($this->safa_trip_id !== FALSE)
            $this->db->where('erp_flight_availabilities.safa_trip_id', $this->safa_trip_id);

        if ($this->erp_path_type_id !== FALSE)
            $this->db->where('erp_flight_availabilities_detail.erp_path_type_id', $this->erp_path_type_id);


        if ($this->safa_ea_id !== FALSE)
            $this->db->where('erp_flight_availabilities.safa_ea_id', $this->safa_ea_id);

        if ($this->flight_number !== FALSE)
            $this->db->where('erp_flight_availabilities_detail.flight_number', $this->flight_number);

        if ($this->flight_date !== FALSE)
            $this->db->where('erp_flight_availabilities_detail.flight_date', $this->flight_date);

         if ($this->arrival_date !== FALSE)
            $this->db->where('erp_flight_availabilities_detail.arrival_date', $this->arrival_date);
            
        if ($this->start_erp_country_id !== FALSE)
            $this->db->where('start_ports.erp_country_id', $this->start_erp_country_id);

        if ($this->end_erp_country_id !== FALSE)
            $this->db->where('end_ports.erp_country_id', $this->end_erp_country_id);


        if ($this->order_by && is_array($this->order_by)) {
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);
        } else {
            $this->db->order_by('erp_flight_availabilities_detail.flight_date, erp_flight_availabilities_detail.departure_time', 'asc');
        }

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $this->db->select("erp_flight_availabilities.erp_flight_availability_id, erp_flight_availabilities_detail.erp_flight_availabilities_detail_id,
		erp_flight_availabilities_detail.flight_number, erp_flight_availabilities_detail.flight_date, erp_flight_availabilities_detail.arrival_date, (SELECT iata FROM ". FSDB .".`fs_airlines` WHERE ". FSDB .".fs_airlines.fs_airline_id = erp_flight_availabilities_detail.erp_airline_id) AS airline,
		erp_flight_availabilities_detail.departure_time, erp_flight_availabilities_detail.arrival_time, 
		erp_flight_availabilities_detail.erp_port_id_from, erp_flight_availabilities_detail.erp_port_hall_id_from, erp_flight_availabilities_detail.erp_port_id_to, erp_flight_availabilities_detail.erp_port_hall_id_to, erp_flight_availabilities_detail.erp_path_type_id,
        erp_flight_availabilities.safa_ea_id, erp_flight_availabilities_detail.safa_externaltriptype_id, safa_externaltriptypes.".name()." as safa_externaltriptype_name, erp_path_types.".name()." as erp_path_type_name, 
        (sum(erp_flight_availabilities_detail.seats_count) - IFNULL((select sum(safa_trip_externalsegments_availabilities.seats_used_count) from safa_trip_externalsegments_availabilities where safa_trip_externalsegments_availabilities.erp_flight_availabilities_detail_id = erp_flight_availabilities_detail.erp_flight_availabilities_detail_id group by safa_trip_externalsegments_availabilities.erp_flight_availabilities_detail_id),0 )) as available_seats_count, 
        start_ports." . name() . " as start_ports_name, end_ports." . name() . " as end_ports_name, start_port_halls." . name() . " as start_port_halls_name, safa_transporters.safa_transporter_id,  safa_transporters.code as safa_transporter_code,  safa_transporters." . name() . " as safa_transporter_name,  erp_flight_status.name as erp_flight_status_name,  erp_flight_classes.name as erp_flight_classes_name", FALSE);

        $this->db->join('erp_flight_availabilities', 'erp_flight_availabilities.erp_flight_availability_id = erp_flight_availabilities_detail.erp_flight_availability_id', 'left');

        $this->db->join('safa_trip_externalsegments_availabilities', 'safa_trip_externalsegments_availabilities.safa_trip_externalsegments_availability_id = erp_flight_availabilities_detail.erp_flight_availabilities_detail_id', 'left');


        $this->db->join('erp_port_halls as start_port_halls', 'erp_flight_availabilities_detail.erp_port_hall_id_from = start_port_halls.erp_port_hall_id', 'left');
        $this->db->join('erp_port_halls as end_port_halls', 'erp_flight_availabilities_detail.erp_port_hall_id_to = end_port_halls.erp_port_hall_id', 'left');
        $this->db->join('erp_ports as start_ports', 'erp_flight_availabilities_detail.erp_port_id_from = start_ports.erp_port_id', 'left');
        $this->db->join('erp_ports as end_ports', 'erp_flight_availabilities_detail.erp_port_id_to = end_ports.erp_port_id', 'left');

        $this->db->join(FSDB . '.fs_airlines', FSDB . '.fs_airlines.fs_airline_id = erp_flight_availabilities_detail.erp_airline_id', 'left');
        $this->db->join('safa_transporters', 'erp_flight_availabilities_detail.airline_code = safa_transporters.code', 'left');
		$this->db->join('safa_externaltriptypes',  ' safa_externaltriptypes.safa_externaltriptype_id  = erp_flight_availabilities_detail.safa_externaltriptype_id', 'left');
        
		$this->db->join('erp_path_types',  ' erp_path_types.erp_path_type_id  = erp_flight_availabilities_detail.erp_path_type_id', 'left');
        
		$this->db->join('erp_flight_status',  ' erp_flight_status.erp_flight_status_id  = erp_flight_availabilities_detail.erp_flight_status_id', 'left');
        $this->db->join('erp_flight_classes',  ' erp_flight_classes.erp_flight_class_id  = erp_flight_availabilities_detail.erp_flight_class_id', 'left');
        
        $this->db->group_by('erp_flight_availabilities_detail.flight_number, erp_flight_availabilities_detail.flight_date, erp_flight_availabilities_detail.departure_time, erp_flight_availabilities_detail.arrival_time, erp_flight_availabilities_detail.erp_port_id_from, erp_flight_availabilities_detail.erp_port_id_to , erp_flight_availabilities_detail.erp_airline_id');

        $query = $this->db->get('erp_flight_availabilities_detail');

        //echo $query_text=$this->db->last_query(); exit;

        if ($rows_no)
            return $query->num_rows();

        if ($this->erp_flight_availabilities_detail_id)
            return $query->row();
        else
            return $query->result();
    }

    function save_trip_externalsegments_availabilities() {

        if ($this->safa_trip_externalsegments_availability_id !== false) {
            $this->db->set('safa_trip_externalsegments_availabilities.owner_erp_company_types_id', $this->owner_erp_company_types_id);
        }
        if ($this->erp_flight_availabilities_detail_id !== false) {
            $this->db->set('safa_trip_externalsegments_availabilities.erp_flight_availabilities_detail_id', $this->erp_flight_availabilities_detail_id);
        }
        if ($this->safa_trip_externalsegment_id !== false) {
            $this->db->set('safa_trip_externalsegments_availabilities.safa_trip_externalsegment_id', $this->safa_trip_externalsegment_id);
        }
        if ($this->seats_used_count !== false) {
            $this->db->set('safa_trip_externalsegments_availabilities.seats_used_count', $this->seats_used_count);
        }

        if ($this->safa_trip_externalsegments_availability_id) {
            $this->db->where('safa_trip_externalsegments_availabilities.safa_trip_externalsegments_availability_id', $this->safa_trip_externalsegments_availability_id)->update('safa_trip_externalsegments_availabilities');
        } else {
            $this->db->insert('safa_trip_externalsegments_availabilities');
            return $this->db->insert_id();
        }
    }

    function save() {
        if ($this->erp_flight_availability_id !== false) {
            $this->db->set('erp_flight_availabilities.erp_flight_availability_id', $this->erp_flight_availability_id);
        }
        if ($this->safa_trip_id !== false) {
            $this->db->set('erp_flight_availabilities.safa_trip_id', $this->safa_trip_id);
        }


        if ($this->erp_flight_availability_id) {
            $this->db->where('erp_flight_availabilities.erp_flight_availability_id', $this->erp_flight_availability_id)->update('erp_flight_availabilities');
        } else {
            $this->db->insert('erp_flight_availabilities');
            return $this->db->insert_id();
        }
    }

    function get($rows_no = FALSE) {
        if ($this->erp_flight_availability_id !== false) {
            $this->db->where('erp_flight_availabilities.erp_flight_availability_id', $this->erp_flight_availability_id);
        }
        if ($this->safa_trip_id !== false) {
            $this->db->where('erp_flight_availabilities.safa_trip_id', $this->safa_trip_id);
        }


        if ($this->order_by && is_array($this->order_by)) {
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);
        } else {
            $this->db->order_by('erp_flight_availability_id');
        }

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('erp_flight_availabilities');
        if ($rows_no)
            return $query->num_rows();

        if ($this->erp_flight_availability_id)
            return $query->row();
        else
            return $query->result();
    }

    public function get_airlines($airline) {
        $get_airline = $this->db->where('iata', $airline)->get(FSDB . '.fs_airlines')->row();
        if ($get_airline)
            return $get_airline->fs_airline_id;
        else
        {
            $this->db->insert(FSDB . '.fs_airlines', array('iata' => $airline));
            return $this->db->insert_id();
        }
    }
    
    function get_airport($port) {
        $portinfo = $this->db->where('code', $port)->get('erp_ports')->row();
        if($portinfo)
            return $portinfo;
        else
        {
            $this->db->insert('erp_ports', array('code' => $port));
            return $this->db->where('code', $this->db->insert_id())->get('erp_ports')->row();
        }
    }

	function delete($rows_no = FALSE) 
	{
        if ($this->erp_flight_availability_id !== false) {
            $this->db->where('erp_flight_availabilities.erp_flight_availability_id', $this->erp_flight_availability_id);
        }
        
        if ($this->safa_trip_id !== false) {
            $this->db->where('erp_flight_availabilities.safa_trip_id', $this->safa_trip_id);
        }

        $this->db->delete('erp_flight_availabilities');
        return $this->db->affected_rows();
    }
}
