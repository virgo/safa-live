<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Erp_languages_model extends CI_Model {

    public $erp_language_id = FALSE;
    public $name = FALSE;
    public $custom_select = FALSE;
    public $limit = FALSE;
    public $offset = FALSE;
    public $order_by = FALSE;

    function __construct() {
        parent::__construct();
    }

    function get($rows_no = FALSE) {

        if ($this->custom_select !== FALSE) {
            $this->db->select('erp_language_id');
            $this->db->select($this->custom_select);
        }

        if ($this->erp_language_id !== FALSE)
            $this->db->where('erp_languages.erp_language_id', $this->erp_language_id);

        if ($this->name !== FALSE)
            $this->db->where('erp_languages.name', $this->name);


        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('erp_languages');
        if ($rows_no)
            return $query->num_rows();

        if ($this->erp_language_id)
            return $query->row();
        else
            return $query->result();
    }

    function get_file_lang($langid = FALSE, $langfileid = FALSE) {
        $query = "SELECT safa_language_keys.safa_language_key_id,safa_language_keys.safa_language_key_name,
safa_languages_phrases.phrase FROM safa_languages_phrases JOIN
erp_languages ON erp_languages.erp_language_id = safa_languages_phrases.safa_language_id
JOIN safa_language_keys ON safa_language_keys.safa_language_key_id = safa_languages_phrases.safa_language_key_id
JOIN safa_languages_files ON safa_languages_files.safa_file_id = safa_language_keys.safa_languages_file_id
WHERE erp_languages.erp_language_id = '$langid' ";
        if ($langfileid)
            $query .= " && safa_languages_files.safa_file_id = '$langfileid' ";
        $values = $this->db->query($query)->result();
        $returnarray = array();
        $returnkeys = array();
        foreach ($values as $row) {
            $returnarray[$row->safa_language_key_id] = $row->phrase;
            $returnkeys[$row->safa_language_key_id] = $row->safa_language_key_name;
        }
        return array('phrases' => $returnarray, 'keys' => $returnkeys);
    }

}

/* End of file erp_languages_model.php */
/* Location: ./application/models/erp_languages_model.php */