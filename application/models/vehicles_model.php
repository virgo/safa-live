<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Vehicles_model extends CI_Model {

    public $safa_vehicle_id = FALSE;
    public $name_la = FALSE;
    public $name_ar = FALSE;
    public $custom_select = FALSE;
    public $order_by = FALSE;
    public $limit = FALSE;
    public $offset = FALSE;

    function __construct() {
        parent::__construct();
    }

    function get($rows_no = FALSE) {

        if ($this->custom_select !== FALSE) {
            $this->db->select('safa_vehicle_id');
            $this->db->select($this->custom_select);
        }

        if ($this->safa_vehicle_id !== FALSE)
            $this->db->where('safa_vehicles.safa_vehicle_id', $this->safa_vehicle_id);

        if ($this->name_ar !== FALSE)
            $this->db->where('safa_vehicles.name_ar', $this->name_ar);

        if ($this->name_la !== FALSE)
            $this->db->where('safa_vehicles.name_la', $this->name_la);


        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);
        $query = $this->db->get('safa_vehicles');
        if ($rows_no)
            return $query->num_rows();

        if ($this->safa_vehicle_id)
            return $query->row();
        else
            return $query->result();
    }

    function save() {
        if ($this->safa_vehicle_id !== FALSE)
            $this->db->set('safa_vehicles.safa_vehicle_id', $this->safa_vehicle_id);


        if ($this->name_ar !== FALSE)
            $this->db->set('safa_vehicles.name_ar', $this->name_ar);

        if ($this->name_la !== FALSE)
            $this->db->set('safa_vehicles.name_la', $this->name_la);

        if ($this->safa_vehicle_id) {
            $this->db->where('safa_vehicles.safa_vehicle_id', $this->safa_vehicle_id)->update('safa_vehicles');
        } else {
            $this->db->insert('safa_vehicles');
            return $this->db->insert_id();
        }
    }

    function delete() {
        if ($this->safa_vehicle_id !== FALSE)
            $this->db->where('safa_vehicles.safa_vehicle_id', $this->safa_vehicle_id);

        if ($this->name_ar !== FALSE)
            $this->db->where('safa_vehicles.name_ar', $this->name_ar);

        if ($this->name_la !== FALSE)
            $this->db->where('safa_vehicles.name_la', $this->name_la);

        $this->db->delete('safa_vehicles');
        return $this->db->affected_rows();
    }

    function search($rows_no = FALSE) {
        if ($this->custom_select !== FALSE) {
            $this->db->select('safa_vehicle_id');
            $this->db->select($this->custom_select);
        }

        if ($this->safa_vehicle_id !== FALSE)
            $this->db->where('safa_vehicles.safa_vehicle_id', $this->safa_vehicle_id);


        if ($this->name_ar !== FALSE)
            $this->db->where('safa_vehicles.name_ar', $this->name_ar);

        if ($this->name_la !== FALSE)
            $this->db->where('safa_vehicles.name_la', $this->name_la);


        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('safa_vehicles');
        if ($rows_no)
            return $query->num_rows();

        return $query->result();
    }

}

/* End of file ports_model.php */
/* Location: ./application/models/ports_model.php */