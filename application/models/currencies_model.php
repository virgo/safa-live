<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class currencies_model extends CI_Model {

    public $erp_currency_id = FALSE;
    public $name_ar = FALSE;
    public $name_la = FALSE;
    public $short_name = FALSE;
    public $symbol = FALSE;
    public $short_name_ar = FALSE;
    public $timestamp = FALSE;
    public $limit = FALSE;
    public $offset = FALSE;

    function __construct() {
        parent::__construct();
    }

    function get($rows_no = FALSE) {

        if ($this->erp_currency_id !== FALSE)
            $this->db->where('erp_currency_id', $this->erp_currency_id);

        if ($this->name_ar !== FALSE)
            $this->db->where('name_ar', $this->name_ar);

        if ($this->name_la !== FALSE)
            $this->db->where('name_la', $this->name_la);

        if ($this->short_name !== FALSE)
            $this->db->where('short_name', $this->short_name);

        if ($this->symbol !== FALSE)
            $this->db->where('symbol', $this->symbol);


        if ($this->timestamp)
            $this->db->where('timestamp', $this->timestamp);

        if (!$rows_no)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('erp_currencies');
        if ($rows_no)
            return $query->num_rows();

        if ($this->erp_currency_id)
            return $query->row();
        else
            return $query->result();
    }

    function save() {

        if ($this->name_ar !== FALSE)
            $this->db->set('name_ar', $this->name_ar);

        if ($this->name_la !== FALSE)
            $this->db->set('name_la', $this->name_la);

        if ($this->short_name !== FALSE)
            $this->db->set('short_name', $this->short_name);

        if ($this->symbol !== FALSE)
            $this->db->set('symbol', $this->symbol);

        if ($this->short_name_ar !== FALSE)
            $this->db->set('short_name_ar', $this->short_name_ar);

        if ($this->timestamp)
            $this->db->set('timestamp', $this->timestamp);

        if ($this->erp_currency_id) {
            $this->db->where('erp_currency_id', $this->erp_currency_id)->update('erp_currencies');
            return $this->erp_currency_id;
        } else {
            $this->db->insert('erp_currencies');
            return $this->db->insert_id();
        }
    }

    function delete() {

        if ($this->id !== FALSE)
            $this->db->where('id', $this->id);

        if ($this->name_ar !== FALSE)
            $this->db->where('name_ar', $this->name_ar);

        if ($this->name_la !== FALSE)
            $this->db->where('name_la', $this->name_la);

        if ($this->short_name !== FALSE)
            $this->db->where('short_name', $this->short_name);

        if ($this->symbol !== FALSE)
            $this->db->where('symbol', $this->symbol);


        $this->db->delete('erp_currencies');
        return $this->db->affected_rows();
    }

    function search() {

        if ($this->name_ar !== FALSE)
            $this->db->or_like('name_ar', $this->name_ar);

        if ($this->name_la !== FALSE)
            $this->db->or_like('name_la', $this->name_la);

        if ($this->symbol !== FALSE)
            $this->db->or_like('symbol', $this->symbol);

        if ($this->timestamp)
            $this->db->or_like('timestamp', $this->timestamp);

        $query = $this->db->get('erp_currencies');
        return $query->result();
    }

    function erp_currencies() {

        if ($this->name_la)
            $this->db->where('name_la', $this->name_la);


        $query = $this->db->get('erp_currencies');
        return $query->result();
    }

}

/* End of file erp_currencies.php */
/* Location: ./application/models/erp_currencies_model.php */