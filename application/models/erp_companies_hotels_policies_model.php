<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Erp_companies_hotels_policies_model extends CI_Model {

    public $erp_companies_hotels_policies_id = FALSE;
    public $erp_hotels_policies_id = FALSE;
    public $policy_description = FALSE;
    public $policy_field_type = FALSE;
    public $erp_companies_hotels_id = FALSE;
    public $custom_select = FALSE;
    public $limit = FALSE;
    public $offset = FALSE;
    public $order_by = FALSE;

    function __construct() {
        parent::__construct();
    }

    function get($rows_no = FALSE) {

        if ($this->custom_select !== FALSE) {
            $this->db->select('erp_companies_hotels_policies_id');
            $this->db->select($this->custom_select);
        }

        if ($this->erp_companies_hotels_policies_id !== FALSE)
            $this->db->where('erp_companies_hotels_policies.erp_companies_hotels_policies_id', $this->erp_companies_hotels_policies_id);

        if ($this->erp_hotels_policies_id !== FALSE)
            $this->db->where('erp_companies_hotels_policies.erp_hotels_policies_id', $this->erp_hotels_policies_id);

        if ($this->policy_description !== FALSE)
            $this->db->where('erp_companies_hotels_policies.policy_description', $this->policy_description);

        if ($this->policy_field_type !== FALSE)
            $this->db->where('erp_companies_hotels_policies.policy_field_type', $this->policy_field_type);

        if ($this->erp_companies_hotels_id !== FALSE)
            $this->db->where('erp_companies_hotels_policies.erp_companies_hotels_id', $this->erp_companies_hotels_id);


        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('erp_companies_hotels_policies');
        if ($rows_no)
            return $query->num_rows();

        if ($this->erp_companies_hotels_policies_id)
            return $query->row();
        else
            return $query->result();
    }

    function save() {
        if ($this->erp_companies_hotels_policies_id !== FALSE)
            $this->db->set('erp_companies_hotels_policies.erp_companies_hotels_policies_id', $this->erp_companies_hotels_policies_id);

        if ($this->erp_hotels_policies_id !== FALSE)
            $this->db->set('erp_companies_hotels_policies.erp_hotels_policies_id', $this->erp_hotels_policies_id);

        if ($this->policy_description !== FALSE)
            $this->db->set('erp_companies_hotels_policies.policy_description', $this->policy_description);

        if ($this->policy_field_type !== FALSE)
            $this->db->set('erp_companies_hotels_policies.policy_field_type', $this->policy_field_type);

        if ($this->erp_companies_hotels_id !== FALSE)
            $this->db->set('erp_companies_hotels_policies.erp_companies_hotels_id', $this->erp_companies_hotels_id);



        if ($this->erp_companies_hotels_policies_id) {
            $this->db->where('erp_companies_hotels_policies.erp_companies_hotels_policies_id', $this->erp_companies_hotels_policies_id)->update('erp_companies_hotels_policies');
        } else {
            $this->db->insert('erp_companies_hotels_policies');
            return $this->db->insert_id();
        }
    }

    function delete() {
        if ($this->erp_companies_hotels_policies_id !== FALSE)
            $this->db->where('erp_companies_hotels_policies.erp_companies_hotels_policies_id', $this->erp_companies_hotels_policies_id);

        if ($this->erp_hotels_policies_id !== FALSE)
            $this->db->where('erp_companies_hotels_policies.erp_hotels_policies_id', $this->erp_hotels_policies_id);

        if ($this->policy_description !== FALSE)
            $this->db->where('erp_companies_hotels_policies.policy_description', $this->policy_description);

        if ($this->policy_field_type !== FALSE)
            $this->db->where('erp_companies_hotels_policies.policy_field_type', $this->policy_field_type);

        if ($this->erp_companies_hotels_id !== FALSE)
            $this->db->where('erp_companies_hotels_policies.erp_companies_hotels_id', $this->erp_companies_hotels_id);



        $this->db->delete('erp_companies_hotels_policies');
        return $this->db->affected_rows();
    }

    function search($rows_no = FALSE) {
        if ($this->custom_select !== FALSE) {
            $this->db->select('erp_companies_hotels_policies_id');
            $this->db->select($this->custom_select);
        }

        if ($this->erp_companies_hotels_policies_id !== FALSE)
            $this->db->where('erp_companies_hotels_policies.erp_companies_hotels_policies_id', $this->erp_companies_hotels_policies_id);

        if ($this->erp_hotels_policies_id !== FALSE)
            $this->db->where('erp_companies_hotels_policies.erp_hotels_policies_id', $this->erp_hotels_policies_id);

        if ($this->policy_description !== FALSE)
            $this->db->where('erp_companies_hotels_policies.policy_description', $this->policy_description);

        if ($this->policy_field_type !== FALSE)
            $this->db->where('erp_companies_hotels_policies.policy_field_type', $this->policy_field_type);

        if ($this->erp_companies_hotels_id !== FALSE)
            $this->db->where('erp_companies_hotels_policies.erp_companies_hotels_id', $this->erp_companies_hotels_id);


        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('erp_companies_hotels_policies');
        if ($rows_no)
            return $query->num_rows();

        return $query->result();
    }

    function count_details() {
        if ($this->erp_companies_hotels_id != false)
            $this->db->where('erp_companies_hotels_id', $this->erp_companies_hotels_id);
        return $this->db->from('erp_companies_hotels_policies')->count_all_results();
    }

}

/* End of file erp_companies_hotels_policies_model.php */
/* Location: ./application/models/erp_companies_hotels_policies_model.php */