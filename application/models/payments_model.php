<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Payments_model extends CI_Model {

    public $erp_payment_id = FALSE;
    public $owner_erp_company_type_id = FALSE;
    public $owner_erp_company_id = FALSE;
    public $payment_type = FALSE;
    public $amount = FALSE;
    public $rate = FALSE;
    public $order_no = FALSE;
    public $erp_company_type_id = FALSE;
    public $erp_company_id = FALSE;
    public $erp_currency_id = FALSE;
    public $voucher = FALSE;
    public $notes = FALSE;
    public $date = FALSE;
    public $from_date = FALSE;
    public $to_date = FALSE;
    public $from_amount = FALSE;
    public $to_amount = FALSE;
    public $submission_datetime = FALSE;
    public $custom_select = FALSE;
    public $limit = FALSE;
    public $offset = FALSE;
    public $order_by = FALSE;

    function __construct() {
        parent::__construct();
    }

    function get($rows_no = FALSE) {

        if ($this->custom_select !== FALSE) {
            $this->db->select($this->custom_select);
        }

        if ($this->owner_erp_company_type_id !== FALSE)
            $this->db->where('erp_payments.owner_erp_company_type_id', $this->owner_erp_company_type_id);

        if ($this->owner_erp_company_id !== FALSE)
            $this->db->where('erp_payments.owner_erp_company_id', $this->owner_erp_company_id);

        if ($this->erp_payment_id !== FALSE)
            $this->db->where('erp_payments.erp_payment_id', $this->erp_payment_id);

        if ($this->rate !== FALSE)
            $this->db->where('erp_payments.rate', $this->rate);

        if ($this->order_no !== FALSE)
            $this->db->where('erp_payments.order_no', $this->order_no);

        if ($this->erp_company_type_id !== FALSE)
            $this->db->where('erp_payments.erp_company_type_id', $this->erp_company_type_id);

        if ($this->erp_company_id !== FALSE)
            $this->db->where('erp_payments.erp_company_id', $this->erp_company_id);

        if ($this->payment_type !== FALSE)
            $this->db->where('erp_payments.payment_type', $this->payment_type);

        if ($this->amount !== FALSE)
            $this->db->where('erp_payments.amount', $this->amount);

        if ($this->erp_currency_id !== FALSE)
            $this->db->where('erp_payments.erp_currency_id', $this->erp_currency_id);

        if ($this->voucher !== FALSE)
            $this->db->where('erp_payments.voucher', $this->voucher);

        if ($this->notes !== FALSE)
            $this->db->where('erp_payments.notes', $this->notes);

        if ($this->date !== FALSE)
            $this->db->where('erp_payments.date', $this->date);

        if ($this->submission_datetime !== FALSE)
            $this->db->where('erp_payments.submission_datetime', $this->submission_datetime);

        if ($this->from_date !== FALSE)
            $this->db->where('erp_payments.date >=', $this->from_date);

        if ($this->to_date !== FALSE)
            $this->db->where('erp_payments.date <=', $this->to_date);

        if ($this->from_amount !== FALSE)
            $this->db->where('erp_payments.amount >=', $this->from_amount);

        if ($this->to_amount !== FALSE)
            $this->db->where('erp_payments.amount <=', $this->to_amount);

        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('erp_payments');
        if ($rows_no)
            return $query->num_rows();

        if ($this->erp_payment_id)
            return $query->row();
        else
            return $query->result();
    }

    function save() {
        if ($this->erp_payment_id !== FALSE)
            $this->db->set('erp_payments.erp_payment_id', $this->erp_payment_id);

        if ($this->rate !== FALSE)
            $this->db->set('erp_payments.rate', $this->rate);

        if ($this->order_no !== FALSE)
            $this->db->set('erp_payments.order_no', $this->order_no);

        if ($this->erp_company_type_id !== FALSE)
            $this->db->set('erp_payments.erp_company_type_id', $this->erp_company_type_id);

        if ($this->erp_company_id !== FALSE)
            $this->db->set('erp_payments.erp_company_id', $this->erp_company_id);

        if ($this->owner_erp_company_type_id !== FALSE)
            $this->db->set('erp_payments.owner_erp_company_type_id', $this->owner_erp_company_type_id);

        if ($this->owner_erp_company_id !== FALSE)
            $this->db->set('erp_payments.owner_erp_company_id', $this->owner_erp_company_id);

        if ($this->payment_type !== FALSE)
            $this->db->set('erp_payments.payment_type', $this->payment_type);

        if ($this->amount !== FALSE)
            $this->db->set('erp_payments.amount', $this->amount);

        if ($this->erp_currency_id !== FALSE)
            $this->db->set('erp_payments.erp_currency_id', $this->erp_currency_id);

        if ($this->voucher !== FALSE)
            $this->db->set('erp_payments.voucher', $this->voucher);

        if ($this->notes !== FALSE)
            $this->db->set('erp_payments.notes', $this->notes);

        if ($this->date !== FALSE)
            $this->db->set('erp_payments.date', $this->date);

        if ($this->submission_datetime !== FALSE)
            $this->db->set('erp_payments.submission_datetime', $this->submission_datetime);

        if ($this->erp_payment_id) {
            $this->db->where('erp_payments.erp_payment_id', $this->erp_payment_id)->update('erp_payments');
            return $this->erp_payment_id;
        } else {
            $this->db->insert('erp_payments');
            return $this->db->insert_id();
        }
    }

    function delete() {
        if ($this->erp_payment_id !== FALSE)
            $this->db->where('erp_payments.erp_payment_id', $this->erp_payment_id);

        if ($this->rate !== FALSE)
            $this->db->where('erp_payments.rate', $this->rate);

        if ($this->order_no !== FALSE)
            $this->db->where('erp_payments.order_no', $this->order_no);

        if ($this->erp_company_type_id !== FALSE)
            $this->db->where('erp_payments.erp_company_type_id', $this->erp_company_type_id);

        if ($this->erp_company_id !== FALSE)
            $this->db->where('erp_payments.erp_company_id', $this->erp_company_id);

        if ($this->payment_type !== FALSE)
            $this->db->where('erp_payments.payment_type', $this->payment_type);

        if ($this->amount !== FALSE)
            $this->db->where('erp_payments.amount', $this->amount);

        if ($this->erp_currency_id !== FALSE)
            $this->db->where('erp_payments.erp_currency_id', $this->erp_currency_id);

        if ($this->voucher !== FALSE)
            $this->db->where('erp_payments.voucher', $this->voucher);

        if ($this->notes !== FALSE)
            $this->db->where('erp_payments.notes', $this->notes);

        if ($this->date !== FALSE)
            $this->db->where('erp_payments.date', $this->date);

        if ($this->submission_datetime !== FALSE)
            $this->db->where('erp_payments.submission_datetime', $this->submission_datetime);

        $this->db->delete('erp_payments');
        return $this->db->affected_rows();
    }

    function search($rows_no = FALSE) {
        if ($this->custom_select !== FALSE) {
            $this->db->select($this->custom_select);
        }

        if ($this->erp_payment_id !== FALSE)
            $this->db->where('erp_payments.erp_payment_id', $this->erp_payment_id);

        if ($this->rate !== FALSE)
            $this->db->where('erp_payments.rate', $this->rate);

        if ($this->order_no !== FALSE)
            $this->db->where('erp_payments.order_no', $this->order_no);

        if ($this->erp_company_type_id !== FALSE)
            $this->db->where('erp_payments.erp_company_type_id', $this->erp_company_type_id);

        if ($this->erp_company_id !== FALSE)
            $this->db->where('erp_payments.erp_company_id', $this->erp_company_id);

        if ($this->payment_type !== FALSE)
            $this->db->where('erp_payments.payment_type', $this->payment_type);

        if ($this->amount !== FALSE)
            $this->db->where('erp_payments.amount', $this->amount);

        if ($this->erp_currency_id !== FALSE)
            $this->db->where('erp_payments.erp_currency_id', $this->erp_currency_id);

        if ($this->voucher !== FALSE)
            $this->db->where('erp_payments.voucher', $this->voucher);

        if ($this->notes !== FALSE)
            $this->db->where('erp_payments.notes', $this->notes);

        if ($this->date !== FALSE)
            $this->db->where('erp_payments.date', $this->date);

        if ($this->submission_datetime !== FALSE)
            $this->db->where('erp_payments.submission_datetime', $this->submission_datetime);

        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('erp_payments');
        if ($rows_no)
            return $query->num_rows();

        return $query->result();
    }

}

/* End of file erp_payments_model.php */
/* Location: ./application/models/erp_payments_model.php */