<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Ports_model extends CI_Model {

    public $erp_port_id = FALSE;
    public $erp_country_id = FALSE;
    public $erp_city_id = FALSE;
    public $name_ar = FALSE;
    public $name_la = FALSE;
    public $code = FALSE;
    public $country_code = FALSE;
    public $country_name = FALSE;
    public $world_area_code = FALSE;
    public $safa_transportertype_id = FALSE;
    public $custom_select = FALSE;
    public $limit = FALSE;
    public $offset = FALSE;
    public $order_by = FALSE;
    public $join = FALSE;

    function __construct() {
        parent::__construct();
    }

    function get($rows_no = FALSE) {

        if ($this->custom_select !== FALSE) {
            $this->db->select('erp_port_id');
            $this->db->select($this->custom_select);
        }

        if ($this->erp_port_id !== FALSE)
            $this->db->where('erp_ports.erp_port_id', $this->erp_port_id);

        if ($this->erp_city_id !== FALSE)
            $this->db->where('erp_ports.erp_city_id', $this->erp_city_id);

        if ($this->erp_country_id !== FALSE)
            $this->db->where('erp_ports.erp_country_id', $this->erp_country_id);

        if ($this->name_ar !== FALSE)
            $this->db->where('erp_ports.name_ar', $this->name_ar);

        if ($this->name_la !== FALSE)
            $this->db->where('erp_ports.name_la', $this->name_la);

        if ($this->code !== FALSE)
            $this->db->where('erp_ports.code', $this->code);

        if ($this->country_code !== FALSE)
            $this->db->where('erp_ports.country_code', $this->country_code);

        if ($this->country_name !== FALSE)
            $this->db->where('erp_ports.country_name', $this->country_name);

        if ($this->world_area_code !== FALSE)
            $this->db->where('erp_ports.world_area_code', $this->world_area_code);

        if ($this->safa_transportertype_id !== FALSE)
            $this->db->where('erp_ports.safa_transportertype_id', $this->safa_transportertype_id);


        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        if ($this->join) {
            $this->db->select('erp_ports.*,erp_cities.name_ar as city_name_ar,erp_cities.name_la as city_name_la,erp_cities.erp_city_id,');
            $this->db->join('erp_cities', 'erp_ports.erp_city_id = erp_cities.erp_city_id', 'left');
        }

        $query = $this->db->get('erp_ports');

        //echo $this->db->last_query(); exit;

        if ($rows_no)
            return $query->num_rows();

        if ($this->erp_port_id)
            return $query->row();
        else
            return $query->result();
    }

    function save() {
        if ($this->erp_port_id !== FALSE)
            $this->db->set('erp_ports.erp_port_id', $this->erp_port_id);

        if ($this->erp_country_id !== FALSE)
            $this->db->set('erp_ports.erp_country_id', $this->erp_country_id);

        if ($this->erp_city_id !== FALSE)
            $this->db->set('erp_ports.erp_city_id', $this->erp_city_id);

        if ($this->name_ar !== FALSE)
            $this->db->set('erp_ports.name_ar', $this->name_ar);

        if ($this->name_la !== FALSE)
            $this->db->set('erp_ports.name_la', $this->name_la);

        if ($this->code !== FALSE)
            $this->db->set('erp_ports.code', $this->code);

        if ($this->country_code !== FALSE)
            $this->db->set('erp_ports.country_code', $this->country_code);

        if ($this->country_name !== FALSE)
            $this->db->set('erp_ports.country_name', $this->country_name);

        if ($this->world_area_code !== FALSE)
            $this->db->set('erp_ports.world_area_code', $this->world_area_code);

        if ($this->safa_transportertype_id !== FALSE)
            $this->db->set('erp_ports.safa_transportertype_id', $this->safa_transportertype_id);



        if ($this->erp_port_id) {
            $this->db->where('erp_ports.erp_port_id', $this->erp_port_id)->update('erp_ports');
        } else {
            $this->db->insert('erp_ports');
            return $this->db->insert_id();
        }
    }

    function delete() {
        if ($this->erp_port_id !== FALSE)
            $this->db->where('erp_ports.erp_port_id', $this->erp_port_id);

        if ($this->erp_city_id !== FALSE)
            $this->db->where('erp_ports.erp_city_id', $this->erp_city_id);

        if ($this->erp_country_id !== FALSE)
            $this->db->where('erp_ports.erp_country_id', $this->erp_country_id);

        if ($this->name_ar !== FALSE)
            $this->db->where('erp_ports.name_ar', $this->name_ar);

        if ($this->name_la !== FALSE)
            $this->db->where('erp_ports.name_la', $this->name_la);

        if ($this->code !== FALSE)
            $this->db->where('erp_ports.code', $this->code);
        if ($this->country_code !== FALSE)
            $this->db->where('erp_ports.country_code', $this->country_code);

        if ($this->country_name !== FALSE)
            $this->db->where('erp_ports.country_name', $this->country_name);

        if ($this->world_area_code !== FALSE)
            $this->db->where('erp_ports.world_area_code', $this->world_area_code);

        if ($this->safa_transportertype_id !== FALSE)
            $this->db->where('erp_ports.safa_transportertype_id', $this->safa_transportertype_id);



        $this->db->delete('erp_ports');
        return $this->db->affected_rows();
    }

    function search($rows_no = FALSE) {
        if ($this->custom_select !== FALSE) {
            $this->db->select('erp_port_id');
            $this->db->select($this->custom_select);
        }

        if ($this->erp_port_id !== FALSE)
            $this->db->where('erp_ports.erp_port_id', $this->erp_port_id);

        if ($this->erp_country_id !== FALSE)
            $this->db->where('erp_ports.erp_country_id', $this->erp_country_id);

        if ($this->erp_city_id !== FALSE)
            $this->db->where('erp_ports.erp_city_id', $this->erp_city_id);

        if ($this->name_ar !== FALSE)
            $this->db->where('erp_ports.name_ar', $this->name_ar);

        if ($this->name_la !== FALSE)
            $this->db->where('erp_ports.name_la', $this->name_la);

        if ($this->code !== FALSE)
            $this->db->where('erp_ports.code', $this->code);

        if ($this->country_code !== FALSE)
            $this->db->where('erp_ports.country_code', $this->country_code);

        if ($this->country_name !== FALSE)
            $this->db->where('erp_ports.country_name', $this->country_name);

        if ($this->world_area_code !== FALSE)
            $this->db->where('erp_ports.world_area_code', $this->world_area_code);

        if ($this->safa_transportertype_id !== FALSE)
            $this->db->where('erp_ports.safa_transportertype_id', $this->safa_transportertype_id);


        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('erp_ports');
        if ($rows_no)
            return $query->num_rows();

        return $query->result();
    }

    function get_all_have_halls($cc = FALSE, $porttype = 2) {
        $this->db->distinct();

        $this->db->where('erp_ports.safa_transportertype_id', $porttype);
        if ($cc)
            $this->db->where('erp_ports.country_code', $cc);

        if ($cc == 'SA')
            $this->db->join('erp_port_halls', 'erp_ports.erp_port_id = erp_port_halls.erp_port_id');
        $this->db->select('erp_ports.*');
        if (session('ea_id'))
            $this->db->where('safa_ea_ports.safa_ea_id', session('ea_id'));
        $this->db->from('erp_ports');
        $this->db->order_by('code');
        $this->db->join('safa_ea_ports', 'safa_ea_ports.erp_port_id = erp_ports.erp_port_id','left');

        $query = $this->db->get();
        return $query->result();
    }

    function check_delete_ability($id) {

        $this->db->select('count(erp_port_halls.erp_port_id)as port_id');
        $this->db->from('erp_ports');
        $this->db->join('erp_port_halls', 'erp_ports.erp_port_id = erp_port_halls.erp_port_id', 'left');
        $this->db->group_by('erp_ports.erp_port_id');
        $this->db->where('erp_ports.erp_port_id', $id);
        $query = $this->db->get();
        $flag = 0;
        foreach ($query->result() as $row => $table) {
            foreach ($table as $col => $value) {
                if ($value > 0) {
                    $flag = 1;
                    break;
                } else {
                    continue;
                }
            }
        }
        return $flag;
    }

    function get_country_name($erp_country_id) {
        $this->db->select('erp_countries.erp_country_id,erp_countries.name_ar,erp_countries.name_la,erp_countries.country_code');
        $this->db->where('erp_countries.erp_country_id', $erp_country_id);
        return $this->db->get('erp_countries')->row();
    }

    function get_country_id($country_name) {

        $this->db->select('erp_countries.erp_country_id');
        $this->db->where('erp_countries.name_ar', $country_name);
        $this->db->or_where('erp_countries.name_la', $country_name);
        return $this->db->get('erp_countries')->row();
    }

    function get_cities($erp_country_id) {
        $this->db->select('erp_cities.erp_city_id,erp_cities.name_ar,erp_cities.name_la');
        $this->db->where('erp_cities.erp_country_id', $erp_country_id);
        return $this->db->get('erp_cities')->result();
    }

    function check_ea_ports($erp_port_id, $safa_ea_id) {

        $this->db->where("erp_port_id", $erp_port_id);
        $this->db->where("safa_ea_id", $safa_ea_id);
        $query = $this->db->get("safa_ea_ports");

        if ($query->num_rows > 0) {

            return $query->num_rows;
        }
    }

}

/* End of file ports_model.php */
/* Location: ./application/models/ports_model.php */