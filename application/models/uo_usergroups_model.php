<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Uo_usergroups_model extends CI_Model {

    public $safa_uo_usergroup_id = FALSE;
    public $name_ar = FALSE;
    public $name_la = FALSE;
    public $custom_select = FALSE;
    public $limit = FALSE;
    public $offset = FALSE;
    public $order_by = FALSE;

    function __construct() {
        parent::__construct();
    }

    function get($rows_no = FALSE) {

        if ($this->custom_select !== FALSE) {
            $this->db->select('safa_uo_usergroup_id');
            $this->db->select($this->custom_select);
        }

        if ($this->safa_uo_usergroup_id !== FALSE)
            $this->db->where('safa_uo_usergroups.safa_uo_usergroup_id', $this->safa_uo_usergroup_id);

        if ($this->name_ar !== FALSE)
            $this->db->where('safa_uo_usergroups.name_ar', $this->name_ar);

        if ($this->name_la !== FALSE)
            $this->db->where('safa_uo_usergroups.name_la', $this->name_la);


        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('safa_uo_usergroups');
        if ($rows_no)
            return $query->num_rows();

        if ($this->safa_uo_usergroup_id)
            return $query->row();
        else
            return $query->result();
    }

    function save() {
        if ($this->safa_uo_usergroup_id !== FALSE)
            $this->db->set('safa_uo_usergroups.safa_uo_usergroup_id', $this->safa_uo_usergroup_id);

        if ($this->name_ar !== FALSE)
            $this->db->set('safa_uo_usergroups.name_ar', $this->name_ar);

        if ($this->name_la !== FALSE)
            $this->db->set('safa_uo_usergroups.name_la', $this->name_la);



        if ($this->safa_uo_usergroup_id) {
            $this->db->where('safa_uo_usergroups.safa_uo_usergroup_id', $this->safa_uo_usergroup_id)->update('safa_uo_usergroups');
        } else {
            $this->db->insert('safa_uo_usergroups');
            return $this->db->insert_id();
        }
    }

    function delete() {
        if ($this->safa_uo_usergroup_id !== FALSE)
            $this->db->where('safa_uo_usergroups.safa_uo_usergroup_id', $this->safa_uo_usergroup_id);

        if ($this->name_ar !== FALSE)
            $this->db->where('safa_uo_usergroups.name_ar', $this->name_ar);

        if ($this->name_la !== FALSE)
            $this->db->where('safa_uo_usergroups.name_la', $this->name_la);



        $this->db->delete('safa_uo_usergroups');
        return $this->db->affected_rows();
    }

    function search($rows_no = FALSE) {
        if ($this->custom_select !== FALSE) {
            $this->db->select('safa_uo_usergroup_id');
            $this->db->select($this->custom_select);
        }

        if ($this->safa_uo_usergroup_id !== FALSE)
            $this->db->where('safa_uo_usergroups.safa_uo_usergroup_id', $this->safa_uo_usergroup_id);

        if ($this->name_ar !== FALSE)
            $this->db->where('safa_uo_usergroups.name_ar', $this->name_ar);

        if ($this->name_la !== FALSE)
            $this->db->where('safa_uo_usergroups.name_la', $this->name_la);


        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('safa_uo_usergroups');
        if ($rows_no)
            return $query->num_rows();

        return $query->result();
    }

    function check_delete_ability($id) {
        $this->db->select('count(safa_uo_usergroups_uoprivileges.safa_uo_usergroup_id)as usergroup_id');
        $this->db->from('safa_uo_usergroups');
        $this->db->join('safa_uo_usergroups_uoprivileges', 'safa_uo_usergroups.safa_uo_usergroup_id = safa_uo_usergroups_uoprivileges.safa_uo_usergroup_id', 'left');
        $this->db->group_by('safa_uo_usergroups.safa_uo_usergroup_id');
        $this->db->where('safa_uo_usergroups.safa_uo_usergroup_id', $id);
        $query = $this->db->get();
        $flag = 0;
        foreach ($query->result() as $row => $table) {
            foreach ($table as $col => $value) {
                if ($value > 0) {
                    $flag = 1;
                    break;
                } else {
                    continue;
                }
            }
        }
        return $flag;
    }

}

/* End of file uo_usergroups_model.php */
/* Location: ./application/models/uo_usergroups_model.php */