<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Trip_hotels_model extends CI_Model {

    public $safa_trip_hotel_id = FALSE;
    public $nights_count = FALSE;
    public $erp_hotel_id = FALSE;
    public $erp_meal_id = FALSE;
    public $safa_trip_id = FALSE;
    public $checkin_datetime = FALSE;
    public $checkout_datetime = FALSE;
    public $custom_select = FALSE;
    public $limit = FALSE;
    public $offset = FALSE;
    public $order_by = FALSE;

    function __construct() {
        parent::__construct();
    }

    function get($rows_no = FALSE) {

        if ($this->custom_select !== FALSE) {
            $this->db->select('safa_trip_hotel_id');
            $this->db->select($this->custom_select);
        }

        if ($this->safa_trip_hotel_id !== FALSE)
            $this->db->where('safa_trip_hotels.safa_trip_hotel_id', $this->safa_trip_hotel_id);

        if ($this->nights_count !== FALSE)
            $this->db->where('safa_trip_hotels.nights_count', $this->nights_count);

        if ($this->erp_hotel_id !== FALSE)
            $this->db->where('safa_trip_hotels.erp_hotel_id', $this->erp_hotel_id);

        if ($this->erp_meal_id !== FALSE)
            $this->db->where('safa_trip_hotels.erp_meal_id', $this->erp_meal_id);

        if ($this->safa_trip_id !== FALSE)
            $this->db->where('safa_trip_hotels.safa_trip_id', $this->safa_trip_id);

        if ($this->checkin_datetime !== FALSE)
            $this->db->where('safa_trip_hotels.checkin_datetime', $this->checkin_datetime);

        if ($this->checkout_datetime !== FALSE)
            $this->db->where('safa_trip_hotels.checkout_datetime', $this->checkout_datetime);


        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('safa_trip_hotels');
        if ($rows_no)
            return $query->num_rows();

        if ($this->safa_trip_hotel_id)
            return $query->row();
        else
            return $query->result();
    }

    function save() {
        if ($this->safa_trip_hotel_id !== FALSE)
            $this->db->set('safa_trip_hotels.safa_trip_hotel_id', $this->safa_trip_hotel_id);

        if ($this->nights_count !== FALSE)
            $this->db->set('safa_trip_hotels.nights_count', $this->nights_count);

        if ($this->erp_hotel_id !== FALSE)
            $this->db->set('safa_trip_hotels.erp_hotel_id', $this->erp_hotel_id);

        if ($this->erp_meal_id !== FALSE)
            $this->db->set('safa_trip_hotels.erp_meal_id', $this->erp_meal_id);

        if ($this->safa_trip_id !== FALSE)
            $this->db->set('safa_trip_hotels.safa_trip_id', $this->safa_trip_id);

        if ($this->checkin_datetime !== FALSE)
            $this->db->set('safa_trip_hotels.checkin_datetime', $this->checkin_datetime);

        if ($this->checkout_datetime !== FALSE)
            $this->db->set('safa_trip_hotels.checkout_datetime', $this->checkout_datetime);



        if ($this->safa_trip_hotel_id) {
            $this->db->where('safa_trip_hotels.safa_trip_hotel_id', $this->safa_trip_hotel_id)->update('safa_trip_hotels');
        } else {
            $this->db->insert('safa_trip_hotels');
            return $this->db->insert_id();
        }
    }

    function delete() {
        if ($this->safa_trip_hotel_id !== FALSE)
            $this->db->where('safa_trip_hotels.safa_trip_hotel_id', $this->safa_trip_hotel_id);

        if ($this->nights_count !== FALSE)
            $this->db->where('safa_trip_hotels.nights_count', $this->nights_count);

        if ($this->erp_hotel_id !== FALSE)
            $this->db->where('safa_trip_hotels.erp_hotel_id', $this->erp_hotel_id);

        if ($this->erp_meal_id !== FALSE)
            $this->db->where('safa_trip_hotels.erp_meal_id', $this->erp_meal_id);

        if ($this->safa_trip_id !== FALSE)
            $this->db->where('safa_trip_hotels.safa_trip_id', $this->safa_trip_id);

        if ($this->checkin_datetime !== FALSE)
            $this->db->where('safa_trip_hotels.checkin_datetime', $this->checkin_datetime);

        if ($this->checkout_datetime !== FALSE)
            $this->db->where('safa_trip_hotels.checkout_datetime', $this->checkout_datetime);



        $this->db->delete('safa_trip_hotels');
        return $this->db->affected_rows();
    }

    function search($rows_no = FALSE) {
        if ($this->custom_select !== FALSE) {
            $this->db->select('safa_trip_hotel_id');
            $this->db->select($this->custom_select);
        }

        if ($this->safa_trip_hotel_id !== FALSE)
            $this->db->where('safa_trip_hotels.safa_trip_hotel_id', $this->safa_trip_hotel_id);

        if ($this->nights_count !== FALSE)
            $this->db->where('safa_trip_hotels.nights_count', $this->nights_count);

        if ($this->erp_hotel_id !== FALSE)
            $this->db->where('safa_trip_hotels.erp_hotel_id', $this->erp_hotel_id);

        if ($this->erp_meal_id !== FALSE)
            $this->db->where('safa_trip_hotels.erp_meal_id', $this->erp_meal_id);

        if ($this->safa_trip_id !== FALSE)
            $this->db->where('safa_trip_hotels.safa_trip_id', $this->safa_trip_id);

        if ($this->checkin_datetime !== FALSE)
            $this->db->where('safa_trip_hotels.checkin_datetime', $this->checkin_datetime);

        if ($this->checkout_datetime !== FALSE)
            $this->db->where('safa_trip_hotels.checkout_datetime', $this->checkout_datetime);


        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('safa_trip_hotels');
        if ($rows_no)
            return $query->num_rows();

        return $query->result();
    }

}

/* End of file trip_hotels_model.php */
/* Location: ./application/models/trip_hotels_model.php */