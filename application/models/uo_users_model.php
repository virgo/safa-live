<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Uo_users_model extends CI_Model {

    public $safa_uo_user_id = FALSE;
    public $name_ar = FALSE;
    public $name_la = FALSE;
    public $username = FALSE;
    public $password = FALSE;
    public $safa_uo_id = FALSE;
    public $email = false;
    public $mobile = false;
    public $safa_uo_usergroup_id = FALSE;
    
    public $safa_uo_user_type_id = FALSE;
    
    public $custom_select = FALSE;
    public $limit = FALSE;
    public $offset = FALSE;
    public $order_by = FALSE;
    public $join = FALSE;

    function __construct() {
        parent::__construct();
    }

    function get($rows_no = FALSE) {

        if ($this->custom_select !== FALSE) {
            $this->db->select('safa_uo_user_id');
            $this->db->select($this->custom_select);
        }

        if ($this->safa_uo_user_id !== FALSE)
            $this->db->where('safa_uo_users.safa_uo_user_id', $this->safa_uo_user_id);

        if ($this->name_ar !== FALSE)
            $this->db->where('safa_uo_users.name_ar', $this->name_ar);

        if ($this->name_la !== FALSE)
            $this->db->where('safa_uo_users.name_la', $this->name_la);

        if ($this->email !== FALSE)
            $this->db->where('safa_uo_users.email', $this->email);

        if ($this->username !== FALSE)
            $this->db->where('safa_uo_users.username', $this->username);

        if ($this->password !== FALSE)
            $this->db->where('safa_uo_users.password', $this->password);

        if ($this->safa_uo_id !== FALSE)
            $this->db->where('safa_uo_users.safa_uo_id', $this->safa_uo_id);

        if ($this->mobile !== FALSE)
            $this->db->where('safa_uo_users.mobile', $this->mobile);

        if ($this->safa_uo_usergroup_id !== FALSE)
            $this->db->where('safa_uo_users.safa_uo_usergroup_id', $this->safa_uo_usergroup_id);

		if ($this->safa_uo_user_type_id !== FALSE)
            $this->db->where('safa_uo_users.safa_uo_user_type_id', $this->safa_uo_user_type_id);
            
        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        if ($this->join !== FALSE) {
            $this->db->select('safa_uo_users.safa_uo_user_id,safa_uo_users.safa_uo_id,safa_uo_users.mobile, safa_uo_users.safa_uo_user_type_id, safa_uo_users.name_ar,safa_uo_users.name_la,
                safa_uo_users.username,safa_uo_users.password,safa_uo_users.safa_uo_usergroup_id,safa_uo_users.email,
                safa_uos.safa_uo_id as safa_uos_id,safa_uos.name_ar as namear,safa_uos.name_la as namela, safa_uo_user_info.erp_city_id, safa_uo_user_info.port_type');
            $this->db->join('safa_uos', 'safa_uo_users.safa_uo_id = safa_uos.safa_uo_id', 'left');

            $this->db->join('safa_uo_user_info', 'safa_uo_users.safa_uo_user_id = safa_uo_user_info.safa_uo_user_id', 'left');
        }

        
        $query = $this->db->get('safa_uo_users');
        if ($rows_no)
            return $query->num_rows();

        if ($this->safa_uo_user_id)
            return $query->row();
        else
            return $query->result();
    }

    function save() {
        if ($this->safa_uo_user_id !== FALSE)
            $this->db->set('safa_uo_users.safa_uo_user_id', $this->safa_uo_user_id);

        if ($this->name_ar !== FALSE)
            $this->db->set('safa_uo_users.name_ar', $this->name_ar);

        if ($this->name_la !== FALSE)
            $this->db->set('safa_uo_users.name_la', $this->name_la);

        if ($this->email !== FALSE)
            $this->db->set('safa_uo_users.email', $this->email);

        if ($this->username !== FALSE)
            $this->db->set('safa_uo_users.username', $this->username);

        if ($this->password != FALSE)
            $this->db->set('safa_uo_users.password', $this->password);

        if ($this->safa_uo_id !== FALSE)
            $this->db->set('safa_uo_users.safa_uo_id', $this->safa_uo_id);

        if ($this->safa_uo_usergroup_id !== FALSE)
            $this->db->set('safa_uo_users.safa_uo_usergroup_id', $this->safa_uo_usergroup_id);


        if ($this->mobile !== FALSE)
            $this->db->set('safa_uo_users.mobile', $this->mobile);

		if ($this->safa_uo_user_type_id !== FALSE)
            $this->db->set('safa_uo_users.safa_uo_user_type_id', $this->safa_uo_user_type_id);
        
        if ($this->safa_uo_user_id) {
            $this->db->where('safa_uo_users.safa_uo_user_id', $this->safa_uo_user_id)->update('safa_uo_users');
        } else {
            $this->db->insert('safa_uo_users');
            return $this->db->insert_id();
        }
    }

    function delete() {
        if ($this->safa_uo_user_id !== FALSE)
            $this->db->where('safa_uo_users.safa_uo_user_id', $this->safa_uo_user_id);

        if ($this->name_ar !== FALSE)
            $this->db->where('safa_uo_users.name_ar', $this->name_ar);

        if ($this->name_la !== FALSE)
            $this->db->where('safa_uo_users.name_la', $this->name_la);

        if ($this->username !== FALSE)
            $this->db->where('safa_uo_users.username', $this->username);

        if ($this->password !== FALSE)
            $this->db->where('safa_uo_users.password', $this->password);

        if ($this->safa_uo_id !== FALSE)
            $this->db->where('safa_uo_users.safa_uo_id', $this->safa_uo_id);

        if ($this->safa_uo_usergroup_id !== FALSE)
            $this->db->where('safa_uo_users.safa_uo_usergroup_id', $this->safa_uo_usergroup_id);

        if ($this->mobile !== FALSE)
            $this->db->where('safa_uo_users.mobile', $this->mobile);
        if ($this->email !== FALSE)
            $this->db->where('safa_uo_users.email', $this->email);

		if ($this->safa_uo_user_type_id !== FALSE)
            $this->db->where('safa_uo_users.safa_uo_user_type_id', $this->safa_uo_user_type_id);
        
        $this->db->delete('safa_uo_users');
        return $this->db->affected_rows();
    }

    function search($rows_no = FALSE) {
        if ($this->custom_select !== FALSE) {
            $this->db->select('safa_uo_user_id');
            $this->db->select($this->custom_select);
        }

        if ($this->safa_uo_user_id !== FALSE)
            $this->db->where('safa_uo_users.safa_uo_user_id', $this->safa_uo_user_id);

        if ($this->name_ar !== FALSE)
            $this->db->where('safa_uo_users.name_ar', $this->name_ar);

        if ($this->name_la !== FALSE)
            $this->db->where('safa_uo_users.name_la', $this->name_la);

        if ($this->username !== FALSE)
            $this->db->where('safa_uo_users.username', $this->username);

        if ($this->password !== FALSE)
            $this->db->where('safa_uo_users.password', $this->password);

        if ($this->safa_uo_id !== FALSE)
            $this->db->where('safa_uo_users.safa_uo_id', $this->safa_uo_id);

        if ($this->safa_uo_usergroup_id !== FALSE)
            $this->db->where('safa_uo_users.safa_uo_usergroup_id', $this->safa_uo_usergroup_id);
        if ($this->mobile !== FALSE)
            $this->db->where('safa_uo_users.mobile', $this->mobile);

        if ($this->safa_uo_user_type_id !== FALSE)
            $this->db->where('safa_uo_users.safa_uo_user_type_id', $this->safa_uo_user_type_id);
        
        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('safa_uo_users');
        if ($rows_no)
            return $query->num_rows();

        return $query->result();
    }

}

/* End of file uo_users_model.php */
/* Location: ./application/models/uo_users_model.php */