<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Safa_contract_phases_model extends CI_Model {

    public $tbl_name = "safa_contract_phases";
    public $safa_contract_phases_id = FALSE;
    public $safa_uo_id = FALSE;
    public $name_ar = FALSE;
    public $name_la = FALSE;

    /**
     * safa_contract_phases_nationalities table fields
     */
    public $nationalities_safa_contract_phases_nationalities_id = FALSE;
    public $nationalities_safa_contract_phases_id = FALSE;
    public $nationalities_erp_nationalities_id = FALSE;

    /**
     * safa_contract_phases_documents table fields
     */
    public $documents_safa_contract_phases_documents_id = FALSE;
    public $documents_safa_contract_phases_id = FALSE;
    public $documents_name_ar = FALSE;
    public $documents_name_la = FALSE;

    /**
     * safa_contract_phases_documents_nationalities table fields
     */
    public $documents_nationalities_safa_contract_phases_documents_nationalities_id = FALSE;
    public $documents_nationalities_safa_contract_phases_documents_id = FALSE;
    public $documents_nationalities_erp_nationalities_id = FALSE;
    public $custom_select = FALSE;
    public $limit = FALSE;
    public $offset = FALSE;
    public $order_by = FALSE;

    function __construct() {
        parent::__construct();
    }

    function save() {
        if ($this->safa_contract_phases_id !== FALSE) {
            $this->db->set('safa_contract_phases.safa_contract_phases_id', $this->safa_contract_phases_id);
        }
        if ($this->safa_uo_id !== FALSE) {
            $this->db->set('safa_contract_phases.safa_uo_id', $this->safa_uo_id);
        }
        if ($this->name_ar !== FALSE) {
            $this->db->set('safa_contract_phases.name_ar', $this->name_ar);
        }
        if ($this->name_la !== FALSE) {
            $this->db->set('safa_contract_phases.name_la', $this->name_la);
        }

        if ($this->safa_contract_phases_id) {
            $this->db->where('safa_contract_phases.safa_contract_phases_id', $this->safa_contract_phases_id)->update('safa_contract_phases');
        } else {
            $this->db->insert('safa_contract_phases');
            return $this->db->insert_id();
        }
    }

    function saveNationalities() {
        if ($this->nationalities_safa_contract_phases_nationalities_id !== FALSE) {
            $this->db->set('safa_contract_phases_nationalities.safa_contract_phases_nationalities_id', $this->nationalities_safa_contract_phases_nationalities_id);
        }
        if ($this->nationalities_safa_contract_phases_id !== FALSE) {
            $this->db->set('safa_contract_phases_nationalities.safa_contract_phases_id', $this->nationalities_safa_contract_phases_id);
        }
        if ($this->nationalities_erp_nationalities_id !== FALSE) {
            $this->db->set('safa_contract_phases_nationalities.erp_nationalities_id', $this->nationalities_erp_nationalities_id);
        }

        if ($this->nationalities_safa_contract_phases_nationalities_id) {
            $this->db->where('safa_contract_phases_nationalities.safa_contract_phases_nationalities_id', $this->nationalities_safa_contract_phases_nationalities_id)->update('safa_contract_phases_nationalities');
        } else {
            $this->db->insert('safa_contract_phases_nationalities');
            return $this->db->insert_id();
        }
    }

    function saveDocuments() {
        if ($this->documents_safa_contract_phases_documents_id !== FALSE) {
            $this->db->set('safa_contract_phases_documents.safa_contract_phases_documents_id', $this->documents_safa_contract_phases_documents_id);
        }
        if ($this->documents_safa_contract_phases_id !== FALSE) {
            $this->db->set('safa_contract_phases_documents.safa_contract_phases_id', $this->documents_safa_contract_phases_id);
        }
        if ($this->documents_name_ar !== FALSE) {
            $this->db->set('safa_contract_phases_documents.name_ar', $this->documents_name_ar);
        }
        if ($this->documents_name_la !== FALSE) {
            $this->db->set('safa_contract_phases_documents.name_la', $this->documents_name_la);
        }

        if ($this->documents_safa_contract_phases_documents_id) {
            $this->db->where('safa_contract_phases_documents.safa_contract_phases_documents_id', $this->documents_safa_contract_phases_documents_id)->update('safa_contract_phases_documents');
        } else {
            $this->db->insert('safa_contract_phases_documents');
            return $this->db->insert_id();
        }
    }

    function saveDocumentsNationalities() {
        if ($this->documents_nationalities_safa_contract_phases_documents_nationalities_id !== FALSE) {
            $this->db->set('safa_contract_phases_documents_nationalities.safa_contract_phases_documents_nationalities_id', $this->documents_nationalities_safa_contract_phases_documents_nationalities_id);
        }
        if ($this->documents_nationalities_safa_contract_phases_documents_id !== FALSE) {
            $this->db->set('safa_contract_phases_documents_nationalities.safa_contract_phases_documents_id', $this->documents_nationalities_safa_contract_phases_documents_id);
        }
        if ($this->documents_nationalities_erp_nationalities_id !== FALSE) {
            $this->db->set('safa_contract_phases_documents_nationalities.erp_nationalities_id', $this->documents_nationalities_erp_nationalities_id);
        }

        if ($this->documents_nationalities_safa_contract_phases_documents_nationalities_id) {
            $this->db->where('safa_contract_phases_documents_nationalities.safa_contract_phases_documents_nationalities_id', $this->documents_nationalities_safa_contract_phases_documents_nationalities_id)->update('safa_contract_phases_documents_nationalities');
        } else {
            $this->db->insert('safa_contract_phases_documents_nationalities');
            return $this->db->insert_id();
        }
    }

    function delete() {
        if ($this->safa_contract_phases_id !== FALSE) {
            $this->db->where('safa_contract_phases.safa_contract_phases_id', $this->safa_contract_phases_id);
        }
        if ($this->safa_uo_id !== FALSE) {
            $this->db->where('safa_contract_phases.safa_uo_id', $this->safa_uo_id);
        }
        if ($this->name_ar !== FALSE) {
            $this->db->where('safa_contract_phases.name_ar', $this->name_ar);
        }
        if ($this->name_la !== FALSE) {
            $this->db->where('safa_contract_phases.name_la', $this->name_la);
        }

        $this->db->delete('safa_contract_phases');
        return $this->db->affected_rows();
    }

    function deleteNationalities() {
        if ($this->nationalities_safa_contract_phases_nationalities_id !== FALSE) {
            $this->db->where('safa_contract_phases_nationalities.safa_contract_phases_nationalities_id', $this->nationalities_safa_contract_phases_nationalities_id);
        }
        if ($this->nationalities_safa_contract_phases_id !== FALSE) {
            $this->db->where('safa_contract_phases_nationalities.safa_contract_phases_id', $this->nationalities_safa_contract_phases_id);
        }
        if ($this->nationalities_erp_nationalities_id !== FALSE) {
            $this->db->where('safa_contract_phases_nationalities.erp_nationalities_id', $this->nationalities_erp_nationalities_id);
        }

        $this->db->delete('safa_contract_phases_nationalities');
        return $this->db->affected_rows();
    }

    function deleteDocuments($rows_no = FALSE) {
        if ($this->documents_safa_contract_phases_documents_id !== FALSE) {
            $this->db->where('safa_contract_phases_documents.safa_contract_phases_documents_id', $this->documents_safa_contract_phases_documents_id);
        }
        if ($this->documents_safa_contract_phases_id !== FALSE) {
            $this->db->where('safa_contract_phases_documents.safa_contract_phases_id', $this->documents_safa_contract_phases_id);
        }
        /*
          if ($this->documents_name_ar !== FALSE) {
          $this->db->where('safa_contract_phases_documents.name_ar', $this->documents_name_ar);
          }
          if ($this->documents_name_la !== FALSE) {
          $this->db->where('safa_contract_phases_documents.name_la', $this->documents_name_la);
          }
         */

        $this->db->delete('safa_contract_phases_documents');
        $query_text = $this->db->last_query();
        return $this->db->affected_rows();
    }

    function deleteDocumentsNationalities($rows_no = FALSE) {
        if ($this->documents_nationalities_safa_contract_phases_documents_nationalities_id !== FALSE) {
            $this->db->where('safa_contract_phases_documents_nationalities.safa_contract_phases_documents_nationalities_id', $this->documents_nationalities_safa_contract_phases_documents_nationalities_id);
        }
        if ($this->documents_nationalities_safa_contract_phases_documents_id !== FALSE) {
            $this->db->where('safa_contract_phases_documents_nationalities.safa_contract_phases_documents_id', $this->documents_nationalities_safa_contract_phases_documents_id);
        }
        if ($this->documents_nationalities_erp_nationalities_id !== FALSE) {
            $this->db->where('safa_contract_phases_documents_nationalities.erp_nationalities_id', $this->documents_nationalities_erp_nationalities_id);
        }

        $this->db->delete('safa_contract_phases_documents_nationalities');
        $query_text = $this->db->last_query();
        return $this->db->affected_rows();
    }

    function get($rows_no = FALSE) {

        if ($this->custom_select !== FALSE) {
            $this->db->select('safa_contract_phases_id');
            $this->db->select($this->custom_select);
        }

        if ($this->safa_contract_phases_id !== FALSE) {
            $this->db->where('safa_contract_phases.safa_contract_phases_id', $this->safa_contract_phases_id);
        }
        if ($this->safa_uo_id !== FALSE) {
            $this->db->where('safa_contract_phases.safa_uo_id', $this->safa_uo_id);
        }
        if ($this->name_ar !== FALSE) {
            $this->db->where('safa_contract_phases.name_ar', $this->name_ar);
        }
        if ($this->name_la !== FALSE) {
            $this->db->where('safa_contract_phases.name_la', $this->name_la);
        }

        if ($this->order_by && is_array($this->order_by)) {
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);
        } else {
            $this->db->order_by('safa_contract_phases_id');
        }

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('safa_contract_phases');
        if ($rows_no)
            return $query->num_rows();

        if ($this->safa_contract_phases_id)
            return $query->row();
        else
            return $query->result();
    }

    function getNationalities($rows_no = FALSE) {

        if ($this->custom_select !== FALSE) {
            $this->db->select('safa_contract_phases_nationalities_id');
            $this->db->select($this->custom_select);
        }

        if ($this->nationalities_safa_contract_phases_nationalities_id !== FALSE) {
            $this->db->where('safa_contract_phases_nationalities.safa_contract_phases_nationalities_id', $this->nationalities_safa_contract_phases_nationalities_id);
        }
        if ($this->nationalities_safa_contract_phases_id !== FALSE) {
            $this->db->where('safa_contract_phases_nationalities.safa_contract_phases_id', $this->nationalities_safa_contract_phases_id);
        }
        if ($this->nationalities_erp_nationalities_id !== FALSE) {
            $this->db->where('safa_contract_phases_nationalities.erp_nationalities_id', $this->nationalities_erp_nationalities_id);
        }

        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('safa_contract_phases_nationalities');
        if ($rows_no)
            return $query->num_rows();

        if ($this->nationalities_safa_contract_phases_nationalities_id)
            return $query->row();
        else
            return $query->result();
    }

    function getDocuments($rows_no = FALSE) {

        if ($this->custom_select !== FALSE) {
            $this->db->select('safa_contract_phases_documents_id');
            $this->db->select($this->custom_select);
        }

        if ($this->documents_safa_contract_phases_documents_id !== FALSE) {
            $this->db->where('safa_contract_phases_documents.safa_contract_phases_documents_id', $this->documents_safa_contract_phases_documents_id);
        }
        if ($this->documents_safa_contract_phases_id !== FALSE) {
            $this->db->where('safa_contract_phases_documents.safa_contract_phases_id', $this->documents_safa_contract_phases_id);
        }

        /*
          if ($this->documents_name_ar !== FALSE) {
          $this->db->where('safa_contract_phases_documents.name_ar', $this->documents_name_ar);
          }
          if ($this->documents_name_la !== FALSE) {
          $this->db->where('safa_contract_phases_documents.name_la', $this->documents_name_la);
          }
         */

        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('safa_contract_phases_documents');

        $query_text = $this->db->last_query();

        if ($rows_no) {
            return $query->num_rows();
        }
        if ($this->documents_safa_contract_phases_documents_id) {
            return $query->row();
        } else {
            return $query->result();
        }
    }

    function getDocumentsNationalities($rows_no = FALSE) {

        if ($this->custom_select !== FALSE) {
            $this->db->select('safa_contract_phases_documents_nationalities_id');
            $this->db->select($this->custom_select);
        }

        if ($this->documents_nationalities_safa_contract_phases_documents_nationalities_id !== FALSE) {
            $this->db->where('safa_contract_phases_documents_nationalities.safa_contract_phases_documents_nationalities_id', $this->documents_nationalities_safa_contract_phases_documents_nationalities_id);
        }
        if ($this->documents_nationalities_safa_contract_phases_documents_id !== FALSE) {
            $this->db->where('safa_contract_phases_documents_nationalities.safa_contract_phases_documents_id', $this->documents_nationalities_safa_contract_phases_documents_id);
        }
        if ($this->documents_nationalities_erp_nationalities_id !== FALSE) {
            $this->db->where('safa_contract_phases_documents_nationalities.erp_nationalities_id', $this->documents_nationalities_erp_nationalities_id);
        }

        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('safa_contract_phases_documents_nationalities');
        if ($rows_no)
            return $query->num_rows();

        if ($this->documents_nationalities_safa_contract_phases_documents_nationalities_id)
            return $query->row();
        else
            return $query->result();
    }

}

/* End of file safa_contract_phases_model.php */
/* Location: ./application/models/safa_contract_phases_model.php */