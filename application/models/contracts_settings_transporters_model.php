<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Contracts_settings_transporters_model extends CI_Model {

    public $safa_uo_contract_setting_transporter_id = FALSE;
    public $safa_uo_contract_id = FALSE;
    public $safa_uo_contract_setting_id = FALSE;
    public $safa_transporter_id = FALSE;
    public $custom_select = FALSE;
    public $limit = FALSE;
    public $offset = FALSE;
    public $order_by = FALSE;
    public $by_eas_id = FALSE;

    function __construct() {
        parent::__construct();
        // if the uo_has loged in// 
    }

    function get($rows_no = FALSE) {

        if ($this->custom_select !== FALSE) {
            $this->db->select('safa_uo_contract_setting_transporter_id');
            $this->db->select($this->custom_select);
        }

        if ($this->safa_uo_contract_id !== FALSE)
            $this->db->where('safa_uo_contracts_settings_transporters.safa_uo_contract_id', $this->safa_uo_contract_id);

        if ($this->safa_uo_contract_setting_id !== FALSE)
            $this->db->where('safa_uo_contracts_settings_transporters.safa_uo_contract_setting_id', $this->safa_uo_contract_setting_id);

        if ($this->safa_transporter_id !== FALSE)
            $this->db->where('safa_uo_contracts_settings_transporters.safa_transporter_id', $this->safa_transporter_id);


        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('safa_uo_contracts_settings_transporters');

        $query_text = $this->db->last_query();
        //echo $query_text; exit;

        if ($rows_no)
            return $query->num_rows();

        if ($this->safa_uo_contract_setting_transporter_id)
            return $query->row();
        else
            return $query->result();
    }

    function save() {

        if ($this->safa_uo_contract_id !== FALSE)
            $this->db->set('safa_uo_contracts_settings_transporters.safa_uo_contract_id', $this->safa_uo_contract_id);

        if ($this->safa_uo_contract_setting_id !== FALSE)
            $this->db->set('safa_uo_contracts_settings_transporters.safa_uo_contract_setting_id', $this->safa_uo_contract_setting_id);

        if ($this->safa_transporter_id !== FALSE)
            $this->db->set('safa_uo_contracts_settings_transporters.safa_transporter_id', $this->safa_transporter_id);



        if ($this->safa_uo_contract_setting_transporter_id) {
            $this->db->where('safa_uo_contracts_settings_transporters.safa_uo_contract_setting_transporter_id', $this->safa_uo_contract_setting_transporter_id)->update('safa_uo_contracts_settings');
        } else {
            $this->db->insert('safa_uo_contracts_settings_transporters');
            return $this->db->insert_id();
        }
    }

    function delete() {
        if ($this->safa_uo_contract_setting_transporter_id !== FALSE)
            $this->db->where('safa_uo_contracts_settings_transporters.safa_uo_contract_setting_transporter_id', $this->safa_uo_contract_setting_transporter_id);

        if ($this->safa_uo_contract_id !== FALSE)
            $this->db->where('safa_uo_contracts_settings_transporters.safa_uo_contract_id', $this->safa_uo_contract_id);

        if ($this->safa_uo_contract_setting_id !== FALSE)
            $this->db->where('safa_uo_contracts_settings_transporters.safa_uo_contract_setting_id', $this->safa_uo_contract_setting_id);

        if ($this->safa_transporter_id !== FALSE)
            $this->db->where('safa_uo_contracts_settings_transporters.safa_transporter_id', $this->safa_transporter_id);


        $this->db->delete('safa_uo_contracts_settings_transporters');
        return $this->db->affected_rows();
    }

}

/* End of file contracts_model.php */
/* Location: ./application/models/contracts_model.php */