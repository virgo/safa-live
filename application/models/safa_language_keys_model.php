<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Safa_language_keys_model extends CI_Model {

    public $safa_language_key_id = FALSE;
    public $safa_language_key_name = FALSE;
    public $safa_languages_file_id = FALSE;
    public $custom_select = FALSE;
    public $limit = FALSE;
    public $offset = FALSE;
    public $order_by = FALSE;

    function __construct() {
        parent::__construct();
    }

    function get($rows_no = FALSE) {

        if ($this->custom_select !== FALSE) {
            $this->db->select('safa_language_key_id');
            $this->db->select($this->custom_select);
        }

        if ($this->safa_language_key_id !== FALSE)
            $this->db->where('safa_language_keys.safa_language_key_id', $this->safa_language_key_id);

        if ($this->safa_language_key_name !== FALSE)
            $this->db->where('safa_language_keys.safa_language_key_name', $this->safa_language_key_name);

        if ($this->safa_languages_file_id !== FALSE)
            $this->db->where('safa_language_keys.safa_languages_file_id', $this->safa_languages_file_id);


        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('safa_language_keys');
        if ($rows_no)
            return $query->num_rows();

        if ($this->safa_language_key_id)
            return $query->row();
        else
            return $query->result();
    }

    function save() {
        if ($this->safa_language_key_id !== FALSE)
            $this->db->set('safa_language_keys.safa_language_key_id', $this->safa_language_key_id);

        if ($this->safa_language_key_name !== FALSE)
            $this->db->set('safa_language_keys.safa_language_key_name', $this->safa_language_key_name);

        if ($this->safa_languages_file_id !== FALSE)
            $this->db->set('safa_language_keys.safa_languages_file_id', $this->safa_languages_file_id);



        if ($this->safa_language_key_id) {
            $this->db->where('safa_language_keys.safa_language_key_id', $this->safa_language_key_id)->update('safa_language_keys');
        } else {
            $this->db->insert('safa_language_keys');
            return $this->db->insert_id();
        }
    }

    function delete() {
        if ($this->safa_language_key_id !== FALSE)
            $this->db->where('safa_language_keys.safa_language_key_id', $this->safa_language_key_id);

        if ($this->safa_language_key_name !== FALSE)
            $this->db->where('safa_language_keys.safa_language_key_name', $this->safa_language_key_name);

        if ($this->safa_languages_file_id !== FALSE)
            $this->db->where('safa_language_keys.safa_languages_file_id', $this->safa_languages_file_id);



        $this->db->delete('safa_language_keys');
        return $this->db->affected_rows();
    }

    function search($rows_no = FALSE) {
        if ($this->custom_select !== FALSE) {
            $this->db->select('safa_language_key_id');
            $this->db->select($this->custom_select);
        }

        if ($this->safa_language_key_id !== FALSE)
            $this->db->where('safa_language_keys.safa_language_key_id', $this->safa_language_key_id);

        if ($this->safa_language_key_name !== FALSE)
            $this->db->where('safa_language_keys.safa_language_key_name', $this->safa_language_key_name);

        if ($this->safa_languages_file_id !== FALSE)
            $this->db->where('safa_language_keys.safa_languages_file_id', $this->safa_languages_file_id);


        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('safa_language_keys');
        if ($rows_no)
            return $query->num_rows();

        return $query->result();
    }

}

/* End of file safa_language_keys_model.php */
/* Location: ./application/models/safa_language_keys_model.php */