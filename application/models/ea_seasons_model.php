<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Ea_seasons_model extends CI_Model {

    public $safa_ea_season_id = FALSE;
    public $name = FALSE;
    public $start_date = FALSE;
    public $end_date = FALSE;
    public $safa_ea_id = FALSE;
    public $custom_select = FALSE;
    public $limit = FALSE;
    public $offset = FALSE;
    public $order_by = FALSE;

    function __construct() {
        parent::__construct();
    }

    function get($rows_no = FALSE) {

        if ($this->custom_select !== FALSE) {
            $this->db->select('safa_ea_season_id');
            $this->db->select($this->custom_select);
        }

        if ($this->safa_ea_season_id !== FALSE)
            $this->db->where('safa_ea_seasons.safa_ea_season_id', $this->safa_ea_season_id);

        if ($this->name !== FALSE)
            $this->db->where('safa_ea_seasons.name', $this->name);

        if ($this->start_date !== FALSE)
            $this->db->where('safa_ea_seasons.start_date', $this->start_date);

        if ($this->end_date !== FALSE)
            $this->db->where('safa_ea_seasons.end_date', $this->end_date);

        if ($this->safa_ea_id !== FALSE)
            $this->db->where('safa_ea_seasons.safa_ea_id', $this->safa_ea_id);


        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('safa_ea_seasons');
        if ($rows_no)
            return $query->num_rows();

        if ($this->safa_ea_season_id)
            return $query->row();
        else
            return $query->result();
    }

    function save() {
        if ($this->safa_ea_season_id !== FALSE)
            $this->db->set('safa_ea_seasons.safa_ea_season_id', $this->safa_ea_season_id);

        if ($this->name !== FALSE)
            $this->db->set('safa_ea_seasons.name', $this->name);

        if ($this->start_date !== FALSE)
            $this->db->set('safa_ea_seasons.start_date', $this->start_date);

        if ($this->end_date !== FALSE)
            $this->db->set('safa_ea_seasons.end_date', $this->end_date);

        if ($this->safa_ea_id !== FALSE)
            $this->db->set('safa_ea_seasons.safa_ea_id', $this->safa_ea_id);



        if ($this->safa_ea_season_id) {
            $this->db->where('safa_ea_seasons.safa_ea_season_id', $this->safa_ea_season_id)->update('safa_ea_seasons');
        } else {
            $this->db->insert('safa_ea_seasons');
            return $this->db->insert_id();
        }
    }

    function delete() {
        if ($this->safa_ea_season_id !== FALSE)
            $this->db->where('safa_ea_seasons.safa_ea_season_id', $this->safa_ea_season_id);

        if ($this->name !== FALSE)
            $this->db->where('safa_ea_seasons.name', $this->name);

        if ($this->start_date !== FALSE)
            $this->db->where('safa_ea_seasons.start_date', $this->start_date);

        if ($this->end_date !== FALSE)
            $this->db->where('safa_ea_seasons.end_date', $this->end_date);

        if ($this->safa_ea_id !== FALSE)
            $this->db->where('safa_ea_seasons.safa_ea_id', $this->safa_ea_id);



        $this->db->delete('safa_ea_seasons');
        return $this->db->affected_rows();
    }

    function search($rows_no = FALSE) {
        if ($this->custom_select !== FALSE) {
            $this->db->select('safa_ea_season_id');
            $this->db->select($this->custom_select);
        }

        if ($this->safa_ea_season_id !== FALSE)
            $this->db->where('safa_ea_seasons.safa_ea_season_id', $this->safa_ea_season_id);

        if ($this->name !== FALSE)
            $this->db->where('safa_ea_seasons.name', $this->name);

        if ($this->start_date !== FALSE)
            $this->db->where('safa_ea_seasons.start_date', $this->start_date);

        if ($this->end_date !== FALSE)
            $this->db->where('safa_ea_seasons.end_date', $this->end_date);

        if ($this->safa_ea_id !== FALSE)
            $this->db->where('safa_ea_seasons.safa_ea_id', $this->safa_ea_id);


        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('safa_ea_seasons');
        if ($rows_no)
            return $query->num_rows();

        return $query->result();
    }

}

/* End of file ea_seasons_model.php */
/* Location: ./application/models/ea_seasons_model.php */