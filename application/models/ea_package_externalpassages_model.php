<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Ea_package_externalpassages_model extends CI_Model {

    public $safa_ea_package_externalsegment_id = FALSE;
    public $start_port_id = FALSE;
    public $end_port_id = FALSE;
    public $safa_transporter_id = FALSE;
    public $safa_externalsegmenttype_id = FALSE;
    public $safa_ea_package_id = FALSE;
    public $erp_port_id = FALSE;
    public $custom_select = FALSE;
    public $limit = FALSE;
    public $offset = FALSE;
    public $order_by = FALSE;

    function __construct() {
        parent::__construct();
    }

    function get($rows_no = FALSE) {

        if ($this->custom_select !== FALSE) {
            $this->db->select('safa_ea_package_externalsegment_id');
            $this->db->select($this->custom_select);
        }

        if ($this->safa_ea_package_externalsegment_id !== FALSE)
            $this->db->where('safa_ea_package_externalsegments.safa_ea_package_externalsegment_id', $this->safa_ea_package_externalsegment_id);

        if ($this->start_port_id !== FALSE)
            $this->db->where('safa_ea_package_externalsegments.start_port_id', $this->start_port_id);

        if ($this->end_port_id !== FALSE)
            $this->db->where('safa_ea_package_externalsegments.end_port_id', $this->end_port_id);

        if ($this->safa_transporter_id !== FALSE)
            $this->db->where('safa_ea_package_externalsegments.safa_transporter_id', $this->safa_transporter_id);

        if ($this->safa_externalsegmenttype_id !== FALSE)
            $this->db->where('safa_ea_package_externalsegments.safa_externalsegmenttype_id', $this->safa_externalsegmenttype_id);

        if ($this->safa_ea_package_id !== FALSE)
            $this->db->where('safa_ea_package_externalsegments.safa_ea_package_id', $this->safa_ea_package_id);

        if ($this->erp_port_id !== FALSE)
            $this->db->where('safa_ea_package_externalsegments.erp_port_id', $this->erp_port_id);


        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('safa_ea_package_externalsegments');
        if ($rows_no)
            return $query->num_rows();

        if ($this->safa_ea_package_externalsegment_id)
            return $query->row();
        else
            return $query->result();
    }

    function save() {
        if ($this->safa_ea_package_externalsegment_id !== FALSE)
            $this->db->set('safa_ea_package_externalsegments.safa_ea_package_externalsegment_id', $this->safa_ea_package_externalsegment_id);

        if ($this->start_port_id !== FALSE)
            $this->db->set('safa_ea_package_externalsegments.start_port_id', $this->start_port_id);

        if ($this->end_port_id !== FALSE)
            $this->db->set('safa_ea_package_externalsegments.end_port_id', $this->end_port_id);

        if ($this->safa_transporter_id !== FALSE)
            $this->db->set('safa_ea_package_externalsegments.safa_transporter_id', $this->safa_transporter_id);

        if ($this->safa_externalsegmenttype_id !== FALSE)
            $this->db->set('safa_ea_package_externalsegments.safa_externalsegmenttype_id', $this->safa_externalsegmenttype_id);

        if ($this->safa_ea_package_id !== FALSE)
            $this->db->set('safa_ea_package_externalsegments.safa_ea_package_id', $this->safa_ea_package_id);

        if ($this->erp_port_id !== FALSE)
            $this->db->set('safa_ea_package_externalsegments.erp_port_id', $this->erp_port_id);



        if ($this->safa_ea_package_externalsegment_id) {
            $this->db->where('safa_ea_package_externalsegments.safa_ea_package_externalsegment_id', $this->safa_ea_package_externalsegment_id)->update('safa_ea_package_externalsegments');
        } else {
            $this->db->insert('safa_ea_package_externalsegments');
            return $this->db->insert_id();
        }
    }

    function delete() {
        if ($this->safa_ea_package_externalsegment_id !== FALSE)
            $this->db->where('safa_ea_package_externalsegments.safa_ea_package_externalsegment_id', $this->safa_ea_package_externalsegment_id);

        if ($this->start_port_id !== FALSE)
            $this->db->where('safa_ea_package_externalsegments.start_port_id', $this->start_port_id);

        if ($this->end_port_id !== FALSE)
            $this->db->where('safa_ea_package_externalsegments.end_port_id', $this->end_port_id);

        if ($this->safa_transporter_id !== FALSE)
            $this->db->where('safa_ea_package_externalsegments.safa_transporter_id', $this->safa_transporter_id);

        if ($this->safa_externalsegmenttype_id !== FALSE)
            $this->db->where('safa_ea_package_externalsegments.safa_externalsegmenttype_id', $this->safa_externalsegmenttype_id);

        if ($this->safa_ea_package_id !== FALSE)
            $this->db->where('safa_ea_package_externalsegments.safa_ea_package_id', $this->safa_ea_package_id);

        if ($this->erp_port_id !== FALSE)
            $this->db->where('safa_ea_package_externalsegments.erp_port_id', $this->erp_port_id);



        $this->db->delete('safa_ea_package_externalsegments');
        return $this->db->affected_rows();
    }

    function search($rows_no = FALSE) {
        if ($this->custom_select !== FALSE) {
            $this->db->select('safa_ea_package_externalsegment_id');
            $this->db->select($this->custom_select);
        }

        if ($this->safa_ea_package_externalsegment_id !== FALSE)
            $this->db->where('safa_ea_package_externalsegments.safa_ea_package_externalsegment_id', $this->safa_ea_package_externalsegment_id);

        if ($this->start_port_id !== FALSE)
            $this->db->where('safa_ea_package_externalsegments.start_port_id', $this->start_port_id);

        if ($this->end_port_id !== FALSE)
            $this->db->where('safa_ea_package_externalsegments.end_port_id', $this->end_port_id);

        if ($this->safa_transporter_id !== FALSE)
            $this->db->where('safa_ea_package_externalsegments.safa_transporter_id', $this->safa_transporter_id);

        if ($this->safa_externalsegmenttype_id !== FALSE)
            $this->db->where('safa_ea_package_externalsegments.safa_externalsegmenttype_id', $this->safa_externalsegmenttype_id);

        if ($this->safa_ea_package_id !== FALSE)
            $this->db->where('safa_ea_package_externalsegments.safa_ea_package_id', $this->safa_ea_package_id);

        if ($this->erp_port_id !== FALSE)
            $this->db->where('safa_ea_package_externalsegments.erp_port_id', $this->erp_port_id);


        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('safa_ea_package_externalsegments');
        if ($rows_no)
            return $query->num_rows();

        return $query->result();
    }

}

/* End of file ea_package_externalpassages_model.php */
/* Location: ./application/models/ea_package_externalpassages_model.php */