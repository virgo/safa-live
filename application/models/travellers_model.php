<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Travellers_model extends CI_Model {

    public $safa_trip_traveller_id = FALSE;
    public $safa_trip_id = FALSE;
    public $offline_id = FALSE;
    public $no = FALSE;
    public $display_order = FALSE;
    public $title_id = FALSE;
    public $first_name_la = FALSE;
    public $second_name_la = FALSE;
    public $third_name_la = FALSE;
    public $fourth_name_la = FALSE;
    public $first_name_ar = FALSE;
    public $second_name_ar = FALSE;
    public $third_name_ar = FALSE;
    public $fourth_name_ar = FALSE;
    public $picture = FALSE;
    public $nationality_id = FALSE;
    public $previous_nationality_id = FALSE;
    public $passport_no = FALSE;
    public $passport_type_id = FALSE;
    public $passport_issue_date = FALSE;
    public $passport_expiry_date = FALSE;
    public $passport_issuing_city = FALSE;
    public $passport_issuing_country_id = FALSE;
    public $passport_dpn_count = FALSE;
    public $dpn_serial_no = FALSE;
    public $relative_relation_id = FALSE;
    public $educational_level_id = FALSE;
    public $occupation = FALSE;
    public $email_address = FALSE;
    public $street_name = FALSE;
    public $date_of_birth = FALSE;
    public $age = FALSE;
    public $birth_city = FALSE;
    public $birth_country_id = FALSE;
    public $relative_no = FALSE;
    public $relative_gender_id = FALSE;
    public $gender_id = FALSE;
    public $marital_status_id = FALSE;
    public $religion = FALSE;
    public $zip_code = FALSE;
    public $city = FALSE;
    public $region_name = FALSE;
    public $erp_country_id = FALSE;
    public $mofa = FALSE;
    public $enumber = FALSE;
    public $id_number = FALSE;
    public $job2 = FALSE;
    public $address = FALSE;
    public $remarks = FALSE;
    public $canceled = FALSE;
    public $mobile = FALSE;
    public $sim_serial = FALSE;
    public $visa_number = FALSE;
    public $notes = FALSE;
    public $entry_datetime = FALSE;
    public $erp_meal_id = FALSE;
    public $custom_select = FALSE;
    public $limit = FALSE;
    public $offset = FALSE;
    public $order_by = FALSE;

    function __construct() {
        parent::__construct();
    }

    function get($rows_no = FALSE) {

        if ($this->custom_select !== FALSE) {
            $this->db->select('safa_trip_traveller_id');
            $this->db->select($this->custom_select);
        }

        if ($this->safa_trip_traveller_id !== FALSE)
            $this->db->where('safa_trip_travellers.safa_trip_traveller_id', $this->safa_trip_traveller_id);

        if ($this->safa_trip_id !== FALSE)
            $this->db->where('safa_trip_travellers.safa_trip_id', $this->safa_trip_id);

        if ($this->offline_id !== FALSE)
            $this->db->where('safa_trip_travellers.offline_id', $this->offline_id);

        if ($this->no !== FALSE)
            $this->db->where('safa_trip_travellers.no', $this->no);

        if ($this->display_order !== FALSE)
            $this->db->where('safa_trip_travellers.display_order', $this->display_order);

        if ($this->title_id !== FALSE)
            $this->db->where('safa_trip_travellers.title_id', $this->title_id);

        if ($this->first_name_la !== false)
            $this->db->where('safa_trip_travellers.first_name_la', $this->first_name_la);

        if ($this->second_name_la !== false)
            $this->db->where('safa_trip_travellers.second_name_la', $this->second_name_la);

        if ($this->third_name_la !== false)
            $this->db->where('safa_trip_travellers.third_name_la', $this->third_name_la);

        if ($this->fourth_name_la !== false)
            $this->db->where('safa_trip_travellers.fourth_name_la', $this->fourth_name_la);

        if ($this->first_name_ar !== false)
            $this->db->where('safa_trip_travellers.first_name_ar', $this->first_name_ar);

        if ($this->second_name_ar !== false)
            $this->db->where('safa_trip_travellers.second_name_ar', $this->second_name_ar);

        if ($this->third_name_ar !== false)
            $this->db->where('safa_trip_travellers.third_name_ar', $this->third_name_ar);

        if ($this->fourth_name_ar !== false)
            $this->db->where('safa_trip_travellers.fourth_name_ar', $this->fourth_name_ar);

        if ($this->picture !== FALSE)
            $this->db->where('safa_trip_travellers.picture', $this->picture);

        if ($this->nationality_id !== FALSE)
            $this->db->where('safa_trip_travellers.nationality_id', $this->nationality_id);

        if ($this->previous_nationality_id !== FALSE)
            $this->db->where('safa_trip_travellers.previous_nationality_id', $this->previous_nationality_id);

        if ($this->passport_no !== FALSE)
            $this->db->where('safa_trip_travellers.passport_no', $this->passport_no);

        if ($this->passport_type_id !== FALSE)
            $this->db->where('safa_trip_travellers.passport_type_id', $this->passport_type_id);

        if ($this->passport_issue_date !== FALSE)
            $this->db->where('safa_trip_travellers.passport_issue_date', $this->passport_issue_date);

        if ($this->passport_expiry_date !== FALSE)
            $this->db->where('safa_trip_travellers.passport_expiry_date', $this->passport_expiry_date);

        if ($this->passport_issuing_city !== FALSE)
            $this->db->where('safa_trip_travellers.passport_issuing_city', $this->passport_issuing_city);

        if ($this->passport_issuing_country_id !== FALSE)
            $this->db->where('safa_trip_travellers.passport_issuing_country_id', $this->passport_issuing_country_id);

        if ($this->passport_dpn_count !== FALSE)
            $this->db->where('safa_trip_travellers.passport_dpn_count', $this->passport_dpn_count);

        if ($this->dpn_serial_no !== FALSE)
            $this->db->where('safa_trip_travellers.dpn_serial_no', $this->dpn_serial_no);

        if ($this->relative_relation_id !== FALSE)
            $this->db->where('safa_trip_travellers.relative_relation_id', $this->relative_relation_id);

        if ($this->educational_level_id !== FALSE)
            $this->db->where('safa_trip_travellers.educational_level_id', $this->educational_level_id);

        if ($this->occupation !== FALSE)
            $this->db->where('safa_trip_travellers.occupation', $this->occupation);

        if ($this->email_address !== FALSE)
            $this->db->where('safa_trip_travellers.email_address', $this->email_address);

        if ($this->street_name !== FALSE)
            $this->db->where('safa_trip_travellers.street_name', $this->street_name);

        if ($this->date_of_birth !== FALSE)
            $this->db->where('safa_trip_travellers.date_of_birth', $this->date_of_birth);

        if ($this->age !== FALSE)
            $this->db->where('safa_trip_travellers.age', $this->age);

        if ($this->birth_city !== FALSE)
            $this->db->where('safa_trip_travellers.birth_city', $this->birth_city);

        if ($this->birth_country_id !== FALSE)
            $this->db->where('safa_trip_travellers.birth_country_id', $this->birth_country_id);

        if ($this->relative_no !== FALSE)
            $this->db->where('safa_trip_travellers.relative_no', $this->relative_no);

        if ($this->relative_gender_id !== FALSE)
            $this->db->where('safa_trip_travellers.relative_gender_id', $this->relative_gender_id);

        if ($this->gender_id !== FALSE)
            $this->db->where('safa_trip_travellers.gender_id', $this->gender_id);

        if ($this->marital_status_id !== FALSE)
            $this->db->where('safa_trip_travellers.marital_status_id', $this->marital_status_id);

        if ($this->religion !== FALSE)
            $this->db->where('safa_trip_travellers.religion', $this->religion);

        if ($this->zip_code !== FALSE)
            $this->db->where('safa_trip_travellers.zip_code', $this->zip_code);

        if ($this->city !== FALSE)
            $this->db->where('safa_trip_travellers.city', $this->city);

        if ($this->region_name !== FALSE)
            $this->db->where('safa_trip_travellers.region_name', $this->region_name);

        if ($this->erp_country_id !== FALSE)
            $this->db->where('safa_trip_travellers.erp_country_id', $this->erp_country_id);

        if ($this->mofa !== FALSE)
            $this->db->where('safa_trip_travellers.mofa', $this->mofa);

        if ($this->enumber !== FALSE)
            $this->db->where('safa_trip_travellers.enumber', $this->enumber);

        if ($this->id_number !== FALSE)
            $this->db->where('safa_trip_travellers.id_number', $this->id_number);

        if ($this->job2 !== FALSE)
            $this->db->where('safa_trip_travellers.job2', $this->job2);

        if ($this->address !== FALSE)
            $this->db->where('safa_trip_travellers.address', $this->address);

        if ($this->remarks !== FALSE)
            $this->db->where('safa_trip_travellers.remarks', $this->remarks);

        if ($this->canceled !== FALSE)
            $this->db->where('safa_trip_travellers.canceled', $this->canceled);

        if ($this->mobile !== FALSE)
            $this->db->where('safa_trip_travellers.mobile', $this->mobile);

        if ($this->sim_serial !== FALSE)
            $this->db->where('safa_trip_travellers.sim_serial', $this->sim_serial);

        if ($this->visa_number !== FALSE)
            $this->db->where('safa_trip_travellers.visa_number', $this->visa_number);

        if ($this->notes !== FALSE)
            $this->db->where('safa_trip_travellers.notes', $this->notes);

        if ($this->entry_datetime !== FALSE)
            $this->db->where('safa_trip_travellers.entry_datetime', $this->entry_datetime);

        if ($this->erp_meal_id !== FALSE)
            $this->db->where('safa_trip_travellers.erp_meal_id', $this->erp_meal_id);


        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);


        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $this->db->select('safa_trip_travellers.*
                            , erp_countries.' . name() . ' as country_name
                            , erp_gender.' . name() . ' as gender_name                                  
                            ', false);

        $this->db->join('erp_countries', 'safa_trip_travellers.nationality_id = erp_countries.erp_country_id', 'left');
        $this->db->join('erp_gender', 'erp_gender.erp_gender_id = safa_trip_travellers.gender_id', 'left');
        $query = $this->db->get('safa_trip_travellers');
        if ($rows_no)
            return $query->num_rows();

        if ($this->safa_trip_traveller_id)
            return $query->row();
        else
            return $query->result();
    }

    function save() {
        if ($this->safa_trip_traveller_id !== FALSE)
            $this->db->set('safa_trip_travellers.safa_trip_traveller_id', $this->safa_trip_traveller_id);

        if ($this->safa_trip_id !== FALSE)
            $this->db->set('safa_trip_travellers.safa_trip_id', $this->safa_trip_id);

        if ($this->offline_id !== FALSE)
            $this->db->set('safa_trip_travellers.offline_id', $this->offline_id);

        if ($this->no !== FALSE)
            $this->db->set('safa_trip_travellers.no', $this->no);

        if ($this->display_order !== FALSE)
            $this->db->set('safa_trip_travellers.display_order', $this->display_order);

        if ($this->title_id !== FALSE)
            $this->db->set('safa_trip_travellers.title_id', $this->title_id);

        if ($this->first_name_la !== FALSE)
            $this->db->set('safa_trip_travellers.first_name_la', $this->first_name_la);

        if ($this->second_name_la !== FALSE)
            $this->db->set('safa_trip_travellers.second_name_la', $this->second_name_la);

        if ($this->third_name_la !== FALSE)
            $this->db->set('safa_trip_travellers.third_name_la', $this->third_name_la);

        if ($this->fourth_name_la !== FALSE)
            $this->db->set('safa_trip_travellers.fourth_name_la', $this->fourth_name_la);

        if ($this->first_name_ar !== FALSE)
            $this->db->set('safa_trip_travellers.first_name_ar', $this->first_name_ar);

        if ($this->second_name_ar !== FALSE)
            $this->db->set('safa_trip_travellers.second_name_ar', $this->second_name_ar);

        if ($this->third_name_ar !== FALSE)
            $this->db->set('safa_trip_travellers.third_name_ar', $this->third_name_ar);

        if ($this->fourth_name_ar !== FALSE)
            $this->db->set('safa_trip_travellers.fourth_name_ar', $this->fourth_name_ar);

        if ($this->picture !== FALSE)
            $this->db->set('safa_trip_travellers.picture', $this->picture);

        if ($this->nationality_id !== FALSE)
            $this->db->set('safa_trip_travellers.nationality_id', $this->nationality_id);

        if ($this->previous_nationality_id !== FALSE)
            $this->db->set('safa_trip_travellers.previous_nationality_id', $this->previous_nationality_id);

        if ($this->passport_no !== FALSE)
            $this->db->set('safa_trip_travellers.passport_no', $this->passport_no);

        if ($this->passport_type_id !== FALSE)
            $this->db->set('safa_trip_travellers.passport_type_id', $this->passport_type_id);

        if ($this->passport_issue_date !== FALSE)
            $this->db->set('safa_trip_travellers.passport_issue_date', $this->passport_issue_date);

        if ($this->passport_expiry_date !== FALSE)
            $this->db->set('safa_trip_travellers.passport_expiry_date', $this->passport_expiry_date);

        if ($this->passport_issuing_city !== FALSE)
            $this->db->set('safa_trip_travellers.passport_issuing_city', $this->passport_issuing_city);

        if ($this->passport_issuing_country_id !== FALSE)
            $this->db->set('safa_trip_travellers.passport_issuing_country_id', $this->passport_issuing_country_id);

        if ($this->passport_dpn_count !== FALSE)
            $this->db->set('safa_trip_travellers.passport_dpn_count', $this->passport_dpn_count);

        if ($this->dpn_serial_no !== FALSE)
            $this->db->set('safa_trip_travellers.dpn_serial_no', $this->dpn_serial_no);

        if ($this->relative_relation_id !== FALSE)
            $this->db->set('safa_trip_travellers.relative_relation_id', $this->relative_relation_id);

        if ($this->educational_level_id !== FALSE)
            $this->db->set('safa_trip_travellers.educational_level_id', $this->educational_level_id);

        if ($this->occupation !== FALSE)
            $this->db->set('safa_trip_travellers.occupation', $this->occupation);

        if ($this->email_address !== FALSE)
            $this->db->set('safa_trip_travellers.email_address', $this->email_address);

        if ($this->street_name !== FALSE)
            $this->db->set('safa_trip_travellers.street_name', $this->street_name);

        if ($this->date_of_birth !== FALSE)
            $this->db->set('safa_trip_travellers.date_of_birth', $this->date_of_birth);

        if ($this->age !== FALSE)
            $this->db->set('safa_trip_travellers.age', $this->age);

        if ($this->birth_city !== FALSE)
            $this->db->set('safa_trip_travellers.birth_city', $this->birth_city);

        if ($this->birth_country_id !== FALSE)
            $this->db->set('safa_trip_travellers.birth_country_id', $this->birth_country_id);

        if ($this->relative_no !== FALSE)
            $this->db->set('safa_trip_travellers.relative_no', $this->relative_no);

        if ($this->relative_gender_id !== FALSE)
            $this->db->set('safa_trip_travellers.relative_gender_id', $this->relative_gender_id);

        if ($this->gender_id !== FALSE)
            $this->db->set('safa_trip_travellers.gender_id', $this->gender_id);

        if ($this->marital_status_id !== FALSE)
            $this->db->set('safa_trip_travellers.marital_status_id', $this->marital_status_id);

        if ($this->religion !== FALSE)
            $this->db->set('safa_trip_travellers.religion', $this->religion);

        if ($this->zip_code !== FALSE)
            $this->db->set('safa_trip_travellers.zip_code', $this->zip_code);

        if ($this->city !== FALSE)
            $this->db->set('safa_trip_travellers.city', $this->city);

        if ($this->region_name !== FALSE)
            $this->db->set('safa_trip_travellers.region_name', $this->region_name);

        if ($this->erp_country_id !== FALSE)
            $this->db->set('safa_trip_travellers.erp_country_id', $this->erp_country_id);

        if ($this->mofa !== FALSE)
            $this->db->set('safa_trip_travellers.mofa', $this->mofa);

        if ($this->enumber !== FALSE)
            $this->db->set('safa_trip_travellers.enumber', $this->enumber);

        if ($this->id_number !== FALSE)
            $this->db->set('safa_trip_travellers.id_number', $this->id_number);

        if ($this->job2 !== FALSE)
            $this->db->set('safa_trip_travellers.job2', $this->job2);

        if ($this->address !== FALSE)
            $this->db->set('safa_trip_travellers.address', $this->address);

        if ($this->remarks !== FALSE)
            $this->db->set('safa_trip_travellers.remarks', $this->remarks);

        if ($this->canceled !== FALSE)
            $this->db->set('safa_trip_travellers.canceled', $this->canceled);

        if ($this->mobile !== FALSE)
            $this->db->set('safa_trip_travellers.mobile', $this->mobile);

        if ($this->sim_serial !== FALSE)
            $this->db->set('safa_trip_travellers.sim_serial', $this->sim_serial);

        if ($this->visa_number !== FALSE)
            $this->db->set('safa_trip_travellers.visa_number', $this->visa_number);

        if ($this->notes !== FALSE)
            $this->db->set('safa_trip_travellers.notes', $this->notes);

        if ($this->entry_datetime !== FALSE)
            $this->db->set('safa_trip_travellers.entry_datetime', $this->entry_datetime);

        if ($this->erp_meal_id !== FALSE)
            $this->db->set('safa_trip_travellers.erp_meal_id', $this->erp_meal_id);



        if ($this->safa_trip_traveller_id) {
            $this->db->where('safa_trip_travellers.safa_trip_traveller_id', $this->safa_trip_traveller_id)->update('safa_trip_travellers');
        } else {
            $this->db->insert('safa_trip_travellers');
            return $this->db->insert_id();
        }
    }

    function delete() {

        if ($this->safa_trip_traveller_id !== FALSE)
            $this->db->where('safa_trip_travellers.safa_trip_traveller_id', $this->safa_trip_traveller_id);

        $this->db->delete('safa_trip_travellers');

        return $this->db->affected_rows();
    }

    function search($rows_no = FALSE) {


        if ($this->custom_select !== FALSE) {
            $this->db->select('safa_trip_traveller_id');
            $this->db->select($this->custom_select);
        }

        if ($this->safa_trip_traveller_id !== FALSE)
            $this->db->where('safa_trip_travellers.safa_trip_traveller_id', $this->safa_trip_traveller_id);

        if ($this->safa_trip_id !== FALSE)
            $this->db->where('safa_trip_travellers.safa_trip_id', $this->safa_trip_id);

        if ($this->offline_id !== FALSE)
            $this->db->where('safa_trip_travellers.offline_id', $this->offline_id);

        if ($this->no !== FALSE)
            $this->db->where('safa_trip_travellers.no', $this->no);

        if ($this->display_order !== FALSE)
            $this->db->where('safa_trip_travellers.display_order', $this->display_order);

        if ($this->title_id !== FALSE)
            $this->db->where('safa_trip_travellers.title_id', $this->title_id);

        if ($this->first_name_la !== FALSE)
            $this->db->where('safa_trip_travellers.first_name_la', $this->first_name_la);

        if ($this->second_name_la !== FALSE)
            $this->db->where('safa_trip_travellers.second_name_la', $this->second_name_la);

        if ($this->third_name_la !== FALSE)
            $this->db->where('safa_trip_travellers.third_name_la', $this->third_name_la);

        if ($this->fourth_name_la !== FALSE)
            $this->db->where('safa_trip_travellers.fourth_name_la', $this->fourth_name_la);

        if ($this->first_name_ar !== FALSE)
            $this->db->where('safa_trip_travellers.first_name_ar', $this->first_name_ar);

        if ($this->second_name_ar !== FALSE)
            $this->db->where('safa_trip_travellers.second_name_ar', $this->second_name_ar);

        if ($this->third_name_ar !== FALSE)
            $this->db->where('safa_trip_travellers.third_name_ar', $this->third_name_ar);

        if ($this->fourth_name_ar !== FALSE)
            $this->db->where('safa_trip_travellers.fourth_name_ar', $this->fourth_name_ar);

        if ($this->picture !== FALSE)
            $this->db->where('safa_trip_travellers.picture', $this->picture);

        if ($this->nationality_id !== FALSE)
            $this->db->where('safa_trip_travellers.nationality_id', $this->nationality_id);

        if ($this->previous_nationality_id !== FALSE)
            $this->db->where('safa_trip_travellers.previous_nationality_id', $this->previous_nationality_id);

        if ($this->passport_no !== FALSE)
            $this->db->where('safa_trip_travellers.passport_no', $this->passport_no);

        if ($this->passport_type_id !== FALSE)
            $this->db->where('safa_trip_travellers.passport_type_id', $this->passport_type_id);

        if ($this->passport_issue_date !== FALSE)
            $this->db->where('safa_trip_travellers.passport_issue_date', $this->passport_issue_date);

        if ($this->passport_expiry_date !== FALSE)
            $this->db->where('safa_trip_travellers.passport_expiry_date', $this->passport_expiry_date);

        if ($this->passport_issuing_city !== FALSE)
            $this->db->where('safa_trip_travellers.passport_issuing_city', $this->passport_issuing_city);

        if ($this->passport_issuing_country_id !== FALSE)
            $this->db->where('safa_trip_travellers.passport_issuing_country_id', $this->passport_issuing_country_id);

        if ($this->passport_dpn_count !== FALSE)
            $this->db->where('safa_trip_travellers.passport_dpn_count', $this->passport_dpn_count);

        if ($this->dpn_serial_no !== FALSE)
            $this->db->where('safa_trip_travellers.dpn_serial_no', $this->dpn_serial_no);

        if ($this->relative_relation_id !== FALSE)
            $this->db->where('safa_trip_travellers.relative_relation_id', $this->relative_relation_id);

        if ($this->educational_level_id !== FALSE)
            $this->db->where('safa_trip_travellers.educational_level_id', $this->educational_level_id);

        if ($this->occupation !== FALSE)
            $this->db->where('safa_trip_travellers.occupation', $this->occupation);

        if ($this->email_address !== FALSE)
            $this->db->where('safa_trip_travellers.email_address', $this->email_address);

        if ($this->street_name !== FALSE)
            $this->db->where('safa_trip_travellers.street_name', $this->street_name);

        if ($this->date_of_birth !== FALSE)
            $this->db->where('safa_trip_travellers.date_of_birth', $this->date_of_birth);

        if ($this->age !== FALSE)
            $this->db->where('safa_trip_travellers.age', $this->age);

        if ($this->birth_city !== FALSE)
            $this->db->where('safa_trip_travellers.birth_city', $this->birth_city);

        if ($this->birth_country_id !== FALSE)
            $this->db->where('safa_trip_travellers.birth_country_id', $this->birth_country_id);

        if ($this->relative_no !== FALSE)
            $this->db->where('safa_trip_travellers.relative_no', $this->relative_no);

        if ($this->relative_gender_id !== FALSE)
            $this->db->where('safa_trip_travellers.relative_gender_id', $this->relative_gender_id);

        if ($this->gender_id !== FALSE)
            $this->db->where('safa_trip_travellers.gender_id', $this->gender_id);

        if ($this->marital_status_id !== FALSE)
            $this->db->where('safa_trip_travellers.marital_status_id', $this->marital_status_id);

        if ($this->religion !== FALSE)
            $this->db->where('safa_trip_travellers.religion', $this->religion);

        if ($this->zip_code !== FALSE)
            $this->db->where('safa_trip_travellers.zip_code', $this->zip_code);

        if ($this->city !== FALSE)
            $this->db->where('safa_trip_travellers.city', $this->city);

        if ($this->region_name !== FALSE)
            $this->db->where('safa_trip_travellers.region_name', $this->region_name);

        if ($this->erp_country_id !== FALSE)
            $this->db->where('safa_trip_travellers.erp_country_id', $this->erp_country_id);

        if ($this->mofa !== FALSE)
            $this->db->where('safa_trip_travellers.mofa', $this->mofa);

        if ($this->enumber !== FALSE)
            $this->db->where('safa_trip_travellers.enumber', $this->enumber);

        if ($this->id_number !== FALSE)
            $this->db->where('safa_trip_travellers.id_number', $this->id_number);

        if ($this->job2 !== FALSE)
            $this->db->where('safa_trip_travellers.job2', $this->job2);

        if ($this->address !== FALSE)
            $this->db->where('safa_trip_travellers.address', $this->address);

        if ($this->remarks !== FALSE)
            $this->db->where('safa_trip_travellers.remarks', $this->remarks);

        if ($this->canceled !== FALSE)
            $this->db->where('safa_trip_travellers.canceled', $this->canceled);

        if ($this->mobile !== FALSE)
            $this->db->where('safa_trip_travellers.mobile', $this->mobile);

        if ($this->sim_serial !== FALSE)
            $this->db->where('safa_trip_travellers.sim_serial', $this->sim_serial);

        if ($this->visa_number !== FALSE)
            $this->db->where('safa_trip_travellers.visa_number', $this->visa_number);

        if ($this->notes !== FALSE)
            $this->db->where('safa_trip_travellers.notes', $this->notes);

        if ($this->entry_datetime !== FALSE)
            $this->db->where('safa_trip_travellers.entry_datetime', $this->entry_datetime);

        if ($this->erp_meal_id !== FALSE)
            $this->db->where('safa_trip_travellers.erp_meal_id', $this->erp_meal_id);


        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('safa_trip_travellers');
        if ($rows_no)
            return $query->num_rows();

        return $query->result();
    }

    function calc_next_no_field($trip_id) {
        return $this->db->where("safa_trip_id", $trip_id)->get('safa_trip_travellers')->num_rows() + 1;
    }

    function get_related_persons($id) {

        //$this->db->select('safa_trip_traveller_id,'.name());

        $query = "SELECT * FROM safa_trip_travellers WHERE safa_trip_id = $id";

        $relative_travellers = $this->db->query($query)->result_array();

        return $relative_travellers;
    }

}

/* End of file travellers_model.php */
/* Location: ./application/models/travellers_model.php */