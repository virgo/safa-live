<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Roomattributes_model extends CI_Model {

    public $safa_room_attribute_id = FALSE;
    public $name_ar = FALSE;
    public $name_la = FALSE;
    public $custom_select = FALSE;
    public $limit = FALSE;
    public $offset = FALSE;
    public $order_by = FALSE;

    function __construct() {
        parent::__construct();
    }

    function get($rows_no = FALSE) {

        if ($this->custom_select !== FALSE) {
            $this->db->select('safa_room_attribute_id');
            $this->db->select($this->custom_select);
        }

        if ($this->safa_room_attribute_id !== FALSE)
            $this->db->where('safa_rooms_attributes.safa_room_attribute_id', $this->safa_room_attribute_id);

        if ($this->name_ar !== FALSE)
            $this->db->where('safa_rooms_attributes.name_ar', $this->name_ar);

        if ($this->name_la !== FALSE)
            $this->db->where('safa_rooms_attributes.name_la', $this->name_la);


        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('safa_rooms_attributes');
        if ($rows_no)
            return $query->num_rows();
        if ($this->safa_room_attribute_id)
            return $query->row();
        else
            return $query->result();
    }

    function save() {
        if ($this->safa_room_attribute_id !== FALSE)
            $this->db->set('safa_rooms_attributes.safa_room_attribute_id', $this->safa_room_attribute_id);

        if ($this->name_ar !== FALSE)
            $this->db->set('safa_rooms_attributes.name_ar', $this->name_ar);

        if ($this->name_la !== FALSE)
            $this->db->set('safa_rooms_attributes.name_la', $this->name_la);

        if ($this->safa_room_attribute_id) {
            $this->db->where('safa_rooms_attributes.safa_room_attribute_id', $this->safa_room_attribute_id)->update('safa_rooms_attributes');
        } else {
            $this->db->insert('safa_rooms_attributes');
            return $this->db->insert_id();
        }
    }

    function delete() {
        if ($this->safa_room_attribute_id !== FALSE)
            $this->db->where('safa_rooms_attributes.safa_room_attribute_id', $this->safa_room_attribute_id);

        if ($this->name_ar !== FALSE)
            $this->db->where('safa_rooms_attributes.name_ar', $this->name_ar);

        if ($this->name_la !== FALSE)
            $this->db->where('safa_rooms_attributes.name_la', $this->name_la);

        if ($this->code !== FALSE)
            $this->db->where('safa_rooms_attributes.code', $this->code);

        $this->db->delete('safa_rooms_attributes');
        return $this->db->affected_rows();
    }

    function search($rows_no = FALSE) {
        if ($this->custom_select !== FALSE) {
            $this->db->select('safa_room_attribute_id');
            $this->db->select($this->custom_select);
        }

        if ($this->safa_room_attribute_id !== FALSE)
            $this->db->where('safa_rooms_attributes.safa_room_attribute_id', $this->safa_room_attribute_id);

        if ($this->name_ar !== FALSE)
            $this->db->where('safa_rooms_attributes.name_ar', $this->name_ar);

        if ($this->name_la !== FALSE)
            $this->db->where('safa_rooms_attributes.name_la', $this->name_la);

        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('safa_rooms_attributes');
        if ($rows_no)
            return $query->num_rows();

        return $query->result();
    }

    function check_delete_ability($id = false) {
        
    }

}

/* End of file meals_model.php */
/* Location: ./application/models/meals_model.php */