<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Transporters_model extends CI_Model {

    public $safa_transporter_id = FALSE;
    public $erp_country_id = FALSE;
    public $erp_transportertype_id = FALSE;
    public $name_ar = FALSE;
    public $name_la = FALSE;
    public $code = FALSE;
    public $custom_select = FALSE;
    public $limit = FALSE;
    public $offset = FALSE;
    public $order_by = FALSE;

    function __construct() {
        parent::__construct();
    }

    function get($rows_no = FALSE) {

        if ($this->custom_select !== FALSE) {
            $this->db->select('safa_transporter_id');
            $this->db->select($this->custom_select);
        }

        if ($this->safa_transporter_id !== FALSE)
            $this->db->where('safa_transporters.safa_transporter_id', $this->safa_transporter_id);

        if ($this->erp_country_id !== FALSE)
            $this->db->where('safa_transporters.erp_country_id', $this->erp_country_id);

        if ($this->erp_transportertype_id !== FALSE)
            $this->db->where('safa_transporters.erp_transportertype_id', $this->erp_transportertype_id);

        if ($this->name_ar !== FALSE)
            $this->db->where('safa_transporters.name_ar', $this->name_ar);

        if ($this->name_la !== FALSE)
            $this->db->where('safa_transporters.name_la', $this->name_la);

        if ($this->code !== FALSE)
            $this->db->where('safa_transporters.code', $this->code);


        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('safa_transporters');
        if ($rows_no)
            return $query->num_rows();

        if ($this->safa_transporter_id)
            return $query->row();
        else
            return $query->result();
    }

    function save() {
        if ($this->safa_transporter_id !== FALSE)
            $this->db->set('safa_transporters.safa_transporter_id', $this->safa_transporter_id);

        if ($this->erp_country_id !== FALSE)
            $this->db->set('safa_transporters.erp_country_id', $this->erp_country_id);

        if ($this->erp_transportertype_id !== FALSE)
            $this->db->set('safa_transporters.erp_transportertype_id', $this->erp_transportertype_id);

        if ($this->name_ar !== FALSE)
            $this->db->set('safa_transporters.name_ar', $this->name_ar);

        if ($this->name_la !== FALSE)
            $this->db->set('safa_transporters.name_la', $this->name_la);

        if ($this->code !== FALSE)
            $this->db->set('safa_transporters.code', $this->code);



        if ($this->safa_transporter_id) {
            $this->db->where('safa_transporters.safa_transporter_id', $this->safa_transporter_id)->update('safa_transporters');
        } else {
            $this->db->insert('safa_transporters');
            return $this->db->insert_id();
        }
    }

    function delete() {
        if ($this->safa_transporter_id !== FALSE)
            $this->db->where('safa_transporters.safa_transporter_id', $this->safa_transporter_id);

        if ($this->erp_country_id !== FALSE)
            $this->db->where('safa_transporters.erp_country_id', $this->erp_country_id);

        if ($this->erp_transportertype_id !== FALSE)
            $this->db->where('safa_transporters.erp_transportertype_id', $this->erp_transportertype_id);

        if ($this->name_ar !== FALSE)
            $this->db->where('safa_transporters.name_ar', $this->name_ar);

        if ($this->name_la !== FALSE)
            $this->db->where('safa_transporters.name_la', $this->name_la);

        if ($this->code !== FALSE)
            $this->db->where('safa_transporters.code', $this->code);



        $this->db->delete('safa_transporters');
        return $this->db->affected_rows();
    }

    function search($rows_no = FALSE) {
        if ($this->custom_select !== FALSE) {
            $this->db->select('safa_transporter_id');
            $this->db->select($this->custom_select);
        }

        if ($this->safa_transporter_id !== FALSE)
            $this->db->where('safa_transporters.safa_transporter_id', $this->safa_transporter_id);

        if ($this->erp_country_id !== FALSE)
            $this->db->where('safa_transporters.erp_country_id', $this->erp_country_id);

        if ($this->erp_transportertype_id !== FALSE)
            $this->db->where('safa_transporters.erp_transportertype_id', $this->erp_transportertype_id);

        if ($this->name_ar !== FALSE)
            $this->db->where('safa_transporters.name_ar', $this->name_ar);

        if ($this->name_la !== FALSE)
            $this->db->where('safa_transporters.name_la', $this->name_la);

        if ($this->code !== FALSE)
            $this->db->where('safa_transporters.code', $this->code);


        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('safa_transporters');
        if ($rows_no)
            return $query->num_rows();

        return $query->result();
    }

    function check_delete_ability($id) {

        $this->db->select('count(safa_trip_externalsegments.safa_transporter_id)as trip_externalsegments , count(safa_uo_contracts_transporters.safa_transporter_id)as contracts_transporters');
        $this->db->from('safa_transporters');
        $this->db->join('safa_trip_externalsegments', 'safa_transporters.safa_transporter_id = safa_trip_externalsegments.safa_transporter_id', 'left');
        $this->db->join('safa_uo_contracts_transporters', 'safa_transporters.safa_transporter_id = safa_uo_contracts_transporters.safa_transporter_id', 'left');
        $this->db->group_by('safa_transporters.safa_transporter_id');
        $this->db->where('safa_transporters.safa_transporter_id', $id);
        $query = $this->db->get();
        $flag = 0;

//        print_r($query->result());
        foreach ($query->result() as $row => $table) {
            foreach ($table as $col => $value) {
                if ($value > 0) {
                    $flag = 1;
                    break;
                } else {
                    continue;
                }
            }
        }
        return $flag;
    }

}

/* End of file transporters_model.php */
/* Location: ./application/models/transporters_model.php */