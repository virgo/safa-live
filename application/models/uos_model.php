<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Uos_model extends CI_Model {

    public $safa_uo_id = FALSE;
    public $name_ar = FALSE;
    public $name_la = FALSE;
    public $code = FALSE;
    public $phone = FALSE;
    public $mobile = FALSE;
    public $email = FALSE;
    public $fax = FALSE;
    public $custom_select = FALSE;
    public $limit = FALSE;
    public $offset = FALSE;
    public $order_by = FALSE;

    function __construct() {
        parent::__construct();
    }

    function get($rows_no = FALSE) {

        if ($this->custom_select !== FALSE) {
            $this->db->select('safa_uo_id');
            $this->db->select($this->custom_select);
        }

        if ($this->safa_uo_id !== FALSE)
            $this->db->where('safa_uos.safa_uo_id', $this->safa_uo_id);

        if ($this->name_ar !== FALSE)
            $this->db->where('safa_uos.name_ar', $this->name_ar);

        if ($this->name_la !== FALSE)
            $this->db->where('safa_uos.name_la', $this->name_la);

        if ($this->code !== FALSE)
            $this->db->where('safa_uos.code', $this->code);

        if ($this->phone !== FALSE)
            $this->db->where('safa_uos.phone', $this->phone);

        if ($this->mobile !== FALSE)
            $this->db->where('safa_uos.mobile', $this->mobile);

        if ($this->email !== FALSE)
            $this->db->where('safa_uos.email', $this->email);

        if ($this->fax !== FALSE)
            $this->db->where('safa_uos.fax', $this->fax);


        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('safa_uos');
        if ($rows_no)
            return $query->num_rows();

        if ($this->safa_uo_id)
            return $query->row();
        else
            return $query->result();
    }

    function save() {
        if ($this->safa_uo_id !== FALSE)
            $this->db->set('safa_uos.safa_uo_id', $this->safa_uo_id);

        if ($this->name_ar !== FALSE)
            $this->db->set('safa_uos.name_ar', $this->name_ar);

        if ($this->name_la !== FALSE)
            $this->db->set('safa_uos.name_la', $this->name_la);

        if ($this->code !== FALSE)
            $this->db->set('safa_uos.code', $this->code);

        if ($this->phone !== FALSE)
            $this->db->set('safa_uos.phone', $this->phone);

        if ($this->mobile !== FALSE)
            $this->db->set('safa_uos.mobile', $this->mobile);

        if ($this->email !== FALSE)
            $this->db->set('safa_uos.email', $this->email);

        if ($this->fax !== FALSE)
            $this->db->set('safa_uos.fax', $this->fax);



        if ($this->safa_uo_id) {
            $this->db->where('safa_uos.safa_uo_id', $this->safa_uo_id)->update('safa_uos');
        } else {
            $this->db->insert('safa_uos');
            return $this->db->insert_id();
        }
    }

    function delete() {
        if ($this->safa_uo_id !== FALSE)
            $this->db->where('safa_uos.safa_uo_id', $this->safa_uo_id);

        if ($this->name_ar !== FALSE)
            $this->db->where('safa_uos.name_ar', $this->name_ar);

        if ($this->name_la !== FALSE)
            $this->db->where('safa_uos.name_la', $this->name_la);

        if ($this->code !== FALSE)
            $this->db->where('safa_uos.code', $this->code);

        if ($this->phone !== FALSE)
            $this->db->where('safa_uos.phone', $this->phone);

        if ($this->mobile !== FALSE)
            $this->db->where('safa_uos.mobile', $this->mobile);

        if ($this->email !== FALSE)
            $this->db->where('safa_uos.email', $this->email);

        if ($this->fax !== FALSE)
            $this->db->where('safa_uos.fax', $this->fax);



        $this->db->delete('safa_uos');
        return $this->db->affected_rows();
    }

    function search($rows_no = FALSE) {
        if ($this->custom_select !== FALSE) {
            $this->db->select('safa_uo_id');
            $this->db->select($this->custom_select);
        }

        if ($this->safa_uo_id !== FALSE)
            $this->db->where('safa_uos.safa_uo_id', $this->safa_uo_id);

        if ($this->name_ar !== FALSE)
            $this->db->where('safa_uos.name_ar', $this->name_ar);

        if ($this->name_la !== FALSE)
            $this->db->where('safa_uos.name_la', $this->name_la);

        if ($this->code !== FALSE)
            $this->db->where('safa_uos.code', $this->code);

        if ($this->phone !== FALSE)
            $this->db->where('safa_uos.phone', $this->phone);

        if ($this->mobile !== FALSE)
            $this->db->where('safa_uos.mobile', $this->mobile);

        if ($this->email !== FALSE)
            $this->db->where('safa_uos.email', $this->email);

        if ($this->fax !== FALSE)
            $this->db->where('safa_uos.fax', $this->fax);


        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('safa_uos');
        if ($rows_no)
            return $query->num_rows();

        return $query->result();
    }

    function get_by_safa_ea_package_id($safa_ea_package_id) {

        $this->db->select('safa_uos.safa_uo_id, safa_uos.name_ar, safa_uos.name_la ');
        $this->db->from('safa_uos');
        $this->db->join('safa_uo_contracts', 'safa_uo_contracts.safa_uo_id = safa_uos.safa_uo_id');
        $this->db->join('safa_uo_contracts_eas', 'safa_uo_contracts_eas.safa_uo_contract_id = safa_uo_contracts.safa_uo_contract_id');
        $this->db->join('safa_ea_packages', 'safa_uo_contracts_eas.safa_uo_contract_ea_id = safa_ea_packages.safa_uo_contract_ea_id');
        $this->db->where('safa_ea_packages.safa_ea_package_id', $safa_ea_package_id);

        $uo_row = $this->db->get()->row();
        return $uo_row;
    }

}

/* End of file uos_model.php */
/* Location: ./application/models/uos_model.php */