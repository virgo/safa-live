<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Safa_uo_users_model extends CI_Model {

    public $safa_uo_user_id = FALSE;
    public $name_ar = FALSE;
    public $name_la = FALSE;
    public $username = FALSE;
    public $password = FALSE;
    public $safa_uo_id = FALSE;
    public $safa_uo_usergroup_id = FALSE;
    public $mobile = FALSE;
    public $email = FALSE;
    public $reset_password = FALSE;
    public $ver_code = FALSE;
    public $timestamp = FALSE;
    public $erp_language_id = FALSE;
    
    public $safa_uo_user_type_id_in = FALSE;
    public $erp_city_id = FALSE;
    public $port_type_in = FALSE;
    
    public $custom_select = FALSE;
    public $limit = FALSE;
    public $offset = FALSE;
    public $order_by = FALSE;
    public $join = FALSE;

    function __construct() {
        parent::__construct();
    }

    function get($rows_no = FALSE) {

        if ($this->custom_select !== FALSE) {
            $this->db->select('safa_uo_user_id');
            $this->db->select($this->custom_select);
        }

        if ($this->safa_uo_user_id !== FALSE)
            $this->db->where('safa_uo_users.safa_uo_user_id', $this->safa_uo_user_id);

        if ($this->name_ar !== FALSE)
            $this->db->where('safa_uo_users.name_ar', $this->name_ar);

        if ($this->name_la !== FALSE)
            $this->db->where('safa_uo_users.name_la', $this->name_la);

        if ($this->username !== FALSE)
            $this->db->where('safa_uo_users.username', $this->username);

        if ($this->password !== FALSE)
            $this->db->where('safa_uo_users.password', $this->password);

        if ($this->safa_uo_id !== FALSE)
            $this->db->where('safa_uo_users.safa_uo_id', $this->safa_uo_id);

        if ($this->safa_uo_usergroup_id !== FALSE)
            $this->db->where('safa_uo_users.safa_uo_usergroup_id', $this->safa_uo_usergroup_id);

        if ($this->email !== FALSE)
            $this->db->where('safa_uo_users.email', $this->email);

        if ($this->mobile !== FALSE)
            $this->db->where('safa_uo_users.mobile', $this->mobile);

        if ($this->ver_code !== FALSE)
            $this->db->where('safa_uo_users.ver_code', $this->ver_code);

        if ($this->reset_password !== FALSE)
            $this->db->where('safa_uo_users.reset_password', $this->reset_password);

        if ($this->timestamp !== FALSE)
            $this->db->where('safa_uo_users.timestamp', $this->timestamp);
        if ($this->erp_language_id !== FALSE)
            $this->db->where('safa_uo_users.erp_language_id', $this->erp_language_id);
            
        if ($this->safa_uo_user_type_id_in !== FALSE)
            $this->db->where_in('safa_uo_users.safa_uo_user_type_id', $this->safa_uo_user_type_id_in);

            
        if ($this->erp_city_id !== FALSE || $this->port_type_in !== FALSE) {
         	$this->db->join('safa_uo_user_info', 'safa_uo_users.safa_uo_user_id = safa_uo_user_info.safa_uo_user_id', 'left');
        }
    	if ($this->erp_city_id !== FALSE) {
         	$this->db->where_in('safa_uo_user_info.erp_city_id', $this->erp_city_id);
        }   
    	if ($this->port_type_in !== FALSE) {
         	$this->db->where_in('safa_uo_user_info.port_type', $this->port_type_in);
        }
        
            
        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);
        if ($this->join) {
            $this->db->select("safa_uo_users.* ,safa_uo_usergroups.name_ar as usergroup_name_ar,safa_uo_usergroups.name_la 
            as usergroup_name_la,safa_uo_usergroups.safa_uo_usergroup_id as  usergroup_id");
            $this->db->join('safa_uo_usergroups', 'safa_uo_users.safa_uo_usergroup_id = safa_uo_usergroups.safa_uo_usergroup_id', 'left');
        }

        $query = $this->db->get('safa_uo_users');
        
        //echo $this->db->last_query(); exit;
        
        if ($rows_no)
            return $query->num_rows();

        if ($this->safa_uo_user_id)
            return $query->row();
        else
            return $query->result();
    }

    function save() {
        if ($this->safa_uo_user_id !== FALSE)
            $this->db->set('safa_uo_users.safa_uo_user_id', $this->safa_uo_user_id);

        if ($this->name_ar !== FALSE)
            $this->db->set('safa_uo_users.name_ar', $this->name_ar);

        if ($this->name_la !== FALSE)
            $this->db->set('safa_uo_users.name_la', $this->name_la);

        if ($this->username !== FALSE)
            $this->db->set('safa_uo_users.username', $this->username);

        if ($this->password !== FALSE)
            $this->db->set('safa_uo_users.password', $this->password);

        if ($this->safa_uo_id !== FALSE)
            $this->db->set('safa_uo_users.safa_uo_id', $this->safa_uo_id);

        if ($this->safa_uo_usergroup_id !== FALSE)
            $this->db->set('safa_uo_users.safa_uo_usergroup_id', $this->safa_uo_usergroup_id);

        if ($this->ver_code !== FALSE)
            $this->db->set('safa_uo_users.ver_code', $this->ver_code);

        if ($this->email !== FALSE)
            $this->db->set('safa_uo_users.email', $this->email);

        if ($this->mobile !== FALSE)
            $this->db->set('safa_uo_users.mobile', $this->mobile);


        if ($this->reset_password !== FALSE)
            $this->db->set('safa_uo_users.reset_password', $this->reset_password);

        if ($this->timestamp !== FALSE)
            $this->db->set('safa_uo_users.timestamp', $this->timestamp);
        if ($this->erp_language_id !== FALSE)
            $this->db->where('safa_uo_users.erp_language_id', $this->erp_language_id);

        if ($this->safa_uo_user_id) {
            $this->db->where('safa_uo_users.safa_uo_user_id', $this->safa_uo_user_id)->update('safa_uo_users');
        } else {
            $this->db->insert('safa_uo_users');
            return $this->db->insert_id();
        }
    }

    function delete() {
        if ($this->safa_uo_user_id !== FALSE)
            $this->db->where('safa_uo_users.safa_uo_user_id', $this->safa_uo_user_id);

        if ($this->name_ar !== FALSE)
            $this->db->where('safa_uo_users.name_ar', $this->name_ar);

        if ($this->name_la !== FALSE)
            $this->db->where('safa_uo_users.name_la', $this->name_la);

        if ($this->username !== FALSE)
            $this->db->where('safa_uo_users.username', $this->username);

        if ($this->password !== FALSE)
            $this->db->where('safa_uo_users.password', $this->password);

        if ($this->safa_uo_id !== FALSE)
            $this->db->where('safa_uo_users.safa_uo_id', $this->safa_uo_id);

        if ($this->safa_uo_usergroup_id !== FALSE)
            $this->db->where('safa_uo_users.safa_uo_usergroup_id', $this->safa_uo_usergroup_id);

        if ($this->email !== FALSE)
            $this->db->where('safa_uo_users.email', $this->email);

        if ($this->mobile !== FALSE)
            $this->db->where('safa_uo_users.mobile', $this->mobile);

        if ($this->ver_code !== FALSE)
            $this->db->where('safa_uo_users.ver_code', $this->ver_code);

        if ($this->reset_password !== FALSE)
            $this->db->where('safa_uo_users.reset_password', $this->reset_password);

        if ($this->timestamp !== FALSE)
            $this->db->where('safa_uo_users.timestamp', $this->timestamp);
        if ($this->erp_language_id !== FALSE)
            $this->db->where('safa_uo_users.erp_language_id', $this->erp_language_id);

        $this->db->delete('safa_uo_users');
        return $this->db->affected_rows();
    }

    function search($rows_no = FALSE) {
        if ($this->custom_select !== FALSE) {
            $this->db->select('safa_uo_user_id');
            $this->db->select($this->custom_select);
        }

        if ($this->safa_uo_user_id !== FALSE)
            $this->db->where('safa_uo_users.safa_uo_user_id', $this->safa_uo_user_id);

        if ($this->name_ar !== FALSE)
            $this->db->where('safa_uo_users.name_ar', $this->name_ar);

        if ($this->name_la !== FALSE)
            $this->db->where('safa_uo_users.name_la', $this->name_la);

        if ($this->username !== FALSE)
            $this->db->where('safa_uo_users.username', $this->username);

        if ($this->password !== FALSE)
            $this->db->where('safa_uo_users.password', $this->password);

        if ($this->safa_uo_id !== FALSE)
            $this->db->where('safa_uo_users.safa_uo_id', $this->safa_uo_id);

        if ($this->safa_uo_usergroup_id !== FALSE)
            $this->db->where('safa_uo_users.safa_uo_usergroup_id', $this->safa_uo_usergroup_id);

        if ($this->email !== FALSE)
            $this->db->where('safa_uo_users.email', $this->email);

        if ($this->mobile !== FALSE)
            $this->db->where('safa_uo_users.mobile', $this->mobile);

        if ($this->ver_code !== FALSE)
            $this->db->where('safa_uo_users.ver_code', $this->ver_code);

        if ($this->reset_password !== FALSE)
            $this->db->where('safa_uo_users.reset_password', $this->reset_password);

        if ($this->timestamp !== FALSE)
            $this->db->where('safa_uo_users.timestamp', $this->timestamp);
        if ($this->erp_language_id !== FALSE)
            $this->db->where('safa_uo_users.erp_language_id', $this->erp_language_id);



        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('safa_uo_users');
        if ($rows_no)
            return $query->num_rows();

        return $query->result();
    }

    function check_login($username, $password, $code) {
        $this->db->select('safa_uo_users.*, safa_uos.' . name() . ' as uo_name');
        $this->db->where('safa_uo_users.username', $username);
        $this->db->where('safa_uo_users.password', $password);
        $this->db->where('safa_uos.code', $code);
        $this->db->join('safa_uos', 'safa_uo_users.safa_uo_id = safa_uos.safa_uo_id');
        return $this->db->get('safa_uo_users')->row();
    }

    function update_user_info($user_id) {

        if (session('language') == 'arabic') {
            $data = array(
                'username' => session('uo_username'),
                'password' => session('uo_password'),
                'erp_language_id' => 2
            );

            $this->db->where('safa_uo_user_id', $user_id);
            $this->db->update('safa_uo_users', $data);
        } else {
            $data = array(
                'username' => session('uo_username'),
                'password' => session('uo_password'),
                'erp_language_id' => 1
            );

            $this->db->where('safa_uo_user_id', $user_id);
            $this->db->update('safa_uo_users', $data);
        }
    }

    function getlanguage($user_id) {
        $this->db->select('erp_languages.name, erp_languages.path');
        $this->db->from('safa_uo_users');
        $this->db->join('erp_languages', 'safa_uo_users.erp_language_id = erp_languages.erp_language_id');
        $this->db->where('safa_uo_users.safa_uo_user_id', $user_id);
        $query = $this->db->get();
        return $query->row();
    }

    function update_user_lang($user_id, $language) {

        if ($language == 'arabic') {
            $data = array(
                'erp_language_id' => 2
            );

            $this->db->where('safa_uo_user_id', $user_id);
            return $this->db->update('safa_uo_users', $data);
        } else {
            $data = array(
                'erp_language_id' => 1
            );

            $this->db->where('safa_uo_user_id', $user_id);
            return $this->db->update('safa_uo_users', $data);
        }
    }

    function getuser($user_id) {

        $this->db->where('safa_uo_user_id', $user_id);
        return $this->db->get('safa_uo_users')->row();
    }

}

/* End of file safa_uo_users_model.php */
/* Location: ./application/models/safa_uo_users_model.php */
