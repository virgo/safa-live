<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Views_model extends CI_Model {

    public $views_id = FALSE;
    public $name = FALSE;
    public $default = FALSE;
    public $order = FALSE;
    public $views_categories_id = FALSE;
    public $datatype = FALSE;
    public $value = FALSE;
    public $custom_select = FALSE;
    public $limit = FALSE;
    public $offset = FALSE;
    public $order_by = FALSE;

    function __construct() {
        parent::__construct();
    }

    function get($rows_no = FALSE) {

        if ($this->custom_select !== FALSE) {
            $this->db->select('views_id');
            $this->db->select($this->custom_select);
        }

        if ($this->views_id !== FALSE)
            $this->db->where('views.views_id', $this->views_id);

        if ($this->name !== FALSE)
            $this->db->where('views.name', $this->name);

        if ($this->default !== FALSE)
            $this->db->where('views.default', $this->default);

        if ($this->order !== FALSE)
            $this->db->where('views.order', $this->order);

        if ($this->views_categories_id !== FALSE)
            $this->db->where('views.views_categories_id', $this->views_categories_id);

        if ($this->datatype !== FALSE)
            $this->db->where('views.datatype', $this->datatype);

        if ($this->value !== FALSE)
            $this->db->where('views.value', $this->value);


        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('views');
        if ($rows_no)
            return $query->num_rows();

        if ($this->views_id)
            return $query->row();
        else
            return $query->result();
    }

    function save() {
        if ($this->views_id !== FALSE)
            $this->db->set('views.views_id', $this->views_id);

        if ($this->name !== FALSE)
            $this->db->set('views.name', $this->name);

        if ($this->default !== FALSE)
            $this->db->set('views.default', $this->default);

        if ($this->order !== FALSE)
            $this->db->set('views.order', $this->order);

        if ($this->views_categories_id !== FALSE)
            $this->db->set('views.views_categories_id', $this->views_categories_id);

        if ($this->datatype !== FALSE)
            $this->db->set('views.datatype', $this->datatype);

        if ($this->value !== FALSE)
            $this->db->set('views.value', $this->value);



        if ($this->views_id) {
            $this->db->where('views.views_id', $this->views_id)->update('views');
        } else {
            $this->db->insert('views');
            return $this->db->insert_id();
        }
    }

    function delete() {
        if ($this->views_id !== FALSE)
            $this->db->where('views.views_id', $this->views_id);

        if ($this->name !== FALSE)
            $this->db->where('views.name', $this->name);

        if ($this->default !== FALSE)
            $this->db->where('views.default', $this->default);

        if ($this->order !== FALSE)
            $this->db->where('views.order', $this->order);

        if ($this->views_categories_id !== FALSE)
            $this->db->where('views.views_categories_id', $this->views_categories_id);

        if ($this->datatype !== FALSE)
            $this->db->where('views.datatype', $this->datatype);

        if ($this->value !== FALSE)
            $this->db->where('views.value', $this->value);



        $this->db->delete('views');
        return $this->db->affected_rows();
    }

    function search($rows_no = FALSE) {
        if ($this->custom_select !== FALSE) {
            $this->db->select('views_id');
            $this->db->select($this->custom_select);
        }

        if ($this->views_id !== FALSE)
            $this->db->where('views.views_id', $this->views_id);

        if ($this->name !== FALSE)
            $this->db->where('views.name', $this->name);

        if ($this->default !== FALSE)
            $this->db->where('views.default', $this->default);

        if ($this->order !== FALSE)
            $this->db->where('views.order', $this->order);

        if ($this->views_categories_id !== FALSE)
            $this->db->where('views.views_categories_id', $this->views_categories_id);

        if ($this->datatype !== FALSE)
            $this->db->where('views.datatype', $this->datatype);

        if ($this->value !== FALSE)
            $this->db->where('views.value', $this->value);


        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('views');
        if ($rows_no)
            return $query->num_rows();

        return $query->result();
    }

}

/* End of file views_model.php */
/* Location: ./application/models/views_model.php */