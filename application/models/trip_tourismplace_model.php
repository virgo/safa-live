<?php

class Trip_tourismplace_model extends CI_Model {

    /**
     * store this safa_trip_tourismplace table name.
     *
     * @var string
     * @access public
     */
    public $table = 'safa_trip_tourismplaces';

    /**
     * Constructor
     *
     * @access public
     */
    function Main_model() {
        parent::__construct();
    }

    function get_trip_tourismplaces_by_trip_id($trip_id) {
        $this->db->select('*');
        $this->db->from('safa_trip_tourismplaces tt');
        $this->db->join('safa_tourismplaces t', 't.safa_tourismplace_id = tt.safa_tourismplace_id');
        $this->db->where("safa_trip_id", $trip_id);
        $this->db->order_by("tt.datetime", 'ASC');
        $query = $this->db->get();
        return $query->result();
    }

    function delete_trip_tourismplace_by_trip_id($trip_id) {
        $this->db->where("safa_trip_id", $trip_id);
        return $this->db->delete("safa_trip_tourismplaces");
    }

    function insert($data) {
        $this->db->insert('safa_trip_tourismplaces', $data);
    }

    function update($trip_tourismplace_id, $data) {
        $this->db->where('safa_trip_tourismplace_id', $trip_tourismplace_id);
        $this->db->update('safa_trip_tourismplaces', $data);
    }

    function delete_by_trip_and_id($trip_id, $trip_tourismplace_id) {
        $this->db->where("safa_trip_id", $trip_id);
        $this->db->where("safa_trip_tourismplace_id", $trip_tourismplace_id);
        return $this->db->delete("safa_trip_tourismplaces");
    }

}

?>