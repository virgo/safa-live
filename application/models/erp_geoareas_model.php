<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Erp_geoareas_model extends CI_Model {

    public $geoarea_id = FALSE;
    public $erp_city_id = FALSE;
    public $name_ar = FALSE;
    public $name_la = FALSE;
    public $distance_from = FALSE;
    public $distance_to = FALSE;
    public $custom_select = FALSE;
    public $limit = FALSE;
    public $offset = FALSE;
    public $order_by = FALSE;

    function __construct() {
        parent::__construct();
    }

    function get($rows_no = FALSE) {

        if ($this->custom_select !== FALSE) {
            $this->db->select('geoarea_id');
            $this->db->select($this->custom_select);
        }

        if ($this->geoarea_id !== FALSE)
            $this->db->where('erp_geoareas.geoarea_id', $this->geoarea_id);

        if ($this->erp_city_id !== FALSE)
            $this->db->where('erp_geoareas.erp_city_id', $this->erp_city_id);

        if ($this->name_ar !== FALSE)
            $this->db->where('erp_geoareas.name_ar', $this->name_ar);

        if ($this->name_la !== FALSE)
            $this->db->where('erp_geoareas.name_la', $this->name_la);

        if ($this->distance_from !== FALSE)
            $this->db->where('erp_geoareas.distance_from', $this->distance_from);

        if ($this->distance_to !== FALSE)
            $this->db->where('erp_geoareas.distance_to', $this->distance_to);


        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('erp_geoareas');
        if ($rows_no)
            return $query->num_rows();

        if ($this->geoarea_id)
            return $query->row();
        else
            return $query->result();
    }

    function save() {
        if ($this->geoarea_id !== FALSE)
            $this->db->set('erp_geoareas.geoarea_id', $this->geoarea_id);

        if ($this->erp_city_id !== FALSE)
            $this->db->set('erp_geoareas.erp_city_id', $this->erp_city_id);

        if ($this->name_ar !== FALSE)
            $this->db->set('erp_geoareas.name_ar', $this->name_ar);

        if ($this->name_la !== FALSE)
            $this->db->set('erp_geoareas.name_la', $this->name_la);

        if ($this->distance_from !== FALSE)
            $this->db->set('erp_geoareas.distance_from', $this->distance_from);

        if ($this->distance_to !== FALSE)
            $this->db->set('erp_geoareas.distance_to', $this->distance_to);



        if ($this->geoarea_id) {
            $this->db->where('erp_geoareas.geoarea_id', $this->geoarea_id)->update('erp_geoareas');
        } else {
            $this->db->insert('erp_geoareas');
            return $this->db->insert_id();
        }
    }

    function delete() {
        if ($this->geoarea_id !== FALSE)
            $this->db->where('erp_geoareas.geoarea_id', $this->geoarea_id);

        if ($this->erp_city_id !== FALSE)
            $this->db->where('erp_geoareas.erp_city_id', $this->erp_city_id);

        if ($this->name_ar !== FALSE)
            $this->db->where('erp_geoareas.name_ar', $this->name_ar);

        if ($this->name_la !== FALSE)
            $this->db->where('erp_geoareas.name_la', $this->name_la);

        if ($this->distance_from !== FALSE)
            $this->db->where('erp_geoareas.distance_from', $this->distance_from);

        if ($this->distance_to !== FALSE)
            $this->db->where('erp_geoareas.distance_to', $this->distance_to);



        $this->db->delete('erp_geoareas');
        return $this->db->affected_rows();
    }

    function search($rows_no = FALSE) {
        if ($this->custom_select !== FALSE) {
            $this->db->select('geoarea_id');
            $this->db->select($this->custom_select);
        }

        if ($this->geoarea_id !== FALSE)
            $this->db->where('erp_geoareas.geoarea_id', $this->geoarea_id);

        if ($this->erp_city_id !== FALSE)
            $this->db->where('erp_geoareas.erp_city_id', $this->erp_city_id);

        if ($this->name_ar !== FALSE)
            $this->db->where('erp_geoareas.name_ar', $this->name_ar);

        if ($this->name_la !== FALSE)
            $this->db->where('erp_geoareas.name_la', $this->name_la);

        if ($this->distance_from !== FALSE)
            $this->db->where('erp_geoareas.distance_from', $this->distance_from);

        if ($this->distance_to !== FALSE)
            $this->db->where('erp_geoareas.distance_to', $this->distance_to);


        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('erp_geoareas');
        if ($rows_no)
            return $query->num_rows();

        return $query->result();
    }

}

/* End of file erp_geoareas_model.php */
/* Location: ./application/models/erp_geoareas_model.php */