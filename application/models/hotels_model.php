<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Hotels_model extends CI_Model {

    public $erp_hotel_id = FALSE;
    public $name_ar = FALSE;
    public $name_la = FALSE;
    public $address_ar = FALSE;
    public $address_la = FALSE;
    public $distance = FALSE;
    public $phone = FALSE;
    public $email = FALSE;
    public $fax = FALSE;
    public $floors_number = FALSE;
    public $rooms_number = FALSE;
    public $beds_number = FALSE;
    public $googel_map_langtitude = FALSE;
    public $googel_map_latitude = FALSE;
    public $occupancy_average = FALSE;
    public $remark = FALSE;
    public $erp_city_id = FALSE;
    public $erp_geoarea_id = FALSE;
    public $erp_street_id = FALSE;
    public $erp_hotellevel_id = FALSE;
    public $erp_hotel_sub_levels_id = FALSE;
    public $bau_hotel_id = FALSE;
    public $gama_hotel_id = FALSE;
    public $twat_hotel_id = FALSE;
    public $wtu_hotel_id = FALSE;
    public $rooms_count_for_disabled = FALSE;
    public $website = FALSE;
    public $flats_count = FALSE;
    public $lifts_count = FALSE;
    public $suites_count = FALSE;
    public $hotel_capacity = FALSE;
    public $hotel_creation_date = FALSE;
    public $custom_select = FALSE;
    public $limit = FALSE;
    public $offset = FALSE;
    public $order_by = FALSE;
    public $join = FALSE;

    function __construct() {
        parent::__construct();
    }

    function get($rows_no = FALSE) {

        if ($this->custom_select !== FALSE) {
            $this->db->select('erp_hotel_id');
            $this->db->select($this->custom_select);
        }

        if ($this->erp_hotel_id !== FALSE)
            $this->db->where('erp_hotels.erp_hotel_id', $this->erp_hotel_id);

        if ($this->name_ar !== FALSE)
            $this->db->like('erp_hotels.name_ar', $this->name_ar, 'both');


        if ($this->name_la !== FALSE)
            $this->db->like('erp_hotels.name_la', $this->name_la, 'both');

        if ($this->address_ar !== FALSE)
            $this->db->where('erp_hotels.address_ar', $this->address_ar);

        if ($this->address_la !== FALSE)
            $this->db->where('erp_hotels.address_la', $this->address_la);

        if ($this->distance !== FALSE)
            $this->db->where('erp_hotels.distance', $this->distance);

        if ($this->phone !== FALSE)
            $this->db->where('erp_hotels.phone', $this->phone);

        if ($this->email !== FALSE)
            $this->db->where('erp_hotels.email', $this->email);

        if ($this->fax !== FALSE)
            $this->db->where('erp_hotels.fax', $this->fax);

        if ($this->floors_number !== FALSE)
            $this->db->where('erp_hotels.floors_number', $this->floors_number);

        if ($this->rooms_number !== FALSE)
            $this->db->where('erp_hotels.rooms_number', $this->rooms_number);

        if ($this->beds_number !== FALSE)
            $this->db->where('erp_hotels.beds_number', $this->beds_number);

        if ($this->googel_map_langtitude !== FALSE)
            $this->db->where('erp_hotels.googel_map_langtitude', $this->googel_map_langtitude);

        if ($this->googel_map_latitude !== FALSE)
            $this->db->where('erp_hotels.googel_map_latitude', $this->googel_map_latitude);

        if ($this->occupancy_average !== FALSE)
            $this->db->where('erp_hotels.occupancy_average', $this->occupancy_average);

        if ($this->remark !== FALSE)
            $this->db->where('erp_hotels.remark', $this->remark);

        if ($this->erp_city_id !== FALSE)
            $this->db->where('erp_hotels.erp_city_id', $this->erp_city_id);

        if ($this->erp_geoarea_id !== FALSE)
            $this->db->where('erp_hotels.erp_geoarea_id', $this->erp_geoarea_id);

        if ($this->erp_street_id !== FALSE)
            $this->db->where('erp_hotels.erp_street_id', $this->erp_street_id);

        if ($this->erp_hotellevel_id !== FALSE)
            $this->db->where('erp_hotels.erp_hotellevel_id', $this->erp_hotellevel_id);

        if ($this->erp_hotel_sub_levels_id !== FALSE)
            $this->db->where('erp_hotels.erp_hotel_sub_levels_id', $this->erp_hotel_sub_levels_id);
        if ($this->bau_hotel_id !== FALSE)
            $this->db->where('erp_hotels.bau_hotel_id', $this->bau_hotel_id);

        if ($this->gama_hotel_id !== FALSE)
            $this->db->where('erp_hotels.gama_hotel_id', $this->gama_hotel_id);

        if ($this->twat_hotel_id !== FALSE)
            $this->db->where('erp_hotels.twat_hotel_id', $this->twat_hotel_id);

        if ($this->wtu_hotel_id !== FALSE)
            $this->db->where('erp_hotels.wtu_hotel_id', $this->wtu_hotel_id);

        if ($this->rooms_count_for_disabled !== FALSE)
            $this->db->where('erp_hotels.rooms_count_for_disabled', $this->rooms_count_for_disabled);

        if ($this->website !== FALSE)
            $this->db->where('erp_hotels.website', $this->website);

        if ($this->flats_count !== FALSE)
            $this->db->where('erp_hotels.flats_count', $this->flats_count);

        if ($this->lifts_count !== FALSE)
            if ($this->suites_count !== FALSE)
                $this->db->where('erp_hotels.suites_count', $this->suites_count);

        if ($this->hotel_capacity !== FALSE)
            $this->db->where('erp_hotels.hotel_capacity', $this->hotel_capacity);

        if ($this->hotel_creation_date !== FALSE)
            $this->db->where('erp_hotels.hotel_creation_date', $this->hotel_creation_date);


        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        if ($this->join !== FALSE) {
            $this->db->select('erp_hotels.*,erp_hotels.' . name() . ' as hotel_name,
                 erp_cities.erp_city_id as city_id,erp_countries.erp_country_id as country_id, 
                erp_cities.' . name() . ' as city_name,erp_countries.' . name() . ' as  country_name,
                erp_hotellevels.' . name() . ' as level_name,erp_hotel_sub_levels.' . name() . ' 
                as sublevel_name
              ');
            $this->db->join('erp_cities', 'erp_hotels.erp_city_id = erp_cities.erp_city_id', 'left');
            $this->db->join('erp_countries', 'erp_cities.erp_country_id = erp_countries.erp_country_id', 'left');
            $this->db->join('erp_hotellevels', 'erp_hotels.erp_hotellevel_id = erp_hotellevels.erp_hotellevel_id', 'left');
            $this->db->join('erp_hotel_sub_levels', 'erp_hotels.erp_hotel_sub_levels_id = erp_hotel_sub_levels.erp_hotel_sub_levels_id', 'left');
        }
        $query = $this->db->get('erp_hotels');
        if ($rows_no)
            return $query->num_rows();

        if ($this->erp_hotel_id)
            return $query->row();
        else
            return $query->result();
    }

    function save() {
        if ($this->erp_hotel_id !== FALSE)
            $this->db->set('erp_hotels.erp_hotel_id', $this->erp_hotel_id);

        if ($this->name_ar !== FALSE)
            $this->db->set('erp_hotels.name_ar', $this->name_ar);

        if ($this->name_la !== FALSE)
            $this->db->set('erp_hotels.name_la', $this->name_la);

        if ($this->address_ar !== FALSE)
            $this->db->set('erp_hotels.address_ar', $this->address_ar);

        if ($this->address_la !== FALSE)
            $this->db->set('erp_hotels.address_la', $this->address_la);

        if ($this->distance !== FALSE)
            $this->db->set('erp_hotels.distance', $this->distance);

        if ($this->phone !== FALSE)
            $this->db->set('erp_hotels.phone', $this->phone);

        if ($this->email !== FALSE)
            $this->db->set('erp_hotels.email', $this->email);

        if ($this->fax !== FALSE)
            $this->db->set('erp_hotels.fax', $this->fax);

        if ($this->floors_number !== FALSE)
            $this->db->set('erp_hotels.floors_number', $this->floors_number);

        if ($this->rooms_number !== FALSE)
            $this->db->set('erp_hotels.rooms_number', $this->rooms_number);

        if ($this->beds_number !== FALSE)
            $this->db->set('erp_hotels.beds_number', $this->beds_number);

        if ($this->googel_map_langtitude !== FALSE)
            $this->db->set('erp_hotels.googel_map_langtitude', $this->googel_map_langtitude);

        if ($this->googel_map_latitude !== FALSE)
            $this->db->set('erp_hotels.googel_map_latitude', $this->googel_map_latitude);

        if ($this->occupancy_average !== FALSE)
            $this->db->set('erp_hotels.occupancy_average', $this->occupancy_average);

        if ($this->remark !== FALSE)
            $this->db->set('erp_hotels.remark', $this->remark);

        if ($this->erp_city_id !== FALSE)
            $this->db->set('erp_hotels.erp_city_id', $this->erp_city_id);

        if ($this->erp_geoarea_id !== FALSE)
            $this->db->set('erp_hotels.erp_geoarea_id', $this->erp_geoarea_id);

        if ($this->erp_street_id !== FALSE)
            $this->db->set('erp_hotels.erp_street_id', $this->erp_street_id);



        if ($this->erp_hotellevel_id !== FALSE)
            $this->db->set('erp_hotels.erp_hotellevel_id', $this->erp_hotellevel_id);

        if ($this->erp_hotel_sub_levels_id !== FALSE)
            $this->db->set('erp_hotels.erp_hotel_sub_levels_id', $this->erp_hotel_sub_levels_id);

        if ($this->bau_hotel_id !== FALSE)
            $this->db->set('erp_hotels.bau_hotel_id', $this->bau_hotel_id);

        if ($this->gama_hotel_id !== FALSE)
            $this->db->set('erp_hotels.gama_hotel_id', $this->gama_hotel_id);

        if ($this->twat_hotel_id !== FALSE)
            $this->db->set('erp_hotels.twat_hotel_id', $this->twat_hotel_id);

        if ($this->wtu_hotel_id !== FALSE)
            $this->db->set('erp_hotels.wtu_hotel_id', $this->wtu_hotel_id);

        if ($this->rooms_count_for_disabled !== FALSE)
            $this->db->set('erp_hotels.rooms_count_for_disabled', $this->rooms_count_for_disabled);

        if ($this->website !== FALSE)
            $this->db->set('erp_hotels.website', $this->website);

        if ($this->flats_count !== FALSE)
            $this->db->set('erp_hotels.flats_count', $this->flats_count);

        if ($this->lifts_count !== FALSE)
            $this->db->set('erp_hotels.lifts_count', $this->lifts_count);


        if ($this->suites_count !== FALSE)
            $this->db->set('erp_hotels.suites_count', $this->suites_count);


        if ($this->hotel_capacity !== FALSE)
            $this->db->set('erp_hotels.hotel_capacity', $this->hotel_capacity);

        if ($this->hotel_creation_date !== FALSE)
            $this->db->set('erp_hotels.hotel_creation_date', $this->hotel_creation_date);



        if ($this->erp_hotel_id) {
            $this->db->where('erp_hotels.erp_hotel_id', $this->erp_hotel_id)->update('erp_hotels');
        } else {
            $this->db->insert('erp_hotels');
            return $this->db->insert_id();
        }
    }

    function delete() {
        if ($this->erp_hotel_id !== FALSE)
            $this->db->where('erp_hotels.erp_hotel_id', $this->erp_hotel_id);

        if ($this->name_ar !== FALSE)
            $this->db->where('erp_hotels.name_ar', $this->name_ar);

        if ($this->name_la !== FALSE)
            $this->db->where('erp_hotels.name_la', $this->name_la);

        if ($this->address_ar !== FALSE)
            $this->db->where('erp_hotels.address_ar', $this->address_ar);

        if ($this->address_la !== FALSE)
            $this->db->where('erp_hotels.address_la', $this->address_la);

        if ($this->distance !== FALSE)
            $this->db->where('erp_hotels.distance', $this->distance);

        if ($this->phone !== FALSE)
            $this->db->where('erp_hotels.phone', $this->phone);

        if ($this->email !== FALSE)
            $this->db->where('erp_hotels.email', $this->email);

        if ($this->fax !== FALSE)
            $this->db->where('erp_hotels.fax', $this->fax);

        if ($this->floors_number !== FALSE)
            $this->db->where('erp_hotels.floors_number', $this->floors_number);

        if ($this->rooms_number !== FALSE)
            $this->db->where('erp_hotels.rooms_number', $this->rooms_number);

        if ($this->beds_number !== FALSE)
            $this->db->where('erp_hotels.beds_number', $this->beds_number);

        if ($this->suites_count !== FALSE)
            $this->db->where('erp_hotels.suites_count', $this->suites_count);


        if ($this->googel_map_langtitude !== FALSE)
            $this->db->where('erp_hotels.googel_map_langtitude', $this->googel_map_langtitude);

        if ($this->googel_map_latitude !== FALSE)
            $this->db->where('erp_hotels.googel_map_latitude', $this->googel_map_latitude);

        if ($this->occupancy_average !== FALSE)
            $this->db->where('erp_hotels.occupancy_average', $this->occupancy_average);

        if ($this->remark !== FALSE)
            $this->db->where('erp_hotels.remark', $this->remark);

        if ($this->erp_city_id !== FALSE)
            $this->db->where('erp_hotels.erp_city_id', $this->erp_city_id);

        if ($this->erp_geoarea_id !== FALSE)
            $this->db->where('erp_hotels.erp_geoarea_id', $this->erp_geoarea_id);

        if ($this->erp_street_id !== FALSE)
            $this->db->where('erp_hotels.erp_street_id', $this->erp_street_id);


        if ($this->erp_hotellevel_id !== FALSE)
            $this->db->where('erp_hotels.erp_hotellevel_id', $this->erp_hotellevel_id);

        if ($this->erp_hotel_sub_levels_id !== FALSE)
            $this->db->where('erp_hotels.erp_hotel_sub_levels_id', $this->erp_hotel_sub_levels_id);

        if ($this->erp_country_id !== FALSE)
            $this->db->where('erp_hotels.erp_country_id', $this->erp_country_id);

        if ($this->bau_hotel_id !== FALSE)
            $this->db->where('erp_hotels.bau_hotel_id', $this->bau_hotel_id);

        if ($this->gama_hotel_id !== FALSE)
            $this->db->where('erp_hotels.gama_hotel_id', $this->gama_hotel_id);

        if ($this->twat_hotel_id !== FALSE)
            $this->db->where('erp_hotels.twat_hotel_id', $this->twat_hotel_id);

        if ($this->wtu_hotel_id !== FALSE)
            $this->db->where('erp_hotels.wtu_hotel_id', $this->wtu_hotel_id);

        if ($this->rooms_count_for_disabled !== FALSE)
            $this->db->where('erp_hotels.rooms_count_for_disabled', $this->rooms_count_for_disabled);

        if ($this->website !== FALSE)
            $this->db->where('erp_hotels.website', $this->website);

        if ($this->flats_count !== FALSE)
            $this->db->where('erp_hotels.flats_count', $this->flats_count);

        if ($this->lifts_count !== FALSE)
            $this->db->where('erp_hotels.lifts_count', $this->lifts_count);

        if ($this->hotel_capacity !== FALSE)
            $this->db->where('erp_hotels.hotel_capacity', $this->hotel_capacity);

        if ($this->hotel_creation_date !== FALSE)
            $this->db->where('erp_hotels.hotel_creation_date', $this->hotel_creation_date);



        $this->db->delete('erp_hotels');
        return $this->db->affected_rows();
    }

    function search($rows_no = FALSE) {
        if ($this->custom_select !== FALSE) {
            $this->db->select('erp_hotel_id');
            $this->db->select($this->custom_select);
        }

        if ($this->erp_hotel_id !== FALSE)
            $this->db->where('erp_hotels.erp_hotel_id', $this->erp_hotel_id);

        if ($this->name_ar !== FALSE)
            $this->db->where('erp_hotels.name_ar', $this->name_ar);

        if ($this->name_la !== FALSE)
            $this->db->where('erp_hotels.name_la', $this->name_la);

        if ($this->address_ar !== FALSE)
            $this->db->where('erp_hotels.address_ar', $this->address_ar);

        if ($this->address_la !== FALSE)
            $this->db->where('erp_hotels.address_la', $this->address_la);

        if ($this->distance !== FALSE)
            $this->db->where('erp_hotels.distance', $this->distance);

        if ($this->phone !== FALSE)
            $this->db->where('erp_hotels.phone', $this->phone);

        if ($this->email !== FALSE)
            $this->db->where('erp_hotels.email', $this->email);

        if ($this->fax !== FALSE)
            $this->db->where('erp_hotels.fax', $this->fax);

        if ($this->floors_number !== FALSE)
            $this->db->where('erp_hotels.floors_number', $this->floors_number);

        if ($this->rooms_number !== FALSE)
            $this->db->where('erp_hotels.rooms_number', $this->rooms_number);

        if ($this->beds_number !== FALSE)
            $this->db->where('erp_hotels.beds_number', $this->beds_number);

        if ($this->googel_map_langtitude !== FALSE)
            $this->db->where('erp_hotels.googel_map_langtitude', $this->googel_map_langtitude);

        if ($this->googel_map_latitude !== FALSE)
            $this->db->where('erp_hotels.googel_map_latitude', $this->googel_map_latitude);

        if ($this->occupancy_average !== FALSE)
            $this->db->where('erp_hotels.occupancy_average', $this->occupancy_average);

        if ($this->remark !== FALSE)
            $this->db->where('erp_hotels.remark', $this->remark);

        if ($this->erp_city_id !== FALSE)
            $this->db->where('erp_hotels.erp_city_id', $this->erp_city_id);

        if ($this->erp_geoarea_id !== FALSE)
            $this->db->where('erp_hotels.erp_geoarea_id', $this->erp_geoarea_id);


        if ($this->erp_street_id !== FALSE)
            $this->db->where('erp_hotels.erp_street_id', $this->erp_street_id);


        if ($this->erp_hotellevel_id !== FALSE)
            $this->db->where('erp_hotels.erp_hotellevel_id', $this->erp_hotellevel_id);

        if ($this->erp_hotel_sub_levels_id !== FALSE)
            $this->db->where('erp_hotels.erp_hotel_sub_levels_id', $this->erp_hotel_sub_levels_id);

        if ($this->erp_country_id !== FALSE)
            $this->db->where('erp_hotels.erp_country_id', $this->erp_country_id);

        if ($this->bau_hotel_id !== FALSE)
            $this->db->where('erp_hotels.bau_hotel_id', $this->bau_hotel_id);

        if ($this->gama_hotel_id !== FALSE)
            $this->db->where('erp_hotels.gama_hotel_id', $this->gama_hotel_id);

        if ($this->twat_hotel_id !== FALSE)
            $this->db->where('erp_hotels.twat_hotel_id', $this->twat_hotel_id);

        if ($this->wtu_hotel_id !== FALSE)
            $this->db->where('erp_hotels.wtu_hotel_id', $this->wtu_hotel_id);

        if ($this->rooms_count_for_disabled !== FALSE)
            $this->db->where('erp_hotels.rooms_count_for_disabled', $this->rooms_count_for_disabled);

        if ($this->website !== FALSE)
            $this->db->where('erp_hotels.website', $this->website);

        if ($this->flats_count !== FALSE)
            $this->db->where('erp_hotels.flats_count', $this->flats_count);

        if ($this->lifts_count !== FALSE)
            $this->db->where('erp_hotels.lifts_count', $this->lifts_count);

        if ($this->suites_count !== FALSE)
            $this->db->where('erp_hotels.suites_count', $this->suites_count);

        if ($this->hotel_capacity !== FALSE)
            $this->db->where('erp_hotels.hotel_capacity', $this->hotel_capacity);

        if ($this->hotel_creation_date !== FALSE)
            $this->db->where('erp_hotels.hotel_creation_date', $this->hotel_creation_date);


        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('erp_hotels');
        if ($rows_no)
            return $query->num_rows();

        return $query->result();
    }

    function get_by_city($city_id, $contract_id) {
        $this->db->select('*');
        $this->db->from('erp_hotels');
        $this->db->join('safa_uo_contracts_hotels', 'safa_uo_contracts_hotels.erp_hotel_id = erp_hotels.erp_hotel_id');
        $this->db->where('erp_hotels.erp_city_id', $city_id);
        $this->db->where('safa_uo_contracts_hotels.safa_uo_contract_id', $contract_id);
        $query = $this->db->get();
        return $query->result();
    }

    function get_by_city_id($city_id) {
        $this->db->select('*');
        $this->db->from('erp_hotels');
        $this->db->where('erp_hotels.erp_city_id', $city_id);
        $query = $this->db->get();
        return $query->result();
    }

    function get_by_country($country_id, $contract_id = 0) {
        $this->db->distinct();
        $this->db->select('erp_hotels.erp_hotel_id, erp_hotels.name_ar, erp_hotels.name_la');
        $this->db->from('erp_hotels');
        $this->db->join('safa_uo_contracts_hotels', 'safa_uo_contracts_hotels.erp_hotel_id = erp_hotels.erp_hotel_id');
        $this->db->join('erp_cities', 'erp_cities.erp_city_id = erp_hotels.erp_city_id');
        $this->db->join('erp_countries', 'erp_countries.erp_country_id = erp_cities.erp_country_id');
        $this->db->where('erp_countries.erp_country_id', $country_id);
        //$this->db->where('safa_uo_contracts_hotels.safa_uo_contract_id', $contract_id);
        $query = $this->db->get();

        //echo $this->db->last_query(); exit;

        return $query->result();
    }

    function check_delete_ability($id) {
        $this->db->select('count(safa_ea_package_hotels.erp_hotel_id) as ea_package,count(safa_uo_package_hotels.erp_hotel_id) as uo_package,
                count(safa_uo_contracts_hotels.erp_hotel_id) as contracts,count(safa_trip_hotels.erp_hotel_id) as trips,
                safa_internalsegments.erp_start_hotel_id,safa_internalsegments.erp_end_hotel_id');
        $this->db->from('erp_hotels');
        $this->db->join('safa_ea_package_hotels', 'erp_hotels.erp_hotel_id = safa_ea_package_hotels.erp_hotel_id', 'left');
        $this->db->join('safa_uo_package_hotels', 'erp_hotels.erp_hotel_id = safa_uo_package_hotels.erp_hotel_id', 'left');
        $this->db->join('safa_uo_contracts_hotels', 'erp_hotels.erp_hotel_id = safa_uo_contracts_hotels.erp_hotel_id', 'left');
        $this->db->join('safa_internalsegments', 'erp_hotels.erp_hotel_id = safa_internalsegments.erp_start_hotel_id', 'left');
        $this->db->join('safa_trip_hotels', 'erp_hotels.erp_hotel_id = safa_trip_hotels.erp_hotel_id', 'left');
        $this->db->group_by('erp_hotels.erp_hotel_id');
        $this->db->where('erp_hotels.erp_hotel_id', $id);
        $query = $this->db->get();
        $flag = 0; // todo after check the databaseschema
//        print_r($query->result());
        foreach ($query->result() as $row => $table) {
            foreach ($table as $col => $value) {
                if ($value > 0) {
                    $flag = 1;
                    break;
                } else {
                    continue;
                }
            }
        }
        return $flag;
    }

    function get_cities($erp_country_id) {
        $this->db->select('erp_cities.erp_city_id,erp_cities.name_ar,erp_cities.name_la');
        $this->db->where('erp_cities.erp_country_id', $erp_country_id);
        return $this->db->get('erp_cities')->result();
    }

    function get_countery_id($erp_city_id) {
        $this->db->select('erp_cities.erp_country_id');
        $this->db->where('erp_cities.erp_city_id', $erp_city_id);
        return $this->db->get('erp_cities')->row();
    }

    function get_city_id($erp_hotel_id) {
        $this->db->select('erp_hotels.erp_city_id');
        $this->db->where('erp_hotels.erp_hotel_id', $erp_hotel_id);
        return $this->db->get('erp_hotels')->row();
    }

    function get_countery_ksa() {
        $this->db->select('erp_countries.erp_country_id');
        $this->db->where('erp_countries.erp_country_id', 966);
        return $this->db->get('erp_countries')->row();
    }

}

/* End of file hotels_model.php */
