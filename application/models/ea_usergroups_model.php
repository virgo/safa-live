<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Ea_usergroups_model extends CI_Model {

    public $safa_ea_usergroup_id = FALSE;
    public $name_ar = FALSE;
    public $name_la = FALSE;
    public $custom_select = FALSE;
    public $limit = FALSE;
    public $offset = FALSE;
    public $order_by = FALSE;

    function __construct() {
        parent::__construct();
    }

    function get($rows_no = FALSE) {

        if ($this->custom_select !== FALSE) {
            $this->db->select('safa_ea_usergroup_id');
            $this->db->select($this->custom_select);
        }

        if ($this->safa_ea_usergroup_id !== FALSE)
            $this->db->where('safa_ea_usergroups.safa_ea_usergroup_id', $this->safa_ea_usergroup_id);

        if ($this->name_ar !== FALSE)
            $this->db->where('safa_ea_usergroups.name_ar', $this->name_ar);

        if ($this->name_la !== FALSE)
            $this->db->where('safa_ea_usergroups.name_la', $this->name_la);


        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('safa_ea_usergroups');
        if ($rows_no)
            return $query->num_rows();

        if ($this->safa_ea_usergroup_id)
            return $query->row();
        else
            return $query->result();
    }

    function save() {
        if ($this->safa_ea_usergroup_id !== FALSE)
            $this->db->set('safa_ea_usergroups.safa_ea_usergroup_id', $this->safa_ea_usergroup_id);

        if ($this->name_ar !== FALSE)
            $this->db->set('safa_ea_usergroups.name_ar', $this->name_ar);

        if ($this->name_la !== FALSE)
            $this->db->set('safa_ea_usergroups.name_la', $this->name_la);



        if ($this->safa_ea_usergroup_id) {
            $this->db->where('safa_ea_usergroups.safa_ea_usergroup_id', $this->safa_ea_usergroup_id)->update('safa_ea_usergroups');
        } else {
            $this->db->insert('safa_ea_usergroups');
            return $this->db->insert_id();
        }
    }

    function delete() {
        if ($this->safa_ea_usergroup_id !== FALSE)
            $this->db->where('safa_ea_usergroups.safa_ea_usergroup_id', $this->safa_ea_usergroup_id);

        if ($this->name_ar !== FALSE)
            $this->db->where('safa_ea_usergroups.name_ar', $this->name_ar);

        if ($this->name_la !== FALSE)
            $this->db->where('safa_ea_usergroups.name_la', $this->name_la);



        $this->db->delete('safa_ea_usergroups');
        return $this->db->affected_rows();
    }

    function search($rows_no = FALSE) {
        if ($this->custom_select !== FALSE) {
            $this->db->select('safa_ea_usergroup_id');
            $this->db->select($this->custom_select);
        }

        if ($this->safa_ea_usergroup_id !== FALSE)
            $this->db->where('safa_ea_usergroups.safa_ea_usergroup_id', $this->safa_ea_usergroup_id);

        if ($this->name_ar !== FALSE)
            $this->db->where('safa_ea_usergroups.name_ar', $this->name_ar);

        if ($this->name_la !== FALSE)
            $this->db->where('safa_ea_usergroups.name_la', $this->name_la);


        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('safa_ea_usergroups');
        if ($rows_no)
            return $query->num_rows();

        return $query->result();
    }

}

/* End of file ea_usergroups_model.php */
/* Location: ./application/models/ea_usergroups_model.php */