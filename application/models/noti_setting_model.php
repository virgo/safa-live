<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class noti_setting_model extends CI_Model {

    public $tbl_name = "safa_notifications_sys_params";
    public $notifications_sys_params_id = FALSE;
    public $send_email = FALSE;
    public $email = FALSE;
    public $send_sms = FALSE;
    public $phone_no = FALSE;
    public $note_me = FALSE;
    public $company_id = FALSE;
    public $company_type = FALSE;
    public $uos = array();
    public $eas = array();
    public $itos = array();
    public $system_id = array('admin' => 1, 'uo' => 2, 'ea' => 3, 'ito' => 4);

    function __construct() {

        parent::__construct();
    }

    function init() {

        $info = array("company_id" => $this->company_id, "company_type" => $this->company_type);

        $rows = $this->db->get_where($this->tbl_name, $info)->result();

        if (!count($rows)) {

            $this->db->insert($this->tbl_name, $info);

            $this->notifications_sys_params_id = $this->db->insert_id();
        } else {


            $this->notifications_sys_params_id = $rows[0]->notifications_sys_params_id;

            $this->send_email = $rows[0]->send_email;

            $this->email = $rows[0]->email;

            $this->send_sms = $rows[0]->send_sms;

            $this->phone_no = $rows[0]->phone_no;

            $this->note_me = $rows[0]->note_me;
        }


        //init uos, eas, itos
        $uos = $this->db->where('notifications_sys_params_id', $this->notifications_sys_params_id)->
                        where('sender_type', $this->system_id['uo'])->
                        get('safa_notifications_sys_params_detail')->result();

        if (count($uos)) {

            foreach ($uos as $uo) :
                $this->uos[] = $uo->sender_id;
            endforeach;
        }

        //array_shift($this->uos);

        $eas = $this->db->where('notifications_sys_params_id', $this->notifications_sys_params_id)->
                        where('sender_type', $this->system_id['ea'])->
                        get('safa_notifications_sys_params_detail')->result();
        if (count($eas)) {

            foreach ($eas as $ea) :
                $this->eas[] = $ea->sender_id;
            endforeach;
        }
        //array_shift($this->eas);


        $itos = $this->db->where('notifications_sys_params_id', $this->notifications_sys_params_id)->
                        where('sender_type', $this->system_id['ito'])->
                        get('safa_notifications_sys_params_detail')->result();
        if (count($itos)) {

            foreach ($itos as $ito) :
                $this->itos[] = $ito->sender_id;
            endforeach;
        }
        //array_shift($this->itos);        
    }

    function save() {

        if ($this->send_email !== false) {
            $this->db->set('send_email', $this->send_email);
        }

        if ($this->email !== false) {
            $this->db->set('email', $this->email);
        }

        if ($this->send_sms !== false) {
            $this->db->set('send_sms', $this->send_sms);
        }

        if ($this->phone_no !== false) {
            $this->db->set('phone_no', $this->phone_no);
        }

        if ($this->note_me !== false) {
            $this->db->set('note_me', $this->note_me);
        }

        $this->db->where('notifications_sys_params_id', $this->notifications_sys_params_id)->
                update($this->tbl_name);

        //update entries of uos, eas, ito

        $this->db->where('notifications_sys_params_id', $this->notifications_sys_params_id)->
                delete('safa_notifications_sys_params_detail');


        if (count($this->uos)) {
            $arr = $this->uos;
            for ($k = 0; $k < count($arr); $k++) {

                $noti = array("sender_id" => $arr[$k],
                    "sender_type" => $this->system_id['uo'],
                    "notifications_sys_params_id" => $this->notifications_sys_params_id);

                $this->db->insert('safa_notifications_sys_params_detail', $noti);
            }
        }



        //insert ea to sent notify
        if (count($this->eas)) {
            $arr = $this->eas;

            for ($k = 0; $k < count($arr); $k++) {

                $noti = array("sender_id" => $arr[$k],
                    "sender_type" => $this->system_id['ea'],
                    "notifications_sys_params_id" => $this->notifications_sys_params_id);

                $this->db->insert('safa_notifications_sys_params_detail', $noti);
            }
        }


        //insert itos to sent notify
        if (count($this->itos)) {
            $arr = $this->uos;

            for ($k = 0; $k < count($arr); $k++) {

                $noti = array("sender_id" => $arr[$k],
                    "sender_type" => $this->system_id['ito'],
                    "notifications_sys_params_id" => $this->notifications_sys_params_id);

                $this->db->insert('safa_notifications_sys_params_detail', $noti);
            }
        }
    }

}

/* End of file countries_model.php */
/* Location: ./application/models/countries_model.php */