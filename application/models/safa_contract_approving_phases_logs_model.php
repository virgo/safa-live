<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Safa_contract_approving_phases_logs_model extends CI_Model {

    public $tbl_name = "safa_contract_approving_phases_logs";
    public $safa_contract_approving_phases_logs_id = FALSE;
    public $safa_uo_contracts_id = FALSE;
    public $safa_contract_phases_id_from = FALSE;
    public $safa_contract_phases_id_to = FALSE;
    public $the_date = FALSE;
    public $custom_select = FALSE;
    public $limit = FALSE;
    public $offset = FALSE;
    public $order_by = FALSE;

    function __construct() {
        parent::__construct();
    }

    function save() {
        if ($this->safa_contract_approving_phases_logs_id !== FALSE) {
            $this->db->set('safa_contract_approving_phases_logs.safa_contract_approving_phases_logs_id', $this->safa_contract_approving_phases_logs_id);
        }
        if ($this->safa_uo_contracts_id !== FALSE) {
            $this->db->set('safa_contract_approving_phases_logs.safa_uo_contracts_id', $this->safa_uo_contracts_id);
        }
        if ($this->safa_contract_phases_id_from !== FALSE) {
            $this->db->set('safa_contract_approving_phases_logs.safa_contract_phases_id_from', $this->safa_contract_phases_id_from);
        }
        if ($this->safa_contract_phases_id_to !== FALSE) {
            $this->db->set('safa_contract_approving_phases_logs.safa_contract_phases_id_to', $this->safa_contract_phases_id_to);
        }
        if ($this->the_date !== FALSE) {
            $this->db->set('safa_contract_approving_phases_logs.the_date', $this->the_date);
        }

        if ($this->safa_contract_approving_phases_logs_id) {
            $this->db->where('safa_contract_approving_phases_logs.safa_contract_approving_phases_logs_id', $this->safa_contract_approving_phases_logs_id)->update('safa_contract_approving_phases_logs');
        } else {
            //By Gouda.
            $this->db->db_debug = FALSE;
            if ($this->db->insert('safa_contract_approving_phases_logs')) {
                return $this->db->insert_id();
            } else {
                return 0;
            }
        }
    }

    function get($rows_no = FALSE) {

        if ($this->custom_select !== FALSE) {
            $this->db->select('safa_contract_approving_phases_logs_id');
            $this->db->select($this->custom_select);
        }

        $this->db->from('safa_contract_approving_phases_logs');

        if ($this->safa_contract_approving_phases_logs_id !== FALSE) {
            $this->db->where('safa_contract_approving_phases_logs.safa_contract_approving_phases_logs_id', $this->safa_contract_approving_phases_logs_id);
        }
        if ($this->safa_uo_contracts_id !== FALSE) {
            $this->db->where('safa_contract_approving_phases_logs.safa_uo_contracts_id', $this->safa_uo_contracts_id);
        }
        if ($this->safa_contract_phases_id_from !== FALSE) {
            $this->db->where('safa_contract_approving_phases_logs.safa_contract_phases_id_from', $this->safa_contract_phases_id_from);
        }
        if ($this->safa_contract_phases_id_to !== FALSE) {
            $this->db->where('safa_contract_approving_phases_logs.safa_contract_phases_id_to', $this->safa_contract_phases_id_to);
        }
        if ($this->the_date !== FALSE) {
            $this->db->where('safa_contract_approving_phases_logs.the_date', $this->the_date);
        }

        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get();
        $query_text = $this->db->last_query();

        if ($rows_no) {
            return $query->num_rows();
        }
        if ($this->safa_contract_approving_phases_logs_id) {
            return $query->row();
        } else {
            return $query->result();
        }
    }

    /*
      function delete()
      {
      if ($this->safa_contract_approving_phases_logs_id !== FALSE) {
      $this->db->where('safa_contract_approving_phases_logs.safa_contract_approving_phases_logs_id', $this->safa_contract_approving_phases_logs_id);
      }
      if ($this->safa_uo_contracts_id !== FALSE) {
      $this->db->where('safa_contract_approving_phases_logs.safa_uo_contracts_id', $this->safa_uo_contracts_id);
      }
      if ($this->safa_contract_phases_id !== FALSE) {
      $this->db->where('safa_contract_approving_phases_logs.safa_contract_phases_id', $this->safa_contract_phases_id);
      }
      if ($this->safa_contract_phases_documents_id !== FALSE) {
      $this->db->where('safa_contract_approving_phases_logs.safa_contract_phases_documents_id', $this->safa_contract_phases_documents_id);
      }

      $this->db->delete('safa_contract_approving_phases_logs');
      return $this->db->affected_rows();
      }
     */
}

/* End of file safa_contract_approving_phases_logs_model.php */
/* Location: ./application/models/safa_contract_approving_phases_logs_model.php */