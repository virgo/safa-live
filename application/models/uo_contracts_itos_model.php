<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Uo_contracts_itos_model extends CI_Model {

    public $safa_uo_contract_ito_id = FALSE;
    public $safa_uo_contract_id = FALSE;
    public $safa_ito_id = FALSE;
    public $custom_select = FALSE;
    public $limit = FALSE;
    public $offset = FALSE;
    public $order_by = FALSE;

    function __construct() {
        parent::__construct();
    }

    function get($rows_no = FALSE) {

        if ($this->custom_select !== FALSE) {
            $this->db->select('safa_uo_contract_ito_id');
            $this->db->select($this->custom_select);
        }

        if ($this->safa_uo_contract_ito_id !== FALSE)
            $this->db->where('safa_uo_contracts_itos.safa_uo_contract_ito_id', $this->safa_uo_contract_ito_id);

        if ($this->safa_uo_contract_id !== FALSE)
            $this->db->where('safa_uo_contracts_itos.safa_uo_contract_id', $this->safa_uo_contract_id);

        if ($this->safa_ito_id !== FALSE)
            $this->db->where('safa_uo_contracts_itos.safa_ito_id', $this->safa_ito_id);

        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('safa_uo_contracts_itos');
        if ($rows_no)
            return $query->num_rows();

        if ($this->erp_admin_id)
            return $query->row();
        else
            return $query->result();
    }

    function save() {


        if ($this->safa_uo_contract_ito_id !== FALSE)
            $this->db->set('safa_uo_contracts_itos.safa_uo_contract_ito_id', $this->safa_uo_contract_ito_id);

        if ($this->safa_uo_contract_id !== FALSE)
            $this->db->set('safa_uo_contracts_itos.safa_uo_contract_id', $this->safa_uo_contract_id);

        if ($this->safa_ito_id !== FALSE)
            $this->db->set('safa_uo_contracts_itos.safa_ito_id', $this->safa_ito_id);


        if ($this->safa_uo_contract_ito_id) {
            $this->db->where('safa_uo_contracts_itos.safa_uo_contract_ito_id', $this->safa_uo_contract_ito_id)->update('safa_uo_contracts_itos');
        } else {
            $this->db->insert('safa_uo_contracts_itos');
            return $this->db->insert_id();
        }
    }

    function delete() {



        if ($this->safa_uo_contract_ito_id !== FALSE)
            $this->db->where('safa_uo_contracts_itos.safa_uo_contract_ito_id', $this->safa_uo_contract_ito_id);

        if ($this->safa_uo_contract_id !== FALSE)
            $this->db->where('safa_uo_contracts_itos.safa_uo_contract_id', $this->safa_uo_contract_id);

        if ($this->safa_ito_id !== FALSE)
            $this->db->where('safa_uo_contracts_itos.safa_ito_id', $this->safa_ito_id);


        $this->db->delete('safa_uo_contracts_itos');
        return $this->db->affected_rows();
    }

    function search_ito($rows_no = FALSE) {
        if ($this->custom_select !== FALSE) {
            $this->db->select('safa_uo_contract_ito_id');
            $this->db->select($this->custom_select);
        }

        if ($this->safa_ito_id !== FALSE)
            $this->db->where('safa_uo_contracts_itos.safa_ito_id', $this->safa_ito_id);



        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('safa_uo_contracts_itos');
        if ($rows_no)
            return $query->num_rows();

        return $query->result();
    }

}

/* End of file safa_uo_contracts_ito_model.php */
/* Location: ./application/models/safa_uo_contracts_ito_model.php */