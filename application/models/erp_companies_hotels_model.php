<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Erp_companies_hotels_model extends CI_Model {

    public $erp_companies_hotels_id = FALSE;
    public $erp_company_type_id = FALSE;
    public $company_id = FALSE;
    public $erp_hotel_id = FALSE;
    public $custom_select = FALSE;
    public $limit = FALSE;
    public $offset = FALSE;
    public $order_by = FALSE;

    function __construct() {
        parent::__construct();
    }

    function get($rows_no = FALSE) {

        if ($this->custom_select !== FALSE) {
            $this->db->select('erp_companies_hotels_id');
            $this->db->select($this->custom_select);
        }

        if ($this->erp_companies_hotels_id !== FALSE)
            $this->db->where('erp_companies_hotels.erp_companies_hotels_id', $this->erp_companies_hotels_id);

        if ($this->erp_company_type_id !== FALSE)
            $this->db->where('erp_companies_hotels.erp_company_type_id', $this->erp_company_type_id);

        if ($this->company_id !== FALSE)
            $this->db->where('erp_companies_hotels.company_id', $this->company_id);

        if ($this->erp_hotel_id !== FALSE)
            $this->db->where('erp_companies_hotels.erp_hotel_id', $this->erp_hotel_id);


        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('erp_companies_hotels');
        if ($rows_no)
            return $query->num_rows();

        if ($this->erp_companies_hotels_id)
            return $query->row();
        else
            return $query->result();
    }

    function save() {
        if ($this->erp_companies_hotels_id !== FALSE)
            $this->db->set('erp_companies_hotels.erp_companies_hotels_id', $this->erp_companies_hotels_id);

        if ($this->erp_company_type_id !== FALSE)
            $this->db->set('erp_companies_hotels.erp_company_type_id', $this->erp_company_type_id);

        if ($this->company_id !== FALSE)
            $this->db->set('erp_companies_hotels.company_id', $this->company_id);

        if ($this->erp_hotel_id !== FALSE)
            $this->db->set('erp_companies_hotels.erp_hotel_id', $this->erp_hotel_id);



        if ($this->erp_companies_hotels_id) {
            $this->db->where('erp_companies_hotels.erp_companies_hotels_id', $this->erp_companies_hotels_id)->update('erp_companies_hotels');
        } else {
            $this->db->insert('erp_companies_hotels');
            return $this->db->insert_id();
        }
    }

    function delete() {
        if ($this->erp_companies_hotels_id !== FALSE)
            $this->db->where('erp_companies_hotels.erp_companies_hotels_id', $this->erp_companies_hotels_id);

        if ($this->erp_company_type_id !== FALSE)
            $this->db->where('erp_companies_hotels.erp_company_type_id', $this->erp_company_type_id);

        if ($this->company_id !== FALSE)
            $this->db->where('erp_companies_hotels.company_id', $this->company_id);

        if ($this->erp_hotel_id !== FALSE)
            $this->db->where('erp_companies_hotels.erp_hotel_id', $this->erp_hotel_id);



        $this->db->delete('erp_companies_hotels');
        return $this->db->affected_rows();
    }

    function search($rows_no = FALSE) {
        if ($this->custom_select !== FALSE) {
            $this->db->select('erp_companies_hotels_id');
            $this->db->select($this->custom_select);
        }

        if ($this->erp_companies_hotels_id !== FALSE)
            $this->db->where('erp_companies_hotels.erp_companies_hotels_id', $this->erp_companies_hotels_id);

        if ($this->erp_company_type_id !== FALSE)
            $this->db->where('erp_companies_hotels.erp_company_type_id', $this->erp_company_type_id);

        if ($this->company_id !== FALSE)
            $this->db->where('erp_companies_hotels.company_id', $this->company_id);

        if ($this->erp_hotel_id !== FALSE)
            $this->db->where('erp_companies_hotels.erp_hotel_id', $this->erp_hotel_id);


        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('erp_companies_hotels');
        if ($rows_no)
            return $query->num_rows();

        return $query->result();
    }

    function check_build() {

        if ($this->company_id != FALSE)
            $this->db->where('erp_companies_hotels.company_id', $this->company_id);
        if ($this->erp_hotel_id)
            $this->db->where('erp_companies_hotels.erp_hotel_id', $this->erp_hotel_id);
        $number_row = $this->db->get('erp_companies_hotels')->row();
        if ($number_row != '') {
            return $number_row->erp_companies_hotels_id;
        }
    }

}

/* End of file erp_companies_hotels_model.php */
/* Location: ./application/models/erp_companies_hotels_model.php */