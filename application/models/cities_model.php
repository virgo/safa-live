<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Cities_model extends CI_Model {

    public $erp_city_id = FALSE;
    public $name_ar = FALSE;
    public $name_la = FALSE;
    public $erp_country_id = FALSE;
    public $custom_select = FALSE;
    public $limit = FALSE;
    public $offset = FALSE;
    public $order_by = FALSE;
    public $join = FALSE;

    function __construct() {
        parent::__construct();
    }

    function get($rows_no = FALSE) {

        if ($this->custom_select !== FALSE) {
            $this->db->select('erp_city_id');
            $this->db->select($this->custom_select);
        }

        if ($this->erp_city_id !== FALSE)
            $this->db->where('erp_cities.erp_city_id', $this->erp_city_id);

        if ($this->name_ar !== FALSE)
            $this->db->where('erp_cities.name_ar', $this->name_ar);

        if ($this->name_la !== FALSE)
            $this->db->where('erp_cities.name_la', $this->name_la);

        if ($this->erp_country_id !== FALSE)
            $this->db->where('erp_cities.erp_country_id', $this->erp_country_id);

        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        if ($this->join !== FALSE) {
            $this->db->select('erp_cities.*,erp_countries.erp_country_id as country_id,
            erp_countries.name_ar as country_name_ar,erp_countries.name_la as country_name_la');
            $this->db->join('erp_countries', 'erp_cities.erp_country_id = erp_countries.erp_country_id', 'left');
        }

        $query = $this->db->get('erp_cities');
        if ($rows_no)
            return $query->num_rows();

        if ($this->erp_city_id)
            return $query->row();
        else
            return $query->result();
    }

    function save() {
        if ($this->erp_city_id !== FALSE)
            $this->db->set('erp_cities.erp_city_id', $this->erp_city_id);

        if ($this->name_ar !== FALSE)
            $this->db->set('erp_cities.name_ar', $this->name_ar);

        if ($this->name_la !== FALSE)
            $this->db->set('erp_cities.name_la', $this->name_la);

        if ($this->erp_country_id !== FALSE)
            $this->db->set('erp_cities.erp_country_id', $this->erp_country_id);



        if ($this->erp_city_id) {
            $this->db->where('erp_cities.erp_city_id', $this->erp_city_id)->update('erp_cities');
        } else {
            $this->db->insert('erp_cities');
            return $this->db->insert_id();
        }
    }

    function delete() {
        if ($this->erp_city_id !== FALSE)
            $this->db->where('erp_cities.erp_city_id', $this->erp_city_id);

        if ($this->name_ar !== FALSE)
            $this->db->where('erp_cities.name_ar', $this->name_ar);

        if ($this->name_la !== FALSE)
            $this->db->where('erp_cities.name_la', $this->name_la);

        if ($this->erp_country_id !== FALSE)
            $this->db->where('erp_cities.erp_country_id', $this->erp_country_id);



        $this->db->delete('erp_cities');
        return $this->db->affected_rows();
    }

    function search($rows_no = FALSE) {
        if ($this->custom_select !== FALSE) {
            $this->db->select('erp_city_id');
            $this->db->select($this->custom_select);
        }

        if ($this->erp_city_id !== FALSE)
            $this->db->where('erp_cities.erp_city_id', $this->erp_city_id);

        if ($this->name_ar !== FALSE)
            $this->db->where('erp_cities.name_ar', $this->name_ar);

        if ($this->name_la !== FALSE)
            $this->db->where('erp_cities.name_la', $this->name_la);

        if ($this->erp_country_id !== FALSE)
            $this->db->where('erp_cities.erp_country_id', $this->erp_country_id);


        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('erp_cities');
        if ($rows_no)
            return $query->num_rows();

        return $query->result();
    }

    function check_delete_ability($id) {
        $this->db->select('count(safa_ea_branches.erp_city_id)as ea_branches,count(erp_hotels.erp_city_id) as hotels,
                count(erp_ports.erp_city_id) as ports,count(safa_tourismplaces.erp_city_id) as tourismplaces');
//                count(safa_uo_contracts.country_id)as contracts');
        $this->db->from('erp_cities');
        $this->db->join('safa_ea_branches', 'erp_cities.erp_city_id = safa_ea_branches.erp_city_id', 'left');
        $this->db->join('erp_hotels', 'erp_cities.erp_city_id = erp_hotels.erp_city_id', 'left');
        $this->db->join('erp_ports', 'erp_cities.erp_city_id = erp_ports.erp_city_id', 'left');
//        $this->db->join('safa_uo_contracts', 'erp_cities.erp_country_id = safa_uo_contracts.country_id', 'left');
        $this->db->join('safa_tourismplaces', 'erp_cities.erp_city_id = safa_tourismplaces.erp_city_id', 'left');

        $this->db->group_by('erp_cities.erp_city_id');
        $this->db->where('erp_cities.erp_city_id', $id);
        $query = $this->db->get();
        $flag = 0;

//        print_r($query->result());
        foreach ($query->result() as $row => $table) {
            foreach ($table as $col => $value) {
                if ($value > 0) {
                    $flag = 1;
                    break;
                } else {
                    continue;
                }
            }
        }
        return $flag;
    }

}

/* End of file cities_model.php */
/* Location: ./application/models/cities_model.php */