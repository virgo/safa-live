<?php

if (!defined('BASEPATH'))
exit('No direct script access allowed');

class Safa_trips_model extends CI_Model {

	public $safa_trip_id = FALSE;
	public $name = FALSE;
	public $date = FALSE;
	public $arrival_date = FALSE;
	public $safa_ea_id = FALSE;
	public $safa_tripstatus_id = FALSE;
	public $safa_tripstatus_id_not = FALSE;
	public $safa_trip_confirm_id = FALSE;
	public $erp_transportertype_id = FALSE;
	public $erp_country_id = FALSE;
	public $notes = FALSE;
	public $uo_refusing_reason = FALSE;
	public $safa_uo_id = FALSE;
	public $date_from = FALSE;
	public $date_to = FALSE;
	public $trip_internal_trip_join = FALSE;
	public $safa_ito_id = FALSE;
	public $safa_ea_supervisor_ids = FALSE;
	public $custom_select = FALSE;
	public $limit = FALSE;
	public $offset = FALSE;
	public $order_by = FALSE;

	function __construct() {
		parent::__construct();
	}

	function get($rows_no = FALSE) {
		$this->db->distinct();

		if ($this->custom_select !== FALSE) {
			$this->db->select('safa_trip_id');
			$this->db->select($this->custom_select);
		}

//By Gouda, For safa_uo_contracts.safa_uo_contract_id problem where session of ea

//		$select_str = 'safa_trips.*,erp_hotels_reservation_orders.order_status as reservestatus,erp_hotels_reservation_orders.erp_hotels_reservation_orders_id as reserveid,
//            ,(SELECT COUNT(*) FROM safa_trip_internaltrips WHERE safa_trip_internaltrips.safa_trip_id = safa_trips.safa_trip_id AND safa_trip_internaltrips.safa_uo_contract_id = safa_uo_contracts.safa_uo_contract_id AND (safa_trip_internaltrips.deleted IS NULL OR safa_trip_internaltrips.deleted=0)) AS internal_trips,
//            ,(SELECT safa_trip_internaltrip_id FROM safa_trip_internaltrips WHERE safa_trip_internaltrips.safa_trip_id = safa_trips.safa_trip_id  and (safa_trip_internaltrips.deleted IS NULL OR safa_trip_internaltrips.deleted=0) LIMIT 1)  AS trip_internaltrip_id,
//            safa_tripstatus.' . name() . ' as trip_status';

		$select_str = 'safa_trips.*,erp_hotels_reservation_orders.order_status as reservestatus,erp_hotels_reservation_orders.erp_hotels_reservation_orders_id as reserveid,
            ,(SELECT COUNT(*) FROM safa_trip_internaltrips WHERE safa_trip_internaltrips.safa_trip_id = safa_trips.safa_trip_id   AND (safa_trip_internaltrips.deleted IS NULL OR safa_trip_internaltrips.deleted=0)) AS internal_trips,
            ,(SELECT safa_trip_internaltrip_id FROM safa_trip_internaltrips WHERE safa_trip_internaltrips.safa_trip_id = safa_trips.safa_trip_id  and (safa_trip_internaltrips.deleted IS NULL OR safa_trip_internaltrips.deleted=0) LIMIT 1)  AS trip_internaltrip_id,
            safa_tripstatus.' . name() . ' as trip_status';
		
		
		if ($this->safa_ito_id !== FALSE) {
			$select_str = $select_str . ', safa_itos.' . name() . ' as ito_name';
		}

		if ($this->safa_uo_id !== FALSE) {
			$select_str = $select_str . ', safa_uo_contracts.' . name() . ' as safa_uo_contract_name';
		}
		
		$select_str = $select_str . ',
            (select count(safa_group_passports.safa_group_passport_id) from safa_group_passports      where  safa_group_passports.safa_trip_id=safa_trips.safa_trip_id  and (SELECT DATE_FORMAT(FROM_DAYS(TO_DAYS(NOW())-TO_DAYS(safa_group_passports.date_of_birth)), "%Y")+0 AS age FROM safa_group_passports as inner_safa_group_passports where inner_safa_group_passports.safa_group_passport_id =safa_group_passports.safa_group_passport_id) > 12 group by (safa_group_passports.safa_trip_id)) as travellers_adult_count,
            (select count(safa_group_passports.safa_group_passport_id) from safa_group_passports      where  safa_group_passports.safa_trip_id=safa_trips.safa_trip_id  and (SELECT DATE_FORMAT(FROM_DAYS(TO_DAYS(NOW())-TO_DAYS(safa_group_passports.date_of_birth)), "%Y")+0 AS age FROM safa_group_passports as inner_safa_group_passports where inner_safa_group_passports.safa_group_passport_id =safa_group_passports.safa_group_passport_id) > 2 and (SELECT DATE_FORMAT(FROM_DAYS(TO_DAYS(NOW())-TO_DAYS(safa_group_passports.date_of_birth)), "%Y")+0 AS age FROM safa_group_passports as inner_safa_group_passports where inner_safa_group_passports.safa_group_passport_id =safa_group_passports.safa_group_passport_id) < 12   group by (safa_group_passports.safa_trip_id)) as travellers_child_count,
            (select count(safa_group_passports.safa_group_passport_id) from safa_group_passports      where  safa_group_passports.safa_trip_id=safa_trips.safa_trip_id  and (SELECT DATE_FORMAT(FROM_DAYS(TO_DAYS(NOW())-TO_DAYS(safa_group_passports.date_of_birth)), "%Y")+0 AS age FROM safa_group_passports as inner_safa_group_passports where inner_safa_group_passports.safa_group_passport_id =safa_group_passports.safa_group_passport_id) < 2  group by (safa_group_passports.safa_trip_id)) as travellers_infant_count,
            
            (select count(safa_group_passports.safa_group_passport_id) from safa_group_passports      where  safa_group_passports.safa_trip_id=safa_trips.safa_trip_id  group by (safa_group_passports.safa_trip_id)) as individuals_count, 
            (select count(safa_group_passports.safa_group_passport_id) from safa_group_passports      where  safa_group_passports.safa_trip_id=safa_trips.safa_trip_id and safa_group_passports.dpn_serial_no=0 group by (safa_group_passports.safa_trip_id)) as passports_count,
            (select count(safa_group_passports.safa_group_passport_id) from safa_group_passports      where  safa_group_passports.safa_trip_id=safa_trips.safa_trip_id and safa_group_passports.mofa != "" group by (safa_group_passports.safa_trip_id)) as mofa_count, 
            (select count(safa_group_passports.safa_group_passport_id) from safa_group_passports      where  safa_group_passports.safa_trip_id=safa_trips.safa_trip_id and safa_group_passports.enumber != "" group by (safa_group_passports.safa_trip_id)) as visa_order_count, 
            (select count(safa_group_passports.safa_group_passport_id) from safa_group_passports      where  safa_group_passports.safa_trip_id=safa_trips.safa_trip_id and safa_group_passports.visa_number != "" group by (safa_group_passports.safa_trip_id)) as visa_number_count 
            
            ';

		$this->db->select($select_str);

		if ($this->trip_internal_trip_join)
		$this->db->select('safa_trip_internaltrips.*');

		if ($this->safa_trip_id !== FALSE)
		$this->db->where('safa_trips.safa_trip_id', $this->safa_trip_id);

		if ($this->safa_ea_id !== FALSE)
		$this->db->where('safa_trips.safa_ea_id', $this->safa_ea_id);

		if ($this->name !== FALSE)
		$this->db->where('safa_trips.name', $this->name);

		if ($this->date !== FALSE)
		$this->db->where('safa_trips.date', $this->date);

		if ($this->arrival_date !== FALSE)
		$this->db->where('safa_trips.arrival_date', $this->arrival_date);

		if ($this->date_from)
		$this->db->where('safa_trips.date >=', $this->date_from);

		if ($this->date_to)
		$this->db->where('safa_trips.date <=', $this->date_to);

		if ($this->erp_country_id)
		$this->db->where('safa_trips.erp_country_id', $this->erp_country_id);

		if ($this->safa_uo_id !== FALSE) {
			$this->db->where('safa_packages.erp_company_type_id', 2);
			$this->db->where('safa_packages.erp_company_id', $this->safa_uo_id);
		}

		if ($this->safa_ito_id !== FALSE) {
			$this->db->where('safa_trip_internaltrips.safa_ito_id', $this->safa_ito_id);
		}

		if ($this->safa_tripstatus_id !== FALSE)
		$this->db->where('safa_trips.safa_tripstatus_id', $this->safa_tripstatus_id);

		if ($this->safa_tripstatus_id_not !== FALSE) {
			$this->db->where('safa_trips.safa_tripstatus_id !=', $this->safa_tripstatus_id_not);
		}

		if ($this->safa_trip_confirm_id !== FALSE)
		$this->db->where('safa_trips.safa_trip_confirm_id', $this->safa_trip_confirm_id);

		if ($this->safa_ea_supervisor_ids !== FALSE) {
			$this->db->where_in('safa_trip_supervisors.safa_ea_supervisor_id', $this->safa_ea_supervisor_ids);
		}

		if ($this->order_by && is_array($this->order_by))
		$this->db->order_by($this->order_by['0'], $this->order_by['1']);

		if (!$rows_no && $this->limit)
		$this->db->limit($this->limit, $this->offset);

		$this->db->from('safa_trips');
		$this->db->join('safa_group_passports', 'safa_group_passports.safa_trip_id = safa_trips.safa_trip_id', 'left');
		$this->db->join('erp_hotels_reservation_orders', 'erp_hotels_reservation_orders.safa_trip_id = safa_trips.safa_trip_id', 'left');

		if ($this->safa_uo_id !== FALSE) {
			//$this->db->join('safa_umrahgroups_passports', 'safa_umrahgroups_passports.safa_group_passport_id = safa_group_passports.safa_group_passport_id');
			//$this->db->join('safa_umrahgroups', 'safa_umrahgroups.safa_umrahgroup_id = safa_umrahgroups_passports.safa_umrahgroup_id');
			//$this->db->join('safa_uo_contracts', 'safa_uo_contracts.safa_uo_contract_id = safa_umrahgroups.safa_uo_contract_id');

			$this->db->join('safa_group_passport_accommodation', 'safa_group_passport_accommodation.safa_group_passport_id = safa_group_passports.safa_group_passport_id', 'left');
			$this->db->join('safa_package_periods', 'safa_package_periods.safa_package_periods_id = safa_group_passport_accommodation.safa_package_periods_id', 'left');
			$this->db->join('safa_packages', 'safa_packages.safa_package_id = safa_package_periods.safa_package_id', 'left');

			$this->db->join('safa_uo_contracts_eas', 'safa_uo_contracts_eas.safa_ea_id = safa_trips.safa_ea_id');
			$this->db->join('safa_uo_contracts', 'safa_uo_contracts.safa_uo_contract_id = safa_uo_contracts_eas.safa_uo_contract_id');
						
		}

		if ($this->safa_ito_id !== FALSE) {
			$this->db->join('safa_trip_internaltrips', 'safa_trip_internaltrips.safa_trip_id = safa_trips.safa_trip_id', 'left');
			$this->db->join('safa_itos', 'safa_itos.safa_ito_id = safa_trip_internaltrips.safa_ito_id', 'left');
		}

		$this->db->join('safa_tripstatus', ' safa_tripstatus.safa_tripstatus_id = safa_trips.safa_tripstatus_id', 'left');
		$this->db->join('erp_flight_availabilities', 'erp_flight_availabilities.safa_trip_id = safa_trips.safa_trip_id', 'left');
		$this->db->join('erp_flight_availabilities_detail', 'erp_flight_availabilities.erp_flight_availability_id = erp_flight_availabilities_detail.erp_flight_availability_id', 'left');
		//$this->db->join(FSDB . '.fs_flights', FSDB . '.fs_flights.airline = erp_flight_availabilities_detail.erp_airline_id', 'left');
		//$this->db->join(FSDB . '.fs_status', FSDB . '.fs_status.fs_status_id = ' . FSDB . '.fs_flights.fs_status_id', 'left');


		$this->db->join('safa_trip_supervisors', 'safa_trip_supervisors.safa_trip_id = safa_trips.safa_trip_id', 'left');

		if ($this->trip_internal_trip_join)
		$this->db->join('safa_trip_internaltrips', 'safa_trip_internaltrips.safa_trip_id = safa_trips.safa_trip_id', 'left');

		$query = $this->db->get();
		//echo $this->db->last_query(); exit;

		if ($rows_no)
		return $query->num_rows();

		if ($this->safa_trip_id)
		return $query->row();
		else
		return $query->result();
	}

	function get_safa_packages($rows_no = FALSE) {
		$this->db->distinct();

		if ($this->custom_select !== FALSE) {
			$this->db->select('safa_trip_id');
			$this->db->select($this->custom_select);
		}


		$select_str = 'safa_trips.*,erp_hotels_reservation_orders.order_status as reservestatus,erp_hotels_reservation_orders.erp_hotels_reservation_orders_id as reserveid,
            ,(SELECT COUNT(*) FROM safa_trip_internaltrips WHERE safa_trip_internaltrips.safa_trip_id = safa_trips.safa_trip_id and (safa_trip_internaltrips.deleted IS NULL OR safa_trip_internaltrips.deleted=0)) AS internal_trips,
            ,(SELECT safa_trip_internaltrip_id FROM safa_trip_internaltrips WHERE safa_trip_internaltrips.safa_trip_id = safa_trips.safa_trip_id AND (safa_trip_internaltrips.deleted IS NULL OR safa_trip_internaltrips.deleted=0) LIMIT 1)  AS trip_internaltrip_id,
            safa_tripstatus.' . name() . ' as trip_status, safa_packages.' . name() . ' as safa_package_name, safa_packages.safa_package_id, erp_package_periods.' . name() . ' as erp_package_period_name';

		if ($this->safa_ito_id !== FALSE) {
			$select_str = $select_str . ', safa_itos.' . name() . ' as ito_name';
		}

		$select_str = $select_str . ',
            
            (select count(safa_group_passports.safa_group_passport_id) from safa_group_passports      where  safa_group_passports.safa_trip_id=safa_trips.safa_trip_id  and (SELECT DATE_FORMAT(FROM_DAYS(TO_DAYS(NOW())-TO_DAYS(safa_group_passports.date_of_birth)), "%Y")+0 AS age FROM safa_group_passports as inner_safa_group_passports where inner_safa_group_passports.safa_group_passport_id =safa_group_passports.safa_group_passport_id) > 12 group by (safa_group_passports.safa_trip_id)) as travellers_adult_count,
            (select count(safa_group_passports.safa_group_passport_id) from safa_group_passports      where  safa_group_passports.safa_trip_id=safa_trips.safa_trip_id  and (SELECT DATE_FORMAT(FROM_DAYS(TO_DAYS(NOW())-TO_DAYS(safa_group_passports.date_of_birth)), "%Y")+0 AS age FROM safa_group_passports as inner_safa_group_passports where inner_safa_group_passports.safa_group_passport_id =safa_group_passports.safa_group_passport_id) > 2 and (SELECT DATE_FORMAT(FROM_DAYS(TO_DAYS(NOW())-TO_DAYS(safa_group_passports.date_of_birth)), "%Y")+0 AS age FROM safa_group_passports as inner_safa_group_passports where inner_safa_group_passports.safa_group_passport_id =safa_group_passports.safa_group_passport_id) < 12   group by (safa_group_passports.safa_trip_id)) as travellers_child_count,
            (select count(safa_group_passports.safa_group_passport_id) from safa_group_passports      where  safa_group_passports.safa_trip_id=safa_trips.safa_trip_id  and (SELECT DATE_FORMAT(FROM_DAYS(TO_DAYS(NOW())-TO_DAYS(safa_group_passports.date_of_birth)), "%Y")+0 AS age FROM safa_group_passports as inner_safa_group_passports where inner_safa_group_passports.safa_group_passport_id =safa_group_passports.safa_group_passport_id) < 2  group by (safa_group_passports.safa_trip_id)) as travellers_infant_count,
            
            (select count(safa_group_passports.safa_group_passport_id) from safa_group_passports      where  safa_group_passports.safa_trip_id=safa_trips.safa_trip_id  group by (safa_group_passports.safa_trip_id)) as individuals_count, 
            (select count(safa_group_passports.safa_group_passport_id) from safa_group_passports      where  safa_group_passports.safa_trip_id=safa_trips.safa_trip_id and safa_group_passports.dpn_serial_no=0 group by (safa_group_passports.safa_trip_id)) as passports_count,
            (select count(safa_group_passports.safa_group_passport_id) from safa_group_passports      where  safa_group_passports.safa_trip_id=safa_trips.safa_trip_id and safa_group_passports.mofa != "" group by (safa_group_passports.safa_trip_id)) as mofa_count, 
            (select count(safa_group_passports.safa_group_passport_id) from safa_group_passports      where  safa_group_passports.safa_trip_id=safa_trips.safa_trip_id and safa_group_passports.enumber != "" group by (safa_group_passports.safa_trip_id)) as visa_order_count, 
            (select count(safa_group_passports.safa_group_passport_id) from safa_group_passports      where  safa_group_passports.safa_trip_id=safa_trips.safa_trip_id and safa_group_passports.visa_number != "" group by (safa_group_passports.safa_trip_id)) as visa_number_count 
            ';

		$this->db->select($select_str);

		if ($this->trip_internal_trip_join)
		$this->db->select('safa_trip_internaltrips.*');

		if ($this->safa_trip_id !== FALSE)
		$this->db->where('safa_trips.safa_trip_id', $this->safa_trip_id);

		if ($this->safa_ea_id !== FALSE)
		$this->db->where('safa_trips.safa_ea_id', $this->safa_ea_id);

		if ($this->name !== FALSE)
		$this->db->where('safa_trips.name', $this->name);

		if ($this->date !== FALSE)
		$this->db->where('safa_trips.date', $this->date);

		if ($this->arrival_date !== FALSE)
		$this->db->where('safa_trips.arrival_date', $this->arrival_date);

		if ($this->date_from)
		$this->db->where('safa_trips.date >=', $this->date_from);

		if ($this->date_to)
		$this->db->where('safa_trips.date <=', $this->date_to);

		if ($this->erp_country_id)
		$this->db->where('safa_trips.erp_country_id', $this->erp_country_id);

		if ($this->safa_uo_id !== FALSE) {
			$this->db->where('safa_packages.erp_company_type_id', 2);
			$this->db->where('safa_packages.erp_company_id', $this->safa_uo_id);
		}

		if ($this->safa_ito_id !== FALSE) {
			$this->db->where('safa_trip_internaltrips.safa_ito_id', $this->safa_ito_id);
		}

		if ($this->safa_tripstatus_id !== FALSE)
		$this->db->where('safa_trips.safa_tripstatus_id', $this->safa_tripstatus_id);

		if ($this->safa_trip_confirm_id !== FALSE)
		$this->db->where('safa_trips.safa_trip_confirm_id', $this->safa_trip_confirm_id);

		if ($this->order_by && is_array($this->order_by))
		$this->db->order_by($this->order_by['0'], $this->order_by['1']);

		if (!$rows_no && $this->limit)
		$this->db->limit($this->limit, $this->offset);

		$this->db->from('safa_trips');
		$this->db->join('safa_group_passports', 'safa_group_passports.safa_trip_id = safa_trips.safa_trip_id', 'left');
		$this->db->join('erp_hotels_reservation_orders', 'erp_hotels_reservation_orders.safa_trip_id = safa_trips.safa_trip_id', 'left');

		//Comment If condition By Gouda, to get packages name.
		//if ($this->safa_uo_id !== FALSE) {

		$this->db->join('safa_group_passport_accommodation', 'safa_group_passport_accommodation.safa_group_passport_id = safa_group_passports.safa_group_passport_id', 'left');
		$this->db->join('safa_package_periods', 'safa_package_periods.safa_package_periods_id = safa_group_passport_accommodation.safa_package_periods_id', 'left');
		$this->db->join('safa_packages', 'safa_packages.safa_package_id = safa_package_periods.safa_package_id', 'left');

		$this->db->join('erp_package_periods', 'erp_package_periods.erp_package_period_id = safa_package_periods.erp_package_period_id', 'left');

		//}

		if ($this->safa_ito_id !== FALSE) {
			$this->db->join('safa_trip_internaltrips', 'safa_trip_internaltrips.safa_trip_id = safa_trips.safa_trip_id', 'left');
			$this->db->join('safa_itos', 'safa_itos.safa_ito_id = safa_trip_internaltrips.safa_ito_id', 'left');
		}

		$this->db->join('safa_tripstatus', ' safa_tripstatus.safa_tripstatus_id = safa_trips.safa_tripstatus_id', 'left');
		$this->db->join('erp_flight_availabilities', 'erp_flight_availabilities.safa_trip_id = safa_trips.safa_trip_id', 'left');
		$this->db->join('erp_flight_availabilities_detail', 'erp_flight_availabilities.erp_flight_availability_id = erp_flight_availabilities_detail.erp_flight_availability_id', 'left');
		//$this->db->join(FSDB . '.fs_flights', FSDB . '.fs_flights.airline = erp_flight_availabilities_detail.erp_airline_id', 'left');
		//$this->db->join(FSDB . '.fs_status', FSDB . '.fs_status.fs_status_id = ' . FSDB . '.fs_flights.fs_status_id', 'left');

		if ($this->trip_internal_trip_join)
		$this->db->join('safa_trip_internaltrips', 'safa_trip_internaltrips.safa_trip_id = safa_trips.safa_trip_id', 'left');

		$query = $this->db->get();
		//echo $this->db->last_query(); exit;

		if ($rows_no)
		return $query->num_rows();

		//		if ($this->safa_trip_id)
		//		return $query->row();
		//		else
		return $query->result();
	}

	function save() {
		if ($this->safa_trip_id !== FALSE)
		$this->db->set('safa_trips.safa_trip_id', $this->safa_trip_id);

		if ($this->safa_ea_id !== FALSE)
		$this->db->set('safa_trips.safa_ea_id', $this->safa_ea_id);

		if ($this->name !== FALSE)
		$this->db->set('safa_trips.name', $this->name);

		if ($this->date !== FALSE)
		$this->db->set('safa_trips.date', $this->date);

		if ($this->arrival_date !== FALSE)
		$this->db->set('safa_trips.arrival_date', $this->arrival_date);

		if ($this->safa_tripstatus_id !== FALSE)
		$this->db->set('safa_trips.safa_tripstatus_id', $this->safa_tripstatus_id);

		if ($this->safa_trip_confirm_id !== FALSE)
		$this->db->set('safa_trips.safa_trip_confirm_id', $this->safa_trip_confirm_id);

		if ($this->erp_transportertype_id !== FALSE)
		$this->db->set('safa_trips.erp_transportertype_id', $this->erp_transportertype_id);

		if ($this->erp_country_id !== FALSE)
		$this->db->set('safa_trips.erp_country_id', $this->erp_country_id);

		if ($this->notes !== FALSE)
		$this->db->set('safa_trips.notes', $this->notes);

		if ($this->uo_refusing_reason !== FALSE)
		$this->db->set('safa_trips.uo_refusing_reason', $this->uo_refusing_reason);


		if ($this->safa_trip_id) {
			$this->db->where('safa_trips.safa_trip_id', $this->safa_trip_id)->update('safa_trips');
			return $this->safa_trip_id;
		} else {
			$this->db->insert('safa_trips');
			return $this->db->insert_id();
		}
	}

	function delete() {
		if ($this->safa_trip_id !== FALSE)
		$this->db->where('safa_trips.safa_trip_id', $this->safa_trip_id);

		if ($this->safa_ea_id !== FALSE)
		$this->db->where('safa_trips.safa_ea_id', $this->safa_ea_id);

		if ($this->name !== FALSE)
		$this->db->where('safa_trips.name', $this->name);

		if ($this->date !== FALSE)
		$this->db->where('safa_trips.date', $this->date);

		if ($this->arrival_date !== FALSE)
		$this->db->where('safa_trips.arrival_date', $this->arrival_date);

		if ($this->date_from)
		$this->db->where('safa_trips.date >=', $this->date_from);

		if ($this->date_to)
		$this->db->where('safa_trips.date <=', $this->date_to);

		if ($this->erp_country_id)
		$this->db->where('safa_trips.erp_country_id', $this->erp_country_id);

		if ($this->safa_tripstatus_id !== FALSE)
		$this->db->where('safa_trips.safa_tripstatus_id', $this->safa_tripstatus_id);

		if ($this->safa_trip_confirm_id !== FALSE)
		$this->db->where('safa_trips.safa_trip_confirm_id', $this->safa_trip_confirm_id);


		$this->db->delete('safa_trips');
		return $this->db->affected_rows();
	}

	function get_contries($rows_no = false) {
		$this->db->distinct();

		if ($this->custom_select !== FALSE) {
			$this->db->select('erp_country_id');
			$this->db->select($this->custom_select);
		}


		$this->db->select('erp_countries.erp_country_id, erp_countries.' . name() . ' as country_name ');


		if ($this->safa_trip_id !== FALSE)
		$this->db->where('safa_trips.safa_trip_id', $this->safa_trip_id);

		if ($this->safa_ea_id !== FALSE)
		$this->db->where('safa_trips.safa_ea_id', $this->safa_ea_id);

		if ($this->name !== FALSE)
		$this->db->where('safa_trips.name', $this->name);

		if ($this->date !== FALSE)
		$this->db->where('safa_trips.date', $this->date);

		if ($this->arrival_date !== FALSE)
		$this->db->where('safa_trips.arrival_date', $this->arrival_date);

		if ($this->date_from)
		$this->db->where('safa_trips.date >=', $this->date_from);

		if ($this->date_to)
		$this->db->where('safa_trips.date <=', $this->date_to);

		if ($this->erp_country_id)
		$this->db->where('safa_trips.erp_country_id', $this->erp_country_id);

		if ($this->safa_uo_id !== FALSE) {
			$this->db->where('safa_packages.erp_company_type_id', 2);
			$this->db->where('safa_packages.erp_company_id', $this->safa_uo_id);
		}

		if ($this->safa_tripstatus_id !== FALSE)
		$this->db->where('safa_trips.safa_tripstatus_id', $this->safa_tripstatus_id);

		if ($this->safa_trip_confirm_id !== FALSE)
		$this->db->where('safa_trips.safa_trip_confirm_id', $this->safa_trip_confirm_id);



		if ($this->order_by && is_array($this->order_by))
		$this->db->order_by($this->order_by['0'], $this->order_by['1']);

		if (!$rows_no && $this->limit)
		$this->db->limit($this->limit, $this->offset);

		$this->db->from('safa_trips');
		$this->db->join('safa_group_passports', 'safa_group_passports.safa_trip_id = safa_trips.safa_trip_id', 'left');
		$this->db->join('safa_group_passport_accommodation', 'safa_group_passport_accommodation.safa_group_passport_id = safa_group_passports.safa_group_passport_id', 'left');
		$this->db->join('safa_package_periods', 'safa_package_periods.safa_package_periods_id = safa_group_passport_accommodation.safa_package_periods_id', 'left');
		$this->db->join('safa_packages', 'safa_packages.safa_package_id = safa_package_periods.safa_package_id', 'left');

		$this->db->join('erp_countries', 'erp_countries.erp_country_id = safa_trips.erp_country_id', 'left');


		$query = $this->db->get();
		//echo $this->db->last_query(); exit;

		if ($rows_no)
		return $query->num_rows();

		if ($this->erp_country_id)
		return $query->row();
		else
		return $query->result();
	}

	function get_trip_hotels($trip_id = false) {
		$query = "
		SELECT safa_passport_accommodation_rooms.from_date,
		safa_passport_accommodation_rooms.to_date,
		safa_passport_accommodation_rooms.erp_hotel_id,
		safa_reservation_forms.safa_trip_id  
		FROM safa_passport_accommodation_rooms
		JOIN safa_reservation_forms ON safa_passport_accommodation_rooms.safa_reservation_form_id = safa_reservation_forms.safa_reservation_form_id
		JOIN safa_group_passport_accommodation ON safa_group_passport_accommodation.safa_passport_accommodation_room_id = safa_passport_accommodation_rooms.safa_passport_accommodation_room_id 
		JOIN erp_hotels ON erp_hotels.erp_hotel_id = safa_passport_accommodation_rooms.erp_hotel_id 
		JOIN erp_cities ON erp_cities.erp_city_id = erp_hotels.erp_city_id 
		WHERE safa_reservation_forms.safa_trip_id = $trip_id AND safa_passport_accommodation_rooms.erp_hotels_availability_room_detail_id IS NULL
		GROUP BY safa_passport_accommodation_rooms.erp_hotel_id ORDER BY to_date ASC";

		$rooms = $this->db->query($query);
		//echo $this->db->last_query();die();
		return $rooms->result();
	}

	function get_trip_hotels_for_internaltrip($trip_id = false, $safa_reservation_form_id = false) {
		$query = "
		SELECT safa_passport_accommodation_rooms.from_date,
		safa_passport_accommodation_rooms.to_date,
		safa_passport_accommodation_rooms.erp_hotel_id,
		safa_reservation_forms.safa_trip_id,
		erp_hotels.erp_city_id   
		FROM safa_passport_accommodation_rooms
		JOIN safa_reservation_forms ON safa_passport_accommodation_rooms.safa_reservation_form_id = safa_reservation_forms.safa_reservation_form_id
		JOIN safa_group_passport_accommodation ON safa_group_passport_accommodation.safa_passport_accommodation_room_id = safa_passport_accommodation_rooms.safa_passport_accommodation_room_id 
		JOIN erp_hotels ON erp_hotels.erp_hotel_id = safa_passport_accommodation_rooms.erp_hotel_id 
		JOIN erp_cities ON erp_cities.erp_city_id = erp_hotels.erp_city_id 
		WHERE safa_reservation_forms.safa_trip_id = $trip_id ";
		if($safa_reservation_form_id!==false) {
			$query = $query." AND safa_reservation_forms.safa_reservation_form_id = $safa_reservation_form_id";
		}
		//$query = $query." AND safa_passport_accommodation_rooms.safa_reservation_form_id = (select min(safa_reservation_forms.safa_reservation_form_id) from safa_reservation_forms where  safa_reservation_forms.safa_trip_id = $trip_id) ";
		$query = $query." GROUP BY safa_passport_accommodation_rooms.erp_hotel_id ORDER BY to_date ASC";

		$rooms = $this->db->query($query);
		//echo $this->db->last_query();die();
		return $rooms->result();
	}

	function get_hotels_rooms($trip_id = false) {
		$query = "
		SELECT safa_passport_accommodation_rooms.from_date AS entry_date,
		safa_passport_accommodation_rooms.to_date AS exit_date,
		safa_group_passport_accommodation.erp_meal_id AS erp_meals_id,
		safa_passport_accommodation_rooms.erp_hotelroomsize_id AS erp_hotelroomsizes_id,
		safa_passport_accommodation_rooms.erp_hotel_id AS erp_hotels_id, (select count(*) from safa_passport_accommodation_rooms as safa_passport_accommodation_rooms2 where safa_passport_accommodation_rooms.erp_hotel_id = safa_passport_accommodation_rooms2.erp_hotel_id and safa_passport_accommodation_rooms.erp_hotelroomsize_id = safa_passport_accommodation_rooms2.erp_hotelroomsize_id and safa_passport_accommodation_rooms.safa_reservation_form_id = safa_passport_accommodation_rooms2.safa_reservation_form_id) AS rooms_count,
		safa_reservation_forms.safa_trip_id, 
		erp_hotels.erp_city_id AS erp_cities_id, erp_hotels." . name() . " as erp_hotel_name, erp_cities." . name() . " as erp_city_name,
		erp_meals." . name() . "  as erp_meal_name, erp_hotelroomsizes." . name() . " as erp_hotelroomsize_name FROM
		safa_passport_accommodation_rooms
		JOIN safa_reservation_forms ON safa_passport_accommodation_rooms.safa_reservation_form_id = safa_reservation_forms.safa_reservation_form_id
		JOIN safa_group_passport_accommodation ON safa_group_passport_accommodation.safa_passport_accommodation_room_id = safa_passport_accommodation_rooms.safa_passport_accommodation_room_id 
		JOIN erp_hotels ON erp_hotels.erp_hotel_id = safa_passport_accommodation_rooms.erp_hotel_id 
		JOIN erp_cities ON erp_cities.erp_city_id = erp_hotels.erp_city_id 
		LEFT OUTER JOIN erp_meals ON erp_meals.erp_meal_id = safa_group_passport_accommodation.erp_meal_id 
		LEFT OUTER JOIN erp_hotelroomsizes ON erp_hotelroomsizes.erp_hotelroomsize_id = safa_passport_accommodation_rooms.erp_hotelroomsize_id 
		WHERE safa_reservation_forms.safa_trip_id = $trip_id 
		GROUP BY safa_passport_accommodation_rooms.erp_hotelroomsize_id,
		safa_passport_accommodation_rooms.erp_hotel_id";
		$rooms = $this->db->query($query);
		return $rooms->result();
	}

	function get_hotels_rooms_for_trip_details($trip_id = false) {
		$query = "
		SELECT safa_passport_accommodation_rooms.from_date AS entry_date,
		safa_passport_accommodation_rooms.to_date AS exit_date,
		safa_passport_accommodation_rooms.erp_hotelroomsize_id AS erp_hotelroomsizes_id,
		safa_passport_accommodation_rooms.erp_hotel_id AS erp_hotels_id, (select count(*) from safa_passport_accommodation_rooms as safa_passport_accommodation_rooms2 where safa_passport_accommodation_rooms.erp_hotel_id = safa_passport_accommodation_rooms2.erp_hotel_id and safa_passport_accommodation_rooms.erp_hotelroomsize_id = safa_passport_accommodation_rooms2.erp_hotelroomsize_id and safa_passport_accommodation_rooms.safa_reservation_form_id = safa_passport_accommodation_rooms2.safa_reservation_form_id) AS rooms_count,
		
		
		safa_reservation_forms.safa_trip_id, 
		erp_hotels.erp_city_id AS erp_cities_id, erp_hotels." . name() . " as erp_hotel_name, erp_cities." . name() . " as erp_city_name,
		erp_hotelroomsizes." . name() . " as erp_hotelroomsize_name, 
		
		(select count(safa_passport_accommodation_rooms_inner.erp_hotel_id) from safa_passport_accommodation_rooms as safa_passport_accommodation_rooms_inner 
		where safa_passport_accommodation_rooms.from_date = safa_passport_accommodation_rooms_inner.from_date 
		and safa_passport_accommodation_rooms.to_date = safa_passport_accommodation_rooms_inner.to_date 
		and  safa_passport_accommodation_rooms.erp_hotel_id = safa_passport_accommodation_rooms_inner.erp_hotel_id) as rows_span
		
		FROM
		safa_passport_accommodation_rooms
		JOIN safa_reservation_forms ON safa_passport_accommodation_rooms.safa_reservation_form_id = safa_reservation_forms.safa_reservation_form_id
		
		JOIN erp_hotels ON erp_hotels.erp_hotel_id = safa_passport_accommodation_rooms.erp_hotel_id 
		JOIN erp_cities ON erp_cities.erp_city_id = erp_hotels.erp_city_id 
		LEFT OUTER JOIN erp_hotelroomsizes ON erp_hotelroomsizes.erp_hotelroomsize_id = safa_passport_accommodation_rooms.erp_hotelroomsize_id 
		WHERE safa_reservation_forms.safa_trip_id = $trip_id 
		GROUP BY safa_reservation_forms.safa_reservation_form_id, safa_passport_accommodation_rooms.erp_hotelroomsize_id,
		safa_passport_accommodation_rooms.erp_hotel_id
		order by safa_passport_accommodation_rooms.erp_hotel_id
		";
		$rooms = $this->db->query($query);

		//echo $this->db->last_query(); exit;
		return $rooms->result();
	}

	function get_hotels_rooms_for_trip($trip_id = false) {
		$query = "
		SELECT safa_passport_accommodation_rooms.from_date AS entry_date,
		safa_passport_accommodation_rooms.to_date AS exit_date,
		safa_passport_accommodation_rooms.erp_hotelroomsize_id AS erp_hotelroomsizes_id,
		safa_passport_accommodation_rooms.erp_hotel_id AS erp_hotels_id, 
                (select count(*) from safa_passport_accommodation_rooms as safa_passport_accommodation_rooms2 where safa_passport_accommodation_rooms.erp_hotel_id = safa_passport_accommodation_rooms2.erp_hotel_id and safa_passport_accommodation_rooms.erp_hotelroomsize_id = safa_passport_accommodation_rooms2.erp_hotelroomsize_id and safa_passport_accommodation_rooms.safa_reservation_form_id = safa_passport_accommodation_rooms2.safa_reservation_form_id) AS rooms_count,
		safa_reservation_forms.safa_trip_id, 
		erp_hotels.erp_city_id AS erp_cities_id, 
                erp_hotels." . name() . " as erp_hotel_name, erp_cities." . name() . " as erp_city_name,
		erp_hotelroomsizes." . name() . " as erp_hotelroomsize_name FROM
		safa_passport_accommodation_rooms
		JOIN safa_reservation_forms ON safa_passport_accommodation_rooms.safa_reservation_form_id = safa_reservation_forms.safa_reservation_form_id
		JOIN erp_hotels ON erp_hotels.erp_hotel_id = safa_passport_accommodation_rooms.erp_hotel_id 
		JOIN erp_cities ON erp_cities.erp_city_id = erp_hotels.erp_city_id 
		LEFT OUTER JOIN erp_hotelroomsizes ON erp_hotelroomsizes.erp_hotelroomsize_id = safa_passport_accommodation_rooms.erp_hotelroomsize_id 
		WHERE safa_reservation_forms.safa_trip_id = $trip_id 
		GROUP BY safa_reservation_forms.safa_reservation_form_id, safa_passport_accommodation_rooms.erp_hotelroomsize_id,
		safa_passport_accommodation_rooms.erp_hotel_id
		order by safa_passport_accommodation_rooms.erp_hotel_id";
		$rooms = $this->db->query($query);

		//echo $this->db->last_query(); exit;
		return $rooms->result();
	}

	function trip_voucher($trip_id = false) {
		$query = "SELECT safa_passport_accommodation_rooms.from_date AS entry_date,
safa_passport_accommodation_rooms.to_date AS exit_date,
safa_passport_accommodation_rooms.erp_hotelroomsize_id AS erp_hotelroomsizes_id,
safa_passport_accommodation_rooms.erp_hotel_id AS erp_hotels_id,
(select count(*) from safa_passport_accommodation_rooms as safa_passport_accommodation_rooms2 where safa_passport_accommodation_rooms.erp_hotel_id = safa_passport_accommodation_rooms2.erp_hotel_id and safa_passport_accommodation_rooms.erp_hotelroomsize_id = safa_passport_accommodation_rooms2.erp_hotelroomsize_id and safa_passport_accommodation_rooms.safa_reservation_form_id = safa_passport_accommodation_rooms2.safa_reservation_form_id) AS rooms_count,
safa_reservation_forms.safa_trip_id,erp_hotels.erp_city_id AS erp_cities_id,
erp_hotels." . name() . " AS erp_hotel_name,erp_cities." . name() . " AS erp_city_name,
erp_hotelroomsizes." . name() . " AS erp_hotelroomsize_name,safa_package_periods_prices.price,
Count(safa_group_passport_accommodation.safa_group_passport_accommodation_id) AS people,
(safa_package_periods_prices.price * Count(safa_group_passport_accommodation.safa_group_passport_accommodation_id)) AS total,
safa_packages." . name() . " AS safa_packages_name,
erp_package_periods." . name() . " AS peroid_name,safa_package_periods.safa_package_periods_id
FROM safa_passport_accommodation_rooms
JOIN safa_reservation_forms ON safa_passport_accommodation_rooms.safa_reservation_form_id = safa_reservation_forms.safa_reservation_form_id
JOIN erp_hotels ON erp_hotels.erp_hotel_id = safa_passport_accommodation_rooms.erp_hotel_id
JOIN erp_cities ON erp_cities.erp_city_id = erp_hotels.erp_city_id
JOIN erp_hotelroomsizes ON erp_hotelroomsizes.erp_hotelroomsize_id = safa_passport_accommodation_rooms.erp_hotelroomsize_id
JOIN safa_package_periods ON safa_package_periods.safa_package_periods_id = safa_reservation_forms.safa_package_period_id
JOIN safa_package_periods_prices ON safa_package_periods_prices.safa_package_periods_id = safa_package_periods.safa_package_periods_id
JOIN safa_group_passport_accommodation ON safa_group_passport_accommodation.safa_package_periods_id = safa_package_periods.safa_package_periods_id AND safa_group_passport_accommodation.safa_passport_accommodation_room_id = safa_passport_accommodation_rooms.safa_passport_accommodation_room_id
JOIN safa_packages ON safa_packages.safa_package_id = safa_package_periods.safa_package_id
JOIN erp_package_periods ON safa_package_periods.erp_package_period_id = erp_package_periods.erp_package_period_id
WHERE safa_reservation_forms.safa_trip_id = $trip_id AND
safa_package_periods_prices.erp_hotelroomsize_id = safa_passport_accommodation_rooms.erp_hotelroomsize_id
GROUP BY
safa_reservation_forms.safa_reservation_form_id,safa_passport_accommodation_rooms.erp_hotelroomsize_id,
safa_passport_accommodation_rooms.erp_hotel_id,safa_passport_accommodation_rooms.from_date,
safa_passport_accommodation_rooms.to_date,safa_reservation_forms.safa_trip_id
ORDER BY 
erp_hotelroomsizes_id ,entry_date";

		$voucher = $this->db->query($query);
		return $voucher->result();
	}
        
	function get_trip_groups($safa_trip_id = FALSE) {
		$query = "SELECT safa_groups.`name` FROM
                safa_group_passports
                INNER JOIN safa_groups ON safa_group_passports.safa_group_id = safa_groups.safa_group_id
                WHERE safa_group_passports.safa_trip_id = $safa_trip_id
                GROUP BY safa_groups.safa_group_id";
		$groups = $this->db->query($query);
		return $groups->result();
	}

	function get_trip_ea($safa_trip_id = FALSE) {
		$uoid = session('uo_id');
		$query = "SELECT safa_eas.safa_ea_id, safa_uo_contracts.". name() ." as ea_name
FROM safa_reservation_forms
INNER JOIN safa_eas ON safa_reservation_forms.safa_ea_id = safa_eas.safa_ea_id 
INNER JOIN safa_uo_contracts_eas ON safa_uo_contracts_eas.safa_ea_id = safa_eas.safa_ea_id 
INNER JOIN safa_uo_contracts ON  safa_uo_contracts.safa_uo_contract_id = safa_uo_contracts_eas.safa_uo_contract_id
WHERE safa_reservation_forms.safa_trip_id = $safa_trip_id AND safa_uo_contracts.safa_uo_id = $uoid
GROUP BY safa_eas.safa_ea_id";
		$ea = $this->db->query($query);
		return $ea->row();
	}

	function get_itos_by_trip($id = 0) {
		$this->db->distinct();
		$select_str = ' safa_itos.' . name() . ' as ito_name';
		$this->db->select($select_str);
		$this->db->where('safa_trips.safa_trip_id', $id);
		$this->db->from('safa_trips');
		$this->db->join('safa_trip_internaltrips', 'safa_trip_internaltrips.safa_trip_id = safa_trips.safa_trip_id', 'left');
		$this->db->join('safa_itos', 'safa_itos.safa_ito_id = safa_trip_internaltrips.safa_ito_id', 'left');

		$query = $this->db->get();
		//echo $this->db->last_query(); exit;

		return $query->result();
	}

	function get_eas($id = 0) {
		$this->db->distinct();
		$select_str = 'safa_eas.safa_ea_id, safa_eas.' . name() . ' as ea_name, count(safa_trips.safa_trip_id) as safa_trips_count ';
		$this->db->select($select_str);

		if ($this->safa_uo_id !== FALSE) {
			$this->db->where('safa_packages.erp_company_type_id', 2);
			$this->db->where('safa_packages.erp_company_id', $this->safa_uo_id);
		}

		$this->db->from('safa_trips');
		$this->db->join('safa_group_passports', 'safa_group_passports.safa_trip_id = safa_trips.safa_trip_id', 'left');

		$this->db->join('safa_group_passport_accommodation', 'safa_group_passport_accommodation.safa_group_passport_id = safa_group_passports.safa_group_passport_id', 'left');
		$this->db->join('safa_package_periods', 'safa_package_periods.safa_package_periods_id = safa_group_passport_accommodation.safa_package_periods_id', 'left');
		$this->db->join('safa_packages', 'safa_packages.safa_package_id = safa_package_periods.safa_package_id', 'left');
		$this->db->join('safa_eas', 'safa_eas.safa_ea_id = safa_trips.safa_ea_id', 'left');

		$query = $this->db->get();
		//echo $this->db->last_query(); exit;

		return $query->result();
	}

	function get_total_mofa_count() {

		$this->db->select('count(safa_group_passports.safa_group_passport_id) as mofa_count');
		$this->db->from('safa_group_passports');
		$this->db->join('safa_umrahgroups_passports', 'safa_umrahgroups_passports.safa_group_passport_id  = safa_group_passports.safa_group_passport_id', 'left');
		$this->db->join('safa_umrahgroups', 'safa_umrahgroups.safa_umrahgroup_id  = safa_umrahgroups_passports.safa_umrahgroup_id', 'left');
		$this->db->join('safa_uo_contracts', 'safa_uo_contracts.safa_uo_contract_id  = safa_umrahgroups.safa_uo_contract_id', 'left');

		if ($this->safa_uo_id !== FALSE) {
			$this->db->where('safa_uo_contracts.safa_uo_id', $this->safa_uo_id);
		}

		$query = $this->db->get();
		return $query->row();
	}

	function get_total_mofa_count_in_KSA($hours_24 = false) {

		$select_str = '
            (select count(safa_group_passports.safa_group_passport_id) from safa_group_passports      where  safa_group_passports.safa_trip_id=safa_trips.safa_trip_id  ) as individuals_count, 
            (select count(safa_group_passports.safa_group_passport_id) from safa_group_passports      where  safa_group_passports.safa_trip_id=safa_trips.safa_trip_id and safa_group_passports.dpn_serial_no=0 ) as passports_count,
            (select count(safa_group_passports.safa_group_passport_id) from safa_group_passports inner join safa_umrahgroups_passports on safa_umrahgroups_passports.safa_group_passport_id = safa_group_passports.safa_group_passport_id where  safa_group_passports.safa_trip_id=safa_trips.safa_trip_id ) as mofa_count, 
            (select count(safa_group_passports.safa_group_passport_id) from safa_group_passports      where  safa_group_passports.safa_trip_id=safa_trips.safa_trip_id and safa_group_passports.enumber != "" ) as visa_order_count, 
            (select count(safa_group_passports.safa_group_passport_id) from safa_group_passports      where  safa_group_passports.safa_trip_id=safa_trips.safa_trip_id and safa_group_passports.visa_number != "" ) as visa_number_count 
            ';

		$this->db->select($select_str);


		$this->db->from('safa_group_passports');
		$this->db->join('safa_trips', 'safa_group_passports.safa_trip_id = safa_trips.safa_trip_id', 'left');
		$this->db->join('safa_group_passport_accommodation', 'safa_group_passport_accommodation.safa_group_passport_id = safa_group_passports.safa_group_passport_id', 'left');
		$this->db->join('safa_package_periods', 'safa_package_periods.safa_package_periods_id = safa_group_passport_accommodation.safa_package_periods_id', 'left');
		$this->db->join('safa_packages', 'safa_packages.safa_package_id = safa_package_periods.safa_package_id', 'left');

		$this->db->join('safa_trip_internaltrips', 'safa_trip_internaltrips.safa_trip_id = safa_trips.safa_trip_id', 'left');
		$this->db->join('safa_internalsegments', 'safa_internalsegments.safa_trip_internaltrip_id = safa_trip_internaltrips.safa_trip_internaltrip_id', 'left');
		$this->db->where('safa_internalsegments.safa_internalsegmentestatus_id', 1);

		if ($this->safa_uo_id !== FALSE) {
			$this->db->where('safa_packages.erp_company_type_id', 2);
			$this->db->where('safa_packages.erp_company_id', $this->safa_uo_id);
		}


		if ($hours_24) {
			$this->db->where('safa_internalsegments.safa_internalsegmenttype_id', 2);
			$this->db->where('safa_internalsegments.start_datetime', 2);

			$now_datetime = date('Y-m-d H:i', time());
			$now_datetime = strtotime($now_datetime);
			$now_datetime_minus_1 = strtotime('-1 day', $now_datetime);
			$now_datetime_minus_1 = date('Y-m-d H:i', $now_datetime_minus_1);

			//echo "<script>alert('$now_datetime_minus_1')</script>";
			$this->db->where('safa_internalsegments.start_datetime >= ', $now_datetime_minus_1);
		}



		$query = $this->db->get();
		return $query->row();
	}

	function get_packages($rows_no = FALSE) 
	{
		$this->db->distinct();

		if ($this->custom_select !== FALSE) {
			$this->db->select('safa_reservation_form_id');
			$this->db->select($this->custom_select);
		}


		$select_str = 'safa_reservation_forms.*, safa_packages.erp_company_type_id, safa_packages.erp_company_id, safa_trips.name as safa_trip_name, safa_trips.erp_transportertype_id, erp_package_periods.'.name().' as erp_package_period_name';

		if ($this->safa_uo_id !== FALSE) {
			$select_str = $select_str . ', safa_uo_contracts.' . name() . ' as safa_uo_contract_name';
		}
		
		$this->db->select($select_str);


		if ($this->safa_trip_id !== FALSE)
		$this->db->where('safa_reservation_forms.safa_trip_id', $this->safa_trip_id);

		if ($this->safa_ea_id !== FALSE)
		$this->db->where('safa_reservation_forms.safa_ea_id', $this->safa_ea_id);

		if ($this->safa_uo_id !== FALSE) {
			$this->db->where('safa_packages.erp_company_type_id', 2);
			$this->db->where('safa_packages.erp_company_id', $this->safa_uo_id);
		}

		if ($this->safa_tripstatus_id !== FALSE)
		$this->db->where('safa_trips.safa_tripstatus_id', $this->safa_tripstatus_id);

		if ($this->safa_tripstatus_id_not !== FALSE) {
			$this->db->where('safa_trips.safa_tripstatus_id !=', $this->safa_tripstatus_id_not);
		}

		if ($this->safa_trip_confirm_id !== FALSE)
		$this->db->where('safa_trips.safa_trip_confirm_id', $this->safa_trip_confirm_id);

		
		$this->db->order_by('safa_packages.erp_company_id');
		
		if (!$rows_no && $this->limit)
		$this->db->limit($this->limit, $this->offset);

		$this->db->from('safa_reservation_forms');
		$this->db->join('safa_trips', 'safa_trips.safa_trip_id = safa_reservation_forms.safa_trip_id', 'left');
		
		//if ($this->safa_uo_id !== FALSE) {
			
			$this->db->join('safa_package_periods', 'safa_package_periods.safa_package_periods_id = safa_reservation_forms.safa_package_period_id', 'left');
			$this->db->join('safa_packages', 'safa_packages.safa_package_id = safa_package_periods.safa_package_id', 'left');
			$this->db->join('erp_package_periods', 'erp_package_periods.erp_package_period_id = safa_package_periods.erp_package_period_id', 'left');
			
			$this->db->join('safa_uo_contracts_eas', 'safa_uo_contracts_eas.safa_ea_id = safa_trips.safa_ea_id', 'left');
			$this->db->join('safa_uo_contracts', 'safa_uo_contracts.safa_uo_contract_id = safa_uo_contracts_eas.safa_uo_contract_id', 'left');
						
		//}
		
		$query = $this->db->get();
		//echo $this->db->last_query(); exit;

		if ($rows_no) {
			return $query->num_rows();
		} else {
			return $query->result();
		}
	}
}

/* End of file safa_trips_model.php */
/* Location: ./application/models/safa_trips_model.php */
