<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Erp_cruise_availabilities_model extends CI_Model 
{

    public $erp_cruise_availability_id = FALSE;  
    public $pay_deposit_value = FALSE;  
    public $check_names = FALSE;  
    public $to_get_names = FALSE;  
    public $to_issue_ticket = FALSE;  
    public $pay_deposit = FALSE;  
    public $safa_ea_id = FALSE;  
    public $safa_trip_id = FALSE;  
    public $private = FALSE;  
    public $by_trip = FALSE;  
    public $by_trip_internaltrip = FALSE;
    
    public $custom_select = FALSE;
    public $limit = FALSE;
    public $offset = FALSE;
    public $order_by = FALSE;

    function __construct() {
        parent::__construct();
    }

    function get($rows_no = FALSE) 
    {

        if ($this->custom_select !== FALSE) {
            $this->db->select('erp_cruise_availability_id');
            $this->db->select($this->custom_select);
        }

        if ($this->erp_cruise_availability_id !== FALSE)
            $this->db->where('erp_cruise_availabilities.erp_cruise_availability_id', $this->erp_cruise_availability_id);

       if ($this->pay_deposit_value !== FALSE)
            $this->db->where('erp_cruise_availabilities.pay_deposit_value', $this->pay_deposit_value);

       if ($this->check_names !== FALSE)
            $this->db->where('erp_cruise_availabilities.check_names', $this->check_names);

       if ($this->to_get_names !== FALSE)
            $this->db->where('erp_cruise_availabilities.to_get_names', $this->to_get_names);

       if ($this->to_issue_ticket !== FALSE)
            $this->db->where('erp_cruise_availabilities.to_issue_ticket', $this->to_issue_ticket);

       if ($this->pay_deposit !== FALSE)
            $this->db->where('erp_cruise_availabilities.pay_deposit', $this->pay_deposit);

       if ($this->safa_ea_id !== FALSE)
            $this->db->where('erp_cruise_availabilities.safa_ea_id', $this->safa_ea_id);

       if ($this->safa_trip_id !== FALSE)
            $this->db->where('erp_cruise_availabilities.safa_trip_id', $this->safa_trip_id);

       if ($this->private !== FALSE)
            $this->db->where('erp_cruise_availabilities.private', $this->private);

       if ($this->by_trip !== FALSE)
            $this->db->where('erp_cruise_availabilities.by_trip', $this->by_trip);

       if ($this->by_trip_internaltrip !== FALSE)
            $this->db->where('erp_cruise_availabilities.by_trip_internaltrip', $this->by_trip_internaltrip);
        
            

        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('erp_cruise_availabilities');
        if ($rows_no)
            return $query->num_rows();

        if ($this->erp_cruise_availability_id)
            return $query->row();
        else
            return $query->result();
    }

    function save() 
    {
        if ($this->erp_cruise_availability_id !== FALSE)
            $this->db->set('erp_cruise_availabilities.erp_cruise_availability_id', $this->erp_cruise_availability_id);

        if ($this->pay_deposit_value !== FALSE)
            $this->db->set('erp_cruise_availabilities.pay_deposit_value', $this->pay_deposit_value);

       if ($this->check_names !== FALSE)
            $this->db->set('erp_cruise_availabilities.check_names', $this->check_names);

       if ($this->to_get_names !== FALSE)
            $this->db->set('erp_cruise_availabilities.to_get_names', $this->to_get_names);

       if ($this->to_issue_ticket !== FALSE)
            $this->db->set('erp_cruise_availabilities.to_issue_ticket', $this->to_issue_ticket);

       if ($this->pay_deposit !== FALSE)
            $this->db->set('erp_cruise_availabilities.pay_deposit', $this->pay_deposit);

       if ($this->safa_ea_id !== FALSE)
            $this->db->set('erp_cruise_availabilities.safa_ea_id', $this->safa_ea_id);

       if ($this->safa_trip_id !== FALSE)
            $this->db->set('erp_cruise_availabilities.safa_trip_id', $this->safa_trip_id);

       if ($this->private !== FALSE)
            $this->db->set('erp_cruise_availabilities.private', $this->private);

       if ($this->by_trip !== FALSE)
            $this->db->set('erp_cruise_availabilities.by_trip', $this->by_trip);

       if ($this->by_trip_internaltrip !== FALSE)
            $this->db->set('erp_cruise_availabilities.by_trip_internaltrip', $this->by_trip_internaltrip);
        


        if ($this->erp_cruise_availability_id) {
            $this->db->where('erp_cruise_availabilities.erp_cruise_availability_id', $this->erp_cruise_availability_id)->update('erp_cruise_availabilities');
        } else {
            $this->db->insert('erp_cruise_availabilities');
            return $this->db->insert_id();
        }
    }

    function delete() 
    {
        if ($this->erp_cruise_availability_id !== FALSE)
            $this->db->where('erp_cruise_availabilities.erp_cruise_availability_id', $this->erp_cruise_availability_id);

        if ($this->pay_deposit_value !== FALSE)
            $this->db->where('erp_cruise_availabilities.pay_deposit_value', $this->pay_deposit_value);

       if ($this->check_names !== FALSE)
            $this->db->where('erp_cruise_availabilities.check_names', $this->check_names);

       if ($this->to_get_names !== FALSE)
            $this->db->where('erp_cruise_availabilities.to_get_names', $this->to_get_names);

       if ($this->to_issue_ticket !== FALSE)
            $this->db->where('erp_cruise_availabilities.to_issue_ticket', $this->to_issue_ticket);

       if ($this->pay_deposit !== FALSE)
            $this->db->where('erp_cruise_availabilities.pay_deposit', $this->pay_deposit);

       if ($this->safa_ea_id !== FALSE)
            $this->db->where('erp_cruise_availabilities.safa_ea_id', $this->safa_ea_id);

       if ($this->safa_trip_id !== FALSE)
            $this->db->where('erp_cruise_availabilities.safa_trip_id', $this->safa_trip_id);

       if ($this->private !== FALSE)
            $this->db->where('erp_cruise_availabilities.private', $this->private);

       if ($this->by_trip !== FALSE)
            $this->db->where('erp_cruise_availabilities.by_trip', $this->by_trip);

       if ($this->by_trip_internaltrip !== FALSE)
            $this->db->where('erp_cruise_availabilities.by_trip_internaltrip', $this->by_trip_internaltrip);
        

        $this->db->delete('erp_cruise_availabilities');
        return $this->db->affected_rows();
    }

}

/* End of file erp_cruise_availabilities_model.php */
/* Location: ./application/models/erp_cruise_availabilities_model.php */