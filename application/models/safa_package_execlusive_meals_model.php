<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Safa_package_execlusive_meals_model extends CI_Model {

    public $safa_package_execlusive_meal_id = FALSE;
    public $safa_package_id = FALSE;
    public $hotel_id = FALSE;
    public $erp_meal_id = FALSE;
    public $price = FALSE;
    public $custom_select = FALSE;
    public $join = FALSE;
    public $limit = FALSE;
    public $offset = FALSE;
    public $order_by = FALSE;

    function __construct() {
        parent::__construct();
    }

    function get($rows_no = FALSE) {

        if ($this->custom_select !== FALSE) {
            $this->db->select('safa_package_execlusive_meal_id');
            $this->db->select($this->custom_select);
        }

        if ($this->safa_package_execlusive_meal_id !== FALSE)
            $this->db->where('safa_package_execlusive_meals.safa_package_execlusive_meal_id', $this->safa_package_execlusive_meal_id);

        if ($this->safa_package_id !== FALSE)
            $this->db->where('safa_package_execlusive_meals.safa_package_id', $this->safa_package_id);

        if ($this->hotel_id !== FALSE)
            $this->db->where('safa_package_execlusive_meals.hotel_id', $this->hotel_id);

        if ($this->erp_meal_id !== FALSE)
            $this->db->where('safa_package_execlusive_meals.erp_meal_id', $this->erp_meal_id);

        if ($this->price !== FALSE)
            $this->db->where('safa_package_execlusive_meals.price', $this->price);

        if($this->join){
            $this->db->select("safa_package_execlusive_meals.*,erp_hotels.".name()." as hotel_name,erp_meals.".name()." as meal_name");
            $this->db->join('erp_hotels','erp_hotels.erp_hotel_id = safa_package_execlusive_meals.hotel_id');
            $this->db->join('erp_meals','erp_meals.erp_meal_id = safa_package_execlusive_meals.erp_meal_id');
        }

        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('safa_package_execlusive_meals');
        if ($rows_no)
            return $query->num_rows();

        if ($this->safa_package_execlusive_meal_id)
            return $query->row();
        else
            return $query->result();
    }

    function save() {

        if ($this->safa_package_execlusive_meal_id !== FALSE)
            $this->db->set('safa_package_execlusive_meals.safa_package_execlusive_meal_id', $this->safa_package_execlusive_meal_id);

        if ($this->safa_package_id !== FALSE)
            $this->db->set('safa_package_execlusive_meals.safa_package_id', $this->safa_package_id);

        if ($this->hotel_id !== FALSE)
            $this->db->set('safa_package_execlusive_meals.hotel_id', $this->hotel_id);

        if ($this->erp_meal_id !== FALSE)
            $this->db->set('safa_package_execlusive_meals.erp_meal_id', $this->erp_meal_id);

        if ($this->price !== FALSE)
            $this->db->set('safa_package_execlusive_meals.price', $this->price);


        if ($this->safa_package_execlusive_meal_id) {
            $this->db->where('safa_package_execlusive_meals.safa_package_execlusive_meal_id', $this->safa_package_execlusive_meal_id)->update('safa_package_execlusive_meals');
        } else {
            $this->db->insert('safa_package_execlusive_meals');
            return $this->db->insert_id();
        }
    }

    function delete() {

        if ($this->safa_package_execlusive_meal_id !== FALSE)
            $this->db->where('safa_package_execlusive_meals.safa_package_execlusive_meal_id', $this->safa_package_execlusive_meal_id);

        if ($this->safa_package_id !== FALSE)
            $this->db->where('safa_package_execlusive_meals.safa_package_id', $this->safa_package_id);

        if ($this->hotel_id !== FALSE)
            $this->db->where('safa_package_execlusive_meals.hotel_id', $this->hotel_id);

        if ($this->erp_meal_id !== FALSE)
            $this->db->where('safa_package_execlusive_meals.erp_meal_id', $this->erp_meal_id);

        if ($this->price !== FALSE)
            $this->db->where('safa_package_execlusive_meals.price', $this->price);


        $this->db->delete('safa_package_execlusive_meals');
        return $this->db->affected_rows();
    }

    function search($rows_no = FALSE) {
        if ($this->custom_select !== FALSE) {
            $this->db->select('safa_package_execlusive_meal_id');
            $this->db->select($this->custom_select);
        }

        if ($this->safa_package_execlusive_meal_id !== FALSE)
            $this->db->where('safa_package_execlusive_meals.safa_package_execlusive_meal_id', $this->safa_package_execlusive_meal_id);

        if ($this->safa_package_id !== FALSE)
            $this->db->where('safa_package_execlusive_meals.safa_package_id', $this->safa_package_id);

        if ($this->hotel_id !== FALSE)
            $this->db->where('safa_package_execlusive_meals.hotel_id', $this->hotel_id);

        if ($this->erp_meal_id !== FALSE)
            $this->db->where('safa_package_execlusive_meals.erp_meal_id', $this->erp_meal_id);

        if ($this->price !== FALSE)
            $this->db->where('safa_package_execlusive_meals.price', $this->price);


        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('safa_package_execlusive_meals');
        if ($rows_no)
            return $query->num_rows();

        return $query->result();
    }

}

/* End of file uo_package_tourismplaces_model.php */
/* Location: ./application/models/uo_package_tourismplaces_model.php */