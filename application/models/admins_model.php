<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Admins_model extends CI_Model {

    public $erp_admin_id = FALSE;
    public $username = FALSE;
    public $password = FALSE;
    public $email = FALSE;
    public $custom_select = FALSE;
    public $limit = FALSE;
    public $offset = FALSE;
    public $order_by = FALSE;

    function __construct() {
        parent::__construct();
    }

    function get($rows_no = FALSE) {

        if ($this->custom_select !== FALSE) {
            $this->db->select('erp_admin_id');
            $this->db->select($this->custom_select);
        }

        if ($this->erp_admin_id !== FALSE)
            $this->db->where('erp_admins.erp_admin_id', $this->erp_admin_id);

        if ($this->username !== FALSE)
            $this->db->where('erp_admins.username', $this->username);

        if ($this->password !== FALSE)
            $this->db->where('erp_admins.password', $this->password);

        if ($this->email !== FALSE)
            $this->db->where('erp_admins.email', $this->email);


        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('erp_admins');
        if ($rows_no)
            return $query->num_rows();

        if ($this->erp_admin_id)
            return $query->row();
        else
            return $query->result();
    }

    function save() {
        if ($this->erp_admin_id !== FALSE)
            $this->db->set('erp_admins.erp_admin_id', $this->erp_admin_id);

        if ($this->username !== FALSE)
            $this->db->set('erp_admins.username', $this->username);

        if ($this->password !== FALSE)
            $this->db->set('erp_admins.password', $this->password);

        if ($this->email !== FALSE)
            $this->db->set('erp_admins.email', $this->email);



        if ($this->erp_admin_id) {
            $this->db->where('erp_admins.erp_admin_id', $this->erp_admin_id)->update('erp_admins');
        } else {
            $this->db->insert('erp_admins');
            return $this->db->insert_id();
        }
    }

    function delete() {
        if ($this->erp_admin_id !== FALSE)
            $this->db->where('erp_admins.erp_admin_id', $this->erp_admin_id);

        if ($this->username !== FALSE)
            $this->db->where('erp_admins.username', $this->username);

        if ($this->password !== FALSE)
            $this->db->where('erp_admins.password', $this->password);

        if ($this->email !== FALSE)
            $this->db->where('erp_admins.email', $this->email);



        $this->db->delete('erp_admins');
        return $this->db->affected_rows();
    }

    function search($rows_no = FALSE) {
        if ($this->custom_select !== FALSE) {
            $this->db->select('erp_admin_id');
            $this->db->select($this->custom_select);
        }

        if ($this->erp_admin_id !== FALSE)
            $this->db->where('erp_admins.erp_admin_id', $this->erp_admin_id);

        if ($this->username !== FALSE)
            $this->db->where('erp_admins.username', $this->username);

        if ($this->password !== FALSE)
            $this->db->where('erp_admins.password', $this->password);

        if ($this->email !== FALSE)
            $this->db->where('erp_admins.email', $this->email);


        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('erp_admins');
        if ($rows_no)
            return $query->num_rows();

        return $query->result();
    }

    function check_login($username, $password) {
        $this->db->where('username', $username);
        $this->db->where('password', $password);
        return $this->db->get('erp_admins')->row();
    }

    function update_user_info($user_id) {

        if (session('language') == 'arabic') {
            $data = array(
                'username' => session('admin_username'),
                'password' => session('admin_password'),
                'erp_language_id' => 2
            );

            $this->db->where('erp_admin_id', $user_id);
            $this->db->update('erp_admins', $data);
        } else {
            $data = array(
                'username' => session('admin_username'),
                'password' => session('admin_password'),
                'erp_language_id' => 1
            );

            $this->db->where('erp_admin_id', $user_id);
            $this->db->update('erp_admins', $data);
        }
    }

    function update_user_lang($user_id, $language) {
        echo $language;
        if ($language == 'arabic') {
            $data = array(
                'erp_language_id' => 2
            );

            $this->db->where('erp_admin_id', $user_id);
            return $this->db->update('erp_admins', $data);
        } else {
            $data = array(
                'erp_language_id' => 1
            );

            $this->db->where('erp_admin_id', $user_id);
            return $this->db->update('erp_admins', $data);
        }
    }

    function getlanguage($user_id) {
        $this->db->select('erp_languages.name, erp_languages.path');
        $this->db->from('erp_admins');
        $this->db->join('erp_languages', 'erp_admins.erp_language_id = erp_languages.erp_language_id');
        $this->db->where('erp_admins.erp_admin_id', $user_id);
        $query = $this->db->get();
        return $query->row();
    }

    function getuser($user_id) {

        $this->db->where('erp_admin_id', $user_id);
        return $this->db->get('erp_admins')->row();
    }

}

/* End of file admins_model.php */
/* Location: ./application/models/admins_model.php */