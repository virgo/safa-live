<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Projects_model extends CI_Model {

    public $projects_id;
    public $name;
    public $database;
    public $limit;
    public $offset;

    function __construct() {
        parent::__construct();
    }

    function get($rows_no = FALSE) {

        if ($this->projects_id)
            $this->db->where('projects_id', $this->projects_id);

        if ($this->name)
            $this->db->where('name', $this->name);

        if ($this->database)
            $this->db->where('database', $this->database);



        if (!$rows_no)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('projects');

        if ($rows_no)
            return $query->num_rows();

        if ($this->projects_id)
            return $query->row();
        else
            return $query->result();
    }

    function save() {

        if ($this->name)
            $this->db->set('name', $this->name);


        if ($this->database)
            $this->db->where('database', $this->database);

        if ($this->projects_id) {
            $this->db->where('projects_id', $this->projects_id)->update('projects');
            return $this->projects_id;
        } else {
            $this->db->insert('projects');
            return $this->db->insert_id();
        }
    }

    function delete() {
        $this->db->where('projects_id', $this->projects_id)->delete('projects');
    }

}

/* End of file projects.php */
/* Location: ./application/transporter_idls/projects.php */