<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Trip_externalpassages_model extends CI_Model {

    public $safa_trip_externalsegment_id = FALSE;
    public $start_airporthall_id = FALSE;
    public $end_airporthall_id = FALSE;
    public $departure_datetime = FALSE;
    public $arrival_datetime = FALSE;
    public $safa_transporter_id = FALSE;
    public $safa_externalsegmenttype_id = FALSE;
    public $safa_trip_id = FALSE;
    public $trip_code = FALSE;
    public $custom_select = FALSE;
    public $limit = FALSE;
    public $offset = FALSE;
    public $order_by = FALSE;

    function __construct() {
        parent::__construct();
    }

    function get($rows_no = FALSE) {

        if ($this->custom_select !== FALSE) {
            $this->db->select('safa_trip_externalsegment_id');
            $this->db->select($this->custom_select);
        }

        if ($this->safa_trip_externalsegment_id !== FALSE)
            $this->db->where('safa_trip_externalsegments.safa_trip_externalsegment_id', $this->safa_trip_externalsegment_id);

        if ($this->start_airporthall_id !== FALSE)
            $this->db->where('safa_trip_externalsegments.start_airporthall_id', $this->start_airporthall_id);

        if ($this->end_airporthall_id !== FALSE)
            $this->db->where('safa_trip_externalsegments.end_airporthall_id', $this->end_airporthall_id);

        if ($this->departure_datetime !== FALSE)
            $this->db->where('safa_trip_externalsegments.departure_datetime', $this->departure_datetime);

        if ($this->arrival_datetime !== FALSE)
            $this->db->where('safa_trip_externalsegments.arrival_datetime', $this->arrival_datetime);

        if ($this->safa_transporter_id !== FALSE)
            $this->db->where('safa_trip_externalsegments.safa_transporter_id', $this->safa_transporter_id);

        if ($this->safa_externalsegmenttype_id !== FALSE)
            $this->db->where('safa_trip_externalsegments.safa_externalsegmenttype_id', $this->safa_externalsegmenttype_id);

        if ($this->safa_trip_id !== FALSE)
            $this->db->where('safa_trip_externalsegments.safa_trip_id', $this->safa_trip_id);

        if ($this->trip_code !== FALSE)
            $this->db->where('safa_trip_externalsegments.trip_code', $this->trip_code);


        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('safa_trip_externalsegments');
        if ($rows_no)
            return $query->num_rows();

        if ($this->safa_trip_externalsegment_id)
            return $query->row();
        else
            return $query->result();
    }

    function save() {
        if ($this->safa_trip_externalsegment_id !== FALSE)
            $this->db->set('safa_trip_externalsegments.safa_trip_externalsegment_id', $this->safa_trip_externalsegment_id);

        if ($this->start_airporthall_id !== FALSE)
            $this->db->set('safa_trip_externalsegments.start_airporthall_id', $this->start_airporthall_id);

        if ($this->end_airporthall_id !== FALSE)
            $this->db->set('safa_trip_externalsegments.end_airporthall_id', $this->end_airporthall_id);

        if ($this->departure_datetime !== FALSE)
            $this->db->set('safa_trip_externalsegments.departure_datetime', $this->departure_datetime);

        if ($this->arrival_datetime !== FALSE)
            $this->db->set('safa_trip_externalsegments.arrival_datetime', $this->arrival_datetime);

        if ($this->safa_transporter_id !== FALSE)
            $this->db->set('safa_trip_externalsegments.safa_transporter_id', $this->safa_transporter_id);

        if ($this->safa_externalsegmenttype_id !== FALSE)
            $this->db->set('safa_trip_externalsegments.safa_externalsegmenttype_id', $this->safa_externalsegmenttype_id);

        if ($this->safa_trip_id !== FALSE)
            $this->db->set('safa_trip_externalsegments.safa_trip_id', $this->safa_trip_id);

        if ($this->trip_code !== FALSE)
            $this->db->set('safa_trip_externalsegments.trip_code', $this->trip_code);



        if ($this->safa_trip_externalsegment_id) {
            $this->db->where('safa_trip_externalsegments.safa_trip_externalsegment_id', $this->safa_trip_externalsegment_id)->update('safa_trip_externalsegments');
        } else {
            $this->db->insert('safa_trip_externalsegments');
            return $this->db->insert_id();
        }
    }

    function delete() {
        if ($this->safa_trip_externalsegment_id !== FALSE)
            $this->db->where('safa_trip_externalsegments.safa_trip_externalsegment_id', $this->safa_trip_externalsegment_id);

        if ($this->start_airporthall_id !== FALSE)
            $this->db->where('safa_trip_externalsegments.start_airporthall_id', $this->start_airporthall_id);

        if ($this->end_airporthall_id !== FALSE)
            $this->db->where('safa_trip_externalsegments.end_airporthall_id', $this->end_airporthall_id);

        if ($this->departure_datetime !== FALSE)
            $this->db->where('safa_trip_externalsegments.departure_datetime', $this->departure_datetime);

        if ($this->arrival_datetime !== FALSE)
            $this->db->where('safa_trip_externalsegments.arrival_datetime', $this->arrival_datetime);

        if ($this->safa_transporter_id !== FALSE)
            $this->db->where('safa_trip_externalsegments.safa_transporter_id', $this->safa_transporter_id);

        if ($this->safa_externalsegmenttype_id !== FALSE)
            $this->db->where('safa_trip_externalsegments.safa_externalsegmenttype_id', $this->safa_externalsegmenttype_id);

        if ($this->safa_trip_id !== FALSE)
            $this->db->where('safa_trip_externalsegments.safa_trip_id', $this->safa_trip_id);

        if ($this->trip_code !== FALSE)
            $this->db->where('safa_trip_externalsegments.trip_code', $this->trip_code);



        $this->db->delete('safa_trip_externalsegments');
        return $this->db->affected_rows();
    }

    function search($rows_no = FALSE) {
        if ($this->custom_select !== FALSE) {
            $this->db->select('safa_trip_externalsegment_id');
            $this->db->select($this->custom_select);
        }

        if ($this->safa_trip_externalsegment_id !== FALSE)
            $this->db->where('safa_trip_externalsegments.safa_trip_externalsegment_id', $this->safa_trip_externalsegment_id);

        if ($this->start_airporthall_id !== FALSE)
            $this->db->where('safa_trip_externalsegments.start_airporthall_id', $this->start_airporthall_id);

        if ($this->end_airporthall_id !== FALSE)
            $this->db->where('safa_trip_externalsegments.end_airporthall_id', $this->end_airporthall_id);

        if ($this->departure_datetime !== FALSE)
            $this->db->where('safa_trip_externalsegments.departure_datetime', $this->departure_datetime);

        if ($this->arrival_datetime !== FALSE)
            $this->db->where('safa_trip_externalsegments.arrival_datetime', $this->arrival_datetime);

        if ($this->safa_transporter_id !== FALSE)
            $this->db->where('safa_trip_externalsegments.safa_transporter_id', $this->safa_transporter_id);

        if ($this->safa_externalsegmenttype_id !== FALSE)
            $this->db->where('safa_trip_externalsegments.safa_externalsegmenttype_id', $this->safa_externalsegmenttype_id);

        if ($this->safa_trip_id !== FALSE)
            $this->db->where('safa_trip_externalsegments.safa_trip_id', $this->safa_trip_id);

        if ($this->trip_code !== FALSE)
            $this->db->where('safa_trip_externalsegments.trip_code', $this->trip_code);


        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('safa_trip_externalsegments');
        if ($rows_no)
            return $query->num_rows();

        return $query->result();
    }

}

/* End of file trip_externalpassages_model.php */
/* Location: ./application/models/trip_externalpassages_model.php */