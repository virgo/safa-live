<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Safa_trips_requests_model extends CI_Model {

    public $safa_trips_requests_id = FALSE;
    public $safa_uo_service_id = FALSE;
    public $remarks = FALSE;
    public $safa_trip_id = FALSE;
    public $custom_select = FALSE;
    public $limit = FALSE;
    public $offset = FALSE;
    public $order_by = FALSE;

    function __construct() {
        parent::__construct();
    }

    function get($rows_no = FALSE) {

        if ($this->custom_select !== FALSE) {
            $this->db->select('safa_trips_requests_id');
            $this->db->select($this->custom_select);
        }

        if ($this->safa_trips_requests_id !== FALSE)
            $this->db->where('safa_trips_requests.safa_trips_requests_id', $this->safa_trips_requests_id);

        if ($this->safa_trip_id !== FALSE)
            $this->db->where('safa_trips_requests.safa_trip_id', $this->safa_trip_id);

        if ($this->safa_uo_service_id !== FALSE)
            $this->db->where('safa_trips_requests.safa_uo_service_id', $this->safa_uo_service_id);

        if ($this->remarks !== FALSE)
            $this->db->where('safa_trips_requests.remarks', $this->remarks);


        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('safa_trips_requests');
        if ($rows_no)
            return $query->num_rows();

        if ($this->safa_trips_requests_id)
            return $query->row();
        else
            return $query->result();
    }

    function save() {
        if ($this->safa_trips_requests_id !== FALSE)
            $this->db->set('safa_trips_requests.safa_trips_requests_id', $this->safa_trips_requests_id);

        if ($this->safa_trip_id !== FALSE)
            $this->db->set('safa_trips_requests.safa_trip_id', $this->safa_trip_id);

        if ($this->safa_uo_service_id !== FALSE)
            $this->db->set('safa_trips_requests.safa_uo_service_id', $this->safa_uo_service_id);

        if ($this->remarks !== FALSE)
            $this->db->set('safa_trips_requests.remarks', $this->remarks);



        if ($this->safa_trips_requests_id) {
            $this->db->where('safa_trips_requests.safa_trips_requests_id', $this->safa_trips_requests_id)->update('safa_trips_requests');
        } else {
            $this->db->insert('safa_trips_requests');
            return $this->db->insert_id();
        }
    }

    function delete() {
        if ($this->safa_trips_requests_id !== FALSE)
            $this->db->where('safa_trips_requests.safa_trips_requests_id', $this->safa_trips_requests_id);

        if ($this->safa_trip_id !== FALSE)
            $this->db->where('safa_trips_requests.safa_trip_id', $this->safa_trip_id);

        if ($this->safa_uo_service_id !== FALSE)
            $this->db->where('safa_trips_requests.safa_uo_service_id', $this->safa_uo_service_id);

        if ($this->remarks !== FALSE)
            $this->db->where('safa_trips_requests.remarks', $this->remarks);


        $this->db->delete('safa_trips_requests');
        return $this->db->affected_rows();
    }

    function search($rows_no = FALSE) {
        if ($this->custom_select !== FALSE) {
            $this->db->select('safa_trips_requests_id');
            $this->db->select($this->custom_select);
        }

        if ($this->safa_trips_requests_id !== FALSE)
            $this->db->where('safa_trips_requests.safa_trips_requests_id', $this->safa_trips_requests_id);

        if ($this->safa_trip_id !== FALSE)
            $this->db->where('safa_trips_requests.safa_trip_id', $this->safa_trip_id);

        if ($this->safa_uo_service_id !== FALSE)
            $this->db->where('safa_trips_requests.safa_uo_service_id', $this->safa_uo_service_id);

        if ($this->remarks !== FALSE)
            $this->db->where('safa_trips_requests.remarks', $this->remarks);


        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('safa_trips_requests');
        if ($rows_no)
            return $query->num_rows();

        return $query->result();
    }

    function get_by_trip_id($trip_id) {
        $this->db->select('safa_trips_requests.*, safa_uo_services.' . name() . ' as safa_uo_service_name');
        $this->db->from('safa_trips_requests');
        $this->db->join('safa_uo_services', 'safa_trips_requests.safa_uo_service_id = safa_uo_services.safa_uo_service_id');
        $this->db->where("safa_trip_id", $trip_id);
        $query = $this->db->get();
        return $query->result();
    }

    function delete_by_trip_id($trip_id) {
        $this->db->where("safa_trip_id", $trip_id);
        return $this->db->delete("safa_trips_requests");
    }

    function insert($data) {
        $this->db->insert('safa_trips_requests', $data);
    }

    function update($safa_trips_requests_id, $data) {
        $this->db->where('safa_trips_requests_id', $safa_trips_requests_id);
        $this->db->update('safa_trips_requests', $data);
    }

    function delete_by_trip_and_id($trip_id, $safa_trips_requests_id) {
        $this->db->where("safa_trip_id", $trip_id);
        $this->db->where("safa_trips_requests_id", $safa_trips_requests_id);
        return $this->db->delete("safa_trips_requests");
    }

}

/* End of file safa_trips_requests_model.php */
/* Location: ./application/models/safa_trips_requests_model.php */