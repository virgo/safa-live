<?php

if (!defined('BASEPATH'))
exit('No direct script access allowed');

class Hotel_exchange_model extends CI_Model {

	public $new_erp_hotel_id = FALSE;
	public $old_erp_hotel_id = FALSE;

	public $custom_select = FALSE;
	public $limit = FALSE;
	public $offset = FALSE;
	public $order_by = FALSE;

	function __construct()
	{
		parent::__construct();
	}

	 
	function save()
	{
		if ($this->new_erp_hotel_id !== FALSE && $this->old_erp_hotel_id) {
			$this->db->set('erp_hotel_id', $this->new_erp_hotel_id);
			$this->db->where('erp_hotel_id', $this->old_erp_hotel_id)->update('erp_companies_hotels');
			
			$this->db->set('erp_hotel_id', $this->new_erp_hotel_id);
			$this->db->where('erp_hotel_id', $this->old_erp_hotel_id)->update('erp_hotel_advantages_details');
			
			$this->db->set('erp_hotel_id', $this->new_erp_hotel_id);
			$this->db->where('erp_hotel_id', $this->old_erp_hotel_id)->update('erp_hotel_speakinglanguage_details');
			
			$this->db->set('erp_hotel_id', $this->new_erp_hotel_id);
			$this->db->where('erp_hotel_id', $this->old_erp_hotel_id)->update('erp_hotels_availability_master');
			
			$this->db->set('erp_hotel_id', $this->new_erp_hotel_id);
			$this->db->where('erp_hotel_id', $this->old_erp_hotel_id)->update('erp_hotels_contact_officials');
			
			$this->db->set('erp_hotel_id', $this->new_erp_hotel_id);
			$this->db->where('erp_hotel_id', $this->old_erp_hotel_id)->update('erp_hotels_photos');
			
			$this->db->set('erp_hotels_id', $this->new_erp_hotel_id);
			$this->db->where('erp_hotels_id', $this->old_erp_hotel_id)->update('erp_hotels_policies_details');
			
			$this->db->set('erp_hotels_id', $this->new_erp_hotel_id);
			$this->db->where('erp_hotels_id', $this->old_erp_hotel_id)->update('erp_hotels_reservation_orders');
			
			$this->db->set('erp_hotels_id', $this->new_erp_hotel_id);
			$this->db->where('erp_hotels_id', $this->old_erp_hotel_id)->update('erp_hotels_reservation_orders_log');
			
			$this->db->set('erp_hotel_id', $this->new_erp_hotel_id);
			$this->db->where('erp_hotel_id', $this->old_erp_hotel_id)->update('erp_hotels_reservation_orders_meals');
			
			$this->db->set('erp_hotels_id', $this->new_erp_hotel_id);
			$this->db->where('erp_hotels_id', $this->old_erp_hotel_id)->update('erp_hotels_reservation_orders_rooms');
			
			$this->db->set('erp_hotel_id', $this->new_erp_hotel_id);
			$this->db->where('erp_hotel_id', $this->old_erp_hotel_id)->update('erp_hotels_videos');
			
			$this->db->set('erp_hotel_id', $this->new_erp_hotel_id);
			$this->db->where('erp_hotel_id', $this->old_erp_hotel_id)->update('safa_ea_package_hotels');
			
			$this->db->set('erp_start_hotel_id', $this->new_erp_hotel_id);
			$this->db->where('erp_start_hotel_id', $this->old_erp_hotel_id)->update('safa_internalsegments');
			
			$this->db->set('erp_end_hotel_id', $this->new_erp_hotel_id);
			$this->db->where('erp_end_hotel_id', $this->old_erp_hotel_id)->update('safa_internalsegments');
			
			$this->db->set('hotel_id', $this->new_erp_hotel_id);
			$this->db->where('hotel_id', $this->old_erp_hotel_id)->update('safa_package_execlusive_meals');
			
			$this->db->set('hotel_id', $this->new_erp_hotel_id);
			$this->db->where('hotel_id', $this->old_erp_hotel_id)->update('safa_package_execlusive_nights');
			
			$this->db->set('erp_hotel_id', $this->new_erp_hotel_id);
			$this->db->where('erp_hotel_id', $this->old_erp_hotel_id)->update('safa_package_hotels');
			
			$this->db->set('erp_hotel_id', $this->new_erp_hotel_id);
			$this->db->where('erp_hotel_id', $this->old_erp_hotel_id)->update('safa_passport_accommodation_rooms');
			
			$this->db->set('erp_hotel_id', $this->new_erp_hotel_id);
			$this->db->where('erp_hotel_id', $this->old_erp_hotel_id)->update('safa_trip_hotels');
			
			$this->db->set('erp_hotel_id', $this->new_erp_hotel_id);
			$this->db->where('erp_hotel_id', $this->old_erp_hotel_id)->update('safa_trip_internaltrips_hotels');
			
			//$this->db->set('erp_hotel_id', $this->new_erp_hotel_id);
			//$this->db->where('erp_hotel_id', $this->old_erp_hotel_id)->update('safa_uo_contracts_hotels');			
		}
	}

}

/* End of file hotel_exchange_model.php */
/* Location: ./application/models/hotel_exchange_model.php */