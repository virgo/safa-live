<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Ea_Ports_model extends CI_Model {

    public $safa_ea_port_id = FALSE;
    public $safa_ea_id = FALSE;
    public $erp_port_id = FALSE;
    public $custom_select = FALSE;
    public $limit = FALSE;
    public $offset = FALSE;
    public $order_by = FALSE;
    public $ea_id = FALSE;

    function __construct() {
        parent::__construct();
    }

    function get($rows_no = FALSE) {

        if ($this->custom_select !== FALSE) {
            $this->db->select('safa_ea_port_id');
            $this->db->select($this->custom_select);
        }

        if ($this->safa_ea_port_id !== FALSE)
            $this->db->where('safa_ea_ports.safa_ea_port_id', $this->safa_ea_port_id);

        if ($this->erp_port_id !== FALSE)
            $this->db->where('safa_ea_ports.erp_port_id', $this->erp_port_id);

        if ($this->safa_ea_id !== FALSE)
            $this->db->where('safa_ea_ports.safa_ea_id', $this->safa_ea_id);

        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        if ($this->ea_id) {
            $this->db->select('safa_ea_ports.*,erp_ports.name_ar,erp_ports.name_la,
            erp_ports.erp_port_id,erp_ports.code');
            $this->db->join('erp_ports', 'safa_ea_ports.erp_port_id = erp_ports.erp_port_id');
            $this->db->where('safa_ea_ports.safa_ea_id', $this->ea_id);
        }

        $query = $this->db->get('safa_ea_ports');
        if ($rows_no)
            return $query->num_rows();

        if ($this->safa_ea_port_id)
            return $query->row();
        else
            return $query->result();
    }

    function save() {

        if ($this->safa_ea_port_id !== FALSE)
            $this->db->set('safa_ea_ports.safa_ea_port_id', $this->safa_ea_port_id);

        if ($this->erp_port_id !== FALSE)
            $this->db->set('safa_ea_ports.erp_port_id', $this->erp_port_id);

        if ($this->safa_ea_id !== FALSE)
            $this->db->set('safa_ea_ports.safa_ea_id', $this->safa_ea_id);

        if ($this->safa_ea_port_id) {
            $this->db->where('safa_ea_ports.safa_ea_port_id', $this->safa_ea_port_id)->update('safa_ea_ports');
        } else {
            $this->db->insert('safa_ea_ports');
            return $this->db->insert_id();
        }
    }

    function delete() {

        if ($this->safa_ea_port_id !== FALSE)
            $this->db->where('safa_ea_ports.safa_ea_port_id', $this->safa_ea_port_id);

        if ($this->erp_port_id !== FALSE)
            $this->db->where('safa_ea_ports.erp_port_id', $this->erp_port_id);

        if ($this->safa_ea_id !== FALSE)
            $this->db->where('safa_ea_ports.safa_ea_id', $this->safa_ea_id);

        $this->db->delete('safa_ea_ports');
        return $this->db->affected_rows();
    }

    function search($rows_no = FALSE) {
        if ($this->custom_select !== FALSE) {
            $this->db->select('safa_ea_port_id');
            $this->db->select($this->custom_select);
        }

        if ($this->safa_ea_port_id !== FALSE)
            $this->db->where('safa_ea_ports.safa_ea_port_id', $this->safa_ea_port_id);

        if ($this->erp_port_id !== FALSE)
            $this->db->where('safa_ea_ports.erp_port_id', $this->erp_port_id);

        if ($this->safa_ea_id !== FALSE)
            $this->db->where('safa_ea_ports.safa_ea_id', $this->safa_ea_id);

        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('safa_ea_ports');
        if ($rows_no)
            return $query->num_rows();

        return $query->result();
    }

    function check_delete_ability($id) {

        $this->db->select('count(safa_trip_externalsegments.start_port_hall_id)as trip_externalsegments_start,count(safa_trip_externalsegments.end_port_hall_id)as trip_externalsegments_end');
        $this->db->from('erp_port_halls');
        $this->db->join('safa_trip_externalsegments', 'erp_port_halls.erp_port_hall_id = safa_trip_externalsegments.start_port_hall_id or erp_port_halls.erp_port_hall_id = safa_trip_externalsegments.end_port_hall_id', 'left');
        $this->db->group_by('erp_port_halls.erp_port_hall_id');
        $this->db->where('erp_port_halls.erp_port_hall_id', $id);
        $query = $this->db->get();
        $flag = 0;

//        print_r($query->result());
        foreach ($query->result() as $row => $table) {
            foreach ($table as $col => $value) {
                if ($value > 0) {
                    $flag = 1;
                    break;
                } else {
                    continue;
                }
            }
        }
        return $flag;
    }

    function get_erp_ports() {
        $this->db->select('CONCAT(erp_ports.code , " , " , erp_ports.' . name() . '," , ",erp_cities.' . name() . ') as name_code,erp_port_id', FALSE);
        $this->db->from('erp_ports');
        $this->db->join('erp_cities', 'erp_ports.erp_city_id=erp_cities.erp_city_id', 'left');
        $result = $this->db->get()->result();
        return $result;
    }

}

/* End of file ea_ports_model.php */
/* Location: ./application/models/ea_ports_model.php */
