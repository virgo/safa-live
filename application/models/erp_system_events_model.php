<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Erp_system_events_model extends CI_Model {

    public $erp_system_events_id = FALSE;
    public $type = FALSE;
    public $name_ar = FALSE;
    public $name_la = FALSE;
    public $custom_select = FALSE;
    public $limit = FALSE;
    public $offset = FALSE;
    public $order_by = FALSE;

    function __construct() {
        parent::__construct();
    }

    function get($rows_no = FALSE) {

        if ($this->custom_select !== FALSE) {
            $this->db->select('erp_system_events_id');
            $this->db->select($this->custom_select);
        }

        if ($this->erp_system_events_id !== FALSE)
            $this->db->where('erp_system_events.erp_system_events_id', $this->erp_system_events_id);

        if ($this->type !== FALSE)
            $this->db->where('erp_system_events.type', $this->type);

        if ($this->name_ar !== FALSE)
            $this->db->where('erp_system_events.name_ar', $this->name_ar);

        if ($this->name_la !== FALSE)
            $this->db->where('erp_system_events.name_la', $this->name_la);


        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('erp_system_events');
        if ($rows_no)
            return $query->num_rows();

        if ($this->erp_system_events_id)
            return $query->row();
        else
            return $query->result();
    }

}

/* End of file erp_system_events_model.php */
/* Location: ./application/models/erp_system_events_model.php */