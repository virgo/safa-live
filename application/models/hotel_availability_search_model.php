<?php

/**
 * Hotel Availability Search Query
 */
class Hotel_availability_search_model extends CI_Model {

    public $main_date_from = FALSE;
    public $main_date_to = FALSE;
    public $city_id = FALSE;
    public $hotel_id = FALSE;
    public $rooms = FALSE;
    public $nights = FALSE;
    public $date_from = FALSE;
    public $date_to = FALSE;
    public $housing_type_id = FALSE;
    public $room_type_id = FALSE;
    public $price_from = FALSE;
    public $erp_hotels_availability_view_id = FALSE;
    public $price_to = FALSE;
    public $currency = FALSE;
    public $distance_from_haram = FALSE;
    public $distance_to_haram = FALSE;
    public $level = FALSE;
    public $nationalities = FALSE;
    public $hotel_features = FALSE;
    public $inclusive_service = FALSE;
    public $exclusive_service = FALSE;
    public $ha_type = FALSE;
    //By Gouda, To filter by contracts options for this EA with its UO.
    public $erp_company_type_id = FALSE;
    public $safa_company_id = FALSE;
    public $limit = FALSE;
    public $offset = FALSE;

    public function get() {
        if (session("uo_id")) {
            $uoid = session("uo_id");
            $whereinstr = "erp_hotels_availability_detail.erp_hotels_availability_detail_id in (select erp_hotels_availability_detail.erp_hotels_availability_detail_id
FROM erp_hotels_availability_detail LEFT JOIN erp_hotels_availability_detail_uo_companies ON erp_hotels_availability_detail.erp_hotels_availability_detail_id = erp_hotels_availability_detail_uo_companies.erp_hotels_availability_detail_id
WHERE erp_hotels_availability_detail_uo_companies.erp_hotels_availability_detail_id IS NULL OR
( erp_hotels_availability_detail_uo_companies.safa_uo_id = $uoid and `status` = 1) AND NOT
( erp_hotels_availability_detail_uo_companies.safa_uo_id = $uoid and `status` = 0)
GROUP BY erp_hotels_availability_detail.erp_hotels_availability_detail_id)";
            $this->db->where($whereinstr);

            $whereexecinstr = "erp_room_execlusive_services.erp_room_exclusive_service_id in (select erp_room_execlusive_services.erp_room_exclusive_service_id
FROM erp_room_execlusive_services 
LEFT JOIN erp_room_execlusive_services_uo_companies ON erp_room_execlusive_services.erp_room_exclusive_service_id = erp_room_execlusive_services_uo_companies.erp_room_exclusive_service_id
WHERE erp_room_execlusive_services_uo_companies.erp_room_exclusive_service_id IS NULL OR
( erp_room_execlusive_services_uo_companies.safa_uo_id = $uoid and `status` = 1) AND NOT
( erp_room_execlusive_services_uo_companies.safa_uo_id = $uoid and `status` = 0)
GROUP BY erp_room_execlusive_services_uo_companies.erp_room_exclusive_service_id)";
            //$this->db->where($whereexecinstr);
        } elseif (session("hm_id")) {
            $hmid = session("hm_id");
            $whereinstr = "erp_room_execlusive_services.erp_room_exclusive_service_id in (select erp_room_execlusive_services.erp_room_exclusive_service_id
FROM erp_room_execlusive_services
LEFT JOIN erp_room_execlusive_services_uo_companies ON erp_room_execlusive_services.erp_room_exclusive_service_id = erp_room_execlusive_services_hm_companies.erp_room_exclusive_service_id
WHERE erp_room_execlusive_services_hm_companies.erp_room_exclusive_service_id IS NULL OR
( erp_room_execlusive_services_hm_companies.safa_hm_id = $hmid and `status` = 1) AND NOT
( erp_room_execlusive_services_hm_companies.safa_hm_id = $hmid and `status` = 0)
GROUP BY erp_room_execlusive_services.erp_room_exclusive_service_id)";
            $this->db->where($whereinstr);

            $whereexecinstr = "erp_hotels_availability_detail.erp_hotels_availability_detail_id in (select erp_hotels_availability_detail.erp_hotels_availability_detail_id
FROM erp_hotels_availability_detail
LEFT JOIN erp_hotels_availability_detail_hm_companies ON erp_hotels_availability_detail.erp_hotels_availability_detail_id = erp_hotels_availability_detail_hm_companies.erp_hotels_availability_detail_id
WHERE erp_hotels_availability_detail_hm_companies.erp_hotels_availability_detail_id IS NULL OR
( erp_hotels_availability_detail_hm_companies.safa_hm_id = $hmid and `status` = 1) AND NOT
( erp_hotels_availability_detail_hm_companies.safa_hm_id = $hmid and `status` = 0)
GROUP BY erp_hotels_availability_detail.erp_hotels_availability_detail_id)";
            //$this->db->where($whereexecinstr);            
        } elseif (session("ea_id")) {
            $eaid = session("ea_id");
            $eadata = $this->db->where('safa_ea_id', $eaid)->get('safa_eas')->row();
            $country_id = $eadata->erp_country_id;
            $whereinstr = "erp_hotels_availability_detail.erp_hotels_availability_detail_id in (select erp_hotels_availability_detail.erp_hotels_availability_detail_id
FROM erp_hotels_availability_detail
LEFT JOIN erp_hotels_availability_detail_ea_companies ON erp_hotels_availability_detail.erp_hotels_availability_detail_id = erp_hotels_availability_detail_ea_companies.erp_hotels_availability_detail_id
LEFT JOIN erp_hotels_availability_detail_countries ON erp_hotels_availability_detail.erp_hotels_availability_detail_id = erp_hotels_availability_detail_countries.erp_hotels_availability_detail_id
WHERE (
erp_hotels_availability_detail_ea_companies.erp_hotels_availability_detail_id IS NULL OR 
( erp_hotels_availability_detail_ea_companies.safa_ea_id = $eaid and erp_hotels_availability_detail_ea_companies.`status` = 1 ) AND NOT
( erp_hotels_availability_detail_ea_companies.safa_ea_id = $eaid and erp_hotels_availability_detail_ea_companies.`status` = 0 )) OR (
erp_hotels_availability_detail_countries.erp_hotels_availability_detail_id IS NULL OR
( erp_hotels_availability_detail_countries.erp_country_id = $country_id and erp_hotels_availability_detail_countries.`status` = 1) AND NOT
( erp_hotels_availability_detail_countries.erp_country_id = $country_id and erp_hotels_availability_detail_countries.`status` = 0)
))";
            $this->db->where($whereinstr);

            $whereexecinstr = "erp_hotels_availability_detail.erp_hotels_availability_detail_id in (select erp_room_execlusive_services.erp_room_exclusive_service_id
FROM erp_room_execlusive_services
LEFT JOIN erp_room_execlusive_services_ea_companies ON erp_room_execlusive_services.erp_room_exclusive_service_id = erp_room_execlusive_services_ea_companies.erp_room_exclusive_service_id
LEFT JOIN erp_room_execlusive_services_countries ON erp_room_execlusive_services.erp_room_exclusive_service_id = erp_room_execlusive_services_countries.erp_room_exclusive_service_id
WHERE(
erp_room_execlusive_services_ea_companies.erp_room_exclusive_service_id IS NULL OR 
( erp_room_execlusive_services_ea_companies.safa_ea_id = $eaid and erp_room_execlusive_services_ea_companies.`status` = 1 ) AND NOT
( erp_room_execlusive_services_ea_companies.safa_ea_id = $eaid and erp_room_execlusive_services_ea_companies.`status` = 0 ) ) OR (
erp_room_execlusive_services_countries.erp_room_exclusive_service_id IS NULL OR
( erp_room_execlusive_services_countries.erp_country_id = $country_id and erp_room_execlusive_services_countries.`status` = 1) AND NOT
( erp_room_execlusive_services_countries.erp_country_id = $country_id and erp_room_execlusive_services_countries.`status` = 0)
))";
            //$this->db->where($whereexecinstr);
        }

        $this->db->distinct();
        $this->db->select("erp_hotels.erp_hotel_id "
                . ", erp_hotels.erp_hotellevel_id"
                . ", erp_hotels.address_" . name(TRUE) . ""
                . ", erp_hotels.distance ,erp_hotelroomsizes." . name() . " as roomtype "
                . ", (erp_hotels_availability_rooms.rooms_beds_count - erp_hotels_availability_rooms.closed_rooms_beds_count -  IFNULL((SELECT
count(*)
FROM
erp_hotels_availability_room_details
WHERE
erp_hotels_availability_room_details.erp_hotels_availability_sub_room_id in (
select erp_hotels_availability_sub_rooms.erp_hotels_availability_sub_room_id from erp_hotels_availability_sub_rooms 
WHERE erp_hotels_availability_sub_rooms.erp_hotels_availability_rooms_id = erp_hotels_availability_rooms.erp_hotels_availability_rooms_id
) AND
erp_hotels_availability_room_details.from_date <= '$this->date_from' AND
erp_hotels_availability_room_details.to_date >= '$this->date_to' AND
erp_hotels_availability_room_details.`status` = 2),0 )) as available_count "
                . ", erp_hotels." . name() . ""
                . ", erp_cities." . name() . " as city"
                . ", erp_hotellevels." . name() . " as hotellevel"
                . ", erp_hotels_availability_master.erp_hotels_availability_master_id"
                . ", erp_hotels_availability_master.safa_company_id"
                . ", erp_hotels_availability_master.erp_company_type_id"
                . ", erp_hotels_availability_detail.erp_hotels_availability_detail_id"
                . ", erp_hotels_availability_detail.sale_price"
                . ", erp_hotels_availability_detail.erp_hotels_availability_view_id"
//        . ", erp_hotels_availability_rooms.erp_hotels_availability_rooms_id"
                . ", erp_hotels_availability_sub_rooms.erp_hotels_availability_sub_room_id"
                . "", false);

//        if ($this->main_date_from)
//            $this->db->where('erp_hotels_availability_master.date_from <=', $this->main_date_from);
//           if ($this->main_date_to)
//            $this->db->where('erp_hotels_availability_master.date_to >=', $this->main_date_to);

        if ($this->erp_hotels_availability_view_id)
            $this->db->where('erp_hotels_availability_detail.erp_hotels_availability_view_id', $this->erp_hotels_availability_view_id);

        if (in_array($this->ha_type, array(1, 2,3))) {
            if ($this->ha_type == 1) {
                if (session("uo_id")) {
                    $this->db->where('erp_hotels_availability_master.erp_company_type_id', 2);
                    $this->db->where('erp_hotels_availability_master.safa_company_id', session("uo_id"));
                } else if (session("ea_id")) {
                    $this->db->where('erp_hotels_availability_master.erp_company_type_id', 3);
                    $this->db->where('erp_hotels_availability_master.safa_company_id', session("ea_id"));
                }
            } elseif ($this->ha_type == 2) {
                if (session("uo_id")) {
                    $this->db->where('(erp_hotels_availability_master.erp_company_type_id <> 2 && '
                            . 'erp_hotels_availability_master.safa_company_id <> ' . session("uo_id") . ')', NULL, FALSE);
                } else if (session("ea_id")) {
                    $this->db->where('(erp_hotels_availability_master.erp_company_type_id <>3 && '
                            . 'erp_hotels_availability_master.safa_company_id <> ' . session("ea_id") . ')', NULL, FALSe);
                }
            } elseif ($this->ha_type == 3) {
                $this->db->where('erp_hotels_availability_master.erp_company_type_id', $this->erp_company_type_id);
                $this->db->where('erp_hotels_availability_master.safa_company_id', $this->safa_company_id);
            }
        }

        $this->db->where_in('erp_hotels_availability_detail.offer_status', '1');

//        if ($this->erp_company_type_id)
//            $this->db->where('erp_hotels_availability_master.erp_company_type_id', $this->erp_company_type_id);
//        if ($this->safa_company_id)
//            $this->db->where('erp_hotels_availability_master.safa_company_id', $this->safa_company_id);

        if ($this->date_from)
            $this->db->where('erp_hotels_availability_master.date_from <= ', $this->date_from);

        if ($this->date_to)
            $this->db->where('erp_hotels_availability_master.date_to >= ', $this->date_to);

        if ($this->city_id)
            $this->db->where('erp_hotels.erp_city_id', $this->city_id);

        if ($this->hotel_id)
            $this->db->where_in('erp_hotels_availability_master.erp_hotel_id', $this->hotel_id);

        if ($this->level)
            $this->db->where_in('erp_hotels.erp_hotellevel_id', $this->level);

        if ($this->nationalities)
            $this->db->where_in('erp_hotels_availability_nationalities.erp_nationality_id', $this->nationalities);

        if ($this->hotel_features)
            $this->db->where_in('erp_hotel_advantages_details.erp_hotel_advantages_id', $this->hotel_features);

        if ($this->inclusive_service)
            $this->db->where_in('erp_room_inclusive_services.erp_room_service_id', $this->inclusive_service);

        if ($this->exclusive_service)
            $this->db->where_in('erp_room_execlusive_services.erp_room_service_id', implode($this->exclusive_service));

        if ($this->distance_from_haram)
            $this->db->where('erp_hotels.distance >=', $this->distance_from_haram);

        if ($this->distance_to_haram)
            $this->db->where('erp_hotels.distance <=', $this->distance_to_haram);
        /*
          if($this->housing_type_id)
          $this->db->where('erp_hotels_availability_detail.erp_housingtype_id', $this->housing_type_id);
         */

        if ($this->housing_type_id)
            $this->db->where('erp_hotels_availability_rooms.erp_housingtype_id', $this->housing_type_id);

        if ($this->price_from)
            $this->db->where('erp_hotels_availability_detail.sale_price >=', $this->price_from);

        if ($this->price_to)
            $this->db->where('erp_hotels_availability_detail.sale_price <=', $this->price_to);

        if ($this->room_type_id) {
            $roomtypestring = "(erp_hotels_availability_rooms.erp_hotelroomsize_id = '$this->room_type_id' ||(";
            $roomtypestring .= "erp_hotels_availability_rooms.erp_hotelroomsize_id is NULL && erp_hotels_availability_rooms.max_beds_count >= '$this->room_type_id' ))";
            $this->db->where($roomtypestring);
        }

        if ($this->currency)
            $this->db->where('erp_hotels_availability_master.sale_currency_id', $this->currency);

        $this->db->join('erp_hotels', 'erp_hotels.erp_hotel_id = erp_hotels_availability_master.erp_hotel_id', 'left');
        $this->db->join('erp_cities', 'erp_cities.erp_city_id = erp_hotels.erp_city_id');

        $this->db->join('erp_hotels_availability_detail', 'erp_hotels_availability_detail.erp_hotels_availability_master_id = erp_hotels_availability_master.erp_hotels_availability_master_id', 'left');
        $this->db->join('erp_hotels_availability_rooms', 'erp_hotels_availability_rooms.erp_hotels_availability_master_id = erp_hotels_availability_master.erp_hotels_availability_master_id', 'left');
        //$this->db->join('erp_hotels_availability_sub_rooms','');
        $this->db->join('erp_hotelroomsizes', 'erp_hotels_availability_rooms.erp_hotelroomsize_id = erp_hotelroomsizes.erp_hotelroomsize_id', 'left');
        $this->db->join('erp_hotel_advantages_details', 'erp_hotel_advantages_details.erp_hotel_id = erp_hotels.erp_hotel_id', 'left');
        $this->db->join('erp_hotellevels', 'erp_hotellevels.erp_hotellevel_id = erp_hotels.erp_hotellevel_id', 'left');
        $this->db->join('erp_hotels_availability_sub_rooms', 'erp_hotels_availability_sub_rooms.erp_hotels_availability_rooms_id = erp_hotels_availability_rooms.erp_hotels_availability_rooms_id', 'left');
        $this->db->join('erp_hotels_availability_room_details', 'erp_hotels_availability_room_details.erp_hotels_availability_sub_room_id = erp_hotels_availability_sub_rooms.erp_hotels_availability_sub_room_id', 'left');

        //By Gouda
        $this->db->join('erp_hotels_reservation_orders_rooms', 'erp_hotels_reservation_orders_rooms.erp_hotels_availability_detail_id = erp_hotels_availability_detail.erp_hotels_availability_detail_id', 'left');
        $this->db->join('erp_hotels_reservation_orders', 'erp_hotels_reservation_orders_rooms.erp_hotels_reservation_orders_id = erp_hotels_reservation_orders.erp_hotels_reservation_orders_id', 'left');
        $this->db->join('erp_hotels_availability_nationalities', 'erp_hotels_availability_nationalities.erp_hotels_availability_master_id = erp_hotels_availability_master.erp_hotels_availability_master_id', 'left');
        $this->db->join('erp_room_inclusive_services', 'erp_room_inclusive_services.erp_hotels_availability_master_id = erp_hotels_availability_master.erp_hotels_availability_master_id', 'left');
        $this->db->join('erp_room_execlusive_services', 'erp_room_execlusive_services.erp_hotels_availability_master_id = erp_hotels_availability_master.erp_hotels_availability_master_id', 'left');

        $this->db->where('(erp_hotels_availability_detail.erp_hotelroomsize_id = erp_hotelroomsizes.erp_hotelroomsize_id OR erp_hotels_availability_detail.erp_hotelroomsize_id IS NULL)');

        $this->db->group_by('erp_hotels_availability_detail.erp_hotels_availability_detail_id');
        $query = $this->db->get("erp_hotels_availability_master");

        //echo $this->db->last_query(); exit;

        return $query->result();
    }

    public function get_rooms_details() {

        if (session("uo_id")) {
            $uoid = session("uo_id");
            $whereinstr = "erp_hotels_availability_detail.erp_hotels_availability_detail_id in (select erp_hotels_availability_detail.erp_hotels_availability_detail_id
		FROM erp_hotels_availability_detail LEFT JOIN erp_hotels_availability_detail_uo_companies ON erp_hotels_availability_detail.erp_hotels_availability_detail_id = erp_hotels_availability_detail_uo_companies.erp_hotels_availability_detail_id
		WHERE erp_hotels_availability_detail_uo_companies.erp_hotels_availability_detail_id IS NULL OR
		( erp_hotels_availability_detail_uo_companies.safa_uo_id = $uoid and `status` = 1) AND NOT
		( erp_hotels_availability_detail_uo_companies.safa_uo_id = $uoid and `status` = 0)
		GROUP BY erp_hotels_availability_detail.erp_hotels_availability_detail_id)";
            $this->db->where($whereinstr);

            $whereexecinstr = "erp_room_execlusive_services.erp_room_exclusive_service_id in (select erp_room_execlusive_services.erp_room_exclusive_service_id
		FROM erp_room_execlusive_services 
		LEFT JOIN erp_room_execlusive_services_uo_companies ON erp_room_execlusive_services.erp_room_exclusive_service_id = erp_room_execlusive_services_uo_companies.erp_room_exclusive_service_id
		WHERE erp_room_execlusive_services_uo_companies.erp_room_exclusive_service_id IS NULL OR
		( erp_room_execlusive_services_uo_companies.safa_uo_id = $uoid and `status` = 1) AND NOT
		( erp_room_execlusive_services_uo_companies.safa_uo_id = $uoid and `status` = 0)
		GROUP BY erp_room_execlusive_services_uo_companies.erp_room_exclusive_service_id)";
            //$this->db->where($whereexecinstr);
        } elseif (session("hm_id")) {
            $hmid = session("hm_id");
            $whereinstr = "erp_room_execlusive_services.erp_room_exclusive_service_id in (select erp_room_execlusive_services.erp_room_exclusive_service_id
		FROM erp_room_execlusive_services
		LEFT JOIN erp_room_execlusive_services_uo_companies ON erp_room_execlusive_services.erp_room_exclusive_service_id = erp_room_execlusive_services_hm_companies.erp_room_exclusive_service_id
		WHERE erp_room_execlusive_services_hm_companies.erp_room_exclusive_service_id IS NULL OR
		( erp_room_execlusive_services_hm_companies.safa_hm_id = $hmid and `status` = 1) AND NOT
		( erp_room_execlusive_services_hm_companies.safa_hm_id = $hmid and `status` = 0)
		GROUP BY erp_room_execlusive_services.erp_room_exclusive_service_id)";
            $this->db->where($whereinstr);

            $whereexecinstr = "erp_hotels_availability_detail.erp_hotels_availability_detail_id in (select erp_hotels_availability_detail.erp_hotels_availability_detail_id
		FROM erp_hotels_availability_detail
		LEFT JOIN erp_hotels_availability_detail_hm_companies ON erp_hotels_availability_detail.erp_hotels_availability_detail_id = erp_hotels_availability_detail_hm_companies.erp_hotels_availability_detail_id
		WHERE erp_hotels_availability_detail_hm_companies.erp_hotels_availability_detail_id IS NULL OR
		( erp_hotels_availability_detail_hm_companies.safa_hm_id = $hmid and `status` = 1) AND NOT
		( erp_hotels_availability_detail_hm_companies.safa_hm_id = $hmid and `status` = 0)
		GROUP BY erp_hotels_availability_detail.erp_hotels_availability_detail_id)";
            //$this->db->where($whereexecinstr);            
        } elseif (session("ea_id")) {
            $eaid = session("ea_id");
            $eadata = $this->db->where('safa_ea_id', $eaid)->get('safa_eas')->row();
            $country_id = $eadata->erp_country_id;
            $whereinstr = "erp_hotels_availability_detail.erp_hotels_availability_detail_id in (select erp_hotels_availability_detail.erp_hotels_availability_detail_id
		FROM erp_hotels_availability_detail
		LEFT JOIN erp_hotels_availability_detail_ea_companies ON erp_hotels_availability_detail.erp_hotels_availability_detail_id = erp_hotels_availability_detail_ea_companies.erp_hotels_availability_detail_id
		LEFT JOIN erp_hotels_availability_detail_countries ON erp_hotels_availability_detail.erp_hotels_availability_detail_id = erp_hotels_availability_detail_countries.erp_hotels_availability_detail_id
		WHERE (
		erp_hotels_availability_detail_ea_companies.erp_hotels_availability_detail_id IS NULL OR 
		( erp_hotels_availability_detail_ea_companies.safa_ea_id = $eaid and erp_hotels_availability_detail_ea_companies.`status` = 1 ) AND NOT
		( erp_hotels_availability_detail_ea_companies.safa_ea_id = $eaid and erp_hotels_availability_detail_ea_companies.`status` = 0 ))AND (
		erp_hotels_availability_detail_countries.erp_hotels_availability_detail_id IS NULL OR
		( erp_hotels_availability_detail_countries.erp_country_id = $country_id and erp_hotels_availability_detail_countries.`status` = 1) AND NOT
		( erp_hotels_availability_detail_countries.erp_country_id = $country_id and erp_hotels_availability_detail_countries.`status` = 0)
		))";
            $this->db->where($whereinstr);

            $whereexecinstr = "erp_hotels_availability_detail.erp_hotels_availability_detail_id in (select erp_room_execlusive_services.erp_room_exclusive_service_id
		FROM erp_room_execlusive_services
		LEFT JOIN erp_room_execlusive_services_ea_companies ON erp_room_execlusive_services.erp_room_exclusive_service_id = erp_room_execlusive_services_ea_companies.erp_room_exclusive_service_id
		LEFT JOIN erp_room_execlusive_services_countries ON erp_room_execlusive_services.erp_room_exclusive_service_id = erp_room_execlusive_services_countries.erp_room_exclusive_service_id
		WHERE(
		erp_room_execlusive_services_ea_companies.erp_room_exclusive_service_id IS NULL OR 
		( erp_room_execlusive_services_ea_companies.safa_ea_id = $eaid and erp_room_execlusive_services_ea_companies.`status` = 1 ) AND NOT
		( erp_room_execlusive_services_ea_companies.safa_ea_id = $eaid and erp_room_execlusive_services_ea_companies.`status` = 0 ) )AND (
		erp_room_execlusive_services_countries.erp_room_exclusive_service_id IS NULL OR
		( erp_room_execlusive_services_countries.erp_country_id = $country_id and erp_room_execlusive_services_countries.`status` = 1) AND NOT
		( erp_room_execlusive_services_countries.erp_country_id = $country_id and erp_room_execlusive_services_countries.`status` = 0)
		))";
            //$this->db->where($whereexecinstr);
        }



        $this->db->distinct();
        $this->db->select("erp_hotels_availability_rooms.erp_hotels_availability_rooms_id, erp_cities.erp_city_id, erp_hotels.erp_hotel_id ,  erp_hotels.erp_hotellevel_id  ,erp_hotels.address_" . name(TRUE) . " , erp_hotels.distance ,erp_hotelroomsizes." . name() . " as roomtype, 
            (erp_hotels_availability_rooms.rooms_beds_count - erp_hotels_availability_rooms.closed_rooms_beds_count -  IFNULL((select sum(erp_hotels_reservation_orders_rooms.rooms_count) from erp_hotels_reservation_orders_rooms, erp_hotels_availability_detail, erp_hotels_reservation_orders where erp_hotels_reservation_orders_rooms.erp_hotels_availability_detail_id = erp_hotels_availability_detail.erp_hotels_availability_detail_id and erp_hotels_reservation_orders.erp_hotels_reservation_orders_id = erp_hotels_reservation_orders_rooms.erp_hotels_reservation_orders_id and erp_hotels_reservation_orders.order_status = 1),0 )) as available_count ,
        	erp_hotels." . name() . ",erp_cities." . name() . " as city,erp_hotellevels." . name() . " as hotellevel,erp_hotels_availability_master.erp_hotels_availability_master_id,erp_hotels_availability_detail.sale_price , erp_hotels_availability_detail.erp_hotels_availability_view_id
            , erp_hotels_availability_room_details.erp_hotels_availability_sub_room_id, erp_hotels_availability_room_details.number , erp_hotels_availability_room_details.from_date, erp_hotels_availability_room_details.to_date
            ", false);


        $this->db->where_in('erp_hotels_availability_detail.offer_status', '1');

//        if ($this->erp_company_type_id)
//            $this->db->where('erp_hotels_availability_master.erp_company_type_id', $this->erp_company_type_id);
//        if ($this->safa_company_id)
//            $this->db->where('erp_hotels_availability_master.safa_company_id', $this->safa_company_id);

        if ($this->main_date_from)
            $this->db->where('erp_hotels_availability_master.date_from <=', $this->main_date_from);

        if ($this->main_date_to)
            $this->db->where('erp_hotels_availability_master.date_to >=', $this->main_date_to);

        if ($this->city_id)
            $this->db->where('erp_hotels.erp_city_id', $this->city_id);

        if ($this->hotel_id)
            $this->db->where_in('erp_hotels_availability_master.erp_hotel_id', $this->hotel_id);


        if ($this->ha_type) {
            if ($this->ha_type == 1) {
                if (session("uo_id")) {
                    $this->db->where('erp_hotels_availability_master.erp_company_type_id', 2);
                    $this->db->where('erp_hotels_availability_master.safa_company_id', session("uo_id"));
                } else if (session("ea_id")) {
                    $this->db->where('erp_hotels_availability_master.erp_company_type_id', 3);
                    $this->db->where('erp_hotels_availability_master.safa_company_id', session("ea_id"));
                }
            } elseif ($this->ha_type == 2) {
                if (session("uo_id")) {
                    $this->db->where('(erp_hotels_availability_master.erp_company_type_id <> 2 && '
                            . 'erp_hotels_availability_master.safa_company_id <> ' . session("uo_id") . ')', NULL, FALSE);
                } else if (session("ea_id")) {
                    $this->db->where('(erp_hotels_availability_master.erp_company_type_id <>3 && '
                            . 'erp_hotels_availability_master.safa_company_id <> ' . session("ea_id") . ')', NULL, FALSe);
                }
            }
        }

        /*
          if ($this->rooms && $this->date_from && $this->date_to) {

          $this->db->where('(' . $this->date_from . ' BETWEEN erp_hotels_availability_room_details.from_date AND erp_hotels_availability_room_details.to_date) AND
          (' . $this->date_to . ' BETWEEN erp_hotels_availability_room_details.from_date AND erp_hotels_availability_room_details.to_date)'
          , false, false);
          }
         */

        if ($this->date_from) {
            $this->db->where("erp_hotels_availability_room_details.from_date <='" . $this->date_from . "'", false, false);
        }
        if ($this->date_to) {
            $this->db->where("erp_hotels_availability_room_details.to_date >='" . $this->date_to . "'", false, false);
        }

        //By Gouda
        $this->db->where("erp_hotels_availability_room_details.from_date IS NOT NULL", false, false);
        $this->db->where("erp_hotels_availability_room_details.to_date IS NOT NULL", false, false);
        $this->db->where("erp_hotels_availability_room_details.status = 1", false, false);


        if ($this->level)
            $this->db->where_in('erp_hotels.erp_hotellevel_id', $this->level);

        if ($this->nationalities)
            $this->db->where_in('erp_hotels_availability_nationalities.erp_nationality_id', $this->nationalities);

        if ($this->hotel_features)
            $this->db->where_in('erp_hotel_advantages_details.erp_hotel_advantages_id', $this->hotel_features);

        if ($this->inclusive_service)
            $this->db->where_in('erp_room_inclusive_services.erp_room_service_id', $this->inclusive_service);

//        if ($this->exclusive_service)
//        	$this->db->where_in('erp_room_exclusive_services.erp_room_service_id', $this->exclusive_service);



        if ($this->distance_from_haram)
            $this->db->where('erp_hotels.distance >=', $this->distance_from_haram);

        if ($this->distance_to_haram)
            $this->db->where('erp_hotels.distance <=', $this->distance_to_haram);
        /*
          if($this->housing_type_id)
          $this->db->where('erp_hotels_availability_detail.erp_housingtype_id', $this->housing_type_id);
         */

        if ($this->housing_type_id)
            $this->db->where('erp_hotels_availability_rooms.erp_housingtype_id', $this->housing_type_id);

        if ($this->price_from)
            $this->db->where('erp_hotels_availability_detail.sale_price >=', $this->price_from);

        if ($this->price_to)
            $this->db->where('erp_hotels_availability_detail.sale_price <=', $this->price_to);

//        if($this->nights)
//            $this->db->where('', $this->nights);

        if ($this->room_type_id)
            $this->db->where('erp_hotels_availability_rooms.erp_hotelroomsize_id', $this->room_type_id);


        if ($this->currency)
            $this->db->where('erp_hotels_availability_master.sale_currency_id', $this->currency);

        $this->db->join('erp_hotels', 'erp_hotels.erp_hotel_id = erp_hotels_availability_master.erp_hotel_id', 'left');
        $this->db->join('erp_cities', 'erp_cities.erp_city_id = erp_hotels.erp_city_id');

        $this->db->join('erp_hotels_availability_detail', 'erp_hotels_availability_detail.erp_hotels_availability_master_id = erp_hotels_availability_master.erp_hotels_availability_master_id', 'left');
        $this->db->join('erp_hotels_availability_rooms', 'erp_hotels_availability_rooms.erp_hotels_availability_master_id = erp_hotels_availability_master.erp_hotels_availability_master_id', 'left');

        $this->db->join('erp_hotelroomsizes', 'erp_hotels_availability_rooms.erp_hotelroomsize_id = erp_hotelroomsizes.erp_hotelroomsize_id', 'left');
        $this->db->join('erp_hotel_advantages_details', 'erp_hotel_advantages_details.erp_hotel_id = erp_hotels.erp_hotel_id', 'left');
        $this->db->join('erp_hotellevels', 'erp_hotellevels.erp_hotellevel_id = erp_hotels.erp_hotellevel_id', 'left');

        //$this->db->where('erp_hotels_availability_detail.erp_hotelroomsize_id = erp_hotelroomsizes.erp_hotelroomsize_id','left');

        //By Gouda
        $this->db->join('erp_hotels_availability_sub_rooms', 'erp_hotels_availability_sub_rooms.erp_hotels_availability_rooms_id = erp_hotels_availability_rooms.erp_hotels_availability_rooms_id', 'left');
        $this->db->join('erp_hotels_availability_room_details', 'erp_hotels_availability_room_details.erp_hotels_availability_sub_room_id = erp_hotels_availability_sub_rooms.erp_hotels_availability_sub_room_id', 'left');

        $this->db->join('erp_hotels_availability_nationalities', 'erp_hotels_availability_nationalities.erp_hotels_availability_master_id = erp_hotels_availability_master.erp_hotels_availability_master_id', 'left');
        $this->db->join('erp_room_inclusive_services', 'erp_room_inclusive_services.erp_hotels_availability_master_id = erp_hotels_availability_master.erp_hotels_availability_master_id', 'left');
        $this->db->join('erp_room_execlusive_services', 'erp_room_execlusive_services.erp_hotels_availability_master_id = erp_hotels_availability_master.erp_hotels_availability_master_id', 'left');


        $this->db->group_by('erp_hotels_availability_rooms.erp_hotels_availability_rooms_id, erp_hotels_availability_room_details.from_date, erp_hotels_availability_room_details.to_date');

        $query = $this->db->get("erp_hotels_availability_master");

        //echo $last_query= $this->db->last_query(); exit;  

        return $query->result();
    }

    public function get_availability_room_details($erp_hotels_availability_room_details_status) 
    {
        if (session("uo_id")) {
            $uoid = session("uo_id");
            $whereinstr = "erp_hotels_availability_detail.erp_hotels_availability_detail_id in (select erp_hotels_availability_detail.erp_hotels_availability_detail_id
		FROM erp_hotels_availability_detail LEFT JOIN erp_hotels_availability_detail_uo_companies ON erp_hotels_availability_detail.erp_hotels_availability_detail_id = erp_hotels_availability_detail_uo_companies.erp_hotels_availability_detail_id
		WHERE erp_hotels_availability_detail_uo_companies.erp_hotels_availability_detail_id IS NULL OR
		( erp_hotels_availability_detail_uo_companies.safa_uo_id = $uoid and `status` = 1) AND NOT
		( erp_hotels_availability_detail_uo_companies.safa_uo_id = $uoid and `status` = 0)
		GROUP BY erp_hotels_availability_detail.erp_hotels_availability_detail_id)";
            $this->db->where($whereinstr);

            $whereexecinstr = "erp_room_execlusive_services.erp_room_exclusive_service_id in (select erp_room_execlusive_services.erp_room_exclusive_service_id
		FROM erp_room_execlusive_services 
		LEFT JOIN erp_room_execlusive_services_uo_companies ON erp_room_execlusive_services.erp_room_exclusive_service_id = erp_room_execlusive_services_uo_companies.erp_room_exclusive_service_id
		WHERE erp_room_execlusive_services_uo_companies.erp_room_exclusive_service_id IS NULL OR
		( erp_room_execlusive_services_uo_companies.safa_uo_id = $uoid and `status` = 1) AND NOT
		( erp_room_execlusive_services_uo_companies.safa_uo_id = $uoid and `status` = 0)
		GROUP BY erp_room_execlusive_services_uo_companies.erp_room_exclusive_service_id)";
            //$this->db->where($whereexecinstr);
        } elseif (session("hm_id")) {
            $hmid = session("hm_id");
            $whereinstr = "erp_room_execlusive_services.erp_room_exclusive_service_id in (select erp_room_execlusive_services.erp_room_exclusive_service_id
		FROM erp_room_execlusive_services
		LEFT JOIN erp_room_execlusive_services_uo_companies ON erp_room_execlusive_services.erp_room_exclusive_service_id = erp_room_execlusive_services_hm_companies.erp_room_exclusive_service_id
		WHERE erp_room_execlusive_services_hm_companies.erp_room_exclusive_service_id IS NULL OR
		( erp_room_execlusive_services_hm_companies.safa_hm_id = $hmid and `status` = 1) AND NOT
		( erp_room_execlusive_services_hm_companies.safa_hm_id = $hmid and `status` = 0)
		GROUP BY erp_room_execlusive_services.erp_room_exclusive_service_id)";
            $this->db->where($whereinstr);

            $whereexecinstr = "erp_hotels_availability_detail.erp_hotels_availability_detail_id in (select erp_hotels_availability_detail.erp_hotels_availability_detail_id
		FROM erp_hotels_availability_detail
		LEFT JOIN erp_hotels_availability_detail_hm_companies ON erp_hotels_availability_detail.erp_hotels_availability_detail_id = erp_hotels_availability_detail_hm_companies.erp_hotels_availability_detail_id
		WHERE erp_hotels_availability_detail_hm_companies.erp_hotels_availability_detail_id IS NULL OR
		( erp_hotels_availability_detail_hm_companies.safa_hm_id = $hmid and `status` = 1) AND NOT
		( erp_hotels_availability_detail_hm_companies.safa_hm_id = $hmid and `status` = 0)
		GROUP BY erp_hotels_availability_detail.erp_hotels_availability_detail_id)";
            //$this->db->where($whereexecinstr);            
        } elseif (session("ea_id")) {
            $eaid = session("ea_id");
            $eadata = $this->db->where('safa_ea_id', $eaid)->get('safa_eas')->row();
            $country_id = $eadata->erp_country_id;
            $whereinstr = "erp_hotels_availability_detail.erp_hotels_availability_detail_id in (select erp_hotels_availability_detail.erp_hotels_availability_detail_id
		FROM erp_hotels_availability_detail
		LEFT JOIN erp_hotels_availability_detail_ea_companies ON erp_hotels_availability_detail.erp_hotels_availability_detail_id = erp_hotels_availability_detail_ea_companies.erp_hotels_availability_detail_id
		LEFT JOIN erp_hotels_availability_detail_countries ON erp_hotels_availability_detail.erp_hotels_availability_detail_id = erp_hotels_availability_detail_countries.erp_hotels_availability_detail_id
		WHERE (
		erp_hotels_availability_detail_ea_companies.erp_hotels_availability_detail_id IS NULL OR 
		( erp_hotels_availability_detail_ea_companies.safa_ea_id = $eaid and erp_hotels_availability_detail_ea_companies.`status` = 1 ) AND NOT
		( erp_hotels_availability_detail_ea_companies.safa_ea_id = $eaid and erp_hotels_availability_detail_ea_companies.`status` = 0 ))AND (
		erp_hotels_availability_detail_countries.erp_hotels_availability_detail_id IS NULL OR
		( erp_hotels_availability_detail_countries.erp_country_id = $country_id and erp_hotels_availability_detail_countries.`status` = 1) AND NOT
		( erp_hotels_availability_detail_countries.erp_country_id = $country_id and erp_hotels_availability_detail_countries.`status` = 0)
		))";
            $this->db->where($whereinstr);

            $whereexecinstr = "erp_hotels_availability_detail.erp_hotels_availability_detail_id in (select erp_room_execlusive_services.erp_room_exclusive_service_id
		FROM erp_room_execlusive_services
		LEFT JOIN erp_room_execlusive_services_ea_companies ON erp_room_execlusive_services.erp_room_exclusive_service_id = erp_room_execlusive_services_ea_companies.erp_room_exclusive_service_id
		LEFT JOIN erp_room_execlusive_services_countries ON erp_room_execlusive_services.erp_room_exclusive_service_id = erp_room_execlusive_services_countries.erp_room_exclusive_service_id
		WHERE(
		erp_room_execlusive_services_ea_companies.erp_room_exclusive_service_id IS NULL OR 
		( erp_room_execlusive_services_ea_companies.safa_ea_id = $eaid and erp_room_execlusive_services_ea_companies.`status` = 1 ) AND NOT
		( erp_room_execlusive_services_ea_companies.safa_ea_id = $eaid and erp_room_execlusive_services_ea_companies.`status` = 0 ) )AND (
		erp_room_execlusive_services_countries.erp_room_exclusive_service_id IS NULL OR
		( erp_room_execlusive_services_countries.erp_country_id = $country_id and erp_room_execlusive_services_countries.`status` = 1) AND NOT
		( erp_room_execlusive_services_countries.erp_country_id = $country_id and erp_room_execlusive_services_countries.`status` = 0)
		))";
            //$this->db->where($whereexecinstr);
        }



        $this->db->distinct();
        $this->db->select("erp_hotels_availability_room_details.erp_hotels_availability_room_detail_id , erp_hotels_availability_room_details.number, erp_hotels_availability_room_details.erp_hotels_availability_sub_room_id", false);

        $this->db->where_in('erp_hotels_availability_detail.offer_status', '1');
        $this->db->where_in('erp_hotels_availability_room_details.status', $erp_hotels_availability_room_details_status);

//        if ($this->erp_company_type_id)
//            $this->db->where('erp_hotels_availability_master.erp_company_type_id', $this->erp_company_type_id);
//        if ($this->safa_company_id)
//            $this->db->where('erp_hotels_availability_master.safa_company_id', $this->safa_company_id);

        if ($this->main_date_from)
            $this->db->where('erp_hotels_availability_master.date_from <=', $this->main_date_from);

        if ($this->main_date_to)
            $this->db->where('erp_hotels_availability_master.date_to >=', $this->main_date_to);

        if ($this->city_id)
            $this->db->where('erp_hotels.erp_city_id', $this->city_id);

        if ($this->hotel_id)
            $this->db->where_in('erp_hotels_availability_master.erp_hotel_id', $this->hotel_id);


        if ($this->ha_type) {
            if ($this->ha_type == 1) {
                if (session("uo_id")) {
                    $this->db->where('erp_hotels_availability_master.erp_company_type_id', 2);
                    $this->db->where('erp_hotels_availability_master.safa_company_id', session("uo_id"));
                } else if (session("ea_id")) {
                    $this->db->where('erp_hotels_availability_master.erp_company_type_id', 3);
                    $this->db->where('erp_hotels_availability_master.safa_company_id', session("ea_id"));
                }
            } elseif ($this->ha_type == 2) {
                if (session("uo_id")) {
                    $this->db->where('(erp_hotels_availability_master.erp_company_type_id <> 2 && '
                            . 'erp_hotels_availability_master.safa_company_id <> ' . session("uo_id") . ')', NULL, FALSE);
                } else if (session("ea_id")) {
                    $this->db->where('(erp_hotels_availability_master.erp_company_type_id <>3 && '
                            . 'erp_hotels_availability_master.safa_company_id <> ' . session("ea_id") . ')', NULL, FALSe);
                }
            }
        }

        /*
          if ($this->rooms && $this->date_from && $this->date_to) {

          $this->db->where('(' . $this->date_from . ' BETWEEN erp_hotels_availability_room_details.from_date AND erp_hotels_availability_room_details.to_date) AND
          (' . $this->date_to . ' BETWEEN erp_hotels_availability_room_details.from_date AND erp_hotels_availability_room_details.to_date)'
          , false, false);
          }
         */

        if ($this->date_from) {
            $this->db->where("erp_hotels_availability_room_details.from_date <='" . $this->date_from . "'", false, false);
        }
        if ($this->date_to) {
            $this->db->where("erp_hotels_availability_room_details.to_date >='" . $this->date_to . "'", false, false);
        }

        //By Gouda
        $this->db->where("erp_hotels_availability_room_details.from_date IS NOT NULL", false, false);
        $this->db->where("erp_hotels_availability_room_details.to_date IS NOT NULL", false, false);
        

        if ($this->level)
            $this->db->where_in('erp_hotels.erp_hotellevel_id', $this->level);

        if ($this->nationalities)
            $this->db->where_in('erp_hotels_availability_nationalities.erp_nationality_id', $this->nationalities);

        if ($this->hotel_features)
            $this->db->where_in('erp_hotel_advantages_details.erp_hotel_advantages_id', $this->hotel_features);

        if ($this->inclusive_service)
            $this->db->where_in('erp_room_inclusive_services.erp_room_service_id', $this->inclusive_service);

//        if ($this->exclusive_service)
//        	$this->db->where_in('erp_room_exclusive_services.erp_room_service_id', $this->exclusive_service);



        if ($this->distance_from_haram)
            $this->db->where('erp_hotels.distance >=', $this->distance_from_haram);

        if ($this->distance_to_haram)
            $this->db->where('erp_hotels.distance <=', $this->distance_to_haram);
        /*
          if($this->housing_type_id)
          $this->db->where('erp_hotels_availability_detail.erp_housingtype_id', $this->housing_type_id);
         */

        if ($this->housing_type_id)
            $this->db->where('erp_hotels_availability_rooms.erp_housingtype_id', $this->housing_type_id);

        if ($this->price_from)
            $this->db->where('erp_hotels_availability_detail.sale_price >=', $this->price_from);

        if ($this->price_to)
            $this->db->where('erp_hotels_availability_detail.sale_price <=', $this->price_to);

//        if($this->nights)
//            $this->db->where('', $this->nights);

    	if ($this->room_type_id) {
            $roomtypestring = "(erp_hotels_availability_rooms.erp_hotelroomsize_id = '$this->room_type_id' ||(";
            $roomtypestring .= "erp_hotels_availability_rooms.erp_hotelroomsize_id is NULL && erp_hotels_availability_rooms.max_beds_count >= '$this->room_type_id' ))";
            $this->db->where($roomtypestring);
        }

        if ($this->currency)
            $this->db->where('erp_hotels_availability_master.sale_currency_id', $this->currency);

        $this->db->join('erp_hotels', 'erp_hotels.erp_hotel_id = erp_hotels_availability_master.erp_hotel_id', 'left');
        $this->db->join('erp_cities', 'erp_cities.erp_city_id = erp_hotels.erp_city_id');

        $this->db->join('erp_hotels_availability_detail', 'erp_hotels_availability_detail.erp_hotels_availability_master_id = erp_hotels_availability_master.erp_hotels_availability_master_id', 'left');
        $this->db->join('erp_hotels_availability_rooms', 'erp_hotels_availability_rooms.erp_hotels_availability_master_id = erp_hotels_availability_master.erp_hotels_availability_master_id', 'left');

        $this->db->join('erp_hotelroomsizes', 'erp_hotels_availability_rooms.erp_hotelroomsize_id = erp_hotelroomsizes.erp_hotelroomsize_id', 'left');
        $this->db->join('erp_hotel_advantages_details', 'erp_hotel_advantages_details.erp_hotel_id = erp_hotels.erp_hotel_id', 'left');
        $this->db->join('erp_hotellevels', 'erp_hotellevels.erp_hotellevel_id = erp_hotels.erp_hotellevel_id', 'left');

        //$this->db->where('erp_hotels_availability_detail.erp_hotelroomsize_id = erp_hotelroomsizes.erp_hotelroomsize_id');

        //By Gouda
        $this->db->join('erp_hotels_availability_sub_rooms', 'erp_hotels_availability_sub_rooms.erp_hotels_availability_rooms_id = erp_hotels_availability_rooms.erp_hotels_availability_rooms_id', 'left');
        $this->db->join('erp_hotels_availability_room_details', 'erp_hotels_availability_room_details.erp_hotels_availability_sub_room_id = erp_hotels_availability_sub_rooms.erp_hotels_availability_sub_room_id', 'left');

        $this->db->join('erp_hotels_availability_nationalities', 'erp_hotels_availability_nationalities.erp_hotels_availability_master_id = erp_hotels_availability_master.erp_hotels_availability_master_id', 'left');
        $this->db->join('erp_room_inclusive_services', 'erp_room_inclusive_services.erp_hotels_availability_master_id = erp_hotels_availability_master.erp_hotels_availability_master_id', 'left');
        $this->db->join('erp_room_execlusive_services', 'erp_room_execlusive_services.erp_hotels_availability_master_id = erp_hotels_availability_master.erp_hotels_availability_master_id', 'left');


        //$this->db->group_by('erp_hotels_availability_rooms.erp_hotels_availability_rooms_id, erp_hotels_availability_room_details.from_date, erp_hotels_availability_room_details.to_date');

        $query = $this->db->get("erp_hotels_availability_master");

        //echo $last_query= $this->db->last_query(); exit;  

        return $query->result();
    }
    
}
