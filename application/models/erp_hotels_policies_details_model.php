<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Erp_hotels_policies_details_model extends CI_Model {

    public $erp_hotels_policies_details_id = FALSE;
    public $erp_hotels_id = FALSE;
    public $erp_hotels_policies_id = FALSE;
    public $policy_description = FALSE;
    public $custom_select = FALSE;
    public $limit = FALSE;
    public $join = FALSE;
    public $offset = FALSE;
    public $order_by = FALSE;

    function __construct() {
        parent::__construct();
    }

    function get($rows_no = FALSE) {

        if ($this->custom_select !== FALSE) {
            $this->db->select('erp_hotels_policies_details_id');
            $this->db->select($this->custom_select);
        }

        if ($this->erp_hotels_policies_details_id !== FALSE)
            $this->db->where('erp_hotels_policies_details.erp_hotels_policies_details_id', $this->erp_hotels_policies_details_id);

        if ($this->erp_hotels_id !== FALSE)
            $this->db->where('erp_hotels_policies_details.erp_hotels_id', $this->erp_hotels_id);

        if ($this->erp_hotels_policies_id !== FALSE)
            $this->db->where('erp_hotels_policies_details.erp_hotels_policies_id', $this->erp_hotels_policies_id);

        if ($this->policy_description !== FALSE)
            $this->db->where('erp_hotels_policies_details.policy_description', $this->policy_description);

        if ($this->join !== FALSE) {
            $this->db->select('erp_hotels_policies.' . name() . ',erp_hotels_policies_details.policy_description');
            $this->db->join('erp_hotels_policies', 'erp_hotels_policies.erp_hotels_policies_id = erp_hotels_policies_details.erp_hotels_policies_id');
        }

        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('erp_hotels_policies_details');
        if ($rows_no)
            return $query->num_rows();

        if ($this->erp_hotels_policies_details_id)
            return $query->row();
        else
            return $query->result();
    }

    function save() {
        if ($this->erp_hotels_policies_details_id !== FALSE)
            $this->db->set('erp_hotels_policies_details.erp_hotels_policies_details_id', $this->erp_hotels_policies_details_id);

        if ($this->erp_hotels_id !== FALSE)
            $this->db->set('erp_hotels_policies_details.erp_hotels_id', $this->erp_hotels_id);

        if ($this->erp_hotels_policies_id !== FALSE)
            $this->db->set('erp_hotels_policies_details.erp_hotels_policies_id', $this->erp_hotels_policies_id);

        if ($this->policy_description !== FALSE)
            $this->db->set('erp_hotels_policies_details.policy_description', $this->policy_description);



        if ($this->erp_hotels_policies_details_id) {
            $this->db->where('erp_hotels_policies_details.erp_hotels_policies_details_id', $this->erp_hotels_policies_details_id)->update('erp_hotels_policies_details');
        } else {
            $this->db->insert('erp_hotels_policies_details');
            return $this->db->insert_id();
        }
    }

    function delete() {
        if ($this->erp_hotels_policies_details_id !== FALSE)
            $this->db->where('erp_hotels_policies_details.erp_hotels_policies_details_id', $this->erp_hotels_policies_details_id);

        if ($this->erp_hotels_id !== FALSE)
            $this->db->where('erp_hotels_policies_details.erp_hotels_id', $this->erp_hotels_id);

        if ($this->erp_hotels_policies_id !== FALSE)
            $this->db->where('erp_hotels_policies_details.erp_hotels_policies_id', $this->erp_hotels_policies_id);

        if ($this->policy_description !== FALSE)
            $this->db->where('erp_hotels_policies_details.policy_description', $this->policy_description);



        $this->db->delete('erp_hotels_policies_details');
        return $this->db->affected_rows();
    }

    function search($rows_no = FALSE) {
        if ($this->custom_select !== FALSE) {
            $this->db->select('erp_hotels_policies_details_id');
            $this->db->select($this->custom_select);
        }

        if ($this->erp_hotels_policies_details_id !== FALSE)
            $this->db->where('erp_hotels_policies_details.erp_hotels_policies_details_id', $this->erp_hotels_policies_details_id);

        if ($this->erp_hotels_id !== FALSE)
            $this->db->where('erp_hotels_policies_details.erp_hotels_id', $this->erp_hotels_id);

        if ($this->erp_hotels_policies_id !== FALSE)
            $this->db->where('erp_hotels_policies_details.erp_hotels_policies_id', $this->erp_hotels_policies_id);

        if ($this->policy_description !== FALSE)
            $this->db->where('erp_hotels_policies_details.policy_description', $this->policy_description);


        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('erp_hotels_policies_details');
        if ($rows_no)
            return $query->num_rows();

        return $query->result();
    }

}

/* End of file Erp_hotels_policies_details_model.php */
/* Location: ./application/models/Erp_hotels_policies_details_model.php */