<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Safa_package_execlusive_nights_model extends CI_Model {

    public $safa_package_execlusive_night_id = FALSE;
    public $safa_package_id = FALSE;
    public $hotel_id = FALSE;
    public $erp_meal_id = FALSE;
    public $custom_select = FALSE;
    public $limit = FALSE;
    public $offset = FALSE;
    public $order_by = FALSE;

    function __construct() {
        parent::__construct();
    }

    function get($rows_no = FALSE) {

        if ($this->custom_select !== FALSE) {
            $this->db->select('safa_package_execlusive_night_id');
            $this->db->select($this->custom_select);
        }

        if ($this->safa_package_execlusive_night_id !== FALSE)
            $this->db->where('safa_package_execlusive_nights.safa_package_execlusive_night_id', $this->safa_package_execlusive_night_id);

        if ($this->safa_package_id !== FALSE)
            $this->db->where('safa_package_execlusive_nights.safa_package_id', $this->safa_package_id);

        if ($this->hotel_id !== FALSE)
            $this->db->where('safa_package_execlusive_nights.hotel_id', $this->hotel_id);

        if ($this->erp_meal_id !== FALSE)
            $this->db->where('safa_package_execlusive_nights.erp_meal_id', $this->erp_meal_id);


        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('safa_package_execlusive_nights');
        if ($rows_no)
            return $query->num_rows();

        if ($this->safa_package_execlusive_night_id)
            return $query->row();
        else
            return $query->result();
    }

    function save() {
        if ($this->safa_package_execlusive_night_id !== FALSE)
            $this->db->set('safa_package_execlusive_nights.safa_package_execlusive_night_id', $this->safa_package_execlusive_night_id);

        if ($this->safa_package_id !== FALSE)
            $this->db->set('safa_package_execlusive_nights.safa_package_id', $this->safa_package_id);

        if ($this->hotel_id !== FALSE)
            $this->db->set('safa_package_execlusive_nights.hotel_id', $this->hotel_id);

        if ($this->erp_meal_id !== FALSE)
            $this->db->set('safa_package_execlusive_nights.erp_meal_id', $this->erp_meal_id);



        if ($this->safa_package_execlusive_night_id) {
            $this->db->where('safa_package_execlusive_nights.safa_package_execlusive_night_id', $this->safa_package_execlusive_night_id)->update('safa_package_execlusive_nights');
        } else {
            $this->db->insert('safa_package_execlusive_nights');
            return $this->db->insert_id();
        }
    }

    function delete() {
        if ($this->safa_package_execlusive_night_id !== FALSE)
            $this->db->where('safa_package_execlusive_nights.safa_package_execlusive_night_id', $this->safa_package_execlusive_night_id);

        if ($this->safa_package_id !== FALSE)
            $this->db->where('safa_package_execlusive_nights.safa_package_id', $this->safa_package_id);

        if ($this->hotel_id !== FALSE)
            $this->db->where('safa_package_execlusive_nights.hotel_id', $this->hotel_id);

        if ($this->erp_meal_id !== FALSE)
            $this->db->where('safa_package_execlusive_nights.erp_meal_id', $this->erp_meal_id);


        $this->db->delete('safa_package_execlusive_nights');
        return $this->db->affected_rows();
    }

    function search($rows_no = FALSE) {
        if ($this->custom_select !== FALSE) {
            $this->db->select('safa_package_execlusive_night_id');
            $this->db->select($this->custom_select);
        }

        if ($this->safa_package_execlusive_night_id !== FALSE)
            $this->db->where('safa_package_execlusive_nights.safa_package_execlusive_night_id', $this->safa_package_execlusive_night_id);

        if ($this->safa_package_id !== FALSE)
            $this->db->where('safa_package_execlusive_nights.safa_package_id', $this->safa_package_id);

        if ($this->hotel_id !== FALSE)
            $this->db->where('safa_package_execlusive_nights.hotel_id', $this->hotel_id);

        if ($this->erp_meal_id !== FALSE)
            $this->db->where('safa_package_execlusive_nights.erp_meal_id', $this->erp_meal_id);


        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('safa_package_execlusive_nights');
        if ($rows_no)
            return $query->num_rows();

        return $query->result();
    }

}

/* End of file uo_package_tourismplaces_model.php */
/* Location: ./application/models/uo_package_tourismplaces_model.php */