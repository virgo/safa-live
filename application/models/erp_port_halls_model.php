<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Erp_port_halls_model extends CI_Model {

    public $erp_port_hall_id = FALSE;
    public $erp_port_id = FALSE;
    public $name_ar = FALSE;
    public $name_la = FALSE;
    public $custom_select = FALSE;
    public $limit = FALSE;
    public $offset = FALSE;
    public $order_by = FALSE;

    function __construct() {
        parent::__construct();
    }

    function get($rows_no = FALSE) {

        if ($this->custom_select !== FALSE) {
            $this->db->select('erp_port_hall_id');
            $this->db->select($this->custom_select);
        }

        if ($this->erp_port_hall_id !== FALSE)
            $this->db->where('erp_port_halls.erp_port_hall_id', $this->erp_port_hall_id);

        if ($this->erp_port_id !== FALSE)
            $this->db->where('erp_port_halls.erp_port_id', $this->erp_port_id);

        if ($this->name_ar !== FALSE)
            $this->db->where('erp_port_halls.name_ar', $this->name_ar);

        if ($this->name_la !== FALSE)
            $this->db->where('erp_port_halls.name_la', $this->name_la);


        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('erp_port_halls');
        if ($rows_no)
            return $query->num_rows();

        if ($this->erp_port_hall_id)
            return $query->row();
        else
            return $query->result();
    }

    function save() {
        if ($this->erp_port_hall_id !== FALSE)
            $this->db->set('erp_port_halls.erp_port_hall_id', $this->erp_port_hall_id);

        if ($this->erp_port_id !== FALSE)
            $this->db->set('erp_port_halls.erp_port_id', $this->erp_port_id);

        if ($this->name_ar !== FALSE)
            $this->db->set('erp_port_halls.name_ar', $this->name_ar);

        if ($this->name_la !== FALSE)
            $this->db->set('erp_port_halls.name_la', $this->name_la);



        if ($this->erp_port_hall_id) {
            $this->db->where('erp_port_halls.erp_port_hall_id', $this->erp_port_hall_id)->update('erp_port_halls');
        } else {
            $this->db->insert('erp_port_halls');
            return $this->db->insert_id();
        }
    }

    function delete() {
        if ($this->erp_port_hall_id !== FALSE)
            $this->db->where('erp_port_halls.erp_port_hall_id', $this->erp_port_hall_id);

        if ($this->erp_port_id !== FALSE)
            $this->db->where('erp_port_halls.erp_port_id', $this->erp_port_id);

        if ($this->name_ar !== FALSE)
            $this->db->where('erp_port_halls.name_ar', $this->name_ar);

        if ($this->name_la !== FALSE)
            $this->db->where('erp_port_halls.name_la', $this->name_la);



        $this->db->delete('erp_port_halls');
        return $this->db->affected_rows();
    }

    function search($rows_no = FALSE) {
        if ($this->custom_select !== FALSE) {
            $this->db->select('erp_port_hall_id');
            $this->db->select($this->custom_select);
        }

        if ($this->erp_port_hall_id !== FALSE)
            $this->db->where('erp_port_halls.erp_port_hall_id', $this->erp_port_hall_id);

        if ($this->erp_port_id !== FALSE)
            $this->db->where('erp_port_halls.erp_port_id', $this->erp_port_id);

        if ($this->name_ar !== FALSE)
            $this->db->where('erp_port_halls.name_ar', $this->name_ar);

        if ($this->name_la !== FALSE)
            $this->db->where('erp_port_halls.name_la', $this->name_la);


        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('erp_port_halls');
        if ($rows_no)
            return $query->num_rows();

        return $query->result();
    }

}

/* End of file erp_port_halls_model.php */
/* Location: ./application/models/erp_port_halls_model.php */