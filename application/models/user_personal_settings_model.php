<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class User_personal_settings_model extends CI_Model {

    public $erp_admin_id = FALSE;
    public $username = FALSE;
    public $password = FALSe;
    public $reset_password = FALSE;
    public $timestamp = FALSE;
    public $limit = FALSE;
    public $offset = FALSE;

    function __construct() {
        parent::__construct();
    }

    function get($rows_no = FALSE) {

        if ($this->erp_admin_id !== FALSE)
            $this->db->where('erp_admins.erp_admin_id', $this->erp_admin_id);

        if ($this->username !== FALSE)
            $this->db->where('erp_admins.username', $this->username);

        if ($this->password !== FALSE)
            $this->db->where('erp_admins.password', $this->password);

        if ($this->reset_password)
            $this->db->where('erp_admins.reset_password', $this->reset_password);


        if ($this->timestamp)
            $this->db->where('erp_admins.timestamp', $this->timestamp);


        $query = $this->db->get('erp_admins', $this->limit, $this->offset);
        if ($rows_no)
            return $query->num_rows();

        if ($this->erp_admin_id)
            return $query->row();
        else
            return $query->result();
    }

    function save() {

        if ($this->erp_admin_id !== FALSE)
            $this->db->set('erp_admins.erp_admin_id', $this->erp_admin_id);

        if ($this->username !== FALSE)
            $this->db->set('erp_admins.username', $this->username);

        if ($this->password !== FALSE)
            $this->db->set('erp_admins.password', $this->password);

        if ($this->reset_password)
            $this->db->set('erp_admins.reset_password', $this->reset_password);

        if ($this->timestamp)
            $this->db->set('erp_admins.timestamp', $this->timestamp);

        if ($this->erp_admin_id) {
            $this->db->where('erp_admin_id', $this->erp_admin_id)->update('erp_admins');
            return $this->erp_admin_id;
        } else {
            $this->db->insert('erp_admins');
            return $this->db->insert_id();
        }
    }

    function delete() {

        if ($this->erp_admin_id !== FALSE)
            $this->db->where('erp_admins.erp_admin_id', $this->erp_admin_id);

        if ($this->username !== FALSE)
            $this->db->where('erp_admins.username', $this->username);

        if ($this->password !== FALSE)
            $this->db->where('erp_admins.password', $this->password);

        if ($this->reset_password)
            $this->db->where('erp_admins.reset_password', $this->reset_password);


        $this->db->delete('erp_admins');
        return $this->db->affected_rows();
    }

}

/* End of file user_personal_settings.php */
/* Location: ./application/models/user_personal_seetings_model.php */