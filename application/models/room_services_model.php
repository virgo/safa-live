<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Room_services_model extends CI_Model {

    public $erp_room_service_id = FALSE;
    public $name_ar = FALSE;
    public $name_la = FALSE;
    public $custom_select = FALSE;
    public $limit = FALSE;
    public $offset = FALSE;
    public $order_by = FALSE;

    function __construct() {
        parent::__construct();
    }

    function get($rows_no = FALSE) {

        if ($this->custom_select !== FALSE) {
            $this->db->select('erp_room_service_id');
            $this->db->select($this->custom_select);
        }

        if ($this->erp_room_service_id !== FALSE)
            $this->db->where('erp_room_services.erp_room_service_id', $this->erp_room_service_id);

        if ($this->name_ar !== FALSE)
            $this->db->where('erp_room_services.name_ar', $this->name_ar);

        if ($this->name_la !== FALSE)
            $this->db->where('erp_room_services.name_la', $this->name_la);


        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('erp_room_services');
        if ($rows_no)
            return $query->num_rows();

        if ($this->erp_room_service_id)
            return $query->row();
        else
            return $query->result();
    }

    function save() {
        if ($this->erp_room_service_id !== FALSE)
            $this->db->set('erp_room_services.erp_room_service_id', $this->erp_room_service_id);

        if ($this->name_ar !== FALSE)
            $this->db->set('erp_room_services.name_ar', $this->name_ar);

        if ($this->name_la !== FALSE)
            $this->db->set('erp_room_services.name_la', $this->name_la);



        if ($this->erp_room_service_id) {
            $this->db->where('erp_room_services.erp_room_service_id', $this->erp_room_service_id)->update('erp_room_services');
        } else {
            $this->db->insert('erp_room_services');
            return $this->db->insert_id();
        }
    }

    function delete() {
        if ($this->erp_room_service_id !== FALSE)
            $this->db->where('erp_room_services.erp_room_service_id', $this->erp_room_service_id);

        if ($this->name_ar !== FALSE)
            $this->db->where('erp_room_services.name_ar', $this->name_ar);

        if ($this->name_la !== FALSE)
            $this->db->where('erp_room_services.name_la', $this->name_la);



        $this->db->delete('erp_room_services');
        return $this->db->affected_rows();
    }

    function search($rows_no = FALSE) {
        if ($this->custom_select !== FALSE) {
            $this->db->select('erp_room_service_id');
            $this->db->select($this->custom_select);
        }

        if ($this->erp_room_service_id !== FALSE)
            $this->db->where('erp_room_services.erp_room_service_id', $this->erp_room_service_id);

        if ($this->name_ar !== FALSE)
            $this->db->where('erp_room_services.name_ar', $this->name_ar);

        if ($this->name_la !== FALSE)
            $this->db->where('erp_room_services.name_la', $this->name_la);


        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('erp_room_services');
        if ($rows_no)
            return $query->num_rows();

        return $query->result();
    }

}

/* End of file room_services_model.php */
/* Location: ./application/models/room_services_model.php */