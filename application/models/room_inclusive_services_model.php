<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Room_inclusive_services_model extends CI_Model {

    public $erp_room_inclusive_service_id = FALSE;
    public $erp_room_service_id = FALSE;
    public $erp_hotels_availability_master_id = FALSE;
    public $custom_select = FALSE;
    public $limit = FALSE;
    public $offset = FALSE;
    public $order_by = FALSE;

    function __construct() {
        parent::__construct();
    }

    function get($rows_no = FALSE) {

        if ($this->custom_select !== FALSE) {
            $this->db->select('erp_room_inclusive_service_id');
            $this->db->select($this->custom_select);
        }

        if ($this->erp_room_inclusive_service_id !== FALSE)
            $this->db->where('erp_room_inclusive_services.erp_room_inclusive_service_id', $this->erp_room_inclusive_service_id);

        if ($this->erp_room_service_id !== FALSE)
            $this->db->where('erp_room_inclusive_services.erp_room_service_id', $this->erp_room_service_id);

        if ($this->erp_hotels_availability_master_id !== FALSE)
            $this->db->where('erp_room_inclusive_services.erp_hotels_availability_master_id', $this->erp_hotels_availability_master_id);


        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('erp_room_inclusive_services');
        if ($rows_no)
            return $query->num_rows();

        if ($this->erp_room_inclusive_service_id)
            return $query->row();
        else
            return $query->result();
    }

    function save() {
        if ($this->erp_room_inclusive_service_id !== FALSE)
            $this->db->set('erp_room_inclusive_services.erp_room_inclusive_service_id', $this->erp_room_inclusive_service_id);

        if ($this->erp_room_service_id !== FALSE)
            $this->db->set('erp_room_inclusive_services.erp_room_service_id', $this->erp_room_service_id);

        if ($this->erp_hotels_availability_master_id !== FALSE)
            $this->db->set('erp_room_inclusive_services.erp_hotels_availability_master_id', $this->erp_hotels_availability_master_id);



        if ($this->erp_room_inclusive_service_id) {
            $this->db->where('erp_room_inclusive_services.erp_room_inclusive_service_id', $this->erp_room_inclusive_service_id)->update('erp_room_inclusive_services');
        } else {
            $this->db->insert('erp_room_inclusive_services');
            return $this->db->insert_id();
        }
    }

    function delete() {
        if ($this->erp_room_inclusive_service_id !== FALSE)
            $this->db->where('erp_room_inclusive_services.erp_room_inclusive_service_id', $this->erp_room_inclusive_service_id);

        if ($this->erp_room_service_id !== FALSE)
            $this->db->where('erp_room_inclusive_services.erp_room_service_id', $this->erp_room_service_id);

        if ($this->erp_hotels_availability_master_id !== FALSE)
            $this->db->where('erp_room_inclusive_services.erp_hotels_availability_master_id', $this->erp_hotels_availability_master_id);



        $this->db->delete('erp_room_inclusive_services');
        return $this->db->affected_rows();
    }

    function search($rows_no = FALSE) {
        if ($this->custom_select !== FALSE) {
            $this->db->select('erp_room_inclusive_service_id');
            $this->db->select($this->custom_select);
        }

        if ($this->erp_room_inclusive_service_id !== FALSE)
            $this->db->where('erp_room_inclusive_services.erp_room_inclusive_service_id', $this->erp_room_inclusive_service_id);

        if ($this->erp_room_service_id !== FALSE)
            $this->db->where('erp_room_inclusive_services.erp_room_service_id', $this->erp_room_service_id);

        if ($this->erp_hotels_availability_master_id !== FALSE)
            $this->db->where('erp_room_inclusive_services.erp_hotels_availability_master_id', $this->erp_hotels_availability_master_id);


        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('erp_room_inclusive_services');
        if ($rows_no)
            return $query->num_rows();

        return $query->result();
    }

}

/* End of file room_inclusive_services_model.php */
/* Location: ./application/models/room_inclusive_services_model.php */