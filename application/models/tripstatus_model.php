<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Tripstatus_model extends CI_Model {

    public $safa_tripstatus_id = FALSE;
    public $name_ar = FALSE;
    public $description_ar = FALSE;
    public $name_la = FALSE;
    public $description_la = FALSE;
    public $code = FALSE;
    public $custom_select = FALSE;
    public $limit = FALSE;
    public $offset = FALSE;
    public $order_by = FALSE;
    //By Gouda, For custom condition
    public $where = FALSE;

    function __construct() {
        parent::__construct();
    }

    function get($rows_no = FALSE) {

        if ($this->custom_select !== FALSE) {
            $this->db->select('safa_tripstatus_id');
            $this->db->select($this->custom_select);
        }

        if ($this->safa_tripstatus_id !== FALSE)
            $this->db->where('safa_tripstatus.safa_tripstatus_id', $this->safa_tripstatus_id);

        if ($this->name_ar !== FALSE)
            $this->db->where('safa_tripstatus.name_ar', $this->name_ar);

        if ($this->description_ar !== FALSE)
            $this->db->where('safa_tripstatus.description_ar', $this->description_ar);

        if ($this->name_la !== FALSE)
            $this->db->where('safa_tripstatus.name_la', $this->name_la);

        if ($this->description_la !== FALSE)
            $this->db->where('safa_tripstatus.description_la', $this->description_la);

        if ($this->code !== FALSE)
            $this->db->where('safa_tripstatus.code', $this->code);

        if ($this->where !== false) {
            $this->db->where($this->where);
        }

        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('safa_tripstatus');
        if ($rows_no)
            return $query->num_rows();

        if ($this->safa_tripstatus_id)
            return $query->row();
        else
            return $query->result();
    }

    function save() {
        if ($this->safa_tripstatus_id !== FALSE)
            $this->db->set('safa_tripstatus.safa_tripstatus_id', $this->safa_tripstatus_id);

        if ($this->name_ar !== FALSE)
            $this->db->set('safa_tripstatus.name_ar', $this->name_ar);

        if ($this->description_ar !== FALSE)
            $this->db->set('safa_tripstatus.description_ar', $this->description_ar);

        if ($this->name_la !== FALSE)
            $this->db->set('safa_tripstatus.name_la', $this->name_la);

        if ($this->description_la !== FALSE)
            $this->db->set('safa_tripstatus.description_la', $this->description_la);

        if ($this->code !== FALSE)
            $this->db->set('safa_tripstatus.code', $this->code);



        if ($this->safa_tripstatus_id) {
            $this->db->where('safa_tripstatus.safa_tripstatus_id', $this->safa_tripstatus_id)->update('safa_tripstatus');
        } else {
            $this->db->insert('safa_tripstatus');
            return $this->db->insert_id();
        }
    }

    function delete() {
        if ($this->safa_tripstatus_id !== FALSE)
            $this->db->where('safa_tripstatus.safa_tripstatus_id', $this->safa_tripstatus_id);

        if ($this->name_ar !== FALSE)
            $this->db->where('safa_tripstatus.name_ar', $this->name_ar);

        if ($this->description_ar !== FALSE)
            $this->db->where('safa_tripstatus.description_ar', $this->description_ar);

        if ($this->name_la !== FALSE)
            $this->db->where('safa_tripstatus.name_la', $this->name_la);

        if ($this->description_la !== FALSE)
            $this->db->where('safa_tripstatus.description_la', $this->description_la);

        if ($this->code !== FALSE)
            $this->db->where('safa_tripstatus.code', $this->code);



        $this->db->delete('safa_tripstatus');
        return $this->db->affected_rows();
    }

    function search($rows_no = FALSE) {
        if ($this->custom_select !== FALSE) {
            $this->db->select('safa_tripstatus_id');
            $this->db->select($this->custom_select);
        }

        if ($this->safa_tripstatus_id !== FALSE)
            $this->db->where('safa_tripstatus.safa_tripstatus_id', $this->safa_tripstatus_id);

        if ($this->name_ar !== FALSE)
            $this->db->where('safa_tripstatus.name_ar', $this->name_ar);

        if ($this->description_ar !== FALSE)
            $this->db->where('safa_tripstatus.description_ar', $this->description_ar);

        if ($this->name_la !== FALSE)
            $this->db->where('safa_tripstatus.name_la', $this->name_la);

        if ($this->description_la !== FALSE)
            $this->db->where('safa_tripstatus.description_la', $this->description_la);

        if ($this->code !== FALSE)
            $this->db->where('safa_tripstatus.code', $this->code);


        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('safa_tripstatus');
        if ($rows_no)
            return $query->num_rows();

        return $query->result();
    }

    function check_delete_ability($id) {

        $this->db->select('count(safa_trips.safa_tripstatus_id)as trips,count(safa_trip_internaltrips.safa_tripstatus_id)as internaltrip');
        $this->db->from('safa_tripstatus');
        $this->db->join('safa_trips', 'safa_tripstatus.safa_tripstatus_id = safa_trips.safa_tripstatus_id', 'left');
        $this->db->join('safa_trip_internaltrips', 'safa_tripstatus.safa_tripstatus_id = safa_trip_internaltrips.safa_tripstatus_id', 'left');
        $this->db->group_by('safa_tripstatus.safa_tripstatus_id');
        $this->db->where('safa_tripstatus.safa_tripstatus_id', $id);
        $query = $this->db->get();
        $flag = 0;

//        print_r($query->result());
        foreach ($query->result() as $row => $table) {
            foreach ($table as $col => $value) {
                if ($value > 0) {
                    $flag = 1;
                    break;
                } else {
                    continue;
                }
            }
        }
        return $flag;
    }

//    function check_delete_ability($id) {
//        $this->db->select('safa_trips.safa_trip_id,safa_trips.safa_tripstatus_id ');
//        $this->db->from('safa_tripstatus');
//        $this->db->join('safa_trips', 'safa_tripstatus.safa_tripstatus_id = safa_trips.safa_tripstatus_id');
//        $this->db->group_by('safa_tripstatus.safa_tripstatus_id');
//        $this->db->where('safa_tripstatus.safa_tripstatus_id', $id);
//
//        $query = $this->db->get();
//        $flag = 0;
//        
////        print_r($query->result());
//        foreach ($query->result() as $row => $table) {
//            foreach ($table as $col => $value) {
//                if ($value > 0) {
//                    $flag = 1;
//                    break;
//                } else {
//                    continue;
//                }
//            }
//        }
//        return $flag;
//
//    }
}

/* End of file tripstatus_model.php */
/* Location: ./application/models/tripstatus_model.php */