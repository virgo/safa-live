<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Safa_package_periods_model extends CI_Model {

    public $safa_package_periods_id = FALSE;
    public $safa_package_id = FALSE;
    public $erp_package_period_id = FALSE;
    public $custom_select = FALSE;
    public $limit = FALSE;
    public $offset = FALSE;
    public $order_by = FALSE;

    function __construct() {
        parent::__construct();
    }

    function get($rows_no = FALSE) {

        if ($this->custom_select !== FALSE) {
            $this->db->select('safa_package_periods_id');
            $this->db->select($this->custom_select);
        }

        if ($this->safa_package_periods_id !== FALSE)
            $this->db->where('safa_package_periods.safa_package_periods_id', $this->safa_package_periods_id);

        if ($this->safa_package_id !== FALSE)
            $this->db->where('safa_package_periods.safa_package_id', $this->safa_package_id);

        if ($this->erp_package_period_id !== FALSE)
            $this->db->where('safa_package_periods.erp_package_period_id', $this->erp_package_period_id);

        $this->db->select('safa_package_periods.*, erp_package_periods.'.name().' as erp_package_period_name');    
        $this->db->join('erp_package_periods', 'erp_package_periods.erp_package_period_id = safa_package_periods.erp_package_period_id', 'left');
        
        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('safa_package_periods');
        if ($rows_no)
            return $query->num_rows();

        if ($this->safa_package_periods_id)
            return $query->row();
        else
            return $query->result();
    }

    function save() {
        if ($this->safa_package_periods_id !== FALSE)
            $this->db->set('safa_package_periods.safa_package_periods_id', $this->safa_package_periods_id);

        if ($this->safa_package_id !== FALSE)
            $this->db->set('safa_package_periods.safa_package_id', $this->safa_package_id);

        if ($this->erp_package_period_id !== FALSE)
            $this->db->set('safa_package_periods.erp_package_period_id ', $this->erp_package_period_id);


        if ($this->safa_package_periods_id) {
            $this->db->where('safa_package_periods.safa_package_periods_id', $this->safa_package_periods_id)->update('safa_package_periods');
        } else {
            $this->db->insert('safa_package_periods');
            return $this->db->insert_id();
        }
    }

    function delete() {
        if ($this->safa_package_periods_id !== FALSE)
            $this->db->where('safa_package_periods.safa_package_periods_id', $this->safa_package_periods_id);

        if ($this->safa_package_id !== FALSE)
            $this->db->where('safa_package_periods.safa_package_id', $this->safa_package_id);

        if ($this->erp_package_period_id !== FALSE)
            $this->db->where('safa_package_periods.erp_package_period_id', $this->erp_package_period_id);


        $this->db->delete('safa_package_periods');
        return $this->db->affected_rows();
    }

    function search($rows_no = FALSE) {
        if ($this->custom_select !== FALSE) {
            $this->db->select('safa_package_periods_id');
            $this->db->select($this->custom_select);
        }


        if ($this->safa_package_periods_id !== FALSE)
            $this->db->where('safa_package_periods.safa_package_periods_id', $this->safa_package_periods_id);

        if ($this->safa_package_id !== FALSE)
            $this->db->where('safa_package_periods.safa_package_id', $this->safa_package_id);

        if ($this->package_period_id !== FALSE)
            $this->db->where('safa_package_periods.package_period_id', $this->package_period_id);


        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('safa_package_periods');
        if ($rows_no)
            return $query->num_rows();

        return $query->result();
    }

}

/* End of file uo_package_tourismplaces_model.php */
/* Location: ./application/models/uo_package_tourismplaces_model.php */