<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Packages_model extends CI_Model {

    public $safa_uo_package_id = FALSE;
    public $safa_uo_id = FALSE;
    public $start_date = FALSE;
    public $end_date = FALSE;
    public $name_ar = FALSE;
    public $name_la = FALSE;
    public $erp_country_id = FALSE;
    public $custom_select = FALSE;
    public $limit = FALSE;
    public $offset = FALSE;
    public $order_by = FALSE;

    function __construct() {
        parent::__construct();
    }

    function get($rows_no = FALSE) {

        if ($this->custom_select !== FALSE) {
            $this->db->select('safa_uo_package_id');
            $this->db->select($this->custom_select);
        }

        if ($this->safa_uo_package_id !== FALSE)
            $this->db->where('safa_uo_packages.safa_uo_package_id', $this->safa_uo_package_id);

        if ($this->safa_uo_id !== FALSE)
            $this->db->where('safa_uo_packages.safa_uo_id', $this->safa_uo_id);

        if ($this->start_date !== FALSE)
            $this->db->where('safa_uo_packages.start_date', $this->start_date);

        if ($this->end_date !== FALSE)
            $this->db->where('safa_uo_packages.end_date', $this->end_date);

        if ($this->name_ar !== FALSE)
            $this->db->where('safa_uo_packages.name_ar', $this->name_ar);

        if ($this->name_la !== FALSE)
            $this->db->where('safa_uo_packages.name_la', $this->name_la);

        if ($this->erp_country_id !== FALSE)
            $this->db->where('safa_uo_packages.erp_country_id', $this->erp_country_id);


        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('safa_uo_packages');
        if ($rows_no)
            return $query->num_rows();

        if ($this->safa_uo_package_id)
            return $query->row();
        else
            return $query->result();
    }

    function save() {
        if ($this->safa_uo_package_id !== FALSE)
            $this->db->set('safa_uo_packages.safa_uo_package_id', $this->safa_uo_package_id);

        if ($this->safa_uo_id !== FALSE)
            $this->db->set('safa_uo_packages.safa_uo_id', $this->safa_uo_id);

        if ($this->start_date !== FALSE)
            $this->db->set('safa_uo_packages.start_date', $this->start_date);

        if ($this->end_date !== FALSE)
            $this->db->set('safa_uo_packages.end_date', $this->end_date);

        if ($this->name_ar !== FALSE)
            $this->db->set('safa_uo_packages.name_ar', $this->name_ar);

        if ($this->name_la !== FALSE)
            $this->db->set('safa_uo_packages.name_la', $this->name_la);

        if ($this->erp_country_id !== FALSE)
            $this->db->set('safa_uo_packages.erp_country_id', $this->erp_country_id);



        if ($this->safa_uo_package_id) {
            $this->db->where('safa_uo_packages.safa_uo_package_id', $this->safa_uo_package_id)->update('safa_uo_packages');
        } else {
            $this->db->insert('safa_uo_packages');
            return $this->db->insert_id();
        }
    }

    function delete() {
        if ($this->safa_uo_package_id !== FALSE)
            $this->db->where('safa_uo_packages.safa_uo_package_id', $this->safa_uo_package_id);

        if ($this->safa_uo_id !== FALSE)
            $this->db->where('safa_uo_packages.safa_uo_id', $this->safa_uo_id);

        if ($this->start_date !== FALSE)
            $this->db->where('safa_uo_packages.start_date', $this->start_date);

        if ($this->end_date !== FALSE)
            $this->db->where('safa_uo_packages.end_date', $this->end_date);

        if ($this->name_ar !== FALSE)
            $this->db->where('safa_uo_packages.name_ar', $this->name_ar);

        if ($this->name_la !== FALSE)
            $this->db->where('safa_uo_packages.name_la', $this->name_la);

        if ($this->erp_country_id !== FALSE)
            $this->db->where('safa_uo_packages.erp_country_id', $this->erp_country_id);



        $this->db->delete('safa_uo_packages');
        return $this->db->affected_rows();
    }

    function search($rows_no = FALSE) {
        if ($this->custom_select !== FALSE) {
            $this->db->select('safa_uo_package_id');
            $this->db->select($this->custom_select);
        }

        if ($this->safa_uo_package_id !== FALSE)
            $this->db->where('safa_uo_packages.safa_uo_package_id', $this->safa_uo_package_id);

        if ($this->safa_uo_id !== FALSE)
            $this->db->where('safa_uo_packages.safa_uo_id', $this->safa_uo_id);

        if ($this->start_date !== FALSE)
            $this->db->where('safa_uo_packages.start_date', $this->start_date);

        if ($this->end_date !== FALSE)
            $this->db->where('safa_uo_packages.end_date', $this->end_date);

        if ($this->name_ar !== FALSE)
            $this->db->where('safa_uo_packages.name_ar', $this->name_ar);

        if ($this->name_la !== FALSE)
            $this->db->where('safa_uo_packages.name_la', $this->name_la);

        if ($this->erp_country_id !== FALSE)
            $this->db->where('safa_uo_packages.erp_country_id', $this->erp_country_id);


        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('safa_uo_packages');
        if ($rows_no)
            return $query->num_rows();

        return $query->result();
    }

}

/* End of file packages_model.php */
/* Location: ./application/models/packages_model.php */