<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Error_logs_model extends CI_Model {

    public $error_logs_id = FALSE;
    public $title = FALSE;
    public $message = FALSE;
    public $trace = FALSE;
    public $agent = FALSE;
    public $current_url = FALSE;
    public $timestamp = FALSE;
    public $custom_select = FALSE;
    public $limit = FALSE;
    public $offset = FALSE;
    public $order_by = FALSE;

    function __construct() {
        parent::__construct();
    }

    function get($rows_no = FALSE) {

        if ($this->custom_select !== FALSE) {
            $this->db->select('error_logs_id');
            $this->db->select($this->custom_select);
        }

        if ($this->error_logs_id !== FALSE)
            $this->db->where('error_logs.error_logs_id', $this->error_logs_id);

        if ($this->title !== FALSE)
            $this->db->where('error_logs.title', $this->title);

        if ($this->message !== FALSE)
            $this->db->where('error_logs.message', $this->message);

        if ($this->trace !== FALSE)
            $this->db->where('error_logs.trace', $this->trace);

        if ($this->agent !== FALSE)
            $this->db->where('error_logs.agent', $this->agent);

        if ($this->current_url !== FALSE)
            $this->db->where('error_logs.current_url', $this->current_url);

        if ($this->timestamp !== FALSE)
            $this->db->where('error_logs.timestamp', $this->timestamp);


        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('error_logs');
        if ($rows_no)
            return $query->num_rows();

        if ($this->error_logs_id)
            return $query->row();
        else
            return $query->result();
    }

    function save() {
        if ($this->error_logs_id !== FALSE)
            $this->db->set('error_logs.error_logs_id', $this->error_logs_id);

        if ($this->title !== FALSE)
            $this->db->set('error_logs.title', $this->title);

        if ($this->message !== FALSE)
            $this->db->set('error_logs.message', $this->message);

        if ($this->trace !== FALSE)
            $this->db->set('error_logs.trace', $this->trace);

        if ($this->agent !== FALSE)
            $this->db->set('error_logs.agent', $this->agent);

        if ($this->current_url !== FALSE)
            $this->db->set('error_logs.current_url', $this->current_url);

        if ($this->timestamp !== FALSE)
            $this->db->set('error_logs.timestamp', $this->timestamp);



        if ($this->error_logs_id) {
            $this->db->where('error_logs.error_logs_id', $this->error_logs_id)->update('error_logs');
        } else {
            $this->db->insert('error_logs');
            return $this->db->insert_id();
        }
    }

    function delete() {
        if ($this->error_logs_id !== FALSE)
            $this->db->where('error_logs.error_logs_id', $this->error_logs_id);

        if ($this->title !== FALSE)
            $this->db->where('error_logs.title', $this->title);

        if ($this->message !== FALSE)
            $this->db->where('error_logs.message', $this->message);

        if ($this->trace !== FALSE)
            $this->db->where('error_logs.trace', $this->trace);

        if ($this->agent !== FALSE)
            $this->db->where('error_logs.agent', $this->agent);

        if ($this->current_url !== FALSE)
            $this->db->where('error_logs.current_url', $this->current_url);

        if ($this->timestamp !== FALSE)
            $this->db->where('error_logs.timestamp', $this->timestamp);



        $this->db->delete('error_logs');
        return $this->db->affected_rows();
    }

    function search($rows_no = FALSE) {
        if ($this->custom_select !== FALSE) {
            $this->db->select('error_logs_id');
            $this->db->select($this->custom_select);
        }

        if ($this->error_logs_id !== FALSE)
            $this->db->where('error_logs.error_logs_id', $this->error_logs_id);

        if ($this->title !== FALSE)
            $this->db->where('error_logs.title', $this->title);

        if ($this->message !== FALSE)
            $this->db->where('error_logs.message', $this->message);

        if ($this->trace !== FALSE)
            $this->db->where('error_logs.trace', $this->trace);

        if ($this->agent !== FALSE)
            $this->db->where('error_logs.agent', $this->agent);

        if ($this->current_url !== FALSE)
            $this->db->where('error_logs.current_url', $this->current_url);

        if ($this->timestamp !== FALSE)
            $this->db->where('error_logs.timestamp', $this->timestamp);


        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('error_logs');
        if ($rows_no)
            return $query->num_rows();

        return $query->result();
    }

}

/* End of file error_logs_model.php */
/* Location: ./application/models/error_logs_model.php */