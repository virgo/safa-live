<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Languages_model extends CI_Model {

    public $language_path = './application/language';

    function __construct() {
        parent::__construct();
        $this->load->helper('directory');
        $this->load->helper('file');
        $this->load->model('safa_languages_files_model');
        $this->load->model('safa_language_keys_model');
    }

    function read_directory($dir_path) {
        $arr_files = array();
        $directories = directory_map($dir_path);
        foreach ($directories as $index => $directory) {
            if (is_array($directory) && count($directory) > 0) {
                $dir_url = $dir_path . '\\' . str_replace('\\', '', $index);
                $arr_files = $this->read_directory($dir_url);
            } else {
                $file_path = $dir_path . '\\' . $directory;
                $arr_files[] = array(
                    'url' => $file_path,
                    'name' => $directory
                );
            }
        }
        return $arr_files;
    }

    function insert_lang_file($file_path) {
        $this->safa_languages_files_model->file_name = $file_path;
        $insert_id = $this->safa_languages_files_model->save();
        return $insert_id;
    }

    function get_file_keys($file_path) {
        $string = read_file($file_path);
        $start_string = substr($string, strpos($string, '$lang'));
        $arr_kys = explode(';', $start_string);
        $array_keys = array();
        foreach ($arr_kys as $key => $value) {
            $key = explode("=", $value);
            if (count($key) == 2) {
                $key_ = substr($key[0], strpos($key[0], '[') + 1, strlen(substr($key[0], strpos($key[0], '[') + 1)) - 2);
                $array_keys[stripslashes($key_)] = $key[1];
            }
        }
        return $array_keys;
    }

}

/* End of file countries_model.php */
/* Location: ./application/models/countries_model.php */
/*
 foreach($value as $file_index=>$file){
                $file_name=str_replace('_lang.php','',$file);
                $this->safa_languages_files_model->file_name=$file_name;
                $file_id=$this->safa_languages_files_model->save();
                $arr_keys=$this->get_file_keys($this->language_path.'/arabic/'.$index.$file);
                if(count($arr_keys)>0 && $file_id){
                    foreach($arr_keys as $key=>$value){
                        $this->safa_language_keys_model->safa_language_key_name=$key;
                        $this->safa_language_keys_model->safa_languages_file_id=$file_id;
                        $key_id=$this->safa_language_keys_model->save();
                        
                    }
                }
             } 
 */