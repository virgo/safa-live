<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Hotel_marketing_companies_model extends CI_Model {

    public $safa_hm_id = FALSE;
    public $name_ar = FALSE;
    public $name_la = FALSE;
    public $phone = FALSE;
    public $fax = FALSE;
    public $email = FALSE;
    public $remarks = FALSE;
    public $custom_select = FALSE;
    public $limit = FALSE;
    public $offset = FALSE;
    public $order_by = FALSE;

    function __construct() {
        parent::__construct();
    }

    function get($rows_no = FALSE) {

        if ($this->custom_select !== FALSE) {
            $this->db->select('safa_hm_id');
            $this->db->select($this->custom_select);
        }

        if ($this->safa_hm_id !== FALSE)
            $this->db->where('safa_hms.safa_hm_id', $this->safa_hm_id);

        if ($this->name_ar !== FALSE)
            $this->db->where('safa_hms.name_ar', $this->name_ar);

        if ($this->name_la !== FALSE)
            $this->db->where('safa_hms.name_la', $this->name_la);

        if ($this->phone !== FALSE)
            $this->db->where('safa_hms.phone', $this->phone);

        if ($this->fax !== FALSE)
            $this->db->where('safa_hms.fax', $this->fax);

        if ($this->email !== FALSE)
            $this->db->where('safa_hms.email', $this->email);

        if ($this->remarks !== FALSE)
            $this->db->where('safa_hms.remarks', $this->remarks);


        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('safa_hms');
        if ($rows_no)
            return $query->num_rows();

        if ($this->safa_hm_id)
            return $query->row();
        else
            return $query->result();
    }

    function save() {

        if ($this->safa_hm_id !== FALSE)
            $this->db->set('safa_hms.safa_hm_id', $this->safa_hm_id);

        if ($this->name_ar !== FALSE)
            $this->db->set('safa_hms.name_ar', $this->name_ar);

        if ($this->name_la !== FALSE)
            $this->db->set('safa_hms.name_la', $this->name_la);

        if ($this->phone !== FALSE)
            $this->db->set('safa_hms.phone', $this->phone);

        if ($this->fax !== FALSE)
            $this->db->set('safa_hms.fax', $this->fax);


        if ($this->safa_hm_id) {
            $this->db->where('safa_hms.safa_hm_id ', $this->safa_hm_id)->update('safa_hms');
        } else {
            $this->db->insert('safa_hms');
            return $this->db->insert_id();
        }
    }

    function delete() {

        if ($this->safa_hm_id !== FALSE)
            $this->db->where('safa_hms.safa_hm_id', $this->safa_hm_id);

        if ($this->name_ar !== FALSE)
            $this->db->where('safa_hms.name_ar', $this->name_ar);

        if ($this->name_la !== FALSE)
            $this->db->where('safa_hms.name_la', $this->name_la);

        if ($this->phone !== FALSE)
            $this->db->where('safa_hms.phone', $this->phone);

        if ($this->fax !== FALSE)
            $this->db->where('safa_hms.fax', $this->fax);

        if ($this->email !== FALSE)
            $this->db->where('safa_hms.email', $this->email);

        if ($this->remarks !== FALSE)
            $this->db->where('safa_hms.remarks', $this->remarks);

        $this->db->delete('safa_hms');
        return $this->db->affected_rows();
    }

    function search($rows_no = FALSE) {
        if ($this->custom_select !== FALSE) {
            $this->db->select('safa_hm_id');
            $this->db->select($this->custom_select);
        }

        if ($this->safa_hm_id !== FALSE)
            $this->db->where('safa_hms.safa_hm_id', $this->safa_hm_id);

        if ($this->name_ar !== FALSE)
            $this->db->where('safa_hms.name_ar', $this->name_ar);

        if ($this->name_la !== FALSE)
            $this->db->where('safa_hms.name_la', $this->name_la);

        if ($this->phone !== FALSE)
            $this->db->where('safa_hms.phone', $this->phone);

        if ($this->fax !== FALSE)
            $this->db->where('safa_hms.fax', $this->fax);

        if ($this->email !== FALSE)
            $this->db->where('safa_hms.email', $this->email);

        if ($this->remarks !== FALSE)
            $this->db->where('safa_hms.remarks', $this->remarks);

        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('safa_hms');
        if ($rows_no)
            return $query->num_rows();

        return $query->result();
    }

}

/* End of file hotel_marketing_companies_model.php */
/* Location: ./application/models/hotel_marketing_companies_model.php */