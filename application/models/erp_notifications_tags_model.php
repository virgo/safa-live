<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Erp_notifications_tags_model extends CI_Model {

    public $erp_notifications_tags_id = FALSE;
    public $name_ar = FALSE;
    public $name_la = FALSE;
    public $notifications_tags = FALSE;
    public $custom_select = FALSE;
    public $limit = FALSE;
    public $offset = FALSE;
    public $order_by = FALSE;

    function __construct() {
        parent::__construct();
    }

    function get($rows_no = FALSE) {

        if ($this->custom_select !== FALSE) {
            $this->db->select('erp_notifications_tags_id');
            $this->db->select($this->custom_select);
        }

        if ($this->erp_notifications_tags_id !== FALSE)
            $this->db->where('erp_notifications_tags.erp_notifications_tags_id', $this->erp_notifications_tags_id);

        if ($this->name_ar !== FALSE)
            $this->db->where('erp_notifications_tags.name_ar', $this->name_ar);

        if ($this->name_la !== FALSE)
            $this->db->where('erp_notifications_tags.name_la', $this->name_la);

        if ($this->notifications_tags !== FALSE)
            $this->db->where('erp_notifications_tags.notifications_tags', $this->notifications_tags);


        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('erp_notifications_tags');
        if ($rows_no)
            return $query->num_rows();

        if ($this->erp_notifications_tags_id)
            return $query->row();
        else
            return $query->result();
    }

}

/* End of file erp_notifications_tags_model.php */
/* Location: ./application/models/erp_notifications_tags_model.php */