<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Erp_hotel_speakinglanguage_details_model extends CI_Model {

    public $erp_hotel_speakinglanguage_details_id = FALSE;
    public $erp_hotel_id = FALSE;
    public $erp_hotelspeakinglanguage_id = FALSE;
    public $custom_select = FALSE;
    public $limit = FALSE;
    public $offset = FALSE;
    public $order_by = FALSE;

    function __construct() {
        parent::__construct();
    }

    function get($rows_no = FALSE) {

        if ($this->custom_select !== FALSE) {
            $this->db->select('erp_hotel_speakinglanguage_details_id');
            $this->db->select($this->custom_select);
        }

        if ($this->erp_hotel_speakinglanguage_details_id !== FALSE)
            $this->db->where('erp_hotel_speakinglanguage_details.erp_hotel_speakinglanguage_details_id', $this->erp_hotel_speakinglanguage_details_id);

        if ($this->erp_hotel_id !== FALSE)
            $this->db->where('erp_hotel_speakinglanguage_details.erp_hotel_id', $this->erp_hotel_id);

        if ($this->erp_hotelspeakinglanguage_id !== FALSE)
            $this->db->where('erp_hotel_speakinglanguage_details.erp_hotelspeakinglanguage_id', $this->erp_hotelspeakinglanguage_id);


        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('erp_hotel_speakinglanguage_details');
        if ($rows_no)
            return $query->num_rows();

        if ($this->erp_hotel_speakinglanguage_details_id)
            return $query->row();
        else
            return $query->result();
    }

    function save() {
        if ($this->erp_hotel_speakinglanguage_details_id !== FALSE)
            $this->db->set('erp_hotel_speakinglanguage_details.erp_hotel_speakinglanguage_details_id', $this->erp_hotel_speakinglanguage_details_id);

        if ($this->erp_hotel_id !== FALSE)
            $this->db->set('erp_hotel_speakinglanguage_details.erp_hotel_id', $this->erp_hotel_id);

        if ($this->erp_hotelspeakinglanguage_id !== FALSE)
            $this->db->set('erp_hotel_speakinglanguage_details.erp_hotelspeakinglanguage_id', $this->erp_hotelspeakinglanguage_id);



        if ($this->erp_hotel_speakinglanguage_details_id) {
            $this->db->where('erp_hotel_speakinglanguage_details.erp_hotel_speakinglanguage_details_id', $this->erp_hotel_speakinglanguage_details_id)->update('erp_hotel_speakinglanguage_details');
        } else {
            $this->db->insert('erp_hotel_speakinglanguage_details');
            return $this->db->insert_id();
        }
    }

    function delete() {
        if ($this->erp_hotel_speakinglanguage_details_id !== FALSE)
            $this->db->where('erp_hotel_speakinglanguage_details.erp_hotel_speakinglanguage_details_id', $this->erp_hotel_speakinglanguage_details_id);

        if ($this->erp_hotel_id !== FALSE)
            $this->db->where('erp_hotel_speakinglanguage_details.erp_hotel_id', $this->erp_hotel_id);

        if ($this->erp_hotelspeakinglanguage_id !== FALSE)
            $this->db->where('erp_hotel_speakinglanguage_details.erp_hotelspeakinglanguage_id', $this->erp_hotelspeakinglanguage_id);



        $this->db->delete('erp_hotel_speakinglanguage_details');
        return $this->db->affected_rows();
    }

    function search($rows_no = FALSE) {
        if ($this->custom_select !== FALSE) {
            $this->db->select('erp_hotel_speakinglanguage_details_id');
            $this->db->select($this->custom_select);
        }

        if ($this->erp_hotel_speakinglanguage_details_id !== FALSE)
            $this->db->where('erp_hotel_speakinglanguage_details.erp_hotel_speakinglanguage_details_id', $this->erp_hotel_speakinglanguage_details_id);

        if ($this->erp_hotel_id !== FALSE)
            $this->db->where('erp_hotel_speakinglanguage_details.erp_hotel_id', $this->erp_hotel_id);

        if ($this->erp_hotelspeakinglanguage_id !== FALSE)
            $this->db->where('erp_hotel_speakinglanguage_details.erp_hotelspeakinglanguage_id', $this->erp_hotelspeakinglanguage_id);


        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('erp_hotel_speakinglanguage_details');
        if ($rows_no)
            return $query->num_rows();

        return $query->result();
    }

}

/* End of file erp_hotel_speakinglanguage_details_model.php */
/* Location: ./application/models/erp_hotel_speakinglanguage_details_model.php */