<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Safa_group_passport_accommodation_model extends CI_Model {

	public $safa_group_passport_accommodation_id = FALSE;
	public $erp_meal_id = FALSE;
	public $safa_passport_accommodation_room_id = FALSE;
	public $safa_group_passport_id = FALSE;
	public $safa_package_periods_id = FALSE;
	public $price = FALSE;
	public $erp_currency_id = FALSE;

	public $safa_reservation_form_id = FALSE;
	
    public $custom_select = FALSE;
    public $limit = FALSE;
    public $offset = FALSE;
    public $order_by = FALSE;

    function __construct() 
    {
        parent::__construct();
    }

    
    function get($rows_no = FALSE) 
    {

        if ($this->custom_select !== FALSE) {
            $this->db->select('safa_group_passport_accommodation_id');
            $this->db->select($this->custom_select);
        }

        $this->db->from('safa_group_passport_accommodation');
        
        if ($this->safa_group_passport_accommodation_id !== FALSE)
            $this->db->where('safa_group_passport_accommodation.safa_group_passport_accommodation_id', $this->safa_group_passport_accommodation_id);
        if ($this->erp_meal_id !== FALSE)
            $this->db->where('safa_group_passport_accommodation.erp_meal_id', $this->erp_meal_id);
        if ($this->safa_passport_accommodation_room_id !== FALSE)
            $this->db->where('safa_group_passport_accommodation.safa_passport_accommodation_room_id', $this->safa_passport_accommodation_room_id);
        if ($this->safa_group_passport_id !== FALSE)
            $this->db->where('safa_group_passport_accommodation.safa_group_passport_id', $this->safa_group_passport_id);
        if ($this->safa_package_periods_id !== FALSE)
            $this->db->where('safa_group_passport_accommodation.safa_package_periods_id', $this->safa_package_periods_id);
        if ($this->price !== FALSE)
            $this->db->where('safa_group_passport_accommodation.price', $this->price);
        if ($this->erp_currency_id !== FALSE)
            $this->db->where('safa_group_passport_accommodation.erp_currency_id', $this->erp_currency_id);
        
		if ($this->safa_reservation_form_id !== FALSE) {
            $this->db->where('safa_passport_accommodation_rooms.safa_reservation_form_id', $this->safa_reservation_form_id);
			$this->db->join('safa_passport_accommodation_rooms', 'safa_passport_accommodation_rooms.safa_passport_accommodation_room_id = safa_group_passport_accommodation.safa_passport_accommodation_room_id', 'left');
		}
		
		$this->db->select('safa_group_passport_accommodation.*, CONCAT( safa_group_passports.first_name_ar, " ", safa_group_passports.second_name_ar, " ", safa_group_passports.third_name_ar, " ", safa_group_passports.fourth_name_ar ) as full_name_ar,
		 CONCAT( safa_group_passports.first_name_ar, " ", safa_group_passports.fourth_name_ar ) as first_fourth_name_ar, 
		 CONCAT( safa_group_passports.first_name_la, " ", safa_group_passports.fourth_name_la ) as first_fourth_name_la,
		CONCAT( safa_group_passports.first_name_la, " ", safa_group_passports.second_name_la, " ", safa_group_passports.third_name_la, " ", safa_group_passports.fourth_name_la ) as full_name_la' , false);
		$this->db->join('safa_group_passports', 'safa_group_passports.safa_group_passport_id = safa_group_passport_accommodation.safa_group_passport_id', 'left');
		
		
        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get();
        if ($rows_no)
            return $query->num_rows();

        if ($this->safa_group_passport_accommodation_id)
            return $query->row();
        else
            return $query->result();
    }

    function get_distinct_passports_by_trip_uo_form($safa_trip_id =false, $safa_uo_id=false, $safa_reservation_form_id =false) 
    {
		$this->db->distinct();
        $this->db->select('safa_group_passports.safa_group_passport_id, safa_reservation_forms.safa_reservation_form_id');
        $this->db->from('safa_group_passports');
        $this->db->join('safa_group_passport_accommodation', 'safa_group_passport_accommodation.safa_group_passport_id = safa_group_passports.safa_group_passport_id', 'left');
        $this->db->join('safa_passport_accommodation_rooms', 'safa_passport_accommodation_rooms.safa_passport_accommodation_room_id = safa_group_passport_accommodation.safa_passport_accommodation_room_id', 'left');
        $this->db->join('safa_reservation_forms', 'safa_reservation_forms.safa_reservation_form_id = safa_passport_accommodation_rooms.safa_reservation_form_id', 'left');
        $this->db->join('safa_package_periods', 'safa_package_periods.safa_package_periods_id = safa_reservation_forms.safa_package_period_id', 'left');
		$this->db->join('safa_packages', 'safa_packages.safa_package_id = safa_package_periods.safa_package_id', 'left');
		$this->db->join('erp_package_periods', 'erp_package_periods.erp_package_period_id = safa_package_periods.erp_package_period_id', 'left');
		
		if($safa_trip_id!==false) {
			$this->db->where('safa_reservation_forms.safa_trip_id', $safa_trip_id);
		}
    
    	if($safa_uo_id!==false) {
			$this->db->where('safa_packages.erp_company_type_id', 2);
			$this->db->where('safa_packages.erp_company_id', $safa_uo_id);
		}
		
    	if($safa_reservation_form_id!==false) {
			$this->db->where('safa_reservation_forms.safa_reservation_form_id', $safa_reservation_form_id);
		}

        $query = $this->db->get();
        
        //echo $this->db->last_query(); exit;
        
	    return $query->result();
    }
    
    
	function get_distinct_passports_by_trip_uo_form_age($safa_trip_id =false, $safa_uo_id=false, $safa_reservation_form_id =false, $age=false) 
    {
		$this->db->distinct();
        $this->db->select('safa_group_passports.safa_group_passport_id, safa_reservation_forms.safa_reservation_form_id');
        $this->db->from('safa_group_passports');
        $this->db->join('safa_group_passport_accommodation', 'safa_group_passport_accommodation.safa_group_passport_id = safa_group_passports.safa_group_passport_id', 'left');
        $this->db->join('safa_passport_accommodation_rooms', 'safa_passport_accommodation_rooms.safa_passport_accommodation_room_id = safa_group_passport_accommodation.safa_passport_accommodation_room_id', 'left');
        $this->db->join('safa_reservation_forms', 'safa_reservation_forms.safa_reservation_form_id = safa_passport_accommodation_rooms.safa_reservation_form_id', 'left');
        $this->db->join('safa_package_periods', 'safa_package_periods.safa_package_periods_id = safa_reservation_forms.safa_package_period_id', 'left');
		$this->db->join('safa_packages', 'safa_packages.safa_package_id = safa_package_periods.safa_package_id', 'left');
		$this->db->join('erp_package_periods', 'erp_package_periods.erp_package_period_id = safa_package_periods.erp_package_period_id', 'left');
		
		if($age=='adult') {
			$this->db->where('(SELECT DATE_FORMAT(FROM_DAYS(TO_DAYS(NOW())-TO_DAYS(safa_group_passports.date_of_birth)), "%Y")+0 AS age FROM safa_group_passports as inner_safa_group_passports where inner_safa_group_passports.safa_group_passport_id =safa_group_passports.safa_group_passport_id) > 12','' , FALSE);
		} else if($age=='child') {
			$this->db->where('(SELECT DATE_FORMAT(FROM_DAYS(TO_DAYS(NOW())-TO_DAYS(safa_group_passports.date_of_birth)), "%Y")+0 AS age FROM safa_group_passports as inner_safa_group_passports where inner_safa_group_passports.safa_group_passport_id =safa_group_passports.safa_group_passport_id) > 2 and (SELECT DATE_FORMAT(FROM_DAYS(TO_DAYS(NOW())-TO_DAYS(safa_group_passports.date_of_birth)), "%Y")+0 AS age FROM safa_group_passports as inner_safa_group_passports where inner_safa_group_passports.safa_group_passport_id =safa_group_passports.safa_group_passport_id) < 12 ','' , FALSE);
		} else if($age=='infant') {
			$this->db->where('(SELECT DATE_FORMAT(FROM_DAYS(TO_DAYS(NOW())-TO_DAYS(safa_group_passports.date_of_birth)), "%Y")+0 AS age FROM safa_group_passports as inner_safa_group_passports where inner_safa_group_passports.safa_group_passport_id =safa_group_passports.safa_group_passport_id) < 2','' , FALSE);
		}
		
		
		if($safa_trip_id!==false) {
			$this->db->where('safa_reservation_forms.safa_trip_id', $safa_trip_id);
		}
    
    	if($safa_uo_id!==false) {
			$this->db->where('safa_packages.erp_company_type_id', 2);
			$this->db->where('safa_packages.erp_company_id', $safa_uo_id);
		}
		
    	if($safa_reservation_form_id!==false) {
			$this->db->where('safa_reservation_forms.safa_reservation_form_id', $safa_reservation_form_id);
		}

        $query = $this->db->get();
        
        //echo $this->db->last_query(); exit;
        
	    return $query->result();
    }
    
    
    function save() 
    {
        if ($this->safa_group_passport_accommodation_id !== FALSE)
            $this->db->set('safa_group_passport_accommodation.safa_group_passport_accommodation_id', $this->safa_group_passport_accommodation_id);

        if ($this->erp_meal_id !== FALSE)
            $this->db->set('safa_group_passport_accommodation.erp_meal_id', $this->erp_meal_id);
        if ($this->safa_passport_accommodation_room_id !== FALSE)
            $this->db->set('safa_group_passport_accommodation.safa_passport_accommodation_room_id', $this->safa_passport_accommodation_room_id);
        if ($this->safa_group_passport_id !== FALSE)
            $this->db->set('safa_group_passport_accommodation.safa_group_passport_id', $this->safa_group_passport_id);
        if ($this->safa_package_periods_id !== FALSE)
            $this->db->set('safa_group_passport_accommodation.safa_package_periods_id', $this->safa_package_periods_id);
        if ($this->price !== FALSE)
            $this->db->set('safa_group_passport_accommodation.price', $this->price);
        if ($this->erp_currency_id !== FALSE)
            $this->db->set('safa_group_passport_accommodation.erp_currency_id', $this->erp_currency_id);
        
            

        if ($this->safa_group_passport_accommodation_id) {
            $this->db->where('safa_group_passport_accommodation.safa_group_passport_accommodation_id', $this->safa_group_passport_accommodation_id)->update('safa_group_passport_accommodation');
        } else {
            $this->db->insert('safa_group_passport_accommodation');
            $ss = $this->db->last_query();
            
            return $this->db->insert_id();
        }
    }

    function delete() 
    {
        if ($this->safa_group_passport_accommodation_id !== FALSE)
            $this->db->where('safa_group_passport_accommodation.safa_group_passport_accommodation_id', $this->safa_group_passport_accommodation_id);

        if ($this->erp_meal_id !== FALSE)
            $this->db->where('safa_group_passport_accommodation.erp_meal_id', $this->erp_meal_id);
        if ($this->safa_passport_accommodation_room_id !== FALSE)
            $this->db->where('safa_group_passport_accommodation.safa_passport_accommodation_room_id', $this->safa_passport_accommodation_room_id);
        if ($this->safa_group_passport_id !== FALSE)
            $this->db->where('safa_group_passport_accommodation.safa_group_passport_id', $this->safa_group_passport_id);
        if ($this->safa_package_periods_id !== FALSE)
            $this->db->where('safa_group_passport_accommodation.safa_package_periods_id', $this->safa_package_periods_id);
        if ($this->price !== FALSE)
            $this->db->where('safa_group_passport_accommodation.price', $this->price);
        if ($this->erp_currency_id !== FALSE)
            $this->db->where('safa_group_passport_accommodation.erp_currency_id', $this->erp_currency_id);
        
       
        $this->db->delete('safa_group_passport_accommodation');
        return $this->db->affected_rows();
    }
    
    function delete_by_room_id_and_ids_arr_not_in($safa_passport_accommodation_room_id=0, $ids_arr=array()) 
    {
       	$this->db->where_not_in('safa_group_passport_accommodation.safa_group_passport_id', $ids_arr);
       	$this->db->where('safa_group_passport_accommodation.safa_passport_accommodation_room_id', $safa_passport_accommodation_room_id);
       	$deleted_rows = $this->db->get('safa_group_passport_accommodation')->result();
        
       	//echo $this->db->last_query(); //exit;
       	
       	$this->db->where_not_in('safa_group_passport_accommodation.safa_group_passport_id', $ids_arr);
       	$this->db->where('safa_group_passport_accommodation.safa_passport_accommodation_room_id', $safa_passport_accommodation_room_id);
       	$this->db->delete('safa_group_passport_accommodation');
        
       	return $deleted_rows;
    }

	
    
    
}

/* End of file uo_packages_model.php */
/* Location: ./application/models/uo_packages_model.php */