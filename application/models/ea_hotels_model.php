<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Ea_hotels_model extends CI_Model {

    public $safa_ea_hotel = FALSE;
    public $erp_hotel_id = FALSE;
    public $safa_ea_id = FALSE;
    public $custom_select = FALSE;
    public $limit = FALSE;
    public $offset = FALSE;
    public $order_by = FALSE;

    function __construct() {
        parent::__construct();
    }

    function get($rows_no = FALSE) {

        if ($this->custom_select !== FALSE) {
            $this->db->select('safa_ea_hotel');
            $this->db->select($this->custom_select);
        }

        if ($this->safa_ea_hotel !== FALSE)
            $this->db->where('safa_ea_hotels.safa_ea_hotel', $this->safa_ea_hotel);

        if ($this->erp_hotel_id !== FALSE)
            $this->db->where('safa_ea_hotels.erp_hotel_id', $this->erp_hotel_id);

        if ($this->safa_ea_id !== FALSE)
            $this->db->where('safa_ea_hotels.safa_ea_id', $this->safa_ea_id);


        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('safa_ea_hotels');
        if ($rows_no)
            return $query->num_rows();

        if ($this->safa_ea_hotel)
            return $query->row();
        else
            return $query->result();
    }

    function save() {
        if ($this->safa_ea_hotel !== FALSE)
            $this->db->set('safa_ea_hotels.safa_ea_hotel', $this->safa_ea_hotel);

        if ($this->erp_hotel_id !== FALSE)
            echo $this->erp_hotel_id;
        $this->db->set('safa_ea_hotels.erp_hotel_id', $this->erp_hotel_id);

        if ($this->safa_ea_id !== FALSE)
            $this->db->set('safa_ea_hotels.safa_ea_id', $this->safa_ea_id);



        if ($this->safa_ea_hotel) {
            $this->db->where('safa_ea_hotels.safa_ea_hotel', $this->safa_ea_hotel)->update('safa_ea_hotels');
        } else {
            $this->db->insert('safa_ea_hotels');
            return $this->db->insert_id();
        }
    }

    function delete() {
        if ($this->safa_ea_hotel !== FALSE)
            $this->db->where('safa_ea_hotels.safa_ea_hotel', $this->safa_ea_hotel);

        if ($this->erp_hotel_id !== FALSE)
            $this->db->where('safa_ea_hotels.erp_hotel_id', $this->erp_hotel_id);

        if ($this->safa_ea_id !== FALSE)
            $this->db->where('safa_ea_hotels.safa_ea_id', $this->safa_ea_id);



        $this->db->delete('safa_ea_hotels');
        return $this->db->affected_rows();
    }

    function search($rows_no = FALSE) {
        if ($this->custom_select !== FALSE) {
            $this->db->select('safa_ea_hotel');
            $this->db->select($this->custom_select);
        }

        if ($this->safa_ea_hotel !== FALSE)
            $this->db->where('safa_ea_hotels.safa_ea_hotel', $this->safa_ea_hotel);

        if ($this->erp_hotel_id !== FALSE)
            $this->db->where('safa_ea_hotels.erp_hotel_id', $this->erp_hotel_id);

        if ($this->safa_ea_id !== FALSE)
            $this->db->where('safa_ea_hotels.safa_ea_id', $this->safa_ea_id);


        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('safa_ea_hotels');
        if ($rows_no)
            return $query->num_rows();

        return $query->result();
    }

}

/* End of file ea_hotels_model.php */
/* Location: ./application/models/ea_hotels_model.php */