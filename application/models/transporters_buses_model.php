<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Transporters_buses_model extends CI_Model {

    public $safa_transporters_buses_id = FALSE;
    public $safa_transporters_id = FALSE;
    public $bus_no = FALSE;
    public $safa_buses_brands_id = FALSE;
    public $safa_buses_models_id = FALSE;
    public $industry_year = FALSE;
    public $passengers_count = FALSE;
    public $auto_generate = FALSE;
    
    public $notes = FALSE;
    public $custom_select = FALSE;
    public $limit = FALSE;
    public $offset = FALSE;
    public $order_by = FALSE;
    public $join;

    function __construct() {
        parent::__construct();
    }

    function get($rows_no = FALSE) {
        if ($this->custom_select !== FALSE) {
            $this->db->select('safa_transporters_buses_id');
            $this->db->select($this->custom_select);
        }

        if ($this->safa_transporters_buses_id !== FALSE)
            $this->db->where('safa_transporters_buses.safa_transporters_buses_id', $this->safa_transporters_buses_id);


        if ($this->bus_no !== FALSE)
            $this->db->where('safa_transporters_buses.bus_no', $this->bus_no);
        if ($this->safa_buses_brands_id !== FALSE)
            $this->db->where('safa_transporters_buses.safa_buses_brands_id', $this->safa_buses_brands_id);
        if ($this->safa_buses_models_id !== FALSE)
            $this->db->where('safa_transporters_buses.safa_buses_models_id', $this->safa_buses_models_id);
        if ($this->industry_year !== FALSE)
            $this->db->where('safa_transporters_buses.industry_year', $this->industry_year);
        if ($this->passengers_count !== FALSE)
            $this->db->where('safa_transporters_buses.passengers_count', $this->passengers_count);
        if ($this->notes !== FALSE)
            $this->db->where('safa_transporters_buses.notes', $this->notes);

        if ($this->auto_generate !== FALSE)
            $this->db->where('safa_transporters_buses.auto_generate', $this->auto_generate);
        
        if ($this->safa_transporters_id !== FALSE)
            $this->db->where('safa_transporters_buses.safa_transporters_id', $this->safa_transporters_id);

        if ($this->join) {
            $this->db->select('safa_transporters.name_ar as transporter_name_ar, safa_transporters.name_la as transporter_name_la, safa_transporters_buses.*, safa_buses_models.' . name() . ' as buses_models_name, safa_buses_brands.' . name() . ' as buses_brands_name');
            $this->db->join('safa_transporters', 'safa_transporters_buses.safa_transporters_id = safa_transporters.safa_transporter_id');
            $this->db->join('safa_buses_brands', 'safa_transporters_buses.safa_buses_brands_id = safa_buses_brands.safa_buses_brands_id', 'left');
            $this->db->join('safa_buses_models', 'safa_transporters_buses.safa_buses_models_id = safa_buses_models.safa_buses_models_id', 'left');
        }


        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('safa_transporters_buses');

        //echo $query_text=$this->db->last_query();

        if ($rows_no)
            return $query->num_rows();

        if ($this->safa_transporters_buses_id)
            return $query->row();
        else
            return $query->result();
    }

    function save() {

        if ($this->safa_transporters_buses_id !== FALSE)
            $this->db->set('safa_transporters_buses.safa_transporters_buses_id', $this->safa_transporters_buses_id);

        if ($this->bus_no !== FALSE)
            $this->db->set('safa_transporters_buses.bus_no', $this->bus_no);
        if ($this->safa_buses_brands_id !== FALSE)
            $this->db->set('safa_transporters_buses.safa_buses_brands_id', $this->safa_buses_brands_id);
        if ($this->safa_buses_models_id !== FALSE)
            $this->db->set('safa_transporters_buses.safa_buses_models_id', $this->safa_buses_models_id);
        if ($this->industry_year !== FALSE)
            $this->db->set('safa_transporters_buses.industry_year', $this->industry_year);
        if ($this->passengers_count !== FALSE)
            $this->db->set('safa_transporters_buses.passengers_count', $this->passengers_count);
        if ($this->notes !== FALSE)
            $this->db->set('safa_transporters_buses.notes', $this->notes);

		if ($this->auto_generate !== FALSE)
            $this->db->set('safa_transporters_buses.auto_generate', $this->auto_generate);
            
        if ($this->safa_transporters_id !== FALSE)
            $this->db->set('safa_transporters_buses.safa_transporters_id', $this->safa_transporters_id);


        if ($this->safa_transporters_buses_id) {
            $this->db->where('safa_transporters_buses.safa_transporters_buses_id', $this->safa_transporters_buses_id)->update('safa_transporters_buses');
        } else {
            $this->db->insert('safa_transporters_buses');
            return $this->db->insert_id();
        }
    }

    function delete() {
        if ($this->safa_transporters_buses_id !== FALSE)
            $this->db->where('safa_transporters_buses.safa_transporters_buses_id', $this->safa_transporters_buses_id);

        if ($this->bus_no !== FALSE)
            $this->db->where('safa_transporters_buses.bus_no', $this->bus_no);
        if ($this->safa_buses_brands_id !== FALSE)
            $this->db->where('safa_transporters_buses.safa_buses_brands_id', $this->safa_buses_brands_id);
        if ($this->safa_buses_models_id !== FALSE)
            $this->db->where('safa_transporters_buses.safa_buses_models_id', $this->safa_buses_models_id);
        if ($this->industry_year !== FALSE)
            $this->db->where('safa_transporters_buses.industry_year', $this->industry_year);
        if ($this->passengers_count !== FALSE)
            $this->db->where('safa_transporters_buses.passengers_count', $this->passengers_count);
        if ($this->notes !== FALSE)
            $this->db->where('safa_transporters_buses.notes', $this->notes);

        if ($this->auto_generate !== FALSE)
            $this->db->where('safa_transporters_buses.auto_generate', $this->auto_generate);
        

        if ($this->safa_transporters_id !== FALSE)
            $this->db->where('safa_transporters_buses.safa_transporters_id', $this->safa_transporters_id);


        $this->db->delete('safa_transporters_buses');
        return $this->db->affected_rows();
    }

}

/* End of file transporter_buses_model.php */
/* Location: ./application/models/transporter_buses_model.php */