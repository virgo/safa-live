<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Ea_itos_model extends CI_Model {

    public $safa_ito_id = FALSE;
    public $name_ar = FALSE;
    public $name_la = FALSE;
    public $username = FALSE;
    public $password = FALSE;
    public $erp_country_id = FALSE;
    public $reference_code = FALSE;
    public $custom_select = FALSE;
    public $limit = FALSE;
    public $offset = FALSE;
    public $order_by = FALSE;

    function __construct() {
        parent::__construct();
    }

    function get($rows_no = FALSE) {

        if ($this->custom_select !== FALSE) {
            $this->db->select('safa_ito_id');
            $this->db->select($this->custom_select);
        }

        if ($this->safa_ito_id !== FALSE)
            $this->db->where('safa_itos.safa_ito_id', $this->safa_ito_id);

        if ($this->name_ar !== FALSE)
            $this->db->where('safa_itos.name_ar', $this->name_ar);

        if ($this->name_la !== FALSE)
            $this->db->where('safa_itos.name_la', $this->name_la);

        if ($this->username !== FALSE)
            $this->db->where('safa_itos.username', $this->username);

        if ($this->password !== FALSE)
            $this->db->where('safa_itos.password', $this->password);

        if ($this->erp_country_id !== FALSE)
            $this->db->where('safa_itos.erp_country_id', $this->erp_country_id);

        if ($this->reference_code !== FALSE)
            $this->db->where('safa_itos.reference_code', $this->reference_code);


        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('safa_itos');
        if ($rows_no)
            return $query->num_rows();

        if ($this->safa_ito_id)
            return $query->row();
        else
            return $query->result();
    }

    function save() {
        if ($this->safa_ito_id !== FALSE)
            $this->db->set('safa_itos.safa_ito_id', $this->safa_ito_id);

        if ($this->name_ar !== FALSE)
            $this->db->set('safa_itos.name_ar', $this->name_ar);

        if ($this->name_la !== FALSE)
            $this->db->set('safa_itos.name_la', $this->name_la);

        if ($this->username !== FALSE)
            $this->db->set('safa_itos.username', $this->username);

        if ($this->password !== FALSE)
            $this->db->set('safa_itos.password', $this->password);

        if ($this->erp_country_id !== FALSE)
            $this->db->set('safa_itos.erp_country_id', $this->erp_country_id);

        if ($this->reference_code !== FALSE)
            $this->db->set('safa_itos.reference_code', $this->reference_code);



        if ($this->safa_ito_id) {
            $this->db->where('safa_itos.safa_ito_id', $this->safa_ito_id)->update('safa_itos');
        } else {
            $this->db->insert('safa_itos');
            return $this->db->insert_id();
        }
    }

    function delete() {
        if ($this->safa_ito_id !== FALSE)
            $this->db->where('safa_itos.safa_ito_id', $this->safa_ito_id);

        if ($this->name_ar !== FALSE)
            $this->db->where('safa_itos.name_ar', $this->name_ar);

        if ($this->name_la !== FALSE)
            $this->db->where('safa_itos.name_la', $this->name_la);

        if ($this->username !== FALSE)
            $this->db->where('safa_itos.username', $this->username);

        if ($this->password !== FALSE)
            $this->db->where('safa_itos.password', $this->password);

        if ($this->erp_country_id !== FALSE)
            $this->db->where('safa_itos.erp_country_id', $this->erp_country_id);

        if ($this->reference_code !== FALSE)
            $this->db->where('safa_itos.reference_code', $this->reference_code);



        $this->db->delete('safa_itos');
        return $this->db->affected_rows();
    }

    function search($rows_no = FALSE) {
        if ($this->custom_select !== FALSE) {
            $this->db->select('safa_ito_id');
            $this->db->select($this->custom_select);
        }

        if ($this->safa_ito_id !== FALSE)
            $this->db->where('safa_itos.safa_ito_id', $this->safa_ito_id);

        if ($this->name_ar !== FALSE)
            $this->db->where('safa_itos.name_ar', $this->name_ar);

        if ($this->name_la !== FALSE)
            $this->db->where('safa_itos.name_la', $this->name_la);

        if ($this->username !== FALSE)
            $this->db->where('safa_itos.username', $this->username);

        if ($this->password !== FALSE)
            $this->db->where('safa_itos.password', $this->password);

        if ($this->erp_country_id !== FALSE)
            $this->db->where('safa_itos.erp_country_id', $this->erp_country_id);

        if ($this->reference_code !== FALSE)
            $this->db->where('safa_itos.reference_code', $this->reference_code);


        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('safa_itos');
        if ($rows_no)
            return $query->num_rows();

        return $query->result();
    }

}

/* End of file ea_itos_model.php */
/* Location: ./application/models/ea_itos_model.php */