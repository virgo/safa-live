<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Internalsegments_drivers_and_buses_model extends CI_Model {

    public $safa_internalsegments_drivers_and_buses_id = FALSE;
    public $safa_internalsegments_id = FALSE;
    public $safa_transporters_drivers_id = FALSE;
    public $safa_transporters_buses_id = FALSE;
    public $seats_count = FALSE;
    
    public $safa_internalsegments_arr = FALSE;
    
    public $custom_select = FALSE;
    public $limit = FALSE;
    public $offset = FALSE;
    public $order_by = FALSE;

    function __construct() {
        parent::__construct();
    }

    function get($rows_no = FALSE) {
    	
    	$this->db->select('safa_internalsegments_drivers_and_buses.*, safa_transporters_buses.bus_no, safa_transporters_drivers.'.name().' as safa_transporters_driver_name, safa_transporters_drivers.phone as safa_transporters_driver_phone, , safa_buses_brands.safa_buses_brands_id, safa_buses_brands.'.name().' as safa_buses_brand_name, safa_buses_models.safa_buses_models_id, safa_buses_models.'.name().' as safa_buses_model_name');
    	
        if ($this->custom_select !== FALSE) {
            $this->db->select($this->custom_select);
        }

        
        if ($this->safa_internalsegments_drivers_and_buses_id !== FALSE)
            $this->db->where('safa_internalsegments_drivers_and_buses.safa_internalsegments_drivers_and_buses_id', $this->safa_internalsegments_drivers_and_buses_id);

        if ($this->safa_internalsegments_id !== FALSE)
            $this->db->where('safa_internalsegments_drivers_and_buses.safa_internalsegments_id', $this->safa_internalsegments_id);
        
        if ($this->safa_internalsegments_arr !== FALSE) {
            $this->db->where_in('safa_internalsegments_drivers_and_buses.safa_internalsegments_id', $this->safa_internalsegments_arr);
        }
        
        /*
          if ($this->safa_transporters_drivers_id !== FALSE)
          $this->db->where('safa_internalsegments_drivers_and_buses.safa_transporters_drivers_id', $this->safa_transporters_drivers_id);

          if ($this->safa_transporters_buses_id !== FALSE)
          $this->db->where('safa_internalsegments_drivers_and_buses.safa_transporters_buses_id', $this->safa_transporters_buses_id);
         
         if ($this->seats_count !== FALSE)
            $this->db->where('safa_internalsegments_drivers_and_buses.seats_count', $this->seats_count);
        
         
         */
        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        $this->db->from('safa_internalsegments_drivers_and_buses');
        $this->db->join('safa_transporters_drivers','safa_transporters_drivers.safa_transporters_drivers_id = safa_internalsegments_drivers_and_buses.safa_transporters_drivers_id','left');
        $this->db->join('safa_transporters_buses','safa_transporters_buses.safa_transporters_buses_id = safa_internalsegments_drivers_and_buses.safa_transporters_buses_id','left');
        $this->db->join('safa_buses_brands','safa_buses_brands.safa_buses_brands_id = safa_transporters_buses.safa_buses_brands_id','left');
        $this->db->join('safa_buses_models','safa_buses_models.safa_buses_models_id = safa_transporters_buses.safa_buses_models_id','left');
        
        
        
        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);
        $query = $this->db->get();
        if ($rows_no)
            return $query->num_rows();

        if ($this->safa_internalsegments_drivers_and_buses_id)
            return $query->row();
        else
            return $query->result();
    }

    function get_distinct_by_safa_trip_internaltrip($safa_trip_internaltrip_id) {
    	
        $this->db->distinct();
    	$this->db->select('count(safa_internalsegments_drivers_and_buses.safa_transporters_buses_id) as bus_count, sum(safa_internalsegments_drivers_and_buses.seats_count) as seats_count , safa_buses_brands.safa_buses_brands_id, safa_buses_brands.'.name().' as safa_buses_brand_name, safa_buses_models.safa_buses_models_id, safa_buses_models.'.name().' as safa_buses_model_name, safa_internalsegments.safa_trip_internaltrip_id');

    	$this->db->from('safa_internalsegments_drivers_and_buses');
    	$this->db->join('safa_internalsegments','safa_internalsegments.safa_internalsegment_id = safa_internalsegments_drivers_and_buses.safa_internalsegments_id','left');
        
        $this->db->join('safa_transporters_buses','safa_transporters_buses.safa_transporters_buses_id = safa_internalsegments_drivers_and_buses.safa_transporters_buses_id','left');
        $this->db->join('safa_buses_brands','safa_buses_brands.safa_buses_brands_id = safa_transporters_buses.safa_buses_brands_id','left');
        $this->db->join('safa_buses_models','safa_buses_models.safa_buses_models_id = safa_transporters_buses.safa_buses_models_id','left');
        
        $this->db->group_by('safa_internalsegments.safa_trip_internaltrip_id'); 
        $this->db->group_by('safa_internalsegments_drivers_and_buses.safa_transporters_buses_id'); 
        
        $this->db->having('safa_internalsegments.safa_trip_internaltrip_id', $safa_trip_internaltrip_id); 
        
        $query = $this->db->get();
       	//echo $this->db->last_query(); exit;
       	
       	return $query->result();
    }

    function get_distinct_bus_brands_by_safa_internalsegment($safa_internalsegment_id) {
    	$this->db->distinct();
    	$this->db->select('count(safa_internalsegments_drivers_and_buses.safa_transporters_buses_id) as bus_count, sum(safa_internalsegments_drivers_and_buses.seats_count) as seats_count , safa_buses_brands.safa_buses_brands_id, safa_buses_brands.'.name().' as safa_buses_brand_name, safa_buses_models.safa_buses_models_id, safa_buses_models.'.name().' as safa_buses_model_name, safa_internalsegments_drivers_and_buses.safa_internalsegments_id');

    	$this->db->from('safa_internalsegments_drivers_and_buses');
        $this->db->join('safa_transporters_buses','safa_transporters_buses.safa_transporters_buses_id = safa_internalsegments_drivers_and_buses.safa_transporters_buses_id','left');
        $this->db->join('safa_buses_brands','safa_buses_brands.safa_buses_brands_id = safa_transporters_buses.safa_buses_brands_id','left');
        $this->db->join('safa_buses_models','safa_buses_models.safa_buses_models_id = safa_transporters_buses.safa_buses_models_id','left');
        
        $this->db->group_by('safa_internalsegments_drivers_and_buses.safa_internalsegments_id'); 
        $this->db->group_by('safa_internalsegments_drivers_and_buses.safa_transporters_buses_id'); 
        
        $this->db->having('safa_internalsegments_drivers_and_buses.safa_internalsegments_id', $safa_internalsegment_id); 
        
        $query = $this->db->get();
       	//echo $this->db->last_query(); exit;
       	
       	return $query->result();
    }
    
    function save() {
        if ($this->safa_internalsegments_drivers_and_buses_id !== FALSE)
            $this->db->set('safa_internalsegments_drivers_and_buses.safa_internalsegments_drivers_and_buses_id', $this->safa_internalsegments_drivers_and_buses_id);

        if ($this->safa_internalsegments_id !== FALSE)
            $this->db->set('safa_internalsegments_drivers_and_buses.safa_internalsegments_id', $this->safa_internalsegments_id);

        if ($this->safa_transporters_drivers_id !== FALSE)
            $this->db->set('safa_internalsegments_drivers_and_buses.safa_transporters_drivers_id', $this->safa_transporters_drivers_id);

        if ($this->safa_transporters_buses_id !== FALSE)
            $this->db->set('safa_internalsegments_drivers_and_buses.safa_transporters_buses_id', $this->safa_transporters_buses_id);

		if ($this->seats_count !== FALSE)
            $this->db->set('safa_internalsegments_drivers_and_buses.seats_count', $this->seats_count);
            
        if ($this->safa_internalsegments_drivers_and_buses_id) {
            $this->db->where('safa_internalsegments_drivers_and_buses.safa_internalsegments_drivers_and_buses_id', $this->safa_internalsegments_drivers_and_buses_id)->update('safa_internalsegments_drivers_and_buses');
        } else {
            $this->db->insert('safa_internalsegments_drivers_and_buses');
            return $this->db->insert_id();
        }
    }

    function delete() {
        if ($this->safa_internalsegments_drivers_and_buses_id !== FALSE)
            $this->db->where('safa_internalsegments_drivers_and_buses.safa_internalsegments_drivers_and_buses_id', $this->safa_internalsegments_drivers_and_buses_id);

        if ($this->safa_internalsegments_id !== FALSE)
            $this->db->where('safa_internalsegments_drivers_and_buses.safa_internalsegments_id', $this->safa_internalsegments_id);

        if ($this->safa_transporters_drivers_id !== FALSE)
            $this->db->where('safa_internalsegments_drivers_and_buses.safa_transporters_drivers_id', $this->safa_transporters_drivers_id);

        if ($this->safa_transporters_buses_id !== FALSE)
            $this->db->where('safa_internalsegments_drivers_and_buses.safa_transporters_buses_id', $this->safa_transporters_buses_id);

        if ($this->seats_count !== FALSE)
            $this->db->where('safa_internalsegments_drivers_and_buses.seats_count', $this->seats_count);
        
        $this->db->delete('safa_internalsegments_drivers_and_buses');
        return $this->db->affected_rows();
    }

}

/* End of file trip_internaltrip_model.php */
/* Location: ./application/models/trip_internaltrip_model.php */
