<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Safa_ea_supervisors_model extends CI_Model {

    public $tbl_name = "safa_ea_supervisors";
    public $safa_ea_supervisor_id = FALSE;
    public $safa_ea_id = FALSE;
    public $name_ar = FALSE;
    public $name_la = FALSE;
    public $phone = FALSE;
    public $mobile = FALSE;
    public $mobile_sa = FALSE;
    public $notes = FALSE;
    public $custom_select = FALSE;
    public $limit = FALSE;
    public $offset = FALSE;
    public $order_by = FALSE;

    function __construct() {
        parent::__construct();
    }

    function save() {
        if ($this->safa_ea_supervisor_id !== FALSE) {
            $this->db->set('safa_ea_supervisors.safa_ea_supervisor_id', $this->safa_ea_supervisor_id);
        }
        if ($this->safa_ea_id !== FALSE) {
            $this->db->set('safa_ea_supervisors.safa_ea_id', $this->safa_ea_id);
        }
        if ($this->name_ar !== FALSE) {
            $this->db->set('safa_ea_supervisors.name_ar', $this->name_ar);
        }
        if ($this->name_la !== FALSE) {
            $this->db->set('safa_ea_supervisors.name_la', $this->name_la);
        }
        if ($this->phone !== FALSE) {
            $this->db->set('safa_ea_supervisors.phone', $this->phone);
        }
        if ($this->mobile !== FALSE) {
            $this->db->set('safa_ea_supervisors.mobile', $this->mobile);
        }
        if ($this->mobile_sa !== FALSE) {
            $this->db->set('safa_ea_supervisors.mobile_sa', $this->mobile_sa);
        }
        if ($this->notes !== FALSE) {
            $this->db->set('safa_ea_supervisors.notes', $this->notes);
        }

        if ($this->safa_ea_supervisor_id) {
            $this->db->where('safa_ea_supervisors.safa_ea_supervisor_id', $this->safa_ea_supervisor_id)->update('safa_ea_supervisors');
        } else {
            $this->db->insert('safa_ea_supervisors');
            return $this->db->insert_id();
        }
    }

    function delete() {
        if ($this->safa_ea_supervisor_id !== FALSE) {
            $this->db->where('safa_ea_supervisors.safa_ea_supervisor_id', $this->safa_ea_supervisor_id);
        }
        if ($this->safa_ea_id !== FALSE) {
            $this->db->where('safa_ea_supervisors.safa_ea_id', $this->safa_ea_id);
        }
        if ($this->name_ar !== FALSE) {
            $this->db->where('safa_ea_supervisors.name_ar', $this->name_ar);
        }
        if ($this->name_la !== FALSE) {
            $this->db->where('safa_ea_supervisors.name_la', $this->name_la);
        }
        if ($this->phone !== FALSE) {
            $this->db->where('safa_ea_supervisors.phone', $this->phone);
        }
        if ($this->mobile !== FALSE) {
            $this->db->where('safa_ea_supervisors.mobile', $this->mobile);
        }
        if ($this->mobile_sa !== FALSE) {
            $this->db->where('safa_ea_supervisors.mobile_sa', $this->mobile_sa);
        }
        if ($this->notes !== FALSE) {
            $this->db->where('safa_ea_supervisors.notes', $this->notes);
        }

        $this->db->delete('safa_ea_supervisors');
        return $this->db->affected_rows();
    }

    function get($rows_no = FALSE) {

        if ($this->custom_select !== FALSE) {
            $this->db->select('safa_ea_supervisor_id');
            $this->db->select($this->custom_select);
        }

        if ($this->safa_ea_supervisor_id !== FALSE) {
            $this->db->where('safa_ea_supervisors.safa_ea_supervisor_id', $this->safa_ea_supervisor_id);
        }
        if ($this->safa_ea_id !== FALSE) {
            $this->db->where('safa_ea_supervisors.safa_ea_id', $this->safa_ea_id);
        }
        if ($this->name_ar !== FALSE) {
            $this->db->where('safa_ea_supervisors.name_ar', $this->name_ar);
        }
        if ($this->name_la !== FALSE) {
            $this->db->where('safa_ea_supervisors.name_la', $this->name_la);
        }
        if ($this->phone !== FALSE) {
            $this->db->where('safa_ea_supervisors.phone', $this->phone);
        }
        if ($this->mobile !== FALSE) {
            $this->db->where('safa_ea_supervisors.mobile', $this->mobile);
        }
        if ($this->mobile_sa !== FALSE) {
            $this->db->where('safa_ea_supervisors.mobile_sa', $this->mobile_sa);
        }
        if ($this->notes !== FALSE) {
            $this->db->where('safa_ea_supervisors.notes', $this->notes);
        }

        if ($this->order_by && is_array($this->order_by)) {
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);
        } else {
            $this->db->order_by('safa_ea_supervisor_id');
        }

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('safa_ea_supervisors');
        if ($rows_no)
            return $query->num_rows();

        if ($this->safa_ea_supervisor_id)
            return $query->row();
        else
            return $query->result();
    }

}

/* End of file safa_ea_supervisors_model.php */
/* Location: ./application/models/safa_ea_supervisors_model.php */