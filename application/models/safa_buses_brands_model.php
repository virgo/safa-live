<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Safa_buses_brands_model extends CI_Model {

    public $safa_buses_brands_id = FALSE;
    public $name_ar = FALSE;
    public $name_la = FALSE;

    public $custom_select = FALSE;
    public $limit = FALSE;
    public $offset = FALSE;
    public $order_by = FALSE;
    public $join;

    function __construct() {
        parent::__construct();
    }

    function get($rows_no = FALSE) {

        if ($this->custom_select !== FALSE) {
            $this->db->select('safa_buses_brands_id');
            $this->db->select($this->custom_select);
        }

        if ($this->safa_buses_brands_id !== FALSE)
            $this->db->where('safa_buses_brands.safa_buses_brands_id', $this->safa_buses_brands_id);

        if ($this->name_ar !== FALSE)
            $this->db->where('safa_buses_brands.name_ar', $this->name_ar);

        if ($this->name_la !== FALSE)
            $this->db->where('safa_buses_brands.name_la', $this->name_la);

        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('safa_buses_brands');
        if ($rows_no)
            return $query->num_rows();

        if ($this->safa_buses_brands_id)
            return $query->row();
        else
            return $query->result();
    }

    function save() {

        if ($this->safa_buses_brands_id !== FALSE)
            $this->db->set('safa_buses_brands.safa_buses_brands_id', $this->safa_buses_brands_id);

        if ($this->name_ar !== FALSE)
            $this->db->set('safa_buses_brands.name_ar', $this->name_ar);

        if ($this->name_la !== FALSE)
            $this->db->set('safa_buses_brands.name_la', $this->name_la);


        if ($this->safa_buses_brands_id) {
            $this->db->where('safa_buses_brands.safa_buses_brands_id', $this->safa_buses_brands_id)->update('safa_buses_brands');
        } else {
            $this->db->insert('safa_buses_brands');
            return $this->db->insert_id();
        }
    }

    function delete() {

        if ($this->safa_buses_brands_id !== FALSE)
            $this->db->where('safa_buses_brands.safa_buses_brands_id', $this->safa_buses_brands_id);

        if ($this->name_ar !== FALSE)
            $this->db->where('safa_buses_brands.name_ar', $this->name_ar);

        if ($this->name_la !== FALSE)
            $this->db->where('safa_buses_brands.name_la', $this->name_la);

        $this->db->delete('safa_buses_brands');
        return $this->db->affected_rows();
    }

    function search($rows_no = FALSE) {
        if ($this->custom_select !== FALSE) {
            $this->db->select('safa_buses_brands_id');
            $this->db->select($this->custom_select);
        }

        if ($this->safa_buses_brands_id !== FALSE)
            $this->db->where('safa_buses_brands.safa_buses_brands_id', $this->safa_buses_brands_id);

        if ($this->name_ar !== FALSE)
            $this->db->where('safa_buses_brands.name_ar', $this->name_ar);

        if ($this->name_la !== FALSE)
            $this->db->where('safa_buses_brands.name_la', $this->name_la);

        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('safa_buses_brands');
        if ($rows_no)
            return $query->num_rows();

        return $query->result();
    }

}

/* End of file safa_buses_brands_model.php */
/* Location: ./application/models/safa_buses_brands_model.php */