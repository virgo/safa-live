<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Package_periods_cities_model extends CI_Model {

    public $tbl_name = "erp_package_periods_cities";
    public $erp_package_period_city_id = FALSE;
    public $erp_package_period_id = FALSE;
    public $erp_city_id = FALSE;
    public $city_days = FALSE;
    
    // By Gouda.
    public $safa_package_period_id = FALSE;
    
    public $custom_select = FALSE;
    public $limit = FALSE;
    public $offset = FALSE;
    public $order_by = FALSE;

    function __construct() {
        parent::__construct();
    }

    function save() {
        if ($this->erp_package_period_city_id !== FALSE) {
            $this->db->set('erp_package_periods_cities.erp_package_period_city_id', $this->erp_package_period_city_id);
        }
        if ($this->erp_package_period_id !== FALSE) {
            $this->db->set('erp_package_periods_cities.erp_package_period_id ', $this->erp_package_period_id);
        }
        if ($this->erp_city_id !== FALSE) {
            $this->db->set('erp_package_periods_cities.erp_city_id', $this->erp_city_id);
        }
        if ($this->city_days !== FALSE) {
            $this->db->set('erp_package_periods_cities.city_days', $this->city_days);
        }



        if ($this->erp_package_period_city_id) {
            $this->db->where('erp_package_periods_cities.erp_package_period_city_id', $this->erp_package_period_city_id)->update('erp_package_periods_cities');
        } else {
            $this->db->insert('erp_package_periods_cities');
            return $this->db->insert_id();
        }
    }

    function delete() {
        if ($this->erp_package_period_id !== FALSE) {
            $this->db->where('erp_package_periods_cities.erp_package_period_id', $this->erp_package_period_id);
        }
        if ($this->erp_package_period_city_id !== FALSE) {
            $this->db->where('erp_package_periods_cities.erp_package_period_city_id', $this->erp_package_period_city_id);
        }
        if ($this->erp_city_id !== FALSE) {
            $this->db->where('erp_package_periods_cities.erp_city_id', $this->erp_city_id);
        }
        if ($this->city_days !== FALSE) {
            $this->db->where('erp_package_periods_cities.city_days', $this->city_days);
        }

        $this->db->delete('erp_package_periods_cities');
        return $this->db->affected_rows();
    }

    function get($rows_no = FALSE) {

        if ($this->custom_select !== FALSE) {
            $this->db->select('erp_package_period_city_id');
            $this->db->select($this->custom_select);
        }

        if ($this->erp_package_period_city_id !== FALSE) {
            $this->db->where('erp_package_periods_cities.erp_package_period_city_id', $this->erp_package_period_city_id);
        }
        if ($this->erp_package_period_id !== FALSE) {
            $this->db->where('erp_package_periods_cities.erp_package_period_id', $this->erp_package_period_id);
        }
        
    	if ($this->safa_package_period_id !== FALSE) {
            $this->db->where('safa_package_periods.safa_package_periods_id', $this->safa_package_period_id);
            $this->db->join('safa_package_periods', 'safa_package_periods.erp_package_period_id = erp_package_periods_cities.erp_package_period_id', 'left');
		
        }

        if ($this->erp_city_id !== FALSE) {
            $this->db->where('erp_package_periods_cities.erp_city_id', $this->erp_city_id);
        }
        if ($this->city_days !== FALSE) {
            $this->db->where('erp_package_periods_cities.city_days', $this->city_days);
        }



        if ($this->order_by && is_array($this->order_by)) {
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);
        } else {
            $this->db->order_by('erp_package_periods_cities.erp_package_period_id');
        }

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('erp_package_periods_cities');
        if ($rows_no)
            return $query->num_rows();

        if ($this->erp_package_period_city_id)
            return $query->row();
        else
            return $query->result();
    }

}

/* End of file erp_package_periods_model.php */
/* Location: ./application/models/erp_package_periods_model.php */