<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Erp_room_execlusive_services_hm_company_model extends CI_Model {

    public $erp_room_execlusive_services_hm_company_id = FALSE;
    public $erp_room_exclusive_service_id = FALSE;
    public $safa_hm_id = FALSE;
    public $status = FALSE;
    public $custom_select = FALSE;
    public $limit = FALSE;
    public $offset = FALSE;
    public $order_by = FALSE;

    function __construct() {
        parent::__construct();
    }

    function get($rows_no = FALSE) {

        if ($this->custom_select !== FALSE) {
            $this->db->select('erp_room_execlusive_services_hm_company_id');
            $this->db->select($this->custom_select);
        }

        if ($this->erp_room_execlusive_services_hm_company_id !== FALSE)
            $this->db->where('erp_room_execlusive_services_hm_companies.erp_room_execlusive_services_hm_company_id', $this->erp_room_execlusive_services_hm_company_id);

        if ($this->erp_room_exclusive_service_id !== FALSE)
            $this->db->where('erp_room_execlusive_services_hm_companies.erp_room_exclusive_service_id', $this->erp_room_exclusive_service_id);

        if ($this->safa_hm_id !== FALSE)
            $this->db->where('erp_room_execlusive_services_hm_companies.safa_hm_id', $this->safa_hm_id);

        if ($this->status !== FALSE)
            $this->db->where('erp_room_execlusive_services_hm_companies.status', $this->status);

        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('erp_room_execlusive_services_hm_companies');
        if ($rows_no)
            return $query->num_rows();

        if ($this->erp_room_execlusive_services_hm_company_id)
            return $query->row();
        else
            return $query->result();
    }

    function save() {

        if ($this->erp_room_exclusive_service_id !== FALSE)
            $this->db->set('erp_room_execlusive_services_hm_companies.erp_room_exclusive_service_id', $this->erp_room_exclusive_service_id);

        if ($this->safa_hm_id !== FALSE)
            $this->db->set('erp_room_execlusive_services_hm_companies.safa_hm_id', $this->safa_hm_id);

        if ($this->status !== FALSE)
            $this->db->set('erp_room_execlusive_services_hm_companies.status', $this->status);


        $this->db->insert('erp_room_execlusive_services_hm_companies');
        return $this->db->insert_id();
    }

    function delete() {
        if ($this->erp_room_execlusive_services_hm_company_id !== FALSE)
            $this->db->where('erp_room_execlusive_services_hm_companies.erp_room_execlusive_services_hm_company_id', $this->erp_room_execlusive_services_hm_company_id);

        if ($this->erp_room_exclusive_service_id !== FALSE)
            $this->db->where('erp_room_execlusive_services_hm_companies.erp_room_exclusive_service_id', $this->erp_room_exclusive_service_id);

        if ($this->safa_hm_id !== FALSE)
            $this->db->where('erp_room_execlusive_services_hm_companies.safa_hm_id', $this->safa_hm_id);

        if ($this->status !== FALSE)
            $this->db->where('erp_room_execlusive_services_hm_companies.status', $this->status);



        $this->db->delete('erp_room_execlusive_services_hm_companies');
        return $this->db->affected_rows();
    }

}

/* End of file hotels_availability_details_model.php */
/* Location: ./application/models/hotels_availability_details_model.php */