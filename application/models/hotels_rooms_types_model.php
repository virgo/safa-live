<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Hotels_rooms_types_model extends CI_Model {

    public $erp_room_id = FALSE;
    public $name_ar = FALSE;
    public $name_la = FALSE;
    public $beds_number = FALSE;
    public $overload = FALSE;
    public $timestamp = FALSE;
    public $custom_select = FALSE;
    public $limit = FALSE;
    public $offset = FALSE;
    public $order_by = FALSE;

    function __construct() {
        parent::__construct();
    }

    function get($rows_no = FALSE) {

        if ($this->custom_select !== FALSE) {
            $this->db->select('erp_room_id');
            $this->db->select($this->custom_select);
        }

        if ($this->erp_room_id !== FALSE)
            $this->db->where('erp_rooms.erp_room_id', $this->erp_room_id);

        if ($this->name_ar !== FALSE)
            $this->db->where('erp_rooms.name_ar', $this->name_ar);

        if ($this->name_la !== FALSE)
            $this->db->where('erp_rooms.name_la', $this->name_la);

        if ($this->beds_number !== FALSE)
            $this->db->where('erp_rooms.beds_number', $this->beds_number);

        if ($this->overload !== FALSE)
            $this->db->where('erp_rooms.overload', $this->overload);

        if ($this->timestamp !== FALSE)
            $this->db->where('erp_rooms.timestamp', $this->timestamp);

        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);


        $query = $this->db->get('erp_rooms');
        if ($rows_no)
            return $query->num_rows();

        if ($this->erp_room_id)
            return $query->row();
        else
            return $query->result();
    }

    function save() {

        if ($this->erp_room_id !== FALSE)
            $this->db->set('erp_rooms.erp_room_id', $this->erp_room_id);

        if ($this->name_ar !== FALSE)
            $this->db->set('erp_rooms.name_ar', $this->name_ar);

        if ($this->name_la !== FALSE)
            $this->db->set('erp_rooms.name_la', $this->name_la);

        if ($this->beds_number !== FALSE)
            $this->db->set('erp_rooms.beds_number', $this->beds_number);

        if ($this->overload !== FALSE)
            $this->db->set('erp_rooms.overload', $this->overload);

        if ($this->timestamp !== FALSE)
            $this->db->set('erp_rooms.timestamp', $this->timestamp);


        if ($this->erp_room_id) {
            $this->db->where('erp_rooms.erp_room_id ', $this->erp_room_id)->update('erp_rooms');
        } else {
            $this->db->insert('erp_rooms');
            return $this->db->insert_id();
        }
    }

    function delete() {

        if ($this->erp_room_id !== FALSE)
            $this->db->where('erp_rooms.erp_room_id', $this->erp_room_id);

        if ($this->name_ar !== FALSE)
            $this->db->where('erp_rooms.name_ar', $this->name_ar);

        if ($this->name_la !== FALSE)
            $this->db->where('erp_rooms.name_la', $this->name_la);

        if ($this->beds_number !== FALSE)
            $this->db->where('erp_rooms.beds_number', $this->beds_number);

        if ($this->overload !== FALSE)
            $this->db->where('erp_rooms.overload', $this->overload);

        if ($this->timestamp !== FALSE)
            $this->db->where('erp_rooms.timestamp', $this->timestamp);

        $this->db->delete('erp_rooms');
        return $this->db->affected_rows();
    }

    function search($rows_no = FALSE) {
        if ($this->custom_select !== FALSE) {
            $this->db->select('erp_rooms');
            $this->db->select($this->custom_select);
        }

        if ($this->erp_hotel_sub_levels_id !== FALSE)
            $this->db->where('erp_hotel_sub_levels.erp_hotel_sub_levels_id', $this->erp_hotel_sub_levels_id);

        if ($this->name_ar !== FALSE)
            $this->db->where('erp_hotel_sub_levels.name_ar', $this->name_ar);

        if ($this->name_la !== FALSE)
            $this->db->where('erp_hotel_sub_levels.name_la', $this->name_la);

        if ($this->beds_number !== FALSE)
            $this->db->where('erp_rooms.beds_number', $this->beds_number);

        if ($this->overload !== FALSE)
            $this->db->where('erp_rooms.overload', $this->overload);


        if ($this->timestamp !== FALSE)
            $this->db->where('erp_rooms.timestamp', $this->timestamp);

        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('erp_rooms');
        if ($rows_no)
            return $query->num_rows();

        return $query->result();
    }

}

/* End of file hotels_rooms_types.php */
/* Location: ./application/models/hotels_rooms_types.php */