<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Safa_languages_phrases_model extends CI_Model {

    public $safa_language_phrase_id = FALSE;
    public $safa_language_key_id = FALSE;
    public $safa_language_id = FALSE;
    public $phrase = FALSE;
    public $custom_select = FALSE;
    public $limit = FALSE;
    public $offset = FALSE;
    public $order_by = FALSE;

    function __construct() {
        parent::__construct();
    }

    function get($rows_no = FALSE) {

        if ($this->custom_select !== FALSE) {
            $this->db->select('safa_language_phrase_id');
            $this->db->select($this->custom_select);
        }

        if ($this->safa_language_phrase_id !== FALSE)
            $this->db->where('safa_languages_phrases.safa_language_phrase_id', $this->safa_language_phrase_id);

        if ($this->safa_language_key_id !== FALSE)
            $this->db->where('safa_languages_phrases.safa_language_key_id', $this->safa_language_key_id);

        if ($this->safa_language_id !== FALSE)
            $this->db->where('safa_languages_phrases.safa_language_id', $this->safa_language_id);

        if ($this->phrase !== FALSE)
            $this->db->where('safa_languages_phrases.phrase', $this->phrase);


        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('safa_languages_phrases');
        if ($rows_no)
            return $query->num_rows();

        if ($this->safa_language_phrase_id)
            return $query->row();
        else
            return $query->result();
    }

    function save() {
        $this->safa_language_phrase_id = $this->is_new();

        if ($this->safa_language_phrase_id !== FALSE)
            $this->db->set('safa_languages_phrases.safa_language_phrase_id', $this->safa_language_phrase_id);

        if ($this->safa_language_key_id !== FALSE)
            $this->db->set('safa_languages_phrases.safa_language_key_id', $this->safa_language_key_id);

        if ($this->safa_language_id !== FALSE)
            $this->db->set('safa_languages_phrases.safa_language_id', $this->safa_language_id);

        if ($this->phrase !== FALSE)
            $this->db->set('safa_languages_phrases.phrase', $this->phrase);



        if ($this->safa_language_phrase_id) {
            $this->db->where('safa_languages_phrases.safa_language_phrase_id', $this->safa_language_phrase_id)->update('safa_languages_phrases');
        } else {
            $this->db->insert('safa_languages_phrases');
            return $this->db->insert_id();
        }
    }

    function is_new() {
        if ($this->safa_language_key_id && $this->safa_language_id) {
            $row = $this->db->where(
                            array("safa_languages_phrases.safa_language_key_id" => $this->safa_language_key_id,
                                "safa_languages_phrases.safa_language_id" => $this->safa_language_id)
                    )->get('safa_languages_phrases')->row();
            if ($row)
                return $row->safa_language_phrase_id;
        }

        return false;
    }

    function delete() {
        if ($this->safa_language_phrase_id !== FALSE)
            $this->db->where('safa_languages_phrases.safa_language_phrase_id', $this->safa_language_phrase_id);

        if ($this->safa_language_key_id !== FALSE)
            $this->db->where('safa_languages_phrases.safa_language_key_id', $this->safa_language_key_id);

        if ($this->safa_language_id !== FALSE)
            $this->db->where('safa_languages_phrases.safa_language_id', $this->safa_language_id);

        if ($this->phrase !== FALSE)
            $this->db->where('safa_languages_phrases.phrase', $this->phrase);



        $this->db->delete('safa_languages_phrases');
        return $this->db->affected_rows();
    }

    function search($rows_no = FALSE) {
        if ($this->custom_select !== FALSE) {
            $this->db->select('safa_language_phrase_id');
            $this->db->select($this->custom_select);
        }

        if ($this->safa_language_phrase_id !== FALSE)
            $this->db->where('safa_languages_phrases.safa_language_phrase_id', $this->safa_language_phrase_id);

        if ($this->safa_language_key_id !== FALSE)
            $this->db->where('safa_languages_phrases.safa_language_key_id', $this->safa_language_key_id);

        if ($this->safa_language_id !== FALSE)
            $this->db->where('safa_languages_phrases.safa_language_id', $this->safa_language_id);

        if ($this->phrase !== FALSE)
            $this->db->where('safa_languages_phrases.phrase', $this->phrase);


        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);


        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('safa_languages_phrases');
        if ($rows_no)
            return $query->num_rows();

        return $query->result();
    }

}

/* End of file safa_languages_phrases_model.php */
/* Location: ./application/models/safa_languages_phrases_model.php */