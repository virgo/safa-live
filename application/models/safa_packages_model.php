<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Safa_packages_model extends CI_Model {

    public $safa_package_id = FALSE;
    public $erp_company_type_id = FALSE;
    public $erp_company_id = FALSE;
    public $package_code = FALSE;
    public $date_type = FALSE;
    public $start_date = FALSE;
    public $end_date = FALSE;
    public $name_ar = FALSE;
    public $name_la = FALSE;
    public $sale_currency_id = FALSE;
    public $local_currency_id = FALSE;   
    public $service_cost = FALSE;   
    public $round_num = FALSE;   
    public $convert = FALSE;   
    public $safa_packages_transportation_type_id = FALSE;
    public $remarks = FALSE;
    public $active = FALSE;
    public $private = FALSE;
    public $custom_select = FALSE;
    public $limit = FALSE;
    public $offset = FALSE;
    public $order_by = FALSE;

    function __construct() {
        parent::__construct();
    }

    function get($rows_no = FALSE) {

        if ($this->custom_select !== FALSE) {
            $this->db->select('safa_package_id');
            $this->db->select($this->custom_select);
        }

        if ($this->safa_package_id !== FALSE)
            $this->db->where('safa_packages.safa_package_id', $this->safa_package_id);

        if ($this->erp_company_type_id !== FALSE)
            $this->db->where('safa_packages.erp_company_type_id', $this->erp_company_type_id);

        if ($this->erp_company_id !== FALSE)
            $this->db->where('safa_packages.erp_company_id', $this->erp_company_id);

        if ($this->package_code !== FALSE)
            $this->db->where('safa_packages.package_code', $this->package_code);

        if ($this->date_type !== FALSE)
            $this->db->where('safa_packages.date_type', $this->date_type);

        if ($this->start_date !== FALSE)
            $this->db->where('safa_packages.start_date', $this->start_date);

        if ($this->end_date !== FALSE)
            $this->db->where('safa_packages.end_date', $this->end_date);

        if ($this->name_ar !== FALSE)
            $this->db->where('safa_packages.name_ar', $this->name_ar);

        if ($this->name_la !== FALSE)
            $this->db->where('safa_packages.name_la', $this->name_la);

        if ($this->sale_currency_id !== FALSE)
            $this->db->where('safa_packages.sale_currency_id', $this->sale_currency_id);

        if ($this->safa_packages_transportation_type_id !== FALSE)
            $this->db->where('safa_packages.safa_packages_transportation_type_id', $this->safa_packages_transportation_type_id);

        if ($this->remarks !== FALSE)
            $this->db->where('safa_packages.remarks', $this->remarks);

        if ($this->active !== FALSE)
            $this->db->where('safa_packages.active', $this->active);

        if ($this->private !== FALSE)
            $this->db->where('safa_packages.private', $this->private);
        
        if ($this->local_currency_id !== FALSE)
            $this->db->where('safa_packages.local_currency_id', $this->local_currency_id);
        
        if ($this->service_cost !== FALSE)
            $this->db->where('safa_packages.service_cost', $this->service_cost);
        
        if ($this->round_num !== FALSE)
            $this->db->where('safa_packages.round_num', $this->round_num);
        
        if ($this->convert !== FALSE)
            $this->db->where('safa_packages.convert', $this->convert);
        

        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('safa_packages');
        if ($rows_no)
            return $query->num_rows();

        if ($this->safa_package_id)
            return $query->row();
        else
            return $query->result();
    }

    function save() {
        if ($this->safa_package_id !== FALSE)
            $this->db->set('safa_packages.safa_package_id', $this->safa_package_id);

        if ($this->erp_company_type_id !== FALSE)
            $this->db->set('safa_packages.erp_company_type_id', $this->erp_company_type_id);

        if ($this->erp_company_id !== FALSE)
            $this->db->set('safa_packages.erp_company_id', $this->erp_company_id);

        if ($this->package_code !== FALSE)
            $this->db->set('safa_packages.package_code', $this->package_code);

        if ($this->date_type !== FALSE)
            $this->db->set('safa_packages.date_type', $this->date_type);

        if ($this->start_date !== FALSE)
            $this->db->set('safa_packages.start_date', $this->start_date);

        if ($this->end_date !== FALSE)
            $this->db->set('safa_packages.end_date', $this->end_date);

        if ($this->name_ar !== FALSE)
            $this->db->set('safa_packages.name_ar', $this->name_ar);

        if ($this->name_la !== FALSE)
            $this->db->set('safa_packages.name_la', $this->name_la);

        if ($this->sale_currency_id !== FALSE)
            $this->db->set('safa_packages.sale_currency_id', $this->sale_currency_id);

        if ($this->safa_packages_transportation_type_id !== FALSE)
            $this->db->set('safa_packages.safa_packages_transportation_type_id', $this->safa_packages_transportation_type_id);

        if ($this->remarks !== FALSE)
            $this->db->set('safa_packages.remarks', $this->remarks);

        if ($this->active !== FALSE)
            $this->db->set('safa_packages.active', $this->active);

        if ($this->local_currency_id !== FALSE)
            $this->db->set('safa_packages.local_currency_id', $this->local_currency_id);
        
        if ($this->service_cost !== FALSE)
            $this->db->set('safa_packages.service_cost', $this->service_cost);
        
        if ($this->round_num !== FALSE)
            $this->db->set('safa_packages.round_num', $this->round_num);
        
        if ($this->convert !== FALSE)
            $this->db->set('safa_packages.convert', $this->convert);


        if ($this->safa_package_id) {
            $this->db->where('safa_packages.safa_package_id', $this->safa_package_id)->update('safa_packages');
        } else {
            $this->db->insert('safa_packages');
            return $this->db->insert_id();
        }
    }

    function delete() {
        if ($this->safa_package_id !== FALSE)
            $this->db->where('safa_packages.safa_package_id', $this->safa_package_id);

        if ($this->erp_company_type_id !== FALSE)
            $this->db->where('safa_packages.erp_company_type_id', $this->erp_company_type_id);

        if ($this->erp_company_id !== FALSE)
            $this->db->where('safa_packages.erp_company_id', $this->erp_company_id);

        if ($this->package_code !== FALSE)
            $this->db->where('safa_packages.package_code', $this->package_code);

        if ($this->date_type !== FALSE)
            $this->db->where('safa_packages.date_type', $this->date_type);

        if ($this->start_date !== FALSE)
            $this->db->where('safa_packages.start_date', $this->start_date);

        if ($this->end_date !== FALSE)
            $this->db->where('safa_packages.end_date', $this->end_date);

        if ($this->name_ar !== FALSE)
            $this->db->where('safa_packages.name_ar', $this->name_ar);

        if ($this->name_la !== FALSE)
            $this->db->where('safa_packages.name_la', $this->name_la);

        if ($this->sale_currency_id !== FALSE)
            $this->db->where('safa_packages.sale_currency_id', $this->sale_currency_id);

        if ($this->safa_packages_transportation_type_id !== FALSE)
            $this->db->where('safa_packages.safa_packages_transportation_type_id', $this->safa_packages_transportation_type_id);

        if ($this->remarks !== FALSE)
            $this->db->where('safa_packages.remarks', $this->remarks);

        if ($this->active !== FALSE)
            $this->db->where('safa_packages.active', $this->active);

        if ($this->local_currency_id !== FALSE)
            $this->db->where('safa_packages.local_currency_id', $this->local_currency_id);
        
        if ($this->service_cost !== FALSE)
            $this->db->where('safa_packages.service_cost', $this->service_cost);
        
        if ($this->round_num !== FALSE)
            $this->db->where('safa_packages.round_num', $this->round_num);
        
        if ($this->convert !== FALSE)
            $this->db->where('safa_packages.convert', $this->convert);


        $this->db->delete('safa_packages');
        return $this->db->affected_rows();
    }

    function get_by_uo_contract($uo_contract_id = FALSE) {
        $country = $this->db->where_in('safa_uo_contract_id', $uo_contract_id)->get('safa_uo_contracts')->row();
        $countryid = $country->country_id;
        $uo_id = $country->safa_uo_id;
        $query = "select safa_packages.* FROM safa_packages
		LEFT JOIN safa_packages_contracts ON safa_packages.safa_package_id = safa_packages_contracts.safa_package_id
		LEFT JOIN safa_packages_countries ON safa_packages.safa_package_id = safa_packages_countries.safa_package_id
		WHERE (
		safa_packages_contracts.safa_package_id IS NULL OR 
		( safa_packages_contracts.safa_uo_contract_id = '$uo_contract_id' and safa_packages_contracts.`status` = 1 ) AND NOT
		( safa_packages_contracts.safa_uo_contract_id = '$uo_contract_id' and safa_packages_contracts.`status` = 0 )) AND (
                    safa_packages_countries.safa_package_id IS NULL OR
		( safa_packages_countries.erp_country_id = '$countryid' and safa_packages_countries.`status` = 1) AND NOT
		( safa_packages_countries.erp_country_id = '$countryid' and safa_packages_countries.`status` = 0)
		) AND safa_packages.erp_company_id = '$uo_id' AND safa_packages.erp_company_type_id = 2 ";
        $packages = $this->db->query($query)->result();
        return $packages;
    }
    
    function get_package_hotels($package_id){
        $query = "
SELECT
    `safa_package_hotels`.`erp_hotel_id`, 
    `erp_hotels`.`erp_city_id`,`erp_hotels`.`".name()."` as hotel_name ,
    `erp_cities`.`".name()."` as city_name
FROM
    `safa_package_hotels`
    INNER JOIN .`erp_hotels` 
        ON (`safa_package_hotels`.`erp_hotel_id` = `erp_hotels`.`erp_hotel_id`)
    INNER JOIN .`erp_cities` 
        ON (`erp_cities`.`erp_city_id` = `erp_hotels`.`erp_city_id`)
WHERE `safa_package_hotels`.`safa_package_id` = '$package_id'";
        $results = $this->db->query($query)->result();
        
        return $results;
    }
}

/* End of file uo_packages_model.php */
/* Location: ./application/models/uo_packages_model.php */