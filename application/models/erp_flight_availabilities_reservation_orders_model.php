<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Erp_flight_availabilities_reservation_orders_model extends CI_Model {
    
    public $tbl_name = "erp_flight_availabilities_reservation_orders";
    
    public $erp_flight_availabilities_reservation_order_id = FALSE;
	public $reservation_order_date = FALSE;
	public $season_id = FALSE;
	public $required_seats_count = FALSE;
	public $reservation_order_status = FALSE;
	public $erp_class_id = FALSE;
	public $reservation_remarks = FALSE;
	public $erp_transporter_branches_id = FALSE;
	public $safa_trip_id = FALSE;
	public $safa_externaltriptype_id = FALSE;
	public $safa_externalsegmenttype_id = FALSE;
	public $erp_airline_branch_id = FALSE;

    public $custom_select = FALSE;
    public $limit = FALSE;
    public $offset = FALSE;
    public $order_by = FALSE;            
    
    function __construct() 
    {
        parent::__construct();
    }

	function save() 
	{
        if ($this->erp_flight_availabilities_reservation_order_id !== FALSE) {
            $this->db->set('erp_flight_availabilities_reservation_orders.erp_flight_availabilities_reservation_order_id', $this->erp_flight_availabilities_reservation_order_id);
        }
		if ($this->reservation_order_date !== FALSE) {
            $this->db->set('erp_flight_availabilities_reservation_orders.reservation_order_date', $this->reservation_order_date);
        }
		if ($this->season_id !== FALSE) {
            $this->db->set('erp_flight_availabilities_reservation_orders.season_id', $this->season_id);
        }
		if ($this->required_seats_count !== FALSE) {
            $this->db->set('erp_flight_availabilities_reservation_orders.required_seats_count', $this->required_seats_count);
        }
		if ($this->reservation_order_status !== FALSE) {
            $this->db->set('erp_flight_availabilities_reservation_orders.reservation_order_status', $this->reservation_order_status);
        }
		if ($this->erp_class_id !== FALSE) {
            $this->db->set('erp_flight_availabilities_reservation_orders.erp_class_id', $this->erp_class_id);
        }
		if ($this->reservation_remarks !== FALSE) {
            $this->db->set('erp_flight_availabilities_reservation_orders.reservation_remarks', $this->reservation_remarks);
        }
		if ($this->erp_transporter_branches_id !== FALSE) {
            $this->db->set('erp_flight_availabilities_reservation_orders.erp_transporter_branches_id', $this->erp_transporter_branches_id);
        }
		if ($this->safa_trip_id !== FALSE) {
            $this->db->set('erp_flight_availabilities_reservation_orders.safa_trip_id', $this->safa_trip_id);
        }
		if ($this->safa_externaltriptype_id !== FALSE) {
            $this->db->set('erp_flight_availabilities_reservation_orders.safa_externaltriptype_id', $this->safa_externaltriptype_id);
        }
		if ($this->safa_externalsegmenttype_id !== FALSE) {
            $this->db->set('erp_flight_availabilities_reservation_orders.safa_externalsegmenttype_id', $this->safa_externalsegmenttype_id);
        }
		if ($this->erp_airline_branch_id !== FALSE) {
            $this->db->set('erp_flight_availabilities_reservation_orders.erp_airline_branch_id', $this->erp_airline_branch_id);
        }
		
        if ($this->erp_flight_availabilities_reservation_order_id) {
            $this->db->where('erp_flight_availabilities_reservation_orders.erp_flight_availabilities_reservation_order_id', $this->erp_flight_availabilities_reservation_order_id)->update('erp_flight_availabilities_reservation_orders');
        } else {
            $this->db->insert('erp_flight_availabilities_reservation_orders');
            return $this->db->insert_id();
        }
    }
    
	function delete() 
	{
		if ($this->erp_flight_availabilities_reservation_order_id !== FALSE) {
            $this->db->where('erp_flight_availabilities_reservation_orders.erp_flight_availabilities_reservation_order_id', $this->erp_flight_availabilities_reservation_order_id);
        }
		if ($this->reservation_order_date !== FALSE) {
            $this->db->where('erp_flight_availabilities_reservation_orders.reservation_order_date', $this->reservation_order_date);
        }
		if ($this->season_id !== FALSE) {
            $this->db->where('erp_flight_availabilities_reservation_orders.season_id', $this->season_id);
        }
		if ($this->required_seats_count !== FALSE) {
            $this->db->where('erp_flight_availabilities_reservation_orders.required_seats_count', $this->required_seats_count);
        }
		if ($this->reservation_order_status !== FALSE) {
            $this->db->where('erp_flight_availabilities_reservation_orders.reservation_order_status', $this->reservation_order_status);
        }
		if ($this->erp_class_id !== FALSE) {
            $this->db->where('erp_flight_availabilities_reservation_orders.erp_class_id', $this->erp_class_id);
        }
		if ($this->reservation_remarks !== FALSE) {
            $this->db->where('erp_flight_availabilities_reservation_orders.reservation_remarks', $this->reservation_remarks);
        }
		if ($this->erp_transporter_branches_id !== FALSE) {
            $this->db->where('erp_flight_availabilities_reservation_orders.erp_transporter_branches_id', $this->erp_transporter_branches_id);
        }
		if ($this->safa_trip_id !== FALSE) {
            $this->db->where('erp_flight_availabilities_reservation_orders.safa_trip_id', $this->safa_trip_id);
        }
		if ($this->safa_externaltriptype_id !== FALSE) {
            $this->db->where('erp_flight_availabilities_reservation_orders.safa_externaltriptype_id', $this->safa_externaltriptype_id);
        }
		if ($this->safa_externalsegmenttype_id !== FALSE) {
            $this->db->where('erp_flight_availabilities_reservation_orders.safa_externalsegmenttype_id', $this->safa_externalsegmenttype_id);
        }
		if ($this->erp_airline_branch_id !== FALSE) {
            $this->db->where('erp_flight_availabilities_reservation_orders.erp_airline_branch_id', $this->erp_airline_branch_id);
        }
        
        $this->db->delete('erp_flight_availabilities_reservation_orders');
        return $this->db->affected_rows();
    }
    
	function get($rows_no = FALSE) 
	{
        
        if ($this->custom_select !== FALSE) {
            $this->db->select('erp_flight_availabilities_reservation_order_id');
            $this->db->select($this->custom_select);
        }
        
		if ($this->erp_flight_availabilities_reservation_order_id !== FALSE) {
            $this->db->where('erp_flight_availabilities_reservation_orders.erp_flight_availabilities_reservation_order_id', $this->erp_flight_availabilities_reservation_order_id);
        }
		if ($this->reservation_order_date !== FALSE) {
            $this->db->where('erp_flight_availabilities_reservation_orders.reservation_order_date', $this->reservation_order_date);
        }
		if ($this->season_id !== FALSE) {
            $this->db->where('erp_flight_availabilities_reservation_orders.season_id', $this->season_id);
        }
		if ($this->required_seats_count !== FALSE) {
            $this->db->where('erp_flight_availabilities_reservation_orders.required_seats_count', $this->required_seats_count);
        }
		if ($this->reservation_order_status !== FALSE) {
            $this->db->where('erp_flight_availabilities_reservation_orders.reservation_order_status', $this->reservation_order_status);
        }
		if ($this->erp_class_id !== FALSE) {
            $this->db->where('erp_flight_availabilities_reservation_orders.erp_class_id', $this->erp_class_id);
        }
		if ($this->reservation_remarks !== FALSE) {
            $this->db->where('erp_flight_availabilities_reservation_orders.reservation_remarks', $this->reservation_remarks);
        }
		if ($this->erp_transporter_branches_id !== FALSE) {
            $this->db->where('erp_flight_availabilities_reservation_orders.erp_transporter_branches_id', $this->erp_transporter_branches_id);
        }
		if ($this->safa_trip_id !== FALSE) {
            $this->db->where('erp_flight_availabilities_reservation_orders.safa_trip_id', $this->safa_trip_id);
        }
		if ($this->safa_externaltriptype_id !== FALSE) {
            $this->db->where('erp_flight_availabilities_reservation_orders.safa_externaltriptype_id', $this->safa_externaltriptype_id);
        }
		if ($this->safa_externalsegmenttype_id !== FALSE) {
            $this->db->where('erp_flight_availabilities_reservation_orders.safa_externalsegmenttype_id', $this->safa_externalsegmenttype_id);
        }
		if ($this->erp_airline_branch_id !== FALSE) {
            $this->db->where('erp_flight_availabilities_reservation_orders.erp_airline_branch_id', $this->erp_airline_branch_id);
        }
            
        if ($this->order_by && is_array($this->order_by)) {
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);
        } else {
        	$this->db->order_by('erp_flight_availabilities_reservation_order_id');
        }
        
        if ( ! $rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('erp_flight_availabilities_reservation_orders');
        if ($rows_no)
            return $query->num_rows();

        if ($this->erp_flight_availabilities_reservation_order_id)
            return $query->row();
        else
            return $query->result();
    }
    
}

/* End of file erp_flight_availabilities_reservation_orders_model.php */
/* Location: ./application/models/erp_flight_availabilities_reservation_orders_model.php */