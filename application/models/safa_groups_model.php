<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Safa_groups_model extends CI_Model {

    public $safa_group_id = FALSE;
    public $name = FALSE;
    public $creation_date = FALSE;
    public $from_creation_date = FALSE;
    public $to_creation_date = FALSE;
    public $safa_ea_id = FALSE;
    public $custom_select = FALSE;
    public $limit = FALSE;
    public $offset = FALSE;
    public $order_by = FALSE;
    public $join = FALSE;
    public $passport_no = FALSE;
    public $first_name_la = FALSE;
    public $second_name_la = FALSE;
    public $third_name_la = FALSE;
    public $ea_id = FALSE;
    public $no_of_mutamers_from = FALSE;
    public $no_of_mutamers_to = FALSE;

    function __construct() {
        parent::__construct();
    }

    function get($rows_no = FALSE) {

        if ($this->custom_select !== FALSE) {
            $this->db->select('safa_group_id');
            $this->db->select($this->custom_select);
        }

        if ($this->safa_group_id !== FALSE)
            $this->db->where('safa_groups.safa_group_id', $this->safa_group_id);

        if ($this->name !== FALSE)
            $this->db->where('safa_groups.name', $this->name);

        if ($this->creation_date !== FALSE)
            $this->db->where('safa_groups.creation_date', $this->creation_date);

        if ($this->from_creation_date !== FALSE)
            $this->db->where('safa_groups.creation_date >=', $this->from_creation_date);

        if ($this->to_creation_date !== FALSE)
            $this->db->where('safa_groups.creation_date <=', $this->to_creation_date);

        if ($this->safa_ea_id !== FALSE)
            $this->db->where('safa_groups.safa_ea_id', $this->safa_ea_id);

        if ($this->passport_no !== FALSE)
            $this->db->where('safa_group_passports.passport_no', $this->passport_no);

        if ($this->first_name_la !== FALSE)
            $this->db->where('safa_group_passports.first_name_la', $this->first_name_la);

        if ($this->second_name_la !== FALSE)
            $this->db->where('safa_group_passports.second_name_la', $this->second_name_la);

        if ($this->third_name_la !== FALSE)
            $this->db->where('safa_group_passports.third_name_la', $this->third_name_la);




        if ($this->order_by && is_array($this->order_by)) {
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);
        } else {
            $this->db->order_by('safa_groups.creation_date', 'desc');
        }

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $this->db->select('safa_groups.*,count(safa_group_passports.safa_group_id)
                 as no_of_mutamers');
        $this->db->join('safa_group_passports', 'safa_groups.safa_group_id = safa_group_passports.safa_group_id', 'left');
        $this->db->where('safa_groups.safa_ea_id', $this->ea_id); ///from session
        $this->db->group_by('safa_groups.safa_group_id');

        //By Gouda
        if ($this->no_of_mutamers_from !== FALSE)
            $this->db->having('no_of_mutamers >=', $this->no_of_mutamers_from);
        if ($this->no_of_mutamers_to !== FALSE)
            $this->db->having('no_of_mutamers <=', $this->no_of_mutamers_to);



        $query = $this->db->get('safa_groups');

        //echo $query_text=$this->db->last_query(); exit;

        if ($rows_no)
            return $query->num_rows();

        if ($this->safa_group_id)
            return $query->row();
        else
            return $query->result();
    }

    function save() {
        if ($this->safa_group_id !== FALSE)
            $this->db->set('safa_groups.safa_group_id', $this->safa_group_id);

        if ($this->name !== FALSE)
            $this->db->set('safa_groups.name', $this->name);

        if ($this->creation_date !== FALSE)
            $this->db->set('safa_groups.creation_date', $this->creation_date);

        if ($this->safa_ea_id !== FALSE)
            $this->db->set('safa_groups.safa_ea_id', $this->safa_ea_id);



        if ($this->safa_group_id) {
            $this->db->where('safa_groups.safa_group_id', $this->safa_group_id)->update('safa_groups');
        } else {
            $this->db->insert('safa_groups');
            return $this->db->insert_id();
        }
    }

    function delete() {
        if ($this->safa_group_id !== FALSE)
            $this->db->where('safa_groups.safa_group_id', $this->safa_group_id);

        if ($this->name !== FALSE)
            $this->db->where('safa_groups.name', $this->name);

        if ($this->creation_date !== FALSE)
            $this->db->where('safa_groups.creation_date', $this->creation_date);

        if ($this->safa_ea_id !== FALSE)
            $this->db->where('safa_groups.safa_ea_id', $this->safa_ea_id);



        $this->db->delete('safa_groups');
        return $this->db->affected_rows();
    }

    function search($rows_no = FALSE) {
        if ($this->custom_select !== FALSE) {
            $this->db->select('safa_group_id');
            $this->db->select($this->custom_select);
        }

        if ($this->safa_group_id !== FALSE)
            $this->db->where('safa_groups.safa_group_id', $this->safa_group_id);

        if ($this->name !== FALSE)
            $this->db->where('safa_groups.name', $this->name);

        if ($this->creation_date !== FALSE)
            $this->db->where('safa_groups.creation_date', $this->creation_date);

        if ($this->from_creation_date !== FALSE)
            $this->db->where('safa_groups.creation_date >=', $this->from_creation_date);

        if ($this->to_creation_date !== FALSE)
            $this->db->where('safa_groups.creation_date <=', $this->to_creation_date);

        if ($this->safa_ea_id !== FALSE)
            $this->db->where('safa_groups.safa_ea_id', $this->safa_ea_id);


        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('safa_groups');
        if ($rows_no)
            return $query->num_rows();

        return $query->result();
    }

    function check_delete_ability($id) {
        $this->db->select('count(safa_group_passports.safa_group_id)as safa_groups,');
        $this->db->from('safa_groups');
        $this->db->join('safa_group_passports', 'safa_groups.safa_group_id = safa_group_passports.safa_group_id', 'left');
        $this->db->group_by('safa_groups.safa_group_id');
        $this->db->where('safa_groups.safa_group_id', $id);
        $query = $this->db->get();
        $flag = 0;

        foreach ($query->result() as $row => $table) {
            foreach ($table as $col => $value) {
                if ($value > 0) {
                    $flag = 1;
                    break;
                } else {
                    continue;
                }
            }
        }
        return $flag;
    }

    function get_passport_no($passport_no) {
        $this->db->select('passport_no');
        $this->db->from('safa_group_passports');
        $this->db->like('passport_no', $passport_no);
//        $this->db->limit('5');

        $query = $this->db->get();
//        print_r($query);
        exit();
    }

}

/* End of file safa_groups_model.php */
/* Location: ./application/models/ea_usergroups_model.php */
