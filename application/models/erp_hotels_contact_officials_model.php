<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Erp_hotels_contact_officials_model extends CI_Model {

    public $erp_hotels_contact_officials_id = FALSE;
    public $position = FALSE;
    public $name = FALSE;
    public $email = FALSE;
    public $phone = FALSE;
    public $skype = FALSE;
    public $messenger = FALSE;
    public $erp_hotel_id = FALSE;
    public $erp_phone_type_id = FALSE;
    public $custom_select = FALSE;
    public $limit = FALSE;
    public $offset = FALSE;
    public $order_by = FALSE;

    function __construct() {
        parent::__construct();
    }

    function get($rows_no = FALSE) {

        if ($this->custom_select !== FALSE) {
            $this->db->select('erp_hotels_contact_officials_id');
            $this->db->select($this->custom_select);
        }

        if ($this->erp_hotels_contact_officials_id !== FALSE)
            $this->db->where('erp_hotels_contact_officials.erp_hotels_contact_officials_id', $this->erp_hotels_contact_officials_id);

        if ($this->position !== FALSE)
            $this->db->where('erp_hotels_contact_officials.position', $this->position);

        if ($this->name !== FALSE)
            $this->db->where('erp_hotels_contact_officials.name', $this->name);

        if ($this->email !== FALSE)
            $this->db->where('erp_hotels_contact_officials.email', $this->email);

        if ($this->phone !== FALSE)
            $this->db->where('erp_hotels_contact_officials.phone', $this->phone);

        if ($this->skype !== FALSE)
            $this->db->where('erp_hotels_contact_officials.skype', $this->skype);

        if ($this->messenger !== FALSE)
            $this->db->where('erp_hotels_contact_officials.messenger', $this->messenger);

        if ($this->erp_hotel_id !== FALSE)
            $this->db->where('erp_hotels_contact_officials.erp_hotel_id', $this->erp_hotel_id);

        if ($this->erp_phone_type_id !== FALSE)
            $this->db->where('erp_hotels_contact_officials.erp_phone_type_id', $this->erp_phone_type_id);


        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('erp_hotels_contact_officials');
        if ($rows_no)
            return $query->num_rows();

        if ($this->erp_hotels_contact_officials_id)
            return $query->row();
        else
            return $query->result();
    }

    function save() {
        if ($this->erp_hotels_contact_officials_id !== FALSE)
            $this->db->set('erp_hotels_contact_officials.erp_hotels_contact_officials_id', $this->erp_hotels_contact_officials_id);

        if ($this->position !== FALSE)
            $this->db->set('erp_hotels_contact_officials.position', $this->position);

        if ($this->name !== FALSE)
            $this->db->set('erp_hotels_contact_officials.name', $this->name);

        if ($this->email !== FALSE)
            $this->db->set('erp_hotels_contact_officials.email', $this->email);

        if ($this->phone !== FALSE)
            $this->db->set('erp_hotels_contact_officials.phone', $this->phone);

        if ($this->skype !== FALSE)
            $this->db->set('erp_hotels_contact_officials.skype', $this->skype);

        if ($this->messenger !== FALSE)
            $this->db->set('erp_hotels_contact_officials.messenger', $this->messenger);

        if ($this->erp_hotel_id !== FALSE)
            $this->db->set('erp_hotels_contact_officials.erp_hotel_id', $this->erp_hotel_id);

        if ($this->erp_phone_type_id !== FALSE)
            $this->db->set('erp_hotels_contact_officials.erp_phone_type_id', $this->erp_phone_type_id);



        if ($this->erp_hotels_contact_officials_id) {
            $this->db->where('erp_hotels_contact_officials.erp_hotels_contact_officials_id', $this->erp_hotels_contact_officials_id)->update('erp_hotels_contact_officials');
        } else {
            $this->db->insert('erp_hotels_contact_officials');
            return $this->db->insert_id();
        }
    }

    function delete() {
        if ($this->erp_hotels_contact_officials_id !== FALSE)
            $this->db->where('erp_hotels_contact_officials.erp_hotels_contact_officials_id', $this->erp_hotels_contact_officials_id);

        if ($this->position !== FALSE)
            $this->db->where('erp_hotels_contact_officials.position', $this->position);

        if ($this->name !== FALSE)
            $this->db->where('erp_hotels_contact_officials.name', $this->name);

        if ($this->email !== FALSE)
            $this->db->where('erp_hotels_contact_officials.email', $this->email);

        if ($this->phone !== FALSE)
            $this->db->where('erp_hotels_contact_officials.phone', $this->phone);

        if ($this->skype !== FALSE)
            $this->db->where('erp_hotels_contact_officials.skype', $this->skype);

        if ($this->messenger !== FALSE)
            $this->db->where('erp_hotels_contact_officials.messenger', $this->messenger);

        if ($this->erp_hotel_id !== FALSE)
            $this->db->where('erp_hotels_contact_officials.erp_hotel_id', $this->erp_hotel_id);

        if ($this->erp_phone_type_id !== FALSE)
            $this->db->where('erp_hotels_contact_officials.erp_phone_type_id', $this->erp_phone_type_id);



        $this->db->delete('erp_hotels_contact_officials');
        return $this->db->affected_rows();
    }

    function search($rows_no = FALSE) {
        if ($this->custom_select !== FALSE) {
            $this->db->select('erp_hotels_contact_officials_id');
            $this->db->select($this->custom_select);
        }

        if ($this->erp_hotels_contact_officials_id !== FALSE)
            $this->db->where('erp_hotels_contact_officials.erp_hotels_contact_officials_id', $this->erp_hotels_contact_officials_id);

        if ($this->position !== FALSE)
            $this->db->where('erp_hotels_contact_officials.position', $this->position);

        if ($this->name !== FALSE)
            $this->db->where('erp_hotels_contact_officials.name', $this->name);

        if ($this->email !== FALSE)
            $this->db->where('erp_hotels_contact_officials.email', $this->email);

        if ($this->phone !== FALSE)
            $this->db->where('erp_hotels_contact_officials.phone', $this->phone);

        if ($this->skype !== FALSE)
            $this->db->where('erp_hotels_contact_officials.skype', $this->skype);

        if ($this->messenger !== FALSE)
            $this->db->where('erp_hotels_contact_officials.messenger', $this->messenger);

        if ($this->erp_hotel_id !== FALSE)
            $this->db->where('erp_hotels_contact_officials.erp_hotel_id', $this->erp_hotel_id);

        if ($this->erp_phone_type_id !== FALSE)
            $this->db->where('erp_hotels_contact_officials.erp_phone_type_id', $this->erp_phone_type_id);


        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('erp_hotels_contact_officials');
        if ($rows_no)
            return $query->num_rows();

        return $query->result();
    }

}

/* End of file erp_contact_officials_model.php */
/* Location: ./application/models/erp_contact_officials_model.php */