<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Package_tourismplaces_model extends CI_Model {

    public $safa_package_tourismplace_id = FALSE;
    public $safa_tourismplace_id = FALSE;
    public $safa_package_id = FALSE;
    public $custom_select = FALSE;
    public $limit = FALSE;
    public $offset = FALSE;
    public $order_by = FALSE;

    function __construct() {
        parent::__construct();
    }

    function get($rows_no = FALSE) {

        if ($this->custom_select !== FALSE) {
            $this->db->select('safa_package_tourismplace_id');
            $this->db->select($this->custom_select);
        }

        if ($this->safa_package_tourismplace_id !== FALSE)
            $this->db->where('safa_package_tourismplaces.safa_package_tourismplace_id', $this->safa_package_tourismplace_id);

        if ($this->safa_tourismplace_id !== FALSE)
            $this->db->where('safa_package_tourismplaces.safa_tourismplace_id', $this->safa_tourismplace_id);

        if ($this->safa_package_id !== FALSE)
            $this->db->where('safa_package_tourismplaces.safa_package_id', $this->safa_package_id);


        $this->db->select('safa_package_tourismplaces.*,safa_tourismplaces.erp_city_id, erp_cities.' . name() . ' as erp_city_name, safa_tourismplaces.' . name() . ' as safa_tourismplace_name ');
        $this->db->join('safa_tourismplaces', 'safa_tourismplaces.safa_tourismplace_id = safa_package_tourismplaces.safa_tourismplace_id', 'left');
        $this->db->join('erp_cities', 'erp_cities.erp_city_id = safa_tourismplaces.erp_city_id', 'left');


        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('safa_package_tourismplaces');
        if ($rows_no)
            return $query->num_rows();

        if ($this->safa_package_tourismplace_id)
            return $query->row();
        else
            return $query->result();
    }

    function save() {
        
        if ($this->safa_package_tourismplace_id !== FALSE)
            $this->db->set('safa_package_tourismplaces.safa_package_tourismplace_id', $this->safa_package_tourismplace_id);

        if ($this->safa_tourismplace_id !== FALSE)
            $this->db->set('safa_package_tourismplaces.safa_tourismplace_id', $this->safa_tourismplace_id);

        if ($this->safa_package_id !== FALSE)
            $this->db->set('safa_package_tourismplaces.safa_package_id', $this->safa_package_id);

        if ($this->safa_package_tourismplace_id) {
            $this->db->where('safa_package_tourismplaces.safa_package_tourismplace_id', $this->safa_package_tourismplace_id)->update('safa_package_tourismplaces');
        } else {
            $this->db->insert('safa_package_tourismplaces');
            return $this->db->insert_id();
        }
    }

    function delete() {
        if ($this->safa_package_tourismplace_id !== FALSE)
            $this->db->where('safa_package_tourismplaces.safa_package_tourismplace_id', $this->safa_package_tourismplace_id);

        if ($this->safa_tourismplace_id !== FALSE)
            $this->db->where('safa_package_tourismplaces.safa_tourismplace_id', $this->safa_tourismplace_id);

        if ($this->safa_package_id !== FALSE)
            $this->db->where('safa_package_tourismplaces.safa_package_id', $this->safa_package_id);


        $this->db->delete('safa_package_tourismplaces');
        return $this->db->affected_rows();
    }

    function search($rows_no = FALSE) {
        if ($this->custom_select !== FALSE) {
            $this->db->select('safa_package_tourismplace_id');
            $this->db->select($this->custom_select);
        }

        if ($this->safa_package_tourismplace_id !== FALSE)
            $this->db->where('safa_package_tourismplaces.safa_package_tourismplace_id', $this->safa_package_tourismplace_id);

        if ($this->safa_tourismplace_id !== FALSE)
            $this->db->where('safa_package_tourismplaces.safa_tourismplace_id', $this->safa_tourismplace_id);

        if ($this->safa_package_id !== FALSE)
            $this->db->where('safa_package_tourismplaces.safa_package_id', $this->safa_package_id);


        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('safa_package_tourismplaces');
        if ($rows_no)
            return $query->num_rows();

        return $query->result();
    }

}

/* End of file uo_package_tourismplaces_model.php */
/* Location: ./application/models/uo_package_tourismplaces_model.php */