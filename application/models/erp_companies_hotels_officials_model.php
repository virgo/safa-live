<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Erp_companies_hotels_officials_model extends CI_Model {

    public $erp_companies_hotels_officials_id = FALSE;
    public $position = FALSE;
    public $name = FALSE;
    public $email = FALSE;
    public $phone = FALSE;
    public $skype = FALSE;
    public $messenger = FALSE;
    public $phone_type_id = FALSE;
    public $erp_companies_hotels_id = FALSE;
    public $custom_select = FALSE;
    public $limit = FALSE;
    public $offset = FALSE;
    public $order_by = FALSE;

    function __construct() {
        parent::__construct();
    }

    function get($rows_no = FALSE) {

        if ($this->custom_select !== FALSE) {
            $this->db->select('erp_companies_hotels_officials_id');
            $this->db->select($this->custom_select);
        }

        if ($this->erp_companies_hotels_officials_id !== FALSE)
            $this->db->where('erp_companies_hotels_officials.erp_companies_hotels_officials_id', $this->erp_companies_hotels_officials_id);

        if ($this->position !== FALSE)
            $this->db->where('erp_companies_hotels_officials.position', $this->position);

        if ($this->name !== FALSE)
            $this->db->where('erp_companies_hotels_officials.name', $this->name);

        if ($this->phone !== FALSE)
            $this->db->where('erp_companies_hotels_officials.phone', $this->phone);

        if ($this->skype !== FALSE)
            $this->db->where('erp_companies_hotels_officials.skype', $this->skype);

        if ($this->email !== FALSE)
            $this->db->where('erp_companies_hotels_officials.email', $this->email);

        if ($this->messenger !== FALSE)
            $this->db->where('erp_companies_hotels_officials.messenger', $this->messenger);

        if ($this->phone_type_id !== FALSE)
            $this->db->where('erp_companies_hotels_officials.phone_type_id', $this->phone_type_id);

        if ($this->erp_companies_hotels_id !== FALSE)
            $this->db->where('erp_companies_hotels_officials.erp_companies_hotels_id', $this->erp_companies_hotels_id);


        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('erp_companies_hotels_officials');
        if ($rows_no)
            return $query->num_rows();

        if ($this->erp_companies_hotels_officials_id)
            return $query->row();
        else
            return $query->result();
    }

    function save() {
        if ($this->erp_companies_hotels_officials_id !== FALSE)
            $this->db->set('erp_companies_hotels_officials.erp_companies_hotels_officials_id', $this->erp_companies_hotels_officials_id);

        if ($this->position !== FALSE)
            $this->db->set('erp_companies_hotels_officials.position', $this->position);

        if ($this->name !== FALSE)
            $this->db->set('erp_companies_hotels_officials.name', $this->name);

        if ($this->phone !== FALSE)
            $this->db->set('erp_companies_hotels_officials.phone', $this->phone);

        if ($this->skype !== FALSE)
            $this->db->set('erp_companies_hotels_officials.skype', $this->skype);

        if ($this->email !== FALSE)
            $this->db->set('erp_companies_hotels_officials.email', $this->email);

        if ($this->messenger !== FALSE)
            $this->db->set('erp_companies_hotels_officials.messenger', $this->messenger);

        if ($this->phone_type_id !== FALSE)
            $this->db->set('erp_companies_hotels_officials.phone_type_id', $this->phone_type_id);

        if ($this->erp_companies_hotels_id !== FALSE)
            $this->db->set('erp_companies_hotels_officials.erp_companies_hotels_id', $this->erp_companies_hotels_id);



        if ($this->erp_companies_hotels_officials_id) {
            $this->db->where('erp_companies_hotels_officials.erp_companies_hotels_officials_id', $this->erp_companies_hotels_officials_id)->update('erp_companies_hotels_officials');
        } else {
            $this->db->insert('erp_companies_hotels_officials');
            return $this->db->insert_id();
        }
    }

    function delete() {
        if ($this->erp_companies_hotels_officials_id !== FALSE)
            $this->db->where('erp_companies_hotels_officials.erp_companies_hotels_officials_id', $this->erp_companies_hotels_officials_id);

        if ($this->position !== FALSE)
            $this->db->where('erp_companies_hotels_officials.position', $this->position);

        if ($this->name !== FALSE)
            $this->db->where('erp_companies_hotels_officials.name', $this->name);

        if ($this->email !== FALSE)
            $this->db->where('erp_companies_hotels_officials.email', $this->email);

        if ($this->phone !== FALSE)
            $this->db->where('erp_companies_hotels_officials.phone', $this->phone);

        if ($this->skype !== FALSE)
            $this->db->where('erp_companies_hotels_officials.skype', $this->skype);

        if ($this->messenger !== FALSE)
            $this->db->where('erp_companies_hotels_officials.messenger', $this->messenger);

        if ($this->phone_type_id !== FALSE)
            $this->db->where('erp_companies_hotels_officials.phone_type_id', $this->phone_type_id);

        if ($this->erp_companies_hotels_id !== FALSE)
            $this->db->where('erp_companies_hotels_officials.erp_companies_hotels_id', $this->erp_companies_hotels_id);



        $this->db->delete('erp_companies_hotels_officials');
        return $this->db->affected_rows();
    }

    function search($rows_no = FALSE) {
        if ($this->custom_select !== FALSE) {
            $this->db->select('erp_companies_hotels_officials_id');
            $this->db->select($this->custom_select);
        }

        if ($this->erp_companies_hotels_officials_id !== FALSE)
            $this->db->where('erp_companies_hotels_officials.erp_companies_hotels_officials_id', $this->erp_companies_hotels_officials_id);

        if ($this->position !== FALSE)
            $this->db->where('erp_companies_hotels_officials.position', $this->position);

        if ($this->email !== FALSE)
            $this->db->where('erp_companies_hotels_officials.email', $this->email);

        if ($this->name !== FALSE)
            $this->db->where('erp_companies_hotels_officials.name', $this->name);

        if ($this->phone !== FALSE)
            $this->db->where('erp_companies_hotels_officials.phone', $this->phone);

        if ($this->skype !== FALSE)
            $this->db->where('erp_companies_hotels_officials.skype', $this->skype);

        if ($this->messenger !== FALSE)
            $this->db->where('erp_companies_hotels_officials.messenger', $this->messenger);

        if ($this->phone_type_id !== FALSE)
            $this->db->where('erp_companies_hotels_officials.phone_type_id', $this->phone_type_id);

        if ($this->erp_companies_hotels_id !== FALSE)
            $this->db->where('erp_companies_hotels_officials.erp_companies_hotels_id', $this->erp_companies_hotels_id);


        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('erp_companies_hotels_officials');
        if ($rows_no)
            return $query->num_rows();

        return $query->result();
    }

    /* count for the refrance */

    function count_details() {
        if ($this->erp_companies_hotels_id != false)
            $this->db->where('erp_companies_hotels_id', $this->erp_companies_hotels_id);
        return $this->db->from('erp_companies_hotels_officials')->count_all_results();
    }

}

/* End of file erp_companies_hotels_officials_model.php */
/* Location: ./application/models/erp_companies_hotels_officials_model.php */