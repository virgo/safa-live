<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Trip_externalsegments_availabilities_model extends CI_Model {

    public $safa_trip_externalsegments_availability_id = FALSE;
    public $erp_flight_availabilities_detail_id = FALSE;
    public $safa_trip_externalsegment_id = FALSE;
    public $seats_used_count = FALSE;
    public $custom_select = FALSE;
    public $limit = FALSE;
    public $offset = FALSE;
    public $order_by = FALSE;

    function __construct() {
        parent::__construct();
    }

    function get($rows_no = FALSE) {

        if ($this->custom_select !== FALSE) {
            $this->db->select('safa_trip_externalsegments_availability_id');
            $this->db->select($this->custom_select);
        }

        if ($this->safa_trip_externalsegments_availability_id !== FALSE)
            $this->db->where('safa_trip_externalsegments_availabilities.safa_trip_externalsegments_availability_id', $this->safa_trip_externalsegments_availability_id);

        if ($this->erp_flight_availabilities_detail_id !== FALSE)
            $this->db->where('safa_trip_externalsegments_availabilities.erp_flight_availabilities_detail_id', $this->erp_flight_availabilities_detail_id);

        if ($this->safa_trip_externalsegment_id !== FALSE)
            $this->db->where('safa_trip_externalsegments_availabilities.safa_trip_externalsegment_id', $this->safa_trip_externalsegment_id);

        if ($this->seats_used_count !== FALSE)
            $this->db->where('safa_trip_externalsegments_availabilities.seats_used_count', $this->seats_used_count);



        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('safa_trip_externalsegments_availabilities');
        if ($rows_no)
            return $query->num_rows();

        if ($this->safa_trip_externalsegments_availability_id)
            return $query->row();
        else
            return $query->result();
    }

}

/* End of file trip_externalpassages_model.php */
/* Location: ./application/models/trip_externalpassages_model.php */