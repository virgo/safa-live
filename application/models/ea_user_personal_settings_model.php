<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Ea_user_personal_settings_model extends CI_Model {

    public $safa_ea_user_id = FALSE;
    public $username = FALSE;
    public $password = FALSe;
    public $reset_password = FALSE;
    public $timestamp = FALSE;
    public $limit = FALSE;
    public $offset = FALSE;

    function __construct() {
        parent::__construct();
    }

    function get($rows_no = FALSE) {

        if ($this->safa_ea_user_id !== FALSE)
            $this->db->where('safa_ea_users.safa_ea_user_id', $this->safa_ea_user_id);

        if ($this->username !== FALSE)
            $this->db->where('safa_ea_users.username', $this->username);

        if ($this->password !== FALSE)
            $this->db->where('safa_ea_users.password', $this->password);

        if ($this->reset_password)
            $this->db->where('safa_ea_users.reset_password', $this->reset_password);

        if ($this->timestamp)
            $this->db->where('safa_ea_users.timestamp', $this->timestamp);


        $query = $this->db->get('safa_ea_users', $this->limit, $this->offset);
        if ($rows_no)
            return $query->num_rows();

        if ($this->safa_ea_user_id)
            return $query->row();
        else
            return $query->result();
    }

    function save() {

        if ($this->safa_ea_user_id !== FALSE)
            $this->db->set('safa_ea_users.safa_ea_user_id', $this->safa_ea_user_id);

        if ($this->username !== FALSE)
            $this->db->set('safa_ea_users.username', $this->username);

        if ($this->password !== FALSE)
            $this->db->set('safa_ea_users.password', $this->password);

        if ($this->reset_password)
            $this->db->set('safa_ea_users.reset_password', $this->reset_password);

        if ($this->timestamp)
            $this->db->set('safa_ea_users.timestamp', $this->timestamp);


        if ($this->safa_ea_user_id) {
            $this->db->where('safa_ea_user_id', $this->safa_ea_user_id)->update('safa_ea_users');
            return $this->safa_ea_user_id;
        } else {
            $this->db->insert('safa_ea_users');
            return $this->db->insert_id();
        }
    }

    function delete() {

        if ($this->safa_ea_user_id !== FALSE)
            $this->db->set('safa_ea_users.safa_ea_user_id', $this->safa_ea_user_id);

        if ($this->username !== FALSE)
            $this->db->set('safa_ea_users.username', $this->username);

        if ($this->password !== FALSE)
            $this->db->set('safa_ea_users.password', $this->password);

        if ($this->reset_password)
            $this->db->set('safa_ea_users.reset_password', $this->reset_password);

        if ($this->timestamp)
            $this->db->set('safa_ea_users.timestamp', $this->timestamp);


        $this->db->delete('safa_ea_users');
        return $this->db->affected_rows();
    }

}

/* End of file user_personal_settings.php */
/* Location: ./application/models/user_personal_seetings_model.php */