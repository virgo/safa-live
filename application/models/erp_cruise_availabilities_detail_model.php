<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Erp_cruise_availabilities_detail_model extends CI_Model 
{

    public $erp_cruise_availabilities_detail_id = FALSE;  
    public $erp_cruise_availability_id = FALSE;
	public $erp_path_type_id = FALSE;
	public $safa_externaltriptype_id = FALSE;
	public $cruise_number = FALSE;
	public $erp_cruise_class_id = FALSE;
	public $cruise_date = FALSE;
	public $arrival_date = FALSE;
	public $erp_port_id_from = FALSE;
	public $erp_port_id_to = FALSE;
	public $erp_cruise_status_id = FALSE;
	public $seats_count = FALSE;
	public $departure_time = FALSE;
	public $arrival_time = FALSE;
	public $safa_transporter_code = FALSE;
    
    public $custom_select = FALSE;
    public $limit = FALSE;
    public $offset = FALSE;
    public $order_by = FALSE;

    function __construct() {
        parent::__construct();
    }

    function get($rows_no = FALSE) 
    {

        if ($this->custom_select !== FALSE) {
            $this->db->select('erp_cruise_availabilities_detail_id');
            $this->db->select($this->custom_select);
        }

        $this->db->select("erp_cruise_availabilities_detail.*, erp_path_types.".name()." as erp_path_type_name, start_ports." . name() . " as start_ports_name, end_ports." . name() . " as end_ports_name,  safa_transporters.code as safa_transporter_code,  safa_transporters." . name() . " as safa_transporter_name", FALSE);
        
        
        if ($this->erp_cruise_availabilities_detail_id !== FALSE)
            $this->db->where('erp_cruise_availabilities_detail.erp_cruise_availabilities_detail_id', $this->erp_cruise_availabilities_detail_id);

        if ($this->erp_cruise_availability_id !== FALSE)
            $this->db->where('erp_cruise_availabilities_detail.erp_cruise_availability_id', $this->erp_cruise_availability_id);

       	if ($this->erp_path_type_id !== FALSE)
            $this->db->where('erp_cruise_availabilities_detail.erp_path_type_id', $this->erp_path_type_id);

       	if ($this->safa_externaltriptype_id !== FALSE)
            $this->db->where('erp_cruise_availabilities_detail.safa_externaltriptype_id', $this->safa_externaltriptype_id);

       	if ($this->cruise_number !== FALSE)
            $this->db->where('erp_cruise_availabilities_detail.cruise_number', $this->cruise_number);

       	if ($this->erp_cruise_class_id !== FALSE)
            $this->db->where('erp_cruise_availabilities_detail.erp_cruise_class_id', $this->erp_cruise_class_id);

       	if ($this->cruise_date !== FALSE)
            $this->db->where('erp_cruise_availabilities_detail.cruise_date', $this->cruise_date);

       	if ($this->arrival_date !== FALSE)
            $this->db->where('erp_cruise_availabilities_detail.arrival_date', $this->arrival_date);

       	if ($this->erp_port_id_from !== FALSE)
            $this->db->where('erp_cruise_availabilities_detail.erp_port_id_from', $this->erp_port_id_from);

       	if ($this->erp_port_id_to !== FALSE)
            $this->db->where('erp_cruise_availabilities_detail.erp_port_id_to', $this->erp_port_id_to);

       	if ($this->erp_cruise_status_id !== FALSE)
            $this->db->where('erp_cruise_availabilities_detail.erp_cruise_status_id', $this->erp_cruise_status_id);

       	if ($this->seats_count !== FALSE)
            $this->db->where('erp_cruise_availabilities_detail.seats_count', $this->seats_count);

       	if ($this->departure_time !== FALSE)
            $this->db->where('erp_cruise_availabilities_detail.departure_time', $this->departure_time);

       	if ($this->arrival_time !== FALSE)
            $this->db->where('erp_cruise_availabilities_detail.arrival_time', $this->arrival_time);

       	if ($this->safa_transporter_code !== FALSE)
            $this->db->where('erp_cruise_availabilities_detail.safa_transporter_code', $this->safa_transporter_code);


        $this->db->join('erp_ports as start_ports', 'erp_cruise_availabilities_detail.erp_port_id_from = start_ports.erp_port_id', 'left');
        $this->db->join('erp_ports as end_ports', 'erp_cruise_availabilities_detail.erp_port_id_to = end_ports.erp_port_id', 'left');
        $this->db->join('erp_path_types',  ' erp_path_types.erp_path_type_id  = erp_cruise_availabilities_detail.erp_path_type_id', 'left');
        $this->db->join('safa_transporters', 'erp_cruise_availabilities_detail.safa_transporter_code = safa_transporters.code', 'left');
        
        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('erp_cruise_availabilities_detail');
        if ($rows_no)
            return $query->num_rows();

        if ($this->erp_cruise_availabilities_detail_id)
            return $query->row();
        else
            return $query->result();
    }

    function save() 
    {
        if ($this->erp_cruise_availabilities_detail_id !== FALSE)
            $this->db->set('erp_cruise_availabilities_detail.erp_cruise_availabilities_detail_id', $this->erp_cruise_availabilities_detail_id);

        if ($this->erp_cruise_availability_id !== FALSE)
            $this->db->set('erp_cruise_availabilities_detail.erp_cruise_availability_id', $this->erp_cruise_availability_id);

       	if ($this->erp_path_type_id !== FALSE)
            $this->db->set('erp_cruise_availabilities_detail.erp_path_type_id', $this->erp_path_type_id);

       	if ($this->safa_externaltriptype_id !== FALSE)
            $this->db->set('erp_cruise_availabilities_detail.safa_externaltriptype_id', $this->safa_externaltriptype_id);

       	if ($this->cruise_number !== FALSE)
            $this->db->set('erp_cruise_availabilities_detail.cruise_number', $this->cruise_number);

       	if ($this->erp_cruise_class_id !== FALSE)
            $this->db->set('erp_cruise_availabilities_detail.erp_cruise_class_id', $this->erp_cruise_class_id);

       	if ($this->cruise_date !== FALSE)
            $this->db->set('erp_cruise_availabilities_detail.cruise_date', $this->cruise_date);

       	if ($this->arrival_date !== FALSE)
            $this->db->set('erp_cruise_availabilities_detail.arrival_date', $this->arrival_date);

       	if ($this->erp_port_id_from !== FALSE)
            $this->db->set('erp_cruise_availabilities_detail.erp_port_id_from', $this->erp_port_id_from);

       	if ($this->erp_port_id_to !== FALSE)
            $this->db->set('erp_cruise_availabilities_detail.erp_port_id_to', $this->erp_port_id_to);

       	if ($this->erp_cruise_status_id !== FALSE)
            $this->db->set('erp_cruise_availabilities_detail.erp_cruise_status_id', $this->erp_cruise_status_id);

       	if ($this->seats_count !== FALSE)
            $this->db->set('erp_cruise_availabilities_detail.seats_count', $this->seats_count);

       	if ($this->departure_time !== FALSE)
            $this->db->set('erp_cruise_availabilities_detail.departure_time', $this->departure_time);

       	if ($this->arrival_time !== FALSE)
            $this->db->set('erp_cruise_availabilities_detail.arrival_time', $this->arrival_time);

       	if ($this->safa_transporter_code !== FALSE)
            $this->db->set('erp_cruise_availabilities_detail.safa_transporter_code', $this->safa_transporter_code);
            


        if ($this->erp_cruise_availabilities_detail_id) {
            $this->db->where('erp_cruise_availabilities_detail.erp_cruise_availabilities_detail_id', $this->erp_cruise_availabilities_detail_id)->update('erp_cruise_availabilities_detail');
        } else {
            $this->db->insert('erp_cruise_availabilities_detail');
            return $this->db->insert_id();
        }
    }

    function delete() 
    {
        if ($this->erp_cruise_availabilities_detail_id !== FALSE)
            $this->db->where('erp_cruise_availabilities_detail.erp_cruise_availabilities_detail_id', $this->erp_cruise_availabilities_detail_id);

        if ($this->erp_cruise_availability_id !== FALSE)
            $this->db->where('erp_cruise_availabilities_detail.erp_cruise_availability_id', $this->erp_cruise_availability_id);

       	if ($this->erp_path_type_id !== FALSE)
            $this->db->where('erp_cruise_availabilities_detail.erp_path_type_id', $this->erp_path_type_id);

       	if ($this->safa_externaltriptype_id !== FALSE)
            $this->db->where('erp_cruise_availabilities_detail.safa_externaltriptype_id', $this->safa_externaltriptype_id);

       	if ($this->cruise_number !== FALSE)
            $this->db->where('erp_cruise_availabilities_detail.cruise_number', $this->cruise_number);

       	if ($this->erp_cruise_class_id !== FALSE)
            $this->db->where('erp_cruise_availabilities_detail.erp_cruise_class_id', $this->erp_cruise_class_id);

       	if ($this->cruise_date !== FALSE)
            $this->db->where('erp_cruise_availabilities_detail.cruise_date', $this->cruise_date);

       	if ($this->arrival_date !== FALSE)
            $this->db->where('erp_cruise_availabilities_detail.arrival_date', $this->arrival_date);

       	if ($this->erp_port_id_from !== FALSE)
            $this->db->where('erp_cruise_availabilities_detail.erp_port_id_from', $this->erp_port_id_from);

       	if ($this->erp_port_id_to !== FALSE)
            $this->db->where('erp_cruise_availabilities_detail.erp_port_id_to', $this->erp_port_id_to);

       	if ($this->erp_cruise_status_id !== FALSE)
            $this->db->where('erp_cruise_availabilities_detail.erp_cruise_status_id', $this->erp_cruise_status_id);

       	if ($this->seats_count !== FALSE)
            $this->db->where('erp_cruise_availabilities_detail.seats_count', $this->seats_count);

       	if ($this->departure_time !== FALSE)
            $this->db->where('erp_cruise_availabilities_detail.departure_time', $this->departure_time);

       	if ($this->arrival_time !== FALSE)
            $this->db->where('erp_cruise_availabilities_detail.arrival_time', $this->arrival_time);

       	if ($this->safa_transporter_code !== FALSE)
            $this->db->where('erp_cruise_availabilities_detail.safa_transporter_code', $this->safa_transporter_code);
            

        $this->db->delete('erp_cruise_availabilities_detail');
        return $this->db->affected_rows();
    }

}

/* End of file erp_cruise_availabilities_detail_model_model.php */
/* Location: ./application/models/erp_cruise_availabilities_detail_model.php */