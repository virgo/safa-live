<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Uasp_model extends CI_Model {

    public $erp_uasp_id = FALSE;
    public $name_ar = FALSE;
    public $name_la = FALSE;
    public $notes = FALSE;
    public $email = FALSE;
    public $website = FALSE;
    public $responsible = FALSE;
    public $custom_select = FALSE;
    public $limit = FALSE;
    public $offset = FALSE;
    public $order_by = FALSE;

    function __construct() {
        parent::__construct();
    }

    function get($rows_no = FALSE) {

        if ($this->custom_select !== FALSE) {
            $this->db->select('erp_uasp_id');
            $this->db->select($this->custom_select);
        }

        if ($this->erp_uasp_id !== FALSE)
            $this->db->where('erp_uasp.erp_uasp_id', $this->erp_uasp_id);

        if ($this->name_ar !== FALSE)
            $this->db->where('erp_uasp.name_ar', $this->name_ar);

        if ($this->name_la !== FALSE)
            $this->db->where('erp_uasp.name_la', $this->name_la);

        if ($this->notes !== FALSE)
            $this->db->where('erp_uasp.notes', $this->notes);

        if ($this->email !== FALSE)
            $this->db->where('erp_uasp.email', $this->email);

        if ($this->website !== FALSE)
            $this->db->where('erp_uasp.website', $this->website);

        if ($this->responsible !== FALSE)
            $this->db->where('erp_uasp.responsible', $this->responsible);


        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('erp_uasp');
        if ($rows_no)
            return $query->num_rows();

        if ($this->erp_uasp_id)
            return $query->row();
        else
            return $query->result();
    }

    function save() {
        if ($this->erp_uasp_id !== FALSE)
            $this->db->set('erp_uasp.erp_uasp_id', $this->erp_uasp_id);

        if ($this->name_ar !== FALSE)
            $this->db->set('erp_uasp.name_ar', $this->name_ar);

        if ($this->name_la !== FALSE)
            $this->db->set('erp_uasp.name_la', $this->name_la);

        if ($this->notes !== FALSE)
            $this->db->set('erp_uasp.notes', $this->notes);

        if ($this->email !== FALSE)
            $this->db->set('erp_uasp.email', $this->email);

        if ($this->website !== FALSE)
            $this->db->set('erp_uasp.website', $this->website);

        if ($this->responsible !== FALSE)
            $this->db->set('erp_uasp.responsible', $this->responsible);



        if ($this->erp_uasp_id) {
            $this->db->where('erp_uasp.erp_uasp_id', $this->erp_uasp_id)->update('erp_uasp');
        } else {
            $this->db->insert('erp_uasp');
            return $this->db->insert_id();
        }
    }

    function delete() {
        if ($this->erp_uasp_id !== FALSE)
            $this->db->where('erp_uasp.erp_uasp_id', $this->erp_uasp_id);

        if ($this->name_ar !== FALSE)
            $this->db->where('erp_uasp.name_ar', $this->name_ar);

        if ($this->name_la !== FALSE)
            $this->db->where('erp_uasp.name_la', $this->name_la);

        if ($this->notes !== FALSE)
            $this->db->where('erp_uasp.notes', $this->notes);

        if ($this->email !== FALSE)
            $this->db->where('erp_uasp.email', $this->email);

        if ($this->website !== FALSE)
            $this->db->where('erp_uasp.website', $this->website);

        if ($this->responsible !== FALSE)
            $this->db->where('erp_uasp.responsible', $this->responsible);



        $this->db->delete('erp_uasp');
        return $this->db->affected_rows();
    }

    function search($rows_no = FALSE) {
        if ($this->custom_select !== FALSE) {
            $this->db->select('erp_uasp_id');
            $this->db->select($this->custom_select);
        }

        if ($this->erp_uasp_id !== FALSE)
            $this->db->where('erp_uasp.erp_uasp_id', $this->erp_uasp_id);

        if ($this->name_ar !== FALSE)
            $this->db->where('erp_uasp.name_ar', $this->name_ar);

        if ($this->name_la !== FALSE)
            $this->db->where('erp_uasp.name_la', $this->name_la);

        if ($this->notes !== FALSE)
            $this->db->where('erp_uasp.notes', $this->notes);

        if ($this->email !== FALSE)
            $this->db->where('erp_uasp.email', $this->email);

        if ($this->website !== FALSE)
            $this->db->where('erp_uasp.website', $this->website);

        if ($this->responsible !== FALSE)
            $this->db->where('erp_uasp.responsible', $this->responsible);


        if ($this->order_by && is_array($this->order_by))
            $this->db->order_by($this->order_by['0'], $this->order_by['1']);

        if (!$rows_no && $this->limit)
            $this->db->limit($this->limit, $this->offset);

        $query = $this->db->get('erp_uasp');
        if ($rows_no)
            return $query->num_rows();

        return $query->result();
    }

}

/* End of file uasp_model.php */
/* Location: ./application/models/uasp_model.php */