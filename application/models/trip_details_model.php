<?php

class Trip_details_model extends CI_Model {

    public $safa_uo_id = FALSE;
    public $safa_ea_id = FALSE;
    public $safa_ito_id = FALSE;
    public $safa_trip_traveller_id = FALSE;
    public $safa_trip_id = FALSE;
    public $safa_trip_internaltrip_id = FALSE;
    
    
    public $limit = FALSE;
    public $offset = FALSE;
    public $order_by = FALSE;

    function __construct() {
        parent::__construct();
    }

    function get() {
        $this->db->select('safa_trips.*
            , (SELECT name FROM safa_ea_seasons WHERE safa_ea_seasons.safa_ea_season_id=safa_ea_packages.safa_ea_season_id) AS season
            , safa_ea_packages.safa_uo_contract_ea_id, 
            , (SELECT safa_uo_contract_id FROM safa_uo_contracts_eas WHERE safa_uo_contracts_eas.safa_uo_contract_ea_id = safa_ea_packages.safa_uo_contract_ea_id) AS uo_contract_id
            , (SELECT ' . name() . ' FROM safa_uo_contracts WHERE safa_uo_contracts.safa_uo_contract_id = uo_contract_id) AS contract
            , (SELECT ' . name() . ' FROM safa_tripstatus WHERE safa_tripstatus.safa_tripstatus_id = safa_trips.safa_tripstatus_id) AS trip_status
            , safa_itos.' . name() . ' AS ito ');
        
        if ($this->safa_trip_id !== FALSE)
            $this->db->where('safa_trips.safa_trip_id', $this->safa_trip_id);

        if ($this->safa_uo_id !== FALSE)
            $this->db->where('safa_uo_contracts.safa_uo_id', $this->safa_uo_id);

        if ($this->safa_ea_id !== FALSE)
            $this->db->where('safa_uo_contracts_eas.safa_ea_id', $this->safa_ea_id);

        if ($this->safa_ito_id !== FALSE)
            $this->db->where('safa_trip_internaltrips.safa_ito_id', $this->safa_ito_id);

        $this->db->join('safa_ea_packages', 'safa_ea_packages.safa_ea_package_id = safa_trips.safa_ea_package_id');

        if ($this->safa_uo_id !== FALSE) {
            $this->db->join('safa_uo_contracts_eas', 'safa_ea_packages.safa_uo_contract_ea_id = safa_uo_contracts_eas.safa_uo_contract_ea_id');
            $this->db->join('safa_uo_contracts', 'safa_uo_contracts.safa_uo_contract_id = safa_uo_contracts_eas.safa_uo_contract_id');
        }

        $this->db->join('safa_trip_internaltrips', 'safa_trip_internaltrips.safa_trip_id = safa_trips.safa_trip_id', 'left');
        $this->db->join('safa_itos', 'safa_itos.safa_ito_id = safa_trip_internaltrips.safa_ito_id', 'left');


        if ($this->safa_ea_id !== FALSE) {
            $this->db->join('safa_uo_contracts_eas', 'safa_ea_packages.safa_uo_contract_ea_id = safa_uo_contracts_eas.safa_uo_contract_ea_id');
        }
        $this->db->join('safa_trip_externalsegments', 'safa_trips.safa_trip_id = safa_trip_externalsegments.safa_trip_id AND safa_externalsegmenttype_id = 1', 'left');
        
        $query = $this->db->get('safa_trips');


        return $query->row();
    }

    function get_travellers() {

        $this->db->select('safa_trip_travellers.*, safa_group_passports.*
            , (SELECT ' . name() . ' FROM erp_gender WHERE erp_gender.erp_gender_id = safa_group_passports.gender_id) AS gender
            , (SELECT ' . name() . ' FROM erp_countries WHERE erp_countries.erp_country_id = safa_group_passports.nationality_id) AS nationality');
        if ($this->safa_trip_id !== FALSE)
            $this->db->where('safa_trip_travellers.safa_trip_id', $this->safa_trip_id);
        if ($this->safa_trip_traveller_id !== FALSE)
            $this->db->where('safa_trip_travellers.safa_trip_traveller_id', $this->safa_trip_traveller_id);
        $this->db->join('safa_group_passports', 'safa_trip_travellers.safa_group_passport_id = safa_group_passports.safa_group_passport_id');
        $query = $this->db->get('safa_trip_travellers');

        if ($this->safa_trip_traveller_id !== FALSE)
            return $query->row();
        else
            return $query->result();
    }

    function movable_trips() {
        
    }

    function get_supervisors() {
        if ($this->safa_trip_id !== FALSE) {
            $this->db->join('safa_trip_supervisors', 'safa_ea_supervisors.safa_ea_supervisor_id = safa_trip_supervisors.safa_ea_supervisor_id');
            $this->db->where('safa_trip_supervisors.safa_trip_id', $this->safa_trip_id);
        }
        $query = $this->db->get('safa_ea_supervisors');
        return $query->result();
    }

    function trip_path($type) {
        $this->db->select('safa_trip_externalsegments.*
            , (SELECT ' . name() . ' FROM erp_port_halls WHERE safa_trip_externalsegments.start_port_hall_id = erp_port_halls.erp_port_hall_id) AS start_port_hall
            , (SELECT ' . name() . ' FROM erp_port_halls WHERE safa_trip_externalsegments.end_port_hall_id = erp_port_halls.erp_port_hall_id) AS end_port_hall');
        if ($this->safa_trip_id !== FALSE)
            $this->db->where('safa_trip_externalsegments.safa_trip_id', $this->safa_trip_id);
        $this->db->where('safa_trip_externalsegments.safa_externaltriptype_id', $type);
        $query = $this->db->get('safa_trip_externalsegments');
        return $query->result();
    }

    function trip_hotels() {
        $this->db->select('safa_trip_hotels.*
            , (SELECT ' . name() . ' FROM erp_cities WHERE erp_cities.erp_city_id = erp_hotels.erp_city_id) AS city
            , erp_hotels.' . name() . ' AS hotel
            , (SELECT ' . name() . ' FROM erp_meals WHERE erp_meals.erp_meal_id = safa_trip_hotels.erp_meal_id) AS meal');
        if ($this->safa_trip_id !== FALSE)
            $this->db->where('safa_trip_hotels.safa_trip_id', $this->safa_trip_id);
        $this->db->join('erp_hotels', 'erp_hotels.erp_hotel_id = safa_trip_hotels.erp_hotel_id');
        $query = $this->db->get('safa_trip_hotels');
        return $query->result();
    }

    function get_hotel_rooms($safa_trip_hotel_id) {
        $this->db->select('safa_trip_hotel_rooms.rooms_count, erp_hotelroomsizes.' . name() . ' AS room_size');
        $this->db->where('safa_trip_hotel_id', $safa_trip_hotel_id);
        $this->db->join('erp_hotelroomsizes', 'erp_hotelroomsizes.erp_hotelroomsize_id = safa_trip_hotel_rooms.erp_hotelroomsize_id');
        $query = $this->db->get('safa_trip_hotel_rooms')->result();
        $detail = NULL;
        foreach ($query as $item) {
            $detail .= "(" . $item->room_size . " -> " . $item->rooms_count . ") ";
        }
        return $detail;
    }

    function get_internal_segments() {
    	
    	
        $this->db->select('`safa_trip_internaltrips`.*
            , `safa_internalsegments`.*
            , `safa_internalsegmenttypes`.`' . name() . '` AS type
            , IF(`safa_internalsegments`.`safa_internalsegmenttype_id` = 1 or `safa_internalsegments`.`safa_internalsegmenttype_id` = 2, (SELECT ' . name() . ' FROM erp_ports WHERE erp_ports.erp_port_id = safa_internalsegments.erp_port_id), NULL) AS port
            , IF(`safa_internalsegments`.`safa_internalsegmenttype_id` = 2 or `safa_internalsegments`.`safa_internalsegmenttype_id` = 3 or `safa_internalsegments`.`safa_internalsegmenttype_id` = 4,
                start.' . name() . ', NULL) AS start_hotel
            , IF(`safa_internalsegments`.`safa_internalsegmenttype_id` = 1 or `safa_internalsegments`.`safa_internalsegmenttype_id` = 4,
                end.' . name() . ', NULL) AS end_hotel
            , IF(`safa_internalsegments`.`safa_internalsegmenttype_id` = 1,
                (SELECT ' . name() . ' FROM erp_cities WHERE erp_cities.erp_city_id = erp_ports.erp_city_id), 
                  IF(`safa_internalsegments`.`safa_internalsegmenttype_id` = 3,
                    (SELECT ' . name() . ' FROM erp_cities WHERE erp_cities.erp_city_id = safa_tourismplaces.erp_city_id), 
                    (SELECT ' . name() . ' FROM erp_cities WHERE erp_cities.erp_city_id = start.erp_city_id))) AS city
                    
            , IF(`safa_internalsegments`.`safa_internalsegmenttype_id` = 3,
                (SELECT ' . name() . ' FROM safa_tourismplaces WHERE safa_tourismplaces.safa_tourismplace_id = safa_internalsegments.safa_tourism_place_id), NULL) AS tourism_place
            , (SELECT ' . name() . ' FROM safa_internalsegmentstatus WHERE safa_internalsegmentstatus.safa_internalsegmentstatus_id = safa_internalsegments.safa_internalsegmentestatus_id) AS internalsegmentestatus 
            , (SELECT ' . name() . ' FROM safa_uo_users WHERE safa_uo_users.safa_uo_user_id = safa_internalsegments.safa_uo_user_id) AS uo_user 
                
            , safa_itos.'.name().' as safa_ito_name
            , safa_uo_contracts.'.name().' as safa_uo_contract_name
            , safa_internalsegments.seats_count
            , safa_uos.fax as uo_fax
            , (SELECT safa_internalsegment_log.safa_uo_agent_id FROM safa_internalsegment_log WHERE safa_internalsegment_log.safa_internalsegment_id = safa_internalsegments.safa_internalsegment_id order by safa_internalsegment_log_id desc limit 1) as safa_uo_agent_id
            
            , ( select CONCAT(erp_flight_availabilities_detail.flight_date,\' \', erp_flight_availabilities_detail.departure_time) as departure_datetime from erp_flight_availabilities, erp_flight_availabilities_detail where safa_trip_internaltrips.erp_flight_availability_id = erp_flight_availabilities.erp_flight_availability_id AND erp_flight_availabilities_detail.erp_flight_availability_id = erp_flight_availabilities.erp_flight_availability_id and erp_flight_availabilities_detail.safa_externaltriptype_id=2  order by erp_flight_availabilities_detail.departure_time desc limit 1)  as departure_datetime 
            
            , CONCAT(safa_transporters.'.name().',\'---\',`erp_flight_availabilities_detail`.`flight_number`,\' \', safa_transporters.`code`) as flight_number 
                         
            ,erp_port_halls_from.'.name().' as erp_port_halls_from_name
            ,erp_port_halls_to.'.name().' as erp_port_halls_to_name
            
            ', FALSE); //ORDER BY `datetime` ASC
        if ($this->safa_trip_id !== FALSE)
            $this->db->where('safa_trip_internaltrips.safa_trip_id', $this->safa_trip_id);
            
        if ($this->safa_trip_internaltrip_id !== FALSE)
            $this->db->where('safa_trip_internaltrips.safa_trip_internaltrip_id', $this->safa_trip_internaltrip_id);
             
        $this->db->where("(safa_trip_internaltrips.deleted IS NULL OR safa_trip_internaltrips.deleted=0)");
            
        $this->db->from('safa_internalsegments');  
        $this->db->join('safa_trip_internaltrips', 'safa_internalsegments.safa_trip_internaltrip_id = safa_trip_internaltrips.safa_trip_internaltrip_id');
        $this->db->join('erp_hotels start', 'start.erp_hotel_id = safa_internalsegments.erp_start_hotel_id', 'left');
        $this->db->join('erp_hotels end', 'end.erp_hotel_id = safa_internalsegments.erp_end_hotel_id', 'left');
        $this->db->join('erp_ports', 'erp_ports.erp_port_id = safa_internalsegments.erp_port_id', 'left');
        $this->db->join('safa_internalsegmenttypes', 'safa_internalsegmenttypes.safa_internalsegmenttype_id = safa_internalsegments.safa_internalsegmenttype_id', 'left');
        $this->db->join('safa_tourismplaces', 'safa_internalsegments.safa_tourism_place_id = safa_tourismplaces.safa_tourismplace_id', 'left');
        
        $this->db->join('safa_itos', 'safa_itos.safa_ito_id = safa_trip_internaltrips.safa_ito_id', 'left');
        $this->db->join('safa_uo_contracts', 'safa_uo_contracts.safa_uo_contract_id = safa_trip_internaltrips.safa_uo_contract_id', 'left');
        $this->db->join('safa_uos', 'safa_uos.safa_uo_id = safa_uo_contracts.safa_uo_id', 'left');
        
        
        $this->db->join('erp_flight_availabilities', 'erp_flight_availabilities.erp_flight_availability_id=safa_trip_internaltrips.erp_flight_availability_id', 'left');
		$this->db->join('erp_flight_availabilities_detail', 'erp_flight_availabilities_detail.erp_flight_availability_id = erp_flight_availabilities.erp_flight_availability_id AND safa_internalsegments.safa_internalsegmenttype_id = erp_flight_availabilities_detail.safa_externaltriptype_id', 'left');
        
		$this->db->join('erp_port_halls erp_port_halls_from', 'erp_port_halls_from.erp_port_hall_id = erp_flight_availabilities_detail.erp_port_hall_id_from', 'left');
        $this->db->join('erp_port_halls erp_port_halls_to', 'erp_port_halls_to.erp_port_hall_id = erp_flight_availabilities_detail.erp_port_hall_id_to', 'left');
        
		$this->db->join('safa_transporters', 'safa_transporters.code = erp_flight_availabilities_detail.airline_code', 'left');
        
        
//		$this->db->join(FSDB . '.fs_flights', FSDB . '.fs_flights.fs_flight_id = ' . SLDB . '.erp_flight_availabilities_detail.fs_flight_id', 'left');
//      $this->db->join(FSDB . '.fs_status', FSDB . '.fs_status.fs_status_id = ' . FSDB . '.fs_flights.fs_status_id', 'left');
//      $this->db->join(FSDB . '.fs_airlines', FSDB . '.fs_airlines.fs_airline_id = ' . SLDB . '.erp_flight_availabilities_detail.erp_airline_id', 'left');
        
        
        
//        if ($this->safa_uo_id !== FALSE)
//            $this->db->where('(safa_trip_internaltrips.safa_uo_id ='. $this->safa_uo_id.' or (safa_trip_internaltrips.erp_company_id ='. $this->safa_uo_id.' and  safa_trip_internaltrips.erp_company_type_id=2))');
//            
        
        //$this->db->where('(erp_flight_availabilities_detail.safa_externaltriptype_id =2 or erp_flight_availabilities_detail.safa_externaltriptype_id is null)');
        
		$this->db->order_by('safa_internalsegments.start_datetime', 'ASC');
		
        $query = $this->db->get();
        
        //echo $this->db->last_query(); exit;    
        
        return $query->result();
    }

    function get_starting_point($item) {
        if ($item->safa_internalsegmenttype_id == '1')
            return $item->port;

        if ($item->safa_internalsegmenttype_id == '2')
            return $item->start_hotel;

        if ($item->safa_internalsegmenttype_id == '3')
            return $item->start_hotel;

        if ($item->safa_internalsegmenttype_id == '4')
            return $item->start_hotel;
    }

    function get_ending_point($item) {
        if ($item->safa_internalsegmenttype_id == '1')
            return $item->end_hotel;

        if ($item->safa_internalsegmenttype_id == '2')
            return $item->port;

        if ($item->safa_internalsegmenttype_id == '3')
            return $item->tourism_place;

        if ($item->safa_internalsegmenttype_id == '4')
            return $item->end_hotel;
    }

}
