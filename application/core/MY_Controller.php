<?php
class Safa_Controller extends CI_Controller {
    public $destination = 'admin';
    public $system_id = '1';
    public $module = FALSE;
    public function __construct() {
        parent::__construct();
        $this->destination = session('type');
        $this->system_id = null;
        if($this->destination == 'admin') {
            $this->system_id = '1';
            $this->{$this->destination . '_id'} = session('admin_user_id');
        }
        elseif($this->destination == 'uo') {
            $this->system_id = '2';
        }
        elseif($this->destination == 'ea') {
            $this->system_id = '3';
        }
        elseif($this->destination == 'ito') {
            $this->system_id = '4';
        }
        elseif($this->destination == 'gov') {
            $this->system_id = '5';
        }
        $this->{$this->destination . '_id'} = session($this->destination . '_id');
    }
}