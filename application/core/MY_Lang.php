<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Lang extends CI_Lang {

    public function __construct() {
        parent::__construct();
    }

    public function load($langfile, $idiom = '', $return = FALSE, $add_suffix = TRUE, $alt_path = '') {
        $langfile = str_replace('.php', '', $langfile);

        $langfile = str_replace('_lang', '', $langfile);


        if (empty($idiom) OR !ctype_alpha($idiom)) {
            $config = & get_config();
            $idiom = empty($config['language']) ? 'english' : $config['language'];
        }

        if ($return === FALSE && isset($this->is_loaded[$langfile]) && $this->is_loaded[$langfile] === $idiom) {
            return;
        }

        
        

        $CI = & get_instance();
        $CI->load->database();
        $languageresult = $CI->db->query("SELECT
safa_language_keys.safa_language_key_name,
safa_languages_phrases.phrase
FROM
safa_languages_phrases
JOIN
erp_languages ON erp_languages.erp_language_id = safa_languages_phrases.safa_language_id
JOIN safa_language_keys ON safa_language_keys.safa_language_key_id = safa_languages_phrases.safa_language_key_id
JOIN safa_languages_files ON safa_languages_files.safa_file_id = safa_language_keys.safa_languages_file_id

WHERE erp_languages.path = '$idiom' && safa_languages_files.file_name='$langfile'

")->result();
        if( ! $languageresult)
            return parent::load($langfile, $idiom, $return, $add_suffix, $alt_path);
            
        $lang = array();
        foreach ($languageresult as $result)
            $lang[$result->safa_language_key_name] = $result->phrase;

        if (count($lang))
            $found = TRUE;
        else
            $found = FALSE;

        /*
          // Load the base file, so any others found can override it
          $basepath = BASEPATH.'language/'.$idiom.'/'.$langfile;
          if (($found = file_exists($basepath)) === TRUE)
          {
          include($basepath);
          }

          // Do we have an alternative path to look in?
          if ($alt_path !== '')
          {
          $alt_path .= 'language/'.$idiom.'/'.$langfile;
          if (file_exists($alt_path))
          {
          include($alt_path);
          $found = TRUE;
          }
          }
          else
          {
          foreach (get_instance()->load->get_package_paths(TRUE) as $package_path)
          {
          $package_path .= 'language/'.$idiom.'/'.$langfile;
          if ($basepath !== $package_path && file_exists($package_path))
          {
          include($package_path);
          $found = TRUE;
          break;
          }
          }
          }
         */
        if ($found !== TRUE) {
            show_error('Unable to load the requested language Table: ' . $langfile);
        }

        if (!isset($lang) OR !is_array($lang)) {
            log_message('error', 'Language file contains no data: language/' . $idiom . '/' . $langfile);

            if ($return === TRUE) {
                return array();
            }
            return;
        }

        if ($return === TRUE) {
            return $lang;
        }

        $this->is_loaded[$langfile] = $idiom;
        $this->language = array_merge($this->language, $lang);

        log_message('debug', 'Language file loaded: language/' . $idiom . '/' . $langfile);
        return TRUE;
    }
        /**
	 * Language line
	 *
	 * Fetches a single line of text from the language array
	 *
	 * @param	string	$line		Language line key
	 * @param	bool	$log_errors	Whether to log an error message if the line is not found
	 * @return	string	Translation
	 */
	public function line($line = '', $log_errors = TRUE)
	{
		$value = ($line === '' OR ! isset($this->language[$line])) ? FALSE : $this->language[$line];
                if(isset($this->language['help_'.$line]))
                {
                    $var = time();
                    $value =  $this->language[$line] 
                            ." <span id=\"bubble-$var\" original-title=\"".$this->language['help_'.$line] ."\">(?)</span><script type=\"text/javascript\">
                                $(document).ready(function(){
                                    $('#bubble-$var').tipsy();
                                });
                                </script>";
                }
		// Because killer robots like unicorns!
		if ($value === FALSE && $log_errors === TRUE)
		{
			log_message('error', 'Could not find the language line "'.$line.'"');
		}

		return $value;
	}
}

/* End of file MY_Lang.php */
/* Location: ./Application/core/MY_Lang.php */