<?php

class MY_Loader extends CI_Loader {
    
    
    function __construct() {
        parent::__construct();
    }
    
    function view($view, $vars = array(), $return = FALSE) {
        
        if(defined('STYLE')){
            $this->_ci_view_paths = array();
            $this->_ci_view_paths[APPPATH."views/".STYLE.'/'] =  true;
            $this->_ci_view_paths[APPPATH."views/"] =  true;
        }
         
        return parent::view($view, $vars, $return);
    }
}
