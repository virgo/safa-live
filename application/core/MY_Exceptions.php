<?php

class MY_Exceptions extends CI_Exceptions {

    public function show_error($heading, $message, $template = 'error_general', $status_code = 500) {
        if($status_code != 404)
            $url = $this->trace("Error", array(
                    'heading' => $heading,
                    'message' => $message,
                    'status' => $status_code,
                    ));

        if (ENVIRONMENT == 'development')
            return parent::show_error($heading, $message, $template, $status_code);
        else
            die( "Error, Please contact the customer support.<!--". $url ."-->");
        return;
    }

    public function show_php_error($severity, $message, $filepath, $line) {
        $url = $this->trace('PHP', array(
                'message' =>$message, 
                'filepath' => $filepath,
                'line' => $line
                ));
        if (ENVIRONMENT == 'development')
            return parent::show_php_error($severity, $message, $filepath, $line);
        return;
    }
    
    public function trace($type = NULL, $message = NULL) {
         if (ENVIRONMENT == 'development')
             return;
        // USER AGENT
        $this->agent = $_SERVER['HTTP_USER_AGENT'];
        // CURRENT URL
        $this->current_url = $_SERVER['PHP_SELF'];
        // REFERRAL URL
        $this->referral_url = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : NULL;
        // POST, GET, SERVER, SESSION AND COOKIES
        $this->globals = array(
            'post' => isset($_POST) ? $_POST : NULL,
            'get' => isset($_GET) ? $_GET : NULL,
            'server' => isset($_SERVER) ? $_SERVER : NULL,
            'session' => isset($_SESSION) ? $_SESSION : NULL,
            'cookie' => isset($_COOKIE) ? $_COOKIE : NULL,
            'env' => isset($_ENV) ? $_ENV : NULL,
        );
        // TIME
        $this->time = date('Y-m-d h:i:s');
        $this->filename = time().rand(1,99999);
        // (($usage = memory_get_usage()) != '' ? number_format($usage).' bytes'
        $var = array(
            'type' => $type,
            'message' => $message,
            'agent' => $this->agent,
            'current_url' => $this->current_url,
            'referral_url' => $this->referral_url,
            'globals' => $this->globals,
            'profiling' => $this->profiling,
            'timestamp' => $this->time
        );

        $this->write_log($var);
//        @mail('dev@virgotel.com', 'SL ERROR #' . $error_code, 'Check SL error # ' . $this->base_url() . 'errors/' . $this->filename);
        echo $this->base_url() . 'errors/' . $this->filename;
    }

    public function write_log($var) {
        $fp = fopen('./errors/' . $this->filename, 'w');
        fwrite($fp, json_encode($var));
        fclose($fp);
    }
    
    public function base_url() {
        $base_url = is_https() ? 'https' : 'http';
        $base_url .= '://'.$_SERVER['HTTP_HOST']
                .str_replace(basename($_SERVER['SCRIPT_NAME']), '', $_SERVER['SCRIPT_NAME']);
        return $base_url;
    }
}