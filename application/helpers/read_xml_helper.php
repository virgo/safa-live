<?php

function getXmlArray($file_name, $file_type) {
    $structure = array('MD_NO' => 'MD_NO',
        'MD_TITLE_ID' => 'MD_TITLE_ID',
        'MD_FIRST_NAME_LA' => 'MD_FIRST_NAME_LA',
        'MD_SECOND_NAME_LA' => 'MD_SECOND_NAME_LA',
        'MD_THIRD_NAME_LA' => 'MD_THIRD_NAME_LA',
        'MD_FOURTH_NAME_LA' => 'MD_FOURTH_NAME_LA',
        'MD_FIRST_NAME_AR' => 'MD_FIRST_NAME_AR',
        'MD_SECOND_NAME_AR' => 'MD_SECOND_NAME_AR',
        'MD_THIRD_NAME_AR' => 'MD_THIRD_NAME_AR',
        'MD_FOURTH_NAME_AR' => 'MD_FOURTH_NAME_AR',
        'MD_NATIONALITY_ID' => 'MD_NATIONALITY_ID',
        'MD_PASSPORT_NO' => 'MD_PASSPORT_NO',
        'MD_PASSPORT_TYPE' => 'MD_PASSPORT_TYPE',
        'MD_PASSPORT_ISSUE_DATE' => 'MD_PASSPORT_ISSUE_DATE',
        'MD_PASSPORT_EXPIRY_DATE' => 'MD_PASSPORT_EXPIRY_DATE',
        'MD_PASSPORT_ISSUING_CITY' => 'MD_PASSPORT_ISSUING_CITY',
        'MD_PASSPORT_ISSUING_COUNTRY_ID' => 'MD_PASSPORT_ISSUING_COUNTRY_ID',
        'MD_PASSPORT_DPN_COUNT' => 'MD_PASSPORT_DPN_COUNT',
        'MD_DPN_SERIAL_NO' => 'MD_DPN_SERIAL_NO',
        'MD_GENDER_ID' => 'MD_GENDER_ID',
        'MD_MARITAL_STATUS_ID' => 'MD_MARITAL_STATUS_ID',
        'MD_DATE_OF_BIRTH' => 'MD_DATE_OF_BIRTH',
        'MD_BIRTH_CITY' => 'MD_BIRTH_CITY',
        'MD_BIRTH_COUNTRY_ID' => 'MD_BIRTH_COUNTRY_ID',
        'MD_RELATIVE_NO' => 'MD_RELATIVE_NO',
        'MD_RELATIVE_RELATION_ID' => 'MD_RELATIVE_RELATION_ID',
        'MD_EDUCATIONAL_LEVEL' => 'MD_EDUCATIONAL_LEVEL',
        'MD_OCCUPATION' => 'MD_OCCUPATION',
        'MD_CITY' => 'MD_CITY',
        'MD_COUNTRY_ID' => 'MD_COUNTRY_ID',
        'MD_AGE' => 'MD_AGE',
        'MD_PREVIOUS_NATIONALITY_ID' => 'MD_PREVIOUS_NATIONALITY_ID',
        'MD_RELATIVE_GENDER_ID' => 'MD_RELATIVE_GENDER_ID'
    );
    $CI = & get_instance();
    if ($file_type == 1 && substr(strtolower($file_name), -3) == 'xml') {
        $file = 'static/xml/' . $file_name . '';
        // load as string
        $xml = file_get_contents($file);
//        $CI->load->library('xmltoarray');
        
    	try {
        	$array = Xmltoarray::createArray($xml);
        } catch (Exception $e) {
        	//echo $e->getMessage(); exit;
        	echo lang('the_xml_format_not_true'); exit;
        }
        
        $i = 0;
        $a = 0;
        $input = $array['DataSetMutamers']['MUTAMERS'];
        foreach ($input as $inputvalue) {
            foreach ($structure as $key => $value) {
                if (isset($inputvalue[$value]))
                    $mutamers[$a][$key] = $inputvalue[$value];
                else
                    $mutamers[$a][$key] = '';
            }$a++;
        }
        return(array('MUTAMERS' => $mutamers));
    }
    elseif ($file_type == 2 && substr(strtolower($file_name), -3) == 'xml') {
        $file = 'static/xml/' . $file_name . '';
        // load as string
        $xml = file_get_contents($file);
        
        
        try {
        	$array = Xmltoarray::createArray($xml);
        } catch (Exception $e) {
        	//echo $e->getMessage(); exit;
        	echo lang('the_xml_format_not_true'); exit;
        }
        
        
        $structure = array(
            'MD_NO' => 'SN',
            'MD_TITLE_ID' => 'TC',
            'MD_FIRST_NAME_LA' => 'FN',
            'MD_SECOND_NAME_LA' => 'FAN',
            'MD_THIRD_NAME_LA' => 'MN',
            'MD_FOURTH_NAME_LA' => 'LN',
            'MD_FIRST_NAME_AR' => 'AFN',
            'MD_SECOND_NAME_AR' => 'AFAN',
            'MD_THIRD_NAME_AR' => 'AMN',
            'MD_FOURTH_NAME_AR' => 'ALN',
            'MD_NATIONALITY_ID' => 'N',
            'MD_PASSPORT_NO' => 'PN',
            'MD_PASSPORT_TYPE' => 'PT',
            'MD_PASSPORT_ISSUE_DATE' => 'DI',
            'MD_PASSPORT_EXPIRY_DATE' => 'DE',
            'MD_PASSPORT_ISSUING_CITY' => 'CIAT',
            'MD_PASSPORT_ISSUING_COUNTRY_ID' => 'CIA',
            'MD_PASSPORT_DPN_COUNT' => 'MNR',
            'MD_DPN_SERIAL_NO' => 'DSN',
            'MD_GENDER_ID' => 'G',
            'MD_MARITAL_STATUS_ID' => 'MS',
            'MD_DATE_OF_BIRTH' => 'DB',
            'MD_BIRTH_CITY' => 'CB',
            'MD_BIRTH_COUNTRY_ID' => 'COB',
            'MD_RELATIVE_NO' => 'RT',
            'MD_RELATIVE_RELATION_ID' => 'RR',
            'MD_EDUCATIONAL_LEVEL' => 'EL',
            'MD_OCCUPATION' => 'J',
            'MD_CITY' => 'CAC',
            'MD_COUNTRY_ID' => 'CACO',
            'MD_AGE' => '',
            'MD_PREVIOUS_NATIONALITY_ID' => '',
            'MD_RELATIVE_GENDER_ID' => ''
        );
        $i = 0;
        $a = 0;
        $input = $array['DataSetMutamers']['MUTAMERS'];
        foreach ($input as $inputvalue) {
            foreach ($structure as $key => $value) {
                if (isset($inputvalue[$value]))
                    if($key == 'MD_DATE_OF_BIRTH' or $key == 'MD_PASSPORT_ISSUE_DATE' or $key == 'MD_PASSPORT_EXPIRY_DATE')
                        $mutamers[$a][$key] = trans($inputvalue[$value]);
                    else
                        $mutamers[$a][$key] = $inputvalue[$value];
                else
                    $mutamers[$a][$key] = '';
            }$a++;
        }
        return(array('MUTAMERS' => $mutamers));

        //text file
    } 
    elseif ($file_type == 3 && substr(strtolower($file_name), -3) == 'txt') {
        $structure = array('MD_NO' => 'PilgrimID',
            'MD_TITLE_ID' => 'Title',
            'MD_FIRST_NAME_LA' => 'FirstName',
            'MD_SECOND_NAME_LA' => 'FatherName',
            'MD_THIRD_NAME_LA' => 'MiddleName',
            'MD_FOURTH_NAME_LA' => 'LastName',
            'MD_FIRST_NAME_AR' => 'AFirstName',
            'MD_SECOND_NAME_AR' => 'AFatherName',
            'MD_THIRD_NAME_AR' => 'AMiddleName',
            'MD_FOURTH_NAME_AR' => 'ALastName',
            'MD_NATIONALITY_ID' => 'CNationality',
            'MD_PASSPORT_NO' => 'PPNo',
            'MD_PASSPORT_TYPE' => 'PPType',
            'MD_PASSPORT_ISSUE_DATE' => 'PPIssueDate',
            'MD_PASSPORT_EXPIRY_DATE' => 'PPExpDate',
            'MD_PASSPORT_ISSUING_CITY' => 'PPIssueCity',
            'MD_PASSPORT_ISSUING_COUNTRY_ID' => 'PPIssueCountry',
            'MD_PASSPORT_DPN_COUNT' => 'No_Of_Relatives',
            'MD_DPN_SERIAL_NO' => 'Dpn_Serial_No',
            'MD_GENDER_ID' => 'Sex',
            'MD_MARITAL_STATUS_ID' => 'MaritalStatus',
            'MD_DATE_OF_BIRTH' => 'BirthDate',
            'MD_BIRTH_CITY' => 'BirthCity',
            'MD_BIRTH_COUNTRY_ID' => 'BirthCountry',
            'MD_RELATIVE_NO' => 'SponserID',
            'MD_RELATIVE_RELATION_ID' => 'RelationWithSponser',
            'MD_EDUCATIONAL_LEVEL' => 'Education',
            'MD_OCCUPATION' => 'Job',
            'MD_CITY' => 'CAddaCity',
            'MD_COUNTRY_ID' => 'CAddCountry',
            'MD_AGE' => '',
            'MD_PREVIOUS_NATIONALITY_ID' => 'PrevNationality',
            'MD_RELATIVE_GENDER_ID' => ''
        );


        $file = 'static/xml/' . $file_name . '';

        //By Gouda.
        $data = file_get_contents($file);
        $data = iconv("windows-1256", "UTF-8", $data);
        file_put_contents($file, $data);

        $array = file($file);

        foreach ($array as $line_num => $line) {
            $arr[] = explode(",", $line);
        }
//                print_r($arr);
//                                exit();
        foreach ($arr as $line) {
            foreach ($line as $key => $value) {
                $header[] = $value;
            }
            break;
        }
        $a = 0;
        $i = 0;
        $count = sizeof($arr);
        foreach ($arr as $key => $line) {
            if ($key < ($count - 1)) {
                foreach ($line as $value) {

                    if ($a < sizeof($header) && $key > 0)
                        $lines[$i]['' . $header[$a] . ''] = $value;
                    $a++;
                }$a = 0;
                $i++;
            }
        }
//                print_r($lines);
//               exit();
        $a = 0;
        foreach ($lines as $inputvalue) {
            foreach ($structure as $key => $value) {
                if (isset($inputvalue[$value]))
                    $mutamers[$a][$key] = $inputvalue[$value];
                else
                    $mutamers[$a][$key] = '';
            }$a++;
        }
        return(array('MUTAMERS' => $mutamers));
    } 
    elseif ($file_type == 4 && substr(strtolower($file_name), -4) == 'safa') {
        $structure = array('MD_NO' => 'PilgrimID',
            'MD_TITLE_ID' => 'Title',
            'MD_FIRST_NAME_LA' => 'FirstName',
            'MD_SECOND_NAME_LA' => 'FatherName',
            'MD_THIRD_NAME_LA' => 'MiddleName',
            'MD_FOURTH_NAME_LA' => 'LastName',
            'MD_FIRST_NAME_AR' => 'AFirstName',
            'MD_SECOND_NAME_AR' => 'AFatherName',
            'MD_THIRD_NAME_AR' => 'AMiddleName',
            'MD_FOURTH_NAME_AR' => 'ALastName',
            'MD_NATIONALITY_ID' => 'CNationality',
            'MD_PASSPORT_NO' => 'PPNo',
            'MD_PASSPORT_TYPE' => 'PPType',
            'MD_PASSPORT_ISSUE_DATE' => 'PPIssueDate',
            'MD_PASSPORT_EXPIRY_DATE' => 'PPExpDate',
            'MD_PASSPORT_ISSUING_CITY' => 'PPIssueCity',
            'MD_PASSPORT_ISSUING_COUNTRY_ID' => 'PPIssueCountry',
            'MD_PASSPORT_DPN_COUNT' => 'No_Of_Relatives',
            'MD_DPN_SERIAL_NO' => 'Dpn_Serial_No',
            'MD_GENDER_ID' => 'Sex',
            'MD_MARITAL_STATUS_ID' => 'MaritalStatus',
            'MD_DATE_OF_BIRTH' => 'BirthDate',
            'MD_BIRTH_CITY' => 'BirthCity',
            'MD_BIRTH_COUNTRY_ID' => 'BirthCountry',
            'MD_RELATIVE_NO' => 'SponserID',
            'MD_RELATIVE_RELATION_ID' => 'RelationWithSponser',
            'MD_EDUCATIONAL_LEVEL' => 'Education',
            'MD_OCCUPATION' => 'Job',
            'MD_CITY' => 'CAddaCity',
            'MD_COUNTRY_ID' => 'CAddCountry',
            'MD_AGE' => '',
            'MD_PREVIOUS_NATIONALITY_ID' => 'PrevNationality',
            'MD_RELATIVE_GENDER_ID' => '',
            'MD_MOFA' => 'Mofa',
            'MD_ENUMBER' => 'ENumber',
            'MD_IDNUMBER' => 'IdNumber',
            'MD_JOB2' => 'Job2',
            'MD_ADDRESS' => 'Address',
        );


        $file = 'static/xml/' . $file_name . '';


        $zip = new ZipArchive;
        if ($zip->open($file) === TRUE) {
            $zip->extractTo('static/xml/');
            $zip->close();
        }
        $file = 'static/xml/group.txt';


        //By Gouda.
        $data = file_get_contents($file);
        $data = iconv("windows-1256", "UTF-8", $data);
        file_put_contents($file, $data);

        $array = file($file);

        foreach ($array as $line_num => $line) {
            $arr[] = explode(",", $line);
        }
//                print_r($arr);
//                                exit();
        foreach ($arr as $line) {
            foreach ($line as $key => $value) {
                $header[] = $value;
            }
            break;
        }
        $a = 0;
        $i = 0;
        $count = sizeof($arr);
        foreach ($arr as $key => $line) {
            if ($key < ($count - 1)) {
                foreach ($line as $value) {

                    if ($a < sizeof($header) && $key > 0)
                        $lines[$i]['' . $header[$a] . ''] = $value;
                    $a++;
                }$a = 0;
                $i++;
            }
        }
//                print_r($lines);
//               exit();
        $a = 0;
        foreach ($lines as $inputvalue) {
            foreach ($structure as $key => $value) {
                if (isset($inputvalue[$value]))
                    $mutamers[$a][$key] = $inputvalue[$value];
                else
                    $mutamers[$a][$key] = '';
            }$a++;
        }
        return(array('MUTAMERS' => $mutamers));
    }
    else {
        return FALSE;
    }
}

function write_xml($array, $file_type) {
    $CI = & get_instance();
    if ($file_type == 1 || $file_type == 'gama') {
        $writer = new XMLWriter();
        $writer->openURI('جاما.XML');
        $writer->startDocument('1.0', 'UTF-8');
        $writer->setIndent(4);
        $writer->setIndentString(' ');
        $writer->startElement('DataSetMutamers');
        foreach ($array as $mutamers) {
            foreach ($mutamers as $mutamer) {
                $writer->startElement("MUTAMERS");
                foreach ($mutamer as $key => $value) {
                    if (isset($value)) {
                        if ($key == 'MD_PASSPORT_ISSUE_DATE' || $key == 'MD_PASSPORT_EXPIRY_DATE' || $key == 'MD_DATE_OF_BIRTH')
                            $writer->writeElement('' . $key . '', '' . date("Y/m/d", strtotime($value)) . '');
                        else
                            $writer->writeElement('' . $key . '', '' . $value . '');
                    } else
                        $writer->writeElement('' . $key . '', '');
                }
                $writer->endElement();
            }
        }

        $writer->endElement();
        $writer->endDocument();
        $writer->flush();
    }
    elseif ($file_type == 2 || $file_type == 'babUmrah') {
        header("Content-Type: text/xml/force-download");
        header("Content-Disposition: attachment; filename=bab_umrah.xml");
        $writer = new XMLWriter();
//        $writer->openURI('باب العمر.XML');
        $writer->openURI('php://output');
        $writer->startDocument('1.0', 'UTF-8');
        $writer->setIndent(4);
        $writer->setIndentString(' ');
        $writer->startElement('DataSetMutamers');
        foreach ($array as $mutamers) {

            foreach ($mutamers as $mutamer) {
                $writer->startElement("MUTAMERS");
//                 foreach ($mutamer as $key=>$value){
                if (isset($mutamer['MD_NO']))
                    $writer->writeElement('SN', '' . $mutamer['MD_NO'] . '');
                else
                    $writer->writeElement('SN', '');
                if (isset($mutamer['MD_FIRST_NAME_LA']))
                    $writer->writeElement('FN', '' . $mutamer['MD_FIRST_NAME_LA'] . '');
                else
                    $writer->writeElement('FN', '');

                if (isset($mutamer[' MD_FOURTH_NAME_LA ']))
                    $writer->writeElement('LN', '' . $mutamer[' MD_FOURTH_NAME_LA '] . '');
                else
                    $writer->writeElement('LN', '');
                if (isset($mutamer[' MD_THIRD_NAME_LA ']))
                    $writer->writeElement('MN', '' . $mutamer[' MD_THIRD_NAME_LA '] . '');
                else
                    $writer->writeElement('MN', '');

                if (isset($mutamer['MD_TITLE_ID']))
                    $writer->writeElement('TC', '' . $mutamer['MD_TITLE_ID'] . '');
                else
                    $writer->writeElement('TC', '');

                if (isset($mutamer['MD_GENDER_ID']))
                    $writer->writeElement('G', '' . $mutamer['MD_GENDER_ID'] . '');
                else
                    $writer->writeElement('G', '');

                if (isset($mutamer['MD_MARITAL_STATUS_ID']))
                    $writer->writeElement('MS', '' . $mutamer['MD_MARITAL_STATUS_ID'] . '');
                else
                    $writer->writeElement('MS', '');
                if (isset($mutamer['MD_NATIONALITY_ID']))
                    $writer->writeElement('N', '' . $mutamer['MD_NATIONALITY_ID'] . '');
                else
                    $writer->writeElement('N', '');
                if (isset($mutamer['MD_CITY']))
                    $writer->writeElement('CAC', '' . $mutamer['MD_CITY'] . '');
                else
                    $writer->writeElement('CAC', '');
                if (isset($mutamer['MD_COUNTRY_ID']))
                    $writer->writeElement('CACO', '' . $mutamer['MD_COUNTRY_ID'] . '');
                else
                    $writer->writeElement('CACO', '');

                if (isset($mutamer['MD_PASSPORT_NO']))
                    $writer->writeElement('PN', '' . $mutamer['MD_PASSPORT_NO'] . '');
                else
                    $writer->writeElement('PN', '');

                if (isset($mutamer['MD_PASSPORT_TYPE']))
                    $writer->writeElement('PT', '' . $mutamer['MD_PASSPORT_TYPE'] . '');
                else
                    $writer->writeElement('PT', '');

                if (isset($mutamer['MD_PASSPORT_ISSUING_COUNTRY_ID']))
                    $writer->writeElement('CIA', '' . $mutamer['MD_PASSPORT_ISSUING_COUNTRY_ID'] . '');
                else
                    $writer->writeElement('CIA', '');

                if (isset($mutamer['MD_PASSPORT_ISSUE_DATE']))
                    $writer->writeElement('DI', '' . date("m/d/Y", strtotime($mutamer['MD_PASSPORT_ISSUE_DATE'])) . '');
                else
                    $writer->writeElement('DI', '');

                if (isset($mutamer['MD_PASSPORT_EXPIRY_DATE']))
                    $writer->writeElement('DE', '' . date("m/d/Y", strtotime($mutamer['MD_PASSPORT_EXPIRY_DATE'])) . '');
                else
                    $writer->writeElement('DE', '');

                if (isset($mutamer['MD_BIRTH_COUNTRY_ID']))
                    $writer->writeElement('COB', '' . $mutamer['MD_BIRTH_COUNTRY_ID'] . '');
                else
                    $writer->writeElement('COB', '');

                if (isset($mutamer['MD_BIRTH_CITY']))
                    $writer->writeElement('CB', '' . $mutamer['MD_BIRTH_CITY'] . '');
                else
                    $writer->writeElement('CB', '');

                if (isset($mutamer['MD_DATE_OF_BIRTH']))
                    $writer->writeElement('DB', '' . date("d/m/Y", strtotime($mutamer['MD_DATE_OF_BIRTH'])) . '');
                else
                    $writer->writeElement('DB', '');

                if (isset($mutamer['MD_SECOND_NAME_LA']))
                    $writer->writeElement('FAN', '' . $mutamer['MD_SECOND_NAME_LA'] . '');
                else
                    $writer->writeElement('FAN', '');

                if (isset($mutamer['MD_PASSPORT_DPN_COUNT']))
                    $writer->writeElement('MNR', '' . $mutamer['MD_PASSPORT_DPN_COUNT'] . '');
                else
                    $writer->writeElement('MNR', '');

                if (isset($mutamer['MD_RELATIVE_NO']))
                    $writer->writeElement('RT', '' . $mutamer['MD_RELATIVE_NO'] . '');
                else
                    $writer->writeElement('RT', '');

                if (isset($mutamer['MD_RELATIVE_RELATION_ID']))
                    $writer->writeElement('RR', '' . $mutamer['MD_RELATIVE_RELATION_ID'] . '');
                else
                    $writer->writeElement('RR', '');

                if (isset($mutamer['MD_DPN_SERIAL_NO']))
                    $writer->writeElement('DSN', '' . $mutamer['MD_DPN_SERIAL_NO'] . '');
                else
                    $writer->writeElement('DSN', '');

                if (isset($mutamer['MD_EDUCATIONAL_LEVEL']))
                    $writer->writeElement('EL', '' . $mutamer['MD_EDUCATIONAL_LEVEL'] . '');
                else
                    $writer->writeElement('EL', '');

                if (isset($mutamer['II']))
                    $writer->writeElement('II', '' . $mutamer['II'] . '');
                else
                    $writer->writeElement('II', '');

                if (isset($mutamer['MD_PASSPORT_NO'])) {
                    $GroupCode = $CI->safa_umrahgroups_passports_model->get_group_id($mutamer['MD_PASSPORT_NO']);
                    $writer->writeElement('MIG', '' . $GroupCode->safa_umrahgroup_id . '');
                } else
                    $writer->writeElement('MIG', '');

                if ($mutamer['MD_RELATIVE_NO'] == 1)
                    $writer->writeElement('IM', 'true');
                else
                    $writer->writeElement('IM', 'false');

                if (isset($mutamer['MD_PASSPORT_ISSUING_CITY']))
                    $writer->writeElement('CIAT', '' . $mutamer['MD_PASSPORT_ISSUING_CITY'] . '');
                else
                    $writer->writeElement('CIAT', '');

                if (isset($mutamer['MD_NATIONALITY_ID'])) {
                    $country_name = $CI->safa_umrahgroups_passports_model->get_country_name($mutamer['MD_NATIONALITY_ID']);
                    $writer->writeElement('CACN', '' . $country_name->country_name . '');
                } else
                    $writer->writeElement('CACN', '');

                if (isset($mutamer['MD_NATIONALITY_ID'])) {
                    $country_name = $CI->safa_umrahgroups_passports_model->get_country_name($mutamer['MD_NATIONALITY_ID']);
                    $country_name = $CI->safa_umrahgroups_passports_model->get();
                    $writer->writeElement('CIAN', '' . $country_name->country_name . '');
                } else
                    $writer->writeElement('CIAN', '');

                if (isset($mutamer['MD_BIRTH_COUNTRY_ID'])) {
                    $country_name = $CI->safa_umrahgroups_passports_model->get_country_name($mutamer['MD_BIRTH_COUNTRY_ID']);
                    $country_name = $CI->safa_umrahgroups_passports_model->get();
                    $writer->writeElement('CBN', '' . $country_name->country_name . '');
                } else
                    $writer->writeElement('CBN', '');

                if (isset($mutamer['MD_OCCUPATION']))
                    $writer->writeElement('J', '' . $mutamer['MD_OCCUPATION'] . '');
                else
                    $writer->writeElement('J', '');

                if (isset($mutamer['MD_FIRST_NAME_AR']))
                    $writer->writeElement('AFN', '' . $mutamer['MD_FIRST_NAME_AR'] . '');
                else
                    $writer->writeElement('AFN', '');

                if (isset($mutamer['MD_FOURTH_NAME_AR']))
                    $writer->writeElement('ALN', '' . $mutamer['MD_FOURTH_NAME_AR'] . '');
                else
                    $writer->writeElement('ALN', '');

                if (isset($mutamer['MD_SECOND_NAME_AR']))
                    $writer->writeElement('AFAN', '' . $mutamer['MD_SECOND_NAME_AR'] . '');
                else
                    $writer->writeElement('AFAN', '');

                if (isset($mutamer['MD_THIRD_NAME_AR']))
                    $writer->writeElement('AMN', '' . $mutamer['MD_THIRD_NAME_AR'] . '');
                else
                    $writer->writeElement('AMN', '');

                $writer->writeElement('TL', '');
//                 }
                $writer->endElement();
            }
        }

        $writer->endElement();
        $writer->endDocument();

        $writer->flush();
//        unset($writer);
    }
    elseif ($file_type == 3 || $file_type == 'wayToUmrah') {
        $structure = array(
            'UO_CODE' => '',
            'EA_CODE' => '',
            'PilgrimID' => 'MD_NO',
            'Title' => 'MD_TITLE_ID',
            'FirstName' => 'MD_FIRST_NAME_LA',
            'AFirstName' => 'MD_FIRST_NAME_AR',
            'FatherName' => 'MD_SECOND_NAME_LA',
            'AFatherName' => 'MD_SECOND_NAME_AR',
            'LastName' => 'MD_FOURTH_NAME_LA',
            'ALastName' => 'MD_FOURTH_NAME_AR',
            'MotherName' => '',
            'AMotherName' => '',
            'MiddleName' => 'MD_THIRD_NAME_LA',
            'AMiddleName' => 'MD_THIRD_NAME_AR',
            'Sex' => 'MD_GENDER_ID',
            'BirthDate' => 'MD_DATE_OF_BIRTH',
            'BirthCountry' => 'MD_BIRTH_COUNTRY_ID',
            'BirthCity' => 'MD_BIRTH_CITY',
            'MaritalStatus' => 'MD_MARITAL_STATUS_ID',
            'Religion' => '',
            'Education' => 'MD_EDUCATIONAL_LEVEL',
            'job' => 'MD_OCCUPATION',
            'GroupCode' => '',
            'PackageCode' => '',
            'SponserID' => 'MD_RELATIVE_NO',
            'RelationWithSponser' => 'MD_RELATIVE_RELATION_ID',
            'No_Of_Relatives' => 'MD_PASSPORT_DPN_COUNT',
            'Dpn_Serial_No' => 'MD_DPN_SERIAL_NO',
            'Emb_City' => '',
            'CNationality' => 'MD_NATIONALITY_ID',
            'PrevNationality' => 'MD_PREVIOUS_NATIONALITY_ID',
            'PPNo' => 'MD_PASSPORT_NO',
            'PPType' => 'MD_PASSPORT_TYPE',
            'PPIssueCity' => 'MD_PASSPORT_ISSUING_CITY',
            'PPIssueDate' => 'MD_PASSPORT_ISSUE_DATE',
            'PPExpDate' => 'MD_PASSPORT_EXPIRY_DATE',
            'PPIssueCountry' => 'MD_PASSPORT_ISSUING_COUNTRY_ID',
            'CAddStreet' => '',
            'CAddAStreet' => '',
            'CAddCity' => '',
            'CAddaCity' => 'MD_CITY',
            'CAddState' => '',
            'CAddAstate' => '',
            'CAddCountry' => 'MD_COUNTRY_ID',
            'CAddZip' => '',
            'CAddMobile' => '',
            'CAddTel' => '',
            'CAddFax' => '',
            'FemaleId' => '',
        );

        $a = 0;
        $input = $array['MUTAMERS'];
        foreach ($input as $inputvalue) {
            foreach ($structure as $key => $value) {
                if (isset($inputvalue[$value])) {
                    if ($key == 'PPIssueDate' || $key == 'PPExpDate' || $key == 'BirthDate')
                        $mutamers[$a][$key] = date("m/d/Y", strtotime($inputvalue[$value]));
                    else
                        $mutamers[$a][$key] = $inputvalue[$value];
                }
                else {
                    if ($key == 'No_Of_Relatives' || $key == 'Dpn_Serial_No' || $key == 'Emb_City')
                        $mutamers[$a][$key] = '0';
                    elseif ($key == 'Religion')
                        $mutamers[$a][$key] = '1';
                    elseif ($key == 'UO_CODE') {
                        $GroupCode = $CI->safa_umrahgroups_passports_model->get_group_id($inputvalue['MD_PASSPORT_NO']);
                        $uo_code = $CI->safa_umrahgroups_passports_model->get_uo_code($inputvalue['MD_PASSPORT_NO'], $GroupCode->safa_umrahgroup_id);
                        $mutamers[$a][$key] = $uo_code->code;
                    } elseif ($key == 'EA_CODE') {
                        $GroupCode = $CI->safa_umrahgroups_passports_model->get_group_id($inputvalue['MD_PASSPORT_NO']);
                        $ea_code = $CI->safa_umrahgroups_passports_model->get_ea_code($inputvalue['MD_PASSPORT_NO'], $GroupCode->safa_umrahgroup_id);
                        $mutamers[$a][$key] = $ea_code->agency_symbol;
                    } elseif ($key == 'GroupCode') {
                        $GroupCode = $CI->safa_umrahgroups_passports_model->get_group_id($inputvalue['MD_PASSPORT_NO']);
                        $mutamers[$a][$key] = $GroupCode->safa_umrahgroup_id;
                    } elseif ($key == 'PackageCode')
                        $mutamers[$a][$key] = 'OFFLN';
                    else
                        $mutamers[$a][$key] = '';
                }
            }$a++;
        }
//        print_r($mutamers);
//        exit();
        header("Content-Type: text/plain; charset=windows-1256");
        $file = '';
        $file.='B1O1R1';
        foreach ($structure as $key => $value) {
            $file.=',' . $key;
        }

        foreach ($mutamers as $mutamer) {
            $file.=',E1O1R1';
            $file.="\n";
            $file.='B1O1R1';
            foreach ($mutamer as $key => $value) {
                $file.=',' . $value;
            }
        }

        $file.=',E1O1R1';
        $file.="\n";
        $file.='E1O1F1';
        foreach ($structure as $key => $value) {
            $file.=',' . $key;
        }
        $file.=',E1O1R1';

        //By Gouda
        $file = iconv("UTF-8", "windows-1256", $file);

        return $file;
    } elseif ($file_type == 4 || $file_type == 'FileSafa') {
        $structure = array(
            'PilgrimID' => 'MD_NO',
            'Title' => 'MD_TITLE_ID',
            'FirstName' => 'MD_FIRST_NAME_LA',
            'AFirstName' => 'MD_FIRST_NAME_AR',
            'FatherName' => 'MD_SECOND_NAME_LA',
            'AFatherName' => 'MD_SECOND_NAME_AR',
            'LastName' => 'MD_FOURTH_NAME_LA',
            'ALastName' => 'MD_FOURTH_NAME_AR',
            'MotherName' => '',
            'AMotherName' => '',
            'MiddleName' => 'MD_THIRD_NAME_LA',
            'AMiddleName' => 'MD_THIRD_NAME_AR',
            'Sex' => 'MD_GENDER_ID',
            'BirthDate' => 'MD_DATE_OF_BIRTH',
            'BirthCountry' => 'MD_BIRTH_COUNTRY_ID',
            'BirthCity' => 'MD_BIRTH_CITY',
            'MaritalStatus' => 'MD_MARITAL_STATUS_ID',
            'Education' => 'MD_EDUCATIONAL_LEVEL',
            'job' => 'MD_OCCUPATION',
            'GroupCode' => '',
            'SponserID' => 'MD_RELATIVE_NO',
            'RelationWithSponser' => 'MD_RELATIVE_RELATION_ID',
            'No_Of_Relatives' => 'MD_PASSPORT_DPN_COUNT',
            'Dpn_Serial_No' => 'MD_DPN_SERIAL_NO',
            'CNationality' => 'MD_NATIONALITY_ID',
            'PrevNationality' => 'MD_PREVIOUS_NATIONALITY_ID',
            'PPNo' => 'MD_PASSPORT_NO',
            'PPType' => 'MD_PASSPORT_TYPE',
            'PPIssueCity' => 'MD_PASSPORT_ISSUING_CITY',
            'PPIssueDate' => 'MD_PASSPORT_ISSUE_DATE',
            'PPExpDate' => 'MD_PASSPORT_EXPIRY_DATE',
            'PPIssueCountry' => 'MD_PASSPORT_ISSUING_COUNTRY_ID',
            //'CAddStreet' => '',
            'CAddAStreet' => '',
            //'CAddCity' => '',
            'CAddaCity' => 'MD_CITY',
            //'CAddState' => '',
            'CAddAstate' => '',
            'CAddCountry' => 'MD_COUNTRY_ID',
            //'CAddZip' => '',
            //'CAddMobile' => '',
            //'CAddTel' => '',
            //'CAddFax' => '',
            //'FemaleId' => '',
            'Mofa' => 'MD_MOFA',
            'ENumber' => 'MD_ENUMBER',
            'IdNumber' => 'MD_IDNUMBER',
            'Job2' => 'MD_JOB2',
            'Address' => 'MD_ADDRESS',
        );

        $a = 0;
        $input = $array['MUTAMERS'];
        foreach ($input as $inputvalue) {
            foreach ($structure as $key => $value) {
                if (isset($inputvalue[$value])) {
                    if ($key == 'PPIssueDate' || $key == 'PPExpDate' || $key == 'BirthDate')
                        $mutamers[$a][$key] = date("m/d/Y", strtotime($inputvalue[$value]));
                    else
                        $mutamers[$a][$key] = $inputvalue[$value];
                }
                else {
                    if ($key == 'No_Of_Relatives' || $key == 'Dpn_Serial_No' || $key == 'Emb_City')
                        $mutamers[$a][$key] = '0';
                    elseif ($key == 'Religion')
                        $mutamers[$a][$key] = '1';
                    elseif ($key == 'UO_CODE') {
                        $GroupCode = $CI->safa_umrahgroups_passports_model->get_group_id($inputvalue['MD_PASSPORT_NO']);
                        $uo_code = $CI->safa_umrahgroups_passports_model->get_uo_code($inputvalue['MD_PASSPORT_NO'], $GroupCode->safa_umrahgroup_id);
                        $mutamers[$a][$key] = $uo_code->code;
                    } elseif ($key == 'EA_CODE') {
                        $GroupCode = $CI->safa_umrahgroups_passports_model->get_group_id($inputvalue['MD_PASSPORT_NO']);
                        $ea_code = $CI->safa_umrahgroups_passports_model->get_ea_code($inputvalue['MD_PASSPORT_NO'], $GroupCode->safa_umrahgroup_id);
                        $mutamers[$a][$key] = $ea_code->agency_symbol;
                    } elseif ($key == 'GroupCode') {
                        $GroupCode = $CI->safa_umrahgroups_passports_model->get_group_id($inputvalue['MD_PASSPORT_NO']);
                        $mutamers[$a][$key] = $GroupCode->safa_umrahgroup_id;
                    } elseif ($key == 'PackageCode')
                        $mutamers[$a][$key] = 'OFFLN';
                    else
                        $mutamers[$a][$key] = '';
                }
            }$a++;
        }
//        print_r($mutamers);
//        exit();
        header("Content-Type: text/plain; charset=windows-1256");
        $file = '';

        //Commented By Gouda
        //$file.='B1O1R1';

        $counter = 0;
        foreach ($structure as $key => $value) {
            if ($counter == 0) {
                $file.= $key;
            } else {
                $file.=',' . $key;
            }
            $counter++;
        }


        foreach ($mutamers as $mutamer) {
            //$file.=',E1O1R1';
            $file.="\n";
            //$file.='B1O1R1';

            $counter = 0;
            foreach ($mutamer as $key => $value) {
                if ($counter == 0) {
                    $file.= $value;
                } else {
                    $file.=',' . $value;
                }
                $counter++;
            }
        }

        /*
          //$file.=',E1O1R1';
          $file.="\n";
          //$file.='E1O1F1';

          $counter=0;
          foreach ($structure as $key => $value) {
          if($counter==0) {
          $file.= $key;
          } else {
          $file.=',' . $key;
          }
          $counter++;
          }
          //$file.=',E1O1R1';
         */



        //By Gouda
        $file = iconv("UTF-8", "windows-1256", $file);

        return $file;
    }
}

function fget_contents() {
    $args = func_get_args();
    // the @ can be removed if you lower error_reporting level
    $contents = @call_user_func_array('file_get_contents', $args);

    if ($contents === false) {
        throw new Exception('Failed to open ' . $file);
    } else {
        return $contents;
    }
}

function trans($date) {
    $d = explode('/', $date);
    return $d[2].'-'.$d[1].'-'.$d[0];
}