<?php

function gen_itconf() {
    $ci = & get_instance();
    if (session('uo_id')) {
        $ci->load->model('safa_uos_model');
        $ci->safa_uos_model->safa_uo_id = session('uo_id');
        $uo = $ci->safa_uos_model->get();
    }
    if (session('ea_id')) {
        $uo = $ci->db->where('safa_uo_contracts_eas.safa_ea_id', session('ea_id'))
                ->join('safa_uo_contracts', 'safa_uos.safa_uo_id = safa_uo_contracts.safa_uo_id')
                ->join('safa_uo_contracts_eas', 'safa_uo_contracts.safa_uo_contract_id = safa_uo_contracts_eas.safa_uo_contract_id')
                ->get('safa_uos')
                ->row();
    }

    //This section Added By Gouda, to get code when ITO add Internal trip.
    if (session('ito_id')) {
        $uo = $ci->db->where('safa_uo_contracts_itos.safa_ito_id', session('ito_id'))
                ->join('safa_uo_contracts', 'safa_uos.safa_uo_id = safa_uo_contracts.safa_uo_id')
                ->join('safa_uo_contracts_itos', 'safa_uo_contracts.safa_uo_contract_id = safa_uo_contracts_itos.safa_uo_contract_id')
                ->get('safa_uos')
                ->row();
    }

    $ci->load->model('trip_internaltrip_model');
    $ci->trip_internaltrip_model->uo_id = $uo->safa_uo_id;
    $ci->trip_internaltrip_model->order_by[0] = 'safa_trip_internaltrips.confirmation_number';
    $ci->trip_internaltrip_model->order_by[1] = 'DESC';
    $ci->trip_internaltrip_model->get_single_row = TRUE;
    $ci->trip_internaltrip_model->limit = 1;
    $latest_trip = $ci->trip_internaltrip_model->get();
	
    // By Gouda.
	//    if (count($latest_trip) > 0)
	//        $conf = (int) str_replace($uo->code . '-', '', $latest_trip[0]->confirmation_number);
	//    else
	//        $conf = (int) str_replace($uo->code . '-', '', 0);

    //return $uo->code . '-' . sprintf('%06d', $conf + 1);
    
    $conf =  0;
    if (count($latest_trip) > 0) {
    	if(isset($latest_trip[0])) {
	  		$conf = (int) $latest_trip[0]->confirmation_number;
    	}
    }
    return $conf + 1;
}
