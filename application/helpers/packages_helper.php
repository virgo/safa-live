<?php

if (!function_exists('get_roomsize_price')) {

    function get_roomsize_price($peroid_id = FALSE, $roomsize = FALSE) {
        if (!$peroid_id || !$roomsize)
            return '0';

        $CI = & get_instance();
        $CI->db->where('safa_package_periods_id', $peroid_id);
        $CI->db->where('erp_hotelroomsize_id', $roomsize);
        $prices = $CI->db->get('safa_package_periods_prices')->row();

        if (!$prices)
            return '0';

        return $prices->price;
    }

}
///////////////////////////////////////////////////////////////////////////////
if (!function_exists('get_nights_price')) {

    function get_nights_price($night_id = FALSE, $roomsize = FALSE) {
        if (!$night_id || !$roomsize)
            return '0';

        $CI = & get_instance();
        $CI->db->where('safa_package_execlusive_night_id', $night_id);
        $CI->db->where('erp_hotelroomsize_id', $roomsize);
        $prices = $CI->db->get('safa_package_execlusive_nights_prices')->row();

        if (!$prices)
            return '0';

        return $prices->price;
    }

}
///////////////////////////////////////////////////////////////////////////////
if (!function_exists('get_package_hotel_nights_price')) {

    function get_package_hotel_nights_price($safa_package_hotel_id = FALSE, $roomsize = FALSE) {
        if (!$safa_package_hotel_id || !$roomsize)
            return '0';

        $CI = & get_instance();
        $CI->db->where('safa_package_hotel_id', $safa_package_hotel_id);
        $CI->db->where('erp_hotelroomsize_id', $roomsize);
        $prices = $CI->db->get('safa_package_hotels_prices')->row();

        if (!$prices)
            return '0';

        return $prices->price;
    }

}
///////////////////////////////////////////////////////////////////////////////
if (!function_exists('get_package_hotel_city')) {

    function get_package_hotel_city($package_id = FALSE, $city_id = 1) {
        if (!$package_id)
            return '';

        $CI = & get_instance();
        $CI->db->select('erp_hotels.name_ar,erp_hotels.name_la');
        $CI->db->join('erp_hotels', 'safa_package_hotels.erp_hotel_id = erp_hotels.erp_hotel_id');
        $CI->db->where('safa_package_hotels.safa_package_id', $package_id);
        $CI->db->where('erp_hotels.erp_city_id', $city_id);
        $hotel = $CI->db->get('safa_package_hotels')->row();

        if (!$hotel)
            return ' ';

        return $hotel->{name()};
    }

}
///////////////////////////////////////////////////////////////////////////////
if (!function_exists('get_peroid_night_count')) {

    function get_peroid_night_count($peroid_id = FALSE, $city_id = 1) {
        
        if (!$peroid_id)
            return '0';

        $CI = & get_instance();
        $CI->db->where('erp_package_periods_cities.erp_package_period_id', $peroid_id);
        $CI->db->where('erp_package_periods_cities.erp_city_id', $city_id);
        $days = $CI->db->get('erp_package_periods_cities')->row();

        if (!$days)
            return '0';

        return $days->city_days;
    }

}
///////////////////////////////////////////////////////////////////////////////
if (!function_exists('get_package_by_contract')) {

    function get_package_by_contract($contract_id = FALSE) {
        if (!$contract_id)
            return array();

        $returnpackages = array();
        $CI = & get_instance();
        $country = $CI->db->where('safa_uo_contract_id', $contract_id)->get('safa_uo_contracts')->row();
        $countryid = $country->country_id;
        $uo_id = $country->safa_uo_id;
        $query = <<<QUER
     select safa_packages.safa_package_id
FROM safa_packages
LEFT JOIN safa_packages_contracts ON safa_packages.safa_package_id = safa_packages_contracts.safa_package_id
LEFT JOIN safa_packages_countries ON safa_packages.safa_package_id = safa_packages_countries.safa_package_id
WHERE (
safa_packages_contracts.safa_package_id IS NULL OR 
( safa_packages_contracts.safa_uo_contract_id = '$contract_id' and safa_packages_contracts.`status` = 1 ) AND NOT
( safa_packages_contracts.safa_uo_contract_id = '$contract_id' and safa_packages_contracts.`status` = 0 )) AND (
safa_packages_countries.safa_package_id IS NULL OR
( safa_packages_countries.erp_country_id = '$countryid' and safa_packages_countries.`status` = 1) AND NOT
( safa_packages_countries.erp_country_id = '$countryid' and safa_packages_countries.`status` = 0)
) AND safa_packages.erp_company_id = '$uo_id' AND safa_packages.erp_company_type_id = 2
QUER;
        $packages = $CI->db->query($query)->result();

        foreach ($packages as $package)
            $returnpackages[] = $package->safa_package_id;
        return $returnpackages;
    }

}
