<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

if (!function_exists('check_id')) {

    function check_id($table, $col_name, $id_value) {
        $CI = & get_instance();
        $CI->db->select($col_name)->from($table)->where($col_name, $id_value);
        $record_set = $CI->db->get()->row();
        if (isset($record_set->$col_name)) {
            return 1;
        } else {
            return 0;
        }
    }

}