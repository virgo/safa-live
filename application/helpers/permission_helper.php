<?php

function permission($role = FALSE) {
	$SL = & get_instance();

	if (! session('type')) {
		if ($SL->uri->segment(1) == 'admin' or $SL->uri->segment(1) == 'uo'
		or $SL->uri->segment(1) == 'ito' or $SL->uri->segment(1) == 'ea' or $SL->uri->segment(1) == 'gov') {
			profiling(2);
			reset_cookies();
			redirect($SL->uri->segment(1));
		} else
		profiling (3);
		redirect();
	}
	if (!$role) {
		$role = NULL;
		$directory = $SL->router->directory;
		$class = $SL->router->class;
		$method = $SL->router->method;
		if ($directory)
		$role .= $directory;
		if ($class)
		$role .= $class . '/';
		if ($method && $method != 'index')
		$role .= $method . '/';
		$role = rtrim($role, '/');
	}
	
	if(session('type')=='admin') {
	    if( ! $SL->db->where('role_name', $role)->get('erp_'.session('type').'privileges')->num_rows())
	            $SL->db->insert('erp_'.session('type').'privileges', array('role_name' => $role, 'visible' => 0));
	} else {
		if( ! $SL->db->where('role_name', $role)->get('safa_'.session('type').'privileges')->num_rows())
	            $SL->db->insert('safa_'.session('type').'privileges', array('role_name' => $role, 'visible' => 0));
	}
	
	if (!check($role)) {
		profiling(1);
		redirect('error');
	}

}

function check($role = FALSE, $type = FALSE) {
	$SL = & get_instance();
	if (!$type)
	$type = session('type');

	if ($type == 'admin') {
		
		$visible = $SL->db->select('erp_adminprivileges.visible')
		->where('erp_adminprivileges.role_name', $role)
		->get('erp_adminprivileges')
		->row();
		
		$user = $SL->db->select('COUNT(*) as availability, erp_admins.password')
		->where('erp_admins.erp_admin_id', session('admin_user_id'))
		//                        ->where('erp_admins.password', session('admin_password'))
		                        ->where_in('erp_adminprivileges.role_name', $role)
		                        ->join('erp_admin_usergroups', 'erp_admin_usergroups.erp_admin_usergroup_id = erp_admins.erp_admin_usergroup_id')
		                        ->join('erp_admin_usergroups_adminprivileges', 'erp_admin_usergroups_adminprivileges.erp_admin_usergroup_id = erp_admin_usergroups.erp_admin_usergroup_id')
		                        ->join('erp_adminprivileges', 'erp_admin_usergroups_adminprivileges.erp_adminprivilege_id = erp_adminprivileges.erp_adminprivilege_id')
		->get('erp_admins')
		->row();
	} elseif ($type == 'ea') {
		
		$visible = $SL->db->select('safa_eaprivileges.visible')
		->where('safa_eaprivileges.role_name', $role)
		->get('safa_eaprivileges')
		->row();
		
		$user = $SL->db->select('COUNT(*) as availability, safa_ea_users.password')
		->where('safa_ea_users.safa_ea_user_id', session('ea_user_id'))
		//                        ->where('safa_ea_users.password', session('ea_password'))
		->where('safa_ea_users.safa_ea_id', session('ea_id'))
		                        ->where_in('safa_eaprivileges.role_name', $role)
		                        ->join('safa_ea_usergroups', 'safa_ea_usergroups.safa_ea_usergroup_id = safa_ea_users.safa_ea_usergroup_id')
		                        ->join('safa_ea_usergroups_eaprivileges', 'safa_ea_usergroups_eaprivileges.safa_ea_usergroup_id = safa_ea_usergroups.safa_ea_usergroup_id')
		                        ->join('safa_eaprivileges', 'safa_eaprivileges.safa_eaprivilege_id = safa_ea_usergroups_eaprivileges.safa_eaprivilege_id')
		->get('safa_ea_users')
		->row();

	} elseif ($type == 'uo') {
		
		$visible = $SL->db->select('safa_uoprivileges.visible')
		->where('safa_uoprivileges.role_name', $role)
		->get('safa_uoprivileges')
		->row();
		//$visible_l_q = $SL->db->last_query(); 
		
		
		$user = $SL->db->select('COUNT(*) as availability, safa_uo_users.password')
		->where('safa_uo_users.safa_uo_user_id', session('uo_user_id'))
		//                        ->where('safa_uo_users.password', session('uo_password'))
		->where('safa_uo_users.safa_uo_id', session('uo_id'))
		->where_in('safa_uoprivileges.role_name', $role)
		->join('safa_uo_usergroups', 'safa_uo_usergroups.safa_uo_usergroup_id = safa_uo_users.safa_uo_usergroup_id')
		->join('safa_uo_usergroups_uoprivileges', 'safa_uo_usergroups_uoprivileges.safa_uo_usergroup_id = safa_uo_usergroups.safa_uo_usergroup_id')
		->join('safa_uoprivileges', 'safa_uoprivileges.safa_uoprivilege_id = safa_uo_usergroups_uoprivileges.safa_uoprivilege_id')
		->get('safa_uo_users')
		->row();

		

	} elseif ($type == 'hm') {
		
		$visible = $SL->db->select('safa_hmprivileges.visible')
		->where('safa_hmprivileges.role_name', $role)
		->get('safa_hmprivileges')
		->row();
		
		$user = $SL->db->select('COUNT(*) as availability, safa_hm_users.password')
		->where('safa_hm_users.safa_hm_user_id', session('hm_user_id'))
		//                        ->where('safa_hm_users.password', session('password'))
		->where('safa_hm_users.safa_hm_id', session('hm_id'))
		                        ->where_in('safa_hmprivileges.role_name', $role)
		                        ->join('safa_hm_usergroups', 'safa_hm_usergroups.safa_hm_usergroup_id = safa_hm_users.safa_hm_usergroup_id')
		                        ->join('safa_hm_usergroups_hmprivileges', 'safa_hm_usergroups_hmprivileges.safa_hm_usergroup_id = safa_hm_usergroups.safa_hm_usergroup_id')
		                        ->join('safa_hmprivileges', 'safa_hmprivileges.safa_hmprivilege_id = safa_hm_usergroups_hmprivileges.safa_hmprivilege_id')
		->get('safa_hm_users')
		->row();
	} elseif ($type == 'ito') {
		
		$visible = $SL->db->select('safa_itoprivileges.visible')
		->where('safa_itoprivileges.role_name', $role)
		->get('safa_itoprivileges')
		->row();
		
		$user = $SL->db->select('COUNT(*) as availability, safa_ito_users.password')
		->where('safa_ito_users.safa_ito_user_id', session('ito_user_id'))
		//                        ->where('safa_ito_users.password', session('password'))
		->where('safa_ito_users.safa_ito_id', session('ito_id'))
		                        ->where_in('safa_itoprivileges.role_name', $role)
		                        ->join('safa_ito_usergroups', 'safa_ito_usergroups.safa_ito_usergroup_id = safa_ito_users.safa_ito_usergroup_id')
		                        ->join('safa_ito_usergroups_itoprivileges', 'safa_ito_usergroups_itoprivileges.safa_ito_usergroup_id = safa_ito_usergroups.safa_ito_usergroup_id')
		                        ->join('safa_itoprivileges', 'safa_itoprivileges.safa_itoprivilege_id = safa_ito_usergroups_itoprivileges.safa_itoprivilege_id')
		->get('safa_ito_users')
		->row();
	}  elseif ($type == 'gov') {
		
		$visible = $SL->db->select('safa_govprivileges.visible')
		->where('safa_govprivileges.role_name', $role)
		->get('safa_govprivileges')
		->row();
		
		$visible_l_q = $SL->db->last_query();
		
		$user = $SL->db->select('COUNT(*) as availability, safa_gov_users.password')
		->where('safa_gov_users.safa_gov_user_id', session('gov_user_id'))
		//                        ->where('safa_gov_users.password', session('password'))
		->where('safa_gov_users.safa_gov_id', session('gov_id'))
		                        ->where_in('safa_govprivileges.role_name', $role)
		                        ->join('safa_gov_usergroups', 'safa_gov_usergroups.safa_gov_usergroup_id = safa_gov_users.safa_gov_usergroup_id')
		                        ->join('safa_gov_usergroups_govprivileges', 'safa_gov_usergroups_govprivileges.safa_gov_usergroup_id = safa_gov_usergroups.safa_gov_usergroup_id')
		                        ->join('safa_govprivileges', 'safa_govprivileges.safa_govprivilege_id = safa_gov_usergroups_govprivileges.safa_govprivilege_id')
		->get('safa_gov_users')
		->row();

	}

	if(isset($visible) && $visible->visible==0) {
//		if(sha1(PASSWORD_SALT.$user->password) == session('password')) {
			return TRUE;
//		} else {
//			echo $l_q = $SL->db->last_query();
//			if($visible_l_q) {
//				echo $visible_l_q; 
//			}
//			exit;
//			
//			profiling(5);
//			reset_cookies();
//		}
	} else {
		if($user->availability)
		{
			if(sha1(PASSWORD_SALT.$user->password) == session('password'))
			return TRUE;
			else {
				profiling(5);
				reset_cookies();
			}
		} else {
			//profiling(4);
			
//			echo $l_q = $SL->db->last_query();
//			if($visible_l_q) {
//				echo $visible_l_q; 
//			}
//			exit;
			
			//reset_cookies();
			
			return false;
		}
	}
}

function SendMail ($var){ //($subject, $body) {
	//    $SMTPIN = fsockopen ("ssl://server3.virgo-host.com", 465);
	//    $SMTPIN = fsockopen ("ssl://smtp.gmail.com", 465);
	//    fputs ($SMTPIN, "EHLO ".$HTTP_HOST."\r\n");
	//    $talk["hello"] = fgets ( $SMTPIN, 1024 );
	//    fputs($SMTPIN, "auth login\r\n");
	//    $talk["res"]=fgets($SMTPIN,1024);
	//    fputs($SMTPIN, base64_encode("noreply@uo.umrah-booking.com")."\r\n");
	//    $talk["user"]=fgets($SMTPIN,1024);
	//    fputs($SMTPIN, base64_encode("Kg@Q;mSuXe^i")."\r\n");
	//    $talk["pass"]=fgets($SMTPIN,256);
	//    fputs ($SMTPIN, "MAIL FROM: <m.elsaeed@virgotel.com>\r\n");
	//    $talk["From"] = fgets ( $SMTPIN, 1024 );
	//    fputs ($SMTPIN, "RCPT TO: <m.elsaeed@virgotel.com>\r\n");
	//    $talk["To"] = fgets ($SMTPIN, 1024);
	//    fputs($SMTPIN, "DATA\r\n");
	//    $talk["data"]=fgets( $SMTPIN,1024 );
	//    fputs($SMTPIN, "To: <m.elsaeed@virgotel.com>\r\nFrom: <m.elsaeed@virgotel.com>\r\nSubject:".$subject."\r\n\r\n\r\n".$body."\r\n.\r\n");
	//    $talk["send"]=fgets($SMTPIN,256);
	//    fputs ($SMTPIN, "QUIT\r\n");
	//    fclose($SMTPIN);

	$fp = fopen('./login_errors/' . microtime(), 'w');
	fwrite($fp, json_encode($var));
	fclose($fp);
}

function profiling($id) {

	$info["TITLE"] = 'LOGOUT LOG #'. $id;
	if(isset($_COOKIES))
	$info["COOKIES"] = appendToProfiller($_COOKIES);
	$info["SERVER"] = appendToProfiller($_SERVER);
	$info["GET"] = appendToProfiller($_GET);
	$info["POST"] = appendToProfiller($_POST);
	if(isset($_SESSION))
	$info["SESSION"] = appendToProfiller($_SESSION);
	$info["ENV"] = appendToProfiller($_ENV);

	SendMail($info);
}

function appendToProfiller($array) {
	//    $result = NULL;
	if(isset($array) && is_array($array))
	//    foreach($array as $key => $value){
	//        $result .= $key . ': ' . $value;
	//    }
	return $array;
}