<?php

/*
 * Display date with a custom format
 * @param date
 * @param string
 * @param type string (date, time or datetime)
 * @return date
 * @author: Muhammad El-Saeed
 */

function view_date($date, $type = 'datetime', $custom_format = FALSE) {
    $date = strtotime($date);
    if ($custom_format)
        return date($custom_format, $date);
    if ($type == 'date')
        return date(DATE_FORMAT, $date);
    elseif ($type == 'datetime')
        return date(DATETIME_FORMAT, $date);
}

/*
 * convert date/time to SQL format
 * @param date
 * @param string
 * @param string
 * @return date
 * @author: Muhammad El-Saeed
 */

function sql_date($date) {
    return date($date, 'Y-m-d h:m:s');
}

function get_notifications() {
    
}
