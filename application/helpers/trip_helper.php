<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

function get_date($datetime) {

    if ($datetime) {

        $d = new DateTime($datetime);
        $q = $d->format('m-d');
        return $q;
    } else {
        return "";
    }
}

function get_time($datetime) {

    if ($datetime) {

        $d = new DateTime($datetime);
        $q = $d->format('H:i');
        return $q;
    } else {
        return "";
    }
}

if (!function_exists('get_reserve_status')) {

    function get_reserve_status($status_id) {
        $CI =& get_instance();
        
        $lookup  = $CI->db->where('erp_hotels_reservation_orders_status_id',$status_id)->get('erp_hotels_reservation_orders_status')->row();
        
        return $lookup->{name()};
    }

}