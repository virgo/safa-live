<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
if (!function_exists('get_uo_companies')) {

    function get_uo_companies($detail_id = FALSE, $status = '1') {
        if (!$detail_id)
            return array();

        $CI = & get_instance();
        $CI->load->model('hotels_availability_details_uo_company_model', 'uocomp');
        $CI->uocomp->status = $status;
        $CI->uocomp->erp_hotels_availability_detail_id = $detail_id;
        $companies = $CI->uocomp->get();
        $returncomp = array();
        foreach ($companies as $company) {
            $returncomp[] = $company->safa_uo_id;
        }

        return $returncomp;
    }

}

if (!function_exists('get_hm_companies')) {

    function get_hm_companies($detail_id = FALSE, $status = '1') {
        if (!$detail_id)
            return array();

        $CI = & get_instance();
        $CI->load->model('hotels_availability_details_hm_company_model', 'hmcomp');
        $CI->hmcomp->status = $status;
        $CI->hmcomp->erp_hotels_availability_detail_id = $detail_id;
        $companies = $CI->hmcomp->get();
        $returncomp = array();
        foreach ($companies as $company) {
            $returncomp[] = $company->safa_hm_id;
        }

        return $returncomp;
    }

}

if (!function_exists('get_ea_countries')) {

    function get_ea_countries($detail_id = FALSE, $status = '1') {
        if (!$detail_id)
            return array();

        $CI = & get_instance();
        $CI->load->model('hotels_availability_details_country_model', 'eacountry');
        $CI->eacountry->status = $status;
        $CI->eacountry->erp_hotels_availability_detail_id = $detail_id;
        $companies = $CI->eacountry->get();
        $returncomp = array();
        foreach ($companies as $company) {
            $returncomp[] = $company->erp_country_id;
        }

        return $returncomp;
    }

}

if (!function_exists('get_ea_companies')) {

    function get_ea_companies($detail_id = FALSE, $status = '1') {
        if (!$detail_id)
            return array();

        $CI = & get_instance();
        $CI->load->model('hotels_availability_details_ea_company_model', 'eacomp');
        $CI->eacomp->status = $status;
        $CI->eacomp->erp_hotels_availability_detail_id = $detail_id;
        $companies = $CI->eacomp->get();
        $returncomp = array();
        foreach ($companies as $company) {
            $returncomp[] = $company->safa_ea_id;
        }

        return $returncomp;
    }

}

if (!function_exists('get_res_uo_companies')) {

    function get_res_uo_companies($detail_id = FALSE, $status = '1') {
        if (!$detail_id)
            return array();

        $CI = & get_instance();
        $CI->load->model('erp_room_execlusive_services_uo_company_model', 'resuocomp');
        $CI->resuocomp->status = $status;
        $CI->resuocomp->erp_room_exclusive_service_id = $detail_id;
        $companies = $CI->resuocomp->get();
        $returncomp = array();

        foreach ($companies as $company) {
            $returncomp[] = $company->safa_uo_id;
        }
        return $returncomp;
    }

}

if (!function_exists('get_res_hm_companies')) {

    function get_res_hm_companies($detail_id = FALSE, $status = '1') {
        if (!$detail_id)
            return array();

        $CI = & get_instance();
        $CI->load->model('erp_room_execlusive_services_hm_company_model', 'reshmcomp');
        $CI->reshmcomp->status = $status;
        $CI->reshmcomp->erp_room_exclusive_service_id = $detail_id;
        $companies = $CI->reshmcomp->get();
        $returncomp = array();
        foreach ($companies as $company) {
            $returncomp[] = $company->safa_hm_id;
        }

        return $returncomp;
    }

}

if (!function_exists('get_res_ea_countries')) {

    function get_res_ea_countries($detail_id = FALSE, $status = '1') {
        if (!$detail_id)
            return array();

        $CI = & get_instance();
        $CI->load->model('erp_room_execlusive_services_country_model', 'reseacountry');
        $CI->reseacountry->status = $status;
        $CI->reseacountry->erp_room_exclusive_service_id = $detail_id;
        $companies = $CI->reseacountry->get();
        $returncomp = array();
        foreach ($companies as $company) {
            $returncomp[] = $company->erp_country_id;
        }

        return $returncomp;
    }

}

if (!function_exists('get_res_ea_companies')) {

    function get_res_ea_companies($detail_id = FALSE, $status = '1') {
        if (!$detail_id)
            return array();

        $CI = & get_instance();
        $CI->load->model('erp_room_execlusive_services_ea_company_model', 'reseacomp');
        $CI->reseacomp->status = $status;
        $CI->reseacomp->erp_room_exclusive_service_id = $detail_id;
        $companies = $CI->reseacomp->get();
        $returncomp = array();
        foreach ($companies as $company) {
            $returncomp[] = $company->safa_ea_id;
        }

        return $returncomp;
    }

}

if (!function_exists('get_h_img')) {

    function get_h_img($hotelid) {
        $CI = & get_instance();
        $CI->load->helper('directory');
        $files = directory_map("static/hotel_album/" . $hotelid, 1);
        return $files[0];
    }

}

if (!function_exists('get_room_services')) {

    function get_room_services($master_id) {
        $CI = & get_instance();
        $CI->db->select('erp_room_services.' . name() . ',sale_price');
        $CI->db->where('erp_room_execlusive_services.erp_hotels_availability_master_id', $master_id);
        $CI->db->join('erp_room_services', 'erp_room_services.erp_room_service_id = erp_room_execlusive_services.erp_room_service_id');
        $paidservice = $CI->db->get('erp_room_execlusive_services')->result();

        $CI->db->select('erp_room_services.' . name());
        $CI->db->where('erp_room_inclusive_services.erp_hotels_availability_master_id', $master_id);
        $CI->db->join('erp_room_services', 'erp_room_services.erp_room_service_id = erp_room_inclusive_services.erp_room_service_id');
        $freeservice = $CI->db->get('erp_room_inclusive_services')->result();
        return array('free' => $freeservice, 'paid' => $paidservice);
    }

}

if (!function_exists('get_company_name')) {

    function get_company_name($company_id, $company_type) {
        $CI = & get_instance();
        if ($company_type == 2) {
            $company = $CI->db->where('safa_uo_id', $company_id)->get('safa_uos')->row();
        } else if ($company_type == 3) {
            $company = $CI->db->where('safa_ea_id', $company_id)->get('safa_eas')->row();
        }
        return (isset($company)) ? $company->{name()} : '';
    }

}