<?php

require_once('tcpdf/examples/lang/ara.php');
require_once('tcpdf/tcpdf.php');

class virgo_pdf extends TCPDF {

    //Page header
    public function Header22() {
        // Logo
        $image_file = 'static/images/alrahma.jpg';
        //$this->Image($image_file, 500, 10, 50, '', 'JPG', '', 'T', false, 300, '', false, false, 0, false, false, false);
        $tahoma = $this->addTTFfont('static/fonts/arial.ttf', 'TrueTypeUnicode', '', 96);
        $this->SetFont($tahoma, '', 10, '', false);
        // Title
        //$this->writeHTML("(( بيان وصول أفواج المعتمرين للديار المقدسة ))<br /> رقم ((    ))", True,false,false,false,'C');
    }
    
    public function Header() {
        $this->setRTL(FALSE);
        // get the current page break margin
        $bMargin = $this->getBreakMargin();
        // get current auto-page-break mode
        $auto_page_break = $this->AutoPageBreak;
        // disable auto-page-break
        $this->SetAutoPageBreak(false, 0);
        // set bacground image
        
        $img_file = 'static/img/uo_companies_logos/'.session('uo_id').'/bg.jpg';
        if(is_file($img_file))
            $this->Image($img_file, 0, 0, 595, 842, '', '', '', false, 300, '', false, false, 0);
 
        // restore auto-page-break status
        $this->SetAutoPageBreak($auto_page_break, $bMargin);
        // set the starting point for the page content
        $this->setPageMark();
        
        $this->setRTL(true);
    }



}
