<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
if (!function_exists('get_user_views')) {

    function get_user_views($user_id, $module_name) {
        $CI = & get_instance();
        $CI->db->select("views_users.views_serialized");
        $CI->db->join('views_categories', 'views_categories.views_categories_id = views_users.views_categories_id');
        $CI->db->where('views_categories.module_name', $module_name);
        $CI->db->where('views_users.users_id', $user_id);
        $record = $CI->db->get("views_users")->row();
        return $record;
    }

    function get_default_views($module_name) {
        $CI = & get_instance();
        $CI->db->select("views.*");
        $CI->db->join('views_categories', 'views.views_categories_id=views_categories.views_categories_id');
        $CI->db->where('views_categories.module_name', $module_name);
        $CI->db->order_by("views.order");
        $rows = $CI->db->get("views")->result_array();
        return $rows;
    }

    function sorting($main_arr, $custom_arr) {
        $count = count($custom_arr);
        foreach ($main_arr as $view) {
            $flag = 0;
            foreach ($custom_arr as $cust_index => $cust_value) {
                if ($view["name"] == $cust_value) {
                    $flag = 1;
                    break;
                } else {
                    continue;
                }
            }
            if ($flag == 0) {
                $custom_arr[$count] = $view["name"];
                $count++;
            }
        }

        return $custom_arr;
    }

    function get_view($module) {
        $CI = & get_instance();
        $view = get_user_views(1, $module);

        if (($view->views_serialized)) {
            $arr_cols = unserialize($view->views_serialized);
            $CI->poll_model->custom_select = $arr_cols;
        } else {
            $view = get_default_views($module);
            if (isset($view)) {
                $arr_cols = seperate_views($view);
                $CI->poll_model->custom_select = $arr_cols;
            }
        }
        return $view;
    }

}   