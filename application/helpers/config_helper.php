<?php

if (!function_exists('config')) {

    function config($key = FALSE, $value = FALSE) {
        $ci = & get_instance();
        if ($value) {
            $ci->config->set_item($key, $value);
            return TRUE;
        } else {
            return $ci->config->item($key);
        }
    }

}

if (!function_exists('session')) {

    function session($key = FALSE, $value = FALSE) {
        if ($value) {
            setcookie($key, $value, strtotime('+1 year'), '/');
        } else {
            if(isset($_COOKIE[$key]))
                return $_COOKIE[$key];
            else
                return FALSE;
        }
    }
}

# SYSTEM REQUIREMENTS
if (!function_exists('url')) {
    if (isset($_SERVER['SCRIPT_NAME']) && isset($_SERVER['HTTP_HOST'])) {

        function url() {
            $base_url = isset($_SERVER['HTTPS']) && strtolower($_SERVER['HTTPS']) !== 'off' ? 'https' : 'http';
            $base_url .= '://' . $_SERVER['HTTP_HOST'];
            $base_url .= str_replace(basename($_SERVER['SCRIPT_NAME']), '', $_SERVER['SCRIPT_NAME']);
            return $base_url;
        }

    }
}

if (!function_exists('name')) {
    function name($suffix = false) {
        $CI = &get_instance();
        if ($CI->config->item('language') == 'arabic')
            if($suffix)
                return "ar";
            else
                return "name_ar";
        else
            if($suffix)
                return "la";
            else
                return "name_la";
    }

}
if (!function_exists('direction')) {
    function direction() {
        $CI = &get_instance();
        if ($CI->config->item('language') == 'arabic')
            return "rtl";
        else
            return "ltr";
    }
}
if (!function_exists('ensure')) {
    function ensure($var) {
        if (isset($var) && is_array($var) && count($var))
            return TRUE;
        else
            return FALSE;
    }
}
// PATHS
if (function_exists('url')) {
    define('NEW_IMAGES', url() . 'static/new_template/img');
    define('NEW_JS', url() . 'static/new_template/js');
    define('NEW_CSS', url() . 'static/new_template/css');
    define('NEW_CSS_JS', url() . 'static/new_template/css-js');
    define('MODULE_JS', url() . 'static/modules/js');
    define('CSS', url() . 'static/css');
    define('IMAGES', url() . 'static/img');
    define('IMAGES2', url() . 'static/images');
    define('CSS_JS', url() . 'static/css-js');
    define('JS', url() . 'static/js');
    define('DOMAIN', $_SERVER['SERVER_NAME']);
    define('URL', url());
    define('PER_PAGE', 20);
    define('HOTEL_REL_PATH', 'static/hotel_album');
    define('HOTEL_ABS_PATH', url().'static/hotel_album');
    
    //By Gouda.
    define('NEW_TEMPLATE', url() . 'static/new_template');
    define('STATIC_FOLDER_DIR', url() . 'static');
}
//if ($_SERVER['SERVER_NAME'] !== 'localhost')
//    register_shutdown_function("shutdown");
//
//function shutdown() {
//    $lasterror = error_get_last();
//    $msg = NULL;
//    if($lasterror)
//    foreach ($lasterror as $key => $value)
//        $msg .= $value . ' -> ';
//    mail("m.elsaeed@hotmail.com", "FATAL ERROR", $msg);
//    $CI = &get_instance();
//    $CI->db->set('title', "Fatal Error");
//    $CI->db->set('message', $msg);
//    $CI->db->set('agent', $_SERVER['HTTP_USER_AGENT']);
//    $CI->db->set('current_url', $_SERVER['PHP_SELF']);
//    $CI->db->set('trace', serialize(debug_backtrace()));
//    $CI->db->set('timestamp', date('Y-m-d H:i'));
//    $CI->db->insert('error_logs');
//}

function post($var) {
    $ci = & get_instance();
    return $ci->input->post($var);
}
function css() {
    $css = array(
        NEW_CSS_JS . '_' . lang('DIR') .'/common-css.css',
        NEW_CSS_JS . '_' . lang('DIR') .'/slide-menu.css',
        NEW_CSS_JS .'/font-awesome.css',
        NEW_CSS_JS . '_' . lang('DIR') .'/glyphicons_filetypes.css',
        NEW_CSS_JS . '_' . lang('DIR') .'/glyphicons_regular.css',
        NEW_CSS_JS . '_' . lang('DIR') .'/glyphicons_social.css',
        NEW_CSS_JS . '_' . lang('DIR') .'/jquery-ui.css'.
        NEW_CSS_JS . '_' . lang('DIR') .'/validationEngine.jquery.css',
        NEW_CSS_JS . '_' . lang('DIR') .'/tipsy.css',
        NEW_CSS_JS . '_' . lang('DIR') .'/accordionmenu.css',
        NEW_CSS_JS . '_' . lang('DIR') .'/wizard/formToWizard.css',
        NEW_JS .'/plugins/scroll/jquery.custom-scrollbar.css',
        NEW_CSS_JS . '_' . lang('DIR') .'/top-dropdown/css/styles.css',
        NEW_CSS_JS . '_' . lang('DIR') .'/multiselect/multiselect.css',
        NEW_CSS_JS . '_' . lang('DIR') .'/multi-select-chosen/chosen.css',
        NEW_CSS_JS . '_' . lang('DIR') .'/fancybox/jquery.fancybox.css',
        NEW_CSS_JS . '_' . lang('DIR') .'/datetimepicker/jquery-ui.css',
        NEW_CSS_JS . '_' . lang('DIR') .'/datetimepicker/jquery-ui-timepicker-addon.css',
        base_url("static/alertify") .'/alertify.core.css',
        base_url("static/alertify") .'/alertify.default.css',
    );
    foreach($this->css as $url)
        if(ENVIRONMENT == 'production')
            echo file_get_contents($url).' ';
        else
            echo file_get_contents($url).' ';
}
function js_() {
    
//        require_once APPPATH."libraries/js_minifier";
    $js = array(
        NEW_CSS_JS . '_' . lang('DIR') .'/jquery-1.9.1.js',
        NEW_CSS_JS . '_' . lang('DIR') .'/jquery-ui.js',
        NEW_CSS_JS . '_' . lang('DIR') .'/jquery.tipsy.js',
        NEW_CSS_JS . '_' . lang('DIR') .'/slide-menu.js',
        NEW_CSS_JS . '/wizard/formToWizard.js',
        NEW_JS .'/plugins/jquery/jquery-ui-1.10.1.custom.min.js',
        NEW_JS .'/plugins/jquery/jquery-migrate-1.1.1.min.js',
        NEW_JS .'/plugins/bootstrap/bootstrap.min.js',
        NEW_JS .'/plugins/bootstrap/bootstrap-dropdown.js',
        NEW_JS .'/plugins/bootstrap/bootstrap-fileupload.min.js',
        NEW_JS .'/plugins/pnotify/jquery.pnotify.min.js',
        NEW_JS .'/plugins/datatables/jquery.dataTables.min.js',
        NEW_JS .'/plugins/uniform/jquery.uniform.min.js',
        NEW_JS .'/plugins/scroll/jquery.custom-scrollbar.js',
        NEW_JS .'/plugins.js',
        base_url() .'static/js/plugins/lang-ar.js',
        NEW_JS .'/jquery.validationEngine-'. name(true) .'.js',
        NEW_JS .'/jquery.validationEngine.js',
        NEW_JS .'/jquery.equalheights.js',
        NEW_JS .'/plugins/select/select2.min.js',
        NEW_CSS_JS . '_' . lang('DIR') .'/multiselect/jquery.multi-select.min.js',
        NEW_CSS_JS . '_' . lang('DIR') .'/multi-select-chosen/chosen.jquery.js',
        NEW_CSS_JS . '_' . lang('DIR') .'/fancybox/jquery.fancybox.pack.js',
        NEW_CSS_JS . '_' . lang('DIR') .'/datetimepicker/jquery-ui.min.js',
        NEW_CSS_JS . '_' . lang('DIR') .'/datetimepicker/jquery-ui-sliderAccess.js',
        NEW_CSS_JS . '_' . lang('DIR') .'/datetimepicker/jquery-ui-timepicker-addon.js'
    );
    $return = NULL;
    foreach($js as $url)
//        if(ENVIRONMENT == 'production')
//            echo file_get_contents($url).' ';
//        else
            $return .= "<script type='text/javascript' src=\"$url\"></script>";
    return $return;
}

function js() {
    
//        require_once APPPATH."libraries/js_minifier";
    $js = array(
        NEW_CSS_JS . '_' . lang('DIR') .'/jquery-1.9.1.js',
        NEW_CSS_JS . '_' . lang('DIR') .'/jquery-ui.js',
        NEW_CSS_JS . '_' . lang('DIR') .'/jquery.tipsy.js',
        NEW_CSS_JS . '_' . lang('DIR') .'/slide-menu.js',
        NEW_CSS_JS . '/wizard/formToWizard.js',
        NEW_JS .'/plugins/jquery/jquery-ui-1.10.1.custom.min.js',
        NEW_JS .'/plugins/jquery/jquery-migrate-1.1.1.min.js',
        NEW_JS .'/plugins/bootstrap/bootstrap.min.js',
        NEW_JS .'/plugins/bootstrap/bootstrap-dropdown.js',
        NEW_JS .'/plugins/bootstrap/bootstrap-fileupload.min.js',
        NEW_JS .'/plugins/pnotify/jquery.pnotify.min.js',
        NEW_JS .'/plugins/datatables/jquery.dataTables.min.js',
        NEW_JS .'/plugins/uniform/jquery.uniform.min.js',
        NEW_JS .'/plugins/scroll/jquery.custom-scrollbar.js',
        NEW_JS .'/plugins.js',
        base_url() .'static/js/plugins/lang-ar.js',
        NEW_JS .'/jquery.validationEngine-'. name(true) .'.js',
        NEW_JS .'/jquery.validationEngine.js',
        NEW_JS .'/jquery.equalheights.js',
        NEW_JS .'/plugins/select/select2.min.js',
        NEW_CSS_JS . '_' . lang('DIR') .'/multiselect/jquery.multi-select.min.js',
        NEW_CSS_JS . '_' . lang('DIR') .'/multi-select-chosen/chosen.jquery.js',
        NEW_CSS_JS . '_' . lang('DIR') .'/fancybox/jquery.fancybox.pack.js',
        NEW_CSS_JS . '_' . lang('DIR') .'/datetimepicker/jquery-ui.min.js',
        NEW_CSS_JS . '_' . lang('DIR') .'/datetimepicker/jquery-ui-sliderAccess.js',
        NEW_CSS_JS . '_' . lang('DIR') .'/datetimepicker/jquery-ui-timepicker-addon.js'
    );
    $return = NULL;
    foreach($js as $url)
//        if(ENVIRONMENT == 'production')
//            echo file_get_contents($url).' ';
//        else
            $return .= "<script type='text/javascript' src=\"$url\"></script>";
    return $return;
}

function reset_cookies() {
    foreach($_COOKIE as $key => $value)
        if($key != 'language')
            setcookie($key, $value, time()-10, '/');
}