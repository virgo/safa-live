<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Logger {

    private $current_url;
    private $referral_url;
    private $agent;
    private $globals = array();
    private $profiling;
    public $CI;
    public $ip;

    public function __construct() {
        $this->CI = &get_instance();
    }

    public function trace($type = NULL, $message = NULL) {
        if (ENVIRONMENT == 'development')
            return;
        echo $_SERVER['SCRIPT_FILENAME'];
        // USER AGENT
        $this->agent = $_SERVER['HTTP_USER_AGENT'];
        // CURRENT URL
        $this->current_url = $_SERVER['PHP_SELF'];
        // REFERRAL URL
        $this->referral_url = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : NULL;
        // POST, GET, SERVER, SESSION AND COOKIES
        $this->globals = array(
            'post' => isset($_POST) ? $_POST : NULL,
            'get' => isset($_GET) ? $_GET : NULL,
            'server' => isset($_SERVER) ? $_SERVER : NULL,
            'session' => isset($_SESSION) ? $_SESSION : NULL,
            'cookie' => isset($_COOKIE) ? $_COOKIE : NULL,
            'env' => isset($_ENV) ? $_ENV : NULL,
        );
        // TIME
        $this->time = date('Y-m-d H:i');
        // (($usage = memory_get_usage()) != '' ? number_format($usage).' bytes'
        $var = array(
            'type' => $type,
            'message' => $message,
            'agent' => $this->agent,
            'current_url' => $this->current_url,
            'referral_url' => $this->referral_url,
            'globals' => $this->globals,
            'profiling' => $this->profiling,
            'timestamp' => $this->time
        );

        $this->write_log($var);
        @mail('m.elsaeed@virgotel.com', 'SL ERROR #' . $error_code, 'Check SL error #' . $this->time);
    }

    public function write_log($var) {
        $fp = fopen('./errors/' . date('Y-m-d H.i.s', strtotime($this->time)) . '.txt', 'w');
        fwrite($fp, json_encode($var));
        fclose($fp);
    }

}
