<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Accounting {

    Public $ci;
    public $entries_array;
    Public $id;
    Public $firm_id;
    Public $simple;
    Public $entry_number;
    Public $master_date;
    Public $currency;
    Public $rate = 1;
    Public $transaction_entry = '0';
    Public $transaction_kind;
    Public $transaction_id;
    Private $message;
    Private $error;
    Private $errorNo;

    function __construct() {
        $this->ci = &get_instance();

        $this->firm_id = $this->ci->session->userdata('firm_id');
        $this->entry_number = $this->getAvailableEntryNumber();
        $this->master_date = date('Y-m-d');
        $this->currency = $this->ci->config->item('default_currency');

        $this->ci->load->model('entries_master_model');
        $this->ci->load->model('entries_details_model');
        $this->ci->load->model('accounts_model');

        if (config('language') == 'arabic') {
            $this->message['1'] = 'من فضلك حدد الحساب';
            $this->message['2'] = 'من فضلك اكتب القيمة';
            $this->message['3'] = 'القيد غير متوازن';
        } else {
            $this->message['1'] = 'Account not set!!';
            $this->message['2'] = 'Value not set!!';
            $this->message['3'] = 'Entry not equivalent!!';
        }
    }

    Public function setRecord($account_id, $debit, $credit, $currency = FALSE, $rate = FALSE, $description = '', $remarks = '', $second_account_id = '0') {
        //
        $record['account'] = (int) $account_id;
        $record['debit'] = round((float) $debit, 2);
        $record['credit'] = round((float) $credit, 2);
        $record['description'] = $description;
        $record['remarks'] = $remarks;
        $record['second_account'] = (int) $second_account_id;
        if ($currency)
            $record['currency'] = (int) $currency;
        else
            $record['currency'] = $this->currency;
        if ($rate)
            $record['rate'] = round((float) $rate, 4);
        else
            $record['rate'] = $this->rate;

        $record['firm_id'] = $this->firm_id;
        $record['entry_number'] = $this->entry_number;
        $record['entry_date'] = $this->master_date;
        $record['voucher'] = $this->transaction_kind;

        $this->entries_array[] = $record;
    }

    Private function getDebitSum() {
        //
        $sum = 0;
        if (count($this->entries_array) > 0) {
            foreach ($this->entries_array as $entry) {
                $sum+=round($entry['debit'] * $entry['rate'], 2);
            }
        }
        return round($sum, 2);
    }

    Private function getCreditSum() {
        //
        $sum = 0;
        if (count($this->entries_array) > 0) {
            foreach ($this->entries_array as $entry) {
                $sum+=round($entry['credit'] * $entry['rate'], 2);
            }
        }
        return round($sum, 2);
    }

    Public function getBalance() {
        //
        $balance = (float) $this->getDebitSum() - (float) $this->getCreditSum();
        return round($balance, 2);
    }

    Private function validateEntry() {
        for ($i = 0; $i < $this->getRecordsCount(); $i++) {
            if (($this->entries_array[$i]['debit'] != 0 || $this->entries_array[$i]['credit'] != 0) && $this->entries_array[$i]['account'] <= 0) {
                $this->setError('1', $this->message['1']);
                return FALSE;
            } else if (($this->entries_array[$i]['debit'] != 0 && $this->entries_array[$i]['credit'] != 0)) {
                $this->setError('2', $this->message['2']);
                return FALSE;
            }
        }
        if ($this->getBalance() != 0) {
            $this->setError('3', $this->message['3']);
            return FALSE;
        }
        return TRUE;
    }

    Public function saveEntry() {
        //
        if ($this->validateEntry()) {
            if ($this->getRecordsCount() == 2) {
                $this->simple = '1';
                if ($this->entries_array[0]['second_account'] == '0')
                    $this->entries_array[0]['second_account'] = $this->entries_array[1]['account'];
                if ($this->entries_array[1]['second_account'] == '0')
                    $this->entries_array[1]['second_account'] = $this->entries_array[0]['account'];
            } else
                $this->simple = '0';
            $this->ci->entries_master_model->simple = $this->simple;
            $this->ci->entries_master_model->firm_id = $this->firm_id;
            $this->ci->entries_master_model->entry_number = $this->entry_number;
            $this->ci->entries_master_model->master_date = $this->master_date;
            $this->ci->entries_master_model->currency = $this->currency;
            $this->ci->entries_master_model->rate = $this->rate;
            $this->ci->entries_master_model->transaction_entry = $this->transaction_entry;
            $this->ci->entries_master_model->transaction_kind = $this->transaction_kind;
            $this->ci->entries_master_model->transaction_id = $this->transaction_id;
            $this->id = $this->ci->entries_master_model->save();

            for ($i = 0; $i < $this->getRecordsCount(); $i++) {
                if ($this->entries_array[$i]['debit'] > 0) {
                    $this->ci->entries_details_model->amount = $this->entries_array[$i]['debit'];
                    $this->ci->entries_details_model->debit = '1';
                    $amount = round($this->entries_array[$i]['debit'] * $this->entries_array[$i]['rate'], 2);
                    //+
                    $debit = 'debit';
                    $add = 'add';
                } elseif ($this->entries_array[$i]['credit'] > 0) {
                    $this->ci->entries_details_model->amount = $this->entries_array[$i]['credit'];
                    $this->ci->entries_details_model->debit = '0';
                    $amount = round($this->entries_array[$i]['credit'] * $this->entries_array[$i]['rate'], 2);
                    //-
                    $debit = 'credit';
                    $add = 'add';
                }
                $this->ci->entries_details_model->entry_id = $this->id;
                $this->ci->entries_details_model->firm_id = $this->firm_id;
                $this->ci->entries_details_model->entry_number = $this->entry_number;
                $this->ci->entries_details_model->entry_date = $this->master_date;
                $this->ci->entries_details_model->account = $this->entries_array[$i]['account'];
                $this->ci->entries_details_model->second_account = $this->entries_array[$i]['second_account'];
                $this->ci->entries_details_model->description = $this->entries_array[$i]['description'];
                $this->ci->entries_details_model->remarks = $this->entries_array[$i]['remarks'];
                $this->ci->entries_details_model->voucher = $this->entries_array[$i]['voucher'];
                $this->ci->entries_details_model->currency = $this->entries_array[$i]['currency'];
                $this->ci->entries_details_model->rate = (float) $this->entries_array[$i]['rate'];
                $this->ci->entries_details_model->save();
                $this->updateAccountAmount($this->entries_array[$i]['account'], $amount, $debit, $add);
            }
            $this->entries_array = FALSE;
            $this->entry_number = $this->getAvailableEntryNumber();
            return $this->id;
        } else
            return FALSE;
    }

    Public function deleteEntry($id, $voucher = FALSE) {
        //
        $this->ci->entries_master_model->id = $id;
        $master = $this->ci->entries_master_model->get();
        if ($master->transaction_entry == '0' || $voucher) {
            $this->ci->entries_details_model->entry_id = $id;
            $details = $this->ci->entries_details_model->get();
            foreach ($details as $detail) {
                if ($detail->debit == '1') {
                    //-
                    $amount = (float) $detail->amount * (float) $detail->rate;
                    $amount = round($amount, 2);
                    $this->updateAccountAmount($detail->account, $amount, 'debit', 'sub');
                }
                if ($detail->debit == '0') {
                    //+
                    $amount = (float) $detail->amount * (float) $detail->rate;
                    $amount = round($amount, 2);
                    $this->updateAccountAmount($detail->account, $amount, 'credit', 'sub');
                }
                $this->ci->entries_details_model->id = $detail->id;
                $this->ci->entries_details_model->delete();
            }
            $this->ci->entries_master_model->id = $id;
            $this->ci->entries_master_model->delete();

            $this->ci->entries_master_model->id = FALSE;
            $this->ci->entries_details_model->id = FALSE;
            $this->ci->entries_details_model->entry_id = FALSE;
            return TRUE;
        } else {
            $this->ci->entries_master_model->id = FALSE;
            return FALSE;
        }
    }

    Public function getRecordsCount() {
        return count($this->entries_array);
    }

    Private function setError($errno, $err) {
        $this->error = $err;
        $this->errorNo = $errno;
    }

    Public function getError($no = FALSE) {
        if ($no)
            return $this->errorNo;
        else
            return $this->error;
    }

////////////////////////////////////////////////////////////////

    Public function updateAccountAmount($account_id, $amount, $debit, $add) {
        $this->ci->db->where('id', $account_id);
        $this->ci->db->where('firm_id', $this->ci->session->userdata('firm_id'));
        $account = $this->ci->db->get('accounts')->row();
        if ($debit == 'debit') {
            if ($add == 'add') {
                $this->addDebit($account_id, $amount);
            } else {
                $this->subDebit($account_id, $amount);
            }
        } else {
            if ($add == 'add') {
                $this->addCredit($account_id, $amount);
            } else {
                $this->subCredit($account_id, $amount);
            }
        }
        if ($account)
            if ($account->parent != '0')
                $this->updateAccountAmount($account->parent, $amount, $debit, $add);
    }

    Private function addDebit($account_id, $amount) {
        $amount = (float) $amount;
        $amount = round($amount, 2);
        $sql = 'UPDATE accounts SET 
                cash_total_debit = ROUND(cash_total_debit + ' . $amount . ', 2)
                , cash_current = ROUND(cash_current + ' . $amount . ', 2)
                WHERE id = ' . $account_id;
        $this->ci->db->query($sql);
    }

    Private function subDebit($account_id, $amount) {
        $amount = (float) $amount;
        $amount = round($amount, 2);
        $sql = 'UPDATE accounts SET 
                cash_total_debit = ROUND(cash_total_debit - ' . $amount . ', 2)
                , cash_current = ROUND(cash_current - ' . $amount . ', 2)
                WHERE id = ' . $account_id;
        $this->ci->db->query($sql);
    }

    Private function addCredit($account_id, $amount) {
        $amount = (float) $amount;
        $amount = round($amount, 2);
        $sql = 'UPDATE accounts SET 
                cash_total_credit = ROUND(cash_total_credit + ' . $amount . ', 2)
                , cash_current = ROUND(cash_current - ' . $amount . ', 2)
                WHERE id = ' . $account_id;
        $this->ci->db->query($sql);
    }

    Private function subCredit($account_id, $amount) {
        $amount = (float) $amount;
        $amount = round($amount, 2);
        $sql = 'UPDATE accounts SET 
                cash_total_credit = ROUND(cash_total_credit - ' . $amount . ', 2)
                , cash_current = ROUND(cash_current + ' . $amount . ', 2)
                WHERE id = ' . $account_id;
        $this->ci->db->query($sql);
    }

    Public function getAccounts($account_id = '0', $table = 'accounts') {
        if (config('language') == 'arabic')
            $name = 'name_ar';
        else
            $name = 'name_la';
        $this->ci->db->where('parent', $account_id);
        $this->ci->db->where('firm_id', $this->ci->session->userdata('firm_id'));
        $accounts = $this->ci->db->get($table)->result();

        $ret = '';
        if ($accounts) {
            foreach ($accounts as $account) {
                if ($account->parent == '0') {
                    $style = 'color:blue;';
                    $ret .= '<tr><td colspan="8" style="background-color:black;height:1px;padding:0px"></td></tr>';
                } elseif ($account->kind == '1') {
                    $style = 'color:green;';
                    $ret .= '<tr><td colspan="8" style="background-color:black;height:1px;padding:0px"></td></tr>';
                } else
                    $style = '';

                $ret .= '
                    <tr style="' . $style . '">';
                $ret .= '<td style="' . $style . '">';
                $ret .= $account->number;
                $ret .= '</td>';
                $ret .= '<td style="' . $style . '">';
                $ret .= $account->$name;
                $ret .= '</td>';
                if ($account->parent == '0')
                    $ret .= '<td class="debit1" style="' . $style . '">';
                else
                    $ret .= '<td class="debit1-" style="' . $style . '">';
                if ($account->cash_first > 0)
                    $ret .= round($account->cash_first, 2);
                $ret .= '</td>';
                if ($account->parent == '0')
                    $ret .= '<td class="credit1" style="' . $style . '">';
                else
                    $ret .= '<td class="credit1-" style="' . $style . '">';
                if ($account->cash_first < 0)
                    $ret .= round((float) $account->cash_first * -1, 2);
                $ret .= '</td>';
                if ($account->parent == '0')
                    $ret .= '<td class="debit2" style="' . $style . '">';
                else
                    $ret .= '<td class="debit2-" style="' . $style . '">';
                $ret .= $account->cash_total_debit;
                $ret .= '</td>';
                if ($account->parent == '0')
                    $ret .= '<td class="credit2" style="' . $style . '">';
                else
                    $ret .= '<td class="credit2-" style="' . $style . '">';
                $ret .= $account->cash_total_credit;
                $ret .= '</td>';
                if ($account->parent == '0')
                    $ret .= '<td class="debit3" style="' . $style . '">';
                else
                    $ret .= '<td class="debit3-" style="' . $style . '">';
                if ($account->cash_current > 0)
                    $ret .= round($account->cash_current, 2);
                $ret .= '</td>';
                if ($account->parent == '0')
                    $ret .= '<td class="credit3" style="' . $style . '">';
                else
                    $ret .= '<td class="credit3-" style="' . $style . '">';
                if ($account->cash_current < 0)
                    $ret .= round((float) $account->cash_current * -1, 2);
                $ret .= '</td>';
                $ret .= '</tr>';
                $ret .= $this->getAccounts($account->id, $table);
            }
        }
        else {
//            $ret .= '
//                <tr><td colspan="8" style="background-color:black;height:1px;padding:0px"></td></tr>
//                ';
        }
        return $ret;
    }

    Public function getAccountsSumArray($account_id = '0', $table = 'accounts') {
        $this->ci->db->where('parent', $account_id);
        $this->ci->db->where('firm_id', $this->ci->session->userdata('firm_id'));
        $accounts = $this->ci->db->get($table)->result();
        if (config('language') == 'arabic')
            $name = 'name_ar';
        else
            $name = 'name_la';
        $items = array();
        if ($accounts) {
            foreach ($accounts as $account) {
                $items[$account->id]['id'] = $account->id;
                $items[$account->id]['name'] = $account->$name;
                $items[$account->id]['debit'] = 0;
                $items[$account->id]['credit'] = 0;
                $items[$account->id]['val'] = 0;
                if ($account->cash_current > 0)
                    $items[$account->id]['debit'] += $account->cash_current;
                else
                    $items[$account->id]['credit'] += $account->cash_current;
                //            $childSum = getChildSum($account->id, $table);
                //            if($childSum > 0)
                //                $items[$account->id]['debit'] += $childSum;
                //            else
                //                $items[$account->id]['credit'] += $childSum;
                $items[$account->id]['val'] += $account->cash_current;
                //            $items[$account->id]['val'] += getChildSum($account->id, $table);
                $items[$account->id]['main'] = $account->final_account;
                $items[$account->id]['alldebit'] = $account->cash_total_debit;
                $items[$account->id]['allcredit'] = $account->cash_total_credit;
                $items[$account->id]['firest'] = $account->cash_first;
                $items[$account->id]['childs'] = $this->getAccountsSumArray($account->id, $table);
            }
        }
        return $items;
    }

    Public function getTree($parent = '0', $table = 'accounts') {

        $this->ci->db->where('parent', $parent);
        $this->ci->db->where('firm_id', $this->ci->session->userdata('firm_id'));
        $trees = $this->ci->db->get($table)->result();
        $items = '';
        if ($trees) {
            if ($parent == 0)
                $items .= '<ul id="black" class="treeview-black"><img src="' . IMAGES . '/base.gif">';
            else
                $items .= '<ul>';
            foreach ($trees as $tree) {

                if (config('language') == 'arabic') :
                    $name = 'name_ar';
                else:
                    $name = 'name_la';
                endif;

                $items .=
                        '<li>';
                if ($tree->kind == '1')
                    $items .= '<img src="' . IMAGES . '/folder-closed.gif">';
                else
                    $items .= '<img src="' . IMAGES . '/file.gif">';
                $items .= '<a href="' . site_url('admin/accounts/edit/' . $tree->id) . '">(' . $tree->number . ') ' . $tree->$name . ' (' . $this->getAccountKind($tree->kind) . ')</a> ';
                if ($tree->kind == '1')
                    $items .= '<a href="' . site_url('admin/accounts/add/?parent=' . $tree->id . '&main=' . $tree->final_account) . '">' . '<img src="' . IMAGES . '/operations/add.png" />' . '</a>';
//                $items .=   '<a href="' . site_url('admin/accounts/edit/' . $tree->id) .'">' . '<img src="'.IMAGES.'/operations/Pencil-icon.png" width="30" height="15" />' . '</a>' ;
                if ($this->isDeletable($tree->id))
                    $items .= '<a href="' . site_url('admin/accounts/delete/' . $tree->id) . '" onclick="return confirm(\'' . lang('global_delete') . '?\')">' . '<img src="' . IMAGES . '/operations/delete.png" />' . '</a>';


                $items .= $this->getTree($tree->id, $table);
                $items .= '</li>';
            }
            $items .= '</ul>';
        }
        return $items;
    }

    Public function getAccountKind($kind_id) {
        if (config('language') == 'arabic') {
            if ($kind_id == '1')
                return 'رئيسي';
            if ($kind_id == '2')
                return 'جزئي';
        }
        else {
            if ($kind_id == '1')
                return 'Primary';
            if ($kind_id == '2')
                return 'Partial';
        }
    }

    Public function isDeletable($account_id) {
        if (!$this->isParent($account_id) && !$this->hasEntries($account_id))
            return TRUE;
        else
            return FALSE;
    }

    Public function isCurrencyChangable($currency = FALSE) {
        if ($currency && is_numeric($currency))
            $this->ci->entries_details_model->currency = $currency;
        $entries = $this->ci->entries_details_model->get(TRUE);

        if ($entries > 0)
            return FALSE;
        else
            return TRUE;
    }

    Private function hasEntries($account_id, $table = 'entries_details') {
        $this->ci->db->where('account', $account_id);
        $this->ci->db->where('firm_id', $this->ci->session->userdata('firm_id'));
        $res = $this->ci->db->get($table)->num_rows();
        if ($res)
            return TRUE;
        else
            return FALSE;
    }

    Private function isParent($account_id, $table = 'accounts') {
        $this->ci->db->where('parent', $account_id);
        $this->ci->db->where('firm_id', $this->ci->session->userdata('firm_id'));
        $res = $this->ci->db->get($table)->num_rows();
        if ($res)
            return TRUE;
        else
            return FALSE;
    }

    Public function isVoucher($entry_id) {
        $this->ci->db->where('id', $entry_id);
        $this->ci->db->where('firm_id', $this->ci->session->userdata('firm_id'));
        $res = $this->ci->db->get('entries_master')->row();
        if ($res->transaction_entry == '1')
            return TRUE;
        else
            return FALSE;
    }

    Public function addSimpleEntry($debit_account_id, $credit_account_id, $amount, $voucher_kind = FALSE, $voucher_id = FALSE, $currency = FALSE, $rate = '1.0000', $description = '', $remarks = '', $entry_date = FALSE, $entry_number = FALSE) {
        $amount = (float) $amount;
        $amount = round($amount, 2);
        $addAmount = (float) $amount * (float) $rate;
        $addAmount = round($addAmount, 2);

        if (!$entry_number)
            $entry_number = $this->entry_number;
        if (!$currency)
            $currency = $this->ci->config->item('default_currency');
        if (!$entry_date)
            $entry_date = date('Y-m-d');

        $this->ci->entries_master_model->simple = '1';
        $this->ci->entries_master_model->firm_id = $this->ci->session->userdata('firm_id');
        $this->ci->entries_master_model->entry_number = $entry_number;
        $this->ci->entries_master_model->master_date = $entry_date;
        $this->ci->entries_master_model->currency = $currency;
        $this->ci->entries_master_model->rate = $rate;
        if ($voucher_kind)
            $this->ci->entries_master_model->transaction_entry = '1';
        else
            $this->ci->entries_master_model->transaction_entry = '0';
        $this->ci->entries_master_model->transaction_kind = $voucher_kind;
        $this->ci->entries_master_model->transaction_id = $voucher_id;
        $entry_id = $this->ci->entries_master_model->save();

        $this->ci->entries_details_model->entry_id = $entry_id;
        $this->ci->entries_details_model->firm_id = $this->ci->session->userdata('firm_id');
        $this->ci->entries_details_model->entry_number = $entry_number;
        $this->ci->entries_details_model->entry_date = $entry_date;
        $this->ci->entries_details_model->description = $description;
        $this->ci->entries_details_model->remarks = $remarks;
        $this->ci->entries_details_model->voucher = $voucher_kind;
        $this->ci->entries_details_model->currency = $currency;
        $this->ci->entries_details_model->rate = $rate;
        $this->ci->entries_details_model->amount = $amount;

        $this->ci->entries_details_model->debit = '1';
        $this->ci->entries_details_model->account = $debit_account_id;
        $this->ci->entries_details_model->second_account = $credit_account_id;
        $this->ci->entries_details_model->save();
        $this->updateAccountAmount($debit_account_id, $addAmount, 'debit', 'add');

        $this->ci->entries_details_model->debit = '0';
        $this->ci->entries_details_model->account = $credit_account_id;
        $this->ci->entries_details_model->second_account = $debit_account_id;
        $this->ci->entries_details_model->save();
        $this->updateAccountAmount($credit_account_id, $addAmount, 'credit', 'add');
    }

    Public function reset() {
        $this->entries_array = FALSE;
        $this->simple = FALSE;
        $this->entry_number = FALSE;
        $this->master_date = FALSE;
        $this->currency = FALSE;
        $this->rate = FALSE;
        $this->transaction_entry = '0';
        $this->transaction_kind = FALSE;
        $this->transaction_id = FALSE;
        $this->error = '';
    }

    Public function getAvailableAccountNumber($parent_id, $zeros = 4) {
        $this->ci->accounts_model->id = $parent_id;
        $parent_account = $this->ci->accounts_model->get();
        $this->ci->accounts_model->id = FALSE;
        $this->ci->accounts_model->parent = $parent_id;
        $child_accounts = $this->ci->accounts_model->get();
        $this->ci->accounts_model->parent = FALSE;
        $max_no = 0;
        foreach ($child_accounts as $ch_acc) {
            $account_number = (int) substr($ch_acc->number, strlen($parent_account->number));
            if ($account_number > $max_no)
                $max_no = $account_number;
        }
        if ($max_no == 0) {
            $max_no = 1;
            $acc_num = sprintf('%0' . $zeros . 'd', $max_no);
            $number = $parent_account->number . $acc_num;
        } else {
            $max_no+=1;
            $acc_num = sprintf('%0' . $zeros . 'd', $max_no);
            $number = $parent_account->number . $acc_num;
        }
        return $number;
    }

    Private function getAvailableEntryNumber() {
        $number = $this->ci->db->select('max(entry_number) as max_no')
                ->where('firm_id', $this->firm_id)
                ->get('entries_master')
                ->row();
        if ($number)
            $entry_number = (int) $number->max_no + 1;
        else
            $entry_number = 1;
        return $entry_number;
    }

    Public function makeDefaultTree($firm_id) {
        $sql_del = "DELETE FROM `accounts` WHERE `firm_id` = '$firm_id'";
        $this->ci->db->query($sql_del);
        $sql = "INSERT INTO `accounts` (`firm_id`, `number`, `name_ar`, `name_la`, `kind`, `parent`, `final_account`, `security_level`, `currency`, `cash_first`, `cash_total_credit`, `cash_total_debit`, `cash_current`, `currency_cash_first`, `currency_cash_current`, `gold_support`, `carat`, `gold_first`, `gold_total_credit`, `gold_total_debit`, `gold_current`) VALUES
            ($firm_id, 1, 'الاصول', 'Assets', '1', 0, 1, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)";
        $this->ci->db->query($sql);
        $id1 = $this->ci->db->insert_id();
        $sql = "INSERT INTO `accounts` (`firm_id`, `number`, `name_ar`, `name_la`, `kind`, `parent`, `final_account`, `security_level`, `currency`, `cash_first`, `cash_total_credit`, `cash_total_debit`, `cash_current`, `currency_cash_first`, `currency_cash_current`, `gold_support`, `carat`, `gold_first`, `gold_total_credit`, `gold_total_debit`, `gold_current`) VALUES
            ($firm_id, 11, 'الاصول الثابتة', 'Fixed Assets', '1', $id1, 1, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)";
        $this->ci->db->query($sql);
        $id11 = $this->ci->db->insert_id();
        $sql = "INSERT INTO `accounts` (`firm_id`, `number`, `name_ar`, `name_la`, `kind`, `parent`, `final_account`, `security_level`, `currency`, `cash_first`, `cash_total_credit`, `cash_total_debit`, `cash_current`, `currency_cash_first`, `currency_cash_current`, `gold_support`, `carat`, `gold_first`, `gold_total_credit`, `gold_total_debit`, `gold_current`) VALUES
            ($firm_id, 12, 'اصول متداولة', 'Current assets', '1', $id1, 1, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)";
        $this->ci->db->query($sql);
        $id12 = $this->ci->db->insert_id();
        $sql = "INSERT INTO `accounts` (`firm_id`, `number`, `name_ar`, `name_la`, `kind`, `parent`, `final_account`, `security_level`, `currency`, `cash_first`, `cash_total_credit`, `cash_total_debit`, `cash_current`, `currency_cash_first`, `currency_cash_current`, `gold_support`, `carat`, `gold_first`, `gold_total_credit`, `gold_total_debit`, `gold_current`) VALUES
            ($firm_id, 121, 'العملاء', 'Clients', '1', $id12, 1, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)";
        $this->ci->db->query($sql);
        $id121 = $this->ci->db->insert_id();
        $sql = "INSERT INTO `accounts` (`firm_id`, `number`, `name_ar`, `name_la`, `kind`, `parent`, `final_account`, `security_level`, `currency`, `cash_first`, `cash_total_credit`, `cash_total_debit`, `cash_current`, `currency_cash_first`, `currency_cash_current`, `gold_support`, `carat`, `gold_first`, `gold_total_credit`, `gold_total_debit`, `gold_current`) VALUES
            ($firm_id, 12101, 'عملاء الفرع الرئيسي', 'Clients of primary branch', '1', $id121, 1, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)";
        $this->ci->db->query($sql);
        $id1211 = $this->ci->db->insert_id();
        $sql = "INSERT INTO `accounts` (`firm_id`, `number`, `name_ar`, `name_la`, `kind`, `parent`, `final_account`, `security_level`, `currency`, `cash_first`, `cash_total_credit`, `cash_total_debit`, `cash_current`, `currency_cash_first`, `currency_cash_current`, `gold_support`, `carat`, `gold_first`, `gold_total_credit`, `gold_total_debit`, `gold_current`) VALUES
            ($firm_id, 122, 'الاموال الجاهزة', 'Current Cash', '1', $id12, 1, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)";
        $this->ci->db->query($sql);
        $id122 = $this->ci->db->insert_id();
        $sql = "INSERT INTO `accounts` (`firm_id`, `number`, `name_ar`, `name_la`, `kind`, `parent`, `final_account`, `security_level`, `currency`, `cash_first`, `cash_total_credit`, `cash_total_debit`, `cash_current`, `currency_cash_first`, `currency_cash_current`, `gold_support`, `carat`, `gold_first`, `gold_total_credit`, `gold_total_debit`, `gold_current`) VALUES
            ($firm_id, 1221, 'الصندوق', 'Cash', '2', $id122, 1, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)";
        $this->ci->db->query($sql);
        $id1221 = $this->ci->db->insert_id();
        $sql = "INSERT INTO `accounts` (`firm_id`, `number`, `name_ar`, `name_la`, `kind`, `parent`, `final_account`, `security_level`, `currency`, `cash_first`, `cash_total_credit`, `cash_total_debit`, `cash_current`, `currency_cash_first`, `currency_cash_current`, `gold_support`, `carat`, `gold_first`, `gold_total_credit`, `gold_total_debit`, `gold_current`) VALUES
            ($firm_id, 2, 'الخصوم', 'Liabilities', '1', 0, 2, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)";
        $this->ci->db->query($sql);
        $id2 = $this->ci->db->insert_id();
        $sql = "INSERT INTO `accounts` (`firm_id`, `number`, `name_ar`, `name_la`, `kind`, `parent`, `final_account`, `security_level`, `currency`, `cash_first`, `cash_total_credit`, `cash_total_debit`, `cash_current`, `currency_cash_first`, `currency_cash_current`, `gold_support`, `carat`, `gold_first`, `gold_total_credit`, `gold_total_debit`, `gold_current`) VALUES
            ($firm_id, 22, 'الخصوم المتداولة', 'Current Liabilities', '1', $id2, 2, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)";
        $this->ci->db->query($sql);
        $id22 = $this->ci->db->insert_id();
        $sql = "INSERT INTO `accounts` (`firm_id`, `number`, `name_ar`, `name_la`, `kind`, `parent`, `final_account`, `security_level`, `currency`, `cash_first`, `cash_total_credit`, `cash_total_debit`, `cash_current`, `currency_cash_first`, `currency_cash_current`, `gold_support`, `carat`, `gold_first`, `gold_total_credit`, `gold_total_debit`, `gold_current`) VALUES
            ($firm_id, 221, 'الموردون', 'Suppliers', '1', $id22, 2, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)";
        $this->ci->db->query($sql);
        $id221 = $this->ci->db->insert_id();
        $sql = "INSERT INTO `accounts` (`firm_id`, `number`, `name_ar`, `name_la`, `kind`, `parent`, `final_account`, `security_level`, `currency`, `cash_first`, `cash_total_credit`, `cash_total_debit`, `cash_current`, `currency_cash_first`, `currency_cash_current`, `gold_support`, `carat`, `gold_first`, `gold_total_credit`, `gold_total_debit`, `gold_current`) VALUES
            ($firm_id, 3, 'المبيعات', 'Sales', '1', 0, 5, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)";
        $this->ci->db->query($sql);
        $id3 = $this->ci->db->insert_id();
        $sql = "INSERT INTO `accounts` (`firm_id`, `number`, `name_ar`, `name_la`, `kind`, `parent`, `final_account`, `security_level`, `currency`, `cash_first`, `cash_total_credit`, `cash_total_debit`, `cash_current`, `currency_cash_first`, `currency_cash_current`, `gold_support`, `carat`, `gold_first`, `gold_total_credit`, `gold_total_debit`, `gold_current`) VALUES
            ($firm_id, 31, 'مبيعات العمرة الرئيسي', 'Umrah Sales Primary', '1', $id3, 5, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)";
        $this->ci->db->query($sql);
        $id31 = $this->ci->db->insert_id();
        $sql = "INSERT INTO `accounts` (`firm_id`, `number`, `name_ar`, `name_la`, `kind`, `parent`, `final_account`, `security_level`, `currency`, `cash_first`, `cash_total_credit`, `cash_total_debit`, `cash_current`, `currency_cash_first`, `currency_cash_current`, `gold_support`, `carat`, `gold_first`, `gold_total_credit`, `gold_total_debit`, `gold_current`) VALUES
            ($firm_id, 310001, 'مبيعات العمرة', 'Umrah Sales', '2', $id31, 5, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)";
        $this->ci->db->query($sql);
        $id310001 = $this->ci->db->insert_id();
        $sql = "INSERT INTO `accounts` (`firm_id`, `number`, `name_ar`, `name_la`, `kind`, `parent`, `final_account`, `security_level`, `currency`, `cash_first`, `cash_total_credit`, `cash_total_debit`, `cash_current`, `currency_cash_first`, `currency_cash_current`, `gold_support`, `carat`, `gold_first`, `gold_total_credit`, `gold_total_debit`, `gold_current`) VALUES
            ($firm_id, 310002, 'خصم مسموح به', 'Discount', '2', $id31, 5, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)";
        $this->ci->db->query($sql);
        $id310002 = $this->ci->db->insert_id();
        $sql = "INSERT INTO `accounts` (`firm_id`, `number`, `name_ar`, `name_la`, `kind`, `parent`, `final_account`, `security_level`, `currency`, `cash_first`, `cash_total_credit`, `cash_total_debit`, `cash_current`, `currency_cash_first`, `currency_cash_current`, `gold_support`, `carat`, `gold_first`, `gold_total_credit`, `gold_total_debit`, `gold_current`) VALUES
            ($firm_id, 4, 'المشتريات', 'Purchases', '1', 0, 5, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)";
        $this->ci->db->query($sql);
        $id4 = $this->ci->db->insert_id();
        $sql = "INSERT INTO `accounts` (`firm_id`, `number`, `name_ar`, `name_la`, `kind`, `parent`, `final_account`, `security_level`, `currency`, `cash_first`, `cash_total_credit`, `cash_total_debit`, `cash_current`, `currency_cash_first`, `currency_cash_current`, `gold_support`, `carat`, `gold_first`, `gold_total_credit`, `gold_total_debit`, `gold_current`) VALUES
            ($firm_id, 5, 'المصاريف', 'Expenses', '1', 0, 4, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)";
        $this->ci->db->query($sql);
        $id5 = $this->ci->db->insert_id();

        $sql = "INSERT INTO `vouchers_defs` (`firm_id`, `main_type`, `name_ar`, `name_la`, `cash_account`, `currency`, `notes`) VALUES
($firm_id, 1, 'ايصال استلام نقدية', 'Receive Voucher', $id1221, 2, '')";
        $this->ci->db->query($sql);
        $sql = "INSERT INTO `vouchers_defs` (`firm_id`, `main_type`, `name_ar`, `name_la`, `cash_account`, `currency`, `notes`) VALUES
($firm_id, 2, 'ايصال صرف نقدية', 'Pay Voucher', $id1221, 2, '')";
        $this->ci->db->query($sql);

        $sql = "UPDATE firms SET 
            umrah_sales_account_id = $id310001 ,
            discount_account_id = $id310002 ,
            suppliers_account_id = $id221 ,
            clients_account_id = $id1211 ,
            cash_account = $id1221 ,
            clients_primary_aid = $id121 ,
            us_primary_aid = $id31 ,
            cash_primary_aid = $id122 ,
            use_accounting = '1'
        
            WHERE id = $firm_id ";
        $this->ci->db->query($sql);
    }

    Public function addAccount($name_ar, $kind, $parent_id, $account_id = FALSE, $number = FALSE, $firm_id = FALSE, $name_la = FALSE, $zeros = 4) {
        if (!$firm_id)
            $firm_id = $this->firm_id;
        if (!$name_la)
            $name_la = $name_ar;
        if (!$number)
            $number = $this->getAvailableAccountNumber($parent_id, $zeros);

        $this->ci->accounts_model->id = $parent_id;
        $parent_account = $this->ci->accounts_model->get();
        $this->ci->accounts_model->id = FALSE;

        $this->ci->accounts_model->name_ar = $name_ar;
        $this->ci->accounts_model->name_la = $name_la;
        $this->ci->accounts_model->parent = $parent_id;
        $this->ci->accounts_model->kind = $kind;
        $this->ci->accounts_model->firm_id = $firm_id;
        $this->ci->accounts_model->number = $number;
        $this->ci->accounts_model->final_account = $parent_account->final_account;
        if ($account_id)
            $this->ci->accounts_model->id = $account_id;

        $id = $this->ci->accounts_model->save();

        if ($account_id)
            return $account_id;
        else
            return $id;
    }

///////////////////// Integration with Safa

    Public function get_account_id($type = 'suppliers', $id = FALSE) {
        return $this->ci->db->select('account_id')->where('id', $id)->get($type)->row()->account_id;
    }

    Public function get_account_name($id) {
        if (config('language') == 'arabic')
            $name = 'name_ar';
        else
            $name = 'name_la';

        return $this->ci->db->select($name)->where('id', $id)->get('accounts')->row()->{$name};
    }

    Public function getSetting($table, $id, $idVal, $setting) {
        $result = $this->ci->db->where($id, $idVal)->get($table)->row();
        if ($result)
            return $result->$setting;
        else
            return FALSE;
    }

    Public function setReservations($res_id = FALSE) {
        $this->transaction_entry = '2';
        if ($res_id)
            $this->transaction_id = $res_id;
    }

    Public function setHotelReservation($res_id = FALSE) {
        $this->transaction_entry = '3';
        if ($res_id)
            $this->transaction_id = $res_id;
    }

    Public function setFlightsReservation($res_id = FALSE) {
        $this->transaction_entry = '4';
        if ($res_id)
            $this->transaction_id = $res_id;
    }

    Public function setInternalTransport($res_id = FALSE) {
        $this->transaction_entry = '5';
        if ($res_id)
            $this->transaction_id = $res_id;
    }

    Public function deleteReservation($res_id) {
        $this->transaction_entry = '2';
        $this->deleteRes($res_id);
    }

    Public function deleteHotelReservation($res_id) {
        $this->transaction_entry = '3';
        $this->deleteRes($res_id);
    }

    Public function deleteFlightsReservation($res_id) {
        $this->transaction_entry = '4';
        $this->deleteRes($res_id);
    }

    Public function deleteInternalTransport($res_id) {
        $this->transaction_entry = '5';
        $this->deleteRes($res_id);
    }

    Private function deleteRes($res_id = FALSE) {
        if ($this->transaction_entry != '0') {
            if ($res_id)
                $this->transaction_id = $res_id;
            $this->ci->entries_master_model->transaction_entry = $this->transaction_entry;
            $this->ci->entries_master_model->transaction_id = $this->transaction_id;
            $entry = $this->ci->entries_master_model->get();
            if ($entry) {
                $this->entry_number = $entry[0]->entry_number;
                foreach ($entry as $ent)
                    $this->deleteEntry($ent->id, true);
            }
            return TRUE;
        } else
            return FALSE;
    }

}

/* End of file accounting.php */
/* Location: ./application/libraries/accounting.php */