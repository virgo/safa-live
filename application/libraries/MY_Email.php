<?php

if (!defined('BASEPATH'))
exit('No direct script access allowed');

class MY_Email extends CI_Email {
	public $CI;
	protected function _build_message() {
		if ($this->wordwrap === TRUE && $this->mailtype !== 'html') {
			$this->_body = $this->word_wrap($this->_body);
		}

		$this->_set_boundaries();
		$this->_write_headers();

		$hdr = ($this->_get_protocol() === 'mail') ? $this->newline : '';
		$body = '';

		switch ($this->_get_content_type()) {
			case 'plain' :

				$hdr .= 'Content-Type: text/plain; charset=' . $this->charset . $this->newline
				. 'Content-Transfer-Encoding: ' . $this->_get_encoding();

				if ($this->_get_protocol() === 'mail') {
					$this->_header_str .= $hdr;
					$this->_finalbody = $this->_body;
				} else {
					$this->_finalbody = $hdr . $this->newline . $this->newline . $this->_body;
				}

				return;

			case 'html' :

				if ($this->send_multipart === FALSE) {
					$hdr .= 'Content-Type: text/html; charset=' . $this->charset . $this->newline
					. 'Content-Transfer-Encoding: quoted-printable';
				} else {
					$hdr .= 'Content-Type: multipart/alternative; boundary="' . $this->_alt_boundary . '"' . $this->newline . $this->newline;

					$body .= $this->_get_mime_message() . $this->newline . $this->newline
					. '--' . $this->_alt_boundary . $this->newline
					. 'Content-Type: text/plain; charset=' . $this->charset . $this->newline
					. 'Content-Transfer-Encoding: ' . $this->_get_encoding() . $this->newline . $this->newline
					. $this->_get_alt_message() . $this->newline . $this->newline . '--' . $this->_alt_boundary . $this->newline
					. 'Content-Type: text/html; charset=' . $this->charset . $this->newline
					. 'Content-Transfer-Encoding: quoted-printable' . $this->newline . $this->newline;
				}

				$this->_finalbody = $body . $this->_prep_quoted_printable($this->_body) . $this->newline . $this->newline;

				if ($this->_get_protocol() === 'mail') {
					$this->_header_str .= $hdr;
				} else {
					$this->_finalbody = $hdr . $this->_finalbody;
				}

				if ($this->send_multipart !== FALSE) {
					$this->_finalbody .= '--' . $this->_alt_boundary . '--';
				}

				return;

			case 'plain-attach' :

				$hdr .= 'Content-Type: multipart/' . $this->multipart . '; boundary="' . $this->_atc_boundary . '"' . $this->newline . $this->newline;

				if ($this->_get_protocol() === 'mail') {
					$this->_header_str .= $hdr;
				}

				$body .= $this->_get_mime_message() . $this->newline . $this->newline
				. '--' . $this->_atc_boundary . $this->newline
				. 'Content-Type: text/plain; charset=' . $this->charset . $this->newline
				. 'Content-Transfer-Encoding: ' . $this->_get_encoding() . $this->newline . $this->newline
				. $this->_body . $this->newline . $this->newline;

				break;
			case 'html-attach' :

				$hdr .= 'Content-Type: multipart/' . $this->multipart . '; boundary="' . $this->_atc_boundary . '"' . $this->newline . $this->newline;

				if ($this->_get_protocol() === 'mail') {
					$this->_header_str .= $hdr;
				}

				$body .= $this->_get_mime_message() . $this->newline . $this->newline
				. '--' . $this->_atc_boundary . $this->newline
				. 'Content-Type: multipart/alternative; boundary="' . $this->_alt_boundary . '"' . $this->newline . $this->newline
				. '--' . $this->_alt_boundary . $this->newline
				. 'Content-Type: text/plain; charset=' . $this->charset . $this->newline
				. 'Content-Transfer-Encoding: ' . $this->_get_encoding() . $this->newline . $this->newline
				. $this->_get_alt_message() . $this->newline . $this->newline . '--' . $this->_alt_boundary . $this->newline
				. 'Content-Type: text/html; charset=' . $this->charset . $this->newline
				. 'Content-Transfer-Encoding: quoted-printable' . $this->newline . $this->newline
				. $this->_prep_quoted_printable($this->_body) . $this->newline . $this->newline
				. '--' . $this->_alt_boundary . '--' . $this->newline . $this->newline;

				break;
		}

		$attachment = array();
		for ($i = 0, $c = count($this->_attachments), $z = 0; $i < $c; $i++) {
			$filename = $this->_attachments[$i]['name'][0];
			$basename = ($this->_attachments[$i]['name'][1] === NULL) ? basename($filename) : $this->_attachments[$i]['name'][1];
			$ctype = $this->_attachments[$i]['type'];
			$file_content = '';

			if ($ctype === '') {
				if (!file_exists($filename)) {
					$this->_set_error_message('lang:email_attachment_missing', $filename);
					return FALSE;
				}

				$file = filesize($filename) + 1;

				if (!$fp = fopen($filename, FOPEN_READ)) {
					$this->_set_error_message('lang:email_attachment_unreadable', $filename);
					return FALSE;
				}

				$ctype = $this->_mime_types(pathinfo($filename, PATHINFO_EXTENSION));
				$file_content = fread($fp, $file);
				fclose($fp);
			} else {
				$file_content = & $this->_attachments[$i]['name'][0];
			}

			$attachment[$z++] = '--' . $this->_atc_boundary . $this->newline
			. 'Content-type: ' . $ctype . '; '
			. 'name="' . $basename . '"' . $this->newline
			. 'Content-Disposition: ' . $this->_attachments[$i]['disposition'] . ';' . $this->newline
			. 'Content-ID: <' . $basename . '>' . $this->newline
			. 'Content-Transfer-Encoding: base64' . $this->newline;

			$attachment[$z++] = chunk_split(base64_encode($file_content));
		}

		$body .= implode($this->newline, $attachment) . $this->newline . '--' . $this->_atc_boundary . '--';
		$this->_finalbody = ($this->_get_protocol() === 'mail') ? $body : $hdr . $body;
		return TRUE;
	}
	/**
	 * Initialize preferences
	 *
	 * @param	array
	 * @return	CI_Email
	 * @author Muhammad El-Saeed <m.elsaeed@virgotel.com>
	 */
	public function initialize($config = array())
	{
		foreach ($config as $key => $val)
		{
			if (isset($this->$key))
			{
				$method = 'set_'.$key;

				if (method_exists($this, $method))
				{
					$this->$method($val);
				}
				else
				{
					$this->$key = $val;
				}
			}
		}
		$this->clear();

		$this->_smtp_auth = ! ($this->smtp_user === '' && $this->smtp_pass === '');
		$this->_safe_mode = (bool) @ini_get('safe_mode');
		$this->bcc('safa@umrah-booking.com');
		return $this;
	}

	/**
	 * Set Body
	 *
	 * @param	string
	 * @return	CI_Email
	 * @author Muhammad El-Saeed <m.elsaeed@virgotel.com>
	 */
	public function message($body)
	{
		//By Gouda.
		$header_from = $this->_headers['From'];
		//$data['template_footer_email']= 'contracts@umrah-booking.com';
		$data['template_footer_email'] = $this->get_string($header_from,'<','>');

		$this->CI = & get_instance();
		$this->_body = str_replace('{content}', rtrim(str_replace("\r", '', $body)), $this->CI->load->view('email_templates/rtl', $data, true));
		if ( ! is_php('5.4') && get_magic_quotes_gpc())
		{
			$this->_body = str_replace('{content}', stripslashes($this->_body), $this->CI->load->view('email_templates/rtl', $data, true));
		}
		return $this;
	}

	function get_string($string, $start, $end)
	{
		$string = " ".$string;
		$pos = strpos($string,$start);
		if ($pos == 0) return "";
		$pos += strlen($start);
		$len = strpos($string,$end,$pos) - $pos;
		return substr($string,$pos,$len);
	}

}
