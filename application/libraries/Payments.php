<?php

class Payments {

    public $SL;

    public function __construct() {
        $SL = & get_instance();
    }

    public function addPayment($voucher = NULL, $order_no = NULL, $date = NULL, $amount = NULL, $erp_currency_id = NULL, $rate = NULL, $payment_type = NULL, $erp_company_type_id = NULL, $erp_company_id = NULL, $owner_erp_company_type_id = NULL, $owner_erp_company_id = NULL, $notes = NULL) {
        $this->SL->db->set('voucher', $voucher);
        $this->SL->db->set('date', $date);
        $this->SL->db->set('amount', $amount);
        $this->SL->db->set('erp_currency_id', $erp_currency_id);
        $this->SL->db->set('order_no', $order_no);
        $this->SL->db->set('payment_type', $payment_type);
        $this->SL->db->set('erp_company_type_id', $erp_company_type_id);
        $this->SL->db->set('erp_company_id', $erp_company_id);
        $this->SL->db->set('owner_erp_company_type_id', $owner_erp_company_type_id);
        $this->SL->db->set('owner_erp_company_id', $owner_erp_company_id);
        $this->SL->db->set('rate', $rate);
        $this->SL->db->set('notes', $notes);
        $this->SL->db->set('submission_datetime', date('Y-m-d H:i'));
        $this->SL->db->insert('erp_payments');
        return $this->SL->db->insert_id();
    }

}
