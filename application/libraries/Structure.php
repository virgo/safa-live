<?php

/**
 * Converts array to web services structure
 */
class Structure {

    public $Gama_Waytoumrah = array('MD_NO' => 'PilgrimID',
        'MD_TITLE_ID' => 'Title',
        'MD_FIRST_NAME_LA' => 'FirstName',
        'MD_SECOND_NAME_LA' => 'FatherName',
        'MD_THIRD_NAME_LA' => 'MiddleName',
        'MD_FOURTH_NAME_LA' => 'LastName',
        'MD_FIRST_NAME_AR' => 'AFirstName',
        'MD_SECOND_NAME_AR' => 'AFatherName',
        'MD_THIRD_NAME_AR' => 'AMiddleName',
        'MD_FOURTH_NAME_AR' => 'ALastName',
        'MD_NATIONALITY_ID' => 'CNationality',
        'MD_PASSPORT_NO' => 'PPNo',
        'MD_PASSPORT_TYPE' => 'PPType',
        'MD_PASSPORT_ISSUE_DATE' => 'PPIssueDate',
        'MD_PASSPORT_EXPIRY_DATE' => 'PPExpDate',
        'MD_PASSPORT_ISSUING_CITY' => 'PPIssueCity',
        'MD_PASSPORT_ISSUING_COUNTRY_ID' => 'PPIssueCountry',
        'MD_PASSPORT_DPN_COUNT' => 'No_Of_Relatives',
        'MD_DPN_SERIAL_NO' => 'Dpn_Serial_No',
        'MD_GENDER_ID' => 'Sex',
        'MD_MARITAL_STATUS_ID' => 'MaritalStatus',
        'MD_DATE_OF_BIRTH' => 'BirthDate',
        'MD_BIRTH_CITY' => 'BirthCity',
        'MD_BIRTH_COUNTRY_ID' => 'BirthCountry',
        'MD_RELATIVE_NO' => 'SponserID',
        'MD_RELATIVE_RELATION_ID' => 'RelationWithSponser',
        'MD_EDUCATIONAL_LEVEL' => 'Education',
        'MD_OCCUPATION' => 'job',
        'MD_CITY' => 'CAddaCity',
        'MD_COUNTRY_ID' => 'CAddCountry',
        'MD_AGE' => '',
        'MD_PREVIOUS_NATIONALITY_ID' => 'PrevNationality',
        'MD_RELATIVE_GENDER_ID' => ''
    );

    public $Gama_Safa = array('MD_NO' => 'PilgrimID',
        'MD_TITLE_ID' => 'Title',
        'MD_FIRST_NAME_LA' => 'FirstName',
        'MD_SECOND_NAME_LA' => 'FatherName',
        'MD_THIRD_NAME_LA' => 'MiddleName',
        'MD_FOURTH_NAME_LA' => 'LastName',
        'MD_FIRST_NAME_AR' => 'AFirstName',
        'MD_SECOND_NAME_AR' => 'AFatherName',
        'MD_THIRD_NAME_AR' => 'AMiddleName',
        'MD_FOURTH_NAME_AR' => 'ALastName',
        'MD_NATIONALITY_ID' => 'CNationality',
        'MD_PASSPORT_NO' => 'PPNo',
        'MD_PASSPORT_TYPE' => 'PPType',
        'MD_PASSPORT_ISSUE_DATE' => 'PPIssueDate',
        'MD_PASSPORT_EXPIRY_DATE' => 'PPExpDate',
        'MD_PASSPORT_ISSUING_CITY' => 'PPIssueCity',
        'MD_PASSPORT_ISSUING_COUNTRY_ID' => 'PPIssueCountry',
        'MD_PASSPORT_DPN_COUNT' => 'No_Of_Relatives',
        'MD_DPN_SERIAL_NO' => 'Dpn_Serial_No',
        'MD_GENDER_ID' => 'Sex',
        'MD_MARITAL_STATUS_ID' => 'MaritalStatus',
        'MD_DATE_OF_BIRTH' => 'BirthDate',
        'MD_BIRTH_CITY' => 'BirthCity',
        'MD_BIRTH_COUNTRY_ID' => 'BirthCountry',
        'MD_RELATIVE_NO' => 'SponserID',
        'MD_RELATIVE_RELATION_ID' => 'RelationWithSponser',
        'MD_EDUCATIONAL_LEVEL' => 'Education',
        'MD_OCCUPATION' => 'Job',
        'MD_CITY' => 'CAddaCity',
        'MD_COUNTRY_ID' => 'CAddCountry',
        'MD_AGE' => '',
        'MD_PREVIOUS_NATIONALITY_ID' => 'PrevNationality',
        'MD_RELATIVE_GENDER_ID' => ''
    );
    public $Gama_Babelumrah = array(
        'MD_NO' => 'SN',
        'MD_TITLE_ID' => 'TC',
        'MD_FIRST_NAME_LA' => 'FN',
        'MD_SECOND_NAME_LA' => 'FAN',
        'MD_THIRD_NAME_LA' => 'MN',
        'MD_FOURTH_NAME_LA' => 'LN',
        'MD_FIRST_NAME_AR' => 'AFN',
        'MD_SECOND_NAME_AR' => 'AFAN',
        'MD_THIRD_NAME_AR' => 'AMN',
        'MD_FOURTH_NAME_AR' => 'ALN',
        'MD_NATIONALITY_ID' => 'N',
        'MD_PASSPORT_NO' => 'PN',
        'MD_PASSPORT_TYPE' => 'PT',
        'MD_PASSPORT_ISSUE_DATE' => 'DI',
        'MD_PASSPORT_EXPIRY_DATE' => 'DE',
        'MD_PASSPORT_ISSUING_CITY' => 'CIAT',
        'MD_PASSPORT_ISSUING_COUNTRY_ID' => 'CIA',
        'MD_PASSPORT_DPN_COUNT' => 'MNR',
        'MD_DPN_SERIAL_NO' => 'DSN',
        'MD_GENDER_ID' => 'G',
        'MD_MARITAL_STATUS_ID' => 'MS',
        'MD_DATE_OF_BIRTH' => 'DB',
        'MD_BIRTH_CITY' => 'CB',
        'MD_BIRTH_COUNTRY_ID' => 'COB',
        'MD_RELATIVE_NO' => 'RT',
        'MD_RELATIVE_RELATION_ID' => 'RR',
        'MD_EDUCATIONAL_LEVEL' => 'EL',
        'MD_OCCUPATION' => 'J',
        'MD_CITY' => 'CAC',
        'MD_COUNTRY_ID' => 'CACO',
        'MD_AGE' => '',
        'MD_PREVIOUS_NATIONALITY_ID' => '',
        'MD_RELATIVE_GENDER_ID' => ''
    );
    public $Gama_Database = array(
        'MD_NO' => 'no',
        'MD_TITLE_ID' => 'title_id',
        'MD_FIRST_NAME_LA' => 'first_name_la',
        'MD_SECOND_NAME_LA' => 'second_name_la',
        'MD_THIRD_NAME_LA' => 'third_name_la',
        'MD_FOURTH_NAME_LA' => 'fourth_name_la',
        'MD_FIRST_NAME_AR' => 'first_name_ar',
        'MD_SECOND_NAME_AR' => 'second_name_ar',
        'MD_THIRD_NAME_AR' => 'third_name_ar',
        'MD_FOURTH_NAME_AR' => 'fourth_name_ar',
        'MD_NATIONALITY_ID' => 'nationality_id',
        'MD_PASSPORT_NO' => 'passport_no',
        'MD_PASSPORT_TYPE' => 'passport_type_id',
        'MD_PASSPORT_ISSUE_DATE' => 'passport_issue_date',
        'MD_PASSPORT_EXPIRY_DATE' => 'passport_expiry_date',
        'MD_PASSPORT_ISSUING_CITY' => 'passport_issuing_city',
        'MD_PASSPORT_ISSUING_COUNTRY_ID' => 'passport_issuing_country_id',
        'MD_PASSPORT_DPN_COUNT' => 'passport_dpn_count',
        'MD_DPN_SERIAL_NO' => 'dpn_serial_no',
        'MD_GENDER_ID' => 'gender_id',
        'MD_MARITAL_STATUS_ID' => 'marital_status_id',
        'MD_DATE_OF_BIRTH' => 'date_of_birth',
        'MD_BIRTH_CITY' => 'birth_city',
        'MD_BIRTH_COUNTRY_ID' => 'birth_country_id',
        'MD_RELATIVE_NO' => 'relative_no',
        'MD_RELATIVE_RELATION_ID' => 'relative_relation_id',
        'MD_EDUCATIONAL_LEVEL' => 'educational_level_id',
        'MD_OCCUPATION' => 'occupation',
        'MD_CITY' => 'city',
        'MD_COUNTRY_ID' => 'erp_country_id',
        'MD_AGE' => 'age',
        'MD_PREVIOUS_NATIONALITY_ID' => 'previous_nationality_id',
        'MD_RELATIVE_GENDER_ID' => 'relative_gender_id'
    );
    public $Gama_Tawaf = array(
        'MD_NO' => 'MUT_CODE',
        'MD_TITLE_ID' => 'MUT_TITLE',
        'MD_FIRST_NAME_LA' => 'FIRST_NAME_LA',
        'MD_SECOND_NAME_LA' => 'FATHER_NAME_LA',
        'MD_THIRD_NAME_LA' => 'MIDDLE_NAME_LA',
        'MD_FOURTH_NAME_LA' => 'LAST_NAME_LA',
        'MD_FIRST_NAME_AR' => 'FIRST_NAME_AR',
        'MD_SECOND_NAME_AR' => 'FATHER_NAME_AR',
        'MD_THIRD_NAME_AR' => 'MIDDLE_NAME_AR',
        'MD_FOURTH_NAME_AR' => 'LAST_NAME_AR',
        'MD_NATIONALITY_ID' => 'NATIONALITY',
        'MD_PASSPORT_NO' => 'PASS_NO',
        'MD_PASSPORT_TYPE' => 'PASS_TYPE',
        'MD_PASSPORT_ISSUE_DATE' => 'PASS_ISSUE',
        'MD_PASSPORT_EXPIRY_DATE' => 'PASS_EXPIRY',
        'MD_PASSPORT_ISSUING_CITY' => 'PASS_CITY',
        'MD_PASSPORT_ISSUING_COUNTRY_ID' => 'PASS_COUNTRY',
        'MD_DPN_SERIAL_NO' => 'DEPEND_NO',
        'MD_GENDER_ID' => 'GENDER',
        'MD_MARITAL_STATUS_ID' => 'MARITAL_STATUS',
        'MD_DATE_OF_BIRTH' => 'BIRTH_DATE',
        'MD_BIRTH_CITY' => 'BIRTH_CITY',
        'MD_BIRTH_COUNTRY_ID' => 'BIRTH_COUNTRY',
        'MD_RELATIVE_NO' => 'MAHRAM_CODE',
        'MD_RELATIVE_RELATION_ID' => 'RELATION',
        'MD_EDUCATIONAL_LEVEL' => 'EDUCATION',
        'MD_OCCUPATION' => 'MUT_JOB',
        'MD_CITY' => 'CURR_CITY',
        'MD_COUNTRY_ID' => 'CURR_COUNTRY',
        'MD_AGE' => 'MUTAGE',
    );
    protected $travellers = array();
    public $original_travellers = array();

    /**
     * Convert From - To This Formats
     * 1- BABELUMRAH
     * 2- WayToUmrah
     * 3- Gama
     * 4- Database
     * 5- Tawaf
     * 6- Safa
     * 
     * @param type $data
     * @param type $from
     * @param type $to
     */
    public function convert($data = NULL, $from = FALSE, $to = FALSE) {
        $this->original_travellers = $data;
        // ALIASES
        if ($from == $to) {
            return $data;
        }
        $this->travellers = $data;
        // BABELUMRAH TO WAYTOUMRAH
        if ($from == '1' && $to == '2') {
            
        }
        // BABELUMRAH TO DATABASE
        if ($from == '1' && $to == '4') {
            $this->travellers = $this->backward('Gama_Babelumrah');
            return $this->forward('Gama_Database');
        }
        // Safa TO BABELUMRAH
        if ($from == '2' && $to == '1') {
            $this->travellers = $this->backward('Gama_Waytoumrah');
            return $this->forward('Gama_Babelumrah');            
        }
        // Safa TO BABELUMRAH
        if ($from == '6' && $to == '1') {
            $this->travellers = $this->backward('Gama_Safa');
            return $this->forward('Gama_Babelumrah');            
        }
        // WAYTOUMRAH TO DATABASE 
        if ($from == '2' && $to == '4') {
            // $Gama_Waytoumrah BACKWORD
            // $Gama_Database FORWARD
            $this->travellers = $this->backward('Gama_Waytoumrah');
            return $this->forward('Gama_Database');
        }
        // WAYTOUMRAH TO DATABASE 
        if ($from == '2' && $to == '3') {
            // $Gama_Waytoumrah BACKWORD
            // $Gama_Database FORWARD
            $this->travellers = $this->backward('Gama_Waytoumrah');
            return $this->travellers;//$this->forward('Gama_Database');
        }
        // GAMA TO BABELUMRAH
        if ($from == '3' && $to == '1') {
            return $this->forward('Gama_Babelumrah');
        }
        // GAMA TO WAYTOUMRAH
        if ($from == '3' && $to == '2') {
            
        }
        // GAMA TO DATABASE
        if ($from == '3' && $to == '4') {
            
        }
        // DATABASE TO BABELUMRAH
        if ($from == '4' && $to == '1') {
            // $Gama_Database BACKWARD
            // $Gama_Babelumrah FORWARD
            $this->travellers = $this->backward('Gama_Database');
            return $this->forward('Gama_Babelumrah');
        }
        // DATABASE TO WAYTOUMRAH
        if ($from == '4' && $to == '2') {
            // $Gama_Database BACKWARD
            // $Gama_Waytoumrah FORWARD
            $this->travellers = $this->backward('Gama_Database');
            return $this->forward('Gama_Waytoumrah');
        }
        
        // DATABASE TO GAMA
        if ($from == '4' && $to == '3') {
            // $Gama_Database BACKWARD
            return $this->backward('Gama_Database');
        }
        // DATABASE TO TAWAF
        if ($from == '4' && $to == '5') {
            // $Gama_Database BACKWARD
            $this->backward('Gama_Database');
            return $this->forward('Gama_Tawaf');
        }
    }

    protected function forward($array) {
        $i = 0;
        $res = array();
        foreach ($this->travellers as $traveller) {
            $i++;
            foreach ($this->$array as $key => $value) {
                if (isset($traveller[$key]))
                    $res[$i][$value] = $traveller[$key];
            }
        }
        return $res;
    }

    protected function backward($array) {
        $i = 0;
        $res = array();
        foreach ($this->travellers as $traveller) {
            $i++;
            foreach ($this->$array as $key => $value) {
                if (isset($traveller[$value]))
                    $res[$i][$key] = $traveller[$value];
            }
        }
        return $res;
    }

    private function objectToArray($d) {
        if (is_object($d)) {
            $d = get_object_vars($d);
        }
        if (is_array($d)) {
            return array_map(__FUNCTION__, $d);
        } else {
            return $d;
        }
    }

    public function farray_converter($process) {
        $i = 0;
        $res = array();
        foreach ($this->$process as $atr) {
            $i++;
            foreach ($this->$array as $key => $value) {
                if (isset($traveller[$key]))
                    $res[$i][$value] = $key;
            }
        }
        return $res;
    }
}
