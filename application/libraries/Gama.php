<?php

class Gama {

    public $auth_info = array(
        'username',
        'password',
        'uo',
    );
    public $accessCode;
    public $eaid;
    public $SL;
    public $subeaid;
    public $userid;
    public $language = "true";
    protected $url = 'http://130.255.176.202/EUmra_Offline_EA_WebService/Group.asmx?WSDL';

    const DEBUG = TRUE;

    public $action;

    public function __construct() {
        $this->SL = &get_instance();
        require APPPATH . '/libraries/Xmltoarray.php';
    }

    public function setup($username, $password, $uo) {
        $this->auth_info['username'] = $username;
        $this->auth_info['password'] = $password;
        $this->auth_info['uo'] = $uo;
    }

    /**
     * Automatic Setup
     */
    public function auto_setup($contract_id = false) {
        $info = $this->SL->db->query("SELECT safa_uos.erp_uasp_id
        , safa_uo_contracts.uasp_eacode
        , safa_uo_contracts.uasp_username
        , safa_uo_contracts.uasp_password
        , safa_uo_contracts.safa_uo_contract_id
        , erp_uasp." . name() . " AS uasp
        , safa_uo_contracts." . name() . " AS contract
        FROM safa_uo_contracts
        INNER JOIN safa_uos ON safa_uo_contracts.safa_uo_id = safa_uos.safa_uo_id
        INNER JOIN safa_uo_contracts_eas ON safa_uo_contracts_eas.safa_uo_contract_id = safa_uo_contracts.safa_uo_contract_id
        INNER JOIN safa_eas ON safa_uo_contracts_eas.safa_ea_id = safa_eas.safa_ea_id
        INNER JOIN erp_uasp ON erp_uasp.erp_uasp_id = safa_uos.erp_uasp_id
        WHERE safa_eas.safa_ea_id = '" . session('ea_id') . "' AND erp_uasp.erp_uasp_id = '3' AND safa_uo_contracts.safa_uo_contract_id = '" . $contract_id . "'")->row();
        $this->setup($info->uasp_username, $info->uasp_password, $info->uasp_eacode);
    }

    /**
     * Getting EA Contracts
     * @return Array
     */
    public function contracts() {
        $auth = $this->SL->db->query("SELECT safa_uos.erp_uasp_id
        , safa_uo_contracts.uasp_eacode
        , safa_uo_contracts.uasp_username
        , safa_uo_contracts.uasp_password
        , safa_uo_contracts.safa_uo_contract_id
        , erp_uasp." . name() . " AS uasp
        , safa_uo_contracts." . name() . " AS contract
        FROM safa_uo_contracts
        INNER JOIN safa_uos ON safa_uo_contracts.safa_uo_id = safa_uos.safa_uo_id
        INNER JOIN safa_uo_contracts_eas ON safa_uo_contracts_eas.safa_uo_contract_id = safa_uo_contracts.safa_uo_contract_id
        INNER JOIN safa_eas ON safa_uo_contracts_eas.safa_ea_id = safa_eas.safa_ea_id
        INNER JOIN erp_uasp ON erp_uasp.erp_uasp_id = safa_uos.erp_uasp_id
        WHERE safa_eas.safa_ea_id = '" . session('ea_id') . "' AND erp_uasp.erp_uasp_id = '3'")->result();
        return $auth;
    }

    /**
     * Authenticate
     */
    public function Auth() {
        $this->action = 'EUMRA.Offline.EA.WebService/GetUserDetailsNew';
        $response = $this->sendRequest(
                '<?xml version="1.0" encoding="utf-8"?>
                  <soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
                    <soap:Body>
                      <GetUserDetailsNew xmlns="EUMRA.Offline.EA.WebService">
                        <UOID>' . $this->auth_info['uo'] . '</UOID>
                        <LoginID>' . $this->auth_info['username'] . '</LoginID>
                        <LoginPassword>' . $this->auth_info['password'] . '</LoginPassword>
                        <ArabicLanguage>true</ArabicLanguage>
                      </GetUserDetailsNew>
                    </soap:Body>
                  </soap:Envelope>');

        $parser = xml_parser_create();
        xml_parser_set_option($parser, XML_OPTION_CASE_FOLDING, 0);
        xml_parser_set_option($parser, XML_OPTION_SKIP_WHITE, 1);
        xml_parse_into_struct($parser, $response, $values, $tags);
        xml_parser_free($parser);

        if (!isset($values['3']['value']))
            return FALSE;
        $response = explode(',', $values['3']['value']);
        $this->accessCode = $response['1'];
        $this->eaid = $response['2'];
        $this->subeaid = $response['3'];
        $this->userid = $response['4'];
        return $response['1'];
    }

    public function InsertGroupAndMutamersData($group, $mutamers) {
        $this->action = 'EUMRA.Offline.EA.WebService/InsertGroupAndMutamersData_New';
        $mutamer_array = array(
            'MD_NO',
            'MD_GENDER_ID',
            'MD_MARITAL_STATUS_ID',
            'MD_RELATIVE_NO',
            'MD_RELATIVE_GENDER_ID',
            'MD_PASSPORT_DPN_COUNT',
            'MD_DPN_SERIAL_NO',
            'MD_RELATIVE_RELATION_ID',
            'MD_NATIONALITY_ID',
            'MD_PASSPORT_NO',
            'MD_PASSPORT_TYPE',
            'MD_PASSPORT_ISSUE_DATE',
            'MD_PASSPORT_EXPIRY_DATE',
            'MD_PASSPORT_ISSUING_COUNTRY_ID',
            'MD_PASSPORT_ISSUING_CITY',
            'MD_BIRTH_COUNTRY_ID',
            'MD_BIRTH_CITY',
            'MD_DATE_OF_BIRTH',
            'MD_AGE',
            'MD_FULL_NAME',
            'MD_FIRST_NAME_LA',
            'MD_SECOND_NAME_LA',
            'MD_THIRD_NAME_LA',
            'MD_FOURTH_NAME_LA',
            'MD_FIRST_NAME_AR',
            'MD_SECOND_NAME_AR',
            'MD_THIRD_NAME_AR',
            'MD_FOURTH_NAME_AR',
            'MD_TITLE_ID',
            'MD_EDUCATIONAL_LEVEL',
            'MD_OCCUPATION',
            'MD_PREVIOUS_NATIONALITY_ID',
            'MD_EMAIL_ADDRESS',
            'MD_PHONE_NO',
            'MD_STREET_NAME',
            'MD_ZIP_CODE',
            'MD_CITY',
            'MD_REGION_NAME',
            'MD_COUNTRY_ID',
            'MD_REMARKS',
        );
        
        $mutamers_req = null;
        $i = 0;
        foreach ($mutamers as $mutamer) {
            $i++;
            $mutamers_req .= "<Table1>\n";
            foreach($mutamer_array as $arr)
                if(isset($mutamer[$arr]))
                    if($arr == 'MD_PASSPORT_ISSUE_DATE' or $arr == 'MD_PASSPORT_EXPIRY_DATE' or $arr == 'MD_DATE_OF_BIRTH')
                        $mutamers_req .= "<$arr>". date('Y-m-d', strtotime($mutamer[$arr])) ."</$arr>\n";
                    elseif($arr == 'MD_AGE'){
                        $year = date('Y')-date('Y', strtotime($mutamer['MD_DATE_OF_BIRTH']));
                        $mutamers_req .= "<$arr>" . $year . "</$arr>\n";
                    }else
                        $mutamers_req .= "<$arr>$mutamer[$arr]</$arr>\n";
            $mutamers_req .= "<MD_OFFLINE_VERSION>$group[OfflineVersion]</MD_OFFLINE_VERSION>\n";
            $mutamers_req .= "<MD_DATA_ENTRY_SOURCE>Online Import</MD_DATA_ENTRY_SOURCE>\n";
            $mutamers_req .= "<MD_GROUP_NO>$group[safa_umrahgroup_id]</MD_GROUP_NO>\n";
            $mutamers_req .=  "</Table1>\n\n";
        }

        $response = $this->sendRequest(
                '<?xml version="1.0" encoding="utf-8"?>'
                . '<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">'
                . '<soap:Body>'
                . '<InsertGroupAndMutamersData_New xmlns="EUMRA.Offline.EA.WebService">'
                . '<AccessCode>' . $this->accessCode . '</AccessCode>'
                . '<UOID>' . $this->auth_info['uo'] . '</UOID>'
                . '<EAID>' . $this->eaid . '</EAID>'
                . '<SubEAID>' . $this->subeaid . '</SubEAID>'
                . '<UserID>' . $this->userid . '</UserID>'
                . '<GroupName>' . $group['GroupName'] . '</GroupName>'
                . '<OfflineVersion>' . $group['OfflineVersion'] . '</OfflineVersion>'
                . '<PackageNo>' . $group['PackageNo'] . '</PackageNo>'
                . '<ConsulateID>' . $group['ConsulateID'] . '</ConsulateID>'
                . '<PlannedArrivalDate>' . $group['PlannedArrivalDate'] . '</PlannedArrivalDate>'
                . '<SendtoUO>' . $group['SendtoUO'] . '</SendtoUO>'
                . '<GroupRemarks>' . $group['GroupRemarks'] . '</GroupRemarks>'
                . '<ArabicInferface>' . $group['ArabicInferface'] . '</ArabicInferface>'
                . '<MutamersData>
                <xs:schema id="DataSet1" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
                <xs:element name="DataSet1" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">
                <xs:complexType>
                <xs:choice minOccurs="0" maxOccurs="unbounded">
                <xs:element name="Table1">
                <xs:complexType>
                <xs:sequence>
                  <xs:element name="MD_NO" type="xs:short" minOccurs="0" />
                  <xs:element name="MD_GENDER_ID" type="xs:unsignedByte" minOccurs="0" />
                  <xs:element name="MD_MARITAL_STATUS_ID" type="xs:unsignedByte" minOccurs="0" />
                  <xs:element name="MD_RELATIVE_NO" type="xs:short" minOccurs="0" />
                  <xs:element name="MD_RELATIVE_GENDER_ID" type="xs:unsignedByte" minOccurs="0" />
                  <xs:element name="MD_PASSPORT_DPN_COUNT" type="xs:unsignedByte" minOccurs="0" />
                  <xs:element name="MD_DPN_SERIAL_NO" type="xs:unsignedByte" minOccurs="0" />
                  <xs:element name="MD_RELATIVE_RELATION_ID" type="xs:unsignedByte" minOccurs="0" />
                  <xs:element name="MD_NATIONALITY_ID" type="xs:short" minOccurs="0" />
                  <xs:element name="MD_PASSPORT_NO" type="xs:string" minOccurs="0" />
                  <xs:element name="MD_PASSPORT_TYPE" type="xs:unsignedByte" minOccurs="0" />
                  <xs:element name="MD_PASSPORT_ISSUE_DATE" type="xs:string" minOccurs="0" />
                  <xs:element name="MD_PASSPORT_EXPIRY_DATE" type="xs:string" minOccurs="0" />
                  <xs:element name="MD_PASSPORT_ISSUING_COUNTRY_ID" type="xs:short" minOccurs="0" />
                  <xs:element name="MD_PASSPORT_ISSUING_CITY" type="xs:string" minOccurs="0" />
                  <xs:element name="MD_BIRTH_COUNTRY_ID" type="xs:short" minOccurs="0" />
                  <xs:element name="MD_BIRTH_CITY" type="xs:string" minOccurs="0" />
                  <xs:element name="MD_DATE_OF_BIRTH" type="xs:string" minOccurs="0" />
                  <xs:element name="MD_AGE" type="xs:unsignedByte" minOccurs="0" />
                  <xs:element name="MD_FULL_NAME" type="xs:string" minOccurs="0" />
                  <xs:element name="MD_FIRST_NAME_LA" type="xs:string" minOccurs="0" />
                  <xs:element name="MD_SECOND_NAME_LA" type="xs:string" minOccurs="0" />
                  <xs:element name="MD_THIRD_NAME_LA" type="xs:string" minOccurs="0" />
                  <xs:element name="MD_FOURTH_NAME_LA" type="xs:string" minOccurs="0" />
                  <xs:element name="MD_FIRST_NAME_AR" type="xs:string" minOccurs="0" />
                  <xs:element name="MD_SECOND_NAME_AR" type="xs:string" minOccurs="0" />
                  <xs:element name="MD_THIRD_NAME_AR" type="xs:string" minOccurs="0" />
                  <xs:element name="MD_FOURTH_NAME_AR" type="xs:string" minOccurs="0" />
                  <xs:element name="MD_TITLE_ID" type="xs:unsignedByte" minOccurs="0" />
                  <xs:element name="MD_EDUCATIONAL_LEVEL" type="xs:unsignedByte" minOccurs="0" />
                  <xs:element name="MD_OCCUPATION" type="xs:string" minOccurs="0" />
                  <xs:element name="MD_PREVIOUS_NATIONALITY_ID" type="xs:short" minOccurs="0" />
                  <xs:element name="MD_EMAIL_ADDRESS" type="xs:string" minOccurs="0" />
                  <xs:element name="MD_PHONE_NO" type="xs:string" minOccurs="0" />
                  <xs:element name="MD_STREET_NAME" type="xs:string" minOccurs="0" />
                  <xs:element name="MD_ZIP_CODE" type="xs:string" minOccurs="0" />
                  <xs:element name="MD_CITY" type="xs:string" minOccurs="0" />
                  <xs:element name="MD_REGION_NAME" type="xs:string" minOccurs="0" />
                  <xs:element name="MD_COUNTRY_ID" type="xs:short" minOccurs="0" />
                  <xs:element name="MD_REMARKS" type="xs:string" minOccurs="0" />
                  <xs:element name="MD_DATA_ENTRY_SOURCE" type="xs:string" minOccurs="0" />
                  <xs:element name="MD_OFFLINE_VERSION" type="xs:string" minOccurs="0" />
                  <xs:element name="MD_GROUP_NO" type="xs:short" minOccurs="0" />
                  </xs:sequence>
                  </xs:complexType>
                  </xs:element>
                  </xs:choice>
                  </xs:complexType>
                  </xs:element>
                  </xs:schema>'
                . '<diffgr:diffgram xmlns:msdata="urn:schemas-microsoft-com:xml-msdata" xmlns:diffgr="urn:schemas-microsoft-com:xml-diffgram-v1">'
                . '<DataSet1 xmlns="">'
                . $mutamers_req
                . '</DataSet1>'
                . '</diffgr:diffgram>'
                . '</MutamersData>'
                . '</InsertGroupAndMutamersData_New>'
                . '</soap:Body>'
                . '</soap:Envelope>');
        return $this->Translate(str_replace(array('<?xml version="1.0" encoding="utf-8"?>', 'xmlns="EUMRA.Offline.EA.WebService"', 'xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"', 'soap:'), '', $response));
    }

    public function getPackages() {
        $this->action = 'EUMRA.Offline.EA.WebService/GetEAPackages';
        $response = $this->sendRequest(
                '<?xml version="1.0" encoding="utf-8"?>
                <soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
                  <soap:Body>
                    <GetEAPackages xmlns="EUMRA.Offline.EA.WebService">
                      <AccessCode>' . $this->accessCode . '</AccessCode>
                      <UOID>' . $this->auth_info['uo'] . '</UOID>
                      <EAID>' . $this->eaid . '</EAID>
                      <SubEAID>' . $this->subeaid . '</SubEAID>
                      <UserID>' . $this->userid . '</UserID>
                    </GetEAPackages>
                  </soap:Body>
                </soap:Envelope>');

        $response = $this->xml2array($response);

        if (!isset($response['soap:Envelope']['soap:Body']['GetEAPackagesResponse']['GetEAPackagesResult']['diffgr:diffgram']['DataSet1']['Table1']))
            return FALSE;

        $packages = $response['soap:Envelope']['soap:Body']['GetEAPackagesResponse']['GetEAPackagesResult']['diffgr:diffgram']['DataSet1']['Table1'];
        return $packages;
    }

    /**
     * sendRequest 
     * it takes the request and return the web service response
     * @param type $xml_request
     * @return string
     */
    protected function sendRequest($xml_request = NULL) {
        $header = array(
            "Content-type: text/xml;charset=\"utf-8\"",
            "Accept: text/xml",
            "Cache-Control: no-cache",
            "Pragma: no-cache",
            "SOAPAction: \"" . $this->action . "\"",
            "Content-length: " . strlen($xml_request),
        );

        $post = curl_init();
        curl_setopt($post, CURLOPT_URL, $this->url);
        curl_setopt($post, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($post, CURLOPT_POST, true);
        curl_setopt($post, CURLOPT_POSTFIELDS, $xml_request);
        curl_setopt($post, CURLOPT_HTTPHEADER, $header);
        $result = curl_exec($post);
        if ($result === false) {
            $result = 'Curl error: ' . curl_error($post);
            curl_close($post);
        } else {
            curl_close($post);
        }
        if (self::DEBUG) {
//               echo "<pre>";
//               print_r(htmlentities(debug_print_backtrace()));
            echo "--------------------- XML REQUEST ---------------------\n";
            echo ($xml_request);
            echo "\n--------------------- XML REQUEST ---------------------\n\n";

//               echo "--------------------- REQUEST ARRAY ---------------------\n";
//               $xml = $this->Translate($xml_request);
//               print_r($xml);
//               echo "\n--------------------- REQUEST ARRAY ---------------------\n\n";

            echo "--------------------- XML RESPONSE ---------------------\n";
            echo ($result);
            echo "\n--------------------- XML RESPONSE ---------------------\n";
//               echo "\n--------------------- RESPONSE ARRAY ---------------------\n";
//               $xml = $this->Translate($result);
//               print_r($xml);
//               echo "\n--------------------- RESPONSE ARRAY ---------------------\n";
            echo "\n******************************************************\n";
//               echo "</pre>";
        }
        return $result;
    }

    private function Translate($xml) {
        $array = Xmltoarray::createArray($xml);
        return ($array);
    }
    
    function xml2array($contents, $get_attributes = 1, $priority = 'tag') {
        if (!function_exists('xml_parser_create')) {
            return array();
        }
        $parser = xml_parser_create('');
        xml_parser_set_option($parser, XML_OPTION_TARGET_ENCODING, "UTF-8");
        xml_parser_set_option($parser, XML_OPTION_CASE_FOLDING, 0);
        xml_parser_set_option($parser, XML_OPTION_SKIP_WHITE, 1);
        xml_parse_into_struct($parser, trim($contents), $xml_values);
        xml_parser_free($parser);
        if (!$xml_values)
            return; //Hmm...
        $xml_array = array();
        $parents = array();
        $opened_tags = array();
        $arr = array();
        $current = & $xml_array;
        $repeated_tag_index = array();
        foreach ($xml_values as $data) {
            unset($attributes, $value);
            extract($data);
            $result = array();
            $attributes_data = array();
            if (isset($value)) {
                if ($priority == 'tag')
                    $result = $value;
                else
                    $result['value'] = $value;
            }
            if (isset($attributes) and $get_attributes) {
                foreach ($attributes as $attr => $val) {
                    if ($priority == 'tag')
                        $attributes_data[$attr] = $val;
                    else
                        $result['attr'][$attr] = $val; //Set all the attributes in a array called 'attr'
                }
            }
            if ($type == "open") {
                $parent[$level - 1] = & $current;
                if (!is_array($current) or (!in_array($tag, array_keys($current)))) {
                    $current[$tag] = $result;
                    if ($attributes_data)
                        $current[$tag . '_attr'] = $attributes_data;
                    $repeated_tag_index[$tag . '_' . $level] = 1;
                    $current = & $current[$tag];
                }
                else {
                    if (isset($current[$tag][0])) {
                        $current[$tag][$repeated_tag_index[$tag . '_' . $level]] = $result;
                        $repeated_tag_index[$tag . '_' . $level] ++;
                    } else {
                        $current[$tag] = array(
                            $current[$tag],
                            $result
                        );
                        $repeated_tag_index[$tag . '_' . $level] = 2;
                        if (isset($current[$tag . '_attr'])) {
                            $current[$tag]['0_attr'] = $current[$tag . '_attr'];
                            unset($current[$tag . '_attr']);
                        }
                    }
                    $last_item_index = $repeated_tag_index[$tag . '_' . $level] - 1;
                    $current = & $current[$tag][$last_item_index];
                }
            } elseif ($type == "complete") {
                if (!isset($current[$tag])) {
                    $current[$tag] = $result;
                    $repeated_tag_index[$tag . '_' . $level] = 1;
                    if ($priority == 'tag' and $attributes_data)
                        $current[$tag . '_attr'] = $attributes_data;
                }
                else {
                    if (isset($current[$tag][0]) and is_array($current[$tag])) {
                        $current[$tag][$repeated_tag_index[$tag . '_' . $level]] = $result;
                        if ($priority == 'tag' and $get_attributes and $attributes_data) {
                            $current[$tag][$repeated_tag_index[$tag . '_' . $level] . '_attr'] = $attributes_data;
                        }
                        $repeated_tag_index[$tag . '_' . $level] ++;
                    } else {
                        $current[$tag] = array(
                            $current[$tag],
                            $result
                        );
                        $repeated_tag_index[$tag . '_' . $level] = 1;
                        if ($priority == 'tag' and $get_attributes) {
                            if (isset($current[$tag . '_attr'])) {
                                $current[$tag]['0_attr'] = $current[$tag . '_attr'];
                                unset($current[$tag . '_attr']);
                            }
                            if ($attributes_data) {
                                $current[$tag][$repeated_tag_index[$tag . '_' . $level] . '_attr'] = $attributes_data;
                            }
                        }
                        $repeated_tag_index[$tag . '_' . $level] ++; //0 and 1 index is already taken
                    }
                }
            } elseif ($type == 'close') {
                $current = & $parent[$level - 1];
            }
        }
        return ($xml_array);
    }

}
