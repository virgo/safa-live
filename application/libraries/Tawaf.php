<?php

class Tawaf {

    public $auth_info = array(
        'username',
        'password',
    );
    public $version = '3.0.0.0';
    protected $url_mutamers = 'http://www.tawaf.com.sa/umra_ws/OfflineMutamerssSoapHttpPort?wsdl';
    public $login;
    const DEBUG = TRUE;

    public $online_group_id = FALSE;
    public $status = FALSE;
    public $SL;
    public $family = 0;

    public function __construct() {
        $this->SL = & get_instance();
        require APPPATH . '/libraries/Xmltoarray.php';
//        $CI->load->library('xmltoarray');
    }

    /**
     * Getting EA Contracts
     * @return Array
     */
    public function contracts() {
        $auth = $this->SL->db->query("SELECT safa_uos.erp_uasp_id
        , safa_uo_contracts.uasp_eacode
        , safa_uo_contracts.uasp_username
        , safa_uo_contracts.uasp_password
        , safa_uo_contracts.safa_uo_contract_id
        , erp_uasp." . name() . " AS uasp
        , safa_uo_contracts." . name() . " AS contract
        FROM safa_uo_contracts
        INNER JOIN safa_uos ON safa_uo_contracts.safa_uo_id = safa_uos.safa_uo_id
        INNER JOIN safa_uo_contracts_eas ON safa_uo_contracts_eas.safa_uo_contract_id = safa_uo_contracts.safa_uo_contract_id
        INNER JOIN safa_eas ON safa_uo_contracts_eas.safa_ea_id = safa_eas.safa_ea_id
        INNER JOIN erp_uasp ON erp_uasp.erp_uasp_id = safa_uos.erp_uasp_id
        WHERE safa_eas.safa_ea_id = '" . session('ea_id') . "' AND erp_uasp.erp_uasp_id = '4'")->result();
        return $auth;
    }

    /**
     * Setup
     */
    public function setup($username, $password) {
        $this->auth_info['username'] = $username;
        $this->auth_info['password'] = $password;
    }

    /**
     * Automatic Setup
     */
    public function auto_setup($contract_id = false) {
        $info = $this->SL->db->query("SELECT safa_uos.erp_uasp_id
        , safa_uo_contracts.uasp_eacode
        , safa_uo_contracts.uasp_username
        , safa_uo_contracts.uasp_password
        , safa_uo_contracts.safa_uo_contract_id
        , erp_uasp." . name() . " AS uasp
        , safa_uo_contracts." . name() . " AS contract
        FROM safa_uo_contracts
        INNER JOIN safa_uos ON safa_uo_contracts.safa_uo_id = safa_uos.safa_uo_id
        INNER JOIN safa_uo_contracts_eas ON safa_uo_contracts_eas.safa_uo_contract_id = safa_uo_contracts.safa_uo_contract_id
        INNER JOIN safa_eas ON safa_uo_contracts_eas.safa_ea_id = safa_eas.safa_ea_id
        INNER JOIN erp_uasp ON erp_uasp.erp_uasp_id = safa_uos.erp_uasp_id
        WHERE safa_eas.safa_ea_id = '" . session('ea_id') . "' AND erp_uasp.erp_uasp_id = '4' AND safa_uo_contracts.safa_uo_contract_id = '" . $contract_id . "'")->row();
        $this->setup($info->uasp_username, $info->uasp_password, $info->uasp_eacode);
    }

    /**
     * Authenticate
     */
    public function auth() {
        $this->action = 'http://www.babalumra.com/BAU/Authenticate';
        $response = $this->sendRequest(
                '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:typ="http://umra.server.com/types/">
                <soapenv:Header/>
                <soapenv:Body>
                   <typ:getLoginElement>
                      <typ:userName>' . $this->auth_info['username'] . '</typ:userName>
                      <typ:password>' . $this->auth_info['password'] . '</typ:password>
                   </typ:getLoginElement>
                </soapenv:Body>
             </soapenv:Envelope>', 'http://www.tawaf.com.sa/umra_ws/Offline_LoginSoapHttpPort?wsdl');
        $data = $this->Translate($response);
        if(isset($data['env:Envelope']['env:Body']['ns0:getLoginResponseElement']['ns0:result']['OFFLINE_LOGIN']['LOGIN'])) {
            $this->login = $data['env:Envelope']['env:Body']['ns0:getLoginResponseElement']['ns0:result']['OFFLINE_LOGIN']['LOGIN'];
//            print_r($this->login);
            return TRUE;
        } else {
            return FALSE;
        }
    }

    private function insertOfflineGroups($group) {
        $response = $this->sendRequest(
                '<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
                <SOAP-ENV:Body>
                    <insertOffGroupsElement xmlns="http://umra.server.com/types/">
                      <GROUP_CODE>'.$group['GROUP_CODE'].'</GROUP_CODE>
                      <GRP_NAME>'.$group['GRP_NAME'].'</GRP_NAME>
                      <GRP_UO_CODE>'.$this->login['UO_CODE'].'</GRP_UO_CODE>
                      <GRP_EA_CODE>'.$this->login['EA_CODE'].'</GRP_EA_CODE>
                      <GRP_EA_BRANCH>'.$this->login['EA_BRANCH_CODE'].'</GRP_EA_BRANCH>
                      <GRP_EMB>'.$group['GRP_EMB'].'</GRP_EMB>
                      <GRP_CITY>0</GRP_CITY>
                      <GRP_CNTRY>'.$this->login['EA_COUNTRY_ID'].'</GRP_CNTRY>
                      <GRP_STATUS>1</GRP_STATUS>
                      <GRP_PACKAGE>0</GRP_PACKAGE>
                      <GRP_UPDATED>0</GRP_UPDATED>
                      <GRP_ARRIVAL />
                      <GRP_DEPARTURE />
                      <GRP_CREATE_DATE>'.date('Y').date('m').date('d').'</GRP_CREATE_DATE>
                      <userName>'.$this->auth_info['username'].'</userName>
                      <password>'.$this->auth_info['password'].'</password>
                    </insertOffGroupsElement>
                </SOAP-ENV:Body>
                </SOAP-ENV:Envelope>', 'http://www.tawaf.com.sa/umra_ws/OffGroupsSoapHttpPort?wsdl');

        $response = $this->Translate($response);
//        return ($response);
        if( isset($response['env:Envelope']['env:Body']['ns0:insertOffGroupsResponseElement']['ns0:result']) && preg_match("/Grp Saved:([0-9]+)/", $response['env:Envelope']['env:Body']['ns0:insertOffGroupsResponseElement']['ns0:result'], $match)){
            $this->SL->db->where('safa_umrahgroup_id', $group['GROUP_CODE'])
                     ->update('safa_umrahgroups', array(
                         'uasp_id' => $match['1']
                     ));
            $this->online_group_id = $match['1'];
        }
        else
        {
            die("خطأ اثناء ارسال جروب");
        }
    }
    
    
    private function insertMutamer($GROUP, $MUTAMER) {
        
        if(isset($MUTAMER['RELATION']) && $MUTAMER['RELATION'] == 0 && isset($MUTAMER['MAHRAM_CODE']) && $MUTAMER['MAHRAM_CODE'] == 0) {
            $this->family++;
            $mut_type = 1;
        }
        if(isset($MUTAMER['RELATION']) && $MUTAMER['RELATION'] == 15) {
            $this->family++;
            $mut_type = 4;
        }
        if( isset($MUTAMER['MAHRAM_CODE']) && $MUTAMER['MAHRAM_CODE'] != 0) {
            $mut_type = 2;
        }
        if(in_array($MUTAMER['PASS_NO'], $this->pass_nums)) {
            $mut_type = 3;
        }
                
        $req = '<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
                    <SOAP-ENV:Body>
                        <insertOffMutamersElement xmlns="http://umra.server.com/types/">';
        if(isset($MUTAMER['MUT_CODE'])) $req .='<MUT_CODE>'.$MUTAMER['MUT_CODE'].'</MUT_CODE>'; else $req .='<MUT_CODE/>';
        if(isset($MUTAMER['MUT_GRP_CODE'])) $req .='<MUT_GRP_CODE>'.$MUTAMER['MUT_GRP_CODE'].'</MUT_GRP_CODE>'; else $req .='<MUT_GRP_CODE/>';
        if(isset($MUTAMER['FIRST_NAME_AR'])) $req .='<FIRST_NAME_AR>'.$MUTAMER['FIRST_NAME_AR'].'</FIRST_NAME_AR>'; else $req .='<FIRST_NAME_AR/>';
        if(isset($MUTAMER['FATHER_NAME_AR'])) $req .='<FATHER_NAME_AR>'.$MUTAMER['FATHER_NAME_AR'].'</FATHER_NAME_AR>' ; else $req .='<FATHER_NAME_AR/>';
        if(isset($MUTAMER['MIDDLE_NAME_AR'])) $req .='<MIDDLE_NAME_AR>'.$MUTAMER['MIDDLE_NAME_AR'].'</MIDDLE_NAME_AR>'; else $req .='<MIDDLE_NAME_AR/>';
        if(isset($MUTAMER['LAST_NAME_AR'])) $req .='<LAST_NAME_AR>'.$MUTAMER['LAST_NAME_AR'].'</LAST_NAME_AR>'; else $req .='<LAST_NAME_AR/>';
        if(isset($MUTAMER['FIRST_NAME_LA'])) $req .='<FIRST_NAME_LA>'.$MUTAMER['FIRST_NAME_LA'].'</FIRST_NAME_LA>'; else $req .='<FIRST_NAME_LA/>';
        if(isset($MUTAMER['FATHER_NAME_LA'])) $req .='<FATHER_NAME_LA>'.$MUTAMER['FATHER_NAME_LA'].'</FATHER_NAME_LA>'; else $req .='<FATHER_NAME_LA/>';
        if(isset($MUTAMER['MIDDLE_NAME_LA'])) $req .='<MIDDLE_NAME_LA>'.$MUTAMER['MIDDLE_NAME_LA'].'</MIDDLE_NAME_LA>'; else $req .='<MIDDLE_NAME_LA/>';
        if(isset($MUTAMER['LAST_NAME_LA'])) $req .='<LAST_NAME_LA>'.$MUTAMER['LAST_NAME_LA'].'</LAST_NAME_LA>'; else $req .='<LAST_NAME_LA/>';
        if(isset($MUTAMER['MUT_TITLE'])) $req .='<MUT_TITLE>'.$MUTAMER['MUT_TITLE'].'</MUT_TITLE>'; else $req .='<MUT_TITLE/>';
        if(isset($MUTAMER['GENDER'])) $req .='<GENDER>'.$MUTAMER['GENDER'].'</GENDER>'; else $req .='<GENDER/>';
        if(isset($MUTAMER['DEPEND_NO'])) $req .='<DEPEND_NO>'.$MUTAMER['DEPEND_NO'].'</DEPEND_NO>'; else $req .='<DEPEND_NO/>';
        if(isset($MUTAMER['NATIONALITY'])) $req .='<NATIONALITY>'.$MUTAMER['NATIONALITY'].'</NATIONALITY>'; else $req .='<NATIONALITY/>';
        if(isset($MUTAMER['MARITAL_STATUS'])) $req .='<MARITAL_STATUS>'.$MUTAMER['MARITAL_STATUS'].'</MARITAL_STATUS>'; else $req .='<MARITAL_STATUS/>';
        if(isset($MUTAMER['EDUCATION'])) $req .='<EDUCATION>'.$MUTAMER['EDUCATION'].'</EDUCATION>' ; else $req .='<EDUCATION/>';
        if(isset($MUTAMER['MUT_JOB'])) $req .='<MUT_JOB>'.$MUTAMER['MUT_JOB'].'</MUT_JOB>'; else $req .='<MUT_JOB/>';
        if(isset($MUTAMER['CURR_CITY'])) $req .='<CURR_CITY>'.$MUTAMER['CURR_CITY'].'</CURR_CITY>'; else $req .='<CURR_CITY/>';
        if(isset($MUTAMER['CURR_COUNTRY'])) $req .='<CURR_COUNTRY>'.$MUTAMER['CURR_COUNTRY'].'</CURR_COUNTRY>'; else $req .='<CURR_COUNTRY/>';
        if(isset($MUTAMER['BIRTH_DATE'])) $req .='<BIRTH_DATE>'.$MUTAMER['BIRTH_DATE'].'</BIRTH_DATE>'; else $req .='<BIRTH_DATE/>';
        if(isset($MUTAMER['BIRTH_CITY'])) $req .='<BIRTH_CITY>'.$MUTAMER['BIRTH_CITY'].'</BIRTH_CITY>'; else $req .='<BIRTH_CITY/>';
        if(isset($MUTAMER['BIRTH_COUNTRY'])) $req .='<BIRTH_COUNTRY>'.$MUTAMER['BIRTH_COUNTRY'].'</BIRTH_COUNTRY>'; else $req .='<BIRTH_COUNTRY/>';
        if(isset($MUTAMER['MUTAGE'])) $req .='<MUTAGE>'.$MUTAMER['MUTAGE'].'</MUTAGE>'; else $req .='<MUTAGE/>';
        if(isset($MUTAMER['MAHRAM_CODE'])) $req .='<MAHRAM_CODE>'.$MUTAMER['MAHRAM_CODE'].'</MAHRAM_CODE>'; else $req .='<MAHRAM_CODE/>';
        if(isset($MUTAMER['RELATION'])) $req .='<RELATION>'.$MUTAMER['RELATION'].'</RELATION>'; else $req .='<RELATION/>';
        if(isset($MUTAMER['PASS_NO'])) $req .='<PASS_NO>'.$MUTAMER['PASS_NO'].'</PASS_NO>'; else $req .='<PASS_NO/>';
        if(isset($MUTAMER['PASS_TYPE'])) $req .='<PASS_TYPE>'.$MUTAMER['PASS_TYPE'].'</PASS_TYPE>'; else $req .='<PASS_TYPE/>';
        if(isset($MUTAMER['PASS_CITY'])) $req .='<PASS_CITY>'.$MUTAMER['PASS_CITY'].'</PASS_CITY>'; else $req .='<PASS_CITY/>';
        if(isset($MUTAMER['PASS_COUNTRY'])) $req .='<PASS_COUNTRY>'.$MUTAMER['PASS_COUNTRY'].'</PASS_COUNTRY>'; else $req .='<PASS_COUNTRY/>';
        if(isset($MUTAMER['PASS_ISSUE'])) $req .='<PASS_ISSUE>'.$MUTAMER['PASS_ISSUE'].'</PASS_ISSUE>'; else $req .='<PASS_ISSUE/>';
        if(isset($MUTAMER['PASS_EXPIRY'])) $req .='<PASS_EXPIRY>'.$MUTAMER['PASS_EXPIRY'].'</PASS_EXPIRY>'; else $req .='<PASS_EXPIRY/>';
        if(isset($MUTAMER['ADDRESS_AR'])) $req .='<ADDRESS_AR>'.$MUTAMER['ADDRESS_AR'].'</ADDRESS_AR>'; else $req .='<ADDRESS_AR/>';
        if(isset($MUTAMER['ADDRESS_LA'])) $req .='<ADDRESS_LA>'.$MUTAMER['ADDRESS_LA'].'</ADDRESS_LA>'; else $req .='<ADDRESS_LA/>';
        if(isset($MUTAMER['AREA_AR'])) $req .='<AREA_AR>'.$MUTAMER['AREA_AR'].'</AREA_AR>'; else $req .='<AREA_AR/>';
        if(isset($MUTAMER['AREA_LA'])) $req .='<AREA_LA>'.$MUTAMER['AREA_LA'].'</AREA_LA>'; else $req .='<AREA_LA/>';
        if(isset($MUTAMER['PHONE_NO'])) $req .='<PHONE_NO>'.$MUTAMER['PHONE_NO'].'</PHONE_NO>'; else $req .='<PHONE_NO/>';
        if(isset($MUTAMER['FAX_NO'])) $req .='<FAX_NO>'.$MUTAMER['FAX_NO'].'</FAX_NO>'; else $req .='<FAX_NO/>';
        if(isset($MUTAMER['MOBILE'])) $req .='<MOBILE>'.$MUTAMER['MOBILE'].'</MOBILE>'; else $req .='<MOBILE/>';
        $req .='<GRP_ONLINE>'.$this->online_group_id.'</GRP_ONLINE>';
        $req .='<MUT_UPDATED>0</MUT_UPDATED>' ;
        $req .='<MUT_OFFLINE>0</MUT_OFFLINE>';
        $req .='<MUT_PACKAGE>0</MUT_PACKAGE>';
        $req .='<UO_CODE>'.$this->login['UO_CODE'].'</UO_CODE>';
        $req .='<EA_CODE>'.$this->login['EA_CODE'].'</EA_CODE>';
        $req .='<EA_BRANCH>'.$this->login['EA_BRANCH_CODE'].'</EA_BRANCH>';
        $req .='<USERID>0</USERID>';
        $req .='<MUT_EMB>'.$GROUP['GRP_EMB'].'</MUT_EMB>';
        $req .='<userName>'.$this->auth_info['username'].'</userName>';
        $req .='<password>'.$this->auth_info['password'].'</password>';
        $req .='<FAMILY>'.$this->family.'</FAMILY>';
        $req .='<MUT_TYPE>'.$mut_type.'</MUT_TYPE>';
        
//        $req .='<MHR_ONLINE>'.$MUTAMER['MHR_ONLINE'].'</MHR_ONLINE>';
        
        
        $req .= '</insertOffMutamersElement>
           </SOAP-ENV:Body>
        </SOAP-ENV:Envelope>';
        
        $response = $this->sendRequest($req, 'http://www.tawaf.com.sa/umra_ws/OffGroupsSoapHttpPort?wsdl');
        print_r($this->Translate($response));
        $this->pass_nums[] = $MUTAMER['PASS_NO'];
        return $response;
        
    }

    public function insertMutamers($GROUP, $MUTAMERS) {
        
        $res = $this->insertOfflineGroups($GROUP);
        foreach($MUTAMERS as $MUTAMER)
        {
//            if()
            $this->insertMutamer($GROUP, $MUTAMER);
        }
            
    }
    /**
     * sendRequest 
     * it takes the request and return the web service response
     * @param type $xml_request
     * @return string
     */
    protected function sendRequest($xml_request = NULL, $url = null) {
        $header = array(
            "Content-type: text/xml;charset=\"utf-8\"",
            "Accept: text/xml",
            "Cache-Control: no-cache",
            "Pragma: no-cache",
//            "SOAPAction: \"" . $this->action . "\"",
            "Content-length: " . strlen($xml_request),
        );

        $soap_do = curl_init();
        curl_setopt($soap_do, CURLOPT_URL, $url);
        curl_setopt($soap_do, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($soap_do, CURLOPT_POST, true);
        curl_setopt($soap_do, CURLOPT_POSTFIELDS, $xml_request);
        curl_setopt($soap_do, CURLOPT_HTTPHEADER, $header);
        $result = curl_exec($soap_do);
        if ($result === false) {
            $result = 'Curl error: ' . curl_error($soap_do);
            curl_close($soap_do);
        } else {
            curl_close($soap_do);
        }

        if (self::DEBUG) {
//               print_r(htmlentities(debug_print_backtrace()));
            echo "--------------------- XML REQUEST ---------------------\n";
            echo ($xml_request);
            echo "\n--------------------- XML REQUEST ---------------------\n\n";

//               echo "--------------------- REQUEST ARRAY ---------------------\n";
//               $xml = $this->Translate($xml_request);
//               print_r($xml);
//               echo "\n--------------------- REQUEST ARRAY ---------------------\n\n";

            echo "--------------------- XML RESPONSE ---------------------\n";
            echo ($result);
            echo "\n--------------------- XML RESPONSE ---------------------\n";
            echo "\n--------------------- RESPONSE ARRAY ---------------------\n";
//            $xml = str_get_html($result);
//            print_r($xml);
            echo "\n--------------------- RESPONSE ARRAY ---------------------\n";
            echo "\n******************************************************\n";
        }

        return $result;
    }
    private function Translate($xml) {
        $xml = str_replace(array('<?xml version="1.0" encoding="utf-8"?>', 'soap:'), '', $xml);
        $array = Xmltoarray::createArray($xml);
        return ($array);
    }

}
