<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class MY_Form_validation extends CI_Form_validation {

    protected $CI;

    public function valid_phone($num) {
        return preg_match('/^([\+][0-9]{1,3}[\ \.\-])?([\(]{1}[0-9]{2,6}[\)])?([0-9\ \.\-\/]{3,20})((x|ext|extension)[\ ]?[0-9]{1,4})?$/', '', $num);
    }

    /**
    * Check to see if a date is valid by passing it to strtotime()
    * @param  String $date Date value to validate
    * @return Boolean      TRUE if it's a date, FALSE otherwise
    */
    public function valid_date($date)
    {
            return (strtotime($date) !== FALSE);
    }

    public function complex_password($password = FALSE) {
        if ($password)
            $password = preg_match('((?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%]).{6,20})', $password);
        else
            $password = FALSE;
        return $password;
    }

    public function max_dblength($str = false, $field = false) {
        $CI = & get_instance();
        $field = explode('.', $field);
        $schema = $CI->db->query("SHOW COLUMNS FROM `$field[0]` WHERE FIELD = '$field[1]'")->row();
        preg_match("/([0-9]+)/", $schema->Type, $matches, PREG_OFFSET_CAPTURE);
        $val = $matches[0][0];
        $field = $val;
        return (MB_ENABLED === TRUE) ? ($val >= mb_strlen($str)) : ($val >= strlen($str));
    }

    public function dropdown($id = FALSE, $length = 15) {
        if ($id)
            return preg_match('/^[1-9][0-9]{0,' . $length . '}$/', $id);
        else
            return TRUE;
    }

    // PREPARATION
    function formatPhone($num) {
        $num = preg_match('/^([\+][0-9]{1,3}[\ \.\-])?([\(]{1}[0-9]{2,6}[\)])?([0-9\ \.\-\/]{3,20})((x|ext|extension)[\ ]?[0-9]{1,4})?$/', '', $num);
//      $num = preg_replace('/[^0-9-]/', '', $num);
//      $num = preg_replace('/([0-9]{2})([0-9]{3})([0-9])/', '$1 $2 $3', $num);
//      $num = trim(str_replace('-', ' ', $num));
        return $num;
    }

    function unique_col($value, $str) {

        $this->CI = & get_instance();
        sscanf($str, '%[^.].%[^.].%[^.]', $table, $field, $id);
        $query = $this->CI->db->query("SHOW KEYS FROM " . $table . " WHERE Key_name = 'PRIMARY' ", false);
        $record = $query->result();
        $colum_name = $record[0]->Column_name;
        if (isset($this->CI->db)) {
            $this->CI->db->limit(1)->where($field, $value);
            if (isset($id))
                $this->CI->db->where($colum_name . '<>' . $id);
            $query = $this->CI->db->get($table);
            return $query->num_rows() === 0;
        }
        return false;
    }

    function unique_ea_user($value, $str) {
        sscanf($str, '%[^.].%[^.].%[^.]', $table, $field, $id);
        $this->CI = &get_instance();
        $query = $this->CI->db->query("SHOW KEYS FROM " . $table . "  WHERE Key_name = 'PRIMARY' ", false);
        $record = $query->result();
        $colum_name = $record[0]->Column_name;
        if (isset($this->CI->db)) {
            $this->CI->db->limit(1)->where($field, $value)->where('safa_ea_id', session('ea_id'));
            if (isset($id))
                $this->CI->db->where($colum_name . '<>' . $id);
            $query = $this->CI->db->get($table);
            return $query->num_rows() === 0;
        }
        return FALSE;
    }

    function unique_by_ito() {
        
    }
    
    //Added By Gouda.
	function unique_col_by_uo($value, $str) {

        $this->CI = & get_instance();
        sscanf($str, '%[^.].%[^.].%[^.]', $table, $field, $id);
        $query = $this->CI->db->query("SHOW KEYS FROM " . $table . " WHERE Key_name = 'PRIMARY' ", false);
        $record = $query->result();
        $colum_name = $record[0]->Column_name;
        if (isset($this->CI->db)) {
            $this->CI->db->limit(1)->where($field, $value);
            if (isset($id))
                $this->CI->db->where($colum_name . '<>' . $id);

            $this->CI->db->where('safa_uo_id', session('uo_id'));
            $query = $this->CI->db->get($table);
            return $query->num_rows() === 0;
        }
        return false;
    }

	function unique_col_by_ea($value, $str) {

        $this->CI = & get_instance();
        sscanf($str, '%[^.].%[^.].%[^.]', $table, $field, $id);
        $query = $this->CI->db->query("SHOW KEYS FROM " . $table . " WHERE Key_name = 'PRIMARY' ", false);
        $record = $query->result();
        $colum_name = $record[0]->Column_name;
        if (isset($this->CI->db)) {
            $this->CI->db->limit(1)->where($field, $value);
            if (isset($id))
                $this->CI->db->where($colum_name . '<>' . $id);

            $this->CI->db->where('safa_ea_id', session('ea_id'));
            $query = $this->CI->db->get($table);
            
            //echo $this->CI->db->last_query(); exit;
            
            return $query->num_rows() === 0;
        }
        return false;
    }
}

/* End of file MY_Form_validation.php */
/* Location: ./application/libraries/MY_Form_validation.php */