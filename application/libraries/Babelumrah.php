<?php

class Babelumrah {

    public $auth_info = array(
        'username',
        'password',
        'ea',
    );
    public $version = '3.0.0.0';
    public $action;
    public $language = 1;
    protected $url = 'http://service.babalumra.com/BAUWebService/BAUWebService.asmx?WSDL';
    protected $offline_headers;

    const DEBUG = FALSE;

    public $status = FALSE;
    public $SL;

    public function __construct() {
        $this->SL = & get_instance();
        require APPPATH . '/libraries/Xmltoarray.php';
//        $CI->load->library('xmltoarray');
    }

    /**
     * Automatic Setup
     */
    public function auto_setup($contract_id = false) {
        $info = $this->SL->db->query("SELECT safa_uos.erp_uasp_id
        , safa_uo_contracts.uasp_eacode
        , safa_uo_contracts.uasp_username
        , safa_uo_contracts.uasp_password
        , safa_uo_contracts.safa_uo_contract_id
        , erp_uasp." . name() . " AS uasp
        , safa_uo_contracts." . name() . " AS contract
        FROM safa_uo_contracts
        INNER JOIN safa_uos ON safa_uo_contracts.safa_uo_id = safa_uos.safa_uo_id
        INNER JOIN safa_uo_contracts_eas ON safa_uo_contracts_eas.safa_uo_contract_id = safa_uo_contracts.safa_uo_contract_id
        INNER JOIN safa_eas ON safa_uo_contracts_eas.safa_ea_id = safa_eas.safa_ea_id
        INNER JOIN erp_uasp ON erp_uasp.erp_uasp_id = safa_uos.erp_uasp_id
        WHERE safa_eas.safa_ea_id = '" . session('ea_id') . "' AND erp_uasp.erp_uasp_id = '1' AND safa_uo_contracts.safa_uo_contract_id = '" . $contract_id . "'")->row();
        $this->setup($info->uasp_username, $info->uasp_password, $info->uasp_eacode);
    }

    /**
     * Getting EA Contracts
     * @return Array
     */
    public function contracts() {
        $auth = $this->SL->db->query("SELECT safa_uos.erp_uasp_id
        , safa_uo_contracts.uasp_eacode
        , safa_uo_contracts.uasp_username
        , safa_uo_contracts.uasp_password
        , safa_uo_contracts.safa_uo_contract_id
        , erp_uasp." . name() . " AS uasp
        , safa_uo_contracts." . name() . " AS contract
        FROM safa_uo_contracts
        INNER JOIN safa_uos ON safa_uo_contracts.safa_uo_id = safa_uos.safa_uo_id
        INNER JOIN safa_uo_contracts_eas ON safa_uo_contracts_eas.safa_uo_contract_id = safa_uo_contracts.safa_uo_contract_id
        INNER JOIN safa_eas ON safa_uo_contracts_eas.safa_ea_id = safa_eas.safa_ea_id
        INNER JOIN erp_uasp ON erp_uasp.erp_uasp_id = safa_uos.erp_uasp_id
        WHERE safa_eas.safa_ea_id = '" . session('ea_id') . "' AND erp_uasp.erp_uasp_id = '1'")->result();
        return $auth;
    }

    /**
     * Setup
     */
    public function setup($username, $password, $ea) {
        $this->auth_info['username'] = $username;
        $this->auth_info['password'] = $password;
        $this->auth_info['ea'] = $ea;
    }

    /**
     * Authenticate
     */
    public function Auth() {
        $this->action = 'http://www.babalumra.com/BAU/Authenticate';
        $response = $this->sendRequest(
                '<?xml version="1.0" encoding="utf-8"?>
                <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:bau="http://www.babalumra.com/BAU">
                <soapenv:Header/>
                <soapenv:Body>
                   <bau:Authenticate>
                      <bau:username>' . $this->auth_info['username'] . '</bau:username>
                      <bau:password>' . $this->auth_info['password'] . '</bau:password>
                      <bau:version>' . $this->version . '</bau:version>
                      <bau:eaCode>' . $this->auth_info['ea'] . '</bau:eaCode>
                   </bau:Authenticate>
                </soapenv:Body>
             </soapenv:Envelope>');

        $_response = str_replace(array('<?xml version="1.0" encoding="utf-8"?>', 'soap:'), '', $response);
        $xml = new SimpleXMLElement($_response);
        if (!isset($xml->Header->OfflineHeader->EA[0]))
            return FALSE;
        $this->offline_headers['EA'] = $xml->Header->OfflineHeader->EA[0];
        $this->offline_headers['UO'] = $xml->Header->OfflineHeader->UO[0];
        $this->offline_headers['Ticket'] = $xml->Header->OfflineHeader->Ticket[0];
        $this->offline_headers['UserIP'] = $xml->Header->OfflineHeader->UserIP[0];
        $this->offline_headers['UserVer'] = $xml->Header->OfflineHeader->UserVer[0];
        $this->offline_headers['UserId'] = $xml->Header->OfflineHeader->UserId[0];
        return $this->status = TRUE;
    }

    /**
     * GetHotels
     * @param type $id
     */
    public function GetHotels($id) {
        $this->action = 'http://www.babalumra.com/BAU/GetHotels';
        $response = $this->sendRequest(
                '<?xml version="1.0" encoding="utf-8"?>
            <soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
              <soap:Header>
                <OfflineHeader xmlns="http://www.babalumra.com/BAU">
                  <EA>' . $this->offline_headers['EA'] . '</EA>
                  <UO>' . $this->offline_headers['UO'] . '</UO>
                  <Ticket>' . $this->offline_headers['Ticket'] . '</Ticket>
                  <UserIP>' . $this->offline_headers['UserIP'] . '</UserIP>
                  <UserVer>' . $this->offline_headers['UserVer'] . '</UserVer>
                  <UserId>' . $this->offline_headers['UserId'] . '</UserId>
                </OfflineHeader>
              </soap:Header>
              <soap:Body>
                <GetHotels xmlns="http://www.babalumra.com/BAU">
                  <groupId>' . $id . '</groupId>
                  <lang>' . $this->language . '</lang>
                </GetHotels>
              </soap:Body>
            </soap:Envelope>');

        $response = str_replace(array('<?xml version="1.0" encoding="utf-8"?>', 'soap:'), '', $response);
        $xml = new SimpleXMLElement($response);
        if (isset($xml->Body->Fault))
            return $this->status = FALSE;
        else
            return $this->status = TRUE;
    }

    /**
     * GetInfo
     */
    public function GetInfo() {
        $this->action = 'http://www.babalumra.com/BAU/GetInfo';
        $response = $this->sendRequest(
                '<?xml version="1.0" encoding="utf-8"?>
            <soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
              <soap:Header>
                <OfflineHeader xmlns="http://www.babalumra.com/BAU">
                  <EA>' . $this->offline_headers['EA'] . '</EA>
                  <UO>' . $this->offline_headers['UO'] . '</UO>
                  <Ticket>' . $this->offline_headers['Ticket'] . '</Ticket>
                  <UserIP>' . $this->offline_headers['UserIP'] . '</UserIP>
                  <UserVer>' . $this->offline_headers['UserVer'] . '</UserVer>
                  <UserId>' . $this->offline_headers['UserId'] . '</UserId>
                </OfflineHeader>
              </soap:Header>
              <soap:Body>
                <GetInfo xmlns="http://www.babalumra.com/BAU" />
              </soap:Body>
            </soap:Envelope>');

        return $this->Translate($response);
    }

    /**
     * GetLookups
     */
    public function GetLookups() {
        $this->action = 'http://www.babalumra.com/BAU/GetLookups';
        $response = $this->sendRequest(
                '<?xml version="1.0" encoding="utf-8"?>
            <soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
              <soap:Header>
                <OfflineHeader xmlns="http://www.babalumra.com/BAU">
                  <EA>' . $this->offline_headers['EA'] . '</EA>
                  <UO>' . $this->offline_headers['UO'] . '</UO>
                  <Ticket>' . $this->offline_headers['Ticket'] . '</Ticket>
                  <UserIP>' . $this->offline_headers['UserIP'] . '</UserIP>
                  <UserVer>' . $this->offline_headers['UserVer'] . '</UserVer>
                  <UserId>' . $this->offline_headers['UserId'] . '</UserId>
                </OfflineHeader>
              </soap:Header>
              <soap:Body>
                <GetLookups xmlns="http://www.babalumra.com/BAU">
                    <lang>' . $this->language . '</lang>
                </GetLookups>
              </soap:Body>
            </soap:Envelope>');
        return $this->Translate($response);
    }

    /**
     * GetMOFA
     */
    public function GetMOFA($int) {
        $this->action = 'http://www.babalumra.com/BAU/GetMOFA';
        $response = $this->sendRequest(
                '<?xml version="1.0" encoding="utf-8"?>
            <soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
              <soap:Header>
                <OfflineHeader xmlns="http://www.babalumra.com/BAU">
                  <EA>' . $this->offline_headers['EA'] . '</EA>
                  <UO>' . $this->offline_headers['UO'] . '</UO>
                  <Ticket>' . $this->offline_headers['Ticket'] . '</Ticket>
                  <UserIP>' . $this->offline_headers['UserIP'] . '</UserIP>
                  <UserVer>' . $this->offline_headers['UserVer'] . '</UserVer>
                  <UserId>' . $this->offline_headers['UserId'] . '</UserId>
                </OfflineHeader>
              </soap:Header>
              <soap:Body>
                 <GetMOFA xmlns="http://www.babalumra.com/BAU">
                    <grID>
                      <int>' . $int . '</int>
                    </grID>
                    <lang>' . $this->language . '</lang>
                  </GetMOFA>
              </soap:Body>
            </soap:Envelope>');

        return $this->Translate($response);
    }

    /**
     * GetOnlineGroups
     */
    public function GetOnlineGroups() {

        $this->action = 'http://www.babalumra.com/BAU/GetOnlineGroups';
        $response = $this->sendRequest(
                '<?xml version="1.0" encoding="utf-8"?>
            <soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
              <soap:Header>
                <OfflineHeader xmlns="http://www.babalumra.com/BAU">
                  <EA>' . $this->offline_headers['EA'] . '</EA>
                  <UO>' . $this->offline_headers['UO'] . '</UO>
                  <Ticket>' . $this->offline_headers['Ticket'] . '</Ticket>
                  <UserIP>' . $this->offline_headers['UserIP'] . '</UserIP>
                  <UserVer>' . $this->offline_headers['UserVer'] . '</UserVer>
                  <UserId>' . $this->offline_headers['UserId'] . '</UserId>
                </OfflineHeader>
              </soap:Header>
              <soap:Body>
                <GetOnlineGroups xmlns="http://www.babalumra.com/BAU">
                    <lang>' . $this->language . '</lang>
                </GetOnlineGroups>
              </soap:Body>
            </soap:Envelope>'
        );

        return $this->Translate($response);
    }

    /**
     * GetOnlineStatus
     * @param type $int
     * @param type $int2
     */
    public function GetOnlineStatus($int, $int2) {
        $this->action = 'http://www.babalumra.com/BAU/GetOnlineStatus';
        $response = $this->sendRequest(
                '<?xml version="1.0" encoding="utf-8"?>
            <soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
              <soap:Header>
                <OfflineHeader xmlns="http://www.babalumra.com/BAU">
                  <EA>' . $this->offline_headers['EA'] . '</EA>
                  <UO>' . $this->offline_headers['UO'] . '</UO>
                  <Ticket>' . $this->offline_headers['Ticket'] . '</Ticket>
                  <UserIP>' . $this->offline_headers['UserIP'] . '</UserIP>
                  <UserVer>' . $this->offline_headers['UserVer'] . '</UserVer>
                  <UserId>' . $this->offline_headers['UserId'] . '</UserId>
                </OfflineHeader>
              </soap:Header>
              <soap:Body>
              <GetOnlineStatus xmlns="http://www.babalumra.com/BAU">
                <grID>
                  <int>' . $int . '</int>
                  <int>' . $int2 . '</int>
                </grID>
                <lang>' . $this->language . '</lang>
              </GetOnlineStatus>
                
              </soap:Body>
            </soap:Envelope>'
        );

        return $this->Translate($response);
    }

    /**
     * GetVoucherInfo
     * @param type $group_id
     */
    public function GetVoucherInfo($group_id) {
        $this->action = 'http://www.babalumra.com/BAU/GetVoucherInfo';
        $response = $this->sendRequest(
                '<?xml version="1.0" encoding="utf-8"?>
            <soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
              <soap:Header>
                <OfflineHeader xmlns="http://www.babalumra.com/BAU">
                  <EA>' . $this->offline_headers['EA'] . '</EA>
                  <UO>' . $this->offline_headers['UO'] . '</UO>
                  <Ticket>' . $this->offline_headers['Ticket'] . '</Ticket>
                  <UserIP>' . $this->offline_headers['UserIP'] . '</UserIP>
                  <UserVer>' . $this->offline_headers['UserVer'] . '</UserVer>
                  <UserId>' . $this->offline_headers['UserId'] . '</UserId>
                </OfflineHeader>
              </soap:Header>
              <soap:Body>
              <GetVoucherInfo xmlns="http://www.babalumra.com/BAU">
                <language>' . $this->language . '</language>
                <GroupId>' . $group_id . '</GroupId>
              </GetVoucherInfo>
              </soap:Body>
            </soap:Envelope>'
        );

        return $this->Translate($response);
    }

    /**
     * InsertGroup
     * @param type $group
     * @param type $dsMutamer
     */
    public function InsertGroup($GROUP = FALSE, $MUTAMERS = FALSE) {
        $groups = '<GROUPS>';
        if (isset($GROUP['GR_INTERNAL_ID']) && $GROUP['GR_INTERNAL_ID'] !== '')
            $groups .='<GR_INTERNAL_ID>' . $GROUP['GR_INTERNAL_ID'] . '</GR_INTERNAL_ID>';
        else
            $groups .= '<GR_INTERNAL_ID />';
        if (isset($GROUP['GR_GROUP_NAME']) && $GROUP['GR_GROUP_NAME'] !== '')
            $groups .='<GR_GROUP_NAME>' . $GROUP['GR_GROUP_NAME'] . '</GR_GROUP_NAME>';
        else
            $groups .= '<GR_GROUP_NAME />';
        if (isset($GROUP['GR_CONSULATE_NAME']) && $GROUP['GR_CONSULATE_NAME'] !== '')
            $groups .='<GR_CONSULATE_NAME>' . $GROUP['GR_CONSULATE_NAME'] . '</GR_CONSULATE_NAME>';
        else
            $groups .= '<GR_CONSULATE_NAME />';
        if (isset($GROUP['GR_YEAR']) && $GROUP['GR_YEAR'] !== '')
            $groups .='<GR_YEAR>' . $GROUP['GR_YEAR'] . '</GR_YEAR>';
        else
            $groups .= '<GR_YEAR />';
        if (isset($GROUP['GR_ID']) && $GROUP['GR_ID'] !== '')
            $groups .='<GR_ID>' . $GROUP['GR_ID'] . '</GR_ID>';
        else
            $groups .= '<GR_ID />';
        if (isset($GROUP['GR_STARTING_DATE']) && $GROUP['GR_STARTING_DATE'] !== '')
            $groups .='<GR_STARTING_DATE>' . $this->convert_date($GROUP['GR_STARTING_DATE']) . '</GR_STARTING_DATE>';
        else
            $groups .= '<GR_STARTING_DATE />';
        if (isset($GROUP['GR_ENDING_DATE']) && $GROUP['GR_ENDING_DATE'] !== '')
            $groups .='<GR_ENDING_DATE>' . $this->convert_date($GROUP['GR_ENDING_DATE']) . '</GR_ENDING_DATE>';
        else
            $groups .= '<GR_ENDING_DATE />';
        if (isset($GROUP['GR_DURATION']) && $GROUP['GR_DURATION'] !== '')
            $groups .='<GR_DURATION>' . $GROUP['GR_DURATION'] . '</GR_DURATION>';
        else
            $groups .= '<GR_DURATION />';
        if (isset($GROUP['GR_CONSULATE_ID']) && $GROUP['GR_CONSULATE_ID'] !== '')
            $groups .='<GR_CONSULATE_ID>' . $GROUP['GR_CONSULATE_ID'] . '</GR_CONSULATE_ID>';
        else
            $groups .= '<GR_CONSULATE_ID />';
        if (isset($GROUP['GR_NUMBER_MUHRAM']) && $GROUP['GR_NUMBER_MUHRAM'] !== '')
            $groups .='<GR_NUMBER_MUHRAM>' . $GROUP['GR_NUMBER_MUHRAM'] . '</GR_NUMBER_MUHRAM>';
        else
            $groups .= '<GR_NUMBER_MUHRAM />';
        if (isset($GROUP['GR_NUMBER_MUTAMERS']) && $GROUP['GR_NUMBER_MUTAMERS'] !== '')
            $groups .='<GR_NUMBER_MUTAMERS>' . $GROUP['GR_NUMBER_MUTAMERS'] . '</GR_NUMBER_MUTAMERS>';
        else
            $groups .= '<GR_NUMBER_MUTAMERS />';
        if (isset($GROUP['GR_NUMBER_COMPANIONS']) && $GROUP['GR_NUMBER_COMPANIONS'] !== '')
            $groups .='<GR_NUMBER_COMPANIONS>' . $GROUP['GR_NUMBER_COMPANIONS'] . '</GR_NUMBER_COMPANIONS>';
        else
            $groups .= '<GR_NUMBER_COMPANIONS />';
        if (isset($GROUP['GR_GROUP_STATUS']) && $GROUP['GR_GROUP_STATUS'] !== '')
            $groups .='<GR_GROUP_STATUS>' . $GROUP['GR_GROUP_STATUS'] . '</GR_GROUP_STATUS>';
        else
            $groups .= '<GR_GROUP_STATUS />';
        if (isset($GROUP['GR_PACKAGE_CODE']) && $GROUP['GR_PACKAGE_CODE'] !== '')
            $groups .='<GR_PACKAGE_CODE>' . $GROUP['GR_PACKAGE_CODE'] . '</GR_PACKAGE_CODE>';
        else
            $groups .= '<GR_PACKAGE_CODE />';
        if (isset($GROUP['GR_PACKAGE_NAME']) && $GROUP['GR_PACKAGE_NAME'] !== '')
            $groups .='<GR_PACKAGE_NAME>' . $GROUP['GR_PACKAGE_NAME'] . '</GR_PACKAGE_NAME>';
        else
            $groups .= '<GR_PACKAGE_NAME />';
        if (isset($GROUP['GR_GROUP_STATUS_TEXT']) && $GROUP['GR_GROUP_STATUS_TEXT'] !== '')
            $groups .='<GR_GROUP_STATUS_TEXT>' . $GROUP['GR_GROUP_STATUS_TEXT'] . '</GR_GROUP_STATUS_TEXT>';
        else
            $groups .= '<GR_GROUP_STATUS_TEXT />';
        if (isset($GROUP['GR_EXPECTED_ARRIVAL_DATE']) && $GROUP['GR_EXPECTED_ARRIVAL_DATE'] !== '')
            $groups .='<GR_EXPECTED_ARRIVAL_DATE>' . $this->convert_date($GROUP['GR_EXPECTED_ARRIVAL_DATE']) . '</GR_EXPECTED_ARRIVAL_DATE>';
        else
            $groups .= '<GR_EXPECTED_ARRIVAL_DATE />';
        if (isset($GROUP['GR_EXPECTED_DEPARTURE_DATE']) && $GROUP['GR_EXPECTED_DEPARTURE_DATE'] !== '')
            $groups .='<GR_EXPECTED_DEPARTURE_DATE>' . $this->convert_date($GROUP['GR_EXPECTED_DEPARTURE_DATE']) . '</GR_EXPECTED_DEPARTURE_DATE>';
        else
            $groups .= '<GR_EXPECTED_DEPARTURE_DATE />';
        if (isset($GROUP['GR_SEND_HOURS']) && $GROUP['GR_SEND_HOURS'] !== '')
            $groups .='<GR_SEND_HOURS>' . $GROUP['GR_SEND_HOURS'] . '</GR_SEND_HOURS>';
        else
            $groups .= '<GR_SEND_HOURS />';
        if (isset($GROUP['GR_WOMEN']) && $GROUP['GR_WOMEN'] !== '')
            $groups .='<GR_WOMEN>' . $GROUP['GR_WOMEN'] . '</GR_WOMEN>';
        else
            $groups .= '<GR_WOMEN />';
        if (isset($GROUP['GR_HASMUHRAM']) && $GROUP['GR_HASMUHRAM'] !== '')
            $groups .='<GR_HASMUHRAM>' . $GROUP['GR_HASMUHRAM'] . '</GR_HASMUHRAM>';
        else
            $groups .= '<GR_HASMUHRAM />';
        $groups .='</GROUPS>';

        $dsMutamer = '<diffgr:diffgram xmlns:msdata="urn:schemas-microsoft-com:xml-msdata" xmlns:diffgr="urn:schemas-microsoft-com:xml-diffgram-v1">
            <DataSetMutamers xmlns="http://www.babalumra.com/DataSetMutamers.xsd">';
        $i = 0;
        if ($MUTAMERS && count($MUTAMERS) && is_array($MUTAMERS))
            foreach ($MUTAMERS as $mutamer) {
                $dsMutamer .= "<MUTAMERS diffgr:id=\"MUTAMERS" . ($i + 1) . "\" msdata:rowOrder=\"" . $i . "\" diffgr:hasChanges=\"inserted\">";
                if (isset($mutamer['SN']))
                    $dsMutamer .= "<SN>" . $mutamer['SN'] . "</SN>";
                else
                    $dsMutamer .= '<SN></SN>';
                if (isset($mutamer['FN']))
                    $dsMutamer .= "<FN>" . $mutamer['FN'] . "</FN>";
                else
                    $dsMutamer .= '<FN></FN>';
                if (isset($mutamer['LN']))
                    $dsMutamer .= "<LN>" . $mutamer['LN'] . "</LN>";
                else
                    $dsMutamer .= '<LN></LN>';
                if (isset($mutamer['MN']))
                    $dsMutamer .= "<MN>" . $mutamer['MN'] . "</MN>";
                else
                    $dsMutamer .= '<MN></MN>';
                if (isset($mutamer['TC']))
                    $dsMutamer .= "<TC>" . $mutamer['TC'] . "</TC>";
                else
                    $dsMutamer .= '<TC></TC>';
                if (isset($mutamer['G']))
                    $dsMutamer .= "<G>" . $mutamer['G'] . "</G>";
                else
                    $dsMutamer .= '<G></G>';
                if (isset($mutamer['MS']))
                    $dsMutamer .= "<MS>" . $mutamer['MS'] . "</MS>";
                else
                    $dsMutamer .= '<MS></MS>';
                if (isset($mutamer['N']))
                    $dsMutamer .= "<N>" . $mutamer['N'] . "</N>";
                else
                    $dsMutamer .= '<N></N>';
                if (isset($mutamer['CAC']))
                    $dsMutamer .= "<CAC>" . $mutamer['CAC'] . "</CAC>";
                else
                    $dsMutamer .= '<CAC></CAC>';
                if (isset($mutamer['CACO']))
                    $dsMutamer .= "<CACO>" . $mutamer['CACO'] . "</CACO>";
                else
                    $dsMutamer .= '<CACO></CACO>';
                if (isset($mutamer['PN']))
                    $dsMutamer .= "<PN>" . $mutamer['PN'] . "</PN>";
                else
                    $dsMutamer .= '<PN></PN>';
                if (isset($mutamer['PT']))
                    $dsMutamer .= "<PT>" . $mutamer['PT'] . "</PT>";
                else
                    $dsMutamer .= '<PT></PT>';
                if (isset($mutamer['CIA']))
                    $dsMutamer .= "<CIA>" . $mutamer['CIA'] . "</CIA>";
                else
                    $dsMutamer .= '<CIA></CIA>';
                if (isset($mutamer['DI']) && !empty($mutamer['DI']) && $mutamer['DI'] != '0')
                    $dsMutamer .= "<DI>" . $this->convert_date($mutamer['DI'], 2) . "</DI>";
                else
                    $dsMutamer .= '<DI />';
                if (isset($mutamer['DE']) && !empty($mutamer['DE']) && $mutamer['DE'] != '0')
                    $dsMutamer .= "<DE>" . $this->convert_date($mutamer['DE'], 2) . "</DE>";
                else
                    $dsMutamer .= '<DE />';
                if (isset($mutamer['COB']))
                    $dsMutamer .= "<COB>" . $mutamer['COB'] . "</COB>";
                else
                    $dsMutamer .= '<COB></COB>';
                if (isset($mutamer['CB']))
                    $dsMutamer .= "<CB>" . $mutamer['CB'] . "</CB>";
                else
                    $dsMutamer .= '<CB></CB>';
                if (isset($mutamer['DB']) && !empty($mutamer['DB']) && $mutamer['DB'] != '0')
                    $dsMutamer .= "<DB>" . $this->convert_date($mutamer['DB'], 2) . "</DB>";
                else
                    $dsMutamer .= '<DB />';
                if (isset($mutamer['FAN']))
                    $dsMutamer .= "<FAN>" . $mutamer['FAN'] . "</FAN>";
                else
                    $dsMutamer .= '<FAN></FAN>';
                if (isset($mutamer['MNR']) && !empty($mutamer['MNR']) && $mutamer['MNR'] != '0')
                    $dsMutamer .= "<MNR>" . $mutamer['MNR'] . "</MNR>";
                if (isset($mutamer['RT']) && !empty($mutamer['RT']) && $mutamer['RT'] != '0')
                    $dsMutamer .= "<RT>" . $mutamer['RT'] . "</RT>";
                if (isset($mutamer['RR']) && !empty($mutamer['RR']) && $mutamer['RR'] != '0')
                    $dsMutamer .= "<RR>" . $mutamer['RR'] . "</RR>";
                if (isset($mutamer['DSN']) && !empty($mutamer['DSN']) && $mutamer['DSN'] != '0')
                    $dsMutamer .= "<DSN>" . $mutamer['DSN'] . "</DSN>";
                if (isset($mutamer['EL']))
                    $dsMutamer .= "<EL>" . $mutamer['EL'] . "</EL>";
                else
                    $dsMutamer .= '<EL></EL>';
                if (isset($mutamer['MIG']) && !empty($mutamer['MIG']) && $mutamer['MIG'] != '0')
                    $dsMutamer .= "<MIG>" . $mutamer['MIG'] . "</MIG>";
                if (isset($mutamer['IM']) && !empty($mutamer['IM']) && $mutamer['IM'] != '0')
                    $dsMutamer .= "<IM>" . $mutamer['IM'] . "</IM>";
                if (isset($mutamer['CIAT']))
                    $dsMutamer .= "<CIAT>" . $mutamer['CIAT'] . "</CIAT>";
                else
                    $dsMutamer .= '<CIAT></CIAT>';
                if (isset($mutamer['J']))
                    $dsMutamer .= "<J>" . $mutamer['J'] . "</J>";
                else
                    $dsMutamer .= '<J></J>';
                if (isset($mutamer['AFN']))
                    $dsMutamer .= "<AFN>" . $mutamer['AFN'] . "</AFN>";
                else
                    $dsMutamer .= '<AFN></AFN>';
                if (isset($mutamer['ALN']))
                    $dsMutamer .= "<ALN>" . $mutamer['ALN'] . "</ALN>";
                else
                    $dsMutamer .= '<ALN></ALN>';
                if (isset($mutamer['AFAN']))
                    $dsMutamer .= "<AFAN>" . $mutamer['AFAN'] . "</AFAN>";
                else
                    $dsMutamer .= '<AFAN></AFAN>';
                if (isset($mutamer['AMN']))
                    $dsMutamer .= "<AMN>" . $mutamer['AMN'] . "</AMN>";
                else
                    $dsMutamer .= '<AMN></AMN>';
                if (isset($mutamer['TL']))
                    $dsMutamer .= "<TL>" . $mutamer['TL'] . "</TL>";
                else
                    $dsMutamer .= '<TL></TL>';
                $dsMutamer .= "<II>" . $mutamer['SN'] . "</II>";
                $dsMutamer .= "</MUTAMERS>";
                $i++;
            }

        $dsMutamer .= "</DataSetMutamers></diffgr:diffgram>";
        $this->action = 'http://www.babalumra.com/BAU/InsertGroup';
        $request = 
                '<?xml version="1.0" encoding="utf-8"?>
            <soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
              <soap:Header>
                <OfflineHeader xmlns="http://www.babalumra.com/BAU">
                  <EA>' . $this->offline_headers['EA'] . '</EA>
                  <UO>' . $this->offline_headers['UO'] . '</UO>
                  <Ticket>' . $this->offline_headers['Ticket'] . '</Ticket>
                  <UserIP>' . $this->offline_headers['UserIP'] . '</UserIP>
                  <UserVer>' . $this->offline_headers['UserVer'] . '</UserVer>
                  <UserId>' . $this->offline_headers['UserId'] . '</UserId>
                </OfflineHeader>
              </soap:Header>
              <soap:Body>
              <InsertGroup xmlns="http://www.babalumra.com/BAU">
                <group>
                    <xs:schema xmlns="http://www.babalumra.com/GroupsSchema.xsd" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata" xmlns:mstns="http://www.babalumra.com/GroupsSchema.xsd" xmlns:xs="http://www.w3.org/2001/XMLSchema" attributeFormDefault="qualified" elementFormDefault="qualified" targetNamespace="http://www.babalumra.com/GroupsSchema.xsd" id="DataSetGroups">
                        <xs:element msdata:IsDataSet="true" msdata:UseCurrentLocale="true" name="DataSetGroups">
                        <xs:complexType>
                        <xs:choice minOccurs="0" maxOccurs="unbounded">
                        <xs:element name="GROUPS">
                        <xs:complexType>
                        <xs:sequence>
                        <xs:element msdata:ReadOnly="true" msdata:AutoIncrement="true" name="GR_INTERNAL_ID" type="xs:int"/>
                        <xs:element minOccurs="0" name="GR_GROUP_NAME" type="xs:string"/>
                        <xs:element minOccurs="0" name="GR_CONSULATE_NAME" type="xs:string"/>
                        <xs:element minOccurs="0" name="GR_YEAR" type="xs:string"/>
                        <xs:element minOccurs="0" name="GR_ID" type="xs:string"/>
                        <xs:element minOccurs="0" name="GR_STARTING_DATE" type="xs:dateTime"/>
                        <xs:element minOccurs="0" name="GR_ENDING_DATE" type="xs:dateTime"/>
                        <xs:element minOccurs="0" name="GR_DURATION" type="xs:int"/>
                        <xs:element minOccurs="0" name="GR_CONSULATE_ID" type="xs:int"/>
                        <xs:element minOccurs="0" name="GR_NUMBER_MUHRAM" type="xs:int"/>
                        <xs:element minOccurs="0" name="GR_NUMBER_MUTAMERS" type="xs:int"/>
                        <xs:element minOccurs="0" name="GR_NUMBER_COMPANIONS" type="xs:int"/>
                        <xs:element minOccurs="0" name="GR_GROUP_STATUS" type="xs:int"/>
                        <xs:element minOccurs="0" name="GR_PACKAGE_CODE" type="xs:int"/>
                        <xs:element minOccurs="0" name="GR_PACKAGE_NAME" type="xs:string"/>
                        <xs:element minOccurs="0" name="GR_MAX_DEPARTURE_DATE" type="xs:dateTime"/>
                        <xs:element minOccurs="0" name="GR_GROUP_STATUS_TEXT" type="xs:string"/>
                        <xs:element minOccurs="0" name="GR_EXPECTED_ARRIVAL_DATE" type="xs:dateTime"/>
                        <xs:element minOccurs="0" name="GR_EXPECTED_DEPARTURE_DATE" type="xs:dateTime"/>
                        <xs:element minOccurs="0" name="GR_SEND_HOURS" type="xs:int"/>
                        <xs:element minOccurs="0" name="GR_WOMEN" type="xs:int"/>
                        <xs:element minOccurs="0" name="GR_ONLINE_ID" type="xs:string"/>
                        <xs:element minOccurs="0" name="GR_HASMOFA" type="xs:int"/>
                        <xs:element minOccurs="0" name="GR_HASMUHRAM" type="xs:int"/>
                        <xs:element minOccurs="0" name="GR_ONLINE_STATUS" type="xs:string"/>
                        </xs:sequence>
                        </xs:complexType>
                        </xs:element>
                        <xs:element name="VoucherInfo">
                        <xs:complexType>
                        <xs:sequence>
                        <xs:element minOccurs="0" name="p1" type="xs:string"/>
                        <xs:element minOccurs="0" name="p2" type="xs:string"/>
                        <xs:element minOccurs="0" name="p3" type="xs:string"/>
                        <xs:element minOccurs="0" name="p4" type="xs:string"/>
                        <xs:element minOccurs="0" name="p5" type="xs:string"/>
                        <xs:element minOccurs="0" name="p6" type="xs:string"/>
                        <xs:element minOccurs="0" name="p7" type="xs:string"/>
                        <xs:element minOccurs="0" name="p8" type="xs:string"/>
                        <xs:element minOccurs="0" name="p9" type="xs:string"/>
                        <xs:element minOccurs="0" name="p10" type="xs:string"/>
                        <xs:element minOccurs="0" name="p11" type="xs:string"/>
                        <xs:element minOccurs="0" name="p12" type="xs:string"/>
                        <xs:element minOccurs="0" name="p13" type="xs:string"/>
                        <xs:element minOccurs="0" name="p14" type="xs:string"/>
                        <xs:element name="GR_Online_ID" type="xs:string"/>
                        </xs:sequence>
                        </xs:complexType>
                        </xs:element>
                        </xs:choice>
                        </xs:complexType>
                        <xs:unique msdata:PrimaryKey="true" name="DataSetGroupsKey1">
                        <xs:selector xpath=".//mstns:GROUPS"/>
                        <xs:field xpath="mstns:GR_INTERNAL_ID"/>
                        </xs:unique>
                        <xs:unique msdata:PrimaryKey="true" name="DataSetGroupsKey2">
                        <xs:selector xpath=".//mstns:VoucherInfo"/>
                        <xs:field xpath="mstns:GR_Online_ID"/>
                        </xs:unique>
                        </xs:element>
                    </xs:schema>
                    <diffgr:diffgram xmlns:msdata="urn:schemas-microsoft-com:xml-msdata" xmlns:diffgr="urn:schemas-microsoft-com:xml-diffgram-v1">
                <DataSetGroups xmlns="http://www.babalumra.com/GroupsSchema.xsd">'.
                $groups 
                .'</DataSetGroups></diffgr:diffgram>
                    </group>
                <dsMutamer>
                    <xs:schema xmlns="http://www.babalumra.com/DataSetMutamers.xsd" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata" xmlns:mstns="http://www.babalumra.com/DataSetMutamers.xsd" xmlns:xs="http://www.w3.org/2001/XMLSchema" attributeFormDefault="qualified" elementFormDefault="qualified" targetNamespace="http://www.babalumra.com/DataSetMutamers.xsd" id="DataSetMutamers">
                        <xs:element msdata:IsDataSet="true" msdata:UseCurrentLocale="true" name="DataSetMutamers">
                        <xs:complexType>
                        <xs:choice minOccurs="0" maxOccurs="unbounded">
                        <xs:element name="MUTAMERS">
                        <xs:complexType>
                        <xs:sequence>
                        <xs:element msdata:ReadOnly="true" msdata:AutoIncrement="true" minOccurs="0" name="SN" type="xs:long"/>
                        <xs:element minOccurs="0" name="FN" type="xs:string"/>
                        <xs:element minOccurs="0" name="LN" type="xs:string"/>
                        <xs:element minOccurs="0" name="MN" type="xs:string"/>
                        <xs:element minOccurs="0" name="TC" type="xs:short"/>
                        <xs:element minOccurs="0" name="G" type="xs:int"/>
                        <xs:element minOccurs="0" name="MS" type="xs:short"/>
                        <xs:element minOccurs="0" name="N" type="xs:int"/>
                        <xs:element minOccurs="0" name="CAS" type="xs:string"/>
                        <xs:element minOccurs="0" name="CAC" type="xs:string"/>
                        <xs:element minOccurs="0" name="CACO" type="xs:int"/>
                        <xs:element minOccurs="0" name="CAR" type="xs:string"/>
                        <xs:element minOccurs="0" name="PN" type="xs:string"/>
                        <xs:element minOccurs="0" name="PT" type="xs:short"/>
                        <xs:element minOccurs="0" name="CIA" type="xs:int"/>
                        <xs:element minOccurs="0" name="DI" type="xs:string"/>
                        <xs:element minOccurs="0" name="DE" type="xs:string"/>
                        <xs:element minOccurs="0" name="COB" type="xs:int"/>
                        <xs:element minOccurs="0" name="CB" type="xs:string"/>
                        <xs:element minOccurs="0" name="DB" type="xs:string"/>
                        <xs:element minOccurs="0" name="FAN" type="xs:string"/>
                        <xs:element minOccurs="0" name="MNR" type="xs:short"/>
                        <xs:element minOccurs="0" name="RT" type="xs:long"/>
                        <xs:element minOccurs="0" name="RR" type="xs:short"/>
                        <xs:element minOccurs="0" name="DSN" type="xs:short"/>
                        <xs:element minOccurs="0" name="EL" type="xs:short"/>
                        <xs:element minOccurs="0" name="VT" type="xs:short"/>
                        <xs:element msdata:AutoIncrement="true" msdata:AutoIncrementSeed="1" name="II" type="xs:int"/>
                        <xs:element minOccurs="0" name="MIG" type="xs:int"/>
                        <xs:element minOccurs="0" name="IM" type="xs:boolean"/>
                        <xs:element minOccurs="0" name="CIAT" type="xs:string"/>
                        <xs:element minOccurs="0" name="CACN" type="xs:string"/>
                        <xs:element minOccurs="0" name="CIAN" type="xs:string"/>
                        <xs:element minOccurs="0" name="CBN" type="xs:string"/>
                        <xs:element minOccurs="0" name="OID" type="xs:string"/>
                        <xs:element minOccurs="0" name="MOFA" type="xs:long"/>
                        <xs:element minOccurs="0" name="J" type="xs:string"/>
                        <xs:element minOccurs="0" name="AFN" type="xs:string"/>
                        <xs:element minOccurs="0" name="ALN" type="xs:string"/>
                        <xs:element minOccurs="0" name="AFAN" type="xs:string"/>
                        <xs:element minOccurs="0" name="AMN" type="xs:string"/>
                        <xs:element minOccurs="0" name="MB" type="xs:string"/>
                        <xs:element minOccurs="0" name="EM" type="xs:string"/>
                        <xs:element minOccurs="0" name="FX" type="xs:string"/>
                        <xs:element minOccurs="0" name="BG" type="xs:string"/>
                        <xs:element minOccurs="0" name="TL" type="xs:string"/>
                        <xs:element minOccurs="0" name="PX" type="xs:string"/>
                        </xs:sequence>
                        </xs:complexType>
                        </xs:element>
                        </xs:choice>
                        </xs:complexType>
                        <xs:unique msdata:PrimaryKey="true" name="YAHAJJ_MUTAMER_DATAKey1">
                        <xs:selector xpath=".//mstns:MUTAMERS"/>
                        <xs:field xpath="mstns:II"/>
                        </xs:unique>
                        </xs:element>
                    </xs:schema>
                    ' . $dsMutamer . '
                    </dsMutamer>
                <language>' . $this->language . '</language>
              </InsertGroup>
              </soap:Body>
            </soap:Envelope>';
        $response = $this->sendRequest($request);
        
        $response = $this->Translate($response);
        if($response['soap:Envelope']['soap:Body']['InsertGroupResponse']['InsertGroupResult'])
            return $response['soap:Envelope']['soap:Body']['InsertGroupResponse']['InsertGroupResult'];
        else
            return FALSE;
    }

    /**
     * InsertMutamerBoardingTransaction
     * @param type $MutamerBoardingTransaction
     */
    public function InsertMutamerBoardingTransaction($MutamerBoardingTransaction = array()) {
        $this->action = 'http://www.babalumra.com/BAU/InsertMutamerBoardingTransaction';
        $response = $this->sendRequest(
                '<?xml version="1.0" encoding="utf-8"?>
            <soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
              <soap:Header>
                <OfflineHeader xmlns="http://www.babalumra.com/BAU">
                  <EA>' . $this->offline_headers['EA'] . '</EA>
                  <UO>' . $this->offline_headers['UO'] . '</UO>
                  <Ticket>' . $this->offline_headers['Ticket'] . '</Ticket>
                  <UserIP>' . $this->offline_headers['UserIP'] . '</UserIP>
                  <UserVer>' . $this->offline_headers['UserVer'] . '</UserVer>
                  <UserId>' . $this->offline_headers['UserId'] . '</UserId>
                </OfflineHeader>
              </soap:Header>
              <soap:Body>
               <InsertMutamerBoardingTransaction xmlns="http://www.babalumra.com/BAU">
                    <MofaNo>' . $MutamerBoardingTransaction['MofaNo'] . '</MofaNo>
                    <ArrivalDate>' . $MutamerBoardingTransaction['ArrivalDate'] . '</ArrivalDate>
                    <DepartureDate>' . $MutamerBoardingTransaction['DepartureDate'] . '</DepartureDate>
                    <ArrivalFlightNO>' . $MutamerBoardingTransaction['ArrivalFlightNO'] . '</ArrivalFlightNO>
                    <DepFlightNO>' . $MutamerBoardingTransaction['DepFlightNO'] . '</DepFlightNO>
                    <ArrTransID>' . $MutamerBoardingTransaction['ArrTransID'] . '</ArrTransID>
                    <DepTransID>' . $MutamerBoardingTransaction['DepTransID'] . '</DepTransID>
                    <EntryPortID>' . $MutamerBoardingTransaction['EntryPortID'] . '</EntryPortID>
                    <ExitPortID>' . $MutamerBoardingTransaction['ExitPortID'] . '</ExitPortID>
                    <FromCityID>' . $MutamerBoardingTransaction['FromCityID'] . '</FromCityID>
                    <ToCityID>' . $MutamerBoardingTransaction['ToCityID'] . '</ToCityID>
                    <VisaDate>' . $MutamerBoardingTransaction['VisaDate'] . '</VisaDate>
                    <VisaDurationDays>' . $MutamerBoardingTransaction['MofaNo'] . '</VisaDurationDays>
                    <VisaNo>' . $MutamerBoardingTransaction['VisaNo'] . '</VisaNo>
                    <language>' . $this->language . '</language>
                </InsertMutamerBoardingTransaction>
              </soap:Body>
            </soap:Envelope>'
        );

        return $this->Translate($response);
    }

    /**
     * GetLookupVersion
     */
    public function getLookupVersion() {
        $this->action = 'http://www.babalumra.com/BAU/getLookupVersion';
        $response = $this->sendRequest(
                '<?xml version="1.0" encoding="utf-8"?>
            <soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
              <soap:Header>
                <OfflineHeader xmlns="http://www.babalumra.com/BAU">
                  <EA>' . $this->offline_headers['EA'] . '</EA>
                  <UO>' . $this->offline_headers['UO'] . '</UO>
                  <Ticket>' . $this->offline_headers['Ticket'] . '</Ticket>
                  <UserIP>' . $this->offline_headers['UserIP'] . '</UserIP>
                  <UserVer>' . $this->offline_headers['UserVer'] . '</UserVer>
                  <UserId>' . $this->offline_headers['UserId'] . '</UserId>
                </OfflineHeader>
              </soap:Header>
              <soap:Body>
                  <getLookupVersion xmlns="http://www.babalumra.com/BAU" />
              </soap:Body>
            </soap:Envelope>'
        );

        return $this->Translate($response);
    }

    /**
     * GetURL
     */
    public function getURL() {
        $this->action = 'http://www.babalumra.com/BAU/getURL';
        $response = $this->sendRequest(
                '<?xml version="1.0" encoding="utf-8"?>
            <soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
              <soap:Header>
                <OfflineHeader xmlns="http://www.babalumra.com/BAU">
                  <EA>' . $this->offline_headers['EA'] . '</EA>
                  <UO>' . $this->offline_headers['UO'] . '</UO>
                  <Ticket>' . $this->offline_headers['Ticket'] . '</Ticket>
                  <UserIP>' . $this->offline_headers['UserIP'] . '</UserIP>
                  <UserVer>' . $this->offline_headers['UserVer'] . '</UserVer>
                  <UserId>' . $this->offline_headers['UserId'] . '</UserId>
                </OfflineHeader>
              </soap:Header>
              <soap:Body>
                  <getURL xmlns="http://www.babalumra.com/BAU" />
              </soap:Body>
            </soap:Envelope>'
        );

        return $this->Translate($response);
    }

    /**
     * sendRequest 
     * it takes the request and return the web service response
     * @param type $xml_request
     * @return string
     */
    protected function sendRequest($xml_request = NULL) {
        $header = array(
            "Content-type: text/xml;charset=\"utf-8\"",
            "Accept: text/xml",
            "Cache-Control: no-cache",
            "Pragma: no-cache",
            "SOAPAction: \"" . $this->action . "\"",
            "Content-length: " . strlen($xml_request),
        );

        $soap_do = curl_init();
        curl_setopt($soap_do, CURLOPT_URL, $this->url);
        curl_setopt($soap_do, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($soap_do, CURLOPT_POST, true);
        curl_setopt($soap_do, CURLOPT_POSTFIELDS, $xml_request);
        curl_setopt($soap_do, CURLOPT_HTTPHEADER, $header);
        $result = curl_exec($soap_do);
        if ($result === false) {
            $result = 'Curl error: ' . curl_error($soap_do);
            curl_close($soap_do);
        } else {
            curl_close($soap_do);
        }
        if (self::DEBUG) {
            echo "<pre>";
//               print_r(htmlentities(debug_print_backtrace()));
            echo "--------------------- XML REQUEST ---------------------\n";
            echo ($xml_request);
            echo "\n--------------------- XML REQUEST ---------------------\n\n";

//               echo "--------------------- REQUEST ARRAY ---------------------\n";
//               $xml = $this->Translate($xml_request);
//               print_r($xml);
//               echo "\n--------------------- REQUEST ARRAY ---------------------\n\n";

            echo "--------------------- XML RESPONSE ---------------------\n";
            echo htmlentities($result);
            echo "\n--------------------- XML RESPONSE ---------------------\n";
            echo "\n--------------------- RESPONSE ARRAY ---------------------\n";
            $xml = $this->Translate($result);
            print_r($xml);
            echo "\n--------------------- RESPONSE ARRAY ---------------------\n";
            echo "\n******************************************************\n";
            echo "</pre>";
        }
        return $result;
    }

    private function Translate($xml) {
        $array = Xmltoarray::createArray($xml);
        return ($array);
    }

    private function convert_date($date, $type = 1) {
        if($type == 2)
            return date('d/m/Y', strtotime($date));
        if($type == 1)
            return date('Y-m-d', strtotime($date)).'T03:00:00.0000000+02:00';
    }

}
