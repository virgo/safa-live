<?php

/**
 * @author Muhammad El-Saeed <m.elsaeed@virgotel.com>
 * @description define flight carrier, flight_number and flight_date then run getFlight to get flight details
 * to get airports, states, equipments, resources or status run getXxxx([PARAM] FlightID)
 * For more details: check this url [welcome/flight_states]
 * 
 * Suggest Carrier of a flight:
 * works only with stored flights on our database
 * Example: $this->flight_states->suggestCarrier( int flight_number );
 *          returns array of carriers
 * 
 * Flight Color:
 *  $this->flight_states->getFlightColor( int $flight_id );
 *  it returns HEX color format
 * 
 */
class Flight_states {

    public $SL;
    public $fsdb;
    public $flight_carrier;
    public $flight_number;
    public $flight_date;
    public $flight_id;

    function __construct() {
        $this->SL = &get_instance();
        $this->SL->load->model('fs_model');
        $this->fsdb = $this->SL->load->database('flight_status', TRUE);
    }

    private function getFromDB() {
        $flight = $this->fsdb
                ->where('fs_flights.airline', $this->flight_carrier)
                ->where('fs_flights.flight', $this->flight_number)
                ->where('date', date('Y-m-d', strtotime($this->flight_date)))
                ->join('fs_status', 'fs_status_id')
                ->get('fs_flights', 1)
                ->row();
        $this->flight_id = $flight->fs_flight_id;
        return $flight;
    }

    private function trip_details($date, $carrier, $flight) {
        list($year, $month, $day) = explode('-', $this->flight_date);
        $url = "https://api.flightstats.com/flex/flightstatus/rest/v2/json/flight/status/$carrier/$flight/arr/$year/$month/$day?appId=" . FS_APPID . "&appKey=" . FS_APPKEY . "&utc=true";
        return json_decode(file_get_contents($url));
    }

    private function getFromWS() {
        $trip_details = $this->trip_details($this->flight_date, $this->flight_carrier, $this->flight_number);
        if (!$trip_details)
            return FALSE;
        if (isset($trip_details)) {
            $this->SL->fs_model->request = $trip_details;
            if ($trip_details->flightStatuses && is_array($trip_details->flightStatuses) && count($trip_details->flightStatuses)) {
                $states = $trip_details->flightStatuses['0'];
                if ($states->status) {
                    return $this->SL->fs_model->save();
                }
            }
        }
        return FALSE;
    }

    public function getFlight() {
        $flight = $this->getFromDB();
        if (!$flight)
            $this->getFromWS();
        $flight = $this->getFromDB();
        return $flight;
    }

    public function get() {
        $flight['main'] = $this->getFlight();
        $flight['airlines'] = $this->getAirlines($flight['main']->flight_id);
        $flight['airports'] = $this->getAirports($flight['main']->flight_id);
        $flight['equipments'] = $this->getEquipments($flight['main']->flight_id);
        $flight['resources'] = $this->getResources($flight['main']->flight_id);
        $flight['states'] = $this->getStates($flight['main']->flight_id);
        $flight['color'] = $this->getFlightColor($flight['main']->flight_id);
    }

    public function getAirports($id = FALSE) {
        if (!$id)
            $id = $this->flight_id;
        return $this->fsdb
                        ->where('fs_flight_id', $id)
                        ->get('fs_airports')
                        ->result();
    }

    public function getAirlines($id = FALSE) {
        if (!$id)
            $id = $this->flight_id;
        return $this->fsdb
                        ->where('fs_flight_id', $id)
                        ->get('fs_airlines')
                        ->result();
    }

    public function getEquipments($id = FALSE) {
        if (!$id)
            $id = $this->flight_id;
        return $this->fsdb
                        ->where('fs_flight_id', $id)
                        ->get('fs_equipments')
                        ->result();
    }

    public function getResources($id = FALSE) {
        if (!$id)
            $id = $this->flight_id;
        return $this->fsdb
                        ->where('fs_flight_id', $id)
                        ->get('fs_resources')
                        ->result();
    }

    public function getStates($id = FALSE) {
        if (!$id)
            $id = $this->flight_id;
        return $this->fsdb
                        ->where('fs_flight_id', $id)
                        ->get('fs_states')
                        ->result();
    }

    public function getStatus($id = FALSE) {
        return $this->fsdb
                        ->select('fs_status.*')
                        ->where('fs_states.fs_flight_id', $id)
                        ->join('fs_states', 'fs_states.status = fs_status.fs_status_id')
                        ->get('fs_status')
                        ->row();
    }

    public function suggestCarrier($fn = FALSE) {
        if (!$fn)
            show_404();
        $flights = $this->fsdb
                ->where('fs_flights.flight', $this->flight_number)
                ->group_by('airline')
                ->get('fs_flights')
                ->result();
        $array = array();
        foreach ($flights as $flight)
            $array[] = $flight->airline;
        return $array;
    }

    public function getFlightColor($flight_id) {
        $flight = $this->fsdb
                ->where('fs_states.fs_flight_id', $flight_id)
                ->get('fs_states')
                ->row();
        if (isset($flight->arrivalGateDelayMinutes)) {
            if ($flight->arrivalGateDelayMinutes >= 15) {
                return "FFEA00";
            } elseif ($flight->arrivalGateDelayMinutes >= 30) {
                return "FF9500";
            } elseif ($flight->arrivalGateDelayMinutes >= 45) {
                return "FF0D00";
            }
        } else {
            return "00FF11";
        }
    }

}
