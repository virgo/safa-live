<?php
$lang['wellcom_title'] = 'مرحبا . قم بادخال بيانات الدخول';
$lang['login_user_name'] = 'اسم المستخدم';
$lang['login_password'] = 'كلمة المرور';
$lang['login_rememberme'] = 'تذكرنى';
$lang['login_email'] = 'البريد الالكترونى';
$lang['login_continue'] = 'اكمل ';
$lang['login_label'] = 'دخول';
$lang['login_forget_password'] = 'نسيت كلمة المرور';
$lang['registration'] = 'التسجيل';

$lang['recover_password'] = 'من فضللك ادخل بريدك الالكترونى لاسترجاع كلمة المرور';
$lang['login_recover'] = 'استرجاع';
$lang['login_placeholder_username'] = 'أسم المستخدم';
$lang['login_placeholder_password'] = 'كلمة المرور';
$lang['login_placeholder_email'] = 'البريد الالكترونى';
$lang['login_back'] = 'رجوع';
$lang['login_forget_password_q'] = 'نسيت كلمة المرور ؟';
$lang['contracts_agent_code'] = 'كود العميل';
$lang['ver_code_instruction']='تم ارساللك بريد الكترونى يكتوى على كود قم بوضعه لاكمال العمليه';
$lang['reset_password']='اعادة كلمة السر';
$lang['code']='الكود';
$lang['login_code']= 'رقم الترخيص';
$lang['login_newpass']='كلمة المرور';
$lang['login_confpass']='تاكيد كلمة المرور';
$lang['login_incorrect_vercode']='الكود غير صالح للاستخدام';
$lang['send_mail_error']='حدث خطا ما , حاول مره اخرى';
$lang['mail_send_to']='ارسل الى';
$lang['login_resetpass']='تغير كلمة المرور';
$lang['send_email_error']='قد حدث خطا فى عملية الارسال  من فضللك حاول  لاحقا مره اخرى';
$lang['login_title_error']='حدث خطا';
$lang['global_err_invalid_email'] = 'هذا  بريد إلكتروني غير موجود لدينا';
$lang['global_check_your_email']='روح شوف ايميلك كده';
$lang['global_activation_error']='روح شوف ايميلك كده';
$lang['global_forget_subject_msg']="تغيير كلمة المرور";
$lang['global_forgot_pass_msg'] = "مرحبا،<br />
<br />
كنت طلبت مؤخرا إعادة تعيين كلمة المرور الخاصة بك في صفا.<br />
انقر هنا لتغيير كلمة المرور الخاصة بك.<br />
 <a href=\"{link}\">{link}</a><br />
 <br />
مع العلم بان بيانات الدخول كالتالي:<br />
<table>
 <tr>
<td>
{username}
</td>
<td>
 :اسم الدخول 
</td>
</tr>
</table>
مع أطيب التحيات،<br />
فريق صفا<br />
";
$lang['global_forget_password_enter_email'] = 'البريد الالكترونى';
$lang['global_check_your_email'] = 'فحص البريد الإلكتروني الخاص بك - أرسلنا إليك رسالة بريد إلكتروني مع لينك لتغيير كلمة المرور الخاصة بك.';

$lang['reset_password_time_out'] = 'إنتهى الوقت لتغير كلمة السر';
$lang['reset_password_new_password'] = ' برجاء إدخال كلمة سر جديدة ';
$lang['reset_password_confirm_password'] = ' برجاءإعادة إدخال كلمة السر';
$lang['reset_password_congrates'] = 'لقد تم تغير كلمة السر بنجاح';
$lang['reset_password_congrates_title'] = 'تم تغيير كلمة السر بنجاح';
$lang['callback_activation_error'] = "هذا الحساب تم تجميده .. يرجى التواصل مع الدعم الفني";


$lang['name_la']='الاسم (LA)';
$lang['name_ar']='الاسم (ع)';
$lang['username']='اسم المستخدم';
$lang['password']='كلمة المرور';
$lang['repeat_password']='اعادة كلمة المرور';
$lang['email']='البريد الالكتروني';
$lang['country']='الدولة';
$lang['phone']='الهاتف';
$lang['fax']='الفاكس';

$lang['username_available']='اسم المستخدم متاح';
$lang['username_not_available']='اسم المستخدم غير متاح';

$lang['contract_not_available']='لا يوجد عقد باسم المستخدم هذا و كلمة المرور';

$lang['contract_username']='اسم المستخدم الخاص بالعقد';
$lang['contract_password']='كلمة المرور الخاصة بالعقد';