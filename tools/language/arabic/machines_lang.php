<?php

$lang['machines_title'] = 'الأجهزة';
$lang['machines_code'] = 'كود';
$lang['machines_name'] = 'اسم';
$lang['machines_latest_activity'] = 'آخر نشاط';
$lang['machines_check_code'] = 'هذا الكود غير صحيح، او تم استخدامه من قبل';
