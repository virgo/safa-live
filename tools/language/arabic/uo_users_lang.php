<?php
$lang['uo_users_name_ar']='الاسم بالعربية';
$lang['uo_users_name_la']='الاسم بالانجليزية';
$lang['uo_users_add_title']='اضافة مستخدم';
$lang['uo_users_edit_title']='تعديل بيانات مستخدم';
$lang['uo_users_search_title']='بحث عن مستخدم';
$lang['uo_users_title']='مستخدمين شركات العمره';
$lang['uo_users_th_name']='الاسم';
$lang['uo_users_th_operations']='العمليات';
$lang['uo_users_th_username']='اسم المستخدم';
$lang['uo_users_th_external_agent']='اسم شركة العمره';
$lang['uo_users_username'] = "اسم المستخدم";
$lang['uo_users_password'] = " كلمة المرور";
$lang['uo_users_repeat_password']='تاكيد كلمة المرور';

