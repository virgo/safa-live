<?php

defined('BASEPATH') OR exit('No direct script access allowed');

$lang['safa_umrahgroups_name'] = 'اسم المجموعة';
$lang['safa_umrahgroups_name_la'] = 'الأسم باللاتينى';
$lang['safa_umrahgroups_creation_date'] = 'تاريخ الانشاء';
$lang['safa_umrahgroups_from_date'] = 'من';
$lang['safa_umrahgroups_to_date'] = 'الى';
$lang['safa_umrahgroups_safa_ea_id'] = 'كود شركة السياحة';
$lang['safa_umrahgroups_add_title'] = 'اضافة مجموعة';
$lang['safa_umrahgroups_edit_title'] = 'تعديل بيانات مجموعة';
$lang['safa_umrahgroups_th_name'] = 'اسم المجموعة';
$lang['safa_umrahgroups_th_name_la'] = 'اسم المجموعة اللاتينى';
$lang['safa_umrahgroups_name_exist'] = 'اسم المجموعة موجود من قبل';
$lang['safa_umrahgroups_name_invalid'] = 'خطأ فى الاسم باللاتينى';
$lang['safa_umrahgroups_title'] = 'مجموعات التأشيرات';
$lang['safa_umrahgroups_mutamer_name'] = 'اسم المعتمر';
$lang['safa_umrahgroups_passport_no'] = 'رقم الجواز';
$lang['safa_umrahgroups_no_of_mutamers'] = 'عدد المعتمرين';
$lang['show_mutamers'] = 'المعتمرين';
$lang['safa_umrahgroups_contract_id'] = 'رقم العقد';

$lang['safa_umrahgroups_status_id'] = 'الحالة';
