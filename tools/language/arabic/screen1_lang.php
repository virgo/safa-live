<?php

$lang['hotel_res_order'] = 'طلب حجز فندقي';
$lang['hotel_res'] = 'حجز الفنادق';
$lang['wizerd1'] = 'الخطوة 1';
$lang['wizerd2'] = 'الخطوة 2';
$lang['wizerd3'] = 'الخطوة 3';
$lang['malls'] = 'الوجبات';
$lang['city'] = 'المدينة';
$lang['entrydate'] = 'تاريخ الدخول';
$lang['hotel'] = 'الفندق';
$lang['mall'] = 'الوجبة';
$lang['leavingdate'] = 'تاريخ المغادرة';
$lang['roomtype'] = 'نوع الغرفة';
$lang['count'] = 'العدد';
$lang['night-num'] = 'عدد الليالي';
$lang['total'] = 'الإجمالي';
$lang['process'] = 'العمليات';
$lang['confirm'] = 'تأكيد';
$lang['back'] = 'رجوع';
$lang['next'] = 'التالي';
$lang['prevuios'] = 'السابق';
$lang['amra_co'] = 'شركة عمرة';
$lang['tourism_co'] = 'شركة سياحة';
$lang['markting_co'] = 'شركة تسويق فندقي';
$lang['company'] = 'الشركة';
$lang['res_person'] = 'الشخص المسؤول';
$lang['comment'] = 'ملاحظات';
$lang['alt_hotel'] = 'فندق بديل';
$lang['dist_haram'] = 'المسافة من الحرم';
$lang['price_from'] = 'السعر من';
$lang['to'] = 'إلى';
$lang['mitter'] = 'متراً';
$lang['haram_look'] = 'إطلالة على الحرم';
$lang['adds'] = 'إضافات';
$lang['more-binfit'] = 'ميزات إضافية';
$lang['currency'] = 'العملة';
$lang['nationalty'] = 'الجنسيات';
$lang['date'] = 'التاريخ';
$lang['ha_add'] = 'إضافة';






