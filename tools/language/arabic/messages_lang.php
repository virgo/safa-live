<?php
$lang['menu_title']='تسجيل تنبيهات النظام - عربى';
$lang['title']='الرسائل';
$lang['event']='الحدث';
$lang['message_subject']='عنوان الرسالة';
$lang['message_body']='نص الرسالة';


$lang['view_all']='رؤية الكل';

$lang['message']='الرسالة';
$lang['messages']='الرسائل';

$lang['from']='من';
$lang['to']='الى';
$lang['the_date']='التاريخ';

$lang['new_message']=  'ارسال رسالة';

$lang['delete_message']=  'حذف الرسالة';
$lang['close']=  'اغلاق';
$lang['restore_message']=  'استعادة الرسالة';
$lang['mark_as_important']='التعليم كرسالة مهمة';
$lang['mark_as_un_important']='التعليم كرسالة غير مهمة';
$lang['important_messages']='الرسائل المهمة';
$lang['deleted_messages']='الرسائل المحذوفة';

$lang['company_type']='نوع الشركة';
$lang['country']='الدولة';
$lang['company']='الشركة';
$lang['attachement']='المرفقات';
$lang['importance']='الأهمية';
$lang['send']='ارسال';

$lang['success_message']='تم ارسال الرسالة بنجاح';

$lang['message_subject_text_for_add_hotel']='طلب اضافة فندق جديد ';
$lang['message_body_text_for_add_hotel']='نص طلب الاضافة: ';
$lang['message_subject_text_for_edit_hotel']='طلب تعديل الفندق: ';
$lang['message_body_text_for_edit_hotel']='نص طلب التعديل: ';
