<?php
/*arabic*/
$lang['bulid_hotel_title']='ابنى فندقك';
$lang['bulid_hotel_edit']='تعديل فندقك';
$lang['general_info']='بيانات عامة ';
$lang['contact_official_info']='بيانات مسئولى الاتصال بالفندق ';
$lang['hotel_label']='الفندق';
$lang['city_label']='المدينة';
$lang['Company_label']='الشركة';
$lang['Company_type_label']='نوع الشركة';
$lang['Company_hotel_policy']='السياسة الخاصة بالعميل ';
$lang['Company_rooms_info']='البيانات الخاصة بالغرف المملوكة للعميل';
$lang['rooms_floor']= 'الدور';
$lang['rooms_section']='القسم';
$lang['rooms_count']='عدد الغرف';
$lang['beds_count']='عدد الاسرة';
$lang['date_from']='الفترة من ';
$lang['date_to']='الفترة  الى';
$lang['compare_dates']='التاريخ غير صحيح';
$lang['policy_fieldtype']='نوع  الحقل ';