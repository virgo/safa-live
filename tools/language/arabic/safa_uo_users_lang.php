<?php
$lang['uo_users']='مستخدمين';
$lang['uo_users_name_la']='الاسم باللغة الانجليزية';
$lang['uo_users_name_ar']='الاسم باللغة العربية';
$lang['uo_users_username']='اسم المستخدم';
$lang['uo_users_password']='كلمة المرور';
$lang['uo_users_repeat_password']='تأكيد كلمة المرور';
$lang['uo_users_safa_uo_id']='شركة العمرة';
$lang['uo_users_safa_uo_usergroup_id']='مجموعات المستخدمين';
$lang['operations']='عمليات';
$lang['add_uo_users']='اضافة مستخدم';
$lang['edit_uo_users']='تعديل  مستخدم ';
$lang['uo_users_email']='البريد الالكتروني';
$lang['uo_users_mobile']='الموبيل';
$lang['uo_users_confirm_password']='تأكيد كلمة المرور';


