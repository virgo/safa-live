<?php
$lang['read_xml_title'] = 'استيراد المعتمرين';

$lang['read_xml_file'] = 'الملف';
$lang['read_xml_upload_file'] = 'تحميل الملف';
$lang['upload_xml_browse'] = 'تصفح';
$lang['file_type'] = 'مصدر الملف';
$lang['gama'] = 'جاما';
$lang['bab_umrah'] = 'باب العمره';
$lang['road_umrah'] = 'طريق العمره';
$lang['safa_file'] = 'ملف صفا';

$lang['writeFile_type'] = 'نوع الملف المحول';
$lang['invalid_file_type'] = 'خطأ مصدر الملف';
$lang['group_name']='اسم المجموعة';