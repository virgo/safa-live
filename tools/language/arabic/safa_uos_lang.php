<?php
$lang['safa_uos_add']='Add Form';
$lang['safa_uos_name_ar']='الاسم باللغة العربيه';
$lang['safa_uos_name_la']='الاسم باللغة الانجليزيه';
$lang['safa_uos_code']='الكود';
$lang['safa_uos_phone']='التليفون';
$lang['safa_uos_mobile']='الموبايل';
$lang['safa_uos_email']='البريد الالكتروني';
$lang['safa_uos_fax']='الفاكس';
$lang['safa_uos_contract_num']='عدد العقود';
$lang['safa_uos_trips_num']='عدد الرحلات';
