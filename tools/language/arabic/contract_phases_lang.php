<?php
$lang['menu_title']='مراحل تصديق العقد';
$lang['title']='مراحل تصديق العقد';
$lang['main_data']='بيانات اساسية';
$lang['phase_documents']='اوراق المرحلة';

$lang['name_ar']='الاسم العربى';
$lang['name_la']='الاسم الانجليزى';

$lang['nationalities']='الجنسيات';
$lang['document_name_ar']='اسم الوثيقة العربى';
$lang['document_name_la']='اسم الوثيقة الانجليزى';

$lang['actions']='العمليات';

$lang['add']='اضافة';
$lang['edit']='تعديل';
$lang['delete']='حذف';
$lang['save']='حفظ';


$lang['add_title']='اضافة حالة عقد';
$lang['edit_title']='تعديل حالة عقد';
$lang['delete_title']='حذف حالة عقد';


$lang['all_nationalities']='كل الجنسيات';
$lang['arabic_nationalities']='الجنسيات العربية';
$lang['foreign_nationalities']='الجنسيات الاجنبية';

$lang['id']='الكود';
