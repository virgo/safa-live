<?php defined('BASEPATH') OR exit('No direct script access allowed');

$lang['XALIGN'] = "left";
$lang['ALIGN'] = "right";
$lang['DIR'] = "rtl";
$lang['global_reset'] = "مسح";
$lang['vehicle'] = "حافلة";
$lang['add_driver_info'] = "أضف حافلة";
$lang['global_direction'] = 'rtl';
$lang['global_lang'] = 'ar';
$lang['global_select_from_menu'] = "اختار من القائمة";
$lang['global_added_successfully']='تمت الاضافة بنجاح';
$lang['global_search'] = "تصفية";
$lang['global_submit'] = "تأكيد";
$lang['global_ok']="موافق";
$lang['global_add'] = "اضافة";
$lang['global_no_data'] = "لا يوجد بيانات";
$lang['global_select_one_record_at_least'] = "من فضلك حدد سجل واحد علي الاقل.";
$lang['global_add_new_record'] = "إضافة جديد";
$lang['global_delete_selected_records'] = "هل انت متأكد من حذف السجلات المحددة";
$lang['global_full_list'] = "القئمة الكاملة";
$lang['global_edit'] = "تعديل";
$lang['global_delete'] = "حذف";
$lang['global_close'] = "اغلاق";
$lang['global_cancel'] = "الغاء";
$lang['global_move']='نقل';
$lang['global_are_you_sure_you_want_to_delete'] = "هل انت متأكد من حذف";
$lang['global_are_you_sure_you_want_to_add']='هل انت متأكد من اضافة';
$lang['global_are_you_sure_you_want_to_activate']='هل انت متاكد من تفعيل';
$lang['global_are_you_sure_you_want_to_block']='هل انت متاكد من ايقاف';
$lang['global_back'] = "رجوع";
$lang['send_to_confirm'] = "ارسال للتاكيد";
$lang['global_browse'] = "تصفح";
$lang['global_save_request'] = "حفظ الطلب";
$lang['global_actions'] = "عمليات";
$lang['global_logout'] = 'تسجيل الخروج';
$lang['global_inbox'] = 'الرسائل';
$lang['global_settings'] = 'تعديل كلمة المرور';
$lang['global_are_you_sure_you_want_to_logout'] = 'هل انت متأكد انك تريد تسجيل الخروج؟';
$lang['global_activate'] = "تفعيل";
$lang['global_virgo'] = "فيرجو";
$lang['global_system_management'] = " الصفحة الرئيسية";
$lang['global_operations']='العمليات';
$lang['global_delete_confirm'] = "تم حذف";
$lang['global_add_confirm']='تم اضافة';
$lang['global_title'] = "صفا | ادارة شركات العمرة";
$lang['global_'] = "صفا | ";
$lang['global_added_message'] = 'تمت الإضافة بنجاح';
$lang['global_updated_message'] = 'تم التعديل بنجاح';
$lang['global_deleted_message'] = 'تم الحذف بنجاح ';
$lang['global_cut_message'] = 'تم النقل بنجاح';
$lang['global_system_message'] = 'رسالة النظام';
$lang['global_error_save'] = 'خطأ لم يتم الحفظ';
$lang['global_yes'] = 'نعم';
$lang['global_no'] = 'لا';
$lang['global_info_not_true']= 'كلمة المرور المستخدمة غير صحيحة';
$lang['global_updated_successfully']= 'تم تعديل البيانات بنجاح';
$lang['global_trip_details']= 'تفاصيل الرحلة';
$lang['global_all']='الكل';
################### Menu 
$lang['menu_main_data']='البيانات الرئيسية';
$lang['menu_management_users_virgo']='ادارة مستخدمى فيرجو';
$lang['menu_management_language_update']=' ادارة الترجمة ';
$lang['menu_technical_data']='البيانات التقنية';
$lang['menu_ksa_agent']='شركات العمرة';
$lang['menu_ksa_agent_privileges']='صلاحيات شركات العمرة';
$lang['menu_ksa_agent_usergroups']='مجموعات مستخدمى شركات العمرة';
$lang['menu_external_agent_privileges']='صلاحيات الوكلاء الخارجيين';
$lang['menu_external_agent_usergroups']='مجموعات مستخدمى الوكلاء الخارجيين';
$lang['menu_itos_privileges']='صلاحيات مشغلين النقل';
$lang['menu_itos_usergroups']='مجموعات مستخدمى مشغلين النقل';
$lang['menu_external_agent']='الوكلاء الخارجيين';
$lang['menu_agent_contracts']='العقود وربطها بالوكلاء';
$lang['menu_flights']='الرحلات';
$lang['menu_internal_transport']='النقل الداخلي';
$lang['menu_reports']='التقارير';
$lang['menu_movement']='الحركة';
$lang['menu_management_users']='المستخدمين';
$lang['menu_management_usersgroups']='مجموعات مستخدمين';
$lang['menu_main_cities']='المدن';
$lang['menu_main_transport_companies']='شركات النقل';
$lang['menu_main_transport_operators']='مشغلين النقل';
$lang['menu_main_transportation_outlets']='منافذ النقل';
$lang['menu_main_hole_airports']='صالات المطارات';
$lang['menu_main_ports']='المنافذ';
$lang['menu_main_sights']='المزارات';
$lang['menu_main_hotels_levels']='مستويات الفنادق';
$lang['menu_main_sub_hotels_levels']='المستويات الفرعية للفنادق';
$lang['menu_main_hotels_meals']='وجبات الفنادق';
$lang['menu_main_types_of_meals_hotels']='انواع وجبات الفنادق';
$lang['menu_main_hotels_rooms']='انواع غرف الفنادق';
$lang['menu_main_seasons']='المواسم';
$lang['menu_main_hotels_polices']='سياسات الفنادق';
$lang['menu_main_hotel_marketing_companies']='شركات التسويق الفندقى';
$lang['menu_main_types_of_currencies']='انواع العملات ';
$lang['menu_main_types_of_hotel_rooms']='انواع غرف الفنادق';
$lang['menu_main_types_of_vehcles']='انواع المركبات ';
$lang['menu_main_hotels']='الفنادق';
$lang['menu_main_hotel_attributes']='الصفات والمزايا للفنادق';
$lang['menu_main_rooms_attributes']='الصفات والمزايا للغرف';
$lang['menu_techn_flight_cases']='حالات الرحلة';
$lang['menu_techn_types_of_transport']='انواع النقل';
$lang['menu_techn_types_of_internal_transport_clips']='انواع مقاطع النقل الداخلي';
$lang['menu_techn_cases_of_transport_clips']='حالات مقاطع النقل';
$lang['menu_techn_cases_is_internal_transport']='حالات امر النقل الداخلي';
$lang['menu_techn_users_Permissions_virgo']='صلاحيات مستخدمين فيرجو';
$lang['menu_techn_external_agentUsers_permissions']='صلاحيات مستخدمين الوكيل الخارجي';
$lang['menu_techn_ksa_agent_permissions']='صلاحيات مستخدمين شركات العمرة';
$lang['menu_ksa_agent_users']='مستخدمين شركات العمرة';
$lang['menu_external_agent_Users']='المستخدمين';
$lang['menu_flights_Add']='اضافة رحلة';
$lang['menu_flights_Search']='بحث عن رحلة';
$lang['menu_internal_transport_Add']='اضافة امر تشغيل';
$lang['menu_internal_transport_Search']='بحث عن امر تشغيل';
$lang['menu_reports_arrival']='تقرير الوصول';
$lang['menu_reports_Departure']='المغادرة';
$lang['menu_movement_movements']='التحركات';
$lang['menu_internal_transport_orders']='طلبات النقل الداخلي';
$lang['current_status']='الوضع الحالي';
$lang['menu_safa_itos']='مشغلين النقل';
$lang['menu_safa_itos_users']='مستخدمين مشغلين النقل';
$lang['menu_transport_companies_users']='مستخدمين شركات النقل';
$lang['menu_transport_companies_info']='بيانات شركات النقل';
$lang['menu_transport_companies_drivers']='سائقين شركات النقل';
$lang['menu_transport_companies_buses']='حافلات شركات النقل';

$lang['global_user_personal_settings']='تعديل بيانات';
$lang['menu_main_rooms_services']='اضافات الغرف';
$lang['menu_hotels']='الفنادق';
$lang['menu_search_hotels']='بحث الفنادق';

$lang['menu_hotels_reservation_orders']='طلبات الحجز';
$lang['menu_incoming_hotels_reservation_orders']='طلبات الحجز الوارده';

$lang['menu_hotels_availabilty_offers']='اعدادات عروض الحجز';
$lang['menu_hotel_availabilty']='الاتاحة الفندقية';
$lang['menu_hotel_availabilty_calendar']='مخطط الاتاحة الفندقية';

$lang['menu_notifications']='التنبيهات';
$lang['menu_add_notifications_data']='نصوص التنبيهات';
$lang['menu_add_notifications_translation']='ترجمة التنبيهات';


####### EAS #####
$lang['menu_management_users_eas']='ادارة النظام';
$lang['menu_contracts_eas']='العقود';
$lang['menu_contracts_report']="تقارير عقود الوكلاء";

$lang['menu_contract_phases']='مراحل تصديق العقود';
$lang['menu_management_users_add']='اضافة مستخدم';
$lang['menu_management_users_search']='بحث عن مستخدم';
$lang['menu_management_usergroups']='مجموعات المستخدمين';
$lang['menu_contracts_lists']='قائمة العقود';
$lang['menu_contracts_list_add']='اضافة عقد';
$lang['menu_trips_eas']='الرحلات';
$lang['menu_trips_lists']='قائمة الرحلات';
$lang['menu_trip_add']='اضافة رحلة';
$lang['menu_trip_search']='بحث عن رحلة';
$lang['menu_trips_programs']='برامج الرحلات';
$lang['menu_trips_programs_lists']='قائمة البرامج';
$lang['menu_trips_programs_add']='اضافة برنامج رحلة';
$lang['menu_seasons']='المواسم';
$lang['menu_seasons_search']='بحث عن موسم';
$lang['menu_seasons_add']='اضافة موسم';
$lang['menu_internal_transportation_eas']='النقل الداخلي';
$lang['menu_internal_transportation_add']='اضافة امر تشغيل';
$lang['menu_internal_transportation_search']='بحث عن امر تشغيل';
$lang['menu_internal_transportation_order']='أمر التشغبل';

$lang['menu_safa_umrah_and_mutamers']='مجموعات المعتمرين';
$lang['menu_safa_groups']='مجموعات المعتمرين';
$lang['menu_safa_umrah']='مجموعات التأشيرات';
$lang['menu_mutamers_import']='استيراد المعتمرين';


######## UOS ######
$lang['menu_management_users_uos']='ادارة النظام';
$lang['menu_management_uos_users']='المستخدمين';
$lang['menu_management_uos_usergroup']='مجموعات المستخدمين';
$lang['menu_contracts_uos']='الوكلاء';
$lang['menu_uos_contracts_add']='اضافة وكيل';
$lang['menu_uos_contracts_search']='بحث عن وكيل';
$lang['menu_uos_contracts_hotel']='تحديد الفنادق';
$lang['menu_uos_contracts_transportation']='تحديد شركات النقل';
$lang['menu_trips_uos']='الرحلات';
$lang['menu_uos_trips_search']='بحث عن رحلة';
$lang['menu_uos_trips_add']='اضافة رحلة';
$lang['menu_trips_programs_uos']='برامج الرحلات';
$lang['menu_uos_trips_programs_list']='قائمة البرامج';
$lang['menu_uos_trips_programs_add']='اضافة برنامج رحلة';
$lang['menu_uos_reports']='التقارير';
$lang['menu_uos_current_status']='الوضع الحالى';
$lang['menu_uos_arrival_report']='تقرير الوصول';
$lang['menu_uos_departure_report']='تقرير المغادرة';
$lang['menu_uos_movements']='التحركات';
$lang['menu_uos_internal_transportation']='النقل الداخلى';
$lang['menu_internal_transportation_add']='اضافة امر تشغيل';
$lang['menu_internal_transportation_search']='بحث عن امر تشغيل';
##########################
#### ITO Menu
$lang['menu_ito_arriving_report']='تقرير الوصول';
$lang['menu_ito_departing_report']='تقرير المغادرة';
$lang['menu_ito_movements']='كل التحركات';
$lang['menu_ito_movementsv']='التحركات بدون بيانات';
$lang['menu_ito_currently_report']='الوضع الحالى';
$lang['menu_internal_transportation_order']='بحث عن امر تشغيل';
$lang['menu_internal_transportation']='النقل الداخلى';

$lang['global_arabic_name']='الأسم باللغة العربية';
$lang['global_english_name']='الأسم باللغة الانجليزية';


$lang['trips'] = "الرحلات";
$lang['global_this_page_is_under_construction'] = "هذة الصفحة قيد الإنشاء";
$lang[''] = "";



// CALLBACKS
$lang['callback_incorrect_data'] = "اسم المستخدم او كلمة المرور غير صحيحة";
$lang['global_this_username_exists']='اسم المستخدم موجود';

//error message permission
$lang['global_you_dont_have_permission']='ليس لديك تصريح دخول';

