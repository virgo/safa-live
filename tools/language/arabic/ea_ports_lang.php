<?php

$lang['ea_ports_add_title'] = 'اضافة مطار';
$lang['ea_ports_edit_title'] = 'تعديل مطار';
$lang['ea_ports_erp_port_id'] = 'المنافذ';
$lang['ea_ports_name_ar'] = 'الاسم باللغة العربية';
$lang['ea_ports_name_la'] = 'الاسم باللغة الانجليزية';
$lang['ea_ports_code'] = 'الكود الدولى';
$lang['ea_ports_country_code'] = 'كود الدولة';
$lang['ea_ports_world_area_code'] = 'كود المنطقة';
$lang['ea_ports_city_id'] = 'اسم المدينة';
$lang['ea_ports_country_name'] = 'اسم الدولة';
$lang['ea_ports_th_city_name'] = 'اسم المدينة';
$lang['ea_ports_th_country_name'] = 'اسم الدولة';
$lang['ea_ports_th_name'] = 'اسم المطار';
$lang['ea_ports_name_exist'] = 'اسم المطار موجود من قبل ';
$lang['ea_ports_select_from_menu_no_cities'] = 'لايوجد مدن فى البلد المختارة';
$lang['ea_ports_select_from_menu_false'] = "الحقل المدينة مطلوب";
