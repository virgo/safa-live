<?php defined('BASEPATH') OR exit('No direct script access allowed');
$lang['XALIGN'] = "left";
$lang['ALIGN'] = "right";
$lang['DIR'] = "rtl";


$lang['curent_report'] = 'الوضع الحالى';
$lang['arriving_report'] = 'تقرير الوصول';
$lang['departing_report'] = 'تقرير المغادرة';
$lang['all_movements_report'] = 'جميع التحركات';
$lang['uo_companies'] = "شركات العمرة";
$lang['ito_companies'] = "شركات النقل";


$lang['from'] = "من";
$lang['to'] = "إلى";
$lang['search'] = "بحث";
$lang['date'] = "التاريخ";
$lang['time'] = "الوقت";
$lang['count'] = "العدد";
$lang['contract'] = "الوكيل";
$lang['trans_company'] = "شركة النقل";
$lang['nationality'] = "الجنسية";
$lang['port'] = "المنفذ";
$lang['city'] = "المدينة";
$lang['hotel'] = "الفندق";
$lang['choose_cols'] = "الحقول";
$lang['trip'] = "الرحلة";
$lang['trip_id'] = "رقم الرحلة";
$lang['dep_date'] = "تاريخ المغادرة";
$lang['dep_time'] = "وقت المغادرة";
$lang['arr_date'] = "تاريخ الوصول";
$lang['arr_time'] = "وقت الوصول";
$lang['supervisor'] = "المشرف";
$lang['departing_form_name'] = "تقرير المغادرة";
$lang['arriving_form_name'] = "تقرير الوصول";
$lang['today'] = "رحلات اليوم";
$lang['twm'] = "رحلات الغد";
$lang['uo_user'] = "المندوب ";

//all movements
$lang['all_movements'] = "جميع التحركات";
$lang['operator'] = "المشغل";
$lang['operation_order_id'] = "أمر التشغيل";
$lang['segment_type'] = "الحركة";
$lang['starting'] = "التاريخ";
$lang['ending'] = "النهاية";
$lang['start_hotel'] = "فندق البداية";
$lang['end_hotel'] = "فندق النهاية";
$lang['tourism_place'] = "المزار";
$lang['seats'] = "المقاعد";
$lang['from_hotel'] = "من الفندق";
$lang['to_hotel'] = "إلى الفندق";
$lang['from_city'] = "من مدينة";
$lang['to_city'] = "إلى مدينة";

$lang['print'] = 'طباعة';
$lang['export'] = 'تصدير';

$lang['status'] = 'الحالة';
$lang['notes'] = 'ملاحظات';

$lang['agent'] = 'مندوب';
$lang['uo_user_agent_notexist'] = 'غير مسند';
$lang['assign_uo_user'] = 'اسناد';
$lang['change_uo_user'] = 'تغير';
$lang['roll_back_user'] = 'الغاء';

$lang['drivers']='السائقين';
$lang['no_drivers']='لايوجد سائقين';
$lang['driver_title']='بيانات السائقين';
$lang['driver_name']='اسم السائق';
$lang['driver_phone']='الهاتف';
$lang['driver_vehicle_number']='رقم السيارة';
$lang['driver_vehicle_type']='نوع السيارة';
$lang['update_segment_status']='تعديل الحالة';
$lang['driver_info']='بيانات السائق';
$lang['driver_name_invalid']='الحقل {field} يجب ان يحتوى فقط على حروف وارقام ';
$lang['driver_vehicle_number_invalid']='الحقل {field} يجب ان يحتوى فقط على حروف وارقام ';
$lang['driver_vehicle_type_invalid']='الحقل {field} يجب ان يحتوى فقط على حروف وارقام ';

$lang['drivers_and_buses']='السائقين و الحافلات';
$lang['driver'] = 'السائق';
$lang['bus'] = 'الحافلة';
$lang['driver_data'] = 'بيانات السائق';
$lang['bus_data'] = 'بيانات الحافلة';
$lang['actions'] = 'العمليات';