<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['hotels_name_ar'] = "الاسم باللغة العربية";
$lang['hotels_name_la'] = "الاسم باللغة الانجليزية";
$lang['hotels_erp_city_id'] = "المدينة";
$lang['hotels_erp_country_id'] = "الدولة";
$lang['hotels_erp_hotellevel_id']='مستوى الفندق';
$lang['hotels_address_ar'] = "العنوان باللغة العربية";
$lang['hotels_address_la'] = "العنوان باللغة الانجليزية";
$lang['hotels_album'] = "الألبوم";
$lang['hotels_add_title']='اضافة فندق';
$lang['hotels_edit_title']='تعديل بيانات فندق';
$lang['hotels_th_hotellevel']='مستوى الفندق';
$lang['hotel_sublevel']='المستوى الفرعى ';
$lang['hotels_th_city']='اسم المدينة';
$lang['hotels_name_exist']='الاسم محجوز';
$lang['hotels_select_from_menu_cities'] = "اختار بلد اولا";
$lang['hotels_select_from_menu_false'] = "حقل المدينة مطلوب";
$lang['hotels_select_from_menu_no_cities']='لايوجد مدن فى البلد المختارة';
$lang['show_details'] = "التفاصيل";

$lang['map_distance'] = "البعد عن الحرم (م)";
$lang['floors_count'] = "عدد الأدوار";
$lang['ROOM_COUNT'] = "عدد الغرف";
$lang['BED_COUNT'] = "عدد الأسرة";
$lang['RESIDENCE_COUNT'] = "متوسط التسكين";
$lang['phone'] = "الهاتف";
$lang['fax'] = "الفاكس";
$lang['email'] = "الايميل";
$lang['advantage'] = "المزايا";
$lang['hotel_policy'] = "سياية الفندق ";
$lang['Policy']='السياسة ';
$lang['policy_description']='الوصف';
$lang['hotels_title']='الفنادق';

$lang['GOOGLE_MAP'] = "العنوان على الخريطة";
$lang['step1_title'] = "البيانات الرئيسية / الملاحظات";
$lang['form1_title'] = "البيانات الرئيسية";
$lang['step2_title'] = "بيانات الاتصال / بينات مسؤول الاتصال";
$lang['form2_title']='بينات الاتصال';
$lang['official_work']='الوظيفة ';
$lang['official_name']='الاسم ';
$lang['contact_official_info']='بينات مسؤول الاتصال';
$lang['official_email']='البريد الالكترونى ';
$lang['official_phonetype']='نوع الهاتف ';
$lang['official_phone']='الهاتف ';
$lang['official_skype']='skype';
$lang['official_massenger']='Massanger';
$lang['step3_title']='بينات اضافية /  سياسة الفنادق ';
$lang['form3_title']='بينات اضافية ';
$lang['step4_title']='اضافة صور / اضافة فيديوهات ';
$lang['form4_title']='الصور ';
$lang['form4_title2']='الفيديوهات ';
$lang['add_pictures']='اضافة صور';
$lang['add_vedios']='اضافة فيديو';
$lang['step5_title']='خريطة الفندق';
$lang['longtitude']='خطوط الطول ';
$lang['latitude']='خطوط العرض';
$lang['time_bycar']='الزمن بالسيارة ';
$lang['time_walk']='الزمن سيرا';
$lang['remarks']='ملاحظات';

$lang['rooms_count_for_disabled']='عدد الغرف للمعاقين';
$lang['flats_count']='عدد الشقق';
$lang['suites_count']='عدد الاجنحة';
$lang['lifts_count']='عدد المصاعد';
$lang['hotel_capacity']='سعة الفندق';
$lang['hotel_creation_date']='تاريخ  انشاء الفندق ';
$lang['website']='الموقع الالكترونى';
$lang['geoarea']='المنطقة الجغرافية ';
$lang['street']='الشارع';
$lang['hotel_languages']='لغات التحدث';
$lang['readmore']='اقرا المزيد';


$lang['hotel_details']='تفاصيل الفندق';

