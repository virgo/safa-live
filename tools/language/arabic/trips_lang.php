<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['add_trip'] = "إنشاء الرحلة";
$lang['step_1'] = "الخطوة 1";
$lang['step_2'] = "الخطوة 2";
$lang['step_3'] = "الخطوة 3";
$lang['step_4'] = "الخطوة 4";

$lang['season_and_ekd'] = "الموسم والعقد";
$lang['name'] = "اسم البرنامج / الرحلة";
$lang['the_date'] = "تاريخ الرحلة";
$lang['safa_ea_season_id'] = "الموسم";
$lang['safa_ea_id'] = "الوكيل";
$lang['safa_ea_package_id'] = "برنامج الوكيل";
$lang['special_package'] = "برنامج خاص";
$lang['moushrif_name'] = "اسم المشرف";
$lang['phone'] = "رقم الهاتف";
$lang['notes'] = "ملاحظات";
$lang['safa_trip_confirm_id'] = "التأكيد";
$lang['out_transport'] = "النقل الخارجى";

$lang['safa_tripstatus_id'] = "الحالة";
$lang['erp_transportertype_id'] = "نوع النقل";
$lang['going_trip'] = "رحلة الذهاب";
$lang['return_trip'] = "رحلة العودة";
$lang['trip_no'] = "رقم الرحلة";
$lang['airport_start'] = "ميناء الاقلاع";
$lang['airport_date_start'] = "تاريخ و وقت الاقلاع";
$lang['airport_end'] = "ميناء الوصول";
$lang['airport_date_end'] = "تاريخ و وقت الوصول";
$lang['add'] = "اضافة";
$lang['hotels_and_tourism_places'] = "الفنادق و المزارات";
$lang['hotels'] = "الفنادق";
$lang['tourism_places'] = "المزارات";
$lang['city'] = "المدينة";
$lang['hotel'] = "الفندق";
$lang['tourism_place'] = "المزار";
$lang['nights_count'] = "عدد الليالي";
$lang['meal'] = "الوجبة";
$lang['rooms'] = "الغرف";

$lang['odd_room_count'] = "عدد الغرف الفردية";
$lang['even_room_count'] = "عدد الغرف الزوجية";
$lang['checkin_datetime'] = "تاريخ الدخول";
$lang['checkout_datetime'] = "تاريخ الخروج";


$lang['visit_date_and_time'] = "تاريخ و وقت الزيارة";
$lang['internal_transport'] = "النقل الداخلي";

$lang['safa_internalpassagetype_id'] = "نوع المقطع";
$lang['internalpassage_start_datetime'] = "تاريخ ووقت البداية";
$lang['internalpassage_end_datetime'] = "تاريخ ووقت النهاية";
$lang['internalpassage_start_hotel'] = "نقطة البداية";
$lang['internalpassage_end_hotel'] = "نقطة النهاية";

$lang['safa_internalpassagestatus_id'] = "الحالة";
$lang['seats_count'] = "عدد المقاعد";




$lang['save'] = "حفظ";
$lang['delete'] = "حذف";
$lang['edit'] = "تعديل";
$lang['search'] = "بحث";

$lang['trips'] = "الرحلات";
$lang['trip'] = "الرحلة";


$lang['travellers_count'] = "عدد المعتمرين";

$lang['didnot_start'] = "لم تبدأ بعد";
$lang['in_progress'] = "جارى التنفيذ";
$lang['finished'] = "تمت";

$lang['confirm'] = "تأكيد";
$lang['cancel_confirm'] = "الغاء تأكيد";

$lang['trip_code'] = "رقم الرحلة";
$lang['airport_departure_datetime'] = "تاريخ الإقلاع";
$lang['airport_arrival_datetime'] = "تاريخ الوصول";

$lang['externalsegmenttype'] = "نوع النقل";
$lang['transporter'] = " شركة النقل";
$lang['room_count'] = "عدد الغرف";
$lang['travellers_adult_count'] = "عدد البالغين";
$lang['travellers_child_count'] = "عدد الاطفال";
$lang['travellers_infant_count'] = "عدد الرضع";
$lang['erp_country_id'] = "الدولة";

#################################################
#### Trip details ###############################
$lang['last_days_trip_details']='الرحلات: تفاصيل رحلة العشر الأواخر';
$lang['sesson']='الموسم';
$lang['contrcat']='الوكيل';
$lang['trip_status']='حالة الرحلة';
$lang['mutamers_info']='بيانات المعتمرين';
$lang['supervisor']='المشرفين';
$lang['external_transportation']='النقل الخارجى';
$lang['hotels']='الفنادق';
$lang['internal_transportation']='النقل الداخلي';
$lang['table']='جدول';
$lang['update_show']='تعديل العرض';
$lang['passport_no']='رقم الجواز';
$lang['birth_date']='تاريخ الميلاد';
$lang['mutamer']='المعتمر';
$lang['visa_number']='رقم التأشيرة';
$lang['nationality']='الجنسية';
$lang['sex']='النوع';
$lang['time']='الوقت';
$lang['date']='التاريخ';
$lang['numbers']='العدد';
$lang['agent']='الوكيل';
$lang['egyption']='مصرى';
$lang['male']='ذكر';
$lang['different']='متخلف';
$lang['transport_to_anothor_trip']='نقل لرحلة أخرى';
$lang['supervisor_name']='اسم المشرف';
$lang['phone_number']='رقم الهاتف';
$lang['notes']='ملاحظات';
$lang['gone_trip']='رحلة الذهاب';
$lang['trip_no']='رقم الرحلة';
$lang['departure_place']='ميناء الإقلاع';
$lang['date_time_departure']='تاريخ ووقت الإقلاع';
$lang['arrival_place']='ميناء الوصول';
$lang['date_time_arrival']='تاريخ ووقت الوصول';
$lang['back_trip']='رحلة العودة';
$lang['cairo']='القاهرة';
$lang['macca']='مكة';
$lang['ramdan']='رمضان';
$lang['agent_s']='الوكيل س';
$lang['not_start']='لم تبدأ بعد';
$lang['trip_internaltrip'] = "أمر التشغيل الداخلي";
$lang['trip_ito'] = "مشغل النقل الداخلي";
$lang['generate_trip_internaltrip'] = "توليد امر تشغيل";
$lang['please_update_your_internal_trip'] = 'يرجي تعديل امر التشغيل لكي يتزامن مع تعديلات الرحلة';
