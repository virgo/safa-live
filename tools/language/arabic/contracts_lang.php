<?php
$lang['contracts_add']='اضافة وكيل';
$lang['contracts_edit']='تعديل وكيل';
$lang['contracts_name_la']='الاسم (LA)';
$lang['contracts_name_ar']='الاسم (ع)';
$lang['contracts_exrenalagent']='شركة السياحة ';
$lang['contracts_name']='اسم الوكيل';
$lang['uo_contracts_title']='الوكلاء';


$lang['contracts_username']='اسم المستخدم';
$lang['contracts_password']='كلمة المرور';
$lang['contracts_repeat_password']='اعادة كلمة المرور';
$lang['contracts_email']='البريد الالكتروني';
$lang['contracts_country']='الدولة';
$lang['contracts_city']='المدينة';
$lang['contracts_files']='ملفات العقد';

$lang['contracts_address']='العنوان';
$lang['contracts_phone']='الهاتف';
$lang['contracts_fax']='الفاكس';
$lang['contracts_nationality']='الجنسية';
$lang['username_validation']='اسم المستخدم غير صحيح';
$lang['contracts_ksaaddress']='عنوان السعودية';
$lang['contracts_ksaphone']='هاتف السعودية';
$lang['contracts_agencyname']='اسم الوكالة';
$lang['contracts_agencysymbol']='رمز الوكالة';
$lang['contracts_ayata_num']='رقم الاياتا';
$lang['contracts_resposible_name']='اسم المسؤول';
$lang['contracts_resposible_phone']='هاتف المسؤول';
$lang['contracts_resposible_email']='البريد الالكترونى للمسؤول';
$lang['contracts_resposible_passport_photo_path']='صورة الجواز';
$lang['choose_photo']='اختر صورة';

$lang['contract_notes']='الملاحظات';

$lang['contracts_services_can_by_inside']=' الخدمات المتاح شراؤها من شركة العمرة';
$lang['contracts_visa']=' تأشيرة ';


$lang['contracts_services_can_by_outside']=' الخدمات المتاح شراؤها من جهات خارجية';
$lang['contracts_other_services']=' خدمات اخرى';
$lang['contracts_hostel']=' سكن';
$lang['contracts_travel']=' نقل';

$lang['contracts_date_from']=' من تاريخ';
$lang['contracts_date_to']=' إلى تاريخ';

$lang['contracts_transport_cycle_from_uo']=' امكانية طلب دورة النقل من شركة العمرة';
$lang['contracts_transport_cycle_from_others']=' امكانية طلب دورة النقل من شركات النقل';
$lang['contracts_transporters_us_all']=' امكانية التعامل مع جميع شركات النقل';
$lang['contracts_transporters_us_selected']=' التعامل مع شركات نقل محددة';

$lang['contracts_other_uo_company']=' اتاحة التعامل مع شركات عمرة خارجيين';
$lang['contracts_hotel_markting']=' اتاحة التعامل مع شركات تسويق فندقى';
$lang['contracts_book_couch']=' امكانية حجز أسرَة';
$lang['contracts_visa_price']=' سعر التأشيرة';
$lang['contracts_currency']=' العملة';
$lang['contracts_max_limit_visa']=' الحد الأقصى لعدد التأشيرات المسموح به في الرحلة';
$lang['contracts_credit_balance']=' رصيد دائن مبدائى';
$lang['contracts_withdrowal']=' المسحوب';
$lang['contracts_rest']=' الباقي';
$lang['contracts_withdrowal_report']=' كشف تفصيلي';
$lang['contracts_can_over_limit']=' امكانية تجاوز قيمة الرصيد الدائن عند الشراء';
$lang['contract_have_to_check_one']=' يجب اختيار خدمة علي الاقل';

$lang['contract_step1']='المعلومات الاساسية';
$lang['contract_step2']=' بيانات الشركة';
$lang['contract_step3']=' بيانات المدير';
$lang['contract_step4']=' اعدادات العقد';
$lang['contract_step4_w1']=' اعدادات عامة';
$lang['contract_step4_w2']=' اعدادات دورات النقل';
$lang['contract_step4_w3']=' اعدادات الحجز الفندقى';
$lang['contract_step4_w4']=' اعدادات مالية';
$lang['contract_save']='حفظ';
$lang['uo_contract']='شركة العمرة';
$lang['contracts_unique_username']='يجب ان يحتوى على اسم غير مسجل';
$lang['companies_contracts']='وكلاء شركة العمرة';
$lang['node_title']='الوكلاء';
$lang['contracts_ito_name']='مشغلين النقل';
$lang['contracts_hotels_name']='الفنادق';
$lang['contracts_hotels_arabic_name']='الاسم باللغة العربية';
$lang['contracts_hotels_english_name']='الاسم باللغة الانجليزية';
$lang['contracts_hotels_city']='المدينة';
$lang['contracts_hotels_hotel_level']='مستوى الفندق';

$lang['connect_contract']='ربط عقد';
$lang['itos']='بمشغلين النقل';
$lang['uo_contracts_itos_title']='ربط العقود بمشغلين النقل';

$lang['ito_exist']='مشغل النقل مضاف بالفعل';
$lang['add_contracts_ito_successfully']='تم اضافة مشغل النقل للعقد';

$lang['delete_contracts_ito_successfully']='تم حذف مشغل النقل';
$lang['contract_itos']='مشغلين النقل';
$lang['contract_hotels']='الفنادق';
$lang['hotels']='بالفنادق';


$lang['safa_uo_contracts_status_id']='حالة العقد';
$lang['safa_contract_phases_id_current']='مرحلة العقد الحالية';
$lang['safa_contract_phases_id_next']='مرحلة العقد التالية';

$lang['contract_approving']='تصديق العقد';

$lang['view_ea_documents']='استعراض وثائق شركة السياحة';
$lang['contract_phases_history']='استعراض مراحل توثيق العقد';

$lang['new_contract_data_email_subject']='بيانات الدخول للعقد';
$lang['new_contract_data_email_body']= "مرحبا,<br />
<br />
بيانات الدخول للعقد الجديد
<br/>
بالضغط هنا تستطيع الدخول على صفحة دخول صفا لايف<br/>
 <a href=\"{link}\">{link}</a><br/>
 <br />
هذة بيانات دخول العقد:</br>
<table>
 <tr>
<td>
{username}
</td>
<td>
اسم المستخدم: 
</td>
</tr>
<tr>
<td>
{password}
</td>
<td>
كلمة المرور: 
</td>
</tr>
</table>
مع تحيات,</br>
فريق صفا</br>
";

$lang['can_over_limit_description']='عنذ اختيار هذا الاختيار سيتم السماح للوكيل بشراء اى خدمات دون النظر الى اي تجاوز لرصيده الدائن';
