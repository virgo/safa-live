<?php
$lang['safa_uo_contracts_add']='اضافة وكيل';
$lang['safa_uo_contracts_contract_username']='اسم المستخدم';
$lang['safa_uo_contracts_contract_password']='كلمة المرور';
$lang['safa_uo_contrcts_repeat_password']='تأكيد كلمة المرور';
$lang['safa_uo_contracts_safa_uo_contract_id']='الوكيل';
$lang['safa_uo_contracts_name_la']='الاسم باللغة الانجليزية';
$lang['safa_uo_contracts_name_ar']='الاسم باللغة العربية';
$lang['operations']='عمليات';
$lang['contracts_ito_name']='مشغلين النقل';
$lang['contracts_hotels_name']='الفنادق';
$lang['add_contracts_ito_successfully']='تم اضافة مشغل النقل للعقد';