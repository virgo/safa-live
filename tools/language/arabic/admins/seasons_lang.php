<?php
$lang['seasons']='المواسم';
$lang['add_season']='اضافة موسم';
$lang['edit_seasons']='تعديل المواسم';
$lang['seasons_name_ar']='الاسم باللغة العربية';
$lang['seasons_name_la']='الاسم باللغة الانجليزية';
$lang['seasons_start_date']='تاريخ البدء';
$lang['seasons_end_date']='تاريخ الانتهاء';
