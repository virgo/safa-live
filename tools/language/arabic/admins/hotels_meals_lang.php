<?php
$lang['parent_child_title']='انواع وجبات الفنادق';
$lang['hotels_meals']='انواع وجبات الفنادق';
$lang['add_hotels_meals']='اضافة وجبة للفنادق';
$lang['edit_hotels_meals']='تعديل وجبة الفنادق';
$lang['hotels_meals_name_ar']='الاسم باللغة العربية';
$lang['hotels_meals_name_la']='الاسم باللغة الانجليزية';
$lang['hotels_meals_description_ar']='الوصف باللغة العربية';
$lang['hotels_meals_description_la']='الوصف باللغة الانجليزية';
$lang['hotels_meals_code']='الكود';