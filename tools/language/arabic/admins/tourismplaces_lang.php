<?php

defined('BASEPATH') OR exit('No direct script access allowed');

$lang['tourismplaces_name_ar']='الأسم بالعربى';
$lang['tourismplaces_name_la']='الأسم باللاتينى';
$lang['tourismplaces_erp_city_id']='المدينة';
$lang['tourismplaces_erp_country_id']='البلد';
$lang['tourismplaces_add_title']='اضافة مزار';
$lang['tourismplaces_edit_title']='تعديل بيانات مزار';
$lang['tourismplaces_th_city_name']='اسم المدينة';
$lang['tourismplaces_th_operations']='العمليات';
$lang['tourismplaces_search_button']='تصفية';
$lang['tourismplaces_name_in_city_exist']='اسم المزار موجود من قبل فى المدينة المختارة';
$lang['tourismplaces_name_invalid']='خطأ فى الاسم باللاتينى';
$lang['tourismplaces_select_from_menu_cities'] = "اختار بلد اولا";
$lang['tourismplaces_select_from_menu_false'] = "حقل المدينة مطلوب";
$lang['tourismplaces_select_from_menu_no_cities']='لايوجد مدن فى البلد المختارة';