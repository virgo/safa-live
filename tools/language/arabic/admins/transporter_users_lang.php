<?php
$lang['safa_transporter_users_add']='اضافة مستخدمين شركات نقل';
$lang['safa_transporter_users_edit']='تعديل مستخدم شركة النقل';
$lang['safa_transporter_users_safa_transporter_id']='شركات النقل';
$lang['safa_transporter_users_name_ar']='الأسم باللغة العربية';
$lang['safa_transporter_users_name_la']='الأسم باللغة الانجليزية';
$lang['safa_transporter_users_username']='اسم المستخدم';
$lang['safa_transporter_users_password']='كلمة المرور';
$lang['safa_transporter_users_repeat_password']='تأكيد كلمة المرور';