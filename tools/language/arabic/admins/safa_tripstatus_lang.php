<?php
$lang['safa_tripstatus_add']='اضافة حالات الرحلة';
$lang['safa_tripstatus_title_edit']='تعديل حالة الرحلة';
$lang['safa_tripstatus_name_ar']='أسم حالة الرحلة بالعربى';
$lang['safa_tripstatus_name_la']='أسم حالة الرحلة بالاتينى';
$lang['safa_tripstatus_description_ar']='الوصف بالعربى';
$lang['safa_tripstatus_description_la']='الوصف بالاتينى';
$lang['safa_tripstatus_code']='كود البرمجة';
$lang['safa_tripstatus_operations']='عمليات';
$lang['virgo']='فيرجو';
$lang['information']='البيانات التقنية';
$lang['add']='أضافة';

