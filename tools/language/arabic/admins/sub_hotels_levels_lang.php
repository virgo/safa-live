<?php
$lang['parent_child_title']='المستويات الفرعية للفنادق';
$lang['sub_hotels_levels']='المستويات الفرعية للفنادق';
$lang['sub_hotels_levels_name_ar']='الاسم باللغة العربية';
$lang['sub_hotels_levels_name_la']='الاسم باللغة الانجليزية';
$lang['add_sub_hotels_levels']='اضافة مستوى فرعى';