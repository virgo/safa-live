<?php
$lang['safa_itos_name_ar']='الاسم بالعربى';
$lang['safa_itos_name_la']='الاسم بالاتينى';
$lang['safa_itos_erp_country_id']='الدولة';
$lang['safa_itos_reference_code']='كود المشغل';
$lang['safa_itos_add_title']='اضافة مشغلين نقل';
$lang['safa_itos_user_username']='المستخدم';
$lang['safa_itos_user_password']='كلمة المرور';
$lang['safa_itos_user_repeat_password']='تأكيد المرور';
$lang['safa_itos_edit_title']='تعديل بيانات مشغل النقل ';
