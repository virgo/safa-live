<?php

defined('BASEPATH') OR exit('No direct script access allowed');

$lang['menu_main_data']='البيانات الرئيسية';
$lang['menu_management_users_virgo']='ادارة مستخدمى فيرجو';
$lang['menu_management_users']='ادارة المستخدمين';

$lang['menu_technical_data']='البيانات التقنية';
$lang['menu_ksa_agent']='شركات العمرة';
$lang['menu_ksa_agent_privileges']='صلاحيات شركات العمرة';
$lang['menu_ksa_agent_usergroups']='مجموعات مستخدمى شركات العمرة';
$lang['menu_external_agent']='الوكلاء الخارجين';
$lang['menu_agent_contracts']='العقود وربطها بالوكلاء';
$lang['menu_flights']='الرحلات';
$lang['menu_internal_transport']='النقل الداخلي';
$lang['menu_reports']='التقارير';
$lang['menu_movement']='الحركة';
$lang['menu_management_users']='المستخدمين';
$lang['menu_management_usersgroups']='مجموعات المستخدمين';
$lang['menu_main_cities']='المدن';
$lang['menu_main_transport_companies']='شركات النقل';
$lang['menu_main_transport_operators']='مشغلين النقل';
$lang['menu_main_transportation_outlets']='منافذ النقل';
$lang['menu_main_hole_airports']='صالات المطارات';
$lang['menu_main_sights']='المزارات';
$lang['menu_main_hotels_levels']='مستويات الفنادق';
$lang['menu_main_types_of_meals_hotels']='انواع وجبات الفنادق';
$lang['menu_main_types_of_hotel_rooms']='انواع غرف الفنادق';
$lang['menu_main_hotels']='الفنادق';
$lang['menu_techn_flight_cases']='حالات الرحلة';
$lang['menu_techn_types_of_transport']='انواع النقل';
$lang['menu_techn_types_of_internal_transport_clips']='انواع مقاطع النقل الداخلي';
$lang['menu_techn_cases_of_transport_clips']='حالات مقاطع النقل';
$lang['menu_techn_cases_is_internal_transport']='حالات امر النقل الداخلي';
$lang['menu_techn_users_Permissions_virgo']='صلاحيات مستخدمين فيرجو';
$lang['menu_techn_external_agentUsers_permissions']='صلاحيات مستخدمين الوكيل الخارجى';
$lang['menu_techn_ksa_agent_permissions']='صلاحيات مستخدمين شركة العمره';
$lang['menu_ksa_agent_users']='مستخدمين شركات العمره';
$lang['menu_external_agent_Users']='مستخدمين الوكلاء الخارجين';
$lang['menu_flights_Add']='اضافة رحلة';
$lang['menu_flights_Search']='بحث عن رحلة';
$lang['menu_internal_transport_Add']='اضافة امر تشغيل';
$lang['menu_internal_transport_Search']='بحث عن امر تشغيل';
$lang['menu_reports_arrival']='تقرير الوصول';
$lang['menu_reports_Departure']='المغادرة';
$lang['menu_movement_movements']='التحركات';
$lang['menu_safa_itos']='مشغليين النقل';


//notifications
$lang['send_noti_via_email'] = "ارسال التنبيهات على البريد الالكترونى";
$lang['send_noti_via_mobile'] = "ارسال التنبيهات على الموبايل (SMS)";
$lang['email'] = "البريد الالكترونى";
$lang['mobile'] = "رقم الموبايل";

$lang['what_is_to_be'] = "ما يتم التنبية به";
$lang['all_noti'] = "كل التنبيهات";
$lang['just_noti_from'] = "فقط التنبيهات المرسلة من";
$lang['noti_sended_from_system'] = "التنبيهات المرسلة من النظام";

$lang['uo_menu'] = "قائمة شركات العمرة";
$lang['ea_menu'] = "قائمة شركات السياحة";
$lang['ito_menu'] = "قائمة شركات مشغلي النقل";
$lang['hotel_brokers_menu'] = "قائمة شركات التسويق الفندقي";