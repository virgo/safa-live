<?php
$lang['parent_child_title']='انواع غرف الفنادق';
$lang['hotels_rooms_types']='انواع غرف الفنادق';
$lang['add_hotel_room_type']='اضافة نوع غرف';
$lang['edit_hotels_rooms_types']='تعديل نوغ الغرف';
$lang['hotels_rooms_types_name_ar']='الاسم باللغة العربية';
$lang['hotels_rooms_types_name_la']='الاسم باللغة الانجليزية';
$lang['hotels_rooms_types_beds_number']='عدد اسرة الغرفه';
$lang['hotels_rooms_types_overload']='الزيادة';