<?php
$lang['hotel_levels']='مستويات الفنادق';
$lang['parent_child_title']='مستويات الفنادق';
$lang['add_hotels_levels']='اضافة مستوى فندق';
$lang['edit_hotels_levels']='تعديل مستوى فندق';
$lang['hotels_levels_name_ar']='الاسم باللغة العربية';
$lang['hotels_levels_name_la']='الاسم باللغة الانجليزية';
$lang['hotels_levels_code']='الكود';