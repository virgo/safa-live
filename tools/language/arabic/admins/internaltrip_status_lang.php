<?php

defined('BASEPATH') OR exit('No direct script access allowed');


$lang['cities_name_ar']='الأسم بالعربى';
$lang['cities_name_la']='الأسم باللاتينى';
$lang['cities_erp_countery_id']='البلد';
$lang['cities_add_title']='اضافة مدينة';
$lang['cities_edit_title']='تعديل';
$lang['cities_search_title']='بحث';
$lang['cities_title']='المدن';
$lang['cities_th_city_name']='اسم المدينة';
$lang['cities_th_country_name']='اسم الدولة';
$lang['cities_th_operations']='العمليات';
$lang['cities_search_button']='تصفية';
$lang['cities_name_exist']='اسم المدينة موجود من قبل';
$lang['cities_name_invalid']='خطأ فى الاسم باللاتينى';
$lang['cities_search_button']='تصفية';
$lang['internaltripstatus_title']='حلات اوامر النقل';
$lang['internaltripstatus_add']='اضافة حلات اوامر النقل';
$lang['internaltripstatus_edit']='تعديل حلات اوامر النقل';
$lang['internaltripstatus_name_ar']='اسم الحالة (ع)';
$lang['internaltripstatus_name_la']='اسم الحالة (LA)';
$lang['internaltripstatus_description_ar']='وصف الحالة العربى';
$lang['internaltripstatus_description_la']='وصف الحالة الاتينى';
$lang['internaltripstatus_color']='لون الحالة';
$lang['internaltripstatus_code']='كود البرمجة';
$lang['internaltripstatus_operations']='العمليات';
$lang['parent_node_title']='ادارة النظام';

$lang['color_pick']='اختر الون ';

