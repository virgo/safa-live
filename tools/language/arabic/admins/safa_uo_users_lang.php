<?php
$lang['safa_uo_users_name_ar']='الاسم بالعربية';
$lang['safa_uo_users_name_la']='الاسم بالانجليزية';

$lang['safa_uo_users_email']='البريد الالكتروني';
$lang['safa_uo_users_mobile']='الموبيل';
$lang['safa_uo_users_usersgroup']='مجموعات المستخدمين ';
$lang['safa_uo_users_add_title']='أضافة شركة عمره';
$lang['safa_uo_users_edit_title']='نموذج تعديل  ';
$lang['safa_uo_users_search_title']='نموذج بحث ';
$lang['safa_uo_users_title']='مستخدمين شركات العمره';
$lang['safa_uo_users_th_name']='الاسم';
$lang['safa_uo_users_th_operations']='العمليات';
$lang['safa_uo_users_th_username']='اسم المستخدم';
$lang['safa_uos_th_usergroup']='مجموعة المستخدمين';
$lang['safa_uo_users_th_no_of_uos']='عدد الموظفين';
$lang['safa_uo_users_user_name_ar']='أسم الموظف باللغة العربية';
$lang['safa_uo_users_user_name_la']='اسم الموظف باللغة الانجليزية';
$lang['safa_uo_users_user_username'] = "اسم المستخدم";
$lang['safa_uo_users_user_password'] =" كلمة المرور";
$lang['safa_uo_users_user_repeat_password']='تاكيد كلمة المرور';
