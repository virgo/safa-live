<?php
    $lang['currencies_id'] = 'Id';
    $lang['currencies_code'] = 'كود';
    $lang['currencies_name_ar'] = 'الاسم العربى ';
    $lang['currencies_name_la'] = 'الاسم الاتينى ';
    $lang['currencies_short_name_la']='الاسم المختصر الاتينى ';
    $lang['currencies_symbol'] = 'الرمز';
    $lang['currencies_title'] = 'العملات';
    $lang['currencies_no_rows'] = 'لا يوجد عملات حتى الان';
    $lang['currencies_short_name_ar']='الاسم المختصر العربى ';
    $lang['parent_child_title']='انواع العملات';
    $lang['currencies']='انواع العملات ';
    $lang['add_currency']='اضافة  عملة ';
    $lang['edit_currency']='تعديل عملة';
   