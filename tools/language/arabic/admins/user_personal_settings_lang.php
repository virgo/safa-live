<?php
$lang['user_personal_settings_username'] = 'اسم المستخدم';
$lang['user_personal_settings_password']='كلمة المرور الحالية';
$lang['user_personal_settings_new_password'] = 'كلمة المرور الجديدة ';
$lang['user_personal_settings_resetpassword'] = 'تأكيد كلمة المرور الجديدة';
$lang['user_personal_settings_language'] = 'اللغة';
$lang['password_error']='كلمة المرور يجب ان تحتوى على حروف وارقام ولا تقل عن 8حروف';

/* End of file user_personal_settings.php */
/* Location: ./application/languages/english/user_personal_settings.php */