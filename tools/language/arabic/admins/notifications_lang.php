<?php
$lang['menu_title']='تسجيل تنبيهات النظام - عربى';
$lang['title']='بيانات التنبيه';
$lang['event']='الحدث';
$lang['subject']='عنوان التنبيه';
$lang['body']='نص التنبيه';
$lang['notifications_add']='اضافة التنبيهات';


$lang['trip']='الرحلة';
$lang['trip_no']='رقم الرحلة';
$lang['trip_date']='تاريخ الرحلة';
$lang['the_date']='التاريخ';
$lang['trip_arrival_datetime']='تاريخ/وقت وصول الرحلة';
$lang['trip_leaving_datetime']='تاريخ/وقت مغادرة الرحلة';
$lang['madina_go_to_datetime']='تاريخ / وقت التحرك للمدينة';
$lang['makka_go_to_datetime']='تاريخ / وقت التحرك لمكة';
$lang['hotel_arrival_datetime']='تاريخ/وقت دخول الفندق ';
$lang['hotel_leaving_datetime']='تاريخ/وقت مغادرة الفندق ';

$lang['view_all']='رؤية الكل';
$lang['notifications']='التنبيهات';

$lang['message']='الرسالة';
$lang['messages']='الرسائل';

$lang['system']='النظام';
$lang['from']='من';

$lang['delete_message']=  'حذف الرسالة';
$lang['close']=  'اغلاق';

$lang['restore_message']=  'استعادة الرسالة';

$lang['mark_as_important']='التعليم كرسالة مهمة';
$lang['mark_as_un_important']='التعليم كرسالة غير مهمة';


$lang['important_messages']='الرسائل المهمة';
$lang['deleted_messages']='الرسائل المحذوفة';

$lang['event_type']='نوع التنبيه';
$lang['notification']='تنبيه';
$lang['email']='بريد الكترونى';