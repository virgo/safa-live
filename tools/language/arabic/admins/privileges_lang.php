<?php

defined('BASEPATH') OR exit('No direct script access allowed');

$lang['privileges_name_ar']='الأسم بالعربى';
$lang['privileges_name_la']='الأسم باللاتينى';
$lang['privileges_description_ar']='الوصف بالعربى';
$lang['privileges_description_la']='الوصف بالاتينى';
$lang['privileges_description']='الوصف';
$lang['privileges_erp_countery_id']='البلد';
$lang['privileges_add_title']='اضافة صلاحية';
$lang['privileges_edit_title']='تعديل بيانات صلاحية';
$lang['privileges_th_privileges_name_ar']='الاسم العربى';
$lang['privileges_th_privileges_name_la']='الاسم اللاتينى';
$lang['privileges_th_privileges_role_name']='اسم الدور';
$lang['privileges_th_privileges_description']='وصف الصلاحية';
$lang['privileges_name_exist']='اسم الصلاحية موجود من قبل';
$lang['privileges_name_invalid']='خطأ فى الاسم باللاتينى';
$lang['uoprivileges_title']='صلاحيات شركات العمره';
$lang['eaprivileges_title']='صلاحيات شركات السياحة';
$lang['itoprivileges_title']='صلاحيات شركات النقل';
$lang['privileges_role_name']='اسم الدور';

