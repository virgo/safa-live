<?php
$lang['safa_transporter_name']='اسم شركة النقل';
$lang['safa_transporter_erp_country_id']='الدولة';
$lang['safa_transporter_erp_transportertype_id']='نوع النقل';
$lang['safa_transporter_name_ar']='الاسم باللغة العربية';
$lang['safa_transporter_name_la']='الاسم باللغة الانجليزية';
$lang['safa_transporter_code']='الكود';
$lang['safa_transporters_add']='اضافة شركات نقل';
$lang['safa_transporters_edit']='تعديل بيانات شركة النقل';

$lang['drivers']='السائقين';
$lang['buses']='الحافلات';

$lang['name_ar']='الاسم باللغة العربية';
$lang['name_la']='الاسم باللغة الانجليزية';
$lang['phone']='الهاتف';

$lang['add_driver']='اضافة سائق';
$lang['edit_driver']='تعديل سائق';
$lang['main_data']='البيانات الاساسية';

$lang['add_bus']='اضافة حافلة';
$lang['edit_bus']='تعديل حافلة';

$lang['bus_no']='رقم الحافلة';
$lang['safa_buses_brands_id']='الماركة';
$lang['safa_buses_models_id']='الموديل';
$lang['industry_year']='سنة الصنع';
$lang['passengers_count']='عدد الركاب';
$lang['notes']='مواصفات عامة';