<?php
$lang['safa_itos_add']='اضافة مستخدمين مشغلين نقل';
$lang['itos_users_name_ar']='الاسم باللغة العربية';
$lang['itos_users_name_la']='الاسم باللغة الاتينية';
$lang['itos_users_username']='اسم المستخدم';
$lang['itos_users_password']='كلمة المرور';
$lang['itos_users_repeat_password']='تأكيد كلمة المرور';
$lang['itos_users_safa_ito_id']='مشغلين النقل';
$lang['itos_users_safa_ito_usergroup_id']='مجموعات مشغلين النقل';
$lang['itos_users_safa_erp_language_id']='اللغة';
$lang['itos_users_ver_code']='الكود';
$lang['itos_users_email']='البريد الالكتروني';
$lang['safa_itos_users_add_title']='اضافة مستخدم';
$lang['safa_itos_edit']='تعديل مستخدم مشغل النقل ';