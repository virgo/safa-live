<?php
$lang['menu_title']='ترجمة تنبيهات النظام';
$lang['title']='بيانات التنبيه';
$lang['event']='الحدث';
$lang['subject']='عنوان التنبيه';
$lang['body']='نص التنبيه';

$lang['subject_lang1']='عنوان التنبيه - لغة 1';
$lang['body_lang1']='نص التنبيه - لغة 1';

$lang['subject_lang2']='عنوان التنبيه - لغة 2';
$lang['body_lang2']='نص التنبيه - لغة 2';

$lang['notifications_translate']='ترجمة التنبيهات';


$lang['trip']='الرحلة';
$lang['trip_no']='رقم الرحلة';
$lang['trip_date']='تاريخ الرحلة';
$lang['the_date']='التاريخ';
$lang['trip_arrival_datetime']='تاريخ/وقت وصول الرحلة';
$lang['trip_leaving_datetime']='تاريخ/وقت مغادرة الرحلة';
$lang['madina_go_to_datetime']='تاريخ / وقت التحرك للمدينة';
$lang['makka_go_to_datetime']='تاريخ / وقت التحرك لمكة';
$lang['hotel_arrival_datetime']='تاريخ/وقت دخول الفندق';
$lang['hotel_leaving_datetime']='تاريخ/وقت مغادرة الفندق';

$lang['translate_from']='الترجمة من ';
$lang['to']='الى ';

$lang['select_from_menu']='اختر من القائمة';

$lang['language_1']='اللغة الأولى';
$lang['language_2']='اللغة الثانية';

$lang['event_type']='نوع التنبيه';
$lang['notification']='تنبيه';
$lang['email']='بريد الكترونى';