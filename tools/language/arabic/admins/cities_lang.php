<?php

defined('BASEPATH') OR exit('No direct script access allowed');

$lang['cities_name_ar']='الأسم بالعربى';
$lang['cities_name_la']='الأسم باللاتينى';
$lang['cities_erp_countery_id']='البلد';
$lang['cities_add_title']='اضافة مدينة';
$lang['cities_edit_title']='تعديل بيانات مدينة';
$lang['cities_th_city_name']='اسم المدينة';
$lang['cities_th_country_name']='اسم الدولة';
$lang['cities_name_exist']='اسم المدينة موجود من قبل';
$lang['cities_name_invalid']='خطأ فى الاسم باللاتينى';