<?php
$lang['ports_add_title']='اضافة مطار';
$lang['ports_edit_title']='تعديل مطار';
$lang['ports_erp_port_id']='المنافذ';
$lang['ports_name_ar']='الاسم باللغة العربية';
$lang['ports_name_la']='الاسم باللغة الانجليزية';
$lang['ports_code']='كود التشغيل';
$lang['ports_country_code']='كود الدولة';
$lang['ports_world_area_code']='كود المنطقة';
$lang['ports_city_id']='اسم المدينة';
$lang['ports_country_name']='اسم الدولة';
$lang['ports_th_city_name']='اسم المدينة';
$lang['ports_th_country_name']='اسم الدولة';
$lang['ports_th_name']='اسم المطار';
$lang['ports_name_exist']='اسم المطار موجود من قبل ';
$lang['ports_select_from_menu_no_cities']='لايوجد مدن فى البلد المختارة';
$lang['ports_select_from_menu_false'] ="الحقل المدينة مطلوب";