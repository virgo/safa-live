<?php
$lang['hotel_marketing_companies']='شركات التسويق الفندقى';
$lang['add_hotel_marketing_companies']='اضافة شركة تسويق فندقى';
$lang['edit_hotel_marketing_companies']='تعديل شركة التسويق الفندقى';
$lang['hotel_marketing_companies_name_ar']='الاسم باللغة العربية';
$lang['hotel_marketing_companies_name_la']='الاسم باللغة الانجليزية';
$lang['hotel_marketing_companies_phone']='الهاتف';
$lang['hotel_marketing_companies_fax']='الفاكس';
$lang['hotel_marketing_comapnies_email']='البريد الاليكترونى';
$lang['hotel_marketing_companies_remarks']='ملاحظات';