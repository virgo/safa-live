<?php
$lang['safa_eas_add_title']='اضافة وكيل خارجى';
$lang['safa_eas_edit_title']='تعديل بيانات وكيل خارجى';
$lang['safa_eas_name_ar']='الاسم بالعربى';
$lang['safa_eas_name_la']='الاسم بالاتينى';
$lang['safa_eas_erp_country_id']='الدولة';
$lang['safa_eas_logo']='الشعار';
$lang['safa_eas_phone']='رقم الهاتف';
$lang['safa_eas_mobile']='رقم الموبايل';
$lang['safa_eas_fax']='رقم الفاكس';
$lang['safa_eas_email']='البريد الإليكتروني';
$lang['safa_eas_user_username']='المستخدم';
$lang['safa_eas_user_password']='كلمة المرور';
$lang['safa_eas_user_repeat_password']='تأكيد المرور';
$lang['safa_eas_search_title']='التصفية';
$lang['safa_eas_direct_contracts']='عدد العقود المباشرة';
$lang['safa_eas_indirect_contracts']='عدد العقود الغير مباشرة';
$lang['safa_eas_trips']='عدد الرحلات';

$lang['eas']='الوكلاء';
$lang['parent_node_title']='فيرجو';
$lang['parent_child_title']='الوكلاء الخارجيين';
$lang['show_ea_user']='عرض  الموظفين ';
$lang['child_node_title_ea_users']='موظفين الوكلاء الخارجيين';

$lang['actions']='العمليات';

$lang['branches']='الفروع';
$lang['name_ar']='الاسم بالعربى';
$lang['name_la']='الاسم بالاتينى';
$lang['address']='العنوان';
$lang['city']='المدينة';
$lang['phone']='رقم الهاتف';
$lang['mobile']='رقم الموبايل';
$lang['fax']='رقم الفاكس';
$lang['email']='البريد الإليكتروني';

