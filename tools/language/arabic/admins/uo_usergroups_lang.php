<?php

defined('BASEPATH') OR exit('No direct script access allowed');

$lang['uo_usergroups_name_ar']='الأسم بالعربى';
$lang['uo_usergroups_name_la']='الأسم باللاتينى';
$lang['uo_usergroups_erp_countery_id']='البلد';
$lang['uo_usergroups_add_title']='اضافة مجموعة';
$lang['uo_usergroups_edit_title']='تعديل بيانات مجموعة';
$lang['uo_usergroups_th_usergroups_name_ar']='اسم المجموعة العربى';
$lang['uo_usergroups_th_usergroups_name_la']='اسم المجموعة اللاتينى';
$lang['uo_usergroups_name_exist']='اسم المجموعة موجود من قبل';
$lang['uo_usergroups_name_invalid']='خطأ فى الاسم باللاتينى';
$lang['uo_usergroups_title']='مجموعات المستخدمين';
$lang['uo_usergroups_privilege']='صلاحيات المستخدمين';