<?php
$lang['title']='اعدادات عروض الحجز الفندقي';

$lang['actions']='العمليات';
$lang['add']='اضافة';
$lang['edit']='تعديل';
$lang['delete']='حذف';
$lang['save']='حفظ';


$lang['add_title']='اضافة اعداد عرض حجز';

$lang['edit_title']='تعديل اعداد عرض حجز';

$lang['delete_title']='حذف اعداد عرض حجز';

$lang['main_data']='البيانات الاساسية';

$lang['availabilty_offer_owner_type']='نوع الشركة';
$lang['availabilty_offer_owner_id']='الشركة';

$lang['name_ar']='الاسم العربى';
$lang['name_la']='الاسم الانجليزى';
$lang['nights_count']='عدد الليالى';
$lang['pricing_methodology']='طريقة التسعير';

$lang['exclude_week_end_days']='اهمال ايام "نهاية الاسبوع" عند التسعير ان وجدت فى الفترة المطلوبة';
$lang['include_week_end_days']='حساب ايام "نهاية الاسبوع" عند التسعير ان وجدت فى الفترة المطلوبة';

$lang['discount_percent']='نسبة الخصم';
$lang['discount_fixed']='قيمة الخصم';
$lang['rounding_value']='الى اقرب';


$lang['ea']='الوكيل الخارجي';
$lang['uo']='شركة العمرة';
$lang['hm']='شركات التسويق الفندقي';

$lang['save']='حفظ';

