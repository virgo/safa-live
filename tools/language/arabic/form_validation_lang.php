<?php

defined('BASEPATH') OR exit('No direct script access allowed');

$lang['form_validation_valid_phone'] = 'الحقل {field} يجب ان يحتوي علي رقم هاتف سليم.';
$lang['form_validation_valid_date'] = 'الحقل {field} يجب ان يحتوي علي تاريخ صحيح.';
$lang['form_validation_complex_password'] = 'الحقل {field} يجب ان يحتوي علي كلمة مرور معقدة.';
$lang['form_validation_dropdown'] = 'الحقل {field} يجب ان تختار قيمة صحيحة من القائمة.';
$lang['form_validation_max_dblength']= "الحقل {field} يجب ان يحتوى على  {param} حرف على الاكثر ";
$lang['form_validation_required']= 'الحقل {field} مطلوب';
$lang['form_validation_matches']= 'الحقل {field} لايساوى الحقل {param}';
$lang['form_validation_is_unique'] = "حقل {field} مسجل بالفعل";
$lang['form_validation_alpha_numeric']= 'الحقل {field} يجب ان يحتوى فقط على حروف وارقام ';
$lang['form_validation_valid_email']= 'الحقل {field} يجب ان يحتوى على بريد الكترونى صحيح';
$lang['form_validation_integer']= "الحقل {field} يجب ان يحتوى على ارقام فقط";
$lang['form_validation_text_space']= "الحقل{field} يجب ان يحتوى على حروف و مسافة فقط";
$lang['form_validation_alpha']= "الحقل {field} يجب  ان يحتوى على حروف";
$lang['form_validation_alpha_dash']		= 'الحقل {field} غير صحيح';
$lang['form_validation_is_natural_no_zero']= " الحقل {field} غير صحيح";
$lang['form_validation_min_length']= "الحقل {field} يجب ان يحتوى على {param} حروف على الاقل ";
$lang['form_validation_max_length']= "الحقل {field} يجب ان يحتوى على  {param} حرف على الاكثر ";
$lang['form_validation_unique_col']= "{field} مسجل بالفعل استخدم اخر";
$lang['form_validation_unique_ea_user']= "{field} مسجل بالفعل استخدم اخر";


/* End of file form_validation_lang.php */
/* Location: ./application/language/english/form_validation_lang.php */