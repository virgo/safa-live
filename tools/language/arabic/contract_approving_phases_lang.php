<?php
$lang['menu_title']='تصديق العقد';
$lang['title']='تصديق العقد';
$lang['edit_title']='تعديل العقد';

$lang['main_data']='بيانات اساسية';

$lang['required_documents']='الوثائق المطلوبة';
$lang['uo_documents']='الوثائق الواردة من شركة السياحة';

$lang['contract_code']='كود العقد';
$lang['contract_name']='الوكيل';

$lang['current_phase']='المرحلة الحالية';
$lang['next_phase']='المرحلة التالية';
$lang['contracts_status']='حالة العقد';

$lang['actions']='العمليات';

$lang['add']='اضافة';
$lang['edit']='تعديل';
$lang['delete']='حذف';
$lang['save']='حفظ';

$lang['remove_all']='حذف الكل';


$lang['document_name']='اسم الوثيقة';
$lang['upload_file']='تحميل الملف';

$lang['remove_file']='حذف الملف';

$lang['upload_files']='تحميل الملفات';
$lang['uploaded_successfully_message']='تم رفع الملفات بنجاح';

$lang['confirm']='تأكيد';
$lang['status']='الحالة';
$lang['refuse_reason']='سبب الرفض';

$lang['previous_phases']='مراحل سابقة';

$lang['username']='اسم المستخدم';
$lang['password']='كلمة المرور';

$lang['contract_open']='اضافة عقد';
$lang['contract_opened_successfully_message']='تم اضافة العقد بنجاح';

$lang['invalid_username_or_password']='خطأ فى اسم المستخدم او كلمة المرور';
$lang['contract_already_opened_message']='العقد مضاف بالفعل';

$lang['contract_approving_reports']='تقرير عقود الوكلاء';

$lang['name_la']='الاسم (LA)';
$lang['name_ar']='الاسم (ع)';
$lang['country']='الدولة';
$lang['ea_data']='بيانات الوكيل الخارجي';
$lang['contract_phases']='مراحل تصديق العقد';
