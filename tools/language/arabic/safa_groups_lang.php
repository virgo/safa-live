<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['safa_groups_name']='اسم المجموعة';
$lang['safa_groups_name_la']='الأسم باللاتينى';
$lang['safa_groups_creation_date']='تاريخ الانشاء';
$lang['safa_groups_from_date']='من';
$lang['safa_groups_to_date']='الى';
$lang['safa_groups_safa_ea_id']='كود شركة السياحة';
$lang['safa_groups_add_title']='اضافة مجموعة';
$lang['safa_groups_edit_title']='تعديل بيانات مجموعة';
$lang['safa_groups_th_name']='اسم المجموعة';
$lang['safa_groups_th_name_la']='اسم المجموعة اللاتينى';
$lang['safa_groups_name_exist']='اسم المجموعة موجود من قبل';
$lang['safa_groups_name_invalid']='خطأ فى الاسم باللاتينى';
$lang['safa_groups_title']='مجموعات المعتمرين';
$lang['safa_groups_mutamer_name']='اسم المعتمر';
$lang['safa_groups_passport_no']='رقم الجواز';
$lang['safa_groups_no_of_mutamers']='عدد المعتمرين';
$lang['show_mutamers']='المعتمرين';
