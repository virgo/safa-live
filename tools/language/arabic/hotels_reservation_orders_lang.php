<?php
$lang['title']='طلبات حجز الفنادق';
$lang['title_incoming']='طلبات حجز الفنادق الوارده';

$lang['main_data']='بيانات اساسية';

$lang['nationalities']='الجنسيات';
$lang['actions']='العمليات';
$lang['add']='اضافة';
$lang['edit']='تعديل';
$lang['delete']='حذف';
$lang['save']='حفظ';


$lang['add_title']='اضافة طلب حجز';
$lang['add_saving_availability_title']='اضافة طلب توفير اتاحة';

$lang['edit_title']='تعديل طلب حجز';
$lang['edit_saving_availability_title']='تعديل طلب توفير اتاحة';

$lang['delete_title']='حذف طلب حجز';

$lang['details_title']='تفاصيل طلب الحجز';


$lang['all_nationalities']='كل الجنسيات';
$lang['arabic_nationalities']='الجنسيات العربية';
$lang['foreign_nationalities']='الجنسيات الاجنبية';
$lang['clear_nationalities']='حذف الجنسيات';

$lang['company_type']='نوع الشركة';
$lang['company']='الشركة';

$lang['order_no']='رقم الطلب';
$lang['order_date']='التاريخ';
$lang['contact_person']='الشخص المسئول';
$lang['city']='المدينة';
$lang['hotel']='الفندق';
$lang['alternative_hotel']='فندق بديل';
$lang['arrival_date']='تاريخ الوصول';
$lang['departure_date']='تاريخ المغادرة';

$lang['general_feature_for_hotel']='مواصفات عامة للفندق المطلوب';

$lang['distance_from']='المسافة من الحرم';
$lang['distance_to']='الى';
$lang['meter']='متر';
$lang['look_to_haram']='اطلاله على الحرم';
$lang['price_from']='السعر من';
$lang['price_to']='الى';
$lang['currency']='العملة';
$lang['nationalities']='الجنسيات';
$lang['inclusive_services']='الاضافات';
$lang['advantages']='المزايا';

$lang['rooms']='الحجرات';
$lang['meals']='الوجبات';

$lang['count']='العدد';
$lang['meal']='الوجبة';

$lang['nights_count']='عدد الليالى';
$lang['rooms_count']='عدد الغرف';

$lang['housing_type']='نوع السكن';
$lang['room_type']='نوع الغرفة';
$lang['entry_date']='تاريخ الدخول';
$lang['exit_date']='تاريخ الخروج';


$lang['ea']='الوكيل الخارجي';
$lang['uo']='شركة العمرة';
$lang['hm']='شركات التسويق الفندقي';

$lang['send']='ارسال';


$lang['available']='المتاح';
$lang['reset_to_complete_order']='اللازم لاستكمال الطلب';

$lang['don_not_have_privilege_to_add_reservation_order']='ليس لديك صلاحية لأضافة طلب حجز';

$lang['address']='العنوان';
$lang['hotel_level']='مستوى الفندق';
$lang['price']='السعر';