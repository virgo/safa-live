<?php
$lang['menu_title']='Contract Phases';
$lang['title']='Contract Phases';
$lang['main_data']='Main Data';
$lang['phase_documents']='Phase Documents';

$lang['name_ar']='Arabic Name';
$lang['name_la']='English Name';

$lang['nationalities']='Nationalities';
$lang['document_name_ar']='Document Arabic Name';
$lang['document_name_la']='Document English Name';

$lang['actions']='Actions';

$lang['add']='Add';
$lang['edit']='Edit';
$lang['delete']='Delete';
$lang['save']='Save';

$lang['add_title']='Add Contract Phase';
$lang['edit_title']='Edit Contract Phase';
$lang['delete_title']='Delete Contract Phase';

$lang['all_nationalities']='All Nationalities';
$lang['arabic_nationalities']='Arabic Nationalities';
$lang['foreign_nationalities']='Foreign Nationalities';

$lang['id']='ID';
