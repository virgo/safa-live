<?php

defined('BASEPATH') OR exit('No direct script access allowed');

$lang['safa_umrahgroups_name'] = 'Group Name';
$lang['safa_umrahgroups_name_la'] = 'English Name';
$lang['safa_umrahgroups_creation_date'] = 'Creation Date';
$lang['safa_umrahgroups_to_date'] = 'To';
$lang['safa_umrahgroups_from_date'] = 'From';
$lang['safa_umrahgroups_safa_ea_id'] = 'EA Code';
$lang['safa_umrahgroups_add_title'] = 'Add group';
$lang['safa_umrahgroups_edit_title'] = 'Edit group';
$lang['safa_umrahgroups_th_name'] = 'Group Name';
$lang['safa_umrahgroups_th_name_la'] = 'English Name';
$lang['safa_umrahgroups_name_exist'] = 'Group Name Exist';
$lang['safa_umrahgroups_name_invalid'] = 'Invalid Name';
$lang['safa_umrahgroups_title'] = 'Umrah Groups';
$lang['safa_umrahgroups_mutamer_name'] = 'Mutamer Name';
$lang['safa_umrahgroups_passport_no'] = 'Passport Number';
$lang['safa_umrahgroups_no_of_mutamers'] = 'No Of Mutamers';
$lang['show_mutamers'] = 'Show Mutamers';
$lang['safa_umrahgroups_contract_id'] = 'Contract Id';

$lang['safa_umrahgroups_status_id'] = 'Status';

