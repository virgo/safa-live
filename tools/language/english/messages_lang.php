<?php
$lang['menu_title']='Sending Messages';
$lang['title']='Send Message';
$lang['message_subject']='Message Subject';
$lang['message_body']='Message Body';

$lang['new_message']='New Message';

$lang['view_all']='View ALl';

$lang['message']='Message';
$lang['messages']='Messages';

$lang['from']='From';
$lang['to']='To';
$lang['the_date']='The Date';

$lang['new_message']='New Message';

$lang['delete_message']=  'Delete Message';
$lang['close']=  'Close';
$lang['restore_message']=  'Restore Message';
$lang['mark_as_important']='Mark As Important';
$lang['mark_as_un_important']='Mark As Un Important';
$lang['important_messages']='Important Messages';
$lang['deleted_messages']='Deleted Messages';

$lang['company_type']='Company Type';
$lang['country']='Country';
$lang['company']='Company';
$lang['attachement']='Attachements';
$lang['importance']='Importance';
$lang['send']='Send';

$lang['success_message']='Message Was Sent Successfully.';


$lang['message_subject_text_for_add_hotel']='Add New Hotel Request';
$lang['message_body_text_for_add_hotel']='New Request text: ';
$lang['message_subject_text_for_edit_hotel']='Request Edit The Hotel: ';
$lang['message_body_text_for_edit_hotel']='Request Edit Hotel Text: ';
