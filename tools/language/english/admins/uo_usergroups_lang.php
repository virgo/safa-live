<?php

defined('BASEPATH') OR exit('No direct script access allowed');

$lang['uo_usergroups_name_ar']='Arabic Name';
$lang['uo_usergroups_name_la']='English Name';
$lang['uo_usergroups_add_title']='Add Usergroup';
$lang['uo_usergroups_edit_title']='Edit Usergroup';
$lang['uo_usergroups_th_usergroups_name_ar']='Arabic Name';
$lang['uo_usergroups_th_usergroups_name_la']='English Name';
$lang['uo_usergroups_name_exist']='Usergroup Name Exist';
$lang['uo_usergroups_name_invalid']='Invalid Name';
$lang['uo_usergroups_title']='Usergroups';
$lang['uo_usergroups_privilege']='Usergroups Priviliges';