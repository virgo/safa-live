<?php
$lang['menu_title']='System Notifications Translation';
$lang['title']='Notification Data';
$lang['event']='Event';
$lang['subject']='Notification Subject';
$lang['body']='Notification Body';

$lang['subject_lang1']='Notification Subject - Language 1';
$lang['body_lang1']='Notification Body - Language 1';

$lang['subject_lang2']='Notification Subject - Language 2';
$lang['body_lang2']='Notification Body - Language 2';

$lang['notifications_translate']='Translate Notifications';

$lang['trip']='Trip';
$lang['trip_no']='Trip No';
$lang['trip_date']='Trip Date';
$lang['the_date']='The Date';
$lang['trip_arrival_datetime']='Trip Arrival Date & Time';
$lang['trip_leaving_datetime']='Trip Leaving Date & Time';
$lang['madina_go_to_datetime']='Go To Madina Date & Time';
$lang['makka_go_to_datetime']='Go To Makka Date & Time';
$lang['hotel_arrival_datetime']='Hotel Arrival Date & Time';
$lang['hotel_leaving_datetime']='Hotel Leaving Date & Time';

$lang['translate_from']='Translate From';
$lang['to']='To';

$lang['select_from_menu']='Select From Menu';

$lang['language_1']='First Language';
$lang['language_2']='Second Language';

$lang['event_type']='Notification Type';
$lang['notification']='Notification';
$lang['email']='Email';