<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['meals'] = "Meals Types ";
$lang['parent_child_title'] = "Meals Types";
$lang['meals_namear'] = "Arabic Name";
$lang['meals_namela'] = "Latin Name";
$lang['meals_code'] =  "Code";
$lang['meals_description_ar'] =  "Arabic Description ";
$lang['meals_description_la'] =  "Englis Description";
$lang['add_meal'] =  "Add Meal";
$lang['edit_meal'] =  "Edit Meal";
?>

