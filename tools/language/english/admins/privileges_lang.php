<?php

defined('BASEPATH') OR exit('No direct script access allowed');

$lang['privileges_name_ar']='Arabic Name';
$lang['privileges_name_la']='English Name';
$lang['privileges_description_ar']='Arabic Description';
$lang['privileges_description_la']='English Description';
$lang['privileges_description']='Description';
$lang['privileges_erp_countery_id']='البلد';
$lang['privileges_add_title']='Add Privilege';
$lang['privileges_edit_title']='Edit Privilege';
$lang['privileges_th_privilege_name_ar']='Arabic Name';
$lang['privileges_th_privilege_name_la']='English Name';
$lang['privileges_th_privileges_role_name']='Role Name';
$lang['privileges_th_privileges_description']='Privilege Description';
$lang['privileges_name_exist']='Privilege Name Exist';
$lang['privileges_name_invalid']='Invalid English Name';
$lang['uoprivileges_title']='UO Privileges';
$lang['eaprivileges_title']='EA Privileges';
$lang['itoprivileges_title']='ITO Privileges';
$lang['privileges_role_name']='Role Name';
