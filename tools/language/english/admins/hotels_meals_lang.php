<?php
$lang['parent_child_title']='Hotels Meals';
$lang['hotels_meals']='Hotels Meals';
$lang['add_hotels_meals']='Add Hotel Meal';
$lang['edit_hotels_meals']='Edit Hotel Meal';
$lang['hotels_meals_name_ar']='Arabic Name';
$lang['hotels_meals_name_la']='English Name';
$lang['hotels_meals_description_ar']='Arabic Name';
$lang['hotels_meals_description_la']='English Name';
$lang['hotels_meals_code']='Code';