<?php
$lang['menu_title']='System Notifications - Arabic';
$lang['title']='Notification Data';
$lang['event']='Event';
$lang['subject']='Notification Subject';
$lang['body']='Notification Body';
$lang['notifications_add']='Add Notifications';

$lang['trip']='Trip';
$lang['trip_no']='Trip No';
$lang['trip_date']='Trip Date';
$lang['the_date']='The Date';
$lang['trip_arrival_datetime']='Trip Arrival Date & Time';
$lang['trip_leaving_datetime']='Trip Leaving Date & Time';
$lang['madina_go_to_datetime']='Go To Madina Date & Time';
$lang['makka_go_to_datetime']='Go To Makka Date & Time';
$lang['hotel_arrival_datetime']='Hotel Arrival Date & Time';
$lang['hotel_leaving_datetime']='Hotel Arrival Date & Time';

$lang['view_all']='View ALl';
$lang['notifications']='Notifications';

$lang['message']='Message';
$lang['messages']='Messages';

$lang['system']='System';
$lang['from']='From';

$lang['delete_message']=  'Delete Message';
$lang['close']=  'Close';

$lang['restore_message']=  'Restore Message';


$lang['mark_as_important']='Mark As Important';
$lang['mark_as_un_important']='Mark As Un Important';

$lang['important_messages']='Important Messages';
$lang['deleted_messages']='Deleted Messages';

$lang['event_type']='Notification Type';
$lang['notification']='Notification';
$lang['email']='Email';
