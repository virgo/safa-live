<?php
$lang['safa_transporter_erp_country_id']='Country';
$lang['safa_transporter_erp_transportertype_id']='Transporter Type';
$lang['safa_transporter_name_ar']='Arabic Name';
$lang['safa_transporter_name_la']='English Name';
$lang['safa_transporter_code']='Code';
$lang['transporters_add']='Add Transporters';
$lang['transporters']='Transporters';
$lang['operations']='Operations';
$lang['safa_transporters_add']='Add Transporter ';
$lang['safa_transporters_edit']='Edit Transporter';

$lang['drivers']='Drivers';
$lang['buses']='Buses';

$lang['name_ar']='Arabic Name';
$lang['name_la']='English Name';
$lang['phone']='Phone';

$lang['add_driver']='Add Driver';
$lang['edit_driver']='Edit Driver';

$lang['main_data']='Main Data';


$lang['add_bus']='Add Bus';
$lang['edit_bus']='Edit Bus';

$lang['bus_no']='Bus No';
$lang['safa_buses_brands_id']='Brand';
$lang['safa_buses_models_id']='Model';
$lang['industry_year']='Industry Year';
$lang['passengers_count']='Passengers Count';
$lang['notes']='Public Features';

