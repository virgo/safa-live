<?php
$lang['parent_child_title']='Hotels Rooms';
$lang['hotels_rooms_types']='Hotels Rooms';
$lang['add_hotel_room_type']='Add Hotel Room';
$lang['edit_hotels_rooms_types']='Edit Room Type';
$lang['hotels_rooms_types_name_ar']='Arabic Name';
$lang['hotels_rooms_types_name_la']='English Name';
$lang['hotels_rooms_types_beds_number']='Beds Number';
$lang['hotels_rooms_types_overload']='Overload';