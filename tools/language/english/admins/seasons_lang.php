<?php
$lang['seasons']='Seasons';
$lang['add_season']='Add Season';
$lang['edit_seasons']='Edit Season';
$lang['seasons_name_ar']='Arabic Name';
$lang['seasons_name_la']='English Name';
$lang['seasons_start_date']='Start Date';
$lang['seasons_end_date']='End Date';
$lang['seasons_safa_ea_id']='External Agents';