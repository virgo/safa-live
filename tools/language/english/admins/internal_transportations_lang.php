<?php

$lang['internal_transportations_general'] = 'General';
$lang['internal_transportations_path_details'] = 'Path Details';
$lang['internal_transportations_global_title'] = 'Internal Transportations';
$lang['internal_transportations_title'] = 'Internal Transportations';
$lang['internal_transportations_id'] = 'ID';
$lang['internal_transportations_code'] = 'Code';
$lang['internal_transportations_res_status_id'] = 'Reservation Status';
$lang['internal_transportations_ref_num'] = 'transportation company reservation code';
$lang['internal_transportations_date'] = 'Date';
$lang['internal_transportations_supplier_id'] = 'Supplier';
$lang['internal_transportations_sales_manager'] = 'Sales Manager';
$lang['internal_transportations_mecca_hotel_id'] = 'Mecca Hotel';
$lang['internal_transportations_madinah_hotel_id'] = 'Madinah Hotel';
$lang['internal_transportations_bus_quantity'] = 'Bus Quantity';
$lang['internal_transportations_transport_com_id'] = 'Transport Company';
$lang['internal_transportations_arrival_hall'] = 'Arrival Hall';
$lang['internal_transportations_notes'] = 'Notes';
$lang['internal_transportations_package_id'] = 'Package';
$lang['internal_transportations_trip_id'] = 'Trip';
$lang['internal_transportations_contract_id'] = 'Contact';
$lang['internal_transportations_supervisor_id'] = 'Supervisor';
$lang['internal_transportations_sub_total'] = 'Sub Total';
$lang['internal_transportations_discount'] = 'Discount';
$lang['internal_transportations_additional_fees'] = 'Additional Fees';
$lang['internal_transportations_total'] = 'Total';
$lang['internal_transportations_step'] = 'Step';
$lang['internal_transportations_firm_id'] = 'Firm';
$lang['internal_transportations_currency_id']='Currency';
$lang['internal_transportations_rate']='Rate';

/* End of file internal_transportations.php */
/* Location: ./application/languages/english/internal_transportations.php */