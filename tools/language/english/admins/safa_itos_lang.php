<?php
$lang['safa_itos_title']='Safa Itos';
$lang['safa_itos_name_ar']='Arabic Name';
$lang['safa_itos_name_la']='English Name';
$lang['safa_itos_erp_country_id']='Country';
$lang['safa_itos_reference_code']='Reference Code';
$lang['safa_itos_add_title']='Add Itos';
$lang['safa_itos_user_username']='Username';
$lang['safa_itos_user_password']='Password';
$lang['safa_itos_user_repeat_password']='Confirm Password';
$lang['safa_itos_edit_title']='Safa Itos Edit ';
