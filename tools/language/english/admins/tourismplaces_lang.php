<?
defined('BASEPATH') OR exit('No direct script access allowed');


$lang['tourismplaces_name_la'] = 'English Name';
$lang['tourismplaces_name_ar'] = 'Arabic Name';
$lang['tourismplaces_erp_city_id'] = 'City';
$lang['tourismplaces_erp_country_id'] = 'Country';
$lang['tourismplaces_add_title']='Add Sight';
$lang['tourismplaces_edit_title']='Edit Sight';
$lang['tourismplaces_search_title']='Search';
$lang['tourismplaces_title']='Tourismplaces';
$lang['tourismplaces_th_tourismplaces_name_ar']='Tourismplace Arabic Name';
$lang['tourismplaces_th_tourismplaces_name_la']='Tourismplace English Name';
$lang['tourismplaces_th_city_name']='City Name';
$lang['tourismplaces_th_operations']='Operations';
$lang['tourismplaces_search_button']='Search';
$lang['tourismplaces_name_exist']='Tourismplace Name Exist.';
$lang['tourismplaces_name_in_city_exist']='Tourismplace Name Exist In The Selected City.';
$lang['tourismplaces_name_invalid']='Invalid Name.';
$lang['tourismplaces_search_button']='Search';
$lang['tourismplaces_select_from_menu_cities'] = "Choose country First";

$lang['tourismplaces_select_from_menu_false'] = "City field is required.";
$lang['tourismplaces_select_from_menu_no_cities']='No Cities in this Country';