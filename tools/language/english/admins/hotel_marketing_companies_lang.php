<?php
$lang['hotel_marketing_companies']='Hotel Marketing Companies';
$lang['add_hotel_marketing_companies']='Add Hotel Marketing Company';
$lang['edit_hotel_marketing_companies']='Edit Hotel Marketing Company';
$lang['hotel_marketing_companies_name_ar']='Arabic Name';
$lang['hotel_marketing_companies_name_la']='English Name';
$lang['hotel_marketing_companies_phone']='Phone';
$lang['hotel_marketing_companies_fax']='Fax';
$lang['hotel_marketing_companies_email']='Email';
$lang['hotel_marketing_companies_remarks']='Notes';