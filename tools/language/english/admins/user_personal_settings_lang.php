<?php
$lang['user_personal_settings_username'] = 'Username';
$lang['user_personal_settings_password']='Current Password';
$lang['user_personal_settings_new_password'] = 'New Password';
$lang['user_personal_settings_resetpassword'] = 'Confirm New Password';
$lang['user_personal_settings_language'] = 'Language';

/* End of file user_personal_settings.php */
/* Location: ./application/languages/english/user_personal_settings.php */