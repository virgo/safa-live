<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['parent_child_title'] = "Hotel Levels";
$lang['hotel_levels'] = "Hotels Levels";
$lang['hotellevels_namear'] = "Arabic Name";
$lang['hotellevels_namela'] = "Latin Name";
$lang['hotellevels_code'] =  "Code";
$lang['add_hotellevel'] =  "Add Hotel Level";
$lang['edit_hotellevel'] =  "Edit Hotel Level";


?>

