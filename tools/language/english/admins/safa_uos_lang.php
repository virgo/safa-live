<?php

$lang['safa_uos_add']='Add UOS';
$lang['safa_uos_name_ar']='Name(A)';
$lang['safa_uos_name_la']='Name(LA)';
$lang['safa_uos_code']='License No';
$lang['safa_uos_phone']='Phone';
$lang['safa_uos_mobile']='Mobile';
$lang['safa_uos_email']='E-mail';
$lang['safa_uos_fax']='Fax';
$lang['safa_uos_add_title']='Add Umrah Operator';
$lang['safa_uos_edit_title']='Edit Umrah Operator';
$lang['safa_uos_search_title']='Search';
$lang['safa_uos_title']='Umrah Operators';
$lang['safa_uos_th_name']='Name';
$lang['safa_uos_th_code']='License No';
$lang['safa_uos_th_phone']='Phone';

$lang['safa_uos_th_no_of_uos']='Employee No';
$lang['safa_uos_user_name_ar']='Name(A)';
$lang['safa_uos_user_name_la']='Name(LA) ';
$lang['safa_uos_user_username'] = "UserName";
$lang['safa_uos_user_password'] = "Password";
$lang['safa_uos_user_repeat_password']='Confirm Password';
$lang['safa_uos_th_username']='UserName';
$lang['safa_uos_users_title']='Umrah Operator Users';
$lang['safa_uos_users_add_title']='Add User';
$lang['safa_uos_users_edit_title']='Edit User';
$lang['safa_uos_contract_num']='Contracts No';
$lang['safa_uos_trips_num']='Trips No';
$lang['uo_removed']='Removed';
$lang['uo_active']='Activated';
$lang['uo_all']='All';
$lang['uos']='Agents';
$lang['parent_node_title']='Virgo';
$lang['parent_child']='KSA Agent';
$lang['child_node_title']='KSA Agent';
$lang['uos']='Umrah Operators';
$lang['parent_child_title']='Umrah Operators';
$lang['show_uo_user']='Show Users';
$lang['child_node_title_ou_users']='Umrah Operator Users';

