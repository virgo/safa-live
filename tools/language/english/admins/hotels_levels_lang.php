<?php
$lang['hotel_levels']='Hotels Levels';
$lang['parent_child_title']='Hotels Levels';
$lang['add_hotels_levels']='Add Hotel Level';
$lang['edit_hotels_levels']='Edit Hotel Level';
$lang['hotels_levels_name_ar']='Arabic Name';
$lang['hotels_levels_name_la']='English Name';
$lang['hotels_levels_code']='Code';