<?php
$lang['safa_eas_title']='External Agents';
$lang['safa_eas_add_title']='Add External Agent';
$lang['safa_eas_edit_title']='Edit External Agent';
$lang['safa_eas_name_ar']='Arabic Name';
$lang['safa_eas_name_la']='English Name';
$lang['safa_eas_erp_country_id']='Country';
$lang['safa_eas_logo']='Logo';
$lang['safa_eas_phone']='Phone';
$lang['safa_eas_mobile']='Mobile';
$lang['safa_eas_fax']='Fax';
$lang['safa_eas_email']='Email';
$lang['safa_eas_user_username']='Username';
$lang['safa_eas_user_password']='Password';
$lang['safa_eas_user_repeat_password']='Repeat Password';
$lang['eas_operations']='Operations';
$lang['safa_eas_search_title']='Search';
$lang['safa_eas_direct_contracts']='Direct Contracts';
$lang['safa_eas_indirect_contracts']='Indirect Contracts';
$lang['add']='Add';
$lang['search']='Search';
$lang['virgo']='Virgo';
$lang['external_agents']='External Agents';


$lang['eas']='Agents';
$lang['parent_node_title']='Virgo';
$lang['parent_child_title']='External Agents';
$lang['show_ea_user']='Users';
$lang['child_node_title_ea_users']='External Agents Users';

$lang['actions']='Actions';

$lang['branches']='Branches';
$lang['name_ar']='Arabic Name';
$lang['name_la']='English Name';
$lang['address']='Address';
$lang['city']='City';
$lang['phone']='Phone';
$lang['mobile']='Mobile';
$lang['fax']='Fax';
$lang['email']='Email';