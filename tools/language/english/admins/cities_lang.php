<?php


defined('BASEPATH') OR exit('No direct script access allowed');


$lang['cities_name_la'] = 'English Name';
$lang['cities_name_ar'] = 'Arabic Name';
$lang['cities_erp_countery_id'] = 'Country';
$lang['cities_title']='Cities';
$lang['cities_add_title']='Add City';
$lang['cities_edit_title']='Edit City';
$lang['cities_search_title']='Search';
$lang['cities_title']='Cities';
$lang['cities_th_city_name']='City Name';
$lang['cities_th_country_name']='Countery Name';
$lang['cities_th_operations']='Operations';
$lang['cities_search_button']='Search';
$lang['cities_name_exist']='City Name Exist';
$lang['cities_name_invalid']='Invalid Name';
$lang['cities_search_button']='Search';
