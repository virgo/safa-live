<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['admins_username'] = "Username";
$lang['admins_password'] = "Password";
$lang['admins_email'] = "Email";
$lang['admins_repeat_password']="Repeate password";
$lang['admins_title']='Admins Edit Form';
$lang['admins_add_title']='Add User';
$lang['admins_edit_title']='Edit User';

$lang['admins_search_title']='Admins Search';

$lang['admins_search_title']='Search';

$lang['admins_title']='Users';
$lang['admins_th_name']='Name';

?>

