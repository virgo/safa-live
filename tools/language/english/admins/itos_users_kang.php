<?php
$lang['safa_itos_add']='Add Itos';
$lang['itos_users_title']='Itos Users';
$lang['itos_users_name_ar']='Arabic Name';
$lang['itos_users_name_la']='English Name';
$lang['itos_users_username']='Username';
$lang['itos_users_password']='Password';
$lang['itos_users_repeat_password']='Repeat Password';
$lang['itos_users_safa_ito_id']='Safa Ito';
$lang['itos_users_safa_ito_usergroup_id']='Ito Usergroup';
$lang['itos_users_safa_erp_language_id']='Language';
$lang['itos_users_ver_code']='Code';
$lang['itos_users_email']='Email';