<?php

defined('BASEPATH') OR exit('No direct script access allowed');

$lang['internaltripstatus_name_ar']='أسم حالة الرحلة بالعربى';
$lang['internaltripstatus_name_la']='أسم حالة الرحلة باللاتينى';
$lang['internaltripstatus_description_ar']='الوصف بالعربى';
$lang['internaltripstatus_description_la']='الوصف باللاتينى';

$lang['internaltripstatus_color']='لون الحالة';
$lang['internaltripstatus_code']='كود البرمجة';

$lang['internaltripstatus_operations']='العمليات';
$lang['parent_node_title']='فيرجو';
$lang['color_pick']='اختر الون ';

$lang['cities_th_operations']='العمليات';
