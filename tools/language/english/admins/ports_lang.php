<?php
$lang['ports_add_title']='Add Airport';
$lang['ports_edit_title']='Edit Airport';
$lang['ports_erp_port_id']='المنافذ';
$lang['ports_name_ar']='Arabic Name';
$lang['ports_name_la']='English Name';
$lang['ports_code']='Code';
$lang['ports_country_code']='Country Code';
$lang['ports_world_area_code']='World Area Code';
$lang['ports_city_id']='City Name';
$lang['ports_country_name']='Country Name';
$lang['ports_th_city_name']='City Name';
$lang['ports_th_country_name']='Country Name';
$lang['ports_th_name']='Airport Name';
$lang['ports_name_exist']='Port Name Exist';
$lang['ports_select_from_menu_cities'] = "Choose country First";
$lang['ports_select_from_menu_false'] = "City field is required.";
