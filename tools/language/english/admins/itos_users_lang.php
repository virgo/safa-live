<?php
$lang['safa_itos_add']='Add ITOS Users';
$lang['itos_users_name_ar']='Arabic Name';
$lang['itos_users_name_la']='English Name';
$lang['itos_users_username']='Username';
$lang['itos_users_password']='Password';
$lang['itos_users_repeat_password']='Confirm Password';
$lang['itos_users_safa_ito_id']='ITOS';
$lang['itos_users_safa_ito_usergroup_id']='ITOS Usergroups';
$lang['itos_users_safa_erp_language_id']='Language';
$lang['itos_users_ver_code']='Code';
$lang['itos_users_email']='Email';
$lang['safa_itos_users_add_title']='Add User';
$lang['safa_itos_edit']='Edit ITO User';