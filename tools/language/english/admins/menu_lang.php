<?php

defined('BASEPATH') OR exit('No direct script access allowed');

$lang['menu_main_data']='Main Data';
$lang['menu_management_users_virgo']='Management Users Virgo';
$lang['menu_technical_data']='Technical Data';
$lang['menu_ksa_agent']='KSA Agents';
$lang['menu_ksa_agent_privileges']='KSA Agents Privileges';
$lang['menu_ksa_agent_usergroups']='KSA Agents Usergroups';
$lang['menu_external_agent']='External Agents ';
$lang['menu_agent_contracts']='Contracts linked to Agents';
$lang['menu_flights']='Flights';
$lang['menu_internal_transport']='Internal Transport';
$lang['menu_reports']='Reports';
$lang['menu_movement']='Movement';
$lang['menu_management_users']='Users';
$lang['menu_management_usersgroups']='Users Groups';
$lang['menu_main_cities']='Cities';
$lang['menu_main_transport_companies']='Transport Companies(All Kinds)';
$lang['menu_main_transport_operators']='Transport Operators';
$lang['menu_main_transportation_outlets']='Transportation Outlets';
$lang['menu_main_hole_airports']='Hole Airports';
$lang['menu_main_sights']='Sights';
$lang['menu_main_hotels_levels']='Hotels Levels';
$lang['menu_main_types_of_meals_hotels']='Types Of Meals Hotels';
$lang['menu_main_types_of_hotel_rooms']='Types Of Hotel Rooms';
$lang['menu_main_hotels']='Hotels';
$lang['menu_techn_flight_cases']='Flight cases';
$lang['menu_techn_types_of_transport']='Types of transport';
$lang['menu_techn_types_of_internal_transport_clips']='Types of internal transport clips';
$lang['menu_techn_cases_of_transport_clips']='Cases of transport clips';
$lang['menu_techn_cases_is_internal_transport']='Cases is internal transport';
$lang['menu_techn_users_Permissions_virgo']='Users Permissions Virgo';
$lang['menu_techn_external_agentUsers_permissions']='External AgentUsers Permissions';
$lang['menu_techn_ksa_agent_permissions']='KSA Agents users Permissions';
$lang['menu_ksa_agent_users']='KSA Agents Users';
$lang['menu_external_agent_Users']='External Agents Users';
$lang['menu_flights_Add']='Add trip';
$lang['menu_flights_Search']='Search for trip';
$lang['menu_internal_transport_Add']='Add an operation order';
$lang['menu_internal_transport_Search']='Search for an operation';
$lang['menu_reports_arrival']='arrival report';
$lang['menu_reports_Departure']='Departure';
$lang['menu_movement_movements']='Movements';
$lang['menu_safa_itos']='Safa Itos';

//notifications
$lang['send_noti_via_email'] = "ارسال التنبيهات على البريد الاليكترونى";
$lang['send_noti_via_mobile'] = "ارسال التنبيهات على الموبايل (SMS)";
$lang['what_is_to_be'] = "ما يتم التنبية به";
$lang['all_noti'] = "كل التنبيهات";
$lang['just_noti_from'] = "فقط التنبيهات المرسلة من";
$lang['noti_sended_from_system'] = "التنبيهات المرسلة من النظام";

$lang['noti_sended_from_system'] = "قائمة شركات العمرة";
$lang['noti_sended_from_system'] = "قائمة شركات السياحة";
$lang['noti_sended_from_system'] = "قائمة شركات مشغلي النقل";
$lang['noti_sended_from_system'] = "قائمة شركات التسويق الفندقي";