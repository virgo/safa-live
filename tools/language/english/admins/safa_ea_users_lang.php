<?php
$lang['ea_users']='مستخدمين الوكلاء الخارجيين';
$lang['ea_users_name_ar']='الاسم باللغة العربية';
$lang['ea_users_name_la']='الاسم باللغة الانجليزية';
$lang['ea_users_username']='اسم المستخدم';
$lang['ea_users_password']='كلمة المرور';
$lang['ea_users_safa_ea_usergroup_id']= 'مجموعات المستخدمين';
$lang['ea_users_safa_ea_id']='شركة السياحة';
$lang['users_add']='اضافة مستخدم جديد';
$lang['operations']='عمليات';
$lang['external_agents']='الوكلاء الخارجيين';
$lang['ea_users_confirm_password']='تأكيد كلمة المرور';
$lang['ea_safa_safa_ea_usergroup_id']='مجموعات المستخدمين';
$lang['users_edit']='تعديل بيانات المستخدم ';
$lang['eas_title']='الوكلاء الخارجيين';
$lang['ea_users_search_title']='التصفية';
$lang['safa_eas_users_add_title']='اضافة موظف';
$lang['ea_users_email']='البريد الالكتروني';