<?php
    $lang['currencies_id'] ='ID';
    $lang['currencies_code'] ='Code';
    $lang['currencies_name_ar'] ='Arabic Name';
    $lang['currencies_name_la'] ='English Name';
    $lang['currencies_short_name']='Short Name';
    $lang['currencies_symbol'] = 'Symbol';
    $lang['currencies_title'] = 'Currencies';
    $lang['currencies_no_rows'] = 'No currencies exist yet';
    $lang['currencies_short_name_ar']='Short Arabic Name';
    $lang['parent_child_title']='Currencies Types';
    $lang['currencies']='Currencies Types';
    $lang['add_currency']='Add Currency';
    $lang['edit_currency']='Edit Currency';
    
    
 
     