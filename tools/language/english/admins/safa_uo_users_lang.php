<?php
$lang['safa_uo_users_name_ar']='Name(A)';
$lang['safa_uo_users_name_la']='Name(LA)';
$lang['safa_uo_users_email']='E-mail';
$lang['safa_uo_users_mobile']='Mobile';
$lang['safa_uo_users_usersgroup']='Usergroups';
$lang['safa_uo_users_add_title']='Add Form';
$lang['safa_uo_users_edit_title']='Edit Form';
$lang['safa_uo_users_search_title']='Search Form';
$lang['safa_uo_users_title']='UOS Users';
$lang['safa_uo_users_th_name']='Name';
$lang['safa_uo_users_th_operations']='Operations';
$lang['safa_uo_users_th_username']='Username';
$lang['safa_uos_th_usergroup']='Usergroups';
$lang['safa_uo_users_th_no_of_uos']='Number Of Uos Users';
$lang['safa_uo_users_user_name_ar']='Arabic User Name';
$lang['safa_uo_users_user_name_la']='English User Name';
$lang['safa_uo_users_user_username'] = "Username";
$lang['safa_uo_users_user_password'] ="Password";
$lang['safa_uo_users_user_repeat_password']='Confirm Password';
