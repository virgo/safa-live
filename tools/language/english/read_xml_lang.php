<?php
$lang['read_xml_title'] = 'Import Mutamers';

$lang['read_xml_file'] = 'File';
$lang['read_xml_upload_file'] = 'Upload File';
$lang['upload_xml_browse'] = 'Browse';
$lang['file_type']='Source File';
$lang['gama']='جاما';
$lang['bab_umrah']='باب العمره';
$lang['road_umrah']='طريق العمره';
$lang['safa_file'] = 'Safa File';

$lang['writeFile_type']='write File Type';
$lang['invalid_file_type'] = 'Invalid Source File';
$lang['group_name'] = 'Group Name';

