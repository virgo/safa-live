<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['add_trip'] = "Create trip";
$lang['step_1'] = "Step 1";
$lang['step_2'] = "Step 2";
$lang['step_3'] = "Step 3";
$lang['step_4'] = "Step 4";
$lang['trip'] = "Trip";

$lang['season_and_ekd'] = "Season & Contract";
$lang['name'] = "Package / Trip Name";
$lang['the_date'] = "Date";
$lang['safa_ea_season_id'] = "Season";
$lang['safa_ea_id'] = "Contract";
$lang['safa_ea_package_id'] = "UO Package";
$lang['special_package'] = "Private Package";
$lang['moushrif_name'] = "Supervisor name";
$lang['phone'] = "Telephone";
$lang['notes'] = "Remarks";
$lang['safa_trip_confirm_id'] = "External Transport";
$lang['out_transport'] = "External Transportation";

$lang['safa_tripstatus_id'] = "Status";
$lang['erp_transportertype_id'] = "Transportation Method";
$lang['going_trip'] = "Going Trip";
$lang['return_trip'] = "Return Trip";
$lang['trip_no'] = "Trip No.";
$lang['airport_start'] = "Departure Airport";
$lang['airport_date_start'] = "Departure Date & Time";
$lang['airport_end'] = "Arrival Airport";
$lang['airport_date_end'] = "Arrival Date & Time";
$lang['add'] = "Add";
$lang['hotels_and_tourism_places'] = "Hotels and Tourismplaces";
$lang['hotels'] = "Hotels";
$lang['tourism_places'] = "Tourismplaces";
$lang['global_operations'] = 'Operations';
$lang['city'] = "City";
$lang['hotel'] = "Hotel";
$lang['tourism_place'] = "Tourismplace";
$lang['nights_count'] = "Nights";
$lang['meal'] = "Meal";
$lang['checkin_datetime'] = "Checkin Date";
$lang['checkout_datetime'] = "Checkout Date";
$lang['externalsegmenttype'] = "Segments Type";
$lang['trip_code'] = "Trip Code";
$lang['airport_departure_datetime'] = "Departure Date/Time";
$lang['airport_arrival_datetime'] = "Arrival Date/Time";
$lang['transporter'] = "Transporter";

$lang['visit_date_and_time'] = "Checkout date";
$lang['internal_transport'] = "Internal Transportation";

$lang['safa_internalpassagetype_id'] = "Internal segment type";
$lang['internalpassage_start_datetime'] = "Checkin date and time";
$lang['internalpassage_end_datetime'] = "Checkout date and time";
$lang['internalpassage_start_hotel'] = "Start point";
$lang['internalpassage_end_hotel'] = "End point";

$lang['save'] = "Save";
$lang['delete'] = "Delete";

$lang['travellers_count'] = "PAX";

$lang['didnot_start'] = "didn't started yet";
$lang['in_progress'] = "in progress";
$lang['finished'] = "done";

$lang['confirm'] = "Confirm";
$lang['cancel_confirm'] = "cancel";
$lang['room_count'] = "Room Count";
$lang['travellers_adult_count'] = "Adult";
$lang['travellers_child_count'] = "Children";
$lang['travellers_infant_count'] = "Infant";
$lang['erp_country_id'] = "Country";
$lang['trip_internaltrip'] = "Internal Segments";
$lang['ito_id'] = "Internal Segments";
$lang['trip_ito'] = "Internal Transportion Operator";
$lang['generate_trip_internaltrip'] = "Generate Internal Transportion Order";
$lang['please_update_your_internal_trip'] = 'Please update your Internal Transportation Order to be up to date with the trip updates.';

