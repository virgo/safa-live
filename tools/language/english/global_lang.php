<?php defined('BASEPATH') OR exit('No direct script access allowed');

$lang['XALIGN'] = "right";
$lang['ALIGN'] = "left";
$lang['DIR'] = "ltr";

$lang['global_reset'] = "Reset";
$lang['vehicle'] = "vehicle";
$lang['add_driver_info'] = 'add vehicles';
$lang['global_direction'] = "ltr";
$lang['global_lang'] = "en";
$lang['global_select_from_menu'] = "Select from the menu";
$lang['global_added_successfully']='Added sucessfully';
$lang['global_search'] = "Search";
$lang['global_submit'] = "Submit";
$lang['global_ok']="OK";
$lang['global_add'] = "Add";
$lang['global_no_data'] = "No data available.";
$lang['global_select_one_record_at_least'] = "Select one record at least.";
$lang['global_add_new_record'] = "Add New";
$lang['global_delete_selected_records'] = "Are you sure you want to delete  the Selected Records";
$lang['global_are_you_sure_you_want_to_activate']="Are you sure you want to activate";
$lang['global_full_list'] = "Full list";
$lang['global_edit'] = "Edit";
$lang['global_delete'] = "Delete";
$lang['global_close'] = "Close";
$lang['global_cancel'] = "Cancel";
$lang['global_move']='Move';
$lang['global_are_you_sure_you_want_to_delete'] = "Are you sure you want to delete?";
$lang['global_are_you_sure_you_want_to_block'] = "Are you sure you want to Block ?";
$lang['global_are_you_sure_you_want_to_add']='Are you sure you want to save ?';
$lang['global_back'] = "Back";
$lang['send_to_confirm'] = "Send to confirm";
$lang['global_browse'] = "Browse";
$lang['global_save_request'] = "Save Request";
$lang['global_actions'] = "Operations";
$lang['global_logout'] = 'Logout';
$lang['global_inbox'] = 'Inbox';
$lang['global_settings'] = 'Change Password';
$lang['global_are_you_sure_you_want_to_logout'] = 'Are you sure you want to logout?';
$lang['global_activate'] = "Activate";
$lang['global_virgo'] = "Virgo";
$lang['global_system_management'] = "System Management";
$lang['gloabl_operations']='Operations';
$lang['global_delete_confirm'] ="has been deleted";
$lang['global_add_confirm']='has been added';
$lang['global_title'] ="Safa | Technology for leaders in Umrah";
$lang['global_'] = "Safa | ";
$lang['global_added_message'] = 'Added successfully';
$lang['global_updated_message'] = 'Updated successfully';
$lang['global_deleted_message'] = 'Deleted successfully';
$lang['global_cut_message'] = 'Moved successfully';
$lang['global_system_message'] = 'System Message';
$lang['global_error_save'] = 'Error Not Saved';
$lang['global_yes'] = 'Yes';
$lang['global_no'] = 'No';
$lang['global_user_personal_settings']='Update Information Of';
$lang['global_info_not_true']='Password Not Correct';
$lang['global_updated_successfully']='Updated Successfully';
$lang['global_trip_details']='Trip Details';
$lang['global_all']='All';

######################## Menu 
$lang['menu_main_data']="Main Data";
$lang['menu_management_users_virgo']='Management Users Virgo';
$lang['menu_management_users']='User Management';

$lang['menu_technical_data']='Technical Data';
$lang['menu_ksa_agent']='Ksa Agents';
$lang['menu_ksa_agent_privileges']='KSA Agents Privileges';
$lang['menu_ksa_agent_usergroups']='KSA Agents Usergroups';
$lang['menu_external_agent_privileges']='External Agents Privileges';
$lang['menu_external_agent_usergroups']='External Agents Usergroups';
$lang['menu_itos_privileges']='ITOS Privileges';
$lang['menu_itos_usergroups']='ITOS Usergroups';
$lang['menu_external_agent']='External Agents';
$lang['menu_agent_contracts']='Agent Contarcts';
$lang['menu_flights']='Trips';
$lang['menu_internal_transport']='Internal Transport';
$lang['menu_reports']='Reports';
$lang['menu_movement']='Movement';
$lang['menu_management_users']='Users';
$lang['menu_management_usersgroups']='Usergroups';
$lang['menu_main_cities']='Cities';
$lang['menu_main_transport_companies']='Transport Companies';
$lang['menu_main_transport_operators']='Transport Operators';
$lang['menu_main_transportation_outlets']='Transportation Outlets';
$lang['menu_main_hole_airports']='Port Halls';
$lang['menu_main_ports']='Ports';
$lang['menu_main_sights']='Sights';
$lang['menu_main_hotels_levels']='Hotels Levels';
$lang['menu_main_sub_hotels_levels']='Sub Hotel Levels';
$lang['menu_main_hotels_meals']='Hotels Meals';
$lang['menu_main_types_of_meals_hotels']='Hotels Meals Types';
$lang['menu_main_hotels_rooms']='Hotel Rooms Types';
$lang['menu_main_seasons']='Seasons';
$lang['menu_main_hotels_polices']='Hotels Polices';
$lang['menu_main_hotel_marketing_companies']='Hotel Marketing Companies';
$lang['menu_main_types_of_meals_hotels']='Types Of Meals Hotels';
$lang['menu_main_types_of_currencies']='Types of currencies';
$lang['menu_main_types_of_hotel_rooms']='Types Of Hotel Rooms';
$lang['menu_main_types_of_vehcles']='Types of Vehicles';
$lang['menu_main_hotels']='Hotels';
$lang['menu_main_hotel_attributes']='Hotels Attributes';
$lang['menu_main_rooms_attributes']='Rooms Attributes';
$lang['menu_techn_flight_cases']='Flight Cases';
$lang['menu_techn_types_of_transport']='Types Of Transport';
$lang['menu_techn_types_of_internal_transport_clips']='Types Of Internal Transport Clips';
$lang['menu_techn_cases_of_transport_clips']='Cases Of Transport Clips';
$lang['menu_techn_users_Permissions_virgo']='Management users permissions';
$lang['menu_techn_external_agentUsers_permissions']='EA users permissions';
$lang['menu_techn_ksa_agent_permissions']='UO users permission';
$lang['menu_ksa_agent_users']='UO users';
$lang['menu_external_agent_Users']='Users';
$lang['menu_flights_Add']='Add a trip';
$lang['menu_flights_Search']='Find a trip';
$lang['menu_internal_transport_Add']='New transportation order';
$lang['menu_internal_transport_Search']='Search transportation order';
$lang['menu_reports_arrival']="Arrivals Report";
$lang['menu_reports_Departure']='Departure';
$lang['menu_movement_movements']='Movements';
$lang['menu_internal_transport_orders']='Internal Transport Orders';
$lang['current_status']='Current Status';
$lang['menu_safa_itos']='Safa ITOS';
$lang['menu_safa_itos_users']='Itos Users';
$lang['global_this_page_is_under_construction']='This page is under construction';
$lang['global_system_message']='System Message';
$lang['menu_transport_companies_users']='Transport Companies Users';
$lang['menu_transport_companies_info']='Transport Companies Information';
$lang['menu_transport_companies_drivers']='Transport Companies Drivers';
$lang['menu_transport_companies_buses']='Transport Companies Buses';

$lang['menu_main_rooms_services']='Rooms Services';
$lang['menu_hotels']='Hotels';
$lang['menu_search_hotels']='Search Hotels';

$lang['menu_hotels_reservation_orders']='Reservation Orders';
$lang['menu_incoming_hotels_reservation_orders']='Incoming Reservation Orders';
$lang['menu_hotels_availabilty_offers']='Availabilty Offers Settings';


$lang['menu_notifications']='Notifications';
$lang['menu_add_notifications_data']='Notifications Texts';
$lang['menu_add_notifications_translation']='Notifications Translation';


####### EAS #####
$lang['menu_management_users_eas']="System Management";
$lang['menu_contracts_eas']="Contracts";
$lang['menu_contract_phases']='Contracts Phases';

$lang['menu_contracts_report']="Contracts Reports";


$lang['menu_management_users_add']='New User';
$lang['menu_management_users_search']='Search User';
$lang['menu_management_usergroups']='Usergroups';
$lang['menu_contracts_lists']='Contracts List';
$lang['menu_contracts_list_add']='Add Contract';
$lang['menu_trips_eas']='Trips';
$lang['menu_trips_lists']='Trips List';
$lang['menu_trip_add']='Create Trip';
$lang['menu_trip_search']='Search Trip';
$lang['menu_trips_programs']="Trip Packages";
$lang['menu_trips_programs_lists']="Packages List";
$lang['menu_trips_programs_add']="New Trip Package";
$lang['menu_seasons']="Seasons";
$lang['menu_seasons_search']='Search Season';
$lang['menu_seasons_add']='New Season';
$lang['menu_internal_transportation_eas']='Internal Transportation';
$lang['menu_internal_transportation_add']='New Transportation Request';
$lang['menu_internal_transportation_search']='Search Transportation Request';


$lang['menu_safa_umrah_and_mutamers']='Mutamers Groups';
$lang['menu_safa_groups']='Mutamers Groups';
$lang['menu_safa_umrah']='Umrah Groups';
$lang['menu_mutamers_import']='Import Mutamers';


######## UOS ######
$lang['menu_management_users_uos']='System Managment';
$lang['menu_management_uos_users']='Users';
$lang['menu_management_uos_usergroup']='Users groups';
$lang['menu_contracts_uos']='Agents';
$lang['menu_uos_contracts_hotel']='Define hotels';
$lang['menu_uos_contracts_transportation']='Define transporters';
$lang['menu_trips_uos']='Trips';
$lang['menu_uos_trips_search']="Find a trip";
$lang['menu_uos_trips_add']="New trip";
$lang['menu_trips_programs_uos']="Trip packages";
$lang['menu_uos_trips_programs_list']='Packages list';
$lang['menu_uos_trips_programs_add']='New trip package';
$lang['menu_uos_reports']="Reports";
$lang['menu_uos_arrival_report']="Arrival Report";
$lang['menu_uos_departure_report']="Departure Report";
$lang['menu_uos_movements']="Movements";
$lang['menu_uos_current_status']='Current Situation';
$lang['menu_uos_internal_transportation']='Internal Transportation';
##########################
#### ITO Menu
$lang['menu_ito_arriving_report']="Arrival Report";
$lang['menu_ito_departing_report']="Departure Report";
$lang['menu_ito_movements']="all Movements";
$lang['menu_ito_movementsv']="Movements without info";
$lang['menu_ito_currently_report']="Current situation";
$lang['menu_internal_transportation_order']='Search Transportation Order';
$lang['menu_internal_transportation']='Internal Transportation';

$lang['global_arabic_name']="Name (AR)";
$lang['global_english_name']="Name (LA)";


$lang['trips'] = "Trips";
$lang['global_this_page_is_under_construction'] = "This page is under construction";
$lang[''] = "";




######## UOS ######
$lang['menu_management_users_uos']='System Management';
$lang['menu_management_uos_users']='Users';
$lang['menu_management_uos_usergroup']='Usergroups';
$lang['menu_contracts_uos']='Agents';
$lang['menu_uos_contracts_add']='Add Agent';
$lang['menu_uos_contracts_search']='Search Agent';
$lang['menu_uos_contracts_hotel']='Determine Hotels';
$lang['menu_uos_contracts_transportation']='Determine Contract Transportation';
$lang['menu_trips_uos']='Trips';
$lang['menu_uos_trips_search']='Search Trip';
$lang['menu_uos_trips_add']='Add Trip';
$lang['menu_trips_programs_uos']='Trips Programs';
$lang['menu_uos_trips_programs_list']='Programs List';
$lang['menu_uos_trips_programs_add']='Add Trip Program';
$lang['menu_uos_reports']='Reports';
$lang['menu_uos_arrival_report']='Arrival Report';
$lang['menu_uos_departure_report']='Departure Report';
$lang['menu_uos_movements']='Movements';
$lang['menu_uos_current_status']='Current Situation';
##########################
// CALLBACKS
$lang['callback_incorrect_data'] = "Username or password is incorrect";
$lang['global_this_username_exists']='Duplicated username';

define("ALIGN", 'left');
define("XALIGN", 'right');
define("DIR", 'ltr');
//error message permission
$lang['global_you_dont_have_permission']='ليس لديك تصريح دخول';