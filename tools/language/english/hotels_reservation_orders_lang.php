<?php
$lang['title']='Hotels Reservation Orders';

$lang['title_incoming']='Incoming Hotels Reservation Orders';

$lang['main_data']='Main Data';

$lang['nationalities']='Nationalities';
$lang['actions']='Actions';
$lang['add']='Add';
$lang['edit']='Edit';
$lang['delete']='Delete';
$lang['save']='Save';


$lang['add_title']='Add Reservation Order';
$lang['add_saving_availability_title']='Add Saving Availability Order';

$lang['edit_title']='Edit Reservation Order';
$lang['edit_saving_availability_title']='Edit Saving Availability Order';


$lang['delete_title']='Delete Reservation Order';
$lang['details_title']='Reservation Order Details';


$lang['all_nationalities']='All Nationalities';
$lang['arabic_nationalities']='Arabic Nationalities';
$lang['foreign_nationalities']='Foreign Nationalities';
$lang['clear_nationalities']='Clear Nationalities';

$lang['company_type']='Company Type';
$lang['company']='Company';

$lang['order_no']='Order No';
$lang['order_date']='The Date';
$lang['contact_person']='Contact Person';
$lang['city']='City';
$lang['hotel']='Hotel';
$lang['alternative_hotel']='Alternative Hotel';
$lang['arrival_date']='Arrival Date';
$lang['departure_date']='Departure Date';

$lang['general_feature_for_hotel']='General Feature For Hotel';

$lang['distance_from']='Distance From HARAM';
$lang['distance_to']='to';
$lang['meter']='Meter';
$lang['look_to_haram']='Look To Haram';
$lang['price_from']='Price From';
$lang['price_to']='To';
$lang['currency']='Currency';
$lang['nationalities']='Nationalities';
$lang['inclusive_services']='Inclusive Services';
$lang['advantages']='Advantages';

$lang['rooms']='Rooms';
$lang['meals']='Meals';

$lang['count']='Count';
$lang['meal']='Meal';

$lang['nights_count']='Nights Count';
$lang['rooms_count']='Rooms Count';

$lang['housing_type']='Housing Type';
$lang['room_type']='Room Type';
$lang['entry_date']='Entry Date';
$lang['exit_date']='Exit Date';

$lang['ea']='External Agent';
$lang['uo']='Umrah Operator';
$lang['hm']='Hotel Marketing Operator';

$lang['send']='Send';

$lang['available']='Available';
$lang['reset_to_complete_order']='Reset To Complete Order';

$lang['don_not_have_privilege_to_add_reservation_order']='You don not have privilege to add reservation order.';

$lang['address']='Address';
$lang['hotel_level']='Hotel Level';
$lang['price']='Price';
