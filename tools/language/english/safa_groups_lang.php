<?php

defined('BASEPATH') OR exit('No direct script access allowed');

$lang['safa_groups_name'] = 'Group Name';
$lang['safa_groups_name_la'] = 'English Name';
$lang['safa_groups_creation_date'] = 'Creation Date';
$lang['safa_groups_to_date']='To';
$lang['safa_groups_from_date']='From';
$lang['safa_groups_safa_ea_id'] = 'EA Code';
$lang['safa_groups_add_title'] = 'Add group';
$lang['safa_groups_edit_title'] = 'Edit group';
$lang['safa_groups_th_name'] = 'Group Name';
$lang['safa_groups_th_namela'] = 'English Name';
$lang['safa_groups_name_exist'] = 'Group Name Exist';
$lang['safa_groups_name_invalid'] = 'Invalid Name';
$lang['safa_groups_title'] = 'Mutamers Groups';
$lang['safa_groups_mutamer_name']='Mutamer Name';
$lang['safa_groups_passport_no']='Passport Number';
$lang['safa_groups_no_of_mutamers']='No Of Mutamers';
$lang['show_mutamers']='Show Mutamers';

