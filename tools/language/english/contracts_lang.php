<?php

$lang['contracts_add']='ADD Agent';
$lang['contracts_edit']='Edit Agent';
$lang['contracts_name_la']='Name (LA)';
$lang['contracts_name_ar']='Name (AR)';
$lang['contracts_exrenalagent']='Umrah Company';
$lang['contracts_name']='Agent Name ';
$lang['uo_contracts_title']='Agents';


$lang['contracts_username']='UserName';
$lang['contracts_password']='PassWord';
$lang['contracts_repeat_password']='Confirm Password';
$lang['contracts_email']='E_Mail';
$lang['contracts_country']='Country';
$lang['contracts_city']='City';

$lang['contracts_address']='Address';
$lang['contracts_phone']='Phone';
$lang['contracts_fax']='Fax';
$lang['contracts_nationality']='Nationality';
$lang['username_validation']='اسم  المستخدم غير صحيح ';
$lang['contracts_ksaaddress']='Ksa Address';
$lang['contracts_ksaphone']='Ksa Phone';
$lang['contracts_agencyname']='Agency Name';
$lang['contracts_agencysymbol']='Agency Symbol';
$lang['contracts_ayata_num']='Ayata Number';
$lang['contracts_resposible_name']='Responsible Name';
$lang['contracts_resposible_phone']='Responsible Phone';
$lang['contracts_resposible_email']='Responsible Email';
$lang['contracts_resposible_passport_photo_path']='Passport Photo';
$lang['choose_photo']='Choose Photo';

$lang['contract_notes']='Notes';
$lang['contract_step1']='Basic Info';
$lang['contract_step2']='Company Info';
$lang['contract_step3']='Manager Info';
$lang['contract_save']='Save';
$lang['uo_contract']='Umrah Operator';
$lang['contracts_unique_username']='Username Must be Unique';
$lang['companies_contracts']='Umrah Operator contracts';
$lang['node_title']='Agents';
$lang['contracts_ito_name']='ITOS';
$lang['contracts_hotels_name']='Hotel';
$lang['contracts_hotels_arabic_name']='Arabic Name';
$lang['contracts_hotels_english_name']='English Name';
$lang['contracts_hotels_city']='City';
$lang['contracts_hotels_hotel_level']='Hotel Level';

$lang['uo_contracts_itos_title']='Agents';

$lang['ito_exist']='Ito Has Already Exist';
$lang['add_contracts_ito_successfully']='Ito Has succesfully Added';

$lang['delete_contracts_ito_successfully']='Ito Has succesfully Removed';
$lang['contract_itos']='ITOS';
$lang['contract_hotels']='Hotels';

$lang['connect_contract']='Connect Contract';
$lang['hotels']='With Hotels';

$lang['safa_uo_contracts_status_id']='Contract Status';
$lang['safa_contract_phases_id_current']='Contract Current Phase';
$lang['safa_contract_phases_id_next']='Contract Next Phase';

$lang['contract_approving']='Contract Approving';
$lang['view_ea_documents']='View EA Documents';

$lang['contract_phases_history']='Contract Phases History';

$lang['new_contract_data_email_subject']='Contract Login Data';
$lang['new_contract_data_email_body']= "Hi,<br />
<br />
New contract login data.<br/>
Click here to go to login page<br/>
 <a href=\"{link}\">{link}</a><br/>
 <br />
By the way your contract login data are:</br>
<table>
 <tr>
<td>
{username}
</td>
<td>
Username: 
</td>
</tr>
<tr>
<td>
{password}
</td>
<td>
Password: 
</td>
</tr>
</table>
Sincerly,</br>
Safa Team</br>
";

$lang['can_over_limit_description']='When you select this option, System will allow to contract to by any service without considering his credit';

