<?php
$lang['wellcom_title'] = 'Welcom. Please Log In';
$lang['login_user_name'] = 'User Nmae';
$lang['login_password'] = 'Password';
$lang['login_rememberme'] = 'Remember Me';
$lang['login_email'] = 'E-mail';
$lang['login_continue'] = 'Continue';
$lang['login_label'] = 'Log In';
$lang['login_forget_password'] = 'Forgot your password';
$lang['registration'] = 'Registration';

$lang['recover_password'] = 'Please enter your email address to recover your password';
$lang['login_recover'] = 'recover';
$lang['login_back'] = 'Back';
$lang['login_placeholder_username'] = 'Username';
$lang['login_placeholder_password'] = 'Password';
$lang['login_placeholder_email'] = 'E-mail';
$lang['login_forget_password_q'] = 'Forgot your password?';
$lang['contracts_agent_code'] = 'كود العميل';
$lang['ver_code_instruction']='An email has been send ,Checvk your email inbox';
$lang['reset_password']='reset password';
$lang['code']='code';
$lang['login_code']= 'License Number';
$lang['login_newpass']='password';
$lang['login_confpass']='confirm password';
$lang['login_incorrect_vercode']='incorrect verfication code';
$lang['send_mail_error']='Error sending email ,Try again later';
$lang['mail_send_to']='Send me';
$lang['login_resetpass']='Login resetpassword';
$lang['login_title_error']='Login Error';
$lang['global_err_invalid_email'] = 'Invalid E-mail';
$lang['global_check_your_email']='Check your inbox email';
$lang['global_forget_subject_msg']="Reset your Paaword";
$lang['global_forgot_pass_msg'] = "Hi,<br />
<br />
You have requested for changing your login password in Safa</br> 
Click here to reset your password</br>
 <a href=\"{link}\">{link}</a><br />
 <br />
By the way your login data are:</br>
<table>
 <tr>
<td>
{username}
</td>
<td>
Username: 
</td>
</tr>
</table>
Sincerly,</br>
Safa Team</br>
";
$lang['global_forget_password_enter_email'] = 'E-mail';
$lang['global_check_your_email'] = 'Check your inbox e-amil,we have send you a link to reset your password.';

$lang['reset_password_time_out'] = 'Time out for this link ,Please try again later. ';
$lang['reset_password_new_password'] = 'please enter new password';
$lang['reset_password_confirm_password'] = 'reenter password';
$lang['reset_password_congrates'] = 'Password has been changed successfully';
$lang['reset_password_congrates_title'] = 'Password has been changed successfully';
$lang['callback_activation_error'] = "this Account has been blocked, please contact technical support";

$lang['name_la']='Name (LA)';
$lang['name_ar']='Name (AR)';
$lang['username']='UserName';
$lang['password']='PassWord';
$lang['repeat_password']='Confirm Password';
$lang['email']='E-Mail';
$lang['country']='Country';
$lang['phone']='Phone';
$lang['fax']='Fax';


$lang['username_available']='Username available';
$lang['username_not_available']='Username not available';

$lang['contract_not_available']='There is no contract with this username and password';


$lang['contract_username']='Contract username';
$lang['contract_password']='Contract password';