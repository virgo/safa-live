<?php
$lang['safa_uos_add']='Add Form';
$lang['safa_uos_name_ar']='Arabic Name';
$lang['safa_uos_name_la']='English Name';
$lang['safa_uos_code']='Code';
$lang['safa_uos_phone']='Phone';
$lang['safa_uos_mobile']='Mobile';
$lang['safa_uos_email']='Email';
$lang['safa_uos_fax']='Fax';
$lang['safa_uos_contract_num']='Number of contracts';
$lang['safa_uos_trips_num']='Number of Trips';
