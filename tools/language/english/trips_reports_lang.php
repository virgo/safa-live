<?php defined('BASEPATH') OR exit('No direct script access allowed');
$lang['XALIGN'] = "right";
$lang['ALIGN'] = "left";
$lang['DIR'] = "ltr";


$lang['curent_report'] = "current report";
$lang['arriving_report'] = "arriving report";
$lang['departing_report'] = "departing report";
$lang['all_movements_report'] = "all movements report";
$lang['uo_companies'] = "umara companies";
$lang['ito_companies'] = "operator companies";


$lang['from'] = "from";
$lang['to'] = "to";
$lang['search'] = "search";
$lang['date'] = "date";
$lang['time'] = "time";
$lang['count'] = "count";
$lang['contract'] = "contract";
$lang['trans_company'] = "transition company";
$lang['nationality'] = "nationality";
$lang['port'] = "port";
$lang['city'] = "city";
$lang['hotel'] = "hotel";
$lang['choose_cols'] = "choose cols";
$lang['trip'] = "trip";
$lang['trip_id'] = "trip no";
$lang['dep_date'] = "departure date";
$lang['dep_time'] = "departure time";
$lang['arr_date'] = "arrival date";
$lang['arr_time'] = "arrival time";
$lang['supervisor'] = "supervisor";
$lang['departing_form_name'] = "departure trips";
$lang['arriving_form_name'] = "arrival trips";
$lang['today'] = "today trips";
$lang['twm'] = "tomorrow trips";

//all movements
$lang['all_movements'] = "all movements";
$lang['operator'] = "operator";
$lang['operation_order_id'] = "operation order";
$lang['segment_type'] = "type";
$lang['starting'] = "date";
$lang['ending'] = "ending";
$lang['start_hotel'] = "start hotel";
$lang['end_hotel'] = "end hotel";
$lang['tourism_place'] = "tourism place";
$lang['seats'] = "seats";
$lang['from_hotel'] = "from hotel";
$lang['to_hotel'] = "to hotel";
$lang['from_city'] = "from city";
$lang['to_city'] = "to city";

$lang['print'] = 'print';
$lang['export'] = 'export';

$lang['agent'] = 'Agent';
$lang['uo_user_agent_notexist'] = 'Not Set';
$lang['assign_uo_user'] = 'Set';
$lang['change_uo_user'] = 'Change';
$lang['roll_back_user'] = 'Remove';


$lang['status'] = 'status';
$lang['drivers']='Drivers';
$lang['no_drivers']='No Drivers';
$lang['driver_title']='Drivers';
$lang['driver_name']='Driver Name';
$lang['driver_phone']='Phone';
$lang['driver_vehicle_number']='Car Number';
$lang['driver_vehicle_type']='Car Type';
$lang['driver_info']='Driver Info';
$lang['update_segment_status']='Update Status';
$lang['driver_name_invalid']='Invalid Name Field';
$lang['driver_vehicle_number_invalid']='Invalid Car Number Field';
$lang['driver_vehicle_type_invalid']='Invalid Car Type Field';

$lang['drivers_and_buses']='Drivers And Buses';
$lang['driver'] = 'Driver';
$lang['bus'] = 'Bus';
$lang['driver_data'] = 'Driver Data';
$lang['bus_data'] = 'Bus Data';
$lang['actions'] = 'Actions';