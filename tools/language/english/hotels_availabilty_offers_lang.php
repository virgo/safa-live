<?php

$lang['title']='Hotels Availabilty Offer Settings';

$lang['actions']='Actions';
$lang['add']='Add';
$lang['edit']='Edit';
$lang['delete']='Delete';
$lang['save']='Save';


$lang['add_title']='Add Availabilty Offer';
$lang['edit_title']='Edit Availabilty Offer';
$lang['delete_title']='Delete Availabilty Offer';

$lang['main_data']='Main Data';

$lang['availabilty_offer_owner_type']='Company Type';
$lang['availabilty_offer_owner_id']='Company';

$lang['name_ar']='Arabic Name';
$lang['name_la']='English Name';
$lang['nights_count']='Nights Count';
$lang['pricing_methodology']='Pricing Methodology';

$lang['exclude_week_end_days']='Execlude Week End Days If Exist In Rquired Length';
$lang['include_week_end_days']='Include Week End Days If Exist In Rquired Length';

$lang['discount_percent']='Discount Percent';
$lang['discount_fixed']='Discount Value';
$lang['rounding_value']='Rounding For';


$lang['ea']='External Agent';
$lang['uo']='Umrah Operator';
$lang['hm']='Hotel Marketing Operator';

$lang['save']='Save';

