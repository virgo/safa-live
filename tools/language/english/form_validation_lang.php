<?php

defined('BASEPATH') OR exit('No direct script access allowed');

$lang['form_validation_valid_phone'] = 'The {field} field must contain a valid phone number.';
$lang['form_validation_valid_date'] = 'The {field} field must contain a valid date.';
$lang['form_validation_complex_password'] = 'The {field} field must contain complex password.';
$lang['form_validation_dropdown'] = 'The {field} menu must contain a valid value.';
$lang['form_validation_max_dblength']= "The {field} filed most be {param} length";
$lang['form_validation_unique_col'] = "{field} is already used.";
$lang['form_validation_unique_by_ea'] = "{field} is already used.";



/* End of file form_validation_lang.php */
/* Location: ./application/language/english/form_validation_lang.php */