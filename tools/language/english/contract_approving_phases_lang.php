<?php
$lang['menu_title']='Contract Approving';
$lang['title']='Contract Approving';
$lang['main_data']='Main Data';

$lang['required_documents']='Required Documents';
$lang['uo_documents']='UO Documents';

$lang['contract_code']='Contract Code';
$lang['contract_name']='EA Name';

$lang['current_phase']='Current Phase';
$lang['next_phase']='Next Phase';
$lang['contracts_status']='Contract Status';

$lang['actions']='Actions';

$lang['add']='Add';
$lang['edit']='Edit';
$lang['delete']='Delete';
$lang['save']='Save';

$lang['remove_all']='Remove All';

$lang['document_name']='Document Name';
$lang['upload_file']='Upload file';

$lang['remove_file']='Remove File';

$lang['confirm']='Confirm';

$lang['status']='Status';
$lang['refuse_reason']='Refusal Reason';

$lang['previous_phases']='Previous Phases';

$lang['username']='Username';
$lang['password']='Password';

$lang['contract_open']='Open Contract';
$lang['contract_opened_successfully_message']='Contract was added successfully';

$lang['invalid_username_or_password']='Invalid username or password';
$lang['contract_already_opened_message']='Contract already added';

$lang['contract_approving_reports']='Contracts Approvings Report';

$lang['name_la']='Name (LA)';
$lang['name_ar']='Name (AR)';
$lang['country']='Country';
$lang['ea_data']='EA Data';
$lang['contract_phases']='Contract Phases';
