<?php

$lang['ea_ports_add_title'] = 'Add Airport';
$lang['ea_ports_edit_title'] = 'Edit Airport';
$lang['ea_ports_erp_port_id'] = 'المنافذ';
$lang['ea_ports_name_ar'] = 'Arabic Name';
$lang['ea_ports_name_la'] = 'English Name';
$lang['ea_ports_code'] = 'International Code';
$lang['ea_ports_country_code'] = 'Country Code';
$lang['ea_ports_world_area_code'] = 'World Area Code';
$lang['ea_ports_city_id'] = 'City Name';
$lang['ea_ports_country_name'] = 'Country Name';
$lang['ea_ports_th_city_name'] = 'City Name';
$lang['ea_ports_th_country_name'] = 'Country Name';
$lang['ea_ports_th_name'] = 'Airport Name';
$lang['ea_ports_name_exist'] = 'Port Name Exist';
$lang['ea_ports_select_from_menu_cities'] = "Choose country First";
$lang['ea_ports_select_from_menu_false'] = "City field is required.";
