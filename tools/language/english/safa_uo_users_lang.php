<?php
$lang['uo_users']='Umrah Operator Users';
$lang['uo_users_name_la']='Name(LA)';
$lang['uo_users_name_ar']='Name(AR)';
$lang['uo_users_username']='UserName';
$lang['uo_users_password']='Password';
$lang['uo_users_repeat_password']='Confirm Password';
$lang['uo_users_safa_uo_id']='Umrah Operator';
$lang['uo_users_safa_uo_usergroup_id']='Users Groups';
$lang['operations']='Operations';
$lang['add_uo_users']='Add User';
$lang['edit_uo_users']='Edit User';
$lang['uo_users_email']='E-Mail';
$lang['uo_users_mobile']='Mobile';
$lang['uo_users_confirm_password']='Confirm Password';
$lang['edit_uo_users']='Edit User';

