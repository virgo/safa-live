<?php
/*
 *english
 */
$lang['bulid_hotel_title']='Build your Hotel';
$lang['bulid_hotel_edit']='Edit your Hotel';
$lang['general_info']='General Information';
$lang['contact_official_info']='Official Contact Info';
$lang['hotel_label']='Hotel';
$lang['city_label']='City';
$lang['Company_label']='Company';
$lang['Company_type_label']='Company Type';
$lang['Company_hotel_policy']='The Hotel Policy';
$lang['Company_rooms_info']='Company Rooms';
$lang['rooms_floor']= 'Floor';
$lang['rooms_section']='Section';
$lang['rooms_count']='num of rooms';
$lang['beds_count']='num of beds';
$lang['date_from']='Date From';
$lang['date_to']='Date To';



$lang['compare_dates']='The Dates incorrect';
$lang['policy_fieldtype']='Field Type';

